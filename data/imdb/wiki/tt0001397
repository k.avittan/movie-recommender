A sister is a woman or girl who shares one or more parents with another individual. The male counterpart is a brother. Although the term typically refers to a familial relationship, it is sometimes used endearingly to refer to non-familial relationships. A full sister is a first degree relative.


== Overview ==

The English word sister comes from Old Norse systir which itself derives from Proto-Germanic *swestēr,  both of which have the same meaning, i.e. sister. Some studies have found that sisters display more traits indicating jealousy around their siblings than their male counterparts, brothers. In some cultures, sisters are afforded a role of being under the protection by male siblings, especially older brothers from issues ranging from bullies or sexual advances by womanizers. In some quarters the term sister has gradually broadened its colloquial meaning to include individuals stipulating kinship. In response, in order to avoid equivocation, some publishers prefer the usage of female sibling over sister. Males with a twin sister sometimes view her as their female alter ego, or what they would have been like, if they had two X chromosomes. A study in Perth Australia found that girls having only youngers brothers resulted in a chastity effect, losing their virginity on average more than a year later than average. This has been hypothesized as being attributed to the pheromones in their brothers' sweat and household-related errands..


== Sororal relationships ==
Various studies have shown that an older sister is likely to give a varied gender role to their younger siblings as well as being more likely to develop a close bond with their younger siblings. Older sisters are more likely to play with their younger siblings. Younger siblings display a more needy behavior when in close proximity to their older sister and are more likely to be tolerant of an older sister's bad behavior. Boys with only an older sister are more likely to display stereotypically male behavior, and such masculine boys increased their masculine behavior with the more sisters they have. The reverse is true for young boys with several sisters, as they tend to be feminine, however, they outgrow this by the time they approach pubescence. Boys with older sisters were less likely to be delinquent or have emotional and behavioral disorders. A younger sister is less likely to be scolded by older siblings than a younger brother. The most common recreational activity between older brother/younger sister pairs is art drawing. Some studies also found a correlation between having an older sister and constructive discussions about safe sexual practices. Some studies have shown that men without sisters are more likely to be ineffectual at courtship and romantic relationships.


== Famous sisters ==

Awan and Azura, daughters of Adam and Eve
Mary I of England and Elizabeth I of England, royalty
Caroline, Princess of Hanover and Princess Stéphanie of Monaco
Tamar Braxton, Toni Braxton, Towanda Braxton, Traci Braxton, and Trina Braxton, known as The Braxtons
Jacqueline Bouvier Kennedy Onassis and Lee Radziwill
Mary and Eliza Chulkhurst, conjoined twins are known as the Biddenden Maids
Jackie Collins, author and Joan Collins, actress
Elizabeth Parke Custis Law, Martha Parke Custis, and Eleanor Parke Custis Lewis, granddaughter of Martha Washington
Annie Elizabeth Delany and Sarah Louise Delany, the subjects of Having Our Say (1993)
Hilary Duff, actress, and Haylie Duff, singer
Eva Gabor, Magda Gabor, and Zsa Zsa Gabor, actresses and singers
Melissa Gilbert and Sara Gilbert, actresses
Ilona and Judit Gófitz, conjoined twins
Sarah Moore Grimké (1792–1873) and Angelina Emily Grimké (1805–1879), known as the Grimké sisters, the first American female advocates of abolition and women's rights
Mariel Hemingway and Margaux Hemingway
Abby and Brittany Hensel; conjoined twins
Paris Hilton and Nicky Hilton, socialites
Maureen "Rebbie" Jackson, La Toya Jackson, and Janet Jackson, members of the Jackson family
Lynda Bird Johnson Robb and Luci Baines Johnson, daughter of Lyndon B. Johnson
Khloe Kardashian, Kim Kardashian, Kourtney Mary Kardashian, reality TV stars
Rose Marie "Rosemary" Kennedy, Kathleen Agnes "Kick" Kennedy Cavendish, Eunice Mary Kennedy Shriver, Patricia Helen "Pat" Kennedy Lawford, and Jean Ann Kennedy Smith, members of the Kennedy family
Beyoncé and Solange Knowles
Caroline Manzo and Dina Manzo, reality TV stars who married brothers Albert and Tommy Manzo
Millie and Christine McCoy, conjoined twins
Brooklyn and Bailey McKnight; American YouTubers
Grand Duchess Olga Nikolaevna of Russia, Grand Duchess Tatiana Nikolaevna of Russia, Grand Duchess Maria Nikolaevna of Russia and Grand Duchess Anastasia Nikolaevna of Russia, also known by the acronym OTMA, daughters of the last Emperor of the Russian Empire
Patricia "Tricia" Nixon Cox and Julie Nixon Eisenhower, daughter of Richard Nixon
Ashley Olsen and Mary-Kate Olsen, also known as "The Olsen Twins", twin actresses and fashion designers, and Elizabeth Olsen, actress
Anita Pointer, Bonnie Pointer, Issa Pointer, June Pointer, Ruth Pointer, and Sadako Pointer, at various times members of the singing group known as The Pointer Sisters
Kim Richards and Kyle Richards, actresses and reality TV stars
Jessica and Krystal Jung, Korean-American singer-songwriters and actresses
Angelica Schuyler Church, Elizabeth Schuyler Hamilton, and Peggy Schuyler
Jessica Simpson and Ashlee Simpson, singer-songwriters
Britney Spears, singer, and Jamie Lynn Spears, actress
Johanna, Martina, Maria, Hedwig, Agathe, Rosemarie, and Eleonore ("Lorli") Von Trapp, daughters of Georg von Trapp
Venus Williams and Serena Williams, tennis players known as Williams sisters
Priscilla "CeCe" Winans, Angelique Winans-Caldwell, Debra Renee Winans-Lowe, members of Winans family of singers and musiciansLillian Gish And Dorothy Gish: Silent Film Actresses


== Fictional works about sisters ==


=== Films ===
What Ever Happened to Baby Jane? (1962 film)
Hannah and Her Sisters (1986), film
Hanging Up (2000), film
Frozen (2013 film)
Sisters (2015), comedy film


=== Literature ===
Little Women
Laura Lee Hope's Bobbsey Twins novels, which included two sets of fraternal twins: 12-year-old Nan and Bert, and six-year-old Flossie and Freddie
In Her Shoes (2002), novel
#Toots (2019), novel


=== Television ===
Hope & Faith, American sitcom
Sisters (TV series)
What I Like About You, TV series
Sister, Sister, TV series


== See also ==
Brother
Sisterhood (disambiguation)


== References ==


== External links ==
 The dictionary definition of sister at Wiktionary