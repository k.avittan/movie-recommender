Of Mice and Men is a play adapted from John Steinbeck's 1937 novel of the same name. The play, which predates the Tony Awards and the Drama Desk Awards, earned the 1938 New York Drama Critics' Circle Best Play.


== Background ==
The 1937 production opened while the novel was still on best seller lists. At the time, George S. Kaufman was the top director in the country. While the play follows the novel closely, Steinbeck altered the character of Curley's Wife, perhaps in response to criticisms from friends. In the play, Curley's wife does not threaten to have Crooks lynched, and in her final scene she talks of her childhood and her father trying to run away with her. This has the effect of softening her character, portraying her as lonely and misunderstood.


== Plot ==

George, an affable migrant farm worker, and Lennie, a towering simple-minded pleasantly humble young man, are the subjects. They are bound by George's devotion and Lennie's "pathetic helplessness". George's guardianship keeps Lennie out of trouble, but we soon see this is a slippery slope. Lennie's displays of love result in several deaths ranging from mice and puppies to a beautiful woman. Eventually, in the face of a lynch mob, George kills Lennie to put him out of his misery.


== Productions ==
Steinbeck adapted the play from the novel.The play had its world premiere circa October 1937 by the San Francisco Theatre Union The play premiered on Broadway at the Music Box Theatre on November 23, 1937 and closed in May 1938 after 207 performances. Directed by George S. Kaufman, the cast starred Broderick Crawford as Lennie and Wallace Ford as George. In 1939 the production was moved to Los Angeles, still with Wallace Ford in the role of George, but with Lon Chaney, Jr., taking on the role of Lennie. Chaney's performance in the role resulted in his casting in the movie.
There have been several revivals, the most recent produced in 2014, directed by Anna D. Shapiro with James Franco (George), Chris O'Dowd (Lennie) and Leighton Meester (Curley's Wife).By the Book Theatre's production won 6 Brickenden Awards including Outstanding Drama, Director, Set Design, Actor, Supporting Actor, and Lighting Design.


== Historical casting ==
The following tables show the casts of the principal original productions:


== Awards ==
The production was chosen as Best Play in 1938 by the New York Drama Critics' Circle. The 2014 production earned two Tony Award nominations at the 68th Tony Awards (O'Dowd—Leading Actor and Japhy Weideman—Lighting Design).


== Reception ==
Brooks Atkinson of The New York Times wrote that "Steinbeck has caught on paper two odd and lovable farm vagrants whose fate is implicit in their characters."


== Notes ==


== External links ==
Of Mice and Men (1937 Broadway production) at the Internet Broadway Database
Of Mice and Men (1974 Broadway production) at the Internet Broadway Database
Of Mice and Men at the Internet Off-Broadway Database