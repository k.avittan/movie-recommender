The Barbary sheep (Ammotragus lervia), also known as the arrui or aoudad, is a species of caprid native to rocky mountains in North Africa.  Six subspecies have been described.  Although it is rare in its native North Africa, it has been introduced to North America, southern Europe, and elsewhere.  It is also known in the Berber language as awdad, waddan, arwi, and arrwis.


== Description ==
Barbary sheep stand 60 to 90 cm (2.0 to 3.0 ft) tall at the shoulder, with a length around 1.5 m (4 ft 11 in), and weigh 40 to 140 kg (88 to 309 lb).  They are sandy-brown, darkening with age, with a slightly lighter underbelly and a darker line along the back. Upper parts and the outer parts of the legs are a uniform reddish- or grayish-brown. Some shaggy hair is on the throat (extending down to the chest in males) with a sparse mane. Their horns have a triangular cross-section.  The horns curve outward, backward, then inward, and can exceed 76 cm (30 in) in length. The horns are fairly smooth, with slight wrinkles evident at the base as the animal matures.


== Range ==


=== Natural range ===
Barbary sheep naturally occur in northern Africa in Algeria, Tunisia, northern Chad, Egypt, Libya, northern Mali, Mauritania, Morocco, Niger, and Sudan (west of the Nile, and in the Red Sea Hills east of the Nile).


=== Introduced populations ===

Barbary sheep have been introduced to southeastern Spain, the southwestern United States (Chinati Mountains on La Escalera Ranch, Guadalupe Mountains National Park, Palo Duro Canyon, the Trans-Pecos, and other parts of Texas and New Mexico), Niihau Island (Hawaii), Mexico, and some parts of Africa.
They have become common in a limited region of southeastern Spain, since its introduction in 1970 to Sierra Espuña regional park as a game species.  Its adaptability enabled it to colonise nearby areas quickly, and private game estates provided other centers of dispersion.  The species is currently expanding, according to recent field surveys, now being found in the provinces of Alicante, Almería, Granada, and Murcia.  This species is a potential competitor to native ungulates inhabiting the Iberian Peninsula.  The species has also been introduced to La Palma (Canary Islands), and has spread throughout the northern and central parts of the island, where it is a serious threat to endemic vegetation.


== Taxonomy ==

A. lervia is the only species in the genus Ammotragus.  However, some authors include this genus in the goat genus Capra, together with the sheep genus Ovis.The subspecies are found allopatrically in various parts of North Africa:
A. l. lervia Pallas, 1777 (vulnerable)
A. l. ornata I. Geoffroy Saint-Hilaire, 1827 (Egyptian Barbary sheep, thought to be extinct in the wild but still found in the eastern desert of Egypt)
A. l. sahariensis Rothschild, 1913 (vulnerable)
A. l. blainei Rothschild, 1913 (vulnerable)
A. l.  angusi Rothschild, 1921 (vulnerable)
A. l. fassini Lepri, 1930 (vulnerable)


== Habitats ==

Barbary sheep are found in arid mountainous areas where they graze and browse grasses, bushes, and lichens.  They are able to obtain all their metabolic water from food, but if liquid water is available, they drink and wallow in it. Barbary sheep are crepuscular  - active in the early morning and late afternoon and rest in the heat of the day. They are very agile and can achieve a standing jump over 2 metres (7 ft).  They are well adapted to their habitat, which consist of steep, rocky mountains and canyons. They often flee at the first sign of danger, typically running uphill. They are extremely nomadic and travel constantly via mountain ranges.  Their main predators in North Africa were the Barbary leopard, the Barbary lion, and caracal, but now only humans threaten their populations.


== Names ==
The binomial name Ammotragus lervia derives from the Greek ἄμμος ámmos ("sand", referring to the sand-coloured coat) and τράγος trágos ("goat").
Lervia derives from the wild sheep of northern Africa described as "lerwee" by Rev. T. Shaw in his "Travels and Observations" about parts of Barbary and Levant.
The Spanish named this sheep the arruis, from Berber arrwis, and the Spanish Legion even used it as a mascot for a time.
Aoudad ([ˈɑː.uːdæd]) is the name for this sheep used by the Berbers, a North African people, and it is also called arui and waddan (in Libya).
A group of aoudad is referred to as an "anger".


== Gallery ==

		
		
		
		
		
		
		
		
		


== References ==


== Further reading ==
Cassinello, J. (1998). Ammotragus lervia: a review on systematics, biology, ecology and distribution. Annales Zoologici Fennici 35: 149-162
Cassinello, J. (2013). Ammotragus lervia: 595–599. In: Mammals of Africa. Vol VI. Pigs, Hippopotamuses, Chevrotain, Giraffes, Deer and Bovids. JS Kingdon & M Hoffmann (Eds.) Bloomsbury Publishing, London.
Cassinello, J. (2015). Ammotragus lervia (aoudad). In: Invasive Species Compendium. http://www.cabi.org/isc/datasheet/94507 CAB International, Wallingford, UK.
Wacher, T., Baha El Din, S., Mikhail, G. & Baha El Din, M. (2002). New observations of the "extinct" Aoudad Ammotragus lervia ornata in Egypt. Oryx 36: 301–304.


== External links ==
A Spanish site on complete biological information about the Aoudad
The Ultimate Ungulate entry on Barbary Sheep
Barbary Sheep in Texas
Barbary Sheep in Sahara