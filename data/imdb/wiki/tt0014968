The term happiness is used in the context of mental or emotional states, including positive or pleasant emotions ranging from contentment to intense joy. It is also used in the context of life satisfaction, subjective well-being, eudaimonia, flourishing and well-being.Since the 1960s, happiness research has been conducted in a wide variety of scientific disciplines, including gerontology, social psychology and positive psychology, clinical and medical research and happiness economics.


== Definitions ==
'Happiness' is the subject of debate on usage and meaning, and on possible differences in understanding by culture.The word is mostly used in relation to two factors:

the current experience of the feeling of an emotion (affect) such as pleasure or joy, or of a more general sense of 'emotional condition as a whole'. For instance Daniel Kahneman has defined happiness as "what I experience here and now". This usage is prevalent in dictionary definitions of happiness.
appraisal of life satisfaction, such as of quality of life. For instance Ruut Veenhoven has defined happiness as "overall appreciation of one's life as-a-whole." Kahneman has said that this is more important to people than current experience.Some usages can include both of these factors. Subjective well-being (swb) includes measures of current experience (emotions, moods, and feelings) and of life satisfaction. For instance Sonja Lyubomirsky has described happiness as “the experience of joy, contentment, or positive well-being, combined with a sense that one's life is good, meaningful, and worthwhile.” Eudaimonia, is a Greek term variously translated as happiness, welfare, flourishing, and blessedness. Xavier Landes has proposed that happiness include measures of subjective wellbeing, mood and eudaimonia.These differing uses can give different results. For instance the correlation of income levels has been shown to be substantial with life satisfaction measures, but to be far weaker, at least above a certain threshold, with current experience measures. Whereas Nordic countries often score highest on swb surveys, South American countries score higher on affect-based surveys of current positive life experiencing.The implied meaning of the word may vary depending on context, qualifying happiness as a polyseme and a fuzzy concept.
A further issue is when measurement is made; appraisal of level of happiness at the time of the experience may be different from appraisal via memory at a later date.Some users accept these issues, but continue to use the word because of its convening power.


== Philosophy ==

Philosophy of happiness is often discussed in conjunction with ethics. Traditional European societies, inherited from the Greeks and from Christianity, often linked happiness with morality, which was concerned with the performance in a certain kind of role in a certain kind of social life. However, with the rise of individualism, begotten partly by Protestantism and capitalism, the links between duty in a society and happiness were gradually broken. The consequence was a redefinition of the moral terms.  Happiness is no longer defined in relation to social life, but in terms of individual psychology. Happiness, however, remains a difficult term for moral philosophy. Throughout the history of moral philosophy, there has been an oscillation between attempts to define morality in terms of consequences leading to happiness and attempts to define morality in terms that have nothing to do with happiness at all.In the Nicomachean Ethics, written in 350 BCE, Aristotle stated that happiness (also being well and doing well) is the only thing that humans desire for their own sake, unlike riches, honour, health or friendship. He observed that men sought riches, or honour, or health not only for their own sake but also in order to be happy. For Aristotle the term eudaimonia, which is translated as 'happiness' or 'flourishing' is an activity rather than an emotion or a state. Eudaimonia (Greek: εὐδαιμονία) is a classical Greek word consists of the word "eu" ("good" or "well-being") and "daimōn" ("spirit" or "minor deity", used by extension to mean one's lot or fortune). Thus understood, the happy life is the good life, that is, a life in which a person fulfills human nature in an excellent way. Specifically, Aristotle argued that the good life is the life of excellent rational activity.  He arrived at this claim with the "Function Argument". Basically, if it is right, every living thing has a function, that which it uniquely does. For Aristotle human function is to reason, since it is that alone which humans uniquely do. And performing one's function well, or excellently, is good. According to Aristotle, the life of excellent rational activity is the happy life. Aristotle argued a second best life for those incapable of excellent rational activity was the life of moral virtue.Western ethicists have made arguments for how humans should behave, either individually or collectively, based on the resulting happiness of such behavior. Utilitarians, such as John Stuart Mill and Jeremy Bentham, advocated the greatest happiness principle as a guide for ethical behavior.Friedrich Nietzsche critiqued the English Utilitarians' focus on attaining the greatest happiness, stating that "Man does not strive for happiness, only the Englishman does." Nietzsche meant that making happiness one's ultimate goal and the aim of one's existence, in his words "makes one contemptible." Nietzsche instead yearned for a culture that would set higher, more difficult goals than "mere happiness." He introduced the quasi-dystopic figure of the "last man" as a kind of thought experiment against the utilitarians and happiness-seekers. these small, "last men" who seek after only their own pleasure and health, avoiding all danger, exertion, difficulty, challenge, struggle are meant to seem contemptible to Nietzsche's reader. Nietzsche instead wants us to consider the value of what is difficult, what can only be earned through struggle, difficulty, pain and thus to come to see the affirmative value suffering and unhappiness truly play in creating everything of great worth in life, including all the highest achievements of human culture, not least of all philosophy.In 2004 Darrin McMahon claimed, that over time the emphasis shifted from the happiness of virtue to the virtue of happiness.


== Culture ==
Personal happiness aims can be effected by cultural factors. Hedonism appears to be more strongly related to happiness in more individualistic cultures.Cultural views on happiness have changed over time. For instance Western concern about childhood being a time of happiness has occurred only since the 19th century.Not all cultures seek to maximise happiness, and some cultures are averse to happiness.


== Religion ==

People in countries with high cultural religiosity tend to relate their life satisfaction less to their emotional experiences than people in more secular countries.


=== Eastern religions ===


==== Buddhism ====

Happiness forms a central theme of Buddhist teachings. For ultimate freedom from suffering, the Noble Eightfold Path leads its practitioner to Nirvana, a state of everlasting peace. Ultimate happiness is only achieved by overcoming craving in all forms. More mundane forms of happiness, such as acquiring wealth and maintaining good friendships, are also recognized as worthy goals for lay people (see sukha). Buddhism also encourages the generation of loving kindness and compassion, the desire for the happiness and welfare of all beings.


==== Hinduism ====
In Advaita Vedanta, the ultimate goal of life is happiness, in the sense that duality between Atman and Brahman is transcended and one realizes oneself to be the Self in all.
Patanjali, author of the Yoga Sutras, wrote quite exhaustively on the psychological and ontological roots of bliss.


==== Confucianism ====
The Chinese Confucian thinker Mencius, who had sought to give advice to ruthless political leaders during China's Warring States period, was convinced that the mind played a mediating role between the "lesser self" (the physiological self) and the "greater self" (the moral self), and that getting the priorities right between these two would lead to sage-hood. He argued that if one did not feel satisfaction or pleasure in nourishing one's "vital force" with "righteous deeds", then that force would shrivel up (Mencius, 6A:15 2A:2). More specifically, he mentions the experience of intoxicating joy if one celebrates the practice of the great virtues, especially through music.


=== Abrahamic religions ===


==== Judaism ====

Happiness or simcha (Hebrew: שמחה‎) in Judaism is considered an important element in the service of God. The biblical verse "worship The Lord with gladness; come before him with joyful songs," (Psalm 100:2) stresses joy in the service of God. A popular teaching by Rabbi Nachman of Breslov, a 19th-century Chassidic Rabbi, is "Mitzvah Gedolah Le'hiyot Besimcha Tamid," it is a great mitzvah (commandment) to always be in a state of happiness. When a person is happy they are much more capable of serving God and going about their daily activities than when depressed or upset.


==== Roman Catholicism ====
The primary meaning of "happiness" in various European languages involves good fortune, chance or happening. The meaning in Greek philosophy, however, refers primarily to ethics.
In Catholicism, the ultimate end of human existence consists in felicity, Latin equivalent to the Greek eudaimonia, or "blessed happiness", described by the 13th-century philosopher-theologian Thomas Aquinas as a Beatific Vision of God's essence in the next life.According to St. Augustine and Thomas Aquinas, man's last end is happiness: "all men agree in desiring the last end, which is happiness." However, where utilitarians focused on reasoning about consequences as the primary tool for reaching happiness, Aquinas agreed with Aristotle that happiness cannot be reached solely through reasoning about consequences of acts, but also requires a pursuit of good causes for acts, such as habits according to virtue. In turn, which habits and acts that normally lead to happiness is according to Aquinas caused by laws: natural law and divine law. These laws, in turn, were according to Aquinas caused by a first cause, or God.According to Aquinas, happiness consists in an "operation of the speculative intellect": "Consequently happiness consists principally in such an operation, viz. in the contemplation of Divine things." And, "the last end cannot consist in the active life, which pertains to the practical intellect." So: "Therefore the last and perfect happiness, which we await in the life to come, consists entirely in contemplation. But imperfect happiness, such as can be had here, consists first and principally in contemplation, but secondarily, in an operation of the practical intellect directing human actions and passions."Human complexities, like reason and cognition, can produce well-being or happiness, but such form is limited and transitory. In temporal life, the contemplation of God, the infinitely Beautiful, is the supreme delight of the will. Beatitudo, or perfect happiness, as complete well-being, is to be attained not in this life, but the next.


==== Islam ====
Al-Ghazali (1058–1111), the Muslim Sufi thinker, wrote "The Alchemy of Happiness", a manual of spiritual instruction throughout the Muslim world and widely practiced today.


== Methods for achieving happiness ==
Theories on how to achieve happiness include "encountering unexpected positive events", "seeing a significant other", and "basking in the acceptance and praise of others".
However others believe that happiness is not solely derived from external, momentary pleasures.


=== Self-fulfilment theories ===


==== Maslow's hierarchy of needs ====
Maslow's hierarchy of needs is a pyramid depicting the levels of human needs, psychological, and physical. When a human being ascends the steps of the pyramid, he reaches self-actualization. Beyond the routine of needs fulfillment, Maslow envisioned moments of extraordinary experience, known as peak experiences, profound moments of love, understanding, happiness, or rapture, during which a person feels more whole, alive, self-sufficient, and yet a part of the world. This is similar to the flow concept of Mihály Csíkszentmihályi.


==== Self-determination theory ====

Self-determination theory relates intrinsic motivation to three needs: competence, autonomy, and relatedness.


==== Modernization and freedom of choice ====
Ronald Inglehart has traced cross-national differences in the level of happiness based on data from the World Values Survey. He finds that the extent to which a society allows free choice has a major impact on happiness. When basic needs are satisfied, the degree of happiness depends on economic and cultural factors that enable free choice in how people live their lives. Happiness also depends on religion in countries where free choice is constrained.


=== Positive psychology ===
Since 2000 the field of positive psychology has expanded drastically in terms of scientific publications, and has produced many different views on causes of happiness, and on factors that correlate with happiness. Numerous short-term self-help interventions have been developed and demonstrated to improve happiness.


=== Indirect approaches ===
John Stuart Mill believed that for the great majority of people happiness is best achieved en passant, rather than striving for it directly. This meant no self-consciousness, scrutiny, self-interrogation, dwelling on, thinking about, 
imagining or questioning on one's happiness. Then, if otherwise fortunately circumstanced, one would "inhale happiness with the air you breathe."Similarly, William Inge observed that "on the whole, the happiest people seem to be those who have no particular cause for being happy except the fact that they are so."


== Possible limits on happiness seeking ==
Some studies, including 2018 work by June Gruber a psychologist at University of Colorado, have suggested that seeking happiness can have negative effects, such as failure to meet over-high expectations. A 2012 study found that psychological well-being was higher for people who experienced both positive and negative emotions. Other research has analysed possible trade-offs between happiness and meaning in life.Not all cultures seek to maximise happiness.Sigmund Freud said that all humans strive after happiness, but that the possibilities of achieving it are restricted because we "are so made that we can derive intense enjoyment only from a contrast and very little from the state of things."


== Examining happiness ==
Happiness can be examined in experiential and evaluative contexts. Experiential well-being, or "objective happiness", is happiness measured in the moment via questions such as "How good or bad is your experience now?". In contrast, evaluative well-being asks questions such as "How good was your vacation?" and measures one's subjective thoughts and feelings about happiness in the past. Experiential well-being is less prone to errors in reconstructive memory, but the majority of literature on happiness refers to evaluative well-being. The two measures of happiness can be related by heuristics such as the peak-end rule.Some commentators focus on the difference between the hedonistic tradition of seeking pleasant and avoiding unpleasant experiences, and the eudaimonic tradition of living life in a full and deeply satisfying way.


== Measurement ==
People have been trying to measure happiness for centuries. In 1780, the English utilitarian philosopher Jeremy Bentham proposed that as happiness was the primary goal of humans it should be measured as a way of determining how well the government was performing.Several scales have been developed to measure happiness:

The Subjective Happiness Scale (SHS) is a four-item scale, measuring global subjective happiness from 1999.  The scale requires participants to use absolute ratings to characterize themselves as happy or unhappy individuals, as well as it asks to what extent they identify themselves with descriptions of happy and unhappy individuals.
The Positive and Negative Affect Schedule (PANAS) from 1988 is a 20-item questionnaire, using a five-point Likert scale (1 = very slightly or not at all, 5 = extremely) to assess the relation between personality traits and positive or negative affects at "this moment, today, the past few days, the past week, the past few weeks, the past year, and in general". A longer version with additional affect scales was published 1994.
The Satisfaction with Life Scale (SWLS) is a global cognitive assessment of life satisfaction developed by Ed Diener. A seven-point Likert scale is used to agree or disagree with five statements about one's life.
The Cantril ladder method has been used in the World Happiness Report. Respondents are asked to think of a ladder, with the best possible life for them being a 10, and the worst possible life being a 0. They are then asked to rate their own current lives on that 0 to 10 scale.
Positive Experience; the survey by Gallup asks if, the day before, people experienced enjoyment, laughing or smiling a lot, feeling well-rested, being treated with respect, learning or doing something interesting. 9 of the top 10 countries in 2018 were South American, led by Paraguay and Panama. Country scores range from 85 to 43.Since 2012, a World Happiness Report has been published. Happiness is evaluated, as in “How happy are you with your life as a whole?”, and in emotional reports, as in “How happy are you now?,” and people seem able to use happiness as appropriate in these verbal contexts. Using these measures, the report identifies the countries with the highest levels of happiness. In subjective well-being measures, the primary distinction is between cognitive life evaluations and emotional reports.The UK began to measure national well-being in 2012, following Bhutan, which had already been measuring gross national happiness.Happiness has been found to be quite stable over time.


== Relationship to physical characteristics ==
As of 2016, no evidence of happiness causing improved physical health has been found; the topic is being researched at the Lee Kum Sheung Center for Health and Happiness at the Harvard T.H. Chan School of Public Health. 
A positive relationship has been suggested between the volume of the brain's gray matter in the right precuneus area and one's subjective happiness score.


== Economic and political views ==

In politics, happiness as a guiding ideal is expressed in the United States Declaration of Independence of 1776, written by Thomas Jefferson, as the universal right to "the pursuit of happiness." This seems to suggest a subjective interpretation but one that goes beyond emotions alone. It has to be kept in mind that the word happiness meant "prosperity, thriving, wellbeing" in the 18th century and not the same thing as it does today. In fact, happiness.Common market health measures such as GDP and GNP have been used as a measure of successful policy. On average richer nations tend to be happier than poorer nations, but this effect seems to diminish with wealth. This has been explained by the fact that the dependency is not linear but logarithmic, i.e., the same percentual increase in the GNP produces the same increase in happiness for wealthy countries as for poor countries. Increasingly, academic economists and international economic organisations are arguing for and developing multi-dimensional dashboards which combine subjective and objective indicators to provide a more direct and explicit assessment of human wellbeing. Work by Paul Anand and colleagues helps to highlight the fact that there many different contributors to adult wellbeing, that happiness judgement reflect, in part, the presence of salient constraints, and that fairness, autonomy, community and engagement are key aspects of happiness and wellbeing throughout the life course.Libertarian think tank Cato Institute claims that economic freedom correlates strongly with happiness preferably within the context of a western mixed economy, with free press and a democracy. According to certain standards, East European countries when ruled by Communist parties were less happy than Western ones, even less happy than other equally poor countries.Since 2003, empirical research in the field of happiness economics, such as that by Benjamin Radcliff, professor of Political Science at the University of Notre Dame, supported the contention that in democratic countries life satisfaction is strongly and positively related to the social democratic model of a generous social safety net, pro-worker labor market regulations, and strong labor unions. Similarly, there is evidence that public policies which reduce poverty and support a strong middle class, such as a higher minimum wage, strongly affect average levels of well-being.It has been argued that happiness measures could be used not as a replacement for more traditional measures, but as a supplement. According to the Cato institute, people constantly make choices that decrease their happiness, because they have also more important aims. Therefore, government should not decrease the alternatives available for the citizen by patronizing them but let the citizen keep a maximal freedom of choice.Good mental health and good relationships contribute more than income to happiness and governments should take these into account.


== Contributing factors and research outcomes ==

Research on positive psychology, well-being, eudaimonia and happiness, and the theories of Diener, Ryff, Keyes, and Seligmann covers a broad range of levels and topics, including "the biological, personal, relational, institutional, cultural, and global dimensions of life."


== See also ==


== References ==


== Further reading ==
Anand Paul "Happiness Explained: What Human Flourishing Is and What We Can Do to Promote It", Oxford: Oxford University Press 2016. ISBN 0-19-873545-6
Michael Argyle "The psychology of happiness", 1987
Boehm, Julia K.; Lyubomirsky, Sonja (February 2008). "Does Happiness Promote Career Success?". Journal of Career Assessment. 16 (1): 101–116. CiteSeerX 10.1.1.378.6546. doi:10.1177/1069072707308140. S2CID 145371516.
Norman M. Bradburn "The structure of psychological well-being", 1969
C. Robert Cloninger, Feeling Good: The Science of Well-Being, Oxford, 2004.
Gregg Easterbrook "The progress paradox – how life gets better while people feel worse", 2003
Michael W. Eysenck "Happiness – facts and myths", 1990
Daniel Gilbert, Stumbling on Happiness, Knopf, 2006.
Carol Graham "Happiness Around the World: The Paradox of Happy Peasants and Miserable Millionaires", OUP Oxford, 2009. ISBN 978-0-19-954905-4
W. Doyle Gentry "Happiness for dummies", 2008
James Hadley, Happiness: A New Perspective, 2013, ISBN 978-1-4935-4526-1
Joop Hartog & Hessel Oosterbeek "Health, wealth and happiness", 1997
Hills P., Argyle M. (2002). "The Oxford Happiness Questionnaire: a compact scale for the measurement of psychological well-being. Personality and Individual Differences". Psychological Wellbeing. 33 (7): 1073–82. doi:10.1016/s0191-8869(01)00213-6.
Robert Holden "Happiness now!", 1998
Barbara Ann Kipfer, 14,000 Things to Be Happy About, Workman, 1990/2007, ISBN 978-0-7611-4721-3.
Neil Kaufman "Happiness is a choice", 1991
Stefan Klein, The Science of Happiness, Marlowe, 2006, ISBN 1-56924-328-X.
Koenig HG, McCullough M, & Larson DB. Handbook of religion and health: a century of research reviewed (see article). New York: Oxford University Press; 2001.
McMahon, Darrin M., Happiness: A History, Atlantic Monthly Press; 2005. ISBN 0-87113-886-7
McMahon, Darrin M., The History of Happiness: 400 B.C. – A.D. 1780, Daedalus journal, Spring 2004.
Richard Layard, Happiness: Lessons From A New Science, Penguin, 2005, ISBN 978-0-14-101690-0.
Luskin, Frederic, Kenneth R. Pelletier, Dr. Andrew Weil (Foreword). "Stress Free for Good: 10 Scientifically Proven Life Skills for Health and Happiness." 2005
James Mackaye "Economy of happiness", 1906
Desmond Morris "The nature of happiness", 2004
David G. Myers, Ph.D., The Pursuit of Happiness: Who is Happy – and Why, William Morrow and Co., 1992, ISBN 0-688-10550-5.
Niek Persoon "Happiness doesn't just happen", 2006
Benjamin Radcliff The Political Economy of Human Happiness (New York: Cambridge University Press, 2013).
Ben Renshaw "The secrets of happiness", 2003
Fiona Robards, "What makes you happy?" Exisle Publishing, 2014, ISBN 978-1-921966-31-6
Bertrand Russell "The conquest of happiness", orig. 1930 (many reprints)
Martin E.P. Seligman, Authentic Happiness, Free Press, 2002, ISBN 0-7432-2298-9.
Alexandra Stoddard "Choosing happiness – keys to a joyful life", 2002
Władysław Tatarkiewicz, Analysis of Happiness, The Hague, Martinus Nijhoff Publishers, 1976
Elizabeth Telfer "Happiness : an examination of a hedonistic and a eudaemonistic concept of happiness and of the relations between them...", 1980
Ruut Veenhoven "Bibliography of happiness – world database of happiness : 2472 studies on subjective appreciation of life", 1993
Ruut Veenhoven "Conditions of happiness", 1984
Joachim Weimann, Andreas Knabe, and Ronnie Schob, eds. Measuring Happiness: The Economics of Well-Being (MIT Press; 2015) 206 pages
Eric G. Wilson "Against Happiness", 2008Articles and videosJournal of Happiness Studies, International Society for Quality-of-Life Studies (ISQOLS), quarterly since 2000, also online
A Point of View: The pursuit of happiness (January 2015), BBC News Magazine
Srikumar Rao: Plug into your hard-wired happiness – Video of a short lecture on how to be happy
Dan Gilbert: Why are we happy? – Video of a short lecture on how our "psychological immune system" lets us feel happy even when things don't go as planned.
TED Radio Hour: Simply Happy – various guest speakers, with some research results


== External links ==

History of Happiness – concise survey of influential theories
The Stanford Encyclopedia of Philosophy entry "Pleasure" – ancient and modern philosophers' and neuroscientists' approaches to happiness
The World Happiness Forum promotes dialogue on tools and techniques for human happiness and wellbeing.
Action For Happiness is a UK movement committed to building a happier society
Improving happiness through humanistic leadership – University of Bath, UK
The World Database of Happiness – a register of scientific research on the subjective appreciation of life.
Oxford Happiness Questionnaire – Online psychological test to measure your happiness.
Track Your Happiness – research project with downloadable app that surveys users periodically and determines personal factors
Pharrell Williams – Happy (Official Music Video) added to YouTube by P. Williams: i Am Other – Retrieved 2015-11-21
Four Levels of Happiness – A modern take on the Greco-Christian understanding of happiness in 4 levels.