Pinkerton, founded as the Pinkerton National Detective Agency, is a private security guard and detective agency established in the United States by Scotsman Allan Pinkerton in 1850 and currently a subsidiary of Securitas AB. Pinkerton became famous when he claimed to have foiled a plot to assassinate president-elect Abraham Lincoln, who later hired Pinkerton agents for his personal security during the Civil War. Pinkerton's agents performed services ranging from security guarding to private military contracting work. The Pinkerton National Detective Agency hired women and minorities from its founding, a practice uncommon at the time. Pinkerton was the largest private law enforcement organization in the world at the height of its power.During the labor strikes of the late 19th and early 20th centuries, businessmen hired the Pinkerton Agency to infiltrate unions, supply guards, keep strikers and suspected unionists out of factories, and recruit goon squads to intimidate workers. One such confrontation was the Homestead Strike of 1892, in which Pinkerton agents were called in to reinforce the strikebreaking measures of industrialist Henry Clay Frick, acting on behalf of Andrew Carnegie.  The ensuing battle between Pinkerton agents and striking workers led to the deaths of three Pinkerton agents and nine steelworkers. The Pinkertons were also used as guards in coal, iron, and lumber disputes in Illinois, Michigan, New York, Pennsylvania, and West Virginia as well as the Great Railroad Strike of 1877 and the Battle of Blair Mountain in 1921.
The company is now a division of the Swedish security company Securitas AB. It operates as "Pinkerton Consulting & Investigations, Inc. d.b.a. Pinkerton Corporate Risk Management". The former Government Services division, PGS, now operates as Securitas Critical Infrastructure Services, Inc.


== Origins ==

In the 1850s, Allan Pinkerton, Scottish detective and spy, met Chicago attorney Edward Rucker in a local Masonic Hall and formed the North-Western Police Agency, later known as the Pinkerton Agency.Historian Frank Morn writes: "By the mid-1850s a few businessmen saw the need for greater control over their employees; their solution was to sponsor a private detective system. In February 1855, Allan Pinkerton, after consulting with six midwestern railroads, created such an agency in Chicago."


== Government work ==

In 1871, Congress appropriated $50,000 (about equivalent to $1,067,000 in 2019) to the new Department of Justice (DOJ) to form a sub-organization devoted to "the detection and prosecution of those guilty of violating federal law." The amount was insufficient for the new DOJ to fashion an internal investigating unit, so they contracted out the services to the Pinkerton National Detective Agency.However, since passage of the Anti-Pinkerton Act in 1893, federal law has stated that an "individual employed by the Pinkerton Detective Agency, or similar organization, may not be employed by the Government of the United States or the government of the District of Columbia."


== Chicago "Special Officers" and watchmen ==
July 27, 1877: J.J. White, who had been hired as a "Special Officer" during a strike, was shot and killed.
July 19, 1919: Hans Rassmuson, Special Officer, was shot and killed.
March 12, 1924: Frank Miller, Pinkerton Watchman, was shot and killed.


== Molly Maguires ==

In the 1870s, Franklin B. Gowen, then president of the Philadelphia and Reading Railroad, hired the agency to "investigate" the labor unions in the company's mines.  A Pinkerton agent, James McParland, using the alias "James McKenna", infiltrated the Molly Maguires, a 19th-century secret society of mainly Irish-American coal miners, leading to the downfall of the labor organization.
The incident inspired Arthur Conan Doyle's Sherlock Holmes novel The Valley of Fear (1914–1915). A Pinkerton agent also appears in a small role in "The Adventure of the Red Circle", a 1911 Holmes story. A 1970 film, The Molly Maguires, was loosely based upon the incident as well.


== Homestead Strike ==

On July 6, 1892, during the Homestead Strike, 300 Pinkerton detectives from New York and Chicago were called in by Carnegie Steel's Henry Clay Frick to protect the Pittsburgh-area mill and strikebreakers. This resulted in a firefight and siege in which 16 men were killed, and 23 others were wounded. To restore order, two brigades of the Pennsylvania militia were called out by the Governor.
As a legacy of the Pinkertons' involvement, a bridge connecting the nearby Pittsburgh suburbs of Munhall and Rankin was named Pinkerton's Landing Bridge.


== Steunenberg murder and trial ==

Harry Orchard was arrested by the Idaho police and confessed to Pinkerton agent James McParland that he assassinated former Governor Frank Steunenberg of Idaho in 1905. Orchard testified, (unsuccessfully) under threat of hanging, against Western Federation of Miners president Big Bill Haywood, naming him as having hired the hit. With a stirring defense by Clarence Darrow, Haywood and the other defendants of the WFM were acquitted in a nationally publicized trial. Orchard received a death sentence, but it was commuted.


== Indiana University ==

In 1890, Indiana University hired the Pinkerton Agency to investigate the authorship of a student "bogus", an underground newsletter, that had been distributed throughout town. While boguses were not uncommon, this particular one attacked IU faculty and students with such graphic language that Bloomington residents complained. The detective arrived in Bloomington on April 26 and spent nearly two weeks conducting interviews and dispatching regular reports back to the home office. In the end, it was town talk that led to the student authors and not the work of the agent. The seven Beta Theta Pi fraternity brothers were from locally prominent families, including the son of a Trustee, but all were expelled. In 1892, however, the Trustees granted five of the men their degrees and all seven were reinstated in good standing.


== Outlaws and competition ==
Pinkerton agents were hired to track western outlaws Jesse James, the Reno Gang, and the Wild Bunch (including Butch Cassidy and the Sundance Kid). On March 17, 1874, two Pinkerton Detectives and a deputy sheriff,  Edwin P. Daniels, encountered the Younger brothers (associates of the James–Younger Gang); Daniels, John Younger, and one Pinkerton agent were killed. In Union, Missouri a bank was robbed by George Collins, aka Fred Lewis, and Bill Randolph; Pinkerton Detective Chas Schumacher trailed them and was killed. Collins was hanged on March 26, 1904, and Randolph was hanged on May 8, 1905, in Union. Pinkertons were also hired for transporting money and other high-quality merchandise between cities and towns, which made them vulnerable to outlaws. Pinkerton agents were usually well paid and well armed. 
G.H. Thiel, a former Pinkerton employee, established the Thiel Detective Service Company in St. Louis, Missouri, a competitor to the Pinkerton agency.  The Thiel company operated in the U.S., Canada, and Mexico.


== Modern era ==
Due to its conflicts with labor unions, the word Pinkerton continues to be associated by labor organizers and union members with strikebreaking. Pinkertons diversified from labor spying following revelations publicized by the La Follette Committee hearings in 1937, and the firm's criminal detection work also suffered from the police modernization movement, which saw the rise of the Federal Bureau of Investigation and the bolstering of detective branches and resources of the public police. With less of the labor and criminal investigation work on which Pinkertons thrived for decades, the company became increasingly involved in protection services, and in the 1960s, even the word "detective" disappeared from the agency's letterhead. The company now focuses on threat intelligence, risk management, executive protection, and active shooter response.
In 1999, the company was bought by Securitas AB, a Swedish security company, for $384 million, followed by the acquisition of the William J. Burns Detective Agency (founded in 1910), longtime Pinkerton rival, to create (as a division of the parent) Securitas Security Services USA. Today, the company's headquarters are located in Ann Arbor, Michigan.


== Badge history ==
1870–1925 — Solid silver badge with engraved lettering
1925–1930 — Silver-colored eagle badge with blue lettering
1930–present — Gold-colored shield with black lettering


== Appearance in popular media ==


=== Books ===
Author Beverly Jenkins makes reference to Pinkerton agents in many of her novels including, "Vivid" and "Before the Dawn".
In the Sherlock Holmes novel The Valley of Fear, Birdy Edwards is a Pinkerton agent. Another Pinkerton agent, Leverton makes his appearance in the short story The Adventure of the Red Circle, which is part of Sir Arthur Conan Doyle's collection, The Memoirs of Sherlock Holmes
Dashiell Hammett's Continental Op detective stories were based on his experiences working for the Pinkerton agency for several periods during 1917-22. The Baltimore branch, where Hammett first worked, was housed in the Continental Trust Building; the Pinkerton agency was not named. Hammett also worked in the San Francisco office, thus many of the Op stories were set in that city. They were published in Black Mask magazine from 1923-30. There have been several Continental Op book collections. Four of the stories were combined to form Hammett's first novel Red Harvest.
In Laird Barron's short story "Bulldozer", the Pinkerton protagonist Jonah Koenig hunts down a wanted criminal who also happens to dabble in profane occult rituals.
Many Louis L'Amour books contain references to the Pinkertons, including Milo Talon
Maria "Belle" Boyd, a lead character in Cherie Priest's steampunk novel Fiddlehead, works for the Pinkerton detective agency.
Clive Cussler is often reputed to base his Isaac Bell series' Van Dorn Detective Agency upon the Pinkerton Detective Agency. This is often disputed, however, as those same books often portray the Pinkerton agency in a very negative light, even appearing as a major antagonist in a prequel novel.
In the 1995 Doctor Who book Shakedown (novelization of Shakedown: Return of the Sontarans) the Doctor's companions Chris and Roz pretend to be agents of the galactic offshoot of the Pinkertons on the planet Megerra in 2376.
In the James Bond novels, Felix Leiter becomes a Pinkerton detective between Live and Let Die and Diamonds Are Forever. He had been a CIA agent, but retired after losing an arm and a leg in the former novel.
In a romance series of novellas, The Pinkerton Matchmaker, written by many different authors.
Former Pinkerton agent Charlie Siringo published an exposé on methods used by the Pinkertons during the 1880s and 1890s in his 1914 book Two Evil-Isms: Pinkertonism and Anarchism. The agency soon suppressed publication of the book and made a request to the Governor of New Mexico, William C. McDonald, to arrest Siringo for criminal libel and extradite him to Chicago. Governor McDonald denied their request, but the agency was successful in obtaining a court order to impound all existing copies of the book.
Author Ray Celestin makes reference to Pinkerton agents in his "City Blues Quartet" book series.
Lucky Luke counters Pinkerton agents in the 2010 album Lucky Luke contre Pinkerton (Lucky Luke versus The Pinkertons) by Daniel Pennac and Tonino Benacquista.


=== Film ===
Allan Pinkerton and several agents play a vital role in the film American Outlaws (2001), starring Colin Farrell and Gabriel Macht as Jesse and Frank James. Pinkerton and his detectives are hired by the owner of the fictional Rock Northern Rail Road to track down Jesse James and his gang following a series of robberies aimed at his company.
The Pinkertons have been featured in the 1980 movie The Long Riders, where Pinkerton agents are depicted investigating the criminal activities of the James brothers.
Two Pinkerton agents were featured in the movie The Legend of Zorro, using knowledge of Zorro's true identity to blackmail him.
Pinkerton detectives are featured in the 3:10 to Yuma remake featuring Russell Crowe and Christian Bale, appearing at the start of the film defending a stagecoach from bandits. All are killed except for one, Byron McElroy (Peter Fonda), who is later killed by Crowe's character.
In the 1994 western film Bad Girls the three main characters are being tracked down by two Pinkerton agents after committing murder.
In the 1997 James Cameron film Titanic, Spicer Lovejoy, Caledon Hockley's valet and bodyguard, is an ex-Pinkerton detective.
In the 2005 film Legend of Zorro, A pair of Pinkerton agents sees Zorro’s face and recognise him. The following day, the Pinkertons confront his wife, Elena, and blackmail her into divorcing him in order to get close to the main antagonist; Armand and learn of his plans without the aid of Zorro, as they dislike Zorro and his vigilante ways.
In the 2016 remake of The Magnificent Seven, agents of the fictional Blackstone Detective Agency, modeled after the Pinkertons, appear as antagonists.
In the 2017 Kenneth Branagh film Murder on the Orient Express, Cyrus Bethman Hardman is revealed to be an undercover Pinkerton detective.
In the 2019 western film Badland, the protagonist was a Pinkerton detective in New Mexico authorized by the President and directed by a black Senator to track down and hang Confederate war criminals.


=== Music ===
The song "Ballad of a Well-Known Gun" on the 1970 Elton John album Tumbleweed Connection references 'the Pinkertons' – "The Pinkertons pulled out my bags and asked me for my name".
The song "Book, Saddle, And Go" on the 2013 Clutch album Earth Rocker references 'Pinkerton Man' – "Pinkerton man, murdering bastard, I’m gonna get even, get even with you, Get even with you".
The song "Diamond In Your Mind" on the 2002 Solomon Burke album Don't Give Up on Me references 'A Pinkerton raid'  – "Said she lost her right arm, blown off in a Pinkerton raid".


=== Television ===
In the television series Damnation, Creeley Turner, one of the two lead characters, is a Pinkerton agent sent to Iowa by a powerful industrialist to stop a farmer strike.
On the "Valentine's Day" (1989) episode of The Golden Girls, drugstore banter between Rose and Dorothy regarding purchasing protection for their trip (condoms), promoting their desire to have safe sex. When Rose asks what kind of protection they need, Dorothy quips “Two armed Pinkerton guards, Rose!”
In "False Colours", the eighth episode of the first series of the 1989 TV show The Young Riders, a detective named Pinkerton is working with the security of a gold shipment.
In the fourth-series episode "Havre de Grace" of Boardwalk Empire, the character Roy Phillips is revealed to be a detective working for the agency.
The character Captain Homer Jackson in the BBC series Ripper Street is also revealed as an ex-Pinkerton agent in series one.
Three Pinkerton Agents were featured in the first-season episode "Husbands & Fathers", of the BBC America show Copper.
In 1966 Irwin Allen series The Time Tunnel on episode 12 "The Death Trap", Mr. Pinkerton, with the help of the two main characters, saved President Lincoln.
In Penny Dreadful series 1 episode 8 "Grand Guignol" Ethan Chandler is confronted by 2 Pinkerton agents in a bar as his past catches up with him.
Pinkerton agents appeared in the first episode of the 2012 Hatfields & McCoys miniseries.
The Pinkertons, a scripted one-hour syndicated starring Angus Macfadyen as Allan Pinkerton, debuted in 2014.
Cole Hauser plays Charlie Siringo, a Pinkerton investigating Lizzie Borden, in the 2015 mini-series The Lizzie Borden Chronicles.
In the Canadian-American television drama When Calls the Heart, Pinkerton security officers are employed by the mining company managed by Henry Gowen (Martin Cummins)
Michael Dorn played Mr. Eastman, a detective sent by the Pinkerton Detective Agency to investigate the kidnapping of a wealthy man's wife, in the tenth episode of the second season of the TV series The Dead Man's Gun titled "The Pinkerton", which originally aired on October 30, 1998.
Allan Pinkerton was portrayed by Charlie Day in the second-series episode of Drunk History entitled "Baltimore". The episode relays the story of Allan Pinkerton successfully protecting Abraham Lincoln, portrayed by Martin Starr, from assassination.
"Bloody Battles", the second episode of the 2012 miniseries The Men Who Built America, focuses on the relationship between Andrew Carnegie and Henry Clay Frick, which is damaged by the 1892 Homestead strike when Frick hires the Pinkertons.
Mentioned in six episodes -- "Let Loose the Dogs," "Werewolves," “Au Naturel,” "Summer of '75," "Manual for Murder," and "Bad Pennies" —of the 2008–present Canadian TV series Murdoch Mysteries.
In the television series Deadwood, the Pinkertons are prominently featured as mercenaries paid for by George Hearst, the final season's antagonist.
In the television mini-series North and South Book III (Based on "Heaven and Hell" by John Jakes), George Hazard hires a Pinkerton detective to track Elkana Bent for the suspected murder of his wife Constance Hazard.


=== Video games ===
The protagonist of the video game BioShock Infinite, Booker DeWitt, is an ex-Pinkerton and former member of the "goon squads" used by the agency to suppress strikes. One of the game's trophies, awarded for completing the highest difficulty, is titled "Stone Cold Pinkerton".
In the online game Poptropica, Pinkerton's services are used to catch a thief on Mystery Train Island.
Pinkerton detectives appear as antagonists in Red Dead Redemption 2. They appear primarily as mercenaries and investigators employed to track down the Van Der Linde gang after they stage a robbery in the town of Blackwater which resulted in several civilian deaths. The modern-day Pinkerton security company issued a cease-and-desist notice to Take-Two Interactive, publisher of Red Dead Redemption 2, in 2019 over the use of the company's trademarks and the game's negative portrayal of Pinkertons as villains. Take-Two filed a suit striking back at the cease-and-desist notice from Pinkerton, wanting a court to rule that its use of the Pinkerton name — as part of a game that emphasizes historical accuracy — was fair use, but on April 11, 2019, Pinkerton dismissed their lawsuit and settled out of court with Take-Two.


== See also ==
Anti-union organizations in the United States
Anti-union violence
Baldwin–Felts Detective Agency
Battle of Blair Mountain
Coal and Iron Police, a Pinkerton-supervised private police force in Pennsylvania
Colorado Labor Wars
George Samuel Dougherty, the private detective for the Pinkerton National Detective Agency from 1888–1911
Morris Friedman, author of Pinkerton Labor Spy
Dashiell Hammett, author and former Pinkerton operative
Historian J. Bernard Hogg, public reaction to Pinkertonism and the Labor Question
Industrial Workers of the World
Labor spying in the United States
Frank Little, American labor leader; lynched in 1917, allegedly by Pinkerton agents
List of worker deaths in United States labor disputes
Timothy Webster


== References ==


== Further reading ==
Jonathan Obert. 2018. "Pinkertons and Police in Antebellum Chicago." in The Six-Shooter State: Public and Private Violence in American Politics. Cambridge University Press.


== External links ==
Official website
Radio Programme with Ward Churchill