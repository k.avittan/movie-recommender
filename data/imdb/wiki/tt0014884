Female (symbol: ♀) is the sex of an organism, or a part of an organism, that produces non-mobile ova (egg cells). Barring rare medical conditions, most female mammals, including female humans, have two X chromosomes. Female characteristics vary between different species with some species containing more well defined female characteristics, such as the presence of pronounced mammary glands. There is no single genetic mechanism behind sex differences in different species and the existence of two sexes seems to have evolved multiple times independently in different evolutionary lineages.The word female comes from the Latin femella, the diminutive form of femina, meaning "woman"; it is not etymologically related to the word male. Female can also be used to refer to gender.


== Defining characteristics ==
Females produce ova, the larger gametes in a heterogamous reproduction system, while the smaller and usually motile gamete, the spermatozoon, is produced by the male. A female cannot reproduce sexually without access to the gametes of a male, and vice versa, but in some species females can reproduce by themselves asexually, for example via parthenogenesis.There is no single genetic mechanism behind sex differences in different species and the existence of two sexes seems to have evolved multiple times independently in different evolutionary lineages. Patterns of sexual reproduction include:

Isogamous species with two or more mating types with gametes of identical form and behavior (but different at the molecular level),
Anisogamous species with gametes of male and female types,
Oogamous species, which include humans, in which the female gamete is very much larger than the male and has no ability to move. Oogamy is a form of anisogamy. There is an argument that this pattern was driven by the physical constraints on the mechanisms by which two gametes get together as required for sexual reproduction.Other than the defining difference in the type of gamete produced, differences between males and females in one lineage cannot always be predicted by differences in another. The concept is not limited to animals; egg cells are produced by chytrids, diatoms, water moulds and land plants, among others. In land plants, female and male designate not only the egg- and sperm-producing organisms and structures, but also the structures of the sporophytes that give rise to male and female plants.


== Mammalian female ==

A distinguishing characteristic of the class Mammalia is the presence of mammary glands. The mammary glands are modified sweat glands that produce milk, which is used to feed the young for some time after birth. Only mammals produce milk. Mammary glands are most obvious in humans, as the female human body stores large amounts of fatty tissue near the nipples, resulting in prominent breasts. Mammary glands are present in all mammals, although they are seldom used by the males of the species.Most mammalian females have two copies of the X chromosome as opposed to males which have only one X and one smaller Y chromosome; some mammals, such as the platypus, have different combinations. To compensate for the difference in size, one of the female's X chromosomes is randomly inactivated in each cell of placental mammals while the paternally derived X is inactivated in marsupials. In birds and some reptiles, by contrast, it is the female which is heterozygous and carries a Z and a W chromosome whilst the male carries two Z chromosomes. Intersex conditions can also give rise to other combinations, such as XO or XXX in mammals, which are still considered as female so long as they do not contain a Y chromosome, except for specific cases of mutations in the genes of XY individuals while in the womb. However, these conditions frequently result in sterility.Mammalian females bear live young, with the exception of monotreme females, which lay eggs. Some non-mammalian species, such as guppies, have analogous reproductive structures; and some other non-mammals, such as sharks, whose eggs hatch inside their bodies, also have the appearance of bearing live young.


== Etymology and usage ==

The word female comes from the Latin femella, the diminutive form of femina, meaning "woman"; it is not etymologically related to the word male, but in the late 14th century the spelling was altered in English to parallel the spelling of male. Female can refer to either sex or gender or a shape of connectors.


== Symbol ==

The symbol ♀ (Unicode: U+2640 Alt codes: Alt+12), a circle with a small cross underneath, is commonly used to represent females. Joseph Justus Scaliger once speculated that the symbol was associated with Venus, goddess of beauty because it resembles a bronze mirror with a handle, but modern scholars consider that fanciful, and the most established view is that the female and male symbols derive from contractions in Greek script of the Greek names of the planets Thouros (Mars) and Phosphoros (Venus).


== Sex determination ==

The sex of a particular organism may be determined by a number of factors. These may be genetic or environmental, or may naturally change during the course of an organism's life.  Although most species have only two sexes (either male or female), hermaphroditic animals have both male and female reproductive organs.


=== Genetic determination ===
The sex of most mammals, including humans, is genetically determined by the XY sex-determination system where males have X and Y (as opposed to X and X) sex chromosomes. During reproduction, the male contributes either an X sperm or a Y sperm, while the female always contributes an X egg. A Y sperm and an X egg produce a male, while an X sperm and an X egg produce a female. The ZW sex-determination system, where males have ZZ (as opposed to ZW) sex chromosomes, is found in birds, reptiles and some insects and other organisms. Members of Hymenoptera, such as ants and bees, are determined by haplodiploidy, where most males are haploid and females and some sterile males are diploid.


=== Environmental determination ===
The young of some species develop into one sex or the other depending on local environmental conditions, e.g. many crocodilians' sex is influenced by the temperature of their eggs. Other species (such as the goby) can transform, as adults, from one sex to the other in response to local reproductive conditions (such as a brief shortage of males).


== See also ==
Feminine side
Femininity
Gestation
Gender
Girl
Lady
Woman
Womyn


== References ==