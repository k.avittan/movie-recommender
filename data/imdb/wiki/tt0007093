Mother Love Bone was an American rock band that formed in Seattle, Washington, in 1988. The band was active from 1988 to 1990. Frontman Andrew Wood's personality and lyrics helped to catapult the group to the top of the burgeoning late 1980s/early 1990s Seattle music scene. Wood died only days before the scheduled release of the band's debut album, Apple, thus ending the group's hopes of success. The album was finally released a few months later.
As well as being influential to grunge, they are also considered early pioneers in the alternative metal genre.


== History ==
Mother Love Bone was established in 1987 by ex-Green River members Jeff Ament, Bruce Fairweather and Stone Gossard, ex-Malfunkshun frontman Andrew Wood and ex-Ten Minute Warning and Skin Yard drummer Greg Gilmore. Initially, the group was formed in 1987 out of the cover band Lords of the Wasteland which featured Wood, Gossard, Ament and Malfunkshun drummer Regan Hagar. By early 1988 the band had added Fairweather, replaced Hagar with drummer Greg Gilmore and changed its name to Mother Love Bone.This new line-up quickly set about recording and playing area shows and by late 1988 had become one of Seattle's more promising bands. Wood's exuberant on-stage personality, outlandish clothes and dreamy lyrics helped bring attention to the band. In the 1996 grunge documentary Hype!, Seattle engineer Jack Endino called Wood "the only stand-up comedian frontman in Seattle," a reference to Wood's playful style of interacting with Mother Love Bone fans.In November 1988, the band signed to PolyGram subsidiary Polydor/Stardog and recorded their debut EP. As part of their contract, PolyGram created the Stardog Records imprint exclusively for the band. In March 1989, the group issued its debut EP Shine, becoming one of the first of the new crop of Seattle bands to have a release on a major label. The record sold well and rapidly increased the profile of the band. John Book of AllMusic said the "record contributed to the buzz about the Seattle music scene."In late 1989, the group returned to the studio (this time in Sausalito, California) to record its debut album, Apple. Despite some initial difficulties, the record was on time for its projected March 1990 release. Only days before Apple was slated to be released, frontman Andrew Wood, who had a long history with drug problems, overdosed on heroin. Unresponsive and without signs of brain activity, he was in the hospital on life support for two days to allow friends and family to say goodbye. Wood's death effectively brought the group to an end. The album would see release later that year on July 19, 1990. Kim Neely of Rolling Stone said that the album "succeeds where countless other hard rock albums have failed, capturing the essence of what made Zep immortal – dynamics, kids! – and giving it a unique Nineties spin."


== Post-Mother Love Bone ==
In the months following Wood's death, Gossard and Ament were approached by Soundgarden frontman Chris Cornell (who had been Wood's roommate), and asked if they would be interested in recording a single containing two songs he had written in tribute to Wood. The project turned into an entire album and the group took the name Temple of the Dog, a reference to a line in the Mother Love Bone song "Man of Golden Words." Cornell recruited singer Eddie Vedder for the project, which afterward Vedder then joined Mike McCready, Gossard, and Ament to form Pearl Jam, one of the most commercially successful and critically acclaimed rock bands of the 1990s.
Fairweather initially remained inactive but later joined Seattle based psychedelic rock band Love Battery, replacing Tommy Simpson on bass in 1992. He played on two of the band's albums and many of its tours before leaving that band. In 2006, he resurfaced in The Press Corps, with Garrett Shavlik (The Fluid) and Dan Peters (Mudhoney).
Gilmore's profile also dropped significantly following Mother Love Bone's demise. Between 1992 and 1994, he drummed with the band Chubby Children, reuniting with former bandmates from 1982–1985, Brian Fox and Garth Brandenburg. Out of the band came a handful of shows and unreleased recordings. He also participated in the reunion of his former band Ten Minute Warning in 1998, and was credited with providing 'inspiration' for the song "Never the Machine Forever" (credited as being written by Kim Thayil) on Soundgarden's studio album, 1996's Down on the Upside. The song initially came out of a jam session Thayil had with Gilmore.In April 2011, Kevin Wood (Andrew Wood's brother) teamed up with hard rock band Lace Weeper to record Mother Love Bone's "Crown of Thorns" in commemoration of 21 years since Andrew's death. The single was released on Kevin's Wammybox Records.


== Reunion concerts ==
On April 14, 2010, the four surviving members of Mother Love Bone reunited for the first time in 20 years (with friend and fellow Seattle musician Shawn Smith serving as frontman for the night) as part of a sold-out "Brad and Friends" evening at Seattle's Showbox.  The songs featured were part of the band's core repertoire from their early days, including "Stardog Champion", "Holy Roller", "Gentle Groove" and a cover of the Argent song "Hold Your Head Up", a favorite encore from the band's early club days around Seattle.
On May 5, 2018, they again got together and performed 14 songs (including a cover of Argent’s 1972 hit “Hold Your Head Up”) during the event at Seattle’s Neptune Theatre.
Local singers Shawn Smith (Pigeonhead) and Ohm Johari (Hell’s Belles) shared singing duties.


== Band members ==
Andrew Wood – lead vocals, piano, keyboard, tambourine (1988–1990)
Bruce Fairweather – lead and rhythm guitar, backing vocals (1988–1990)
Stone Gossard – rhythm and lead guitar, backing vocals (1988–1990)
Jeff Ament – bass, backing vocals (1988–1990)
Greg Gilmore – drums (1988–1990)
Regan Hagar - drums (1988)


== Discography ==


=== Studio albums ===


=== Compilations ===


=== Extended plays ===


=== Singles ===


=== Videos ===


=== Music videos ===
1990 - "Stardog Champion"
1990 - "Holy Roller"
2016 - "Captain Hi-Top"


== See also ==
List of alternative rock artists


== Notes and references ==


== External links ==
Mother Love Bone Official website