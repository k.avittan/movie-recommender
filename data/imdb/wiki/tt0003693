A Suitable Boy is a novel by Vikram Seth, published in 1993. With 1,349 pages or 1,488 pages soft cover, and 591,552 words, the English language book is one of the longest novels published in a single volume.A Suitable Boy is set in a newly post-independence, post-partition India. The novel follows the story of four families over a period of 18 months, and centres on Mrs. Rupa Mehra's efforts to arrange the marriage of her younger daughter, Lata, to a "suitable boy". Lata is a 19-year-old university student who refuses to be influenced by her domineering mother or opinionated brother, Arun. Her story revolves around the choice she is forced to make between her suitors Kabir, Haresh, and Amit.
It begins in the fictional town of Brahmpur, located along the Ganges between Banares and Patna. Brahmpur, along with Calcutta, Delhi, Lucknow and other Indian cities, forms a colourful backdrop for the emerging stories.
Seth has stated that the biggest influence on writing A Suitable Boy was the five-volume 18th century Chinese novel The Story of the Stone by Cao Xueqin.The novel alternately offers satirical and earnest examinations of national political issues in the period leading up to the first post-Independence national election of 1952, including Hindu–Muslim strife, the status of lower caste peoples such as the jatav, land reforms and the eclipse of the feudal princes and landlords, academic affairs, abolition of the Zamindari system, family relations and a range of further issues of importance to the characters.
The novel is divided into 19 parts with, generally, each part focusing on a different subplot. Each part is described in rhyming couplet form on the contents page.  A sequel, to be called A Suitable Girl, was due for publication in 2017. As of 2020 this was still unpublished.


== Plot summary ==
In 1951, 19-year old Lata Mehra attends the wedding of her older sister, Savita, to Pran Kapoor, a university lecturer. Lata’s mother, Mrs. Rupa Mehra, informs Lata that it is time she marries, a notion which Lata dismisses as she intends to concentrate on her studies in English literature. Nevertheless Mrs. Rupa Mehra begins to put out feelers for a suitable boy for Lata to her friends and family.  
In the meantime Lata is approached several times by a boy her own age and after a few meetings feels she has already fallen in love with him. She learns his name is Kabir Durrani and is distressed when she realises he is Muslim as her Hindu family would never allow her to marry a Muslim man. When her early morning meetings with Kabir are discovered she tries to run away with Kabir, who refuses. Ultimately Lata agrees to go with her mother to Calcutta to live with her arrogant older brother Arun, who is already married.

As Lata is leaving she is spotted by Haresh Khanna, an ambitious shoe manufacturer who is involved in business with Kedarnath Tandon, the husband of Pran’s older sister, Veena. He is intrigued by her beauty and sadness. 
In Calcutta, Lata is surprised to find herself enjoying her time with her brother and sister-in-law. She meets her sister-in-law Meenakshi’s eccentric family, the Chatterjis, and bonds with her older brother, Amit, an England-educated poet who is under pressure from his family to grow up and marry. Though Amit initially only intends on being friendly to Lata as a member of his family, he begins to wonder whether she might make a suitable wife. Mrs. Rupa Mehra is horrified when she realises that Amit and Lata might be considering each other as spouses as she dislikes Meenakshi and disapproves of the Chatterjis as a result. She decides to go to Delhi to renew her efforts to find a spouse for Lata. By accident she is introduced to Haresh Khanna and decides he is suitable for Lata. Despite the fact that he is in love with another woman, who he is also unable to marry due to objections from her family, Haresh agrees to meet Lata. Lata finds the idea of marrying Haresh ridiculous but nevertheless has an agreeable time with him and gives him permission to write to her. 
Returning home she hears that Kabir was involved in reuniting her sister-in-law Veena with her son after a mass stampede separated them. She nevertheless vows to forget about Kabir only to be surprised when they are both cast in the university’s production of Twelfth Night. During rehearsals Pran is hospitalised and Savita gives birth. Lata takes on a more prominent role in taking care of her sister and niece which results in her realising her mother is only trying to ensure her happiness and safety. She begins corresponding more warmly with Haresh and despite still being attracted to Kabir tells him that she is no longer interested in marrying him.
Haresh loses his managerial job at a shoe factory but inveigles his way into a lesser position as the foreman at the Praha shoe factory with promise of upward mobility. His new circumstances fail to impress Arun and Meenakshi who are also biased against him as they are aware of Amit's burgeoning attraction to Lata and want to encourage that match. 
In the new year the Mehra family once again travels to Calcutta to spend time with Arun and Meenakshi and to reconnect with Haresh. At a cricket match Haresh, Kabir and Amit all meet and recognise that they are all loosely acquainted, but fail to realise that they are all, in one way or another, courting Lata. Kabir is in Calcutta trying to work up the courage to speak to Lata, however he fails to do so and Lata receives a letter from her best friend informing her that Kabir was spotted in an intimate conversation with another woman. Haresh is more persistent in his courtship of Lata, but after she off-handedly calls him mean, he takes offence and their relationship comes to a standstill.
In the new year, based on Kabir's invitation, Amit comes to speak at Lata's school. She reconnects with Kabir where she learns that the information he was courting another woman was false. However she tells him she is seriously writing to Haresh and is strongly considering marrying him. Amit also takes this opportunity to more seriously propose to Lata. Lata meets Kabir one last time where she realises that the passion she feels for him is not the basis for a good marriage. After receiving an apologetic letter from Haresh renewing his offer of marriage and a second letter from Arun, strongly encouraging her to reject Haresh, Lata decides once and for all to marry Haresh.
Concurrent to the main plot is the story of Maan Kapoor, Lata's brother-in-law Pran's brother. Maan is the feckless youngest child of respected politician Mahesh Kapoor, the state Minister of Revenue. At a Holi celebration, Maan watches the courtesan singer, Saeeda Bai, perform. He visits her house and begins to court her. They become lovers. Saeeda Bai later feels that her feelings for him are interfering with her work and reputation. She sends him away with Tasneem's Urdu teacher, Rasheed, to his remote village under the pretence of wanting Maan to learn flawless Urdu. Maan spends the time becoming acquainted with Rasheed's family who are politically influential.
When Maan returns to Brahmpur he resumes his love affair with Saeeda Bai and gains favour with his father who decides to run for office again in the seat where Rasheed's family lives. After campaigning with his father, Maan returns to Saeeda Bai's house where he sees his friend Firoz and believes from veiled comments that Saeeda Bai makes that the two have been having a love affair behind his back. In reality, Firoz had come to propose to Tasneem who Saeeda Bai revealed to be her secret daughter and Firoz's half-sister. In the ensuing confusion Maan stabs Firoz in a fit of jealousy. The ensuing scandal causes his father to lose his seat and his mother to die after a series of strokes.
However once Firoz recovers he insists that the stabbing was caused by his own clumsiness and Maan is made a free man.
The wedding between Lata and Haresh takes place with joy to all except Kabir who is invited but does not come. A few days later Lata and Haresh take a train to his home to begin their new lives together.


== Characters in A Suitable Boy ==
Four family trees are provided in the beginning of the novel to help readers keep track of the complicated interwoven family networks. The four main families in the novel are:

The Mehras
Mrs. Rupa Mehra, a mother searching for a suitable boy for her youngest daughter
Raghubir Mehra, her deceased husband
Arun, Mrs. Mehra's oldest son (married to Meenakshi Chatterji)
Aparna, daughter of Arun and Meenakshi
Varun
Savita (married to Pran Kapoor)
Uma Kapoor, daughter of Savita and Pran
Lata, whose arranged marriage forms the basis of the main plot
The Kapoors
Mr. Mahesh Kapoor (state Minister of Revenue) and Mrs. Mahesh Kapoor
Veena (married to Kedarnath Tandon)
Bhaskar Tandon, son of Veena and Kedarnath
Pran (married to Savita Mehra)
Maan
The Khans
The Nawab Sahib of Baitar
Zainab, his daughter
Hassan and Abbas, her sons
Imtiaz, a doctor
Firoz, a lawyer
Ustad Majeed Khan, a famed musician, relation to the family (if any) not specified
Begum Abida Khan, politician (sister-in-law of the Nawab Sahib)
The Chatterjis
Mr. Justice Chatterji and Mrs. Chatterji
Amit, eldest son and internationally acclaimed poet and author. A prominent love interest of Lata
Meenakshi (married to Arun Mehra)
Dipankar
Kakoli
TapanSome other prominent characters, not mentioned above, include:

Dr Durrani, mathematician at the university that Kabir and Lata attend
Kabir Durrani, a love interest of Lata and a central hub of one of the main themes of the novel. Kabir is a highly successful player on the university cricket team. Lata and Kabir have a brief, intense courtship, the ramifications of which echo through the rest of the novel.
Hashim Durrani, Kabir's brother
Haresh Khanna, an enterprising and determined shoe-businessman, who is also a love interest of the heroine
Nehru
Malati, best friend of Lata
Mrs Tandon
Kedarnath Tandon (married to Veena Kapoor)
Saeeda Bai, courtesan and musician
Tasneem, family member of Saeeda Bai
Bibbo, servant at Saeeda Bai's house
Rasheed, student at Brahmpur University; Tasneem's Urdu teacher
Ishaq, sarangi player
S S Sharma, Chief Minister
Agarwal, Home Minister
Priya, his daughter (married to Ram Vilas Goyal)
Simran, a Sikh woman and former love interest of Haresh Khanna
Kalpana Gaur, friend of the Mehra family
Billy Irani, friend of Arun Mehra, later has an affair with Meenakshi
Shireen, his fiancee
Bishwanath Bhaduri
Abdus Salam
Raja of Marh
Rajkumar of Marh, his son
Dr Bilgrami
Professor Mishra, an English professor
Dr Ila Chattopadhay, an English professor
Hans, an Austrian diplomat
The Guppi, inhabitant of Salimpur
Netaji, Rasheed's uncle
Sahgal
Makhijani, indulgent poet
Sandeep Lahiri
Waris, servant at the Baitar Fort and competes with Mahesh Kapoor in the General Election
The Munshi, in charge of the Baitar Fort
Jagat Ram, a shoemaker
Badrinath
Dr Kishen Chand Seth
Professor Nowrojee, who runs the university literary club attended by Kabir and Lata
Sunil Patwardhan, mathematician at Brahmpur University
Parvati, Mrs Rupa Mehra's stepmother


== Real people and events ==
For the character of Tapan, the youngest in Chatterjee family, Seth drew on his own experiences of being bullied at The Doon School in India.
The Praha Shoe Company of the novel is modeled on Bata Shoes.
Pul mela is based on  Kumbh Festival, which takes place at Sangam, Allahabad.


== Critical reception ==
On 5 November 2019 BBC News included A Suitable Boy on its list of the 100 most inspiring novels. The Independent wrote that "the movement and music of the writing in A Suitable Boy take time to absorb, but its unobtrusive, powerfully rational sweetness eventually compels the reader to its way of seeing."Daniel Johnson, in The Times, wrote: "A Suitable Boy is not merely one of the longest novels in English: it may also prove to be the most fecund as well as the most prodigious work of the latter half of this century - perhaps even the book to restore the serious reading public's faith in the contemporary novel. I have little doubt that... Vikram Seth is already the best writer of his generation", while Eugene Robinson of the Washington Post compared Seth favourably to Tolstoy.Christopher Hitchens, in Vanity Fair, gave the novel a glowing review, commenting that the prose "has a deceptive lightness and transparency to it".


== Television ==

A six-part series adapted from the novel and titled A Suitable Boy, directed by Mira Nair, written by Andrew Davies and starring Tabu, Ishaan Khatter, Tanya Maniktala and Rasika Dugal, among others, is broadcast on BBC One in the United Kingdom from Sunday, 26 July 2020. The production has drawn attention for the fact that it is the first time that a BBC historical drama has a cast completely featuring people of colour, except for Thomas Weinhappel as 'Hans'.


== References ==


== Reviews ==
McGirk, Tim (27 March 1993). "Playing happy families in Brahmpur: Tim McGirk on the exhausting charms of Vikram Seth's frothy comedy manners, marriages and mynah birds: A suitable boy". The Independent.
Tejpal, Tarun J. (March 15, 1993). "Epic dimensions". India Today.


== External links ==
A Suitable Boy at the Internet Archive
Vikram Seth discusses A Suitable Boy on the BBC World Book Club
Author interview with Vikram Seth at HarperCollins
Analysis of the novel at Let's Talk about Bollywood
A book review
"Total immersion in 1950s India: Vikram Seth's A Suitable Boy", 2010 review by Jo Walton
Richard B. Woodward (May 2, 1993). "Vikram Seth's Big Book". The New York Times Magazine.


== Further reading ==
Shyam S. Agarwalla (1995). Vikram Seth's "A Suitable Boy": Search for Indian Identity. Prestige Books. ISBN 978-81-85218-97-7.
Angela Atkins (26 June 2002). Vikram Seth's Suitable Boy: A Reader's Guide. A&C Black. ISBN 978-0-8264-5707-3.