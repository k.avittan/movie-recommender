Contraband (from Medieval French contrebande "smuggling") refers to any item that, relating to its nature, is illegal to be possessed or sold. It is used for goods that by their nature are considered too dangerous or offensive in the eyes of the legislator—termed contraband in se—and forbidden.
So-called derivative contraband refers to goods that may normally be owned, but are liable to be seized because they were used in committing an unlawful act and hence begot illegally, e.g. smuggling goods; stolen goods – knowingly participating in their trade is an offense in itself, called fencing.


== Law of armed conflict ==

In international law, contraband means goods that are ultimately destined for territory under the control of the enemy and may be susceptible for use in armed conflict. Traditionally, contraband is classified into two categories, absolute contraband and conditional contraband. The former category includes arms, munitions, and various materials, such as chemicals and certain types of machinery that may be used directly to wage war or be converted into instruments of war.
Conditional contraband, formerly known as occasional contraband, consists of such materials as provisions and livestock feed. Cargo of that kind, presumably innocent in character, is subject to seizure if in the opinion of the belligerent nation that seizes them, the supplies are destined for the armed forces of the enemy rather than for civilian use and consumption. In former agreements among nations, certain other commodities, including soap, paper, clocks, agricultural machinery and jewelry, have been classified as non-contraband, but the distinctions have proved meaningless in practice.
Under the conditions of modern warfare, in which armed conflict has largely become a struggle involving the total populations of the contending powers, virtually all commodities are classified by belligerents as absolute contraband.


== American Civil War enslaved people ==

During the American Civil War, Confederate-owned slaves who sought refuge in Union military camps or who lived in territories that fell under Union control were declared "contraband of war". The policy was first articulated by General Benjamin F. Butler in 1861, in what came to be known as the "Fort Monroe Doctrine," established in Hampton, Virginia. By war's end, the Union had set up 100 contraband camps in the South, and the Roanoke Island Freedmen's Colony (1863–1867) was developed to be a self-sustaining colony. Many adult freedmen worked for wages for the Army at such camps, teachers were recruited from the North for their schools by the American Missionary Association, and thousands of freedmen enlisted from such camps in the United States Colored Troops to fight with the Union against the Confederacy.


== Treaties ==
Numerous treaties defining contraband have been concluded among nations. In time of war, the nations involved have invariably violated the agreements, formulating their own definitions as the fortunes of war indicated. The Declaration of London, drafted at the London Naval Conference of 1908–1909 and made partly effective by most of the European maritime nations at the outbreak of World War I, established comprehensive classifications of absolute and conditional contraband. As the war developed, the lists of articles in each category were constantly revised by the various belligerents despite protests by neutral powers engaged in the carrying trade. By 1916, the list of conditional contraband included practically all waterborne cargo. Thereafter, for the duration of World War I, nearly all cargo in transit to an enemy nation was treated as contraband of war by the intercepting belligerent, regardless of the nature of the cargo. A similar policy was inaugurated by the belligerent powers early in World War II.


== Neutral nations ==
Under international law, the citizens of neutral nations are entitled to trade, at their own risk, with any or all powers engaged in war. No duty to restrain contraband trade is imposed on the neutral governments, but no neutral government has the right to interfere on behalf of citizens whose property is seized by one belligerent if it is in transit to another. The penalty traditionally imposed by belligerents on neutral carriers engaged in commercial traffic with the enemy consists of confiscation of cargo. By the Declaration of London, it was extended to include condemnation of the carrying vessel if more than half the cargo was contraband. The right of warring nations to sink neutral ships transporting contraband is not recognized in international law, but the practice was initiated by Germany in World War I and was often resorted to by the Axis Powers in World War II.


== References ==


== Bibliography ==
Caruana, Joseph (2007). "The British Contraband Control Service in the Mediterranean". Warship International. XLIV (4): 367–375. ISSN 0043-0374.


== Sources ==
Dictionary.com
EtymologyOnLine