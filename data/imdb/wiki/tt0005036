Charles Hale Hoyt (July 26, 1859 – November 20, 1900) was an American dramatist.


== Biography ==
Hoyt was born in Concord, New Hampshire.  He had a difficult childhood, as his mother died when he was nine years old.  He graduated at the Boston Latin School and, after being engaged in the cattle business in Colorado for a time, took up newspaper work, first with the St. Albans (Vt.) Advertiser, and later becoming musical and dramatic critic of The Boston Post.
Beginning in 1883, Hoyt turned playwright and wrote a series of 20 farcical comedies (roughly one per year until his death) and a comic opera. Hoyt's plays emphasized individualized characters drawn from the everyday experiences of ordinary people.  His 10th play, A Trip to Chinatown (1891), with its hit tune "The Bowery" and its then-record 657 performance run, and his 1883 play, A Milk-White Flag, were the most successful.  Both were performed at Hoyt's Madison Square Theatre in New York, often called simply "Hoyt's Theatre" during the seven years he ran it.  He was a highly popular playwright and producer, and was very financially successful, thanks in part to the assistance of his business partners, Charles W. Thomas and Frank McKee. A Parlor Match (1884), adapted from a vaudeville act, was another popular Hoyt play.
Hoyt was the 19th-century playwright who did the most to combine baseball with his love for the theatre. Besides having covered Boston Beaneater baseball for The Boston Post, he was a member of the Boston Elks lodge, whose members included fellow theatrical-sports buff Nat Goodwin.  In early 1888, Hoyt was responsible for the stage debut of Boston Elk-Boston Beaneater Mike "King" Kelly in his A Rag Baby, and for the first-ever star billing given to a ballplayer on the stage.  The latter took place in 1895, with longtime Chicago diamond star Cap Anson drawing the distinction through his A Runaway Colt.

Hoyt was also responsible for two of Kelly and Anson's lesser roles: At the end of the 1888 season, he gave Anson a bit part one day in the role of Monk in one of his new pieces; Anson wore "old gray whiskers and an old man’s wig" and stalked forth and shouted, "Good marnin, me min; I want yez to git that hole complaited [completed] to-day."  Those were all of his lines, and New York and Chicago players were present.  Around Christmas that year, and then New Year’s Day of 1889 in New York, Kelly played a "tough baggage-smasher" in Hoyt’s A Tin Soldier.  Kelly was dressed as a tramp, and some of his lines concerned a character named Rats.  When someone said to him, "Do have that Rats licked.  I’ll give $10 to have that Rats licked," Kelly replied, "Make it $15 and I’ll lick him."  Later, when someone said, "Do it well and I’ll make it [$]25," Kelly replied, "Young feller, I’ll blot him off the earth."
Hoyt, once considered one of the most famous citizens of Charlestown, New Hampshire, was twice a member of the New Hampshire Legislature and was Democratic candidate for Speaker.Both his first wife, actress Flora Walsh, and his second wife, actress Caroline Miskel, died after only a few years of marriage. The death of his second wife in 1898 led to Hoyt's being committed to an insane asylum in 1900. Although his stay was brief, he returned to his home in Charlestown, New Hampshire, and died four months later.


== Plays ==
New York debut year otherwise indicated.

Gifford's Luck (Boston 1881)
Cezalia (Boston 1882)
A Bunch of Keys (1883)
A Rag Baby (1883, Broadway 1884)
A Parlor Match (1884)
A Tin Soldier (1886)
The Maid and the Moonshiner (1886)
A Hole in the Ground (1887)
A Brass Monkey (1888)
A Midnight Bell (play) (1889)
A Texas Steer (1890)
A Trip to Chinatown (1891)
A Temperance Town (1893)
A Milk White Flag (1894)
A Runaway Colt (1895)
A Black Sheep (1896)
A Contented Woman (1897)
A Stranger in New York (1897)
A Day and Night in New York (1898)
A Dog in the Manger (Washington, D.C., 1899)


== References ==

Extensive biography on Hoyt with numerous images
Nancy Foell Swortzell (Feb 2000). "Hoyt, Charles Hale". American National Biography Online.
This article incorporates text from a publication now in the public domain: Gilman, D. C.; Peck, H. T.; Colby, F. M., eds. (1905). New International Encyclopedia (1st ed.). New York: Dodd, Mead. 
Howard W. Rosenberg, Cap Anson 2: The Theatrical and Kingly Mike Kelly: U.S. Team Sport's First Media Sensation and Baseball's Original Casey at the Bat (Arlington, Virginia: Tile Books, 2004).  Rosenberg subsequently re-read one of his sources, the Chicago Daily News of October 6, 1888, and discovered that Anson's bit part in 1888 was not in Hoyt's A Bunch of Keys, as appears in his book, but in the role of Monk in what the Daily News merely described as one of Hoyt's "new pieces."


== External links ==
Charles H. Hoyt at the Internet Broadway Database 
Charles H. Hoyt on IMDb