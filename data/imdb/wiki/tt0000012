L'arrivée d'un train en gare de La Ciotat (translated from French into English as The Arrival of a Train at La Ciotat Station, Arrival of a Train at La Ciotat (US) and The Arrival of the Mail Train, and in the United Kingdom the film is known as Train Pulling into a Station) is an 1895 French short documentary film directed and produced by Auguste and Louis Lumière. Contrary to myth, it was not shown at the Lumières' first public film screening on 28 December 1895 in Paris, France: the programme of ten films shown that day makes no mention of it. Its first public showing took place in January 1896. It is indexed as Lumière No. 653.


== Content ==

This 50-second silent film shows the entry of a train pulled by a steam locomotive into the gare de La Ciotat, the train station of the French southern coastal town of La Ciotat, near Marseille. Like most of the early Lumière films, L'arrivée d'un train en gare de La Ciotat consists of a single, unedited view illustrating an aspect of everyday life, a style of filmmaking known as actuality. There is no apparent intentional camera movement, and the film consists of one continuous real-time shot.


== Production ==
This 50-second movie was filmed in La Ciotat, Bouches-du-Rhône, France. It was filmed by means of the Cinématographe, an all-in-one camera, which also serves as a printer and film projector. As with all early Lumière movies, this film was made in a 35 mm format with an aspect ratio of 1.33:1.


== Contemporary reaction ==

The film is associated with a myth well known in the world of cinema. The story goes that when the film was first shown, the audience was so overwhelmed by the moving image of a life-sized train coming directly at them that people screamed and ran to the back of the room. Hellmuth Karasek in the German magazine Der Spiegel wrote that the film "had a particularly lasting impact; yes, it caused fear, terror, even panic." However, some have doubted the veracity of this incident such as film scholar and historian Martin Loiperdinger in his essay, "Lumiere's Arrival of the Train: Cinema's Founding Myth". Others such as theorist Benjamin H. Bratton have speculated that the alleged reaction may have been caused by the projection being mistaken for a camera obscura by the audience which at the time would have been the only other technique to produce a naturalistic moving image. Whether or not it actually happened, the film undoubtedly astonished people unaccustomed to the illusion created by moving images.


== Cinematic techniques ==
This film contains the first example of several common cinematic techniques: camera angle, long shot, medium shot, close-up, and forced perspective.
It is evident from their films, taken as a whole, that the Lumière brothers knew what the effect of their choice of camera placement would be. They placed the camera on the platform to produce a dramatic increase in the size of the arriving train. The train arrives from a distant point and bears down on the viewer, finally crossing the lower edge of the screen.
A significant aspect of the film is that it illustrates the use of the long shot to establish the setting of the film, followed by a medium shot, and then a close-up.  As the camera is static for the entire film, the effect of these various "shots" is achieved by the movement of the subject alone. Nonetheless, these different types of shots are clearly illustrated here. Later filmmakers moved their cameras to achieve these shots.


== 3D and other versions ==
What most film histories leave out is that the Lumière Brothers were trying to achieve a 3D image even prior to this first-ever public exhibition of motion pictures. Louis Lumière eventually re-shot L'Arrivée d’un Train with a stereoscopic film camera and exhibited it (along with a series of other 3D shorts) at a 1934 meeting of the French Academy of Science. Given the contradictory accounts that plague early cinema and pre-cinema accounts, it is plausible that early cinema historians conflated the audience reactions at these separate screenings of L'Arrivée d’un Train. The intense audience reaction fits better with the latter exhibition, when the train apparently was actually coming out of the screen at the audience. But due to the fact that the 3D film never took off commercially as the conventional 2D version did, including such details would not make for a compelling myth.Additionally, Loiperdinger notes that "three versions ofL'Arrivée d'un train en gare de La Ciotat are known to have existed". According to L’œuvre cinématographique des frères Lumière, the Lumière catalogue website, the version most found online is of an 1897 reshot which prominently features women and children boarding the train.


== Current status ==

The short has been featured in a number of film collections including Landmarks of Early Film volume 1. A screening of the film was depicted in the 2011 film Hugo, and in the intro sequence for the 2013 video game Civilization V: Brave New World. The scene of the train pulling in was placed at #100 on Channel 4's two-part documentary The 100 Greatest Scary Moments.


== References ==


== External links ==
L'Arrivée d'un train en gare de La Ciotat on IMDb
Original film (Lumière No. 653) on The Internet Archive
L'Arrivée d'un train en gare de La Ciotat - Relief 3D a stereoscopic/3D film of the 1934 Lumière reshot.
The Lumiere Institute, Lyon, France
L'Arrivée d'un train en gare de La Ciotat: an interpretation at the Cinemaven blog.