Human Wreckage is a 1923 American independent silent drama propaganda film that starred Dorothy Davenport and featured James Kirkwood, Sr., Bessie Love, and Lucille Ricksen. The film was co-produced by Davenport and Thomas H. Ince and distributed by Film Booking Offices of America. No print of this film is known to exist today, and it is considered a lost film.Davenport's husband Wallace Reid was addicted to morphine, which had been prescribed to him after an injury. The film portrayed the dangers of drug addiction and was shown across the country by Davenport herself, billed as Mrs. Wallace Reid, in an early example of what would later be called a roadshow engagement.


== Plot ==
Ethel McFarland (Davenport) presents her attorney husband, Alan (Kirkwood), with the case of a dope addict named Jimmy Brown (Hackathorne). With the help of Alan's impassioned defense, Jimmy gets acquitted.
Alan feels the pressures of his job and is introduced to a doctor at his club. When he becomes addicted, he is blackmailed by his peddlers to represent their friends in court. Jimmy, now off the smack and a taxi driver, hears of these goings-on. When he discovers that his passenger is the leader of the dope ring, he resolves to aid the war on narcotics by crashing the vehicle head-on into an oncoming train, killing them both. Alan gets treated for his addiction and begins to fight the pushers in court, all the while pushing for stronger laws against addictive substances.
At the film's close, Davenport addresses the audience directly, imploring them to support her in her crusade to wipe out the menace of narcotics.


== Cast ==


== Censorship ==

On January 14, 1923, Will H. Hays was appointed as president of the Motion Picture Producers and Distributors of America. Wallace Reid died just four days later, on January 18.
Although it took years for the so-called "Hays Code" to be finalized, the Code did set certain standards for movies from the very beginning, including a ban on any reference to drug use. Despite this, Davenport received a dispensation from Hays allowing her to produce Human Wreckage because of its anti-drug message.The film was banned by the British Board of Film Censors in 1924.


== See also ==
List of lost films


== References ==


== External links ==
Human Wreckage on IMDb
Human Wreckage at AllMovie
Human Wreckage at the American Film Institute Catalog
Human Wreckage at SilentEra
Lantern slide