Shadowed, also known as The Gloved Hand, is a 1946 American film noir crime film directed by John Sturges and starring Anita Louise, Lloyd Corrigan, and Robert Scott.


== Plot ==
Salesman Fred J. Johnson manages to hit a hole-in-one as he plays golf one day, and he writes his initials and the date on the lucky ball. He swings at the same ball once more, but sends it into a ditch instead of towards the hole. When he goes to the ditch to get the ball, he finds a dead body instead, and next to the body lies a small package, which contains plates for forging dollar bills.
Fred keeps the package and realizes it belongs to another member in the same golf club, Tony Montague, when he happens to overhear the man talking to his wife, Edna. Tony finds Fred's marked golf ball and suspects that a man with the initials F.J. Has taken the package. Before leaving, Tony raises his voice on the golf course, warning "F.J." not to go to the police with the package, threatening to kill him and his whole family if he did.
Fred goes back to his family, and opens the package, finding the address to a print shop with the plates. He doesn't call the police, remembering Tony's threat. When Tony and Edna get back to their boss, Lefty, they are chastized for losing the package and sent to recover it.
The murder is all over the news the next day, and Fred discovers that his lucky charm is missing from the wrist band of his watch. His daughter Carol goes out on a date with a banker named Mark Bellaman, and his other daughter Ginny goes to the golf course with her beau Lester Binkey. Examining the location where the body was found, she finds her father's lucky charm.
Tony goes back to the golf course to get a list of everyone who was there the day before, and pretends to be a detective investigating the murder. He sees Ginny and asks her name, then realizing she is Fred's daughter. He offers to drive her home to her father, saying he is an old friend of his.
When Fred goes to send the plates to the police anonymously by mail, he is followed by Lefty, who threatens to kill Ginny if Fred involves the police.
Ginny talks to police Lt. Braden about the murder and shows him her father's lucky charm she found at the crime scene. This leads Braden to question Fred, and he seems suspicious enough for Braden to order one of his men, Sellers, to tail Fred afterwards.
Layer that night Ginny is kidnapped by Lefty and his gang. They tell Fred that she will be killed if he doesn't give them the plates. They agree on an exchange, but Fred won't give the plates away even after Ginny is returned safely. Tony pulls a gun on him, but his wife Edna panics and rushes to the window to scream for help. Tony shoots Edna in her back and Fred manages to knock down Tony with a gardening tool. Lefty tries to escape but is caught outside by the police.
In the papers the next day, Fred is mentioned as a hero who caught the murderer.


== Cast ==
Anita Louise as Carol Johnson
Lloyd Corrigan as Fred Johnson
Michael Duane as Lt. Braden
Mark Roberts as Mark Bellman (as Robert Scott)
Doris Houck as Edna Montague
Terry Moore as Virginia 'Ginny' Johnson (as Helen Koford)
Wilton Graff as Tony Montague
Eric Roberts as Lester Binkey
Paul E. Burns as Lefty


== References ==


== External links ==
Shadowed on IMDb
Shadowed at AllMovie