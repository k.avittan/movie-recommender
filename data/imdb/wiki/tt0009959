The Bramble Bush is a 1960 American drama film, based on the controversial novel of the same name, directed by Daniel Petrie and starring Richard Burton, Angie Dickinson, Barbara Rush, Jack Carson and James Dunn. It was released by Warner Bros. 


== Plot ==
Dr. Guy Montford moves back to his seaside Massachusetts hometown at the request of old friend Larry McFie, who is dying of cancer. Over the objections of Larry's father, hospital administrator Dr. Sol Kelsey puts the patient in Guy's personal care.
Guy runs into Bert Mosley, an unscrupulous lawyer who is running for district attorney. He is unaware that Mosley is having an affair with Kelsey's chief nurse, Fran, until one night he is summoned to a motel fire and finds that Bert and Fran were secretly meeting there.
Larry knows his condition is terminal, despite Guy's mentions of a possible miracle drug. Larry's death-bed wish is that his wife, Margaret, will end up married to Guy, whom he trusts. Sam McFie, for some reason, does not want his son being treated by Guy.
Margaret goes sailing with Guy, but is devoted to her husband. She is also unhappy with Guy's cruel treatment of a town drunk, Stew, until she learns that the man once had an illicit romance with Guy's mother, resulting in the suicide of his father. Margaret and Guy briefly become lovers.
Fran has hopelessly fallen in love with Guy, but is being blackmailed by reporter Parker Welk, who knows of the motel affair and threatens to go public unless Fran poses for provocative photographs. Bert finds out about it and assaults Parker, who receives medical attention from Guy.
Complications develop when Larry pleads with Guy to put him out of his misery and Margaret discovers she is pregnant from the one-night stand. Guy can't bear to see his friend in pain. He gives him a fatal overdose of morphine. Fran realizes what happened and tells Bert, who has Guy placed under arrest.
Larry's father lies on the witness stand that his son feared for his life in Guy's care, believing the doctor was in love with his wife. Sol, however, testifies that he personally heard Larry beg Guy for euthanasia. A jury acquits Guy, who hopes he and Margaret can move beyond all that has happened someday.


== Cast ==
Richard Burton as Dr. Guy Montford
Angie Dickinson as Fran
Jack Carson as Bert Mosley
Barbara Rush as Margaret
Frank Conroy as Sol Kelsey
Carl Benton Reid as Sam McFie
Tom Drake as Larry McFie
Henry Jones as Parker Welk
James Dunn as Stew


== Production ==

The film was based on a novel by Charles Mergendahl which was published in 1958. Reviewers compared it with other novels about the underbelly of small towns such as Peyton Place and King's Row.In August 1958, film rights were bought by Milton Sperling, who had a production unit, United States Pictures at Warner Bros.In January 1959 Richard Burton signed to play the male lead. Sperling wanted Carolyn Jones to play the female lead. Eventually the part went to Angie Dickinson, who had just impressed in Rio Bravo. In February Daniel Petrie, best known for his work on television including adaptations of Wuthering Heights, signed to direct.A support role was given to James Dunn, making his first film in eight years, and his first movie at Warners since 1935.Filming began 30 March 1959. The film was mostly shot at the studio, with a few days location work at Newport and Balboa to look like Cape Cod.During filming, Mergendahl died after a fall at his home. He was only 40 years old.Bantam Books published 1.5 million editions of the novel to coincide with the release of the film. It was the largest order in Bantam's history.


== Reception ==
The film earned rentals of $3 million in the United States and Canada.


== See also ==
List of American films of 1960


== References ==


== External links ==
The Bramble Bush at TCMDB
The Bramble Bush at Letterbox DVD
Review of film at Variety
The Bramble Bush on IMDb