Cabbage Patch Kids are a line of soft sculptured dolls sold by Xavier Roberts and registered in the United States copyright office in 1978 as 'The Little People'. The brand was renamed 'Cabbage Patch Kids' when the dolls went into mass production in 1982. 
The doll brand was one of the most popular toy fads of the 1980s and one of the longest-running doll franchises in the United States. The characters appeared in many other Cabbage Patch merchandising products ranging from animated cartoons to record albums to board games.


== Production history ==


=== Creation and development ===
According to Roberts, as a 21-year-old art student, he utilized the quilting skills he learned from his mother and the historic technique of "needle molding" to develop his own line of fabric sculptures. He called these hand-stitched, one-of-a-kind, soft fabric sculptures "The Little People". (Other soft sculpture dolls dating back to the 19th century were created using similar needle molding techniques.) His Little People were not offered for sale, but were "adopted" each with their own individual name and birth certificate.  Instead of paying a purchase price, buyers of Little People would have to pay an adoption fee.The Little People were first sold at arts and crafts shows, then later at Babyland General Hospital, an old medical clinic that Roberts and his friends-turned-employees converted into a toy store, in Cleveland, Georgia.An early transaction at Babyland General Hospital worth noting is the sale of the only Little People quintuplets ever made at the facility.  Bennie and Jeannie Shelton of Cumming, Georgia paid $5,000 to buy the one-of-a-kind set of five identical dolls.  Also worth noting is one of Roberts' first Little People dolls he ever created sold at an auction in Virginia in the early 1980s for $3,000.
According to the discovery legend Roberts came up with for his Little People, and later reproduced on every Cabbage Patch Kids product from 1983 onward:

Xavier Roberts was a ten-year-old boy who discovered the Cabbage Patch Kids by following a BunnyBee behind a waterfall into a magical Cabbage Patch, where he found the Cabbage Patch babies being born. To help them find good homes he built BabyLand General in Cleveland, Georgia where the Cabbage Patch Kids could live and play until they were adopted. BunnyBees are bee-like creatures with rabbit ears they use as wings. They pollinate cabbages with their magic crystals to make Cabbage Patch babies. Colonel Casey is a large stork who oversees Babyland General Hospital. He's the narrator of the Cabbage Patch Kids' story.  Otis Lee is the leader of the gang of Cabbage Patch Kids that befriended Xavier.


=== Coleco years ===
The name change to Cabbage Patch Kids was made in 1982 when Xavier's company, Original Appalachian Artworks, began to license a smaller version of the handmade creations to a toy manufacturer named Coleco which began mass production the same year. The Coleco Cabbage Patch Kids had large, round vinyl heads (originally of a different, hard plastic), and soft fabric bodies, and were produced from 1982 to 1989, many at a factory in Amsterdam, New York. The first two years production was all from the Far East, with nine head variations produced and computer-matched with bodies to ensure each doll was "different". It was, in fact, a marketing ploy that worked quite well as a wide range of variations resulted. (Source: Larry Moniz, then senior account supervisor for the Coleco account at Richard Weiner Public Relations in NYC.)
At the peak of their popularity, the dolls were a must-have toy for Christmas. Parents across the United States flocked to stores to try to obtain one of the Cabbage Patch Kids for their children, with fights occasionally erupting between parents over the hard-to-find dolls. In later years, Coleco introduced variants on the original Cabbage Patch Kids, and derivatives of the original line of dolls continued to be marketed.


=== International variations ===
In the 1980s when Coleco was producing the dolls for the North American market, the global craze was fulfilled by other companies:

Jesmar Toy Company made the dolls for the European Market, including Spain, Italy, and West Germany.
Lili Ledy Toy Company made the dolls for Mexico and South America.
Triang-Pedigree Toy Company made the dolls for South Africa.
Tsukuda Toy Company manufactured the dolls for Japan and Asia.The Dolls manufactured by each of these companies, and along with the factories that produced the dolls for North America, produced dolls that were slightly different from one another. Dolls that were made for consumers in other countries than the United States hold a higher value in the eye of some American collectors.


=== Hasbro years ===
Hasbro took over the rights to produce Cabbage Patch dolls in 1988 after Coleco filed for Chapter 11 bankruptcy, and continued to make the dolls with various gimmicks, including dolls that played kazoos. Some of the more popular doll lines to come out under the Cabbage Patch Kids name included the "Birthday Kids", "Splash 'n' Tan Kids", and "Pretty Crimp and Curl". Hasbro produced a 10th anniversary doll, available in either Caucasian or African American styles. It was unique in that the vinyl faces were covered in fabric and the facial design was screened onto the fabric as a call back to the original soft sculpture dolls. Hasbro gradually began making the dolls for younger children, which led to smaller and smaller dolls.  Although Cabbage Patch dolls were still best selling toys, Hasbro never really revitalized the Cabbage Patch market. In 1994, Mattel acquired the licensing rights to the dolls from Original Appalachian Artworks.


=== Mattel years ===
In 1994, Mattel took over the Cabbage Patch brand, including production. The first Mattel Cabbage Patch dolls hit the stores in 1995.The Mattel Cabbage Patch dolls are not limited to cloth bodies and included dolls made from vinyl, which produced a more durable play doll. The Mattel dolls are mostly sized 14" or smaller, and most variants were individualized with a gimmick to enhance their collectibility, e.g. some dolls played on water-toys, swam, ate food, or brushed their teeth.Some memorable Mattel lines include the updated Kids line of basic cloth dolls that came with birth certificates, the OlympiKids that were made to coincide with the 1996 Olympics, and the Cabbage Patch Fairies. Additionally, to celebrate the dolls' 15th anniversary, Mattel created a line of exclusively female dolls with reproduction face molds, dressed in a reproduction dresses reminiscent of the original line and packaged in retro style box. These were 16 inches tall, the same measurement of the first Coleco Cabbage Patch Kids.


=== Toys "R" Us Kids ===
In 2001, retailer Toys "R" Us took over the Cabbage Patch brand from Mattel, producing 20-inch Kids and 18-inch babies, both with cloth bodies and vinyl heads. They were packaged in cardboard cabbage leaf seats. In 2001, the 20-inch dolls debuted in the Times Square flagship store. These were created to celebrate the 20th anniversary of the line, and were available both online and in stores around the US.


=== Play Along Toys ===
The Toys "R" Us line lasted until Play Along toys obtained exclusive licensing rights to produce the Cabbage Patch Kids doll line. In 2003, Play Along launched a Cabbage Patch Kids 25th Anniversary collection using some of the original head sculpts from the very first Coleco editions. Play Along also partnered with Carvel Ice Cream in a co-branding campaign.  The resulting co-branded Cabbage Patch Kids were packaged with a Carvel-branded ice cream cone.


=== Jakks Pacific ===
JAKKS Pacific acquired Play Along Toys and assumed the master toy licensee (c2011) for the Cabbage Patch Kids.  Jakks introduced a 14-inch Cabbage Patch Kids Fashionality(TM) line and other Cabbage Patch Kid products. In 2013 Jakks Pacific released the Celebration edition to commemorate the 30th Birthday of the licensed Cabbage Patch Kids.


=== Wicked Cool Toys ===
Wicked Cool Toys (Owned by Jazwares) is now the current master toy licensee for the Cabbage Patch Kids.In this line, WCT released new additions like Little Sprouts, a toyline of tiny collectable dolls, and Adoptimals, plush pets who interact with the Kids.


== Cabbage Patch Kids brand ==
The original 1982 Cabbage Patch Kids license agreement with Coleco Industries was negotiated by Roger L. Schlaifer on behalf of Schlaifer Nance & Company, the exclusive worldwide licensing agent for Roberts' company at the time.Following Schlaifer Nance & Company's signing of Coleco Industries, SN&C signed over one hundred and fifty licenses for branded products ranging from the first children's licensed character diapers and low-sugar cereal to clothing, backyard pools, and thousands of other children's products – generating over $2 billion in retail sales for 1984, alone. Total sales during the Schlaifers' tenure exceeded $4.5 billion. While sales of the dolls and other licensed products declined precipitously in the late 1980s, the dolls have become a mainstay of the toy industry, and one of the few long-running doll brands in history.


== Porcelain Cabbage Patch Kids ==
These dolls were available by direct mail from the Danbury Mint. They have a rigid fabric body with porcelain legs, arms, and head.


== Talking Cabbage Patch Kids ==
An extension to the line was the "Talking Cabbage Patch Kid" introduced by Coleco, equipped with a voice chip, touch sensors, a microphone, short range 49 MHz AM transmitter and receiver for communicating with other such dolls. Touch sensors in the hands enabled the toy to detect when and how it was being played with in response to its vocalizations. For example, the doll might say "hold my hand" and give an appropriate speech response when the touch sensor in either hand detected pressure. It also had a movement detector to show the positioning of the doll and whether it was sensed to be on its belly, back, or even upside down. A special plastic 'drinking' cup containing a hidden magnet, which could be identified with the aid a small reed relay in the built into the head of the toy above the mouth, to signify when it should be seen to be 'drinking'. A more remarkable effect occurred when one doll detected the presence of another through its 49 MHz AM transmitter/receiver. The dolls were programmed to signal their "awareness" of each other with a short phrase, e.g. "I think there's someone else to play with here!", and then to initiate simple conversations between the dolls themselves with enough randomness to sound somewhat natural. The joint synchronised singing of 'rounds' being particularly impressive. The inclusion of the microphone was to delay the search and communication with another of its type when the ambient noise was above a certain level.


== Babyland General Hospital ==
Babyland General Hospital is the "birthplace" of Cabbage Patch Kids and is located in Cleveland, Georgia. Roberts converted an old clinic into a "hospital" from which to sell his dolls, originally called "Little People". The facility is presented as a birthing, nursery, and adoption center for the Cabbage Patch Kids. In accordance with the theme, employees dress and act the parts of the doctors and nurses caring for the dolls as if they are real. Babyland General moved to a new facility on the outskirts of Cleveland, Georgia in 2010 and has been voted one of the Travel Channel's top 10 toylands.


== Adaptations ==
Five half-hour animated specials were produced based on the Cabbage Patch Kids. The first, The Cabbage Patch Kids' First Christmas, was produced by Ruby-Spears Enterprises and premiered on ABC on December 7, 1984. The second, Cabbage Patch Kids: The New Kid, was a stop-motion animated special produced by Goldhill Entertainment, and aired on the FOX Kids Network programming block on August 26, 1995.  It was followed by three other stop-motion animated specials, Cabbage Patch Kids: The Club House in 1996; Cabbage Patch Kids: The Screen Test in 1997; and Cabbage Patch Kids: Vernon’s Christmas in 1999.


== Controversies ==


=== Lawsuits ===
Though Xavier Roberts was the creator of the Cabbage Patch Kids brand, many of the brand's defining characteristics – such as the dolls' overly round faces and that they came with an adoption certificate – were taken from Martha Nelson Thomas, an American folk artist from Kentucky. Before Roberts became involved in the toy industry, Thomas had created and marketed her own line of dolls, called Doll Babies, which she sold at local arts and craft shows and markets. The two crossed paths at a state fair in 1976, whereupon Roberts began purchasing Thomas' dolls to sell at a profit at his own store in Georgia. Thomas eventually confronted Roberts about his unethical business practices and ceased to sell additional dolls to him, prompting him to turn to a manufacturing company in Hong Kong to mass produce dolls similar in appearance to Thomas' at a cheaper cost to him. Thomas brought suit against Roberts and eventually settled with him out of court for an undisclosed amount in 1985. She and her husband, Tucker Thomas, told the press that she was more upset by the corruption of her dolls, for which she cared deeply, than the money she'd lost as the result of Roberts' actions. Thomas died in 2013, at the age of 62, with her most favorite dolls attending her funeral alongside her family members and friends.Xavier Roberts himself later brought a $30 million lawsuit against Topps, the company that produced grotesque trading cards parodying his company's dolls called the Garbage Pail Kids, for copyright infringement. In 1987, in the midst of the legal proceedings, the makers of the Garbage Pail Kids announced their decision to cease parodying the Cabbage Patch Kids, to which Roberts commented to the press "I could scream, I'm so excited."


=== Product safety ===
One line of Cabbage Patch Kids dolls, the Cabbage Patch Snacktime Kids, was designed to "eat" plastic snacks. The mechanism enabling this was a pair of one-way smooth metal rollers behind plastic lips. The snacks would exit the doll's back and "magically" appear into a backpack. The mechanism could be de-activated by releasing the backpack. They were extremely popular during Christmas 1996. The line was voluntarily withdrawn from the market following an agreement between Mattel and the Consumer Product Safety Commission in January 1997 following several incidents where children got their fingers or hair stuck in the dolls' mouths leading to safety warnings from Connecticut's consumer protection commissioner, Mark Shiffrin.


== Timeline ==
1977: Xavier Roberts is introduced to Martha Nelson Thomas' "Doll Babies" concept
1978: The first "Little People Originals" were delivered by Xavier Roberts, who incorporated Original Appalachian Artworks, Inc.
1981: There was coverage of the dolls' popularity in Newsweek, The Wall Street Journal, and Atlanta Weekly.
1982: Original Appalachian Artworks, Inc. signed a long term licensing agreement with Coleco Industries to produce the dolls in August 1982.
1983: Cabbage Patch Kids were introduced with great fanfare at the International Toy Fair in NYC. By October, riots were occurring in stores around the country. The dolls made the cover of Newsweek before Christmas and stories of their success were heralded around the world.
1984: Sales for Cabbage Patch Kids branded products, from toys to children's apparel, came close to the record setting $2 billion mark. The CPK record, titled Cabbage Patch Dreams, produced by the Chapin Brothers for Parker Brothers' music, went Gold and Platinum. The video game Cabbage Patch Kids: Adventures in the Park was released.
1985: Cabbage Patch Kids low-sugar breakfast cereal and real children's character art diapers were introduced. The Cabbage Patch Kids' First Christmas was number one in its time slot on ABC.
1986: The first talking Cabbage Patch Kids were released.
1988: Coleco Industries filed for bankruptcy, but the dolls continued to be made, with the licensing rights being granted to Hasbro Industries and later to Mattel.
1992: Cabbage Patch Kids were named the official mascot of the 1992 U.S. Olympic team and members of the team were given their own dolls to take to the games.
1996: The Cabbage Patch Snacktime Kids were released.
1999: By popular vote, the dolls were selected as one of the 15 commemorative US postage stamps representing the 1980s. They were voted fifth behind E.T, Washington's Vietnam Veterans Memorial, the end of the Berlin Wall and video games.
2008: Democrat and Republican US Presidential and Vice Presidential Candidates had their own Cabbage Patch Kids. Barack Obama was depicted with a blue suit. John McCain was depicted in a suit with grey hair. Joe Biden was also depicted in a suit with his hair slicked up. Sarah Palin was depicted in a trademark suit and skirt with high heeled pumps. Also, Palin's signature hair and eyeglasses were featured.


== See also ==
Garbage Pail Kids
The Garbage Pail Kids Movie
Garbage Pail Kids (TV series)


== References ==


== Further reading ==
Hoffman, William (1984). Fantasy: The Incredible Cabbage Patch Phenomenon. Dallas: Taylor Publishing. ISBN 9780878333868. OCLC 10996773.


== External links ==

Cabbage Patch Kids Official Site
Cabbage Patch Kid Restoration Official Site
Cabbage Patch Kid mania - CBC Digital Archives
Urban Legends Reference Page on Cabbage Patch Kids legends
Urban Legends Reference Page on Cabbage Patch death certificates
Whatever Happened To Cabbage Patch Dolls?
The Secret History of Cabbage Patch Kids