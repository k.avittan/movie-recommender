Smouldering (British English) or smoldering (American English; see spelling differences) is the slow, flameless form of combustion, sustained by the heat evolved when oxygen directly attacks the surface of a condensed-phase fuel. Many solid materials can sustain a smouldering reaction, including coal, cellulose, wood, cotton, tobacco, cannabis, peat, plant litter, humus, synthetic foams, charring polymers including polyurethane foam and some types of dust. Common examples of smouldering phenomena are the initiation of residential fires on upholstered furniture by weak heat sources (e.g., a cigarette, a short-circuited wire), and the persistent combustion of biomass behind the flaming front of wildfires.


== Fundamentals ==

The fundamental difference between smouldering and flaming combustion is that smouldering occurs on the surface of the solid rather than in the gas phase. Smouldering is a surface phenomenon but can propagate to the interior of a porous fuel if it is permeable to flow. The characteristic temperature and heat released during smouldering are low compared to those in the flaming combustion (i.e., ~600 °C  vs. ~1500 °C). Smouldering propagates in a creeping fashion, around 0.1 mm/s (0.0039 in/s), which is about ten times slower than flames spread over a solid. In spite of its weak combustion characteristics, smouldering is a significant fire hazard. Smouldering emits toxic gases (e.g., carbon monoxide) at a higher yield than flaming fires and leaves behind a significant amount of solid residue. The emitted gases are flammable and could later be ignited in the gas phase, triggering the transition to flaming combustion.


== Smouldering materials ==

Many materials can sustain a smouldering reaction, including coal, tobacco, decaying wood and sawdust, biomass fuels on the forest surface (duff) and subsurface (peat), cotton clothing and string, and polymeric foams (e.g., upholstery and bedding materials). Smouldering fuels are generally porous, permeable to flow and formed by aggregates (particulates, grains, fibres or of cellular structure). These aggregates facilitate the surface reaction with oxygen by allowing gas flow through the fuel and providing a large surface area per unit volume. They also act as thermal insulation, reducing heat losses. The most studied materials to date are cellulose and polyurethane foams.


== Threats from smouldering ==
The characteristics of smouldering fires make them a threat of new dimensions, taking the form of colossal underground fires or silent fire safety risks, as summarized below.

 Fire safety: The main hazards posed by smouldering arise from the fact that it can be easily initiated (by heat sources too weak to ignite flames) and is difficult to detect. Fire statistics draw attention to the magnitude of smouldering combustion as the leading cause of fire deaths in residential areas (i.e., more than 25% of the fire deaths in the United States are attributed to smoulder-initiated fires, with similar figures in other developed countries). A particularly common fire scenario is a cigarette igniting a piece of upholstered furniture. This ignition leads to a smouldering fire that lasts for a long period of time (in the order of hours), spreading slowly and silently until critical conditions are attained and flames suddenly erupt; fire-resistant cigarettes have been developed to reduce the risk of fire due to smouldering. Smouldering combustion is also a fire-safety concern aboard space facilities (e.g., International Space Station), because the absence of gravity is thought to promote smouldering ignition and propagation.
 Wildfires: Smouldering combustion of the forest ground does not have the visual impact of flaming combustion; however, it has important consequences for the forest ecosystem. Smouldering of biomass can linger for days or weeks after flaming has ceased, resulting in large quantities of fuels consumed and becoming a global source of emissions to the atmosphere. The slow propagation leads to prolonged heating and might cause sterilizations of the soil or the killing of roots, seeds, and plant stems at the ground level.
Subsurface fires: Fires occurring many meters below the surface are a type of smouldering event of colossal magnitude. Subsurface fires in coal mines, peat lands and landfills are rare events, but when active they can smoulder for very long periods of time (months or years), emitting enormous quantities of combustion gases into the atmosphere, causing deterioration of air quality and subsequent health problems. The oldest and largest fires in the world, burning for centuries, are smouldering fires. These fires are fed by the oxygen in the small but continuous flow of air through natural pipe networks, fractured strata, cracks, openings or abandoned mine shafts which permit the air to circulate into the subsurface. The reduced heat losses and high thermal inertia of the underground together with high fuel availability promote long-term smouldering combustion and allow for creeping but extensive propagation. These fires prove difficult to detect, and frustrate most efforts to extinguish them. The dramatic 1997 peatland fires in Borneo caused the recognition of subsurface smouldering fires as a global threat with significant economic, social and ecological impacts. The summer of 2006 saw the resurgence of the Borneo peat fires.
World Trade Center debris: After the attack, fire and subsequent collapse of the Twin Towers on September 11, 2001, the colossal pile (1.8 million tons) of debris left on the site smouldered for more than five months. It resisted attempts by fire fighters to extinguish it until most of the rubble was removed. The effects of the gaseous and aerosolized products of smouldering on the health of the emergency workers were significant but the details are still a matter of debate.


== Beneficial applications ==
Smouldering combustion has some beneficial applications.

Biochar is the charcoal produced from the smouldering and/or pyrolysis of biomass. It has the potential to be a short-term solution to lower CO2 concentrations in the atmosphere. Charcoal is a stable solid and rich in carbon content, and thus, it could be used to lock carbon in the soil. The natural decomposition and burning of trees and agricultural waste contributes with a large amount of CO2 released to the atmosphere. Biochar could be used to store part of this carbon content in the ground, at the same time that its presence in the earth increases soil productivity. Biochar has carbon negative applications for energy production.
In wildfire management, smouldering controlled burns can be used to reduce shallow layers of natural fuels at a slow propagation rate. These fires have two benefits when kept in very shallow layers: they are easy to control and result in little damage to the forest stand
Smouldering of tires produces tar and energy at the same time, fostering the recycling of tires.
In situ combustion of petroleum sites is increasingly used for oil recovery when traditional extraction methods prove inefficient or too costly.
In situ smouldering combustion is being explored as a novel remediation technology for soil contamination.


== See also ==
Grilling


== References ==


== External links ==
Microgravity smouldering (NASA-funded) research facility at the University of California at Berkeley.
de Souza Costa, Fernando; Sandberg, David (2004). "Mathematical model of a smoldering log" (PDF). Combustion and Flame (139): 227–238. Retrieved 2009-02-06.
Fire Danger. Cigarette Litter.org