My Life in the Bush of Ghosts is a studio album by Brian Eno and David Byrne, released in February 1981. It was Byrne's first album without his band Talking Heads. it integrates sampled vocals and found sounds, African and Middle Eastern rhythms, and electronic music techniques. It was recorded prior to Eno and Byrne's work on Talking Heads' 1980 album Remain in Light, but problems clearing samples delayed its release by several months.
The extensive sampling is considered innovative, though its influence on later sample-based music genres is debated. Pitchfork listed it as the 21st best album of the 1980s, while Slant Magazine listed the album at No. 83 on its list of the "Best Albums of 1980s".


== Recording ==
Eno and Byrne first worked together on More Songs About Buildings and Food, the 1978 album by Byrne's band Talking Heads. My Life in the Bush of Ghosts was primarily recorded during a break between the Talking Heads albums Fear of Music (1979) and Remain in Light (1980), both produced by Eno.
Eno described My Life as a "vision of a psychedelic Africa". Rather than conventional pop or rock singing, most of the vocals are sampled from other sources, such as commercial recordings of Arabic singers, radio disc jockeys, and an exorcist. Musicians had previously used similar sampling techniques, but, according to Guardian writer Dave Simpson, it had never before been used "to such cataclysmic effect". In 2001, Eno cited Holger Czukay's experiments with dictaphones and short-wave radios as earlier examples of sampling. He felt that the "difference was, I suppose, that I decided to make [sampling] the lead vocal". The release was delayed while legal rights were sought for the large number of samples used in the album.The album title is derived from Amos Tutuola's 1954 novel My Life in the Bush of Ghosts. According to Byrne’s 2006 sleeve notes, neither he nor Eno had read the novel, but felt the title "seemed to encapsulate what this record was about".Soon after the album was released, an Islamic organization in London objected to the use of samples of Muslim chants in the track "Qu'ran", considering it blasphemy. Byrne and Eno removed the track from later pressings. In 2006, Byrne said:

We thought, "Okay, in deference to somebody's religion, we'll take it off." You could probably argue for and against monkeying with something like that. But I think we were certainly feeling very cautious about this whole thing. We made a big effort to try and clear all the voices, and make sure everybody was okay with everything ... So I think in that sense we reacted maybe with more caution than we had to.


=== Samples ===
Notes below indicated the voices sampled, from the liner notes.Side one

"America Is Waiting" – Ray Taliaferro of KGO NEWSTALK AM 810, San Francisco, April 1980.
"Mea Culpa" – Inflamed caller and smooth politician replying, both unidentified. Radio call-in show, New York, July 1979.
"Regiment" – Dunya Yunis  [sic], Lebanese mountain singer, from The Human Voice in the World of Islam (Tangent Records TGS131)
"Help Me Somebody" – Reverend Paul Morton, broadcast sermon, New Orleans, June 1980.
"The Jezebel Spirit" – Unidentified exorcist, New York, September 1980.Side two

"Qu'ran" – Algerian Muslims chanting the Qur'an. (same source as track 3)
"Moonlight in Glory" – The Moving Star Hall Singers, Sea Island, Georgia. (From The Moving Star Hall Singers, Folkways FS 3841), produced by Guy Carawan.
"The Carrier" – *Dunya Yunis. (same source as track 3)
"A Secret Life" – Samira Tewfik ("Hobak Mor"), Lebanese popular singer. (from Les Plus Grandes Artistes du Monde Arabe, EMI)
"Come with Us" – *Unidentified radio evangelist, San Francisco, April 1980


== Packaging ==
The original package design was created by Peter Saville. The cover image was created by pasting small cutout humanoid shapes onto a monitor and pointing a camera at it to create video feedback, infinitely multiplying the shapes: "Somehow, despite it being very techie, these techniques also seemed analogous to what we were doing on the record. It was funky as well as being techie. Extremely lo-tech, actually, and not what you were supposed to do with a TV set."


== Critical reception ==
In Rolling Stone, Jon Pareles applauded My Life in the Bush of Ghosts as "an undeniably awesome feat of tape editing and rhythmic ingenuity" that generally avoids "exoticism or cuteness" by "complementing the [speech] sources without absorbing them". Village Voice critic Robert Christgau was less impressed, finding the recordings "as cluttered and undistinguished as the MOR fusion and prog-rock it brings to the mind's ear", while lacking "the songful sweep of Remain in Light or the austere weirdness of Jon Hassell". AllMusic critic John Bush describes it as a "pioneering work for countless styles connected to electronics, ambience and Third World music".In a 1985 interview, singer Kate Bush said that My Life had "left a very big mark on popular music". Pink Floyd keyboardist Rick Wright said it "knocked me sideways when I first heard it – full of drum loops, samples and soundscapes. Stuff that we really take for granted now, but which was unheard of in all but the most progressive musical circles at the time... The way the sounds were mixed in was so fresh, it was amazing."


== Reissue ==
My Life in the Bush of Ghosts was reissued on March 27, 2006 in the UK and April 11, 2006 in the US, remastered and with seven extra tracks. To mark the reissue, two songs were made available to download, consisting of the entire multitracks. Under the Creative Commons License, members of the public are able to download the multitracks, and use them for their own remixes.


== Track listing ==
All music is composed by Brian Eno and David Byrne, except "Regiment" by Eno, Byrne, and Michael "Busta Cherry" Jones.In the 1982 second edition, the track "Qu'ran"—which features samples of Qur'anic recital—was removed at the request of the Islamic Council of Great Britain. In its place "Very, Very Hungry" (the B-side of "The Jezebel Spirit" 12" EP) was substituted. The first edition of the CD (1986) included both tracks, with "Very, Very Hungry" as a bonus track. Later editions (1990 and later) followed the revised LP track order without "Qu'ran."


=== Ghosts ===
A widely circulated bootleg of outtakes was released in 1992 as Klondyke Records KR 21. Sound quality is nearly equal to the original CD release.

"Interview" – 3:03 (excerpt from Brian's February 2, 1980 KPFA-FM interview, where he discusses recording the album)
"Mea Culpa" – 4:56
"Into the Spirit Womb"  [sic](actual title as spoken on the track is "Into the Spirit World") – 6:07 ("The Jezebel Spirit" with the original Kathryn Kuhlman vocals, which her estate refused to license)
"Regiment"  (Byrne, Eno, Jones) – 4:13
"The Friends of Amos Tutuola" – 2:01 ("Two Against Three" in the official 2006 re-release)
"America Is Waiting"  (Byrne, Eno, Bill Laswell, Wright, David Van Tieghem) – 3:42
"The Carrier" – 4:22
"Very Very Hungry" – 3:25
"On the Way to Zagora" – 2:43 ("Pitch to Voltage" in the official 2006 re-release)
"Les Hommes Ne Le Sauront Jamais" – 3:33 ("Number 8 Mix" in the official 2006 re-release)
"A Secret Life" – 2:34
"Come with Us" – 2:42
"Mountain of Needles" – 2:31Except as noted, the tracks are the same mix as originally released.


=== 2006 expanded issue ===
Remastered, with bonus tracks. 2, 3, 7 and 8 are longer than on the original album.

"America Is Waiting"  (Byrne, Eno, Laswell, Wright, Van Tieghem) – 3:38
"Mea Culpa" – 4:57
"Regiment"  (Byrne, Eno, Jones) – 4:11
"Help Me Somebody" – 4:17
"The Jezebel Spirit" – 4:56
"Very, Very Hungry" – 3:21
"Moonlight in Glory" – 4:30
"The Carrier" – 4:19
"A Secret Life" – 2:31
"Come with Us" – 2:42
"Mountain of Needles" – 2:39
"Pitch to Voltage" – 2:38
"Two Against Three" – 1:55
"Vocal Outtakes" – 0:36
"New Feet" – 2:26
"Defiant" – 3:41
"Number 8 Mix" – 3:30
"Solo Guitar with Tin Foil" – 3:00


== Personnel ==
Adapted from liner notes

David Byrne and Brian Eno – guitars, bass guitars, synthesizers, drums, percussion, found objects (Eno plays the synthesizer solo on "Regiment")
John Cooksey – drums on "Help Me Somebody" and "Qu'ran"
Chris Frantz – drums on "Regiment"
Robert Fripp – Frippertronics on "Regiment"
Michael "Busta Cherry" Jones – bass guitar on "Regiment"
Dennis Keeley – bodhrán on "Mea Culpa"
Bill Laswell – bass guitar on "America Is Waiting"
Mingo Lewis – batá, sticks on "The Jezebel Spirit" and "The Carrier"
Prairie Prince – can, bass drum on "The Jezebel Spirit" and "The Carrier"
José Rossy – congas, agong-gong on "Moonlight in Glory"
Steve Scales – congas, metals on "Help Me Somebody"
David Van Tieghem – drums, percussion (scrap metal, found objects) on "America Is Waiting" and "Regiment"
Tim Wright – click bass on "America Is Waiting"
Rooks on "Help Me Somebody" courtesy of April Potts, recorded at Eglingham Hall


== Release history ==


== Chart performance ==


== References ==


== External links ==
My Life in the Bush of Ghosts (reissue)  at MusicBrainz
Website for the 2006 reissue
Transcriptions, and possible alternatives, of the "lyrics"