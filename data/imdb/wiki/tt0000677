Florence Turner (January 6, 1885 – August 28, 1946) was an American actress who became known as the "Vitagraph Girl" in early silent films.


== Biography ==
Born in New York City, Turner was pushed into appearing on the stage at age three by her ambitious mother. Turner became a regular performer in a variety of productions.
In 1906 she joined the fledgling motion picture business, signing with the pioneering Vitagraph Studios and making her film debut in How to Cure a Cold (June 8, 1907).At the time there were no stars per se, unless an already famous stage star made a movie. Performers were not even mentioned by name. Long, drawn out screen credits were not thought of. There was nothing but the name of the company and the picture. As the content of movies evolved from simple incidents, or situations, into definite stories, some of the heroes and heroines were conceded a vague identity, such as the "Edison Girl", etc.

Though she was known only as the "Vitagraph Girl" in the early motion picture shorts, Turner became the most popular American actress to appear on screen (at that time still dominated by French pictures, especially from the Pathe and Gaumont companies). Her worth to the studio, as its biggest box-office draw, was recognised in 1907 when her pay was upped to $22 a week, as proto-star plus part-time seamstress. It was somewhat less than the male leading players, especially those with stage experience, particularly the super-popular Maurice Costello. In March 1910 she and Florence Lawrence became the first screen actors not already famous in another medium to be publicized by name by their studios to the general public.Later that year, Florence was paired several times with heart throb Wallace Reid, who was on his way to stardom. But with the rise of more stars such as Gene Gauntier and Marin Sais at Kalem Studios, Marion Leonard and Mary Pickford at Biograph Studios, and Florence Lawrence (Biograph, moving to IMP in 1910), Florence Turner was no longer quite as special. By 1913 she was looking for new pastures and left the United States accompanied by longtime friend Laurence Trimble, who directed her in a number of movies. They moved to England, where she and Larry began performing together in London music halls.
Turner sometimes wrote screenplays and directed her own movies, including a number of comedies. She also organized her own production company, Turner Films, for which she made more than thirty shorts. These were shot at the Walton Studios of Cecil Hepworth, west of London.
Turner entertained Allied troops during World War I. She returned to the U.S. after the Armistice, but was not as successful as before. In 1920, she again went to England, where she remained until moving to Hollywood, virtually forgotten, in 1924.By then she was thirty-nine years of age and her starring days were long behind her. She continued to act in supporting roles into the 1930s.
In 1928 she acted in a minor role on Broadway in Sign of the Leopard, which ran for 39 performances. Turner was placed on the payroll at MGM by Louis B. Mayer in the 1930s, but was limited in the assignments offered. She mostly played bit or small parts and worked as an extra.
She later moved to the Motion Picture Country House, a retirement community for the industry in Woodland Hills, California.
After appearing in more than 160 motion pictures, Florence Turner died at 61 in Woodland Hills. She was cremated at a mortuary in Hollywood and, at her request, there was no funeral service. She was buried at Chapel of the Pines Crematory.


== Film appearances ==


== Other film credits ==
Through the Valley of Shadows (1914),  Scenario
A Welsh Singer (1915), Producer
As Ye Repent (1915), Story
Caste (1915), Producer
Far from the Madding Crowd (1915), Producer
The Great Adventure (1915), Producer
Grim Justice (1916), Producer
Sally in Our Alley (1916), Producer


== References ==

Morton, David. “The Vitagraph Girl” or “The Girl From Sheepshead Bay”?: Florence Turner Constructed as an Everywoman Matinee Idol," Senses of Cinema, 84 (Sept 2017), http://sensesofcinema.com/2017/feature-articles/the-vitagraph-girl/


== External links ==
Florence Turner on IMDb
Florence Turner at Women Film Pioneers Project
AFI Catalog Silent Films entry for Florence Turner
Literature on Florence Turner
1915 portrait, during the production of Far from the Madding Crowd