William Mark Price (born February 15, 1964) is an American former basketball player and coach. He was most recently the head coach of the Charlotte 49ers. As a player, he played for 12 seasons in the National Basketball Association (NBA), from 1986 to 1998. Spending the majority of his career with the Cleveland Cavaliers, his last three years consisted of one season each with the Washington Bullets, Golden State Warriors, and Orlando Magic.


== College career ==
Standing at 6 feet (183 cm) tall, Price played college basketball at Georgia Tech. During his time playing on the Georgia Tech Yellow Jackets men's basketball team, he was a two-time All American and four-time All ACC basketball player who helped lead the Yellow Jackets to an ACC Championship his junior year by defeating North Carolina in the ACC Tournament championship game. He was named the ACC Player of the Year for the 1984–85 season and his jersey was retired. He was inducted into the school's Hall of Fame in 1991 and into the Georgia Sports Hall of Fame in 2005. Price graduated in four years with a degree in Industrial Management.


=== College records ===
All-time Georgia Tech leader in 3-point field goal percentage (.440, 1983–86)
All-time Georgia Tech leader in steals (240, 1983–86)
All-time Georgia Tech leader in consecutive games started (126, 1983–86)
All-time Georgia Tech leader in minutes played (4,604, 1983–86)


== Professional career ==
A point guard, he mystified critics who said he was too slow, too small and too deliberate for a high-level game. Selected first in the second round (25th overall) by the Dallas Mavericks in the 1986 NBA draft, he was acquired by the Cleveland Cavaliers in a draft day trade that helped turn the team into an Eastern Conference powerhouse.


=== Cleveland Cavaliers ===
Price was known as one of the league's most consistent shooters. He finished his career with a 90.4% free throw shooting percentage and a 40% three-point field goal shooting percentage. During the 1988–89 season, Price became the second player, after Larry Bird, to join the NBA's 50–40–90 club for those who shot at least 40% from three-point range, at least 50% from the field and at least 90% from the free throw line in a single season, and is still one of only eight players to have ever done this while also achieving the NBA league minimum number of makes in each category. Price ranked consistently among the assist leaders (as of March 11, 2015, LeBron James surpassed Price's Cavs record of 4,206 assists, taking over 1st place), twice won the Three Point Contest (in 1993 and 1994), and was a four-time All-Star. Price was named to the All-NBA First Team after the 1992–93 season. Price was second in franchise steals with 734, a Cavaliers record that stood until December 9, 2008 when LeBron James surpassed him.Another one of Price's distinguishing traits on the court was his pioneering of the splitting of the double team. As former teammate Steve Kerr explains, "Mark really revolutionized the way that people attack the screen and roll. To me, he was the first guy in the NBA who really split the screen and roll. A lot of teams started blitzing the pick and roll and jumping two guys at it to take the ball out of the hands of the point guard. He’d duck right between them and shoot that little runner in the lane. Nobody was doing that at that time. You watch an NBA game now and almost everybody does that. Mark was a pioneer in that regard."


=== Later career ===
Price was plagued by injuries late in his career, a factor in his trade to the Washington Bullets prior to the 1995–96 season. He played one season for Washington before moving on to the Golden State Warriors with whom he spent the 1996–97 season. On October 28, 1997, Price was traded to the Orlando Magic for David Vaughn III and Brian Shaw. He spent one season with the Magic before being waived on June 30, 1998, effectively ending his career.


== National team career ==
During his career Price represented the United States national team. He played for them in the 1983 Pan American Games where the team won gold medals, and also represented the national team in the 1994 FIBA World Championship, where they were known as Dream Team II, and won gold medals.


== Legacy ==
Not long after retirement, Price's number, 25, was retired by the Cleveland Cavaliers. He is a member of the Georgia, Ohio, and Oklahoma Sports Halls of Fame.
The city of Enid, Oklahoma, renamed the basketball arena Mark Price Arena, as a tribute to the NBA player's accomplishments, since he was one of the best basketball athletes in Enid High School history.


== Personal life ==
His younger brother Brent Price played ten seasons in the NBA. His daughter Caroline had a short stint in professional tennis after playing for the North Carolina Tar Heels. His son Josh plays college basketball for the Liberty Flames.


== Coaching career ==
Mark Price began his coaching career during the 1998–99 basketball season as a community coach under head coach and friend Joe Marelle at Duluth High School for the varsity boys team. After Marelle discovered he had non-Hodgkin's lymphoma, Price became a primary factor in the team's return trip to the final four of the class 5A GHSA state tournament. It was the first time Duluth High School returned to this point in the state tournament in 16 years. Price then went on to be an assistant coach to Bobby Cremins at Georgia Tech during the 1999–2000 season.After Cremins retired from coaching at Georgia Tech, Price then went on the following year to be the head coach at Whitefield Academy in Atlanta for the 2000–01 season leading the team to a 27-5 record and the final eight teams of the state Class A tournament, a 20 win improvement over the prior season and 27 win improvement two seasons before Price arrived. NBA player Josh Smith also played at Whitefield Academy the same season Price was coach.In 2002, Price won the Coach Wooden "Keys to Life" Award.In 2003, Price was a consultant for the NBA's Denver Nuggets. He then became an NBA television analyst and color commentator for both the Cleveland Cavaliers and the Atlanta Hawks.
In March 2006, Price was named the inaugural head coach of the Australian NBL's South Dragons, a new franchise for the 2006–07 season.Price was the shooting consultant for the Memphis Grizzlies for the 2007–08 season and named the shooting coach for the Atlanta Hawks for the 2008–09 and 2009–10 seasons. Price helped to improve the Hawks offensive output in their first return to the Eastern Conference Semi-Finals in nearly 10 years during the 2009 NBA Playoffs.Price is credited with helping Boston Celtics point guard Rajon Rondo improve his jump shot. Rondo's scoring was a key factor in the Celtics reaching the 2010 NBA Finals, where they pushed the Los Angeles Lakers to a full seven game series. For the 2010–2011 season, Price joined the Golden State Warriors as an assistant coach with the primary task of improving the Warriors shooting and free throw percentages.In December 2011, Price was hired as a player development coach for the Orlando Magic. In July 2012, Price served as the head coach of the Orlando Magic's Summer League team.On July 1, 2013, Price was hired as an assistant coach by the Charlotte Bobcats, joining the staff of head coach Steve Clifford and associate head coach Patrick Ewing for the 2013–14 season.On March 25, 2015 Price was introduced as the head coach of the Charlotte 49ers. He replaced Coach Alan Major, who parted ways with Charlotte after two medical leaves during the past season. On December 14, 2017, it was announced that Mark Price was relieved of his duties as head coach of the Charlotte 49ers basketball program.


== NBA career statistics ==


=== Regular season ===


=== Playoffs ===


== Head coaching record ==


== See also ==
List of National Basketball Association players with most assists in a game


== References ==


== External links ==

Career statistics and player information from NBA.com or Basketball-Reference.com