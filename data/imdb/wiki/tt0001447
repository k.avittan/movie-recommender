"Didn't We Almost Have It All?" is the twenty-fifth episode and the season finale of the third season of the American television medical drama Grey's Anatomy, and is the 61st episode overall. The episode runs for 53:05 minutes, making it the longest episode of the series, excluding 2-part episodes. The episode opened to polarized reviews from critics; however, the performances of Washington and Oh received high critical acclaim.
The episode marked Isaiah Washington's (Dr. Preston Burke) final appearance to the series until Season 10, Episode 22 entitled "We Are Never Ever Getting Back Together". It was Kate Walsh's (Dr. Addison Montgomery) last episode with regular billing who became a main character in Grey's Anatomy's spin-off Private Practice and Chyler Leigh's (Dr. Lexie Grey) last appearance with recurring billing as she would be upgraded to a series regular in season 4.


== Plot ==
The episode opens to a voice-over narrative from Richard Webber (James Pickens, Jr.). 
Addison gets to the ER with Joe (Steven W. Bailey) and Walter's (Jack J. Yang) surrogate from the bar, the previous episode. Burke starts treating her and decides that the twins must be born. Derek (Patrick Dempsey), Mark (Eric Dane) and Bailey (Chandra Wilson) start treating the newly-found fourth climber. The other climbers eventually talk to the police. Cristina (Sandra Oh) attempts to write her vows. Callie (Sara Ramirez) writes them on Cristina's hand. At work, Cristina discovers that the wedding has given her the day off. Jeff Pope (Jason London) arrives and hugs Rebecca (Elizabeth Reaser). Alex (Justin Chambers) asks Jeff why he didn't search for his wife before. Rebecca wants Alex, but he tells her to stay with her 'decent guy'. 
Derek tells Meredith (Ellen Pompeo) that he met a girl in the bar. Meredith seems confused. Meanwhile, Adele (Loretta Devine) has a miscarriage, revealing that Richard was the father. Mama Burke (Diahann Carroll) forces Cristina to remove her eyebrows; she panics and convinces Bailey to let her scrub in. Bailey agrees, having found out that Callie is the new chief resident. Derek removes the axe from the fourth climber while Burke treats the new babies. Callie and George (T. R. Knight) agree to have children, but Izzie (Katherine Heigl) tells George she's in love with him. The interns receive their test results. George hides the fact that he didn't pass. Derek feels Meredith is taking him for granted. She evades responding and goes to the chapel. 
Mark, Addison and Burke are rejected as Chief of Surgery candidates. At the chapel, Burke finds out Derek is not the new Chief, though Derek doesn't explain. Addison tells Alex to fight for Rebecca. Outside SGH, Bailey and George talk. She thinks that she failed him, but he says: "I failed you". Alex decides to go after Rebecca. Callie tells Izzie she and George is planning to have a baby; Izzie is stunned. Cristina realizes that she washed her vows off when she scrubbed in, and freaks out. Meredith convinces her to go on. Burke sees the delay and calls off the wedding, feeling he has forced her to change. He walks away. Meredith tells everybody that it is over. She looks at Derek and leaves the chapel. 
Cristina arrives at Burke's apartment to discover that he has gone. Cristina starts shaking uncontrollably and crying. To calm her down, Meredith hugs her and cuts her out of her wedding dress. George runs into the new interns, and meets Lexie Grey, Meredith's paternal half-sister. Richard tells Derek he is the new Chief, but Derek suggests that he start over. We see Richard looking over the hospital.


== Production ==

After an off-screen incident, in which Isaiah Washington aimed a homophobic slur at gay actor T. R. Knight, ABC chose not to renew Washington's contract, making "Didn't We Almost Have It All?" his final appearance to the series. It was also the last episode to feature Chyler Leigh as a guest star, since she was promoted to star billing in the season 4 premiere and Kate Walsh's last regular episode as she would only make guest appearances in the future, due to her being a main character on the Grey's Anatomy spin-off Private Practice.
The wedding scenes were filmed at the First Baptist Church of Los Angeles. Kenneth Pool designed the Amsale wedding dress worn by Cristina.


=== Music ===
"Roboxula" - The Jealous Girlfriends
"Falling Or Flying" - Grace Potter
"Hold You In My Arms" - Ray LaMontagne
"Closer" - Coburn
"Within You" - Ray LaMontagne
"Eulogy" - The Hereafter
"Explosions" - The Mary Onettes
"Keep Breathing" - Ingrid Michaelson


== Reception ==
Maureen Ryan from the Chicago Tribune wrote that the episode was "as overstuffed as a clown car at the circus." She added: "That finale was just too much for me. Too much padding, too much repetition, too many characters forced into ridiculous situations", citing as example Adele Webber's pregnancy at 52. She criticized Shepherd for being arrogant and condescending. Salon.com's TV critic Heather Havrilesky found that Shepherd's threat to break up with Grey came in a bad timing after what she went through during the season and wondered why he did not so when she questioned their relationship. She then wrote: "Grey’s Anatomy has countless charms — snappy yet moving dialogue, nice-looking humans, fast-moving story lines. But lately one of those strengths — those hurtling plotlines — seems to be killing creator Shonda Rhimes’ golden goose." Havrilesky cited Sandra Oh as one of the episode's highlight deeming her "fantastic" and "believable", and named the moment when Yang goes home to find Burke and breaks down the best scene of the finale.Writing for Entertainment Weekly, Gregory Kirschling thought that "as a finale, maybe the show lacked a defining happy, warm-gooseflesh moment" but that in comparison to normal episodes, this one was "well above average". To him, Shepherd's speech to Grey before the start of the wedding was the highlight of the episode, explaining he likes to see "these two flailing hot people — he soft, she hard — trade well-turned TV wooing dialogue. It still works; it's still romantic." However, he considered what followed, Grey's non-response, not romantic and though he likes their goo-goo courtship scenes together, he wrote it was time for them to be happy together. Kirschling criticized the choice of Torres as chief resident instead of Bailey. He praised Oh's performance in the episode, especially during Burke's dumping speech calling it "so affecting" and in her last scene, which he described as "an amazing breakdown". Commenting on the finale, Andy Dehnart of MSNBC wrote that the stories were completed "in mostly unsatisfying, occasionally depressing, and sometimes illogical ways that will leave the show decidedly changed."


== References ==


== External links ==
"Didn't We Almost Have It All" at TV.com
"Didn't We Almost Have It All" on IMDb