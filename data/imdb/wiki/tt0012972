Breaking Home Ties is a painting by American illustrator Norman Rockwell, created for the September 25, 1954, cover of The Saturday Evening Post. The picture represents a father and son waiting for a train that will take the young man to the state university. The painting, considered by experts to be one of Rockwell's masterworks, is also one of the most widely reproduced, and was voted the second-most popular image in Post history.


== Description ==
The details of the picture, as with most of Rockwell's works, tell a story, in this case a story of endings and beginnings, as a boy from New Mexico leaves home for the first time. The young man, his father and a dog, sit on the running board of the family's stakesided farm truck. The ticket protruding from the son's pocket, and the single rail visible at the lower corner of the painting, by which the trio sit, suggest that they are at a whistle stop waiting for the train.
The son's books are stacked on a new suitcase bearing a "State U" pennant. With his tie and socks perfectly matched, wearing pressed white trousers and matching jacket, he is ready for his new life in college. The young man's shoes are shined to a polished gleam, as, hands folded, and with the family dog resting his head in his lap, his gaze focuses eagerly toward the horizon, and on the next chapter in his life.
In contrast, the father sits slumped with both his and his son's hats clutched in his hand, as if reluctant to let him go. The direction of his gaze is opposite to his son's. The tag from a Bull Durham tobacco satchel dangles, near at hand, from his shirt pocket. There is a red flag and a lantern at the ready, near his right hand, atop a well-used trunk. With the son's luggage unloaded and waiting next to them, there is nothing left for him to do but signal the train to stop, and his pose suggests that he is looking up the track, dreading the imminent arrival of the train that will carry his son away.
Though the two figures are not looking at each other, their legs are touching and emphasize the very strong sense of family ties in this iconic 1954 painting.


== History ==
The painting was shown in many locations over many years, including Washington, D.C.'s Corcoran Gallery of Art in 1955, and in Moscow and Cairo in 1964. It was also the inspiration for an eponymous 1987 TV movie, featuring Jason Robards and Doug McKeon as the father and son.In 2003, the painting was put on display at the Norman Rockwell Museum, which was its first public showing in over 25 years. Despite some experts noticing discrepancies between the painting on display and the published Post image, the painting on display was believed to be the original.


=== Finding the original ===
In 1962, the painting had been purchased for $900 by cartoonist Don Trachte, a friend of Rockwell's, and was in his ownership until his death in May 2005. In March 2006, two of his sons found a hidden area in Trachte's house, which contained several paintings, including Breaking Home Ties. Further investigation found that Trachte had created copies of several paintings he owned, and hidden the originals, unknown to his family.  Thus, the Breaking Home Ties that had been put on display at the Norman Rockwell Museum in 2003, was actually a replica. The museum later placed the original on display, along with the replica.


=== 2006 sale ===
On November 29, 2006, Sotheby's sold the original painting at auction for $15.4 million, which at the time was a record sum paid for a Rockwell work. The buyer or buyers chose to remain anonymous.


== Cultural reference ==
The painting was the inspiration for a 1987 television movie, Breaking Home Ties, on a coming of age theme.


== References ==


== Further reading ==
Sanden, John Howard (2006). "A Puzzling Mystery at The Norman Rockwell Museum". The World of Portrait Painting. What was a world-class museum doing showing an inferior replica, proclaiming it to be the original?
"An Iconic Norman Rockwell – Not Known to Have Been Missing – Found Again". Norman Rockwell Museum (Press release). April 6, 2006. Archived from the original on September 29, 2006 – via archive.org. Through an improbable convergence of circumstances, an iconic Norman Rockwell painting, not known to have been missing, has been found.
Laughter, Shayne (May 8, 2017). "Breaking Home Ties — Prelude To A Masterpiece". indianapublicmedia.org. Retrieved May 12, 2017. For 40 years, the Indiana University School of Journalism displayed a charcoal sketch that the general public was never supposed to see.


== External links ==
Detailed record of the painting via the Norman Rockwell Museum website