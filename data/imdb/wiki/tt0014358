A pilgrim (from the Latin peregrinus) is a traveler (literally one who has come from afar) who is on a journey to a holy place. Typically, this is a physical journey (often on foot) to some place of special significance to the adherent of a particular religious belief system. In the spiritual literature of Christianity, the concept of pilgrim and pilgrimage may refer to the experience of life in the world (considered as a period of exile) or to the inner path of the spiritual aspirant from a state of wretchedness to a state of beatitude.


== History ==
Pilgrims and the making of pilgrimages are common in many religions, including the faiths of ancient Egypt, Persia in the Mithraic period, India, China, and Japan. The Greek and Roman customs of consulting the gods at local oracles, such as those at Dodona or Delphi, both in Greece, are widely known. In Greece, pilgrimages could either be personal or state-sponsored.In the early period of Hebrew history, pilgrims traveled to Shiloh, Dan, Bethel, and eventually Jerusalem (see also Three Pilgrimage Festivals, a practice followed by other Abrahamic religions). While many pilgrims travel toward a specific location, a physical destination is not always a necessity. One group of pilgrims in early Celtic Christianity were the Peregrinari Pro Christ, (Pilgrims for Christ), or "white martyrs", who left their homes to wander in the world. This sort of pilgrimage was an ascetic religious practice, as the pilgrim left the security of home and the clan for an unknown destination, trusting completely in Divine Providence. These travels often resulted in the founding of new abbeys and the spread of Christianity among the pagan population in Britain and in continental Europe.


== Modern era ==
Many religions still espouse pilgrimage as a spiritual activity. The great Islamic pilgrimage to Mecca (now in Saudi Arabia), is an obligatory duty at least once for every Muslim who is able to make the journey. Other Islamic devotional pilgrimages, particularly to the tombs of Shia Imams or Sufi saints, are also popular across the Islamic world. As in the Middle Ages, modern Christian pilgrims may choose to visit Rome, where according to the New Testament the church was established by St. Peter, sites in the 'Holy Land' connected with the life of Christ (such as Bethlehem, Jerusalem and the Sea of Galilee) or places associated with saints, visions and miracles such as Lourdes, Santiago of Compostela, Canterbury and Fatima. 

Places of pilgrimage in the Buddhist world include those associated with the life of the historical Buddha: his supposed birthplace and childhood home (Lumbini and Kapilavastu in Nepal) and place of enlightenment (Bodh Gaya in northern India), other places he is believed to have visited and the place of his death (or Parinirvana), Kushinagar, India. Others include the many temples and monasteries with relics of the Buddha or Buddhist saints such as the Temple of the Tooth in Sri Lanka and the numerous sites associated with teachers and patriarchs of the various traditions. Hindu pilgrimage destinations may be holy cities (Varanasi, Badrinath); rivers (the Ganges, the Yamuna); mountains (several Himalayan peaks are sacred to both Hindus and Buddhists); caves (such as the Batu Caves near Kuala Lumpur, Malaysia); temples; festivals, such as the peripatetic Kumbh Mela, in 2001 the biggest public gathering in history; or the tombs and dwelling places of saints (Alandi, Shirdi).
Beginning in 1894, Christian ministers under the direction of Charles Taze Russell were appointed to travel to and work with local Bible Students congregations for a few days at a time; within a few years appointments were extended internationally, formally designated as "pilgrims", and scheduled for twice-yearly, week-long visits at each local congregation. International Bible Students Association (IBSA) pilgrims were excellent speakers, and their local talks were typically well-publicized and well-attended. Prominent Bible Students A. H. Macmillan and J. F. Rutherford were both appointed pilgrims before they joined the board of directors of the Watch Tower Bible and Tract Society of Pennsylvania; the IBSA later adopted the name Jehovah's Witnesses and renamed pilgrims as traveling overseers.

A modern phenomenon is the cultural pilgrimage which, while involving a personal journey, is secular in nature. Destinations for such pilgrims can include historic sites of national or cultural importance, and can be defined as places "of cultural significance: an artist's home, the location of a pivotal event or an iconic destination". An example might be a baseball fan visiting Cooperstown, New York. Destinations for cultural pilgrims include Auschwitz concentration camp, Gettysburg Battlefield or the Ernest Hemingway House. Cultural pilgrims may also travel on religious pilgrimage routes, such as the Way of St. James, with the perspective of making it a historic or architectural tour rather than – or as well as – a religious experience. Under communist regimes, devout secular pilgrims visited locations such as the Mausoleum of Lenin, the Mausoleum of Mao Zedong and the Birthplace of Karl Marx. Such visits were sometimes state-sponsored. Sites such as these continue to attract visitors. The distinction between religious, cultural or political pilgrimage and tourism is not necessarily always clear or rigid. Pilgrimage could also refer symbolically to journeys, largely on foot, to places where the concerned person(s) expect(s) to find spiritual and/or personal salvation. In the words of adventurer-author Jon Krakauer in his book Into The Wild, Christopher McCandless was 'a pilgrim perhaps' to Alaska in search of spiritual bliss.


== Notable pilgrims ==

Many national and international leaders have gone on pilgrimages for both personal and political reasons.

Guru Nanak
Pope Benedict XVI
Pope Francis
Bridget of Sweden
Columba
Rangjung Rigpe Dorje
Egeria
El-Hajj Malik el-Shabazz
Ruslan Gelayev
Godric of Finchale
Tenzin Gyatso, 14th Dalai Lama
Ignatius of Loyola
James, son of Zebedee
Christopher Jones (Mayflower Captain)
Judah HaLevi
Mustapha Kartali
Margery Kempe
Junichiro Koizumi
Mansa Musa
Peace Pilgrim
Pope John Paul II
Bill Porter (author)
Thomas the Apostle


== In culture ==
Some prominent literary characters who were pilgrims include:

In the epic poem Divine Comedy, Dante Alighieri portrays himself as a pilgrim traveling through the afterlife realms of Hell, Purgatory, and Paradise.
John Bunyan depicted multiple pilgrims (e.g., Christian – the protagonist, Faithful, Talkative, Christiana, Mercy, Old Honest, Mr. Fearing, Mr. Feeble-Mind, Mr. Ready-to-Halt, and Mr. Valiant) as well as false pilgrims (e.g., Formalist, Hypocrisy, and Mr. By-Ends) in his Christian allegory, The Pilgrim's Progress (1678)
Wilfred of Ivanhoe, a palmer (medieval Christian from Europe who makes a pilgrimage to Jerusalem) and the titular character of Sir Walter Scott's book Ivanhoe
A palmer plays a significant role representing Reason in Book II of Edmund Spenser's epic poem The Faerie Queene


== See also ==
Pilgrimage
Hajji


== References ==


== Literature ==
Documentation, of a modern pilgrimage to Rome. Kerschbaum & Gattinger, Via Francigena. DVD. Vienna: Verlag EUROVIA, 2005. ISBN 3-200-00500-9


== External links ==

 Media related to Pilgrims at Wikimedia Commons
Medieval Pilgrims' Clothing Illustrations of 13th–16th century pilgrims, and links to photos of 16th century clothing made for pilgrimage