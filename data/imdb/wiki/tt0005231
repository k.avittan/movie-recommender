The Hound of the Baskervilles is the third of the four crime novels written by Sir Arthur Conan Doyle featuring the detective Sherlock Holmes. Originally serialised in The Strand Magazine from August 1901 to April 1902, it is set largely on Dartmoor in Devon in England's West Country and tells the story of an attempted murder inspired by the legend of a fearsome, diabolical hound of supernatural origin. Sherlock Holmes and his companion Dr. Watson investigate the case. This was the first appearance of Holmes since his apparent death in "The Final Problem", and the success of The Hound of the Baskervilles led to the character's eventual revival.One of the most famous stories ever written, in 2003, the book was listed as number 128 of 200 on the BBC's The Big Read poll of the UK's "best-loved novel". In 1999, it was listed as the top Holmes novel, with a perfect rating from Sherlockian scholars of 100.


== Plot ==

Dr James Mortimer calls on Sherlock Holmes in London for advice after his friend Sir Charles Baskerville was found dead in the yew alley of his manor on Dartmoor in Devon. The death was attributed to a heart attack, but according to Mortimer, Sir Charles's face retained an expression of horror, and not far from the corpse the footprints of a gigantic hound were clearly visible. According to an old legend, a curse runs in the Baskerville family since the time of the English Civil War, when a Hugo Baskerville abducted and caused the death of a maiden on the moor, only to be killed in turn by a huge demonic hound. Allegedly the same creature has been haunting the manor ever since, causing the premature death of many Baskerville heirs. Sir Charles believed in the plague of the hound and so does Mortimer, who now fears for the next in line, Sir Henry Baskerville.
Even though he dismisses the curse story as nonsense, Holmes agrees to meet Sir Henry in London as soon as Sir Henry arrives from Canada, where he has been living. He is a young and jovial good-looking fellow, sceptical toward the grim legend and eager to take possession of Baskerville Hall, even though he has just found an anonymous note in the mail warning him to stay away from the moor. When someone shadows Sir Henry while he is walking down a street, however, Holmes asks Watson to go with the young man and Mortimer to Dartmoor, in order to protect Sir Henry and search for any clues about who is menacing his life. 

The trio arrives at Baskerville Hall, an old and imposing manor in the middle of a vast park, managed by a butler and his wife the housekeeper. The estate is surrounded by the moor and borders the Grimpen Mire, where animals and humans can sink to death in quicksand. The news that a convict named Selden, a murderer, has escaped from the nearby Dartmoor Prison and is hiding in the nearby hills adds to the barren landscape and the gloomy atmosphere.
There are inexplicable events during the first night, keeping the guests awake, and only in the daylight can Watson and Sir Henry relax while exploring the neighborhood and meeting the scattered and idiosyncratic residents of the district. Watson keeps on searching for any lead to the identity of whoever is threatening Sir Henry's life, and faithfully sends the details of his investigation to Holmes. Among the residents, the Stapletons, brother and sister, stand out: Jack is overfriendly and a bit too curious toward the newly arrived, while Beryl, a rare beauty, seems all too weary of the place and attempts to warn Sir Henry of danger.
Distant howls and strange sightings trouble Watson during his long walks on the hills, and his mood gets no better even inside Baskerville Hall. Watson grows suspicious of the butler, Barrymore, who at night appears to be signaling from a window of the house with a candle to someone on the moor. Meanwhile, Sir Henry is drawn to Beryl, who seems to be afraid of her brother's opinion on the matter. To make the puzzle more complex there are Mortimer, maybe too eager to convince Sir Henry that the curse is real; an old and grumpy neighbour, who likes to pry with his telescope into other people's doings; his daughter Laura, who had unclear ties to Sir Charles; and even a bearded man roaming free in the hills and apparently hiding on a tor where ancient tombs have been excavated by Stapleton for an unclear purpose.
Unknown to everyone, even to his friend Watson, Holmes has been hiding in the moor all the time and has solved the mystery. He reveals that the hound is real and belongs to Stapleton, who seduced Laura and convinced her to lure Sir Charles out of his house by night, in order to frighten him with the apparition of the legendary hound. Beryl is indeed Jack's legitimate wife, abused and forced into posing as his sister to seduce Sir Henry and expose him also to the fangs of the hound, since Stapleton is in fact a descendant of the Baskervilles wanting to claim their inheritance. Meanwhile, the hound attempts to kill Sir Henry, but Barrymore had given the former's clothes to Selden, his brother-in-law, who dies in his place.
Unfortunately the collected evidence is not enough for a jury to condemn Stapleton, so Holmes decides to use young Baskerville as a bait to catch the criminal red-handed. Sir Henry will accept an invitation to Stapleton's house and will walk back alone after dark, giving his enemy every chance to unleash the hound on him. Holmes and Watson pretend to leave Dartmoor by train, but instead they hide near Stapleton's house with Inspector Lestrade of Scotland Yard. Despite the dark and a thick fog, Holmes and Watson are able to kill the fearsome beast as soon as it attacks the designated victim, while Stapleton, in his panicked flight from the scene, drowns in the mire.


== Origins and background ==

Sir Arthur Conan Doyle wrote this story shortly after returning to his home Undershaw from South Africa, where he had worked as a volunteer physician at the Langman Field Hospital in Bloemfontein during the Second Boer War. He had not written about Sherlock Holmes in eight years, having killed off the character in the 1893 story "The Final Problem". Although The Hound of the Baskervilles is set before the latter events, two years later Conan Doyle brought Holmes back for good, explaining in "The Adventure of the Empty House" that Holmes had faked his own death.
He was assisted with the Legend and local colour by a 30-year-old Daily Express journalist named Bertram Fletcher Robinson (1870–1907), with whom he explored Dartmoor, and to whom a 1/3 royalty was paid on the serialization, amounting to several hundred pounds.


== Inspiration ==
His ideas came from the legend of Squire Richard Cabell of Brook Hall, in the parish of Buckfastleigh, Devon, which was the fundamental inspiration for the Baskerville tale of a hellish hound and a cursed country squire. Cabell's tomb survives in the village of Buckfastleigh.Cabell lived for hunting, and was what in those days was described as a "monstrously evil man". He gained this reputation, amongst other things, for immorality and having sold his soul to the Devil. There was also a rumour that he had murdered his wife, Elizabeth Fowell, a daughter of Sir Edmund Fowell, 1st Baronet (1593–1674), of Fowelscombe. On 5 July 1677, he died and was buried in the sepulchre. The night of his interment saw a phantom pack of hounds come baying across the moor to howl at his tomb. From that night on, he could be found leading the phantom pack across the moor, usually on the anniversary of his death. If the pack were not out hunting, they could be found ranging around his grave howling and shrieking. To try to lay the soul to rest, the villagers built a large building around the tomb, and to be doubly sure a huge slab was placed.Moreover, Devon's folklore includes tales of a fearsome supernatural dog known as the Yeth hound that Conan Doyle may have heard.
Weller (2002) believes that Baskerville Hall is based on one of three possible houses on or near Dartmoor: Fowelscombe in the parish of Ugborough, the seat of the Fowell Baronets; Hayford Hall, near Buckfastleigh (also owned by John King (d.1861) of Fowelscombe) and Brook Hall, in the parish of Buckfastleigh, about two miles east of Hayford, the actual home of Richard Cabell. It has also been claimed that Baskerville Hall is based on a property in Mid Wales, built in 1839 by one Thomas Mynors Baskerville. The house was formerly named Clyro Court and was renamed Baskerville Hall towards the end of the last century. Arthur Conan Doyle was apparently a family friend who often stayed there and may have been aware of a local legend of the hound of the Baskervilles.Still other tales claim that Conan Doyle was inspired by a holiday in North Norfolk, where the tale of Black Shuck is well known. The pre-Gothic Cromer Hall, where Conan Doyle stayed, also closely resembles Doyle's vivid descriptions of Baskerville Hall.James Lynam Molloy, a friend of Doyle's, and author of "Love's Old Sweet Song", married Florence Baskerville, daughter of Henry Baskerville of Crowsley Park, Oxfordshire. The gates to the park had statues of hell hounds, spears through their mouths. Above the lintel there was another statue of a hell hound.


=== Original manuscript ===
In 1902, Doyle's original manuscript of the book was broken up into individual leaves as part of a promotional campaign by Doyle's American publisher – they were used in window displays by individual booksellers. Out of an estimated 185-190 leaves, only 36 are known still to exist, including all the leaves from Chapter 11, held by the New York Public Library. Other leaves are owned by university libraries and private collectors. A newly rediscovered example was sold at auction in 2012 for US$158,500.


== Technique ==
The novel uses many traditional novelistic techniques which had been largely abandoned by the time of writing, such as letters, diary extracts, interpolated manuscripts, and the like as seen in the works of Henry Fielding and, later, Wilkie Collins. It incorporates five plots: the ostensible 'curse' story, the two red-herring subplots concerning Selden and the other stranger living on the moor, the actual events occurring to Baskerville as narrated by Watson, and the hidden plot to be discovered by Holmes. Doyle wrote that the novel was originally conceived as a straight 'Victorian creeper' (as seen in the works of J. Sheridan Le Fanu), with the idea of introducing Holmes as the deus ex machina only arising later.


== Publication ==
The Hound of the Baskervilles was first serialized in The Strand Magazine in 1901. It was well-suited for this type of publication, as individual chapters end in cliffhangers. It was printed in the form of a novel the following year.


== Adaptations ==
The Hound of the Baskervilles has been adapted for many media.


=== Film and television adaptations ===
Over 20 film and television versions of The Hound of the Baskervilles have been made.


=== Audio ===
Edith Meiser adapted the novel as six episodes of the radio series The Adventures of Sherlock Holmes. The episodes aired in February and March 1932, with Richard Gordon as Sherlock Holmes and Leigh Lovell as Dr. Watson. Another dramatisation of the story aired in November and December 1936, with Gordon as Holmes and Harry West as Watson.The story was also adapted by Meiser as six episodes of The New Adventures of Sherlock Holmes with Basil Rathbone as Holmes and Nigel Bruce as Watson. The episodes aired in January and February 1941.A dramatisation of the novel by Felix Felton aired on the BBC Light Programme in 1958 as part of the 1952–1969 radio series, with Carleton Hobbs as Sherlock Holmes and Norman Shelley as Dr. Watson. A different production of The Hound of the Baskervilles, also adapted by Felton and starring Hobbs and Shelley with a different supporting cast, aired in 1961 on the BBC Home Service.The novel was adapted as an episode of CBS Radio Mystery Theater. The episode, which aired in 1977, starred Kevin McCarthy as Holmes and Lloyd Battista as Watson.The Hound of the Baskervilles has been adapted for radio for the BBC by Bert Coules on two occasions. The first starred Roger Rees as Holmes and Crawford Logan as Watson and was broadcast in 1988 on BBC Radio 4. Following its good reception, Coules proposed further radio adaptations, which eventually led to the 1989–1998 radio series of dramatisations of the entire canon, starring Clive Merrison as Holmes and Michael Williams as Watson. The second adaptation of The Hound of the Baskervilles, featuring this pairing, was broadcast in 1998, and also featured Judi Dench as Mrs Hudson and Donald Sinden as Sir Charles Baskerville.The Hound of the Baskervilles was adapted as three episodes of the American radio series The Classic Adventures of Sherlock Holmes, with John Patrick Lowrie as Holmes and Lawrence Albert as Watson. The episodes first aired in March 2008.In 2011, Big Finish Productions released their adaptation of the book as part of their second series of Holmes dramas. Holmes was played by Nicholas Briggs, and Watson was played by Richard Earl.In 2014, L.A. Theatre Works released their production, starring Seamus Dever as Holmes, Geoffrey Arend as Watson, James Marsters as Sir Henry, Sarah Drew as Beryl Stapleton, Wilson Bethel as Stapleton, Henri Lubatti as Dr Mortimer, Christopher Neame as Sir Charles and Frankland, Moira Quirk as Mrs Hudson & Mrs Barrymore, and Darren Richardson as Barrymore.
In July 2020, Lions Den Theatre released a new adaptation of the novel written and directed by Keith Morrison on the company's YouTube channel. An early version of the play was performed in various locations around Nova Scotia in 2018.


=== Stage ===
In 2007, Peepolykus Theatre Company premiered a new adaptation of The Hound of the Baskervilles at West Yorkshire Playhouse in Leeds. Adapted by John Nicholson and Steven Canny, the production involves only three actors and was praised by critics for its physical comedy. Following a UK tour, it transferred to the Duchess Theatre in London's West End. The Daily Telegraph described it as a 'wonderfully delightful spoof', whilst The Sunday Times praised its 'mad hilarity that will make you feel quite sane'. This adaptation continues to be presented by both amateur and professional companies around the world.Ken Ludwig authored an adaptation entitled Baskerville: A Sherlock Holmes Mystery which premiered as a co-production at Arena Stage (Washington, D.C.) in January 2015 and McCarter Theatre Center in March 2015.


=== Video games ===
The Hound of Baskervilles serves as the primary inspiration for the final case in Dai Gyakuten Saiban: Naruhodō Ryūnosuke no Bōken in which the protagonist teams up with Sherlock Holmes to investigate mysteries based on various entries in the Holmes chronology.Sherlock Holmes and the Hound of the Baskervilles is a casual game by Frogwares. It departs from the original plot by introducing clear supernatural elements. Despite its non-canonical plot, it received good reviews.


== Related works ==
The movie The Life and Death of Colonel Blimp (1941) makes references to The Hound of the Baskervilles.
Mad magazine satirized this novel in issue #16 (October 1954) as "The Hound of the Basketballs", art by Bill Elder.
Disney cartoonist Carl Barks parodied this story with The Hound of the Whiskervilles (1960), starring Uncle Scrooge.
A 1965 issue of Walt Disney's Comics and Stories (comic book) featured The Hound of Basketville, starring Mickey Mouse, Goofy, Gladstone Gander, and Pluto, as Sherlock Mouse, Doctor Goofy, Sir Gladstone Basketville, and the hound.
Stapleton reappears in Richard L. Boyer's version of The Giant Rat of Sumatra (1976). It turns out that he did not die, as Holmes and Watson assumed, but had escaped by another route, committing further crimes and vowing vengeance on Sherlock Holmes.
William of Baskerville, protagonist of Umberto Eco's novel The Name of the Rose (1980), is a Franciscan friar and a sleuth, inspired by Sherlock Holmes and perhaps William of Occam and other real and fictional characters.
The hound of the Baskervilles is a character in Kouta Hirano's supernatural manga series Hellsing (1997–2008).
Spike Milligan satirised the novel in his book, The Hound of the Baskervilles According to Spike Milligan (1997), combining elements of the original novel with the Basil Rathbone serials.
The Moor (1998), a novel in Laurie R. King's series about Sherlock Holmes and Mary Russell, uses the setting and various plot elements, with Holmes returning to Dartmoor on a later case.
Pierre Bayard's book Sherlock Holmes Was Wrong (2008) re-opens the case and, by careful re-examination of all the clues, clears the hound of all wrongdoing and argues that the actual murderer got away with the crime completely unsuspected by Holmes, countless readers of the book over the past century—and even, in a sense, the author himself.


== Critical reception ==
On 5 November 2019, the BBC News listed The Hound of the Baskervilles on its list of the 100 most influential novels.


== See also ==

Baskerville effect
Edinburgh Phrenological Society
Le Monde's 100 Books of the Century
Princetown#Geography


== References ==


== External links ==
Read The Hound of the Baskervilles at Sir Arthur Conan Doyle–His Life, All His Works and More

 The Hound of the Baskervilles at Project Gutenberg
The Hound of the Baskervilles (Part I) at BFRonline.biz.
The Hound of the Baskervilles (Part II) at BFRonline.biz.
The Hound of the Baskervilles (Conclusion) at BFRonline.biz.
 The Hound of the Baskervilles public domain audiobook at LibriVox