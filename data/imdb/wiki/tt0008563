Seven Swans is a folk rock music album by Sufjan Stevens, released on March 16, 2004 on Sounds Familyre Records. It is Stevens' fourth studio album and features songs about Christian spiritual themes, figures such as Abraham, and Christ's Transfiguration. The songs are primarily "lush acoustic compositions" with Stevens' banjo. It was recorded and produced by Daniel C. Smith, Sufjan's close friend. The album was released on compact disc by Sounds Familyre Records and vinyl LP; the vinyl was released by Burnt Toast Vinyl.


== Critical reception ==
Seven Swans has a score of 87 out of 100 based on 23 reviews at Metacritic, indicating a "universal acclaim" rating from the site. The Guardian called it "a record of remarkable delicacy" and Spin described the album as sounding "like Elliott Smith after ten years of Sunday school". In December 2004, American webzine Somewhere Cold ranked Seven Swans No. 3 on their 2004 Somewhere Cold Awards Hall of Fame list.


== Thematic elements ==
Many of the songs on Seven Swans tell stories directly from the Bible. "Abraham" references the bible story wherein Abraham is tested by God and told to sacrifice his only son Isaac. Abraham, at the last moment and with knife in hand, is stopped by an angel and instead sacrifices a ram to God. The final song, "Transfiguration", is a "bittersweet note of Jesus' requisite suffering". "A Good Man Is Hard To Find" is based on a first-person account by The Misfit character from Flannery O'Connor short story of the same name. The lyrics of the title song, "Seven Swans," loosely allude to the events of the Book of Revelation.


== Track listing ==
All tracks written by Sufjan Stevens.

"All the Trees of the Field Will Clap Their Hands" – 4:14
"The Dress Looks Nice on You" – 2:32
"In the Devil's Territory" – 4:57
"To Be Alone with You" – 2:44
"Abraham" – 2:33
"Sister" – 6:00
"Size Too Small" – 3:04
"We Won't Need Legs to Stand" – 2:12
"A Good Man Is Hard to Find" – 3:16
"He Woke Me Up Again" – 2:43
"Seven Swans" – 6:33
"The Transfiguration" – 5:18Bonus 7"

"I Went Dancing with My Sister"
"Waste of What Your Kids Won't Have"


== Personnel ==
Sufjan Stevens – composition and performance
Laura Normandin – calligraphy
Rafter Roberts – mastering
Andrew Smith – drums
Daniel Smith – vocals, bass guitar, and production at his home studio and the New Jerusalem Rec Room in Clarksboro, New Jersey
David Smith – drums
Megan Smith – vocals
Marzuki Stevens – artwork
Elin Smith - vocals


== Seven Swans Reimagined ==
In 2011 a cover album reimagining the songs from Seven Swans was released. Many of the artists featured on this album had worked with Stevens in the past.


=== Track Listing ===
All lyrics are written by Sufjan Stevens.


== "The Dress Looks Nice on You" ==
A 7" limited edition single of "The Dress Looks Nice on You" was released by Rough Trade in support of the album on March 8, 2004. The single features the song "Borderline" as a B-side.

"The Dress Looks Nice on You"  – 2:32
"Borderline"  – 3:25


== References ==


== External links ==
Rough Trade official website