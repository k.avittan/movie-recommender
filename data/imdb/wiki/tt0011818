A Hidden Life is a 2019 German-American epic historical drama film written and directed by Terrence Malick, starring August Diehl, Valerie Pachner, and Matthias Schoenaerts with both Michael Nyqvist and Bruno Ganz in their final performances. The film depicts the life of Franz Jägerstätter, an Austrian farmer and devout Catholic who refused to fight for the Nazis in World War II. The film's title was taken from George Eliot's book Middlemarch.
The film had its world premiere at the Cannes Film Festival in May 2019 and was theatrically released in the United States on December 13, 2019. It was the final feature film to be released under the Fox Searchlight Pictures name before Walt Disney Studios changed the company's name to Searchlight Pictures on January 17, 2020.


== Plot ==
Austria, 1939. Peasant farmer Franz Jägerstätter (August Diehl), born and brought up in the small village of St. Radegund, is working his land when war breaks out. Married to Franziska (Fani) (Valerie Pachner), the couple are important members of the tight-knit rural community. They live a simple life with the passing years marked by the arrival of the couple's three girls. Franz is called up for basic training in the German army and is away from his beloved wife and children for months. Eventually, when France surrenders and it seems the war might end soon, he is sent back from training.
With his mother and sister-in-law Resie (Maria Simon), he and his wife farm the land and raise their children amid the mountains and valleys of upper Austria. Many scenes depict cutting and gathering hay, as well as the broad Inn River. As the war goes on, Jägerstätter and the other able-bodied men in the village are called up to fight. Their first requirement is to swear an oath of allegiance to Adolf Hitler and the Third Reich. Despite pressure from the Mayor and his farm neighbors, who increasingly ostracize him and his family, and from the Bishop of Salzburg, Jägerstätter refuses. Wrestling with the knowledge that his decision will mean arrest and even death, Jägerstätter finds strength in Fani's love and support. Jägerstätter is taken to prison, first in Enns, then in Berlin and waits months for his trial. During his time in prison, he and Fani write letters to each other and give each other strength. Fani and their daughters are victims of growing hostility in the village over her husband's decision not to fight. Fani is eventually able to visit her husband in Berlin.
After months of brutal incarceration, his case goes to trial. He is found guilty and sentenced to death. Despite many opportunities to sign the oath of allegiance, and the promise of non-combatant work, Jägerstätter continues to stand up for his beliefs and is executed by the Third Reich in August 1943, while his wife and three daughters survive.


== Cast ==


== Production ==


=== Development ===
On June 23, 2016, reports emerged that the film, then titled Radegund, would depict the life of Austria’s Franz Jägerstätter, a conscientious objector during World War II who was put to death at the age of 36 for undermining military actions, and was later declared a martyr and beatified by the Catholic Church. It was announced that August Diehl was set to play Jägerstätter and Valerie Pachner to play his wife, Franziska Jägerstätter. Jörg Widmer was appointed as the director of photography, having worked in all of Malick's films since The New World (2005) as a camera operator.


=== Writing ===
Malick said A Hidden Life will have a more structured narrative than his previous works: "Lately – I keep insisting, only very lately – have I been working without a script and I've lately repented the idea. The last picture we shot, and we're now cutting, went back to a script that was very well ordered."


=== Filming ===

The film began production in Studio Babelsberg in Potsdam, Germany in summer 2016. From 11 July through 19 August 2016 the production shot on location in South Tyrol. Locations there were the church of St. Valentin in Seis am Schlern, the valley of Gsies, the village of Rodeneck, the mills in Terenten, the meadows of Albions in Lajen, the Seiser Alm, the Taufers Castle, the Fane Alm in Mühlbach, the Puez-Geisler Nature Park, the renaissance Velthurns Castle in the village of Feldthurns, the Franzensfeste Fortress, the gardens of the bishop's Hofburg in Brixen and the Neustift cloister.In August 2016 reports emerged that some of the film's scenes were shot in the small Italian mountain village of Sappada.


=== Post-production ===
Actor Franz Rogowski said in a March 2019 interview that no one knew how the film would turn out or when it would be released, considering that it had been in post-production for more than two years at that point. Rogowski added that Malick is "a director who creates spaces rather than produces scenes; his editing style is like that."


== Music ==
The film's original score was composed by James Newton Howard and features violinist James Ehnes, who had also performed with the composer on his violin concerto released in 2018. It was released by Sony Classical Records on December 6, 2019. Speaking about the score, Newton Howard stated that "It is a spiritual sounding score... Terry often spoke about the suffering inherent in love, and you feel yearning, suffering and love in that piece" The score features 40 minutes of original score mixed with selected classical works by Bach, Handel, Dvorak, Gorecki, Pärt and many others. It was recorded at Abbey Road Studios in London in one day in June 2018 with a 40-piece string section conducted by Pete Anthony with Shawn Murphy as score mixer.
All music is composed by James Newton Howard, except where noted.


== Release ==
A Hidden Life premiered in competition at the 72nd Cannes Film Festival on May 19, 2019. The following day, the film was acquired by Fox Searchlight Pictures for $12–14 million. The film screened at the Vatican Film Library on December 4, 2019, with Malick making a rare public appearance to introduce the film. It was released in limited release in the United States on December 13, 2019 followed by a wide release in January.


== Reception ==
On review aggregation website Rotten Tomatoes, the film holds an approval rating of 81% based on 227 reviews, with an average rating of 7.39/10. The site's critical consensus reads, "Ambitious and visually absorbing, A Hidden Life may prove inscrutable to non-devotees—but for viewers on Malick's wavelength, it should only further confirm his genius." On Metacritic, the film has a weighted average score of 78 out of 100, based on 43 critics, indicating "generally favorable reviews."Peter DeBruge of Variety writes: "Whether or not he is specifically referring to the present day, its demagogues, and the way certain evangelicals have once again sold out their core values for political advantage, A Hidden Life feels stunningly relevant as it thrusts this problem into the light." Jägerstätter biographer Erna Putz was touched by the spirituality of the film after a private screening in June 2019, stating that Malick has made an "independent and universal work". She also considered Diehl and Pachner's performances to be accurate to who Franz and Franziska were ("Franz, as I know him from the letters, and Franziska, as I know from encounters.").


== Accolades ==


== References ==


== External links ==
Official website
A Hidden Life on IMDb