The Worm Ouroboros is a heroic high fantasy novel by English writer E. R. Eddison, first published in 1922. The book describes the protracted war between the domineering King Gorice of Witchland and the Lords of Demonland in an imaginary world that appears mainly medieval and partly reminiscent of Norse sagas. The work is slightly related to Eddison's later Zimiamvian Trilogy, and collectively they are sometimes referred to as the Zimiamvian series.
The book was illustrated by Keith Henderson, who also illustrated books by Geoffrey Chaucer and  W. H. Hudson.


== Plot ==
A framing story in the first two chapters describes the world of the novel as Mercury, though it is clearly a fantasy version of Earth, a "secondary world"; no effort was made to conform to the scientific knowledge of Mercury as it existed at the time of writing. At a number of points the characters refer to their land as Middle earth, used here in its original sense of "the known world", and the gods worshiped have the names of deities from Greek mythology.
Oddly, the introductory framing story has a beginning, but is not referred to once the actual fantasy adventure begins. (Framing, as a technique, usually opens and closes a separate story contained inside the frame-narrative.)
The framing story having introduced the chief lords of Demonland—the brothers Juss, Spitfire, and Goldry Bluszco, and their cousin Brandoch Daha—the story begins in earnest with a dwarf ambassador from Witchland arriving in Demonland to demand that the Demons recognize King Gorice XI of Witchland as their overlord.  Juss and his brothers reply that they and all of Demonland will submit if the king (a famous wrestler) can defeat Goldry Bluszco in a wrestling match.
The match is held in the neutral territory of the Foliot Isles, and Gorice is killed.  His successor (or reincarnation) Gorice XII is a sorcerer who  banishes Goldry to an enchanted mountain prison, by means of a perilous sorcery requiring the help of the devious Goblin traitor Lord Gro.
While Lord Spitfire is sent back to raise an army out of Demonland, Lord Juss and his cousin Brandoch Daha, aided by King Gaslark of Goblinland, attempt an assault on Carcë, the capital of the Witches, where they think Goldry is held. The rescue fails, the Goblins flee, and Juss and Brandoch Daha are both captured. They escape with the aid of La Fireez, the prince of Pixyland and vassal of King Gorice, who helps them at great personal cost because he owes them a debt of honor.
Juss and Brandoch Daha return home to Demonland and then start an expedition to rescue Goldry Bluszco from his terrible prison, somewhere past the mountains of Impland. Lord Spitfire again stays behind to lead Demonland's armies against an expected invasion from Witchland.
The expedition's fleet is smashed and its army destroyed.  Juss and Brandoch Daha meet with three strange enchanted heroes of an earlier time, and Lord Juss is later nearly killed by a manticore. After a year of wandering they climb the mighty peak of Koshtra Pivrarcha and then attempt the even more difficult peak of Koshtra Belorn. Before reaching the summit of Koshtra Belorn they encounter Queen Sophonisba, a royal from that area to whom the gods had granted eternal youth when her realm was laid waste by the Witches.
From Sophonisba they learn that Goldry is held in prison on the top of Zora Rach Nam, a mountain which cannot be climbed and whose peak is surrounded by unceasing flames. There is only one way to free him: they must find a hippogriff's egg, and one of them must ride the newly hatched hippogriff. Queen Sophonisba gives Lord Juss one hippogriff egg. Alas, their lone companion, the Impland native Mivarsh Faz (knowing that he will have to walk back home alone, if the Demons get the hippogriff) steals the egg and tries to use it himself, causing his death. Lord Juss and Brandoch Daha set out for home, their quest defeated for the time being. But matters are not completely hopeless, as one of Queen Sophonisba's martlet scouts have told them of another hippogriff egg, lying at the bottom of a lake in Demonland.
Meanwhile, the armies of Witchland have attacked Demonland. Duke Corsus is the first commander of the Witchland army, and conquers part of Demonland, but is defeated by Spitfire. A new Witchland army, under the command of Lord Corinius, defeats Spitfire and captures most of Demonland. This includes Brandoch Daha's castle of Krothering, which had been watched over by his sister Lady Mevrian.
At this point, Lord Gro changes sides and helps Lady Mevrian escape from the grasp of Corinius, who wishes to marry her against her will. A few months later, Lord Juss and Brandoch Daha return and expel the Witches from Demonland.
Equipped with a new hippogriff egg, Lord Juss makes a second attempt to rescue his brother, and this time is successful. However, his forces are trapped in an inland sea by the Witchland navy; forced to engage in battle directly, they completely destroy that navy. La Fireez dies in this battle.
The Demons then sail to Carcë and face the remaining forces of Witchland in a climactic struggle. In the battle, Lord Gro is lambasted by Corund for switching sides; Gro responds by killing a Demon and is himself killed by Spitfire. Corund dies from wounds he suffers fighting with the heroes of Demonland.
His armies having failed, King Gorice attempts another terrible summoning; lacking the aid of Gro, he is unable to complete the spell and is destroyed. Duke Corsus poisons the remaining nobles of Witchland, and is killed himself by the dying Corinius.
Though triumphant, the Demon lords find that victory is bitter because there are no more enemies worthy of their heroism, no more great deeds to perform. Sophonisba, seeking to reward their heroism, prays to the gods who return the world to how it had been four years before. And so, with a blare of trumpets, an ambassador from Witchland arrives, "craving present audience" and the story starts anew.


== Characters ==

The Demons and their allies

Lord Juss is the chief lord of Demonland and a chief player in most of the battles in the story. He also leads two expeditions to rescue his brother Goldry Bluszco.
Goldry Bluszco is the brother of the hero and one of the chief lords of Demonland. Bluszco has two brothers, Lord Juss and Lord Spitfire. Unlike his bachelor brothers, Bluszco was betrothed to marry Princess Armelline of Goblinland.
Lord Spitfire is a Demon lord who spends most of the story in Demonland fighting the various Witchland invasions. His chief residence is the castle Owlswick.
Brandoch Daha is a lord of Demonland and the greatest swordsman of the age. After Corinius sacks his castle of Krothering, Brandoch Daha swears vengeance against Corinius.
Lady Mevrian is a great lady of Demonland and the sister of Brandoch Daha, who is left the task of defending her brother's castle of Krothering against Corinius's army. After a siege, the castle is taken and Lady Mevrian has to fend off the advances of Corinius. She is able to escape only with the aid of Heming and Cargo (Corund's sons) and Lord Gro, who betrays Witchland to help her.
Prince La Fireez is the ruler of Pixyland. Witchland has asserted suzerainty over his land, but he is consistently a Demon ally by way of repaying Lord Juss for saving his life. He dies in a naval battle.
The Witchlanders and their allies

Gorice is the King of Witchland, ruling from Carcë; he is said to be a single king with twelve incarnations. His eleventh incarnation, a champion wrestler, is killed in a wrestling match by Goldry Bluszco; his twelfth, a sorcerer, wears a signet ring in the shape of the ouroboros of the book's title.
Corinius is a warrior leader of Witchland, full of life and ambition. He is an enemy of Demonland, and as warrior of some skill he commands some respect from the lords of Demonland. He is poisoned by Duke Corsus and dies right after the Demons take Carcë.
Duke Corsus is one of the war leaders of the armies of Witchland. Duke Corsus was the chief war leader of Witchland a decade before the story begins. Corsus is wily and skillful but prone to drinking and no longer young. Right before the Demons conquer Carcë, he poisons Corinius, his wife Zenambria, his son Dekalajus, Corund's son Viglus, and Corund's son Heming. Corinius kills him when he realizes that Corsus poisoned him.
Lord Corund is the chief war leader of the armies of Witchland. He is a noble man, a mighty warrior, and has some respect from the main characters. Corund leads the Witchland army against the Demonland expedition to Impland. He is fatally wounded by Lord Juss.
Lord Gro is an advisor to Witchland; later he is an ally of Demonland. Gro is originally from Goblinland and was the foster-brother of King Gaslark before a falling-out. Gro is a famous explorer and wrote a book about his travels through Impland. Gro provides crucial aid to King Gorice XII of Witchland when he summons the magic which carries Goldry Bluszco away. He acts as an advisor and trusted messenger to the King regarding the campaign in Demonland. While in Demonland he meets and falls in love with Brandoch Daha's sister, the Lady Mevrian. Because of his love, he betrays the Witchland army and helps Mevrian escape from Corinius after Corinius captures her brother's castle of Krothering.
Lady Prezmyra is the young wife of Corund and the sister of La Fireez of Pixyland. She is a close friend of Lord Gro, and poisons herself, after the death of her husband and her brother, when the Demons conquer Carcë.


== The kingdoms of Mercury ==

The named nations and countries are:

Witchland
Demonland
Pixyland
Impland
Goblinland
The Foliot Isles
The land Zimiamvia (beyond the known world)
The Ghouls, wiped out in a genocidal war a few years before the story opensThe King of Witchland claims lordship over a number of locations which are not described (page 12):

Duke of Buteny and Estremerine
Commander of Shulan, Thramnë, Mingos, and Permio
Warden of the Esamocian Marches
Duke of Trace
King Paramount of Beshtria and Nevria
Prince of Ar
Great Lord over Ojedia, Maltraëny, Baltary, and ToribiaDespite the names of the nations, all the characters in the book are recognizably human and they are all the same species, or at least able to intermarry (e.g., Goldry Bluszco and Princess Armelline, Lord Corund and Lady Prezmyra).  Witchland, Demonland, and others appear to be country names, like England and France.  When first presented, the Demons are seen to have horns on their heads, but these horns are not mentioned again, nor is it said whether the other peoples have horns.


=== Maps ===
Gerald Hayes, a cartographer with the Royal Navy, created the first map for Ouroboros circa 1925, initially from internal evidence, and later in consultation with Eddison. He states in a letter to C.S. Lewis that he secured Eddison's approval "as a true presentation of all the lands, seas, and countries of his history."
Another map of the world by Bernard Morris was published in Twilight Zine No. 4 and reprinted in the book An Atlas of Fantasy (compiled by Jeremiah Benjamin Post) in 1979.
For another map of these lands created by J. B. Hare in 2004, see The world of The Worm Ouroboros.  For a German map by Erhard Ringer see Der Wurm Ouroboros
The map on this page was created by David Bedell in 1978.


== Background ==
Research done by Paul Edmund Thomas (who wrote an introduction to the 1991 Dell edition) shows that Eddison started imagining the stories which would turn into The Worm Ouroboros at a very early age. An exercise book titled The Book of Drawings dated 1892 and created by Eddison is to be found at the Bodleian Library. In this book are 59 drawings in pencil, captioned by the author, containing many of the heroes and villains of the later work.  Some of the drawings, such as The murder of Gallandus by Corsus and Lord Brandoch Daha challenging Lord Corund, depict events of Ouroboros.
As might be expected, significant differences exist between the ideas of a 10-year-old boy and the work of a 40-year-old man. Perhaps the most interesting change is in Lord Gro's character. In the drawings Lord Gro is a hero of skill and courage, while in the book he is a conflicted character, never able to pick a side and stick to it. Another curious change is that Goldry Bluszco is the main hero of the drawings, but off-stage in an enchanted prison for most of the novel.
Many people (including J. R. R. Tolkien) have wondered at and criticized Eddison's curious names for his characters (e.g. La Fireez, Fax Fay Faz), places and nations. According to Thomas, the answer appears to be that these names originated in the mind of a young boy, and Eddison could not, or would not, change them thirty years later when he wrote the stories down.


== Meaning of the title ==

The title refers to Ouroboros (Jörmungandr in Norse mythology), the snake or dragon that swallows its own tail and therefore has no terminus (in Old English, the word "worm" could mean a serpent or dragon).
Like the Ouroboros, the story ends at the same place as it begins, when the heroes realize that their lives have little meaning without the great conflict and wish that it could continue, and their wish is granted.
The theme of repetition pervades the work. Near the beginning and again near the end, a king of Witchland dies, Carcë is attacked, and Gorice XII carries out a conjuring in the fortress's Iron Tower. There are two quests to find and recover Goldry Bluszco. Three armies, under the influence of an enchantment, chase each other in an endless campaign until the heroes shatter the cycle on their quest.


== Comparison with other works ==
The Worm Ouroboros is often compared with J. R. R. Tolkien's The Lord of the Rings (which it predates by 32 years). Tolkien read The Worm Ouroboros, and praised it in print. C. S. Lewis wrote a short preface to an anthology of Eddison's works, including The Worm Ouroboros, concluding that "No writer can be said to remind us of Eddison."In contrast to The Lord of the Rings, to which mythopoeia is central, Eddison makes few references either to actual mythology or to an invented mythology after the fashion of the Silmarillion. One example of this is Eddison's ad hoc names for people and places versus Tolkien's invention of entire languages.
Also, while The Lord of the Rings is written mostly in modern English, Eddison wrote The Worm Ouroboros largely in sixteenth-century English, making use of his experience translating Norse sagas and reading medieval and Renaissance poetry; a nearly unique approach among popular fantasy novels. Eddison incorporates a number of actual early modern poems into the story, including Shakespeare's 18th sonnet, all meticulously credited in an appendix.
The tale's morality has also been described as uncommon in modern fantasy; in particular, it differs sharply from Tolkien's heroism of the common man in a fight against evil and C. S. Lewis's Christian allegory. The Demon lords hold to the Old Norse warrior ethic of loyalty and glory. The leaders of Witchland are regarded as noble and worthy opponents; in the final chapter, Goldry Bluszco compares them very favorably with the "uncivil races" of Impland.


== Influence and reception ==
New York Times critic Edwin Clark praised the novel lavishly, saying "This romance has the gaudiness and flair of the Elizabethans. It has the exuberance of great appetites and vigorous living. It transcends all ordinary life. It burns with the wonder and awe of excess." But Clark also noted that Eddison "is stylistic in the grand and heroic manner that evokes beauty and vigorous life, but it seems to us that without injury to his verbal charm or loss of beauty in his passage of atmosphere saturated with glamour of nature, he could have removed much that would quicken the action of his narration to a more attractive pace."Reviewing a 1952 edition, Boucher and McComas described it as "one of the major imaginative novels of this century" and "the detailed creation of a vividly heroic alien history." They particularly commended "the resonant clangor of its prose, the tremendous impetus of its story-telling, [and] the magnificent audacity (and sternly convincing consistency) of its fantasy concepts." Donald Barr declared that Eddison wrote "in a heroic prose made of high ceremonial gestures and tropes from the great age of metaphor and described The Worm as being "quite unique among modern novels" as "a narrative of pure event" where, with a lone exception, "we are never given the interior of a character, only the actions".In 1963, Avram Davidson praised the novel's prose for "abound[ing] in beautiful, quotable language" and its story as one of "war, witchcraft, adventure, conspiracy, violence, bloodshed, intrigue." Davidson, though, faulted Eddison's conception, saying "Ouroboros is a classic, but it is not and cannot be a great classic," because it lacks "humanity"—the realistic detail of great works like the Arabian Nights, where characters "do not merely kiss and declaim and posture." However, J. Max Patrick, also reviewing the Xanadu paperback, dismissed the novel as "a pseudo-Ossianic epic, adolescent in tone and pretentiously archaic", although commenting that "Eddison sometimes achieves the splendid prose and gorgeous artifice appropriate to his sagas."Karl Edward Wagner was influenced by The Worm Ouroboros as a teenager. Michael Swanwick quotes from Ouroboros in The Dragons of Babel.In 1983, E. F. Bleiler praised The Worm Ouroboros as "still the finest heroic fantasy."American herpetologist Emmett Reid Dunn named a species of South American lizard Morunasaurus groi, after the character Lord Gro.


== Publication history ==
1922: Original publication in London by Jonathan Cape with illustrations by Keith Henderson
1924: "New and Cheaper Edition", Jonathan Cape (actually the remaindered copies of the first edition with an inserted cancel page which lists the publishing details of this 'edition')
1926: American hardcover issued by Albert & Charles Boni
1952: Hardcover publication from E. P. Dutton, featuring illustrations by Keith Henderson and an introduction by Orville Prescott
1962: Trade paperback publication in the Xanadu Fantasy Library
1967: Paperback edition from Ballantine Books (following the success of The Lord of the Rings), with several printings in different years (incorporated into the Ballantine Adult Fantasy series)
1999: Paperback reissue in Replica Books of Bridgewater, New Jersey
2000: A UK paperback edition in the Fantasy Masterworks series
2006: American trade softcover edition by Barnes & Noble, ISBN 978-0-7607-7364-2.
2008: A new edition by Forgotten Books and on Amazon's Kindle


== References ==
Notes


== External links ==
The Worm Ouroboros at Faded Page (Canada)
The Worm Ouroboros at Internet Sacred Text Archive
The Worm Ouroboros at Project Gutenberg Australia
 The Worm Ouroboros public domain audiobook at LibriVox
The Works of E.R. Eddison
"Where Head and Tail Meet: The Worm Ouroboros" by Ryan Harvey