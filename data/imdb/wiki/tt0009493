A petticoat or underskirt is an article of clothing, a type of undergarment worn under a skirt or a dress. Its precise meaning varies over centuries and between countries.
According to the Oxford English Dictionary, in current British English, a petticoat is "a light loose undergarment ... hanging from the shoulders or waist".  In modern American usage,  "petticoat" refers only to a garment hanging from the waist. They are most often made of cotton, silk or tulle. Without petticoats, skirts of the 1950’s would not have the floof they were known for. In historical contexts (sixteenth to mid-nineteenth centuries), petticoat refers to any separate skirt worn with a gown, bedgown, bodice or jacket; these petticoats are not, strictly speaking, underwear, as they were made to be seen. In both historical and modern contexts, petticoat refers to skirt-like undergarments worn for warmth or to give the skirt or dress the desired attractive shape.
Petticoat is the standard name in English for any underskirt worn as part of non-Western clothing such as the lehenga with the sari.


== Terminology ==
Sometimes a petticoat may be called a waist slip or underskirt (UK) or half slip (US), with petticoat restricted to extremely full garments. A chemise hangs from the shoulders. Petticoat can also refer to a full-length slip in the UK, although this usage is somewhat old-fashioned.


== History ==

In the fourteenth century, both men and women wore undercoats called "petticotes". The word "petticoat" came from Middle English pety cote or pety coote, meaning "a small coat/cote". Petticoat is also sometimes spelled "petty coat". The original petticoat was meant to be seen and was worn with an open gown. The practice of wearing petticoats as undergarments was well established in England by 1585. In French, petticoats were called jupe. The basquina, worn in Spain, was considered a type of petticoat.In the 18th century in Europe and in America, petticoats were considered a part of the exterior garment and were meant to be seen. An underpetticoat was considered an undergarment and was shorter than a regular petticoat. Underpetticoats were also known as a dickey. Also in the American colonies, working women wore shortgowns (bedgowns) over petticoats that normally matched in color. The hem length of a petticoat in the 18th century depended on what was fashionable in dress at the time. Often, petticoats had slits or holes for women to reach pockets inside. Petticoats were worn by all classes of women throughout the 18th century. The style known as polonaise revealed much of the petticoat intentionally.In the early 19th century, dresses became narrower and simpler with much less lingerie, including "invisible petticoats". Then, as the waltz became popular in the 1820s, full-skirted gowns with petticoats were revived in Europe and the United States. In the Victorian era, petticoats were used to give bulk and shape to the skirts worn over the petticoat. By the mid 19th century, petticoats were worn over hoops. As the bustle became popular, petticoats developed flounces towards the back. In the 1870s, petticoats were worn in layers. Colored petticoats came into fashion by the 1890s.In the early 20th century, petticoats were circular, had flounces and buttons in which women could attach additional flounces to the garment. Bloomers were also touted as a replacement for petticoats when working and by fashion reformers.After World War I, silk petticoats were in fashion.Petticoats were revived by Christian Dior in his full-skirted "New Look" of 1947 and tiered, ruffled, stiffened petticoats remained extremely popular during the 1950s and 1960s.  These were sold in a few clothing stores as late as 1970.


== Asian petticoats ==
It is the main undergarment worn with a sari. Sari petticoats usually match the color of the sari and are made of satin or cotton.  Notable differences between the western petticoat and sari petticoat include that the latter is rarely shorter than ankle length and is always worn from the waist down. In India, it is also called inner skirt or an inskirt.


== In popular culture ==
The early feminist Mary Wollstonecraft was disparaged by Horace Walpole as a "hyena in petticoats". Florentia Sale was dubbed "the Grenadier in Petticoats" for travelling with her military husband Sir Robert Henry Sale around the British Empire.
The phrase "petticoat government" has referred to women running government or domestic affairs. The phrase is usually applied in a positive tone welcoming female governance of society and home, but occasionally is used to imply a threat to "appropriate" government by males, as was mentioned in several of Henry Fielding's plays. An Irish pamphlet Petticoat Government, Exemplified in a Late Case in Ireland was published in 1780. The American writer Washington Irving used the phrase in Rip Van Winkle (1819). Frances Trollope wrote Petticoat Government: A Novel in 1850. Emma Orczy wrote Petticoat Government, another novel, in 1911. G. K. Chesterton (1874–1936) mentions petticoat in a positive manner; to the idea of female dignity and power in his book, What's Wrong With the World, (1910) he states:

It is quite certain that the skirt means female dignity, not female submission; it can be proved by the simplest of all tests. No ruler would deliberately dress up in the recognized fetters of a slave; no judge would appear covered with broad arrows. But when men wish to be safely impressive, as judges, priests or kings, they do wear skirts, the long, trailing robes of female dignity. The whole world is under petticoat government; for even men wear petticoats when they wish to govern.
President Andrew Jackson's administration was beset by a scandal called the "Petticoat affair", dramatized in the 1936 film The Gorgeous Hussy. A 1943 comedy film called Petticoat Larceny (cf. petty larceny) depicted a young girl being kidnapped by grifters. In 1955, Iron Curtain politics were satirized in a Bob Hope and Katharine Hepburn film, The Iron Petticoat. In the same year Western author Chester William Harrison wrote a short story "Petticoat Brigade" that was turned into the film The Guns of Fort Petticoat in 1957. Blake Edwards filmed a story of an American submarine filled with nurses from the Battle of the Philippines called Operation Petticoat (1959). Petticoat Junction was a CBS TV series that aired in 1963. CBS had another series in the 1966–67 season called Pistols 'n' Petticoats.


== See also ==
Breeching (boys), a historical practice involving the change of dress from petticoat-like garments to trouser-like ones
Hoop skirt, a women's undergarment worn in various periods to extend skirts into a fashionable shape


== References ==


=== Citations ===


=== Sources ===
Baumgarten, Linda (2002). What Clothes Reveal: The Language of Clothing in Colonial and Federal America. New Haven: Yale University Press. ISBN 9780300095807.CS1 maint: ref=harv (link)
Cunningham, Patricia A. (2003). Reforming Women's Fashion, 1850-1920: Politics, Health and Art. Kent, Ohio: The Kent State University Press. ISBN 0873387422.CS1 maint: ref=harv (link)
Cunnington, C. Willett; Cunnington, Phillis (1992). The History of Underclothes (New ed.). Dover. ISBN 9780486271248.CS1 maint: ref=harv (link)
Higgins, Padhraig (2010). A Nation of Politicians: Gender, Patriotism, and Political Culture in Late Eighteenth-Century Ireland. University of Wisconsin Press. ISBN 9780299233334 – via Project MUSE.CS1 maint: ref=harv (link)
Picken, Mary Brooks (1957). The Fashion Dictionary: Fabric, Sewing, and Dress as Expressed in the Language of Fashion. New York: Funk & Wagnalls Company.CS1 maint: ref=harv (link)
Sholtz, Mackenzie Anderson (2016). "Petticoat, 1715-1785".  In Blanco, Jose; Doering, Mary D. (eds.). Clothing and Fashion: American Fashion from Head to Toe. 1. Santa Barbara, California: ABC-CLIO. ISBN 9781610693103.CS1 maint: ref=harv (link)


== External links ==
Quilted Petticoat, 1750-1790, in the Staten Island Historical Society Online Collections Database
Petticoat-Government in a Letter to the Court Lords (1702)