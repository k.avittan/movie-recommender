Golden Rule Kate is a 1917 American silent western drama film starring Louise Glaum, William Conklin, Jack Richardson, Mildred Harris, and John Gilbert. It was directed by Reginald Barker from a story written by Monte M. Katterjohn and produced and distributed by the Triangle Film Corporation.
The title role in this feature length western was a big departure for Glaum. One of the leading vamps of the mid 1910s—who played wicked dance hall girls in several westerns opposite William S. Hart—she stars here as the heroine, playing a female Bill Hart, with two pistols, before such roles were common among women.


== Plot ==
The setting is the Old West town of Paradise, Nevada, where a young woman, Mercedes Murphy (played by Louise Glaum), co-owns and operates a combination saloon and dance hall called the Red Hen with her business partner, Slick Barney (played by Jack Richardson). Her little half-sister, Olive "Live" Sumner (played by Mildred Harris), who is crippled, lives with her and she makes every effort to protect the child. A tough, but good-hearted businesswoman, Mercedes shows a tender side at home with Live. Her partner, Slick, and a cowboy called the Heller (played by John Gilbert), who has a heart of gold, are both interested in Live.
A reform movement comes to Paradise with the arrival of Reverend Gavin McGregor (played by William Conklin), who wants to clean up the town and sets up a church next to the saloon and dance hall. Initially, Mercedes is opposed to the church and there is immediate antagonism between her and the reverend. He and Mercedes come to respect each other, however, and she is so impressed by his sermons that she closes down her business.
When her little sister is sexually abused, Mercedes blames the reverend and is filled with wrath. She begins a vigorous attack on the church and goes gunning for him. But the Heller discovers that it was actually Mercedes' partner, Slick, who compromised Live's virtue and shoots him dead. After Mercedes learns that it was Slick and not the reverend who betrayed Live, she and the reverend become friends. She closes down the saloon and dance hall permanently and prepares to leave town with Live. The reverend then professes his love for her and begs her to stay.


== Cast in credits order ==
Louise Glaum as Mercedes Murphy aka "Golden Rule Kate"
William Conklin as Reverend Gavin McGregor
Jack Richardson as "Slick" Barney
Mildred Harris as Olive "Live" Sumner, Kate's half-sister
John Gilbert as the Heller


== Uncredited cast listed alphabetically ==
Gertrude Claire as Mrs. McGregor
Josephine Headley as Vegas Kate
J.P. Lockney as "Nose Paint" Jonas
Milton Ross as Jim Preston


== Reviews ==
A Los Angeles Times review of Sunday, August 12, 1917, reads:

"Attention to details in the modern photoplay is strenuous work, and particularly is this the case since motion picture "fans" are keenly critical of the most minute errors. Woe to the director who lets hero leave the house with a fedora hat and arrive at the cafe with a derby, or perform any other such discrepancy.
In "Golden Rule Kate," in which Louise Glaum is being starred at Clune's Broadway this week, Director Reginald Barker had all the trials that such alternating of scenes necessitates.
The production is a big dramatic western play of the Bill Hart type, and all the exteriors were filmed at the Santa Monica ranch studios of the Triangle, where a complete western town has been built for productions of this nature.
All through the production the action shifts from the dance hall to the church and from various interiors to the western street. Needless to say that the director and actors had a busy time of it, remembering all the infinitesimal details that combine to make the picture as artistically perfect as possible.
Miss Glaum in this new production has a role radically different from her usual "siren" parts. It is a sort of feminized Bill Hart character she portrays as "The Sage Brush Hen," owner of a saloon and dance hall in Paradise, Nev. The picture is full of striking dramatic situations, tense moments and acting. It makes a strong human interest appeal, combining rapid-fire action, romance and comedy. Jack Gilbert, Jack Richardson, Mildred Harris, William Conklin, Gertrude Claire and J. P. Lockney form a strong supporting cast for Miss Glaum."

A Times review of Wednesday, August 15, 1917, reads: 

"Louise Glaum isn't "vamping" a single "vamp"—and she's wearing only one gown in "Golden Rule Kate" at Clune's Broadway, this week. I know this will sound to you just like announcing Bill Hart without Fritz, Mary Pickford without curls, the American people without a vote, or any other national calamity.
Yet "Golden Rule Kate" is one of the most original piquant picture plays we've had in a blue moon, and Monte Katterjohn should mark up a red-letter day on his calendar. In this two-dimension drama, Miss Glaum plays the role of a young woman who, having come to a new Nevada mining camp with the gold rush, and finding no gold, courageously starts out to run a dance hall. She has her little sister, Mildred Harris, with her, and strives to have the child lead the sheltered life. Along comes a preacher, played with superior strength and sincerity by William Conklin, who in this role proves himself possessed in high degree of a nice sense of dramatic values. At once there is antagonism between Kate—who is a sort of Diana of the dance halls for personal chastity and a sense of justice—and the minister. He is lassoed in his pulpit and dragged to the dance hall, but Kate's sense of fair play is outraged by the act, and she makes the cowboys liberate him. He calls on her, and tells her he misses her at church. "And I've missed you at the dance hall," she says. "If you'll tend bar for me for an hour, I'll go to your church next Sunday," says Kate. And he takes her up, with a mixture of humor and devotion to the cause which makes a delicious combination of motive such as is seldom put over on the screen. Of course in the final film foot, she comes into the fold and the minister's embrace.
There is some unusually good acting done in this picture, which is directed by Reginald Barker. Jack Gilbert plays "The Heller" with zest, not alone his acting, but his personality getting over wonderfully well. Gertrude Claire, one of the sweetest "mothers" in the films wins a golden place by her acting in this picture; and there is a drunk played by someone whose name I do not know that is an artistic creation in itself."


== Censorship ==
Like many American films of the time, Golden Rule Kate was subject to cuts by city and state film censorship boards. The Chicago Board of Censors issued the film an Adults Only permit and required the following changes: reel 1, flash gambling scenes and cut all scenes of women embracing men at the bar or women drinking at the bar; reel 2, flash two gambling scenes and cut all but one scene of girl pointing gun at minister and all but one scene of girl with gun holding back crowd at saloon; reel 3, cut close-up of blonde woman and man drinking at bar; reel 4, flash all gambling scenes and cut 5 scenes of man and girl embracing at bar; and reel 5, cut scene of shooting man.


== Preservation status ==
A print film still exists although possibly in an abridged form.


== References ==


== External links ==
Golden Rule Kate on IMDb
Golden Rule Kate at the AFI Catalog of Feature Films
Golden Rule Kate synopsis at AllMovie
Lantern slide and film stills at silenthollywood.com