Please Excuse Me for Being Antisocial (on-cover styling Please excuse me for being antisocial) is the debut studio album by American rapper Roddy Ricch, released by Atlantic Records and Bird Vision Entertainment on December 6, 2019. It features guest appearances from Gunna, Lil Durk, Meek Mill, Mustard, Ty Dolla Sign, and A Boogie wit da Hoodie. The album debuted at number one on the US Billboard 200 and spent four non-consecutive weeks atop. It features the singles, "Big Stepper", "Start wit Me" featuring Gunna, "Tip Toe" featuring A Boogie wit da Hoodie, and "High Fashion". Prior to being released as a single, "The Box" became Roddy Ricch's highest-charting song of his career, reaching number one on the US Billboard Hot 100; the song later became the album's fourth single.


== Background and production ==
In an interview with Revolt, the album's audio engineer Chris Dennis uncovered some of the album's recording sessions. Dennis recalls first meeting Roddy Ricch at a studio session one a day in March 2019, where after they "just kept working ever since then" from that day on. Ricch had just returned to the US after touring with Post Malone on the European leg of Malone's Beerbongs & Bentleys Tour. Ricch's label wanted to start working on his debut album, something which Ricch also expressed interest in. Dennis says they spent a "solid year" working on the album, changing tracklists constantly and recording new music. He used production software Plugin Alliance which, he explained, "has no latency in the recording on any of the plugins". For the album, Ricch didn't want to employ a lot of effects or reverb, because "he likes his stuff really clean, dry, and in your face. That was the learning curve in the beginning — getting his clean vocals. You also have to work fast because he can record a song in 10 minutes", Dennis stated. "The Box", for instance, was recorded in roughly 15 minutes. On the track "War Baby", a choir was used, an idea Ricch came up with. The choir was arranged through Ricch's cousin.
Around 250 songs were recorded for the album. Dennis stated that a lot of those songs will instead appear on other artists' albums.


== Commercial performance ==
Please Excuse Me for Being Antisocial debuted atop the US Billboard 200 with 101,000 album-equivalent units (including 3,000 pure album sales) in its first week. It is Roddy Ricch's first number one on the chart. In its second week, the album fell to number three on the chart with 81,000 units. In its third week, the album remained at number three on the chart earning 73,000 more units that week. In its fourth week, the album climbed to number two on the chart with 74,000 units. In its fifth week, it regained the number one position on the chart, earning 97,000 album-equivalent units, an increase of 31% in total units over the previous week. The album topped the charts for a third time with 95,000 units sold in its 8th week of release. The album later returned to number one on the chart for a fourth time with 79,000 units sold in its 10th week on the chart. It became the longest running number one debut rap album in the US since 2003. On April 10, 2020, the album was certified platinum by the Recording Industry Association of America (RIAA) for combined sales and album-equivalent units of over one million units in the United States.Five songs off the album also managed to chart on the Billboard Hot 100, with "The Box" being the highest charting song, spending eleven weeks at number one on the chart despite no initial single release.


== Track listing ==
Credits adapted from Tidal.
Notes

^[a]  signifies a co-producer
^[b]  signifies an additional producer


== Personnel ==
Credits adapted from Tidal:
Chris Dennis – recording (tracks 1–9, 11–16)
Curtis "Sircut" Bye – engineering assistant (tracks 1, 2, 4, 6–9, 11–16), mixing (track 5)
Zachary Acosta – engineering assistant (tracks 1, 2, 4–9, 11–16)
Cyrus "NOIS" Taghipour – mixing (tracks 1, 2, 4–9, 11–16)
Derek "MixedByAli" Ali – mixing (tracks 1–9, 11–16)
Nicolas de Porcel – mastering (tracks 1, 2, 4–9, 11–16)
Mike Bozzi – mastering (track 3)


== Charts ==


== Certifications ==


== References ==