A locket is a pendant that opens to reveal a space used for storing a photograph or other small item such as a lock of hair. Lockets are usually given to loved ones on holidays such as Valentine's Day and occasions such as christenings, weddings and, most noticeably during the Victorian Age, funerals.  Historically, they often opened to reveal a portrait miniature.
Lockets are generally worn on chains around the neck and often hold a photo of the person who gave the locket, or they could form part of a charm bracelet. They come in many shapes such as ovals, hearts and circles and are usually made of precious metals such as gold or silver befitting their status as decorative jewellery.
Lockets usually hold only one or two photographs, but some specially made lockets can hold up to eight. Some lockets have been fashioned as 'spinner' lockets, where the bail that attaches to the necklace chain is attached but not fixed to the locket itself which is free to spin. This was a common style in the Victorian Age. Around 1860 memento lockets started to replace mourning rings as the preferred style of mourning jewellery.Keepsake lockets can also be made with a glass pane at the front so that what is inside can be seen without opening the locket. Such lockets are generally used for items like locks of hair which could fall out and become lost if the locket were repeatedly opened, whereas photograph lockets are generally enclosed on all sides and the photographs are secured by pieces of clear plastic.
Another kind of locket still made was in a filigree style with a small cushion in the centre to which a few drops of perfume should be added. Perfume lockets  were popular in eras when personal hygiene was restricted and sweet smelling perfume was used to mask the odour of a person or their companions.
Very rare World War I- and World War II-era British and American military uniform locket buttons exist, containing miniature working compasses.


== See also ==
Stanhope (optical bijou)


== References ==