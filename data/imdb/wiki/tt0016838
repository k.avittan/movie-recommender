A false alarm, also called a nuisance alarm, is the deceptive or erroneous report of an emergency, causing unnecessary panic and/or bringing resources (such as emergency services) to a place where they are not needed. False alarms may occur with residential burglary alarms, smoke detectors, industrial alarms, and in signal detection theory.  False alarms have the potential to divert emergency responders away from legitimate emergencies, which could ultimately lead to loss of life. In some cases, repeated false alarms in a certain area may cause occupants to develop alarm fatigue and to start ignoring most alarms, knowing that each time it will probably be false. The activation of false alarms in businesses and schools can lead to serious disciplinary actions and criminal penalties such as fines and jail time.


== Overview ==
The term “false alarm” refers to alarm systems in many different applications being triggered by something other than the expected trigger-event. Examples of this those applications include residential burglar alarms, smoke detectors, industrial alarms, and signal detection theory. The term “false alarm” may actually be semantically incorrect in some uses. For example, a residential burglar alarm could easily be triggered by the residents of a home accidentally. The alarm is not necessarily false – it was triggered by the expected event – but it is “false” in the sense that the police should not be alerted. Due to this problem, false alarms can also be referred to as “nuisance alarms.”
Sociologist Robert Bartholomew explains that there are many negative effects of false alarms, such as "fear, havoc, disruptions to emergency services, and wasted resources." Health and safety can also be effected, as they can cause anxiety and encourage people to race toward an alarm or away from it, which can result in accidents in the panic. One more problem is the "Cry Wolf Effect", which can cause people to ignore legitimate alarms; "in the event of a real attack, subsequent warnings may be taken lightly or ignored altogether."


== Types ==


=== Residential burglar alarms ===
In the United States, between 94% and 98% of all burglar alarm activations are falsely triggered.


==== Causes and prevention ====
Residential burglar alarms can be caused by improper arming and disarming of the system, power outages and weak batteries, wandering pets, and unsecured doors and windows.  In the U.S. false alarms cost police agencies up 6.5 million personnel hours, according to the International Association of Chiefs of Police. A 2002 study by the U.S. Justice Department estimated the cost of false alarms to be as high as $1.5 billion. Due to this cost, many cities now require permits for burglar alarms, have enacted verified response protocols, or have introduced fines for excessive false alarms.


===== Improper arming and disarming of the system =====
This is typically caused by simple mistakes like entering the wrong passcode or letting too much time pass before entering the code. These types of false alarms can be prevented by taking more time to disarm systems, and entering a home with at least one hand free to properly disarm one's system.


===== Untrained users =====
Untrained users can be anyone who may need to temporarily access one's home but is unfamiliar with one's system. Common untrained users include cleaning crews, repairmen, dog walkers, or babysitters. Better educating temporary users about a particular system can prevent them from accidentally triggering it.


===== Power problems =====
If a power outage occurs on a system with a weak backup battery, it can cause the alarm to trigger. Preventing this type of false alarm requires the user to periodically replace the backup battery.
Users should have surge suppression on the AC power as well as the RJ31X area.  If properly installed, the surges should bypass the system. To help prevent a feedback loop up case ground, the RJ31X surge suppression should not be installed inside the burglar alarm panel, and should only be grounded to the ground lug of the AC surge suppression. To greater reduce false alarms, three to four knots should be tied in the phone line between the RJ31X Surge suppression and the panel as well as three to four knots in the low voltage AC power source feeding the panel after the AC surge suppression.
A voltage drop, current rise is another issue caused by power induced by HVAC systems and other heavy loads.  AC surge suppression does not stop this problem so a power monitoring device works.  Example, in a nursing home a fire alarm system keeps having problems with their power supply and at times deprogramming the panel every time the backup generator came on.  In facilities like these, users are required to test generators every month. When the power comes on, if all of the HVAC systems are on the same time delay, the heavy load is what causes the voltage drop and current rise, which will trigger a false alarm. There is technology like the 120HWCP20CBPLC that monitors the power and if the voltage drops, the unit shuts off the power. The batteries inside the fire alarm will sustain the system until the power is back to normal.
Close proximity strikes by lightning can set off the ground fault circuit and at times break the system if the system does not comply with NEC code Art. 250.94 which illustrates how to get rid of grounding differentials. A ground filter/notch filter can prevent a feedback loop up case ground, which can cause false alarms. Also, surge suppression improperly installed inside a fire alarm panel can also cause a feedback loop up case ground and cause false alarms. The surge suppression must be outside of the box, in a plastic box and not grounded directly to the fire panel, rather than grounded to the ground side/line side of the in-line series three stage two tank circuit designed AC surge suppression. Parallel does not protect a fire alarm panel nor prevent false alarms. In series, surge suppression can help the user control where the surges are coming from and the ground filter can also help. Tying three to four knots in the low voltage wire between the low voltage surge suppress and the fire alarm panel and tying at least one knot in the phase/neutral/and ground wire attached to the power supply can help mitigate false alarms, as well as help the surge suppression do a better job.


===== Pets =====
Some motion sensors will be triggered by pets moving around a home. This problem can be fixed by finding motion detectors that are not sensitive to infrared signatures belonging to anything less than eighty pounds, or by restricting the access of pets to rooms with motion detectors.


===== Unsecured windows and doors =====
Windows and doors that are not fully closed can cause the alarm contacts to be misaligned which can result in a false alarm. In addition, if a door or window is left slightly ajar, wind may be able to blow them open which will also cause a false alarm. To prevent this from happening, door and windows should always be shut securely and locked.


=== Smoke detectors ===
False alarms are also common with smoke detectors and building fire alarm systems. They occur when smoke detectors are triggered by smoke that is not a result of a dangerous fire. Smoking cigarettes, cooking at high temperatures, burning baked goods, blowing out large numbers of birthday candles, fireplaces and woodburners when used around a smoke detector can all be causes of these false alarms. Additionally, steam can trigger an ionisation smoke detector that is too sensitive, another potential cause of false alarms.


=== Industrial alarms ===
In industrial alarm management, a false alarm (nuisance alarm) could refer either to an alarm with little information content that can usually safely be eliminated, or one that could be valid but is triggered by a faulty instrument.  Both types are problematic because of the "cry wolf" effect described above.


=== Signal detection theory ===
In (signal) detection theory, a false alarm occurs where a non-target event exceeds the detection criterion and is identified as a target (see Constant false alarm rate).


== Examples ==
Soviet Lieutenant Colonel Stanislav Petrov correctly assessed the 1983 Soviet nuclear false alarm incident, thus averting a nuclear war.
One tragic example of the consequences of continued false alarms was the Boland Hall Fire on January 19, 2000. Because of false fire alarms many students started ignoring the fire alarms. However, when an actual fire broke out, three students who ignored the fire alarms died and many others suffered injuries.
Likewise, after too many audible car alarms are found false, most people no longer pay attention to see whether someone is stealing a vehicle, so even certain experienced thieves may confess that these alarms would not deter them from stealing vehicles.Another notable false alarm was the Hawaii missile alert on January 13, 2018, in which an emergency message was sent incorrectly warning people in Hawaii of an incoming ballistic missile.


== Semantics ==
The term "false alarm" is actually a misnomer, and is regularly replaced by the term "nuisance alarm".  When a sensor operates, it is hardly false, and it is usually a true indication of the present state of the sensor.  A more appropriate term is nuisance, indicating that the alarm activation is inconvenient, annoying, or vexatious.  A prime example of this difference is burglar alarms being set off by spiders. (A spider crawling on a web in front of the motion detector appears very large to the motion detector.)False alarms could also refer to situations where one becomes startled about something that is later determined to be untrue.


== References ==


== External links ==
False Alarms: Their Causes and Prevention