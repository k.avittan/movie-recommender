Ladislas Starevich (Russian: Владисла́в Алекса́ндрович Старе́вич, Polish: Władysław Starewicz; August 8, 1882 – February 26, 1965) was a Polish-Russian stop-motion animator notable as the author of the first puppet-animated film The Beautiful Leukanida (1912). He also used dead insects and other animals as protagonists of his films. Following the Russian Revolution, Starevich settled in France.


== Early career ==

Władysław Starewicz was born in Moscow to ethnic Polish parents from present-day Lithuania. His father, Aleksander Starewicz, was from Surviliškis near Kėdainiai and his mother, Antonina Legęcka, from Kaunas. Both belonged to lesser nobility and were in hiding after the failed January Uprising against the Tsarist Russian domination. The boy was raised by his grandmother in Kaunas, then the capital of Kaunas Governorate within the Russian Empire. He attended Gymnasium in Dorpat (today Tartu, Estonia).
Starewicz had interests in a number of different areas; by 1910 he was named Director of the Museum of Natural History in Kaunas, Lithuania. There he made four short live-action documentaries for the museum. For the fifth film, Starewicz wished to record the battle of two stag beetles, but was stymied by the fact that the nocturnal creatures inevitably die whenever the stage lighting was turned on. Inspired by a viewing of Les allumettes animées [Animated Matches] (1908) by Émile Cohl, Starewicz decided to re-create the fight through stop-motion animation: by replacing the beetles' legs with wire, attached with sealing wax to their thorax, he is able to create articulated insect puppets. The result was the short film Lucanus Cervus (1910), apparently the first animated puppet film  and the natal hour of Russian animation.
In 1911, Starewicz moved to Moscow and began work with the film company of Aleksandr Khanzhonkov. There he made two dozen films, most of them puppet animations using dead animals. Of these, The Beautiful Leukanida (premiere – 1912), first puppet film with a plot inspired in the story of Agamenon and Menelas, earned international acclaim (one British reviewer was tricked into thinking the stars were live trained insects), while The Grasshopper and the Ant (1911) got Starewicz decorated by the czar. But the best-known film of this period, was Mest' kinematograficheskogo operatora (Revenge of the Kinematograph Cameraman, aka The Cameraman's Revenge) (1912), a cynical work about infidelity and jealousy among the insects. Some of the films made for Khanzhonkov feature live-action/animation interaction. In some cases, the live action consisted of footage of Starewicz's daughter Irina. Particularly worthy of note is Starevich's 41-minute 1913 film The Night Before Christmas, an adaptation of the Nikolai Gogol story of the same name. The 1913 film Terrible Vengeance won the Gold Medal at an international festival in Milan in 1914, being just one of five films which won awards among 1005 contestants.During World War I, Starewicz worked for several film companies, directing 60 live-action features, some of which were fairly successful. After the October Revolution of 1917, the film community largely sided with the White Army and moved from Moscow to Yalta on the Black Sea. After a brief stay, Starewicz and his family fled before the Red Army could capture the Crimea, stopping in Italy for a while before joining the Russian émigrés in Paris.


== After World War I ==
At this time, Władysław Starewicz changed his name to Ladislas Starevich, as it was easier to pronounce in French. He first stablished with his family in Joinville-le-pont, while he worked as a cameraman. He rapidly returned to make puppet films. He made Le mariage de Babylas (Midnight Wedding), L'épouvantail (The Scarecrow, 1921 ), Les grenouilles qui demandent un roi (alternately called Frogland and The Frogs Who Wanted a King) (1922)), Amour noir et blanc (Love in Black and White, 1923), La voix du rossignol (The Voice of the Nightingale, 1923) and La petite chateuse des rues (The Little Street Singer, 1924).  In these films he was assisted first by his daughter Irina (who had changed her name to Irène) who collaborated in all his films and defended his rights, his wife Anna Zimermann, who made the costumes for the puppets and Jeane Starewitch (aka Nina Star) who was engaged by his father in some films (The Little Street Singer, The Queen of the Butterflies, The Voice of the Nightingale, The Magical clock, etc.)
In 1924, Starevich moved to Fontenay-sous-Bois, where he lived until his death in 1965. There he made the rest of his films. Among the most notable are The Eyes of the Dragon (1925), a Chinese tale with complex and wonderful sets and character design, in which Starewitch shows his talent of decorator artist and ingenious trick-filmmaker, The Town Rat and the Country Rat (1927), a parody of American slapstick films, The Magical Clock (1928), a fairy tale with amazing middle-age puppets and sets, starring Nina Star and music by Paul Dessau, The Little Parade, from Andersen's tale The Steadfast Tin Soldier. Six weeks after the premiere of The Little Parade, sound was added by Louis Nalpas company. Starewitch started a collaboration with him, wishing to make a feature full-length film: Le Roman de Renard. All his 1920s films are available on DVD.


== "Le Roman de Renard" ==
Often mentioned as being among his best work, The Tale of the Fox (French: Le Roman de Renard, German: Reinicke Fuchs) was also his first animated feature. It was entirely made by him and his daughter, Irene. Production took place in Fontenay-sous-Bois from 1929–1930. When the film was ready, the producer, Louis Nalpas, decided to add sound by disc support but this system failed and the film was not released. German film studio UFA got interest to show the film in two parts. Sound was added in German and it premiered in Berlin in 1937. Later, in 1941, Roger Richebé (Paris Cinéma Location) produced a French sound version, which premiered in April 1941. It was the third animated feature film to have sound, after Quirino Cristiani's Peludópolis (1931) and The New Gulliver (1935) from the Soviet Union.


== The "Fétiche" series (Mascott) ==
In 1933 Ladislas and Irene Starewitch produced and directed a film of about 1000 meters title at first in the laboratory "LS 18". Under pressure from distributors, length was greatly reduced, the film became Fétiche Mascotte (The Mascot), about 600 meters, distributed in 1934. Starewitch made a contract with Marc Gelbart (Gelma Films) to make a series with this character. It was intended to make 12 episodes, but for economic reasons, only 5 were made between 1934 and 1937 and distributed in all the world. These are Fétiche prestidigitateur (The Ringmaster, 1934), Fétiche se marie (The Mascott's Wedding, 1935), Fétiche en voyage de noces (The Navigator, 1936) and Fétiche et les sirènes (The Mascott and the Mermaids, 1937) which was not released because sound could not be added. There is an unfinished film, Fétiche père de famille (The Mascott and His Family, 1938).
In 1954, L. Starewitch conceived The Hangover, using the images not included in The Mascot.  Just recently, Léona Béatrice Martin-Starewitch, his granddaughter, and her husband, François Martin, started the reconstruction of the original movie from multiple copies of "The Mascot" distributed in the United Kingdom and the United States of America, negative of The Hangover and material from the archives of Ladislas Starewitch. In 2012 LS 18 has found its length and montage from 1933. It was named Fetish 33-12.


== During and after World War II ==
During this period (1937–1946), Starewitch ceased his productions. He expressed some intent to make commercial films, but none are known to have been produced during the war.


== After World War II ==
In 1946 he tried to make The Midsummer Night's Dream but abandoned the project due to financial problems. Next year, he made Zanzabelle a Paris adapted from a story by Sonika Bo. In 1949, he met Alexandre Kamnka (Alkam Films), an old Russian friend, who produced Starewitch's first colour film Fleur de fougère (Fern Flower). It's based on an Eastern European story, in which a child goes to the forest to collect a fern flower, which grows during the night of Saint-Jean, and which makes wishes come true. In 1950, Fern Flower won the first prize as an animated film in the 11th International Children Film Festival in Venice Biennale. Then he started a collaboration with Sonika Bo to adapt another of her stories, "Gazouilly petit oiseau", followed by "Un dimanche de Gazouillis" (Gazouillis's Sunday picnic).
Again produced by Alkam films, Starewitch made Nose to Wind, which tells the adventures of Patapouf, a bear who escapes from school to play with his friends the rabbit and the fox. That year, his wife Anna died. Due to the success from the previous film, Winter Carousel was made, starring the bear Patapouf and the rabbit going through seasons. This was his last completed film. All his family co-labored on it, as remembers his granddaughter Léona Béatrice, whose hands can be seen in animation tests from Like Dog and Cat, Starewitch's unfinished film.
Ladislas Starewitch died on 26 February 1965, while working on Comme chien et chat (Like Dog and Cat). He was one of the few European animators to be known by name in the United States before the 1960s, largely on account of La Voix du rossignol and Fétiche Mascotte (The Tale of the Fox was not widely distributed in the US). His Russian films were known for their dark humor. He kept every puppet he made, so stars in one film tended to turn up as supporting characters in later works (the frogs from The Frogs Who Wanted a King are the oldest of these). For example, in Fétiche mascotte (1933) we can see puppets from The Scarecrow, The Little Parade, and The Magical Clock. The films are united, incredible imagination and development of techniques, like motion blur, replacement animation, multiple frame exposing, and reverse shooting.


== Posterity ==
Since 1991, Leona Beatrice Martin-Starewitch, Ladislas Starevich's granddaughter and her husband, François Martin, restore and distribute her grandfather's films.
Filmmaker Terry Gilliam ranks The Mascot among the ten best animated movies of all time.
In 2005, Xavier Kawa-Topor and Jean Rubak joined three Starewitch short films in a feature film, with music by Jean-Marie Senia. The film, entitled Tales of the Magical Clock, contributes to the recognition by the press and the public of Starewitch Engineering.
In 2009, Wes Anderson paid homage to Le Roman de Renard in Fantastic Mr. Fox.
In 2012, a new film by Ladislas Starevich was reconstituted Fetish 33-12. This is the original version of the film The Mascot, the 1933 film about 1000 m, but decreased by 600 m distributors by the time.
In 2014, the town of Fontenay-sous-Bois and service Documentation Archive with the family Martin-Starewich organized projections of Ladislas Starewich films in municipal Kosmos cinema with the release of all the preserved films, more than 7 hours on two projection days.


== Filmography ==


=== Films directed in Kaunas (1909–1910) ===
(with original titles in Polish)

Nad Niemnem (1909) – Beyond the River Nemunas
Zycie Ważek (1909) – The Life of the Dragonfly
Walka żuków (1909)  – The Battle of the Stag Beetles
Piękna Lukanida (1910) – The Beautiful Leukanida (the first puppet animation film)These films except for The Beautiful Leukanida are currently considered lost.


=== Films directed in Russia (1911–1918) ===
(with original titles in Russian)

Lucanus Cervus (1910) – Lucanus Cervus
Rozhdyestvo Obitatelei Lyesa (1911) – The Insects' Christmas
Aviacionnaya Nedelya Nasekomykh (1912) – Insects' Aviation Week
Strashnaia Myest (1912) – The Terrible Vengeance
Noch' Pered Rozhdestvom (1912) – The Night Before Christmas
Veselye Scenki Iz Zhizni Zhivotnykh (1912) – Amusing Scenes from the Life of Insects
Miest Kinomatograficheskovo Operatora (1912) – The Cameraman's Revenge
Puteshestvie Na Lunu (1912) – A Journey to the Moon
Ruslan I Ludmilla. (1913) – Ruslan and Ludmilla
Strekoza I Muravei (1913) – The Grasshopper and the Ant
Snegurochka. (1914) – The Snow Maiden
Pasynok Marsa (1914) – Mars’s Stepson
Kayser-Gogiel-Mogiel (1914) – Gogel-Mogel General
Troika (1914) – Troika
Fleurs Fanees 1914 – Faded Flowers
Le Chant Du Bagnard (1915) – The Convict's Song
Portret (1915) (May Be Produced By The Skobeliew Committee) – The Portrait
Liliya Bel'gii (1915) – The Lily of Belgium
Eto Tyebye Prinadlezhit (1915) – It’s Fine for You
Eros I Psyche (1915) – Eros and Psyche
Dvye Vstryechi (1916) – Two Meetings
Le Faune En Laisse (1916) – The Chained Faun
O Chom Shumielo Morie (1916) – The Murmuring Sea
Taman (1916) – Taman
Na Varshavskom Trakte (1916) – On the Warsaw Highway
Pan Twardowski (in Polish)(1917) – Mister Twardowski
Sashka-Naezdnik (1917) – Sashka the Horseman
K Narodnoi Vlasti (1917) – Towards People’s Power
Kaliostro (1918) – Cagliostro
Yola (1918) – Iola
Wij (1918) – Vij
Sorotchinskaia Yarmaka (1918) – The Sorotchninsk Fair
Maiskaya Noch (1918) – May Night
Stella Maris (1918) – Starfish


=== Films directed in France (1920–1965) ===
(with original titles in French)

Dans les Griffes de L'araignée (1920) – In The Claws of the Spider
Le Mariage de Babylas (1921) – Babylas’s Marriage
L’épouvantail (1921) – The Scarecrow
Les Grenouilles qui Demandent un Roi (1922) – Frogland
La Voix du Rossignol (1923) – The Voice of the Nightingale
Amour Noir et Blanc (1923) – Love In Black and White
La Petite Chanteuse des Rues (1924) – The Little Street Singer
Les Yeux du Dragon (1925) – The Eyes of the Dragon
Le Rat de Ville et le Rat Des Champs  (1926) – The Town Rat and the Country Rat
La Cigale et la Fourmi (1927) – The Ant and the Grasshopper
La Reine des Papillons (1927) – The Queen of the Butterflies
L'horloge Magique (1928) – The Magic Clock
La Petite Parade (1928) – The Little Parade
Le Lion et le Moucheron (1932) – The Lion and the Fly
Le Lion Devenu Vieux (1932) – The Old Lion
Fétiche Mascotte (1933) – The Mascot
Fétiche Prestidigitateur (1934) – The Ringmaster
Fétiche se Marie (1935) – The Mascot’s Marriage
Fétiche en Voyage De Noces (1936) – The Navigator
Fétiche Chez les Sirènes (1937) – The Mascot and the Mermaids
Le Roman de Renard (1930–1939) – The Tale of the Fox
Zanzabelle a Paris (1947) – Zanzabelle in Paris
Fleur de Fougère (1949) – Fern Flowers
Gazouilly Petit Oiseau. (1953) – Little Bird Gazouilly
Gueule de Bois (1954) – Hangover
Un Dimanche de Gazouilly (1955) – Gazouilly’s Sunday Picnic
Nez au Vent (1956) – Nose to the Wind
Carrousel Boréal (1958) – Winter Carousel
Comme Chien et Chat (1965) – Like Dog and CatA documentary about Starevich called The Bug Trainer was made in 2008.


== DVD Editions ==
Le monde magique de Ladislas Starewitch, Doriane Films, 2000.Content: The Old Lion, The Town Rat and the Country Rat (1932 sound version) The mascot and Fern Flowers.
Bonus: The Town Rat and the Country Rat (1926 silent version)

Le Roman de Renard(The Tale of the fox), Doriane Films, 2005.Bonus: The Navigator

Les Contes de l'horloge magique, Éditions Montparnasse, 2005.Content: The Little Street Singer, The Little Parade and The Magic Clock.

The Cameraman's Revenge and other fantastic tales, Milestone, Image Entertainment, 2005Content: The Cameraman's Revenge, The insect's christmas, The frogs who wanted a king (short version), The voice of the nightingale, The mascot and Winter Carrousel.

Les Fables de Starewitch d'aprés la Fontaine, Doriane Films, 2011.Content: The Lion and the Fly, The Town Rat and the Country Rat (1926), The frogs who wanted a king (original version), The Ant and the Grasshopper (1927 version), The Old Lion and Comment naît et s'anime une ciné-marionnette (How a Ciné marionette born and comes to life).
Bonus: The Old Lion (French narrated version) and The Town Rat and the Country Rat (1932 version)

Nina Star, Doriane Films, 2013.Content: The Sacarecrow, The Babylas's wedding, The voice of the nightingale, The Queen of the butterflies.
Bonus: The Babylas's wedding (tinted colours), The Queen of the butterflies (United Kingdom version), Comment naît et s'anime une ciné-marionnette.

L'homme des confins, Doriane Films, 2013.Content: In the spider's claws, The eyes of the dragon, Love black and white
Bonus: The eyes of the dragon (1932 sound version), Love black and white (1932 sound version), Comment naît et s'anime une ciné-marionnette

Fétiche 33-12, Doriane Films, 2013Bonus: The mascot, Gueule de bois, Comment naît et s'anime une ciné marionnette.


== Notes ==


== References ==
Donald Crafton; Before Mickey: The Animated Film, 1898–1928; University of Chicago Press; ISBN 0-226-11667-0 (2nd edition, paperback, 1993)
Giannalberto Bendazzi (Anna Taraboletti-Segre, translator); Cartoons: One Hundred Years of Cinema Animation; Indiana University Press; ISBN 0-253-20937-4 (reprint, paperback, 2001)
Liner notes to the DVD The Cameraman's Revenge and Other Fantastic Tales


== External links ==
Starewitch official homepage – made by his granddaughter
The Cameraman's Revenge can be viewed at the Internet Archive
Fétiche Mascotte (1934) at the Internet Archive
Ladislas Starewich Biography – part of "Animation Heaven and Hell", by Tim Fitzpatrick. Website also includes a few video clips.
Ladislas Starevich on IMDb
Entomology and Animation: A Portrait of an Early Master Ladislaw Starewicz (May 2000 6-page article from Animation World Magazine)
Starevich at UbuWeb (view some of his early films)
(in Spanish) Biography (with many pictures)
creative documentary "The Bug Trainer" about L. Starewitch
use in music videos on YouTube
El entrenador de insectos, Abril de 2014.