Narrow Street is a narrow road running parallel to the River Thames through the Limehouse area of east London, England. It used to be much narrower, and is the oldest part of Limehouse, with many buildings originating from the eighteenth century.


== History ==
Most archaeologists believe that Narrow Street represents the line of the medieval river wall.  The eastern end of Narrow Street was previously known as Fore Street.A combination of tides and currents made this point on the Thames a natural landfall for ships, the first wharf being completed in 1348. Lime kilns or oasts ("lymehostes") used in the production of mortar and pottery were built here in the fourteenth century.
The area grew rapidly in Elizabethan times as a centre for world trade and by the reign of James I nearly half of the area's 2,000 population were mariners. The area supplied ships with ropes and other necessities; pottery was also made here for the ships. Ship chandlers settled here building wooden houses and wharves in the cramped space between street and river. Narrow Street may take its name from the closeness of the original buildings.  A 1865 report to Parliament noted that no part of Narrow Street was wider than 25 feet.The Limehouse Cut for barges, which ran under Narrow Street and led to the Lee Navigation, was established in 1766. Limehouse Basin was built in 1820, to transship goods to barges on the Regent's Canal.
In 1661, Samuel Pepys wrote in his diary of a visit to a porcelain factory in Narrow Street alighting via Duke Shore Stairs while en route to view work on boats being built for herring fishing. The Limehouse area fitted out, repaired and resupplied ships. In 1772, Smith & Sykes ran a sugar house, a small factory that baked and refined sugar.Limehouse Cut was redirected into Limehouse Basin, which was one of the first docks to close in the late 1960s. Nicholas Hawksmoors' Church St Anne's Limehouse was designated a conservation area by the London Docklands Development Corporation in the 1980s.
For much of the 20th century the area was dominated by the tall chimney of Stepney Power Station at Blyth Wharf, which has since been demolished.
Access to the area was always difficult, with the dock standing to the north, and the entrance to the Rotherhithe Tunnel at one end. In 1993 the 1.8 kilometres (1.1 mi) Limehouse Link tunnel was completed, further restricting traffic to the riverside area. The Narrow Street Swing Bridge is sited between the Limehouse Basin Lock and the Thames.
In 1977 the north side of the street was demolished.  Until then it was half its present width.


== Chinatown ==
In the eighteenth century a small group of Chinese sailors from Canton and Southern China settled along the old Limehouse Causeway creating the original London Chinatown. The Chinese community later moved to Soho following the Blitz.


== Historic buildings ==

On the south side of Narrow Street is a rare example of an early Georgian brick terrace. With the exception of the westernmost property (The Grapes public house) it was standing derelict and abandoned, but in 1964 the writer Andrew Sinclair bought and saved one of the houses and persuaded his Cambridge friends to buy the others. (Early Georgian houses can be distinguished from late ones in the way that the windows are not set back from the brick frontage.)  The Grapes (formerly The Bunch of Grapes, and known to the young Charles Dickens) was notably bought in 2011 by actor Sir Ian McKellen, director Sean Mathias and Evening Standard owner Evgeny Lebedev.


== Redevelopment ==
The late twentieth century brought much development to the area, with the erection of the Canary Wharf tower close by. Since the 1990s, many new apartment complexes have been built around the Limehouse Basin as well as Victorian warehouse conversions, with Limehouse now being one of the most sought after property sites in London. Its close proximity to the River Thames has made property prices around Limehouse and the Docklands soar over the last decade. However a 2001 Census listed 5.4 percent of homes in Poplar and Limehouse as being without central heating and/or private bathroom.The street is home to a number of pubs and restaurants, including The Narrow, a gastropub run by Gordon Ramsay. Booty's Riverside Bar, which closed in 2013, was an independently owned pub dating to the 16th century. In the 18th century Booty's was an engineering shop for the barge builders, Sparkes. In the 19th century it was re-fronted, and by the 1870s it was a licensed bar called The Waterman's Arms owned by Taylor Walker. It was absorbed by Woodward Fisher, a lighterage firm run by Anne Fisher, popularly known as Tugboat Annie. She was a real-life London version of the protagonist in the film of that name. One of the great East End characters, she commanded a fleet of 200 barges.


== Residents ==
Famous residents include or have included the poet Ernest Dowson, actors Ian McKellen, Steven Berkoff and Cleo Rocos, the politician Lord Owen, the authors Matthew Parris and Andrew Sinclair and Eagle founder and deputy Chairman of the National Magazine Company Marcus Morris OBE. It was also the home of the film director David Lean.


== Art and literature ==
Its picturesque buildings and atmospheric location abutting onto the River Thames attracted artists and writers.

Charles Dickens' godfather Christopher Huffam ran his sail-making business from Newell Street,  Limehouse.
James McNeill Whistler and Charles Napier Hemy sketched and painted at locations on Narrow Street's river waterfront.


== Transport ==
Cycle Superhighway 3 CS3 between Tower Gateway to Barking and is one of London's first Cycle Superhighways. Some residents raised a petition calling for CS3 to be moved from Narrow Street onto the A13 Commercial Road, arguing that the street was too narrow and that incidents of abuse and aggression had risen sharply since the route was introduced. Transport for London (TfL) said they had no plans to move it. Mayor Boris Johnson defended the choice as a road that was already popular with cyclists to and from the City. The London Cycling Campaign supported the route but called for improvements. In 2011, TfL agreed to remove logos from the road surface, but not to change the route.The National Trail Thames Path for walkers runs along Narrow Street and it is also included in the London Marathon course.
Docklands Light Railway stations are Limehouse (for National Rail as well) and Westferry.
The London River Services pier is Canary Wharf Pier.


== See also ==

Henry Mayhew
George Robert Sims
Sir Walter Besant
Clement Attlee
Colin Gwyer & Associates Ltd v London Wharf (Limehouse) Ltd [2003] BCC 885, litigation which took place on a Narrow Street property
Stepney Historical Trust


== Notes ==

The Anglo-Saxon word tirl, means 'narrow street' or a 'gate' to keep horses and other cattle out of the city.


== External links ==
LDDC
Chinatown
Taylor Walker & Co http://www.quaffale.org.uk/php/brewery/746
Limehouse https://web.archive.org/web/20051226021110/http://www.eolfhs.org.uk/parish/limehouse.htm
Early history http://www.mernick.co.uk/thhol/limehouse.html
Early history https://web.archive.org/web/20051212093541/http://www.towerhamlets.gov.uk/data/discover/data/Poplar/index.cfm
Duke Shore stairs http://www.burkes-peerage.net/sites/common/sitepages/lwallindex.asp
Image reference: Greenwood's Map of London 1827 https://web.archive.org/web/20110406112050/http://users.bathspa.ac.uk/greenwood/imagemap.html
Image reference: Limehouse Link 1993