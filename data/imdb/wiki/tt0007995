The Gentle Intruder is a 1917 American silent drama film directed by James Kirkwood. The film was Mary Miles Minter's sixth production with Mutual Film. The cast also includes George Fisher, Eugenie Forde, Harvey Clark, Franklin Ritchie and George Periolat.
A copy of the film is held by the Dutch Filmmuseum.


== Plot ==
Sylvia's uncle leaves her a fortune. However, his lawyer, Mr. Baxter, uses the money to support himself and his wife. Sylvia visits him, but isn't wanted there. She falls in love with his son Arnold, who has a drinking and gambling problem. While she is trying to get him straightened up, Arnold finds out that his parents are keeping Sylvia's money for themselves.


== Production ==
The December 20, 1916 issue of Reel Life had a news item describing an element of the film's production: "George Periolat, who plays an important role in the first scenes of The Gentle Intruder, the new Mary Miles Minter feature being directed by James Kirkwood, is having an easy time this week at the Santa Barbara studio of the American company. All he has to do is to lie in bed all day while the camera records his failing health and death. After three or four days of it, however, Periolat says he doesn't wish to see a bed for a week."In the February 3, 1917 issue, Reel Life also observed that The Gentle Intruder was Minter's first film "with her hair done up."


== Reception ==
The Moving Picture World said on February 24, 1917 that the film "will be a fit companion piece to the preceding pictures in which the young American star [Minter] has appeared, and affords her a wide opportunity in a difficult role."Motion Picture News said on March 3, 1917 that the film "is a typical Mary Miles Minter-Cinderella production. By this we mean that the diminutive star has the same tribulations that she has encountered so many times in the celluloid... These plot high-lights have been incorporated into nearly every picture starring Miss Minter. They will probably continue to form the foundation of Minter releases, because these circumstances are ideal for displaying the star's youth, wistfulness, and engaging mannerisms. The public would probably resent seeing Miss Minter in a different kind of role, just as it wants Maude Adams to play nothing but Peter Pan."Proving the Motion Picture News critic correct, the Dayton, Ohio Journal review objected to Minter's hairstyle: "Give us back our Mary. In The Gentle Intruder, Mary is no longer the laughing care-free child. She has her hair done up. Lord! fancy Mary Miles with done up hair. We refuse to stand for it. Of course Mary is gentle and wistful and awfully nice, but we want her with her hair down."


== Cast ==
Mary Miles Minter - Sylvia
George Fisher - Arnold Baxter
Eugenie Forde - Mrs. Baxter
Harvey Clark - Mr. Baxter
Franklin Ritchie - The Count
George Periolat - Sylvia's uncle


== References ==


== External links ==
The Gentle Intruder on IMDb