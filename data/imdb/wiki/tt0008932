Brown of Harvard is a 1926 American silent drama film directed by Jack Conway, and starring William Haines, Jack Pickford and Mary Brian. Released by Metro-Goldwyn-Mayer, the film is based on the successful 1906 Broadway play Brown of Harvard by Rida Johnson Young, who also co-wrote the popular music for the play along with Melvin Ellis. The film is best known of the three Brown of Harvard films. It was John Wayne's film debut. Uncredited, Wayne played a Yale football player. Grady Sutton and Robert Livingston, both of whom went on to long and successful careers, also appear uncredited. The 1918 film included future Boston Redskins coach William "Lone Star" Dietz and the only Washington State University football team to win a Rose Bowl.


== Plot ==
Harvard University student Tom Brown (William Haines) is a handsome, athletic, and carefree young man who has a reputation as a Don Juan among the ladies. Although he is popular on campus, he finds himself at odds with Bob McAndrew (Ralph Bushman), a studious, reserved boy who becomes his chief rival for the affections of beautiful Mary Abbott (Mary Brian), a professor's daughter. Tom rooms with Jim Doolittle (Jack Pickford), an awkward weakling but goodhearted backwoods youth who idolizes him. The brash and cocky Brown easily wins over his dormitory mates, but refuses to let them ostracize Jim.
One night at a party, Tom forcibly kisses Mary, which initiates a fight with Bob. Afterwards, Tom challenges Bob to a rowing competition; Bob is stroker on the college rowing team. Tom ends up losing. When he forces a confession of love from Mary, he begins to drink in shame. When he replaces Bob in a match against Yale, Tom collapses and is disgraced. He is persuaded by his father to go out for football.
To save his friend's reputation, the sickly Jim goes out and takes his place in the rain and is soon hospitalized. Tom plays in the game against Yale and at a crucial point gives Bob a chance to score for the team. After the game, Tom goes to the hospital to tell Jim of the victory, but Jim dies shortly afterward. Tom is acclaimed a school hero and is happily united with Mary.


== Cast ==

William Haines as Tom Brown
Jack Pickford as Jim Doolittle
Mary Brian as Mary Abbott
Francis X. Bushman Jr. as Bob McAndrew
Mary Alden as Mrs. Brown
David Torrence as Mr. Brown
Edward Connelly as Professor Abbott
Guinn Williams as Hal Walters
Donald Reed as Reggie Smythe
Richard Alexander as Football Fan (uncredited)
Robert Livingston as Harvard Student / Yale Cheering Section / Harvard Spectator (uncredited)
Doris Lloyd as Nurse (uncredited)
Grady Sutton as One of the Dickeys (uncredited)
Daniel G. Tomlinson as Football Trainer (uncredited)
John Wayne as a Yale College football player (uncredited)


== References ==


== External links ==
Brown of Harvard on IMDb
Brown of Harvard at AllMovie
Brown of Harvard at the TCM Movie Database