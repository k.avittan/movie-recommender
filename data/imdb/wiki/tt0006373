Arsène Lupin is a fictional gentleman thief and master of disguise created in 1905 by French writer Maurice Leblanc. He was originally called Arsène Lopin, until a local politician of the same name protested. The character was first introduced in a series of short stories serialized in the magazine Je sais tout. The first story, "The Arrest of Arsène Lupin", was published on 15 July 1905.
Lupin was featured in 17 novels and 39 novellas by Leblanc, with the novellas or short stories collected into book form for a total of 24 books. The number becomes 25 if the 1923 novel The Secret Tomb  is counted: Lupin doesn't appear in it, but the main character Dorothée solves one of Arsène Lupin's four fabulous secrets.
The character has also appeared in a number of books from other writers as well as numerous film, television, stage play, and comic book adaptations. Five authorized sequels were written in the 1970s by the celebrated mystery writing team of Boileau-Narcejac.


== Origins ==
Arsène Lupin is a literary descendant of Pierre Alexis Ponson du Terrail's Rocambole, whose adventures were published from 1857 to 1870. Like him, he is often a force for good, while operating on the wrong side of the law. Those whom Lupin defeats, always with his characteristic Gallic style and panache, are worse villains than he is.  Lupin shares distinct similarities with E. W. Hornung's archetypal gentleman thief A. J. Raffles, whose stories were published from 1898 to 1909. Both Raffles and Lupin can be said to anticipate and have inspired later characters such as Louis Joseph Vance's The Lone Wolf (created in 1914) and Leslie Charteris's The Saint (created in 1928).
The character of Arsène Lupin might also have been based by Leblanc on French anarchist Marius Jacob (1879–1954), whose trial made headlines in March 1905, but Leblanc had also read Octave Mirbeau's Les 21 jours d'un neurasthénique (1901), which features a gentleman thief named Arthur Lebeau, and had seen Mirbeau's comedy Scrupules (1902), whose main character is a gentleman thief.


== Fantasy elements ==
Several Arsène Lupin novels contain some fantasy elements: a radioactive 'god-stone' that cures people and causes mutations is the object of an epic battle in L’Île aux trente cercueils; the secret of the Fountain of Youth, a mineral water source hidden beneath a lake in the Auvergne, is the goal sought by the protagonists in La Demoiselle aux yeux verts; finally, in La Comtesse de Cagliostro, Lupin's arch-enemy and lover is none other than Joséphine Balsamo, the alleged granddaughter of Cagliostro himself.


== Arsène Lupin and Sherlock Holmes ==

Leblanc introduced Sherlock Holmes to Lupin in the short story "Sherlock Holmes Arrives Too Late" in Je sais tout No. 17, 15 June 1906. In it, an aged Holmes meets a young Lupin for the first time. After legal objections from Doyle, the name was changed to "Herlock Sholmes" when the story was collected in book form in Volume 1.
Sholmes returned in two more stories collected in Volume 2, "Arsène Lupin contre Herlock Sholmes", and then in a guest-starring role in the battle for the secret of the Hollow Needle in L'Aiguille creuse. Arsène Lupin contre Herlock Sholmes was published in the United States in 1910 under the title "The Blonde Lady" which used the name "Holmlock Shears" for Sherlock Holmes, and "Wilson" for Watson.
In 813, Lupin manages to solve a riddle that Herlock Sholmes was unable to figure out.
Sherlock Holmes, this time with his real name and accompanied by familiar characters such as Watson and Lestrade (all copyright protection having long expired), also confronted Arsène Lupin in the 2008 PC 3D adventure game Sherlock Holmes Versus Arsène Lupin. In this game Holmes (and occasionally others) are attempting to stop Lupin from stealing five valuable British items. Lupin wants to steal the items in order to humiliate Britain, but he also admires Holmes and thus challenges him to try to stop him.
In a novella "The Prisoner of the Tower, or A Short But Beautiful Journey of Three Wise Men" by Boris Akunin published in 2008 in Russia as the conclusion of "Jade Rosary Beads" book, Sherlock Holmes and Erast Fandorin oppose Arsène Lupin on December 31, 1899.


== Bibliography ==
Arsène Lupin, Gentleman Burglar (Arsène Lupin, gentleman cambrioleur, 1907 coll., 9 novellas) (AKA: Exploits of Arsène Lupin, Extraordinary Adventures of Arsène Lupin)
Arsène Lupin vs. Herlock Sholmes (Arsène Lupin contre Herlock Sholmès, 1908 coll., 2 stories) (AKA: The Blonde Lady)
The Hollow Needle (L'Aiguille creuse, 1909, novel)
813 (813, 1910, novel)
The Crystal Stopper (Le Bouchon de cristal, 1912, novel)
The Confessions of Arsène Lupin (Les Confidences d'Arsène Lupin, 1913 coll., 9 novellas)
The Teeth of The Tiger (Les Dents du tigre, 1914, novel) Published in English in 1914, but remained unpublished in French until 1920.
The Shell Shard (L'Éclat d'obus, 1916, novel) (AKA: Woman of Mystery) Not originally part of the Arsène Lupin series, Lupin was written into the story in the 1923 edition.
The Golden Triangle (Le Triangle d'or, 1918, novel) (AKA: The Return of Arsène Lupin)
The Island of Thirty Coffins (L’Île aux trente cercueils, 1919, novel) (AKA: The Secret of Sarek)
The Eight Strokes of The Clock (Les Huit Coups de l'horloge, 1922 coll., 8 novellas)
The Secret Tomb (Dorothée, Danseuse de Corde, 1923. The main character Dorothée solves one of Arsène Lupin's four fabulous secrets.
The Countess of Cagliostro (La Comtesse de Cagliostro, 1924, novel) (AKA: Memoirs of Arsène Lupin) Published in English in 1925.
The Overcoat of Arsène Lupin (Le Pardessus d'Arsène Lupin, published in English in 1926) Novella first published in 1924 in France as La Dent d'Hercule Petitgris. Altered into a Lupin story and published in English as The Overcoat of Arsène Lupin in 1926 in The Popular Magazine
The Damsel With Green Eyes (La Demoiselle aux yeux verts, 1927, novel) (AKA: The Girl With the Green Eyes, Arsène Lupin, Super Sleuth)
The Man with the Goatskin (L'Homme à la peau de bique (1927, novella)
The Barnett & Co. Agency (L'Agence Barnett et Cie., 1928 coll., 8 novellas) (AKA: Jim Barnett Intervenes, Arsène Lupin Intervenes) The English edition includes The Bridge That Broke story, which was unpublished in France at the time.
The Mysterious Mansion (La Demeure mystérieuse, 1929, novel) (AKA: The Melamare Mystery)
The Emerald Cabochon (Le Cabochon d'émeraude (1930, novella)
The Mystery of The Green Ruby (La Barre-y-va, 1931, novel)
The Woman With Two Smiles (La Femme aux deux sourires, 1933, novel) (AKA: The Double Smile)
Victor of the Vice Squad (Victor de la Brigade mondaine, 1933, novel) (AKA: The Return of Arsène Lupin)
The Revenge of The Countess of Cagliostro (La Cagliostro se venge, 1935, novel)
The Billions of Arsène Lupin (Les Milliards d'Arsène Lupin, 1939/1941, novel) - The official last book of the series, The Billions of Arsène Lupin, was serialised in 1939 and published posthumously as a book in 1941 - yet without the ninth chapter "The Safe" ("IX. Les coffres-forts"). This edition was later withdrawn at the request of Leblanc's son. In 2002, through the efforts of some Lupinians and Korean translator Seong Gwi-Soo, the missing chapter was restored and the complete final Lupin novel published in Korea by Kachi Publishing House. A complete French e-book is now also available, as well as a printed edition by Editions Manucius (2015).
The Last Love of Arsène Lupin (Le Dernier Amour d'Arsène Lupin, novel), written around 1936 and posthumously published in 2012 after being found by chance in 2011 "on top of a cupboard in a beige shirt with rusty hooks" by Florence Boespflug-Leblanc.


=== Plays written by Leblanc ===
Arsène Lupin Originally a 4-part play written by Maurice Leblanc and Francis de Croisset (1908), it was subsequently novelized by Edgar Jepson and published in 1909 by Doubleday as "Arsène Lupin: By Edgar Jepson"
An Adventure of Arsène Lupin (1911)
The Return of Arsène Lupin (1920) Written by Maurice Leblanc and Francis de Croisset.
This Woman is Mine (Cette femme est à moi, (1930)
A Quarter-hour with Arsène Lupin (Un quart d'heure avec Arsène Lupin, 1932)


== Stories by other writers ==
"The Adventure of Mona Lisa" by Carolyn Wells in The Century (January, 1912), a short parody featuring an "International Society of Infallible Detectives" with Sherlock Holmes as the president and Arsène Lupin, The Thinking Machine, Monsieur Lecoq, A. J. Raffles, C. Auguste Dupin and Luther Trant as the other members.
Sure Way to Catch Every Criminal. Ha! Ha! by Carolyn Wells in The Century (July, 1912)
The Adventure of the Clothes-Line by Carolyn Wells in The Century (May, 1915)
The Silver Hair Crime (= Clue?) by Nick Carter in New Magnet Library No. 1282 (1930)
Ōgon-kamen (The Golden Mask) by Edogawa Rampo (1930). Here Rampo's recurring private sleuth Kogoro Akechi would match wits with Lupin, where the thief plays a central role as the Golden Mask.
La Clé est sous le paillasson by Marcel Aymé (1934)
Gaspard Zemba who appears in The Shadow Magazine (December 1, 1935) by Walter B. Gibson
Arsène Lupin vs. Colonel Linnaus by Anthony Boucher in Ellery Queen's Mystery Magazine Vo. 5, No. 19 (1944)
L’Affaire Oliveira by Thomas Narcejac in Confidences dans ma nuit (1946)
Le Gentleman en Noir by Claude Ferny (c. 1950) (two novels)
International Investigators, Inc. by Edward G. Ashton in Ellery Queen’s Mystery Magazine (February 1952)
Le Secret des rois de France ou La Véritable identité d’Arsène Lupin by Valère Catogan (1955)
In Compartment 813 by Arthur Porges in Ellery Queen’s Mystery Magazine (June 1966)
Le Secret d’Eunerville (1973) by the writing duo Boileau-Narcejac
La Poudrière (1974) by the writing duo Boileau-Narcejac
Le Second visage d’Arsène Lupin (1975) by the writing duo Boileau-Narcejac
La Justice d’Arsène Lupin (1977) by the writing duo Boileau-Narcejac
Le Serment d’Arsène Lupin (1979) by the writing duo Boileau-Narcejac
Arsène Lupin, gentleman de la nuit by Jean-Claude Lamy (1983)
Various stories in the Tales of the Shadowmen anthology series, ed. by Jean-Marc Lofficier and Randy Lofficier, Black Coat Press (2005-ongoing)
Případ Grendwal (A Grendwal Case), a play by Pavel Dostál, Czech playwright and Minister of Culture
Arsène Lupin et le mystère d'Arsonval by Michel Zink
Qui fait peur à Virginia Woolf ? (... Élémentaire mon cher Lupin !) by Gabriel Thoveron
Crimes parfaits by Christian Poslaniec
La Dent de Jane by Daniel Salmon (2001)
Les Lupins de Vincent by Caroline Cayol et Didier Cayol (2006)
Code Lupin by Michel Bussi (2006)
The Prisoner of the Tower, or A Short But Beautiful Journey of Three Wise Men (Узница башни, или Краткий, но прекрасный путь трёх мудрых) by Boris Akunin in a selection of stories The Jade Beads (Нефритовые чётки) (2006, in Russian). Arsène Lupin appears in this novella with Sherlock Holmes, Dr. Watson, and Akunin's own characters Erast Fandorin and Masa, the Japanese. The story is dedicated to Sir Arthur Conan Doyle and Maurice Leblanc.
L'Église creuse by Patrick Genevaux (2009) (short story)
The Many Faces of Arsène Lupin collection of short stories edited by Jean-Marc Lofficier & Randy Lofficier (Black Coat Press, 2012)
Sherlock, Lupin et Moi, a children's book series written by Italian author Alessandro Gatti, where Irene Adler tells the adventures that she, Sherlock Holmes and Arsène Lupin had when they were kids. The books are written under the pseudonym Irene Adler. Five books have been published so far : Le Mystère de la Dame en Noir, Dernier Acte à l'Opéra, L'Énigme de la Rose Écarlate, La Cathédrale de la Peur, and LeChâteau de Glace.


== In other media ==


=== Films ===
The Gentleman Burglar (B&W., US, 1908) with William Ranows (Lupin).
Arsène Lupin contra Sherlock Holmes (B&W., Germany, 1910) with Paul Otto (Lupin).
Arsène Lupin (B&W., France, 1914) with Georges Tréville (Lupin).
The Gentleman Burglar (B&W., US, 1915) with William Stowell (Lupin).
Arsène Lupin (B&W., UK, 1916) with Gerald Ames (Lupin).
Arsène Lupin (B&W., US, 1917) with Earle Williams (Lupin).
The Teeth of the Tiger (B&W., US, 1919) with David Powell (Lupin).
813 (B&W., US, 1920) with Wedgwood Nowell (Lupin) and Wallace Beery.
Les Dernières aventures d'Arsène Lupin (B&W., France/Hungary, 1921).
813 - Rupimono (B&W., Japan, 1923) with Minami Mitsuaki (Lupin).
Arsène Lupin (B&W., US, 1932) with John Barrymore (Lupin).
Arsene Lupin, Detective (Arsène Lupin détective, B&W., France, 1937) with Jules Berry (Lupin).
Arsène Lupin Returns (B&W., US, 1938) with Melvyn Douglas (Lupin).
Enter Arsène Lupin (B&W., US, 1944) with Charles Korvin (Lupin).
Arsenio Lupin (B&W., Mexico, 1945) with R. Pereda (Lupin).
Nanatsu-no Houseki (B&W., Japan, 1950) with Keiji Sada (Lupin).
Tora no-Kiba (B&W., Japan, 1951) with Ken Uehara (Lupin).
Kao-no Nai Otoko (B&W., Japan, 1955) with Eiji Okada (Lupin).
The Adventures of Arsène Lupin (Les Aventures d'Arsène Lupin, col., France, 1957) with Robert Lamoureux (Lupin).
Signé Arsène Lupin (B&W., France, 1959) with Robert Lamoureux (Lupin).
Arsène Lupin contre Arsène Lupin (B&W., France, 1962) with Jean-Pierre Cassel and Jean-Claude Brialy (Lupins).
Arsène Lupin (col., France, 2004) with Romain Duris (Lupin).
Lupin no Kiganjo (col., Japan, 2011) with Kōichi Yamadera (Lupin).


=== Television ===
Arsène Lupin, 26 60-minute episodes (1971, 1973–1974) with Georges Descrières (Lupin)
L'Île aux trente cercueils, six 60-minute episodes (1979) The character of Lupin, who only appears at the end of the novel, was removed entirely.
Arsène Lupin joue et perd, six 52-minute episodes (1980) loosely based on 813 with Jean-Claude Brialy (Lupin).
Le Retour d'Arsène Lupin, twelve 90-minute episodes (1989–1990) and Les Nouveaux Exploits d'Arsène Lupin, eight 90-minute episodes (1995–1996) with François Dunoyer (Lupin).
Les Exploits d'Arsène Lupin (also known as Night Hood), 26 episodes for 24 min. (1996), produced by Cinar & France-Animation, with Luis de Cespedes (Lupin).
Lupin, Philippine series (2007) with Richard Gutierrez (Lupin).
The 2014 movie Kamen Rider × Kamen Rider Drive & Gaim: Movie War Full Throttle features a character based on Lupin named Kamen Rider Lupin.
Code: Realize ~Guardian of Rebirth~, anime television series (2017) based on the video game, produced by the studio M.S.C, with Tomoaki Maeno and J. Michael Tatum (Lupin in Japanese and English, respectively).
The 2018 42nd Season Super Sentai series Kaitou Sentai Lupinranger VS Keisatsu Sentai Patranger features two teams, one of which is the Lupinrangers. Consisting of three members,  they are recruited by Kogure, Arsène's butler, and  executor of  Arsène Lupin's will, to retrieve the Lupin Collection, Arsène's personal collection of the most dangerous artifacts he ever stole, with the understanding that if they collect them all, they will be granted a single wish, to retrieve the people they care most about from the clutches of the Gangler, (later on they are joined by part-time Phantom Thief Noel Takao, the adopted son of Arsène Lupin, who seeks to destroy the Don of the Gangler, and bring together all the pieces of the collection to bring his father back, since when the Ganglers broke in they murdered Arsène and stole most of the pieces.)
The 2019 Girls X Heroine Series Secret × Heroine Phantomirage! features playing card suits and phantom thief which is the Phantomirage's main motifs.
The Netflix series Lupin is scheduled to be released in 2021. It stars Omar Sy as a man who receives a book about Arsène Lupin, which grants him resources and wealth, and multiple lives in which to spend them.


=== Stage ===
 Arsène Lupin by Francis de Croisset and Maurice Leblanc. Four-act play first performed on October 28, 1908, at the Athenée in Paris.
Arsène Lupin contre Herlock Sholmès by Victor Darlay & Henry de Gorsse. Four-act play first performed on October 10, 1910, at the Théâtre du Châtelet in Paris. (American edition ISBN 1-932983-16-3)
Le Retour d'Arsène Lupin by Francis de Croisset and Maurice Leblanc. One-act play first performed on September 16, 1911, at the Théâtre de la Cigale in Paris.
Arsène Lupin, Banquier by Yves Mirande & Albert Willemetz, libretto by Marcel Lattès. Three-act operetta, first performed on May 7, 1930, at the Théâtre des Bouffes Parisiennes in Paris.
A/L The Youth of Phantom Thief Lupin by Yoshimasa Saitou . Takarazuka Revue performance, 2007, starring Yūga Yamato and Hana Hizuki.
Rupan －ARSÈNE LUPIN－ by Haruhiko Masatsuka . Takarazuka Revue performance, 2013, starring Masaki Ryū and Reika Manaki (after Le Dernier Amour d'Arsène Lupin)


=== Comics ===
Arsène Lupin, written by Georges Cheylard, art by Bourdin. Daily strip published in France-Soir in 1948-49.
Arsène Lupin, written & drawn by Jacques Blondeau. 575 daily strips published in Le Parisien Libéré from 1956-58.
Arsène Lupin contre Herlock Sholmès: La Dame blonde, written by Joëlle Gilles, art by Gilles & B. Cado, published by the authors, 1983.
Arsène Lupin, written by André-Paul Duchateau, artist Géron, published by C. Lefrancq.
Le Bouchon de cristal (1989)
813 — La Double Vie d'Arsène Lupin (1990)
813 — Les Trois crimes d'Arsène Lupin (1991)
La Demoiselle aux yeux verts (1992)
L'Aiguille creuse (1994)
Lupin the Third is a Japanese manga series written and illustrated by Monkey Punch that follows the escapades of master thief Arsène Lupin III, the grandson of Arsène Lupin.
In Alan Moore's The League of Extraordinary Gentlemen: Black Dossier, Lupin is featured as a member of Les Hommes Mysterieux, the French analogue of Britain's League of Extraordinary Gentlemen.
The manga series Soul Eater features a thief character named Lupin in chapter three, which is an obvious reference to Arsène Lupin.
There is a  manga adaptation of Arsène Lupin first published in 2011, from Gundam artist Takashi Morita.


=== Video games ===
Sherlock Holmes Versus Arsène Lupin (known in North America and some parts of England as Sherlock Holmes: Nemesis) is an adventure game for Windows-compatible computers. It was developed by the game development studio Frogwares, and released in October, 2007. The game follows Holmes and Watson as Holmes is challenged by the legendary gentleman thief Arsène Lupin, who threatens to steal England's most-prized treasures.
Persona 5 and Persona 5 Royal feature beings known as Personas that are the manifestation of their owners' rebellious spirit are inspired by Fictional characters and mythological beings. The protagonist utilizes Arsène as his initial persona and also resides in the attic of a café named Leblanc, a reference to Maurice Leblanc. Similarly to Arsène, the protagonist is also a phantom thief who fights for good on the wrong side of the law. In Royal, new Personas are available for the main cast, and the protagonist gains access to a different version of the Arsène Persona named Raoul, based of one of Lupin's most common aliases.
Code: Realize ~Guardian of Rebirth~, an otome game for the PlayStation Vita, features several fictional characters as potential romantic interests for the player character. The main male character is a gentleman thief named Arsène Lupin.


== References ==


== External links ==
Arsène Lupin eBooks at Project Gutenberg
Works by or about Arsène Lupin at Internet Archive
Works by Maurice Leblanc at LibriVox (public domain audiobooks) 
Arsène Lupin at Cool French Comics