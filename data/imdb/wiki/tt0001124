In Central European folklore, Krampus is a horned, anthropomorphic figure described as "half-goat, half-demon", who, during the Christmas season, punishes children who have misbehaved. This contrasts with Saint Nicholas, who rewards the well-behaved with gifts. Krampus is one of the companions of Saint Nicholas in several regions including Austria, Bavaria, Croatia, Hungary, Northern Italy including South Tyrol and the Trentino, and Slovenia. The origin of the figure is unclear; some folklorists and anthropologists have postulated it as having pre-Christian origins.
In traditional parades and in such events as the Krampuslauf (English: Krampus run), young men dressed as Krampus participate. Such events occur annually in most Alpine towns. Krampus is featured on holiday greeting cards called Krampuskarten.


== Origins ==

The history of the Krampus figure has been theorized as stretching back to pre-Christian Alpine traditions. In a brief article discussing the figure, published in 1958, Maurice Bruce wrote:

There seems to be little doubt as to his true identity for, in no other form is the full regalia of the Horned God of the Witches so well preserved. The birch – apart from its phallic significance – may have a connection with the initiation rites of certain witch-covens; rites which entailed binding and scourging as a form of mock-death. The chains could have been introduced in a Christian attempt to 'bind the Devil' but again they could be a remnant of pagan initiation rites.
Discussing his observations in 1975 while in Irdning, a small town in Styria, anthropologist John J. Honigmann wrote that:

The Saint Nicholas festival we are describing incorporates cultural elements widely distributed in Europe, in some cases going back to pre-Christian times. Nicholas himself became popular in Germany around the eleventh century. The feast dedicated to this patron of children is only one winter occasion in which children are the objects of special attention, others being Martinmas, the Feast of the Holy Innocents, and New Year's Day. Masked devils acting boisterously and making nuisances of themselves are known in Germany since at least the sixteenth century while animal masked devils combining dreadful-comic (schauriglustig) antics appeared in Medieval church plays. A large literature, much of it by European folklorists, bears on these subjects. ...
Austrians in the community we studied are quite aware of "heathen" elements being blended with Christian elements in the Saint Nicholas customs and in other traditional winter ceremonies. They believe Krampus derives from a pagan supernatural who was assimilated to the Christian devil.
The Krampus figures persisted, and by the 17th century Krampus had been incorporated into Christian winter celebrations by pairing Krampus with St. Nicholas.Countries of the former Habsburg Empire have largely borrowed the tradition of Krampus accompanying St. Nicholas on 5 December from Austria.
In recent years, the myth that the Krampus was the son of Hel, Norse goddess of the underworld, has been popularised on the internet, even appearing in articles in National Geographic and Smithsonian Magazine. However, this connection is likely the invention of the American fantasy artist and author Gerald Brom, whose 2012 novel Krampus the Yule Lord features Krampus as the main protagonist. The same idea also appeared shortly afterwards in two online games by the Norwegian games producer Funcom.


== Modern history ==
In the aftermath of the 1932 election in Austria, the Krampus tradition was prohibited by the Dollfuss regime under the Fatherland's Front (Vaterländische Front) and the Christian Social Party. In the 1950s, the government distributed pamphlets titled "Krampus Is an Evil Man". Towards the end of the century, a popular resurgence of Krampus celebrations occurred and continues today.The Krampus tradition is being revived in Bavaria as well, along with a local artistic tradition of hand-carved wooden masks. In 2019 there were reports of drunken or disorderly conduct by masked Krampuses in some Austrian towns.


== Appearance ==

Although Krampus appears in many variations, most share some common physical characteristics. He is hairy, usually brown or black, and has the cloven hooves and horns of a goat. His long, pointed tongue lolls out, and he has fangs.Krampus carries chains, thought to symbolize the binding of the Devil by the Christian Church. He thrashes the chains for dramatic effect. The chains are sometimes accompanied with bells of various sizes. Of more pagan origins are the Ruten, bundles of birch branches that Krampus carries and with which he occasionally swats children. The Ruten may have had significance in pre-Christian pagan initiation rites. The birch branches are replaced with a whip in some representations. Sometimes Krampus appears with a sack or a basket strapped to his back; this is to cart off evil children for drowning, eating, or transport to Hell. Some of the older versions make mention of naughty children being put in the bag and taken away. This quality can be found in other Companions of Saint Nicholas such as Zwarte Piet.


== Krampusnacht ==
The Feast of St. Nicholas is celebrated in parts of Europe on 6 December. On the preceding evening of  5 December, Krampus Night or Krampusnacht, the wicked hairy devil appears on the streets. Sometimes accompanying St. Nicholas and sometimes on his own, Krampus visits homes and businesses. The Saint usually appears in the Eastern Rite vestments of a bishop, and he carries a golden ceremonial staff. Unlike North American versions of Santa Claus, in these celebrations Saint Nicholas concerns himself only with the good children, while Krampus is responsible for the bad. Nicholas dispenses gifts, while Krampus supplies coal and the Ruten bundles.


=== Perchtenlauf ===
A seasonal play that spread throughout the Alpine regions was known as the Nikolausspiel ("Nicholas play"). Inspired by Paradise plays, which focused on Adam and Eve's encounter with a tempter, the Nicholas plays featured competition for the human souls and played on the question of morality. In these Nicholas plays, Saint Nicholas would reward children for scholarly efforts rather than for good behavior. This is a theme that grew in Alpine regions where the Roman Catholic Church had significant influence.
There were already established pagan traditions in the Alpine regions that became intertwined with Catholicism. People would masquerade as a devilish figure known as Percht, a two-legged humanoid goat with a giraffe-like neck, wearing animal furs. People wore costumes and marched in processions known as Perchtenlaufs, which are regarded as any earlier form of the Krampus runs. Perchtenlaufs were looked at with suspicion by the Catholic Church and banned by some civil authorities. Due to sparse population and rugged environments within the Alpine region, the ban was not effective or easily enforced, rendering the ban useless. Eventually the Perchtenlauf, inspired by the Nicholas plays, introduced Saint Nicholas and his set of good morals. The Percht transformed into what is now known as the Krampus and was made to be subjected to Saint Nicholas' will.


=== Krampuslauf ===
It is customary to offer a Krampus schnapps, a strong distilled fruit brandy. These runs may include Perchten, similarly wild pagan spirits of Germanic folklore and sometimes female in representation, although the Perchten are properly associated with the period between winter solstice and 6 January.


== Krampuskarten ==
Europeans have been exchanging greeting cards featuring Krampus since the 1800s. Sometimes introduced with Gruß vom Krampus (Greetings from Krampus), the cards usually have humorous rhymes and poems. Krampus is often featured looming menacingly over children. He is also shown as having one human foot and one cloven hoof. In some, Krampus has sexual overtones; he is pictured pursuing buxom women. Over time, the representation of Krampus in the cards has changed; older versions have a more frightening Krampus, while modern versions have a cuter, more Cupid-like creature. Krampus has also adorned postcards and candy containers.


== Regional variations ==
In Styria, the Ruten bundles are presented by Krampus to families. The twigs are painted gold and displayed year-round in the house—a reminder to any child who has temporarily forgotten Krampus. In smaller, more isolated villages, the figure has other beastly companions, such as the antlered "wild man" figures, and St Nicholas is nowhere to be seen. These Styrian companions of Krampus are called Schabmänner or Rauhen.A toned-down version of Krampus is part of the popular Christmas markets in Austrian urban centres like Salzburg. In these, more tourist-friendly interpretations, Krampus is more humorous than fearsome.In Cave del Predil, in the northern part of the Udine province in Italy, an annual Krampus festival is held in early December. Just before the sun sets, the Krampus come out from an old cave and chase children—boys but also adults—punishing them with strokes on the legs. To satisfy their anger children and young people must recite a prayer.

North American Krampus celebrations are a growing phenomenon.Similar figures are recorded in neighboring areas. Klaubauf Austria, while Bartl or Bartel, Niglobartl, and Wubartl are used in the southern part of the country. In most parts of Slovenia, whose culture was greatly affected by Austrian culture, Krampus is called parkelj and is one of the companions of Miklavž, the Slovenian form of St. Nicholas.In many parts of Croatia, Krampus is described as a devil wearing a cloth sack around his waist and chains around his neck, ankles, and wrists. As a part of a tradition, when a child receives a gift from St. Nicholas he is given a golden branch to represent his good deeds throughout the year; however, if the child has misbehaved, Krampus will take the gifts for himself and leave only a silver branch to represent the child's bad acts.


== Costumes ==
Costumed characters are a central part of all Krampus celebrations.  These characters include: Krampus, Saint Nikolaus, the woodsman, angels, and the old woman.  As Krampus is half-goat and half-demon, the costume normally shares certain primary elements such as: a fur suit, horns, demon mask, and hooves. Props commonly used are; bells, a  birch switch, basket worn on the back, chains, walking staff, and a horse hair or hemp flogger.  The most traditional Krampus costumes are made from goat/sheep skins, animal horns, and hand carved masks.  More often they are made with modern and less costly materials, such as: fake fur and latex masks.  Several Krampus costume instructional YouTube videos are available.


== In popular culture ==

The character of Krampus has been imported and modified for various North American media, including print (e.g. Krampus: The Devil of Christmas, a collection of vintage postcards by Monte Beauchamp in 2004; Krampus: The Yule Lord, a 2012 novel by Gerald Brom), television – both live action ("A Krampus Carol", a 2012 episode of The League) and animation ("A Very Venture Christmas", a 2004 episode of The Venture Bros., "Minstrel Krampus", a 2013 episode of American Dad!)–video games (CarnEvil, a 1998 arcade game, The Binding of Isaac: Rebirth, a 2014 video game), and film (Krampus, a 2015 Christmas comedy horror movie from Universal Pictures).


== Gallery ==

		
		
		
		

		
		

		


== See also ==


=== Related figures ===
Companions of Saint Nicholas, a group of closely related figures that accompany Saint Nicholas throughout territories within the former Holy Roman Empire
Belsnickel – German Christmas gift-bringer, another West Germanic figure associated with the midwinter period
Perchta – German Alpine goddess, a female figure in West Germanic folklore whose procession (Perchtenlauf) occurs during the midwinter period
Pre-Christian Alpine traditions
Germanic paganism – Ethnic religion practiced by the Germanic peoples from the Iron Age until Christianisation
Yule Goat, a goat associated with the midwinter period among the North Germanic peoples
Namahage
Knecht Ruprecht
Koliada – Ancient pre-Christian Slavic winter festival, an ancient pre-Christian Slavic festival where participants wear masks and costumes and run around.
Turoń – Creature in Polish folklore
Ded Moroz – Fictional Christmas character in eastern Slavic cultures
Sinterklaas – Legendary figure based on Saint Nicholas, celebrated in the Low Countries on 5 or 6 December. He has a companion called Zwarte Piet (Black Pete), who used to punish bad children with a "roe", and kidnap them in bags to Spain. But nowadays they are just as friendly as Sinterklaas ("de Sint"), and give sweets and presents to all children.
Kurentovanje


=== Other ===
Bogeyman
Demon
Devil


== References ==


== External links ==

Bruce, Maurice (1958). "The Krampus in Styria". Folklore. 69: 45. doi:10.1080/0015587X.1958.9717121.
Honigmann, John J. (1977). "The Masked Face". Ethos. 5 (3): 263. doi:10.1525/eth.1977.5.3.02a00020.
Roncero, Miguel. "Trailing the Krampus", Vienna Review, 2 December 2013