The Prayer to Saint Michael usually refers to one specific Catholic prayer to Michael the Archangel, among the various prayers in existence that are addressed to him. From 1886 to 1964, this prayer was recited after Low Mass in the Catholic Church, although not incorporated into the text or the rubrics of the Mass.
Other prayers to Saint Michael have also been officially approved and printed on prayer cards.


== In the Leonine Prayers ==
In 1886, Pope Leo XIII added a Prayer to Saint Michael to the Leonine Prayers, which he had directed to be prayed after Low Mass two years earlier.

The best-known English translation is that which was used in Ireland and is quoted in James Joyce's novel Ulysses. Variant English translations include: "Holy Michael", "Saint Michael", "defend us in battle", "malice and snares", "may God rebuke him", "thrust into hell", "all evil spirits", "prowl about the world seeking the ruin", and "roam throughout the world seeking the ruin".The prayer's opening words are similar to the Alleluia verse for Saint Michael’s feasts on 8 May and 29 September in the Roman Missal of the time, which ran:


=== History ===

The 'Leonine Prayers' originated in 1884, when Pope Leo XIII ordered certain prayers to be said after Low Mass, in defense of the independence of the Holy See. God's help was sought for a satisfactory solution to the loss of the Pope's temporal sovereignty, which deprived him of the independence felt to be required for effective use of his spiritual authority. The prayer to St Michael described above was added to the Leonine Prayers in 1886.
The Pope's status as a temporal leader was restored in 1929 by the creation of the State of Vatican City, and in the following year, Pope Pius XI ordered that the intention for which these prayers should from then on be offered was "to permit tranquility and freedom to profess the faith to be restored to the afflicted people of Russia".The practice of reciting this and the other Leonine prayers after Mass was officially suppressed by the 26 September 1964 Instruction Inter oecumenici which came into effect on 7 March 1965.Removing the obligation to recite this prayer (along with the three Hail Marys, the Hail Holy Queen, and the prayer for the Church) after Low Mass did not mean forbidding its use either privately or publicly in other circumstances. Thirty years later, Pope John Paul II recommended its use, saying:
May prayer strengthen us for the spiritual battle that the Letter to the Ephesians speaks of: "Be strong in the Lord and in the strength of his might" (Ephesians 6:10). The Book of Revelation refers to this same battle, recalling before our eyes the image of St Michael the Archangel (cf. Revelation 12:7). Pope Leo XIII certainly had this picture in mind when, at the end of the last century, he brought in, throughout the Church, a special prayer to St Michael:
"Saint Michael the Archangel, defend us in battle. Be our protection against the wickedness and snares of the devil; May God rebuke him, we humbly pray; And do thou, O Prince of the Heavenly Host, by the power of God, thrust into hell Satan and all evil spirits who wander through the world for the ruin of souls. Amen."

Although this prayer is no longer recited at the end of Mass, I ask everyone not to forget it and to recite it to obtain help in the battle against the forces of darkness and against the spirit of this world.
On 29 September 2018, Pope Francis asked Catholics everywhere to pray the Rosary each day during the following month of October and to conclude it with the ancient prayer "Sub tuum praesidium" and the Leonine prayer to Saint Michael. He asked them "to pray that the Holy Mother of God place the Church beneath her protective mantle: to preserve her from the attacks by the devil, the great accuser, and at the same time to make her more aware of the faults, the errors and the abuses committed in the present and in the past, and committed to combating without any hesitation, so that evil may not prevail".A month earlier, Pope Francis called more generically to "a penitential exercise of prayer and fasting" in view of scandals concerning Catholic Church sexual abuse cases.


== Exorcism prayer ==
A quite different prayer to Saint Michael was included in an exorcism formula published in the 1890−1891 edition of Acta Sanctae Sedis for use by bishops and by those priests authorized to perform exorcisms. To any of these who devoutly recited the formula daily Pope Leo XIII granted on 18 May 1890 a partial indulgence on each day and a plenary indulgence whenever they did so for a whole month.
This "Exorcism against Satan and the apostate angels" (Latin: Exorcismus in Satanam et angelos apostaticos) opens with some verses from the Psalms and then presents a long prayer to Saint Michael followed immediately by the actual prayer of exorcism, which began with a series of ten conjurations.The exorcism formula, with its incorporated prayer to Saint Michael, was inserted into the 1898 edition of the Roman Ritual.


=== The 1890 prayer to Saint Michael ===


=== The 1902 prayer to Saint Michael ===
In 1902, a year and a half before the death of Pope Leo XIII, a new edition of the Roman Ritual considerably shortened the exorcism formula as a whole and in particular the prayer to Saint Michael within it.
In 1999, the Holy See issued a revised version of its Rite of Exorcism. In its "supplication and exorcism that may be used in special circumstances affecting the Church", it includes, for optional use, the 1902 prayer to Saint Michael and also allows it to be replaced by another prayer better known by the people (pp. 76−77). It includes the 1886 (Leonine Prayers) text as one of the "supplications that the faithful can use in their struggle against the powers of darkness" (p. 83). The 1890 prayer is not included.


=== Historical context ===

The 1890 text was composed and published twenty years after the capture of Rome had deprived the Pope of the last vestige of his temporal sovereignty. The papal residence at the Quirinal Palace had been converted into that of the King of Italy. In the view of Anthony Cekada, that situation explains the phrases: "These most crafty enemies have filled and inebriated with gall and bitterness the Church, the spouse of the Immaculate Lamb, and have laid impious hands on her most sacred possessions"; and "In the Holy Place itself, where has been set up the See of the most blessed Peter and the Chair of Truth for the light of the world, they have raised the throne of their abominable impiety, with the iniquitous design that when the Pastor has been struck, the sheep may be scattered." Cekada considers that the omission of these phrases from the 1902 revision of the text reflected improved relations between the Holy See and the Kingdom of Italy.


=== Unauthorized use ===
On 29 September 1985, Cardinal Joseph Ratzinger, Prefect of the Congregation for the Doctrine of the Faith, wrote to Catholic Ordinaries, recalling the need to maintain the canonical norm that exorcisms are to be performed only by select priests who have been authorized by the local Ordinary, and that it is therefore illicit for other Catholics to use the formula of exorcism against Satan and the fallen angels, extracted from the one published by order of Pope Leo XIII, still less to use the integral text of this exorcism.


== Prayer of consecration ==

The Opus Sanctorum Angelorum presents the following prayer as an Act of Consecration to Saint Michael the Archangel:
Oh most noble Prince of the Angelic Hierarchies, valorous warrior of Almighty God and zealous lover of His glory, terror of the rebellious angels, and love and delight of all the just angels, my beloved Archangel Saint Michael, desiring to be numbered among your devoted servants, I, today offer and consecrate myself to you, and place myself, my family, and all I possess under your most powerful protection.
I entreat you not to look at how little, I, as your servant have to offer, being only a wretched sinner, but to gaze, rather, with favorable eye at the heartfelt affection with which this offering is made, and remember that if from this day onward I am under your patronage, you must during all my life assist me, and procure for me the pardon of my many grievous offenses, and sins, the grace to love with all my heart my God, my dear Savior Jesus, and my Sweet Mother Mary, and to obtain for me all the help necessary to arrive to my crown of glory.
Defend me always from my spiritual enemies, particularly in the last moments of my life.

Come then, oh Glorious Prince, and succor me in my last struggle, and with your powerful weapon cast far from me into the infernal abysses that prevaricator and proud angel that one day you prostrated in the celestial battle. Amen.


== Stories about the origin of the prayer ==

An article in the Roman journal Ephemerides Liturgicae (V. LXIX, pages 54–60) in 1955 gave an account in Latin and Italian of how the Saint Michael prayer (in the Leonine Prayers) originated. Footnote nine of this account quotes an article in another Italian journal called La Settimana del Clero in 1947 by Domenico Pechenino, who worked at the Vatican during the time of Leo XIII, in which he said that after Leo had celebrated a Mass, he seemed to be staring at something, then went to his private office, with his attendants asking if he was well. Half an hour later he had written the Saint Michael prayer.
According to the same article in Ephemerides Liturgicae, Giovanni Nasalli Rocca Cardinal di Corneliano wrote in his Litteris Pastoralibus pro Quadragesima (Pastoral Letters for Lent) that according to Leo's private secretary, Rinaldo Angeli, Leo had seen a vision of demonic spirits who were congregating on the Eternal City (Rome); he wrote the Saint Michael prayer, and often said it, in response to the vision. Leo also personally wrote an exorcism prayer  included in the Roman Ritual, and recommended that bishops and priests read these exorcisms often in their dioceses and parishes. He himself often recited them. This account, which speaks not of the prayer included in the Leonine Prayers but of the general exorcism of which the prayer was at first a part, and for which it later (1902) served as a sort of preface, an exorcism that the Pope recommended bishops and exorcist priests to perform often, indeed daily, in their dioceses and parishes, and that he himself recited often throughout the day.
Several variants of this story are told. The first to appear in print was in a 1933 German Sunday newspaper article, which stated that, as a result of the vision, shortly after 1880 Leo ordered the prayer to Saint Michael to be recited. In reality, it was only in 1884 that the Pope instituted the Leonine Prayers, still at that time without the prayer to Saint Michael. A year later, a German writer, Fr. Bers, tried to trace the origin of the story and declared that, though the story was widespread, nowhere could he find a trace of proof. Sources close to the institution of the prayer in 1886, including an account of a conversation with Leo XIII about his decision, say nothing of the alleged vision. Bers concluded that the story was a later invention that was spreading like a virus. The story is also found in Carl Vogl's 1935 Begone Satan: A Soul-Stirring Account of Diabolical Possession in Iowa.In a later version, the vision is said to have occurred not in 1880, but on 13 October 1884, the year in which the Leonine Prayers were instituted but without the Prayer to Saint Michael.  And yet another date, 25 September 1888, two years after Pope Leo XIII had added the prayer to the Leonine Prayers, was given in a 1991 version.Another reported version of the vision relates a detailed conversation between the voice of Satan, who said he would destroy the Church if given enough power and time, and the voice of God, who permits Satan to do what he will. According to William Saunders, writing in the Arlington Catholic Herald, Leo said that God permitted Satan to choose a single century in which to work his worst against the Church; he chose the 20th century, and God privately revealed the then-future events of the 20th century to Leo.


== See also ==
Novena to Saint Michael
Saint Michael in the Catholic Church
Vade retro satana


== References ==


== Further reading ==
Huber, Georges (1992). Arrière Satan ! le diable aujourd'hui (in French). Paris: Éditions Pierre Téqui. pp. 18–20. ISBN 978-2-7403-0061-9. — El diablo hoy: ¡Apártate, Satanás! (translation). Cuadernos Palabra 110 (in Spanish). Madrid: Ediciones Palabra. 1997. ISBN 978-84-8239-410-7.
Symonds, Kevin J. (2015). Pope Leo XIII and the Prayer to St. Michael: An Historical and Theological Examination. Boonville, New York: Preserving Christian Publications. ISBN 978-0-9840139-6-8. — "Foreword | Chapter One" (publisher's book preview). Archived from the original on 2018-07-08.


== External links ==
John Paul II (Apr 24, 1994). "Regina Coeli" (in Spanish). Holy See. Archived from the original on 2015-10-10. Retrieved 2020-05-19.
Exorcismus in satanam et angelos apostaticos (official version) (PDF). Acta Sanctae Sedis (in Latin). XXIII. Rome: Polyglot Vatican Typography - Vatican Publishing House. 1890–1891. p. 743. Archived (PDF) from the original on Jun 21, 2015.Translations

"Exorcism against Satan and his apostate angels" (in Latin and English). Westminster, MA: The Franciscan Archive. Retrieved 2020-05-19.
"Exorcismus in satanam et angelos apostaticos". unavox.it (in Latin and French). Archived from the original on May 10, 2000.