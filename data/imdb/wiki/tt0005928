The Prince and the Pauper is a novel by American author Mark Twain. It was first published in 1881 in Canada, before its 1882 publication in the United States. The novel represents Twain's first attempt at historical fiction. Set in 1547, it tells the story of two young boys who were born on the same day and are identical in appearance: Tom Canty, a pauper who lives with his abusive, alcoholic father in Offal Court off Pudding Lane in London, and Edward VI of England, son of Henry VIII of England.


== Plot ==

Tom Canty, youngest son of a poor family living in Offal Court located in London, has always aspired to have a better life, encouraged by the local priest, who has taught him to read and write. Loitering around the palace gates one day, he meets Edward Tudor, the Prince of Wales. Coming too close in his intense excitement, Tom is nearly caught and beaten by the Royal Guards. However, Edward stops them and invites Tom into his palace chamber. There, the two boys get to know one another.  Fascinated by each other's life and their uncanny resemblance to each other and learning they were even born on the same day, they decide to switch places "temporarily". The Prince hides an item, which the reader later learns is the Great Seal of England, then goes outside; however, dressed as Tom, he is not recognized by the guards, who drive him from the palace. He eventually finds his way through the streets to the Canty home. There, he is subjected to the brutality of Tom's alcoholic and abusive father, from whom he manages to escape, and meets one Miles Hendon, a soldier and nobleman returning from war. Although Miles does not believe Edward's claims to royalty, he humors him and becomes his protector. Meanwhile, news reaches them that King Henry VIII has died and Edward is now the king.
Tom, dressed as Edward, tries to cope with court customs and manners. His fellow nobles and palace staff think the prince has an illness, which has caused memory loss and fear he will go mad. They repeatedly ask him about the missing Great Seal of England, but he knows nothing about it. However, when Tom is asked to sit in on judgments, his common-sense observations reassure them his mind is sound.
As Edward experiences the brutal life of a London pauper firsthand, he becomes aware of the stark class inequality in England. In particular, he sees the harsh, punitive nature of the English judicial system where people are burned at the stake, pilloried, and flogged. He realizes that the accused are convicted on flimsy evidence and branded or hanged for petty offenses, and vows to reign with mercy when he regains his rightful place. When Edward declares to a gang of thieves that he is the king and will put an end to unjust laws, they assume he is insane and hold a mock coronation.
After a series of adventures, including a stint in prison, Edward interrupts the coronation as Tom is about to be crowned as king. The nobles are shocked at their resemblance, and refuse to believe that Edward is the rightful king wearing Tom's clothes until he produces the Great Seal of England that he hid before leaving the palace. 
Edward and Tom switch back to their original places and Edward is crowned King Edward VI of England. Miles is rewarded with the rank of earl and the family right to sit in the presence of the king. In gratitude for supporting the new king's claim to the throne, Edward names Tom the "King's Ward", a privileged position he holds for the rest of his life.
The ending explains that though Edward died at the age of 15, he reigned mercifully due to his experiences, while Tom lived to be a very old man.


== Themes ==
The introductory quote—"The quality of mercy is . . . twice blest; / It blesseth him that gives and him that takes: / 'Tis mightiest in the mightiest: it becomes / The throned monarch better than his crown"—is part of "The quality of mercy" speech from Shakespeare's The Merchant of Venice. 
While written for children, The Prince and the Pauper is both a critique of social inequality and a criticism of judging others by their appearance. Twain wrote of the book, "My idea is to afford a realizing sense of the exceeding severity of the laws of that day by inflicting some of their penalties upon the King himself and allowing him a chance to see the rest of them applied to others..."


== History ==
Having returned from a second European tour—which formed the basis of A Tramp Abroad (1880)—Twain read extensively about English and French history. Initially intended as a play, the book was originally set in Victorian England before Twain decided to set it further back in time. He wrote The Prince and the Pauper having already started Adventures of Huckleberry Finn. 
The "whipping-boy story", originally meant as a chapter to be part of The Prince and the Pauper, was published in the Hartford Bazar Budget of July 4, 1880, before Twain deleted it from the novel at the suggestion of William Dean Howells.Ultimately, The Prince and the Pauper was published by subscription by James R. Osgood of Boston, with illustrations by Frank Thayer Merrill and John Harley.The book bears a dedication to Twain's daughters, Susie and Clara Clemens, and is subtitled "A Tale For Young People of All Ages".


== Adaptations ==


=== Theater ===

The Prince and the Pauper was adapted for the stage during Twain's lifetime, an adaptation which involved Twain in litigation with the playwright. In November 1920, a stage adaption by Amélie Rives opened on Broadway under the direction of William Faversham, with Faversham as Miles Hendon and Ruth Findlay playing both Tom Canty and Prince Edward.An Off-Broadway musical with music by Neil Berg opened at Lamb's Theatre on June 16, 2002. The original cast included Dennis Michael Hall as Prince Edward, Gerard Canonico as Tom Canty, Rob Evan as Miles Hendon, Stephen Zinnato as Hugh Hendon, Rita Harvey as Lady Edith, Michael McCormick as John Canty, Robert Anthony Jones as the Hermit/Dresser, Sally Wilfert as Mary Canty, Allison Fischer as Lady Jane and Aloysius Gigl as Father Andrew. The musical closed August 31, 2003. 
English playwright Jemma Kennedy adapted the story into a musical drama which was performed at the Unicorn Theatre in London 2012–2013, directed by Selina Cartmell and starring twins Danielle Bird and Nichole Bird as the Prince and Pauper and Jake Harders as Miles Hendon.


=== Comics ===

In 1946, the story was adapted into comics form by Arnold L. Hicks in Classics Illustrated ("Classic Comics") #29, published by Gilberton.
In 1962, Dell Comics published Walt Disney's the Prince and the Pauper, illustrated by Dan Spiegle, based on the three-part television adaptation produced by Walt Disney's Wonderful World of Color.In 1990, Disney Comics published Disney's The Prince and the Pauper, by Scott Saavedra and Sergio Asteriti, based on the animated featurette starring Mickey Mouse.


=== Film ===
The novel has also been the basis of several films. In some versions, Prince Edward carries identification when he assumes Tom's role. While animations such as the Mickey Mouse version retell the story, other cartoons employ parody (including an episode of the animated television show Johnny Bravo in which Twain appears, begging cartoonists to "let this tired story die"). Film critic Roger Ebert suggested that the 1983 comedy film Trading Places (starring Dan Aykroyd and Eddie Murphy) has similarities to Twain's tale due to the two characters' switching lives (although not by choice).A much-abridged 1920 silent version was produced (as one of his first films) by Alexander Korda in Austria entitled Der Prinz und der Bettelknabe. The 1937 version starred Errol Flynn (as Hendon) and twins Billy and Bobby Mauch as Tom Canty and Edward Tudor, respectively.
A Telugu film version, Raju Peda, was released in 1954 and directed and produced by B. A. Subba Rao and dubbed in to Tamil as Aandi Petra Selvam in 1957. Later a Hindi film version, Raja Aur Runk, was released in 1968 and directed by Kotayya Pratyagatma. These films "Indianized" many of the episodes in the original story. 
1983 Kannada movie Eradu Nakshatragalu was inspired by The Prince and the Pauper.
The Parent Trap has a plot about twins swapping places - a Walt Disney adaptation of a 1949 children's book "Lottie and Lisa" (original German title: Das doppelte Lottchen "The double Lottie") by Erich Kästner, in which identical twins separated in early life swap places to reunite their parents. The film starred Hayley Mills as twins Susan and Sharon. The film was then remade in 1998 and starred Lindsay Lohan in her feature film debut as twins Hallie and Annie. 
A 1977 film version of the story, starring Oliver Reed as Miles Hendon, starring Rex Harrison (as the Duke of Norfolk), Mark Lester and Raquel Welch and directed by Richard Fleischer, was released in the UK as The Prince and the Pauper and in the US as Crossed Swords.
Walt Disney Feature Animation made a 1990 animated 25-minute short film, inspired by the novel and starring Mickey Mouse. In this version, Mickey trades places with himself and is supported by other Disney characters.
It Takes Two, starring twins Mary-Kate and Ashley Olsen, is a loose translation of this story in which two girls (one wealthy and the other an orphan, who resemble each other) switch places in order to experience each other's lives.
The 1996 Bollywood film Tere Mere Sapne is loosely based upon this story, in which two boys born on exactly the same date switch places to experience the other's life, whilst learning valuable lessons along the way.
A 2000 film directed by Giles Foster starred Aidan Quinn (as Miles Hendon), Alan Bates, Jonathan Hyde, and identical twins Jonathan and Robert Timmins. 
In 2004, it was adapted into an 85-minute CGI-animated musical, Barbie as the Princess and the Pauper, with Barbie playing the blond Princess Anneliese and the brunette pauper Erika. In 2012, a second CGI musical adaptation was released, entitled Barbie: The Princess and the Popstar. In it, Barbie plays a princess blonde named Victoria (Tori) and a brunette popstar named Keira. Both crave the life of another, one day they meet and magically change places.
In 2006, Garfield's second live-action film entitled Garfield: A Tail of Two Kitties, was another adaptation of the classic story.
A 2007 film, A Modern Twain Story: The Prince and the Pauper starred identical twins Dylan and Cole Sprouse.
Monte Carlo was another loose adaptation released in 2011 by 20th Century Fox and starred Selena Gomez.


=== Television ===
Raju Peda, produced for Indian television in 1954, is a Telugu-version adaptation of the novel starring N. T. Rama Rao and directed by B. A. Subba Rao.
A 1962 three-part Walt Disney's Wonderful World of Color television adaptation featured Guy Williams as Miles Hendon. Both Prince Edward and Tom Canty were played by Sean Scully, using the split-screen technique which the Disney studios had used in The Parent Trap (1961) with Hayley Mills. 
The 21st episode of The Monkees, aired on February 6, 1967, was entitled "The Prince and the Paupers".

The "Josie And The Pussycats" episode "Swap Plot Flop" has Valerie agreeing to pose as a kidnapped princess who looks just like her, only for the plan to backfire.An episode of "The Osmonds" called "Jimmy And James In London" has Jimmy and Fuji switching places with their doppelgangers.A 1975 BBC television adaptation starred Nicholas Lyndhurst.
In a 1976 ABC Afterschool Special, Lance Kerwin played the dual role in a modern American-based adaptation of the story entitled P.J. and the President's Son. The BBC produced a television adaptation by writer Richard Harris, consisting of six thirty-minute episodes, in 1976. Nicholas Lyndhurst played both Prince Edward and Tom Canty.
Ringo, a 1978 TV special starring Ringo Starr, involves the former Beatles drummer trading places with a talentless look-alike.
The BBC TV comedy series Blackadder the Third has an episode, "Duel and Duality," where the Prince Regent believes that the Duke of Wellington is after him. The prince swaps clothes with his butler Blackadder and says, "This reminds of that story 'The Prince and the Porpoise'." Blackadder corrects him: "and the Pauper," to which the prince replies "ah yes, the Prince and the Porpoise and the Pauper." Since Blackadder the Third is set during the early 1800s, this is an anachronism.
In 1996, PBS aired a Wishbone adaptation titled "The Pooch and the Pauper" with Wishbone playing both Tom Canty and Edward VI.
The BBC produced a six-part dramatization of the story in 1996, adapted by Julian Fellowes, starring James Purefoy, with Keith Michell reprising his role of Henry VIII.
A 2011 episode of Phineas and Ferb ("Make Play", season 2, episode 64) follows a similar storyline, with Candace switching places with Princess Baldegunde of Duselstein and discovering that royal life is dull.
Starting with the episode "The Shepherd" (premiered on December 4, 2011), the TV series Once Upon a Time  introduced a version of the story where the shepherd named Prince Charming is the Pauper and Prince James is the Prince.
The 2017 Japanese anime series Princess Principal uses a similar story as the background for the characters Ange and Princess Charlotte; their history is revealed by Ange under the guise of a fairy tale named "The Princess and the Pickpocket". Ten years prior to the start of the series, Ange, who was actually the real Princess Charlotte, met Princess, who was actually a common pickpocket named Ange and looked identical to her. They befriended one another and eventually decided to trade places for a day. Soon after the switch, however, a Revolution broke out and divided their country, separating the girls and leaving them trapped in each other's roles.
In The Princess Switch (a Netflix romantic Christmas film released in November 2018 starring Vanessa Hudgens) Margaret, the Duchess of Montenaro, changes place with baker Stacy who she accidentally meets. That plot results in 2 new love stories.


=== Video games ===
In 1996, C&E, a Taiwanese software company, released an RPG video game for Sega Genesis entitled Xin Qigai Wangzi ("New Beggar Prince"). Its story was inspired by the book, with the addition of fantastic elements such as magic, monsters, and other RPG themes. The game was ported to PC in 1998. It was eventually licensed in an English translation and released in 2006 as Beggar Prince by independent game publisher Super Fighter Team. This was one of the first new games for the discontinued Sega platform since 1998 and is perhaps the first video game adaptation of the book.


== See also ==
Cultural depictions of Edward VI of England


== References ==


== External links ==

 Media related to The Prince and the Pauper at Wikimedia Commons
Project Gutenberg: The Prince and the Pauper
The Prince and the Pauper at Standard Ebooks