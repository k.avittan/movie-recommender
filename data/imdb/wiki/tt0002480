"Seaside Rendezvous" is a song by British rock group Queen, and is the seventh track on their fourth studio album A Night at the Opera. The song was written by Freddie Mercury under his vaudevillian/musichall influence.
The song is probably best remembered for its "jazz band" bridge section performed vocally by Mercury and Roger Taylor in the middle part of the song.


== Composition and recording ==
The song's "musical" bridge section was performed entirely by Mercury and Taylor using their voices alone, with Taylor at one point hitting the highest note on the whole album – a C6.
Mercury imitates woodwind instruments including a clarinet. Taylor voices mainly brass instruments such as tubas and trumpets, and even a kazoo. The tap dance segment is also "performed" by Mercury and Taylor on the mixing desk with thimbles on their fingers. Mercury plays both grand piano and jangle honky-tonk.
This is one of the few Queen songs without Brian May participating at all.


== Live performances ==
Queen has never performed "Seaside Rendezvous" live, although the Queen Extravaganza performed it along with the rest of A Night at the Opera for the album's 40th anniversary in 2015.


== Personnel ==
Information is taken from the album's liner notes.
Freddie Mercury - lead and backing vocals, grand piano, jangle piano, vocal orchestration of woodwinds
Roger Taylor - backing vocals, drums, triangle, vocal orchestration of brass
John Deacon - bass guitar


== References ==