The Royal Romance of Charles and Diana is a 1982 American made-for-television biographical drama film about the marriage between Prince Charles and Lady Diana Spencer starring Catherine Oxenberg, Christopher Baines, Olivia de Havilland, Dana Wynter and Ray Milland. It originally aired September 20, 1982 on CBS.


== Cast ==
Catherine Oxenberg - Lady Diana Spencer
Christopher Baines - Charles, Prince of Wales
Olivia de Havilland - Queen Elizabeth The Queen Mother
Dana Wynter - Queen Elizabeth II
Ray Milland - Mr. Griffiths
Stewart Granger - Prince Philip, Duke of Edinburgh
Holland Taylor - Frances Shand Kydd
George Martin - John Spencer, 8th Earl Spencer


== Reception ==
The movie debuted on United States television on the CBS network on Monday, September 20, 1982, and was the most watched prime-time television show of the week, with a 24.0 Nielsen rating and 37 share.  It outperformed ABC's Charles and Diana: A Royal Love Story, which had aired three days earlier but had been only the 28th most watched show in America for the prior week.John J. O'Connor, television critic for The New York Times, found that the movie was "much more fixated on the purely romantic possibilities of the story" of Charles and Diana compared to its ABC competition.  Instead of touching on any contemporary problems in Britain, the film provides "an outrageously gooey theme song and periodic helicopter shots of stunning countryside," and "emotional high points" such as Diana deciding to get a hair cut.  O'Connor opined that Olivia de Havilland played the Queen Mother "with beatific elegance" and Dana Wynter played Queen Elizabeth II "with infinite reserve."Tom Shales' review for The Washington Post was a great deal less kind, disliking the fairytale nature of the movie.  He called it a piece of "slack-jawed heraldic voyeurism incapable of, and apparently uninterested in, transforming remote news figures into believable mortals."  Shales preferred the ABC movie, calling it "48 times better," and decrying Diana's representation as "something of a Pollyanna, or a Cinderella - a sweet innocent sprite upon whom a fairy godmother bestowed a few crucial wishes."


== Production ==
The movie was filmed in Westbury, New York; Brewster, New York; and New York City, as well as in England.That the ABC and CBS films debuted three days apart was not a coincidence.  CBS had planned to show Royal Romance in December as a Hallmark-sponsored pre-Christmas film, but then moved the air date back to September, even before the official start of the new ratings season.  ABC then jumped their movie ahead of CBS, so late in the game that TV Guide did not list it.  When one reviewer asked to get a preview of either movie, he was told in one case "Are you kidding? They're still gluing the tape together."


== Legacy ==
As is typical of television movies developed to leverage the popularity of current events, The Royal Romance of Charles and Diana has been called a "forgettable" movie with only occasional press references (such as that it was Oxenburg's acting debut).  The movie has never been officially released on VHS or DVD.  In 2012, the New York Times reported on a brief spat over the credits for the film, which is likely the most ever written about the movie since 1982, albeit about a minor issue.  A dispute arose over whether Janet Grillo could properly claim a "story writer" credit, though it was agreed that she did co-write a story outline for the film.In 2011, producer Linda Yellen returned to the well using the next generation of British royalty to direct the television movie William & Catherine: A Royal Romance.


== References ==


== External links ==
Full film online, YouTube.com
The Royal Romance of Charles and Diana on IMDb