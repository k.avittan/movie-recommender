Misplaced Childhood is the third studio album by the British neo-progressive rock band Marillion, released in 1985. It is a concept album loosely based on the childhood of Marillion's lead singer, Fish, who was inspired by a brief incident that occurred while he was under the influence of acid.
The album was recorded during the spring of 1985 at Hansa Tonstudio in Berlin and produced by Chris Kimsey, who had previously worked with the Rolling Stones. Misplaced Childhood is the group's most commercially successful album to date, peaking immediately at number one in the UK charts and spending a total of 41 weeks on the chart. It ultimately gained the Platinum status. It features Marillion's two most successful singles, the guitar-led rock ballad "Kayleigh", which reached number two in the UK, and piano-led "Lavender", which peaked at number five.Misplaced Childhood was listed as the sixth best album of 1985 by Kerrang! and chosen as the fourth greatest concept album of all time by Classic Rock in 2003.


== Concept ==

Misplaced Childhood was Marillion's first full concept album consisting of two continuous pieces of music on the two sides of the vinyl record. The story has thematic elements of lost love, sudden success, acceptance, and lost childhood, along with an upbeat ending. As Fish explains, he conceived the concept during a 10-hour acid trip.
Several of the songs and titles contain notable autobiographical references; for example, "Kayleigh" references the breakdown of relationships as a whole but is centered around a Fish's past girlfriend named Kay Lee. Fish came up with the name Kayleigh in order to obscure the original name due to the song being too personal. Another example is "Heart of Lothian" ("I was born with the heart of Lothian") which is a reference to a traditional region of Scotland – Fish himself being from Midlothian – and a reference to the Heart of Midlothian (Royal Mile) – a mosaic heart in the pavement of Edinburgh's Royal Mile.
The theme of childhood is developed in "Lavender", which is partly based on the traditional folk song "Lavender Blue". Like "Kayleigh" it is a love song, but whereas "Kayleigh" was about the failure of an adult relationship, "Lavender" recalls the innocence of childhood.


== Packaging and cover art ==
Like Script for a Jester's Tear and Fugazi, the original vinyl edition of Misplaced Childhood was released in a gatefold sleeve. The artwork was created by Mark Wilkinson who was commissioned to the role on all Marillion albums and 12" singles of the Fish-era.
The front cover features a soldier drummer portrayed by Robert Mead, a then ten-year-old boy who lived next door to Wilkinson. Mead also appeared on the artwork of the album's three hit singles, "Kayleigh", "Lavender", and "Heart of Lothian", and can be seen in the music video for "Kayleigh". The Jester from the two previous studio albums is imagined escaping through the window on the back cover.


== Release ==
Misplaced Childhood was released in the United Kingdom on 17 June 1985 by EMI Records on LP, 12" picture disc and cassette and went on to be the band's biggest selling album. It topped the UK Albums Chart, becoming the first and the only Marillion album to do so. It stayed on the charts for 41 weeks, the longest chart residency of any of the band's albums. Misplaced Childhood was certified Platinum by the BPI for sales in excess of 300.000 copies on 26 November 1985 just 5 months after the release. It was the 20th best selling album in the United Kingdom for 1985.
The album was also highly successful across mainland Europe reaching number 3 in Germany, number 6 in Switzerland and the Netherlands, the latter of which is where Marillion have one of their largest fanbases, and number 10 in Norway. In the United States Misplaced Childhood came out on the Capitol Records label and reached number 47 on the Billboard 200 chart, the highest position the band has ever achieved.Three singles, "Kayleigh", "Lavender", and "Heart of Lothian" were released, with the first preceding the album. "Kayleigh" peaked at number 2 in the UK Singles Chart turning out to be the biggest hit for Marillion and prompting the success of Misplaced Childhood. The two further singles were less successful but still ended up at high positions as "Lavender" reached number 5 and "Heart of Lothian" peaked at number 29.


=== Remastering and reissues ===
As part of a series of Marillion's first eight studio albums, EMI Records re-released Misplaced Childhood on 17 October 1998 with 24-bit digital remastered sound and a second disc containing bonus tracks. The remastered version was also made available without the bonus disc in 2000 and again in 2005 as a Japanese mini-LP replica.A new 180g heavy weight vinyl pressing identical to the original 1985 edition was released in 2013.On 21 July 2017, a deluxe edition of Misplaced Childhood was released via Parlophone as a 4CD/Blu-ray set along with a 4LP boxed version. The deluxe edition includes a new remaster, as well as, on the Blu-ray disc, new high-resolution stereo and 5.1 surround remixes by Steven Wilson. The set also includes a previously unreleased 1985 concert from Utrecht featuring a performance of Misplaced Childhood in its entirety, along with demos and rarities.


== Reception ==

John Franck of AllMusic gives the album a 4.5 star rating. He has retrospectively said that Misplaced Childhood was "not only the band's most accomplished release to date, but also its most streamlined... With its lush production and punchy mix, the album went on to become the band's greatest commercial triumph, especially in Europe where they would rise from theater attraction to bona fide stadium royalty". In 2015, Ryan Reed of Ultimate Classic Rock called the record "the cornerstone of the entire 'neo-prog' movement".


=== Accolades ===

Kerrang! listed the album the sixth best LP of 1985. Classic Rock placed Misplaced Childhood fourth on its list of "Rock's 30 Greatest Concept Albums" in 2003, and named it one of the top 10 essential progressive rock releases of the 1980s in 2016. In the special edition Pink Floyd & The Story of Prog Rock released by Q Classic and Mojo, the album was ranked number 17 in its list of "40 Cosmic Rock Albums". In 2014, it was included in Rhythm magazine's "30 most influential prog drumming albums" at number 16.


=== Legacy ===
Misplaced Childhood was the inspiration for comedian Will Smith's Edinburgh Festival Fringe show of the same name in 2005, which also led to a successful tour in 2006. In 2006, former Marillion lead singer Fish performed a 20th anniversary tour of Misplaced Childhood, and a 30th anniversary tour was performed in 2015. The album was played in full, and the albums Return to Childhood and Farewell to Childhood were released as documents respectively.


== Promotion and tour ==
Marillion supported Misplaced Childhood with a one-year tour which began before the album was released. The band made two promotional appearances on BBC Television, firstly on the Wogan talk show on 20 May 1985 and then on Top of the Pops three days later, in both cases showcasing "Kayleigh". The tour consisted of European, Japanese and North American legs. In live performances preceding the album Fish claimed as a teaser that the next LP would consist of only two tracks, "Side 1" and "Side 2".


== Track listing ==
All lyrics are written by Fish; all music is composed by Mark Kelly, Ian Mosley, Steve Rothery and Pete Trewavas.Tracks 6–17 on the 1998 remastered edition bonus disc are Misplaced Childhood album demos, recorded in February 1985 and previously unreleased.


== 4-CD + Blu-ray Disc, 2017, Remastered, Digi-Book ==
CD 2 Live At Utrecht 1985
1. Emerald Lies [Intro]	0:50
2. Script For A Jester's Tear	8:41
3. Incubus	9:41
4. Chelsea Monday	9:59
5. The Web	8:17
CD 3 Live At Utrecht 1985
1. Pseudo Silk Kimono	3:15
2. Kayleigh	4:00
3. Lavender	2:20
4. Bitter Suite	8:20
5. Heart Of Lothian	4:02
6. Waterhole (Expresso Bongo)	2:26
7. Lords Of The Backstage	1:47
8. Blind Curve	9:36
9. Childhoods End?	4:13
10. White Feather	5:48
11. Fugazi	12:35
12. Garden Party	6:14
13. Market Square Heroes	7:25
Blu-ray Disc
1. Misplaced Childhood [Steven Wilson Remix]	41:11
2. Lady Nina [5.1 Surround Mix]	3:43
3. Lady Nina [Stereo Remix]	3:43
4. Misplaced Childhood [2017 Remaster]	41:11
5. Childhood Memories [Documentary]	72:00
6. Kayleigh [Promo Video]	3:37
7. Lavender [Promo Video]	3:41
8. Heart Of Lothian [Promo Video]	3:27
9. Lady Nina [Promo Video]	3:38 


== Personnel ==


== Charts ==


== Certifications ==


== References ==
Notes
Citations


== External links ==
The Official Marillion Website