Men Don't Leave is a 1990 American comedy-drama film starring Jessica Lange as a housewife who, after the death of her husband, moves with her two sons to Baltimore. Chris O'Donnell, Arliss Howard, Joan Cusack, Charlie Korsmo and Kathy Bates also co-star in this film. The film, directed by Paul Brickman and co-written with Barbara Benedek, is a remake of the French film La Vie Continue. The original music score was composed by Thomas Newman. Warner Brothers released the film on DVD for the first time on September 15, 2009, as part of the "Warner Archive Collection".


== Plot ==
Weighed down by her late contractor husband's debts in Bingham, Maryland, widowed mother Beth Macauley is compelled to sell her house and move to a less costly locale. She relocates in Baltimore with her sons Chris and Matt and takes a job at a gourmet food store managed by Lisa Coleman. Seventeen-year-old Chris (Chris O'Donnell) turns angry and aggressive while nine-year-old Matt (Charlie Korsmo) hides his deep sense of loss under a steely exterior. Beth is drawn into a relationship with Charles Simon, a musician who builds her self-esteem. However, after losing her job, she plunges into a five-day depression during which she refuses to leave her bedroom.
Beth is an extremely vulnerable, easily discouraged person who cannot seem to get a grip on her circumstances. Chris falls in love with Jody, an older radiographer who lives in the same building. Matt falls under the influence of a young schoolmate who breaks into houses and steals VCRs. His dream is to get enough money to buy back their suburban house. Beth and her sons eventually pull themselves together, and realize that to abandon each other is not the answer. Beth tells her sons, "Heartbreak is life educating us," and the lessons turn out to be worth learning.


== Main cast ==
Jessica Lange as Beth Macauley
Arliss Howard as Charles Simon
Joan Cusack as Jody
Chris O'Donnell as Chris Macauley
Kathy Bates as Lisa Coleman
Charlie Korsmo as Matt Macauley
Belita Moreno as Mrs. Buckley


== Reception ==
The film received generally positive reviews, and has a score of 81% on Rotten Tomatoes based on sixteen reviews.Shelia Benson of the Los Angeles Times praised the film as "a tender, beautifully acted, diabolically droll film on the subject of love, loss and the sheer blissful unpredictability of life."Roger Ebert said that the film "is the story of how a warm and believable suburban wife and mother becomes a widow who is trapped in a series of Hollywood improbabilities. The movie opens with enormous appeal and then spends its last hour chipping away at the sympathy it has earned. By the end, when some scenes are from the cliche factory and others seem to be missing altogether, I felt a great disappointment: The story started out too strongly to end as such a mess."Janet Maslin of The New York Times recalled that "among the film's more memorable moments is the scene in which Beth, having collapsed into such depression that she sits in a littered apartment eating cold spaghetti out of a can, is serenaded by her musician friend to the tune of Bella Notte, the Italian restaurant song from Lady and the Tramp. Bruce Surtees's cinematography gives the film a warm, vibrant look that's particularly flattering to its star. The editing is so abrupt in spots that it suggests there may have been more to Men Don't Leave at some earlier stage."Peter Travers of the Rolling Stone said that "Brickman has made an imperfect movie but not an impersonal one. Combining humor and heartbreak with rare grace, Men Don’t Leave means to get under your skin and does."Lange's performance was praised by critics.


=== Box office ===
The film was unsuccessful at the box office, grossing just over $6 million in the US on a $7 million budget.


== References ==


== External links ==
Men Don't Leave on IMDb
Men Don't Leave at AllMovie
Men Don't Leave at Rotten Tomatoes