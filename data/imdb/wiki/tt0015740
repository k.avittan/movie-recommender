The New School is a private research university in New York City. It was founded in 1919 as The New School for Social Research with an original mission dedicated to academic freedom and intellectual inquiry and a home for progressive thinkers. Since then, the school has grown to house five divisions within the university. These include the Parsons School of Design, the Eugene Lang College of Liberal Arts, The New School for Social Research, the Schools of Public Engagement, the College of Performing Arts which consists of the Mannes School of Music, the School of Drama, and the School of Jazz and Contemporary Music. In addition, the university maintains the Parsons Paris campus and has also launched or housed a range of institutions, such as the international research institute World Policy Institute, the Philip Glass Institute, the Vera List Center for Art and Politics, the India China Institute, the Observatory on Latin America, and the Center for New York City Affairs. It is classified among "R2: Doctoral Universities – High research activity".Its faculty and alumni include numerous notable designers, writers, musicians, artists, and political activists. Approximately 10,000 students are enrolled in undergraduate and postgraduate programs and disciplines including social sciences, liberal arts, humanities, architecture, fine arts, design, music, drama, finance, psychology, and public policy.


== History ==


=== Name ===
From its founding in 1919 by progressive New York educators, and for most of its history, the university was known as The New School for Social Research. Between 1997 and 2005 it was known as New School University. The university and each of its colleges were renamed in 2005.
The New School established the University in Exile and the École libre des hautes études in 1933 as a graduate division to serve as an academic haven for scholars escaping from Nazi Germany among other adversarial regimes in Europe. In 1934, the University in Exile was chartered by New York State and its name was changed to the Graduate Faculty of Political and Social Science. In 2005, it adopted what had initially been the name of the whole institution, the New School for Social Research, while the larger institution was renamed The New School.


=== Founding ===
The New School for Social Research was founded by a group of university professors and intellectuals in 1919 as a modern, progressive, free school where adult students could "seek an unbiased understanding of the existing order, its genesis, growth and present working". Founders included economist and literary scholar Alvin Johnson, historian Charles A. Beard, economists Thorstein Veblen and James Harvey Robinson, and philosophers Horace M. Kallen and John Dewey. Several founders were former professors at Columbia University.
In October 1917, after Columbia University imposed a loyalty oath to the United States upon the entire faculty and student body, it fired several professors. Charles A. Beard, Professor of Political Science, resigned his professorship at Columbia in protest. His colleague James Harvey Robinson resigned in 1919 to join the faculty at the New School.
The New School plan was to offer the rigorousness of postgraduate education without degree matriculation or degree prerequisites. It was theoretically open to anyone, as the adult division today called Schools of Public Engagement remains. The first classes at the New School took the form of lectures followed by discussions, for larger groups, or as smaller conferences, for "those equipped for specific research". In the first semester, 100 courses, mostly in economics and politics, were offered by an ad hoc faculty that included Thomas Sewall Adams, Charles A. Beard, Horace M. Kallen, Harold Laski, Wesley Clair Mitchell, Thorstein Veblen, James Harvey Robinson, Graham Wallas, Charles B. Davenport, Elsie Clews Parsons, and Roscoe Pound. Only after did the New School begin to offer degrees in line with the traditional university model. John Cage later pioneered the subject of Experimental Composition at the school.


==== Motto ====
The New School uses "To the Living Spirit" as its motto. In 1937, Thomas Mann remarked that a plaque bearing the inscription "be the Living Spirit" had been torn down by the Nazis from a building at the University of Heidelberg. He suggested that the University in Exile adopt that inscription as its motto, to indicate that the 'living spirit,' mortally threatened in Europe, would have a home in this country. Alvin Johnson adopted that idea, and the motto continues to guide the division in its present-day endeavors


==== University in Exile ====
The Graduate Faculty of Political and Social Science was founded in 1933 as the University in Exile for scholars who had been dismissed from teaching positions by the Italian fascists or had to flee Nazi Germany. The University in Exile was initially founded by the director of the New School, Alvin Johnson, through the financial contributions of Hiram Halle and the Rockefeller Foundation.  The University in Exile and its subsequent incarnations have been the intellectual heart of the New School. Notable scholars associated with the University in Exile include psychologists Erich Fromm, Max Wertheimer and Aron Gurwitsch, political theorists Hannah Arendt and Leo Strauss, and philosopher Hans Jonas.In 1934, the University in Exile was chartered by New York State and its name was changed to the Graduate Faculty of Political and Social Science.  In 2005 the Graduate Faculty was again renamed, this time taking the original name of the university, The New School for Social Research.


==== École libre des hautes études ====
The New School played a similar role with the founding of the École Libre des Hautes Études after the Nazi invasion of France. Receiving a charter from de Gaulle's Free French government in exile, the École attracted refugee scholars who taught in French, including philosopher Jacques Maritain, anthropologist Claude Lévi-Strauss, and linguist Roman Jakobson. The École Libre gradually evolved into one of the leading institutions of research in Paris, the École des Hautes Études en Sciences Sociales, with which the New School maintains close ties.


==== Dramatic Workshop/School of Drama ====
Between 1940 and 1949, The New School included the "Dramatic Workshop," a groundbreaking theater education program and predecessor of School of Drama that was founded by German emigrant theatre director Erwin Piscator. Important acting teachers during this period were Stella Adler and Elia Kazan. Among the famous students of the Dramatic Workshop were Beatrice Arthur, Harry Belafonte, Marlon Brando, Tony Curtis, Ben Gazzara, Michael V. Gazzo, Rod Steiger, Elaine Stritch, Shelley Winters and Tennessee Williams.
I attended The New School for Social Research for only a year, but what a year it was. The school and New York itself had become a sanctuary for hundreds of extraordinary European Jews who had fled Germany and other countries before and during World War II, and they were enriching the city's intellectual life with an intensity that has probably never been equaled anywhere during a comparable period of time.


== Organization ==
The New School is divided into autonomous colleges called "divisions". Each one is led by a dean and has its own scholarships, standards of admission, and acceptance rates.


=== Divisions ===


==== Major colleges ====


==== Former divisions ====


=== 2005 rebranding ===
In June 2005, the university was rebranded with a new logo and all schools were officially renamed to include "The New School" within their formal names.Some faculty, students, and alumni expressed concern over the rebranding of the university, and especially the dramatic redesign of the logo from a six-sided shield against a green background to a spray-painted graffiti mark reading simply, in capital letters, "THE NEW SCHOOL" with, in smaller letters beneath, "A UNIVERSITY". They claimed that the university's new identity campaign, while maintaining a slick urban edge, did little to suggest academic rigor or collegiate legacy.


=== 2015 rebranding ===
In 2015 the New School rebranded using elements designed by Paula Scher of Pentagram using a bespoke font called "Neue".In addition to the new logo, the school announced that it was combining Mannes College of Music, New School for Jazz and Contemporary Music, and New School for Drama into a College of Performing Arts in fall 2015, relocating most of the performing arts to Arnhold Hall at 55 West 13th St., where the School of Jazz had occupied two floors since the early 90s.


== Academics ==
Unlike most U.S. universities, The New School has a "student-directed curriculum", which does not require its undergraduates to take general education courses. Instead, students are encouraged to explore before focusing on a major, selecting topics that are of interest to them. An exception to this is in the performing arts, where students must declare majors at enrollment. Although all "New Schoolers" are required to complete rigorous core training—usually of a literary, conservatory, or artistic nature—students are expected to be the primary designer of their own individualized and eclectic education.
The university offers 81 degree/diploma programs and majors, with a student-to-faculty ratio of 9:1. This small class size allows The New School to teach most of its classes in the seminar style—especially at Eugene Lang College, which consistently ranks at the top of The Princeton Review's "class discussions encouraged" national listing.


=== Dual degree programs ===
The university offers a range of dual degree programs. These include a bachelor of arts and bachelor of fine arts (colloquially called the "BA/FA pathway") program or a bachelor of arts and masters program. The former is a comprehensive five-year program that allows students to obtain their B.A. from Eugene Lang College and their B.F.A. from either Parsons or The School of Jazz and Contemporary Music. The latter is also a five-year program that allows students at Eugene Lang to obtain their masters from the New School For Social Research. The university also offers a Master of Arts Management and Entrepreneurship program, which can be obtained along with either a Bachelor of Music (Mannes) or a Bachelor of Fine Arts (Drama or Jazz) in five-years.


=== Institutes and research centers ===
There are several important institutes and research centers at The New School which are focused on various study fields. Their work is concentrated in the following areas:

International Affairs and Global Perspectives
Philosophy and Intellectual Culture
Humanities Action Lab
Politics, Policy, and Society
Art, Design, and Theory
Environment
Urban and Community Development
Center for New York City Affairs
Center for Public Scholarship
Philip Glass InstituteThe New School's College of Performing Arts is home to the influential experimental music venue, The Stone, offering 240 concerts a year.


=== Enrollment demographics ===
33% of New School students are international, with 112 foreign countries being represented at the university. U.S. students come from all 50 states and the District of Columbia. 43% of them are people of color, and 5% of American students identify as more than one race. Of the entire student population, 63% receive financial aid, and 17% study abroad before graduating.


== Campus ==

The New School's campus is composed of many buildings, most of which are minutes from Union Square.
The university's Parsons division also has affiliations with schools that operate independently but embrace Parsons' philosophy and teaching methodology, including Parsons Paris in France, India School of Design and Innovation in Mumbai, and La Escuela de Diseño at Altos de Chavón in La Romana, Dominican Republic.

The New School campus


=== University Center ===

The New School opened the 16-story University Center ("UC") at 65 5th Avenue in January 2014.While the 65 Fifth Avenue plans were initially controversial among students and Village residents (spurring in 2009 a major student occupation that was held at The New School's previous building on that site), plans for the University Center were adjusted in response to community concerns and have since been well received. In a review of the University Center's final design, The New York Times architecture critic Nicolai Ouroussoff called the building "a celebration of the cosmopolitan city".
The UC serves as a central hub for all university students. The tower, which was designed by Skidmore, Owings and Merrill's Roger Duffy, is the biggest capital project the university has ever undertaken. The building added classrooms, new residences, computer labs, event facilities, and a cafeteria to the downtown New York City campus in addition to a library, and lecture hall.


=== Historical significance ===
Several of the university buildings are certified by New York City as historical landmarks. Prominent among these is the egg-shaped Tishman Auditorium, considered by many to be the first building to employ modern architecture. It was designed by architect Joseph Urban, along with the entirety of The New School's historic 66 West 12th Street building. Thousands of writer's forums, author visits, political debates, award ceremonies, academic lectures, performances, and public hearings are held for both the academic community and general public throughout the year in Tishman.
Newer buildings have garnered a multitude of awards. Among these is The Sheila Johnson Design Center, which attracted media attention for its revolutionary design. In 2009, it won the SCUP's Excellence in Architecture Renovation/Adaptive Reuse Award. In addition to being a Parsons core academic building, the Center also serves as a public art gallery. The New School Welcome Center, located on 13th Street and Fifth Avenue, won the American Institute of Architects, New York Chapter's Interiors Merit Award in 2010. In October 2019, the university celebrated its centennial with The Festival of New. 


== Libraries ==
The New School owns several libraries throughout New York City and is a member of the Research Library Association of South Manhattan. In 2009, its libraries counted a total of 1,906,046 holdings.
Fogelman Social Sciences and Humanities Library (migrated to the List Center)
Kellen Archives – design and Parsons' history (migrated to Archives & Special Collections)
Visual Resource Center (no longer active)
Adam and Sophie Gimbel Design Library (migrated to University Center Library in 2013)
Scherman Music Library (no longer active)
Archives & Special Collections
University Center Library – art and design
List Center Library – humanities and social sciences


=== Art collection ===
In 1931 the New School commissioned two mural cycles: José Clemente Orozco's "A Call for Revolution" and "Universal Brotherhood" and Thomas Hart Benton's epic America Today. The New School Art Collection was established in 1960 with a grant from the Albert A. List Foundation. The collection, now grown to approximately 1,800 postwar and contemporary works of art, includes examples in almost all media. Parts of it are exhibited throughout the campus. Notable artists such as Andy Warhol, Kara Walker, Richard Serra, and Sol LeWitt all have pieces displayed in New School's academic buildings.


== Publications ==


=== Academic journals ===
The New School publishes the following journals:

Constellations
Social Research
The Graduate Faculty Philosophy Journal
International Journal of Politics, Culture, and Society
New School Economic Review
New School Psychology Bulletin
The Journal of Design Strategies
The Parsons Journal for Information Mapping (PJIM), a quarterly publication by the Parsons Institute for Information Mapping


=== Other university publications ===
The New School Free Press, The New School's only student-run newspaper, with a monthly print edition distributed around campus and continually updated online content
 Public Seminar, an independent project of The New School Publishing Initiative, produced by The New School faculty, students and staff and supported by colleagues and collaborators around the globe.
LIT, a nationally distributed literary journal – contains works selected by the MFA Creative Writing Program
12th Street, a nationally distributed literary journal from The New School's Riggio Honor Program that contains work from undergraduate writers at the university
Voices, the literary journal of New School's The Institute For Retired Professionals
Eleven and a Half, the literary journal of Eugene Lang College
NEW_S, an e-newsroom showcasing The New School in major media, major student and alumni achievements, university programs, and other news
Canon Magazine, a quarterly publication of student writings published by The New School for Social Research
re:D, the magazine for Parsons alumni and the wider Parsons community, published by the New School Alumni Association.
Scapes, the annual journal of the School of Constructed Environments
BIAS: Journal of Dress Practice, a journal published by the MA Fashion Studies Dress Practice Collective started in the spring of 2013 that aims to join elements of "visual culture, fashion theory, design studies and personal practice through a variety of media".
The Weekly Observer, an online newsletter showcasing major student and alumni achievements, special program announcements, and other university-wide news. Distributed via MyNewSchool web portal


== Student life ==


=== Student organizations ===
The New School houses over 50 recognized student organizations, most of which are geared towards artistic endeavors or civic engagement. Notable among these are The Theatre Collective, which stages numerous dramatic productions throughout the year, Students for a Democratic Society (SDS), the New School Debate Team (intercollegiate competition in Policy/Cross Examination style debate), ReNew School (sustainability and environmental advocacy group) Moxie (feminist alliance), the New Urban Grilling Society (NUGS), and The Radical Student Union (RSU).


=== Student-run media ===
PrintA noted student newspaper, The New School Free Press, also known as NSFP is widely distributed throughout the campus. Hard print copies are available in most academic buildings, while an online edition is available as well. Students at Eugene Lang College publishes Release, a student-run literary magazine.

WNSR radioWNSR, a student-run, faculty-advised online-only radio station, also operates at the university. Programming is currently delivered in the form of streamable mp3s and, in the near future, subscribable podcasts. It is a station for all divisions of The New School.


=== Athletics and recreation ===
Former Athletics and Recreation Director Diane Yee joined The New School in August 2012. On October 25, 2012 a school-wide election was held to select a mascot, where The New School Narwhals were born. On January 25, 2013 the athletics logo was launched, designed by Parsons’ student Matthew Wolff (Graphic Design '14).The department began in December 2008 under its original name Recreation and Intramural sports. The initial director, Michael McQuarrie, held the position for four years. He built a relationship with the McBurney YMCA where intramurals continue to be held on Wednesday nights and created the ongoing New School Olympics and charitable 5K Turkey Trot.
The Narwhals feature several intercollegiate teams: basketball (2009), cross country (2010), cycling (2013), soccer (2013), tennis (2014), ultimate Frisbee (2014). The New School Narwhals are an independent school, unaffiliated with the NCAA, but regularly compete against NCAA Division III schools.
Basketball – competes regularly against Cooper Union, Culinary Institute of America, Pratt Institute, and Vaughn College
Cross Country – competes in CUNYAC and HVIAC conference invitationals as an unaffiliated school
Cycling – a member of the Eastern Collegiate Cycling Conference
Soccer – competes against Cooper Union, Culinary Institute of America, St. Joseph's College, and Vaughn College
In addition to sports, the recreation department offers a myriad of free fitness classes to its community including boxing, dance, HIIT, Pilates, tai chi, yoga, and Zumba. Personal training is also offered at an affordable rate ranging from $16.50 to $40 per session.
Outdoor Adventure trips are offered several times/week and what started to be wilderness in nature (camping, hiking, rafting) has expanded to include excursions such as archery, biking, horseback riding, skiing/snowboarding, surfing, rock climbing and trapeze.
Yee has increased programming to include a second charitable race that takes place annually in April called the 5K Rabbit Run. She has also started the Urban Hunt (a scavenger hunt around campus and the Village) and Club New (a dance party for first-year students the weekend before first day of classes).


=== Sexual harassment ===
The psychology department at The New School has had issues with sexual harassment. According to a December 2017 account in the Stanford Daily, Marcel Kinsbourne retired after being investigated by the school's Title IX office for sexual harassment of several students.  In the fall of 2017, another professor in the same division, Emanuele Castano, left the New School under the same conditions.  In April 2018, a former undergraduate student filed a lawsuit against the school over the school's conduct regarding Castano's behavior.In May 2010 The New School was found liable for sexual harassment and retaliation endured by a freshman student employee. The case was Zakrzewska v The New School, 2010 NY Slip Op 03796, and in it, the New York Court of Appeals found that the typical defense of an employer against a sexual harassment suit (called the Faragher-Ellerth affirmative defense) was not applicable under New York City Human Rights Law.


== Activist culture and social change ==
Historically, The New School has been associated with leftist politics, campus activism, civic engagement, and social change. It is a "Periclean University", or member Project Pericles, meaning that it teaches "education for social responsibility and participatory citizenship as an essential part of their educational programs, in the classroom, on the campus, and in the community". The New School is one of nine American universities to be inducted into Ashoka's "Changemaker" consortium for social entrepreneurship.In 2010, NYC Service awarded New School special recognition in The College Challenge, a volunteer initiative, for the "widest array of [civic] service events both on and off campus". Miriam Weinstein also cites the Eugene Lang division in her book, Making a Difference Colleges: Distinctive Colleges to Make a Better World.


=== Kerrey presidency and opposition ===
Former U.S. Senator Bob Kerrey became president of The New School in 2000. Kerrey drew praise and criticism for his streamlining of the university, as well as censure for his support of the 2003 invasion of Iraq, generally opposed by the university's faculty.In 2004, Kerrey appointed Arjun Appadurai as provost. Appadurai resigned as provost in early 2006, but retained a tenured faculty position. He was succeeded by Joseph W. Westphal, yet on December 8, 2008 Kerrey announced that Westphal was stepping down to accept a position in President Barack Obama's Department of Defense transition team. Kerrey then took the highly unorthodox step of appointing himself to the provost position while remaining president. This decision was strongly criticised by faculty and other members of the university community as a power-grab involving potential conflicts of interest. This was seen as a threat to scholarly integrity since the role of provost in overseeing the academic functions of a university has traditionally been insulated from fundraising and other responsibilities of a college president. After a series of rifts including protests involving student occupations of university buildings, Kerrey later appointed Tim Marshall, Dean of Parsons School of Design, as Interim Provost through June 2011. Marshall has since been reappointed in this role.
On December 10, 2008, 74 of the New School's senior professors gave a vote of no confidence for the New School's former president, Bob Kerrey. By December 15, 98% of the university's full-time faculty had voted no confidence. On December 17, over 100 students barricaded themselves in at a dining hall on the campus while hundreds more waited on the streets outside. They considered the current school administration opaque and harmful. Their chief demand, among others, was that Bob Kerrey resign. The students soon enlarged their occupied area, blocking security and police from entering the building. At 3 AM the next morning, the students left the building after Kerrey agreed to some of their demands (the most important elements on their first list of demands were not agreed to), including increased study space and amnesty from any actions performed during the protest. He did not, however, concede to resignation. In total, the occupation lasted 30 hours.
The following year, on April 10, 2009, students, mostly from New School but also from other New York colleges, reoccupied the building at 65 Fifth Avenue, this time holding the entire building for about six hours. Once again, the students demanded the resignation of Bob Kerrey. The New York Police Department arrested the occupiers; the New School students involved were then suspended. The next month, Kerrey announced he would fulfill his presidency at the university through the end of his term and expressed his intent to leave office in June 2011. However, he ended up resigning a semester early, on January 1, 2011. In August, the board of trustees appointed Dr. David E. Van Zandt the university's president.


=== Environmental sustainability ===
In 2010, The Princeton Review gives the university a sustainability rating of 94 out of 99. In 2010, the organization also named The New School one of America's "286 Green Colleges". The New School has a student-led environment and sustainability group, called Renew School, as well as full-time employees devoted to the school's sustainability. The university signed the Presidents' Climate Commitment and PlaNYC. The institution's sustainability website outlines many goals and projects for the future which will hopefully help The New School receive a good rating in the 2010 College Sustainability Report Card. The New School had the lowest reported carbon footprint of any college and university submitting inventories under the Green Report Card program, totaling about 1.0 metric tons CO2 per student.  Subsequently, with the completion of the LEED certified but large University Center, The New School's carbon footprint increased to about 1.5 metric tons.


=== Labor movement ===
In 2003, adjunct faculty in several divisions of the New School began to form a labor union chapter under the auspices of the United Auto Workers. Though the university at first tried to contest the unionization, after several rulings against it by regional and national panels of the National Labor Relations Board the university recognized the local chapter, ACT-UAW, as the bargaining agent for the faculty. As a result of a near strike in November 2005 on the part of the adjunct faculty, the ACT-UAW union negotiated its first contract which included the acknowledgment of previously unrecognized part-time faculty at Mannes College The New School for Music. In October 2018, graduate students received a tentative union contract from the administration after months of negotiations.


== Noted alumni, faculty, and current students ==

According to the university, The New School has a living alumni pool of over 56,000 and graduates live in 112 different countries.


=== Notable alumni ===

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		


=== Notable faculty ===

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		


== See also ==
Education in New York City
National Book Award
The New York Foundation
The New York Intellectuals
Project Pericles
Free University of New York


== References ==


== Further reading ==
Magg, P. "Education for the Age of Labor", The Kenyon Review, vol. 6, no. 4 (Autumn 1944), pp. 632–644.
Rutkoff, Peter M. and Scott, William B. New School: A History of the New School for Social Research. New York: Free Press, 1986.


== External links ==
Official website