Gold Heels (foaled 1898 in Pennsylvania) was an American Thoroughbred Champion racehorse who, in a two-year period, set one new stakers record and four track records, including a world record.


== Background ==
Gold Heels was bred by Alexander Cassatt at his Chesterbrook Farm in Berwyn, Pennsylvania. He was sired by Cassatt's outstanding runner, The Bard, a son of U.S. Racing Hall of Fame inductee, Longfellow. Gold Heels was out of the very good race mare Heel-and-Toe. A daughter of four-time Leading sire in North America, Glenelg, the durable Heel-and-Toe made 107 career starts winning 21 times.


== Racing career ==


=== 1900: Two-Year-Old season ===
Gold Heels was purchased by William C. Whitney but after racing him a short time at age two, the colt was deemed to have limited potential and in June 1900 was sold for $1,500 to trainer David Sloan, a cousin of future Hall of Fame jockey Tod Sloan. David Sloan raced the colt during the remainder of 1900 in mainly lower class races, finishing the year with five wins from twenty-four starts including the  Chappaqua Handicap at Empire City Race Track. Facing financial problems, David Sloan put Gold Heels up for sale and on the advice of trainer, Matthew Allen, he was purchased for $7,000 by the racing partnership of Fred C. McLewee and Diamond Jim Brady.


=== 1901: Three-Year-Old season ===
In 1901, under the conditioning of Matthew Allen, three-year-old Gold Heels won seven of his twelve starts while setting three track records. On June 27 he won the Spindrift Stakes in which he set a new Sheepshead Bay Race Track record for one mile and one furlong on dirt. On July 2 he won the Long Island Handicap at Sheepshead Bay  and then on July 25 won the richest race for three-year-olds at Brighton Beach Race Course, the mile and one furlong Seagate Stakes. He followed this up with a ten length victory on September 25, 1901 in the one mile and one furlong Monarch Stakes at Gravesend Race Track. In winning the October 5 Oriental Handicap at Gravesend he set a new track record time for a mile and a quarter on dirt.  On October 26, at Morris Park Racecourse, Gold Heels showed he was not only capable at longer distances but a truly outstanding stayer when he won the 2¼ mile Woodlawn Vase in a track record time of 3:56.00.


=== 1902: Four-Year-Old season ===
At age four in 1902, Gold Heels won four of his five starts, setting a stakes record and a world record. With jockey Otto Wonderly aboard, Gold Heels won the June 14 Suburban Handicap at Sheepshead Bay Race Track. Not only did he win what was then America's most prestigious race, he broke the stakes record on an off track while carrying top weight. On June 28 Gold Heels won the 1 ½ mile Advance Stakes at Sheepshead Bay Race Track
 and was 
top-weighted again when he won the July 5 Brighton Handicap at Brighton Beach Race Course in a world record time of 2:03.80 for a mile and a quarter on dirt. For the July 26 Brighton Cup, Gold Heels was again given highweight but still earned the win in the 2¼ mile endurance test. Even though he still won by twenty lengths, near the finish jockey George Odom slowed him to a canter due to an injury that ended his racing career.
The New York Times wrote on October 5, 1902 that Gold Heels was the "accepted champion of the year"  and Thoroughbred Heritage selected him as the retrospective American Champion Older Male Horse for that year.


== Sale and stud career ==
The September 20, 1902, issue of the Chicago Daily Tribune announced that the racing partnership of McLewee and Daly was to be dissolved 
 and on October 4, 1902 Gold Heels was sold at a dispersal auction. Expected to bring $15,000, jockey Winfield O'Connor bought him for only $6,500 
 then quickly resold the horse to St. Louis, Missouri breeder, E. J. Arnold.At stud, Gold Heels met with little success although Covadonga (b. 1908) became one of the first American-bred horses to be imported to Puerto Rico where he raced with considerable success. Gold heels was sold in December 1912 to the U.S. Cavalry Remount Service for use as a sire for military horses.


== References ==

Gold Heels' pedigree and partial racing stats