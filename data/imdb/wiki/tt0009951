Bondage, in the BDSM subculture, is the practice of consensually tying, binding, or restraining a partner for erotic, aesthetic, or somatosensory stimulation. A partner may be physically restrained in a variety of ways, including the use of rope, cuffs, bondage tape, or self-adhering bandage.
Bondage itself does not necessarily imply sadomasochism. Bondage may be used as an end in itself, as in the case of rope bondage and breast bondage. It may also be used as a part of sex or in conjunction with other BDSM activities. The letter "B" in the acronym "BDSM" comes from the word "bondage". Sexuality and erotica are an important aspect in bondage, but are often not the end in itself. Aesthetics also plays an important role in bondage.
A common reason for the active partner to tie up their partner is so both may gain pleasure from the restrained partner's submission and the feeling of the temporary transfer of control and power. For sadomasochistic people, bondage is often used as a means to an end, where the restrained partner is more accessible to other sadomasochistic behaviour. However, bondage can also be used for its own sake. The restrained partner can derive sensual pleasure from the feeling of helplessness and immobility, and the active partner can derive visual pleasure and satisfaction from seeing their partner tied up.


== Bedroom ==

Many couples incorporate bondage into their sex lives, often sporadically but sometimes more regularly, and find sexual bondage to be relationship-affirming. This sometimes takes the form of a sex game or sexual fantasy enactment. Bedroom bondage games may be used for sexual arousal or as a form of foreplay, that requires and implies a level of trust and a surrender of control by the restrained to the active partner. The restrained partner (called a submissive) surrenders control to the other partner (called a dominant). This surrender of control happens voluntarily and under mutual understanding and consent.
The main feature of sexual bondage is that it renders the restrained person vulnerable to a variety of sex acts, including some that they may be inhibited from otherwise engaging. The restrained partner is dependent for their sexual satisfaction on the actions of their partner, who may treat the restrained partner as their sex object.
There are many reasons why people allow themselves to be bound. Some people feel a kind of freedom during corporal passivity, they can concentrate on their inner spirituality and feel at peace, as a participant in a study about motivation for bondage explained: "Some people have to be tied up to be free". Others experience helplessness, struggle against their bonds, and feel a degree of masochistic pleasure from the restraint and pain, as well as being unobstructed for erotic stimulation by their partner.
Bondage can be relatively simple to apply, enabling improvisation using household items and little experience, though sophisticated commercial apparatus is available. Bedroom bondage is usually mild bondage, with one partner voluntarily being put into restraints by being tied up or handcuffed. It may involve simple hand ties, bed restraints, being tied to a chair, etc. Blindfolds are a common part of bedroom play. The restrained partner may then be sexually stimulated by masturbation, fingering, handjob, oral sex, a vibrator, intercourse or other sex acts. Bondage can also be used for purposes other than sexual foreplay, for example, it may be used in erotic tickling or for sexual teasing.
The free partner may derive erotic pleasure or achieve sexual arousal from being in a dominant situation, while the tied partner may achieve arousal from being in a largely "helpless" position in the hands of a trusted partner. Either way, the partners are usually playing out bondage games to act out their sexual fantasies.In 1995, psychologists Kurt Ernulf and Sune Innala from Sweden published an analysis based on answers from members of the bondage-oriented Usenet group alt.sex.bondage. Most of the answers (76%) were from men. In 71% of the answers the active (restraining) role in bondage was played by heterosexual men, in 11% by heterosexual women and in 12% by homosexual men. 29% of the heterosexual men, 89% of the heterosexual women and 88% of the homosexual men played the passive (restrained) role. A third of the people who answered said they practised bondage in connection with sadomasochistic activities or at least thought bondage and sadomasochism belonged together.In a survey of American students conducted by a magazine in 1996, 24% of the people who replied claimed to have sexual fantasies involving bondage. This was claimed by 40% of the homosexual and bisexual men, 32% of the lesbian and bisexual women, 24% of the heterosexual women and 21% of the heterosexual men. 48% of the lesbian and bisexual women, 34% of the homosexual and bisexual men, and 25% of the heterosexual men and women had had practical experiences of bondage. In a survey conducted in the USA in 1985, about half of the men considered bondage erotic, but according to the 1993 publication Janus Report on Sexual Behavior, only 11% of the representatives had had practical experiences of bondage.


== Types ==

Because of the diversity in its forms, bondage can be divided into various different types based on its motivation.


=== Bondage for a purpose ===
This form of bondage is the best known in BDSM, and denotes restraining the passive partner for an ulterior purpose, such as making them more accessible for a spanking session. Bondage for its own sake is not considered in this category.


=== Decorative bondage ===
In this form of bondage, the restrained partner is bound for a decorative purpose, to be used as an aesthetic object, for example for erotic photography, or a form of human furniture in a BDSM party.


=== Torture bondage ===
In this form of bondage, the restrained partner is purposefully bound in an uncomfortable or painful position, for example as a punishment in connection of a dominant/submissive sexual play. Almost any form of bondage, when the restrained partner is left tied up long enough, can be used as torture bondage. How long this punitive form of bondage is used for varies greatly, however in bondage erotica such as John Willie's Sweet Gwendoline or Japanese bondage photography, it is often extensive and long-lasting.


=== Film bondage ===
Film bondage is a form of completely non-violent bondage for aesthetic purposes only. In this form of bondage, the restrained partner is bound lightly and is capable of escaping without great effort.


=== Meditative bondage ===
This form of bondage is seldom used in western bondage. However, in Japanese bondage (Japanese: shibari), it is an important aspect, possibly originally having evolved from a religious tradition, where the interest is in the restrained partner's spiritual situation rather than their corporal situation.


=== Metal bondage ===

Metal bondage is bondage involving the use of metal apparatus to restrain a submissive as part of BDSM activities. In contrast to the use of rope to secure a body, metal bondage normally requires careful preparation, since more extravagant devices have either to be built or bought from a specialist. The range of devices can vary significantly, from simple handcuffs or chains to specifically designed chairs or complex bar linkages. Chains are occasionally preferred for suspension due to their strong nature and ability to hold large amounts of weight including various other objects on top of the person being suspended. Because they feel cold to touch and rattle and clank chains add auditory and tactile sensory stimulation to the bondage scene, and can similarly be used to enhance the atmosphere of a dungeon.


== Public ==

A subculture of gay men, sometimes called leathermen, were among the first groups to make obvious hints of their tastes in bondage in public. Other groups, including pansexual and heterosexual BDSM enthusiasts, later followed suit. Early public displays were mainly limited to the wearing of certain fashion items, such as collars and cuffs.
Over time, more explicit public displays arose. The most prominent examples are LGBT street fairs, such as the famous Folsom Street Fair. These events are few in number and highly controversial in most regions.
Exhibitionist displays are another manifestation of public bondage. They are typically undertaken by individuals who fetishize public displays of sex and sexuality. However, some exhibitionist bondage is done as a social or political statement. This could be an effort to raise awareness of alternative sexuality or a political metaphor for oppression.
BDSM clubs feature semi-public bondage. While the clubs and events are considered private, play parties feature open spaces where play occurs that allows other attendees to watch scenes in progress. Public play of this variety is more rooted in social activity and the safe space afforded by such clubs than exhibitionist fetishism.


== BDSM ==

Bondage features prominently in BDSM scenes and sexual roleplay. It is the best known aspect of BDSM even outside the BDSM scene, and does not require a BDSM-oriented sexual identity to practice. Even so-called "vanilla" people can become masters of the technical aspects of tying their partners up.
Bondage has a sexual appeal to people of all sexes and all sexual orientations, in a switch, dominant (top) or submissive (bottom) role.
There are also some common fantasy settings in which bondage may be a component. These include:

Rape, ravishment or abduction: The top fictitiously seizes or abducts the consenting bottom and has complete control.
Dominance/submission: A training session occurs in which rewards for obedience and punishment for defiance are given. Humiliation is usually involved.
Predicament bondage: In it a person is restrained with an option of placing themself in one of a pair of uncomfortable positions, such that the person keeps shifting between the two positions from time to time.Self-bondage is more complex, and may involve special techniques to apply bondage to oneself, and also to effect a release after a lapsed period of time. Self-bondage is also notably risky: see the safety notes below. 

A large variety of bondage equipment is available for use in BDSM scenes for a number of results. These include rope, straps, or harnesses which can be used to hold limbs together; spreader bars, X-frames which can be used to keep limbs apart; the body or limbs can be tied to an object, such as to chairs or stocks; the body may be suspended from another object, as in suspension bondage; or it may used to restrict normal movement, such as use of hobble skirts, handcuffs, or pony harness. Bondage may also be used to wrap the whole body or a part of it in bindings, such as cloth or plastic (saran wrap or cling film "mummification") as well as sleepsack bondage.
One of the purposes of bondage in BDSM is to restrain a person (typically called the bottom) in a BDSM position. This may involve simply tying the hands together in front or behind. Other positions involve the use of a waist belt to anchor the hands to the front, back or sides. Other popular positions are the spread eagle, with the limbs splayed out and fastened by wrists and ankles to bedposts, door frame or some other anchoring point; the hogtie, which secures each wrist to its corresponding ankle behind the back (wider, padded restraints such as bondage cuffs are recommended for this); the balltie, which secures wrists to ankles, in front, with the knees drawn up to the chest; the crotch rope, which involves pulling a rope between the labia to apply pressure to the female genitals. Sometimes a knot is placed in the rope at the position of the clitoris to intensify the sensation. A crotch rope can also be used on males, either placing pressure directly on the scrotum or including a tie to capture the scrotum. Other positions include the reverse prayer position (not recommended unless the subject has flexible shoulders), and an over-arm tie, in which the arms are brought over the head, and the wrists fastened together behind the head and then by a length of rope, chain or strapping to a belt at the waist.
The types of restraints used in bondage include rope, which is often preferred because of its flexibility. Rigging, however, requires considerable skill and practice to do safely. Other types of restraints include chains, handcuffs, thumbcuffs and belly chains. Institutional restraints, such as straitjackets may be used in some roleplays, and purpose-made bondage gear, such as monogloves, sleepsacks, bondage hooks and bondage tables, are also available.
Some BDSM play parties offer "bondage workshops", where couples, or people otherwise consenting with each other, can practice tying under the instruction and supervision of an experienced bondage rigger.


== Safety ==
Bondage is safer when conducted between sober, trusted partners who are fully aware of the risks involved and the precautions necessary to ensure safety, such as informed consent. Partners who are in committed relationships may have a greater basis for trusting each other. Performing acts in a supervised location, such as a dungeon, or with a group of trusted friends may also increase safety.There is also a subculture of people who seek out others interested in bondage and pursue such activities with people who they do not know well. This subculture has given rise to Safe, Sane & Consensual.


=== Precautions ===
Safety precautions include:
The use of a "safeword", or some clear way for the subject to indicate genuine distress and a wish to discontinue, temporarily stop or vary the activities of the play.
Never leaving a bound person alone.
Avoiding positions or restraints which may induce postural asphyxia.
Making sure that the subject changes positions at least once an hour (to avoid circulation problems).
Making sure that the subject can be released quickly in an emergency.
Avoiding restraints which impair breathing (gags or hoods which block the mouth can become asphyxial hazards if the subject vomits or the nose becomes otherwise blocked).
Remaining sober; alcohol and drugs should be avoided.
Accidents and lasting damage can generally be avoided by very simple security precautions and a rudimentary knowledge of the human anatomy. One very simple safety measure is to ask the subject every so often if he or she is all right. Another is to check body parts like hands and feet for numbness or coldness, which can happen if nerves have been pinched or blood circulation has been blocked. Another is to check for skin discoloration. Skin that does not get enough oxygen turns bluish. If blood can get in, but cannot get out because one of the veins has been blocked, that part of the body turns purple.
If the subject has been gagged or can otherwise not verbally communicate, a different form of the safeword is needed. For instance, they may hum a simple tune, or opening and closing one or both hands repeatedly, or releasing an object held in one hand (such as a rubber ball, or a scarf).
Suspension bondage by hanging upside-down can be especially risky. The danger most often associated with it is falling on your head.  Other dangers include nerve compression, circulation problems and fainting due to increase in blood pressure.


=== Preparations ===
Some simple preparations may also be helpful:

Food. It is common for people (especially those on diets) to faint during a long session. Having a regular meal beforehand is recommended; being fed small snacks during play may also help prevent fainting.
Cutting tools. A pair of EMT scissors is recommended (useful for safely cutting rope and tape off skin).
Keyed-alike padlocks, if chains are being used.Scenes depicted in bondage photographs and videos are chosen for their visual appeal and fantasy value. Sometimes these positions are dangerous or cannot be maintained for more than a few minutes (i.e., "don't try this at home") such as inverted bondage or suspension from the wrists and ankles. In many cases they cannot be "acted out" with good results and are only for extremely physically fit and very experienced BDSM participants. Especially in highly artistic Japanese bondage, years of experience of bondage is required to avoid the risks.
Self-bondage carries a higher risk, particularly because it violates an important principle of bondage safety; to never leave a bound person alone. For the feeling of being tied up to be as authentic as possible, practitioners of self-bondage can use time-limit clocks, freeze their keys in blocks of ice, or use self-invented devices, in order to temporarily abandon power over their own restraint and freedom (called "tunnel play").
Without someone to release them in the event of an emergency or medical crisis, self-bondage can lead to severe and permanent physical damage. Especially in combination with asphyxiation, self-bondage can be lethal to its practitioners.


== Techniques ==

Bondage techniques can be divided into six main categories:

Binding body parts, such as arms or legs, together.
Spreading out body parts, such as arms or legs.
Binding the restrained partner to an outside object, such as a Saint Andrew's cross, a chair, or a table.
Suspending the restrained partner from the ceiling.
Hindering or slowing down the movement of the restrained partner, such as with a hobble skirt or a corset.
Wrapping the restrained partner up in soft, elastic material, thus restraining their entire body. This is known as mummification.Many people feel that bondage must be "rough and tough", as seen in many images of bondage erotica, but this is not always true. In so-called "soft bondage", the active partner can simply hold the restrained partner's hands together with their own hands, handcuff the restrained partner, or simply order the restrained partner not to move their hands, without using any physical restraint. This latter case, called "verbal bondage", appeals to many people and is far more common than most people think.
A popular variant of bondage considers the artistic and aesthetic part of tying a person up more than the transfer of power and control, or the sexual pleasure. This type of bondage is called "shibari" or "kinbaku", and comes originally from Japanese bondage. Shibari / kinbaku can be practiced either in the traditional Japanese way or in conjunction with western bondage.


=== Techniques of rope bondage ===
Rope bondage is perhaps the best known and most used form of bondage. There are several forms of rope bondage.

Rope wrapping
The rope is simply wrapped around the restrained partner's upper body, or in some cases, his/her entire body.
Rope weaving
A more complicated technique, where a rope is first wrapped around the restrained partner's body in a zig-zag pattern, and then a second rope is woven around it.
Double rope technique
The restrained partner is bound by two ropes at once, allowing decorative rope patterns to be applied quickly.
Single rope technique
A sure technique, most often used in conjunction with sadomasochism, where the restrained partner is bound by only one rope.


=== Terms used in bondage ===

In the American-European bondage scene, specific terms have developed for different kinds of bondage. The terms most often used in the European bondage subculture are in English, although some bondage term come from the Japanese language, such as kata (bondage position) or musubime (bondage knot). Examples of simple bondage techniques are the "spread eagle", where the restrained partner's each limb is tied to a different corner of a bed, or the "hogtie", where the restrained partner's hands and feet are both tied, and the ropes tying these are connected with each other, holding the restrained partner in a bent position. A "crotch rope" is sometimes used, where a rope is passed between the partner's legs, applying pressure to the genitals.


== Materials ==
Just about any material that can be used to hinder or restrain a person's movement can be used in bondage. Bondage can be performed with everyday objects or specially designed BDSM equipment.
In less BDSM-oriented vanilla bondage everyday objects, such as silk scarves, stockings, neckties and belts are often used. Soft objects such as these can also be used for binding in front of the restrained partner's eyes, temporarily blinding them.
"Verbal bondage" is bondage without physical restraint, the subject is simply given orders not to move, or only move in a restrained fashion.


=== Rope ===

Rope is very often used in bondage as a material for physical restraint. In the western world, almost any kind of rope can be used for bondage, such as cotton, artificial fibers, or other materials. However, in Japanese bondage (shibari), only ropes made of hemp or jute are usually used. In Japanese bondage, the rope is usually well prepared before it can be used, so that it becomes soft and easily bendable. Between bondage sessions, the rope is cleaned by washing it.
Regardless of the type of rope used for bondage, temporary superficial damage to the skin can appear where the rope has applied pressure, this is called "rope burns". In emergency situations, EMT scissors provide a quick way to free the restrained partner, however this destroys the rope.


=== Metal ===

Metal (especially steel) is often used in bondage, most often in handcuffs, legcuffs, thumbcuffs, hooks and chains, and is often combined with other materials. Apart from the durability of metal and the flexibility of metal items, metal appeals to many people because of its tough image and the sheer toughness of being bound by metal items. Metal is practically immune to struggling and escape. In BDSM erotica, metal chains are often associated with historical torture and prison scenarios.Apart from pure metal chains, cable ties are also often used as a quick way to tie someone up securely. These ties can leave burn marks on the skin when applied too tightly, and because of their durability and resistance to struggling, must be handled carefully when applied to joints or being left in place for a longer time.


=== Leather, latex and polish ===

Products made of leather are often used in bondage because of their flexibility and resistance to tearing. Because leather is easy to acquire, care for and work upon, it is one of the most popular materials for home-made bondage items. Many people have a fetish for leather, latex and polish and use these materials in connection with bondage, for example for cuffs, belts or neck bands. There are special bondage skirts, trousers or sacks made of these materials, as well as clothing and equipment for BDSM role play that can be used in connection with bondage. For example, harnesses are used in pony play, but would not be used without the corresponding role play.


== Bondage erotica ==

Some people regard bondage to be erotically stimulating or sexually arousing. Bondage features in some sexual fantasy scenarios. Bondage themes are present in some erotica and pornography.
Bondage pornography for heterosexual men almost overwhelmingly depicts bound women, rather than bound men, despite the fact that one of the most common fantasies in both sexes is being bound.
Bondage fantasies often involve dressing in a role outfit. Typical outfits for the submissive person invoke common icons of passivity or sexual innocence (e.g. a shepherdess, nun or schoolgirl outfit for women, or a leather slave harness and cuffs, thong, or ancient slave outfit for men). In a similar respect, the dominant person's attire often reflect images of power, control, and extreme discipline (a Nazi officer, military officer, police or prison warden uniform).


== Spiritual aspects ==
Some people who have been put into long-term deep bondage (mummification) have reported having out-of-the-body experiences and there are some who desire to be put in deep bondage for this reason.  A New Age form of bondage is being immersed a sensory deprivation tank for the express purpose of having an out-of-the-body experience as was practiced while on ketamine by John C. Lilly.


== In art and popular culture ==

Bondage is also presented in erotic and mainstream literary forms, including cartoons and magazines, and depicted in erotic art, such as some of the works of Gustave Doré and John Everett Millais. The mythical Andromeda was a popular subject for bondage in art by painters including Rembrandt's Andromeda Chained to the Rocks (1630), Théodore Chassériau (1840), Edward Poynter (1869) and Gustave Doré (1869).
Other popular scenarios for bondage in art was that of Angelica from the 15th century epic poem Orlando Innamorato, which is itself a continuation of the romantic epic saga Orlando Furioso, which is similar to that of Andromeda in that the heroine is offered as a sacrifice to the sea gods; and the damsel in distress theme. The damsel in distress theme was also used in the motion picture serial The Perils of Pauline (1914), which found Pearl White in mortal danger on a weekly basis.
Depictions of bondage in art may be erotic, in which case they follow a heteronormative model and tend to depict a young woman in danger and fear. Examples can be found in Bizarre, which was a fetish and bondage magazine published between 1946–1959 by bondage artist John Willie. It included drawings and photographs using professional models in bondage or sadomasochistic scenes. 
Sweet Gwendoline was the main female character in John Willie’s works, and possibly the most famous bondage icon after Bettie Page. Sweet Gwendoline was repeatedly depicted as the stereotypical naïve blonde damsel in distress. A serial about that character was published in Robert Harrison’s mainstream girlie magazine Wink from June 1947 to February 1950 and later in several other magazines over the years. Bettie Page was the first famous bondage model. Specifically, from late 1951 or early 1952 through 1957, she posed for photographer Irving Klaw for mail-order photographs with pin-up and BDSM themes, making her the first famous bondage model.
Bondage received a positive (if brief) treatment in The Joy of Sex, a mainstream sex manual popular in the 1970s. 
The Claiming of Sleeping Beauty was an erotic trilogy published in the 1980s by Anne Rice which contained bondage scenarios, as part of a wide range of BDSM acts.
By the 1990s, references to bondage could be found in mainstream prime-time television series such as Buffy the Vampire Slayer, where equipment such as handcuffs or collars and concepts such as the safeword were included as a matter of course.
The publication of Madonna's book, Sex (1992), which included photographs of bound nudes, did a great deal to improve public awareness of the acceptance of bondage.
Other examples of bondage erotica include Anne Desclos’s Story of O (published under the pen name Pauline Réage), Frank E. Campbell's books and the artwork of Robert Bishop.
The Fifty Shades trilogy by E.L. James was first published in ebook in 2011 and subsequently went on to become a bestseller. The print edition of the first book, Fifty Shades of Grey was published in 2012 and became the fastest selling bestseller, breaking multiple sales records. The trilogy revolves entirely around a fictional BDSM relationship, though it is widely considered a poor representation of BDSM relationships by those in the BDSM scene.


== See also ==


== References ==


=== Bibliography ===


=== Notes ===


== External links ==
Unrealities.com, soc.subculture.bondage-bdsm FAQ and its mirror at Sexuality.org
Wipipedia – A bondage Wiki