In the common law tradition, a heartbalm tort or heartbalm action is a civil action that a person may bring to seek monetary compensation for the end or disruption of a romantic or marital relationship.  A heartbalm statute is a statute abolishing such actions.Heartbalm actions in the United States typically include seduction, criminal conversation, alienation of affection, and breach of promise to marry. Of these, criminal conversation and alienation of affection are marital torts, originally restricted to husbands but in many states later made available to spouses regardless of gender. Seduction and breach of promise are nonmarital torts.In England and other common law jurisdictions, additional heartbalm actions were traditionally recognized, such as enticement and wrongful harbouring (tortious refusal to allow a husband to visit a wife who has left him). A claim for damages based on loss of consortium is also sometimes considered a heartbalm action in England and elsewhere.In the United States, heartbalm actions were widespread until high-profile stories in the early 20th century about heartbalm claims being abused for blackmail and extortion led to calls for repeal.  The first state to abolish all heartbalm actions was Indiana, with “An Act to promote public morals” in 1935. By 1952, 16 more states had followed its example.  Many states that abolished other heartbalm torts retained the tort of seduction, however; of the ten states that had abolished heartbalm actions by 1938, four allowed minors to sue for seduction and three more kept the tort of seduction intact.Following a report by the Law Reform Committee in 1963, England abolished all of the traditional heartbalm torts (excluding loss of consortium) by statute in 1970.In the United States, as of 2016, eight states still allow heartbalm actions: Hawaii, Mississippi, Missouri, New Hampshire, New Mexico, North Carolina, South Dakota, and Utah.  However, such actions are uncommon even where they are still allowed.


== Works cited ==
Brode, Patrick (2002). Courted and Abandoned: Seduction in Canadian Law. ISBN 9780802037503.CS1 maint: ref=harv (link)
Ontario Law Reform Commission (1969). Report on Family Law, Part I: Torts.


== See also ==
Heart balm (German law)


== References ==