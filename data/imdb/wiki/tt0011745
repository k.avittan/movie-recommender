Sweet Lavender is a play in three acts by Arthur Wing Pinero, first performed in 1888.  The sentimental and gently humorous story concerns the romance of Lavender Rolt and Clement Hale, and the complications impeding the course of true love.


== Premiere ==
The original production opened on 21 March 1888 at Terry's Theatre in London, starring Edward Terry as Dick Phenyl, Brandon Thomas as Geoffrey Wedderburn, Carlotta Addison as Mrs Rolt, and Rose Norreys and Harry Reeves-Smith in the romantic leads.  The play was so popular that it ran for 684 performances – a conspicuously  long run for the time – until 25 January 1890; on its revival less than a year later it ran for a further 737 nights.The play was produced over the next few years in Australia, Canada, Germany, India, Italy, Russia, South Africa, the United States, and the West Indies. The Broadway production opened in November 1888 at the Lyceum Theatre with W J LeMoyne and  Henry Miller  in the cast. The Australian premiere,  in 1889, was presented by and starred Frank Thornton


== Original cast ==

Mr Geoffrey Wedderburn (of Wedderburn, Green and Hoskett, Bankers, Barnchester)  – Brandon Thomas
Clement Hale (his adopted son, studying for the bar) – Harry Reeves-Smith
Dr Delaney (a fashionable physician) – Alfred Bishop
Dick Phenyl (a barrister) – Edward Terry
Horace Bream (a young American) – F Kerr
Mr Mew (a solicitor) – Sant Matthews
Mr Bodger (hairdresser and wig maker) – T C Valentine (later in the run, Prince Miller)
Mrs Gilfillian (a widow – Mr Wedderburn's sister) – Miss M A Victor
Minnie (her daughter) – Maude Millett
Ruth Rolt (housekeeper and laundress at Brain Court, Temple) – Carlotta Addison
Lavender (her daughter) – Rose Norreys (later in the run, Blanche Horlock)


== Synopsis ==
The play is set in the Chambers of Mr Phenyl and Mr Hale, No 3, Brain Court, Temple. Springtime. The present day.
Act 1 – Morning – "Nobody's business"
Act 2 – Evening of the next day – "Somebody's business"
Act 3 – A Week afterwards – "Everybody's business"Ruth Rolt, a housekeeper, has a daughter, Lavender, who is in love with Clement Hale,  who is studying for the bar under a disreputable but lovable barrister, Dick Phenyl.  Clement is the adopted son of Geoffrey Wedderburn, a banker, who wants him to marry his niece, Minnie.  Minnie has another suitor, Horace, an American.  Mrs. Rolt discovers that Wedderburn is not only Clement's adoptive father but also Lavender's real father.  She is very angry and breaks off the relationship between Clement and Lavender.
Wedderburn's bank is threatened with failure; he takes ill and learns humility.  Mrs. Rolt forgives and nurses him, while Lavender flies back into Clement's arms to console him after the financial ruin.  Phenyl unexpectedly offers his own fortune to save the bank and Wedderburn's reputation, and all ends happily.


== Revivals and adaptations ==
The play was revived at Terry's in September 1890, and in February 1899. It was revived at the Ambassadors in December 1922, and at the Lyric, Hammersmith in June 1932. On Broadway the play was revived in 1905 (with Terry in his original role), and again in 1923.


== Notes ==


== References ==
Clark, Barrett H. (1915). The British and American Drama of Today. New York: Henry Holt.
Gaye, Freda (ed.) (1967). Who's Who in the Theatre (fourteenth ed.). London: Sir Isaac Pitman and Sons. OCLC 5997224.CS1 maint: extra text: authors list (link)
Salaman, Malcolm C (1893). "Introductory Note". Sweet Lavender. Boston: Walter H Baker. OCLC 3879011.