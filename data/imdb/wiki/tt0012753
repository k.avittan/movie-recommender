Seven Mary Three, (occasionally abbreviated to 7 Mary 3 or 7M3) was an American rock band. They released seven studio albums and one live album, and are best known for their hit single "Cumbersome".


== Career ==


=== Formation ===
Seven Mary Three formed in 1992 when Jason Ross and Jason Pollock met while attending The College of William & Mary in Williamsburg, Virginia. Ross and Pollock split songwriting duties with Ross on lead vocals and rhythm guitar and Pollock on lead guitar and backing vocals. Bassist Casey Daniel and drummer Giti Khalsa joined the two, and the band began playing in coffeehouses and clubs.


=== Origin of group name ===
Jason Pollock revealed in The Cavalier Daily that they came up with the name while watching the 1980s TV series CHiPs. '7 Mary 3' was the call sign for Officer Jon Baker, who was played by actor Larry Wilcox. (7M3: police radio call sign; 7 designates the patrol beat, M for Mary designates that he is a motorcycle unit and 3 is his unit number.) Pollock noted, "There's no great significance or anything. We were just tired of trying to think of a cool name."


=== Mainstream success ===
1994's album Churn, a self-produced independent release garnered the band airplay on an FM rock station in Orlando, Florida for the future hit single "Cumbersome".  Given this minor success, the band relocated to the Orlando area where they continued to expand their fan base.  This regional success soon caught the attention of major-label scouts. In May 1995, Ross, Pollock, and Khalsa graduated from William & Mary and moved to Florida. The band signed with Mammoth and rerecorded the songs on Churn, plus two new ones, to create the commercially successful American Standard in 1995. Despite some critics accusing the band of mimicking Pearl Jam and other alternative rock acts, only seven months after its release, American Standard achieved platinum status. This accomplishment can be attributed to the success of "Cumbersome," which was a Top 40 hit, as well as another  single, "Water's Edge", which fared well on the rock charts.
After touring throughout 1996, the band returned to the studio for a follow up to American Standard. During this time, Mammoth and Atlantic split, forcing Seven Mary Three to sign with Atlantic Records,  releasing RockCrown in 1997. The album saw the band deemphasize hard rock, focusing more on acoustic folk rock and a "traditional singer/songwriter" style. RockCrown did not match fan expectations, reaching No. 75 on the Billboard 200 and failing to match the success of its predecessor.
A second effort under Atlantic, Orange Ave. debuted the following year and charted considerably lower; although, its single "Over Your Shoulder" performed moderately. In an interview with Rolling Stone, Giti Khalsa explained the differences between the albums as a result of the band's maturity and position in life:

We made American Standard when we were fresh out of college, and it represented that time. With Rock Crown, it was very much a response to going from playing bars and fraternities to getting a record deal to selling a million records in a year. And Orange Ave. is a response to the last few years and us being a little further away than at the beginning and being able to look back and go, 'Okay, I get it now.'"
In 1999, Jason Pollock left the band after suffering from burnout and writer's block. Thomas Juliano was chosen as the band's new guitarist. In the summer of 2001, Seven Mary Three returned to Mammoth Records and producer Tom Morris. The resulting effort became The Economy of Sound. This fifth studio album included the single "Wait", a track that served as the lead single from the Crazy/Beautiful film soundtrack.
After The Economy of Sound, the group shifted once again to DRT Entertainment and, in 2004, released Dis/Location. Their sixth studio album, it failed entirely to chart as did its only single. Four years later, a seventh album, Day & Nightdriving, was released under Bellum Records.
Giti Khalsa left the band in 2006 after recording the drum tracks for Day & Nightdriving, and Mike Levesque joined the band on drums.
In December 2008, the group re-released their long out-of-print debut album Churn. On February 9, 2010, Seven Mary Three released the live acoustic album Backbooth.
Seven Mary Three broke up suddenly in December 2012, without any announcements or updates to their website or Facebook page. The following year, Jason Ross became the head of media and strategic partnerships for The Bowery Presents, one of New York's most powerful music production companies.


== Band members ==
Jason Ross - lead vocals, rhythm guitar  (1992-2012)
Jason Pollock - lead guitar, backing vocals  (1992-1999)
Casey Daniel - bass  (1992-2012)
Giti Khalsa - drums  (1992-2006)
Thomas Juliano - lead guitar, backing vocals  (1999-2012)
Mike Levesque - drums  (2006-2012)


=== Timeline ===


== Discography ==


=== Studio albums ===


=== Live albums ===


=== Singles ===


=== EPs ===
B-Sides & Rarities (1997) Seven-cut promo EP issued by Atlantic Recording Corp. and Mammoth Records. PRCD 8339-2
Weed, CA (2002)
Welcome Race Fans (2003)


=== Compilation and soundtrack contributions ===
"Shelf Life" – The Crow: City of Angels (1996 soundtrack)
"Blackwing" – MOM: Music for Our Mother Ocean (1996 Surfrider Foundation benefit album)
"My, My" – Milk It for All It's Worth (1996 compilation)
"My, My" – ESPN Presents X Games Volume 1: Music from the Edge (1996 compilation)
"Blackwing" – Hurricane Streets (1998 soundtrack)
"Lucky" – Pepsi Pop Culture (1998 compilation)
"Lucky" – 101.5 KZON The Zone Collectibles: Volume Six (1998 compilation)
"Wait" – Crazy/Beautiful (2001 soundtrack)
"Cumbersome" – Live in the X Lounge IV (2001 charity album)
"Sleepwalking" – 8 Ways to Rock: A Mammoth Records Thriller (compilation)
"Laughing Out Loud" – Paste Magazine Issue 40 CD Sampler (2008 compilation)


== References ==


== External links ==
Official website