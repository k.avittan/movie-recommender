Rebecca of Sunnybrook Farm is a classic American 1903 children's novel by Kate Douglas Wiggin that tells the story of Rebecca Rowena Randall and her aunts, one stern and one kind, in the fictional village of Riverboro, Maine.  Rebecca's joy for life inspires her aunts, but she faces many trials in her young life, gaining wisdom and understanding. Wiggin wrote a sequel, New Chronicles of Rebecca. Eric Wiggin, a great-nephew of the author, wrote updated versions of several Rebecca books, including a concluding story. The story was adapted for the theatrical stage and filmed three times, once with Shirley Temple in the title role, as well as a Japanese animated short as part of the Anime Tamago project.


== Synopsis ==
The novel opens with Rebecca's journey from her family's farm to live with her two aunts, her mother's older sisters Miranda and Jane Sawyer, in their brick house in Riverboro. Rebecca is the second-oldest of seven children. Most of the children have fanciful names, such as Marquis and Jenny Lind, influenced by their father's artistic background; Rebecca is named after both the heroines in Ivanhoe. The family is quite poor, due to the number of children, Mr. Lorenzo DeMedici Randall's inability to stick to a job, and the farm being mortgaged. As the novel begins, the family is barely scraping by three years after the father's death. Rebecca's stay with her aunts is a chance to improve her opportunities in life and to ease the strain on her family's budget. Despite her impoverished background, Rebecca is imaginative and charming.  She often composes little poems and songs to express her feelings or to amuse her siblings. It is she who named their farm "Sunnybrook".
Miranda and Jane had wanted Hannah, the eldest sister, because of her household skills and pragmatic nature, but her mother needs her at home for the same reasons and sends Rebecca instead. Miranda is unimpressed by Rebecca's imagination, chatter, and dark complexion, calls her the image of her shiftless father, and determines to train Rebecca to be a proper young lady who won't shame the Sawyer name. Jane becomes Rebecca's protector and acts as a buffer between her and Miranda. Jane teaches Rebecca to sew, cook, and manage a household. Rebecca's liveliness and curiosity brighten Jane's life and refresh her spirit. Although Rebecca strives to win Miranda's approval, she struggles to live up to Miranda's rigid standards, as Miranda views her as "all Randall and no Sawyer." 
The middle of the novel is mainly a description of life at Riverboro and its inhabitants. Important characters are Jeremiah and Sarah Cobb, who first encounter Rebecca's charm; Rebecca's schoolmate and best friend, Emma Jane Perkins; and young businessman Adam Ladd, who takes an interest in Rebecca's education. Adam meets Rebecca when she and Emma Jane are selling soap to help a poor family receive a lamp as a premium; Rebecca nicknames him "Mr. Aladdin." 

Rebecca proves to be a good student, especially in English, and goes on to attend high school in Wareham. In the book's last section she has become a young lady with the same high spirit and a talent for writing. She applies for a teaching job in Augusta, but her mother has an accident and Rebecca must go home to look after her and the farm. While she's away, Miranda dies and leaves the Sawyer house and land to Rebecca. A railway company will buy Sunnybrook Farm for construction purposes, which will give the Randall family a sufficient living, and Miranda's will provides Rebecca enough money to become an independent woman who can help her siblings. The novel ends with her exclaiming, "God bless Aunt Miranda! God bless the brick house that was! God bless the brick house that is to be!"


== Adaptations ==


=== Play ===

Rebecca of Sunnybrook Farm was dramatized for the theater in 1909. Wiggin co-wrote the play with Charlotte Thompson. It was produced for Broadway by Klaw & Erlanger in 1909. Before opening on Broadway, it toured Boston and New England where it was warmly received.


=== Film ===
The story was filmed three times. Shirley Temple played Rebecca in the more freely interpreted adaptation of 1938.

Rebecca of Sunnybrook Farm (1917 film)
Rebecca of Sunnybrook Farm (1932 film)
Rebecca of Sunnybrook Farm (1938 film)


=== Animation ===
An anime short film was made in 2020. 
https://www.animenewsnetwork.com/news/2019-12-28/anime-tamago-2020-shorts-reveal-visuals-in-promo-video/.154851


== References ==


== External links ==

 Rebecca of Sunnybrook Farm at Project Gutenberg
 Rebecca of Sunnybrook Farm public domain audiobook at LibriVox
Wiggin, Kate Douglas (1995). Rebecca of Sunnybrook Farm (reissue ed.). Puffin. ISBN 0-14-036759-4.