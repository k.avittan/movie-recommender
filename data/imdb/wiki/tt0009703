"Tinker Tailor" is a counting game, nursery rhyme and fortune telling song traditionally played in England, that can be used to count cherry stones, buttons, daisy petals and other items. It has a Roud Folk Song Index number of 802.  It is commonly used by children in both Britain and America for "counting out", e.g. for choosing who shall be "It" in a game of tag.


== Lyrics ==
The most common modern version is:

Tinker, Tailor,
Soldier, Sailor,
Rich Man, Poor Man,
Beggar Man, Thief.The most common American version is:

Rich Man, Poor Man,
Beggar Man, Thief,
Doctor, Lawyer, (or "Merchant")
Indian Chief.


== Origins ==
A similar rhyme has been noted in William Caxton's, The Game and Playe of the Chesse (c. 1475), in which pawns are named: "Labourer, Smith, Clerk, Merchant, Physician, Taverner, Guard and Ribald."The first record of the opening four professions being grouped together is in William Congreve's Love for Love (1695), which has the lines:

A Soldier and a Sailor, a Tinker and a Tailor,
Had once a doubtful strife, sir.When James Orchard Halliwell collected the rhyme in the 1840s, it was for counting buttons with the lines:
"My belief – a captain, a colonel, a cow-boy, a thief." The version printed by William Wells Newell in Games and Songs of American Children in 1883 was: "Rich man, Poor man, beggar-man, thief, Doctor, lawyer (or merchant), Indian chief", and it may be from this tradition that the modern American lyrics solidified.


== Alternative versions ==
A. A. Milne's Now We Are Six (1927) had the following version of "Cherry stones":

Tinker, tailor, soldier, sailor, rich man, poor man, beggar man, thief,
Or what about a cowboy, policeman, jailer, engine driver, or a pirate chief?
Or what about a ploughman or a keeper at the zoo,
Or what about a circus man who lets the people through?
Or the man who takes the pennies on the roundabouts and swings,
Or the man who plays the organ or the other man who sings?
Or what about the rabbit man with rabbits in his pockets
And what about a rocket man who's always making rockets?
Oh it's such a lot of things there are and such a lot to be
That there's always lots of cherries on my little cherry tree.The "tinker, tailor" rhyme is one part of a longer counting or divination game, often played by young girls to foretell their futures; it runs as follows:

When shall I marry?
This year, next year, sometime, never.
What will my husband be?
Tinker, tailor, soldier, sailor, rich-man, poor-man, beggar-man, thief.
What will I be?
Lady, baby, gypsy, queen.
What shall I wear?
Silk, satin, cotton, rags (or silk, satin, velvet, lace) (or silk, satin, muslin, rags) 
How shall I get it?
Given, borrowed, bought, stolen.
How shall I get to church?
Coach, carriage, wheelbarrow, cart.
Where shall I live?
Big house, little house, pig-sty, barn.During the divination, the girl will ask a question and then count out a series of actions or objects by reciting the rhyme. The rhyme is repeated until the last of the series of objects or actions is reached. The last recited term or word is that which will come true. Buttons on a dress, petals on a flower, bounces of a ball, number of jumps over a rope, etc., may be counted.


== Notes ==


== Further reading ==

Gomme, Alice Bertha. The Traditional Games of England, Scotland, and Ireland. London: David Nutt (1898).
Hazlitt, W. Carew. Faiths and Folklore: A Dictionary of National Beliefs, Superstitions and Popular Customs, Past and Current, With Their Classical and Foreign Analogues, Described and Illustrated (Brand's Popular Antiquities of Great Britain). London: Reeves and Turner (1905).