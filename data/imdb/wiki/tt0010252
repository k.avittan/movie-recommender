The long jump is a track and field event in which athletes combine speed, strength and agility in an attempt to leap as far as possible from a take off point.  Along with the triple jump, the two events that measure jumping for distance as a group are referred to as the "horizontal jumps". This event has a history in the Ancient Olympic Games and has been a modern Olympic event for men since the first Olympics in 1896 and for women since 1948.


== Rules ==

At the elite level, competitors run down a runway (usually coated with the same rubberized surface as running tracks, crumb rubber also vulcanized rubber—known generally as an all-weather track) and jump as far as they can from a wooden board 20 cm or 8 inches wide that is built flush with the runway into a pit filled with finely ground gravel or sand. If the competitor starts the leap with any part of the foot past the foul line, the jump is declared a foul and no distance is recorded. A layer of plasticine is placed immediately after the board to detect this occurrence. An official (similar to a referee) will also watch the jump and make the determination. The competitor can initiate the jump from any point behind the foul line; however, the distance measured will always be perpendicular to the foul line to the nearest break in the sand caused by any part of the body or uniform. Therefore, it is in the best interest of the competitor to get as close to the foul line as possible. Competitors are allowed to place two marks along the side of the runway in order to assist them to jump accurately. At a lesser meet and facilities, the plasticine will likely not exist, the runway might be a different surface or jumpers may initiate their jump from a painted or taped mark on the runway. At a smaller meet, the number of attempts might also be limited to four or three.
Each competitor has a set number of attempts. That would normally be three trials, with three additional jumps being awarded to the best 8 or 9 (depending on the number of lanes on the track at that facility, so the event is equatable to track events) competitors. All legal marks will be recorded but only the longest legal jump counts towards the results. The competitor with the longest legal jump (from either the trial or final rounds) at the end of competition is declared the winner. In the event of an exact tie, then comparing the next best jumps of the tied competitors will be used to determine place. In a large, multi-day elite competition (like the Olympics or World Championships), a set number of competitors will advance to the final round, determined in advance by the meet management. A set of 3 trial round jumps will be held in order to select those finalists. It is standard practice to allow at a minimum, one more competitor than the number of scoring positions to return to the final round, though 12 plus ties and automatic qualifying distances are also potential factors. (For specific rules and regulations in United States Track & Field see Rule 185).For record purposes, the maximum accepted wind assistance is two metres per second (m/s) (4.5 mph).


== History ==

The long jump is the only known jumping event of Ancient Greece's original Olympics' pentathlon events. All events that occurred at the Olympic Games were initially supposed to act as a form of training for warfare. The long jump emerged probably because it mirrored the crossing of obstacles such as streams and ravines. After investigating the surviving depictions of the ancient event it is believed that unlike the modern event, athletes were only allowed a short running start. The athletes carried a weight in each hand, which were called halteres (between 1 and 4.5 kg). These weights were swung forward as the athlete jumped in order to increase momentum. It was commonly believed that the jumper would throw the weights behind him in midair to increase his forward momentum; however, halteres were held throughout the duration of the jump. Swinging them down and back at the end of the jump would change the athlete's center of gravity and allow the athlete to stretch his legs outward, increasing his distance. The jump itself was made from the bater ("that which is trod upon"). It was most likely a simple board placed on the stadium track which was removed after the event. The jumpers would land in what was called a skamma ("dug-up" area). The idea that this was a pit full of sand is wrong. Sand in the jumping pit is a modern invention. The skamma was simply a temporary area dug up for that occasion and not something that remained over time.
The long jump was considered one of the most difficult of the events held at the Games since a great deal of skill was required. Music was often played during the jump and Philostratus says that pipes at times would accompany the jump so as to provide a rhythm for the complex movements of the halteres by the athlete. Philostratus  is quoted as saying, "The rules regard jumping as the most difficult of the competitions, and they allow the jumper to be given advantages in rhythm by the use of the flute, and in weight by the use of the halter." Most notable in the ancient sport was a man called Chionis, who in the 656 BC Olympics staged a jump of 7.05 metres (23 feet and 1.7 inches).There has been some argument by modern scholars over the long jump. Some have attempted to recreate it as a triple jump. The images provide the only evidence for the action so it is more well received that it was much like today's long jump. The main reason some want to call it a triple jump is the presence of a source that claims there once was a fifty-five ancient foot jump done by a man named Phayllos.The long jump has been part of modern Olympic competition since the inception of the Games in 1896. In 1914, Dr. Harry Eaton Stewart recommended the "running broad jump" as a standardized track and field event for women. However, it was not until 1948 that the women's long jump was added to the Olympic athletics programme.


== Technique ==

There are five main components of the long jump: the approach run, the last two strides, takeoff, action in the air, and landing. Speed in the run-up, or approach, and a high leap off the board are the fundamentals of success. Because speed is such an important factor of the approach, it is not surprising that many long jumpers also compete successfully in sprints. A classic example of this long jump / sprint doubling are performances by Carl Lewis.


=== The approach ===
The objective of the approach is to gradually accelerate to a maximum controlled speed at takeoff. The most important factor for the distance travelled by an object is its velocity at takeoff – both the speed and angle. Elite jumpers usually leave the ground at an angle of twenty degrees or less; therefore, it is more beneficial for a jumper to focus on the speed component of the jump. The greater the speed at takeoff, the longer the trajectory of the center of mass will be. The importance of a takeoff speed is a factor in the success of sprinters in this event.
The length of the approach is usually consistent distance for an athlete. Approaches can vary between 12 and 19 strides on the novice and intermediate levels, while at the elite level they are closer to between 20 and 22 strides. The exact distance and number of strides in an approach depends on the jumper's experience, sprinting technique, and conditioning level. Consistency in the approach is important as it is the competitor's objective to get as close to the front of the takeoff board as possible without crossing the line with any part of the foot.
Inconsistent approaches are a common problem in the event. As a result, the approach is usually practiced by athletes about 6–8 times per jumping session (see Training below).


=== The last two strides ===
The objective of the last two strides is to prepare the body for takeoff while conserving as much speed as possible.
The penultimate stride is longer than the last stride. The competitor begins to lower his or her center of gravity to prepare the body for the vertical impulse. The final stride is shorter because the body is beginning to raise the center of gravity in preparation for takeoff.
The last two strides are extremely important because they determine the velocity with which the competitor will enter the jump.


=== Takeoff ===

The objective of the takeoff is to create a vertical impulse through the athlete's center of gravity while maintaining balance and control.
This phase is one of the most technical parts of the long jump. Jumpers must be conscious to place the foot flat on the ground, because jumping off either the heels or the toes negatively affects the jump. Taking off from the board heel-first has a braking effect, which decreases velocity and strains the joints. Jumping off the toes decreases stability, putting the leg at risk of buckling or collapsing from underneath the jumper. While concentrating on foot placement, the athlete must also work to maintain proper body position, keeping the torso upright and moving the hips forward and up to achieve the maximum distance from board contact to foot release.
There are four main styles of takeoff: the kick style, double-arm style, sprint takeoff, and the power sprint or bounding takeoff.


==== Kick ====
The kick style takeoff is where the athlete actively cycles the leg before a full impulse has been directed into the board then landing into the pit. This requires great strength in the hamstrings. This causes the jumper to jump to large distances.


==== Double-arm ====
The double-arm style of takeoff works by moving both arms in a vertical direction as the competitor takes off. This produces a high hip height and a large vertical impulse.


==== Sprint ====
The sprint takeoff is the style most widely instructed by coaching staff. This is a classic single-arm action that resembles a jumper in full stride. It is an efficient takeoff style for maintaining velocity through takeoff.


==== Power sprint or bounding ====
The power sprint takeoff, or bounding takeoff, is one of the more common elite styles. Very similar to the sprint style, the body resembles a sprinter in full stride. However, there is one major difference. The arm that pushes back on takeoff (the arm on the side of the takeoff leg) fully extends backward, rather than remaining at a bent position. This additional extension increases the impulse at takeoff.
The "correct" style of takeoff will vary from athlete to athlete.


=== Action in the air and landing ===
There are three major flight techniques for the long jump: the hang, the sail, and the hitch-kick. Each technique is to combat the forward rotation experienced from take-off but is basically down to preference from the athlete. It is important to note that once the body is airborne, there is nothing that the athlete can do to change the direction they are traveling and consequently where they are going to land in the pit. However, it can be argued that certain techniques influence an athlete's landing, which can affect the distance measured. For example, if an athlete lands feet first but falls back because they are not correctly balanced, a lower distance will be measured.
In the 1970s, some jumpers used a forward somersault, including Tuariki Delamere who used it at the 1974 NCAA Championships, and who matched the jump of the then Olympic champion Randy Williams. The somersault jump has potential to produce longer jumps than other techniques because in the flip, no power is lost countering forward momentum, and it reduces wind resistance in the air. The front flip jump was subsequently banned due to fear of it being unsafe.


== Training ==
The long jump generally requires training in a variety of areas. These areas include: speed work, jumping, over distance running, weight training, plyometric training.


=== Speed work ===
Speed work is essentially short distance speed training where the athlete would be running at top or near top speeds. The distances for this type of work would vary between indoor and outdoor season but are usually around 30–60 m for indoors and up to 100 m for outdoors.


=== Jumping ===
Long Jumpers tend to practice jumping 1–2 times a week. Approaches, or run-throughs, are repeated sometimes up to 6–8 times per session.
Short approach jumps are common for jumpers to do, as it allows for them to work on specific technical aspects of their jumps in a controlled environment. Using equipment such as low hurdles and other obstacles are common in long jump training, as it helps the jumper maintain and hold phases of their jump. As a common rule, it is important for the jumper to engage in full approach jumps at least once a week, as it will prepare the jumper for competition.


=== Over-distance running ===
Over-distance running workouts helps the athlete jump a further distance than their set goal. For example, having a 100 m runner practice by running 200 m repeats on a track. This is specifically concentrated in the season when athletes are working on building endurance. Specific over-distance running workouts are performed 1–2 times a week. This is great for building sprint endurance, which is required in competitions where the athlete is sprinting down the runway 3–6 times. Typical workouts would include 5×150 m. Preseason workouts may be longer, including workouts like 6×300 m.


=== Weight training ===
During pre-season training and early in the competition season weight training tends to play a major role in the sport. It is customary for a long jumper to weight train up to 4 times a week, focusing mainly on quick movements involving the legs and trunk. Some athletes perform Olympic lifts in training. Athletes use low repetition and emphasize speed to maximize the strength increase while minimizing adding additional weight to their frame. Important lifts for a long jumper include the back squat, front squat, power cleans and hang cleans. The emphasis on these lifts should be on speed and explosive as those are crucial in the long jump take off phase.


=== Plyometrics ===
Plyometrics, including running up and down stairs and hurdle bounding, can be incorporated into workouts, generally twice a week. This allows an athlete to work on agility and explosiveness. Other plyometric workouts that are common for long jumpers are box jumps. Boxes of various heights are set up spaced evenly apart and jumpers can proceed jumping onto them and off moving in a forward direction. They can vary the jumps from both legs to single jumps. Alternatively, they can set up the boxes in front of a high jump mat if allowed, and jump over a high jump bar onto the mat mimicking a landing phase of the jump. These plyometric workouts are typically performed at the end of a workout.


=== Bounding ===
Bounding is any sort of continuous jumping or leaping. Bounding drills usually require single leg bounding, double-leg bounding, or some variation of the two.  The focus of bounding drills is usually to spend as little time on the ground as possible and working on technical accuracy, fluidity, and jumping endurance and strength. Technically, bounding is part of plyometrics, as a form of a running exercise such as high knees and butt kicks.


=== Flexibility ===
Flexibility is an often forgotten tool for long jumpers. Effective flexibility prevents injury, which can be important for high-impact events such as the long jump. It also helps the athlete sprint down the runway.
Hip and groin injuries are common for long jumpers who may neglect proper warm-up and stretching.
Hurdle mobility drills are a common way that jumpers improve flexibility. Common hurdle drills include setting up about 5–7 hurdles at appropriate heights and having athletes walk over them in a continuous fashion. Other variations of hurdle mobility drills are used as well, including hurdle skips.
This is a crucial part of a jumper's training since they perform most exercises for a very short period of time and often aren't aware of their form and technique.
A common tool in many long jump workouts is the use of video taping. This enables the athlete to go back and watch their own progress as well as letting the athlete compare their own footage to that of some of the world-class jumpers.
Training styles, duration, and intensity vary immensely from athlete to athlete and are based on the experience and strength of the athlete as well as on their coaching style.


== Culture ==

Track and field events have been selected as a main motif in numerous collectors' coins. One of the recent samples is the €10 Greek Long Jump commemorative coin, minted in 2003 to commemorate the 2004 Summer Olympics. The obverse of the coin portrays a modern athlete at the moment he is touching the ground, while the ancient athlete in the background is shown while starting off his jump, as he is seen on a black-figure vase of the 5th century BC.


== Records ==

The men's long jump world record has been held by just four individuals for the majority of time since the IAAF started to ratify records.  The first mark recognized by the IAAF in 1912, the 1901 performance by Peter O'Connor, stood just short of 20 years (nine years as an IAAF record). After it was broken in 1921, the record changed hands six times until Jesse Owens set the record at the 1935 Big Ten track meet in Ann Arbor, Michigan, of 8.13 m (26 ft 8 in) that was not broken for 25 years and 2 months, until 1960, by Ralph Boston. Boston improved upon it and exchanged records with Igor Ter-Ovanesyan seven times over the next seven years.  At the 1968 Summer Olympics Bob Beamon jumped 8.90 m (29 ft 2 1⁄4 in), a jump not exceeded for 23 years, and which remains the second longest legal jump of all time; yet it has stood as the Olympic record for 52 years. On 30 August 1991, Mike Powell of the United States set the current men's world record at the World Championships in Tokyo.  It was in a dramatic showdown against Carl Lewis who also surpassed Beamon's record that day but his jump was wind-assisted (and thus not legal for record purposes). Powell's record 8.95 m (29 ft 4 1⁄4 in) has now stood for over 29 years.
Some jumps over 8.95 m (29 ft 4 1⁄4 in) have been officially recorded. 8.99 m (29 ft 5 3⁄4 in) was recorded by Powell (wind-assisted +4.4) set at high altitude in Sestriere, Italy, in 1992. A potential world record of 8.96 m (29 ft 4 3⁄4 in) was recorded by Iván Pedroso, with a "legal" wind reading also in Sestriere, but the jump was not validated because videotape revealed someone was standing in front of the wind gauge, invalidating the reading (and costing Pedroso a Ferrari valued at $130,000—the prize for breaking the record at that meet). As mentioned above, Lewis jumped 8.91 m (29 ft 2 3⁄4 in) moments before Powell's record-breaking jump with the wind exceeding the maximum allowed.  This jump remains the longest ever not to win an Olympic or World Championship gold medal, or any competition in general.
The women's world record has seen more consistent improvement, though the current record has stood longer than any long jump record by men or women.  The longest to hold the record prior was by Fanny Blankers-Koen during World War II. who held it for 10 years.  There have been three occasions where the record was tied or improved upon twice in the same competition. The current world record for women is held by Galina Chistyakova of the former Soviet Union who leapt 7.52 m (24 ft 8 in) in Leningrad on 11 June 1988, a mark that has stood for over 32 years.


== Continental records ==


=== Outdoor ===
Updated 1 February 2020.


==== Notes ====


=== Indoor ===
Updated 17 September 2020.


== All-time top 25 long jumpers ==


=== Men (absolute) ===
As of 1 February 2020.


==== Notes ====
Below is a list of all other legal jumps equal or superior to 8.70 m:

Carl Lewis also jumped 8.84 (1991 ancillary jump), 8.79 (1983 & 1984 indoors), 8.76 (1982 & 1988), 8.75 (1987), 8.72 (1988) and 8.71 (1984).
Larry Myricks also jumped 8.70 (1989).
Mike Powell also jumped 8.70 (1993).
Ivan Pedroso also jumped 8.70 (1995).


==== Wind-assisted jumps ====
Any performance with a following wind of more than 2.0 metres per second is not counted for record purposes. Below is a list of the best wind-assisted jumps (equal or superior to 8.52 m). Only marks that are superior to legal bests are shown:

Mike Powell  jumped 8.99 (+4.4) at altitude in Sestriere, Italy on 21 July 1992.
Iván Pedroso  jumped 8.96  (+1.2) at altitude in Sestriere, Italy on 29 July 1995. The jump was ruled invalid due to an obstructed wind-gauge.
Juan Miguel Echevarría  jumped 8.92 (+3.3) in Havana, Cuba on 10 March 2019.
Carl Lewis  jumped 8.91 (+3.0) at the World Championships in Tokyo, Japan on 30 August 1991.
Fabrice Lapierre  jumped 8.78 (+3.1) in Perth, Australia on 18 April 2010.
James Beckford  jumped 8.68 (+4.9) in Odessa, Ukraine on 20 May 1995.
Joe Greene  jumped 8.68 (+4.0) at altitude in Sestriere, Italy on 21 July 1995.
Marquis Dendy  jumped 8.68 (+3.7) in Eugene, Oregon on 25 June 2015.
Kareem Streete-Thompson  jumped 8.64 (+3.5) in Knoxville, Tennessee on 18 June 1995.
Mike Conley  jumped 8.63 (+3.9) in Eugene, Oregon on 20 June 1986.
Jeff Henderson  jumped 8.59 (+2.9) in Eugene, Oregon on 3 July 2016.
Jason Grimes  jumped 8.57 (+5.2) in Durham, North Carolina on 27 June 1982.
Kevin Dilworth  jumped 8.53 (+4.9) in Fort-de-France, Martinique on 27 April 2002.


=== Women (absolute) ===
As of 1 February 2020.


==== Notes ====
Below is a list of all other legal jumps equal or superior to 7.40 m:

Jackie Joyner-Kersee also jumped 7.49 (1994 at altitude), 7.45 (1987) and 7.40 (1988).
Heike Drechsler also jumped 7.48 (1992), 7.45 (June 1986), 7.45 (July 1986), 7.44 (1985) and 7.40 (1984 & 1987).
Galina Chistyakova also jumped 7.45 (June 1988 ancillary jump during world record competition), 7.45 (August 1988).


==== Wind-assisted jumps ====
Any performance with a following wind of more than 2.0 metres per second is not counted for record purposes. Below is a list of the best wind-assisted jumps (equal or superior to 7.16 m). Only marks that are superior to legal bests are shown:

Heike Drechsler  jumped 7.63 (+2.1) at altitude in Sestriere, Italy on 21 July 1992.
Fiona May  jumped 7.23 (+4.3) at altitude in Sestriere, Italy on 29 July 1995.
Susen Tiedtke  jumped 7.22 (+3.7) at altitude in Sestriere, Italy on 28 July 1993.
Anastassia Mirochuk-Ivanova  jumped 7.22 (+4.3) in Grodno, Belarus on 6 July 2012.
Eva Murková  jumped 7.17 (+3.6) in Nitra, Czechoslovakia on 26 August 1984.


== Olympic medalists ==


=== Men ===


=== Women ===


== World Championships medalists ==


=== Men ===


=== Women ===


== World Indoor Championships medalists ==


=== Men ===


=== Women ===
A  Known as the World Indoor Games


== Season's bests ==


== National records ==
Updated 24 June 2020.


=== Men ===
Outdoor NR's equal or superior to 8.00 m:


=== Women ===
Outdoor NR's equal or superior to 6.75 m:


== Notes and references ==


== Cited sources ==
Stephen G. Miller (2004). Ancient Greek Athletics. New Haven: Yale University Press. ISBN 0300115296.


== Further reading ==
Guthrie, Mark (2003). Coach Track & Field Successfully. Champaign, Illinois: Human Kinetics. pp. 149–155. ISBN 0-7360-4274-1.
Rogers, Joseph L. (2000). USA Track & Field Coaching Manual. Champaign, Illinois: Human Kinetics. pp. 141–157. ISBN 0-88011-604-8.
Ernie Gregoire, Larry Myricks (1991). World Class Track & Field Series: Long Jump (VHS). Ames, Iowa: Championship Books & Video Productions.


== External links ==
IAAF long jump homepage
IAAF list of long-jump records in XML
Powell vs Lewis Tokyo 91 (video)