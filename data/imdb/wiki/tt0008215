The Lorelei ( (listen) LORR-ə-ly; German: [loːʁəˈlaɪ, ˈloːʁəlaɪ]), also spelled Loreley in German, is a 132 m (433 ft) high, steep slate rock on the right bank of the River Rhine in the Rhine Gorge (or Middle Rhine) at Sankt Goarshausen in Germany. The Loreley Amphitheatre on top of the rock is a UNESCO World Heritage Site.


== Etymology ==

The name comes from the old German words lureln, Rhine dialect for 'murmuring', and the Celtic term ley "rock". The translation of the name would therefore be: 'murmur rock' or 'murmuring rock'. The heavy currents, and a small waterfall in the area (still visible in the early 19th century) created a murmuring sound, and this combined with the special echo the rock produces to act as a sort of amplifier, giving the rock its name. The murmuring is hard to hear today owing to the urbanization of the area. Other theories attribute the name to the many boating accidents on the rock, by combining the German verb lauern ('to lurk, lie in wait') with the same "ley" ending, with the translation "lurking rock".
After the German spelling reform of 1901, in almost all German terms, the letter "y" was changed to the letter "i", but some proper nouns have kept their "y", such as Bayern, Speyer, Spay, Tholey, (Rheinberg-)Orsoy and including Loreley, which is thus the correct spelling in German.


== Original folklore and modern myth ==

The rock and the murmur it creates have inspired various tales. An old legend envisioned dwarfs living in caves in the rock.
In 1801, German author Clemens Brentano composed his ballad Zu Bacharach am Rheine as part of a fragmentary continuation of his novel Godwi oder Das steinerne Bild der Mutter. It first told the story of an enchanting female associated with the rock. In the poem, the beautiful Lore Lay, betrayed by her sweetheart, is accused of bewitching men and causing their death. Rather than sentence her to death, the bishop consigns her to a nunnery. On the way thereto, accompanied by three knights, she comes to the Lorelei rock. She asks permission to climb it and view the Rhine once again. She does so and thinking that she sees her love in the Rhine, falls to her death; the rock still retained an echo of her name afterwards. Brentano had taken inspiration from Ovid and the Echo myth.
In 1824, Heinrich Heine seized on and adapted Brentano's theme in one of his most famous poems, "Die Lorelei". It describes the eponymous female as a sort of siren who, sitting on the cliff above the Rhine and combing her golden hair, unwittingly distracted shipmen with her beauty and song, causing them to crash on the rocks. In 1837 Heine's lyrics were set to music by Friedrich Silcher in the art song "Lorelei" that became well known in German-speaking lands. A setting by Franz Liszt was also favored and dozens of other musicians have set the poem to music. During the Nazi regime and World War II, Heinrich Heine (born as a Jew) became discredited as author of the lyrics, in an effort to dismiss and hide Jewish contribution to German art. 
The Lorelei character, although originally imagined by Brentano, passed into German popular culture in the form described in the Heine–Silcher song and is commonly but mistakenly believed to have originated in an old folk tale. The French writer Guillaume Apollinaire took up the theme again in his poem "La Loreley", from the collection Alcools which is later cited in Symphony No. 14 (3rd movement) of Dmitri Shostakovich.


== Accidents ==
A barge carrying 2,400 tons of sulphuric acid capsized on 13 January 2011, near the Lorelei rock, blocking traffic on one of Europe's busiest waterways.


== In popular culture ==


=== Literature ===
Heine's poemGerman composer Clara Schumann composed a setting of Heine's poem in 1843. Dozens of other composers have also set this poem.
Soviet composer Dmitri Shostakovich composed a setting of the poem in a 1913 translation by Guillaume Apollinaire as part of his Symphony No. 14.
The Heine Memorial in the Bronx, New York City, better known as the "Lorelei Fountain", takes the form of the mythical siren from Heine's poem.OtherIn Eichendorff's 1812 poem "Waldesgespräch", a rider meets a beautiful young woman in the forest who turns out to be "the witch Loreley"; she tells him that he will never leave the forest. Robert Schumann set the poem to music in his 1840 song cycle Liederkreis, Op. 39.
Sylvia Plath wrote a poem entitled "Lorelei" (part of the collection The Colossus and Other Poems, first published in 1960).
“Lorelei”, a 14-line poem made up of seven discreetly-spaced, unrhymed couplets, is the opening “emblem” poem in James Merrill's ‘’The Fire Screen’’ (1969), his fifth collection of poetry.
Israeli Poet Nathan Alterman wrote two poems in "The Seventh Column" titled "Lorelei" and "Lorelei's Liberation", citing Heine's poem and using the image of Lorelei as a symbol for Germany and its people in WW2. The first written in 1942 at the height of Germany's attack on the Soviet Union near Stalingrad (battle), the second written in 1944 during the Allies' campaign in Germany.


=== Music ===
OperaGerman composer Felix Mendelssohn began an opera in 1846 after a libretto by Emmanuel Geibel based on the legend of the Lorelei Rhine maiden for Swedish soprano Jenny Lind. However, he died before he had the chance to finish it.
Lorelei is the main character in the English opera Lurline by William Vincent Wallace, first performed in 1860.
Alfredo Catalani composed the opera Loreley in 1890 with Lorelei as the main character.
The 2018 Australian "operatic cabaret" Lorelei re-cast the Lorelei as three sirens who begin to question their life on the rock in the Rhine. It was composed by Julian Langdon, Casey Bennetto and Gillian Cosgriff, with a book by Bennetto and Cosgriff, and starred Dimity Shepherd, Ali McGregor and Antoinette Halloran.RockAmerican rock band Styx featured a song called "Lorelei" on their 1975 album Equinox.
American rock band Clutch featured a song called "Lorelei" on their 2018 album Book of Bad Decisions.
Japanese rock band L'Arc~en~Ciel released the song "Loreley" through the album Heart in 1998.
Japanese gothic rock / post-punk band Madame Edwarda released the song "Lorelei" on its 1985 EP "Lorelei ・・ Étranger ・・ Héliogabale".
UK rock band Wishbone Ash produced a song about the mystery and dangers of Lorelei on their 1976 album New England.
The folk rock duo Blackmore's Night released the song "Loreley" on their 2003 album Ghost of a Rose; the song describes the legend of Lorelei as a siren and "river queen," intentionally charming men to their deaths.
The American new wave band Tom Tom Club has a song entitled "Lorelei" on their self-titled album, Tom Tom Club (album).OtherGerman composer Friedrich Silcher composed a famous song Die Lorelei with words by Heinrich Heine
Austrian composer Johann Strauss I composed the waltz Lorelei Rhein Klänge (Echoes of the Rhine Lorelei), op. 154, in 1843.
“The Lorelei" is a song composed by George Gershwin, with lyrics by Ira Gershwin; it was written for their musical Pardon My English (1933).The Lorelei is also a song written by Barclay Allen (pianist and composer) and recorded instrumentally by Roger Williams and vocally by Jack Jones. It is exquisite.
Roxy Music's song "Editions of You" references sirens wailing on the Lorelei.
Scottish dream pop band Cocteau Twins released a song named "Lorelei" on their 1984 album Treasure.
Polish musician Kapitan Nemo released a synthpop song, "Twoja Lorelei" ("Your Lorelei"), in 1984.
German heavy metal band Scorpions released their song "Lorelei", which is about the love of a sailor, in their 2010 album Sting in the Tail.
Norwegian goth metal band Theatre of Tragedy released a song entitled "Lorelei", with lyrics by band member Raymond Rohonyi, on their 1998 album Aégis.
The Pogues recorded "Lorelei" in 1989 on their album Peace and Love. "But if my ship, which sails tomorrow / Should crash against these rocks, / My sorrows I will drown before I die / It's you I'll see, not Lorelei". A cover version of this song was recorded by Marc Seymour (formerly of the Australian band Hunters and Collectors) on his 2013 album 'The Seventh Heaven Club'.
Swedish musician Eagle-Eye Cherry likens his story with that of Lorelei and her lover in his song "When Mermaids Cry" on the album Desireless.
The K-pop group Ladies' Code released a song named "Lorelei" on their 2016 album STRANG3R.
Australian extreme progressive metal group Ne Obliviscaris refers to Lorelei in the song "Eyrie" on the album Urn, released in 2017.
German dark rock band Lord of the Lost released the song "Loreley" in their 2018 album named Thornstar.
There is a Lorelei reference in the Paul McCartney song Beautiful Night, from his 1997 Flaming Pie album.
American Artist Noah Levine wrote and recorded the song "Lorelei" in 2019.


=== Painting ===
The Surrealist painter Edgar Ende produced an oil painting titled Loreley toward the end of his career, in 1963
The British painter David Wightman has several landscape paintings and an exhibition named after the folklore.


=== Other ===
The 1973 animated Star Trek episode "The Lorelei Signal" is about a planet of beautiful women who periodically lure ships to their world and draw the ships' men to their deaths.
A 1965 episode of Hogan's Heroes involved the prisoners building Colonel Klink an officer’s club in the shape of a boat. Wearing a sailing outfit, Klink flirts with his secretary and says, “you are my Lorelei.” (Season 1, Episode 16.)
Marilyn Monroe's character in the 1953 movie "Gentlemen Prefer Blondes" is named Lorelei.
The Loreley's Grasp (1974), horror film from Spain directed by Amando de Ossorio.
The Marvel Comics character Lorelei was inspired by this legend.
The Elite Four class of Pokémon trainers in the Pokémon games set in the Kanto region have a member named Lorelei. She is a master Ice-type trainer and some of her Pokémon are part Water-type, possibly alluding to the water spirit myth.
In episode 20 of Pokémon, "The Ghost of Maiden's Peak," the pokémon Gastly appears in the form of a maiden whose story mirrors the myth of LoreLay.
The After War Gundam X references the folklore in episode "The Sea of Lorelei", a location named for the legend, and has a character recite part of Heine's poem.
The name of Hong Kong's dragon boat team Loreley in Chinese is 暴風女神 (Bou Fung Neoi San, lit. "Goddess of the Storm").
The manga/anime series Monster Musume references the legend in its character Meroune Lorelei, a tragedy-obsessed mermaid.


== Gallery ==

		
		
		
		
		


== See also ==
Siren - Ancient Greek
Sirin - Russian


== References ==


== External links ==
Loreley Information about the Lorelei rock and surrounding area
Die Lorelei – Heinrich Heine's poem with English translation
The Lorelei – Translation of the tale, from Ludwig Bechstein's German Saga Book
Recordings from the Cylinder Preservation and Digitization Project; search results for Loreley and Lorelei