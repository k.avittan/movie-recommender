A police car (also called a police cruiser, police interceptor, patrol car, cop car, prowler, squad car, radio car, or radio motor patrol (RMP)) is a ground vehicle used by police for transportation during patrols and to enable them to respond to incidents and chases. Typical uses of a police car include transporting officers so they can reach the scene of an incident quickly, transporting and temporarily detaining suspects in the back seats, as a location to use their police radio or laptop, or to patrol an area, all while providing a visible deterrent to crime. Some police cars are specially adapted for certain locations (e.g., traffic duty on busy roads) or for certain operations (e.g., to transport police dogs or bomb squads). Police cars typically have rooftop flashing lights, a siren, and emblems or markings indicating that the vehicle is a police car. Some police cars may have reinforced bumpers and alley lights, for illuminating darkened alleys.
Terms for police cars include area car and patrol car. In some places, a police car may also be informally known as a cop car, a black and white, a cherry top, a gumball machine, a jam sandwich or panda car. Depending on the configuration of the emergency lights and livery, a police car may be considered a marked or unmarked unit.


== History ==

The first police car was a wagon run by electricity fielded on the streets of Akron, Ohio, in 1899. The first operator of the police patrol wagon was Akron Police officer Louis Mueller, Sr. It could reach 16 mph (26 km/h) and travel 30 mi (48 km) before its battery needed to be recharged. The car was built by city mechanical engineer Frank Loomis. The US$2,400 vehicle was equipped with electric lights, gongs, and a stretcher.  The car's first assignment was to pick up a drunken man at the junction of Main and Exchange streets.Ford introduced the Ford flathead V-8 in its Model B, as the first mass-marketed V8 car in 1932. In the 1940s, major American car makers began to manufacture specialized police cars.


== Usage ==
In some areas of the world, the police car has become more widely used than police officers "walking the beat". Placing officers in vehicles also allows them to carry more equipment, such as automated external defibrillators for people in cardiac arrest or road cones for traffic obstructions, and allows for more immediate transport of suspects to holding facilities. Vehicles also allow for the transport of larger numbers of personnel, such as a SWAT team.
Decommissioned police cars are often sold to the general public, either through a police auction or a private seller, after about 3–5 years of use. Such cars are usually sold relatively cheaply due to the extremely high mileage on such cars, in some cases exceeding the 300,000-mile (480,000 km) mark. In some cases, the cars are re-purposed as a taxicab as an inexpensive way for cab companies to buy cars instead of fleet vehicle services. In all cases, the cars are stripped of their police markings as well as most internal equipment; however the engines are usually left intact, and are often larger engines than their civilian counterparts.

.


== Functional types ==
There are several types of police car.


=== Patrol car ===

The car that replaces walking for the 'beat' police officer. Their primary function is to convey normal police officers between their duties (such as taking statements or visiting warnings). Patrol cars are also able to respond to emergencies, and as such are normally fitted with visual and audible warnings.


=== Response car (pursuit car) ===
A response car is similar to a patrol car, but is likely to be of a higher specification, capable of higher speeds, and will certainly be fitted with audible and visual warnings. These cars are usually only used to respond to emergency incidents, so are designed to travel fast, and may carry specialist equipment, such as assault rifles, or shotguns. In the UK, each station usually only has one, which is called an Area car.


=== Traffic car ===
Traffic police cars, known in the UK as Road Policing Units, are cars designed for the job of enforcing traffic laws, and as such usually have the highest performance of any of the police vehicles, as they must be capable of catching most other vehicles on the road. They may be fitted with special bumpers designed to force vehicles off the road, and may have visual and audible warnings, with special audible warnings which can be heard from a greater distance. In some police forces, the term traffic car may refer to cars specifically equipped for traffic control in addition to enforcing traffic laws. As such, these cars may differ only slightly from a patrol car, including having radar and laser speed detection equipment, traffic cones, flares, and traffic control signs.


=== Multi-purpose car ===
Some police forces do not distinguish between patrol, response and traffic cars, and may use one vehicle to fulfill some or all roles even though in some cases this may not be appropriate (such as a police city vehicle in a motorway high speed pursuit chase). These cars are usually a compromise between the different functions with elements added or removed.


=== Sport utility vehicles (SUV) and pickup trucks ===
SUVs and Pickups are used for a variety of reasons; off-road needs, applications where  a lot of equipment must be carried, K-9 units, etc. Some police departments use pickup trucks with cages for animal control units.


=== Community liaison car ===

This is a standard production car, visibly marked, but without audible and visual warning devices. It is used by community police officers to show a presence, transport them between jobs and make appearances at community events. These cars do not respond to emergencies.


=== Unmarked car ===

Many forces also operate unmarked cars, in any of the roles shown above, but most frequently for the use of traffic enforcement or detectives. They have the advantage of not being immediately recognizable, and are a valuable tool in catching criminals while the crime is still taking place. In the United States, unmarked cars are also used by federal law enforcement agencies such as the FBI and the Secret Service, but can be recognized by their U.S. government plates, however they often have normal license plates. All unmarked cars bear license plates. Many U.S. jurisdictions use regular civilian issued license plates on unmarked cars, especially gang suppression and vice prevention units. Also see Q-car. Unmarked vehicles can range from normal patrol vehicles: Ford Explorer, Dodge Charger, Ford Crown Victoria, Chevrolet Impala, etc, to completely unmarked foreign vehicles: Toyota, Honda, Nissan, Mazda, etc.
There have been cases where criminals have pulled over motorists while pretending to be driving unmarked police cars, a form of police impersonation. Some US police officers advise motorists that they do not have to pull over in a secluded location and instead can wait until they reach somewhere safer. In the UK, officers must be wearing uniform in order to make traffic stops. Motorists can also ask for a police badge and peace officer identification. Motorists often have the option to call a non-emergency number (like Police 101 in the UK) or, if the country does not have one, the emergency number. This telephone call can then be used verify that the police car ( and officer(s) ) is genuine.


=== Dog Squad ===

This type of car is used to transport police dogs. In some jurisdictions, this will be a station wagon or car based van, due to the installation of cages to carry the dogs. These units may also be known as K9 units (a homophone of canine, also used to refer to the animals themselves).  These cars are typically marked in order to warn people that there is a police dog on board.


=== Surveillance car ===
Forces may operate surveillance cars. These cars can be marked or unmarked, and are there to gather evidence of any criminal offence. Overt marked cars may have CCTV cameras mounted on the roof to discourage wrongdoing, whereas unmarked cars would have them hidden inside. This type of vehicle is particularly common in the United Kingdom. In the United States, some police departments' vice, narcotics, and gang suppression units utilize vehicles that contain no identifiable police equipment (such as lights, sirens or radios) to conduct covert surveillance. Some police vehicles equipped with surveillance are Bait cars which are deployed in high volume car theft areas.


=== High visibility decoy car ===
Some police forces use vehicles (or sometimes fake "cut outs" of vehicles) to deter crime. They may be old vehicles retired from use, stock models restyled as police cars, or a metal sign made to look like a police car. They are placed in areas thought to be susceptible to crime in order to provide a high visibility presence without committing an officer. Examples of these can be seen on many main roads, freeways and motorways. In 2005, Virginia's (United States) legislature considered a bill which stated, in part: "Whenever any law-enforcement vehicle is permanently taken out of service ... such vehicle shall be placed at a conspicuous location within a highway median in order to deter violations of motor vehicle laws at that location. Such vehicles shall ... be rotated from one location to another as needed to maintain their deterrent effect."; Such cars may also be used in conjunction with manned units hidden further down the road to trick speeders into speeding back up again, and being clocked by the manned car. In Chicago, Illinois a small fleet of highly visible vans are parked alongside major state and federal routes with automated speed detection and camera equipment, monitoring both for speeders and other offenders by license plate. Tickets are then mailed to the offenders or, in case of other crimes related to the licensed owner, may be served by a manned vehicle further down the road.


=== Rescue unit ===
In some jurisdictions, the police may operate a rescue service, and special units will be required for this.


=== Explosive ordnance disposal ===
In jurisdictions where the police are responsible for, or participate in, explosive ordnance disposal squads (bomb squads), dedicated vehicles transport the squads' crews and equipment.


=== Demonstration cars ===

Cars which are not for active duty, but simply for display. These are often high performance or modified cars, sometimes seized from criminals, used to try to get across specific messages (such as with the D.A.R.E. program), or to help break down barriers with certain groups (such as using a car with modified 'jumping' suspension as a talking point with young people).
To show the police what is new, a marked police car with the manufacturer's name (Ford, General Motors, Chrysler) can be displayed with the words "Not In Service" to show what is new with that model of car and get feedback from police departments.  Companies like Whelen, Federal Signal and Code 3 also have demo cars with their names on the side and showing the police what is new in the field of emergency vehicle equipment.


=== Riot control vehicles ===
These vehicles could be divided into three sub-categories. Modified trucks equipped with water cannons, modified stock cars and modified APCs (Armored Personnel Carriers). Their function is to help control riots. Modified stock cars will have caged windows for protection against objects thrown at them and could include mini-buses, 4x4s or prisoner transport vans. APCs usually will not require any added protection but their modifications might include some sort of tear gas ejecting method or shields that unfold to create barriers. The water cannon vehicles are used either to break up riots or extinguish fires set by the rioters. Although plain water is usually used some variations might include tear gas or special dye (to mark the people that are present for later apprehension). Previously fire trucks were used as anti-riot vehicles of this type. As a non lethal, and effective method of clearing out protesters or rioters, the Long Range Acoustic Device (LRAD) can be used. The LRAD is a device that can send announcements, warnings, and harmful pain-inducing tones.

		
		


== Equipment ==

Police cars are usually passenger car models which are upgraded to the specifications required by the purchasing force. Several vehicle manufacturers, such as Ford, General Motors and Dodge, provide a "police package" option, which is built to police specifications in the factory. Police forces may add to these modifications by adding their own equipment and making their own modifications after purchasing a vehicle.


=== Mechanical modifications ===
Modifications a police car might undergo include adjustments for higher durability, speed, high-mileage driving, and long periods of idling at a higher temperature. This is usually accomplished by heavy duty suspension, brakes, calibrated speedometer, tires, alternator, transmission, and cooling systems, and also sometimes includes slight modifications to the car's stock engine or the installation of a more powerful engine than would be standard in that model. It is also usual to upgrade the capacity of the electrical system of the car to accommodate the use of additional electronic equipment.


=== Safety equipment ===
Police vehicles are often outfitted with  AEDs (Automated external defibrillator), first aid kits, fire extinguishers, flares, life buoys, barrier tapes, etc.


=== Audible and visual warnings ===
Police vehicles are often fitted with audible and visual warning systems to alert other motorists of their approach or position on the road. In many countries, use of the audible and visual warnings affords the officer a degree of exemption from road traffic laws (such as the right to exceed speed limits, or to treat red stop lights as a yield sign) and may also suggest a duty on other motorists to move out of the direction of passage of the police car or face possible prosecution.
Visual warnings on a police car can be of two types: either passive or active.


==== Passive visual warnings ====
Passive visual warnings are the markings on the vehicle. Police vehicle markings usually make use of bright colors or strong contrast with the base color of the vehicle. Modern police vehicles in some countries have retroreflective markings which reflect light for better visibility at night. Other police vehicles may only have painted on or non-reflective markings. Most marked police vehicles in the United Kingdom and Sweden have reflective Battenburg markings on the sides, which are large blue and yellow rectangles. These markings are designed to have high contrast and be highly visible on the road, to deter crime and improve safety. Another passive visual warning of police vehicles is simply the interceptor's silhouette. This is easily observed in the United States and Canada, where the ubiquitous nature of the Ford Crown Victoria in police fleets has made the model synonymous with police vehicles.

Police vehicle marking schemes usually include the word Police or similar phrase (such as State Trooper or Highway Patrol) or the force's crest. Some police forces use unmarked vehicles, which do not have any passive visual warnings at all, and others (called stealth cars) have markings that are visible only at certain angles, such as from the rear or sides, making these cars appear unmarked when viewed from the front.


==== Active visual warnings ====

The active visual warnings are usually in the form of flashing colored lights (also known as 'beacons' or 'lightbars'). These flashes are used in order to attract the attention of other road users as the police car approaches, or to provide warning to motorists approaching a stopped vehicle in a dangerous position on the road. Common colours for police warning beacons are blue and red, however this often varies by force. Several types of flashing lights are used, such as rotating beacons, halogen lights, or light emitting diode strobes. Some police forces also use arrow sticks to direct traffic, or message display boards to provide short messages or instructions to motorists. The headlights of some vehicles can be made to flash, or small strobe lights can be fitted in the headlight, tail light and indicator lights of the vehicle.


==== Audible warnings ====

In addition to visual warnings, most police cars are also fitted with audible warnings, sometimes known as sirens, which can alert people and vehicles to the presence of an emergency vehicle before they can be seen. The first audible warnings were mechanical bells, mounted to either the front or roof of the car. A later development was the rotating air siren, which made noise when air moved past it. Most modern vehicles are now fitted with electronic sirens, which can produce a range of different noises. Police driving training often includes the use of different noises depending on traffic conditions and maneuver being performed. In North America for instance, on a clear road, approaching a junction, the "wail" setting may be used, which gives a long up and down variation, with an unbroken tone, whereas, in heavy slow traffic, a "yelp" setting may be preferred, which is a sped up version of the "wail". Some vehicles may also be fitted with airhorn audible warnings. Also in some European countries, where a hi-lo two tone siren is the only permitted siren for emergency vehicles, a "stadt" siren will be used in cities where it has loud echo that can be heard from blocks away to warn the traffic an emergency vehicle is coming, or a "land" siren will be used on highways to project its noise to the front to produce more penetration into the vehicles ahead to alert the drivers.
A development is the use of the RDS, a system of car radios, whereby the vehicle can be fitted with a short range FM transmitter, set to RDS code 31, which interrupts the radio of all cars within range, in the manner of a traffic broadcast, but in such a way that the user of the receiving radio is unable to opt out of the message (as with traffic broadcasts). This feature is built into all RDS radios for use in national emergency broadcast systems, but short range units on emergency vehicles can prove an effective means of alerting traffic to their presence, although is not able to alert pedestrians, non-RDS radio users, or drivers with their radios turned off.
A new technology has been developed and is slowly becoming more popular with police. Called the Rumbler, it is a siren that emits a low frequency sound which can be felt. Motorists that may have loud music playing in their car, for example, may not hear the audible siren of a police car behind them, but will feel the vibrations of the Rumbler. The feeling is that of standing next to a large speaker with pumped bass.


=== Police-specific equipment ===

Police officers' additional equipment may include:

Two-way radio
Equipment consoles
These are used to house two way radios, light switches, and siren switches. Some may be equipped with locking compartments for safe storage of firearms or file compartments.
Suspect transport enclosures
These are steel and plastic barriers which ensure that a suspect—who has been frisked, disarmed, handcuffed and seat belted, is unable to attack the driver or passenger and unable to tamper with equipment in the front seat. These may be simple bars or grilles, although they can include highly impact resistant but not bullet resistant glass. Many use expanded steel instead of plastic glazing for the upper half of the partition.
Firearm lockers
In certain countries, including the United States, some police vehicles are equipped with lockers or locking racks in which to store firearms. These are usually tactical firearms such as shotguns or rifles, which would not normally be carried on the person of the officer.
Mobile data terminal
Many police cars are fitted with mobile data terminals (or MDTs), which are connected via wireless methods to the police central computer, and enable the officer to call up information such as vehicle license details, offender records, and incident logs.
Vehicle tracking system
Some police vehicles, especially traffic units, may be fitted with equipment which will alert the officers to the presence nearby of a stolen vehicle fitted with a special transponder, and guide them towards it, using GPS or simpler radio triangulation.
Evidence gathering CCTV
Police vehicles can be fitted with video cameras used to record activity either inside or outside the car. They may also be fitted with sound recording facilities. This can then later be used in a court to prove or disprove witness statements, or act as evidence in itself (such as evidence of a traffic violation).
Automatic number plate recognition (ANPR)
This computerised system uses cameras to observe the number plates of all vehicles passing or being passed by the police car, and alerts the driver or user to any cars which are on a 'watch list' as being stolen, used in crime, or having not paid vehicle duty.
Speed recognition device
Some police cars are fitted with devices to measure the speed of vehicles being followed, such as ProViDa, usually through a system of following the vehicle between two points a set distance apart. This is separate to any radar gun device which is likely to be handheld, and not attached to the vehicle.
Remote rear door locking
This enables officers in the front to remotely control the rear locks—usually used in conjunction with a transport enclosure.
PIT bumper
The Pursuit Intervention Technique (PIT) bumper attaches to the front frame of a patrol car. It is designed to end vehicle pursuits by spinning the fleeing vehicle with a nudge to the rear quarter panel.  Cars not fitted with a PIT Bumper can still attempt a PIT Maneuver at risk of increased front-end damage and possible disablement if the maneuver fails and the pursuit continues.
Push bumper (aka nudge bars)
Fitted to the chassis of the car and located to augment the front bumper, to allow the car to be used as a battering ram for simple structures or fences, or to push disabled vehicles off the road.
Runlock
This allows the vehicle's engine to be left running without the keys being in the ignition. This enables adequate power, without battery drain, to be supplied to the vehicle's equipment at the scene of an incident. The vehicle can only be driven off after re-inserting the keys. If the keys are not re-inserted, the engine will switch off if the handbrake is disengaged or the footbrake is activated.The installation of this equipment in a car partially transforms it into a desk. Police officers use their car to fill out different forms, print documents, type on a computer or a console, consult and read different screens, etc. Ergonomics in layout and installation of these items in the police car plays an important role in the comfort and safety of the police officers at work and preventing injuries such as back pain and musculoskeletal disorders.


=== Ballistic protection ===
Some police cars can be optionally upgraded with bullet resistant armor in the car doors. The armor is typically made from ceramic ballistic plates and aramid baffles. A 2016 news report said that Ford sells 5 to 10 percent of their US police vehicles with ballistic protection in the doors. In 2017 Bill de Blasio, the mayor of New York City, announced that all NYPD patrol cars would be installed with bullet-resistant door panels and bullet-resistant window inserts.


== In popular culture ==
Police chases have been dramatized in television programs and movies, and occasionally feature in television news coverage of unusual circumstances, showing footage from an airborne camera.
In crime drama, such as Police procedural stories, police cars are often portrayed as containing a team of at least two police officers so that they may converse and interact with each other while on patrol. Depending on local policy, real patrols (especially rural and low population areas) may have only one officer per vehicle, although at night this may increase to two.


== Use by country ==


== See also ==


=== General ===


=== Other types of emergency vehicles ===


== References ==


== External links ==
Police car showrooms from various manufacturers:
Ford
General Motors
Article on the new police edition Dodge Charger—The New York Times
NYPD siren sounds—The New York Times
Restored examples of many types of UK police vehicles
Police Cars Collection Photos
visual.merriam-webster.com