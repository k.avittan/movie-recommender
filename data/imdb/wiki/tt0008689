Too Much, Too Soon is a 1958 biographical film about Diana Barrymore produced by Warner Bros. It was directed by Art Napoleon and produced by Henry Blanke from a screenplay by Art Napoleon and Jo Napoleon, based on the autobiography by Diana Barrymore and Gerold Frank. The music score was by Ernest Gold and the cinematography by both Nicholas Musuraca and Carl E. Guthrie. Diana died in 1960, two years after the release of this film.
The film stars Dorothy Malone and Errol Flynn (playing his real-life friend and mentor John Barrymore), with Efrem Zimbalist Jr., Ray Danton, Neva Patterson, Murray Hamilton and Martin Milner.


== Plot ==
Fourteen-year-old Diana Barrymore is being raised by her domineering mother, a poet. Her father, the famed actor John Barrymore, has not laid eyes on Diana for 10 years, but they share an evening on his boat before John abandons her again.
At 18, Diana has become an actress and has a steady boyfriend, Lincoln Forrester. When a Hollywood contract comes her way, Diana's mother warns her not to live with John, now a washed-up alcoholic.
She finds her father living in a nearly empty mansion, having sold or pawned his belongings to pay his bills. He keeps a bald eagle in a cage indoors and has a servant, Gerhardt, who must physically knock out John to put him to bed.
Diana's famous name gains her some publicity, but her performances are panned. Her new husband, actor Vince Bryant, is away a lot, so Diana turns to drink and leaves Vince for tennis player John Howard. When her father dies alone, a penniless and often drunk Diana and her husband move in with her mother, who can only stand so much before making them leave.
After marrying again, this time to recovering alcoholic Bob Wilcox, she discovers after her mother's death that she has been left no inheritance. Diana takes demeaning jobs, including a striptease. She becomes violent and is hospitalized. Her only hope at salvation is an offer to write her memoirs, and old friend Linc returns to her life, offering some badly needed kindness.


== Cast ==
Dorothy Malone as Diana
Errol Flynn as John Barrymore
Efrem Zimbalist, Jr. as Vincent Bryant – in real life this was Bramwell Fletcher
Martin Milner as Lincoln Forrester
Neva Patterson as Diana's mother Blanche Oelrichs
Ray Danton as John Howard
Ed Kemmer as Robert Wilcox
Robert Ellenstein as Gerold Frank
Shirley Mitchell as Mrs. Magda Snow


== Original book ==
The film was based on the tumultuous anecdotes of Barrymore and Gerold Frank's. 1957 best-selling autobiography. Frank was a renowned ghostwriter and had previously worked on I'll Cry Tomorrow, a popular book about another alcoholic celebrity, Lillian Roth. The book, released as Too Soon Too Much, was published through Henry Holt & Co. in 1957, and re-published in 1958 through Signet publishing. Warner Bros. picked up the film a year later in 1958. The book aimed to assuage the immense negativity surrounding Diana’s name; the book’s subtitle, “the Cinderella story — in reverse,” epitomizes the self-aware and reflective tone of the book. Barrymore focuses on the theme of self redemption in the book, primed with an explanation of her plunge followed with a triumphant description of her resilience.
The devastating mental health issues plaguing the Barrymore family is well documented — Diana expands upon her childhood of neglect and the cycle of abuse she suffered throughout her life in her book. The writing process served as an outlet for Barrymore, where she was able to express her frustrations which "[she] usually only [got] rid of on a psychiatrist’s couch."Although the book was intended to reintroduce Barrymore into the limelight, Diana faced an untimely death in 1960, a meager three years after the publishing. Unfortunately, it appeared as if she had never overcome her demons, and succumbed to a drug and alcohol overdose. Articles concerning Diana’s public perception revealed some negative opinions, especially around her death. For example, Hugh Strathmore analyzes the waning days up until her death, and inconsiderately concludes that Diana’s “stubborn pride” and that the fact that she “wouldn’t admit that the hooch had her licked” was what cause her ultimate overdose. He also describes her demise as “unsurprising.""There's no message, I didn't set out to point a moral", said Barrymore. "But writing it has been a cleansing process. It's like psychiatry in a way."


=== Reception ===
When the book was published, The New York Times called it "an extremely skillful piece of work, a craftsman's product aimed at a mood and a market that spell big business. It is a book for the mass audience... as an artisan, Mr Frank is no slouch."The Washington Post thought the book "fails to touch the heart even though it spins a recognizably sad story."Louella Parsons said the book "told too much too loudly."The Cincinnati Enquirer describes the tale as “sordid,” “pathetic,” “outrageous,” and “oddly admirable.” Moreover, the New York Herald Tribune lauds the narrative’s emotional appeal, going as far to label those who downplay the potent emotion as “heartless or hypocritical.”By the time the book came out Diana Barrymore tried to reactivate her acting career and was seeing a psychiatrist but had not given up drinking.


== Production ==
There was film interest in the book early on – I'll Cry Tomorrow had been a box office hit and Diana Barrymore had been fictionalised in a popular movie, The Bad and the Beautiful (1952) (the character played by Lana Turner). In December 1956, even before the book had been published, Warner Bros took an option on the film rights for a reported minimum of $100,000. (Another source said it was $150,000.)
In January, it was announced that Gerold Frank would work on the script in collaboration with Irving Wallace, and that Irving Rapper would direct and Henry Blanke would produce. By June however it was reported that the film was having "script problems" with the script two months overdue. Casablanca (1942) director, Michael Curtiz was initially in talks to direct the picture until he ultimately decided that the story was too sordid. Additionally, Errol Flynn had refused to work on another movie with director Curtiz, after they paired together for 12 films. In August, Warners said that Art and Jo Napoleon would write and direct the movie.


=== Casting ===
Originally, Carroll Baker, who had just made a big impression with Baby Doll (1956) and was under contract to Warners, was to star as Diana. Fredric March was mentioned as a possible John Barrymore. However, Baker refused to play the role, and Warner Bros put her on suspension and refused to let her make The Brothers Karamazov (1958) at MGM.Natalie Wood, also under contract to Warners, was mentioned as a possibility for the lead, as was Anne Baxter. Finally in August 1957 it was announced Dorothy Malone, who had recently won an Oscar for Written on the Wind would play Diana Barrymore. Malone never met Diana Barrymore. (She was invited to the set but declined.)
Gene Wesson was mentioned as auditioning for the part of John Barrymore. Jo Van Fleet was discussed for the part of Michael Strange.By September 1957 Errol Flynn had signed to play John Barrymore. Errol Flynn was a friend of John Barrymore's and the film was the first he had made for Warner Bros in a number of years.
Flynn flew back into Hollywood to make the movie and was arrested only a few days later for public drunkenness, stealing an off duty policeman's badge and trying to kiss a girl. Flynn denied he was drunk and was released from jail on bail after an hour.


=== Shooting ===
Warner Bros recreated John Barrymore's yacht and house for the film.  A Hollywood mansion that used to be owned by Madge Kennedy and Pola Negri was rented for the latter.A number of characters in the movie were fictionalised due to legal reasons – for instance first husband Bramwell Fletcher was turned into "Vincent Bryant". Real names were used for her last two husbands, despite their unsympathetic portrayals – John Howard had been arrested on white slavery charges and Robert Wilcox was dead.  Howard later became a car salesman and threatened to sue Warner Bros.Ray Danton, who played Howard, a tennis professional, received tennis coaching from Tony Trabert.Flynn said "it would have been easy for me to simulate Jack Barrymore's physical characteristics for I can do, with the lifted eyebrow, an imitation about as good as anyone else’s." However he wanted to:

Delve into his inner self, not to imitate him ~ that was too easy. I wanted to show a man with a heart, a man eaten up inside - as I knew him to be in those final days when I was close to him - a man full of regrets and all ready to die, but with one last thing to live for, the love of his daughter, Diana, his desire to get back her love. I determined that I would stay away from the least suggestion or imitation of manners. That would have been deadly wrong. The only concession I made to that was to try to look like him. To 
facilitate that, the studio put a tip on the end of my nose which aided in conveying his profile.


== Reception ==


=== Critical ===
The New York Times wrote that the film was "not bad, just ineffectual... undaring and even unsurprising. Gone is most of the endless soiled linen that aggressively flapped through Miss Barrymore's best-selling autobiography – and, with it, it's left wallops, perhaps the book's only real substance... Mr Flynn steals the picture lock, stock and keg. It is only in the scenes of his savage disintegration, as the horrified girl looks on, that the picture approaches real tragedy."The Los Angeles Times called the film a "depressing affair, one that never should have been considered... it doesn't stick to the facts... it is not good storytelling, either in structural form or characterisation... For all his capturing of John's surface mannerisms, some of the physical appearance and, most effortlessly, his way with a bottle, Flynn is not the great profile and great actor of our time. I resented him in the part."The Washington Post called Too Much, Too Soon "a sorry film" in which Errol Flynn's performance "may seem to have at least dazzling vitality, but it's about as dishonest a portrait of the volatile actor as you're likely to find."The Chicago Daily Tribune called Too Much, Too Soon "a sordid, unattractive tale, poorly written and badly acted".Filmink magazine wrote that "Flynn never had Barrymore’s reputation as a great actor but he’s perfectly cast – full of charisma, charm and sadness, with a beautiful speaking voice and fondness for the bottle... the actor really tried on this one and you can tell. Everything he does is memorable... It was his best film of the decade."  


== See also ==
List of American films of 1958


== References ==


== External links ==
Too Much, Too Soon on IMDb
Too Much, Too Soon at AllMovie
Too Much, Too Soon at Rotten Tomatoes