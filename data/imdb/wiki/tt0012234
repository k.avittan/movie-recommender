Count Alessandro di Cagliostro (US: , Italian: [alesˈsandro kaʎˈʎɔstro]; 2 June 1743 – 26 August 1795) was the alias of the occultist Giuseppe Balsamo (pronounced [dʒuˈzɛppe ˈbalsamo]; in French usually referred to as Joseph Balsamo).
Cagliostro was an Italian adventurer and self-styled magician. He became a glamorous figure associated with the royal courts of Europe where he pursued various occult arts, including psychic healing, alchemy and scrying. His reputation lingered for many decades after his death, but continued to deteriorate, as he came to  be regarded as a charlatan and impostor, this view fortified by the savage attack of  Thomas Carlyle (1795–1881) in 1833, who pronounced him the "Quack of Quacks". Later works—such as that of  W.R.H. Trowbridge (1866-1938) in his Cagliostro: the Splendour and Misery of a Master of Magic (1910), attempted a rehabilitation.


== Biography ==


=== Origin ===
The history of Cagliostro is shrouded in rumour, propaganda, and mysticism. Some effort was expended to ascertain his true identity when he was arrested because of possible participation in the Affair of the Diamond Necklace.
Johann Wolfgang von Goethe relates in his Italian Journey that the identification of Cagliostro with Giuseppe Balsamo was ascertained by a lawyer from Palermo who, upon official request, had sent a dossier with copies of the pertinent documents to France. Goethe met the lawyer in April 1787 and saw the documents and Balsamo's pedigree: Balsamo's great-grandfather Matteo Martello had two daughters: Maria, who married Giuseppe Bracconeri; and Vincenza, who married Giuseppe Cagliostro. Maria and Giuseppe Bracconeri had three children: Matteo; Antonia; and Felicità, who married Pietro Balsamo (the son of a bookseller, Antonino Balsamo, who had declared bankruptcy before dying at age 44). The son of Felicità and Pietro Balsamo was Giuseppe, who was christened with the name of his great-uncle and eventually adopted his surname, too. Felicità Balsamo was still alive in Palermo at the time of Goethe's travels in Italy, and he visited her and her daughter. Goethe wrote that Cagliostro was of Jewish origin,  and it may be that the name "Balsamo" comes from the hebrew Baal Shem (Cagliostro himself publicly ascertained that he was a disciple of Haĩm Falk, the Baal Shem of London).
Cagliostro himself stated during the trial following the Affair of the Diamond Necklace that he had been born of Christians of noble birth but abandoned as an orphan upon the island of Malta. He claimed to have travelled as a child to Medina, Mecca, and Cairo and upon return to Malta to have been admitted to the Sovereign Military Order of Malta, with whom he studied alchemy, the Kabbalah, and magic.


=== Early life ===
Giuseppe Balsamo was born to a poor family in Albergheria, which was once the old Jewish Quarter of Palermo, Sicily. Despite his family's precarious financial situation, his grandfather and uncles made sure the young Giuseppe received a solid education: he was taught by a tutor and later became a novice in the Catholic Order of St. John of God, from which he was eventually expelled.During his period as a novice in the order, Balsamo learned chemistry as well as a series of spiritual rites. In 1764, when he was twenty one, he convinced Vincenzo Marano—a wealthy goldsmith—of the existence of a hidden treasure buried several hundred years previously at Mount Pellegrino. The young man's knowledge of the occult, Marano reasoned, would be valuable in preventing the duo from being attacked by magical creatures guarding the treasure. In preparation for the expedition to Mount Pellegrino, however, Balsamo requested seventy pieces of silver from Marano.When the time came for the two to dig up the supposed treasure, Balsamo attacked Marano, who was left bleeding and wondering what had happened to the boy—in his mind, the beating he had been subjected to had been the work of djinns.The next day, Marano paid a visit to Balsamo's house in via Perciata (since then renamed via Conte di Cagliostro), where he learned the young man had left the city. Balsamo (accompanied by two accomplices) had fled to the city of Messina. By 1765–66, Balsamo found himself on the island of Malta, where he became an auxiliary (donato) for the Sovereign Military Order of Malta and a skilled pharmacist.


=== Travels ===

In early 1768 Balsamo left for Rome, where he managed to land himself a job as a secretary to Cardinal Orsini. The job proved boring to Balsamo and he soon started leading a double life, selling magical "Egyptian" amulets and engravings pasted on boards and painted over to look like paintings. Of the many Sicilian expatriates and ex-convicts he met during this period, one introduced him to a seventeen-year-old girl named Lorenza Seraphina Feliciani (ca. 8 April 1751 – 1794), known as Serafina, whom he married 1768.
The couple moved in with Lorenza's parents and her brother in the vicolo delle Cripte, adjacent to the strada dei Pellegrini. Balsamo's coarse language and the way he incited Lorenza to display her body contrasted deeply with her parents' deep-rooted religious beliefs. After a heated discussion, the young couple left.
At this point Balsamo befriended Agliata, a forger and swindler, who proposed to teach Balsamo how to forge letters, diplomas and myriad other official documents. In return, though, Agliata sought sexual intercourse with Balsamo's young wife, a request to which Balsamo acquiesced.The couple traveled together to London, where Balsamo, now styling himself with one of several pseudonyms and self-conferred titles before settling on "Count Alessandro di Cagliostro", allegedly met the Comte de Saint-Germain. Cagliostro traveled throughout Europe, especially to Courland, Russia, Poland, Germany, and later France. His fame grew to the point that he was even recommended as a physician to Benjamin Franklin during a stay in Paris.
On 12 April 1777 "Joseph Cagliostro" was admitted as a Freemason of the Espérance Lodge No. 289 in Gerrard Street, Soho, London. In December 1777 Cagliostro and Serafina left London for the mainland, after which they travelled through various German states, visiting lodges of the Rite of Strict Observance looking for converts to Cagliostro's "Egyptian Freemasonry". In February 1779 Cagliostro traveled to Mitau, where he met the poetess Elisa von der Recke. In September 1780, after failing in Saint Petersburg to win the patronage of Russian Tsaritsa Catherine the Great, the Cagliostros made their way to Strasbourg, at that time in France. In October 1784, the Cagliostros travelled to Lyon. On 24 December 1784 they founded the co-Masonic mother lodge La Sagesse Triomphante of his rite of Egyptian Freemasonry at Lyon. In January 1785 Cagliostro and his wife went to Paris in response to the entreaties of Cardinal Rohan.


=== Affair of the diamond necklace ===

Cagliostro was prosecuted in the Affair of the Diamond Necklace which involved Marie Antoinette and Prince Louis de Rohan, and was held in the Bastille for nine months but finally acquitted, when no evidence could be found connecting him to the affair. Nonetheless, he was banished from France by order of Louis XVI, and departed for England. There he was accused by French expatriate Theveneau de Morande of being Giuseppe Balsamo, which he denied in his published Open Letter to the English People, forcing a retraction and apology from Morande.


=== Betrayal, imprisonment, and death ===
Cagliostro left England to visit Rome, where he met two people who proved to be spies of the Inquisition. Some accounts hold that his wife was the one who initially betrayed him to the Inquisition. On 27 December 1789 he was arrested and imprisoned in the Castel Sant'Angelo. He was tried and originally sentenced to death, but the sentence was later commuted to life imprisonment at the Forte di San Leo, where he would die on 26 August 1795.


== Legacy ==
Portuguese author Camilo Castelo Branco credits to Balsamo the creation of the Egyptian Rite of the Freemasons and intensive work in the diffusion of Freemasonry, by opening lodges all over Europe and by introducing the acceptance of women into the community. The idea of an "Egyptian freemasonry" was maintained in Italy by the Rite of Misraim, founded in 1813 by the three Jewish Bédarride brothers and in France, the Rite of Memphis founded in 1838 by Jacques Etienne Marconis de Nègre; these unified under Giuseppe Garibaldi as the Ancient and Primitive Rite of Memphis-Misraïm in 1881.
Cagliostro was an extraordinary forger. Giacomo Casanova, in his autobiography, narrated an encounter in which Cagliostro was able to forge a letter by Casanova, despite being unable to understand it.Occult historian Lewis Spence comments in his entry on Cagliostro that the swindler put his finagled wealth to good use by starting and funding a chain of maternity hospitals and orphanages around the continent.He carried an alchemistic manuscript The Most Holy Trinosophia amongst others with him on his ill-fated journey to Rome and it is alleged that he wrote it.Occultist Aleister Crowley believed Cagliostro was one of his previous incarnations.


== Cultural references ==


=== Fiction ===
Catherine the Great wrote two skits lampooning Cagliostro in the guise of characters loosely based upon him.
Johann Wolfgang Goethe wrote a comedy based on Cagliostro's life, also in reference to the Affair of the Diamond Necklace, The Great Cophta (Der Groß-Coptha) which was published in 1791.
Alexandre Dumas, père used Cagliostro in several of his novels (especially in Joseph Balsamo and in Le Collier de la Reine where he claims to be over 3,000 years old and to have known Helen of Troy).
George Sand includes Cagliostro as a minor character in her historical novel, The Countess of Rudolstadt (1843).
Aleksey Nikolayevich Tolstoy wrote the supernatural love story Count Cagliostro where the Count brings to life a long dead Russian princess, materializing her from her portrait. The story was made into a 1984 Soviet TV movie Formula of Love.
Cagliostro is prominently figured in three stories by Rafael Sabatini: "The Lord of Time", "The Death Mask" and "The Alchemical Egg", all of which are included in Sabatini's collection Turbulent Tales.
He is mentioned in the story The Sandman by ETA Hoffmann where Spalanzani is said to look like a portrait of Cagliostro by Chodowiecki.
He is mentioned in the story The Book and the Beast by Robert Arthur, Jr. A conjuring book attributed to him causes the gruesome death of any man foolish enough to examine it, until a fire destroys the book.
He is mentioned in the novel It Happened in Boston? by Russell H. Greenan. The narrator is reading the life of Cagliostro when he has his first reverie.
He is mentioned in the novel Kun Lun by Kilburn Hall (2014) where it is revealed that Alessandro Cagliostro, Joseph and Giuseppe Balsamo are just a few of the names that time traveler Count St. Germain has used throughout history.
He is mentioned in the book The Red Lion—The Elixir of Eternal Life by Mária Szepes
Friedrich Schiller wrote an unfinished novel Der Geisterseher (The Ghost-Seer) between 1786 and 1789 about Cagliostro.
Latvian playwright Mārtiņš Zīverts wrote the play Kaļostro Vilcē (Cagliostro in Vilce) in 1967.
The Phantom comic book featured Cagliostro as a character in the story The Cagliostro Mystery from 1988, written by Norman Worker and drawn by Carlos Cruz.
The Kid Eternity comic book featured Cagliostro's risen spirit in issue 3 (1946).
In the DC Comics universe, Cagliostro is described as an immortal (JLA Annual 2), a descendant of Leonardo da Vinci as well as an ancestor of Zatara and Zatanna (Secret Origins 27).
Cagliostro is a character in Robert Anton Wilson's The Historical Illuminatus Chronicles.
Cagliostro is frequently alluded to in Umberto Eco's novel Foucault's Pendulum.
Mikhail Kuzmin wrote a novella called The Marvelous Life of Giuseppe Balsamo, Count Cagliostro (1916).
Cagliostro is a character in Psychoshop, a novel by Alfred Bester and Roger Zelazny.
Cagliostro is mentioned in the story "The Last Defender of Camelot," by Roger Zelazny.
Josephine Balsamo, a descendant of Joseph Balsamo who calls herself Countess Cagliostro, appears in Maurice Leblanc's Arsene Lupin novels.
Cagliostro makes several cameo appearances as a vampire in Kim Newman's Anno Dracula novels.
The manga Rozen Maiden reveals Count Cagliostro to be merely one of many different aliases adopted by the legendary dollmaker Rozen. He was shown to be in prison whittling wood.
There are numerous references to Cagliostro in the detective novel He Who Whispers by John Dickson Carr (aka Carter Dickson), one of his Dr. Gideon Fell mysteries, published by Hamish Hamilton (UK) & Harper (USA) in 1946. In this book, a French professor, Georges Antoine Rigaud, has written a history: Life of Cagliostro. Also an attempted murder committed in He Who Whispers is similar in technique to part of an initiation ceremony undergone by Cagliostro into the lodge of a secret society.
There is a passing and utterly inconsequential reference to Cagliostro in Hilary Mantel's 1992 novel A Place of Greater Safety.
Cagliostro is a character in the 1997 novel, 'Superstition' by David Ambrose; Cagliostro is an acquaintance of the fictional character, Adam Wyatt.
Cagliostro is a Playable character in the Japanese Mobile game Granblue Fantasy.
Cogliostro is a character in Todd McFarlanes's comic saga Spawn, introduced to the series by Neil Gaiman to give greater depth to the curse of spawn. Cogliostro was once a spawn of Hell bound to his duty to the daemon Malebolgia, and manages to free himself of the curse through alchemy and sorcery, teaching Spawn to do the same throughout the series.
He is often mentioned in the book Napoleon's Pyramids by William Dietrich in connection with Freemasons and ancient Egyptian artifacts.
In Robert A. Heinlein's Glory Road, Star uses "Balsamo" as an alias, and refers to Giuseppe as her uncle.
Cagliostro is one of the characters appear in Senki Zeshou Symphogear AXZ
William Bolitho’s “Twelve Against The Gods”, has a section on Cagliostro.


=== Music ===
He appears as a principal character in the 1794 opera Le congrès des rois, a collaborative work of 12 composers.
The French composer Victor Dourlen (1780–1864) composed the first act to Cagliostro, ou Les illuminés which premiered on 27 November 1810. The second and third act were composed by Anton Reicha (1770–1836).
The Irish composer William Michael Rooke (1794–1847) wrote an unperformed work Cagliostro.
Adolphe Adam wrote the opéra comique Cagliostro which premiered on 10 February 1844.
Albert Lortzing wrote in 1850 the libretto for a comic opera in three acts, Cagliostro, but did not compose any music for it.
Johann Strauß (Sohn) wrote the operetta Cagliostro in Wien (Cagliostro in Vienna) in 1875.
The French composer Claude Terrasse (1867–1923) wrote Le Cagliostro which premiered in 1904.
The Polish composer Jan Maklakiewicz (1899–1954) wrote the ballet in three scenes Cagliostro w Warszawie which premiered in 1938.
The Romanian composer Iancu Dumitrescu (1944–) wrote the 1975 work Le miroir de Cagliostro for choir, flute and percussion.
The American composer John Zorn (1953–) composed Cagliostro for solo viola in 2015. The performer uses two bows in the right hand to play on all four strings at once throughout the work.
The opera Cagliostro by the Italian composer Ildebrando Pizzetti (1880–1968) was performed on Italian radio in 1952 and at La Scala on 24 January 1953.
The comic opera Graf Cagliostro was written by Mikael Tariverdiev in 1983.


=== Film ===
Cagliostro has been played in film by:
Fryderyk Jarossy (Kaliostro, 1918)
Reinhold Schünzel (The Count of Cagliostro, 1920)
Hans Stüwe (Cagliostro, 1929)
Ferdinand Marian (Münchausen, 1943)
Orson Welles (Black Magic, 1949)
Howard Vernon (Erotic Rites of Frankenstein, 1972)
Jean Marais (Joseph Balsamo, 1973, TV miniseries)
Bekim Fehmiu (Cagliostro, 1975)
Nodar Mgaloblishvili (Formula of Love, 1984, TV film)
Nicol Williamson (Spawn, 1997)
Christopher Walken (The Affair of the Necklace, 2001)
Robert Englund (The Return of Cagliostro, 2003)
In the 1943 German epic Münchhausen, Cagliostro appears as a powerful, morally ambiguous magician portrayed by Ferdinand Marian.
The French film director Georges Méliès (1861–1938) directed the 1899 film Le Miroir de Cagliostro.
The Japanese animated movie The Castle of Cagliostro draws on Maurice Leblanc's Arsène Lupin novels. Cagliostro appears as the main antagonist of the film, a ruler of a fictional country who influences the world's economy through counterfeiting.
The Mummy (1932), starring Boris Karloff, was adapted from an original story treatment by Nina Wilcox Putnam titled "Cagliostro". Based on Cagliostro and set in San Francisco, the story was about a 3000-year-old magician who survives by injecting nitrates.
Cagliostro and his wife, Lorenza, appear as antagonists in the 2006 anime Le Chevalier d'Eon. While Cagliostro is mostly portrayed as a bumbling money-grubber, Lorenza is shown to have arcane magic powers.
Cagliostro is mentioned frequently in the successful Marvel movie Doctor Strange. The 'Book of Cagliostro: Study of Time' is an ancient artifact containing several dark spells of magic and is used as an item of power.


=== Television ===
Cagliostro appears as a villainous magician in an episode of the 1960s series Thriller, entitled "The Prisoner in the Mirror"; he is played by Henry Daniell and Lloyd Bochner.
In a 1978 episode of the Wonder Woman TV series, a descendant of the Count, still attempting alchemy (and succeeding to the extent of turning lead into gold for a time, after which it turns back into its original form) is the villain, and Wonder Woman, in her Diana Prince identity, indicates that she faced his ancestor, the original Count Cagliostro, in the past.
Cagliostro is mentioned in the Twilight Zone (new) series, in an episode called "The Pharaoh's Curse" when a magician performing the trick says that this act was passed down from a lineage of famous magicians.
Cagliostro is a character in Todd McFarlane's Spawn. An animated television series which aired on HBO from 1997 through 1999. The Character Cagliostro had lived as many identities in his over 800 year life. He was a Hell-Spawn who managed to free himself of the curse though the practice of Alchemy and Sorcery. He accompanies Spawn and teaches him to do the same throughout the series.
In Samurai Jack episode 7 of season 3, Samurai Jack follows a quest for the crystal of Cagliostro.
The 2016 Lupin III yearly special featured a hunt for the treasure of Cagliostro. Prior to this, the name was also used for the 1979 Lupin III theatrical release The Castle of Cagliostro, though with little relation to the historical Cagliostro.


== References ==


== Further reading ==
Giovanni Barberi. The Life of Joseph Balsamo Commonly called Count Cagliostro. London, 1791.
Thomas Carlyle: Count Cagliostro, Fraser's Magazine (July, Aug. 1833).
Carlyle, Thomas. "The French Revolution"
Camilo Castelo Branco. Compêndio da Vida e Feitos de José Bálsamo Chamado Conde de Cagliostro ou O Judeu Errante. E. Chardron, 1874.
Giacomo Casanova, Soliloque d’un penseur (1786).  A pamphlet contra Cagliostro, published anonymously.
Le Couteulx de Canteleu, Les sectes et sociétés secrètes, politiques et religieuses (1863); Ch. XIII “Saint-Germain, Cagliostro, et l’affaire du collier”.
Philippa Faulks and Robert L. D. Cooper. The Masonic Magician; The Life and Death of Count Cagliostro and his Egyptian Rite, London, Watkins, 2008.
Alexander Lernet-Holenia. Das Halsband der Königin (Paul Zsolnay Verlag, Hamburg/Vienna, 1962, historical study on the Affair of the Diamond Necklace, including a description of Cagliostro's background).
W. R. H Trowbridge. Cagliostro: The Splendour and Misery of a Master of Magic (Chapman & Hall, London 1910).


== External links ==
Biography from TheMystica.com, identifying him with Giuseppe Balsamo.
Biography from DJMcAdam.com, an account that just denies this hypothesis without giving a reason.
The Great Cagliostro: Master Illusionist and King of Liars