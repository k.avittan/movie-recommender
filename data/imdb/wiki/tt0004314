Master Minds is an American game show airing on the Game Show Network. The show originally debuted on June 10, 2019 under the title Best Ever Trivia Show hosted by Sherri Shepherd and regularly featuring Ken Jennings, Muffy Marracco. Jonathan Corbblah and Ryan Chaffee. A second season, with the series re-titled Master Minds, debuted on April 6, 2020 with Brooke Burns as the new host.


== Gameplay ==
In both cases, the show features three contestants competing against three "trivia experts." The pool of experts includes Ken Jennings, Muffy Marracco, Raj Dhuwalia, Arianna Haut, Ryan Chaffee, Susannah Brooks, David Schuchinski, and Jonathan Corbblah. The winning contestant faces off against the best-performing expert in the "Ultimate Trivia Challenge."


=== Best Ever Trivia Show ===


==== Round 1 ====
In the first round, one of the contestants selects one of two categories for the round, and then also selects one of the three experts to play along with the round. The contestants are asked four questions in the category, each question has three multiple-choice options. Everyone (including the other two experts) secretly and simultaneously locks in their answers, a contestant's correct answer is worth 50 points if the chosen expert got it right, and 100 points if not.
Answers given by the experts not chosen are generally not revealed.


==== Round 2 ====
The second round is played similarly to the first round. However:

The category not selected in the first round is made available again, along with a new category.
The selection of category and expert is now made by the leading contestant.
A different expert must be selected.
There are only three questions in this round.
The value for each question is now 100 points if the expert gets it right, and 200 points if not.


==== Round 3 ====
The last remaining expert plays in this round, and the leading contestant chooses one of two new categories. Unlike in the previous rounds, the chosen expert secretly submits an answer first, and then, without disclosing their answer, states how confident they are in their answer. Based on the expert's stated confidence level, and their own knowledge, the contestants may either use the expert's answer, sight unseen, for 200 points, or try to select the correct answer themselves for 400 points. This round lasts for a maximum of three questions, the contestant with the most points at the end of this round wins $1,000 and plays the "Ultimate Trivia Challenge.“ If it becomes mathematically impossible for both of the trailing players to overtake the leader, the game ends immediately.
In the event of a tie, the players involved in the tie are asked one additional question. The player who submits the correct answer the fastest wins the game.


==== Ultimate Trivia Challenge ====
The day's winner plays the Ultimate Trivia Challenge against the expert who performed the best on all questions asked during the game (based on most correct answers and fastest time). The contestant and the expert are asked five questions, again with three multiple-choice options. Whoever answers more questions correctly wins the round, if this is the contestant, their winnings are increased to $10,000 plus the $1,000 from the main game for a total $11,000. If either of the two participants is unable to catch up, the round ends immediately.
If the scores are tied after five questions, then the contestant is asked the "Ultimate Trivia Question," a question chosen by all three experts off stage before the show. The contestant also wins the $10,000 and the $1,000 if they answer the Ultimate Trivia Question correctly.
A contestant who wins the $10,000 plus the $1,000 from the main game for a total of $11,000, whether by outscoring the expert or by correctly answering the Ultimate Trivia Question, returns to play again on the next show. Any contestant that wins the Ultimate Trivia Challenge three times is invited back for future shows to play as an expert. This has yet to happen in the show's history.


=== Master Minds ===
In this season, all experts compete against one another in all rounds. Point scores are kept for both contestants and experts (now called "Master Minds"), and the winners of both groups face off in the Ultimate Trivia Challenge.


==== Round 1 ====
All six participants are asked seven questions, each with three multiple-choice options, each question is on a different topic. All participants secretly and simultaneously lock in their answers, each correct answer is worth 100 points.


==== Round 2 ====
This round consists of five open-ended questions, each on a different topic. The first four questions are worth 200 points, and the last is worth 400 points. After the question is read, the contestants and Master Minds buzz in for the right to answer, only the first contestant and the first Master Mind to buzz in will be able to write their answers. If the answer is correct, the points are added to those participants' scores, however, in this round, incorrect answers result in a score deduction. After the 400-point question is asked, the lowest-scoring contestant and the lowest-scoring Master Mind are eliminated from further play.


==== Round 3 ====
In this round, all questions are asked on the buzzers, and answers are given verbally. The two contestants face off for 60 seconds, followed by the Master Minds on a separate set of questions. The first question is worth 500 points, and each subsequent question is worth 100 points more than the previous. As in the previous round, the contestants are not able to buzz in until the question is completed. Unlike Round 2, there is no penalty for an incorrect answer, instead, the opponent has a chance to answer for the same points. The contestant with the higher score wins $1,000, that contestant and the winning Master Mind advance to the Ultimate Trivia Challenge. In the event of a tie, a tiebreaker question is played, the contestant or Master Mind who answers correctly advances.


==== Ultimate Trivia Challenge ====
The round consists of five open-ended questions. The contestant answers the questions while the Master Mind is isolated offstage, the Master Mind then returns to the stage to answer the same five questions. The correct answers are revealed after both the contestant and Master Mind have played, the contestant wins the round by either outscoring the Master Mind, or by achieving a tie and then correctly answering the Ultimate Trivia Question. If the contestant wins the round, their winnings are increased to $10,000 plus $1,000 from the main game and they return on the next show, as before, if the contestant wins the $10,000 and $1,000 three times, they are invited to become a Master Mind themselves.


== Production ==
The show's first season premiered on June 10, 2019 and ran every weekday for 65 episodes until September 6, 2019. A second season of the show in 2020 was announced, along with a name change to Master Minds and Brooke Burns replacing Sherri Shepherd as host. Master Minds debuted with a new format on April 6, 2020 with 65 episodes until June 26, 2020. New episodes will be coming in December 2020.


== References ==


== External links ==
Official website