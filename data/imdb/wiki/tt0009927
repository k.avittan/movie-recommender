Between the Acts is the final novel by Virginia Woolf. It was published shortly after her death in 1941. At the time of her death Woolf had yet to correct the typescript of the novel and a number of critics consider it to be an unfinished novel.The book describes the mounting, performance and audience of a play at a festival in a small English village, just before the outbreak of the Second World War.


== Plot ==
The story takes place in a country house somewhere in England, just before the Second World War. It is the day when the annual pageant is to be performed in the grounds of the house. The pageant is traditionally a celebration of English history, and is attended by the entire local community.
The owner of the house is Bartholomew Oliver, a widower and retired Indian Army officer. His sister Lucy, who is also living in the house, is slightly eccentric but harmless. Bartholomew has a son, Giles, who has a job in London, and is restless and frustrated. His wife, Isa, is staying at the house with her two children and has lost interest in Giles. She is attracted to a local gentleman farmer, Haines, although the relationship goes no further than eye contact. In the course of the day, Mrs Manresa and her friend William Dodge turn up and stay for the pageant. The pageant has been written by Miss La Trobe, a strange and domineering spinster.
The day is interspersed with events leading up to the pageant. Lucy Swithin fusses around making all kinds of preparations, from the decorations to the food. Bartholomew frightens his grandson by jumping out at him from behind a newspaper and then calls him a coward when he cries. Mrs Manresa flirts with Bartholomew and Giles and is clearly being provocative. William Dodge, understood by the others to be homosexual, is the subject of homophobic attitudes by many of the characters but strikes a friendship with Lucy.
The pageant takes place in the evening and is made up of three main parts. After a prologue by a child, the first scene is a Shakespearean scene with romantic dialogue. The second scene is a parody of a restoration comedy, and the third scene is a panorama of Victorian triumph based on a policeman directing the traffic in Hyde Park. The final scene is entitled "Ourselves", at which point Miss La Trobe shocks the audience by turning mirrors on them.
The pageant ends and the audience disperses. Miss La Trobe retreats to the village pub and, brooding over what she perceives to be the pageant's failure, begins to plan her next drama. As darkness descends, cloaking the country house, Giles and Isa are left alone with the final moments of the novel suggesting both conflict and reconciliation.


== Characters ==


== References ==

Wolfe, Graham (2019). Theatre-Fiction in Britain from Henry James to Doris Lessing: Writing in the Wings. Routledge. ISBN 9781000124361.
Hussey, Mark (2011). “Introduction.” Between the Acts. Cambridge University Press, xxxiv-lxxiv.


== External links ==
Between the Acts at Faded Page (Canada)