The Exploits of Elaine is a 1914 American film serial in the damsel in distress genre of The Perils of Pauline (1914).
The Exploits of Elaine tells the story of a young woman named Elaine who, with the help of a detective, tries to find the man, known only as "The Clutching Hand", who murdered her father. The Clutching Hand was the first mystery villain to appear in a film serial. The concept was widely used for the remainder of the format's existence.
The serial stars Pearl White (who also starred in The Perils of Pauline), Arnold Daly, Sheldon Lewis, Creighton Hale, and Riley Hatch. Lionel Barrymore had a small role. The serial was written by Arthur B. Reeve (novel), Charles W. Goddard, and George B. Seitz, and directed by Louis J. Gasnier, Seitz, and Leopold Wharton. The film was produced by the Whartons Studios and distributed by Pathé Exchange, the American distribution branch of the French company Pathé at that time. Pathé was the largest film equipment and production company in the world during the first part of the 20th century.
The film was followed in 1915 by The New Exploits of Elaine.
The serial, which is extant, was named to the United States National Film Registry in 1994 for its cultural and historic importance. 


== Cast ==
Pearl White - Elaine Dodge
Arnold Daly - Detective Craig Kennedy
Creighton Hale - Walter Jameson (Ep. 1, 2, 3)
Raymond Owens - Walter Jameson (Ep. 4-14)
Sheldon Lewis - Perry Bennett / The Clutching Hand. Sheldon Lewis, as the handsome attorney Perry Bennett and The Clutching Hand, was "the first in the serial genre's long parade of unknown menaces."
Edwin Arden - Wu Fang
Leroy Baker - The Butler
Bessie Wharton - Aunt Josephine, Mrs. Dodge
Riley Hatch - President Dodge (as William Riley Hatch)
Robin H. Townley - Limpy Red
Floyd Buckley - Michael
Lionel Barrymore - Undetermined Role
M.W. Rale - Wong Lang Sin.  Long Sin is a Yellow Peril character, who wants the Clutching Hand's treasure map. He became just an agent of Wu Fang in the sequel. Wu Fang appeared in several Pearl White serials.
George B. Seitz
Howard Cody - Undetermined Role
Paul Panzer - Undetermined Role (unconfirmed)


== Production ==
The Exploits of Elaine was based on a book in the "Craig Kennedy, Scientific Detective" series by Arthur B. Reeve. It was a prototype for the scientific mystery serials but has less interest for later audiences. A lot of the technology and science demonstrated in the serial soon became out of date or considered mundane. For example, the serial has to explain the concept of fingerprinting in dramatic fashion. Nevertheless, the serial was a success on its release and led to two sequels, The New Exploits of Elaine (1915) and The Romance of Elaine (1915).


== Cliffhangers ==
Similar to other film serials, each chapter typically closed with a cliffhanger with Elaine in some physical peril or confronted with a shocking revelation. For example, at the close of Chapter 10 Elaine actually dies. She is then brought back to life in the next chapter by Craig Kennedy.


== Critical reception ==
In the opinion of film critic Stedman, this serial is an improvement on The Perils of Pauline, with better acting, script, and direction.


== Chapter titles ==
The Clutching Hand
The Twilight Sleep
The Vanishing Jewels
The Frozen Safe
The Poisoned Room
The Vampire
The Double Trap
The Hidden Voice
The Death Ray
The Life Current
The Hour of Three
The Blood Crystals
The Devil Worshippers
The Reckoning


== References ==


== External links ==
The Exploits of Elaine essay by Margaret Hennefeld at National Film Registry [1]
The Exploits of Elaine essay by Daniel Eagan in America's Film Legacy: The Authoritative Guide to the Landmark Movies in the National Film Registry, A&C Black, 2010 ISBN 0826429777, pages 39 - 40 [2]
The Exploits of Elaine on IMDb
The Exploits of Elaine at AllMovie
Reeve, Arthur B. (1915). The Exploits of Elaine; a Detective Novel. New York: Hearst's International Library Company. (via the Internet Archive)
 The Exploits Of Elaine public domain audiobook at LibriVox