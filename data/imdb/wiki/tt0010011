Close to nature forestry is a management approach treating forest as an ecological system performing multiple functions.  Close to nature silviculture tries to achieve the management objectives with minimum necessary human intervention aimed at accelerating the processes that nature would do by itself more slowly. It works with natural populations of trees, ongoing processes and existing structures using cognitive approach, as in the case of uneven-aged forest (Plenterwald). Its theory and practice takes forest as a self regulating ecosystem and manages it as such.
It aims at overcoming the divorce between forestalist and ecologist management systems of forest. As an important consequence, it concludes that if properly applied, it would render the segregation of forest lands into "productive" and "reserves" or national parks unnecessary.


== History ==


=== Europe ===
The Arbeitsgemeinschaft für Naturgemässe Waldwirtschaft (ANW) (German: Working Group for the Close to Nature Forestry) was established in Germany in 1950. In recent years this association has increased a lot its membership. The main reasons being the increase of ecological consciousness, the growing demand for forest products or services other than wood, the damages suffered by regular forest stands, the forest death fear.  Another term often used to describe a close to nature approach to forest management, especially in Britain and Ireland, is Continuous Cover Forestry.Because of a 1948 forest law, Slovenia has many forests managed according to the principles of close to nature forestry. In 1989 ANW promoted a meeting at Robanov Kot in Slovenia, in the Julian Alps, and the Pro Silva organization was created, with representatives of 10 countries. At present the organization headquarters are in the French region of Alsace.


=== North America ===
In the United States, professor Thomas J. McEvoy has published the book Positive Impact Forestry, which recommends forestry practices similar to those of the "close to nature" movement. He thinks that the precursors of this type of forestry are to be found in Europe, mainly in Germany, and particularly makes mention of Heinrich Cotta, and his famous Cotta's Preface, which highlighted the importance that the study and understanding of nature should have for the foresters. As a more immediate precursor he makes reference to American forester and ecologist Aldo Leopold.
The Ecoforestry Institute consists on educational, non profit and non governmental organizations operating in US and Canada. They propose a forestry based on ecological principles, very similar to those of Pro Silva.


== Forest management/ecosystem management ==
The close to nature approach intends to bridge the discrepancies, or even antagonisms between the silvicultural and ecological visions on the single reality of forest, considering the forest as an ecological system that produces wood. The sought after solution is not to segregate the territory into areas devoted to either forestry or ecology, but to integrate all functions.


== Objective ==
The management has to obtain healthy and stable forest systems that produce wood with a minimum human intervention. The products to obtain, other than wood, are fauna habitats, biodiversity, recreational, aesthetics, and water management. The human action has the object of accelerating natural processes, but not substitute them.


== Silvicultural models ==

Pro Silva recommends to use the uneven-aged forest system, in which the ages, and consequently sizes, of trees in a forest are different. It has the advantage to offer a stable structure regarding natural disasters and plagues, and is very adequate for fauna habitat and biodiversity promotion. It provides a better soil protection, since there is a permanent tree cover.McEvoy considers that in spite of being the most close to nature system, it is difficult to implement, and proposes to use the high regular forest model, in which all trees are of the same age/size, but recommends using a regeneration system with a generous cover, to avoid soil erosion, and prevent excessive light entrance, which would promote the growth of a potent understorey.The Ecoforestry Institute, similarly to Pro Silva, recommends multi-aged and multi-species forests.


== Recommended practices ==
Proposed thinning frequency is about ten years, and intensity low, in order to limit the ingress of excessive light, which could promote too much understory, or the growing of epicormic shoots. It has to be directed to favor the trees that show good prospect for the future. The operations have to be done in a way that will avoid soil compaction or damage to the trees that will remain standing.


== Native/introduced species ==
When foresters plant trees, they may use either native or introduced species. Whenever foresters decide to use a species that is not native, they do it because they think that there are silvicultural advantages linked to this choice, be they the wood quality, ease of management, adaptation to the climatic conditions, shorter production delay, etc. It may be that there is information available about the behavior of this species in the habitat, or the forester is ready to make a trial.
From the ecological point of view, the introducing species is considered as a threat. The introduced species risks being invasive.  Invasives displace local species, resulting in a reduction of biodiversity, a condition also to be expected if great extensions are forested using introduced species.
McEvoy is very clear and strict: introduced species can not be used at all when working in a close to nature forestry system. Pro Silva makes some distinctions, based on species and conditions. Natural forest systems are to be preserved, but the enrichment with certain introduced species may be positive, depending on circumstances.
Pro Silva recommendations
The natural  vegetation systems for each region are an asset to maintain, and constitute an important basis for the silvicultural planning.
Introduced species can, depending on circumstances, supplement natural species, and add economical revenue.
The forest species that are introduced to a region are termed exotic.
The introduction of exotic species should only be permitted after trials conducted from the qualitative and quantitative point of view.


== Herbivore fauna management ==
The herbivore fauna, be it domesticated or wild, acts on tree seedlings and small trees. In high regular forests, the regeneration periods are chosen by the forester, and therefore it is possible to establish some control over fauna action, particularly domesticated fauna, avoiding grazing during regeneration. The un-even aged forest is continuously regenerating, and therefore it is difficult to make it compatible with grazing, and does not admit a high density of wild herbivore fauna. The pressure of herbivore fauna, mainly cervids, in some European forests, has reached an intensity that is threatening the practice of close to nature forestry.


== Economic aspects ==
Forestry's economic profitability has progressively diminished in developed countries, beginning at the end of the 20th century. This has been the result of lower lumber prices and higher operating costs. Because it requires less human intervention, the close to nature forestry has lower labor costs. Also, it encourages the evolution of forests toward higher ecologic and landscape value structures. This is in demand by society, and the payment for ecosystem services is being considered


== Social and Community Aspects ==
Social and community aspects entail the management of the forestry block with the minimum disturbance caused from outside. This is important as nature forestry is taking place not in complete isolation from human activities and animals grazing/browsing but within the same landscape where communities are present. The people around should be well-integrated into the forest lot in order to ensure its sustainable management. In this respect the three important issues are: protect the forest from all types of fires including extinguishing any fires that may occur during forest growth, prevention of all types of illegal removal of timber, poles and the like and, prevention of animals especially browsers entering the reserve. The trio activities are heavily influenced by the people who live around the forest block being managed. It is therefore of paramount importance that the local people should be included in any program leading to the management of nature forestry initiative. The purpose of community involvement in its management is basically to ensure forest protection against activities either introduced such as illegal removal of timber, driving of animals into the forest or natural such as control of bush fires. As the community is in close proximity to the site who has the labor power and other means to prevent destructive activities and their presence is on 24/7 basis, it became all the more important to utilize their potential power for the protection of the forest lot. In this connection, they need to be fully made aware of the purpose of forest management, they be provided with education and other skills development opportunities for forest management, they be allowed to reap some benefits such as the collection of non-timber forest products. The community is also able to provide labor for forest management activities at a cheaper rate compared to the work being done by people brought from elsewhere. It is unrealistic to think about any nature forestry initiative without considering the needs, aspirations, customs of the local community.


== Close to nature forestry/sustainable forestry ==
Close to nature forestry is a sustainable forestry, but the reverse does not apply. These are some of the distinctions between both systems.

Sustainable forestry tries to preserve forests for the future, close to nature forestry aims to improve them.
Sustainable forestry seeks a balance between competing interests, close to nature forestry integrates mutually interacting interests.
Sustainable forestry aims to restrain forestry practice in order to preserve ecological values, close to nature forestry uses ecological principles to promote forestry results.


== See also ==

Continuous cover forestry
Ecoforestry
Forest farming


== References ==


== External links ==
Pro Silva Europe
The Ecoforestry Institute
Forest Vision Germany