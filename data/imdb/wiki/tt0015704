Billy West (September 22, 1892 – July 21, 1975) was a film actor, producer, and director. Active during the silent film era, he is best known as a semi-successful Charlie Chaplin impersonator. Beyond acting, he also directed shorts in the 1910s and 20s, as well as produced films. West ultimately retired in 1935.


== Early career ==

Known as Roy B. Weisberg  (or Weissberg) in the Russian Empire to a Jewish family, West emigrated to Chicago with his family in 1896. He appeared in many short films, first in Apartment No. 13 in 1912.


== Chaplin impersonation ==
In 1917 movie theaters could not get enough Charlie Chaplin comedies, and an enterprising producer hired West, who had been doing comic pantomimes on the vaudeville stage, to make imitation-Chaplin subjects to meet the demand. West, wearing the identical "tramp" costume and makeup, copied Chaplin's movements and gestures so accurately that he is often mistaken for the genuine performer. Reportedly, Chaplin himself saw the Billy West company filming on a Hollywood street once, and told West, "You're a damned good imitator." Some West comedies were later re-released on the home-movie market as "Charlie Chaplin" pictures. Most of the West comedies of 1917–18 resembled the Chaplin comedies of 1916–17, with Oliver Hardy approximating the villainy of Eric Campbell, and Leatrice Joy in the Edna Purviance ingenue role.

King-Bee executives were determined to showcase West in a five-reel feature.Motion Picture News (September 22, 1917)
The King-Bee Film Corporation announces that it is considering the production of a five-reel comedy with Billy West as the star in a modern version of Shakespeare's "Romeo and Juliet," with Billy West playing the great lover.
Moving Picture World (November 10, 1917)
About January 1, Billy West will be seen in the first five-reeler made by the King-Bee company entitled King Solomon.
Motion Picture News (December 1, 1917)

King Solomon, the title of the King-Bee Comedy, featuring Billy West, now under production, has been changed to Old King Sol.   This change was made by Nat I. Spitzer, sales manager of King-Bee after receipt of a letter from a London company, stating that it had the world rights to a picture under the title King Solomon.   Old King Sol will be the first five-reel production made by King- Bee with Billy West featured. The feature was never produced.
West became his own producer at a point. Moving behind the cameras in 1925, West produced a brief series of slapstick comedies co-starring the fat-and-skinny team of Oliver "Babe" Hardy and Bobby Ray, and a series of "Winnie Winkle" comedies with Ethelyn Gibson.
West took small roles in sound films, first for small independent companies and later for Columbia Pictures.


== After cinema ==
He became manager of the Columbia Grill restaurant. He died July 21, 1975, of a heart attack while leaving the Hollywood Park racetrack in Hollywood, California. He is buried at Forrest Lawn Cemetery in Los Angeles.

Kalton C. Lahue and Sam Gill wrote:Billy's tramp was another dimension of Charlie's.  Where Chaplin's little fellow exhibited a tendency towards cynicism, tempered with a degree of hopeful optimism (which was always badly bent by the fade out), Billy's tramp was the cheerful optimism who is treated pretty decently by faith.  Most of his problems came about as a result of his own carefree ineptitude.


== Filmography ==


=== Actor ===
Apartment No. 13 (1912)Unicorn Film Service (1916)

His Married Life (December 1, 1916)
Bombs and Boarders (1916)
His Waiting Career (1916)King-Bee Film Corporation (1917-1918)

Back Stage (May 15, 1917) (A print is held at Nederlands Filmmuseum)
The Hero (June 1, 1917)
Dough Nuts (June 15, 1917)
Cupid's Rival (July 1, 1917)
The Villain (July 15, 1917)
The Millionaire (August 1, 1917) (Nederlands Filmmuseum)
The Goat (August 15, 1917)
The Fly Cop (September 15, 1917)
The Chief Cook (October 1, 1917)
The Candy Kid (October 15, 1917)
The Hobo (November 1, 1917)
The Pest (working title: The Freeloader) (November 15, 1917)
The Band Master (December 1, 1917)
The Slave (December 15, 1917)
The Stranger (working title: The Prospector) (January 1, 1918)
His Day Out (working title: The Barber) (January 15, 1918)
The Rogue (February 15, 1918)
The Orderly (March 1, 1918)
The Scholar (March 15, 1918)
The Messenger (April 1, 1918)
The Handy Man (May 1, 1918)
Bright and Early (May 15, 1918)
The Straight and Narrow (June 1, 1918)
Playmates (July 1, 1918)
Beauties in Distress (working title: The King of the Volcano) (July 15, 1918)Higrade Film Enterprises (1918)

Bunco Billy Billy (1918)
Bombs and Bull (1918)
Billy in Harness (1918)Bull's Eye Film Corporation (1918-1919)

He's in Again (working title: A Good Day) (December 15, 1918)
Rolling Stone (January 20, 1919)
Ship Ahoy! (February, 1919)
Her First False Hare (May 19, 1919)
Her Nitro Night (December 1, 1919)
Haunted Hearts (December 15, 1919)
The Chauffeur (1919)
Lured (1919)
Coppers and Scents (1919)
Out of Tune (1919)
Soaked (1919)
The Wrong Flat (newspaper ads: January 28, 1919 and July 19, 1919)Reelcraft (1920)

The Strike Breaker (March, 1920)
The Masquerader (April, 1920)
Brass Buttons (April, 1920)
The Dreamer (May, 1920)
The Beauty Shop (May, 1920)
The Artist (June, 1920)
Hard Luck (June, 1920)
What Next? (July, 1920)
Italian Love (July, 1920)
Hands Up (August, 1920)
Going Straight (August, 1920)
The Dodger
Mustered Out
Happy Days
Foiled
Cleaning Up
Blue Blood and BevoJoan Comedies (1920-1921)

Sweethearts (November, 1920)
Service Stripes (December, 1921)
He's In Again (January, 1921)
The Conquering Hero (February, 1921)
Best Man Wins (March, 1921)
He Loves Her Still (April, 1921)
Why Marry? (May 1921)
Happy Days (June, 1921)
Italian Love (August, 1921?)
The Darn Fool (October, 1921?)
The Sap (1921)Sunrise Comedies (1922)

Don't Be Foolish (1922)
You'd Be Surprised (1922)
Wedding Dumbbells (1922)
I'm Here (1922)Smart Films (1922)

Why Worry? (1922) (Not related to Why Worry? from 1923)
It's Going to Be a Coal Winter (1922)Broadway Comedies (1923-1925)

One Exciting Evening (October 1, 1923)
Be Yourself (November 1, 1923)
Hello Bill (December 1, 1923)
Pay Up (January 1, 1924)
Hello, Stranger (February 1, 1924)
The Nervous Reporter (March 1, 1924)
Not Wanted (April 1, 1924)
Oh, Billy! (May 1, 1924)
Dyin' for Love (May 15, 1924)
Two After One (June 1, 1924)
That's That (July 1, 1924)
Don't Slip (September 1, 1924)
Line's Busy (September 15, 1924)
Love (October 15, 1924)
Meet Father (November 15, 1924)
Watch Out! (December 15, 1924)
Phone Troubles (newspaper ad: July 5, 1924)
This could be an alternate title for Line's Busy.
Midnight Watch (newspaper ad: August 29, 1924)
So Long, Dad (newspaper ad: September 10, 1924)
Believe Me (working title: Start Here) (January 15, 1925)
Hard-Hearted Husbands (February 15, 1925)
Rivals (March 15, 1925)
Copper Butt-Ins (April 15, 1925)
West Is West (May 15, 1925)
Fiddlin' Around (June 15, 1925)
The Joke's On You (working title: A Day's Vacation) (July 15, 1925)
So Long, Bill (August 15, 1925)
Hard Boiled Yeggs (1926)The Features (1926-1927)

Thrilling Youth (August 3, 1926)
Oh, Billy, Behave (October 27, 1926)
The Trouble Chaser (March 9, 1927)
Lucky Fool (working title: Help! Police!) (April 18, 1927)Later Career

One Hour to Play (1927)
Possibly consists of footage from features
The Shadow of the Eagle (1932)
Ex-Lady (1933)
Jimmy the Gent (1934)
The Whole Town's Talking (1935)More minor roles on IMDb


=== Producer ===
Stick Around (1925)
Hey, Taxi! (1925)
Hop to It! (1925)
They All Fall (1925)


== References ==


== External links ==
Billy West on IMDb
Billy West at Find a Grave
Filmography