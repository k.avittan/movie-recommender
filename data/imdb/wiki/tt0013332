Lorna Doone: A Romance of Exmoor is a novel by English author Richard Doddridge Blackmore, published in 1869. It is a romance based on a group of historical characters and set in the late 17th century in Devon and Somerset, particularly around the East Lyn Valley area of Exmoor. In 2003, the novel was listed on the BBC's survey The Big Read.


== Introduction ==
Blackmore experienced difficulty in finding a publisher, and the novel was first published anonymously in 1869, in a limited three-volume edition of just 500 copies, of which only 300 sold. The following year it was republished in an inexpensive one-volume edition and became a huge critical and financial success. It has never been out of print.
It received acclaim from Blackmore's contemporary, Margaret Oliphant, and as well from later Victorian writers including Robert Louis Stevenson, Gerard Manley Hopkins, and Thomas Hardy. George Gissing wrote in a letter to his brother Algernon that the novel was 'quite admirable, approaching Scott as closely as anything since the latter'. A favourite among females, it is also popular among male readers, and was chosen by male students at Yale in 1906 as their favourite novel.By his own account, Blackmore relied on a "phonologic" style for his characters' speech, emphasising their accents and word formation. He expended great effort, in all of his novels, on his characters' dialogues and dialects, striving to recount realistically not only the ways, but also the tones and accents, in which thoughts and utterances were formed by the various sorts of people who lived on Exmoor in the 17th century.
Blackmore incorporated real events and places into the novel. The Great Winter described in chapters 41–45 was a real event.
He himself attended Blundell's School in Tiverton which serves as the setting for the opening chapters. One of the inspirations behind the plot is said to be the shooting of a young woman at a church in Chagford, Devon, in the 17th century. Unlike the heroine of the novel, she did not survive, but is commemorated in the church. Apparently, Blackmore invented the name "Lorna", possibly drawing on a Scottish source.According to the preface, the work is a romance and not a historical novel, because the author neither "dares, nor desires, to claim for it the dignity or cumber it with the difficulty of an historical novel." As such, it combines elements of traditional romance, of Sir Walter Scott's historical novel tradition, of the pastoral tradition, of traditional Victorian values, and of the contemporary sensation novel trend. The basis for Blackmore's historical understanding is Macaulay's History of England and its analysis of the Monmouth rebellion. Along with the historical aspects are folk traditions, such as the many legends based around both the Doones and Tom Faggus. The composer Puccini once considered using the story as the plot for an opera, but abandoned the idea.


== Plot summary ==

The book is set in the 17th century in the Badgworthy Water region of Exmoor in Devon and Somerset, England. John (in West Country dialect, pronounced "Jan") Ridd is the son of a respectable farmer who was murdered in cold blood by one of the notorious Doone clan, a once noble family, now outlaws, in the isolated Doone Valley. Battling his desire for revenge, John also grows into a respectable farmer and takes good care of his mother and two sisters. He falls hopelessly in love with Lorna, a girl he meets by accident, who turns out to be not only (apparently) the granddaughter of Sir Ensor Doone (lord of the Doones), but destined to marry (against her will) the impetuous, menacing, and now jealous heir of the Doone Valley, Carver Doone. Carver will let nothing get in the way of his marriage to Lorna, which he plans to force upon her once Sir Ensor dies and he comes into his inheritance.

Sir Ensor dies, and Carver becomes lord of the Doones. John Ridd helps Lorna escape to his family's farm, Plover's Barrows. Since Lorna is a member of the hated Doone clan, feelings are mixed toward her in the Ridd household, but she is nonetheless defended against the enraged Carver's retaliatory attack on the farm. A member of the Ridd household notices Lorna's necklace, a jewel that she was told by Sir Ensor belonged to her mother. During a visit from the Counsellor, Carver's father and the wisest of the Doone family, the necklace is stolen from Plover's Barrows. Shortly after its disappearance, a family friend discovers Lorna's origins, learning that the necklace belonged to a Lady Dugal, who was robbed and murdered by a band of outlaws. Only her daughter survived the attack. It becomes apparent that Lorna, being evidently the long-lost girl in question, is in fact heiress to one of the largest fortunes in the country, and not a Doone after all (although the Doones are remotely related, being descended from a collateral branch of the Dugal family). She is required by law, but against her will, to return to London to become a ward in Chancery. Despite John and Lorna's love for one another, their marriage is out of the question.
King Charles II dies, and the Duke of Monmouth (the late king's illegitimate son) challenges Charles's brother James for the throne. The Doones, abandoning their plan to marry Lorna to Carver and claim her wealth, side with Monmouth in the hope of reclaiming their ancestral lands. However, Monmouth is defeated at the Battle of Sedgemoor, and his associates are sought for treason. John Ridd is captured during the rebellion. Innocent of all charges, he is taken to London by an old friend to clear his name. There, he is reunited with Lorna (now Lorna Dugal), whose love for him has not diminished. When he thwarts an attack on Lorna's great-uncle and legal guardian Earl Brandir, John is granted a pardon, a title, and a coat of arms by the king and returns a free man to Exmoor.
In the meantime, the surrounding communities have grown tired of the Doones and their depredations. Knowing the Doones better than any other man, John leads the attack on their land. All the Doone men are killed, except the Counsellor (from whom John retrieves the stolen necklace) and his son Carver, who escapes, vowing revenge. When Earl Brandir dies and Judge Jeffreys, as Lord Chancellor, becomes Lorna's guardian, she is granted her freedom to return to Exmoor and marry John. During their wedding, Carver bursts into the Church of St Mary, Oare. He shoots Lorna and flees. Distraught and filled with blinding rage, John pursues and confronts him. A struggle ensues in which Carver is left sinking in a mire. Exhausted and bloodied from the fight, John can only pant as he watches Carver slip away. He returns to discover that Lorna is not dead, and after a period of anxious uncertainty, she survives to live happily ever after.


== Chronological key ==
The narrator, John Ridd, says he was born on 29 November 1661; in Chapter 24, he mentions Queen Anne as the current monarch, so the time of narration is 1702–1714 making him 40–52 years old. Although he celebrates New Year's Day on 1 January, at that time in England the year in terms of A.D. "begins" Annunciation Style on 25 March, so 14 February 1676 would still be 1675 according to the old reckoning. Most of the dates below are given explicitly in the book.


== Other versions and cultural references ==

Lorna Doone is also a shortbread cookie made by Nabisco.
Title character Lorna Doone, a B-movie actress in a Thomas Tryon novella was christened in honor of Blackmore's character.
Lorna Doone was said to be the favourite book of Australian bushranger and outlaw Ned Kelly, who may have thought of the idea of his armour by reading of the outlaw Doones "with iron plates on breast and head."
The phrase "Lorna Doone" is used in Cockney rhyming slang for spoon.
Lorna Doone is a character portrayed by Christine McIntyre in The Three Stooges shorts The Hot Scots and Scotched in Scotland.
The book inspired the song "Pangs of Lorna" by Kraus.
There is reference to R.D. Blackmore's Lorna Doone in John Galsworthy's play Justice (1910).
Atholl Oakeley, British wrestling promoter, was fascinated by the book, and billed Exmoor-born wrestler Jack Baltus as Carver Doone in the 1930s.


== References ==


== Further reading ==
Blackmore, R. D. (1908) Lorna Doone: a romance of Exmoor; Doone-land edition; with introduction and notes by H. Snowden Ward and illustrations by Mrs. Catharine Weed Ward. lii, 553 pp., plates. London: Sampson Low, Marston and Company (includes "Slain by the Doones", pp. 529–53)
Delderfield, Eric (1965?) The Exmoor Country: [a] brief guide & gazetteer; 6th ed. Exmouth: The Raleigh Press
Elliott-Cannon, A. (1969) The Lorna Doone Story. Minehead: The Cider Press


== External links ==
 Lorna Doone: A Romance of Exmoor at Project Gutenberg Lorna Doone: A Romance of Exmoor at Project Gutenberg – a lavishly illustrated edition (Burrows Brothers Company, 1889)
IMDb Listing of movies based on Lorna Doone
HTML online text of Lorna Doone
Lorna Doone at Silver Sirens
 Lorna Doone public domain audiobook at LibriVox