A talker is a chat system that people use to talk to each other over the Internet. Dating back to the 1980s, they were a predecessor of instant messaging.
A talker is a communication system precursor to MMORPGs and other virtual worlds such as Second Life. Talkers are a form of online virtual worlds in which multiple users are connected at the same time to chat in real-time. People log into the talkers remotely (usually via Telnet), and have a basic text interface with which to communicate with each other.
The early talkers were similar to MUDs with most of the complex game machinery stripped away, leaving just the communication level commands – hence the name "talker". ew-too was, in fact, a MUD server with the game elements removed.
Most talkers are free and based on open source software.
Many of the online metaphors used on talkers, such as "rooms" and "residency", were established by these early pioneering services and remain in use by modern 3D interfaces such as Second Life.


== History of talkers ==


=== Early Internet talkers ===
In the school year of 1983–1984, Mark Jenks and Todd Krause, two students at Washington High School in Milwaukee, wrote a software program for talking among a group of people. They used the PDP-11 at the Milwaukee Public Schools (MPS) central office. After searching around the PDP-11 files and directories, Mark found the PDP-11 program talk, and decided that they could do better. The system had approximately 40 300–2400 bit per second modems attached to it, with a single phone number and a hunt group. The talk program was named TALK and was written to handle many options that are seen in IRC today: tables, private messages, actions, moderators and inviting to tables. Another talk server called NUTS, which stood for Neil's Unix Talk Server, was released in 1993 and became fairly popular on Unix systems. Its command system was broadly based on the Unaxcess BBS and being room based it took a lot of inspiration from MUDs too. The source code was given away and became the basis of a huge number of variants and rewrites during the 1990s.
Cat Chat was the first Internet / JANET talker, created in 1990.


=== Talker hosting ===
In 1996, talker.com was formed, the first server to sell space for talkers, later giving it the name Dragonroost.  The server had over 90 talkers on it at one time, during the mid-1990s boom of talkers. A number of other hosts started up as alternative hosting companies to talker.com. Talker.com ceased hosting any other talkers besides its owners' on September 28, 2009.


== See also ==
Instant messaging
IRC
ICQ
MUD
Online chat
LPMud
Talk, a Unix text chat program
Telnet


== References ==


== Further reading ==
Thomas, Angela (July 2007). Youth Online: Identity and Literacy in the Digital Age. Peter Lang Publishing. ISBN 1-4331-0033-9. - an ethnographic study of youth online, analyzes textual interactions, including at Middle Earth-related talkers


== External links ==
BBC h2g2 (wikipedia-style) Article on talkers
Cheeseplant's House History, of some historical significance.
Playground Plus Code Base