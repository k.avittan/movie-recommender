Traditionally an oath (from Anglo-Saxon āð, also called plight) is either a statement of fact or a promise with wording relating to something considered sacred as a sign of verity. A common legal substitute for those who conscientiously object to making sacred oaths is to give an affirmation instead. Nowadays, even when there is no notion of sanctity involved, certain promises said out loud in ceremonial or juridical purpose are referred to as oaths. "To swear" is a verb used to describe the taking of an oath, to making a solemn vow.


== Etymology ==
The word come from Anglo-Saxon āð judicial swearing, solemn appeal to deity in witness of truth or a promise," from Proto-Germanic *aiþaz (source also of Old Norse eiðr, Swedish ed, Old Saxon, Old Frisian eth, Middle Dutch eet, Dutch eed, German Eid, Gothic aiþs "oath"), from PIE *oi-to- "an oath" (source also of Old Irish oeth "oath"). Common to Celtic and Germanic, possibly a loan-word from one to the other, but the history is obscure and it may ultimately be non-Indo-European, in reference to careless invocations of divinity, from the late 12th century.


== Divine oath ==
Oaths usually have referred to a deity significant in the cultural sphere in question. The reciter's personal views upon the divinity of the aspects considered sacred in a predictated text of an oath may or may not be taken in to account. There might not be alternative personal proclamations with no mention of the sacred dogma in question, such as affirmations, to be made. This might mean an impasse to those with unwillingness to edify the dogma they see as untrue and those who decline to refer to sacred matters on the subject at hand.The essence of a divine oath is an invocation of divine agency to be a guarantor of the oath taker's own honesty and integrity in the matter under question. By implication, this invokes divine displeasure if the oath taker fails in his or her sworn duties. It therefore implies greater care than usual in the act of the performance of one's duty, such as in testimony to the facts of the matter in a court of law.A person taking an oath indicates this in a number of ways. The most usual is the explicit "I swear", but any statement or promise that includes "with * as my witness" or "so help me *", with '*' being something or someone the oath-taker holds sacred, is an oath. Many people take an oath by holding in their hand or placing over their head a book of scripture or a sacred object, thus indicating the sacred witness through their action: such an oath is called corporal. However, the chief purpose of such an act is for ceremony or solemnity, and the act does not of itself make an oath.


== Historical development as a legal concept ==

Making vows and taking oaths became a symbolic concept in law practice that developed over time in different cultures.


=== Jewish tradition ===
The concept of oaths is deeply rooted within Judaism. It is found in Genesis 8:21, when God swears that he will "never again curse the ground because of man and never again smite every living thing". This repetition of the term never again is explained by Rashi, the pre-eminent biblical commentator, as serving as an oath, citing the Talmud Shavous 36a for this ruling.The first personage in the biblical tradition to take an oath is held to be Eliezer, the chief servant of Abraham, when the latter requested of the former that he not take a wife for his son Isaac from the daughters of Canaan, but rather from among Abraham's own family.
The foundational text for oath making is in Numbers 30:2: "When a man voweth a vow unto the Lord, or sweareth an oath to bind his soul with a bond, he shall not break his word; he shall do according to all that proceedeth out of his mouth." According to the Rabbis, a neder (usually translated as "vow") refers to the object, a shâmar (usually translated as "oath") to the person. The passage distinguishes between a neder and a shvua, an important distinction between the two in Halakha: a neder changes the status of some external thing, while a shvua initiates an internal change in the one who swears the oath.


=== Roman tradition ===
In the Roman tradition, oaths were sworn upon Iuppiter Lapis or the Jupiter Stone located in the Temple of Jupiter, Capitoline Hill. Iuppiter Lapis was held in the Roman tradition to be an Oath Stone, an aspect of Jupiter in his role as divine law-maker responsible for order and used principally for the investiture of the oathtaking of office.
According to Cyril Bailey, in "The Religion of Ancient Rome" (1907):

We have, for instance, the sacred stone (silex) which was preserved in the temple of Iuppiter on the Capitol, and was brought out to play a prominent part in the ceremony of treaty-making. The fetial, who on that occasion represented the Roman people, at the solemn moment of the oath-taking, struck the sacrificial pig with the silex, saying as he did so, "Do thou, Diespiter, strike the Roman people as I strike this pig here to-day, and strike them the more, as thou art greater and stronger." Here no doubt the underlying notion is not merely symbolical, but in origin the stone is itself the god, an idea which later religion expressed in the cult-title specially used in this connection, Iuppiter Lapis.
The punisher of broken oaths was the infernal deity Orcus.


=== Hindu tradition ===

In Hindu epics, like the Ramayana and the Mahabharata, oaths, called pratigya, are taken seriously. It is mentioned that people would give up their lives, but not break a vow. Due to this, King Dasharatha took an oath for his Queen Kaikeyi (on her maid, Manthara's insistence) and thus had to exile his favorite son, Lord Rama along with his wife Devi Sita and brother Lakshmana for fourteen years in the forest.
In the Mahabharata, Devrata took an oath of celibacy so that Satyavati's father would marry her to Devrata's father, King Shantanu. He also took an oath to not rule the kingdom and remain loyal to the king, who would be a descendant of Satyavati. Thus, Devarata got the name Bhishma, which means someone, who has taken a terrible oath. Many others also took oaths that they fulfilled.


=== Greek tradition ===
Walter Burkert has shown that since Lycurgus of Athens (d. 324 BCE), who held that "it is the oath which holds democracy together", religion, morality and political organization had been linked by the oath, and the oath and its prerequisite altar had become the basis of both civil and criminal, as well as international law.


=== Christian tradition ===
Various religious groups have objected to the taking of oaths, most notably the Religious Society of Friends (Quakers) and Anabaptist groups, like Mennonites, Amish, Hutterites and Schwarzenau Brethren.  This is principally based on Matthew 5:34–37, the Antithesis of the Law. Here, Christ is reported as having said: "I say to you: 'Swear not at all'".  James the Just stated in James 5:12, "Above all, my brothers, do not swear—not by heaven or by earth or by anything else. Let your 'Yes' be yes, and your 'No', no, or you will be condemned." Beyond this scriptural authority, Quakers place importance on being truthful at all times, so the testimony opposing oaths springs from a view that "taking legal oaths implies a double standard of truthfulness" suggesting that truthfulness in legal contexts is somehow more important than truthfulness in non-legal contexts and that truthfulness in those other contexts is therefore somehow less important.
Not all Christians interpret this reading as forbidding all types of oaths, however. Opposition to oath-taking among some groups of Christian caused many problems for these groups throughout their history. Quakers were frequently imprisoned because of their refusal to swear loyalty oaths. Testifying in court was also difficult; George Fox, Quakers' founder, famously challenged a judge who had asked him to swear, saying that he would do so once the judge could point to any Bible passage where Jesus or his apostles took oaths — the judge could not, but this did not allow Fox to escape punishment. Legal reforms from the 18th century onwards mean that everyone in the United Kingdom now has the right to make a solemn affirmation instead of an oath. The United States has permitted affirmations since it was founded; it is explicitly mentioned in the Constitution. Only President Franklin Pierce has chosen to affirm rather than swear at his inauguration.
As late as 1880, Charles Bradlaugh was denied a seat as an MP in the Parliament of the United Kingdom because of his professed atheism as he was judged unable to swear the Oath of Allegiance in spite of his proposal to swear the oath as a "matter of form".


=== Islamic tradition ===
Islam takes the fulfillment of oaths extremely seriously.

God does not hold you responsible for the mere utterance of oaths; He holds you responsible for your actual intentions. If you violate an oath, you shall atone by feeding ten poor people from the same food you offer to your own family, or clothing them, or by freeing a slave. If you cannot afford this, then you shall fast three days. This is the atonement for violating the oaths that you swore to keep. You shall fulfill your oaths. God thus explains His revelations to you, that you may be appreciative.


=== Germanic tradition ===

Germanic warrior culture was significantly based on oaths of fealty.
A prose passage inserted in the eddic poem Helgakviða Hjörvarðssonar relates: Hedin was coming home alone from the forest one Yule-eve, and found a troll-woman; she rode on a wolf, and had snakes in place of a bridle. She asked Hedin for his company. "Nay," said he. She said, "Thou shalt pay for this at the bragarfull." That evening the great vows were taken; the sacred boar was brought in, the men laid their hands thereon, and took their vows at the bragarfull. Hedin vowed that he would have Sváva, Eylimi's daughter, the beloved of his brother Helgi; then such great grief seized him that he went forth on wild paths southward over the land, and found Helgi, his brother.
Such Norse traditions are directly parallel to the "bird oaths" of late medieval France, such as the voeux du faisan (oath on the pheasant) or the (fictional) voeux du paon (oath on the peacock). Huizinga, The Autumn of the Middle Ages (ch. 3); Michel Margue, "Vogelgelübde" am Hof des Fürsten. Ritterliches Integrationsritual zwischen Traditions- und Gegenwartsbezug (14. – 15. Jahrhundert)


=== Modern law ===

In the modern law, oaths are made by a witness to a court of law before giving testimony and usually by a newly appointed government officer to the people of a state before taking office. However, in both of those cases, an affirmation can usually be replaced with a written statement, only if the author swears the statement is true. This statement is called an affidavit. The oath given to support an affidavit is frequently administered by a notary, who will certify the giving of the oath by affixing her or his seal to the document. Willfully delivering a false oath (or affirmation) is the crime of perjury. 
There are some places where there is a confusion between the "oath" and other statements or promises. For example, the current Olympic Oath is really a pledge, not properly an oath, since there is only a promise but there is no appeal to a sacred witness. Oaths may also be confused with vows, but vows are really just a special kind of an oath.


== Hand gestures ==

Instead of, or in addition to, holding one's hand upon an object of ceremonial importance, it can be customary for a person swearing an oath to hold a raised hand in a specific gesture. Most often the right hand is raised. This custom has been explained with reference to medieval practices of branding palms.


=== Schwurhand ===


=== Serbian custom ===


=== International customs ===
The Scout Sign can be made while giving the Scout Promise. In Scouting for Boys the movement's founder, Robert Baden-Powell, instructed: "While taking this oath the scout will stand, holding his right hand raised level with his shoulder, palm to the front, thumb resting on the nail of the digitus minimus (little finger) and the other three fingers upright, pointing upwards."


== Types of oaths ==

Hippocratic Oath, an oath historically taken by physicians and other healthcare professionals swearing to practice medicine honestly.
Oath of allegiance, an oath whereby a subject or citizen acknowledges a duty of allegiance and swears loyalty to monarch or country.
Oath of citizenship,  an oath taken by immigrants that officially naturalizes immigrants into citizens.
Oath of office, an oath or affirmation a person takes before undertaking the duties of an office.
Pauper's oath, a sworn statement or oath by a person that he or she is completely without any money or property.
Veterinarian's Oath, an oath taken by veterinarians as practitioners of veterinary medicine in a manner similar to the Hippocratic Oath.


=== Famous oaths ===
Anti-Modernist oath
Bhishma
Hittite military oath
Ironclad oath
Lwów Oath
Oaths in Freemasonry
Oath More Judaico (Jewish)
Oaths of Strasbourg
Omertà
Scout Oath
Tennis Court Oath


=== Fictional oaths ===
Oath of Eorl
Oath of Fëanor
Oath of the Peach Garden
Oath Of the Skull (The Phantom)


== See also ==

ACLU of N.C. & Syidah Matteen v. State of North Carolina a court case in a state of the United States about taking oaths by different scriptures. The results have reversed several times.
Australasian Police Multicultural Advisory Bureau has several publications for Australia dealing with multi-faith issues and A Practical Reference to Religious Diversity for Operational Police and Emergency Services covers oaths as well as many other topics (in review as of 12/2/2006 but the 2nd Edition is available.)
Confirmation
Ephebic Oath
Performativity
So help me God
Statutory declaration
Sworn declaration
Vow


== Notes ==


== References ==
Bailey, Cyril (1907). The Religion of Ancient Rome. London, UK: Archibald Constable & Co. Ltd. (Source: Project Gutenberg. Accessed: March 16, 2011)


== External links ==
Oaths in the Qur'an
Courtroom oaths from the North Dakota Supreme Court website (jury oath, witness oath and so on)
North Carolina faith leaders supporting Quran oath
Comments about John Quincy Adams' Oath of Office
The Oath, BBC Radio 4 discussion with Alan Sommerstein, Paul Cartledge & Mary Beard (In Our Time, Jan. 5, 2006)