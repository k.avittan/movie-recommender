Bring It On: Fight to the Finish is a 2009 American teen comedy film starring Christina Milian, Rachele Brooke Smith, Cody Longo, Vanessa Born, Gabrielle Dennis and Holland Roden. Directed by Bille Woodruff and the fifth installment in the series of stand-alone films starting with the 2000 film Bring It On. The film was released direct-to-video on DVD and Blu-ray on September 1, 2009.


== Plot ==
Lina Cruz is a tough, sharp-witted cheerleader from East L.A. who transfers to Malibu Vista High School after her widowed mother remarries a wealthy man. Lina not only finds herself a fish-out-of-water at her new high school, she also faces off against Avery, the snobbish and ultra-competitive All-Star cheerleading Captain who leads her own squad, 'The Jaguars' after the high school squad, 'The Sea Lions', did not vote for her to be Captain.
After Lina upsets Sky, her stepsister, she is forced to join The Sea Lions by her mother. She goes into the school stadium to check them out and finds Evan, Avery’s younger brother, practicing basketball. Lina impresses Evan, and The Sea Lions vote her Captain. When Lina is Captain, Gloria, her friend from East L.A, is called to help her out.
After a team member from the Sea Lions quits, Lina calls her other friend, Treyvonetta ("Trey") to come and help her out. At a basketball game, the Sea Lions go on and perform, but a fall takes place, so The Jaguars, led by Avery, are there and save them from their misery. Lina calls for back up and takes the Sea Lions to an impromptu flavour school to work on their movements. She later meets Evan waiting for her there, and Victor, Gloria's boyfriend, befriends him.
The next day, Lina comes up with the idea of The Sea Lions competing in the All Star Championship. After the team agrees to double up their practices, The Sea Lions are invited to a Rodeo Drive Divas (RDD) party. Following Sea Lion practice, Gloria and Trey are expelled when Avery goes to the principal and gets Lina in trouble for sneaking them in without approval. Lina refuses to go to the dance, but is confronted by Sky. Evan takes Lina as his date to the party, where Gloria and Trey turn up. Lina and Avery proceed to have a dance off. Lina wins the dance off, and Avery tells her that she does not belong in Malibu using multiple racial slurs. Lina, angered, runs off the dance floor and outside, where Evan follows her. There she breaks up with Evan, sends for Gloria to take her back to East L.A, and quits being Captain of The Sea Lions.
There, Lina is confronted by Gloria and Trey, so she stays at Malibu and becomes Captain of The Sea Lions again. The next day at school, half of the Sea Lions squad quits because of Lina's routines and practices. Avery and Kayla approach Lina, Christina, and Sky to tell them that they are dreaming if they think they have a chance at winning the Spirit Championship. Sky loses her temper and threatens to hurt the girls if they don’t back off.
Lina then goes on a field trip to East L.A with the remaining Sea Lions, where Gloria has persuaded a gym to sponsor the Sea Lions and some of the members of The East L.A. Rough Riders as an All Star squad. By combining the Sea Lions and the Rough Riders, they become The Dream Team. The next day after practice, while Lina is at her locker talking with Sky and Christina, Evan kisses her and tells her exactly how he feels in front of a crowd in the hallway that is recording the entire scene. They get back together, and Lina and her team make it to the final round of the All Star Championship and end up defeating The Jaguars, after which Avery breaks down. Evan comforts her but motions a "call me" signal to Lina over Avery's shoulder. The film ends with Lina taking a picture with Trey, Gloria and Sky, claiming all of them as her cheer sisters.


== Cast ==
Christina Milian as Catalina "Lina" Cruz
Rachele Brooke Smith as Avery Whitbourne
Cody Longo as Evan Whitbourne
Vanessa Born as Gloria
Gabrielle Dennis as Treyvonetta (Trey)
Holland Roden as Sky
Nikki SooHoo as Christina
Meagan Holder as Kayla
Laura Cerón as Isabel Cruz
David Starzyk as Henry
Brandon Gonzales as Victor
Giuliana Rancic as Herself
Prima J as Themselves


== Production ==
The movie was filmed at locations around Malibu. The field shots were filmed at Occidental College, in western Los Angeles County. The spirit championships were filmed at Glendale Community College in Glendale, California and California State University Northridge in Northridge, California.


== Soundtrack ==
This was the only film in the series other than the first to actually have a soundtrack album released - via Arsenal Records on September 15, 2009.

Lean Like a Cholo  - Down AKA Kilo (3:18)
Rock Like Us - Kottonmouth Kings (2:36)
I Gotta Get to You - Christina Milian (4:05)
Ya Mama, Ya Mama - Alabina (4:03)
Popular - The Veronicas (2:42)
Corazon (You're Not Alone) - Prima J (3:04)
Bounce - Fizz & Boog (3:59)
Get It Girl - World's First (3:59)
Candy Swirl - Montana Tucker(3:09)
Footworkin' - Keke Palmer (3:15)
Lift Off - Rachel Suter (3:46)
Whine Up (Johnny Vicious Spanish Mix) - Kat Deluna (3:18)
Whoa Oh! (Me vs. Everyone) - Forever the Sickest Kids (3:25)
Dale - L.A. Rouge (2:27)
Mueve La Caderas - Andrew Gross (1:03)
Viva La Celebration - Andrew Gross (1:45)


== Marketing ==
American Cheerleader Magazine had an interview and photo shoot with cast members Christina Milian, Cody Longo, Vanessa Born, Rachele Brooke Smith and Gabrielle Dennis, which can be seen in the August 2009 issue. Christina Milian is also on the cover of American Cheerleader Magazine's August 2009 issue.


== Reception ==
Common Sense Media gave the film 2 out of 5 stars.


== References ==


== External links ==
Bring It On: Fight to the Finish on IMDb