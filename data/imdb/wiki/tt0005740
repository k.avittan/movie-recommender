Maxim's is a restaurant in Paris, France, located at No. 3 rue Royale in the 8th arrondissement. It is known for its Art Nouveau interior decor. In the mid 20th century Maxim's was regarded as the most famous restaurant in the world.


== History ==

Maxim's was founded as a bistro in 1893 by Maxime Gaillard, formerly a waiter. It became one of the most popular and fashionable restaurants in Paris under its next owner, Eugene Cornuché. He gave the dining room its Art Nouveau decor, installed a piano, and made sure that it was always filled with beautiful women. Cornuché was accustomed to say: "An empty room... Never! I always have a beauty sitting by the window, in view from the sidewalk." It was so famous that the third act of Franz Lehár's 1905 operetta The Merry Widow was set there.
In 1913, Jean Cocteau said of Maxim's: "It was an accumulation of velvet, lace, ribbons, diamonds and what all else I couldn't describe. To undress one of these women is like an outing that calls for three weeks' advance notice, it's like moving house."  
In 1932, Octave Vaudable, owner of the restaurant Noel Peters, bought Maxim's. He started selecting his clients, favouring the regulars, preferably famous or rich, beginning a new era of prestigious catering under the famous Vaudable family which lasted more than half a century. Famous guests of the 1930s included Edward VIII and Jean Cocteau, a close friend and neighbor of the Vaudables. The playwright Georges Feydeau wrote a popular comedy called La Dame de chez Maxim ("The Lady from Maxim's").
During World War II, Maxim's was the most popular Parisian restaurant of the German high command and collaborationist celebrities.  Hermann Göring, Otto Abetz, and Ernst Jünger favored Maxim's when in Paris.  Due to the support of officials, Maxim's enjoyed protected status during the occupation: its employees were not deported and it was exempt from food restrictions.  The French resistance closed Maxim's after the liberation, but it reopened in September 1946.Maxim's was also immensely popular with the international celebrities of the 1950s, with guests such as Aristotle Onassis, Maria Callas, the Duke of Windsor and his wife Wallis Simpson, Porfirio Rubirosa, Max Ophüls, and Barbara Hutton. When the restaurant was renovated at the end of the decade, workmen discovered a treasure trove of lost coins and jewelry that had slipped out of the pockets of the wealthy and been trapped between the cushions of the banquettes.
In 1956, the fame of the restaurant led to it becoming the namesake for a Western-inspired restaurant in Hong Kong, Maxim's Boulevard. The restaurant achieved rapid success and eventually developed into the conglomerate Maxim's Caterers, the largest catering corporation in Hong Kong by revenue and market share, and one of the largest in all of East Asia.
In the 1970s, Brigitte Bardot caused a scandal when she entered the restaurant barefoot. Other guests of this time period were Sylvie Vartan, John Travolta, Jeanne Moreau, Barbra Streisand, and Kiri Te Kanawa. It was during the fifties, sixties and seventies that Maxim's, under the management of Octave Vaudable's son, Louis Vaudable, became the most famous restaurant in the world and one of the most expensive ones as well. With his wife Magguy, Louis Vaudable assured Maxim's international reputation.
François Vaudable, who had been directing the restaurant by his father's side for years, pursued the work of his family which gave Maxim's its era of glory. In 1981, more attracted by the scientific field than by the jet-set, the Vaudables offered to sell Maxim's to fashion designer Pierre Cardin. A rich Arab had offered to buy the restaurant, but they were upset at the idea of it falling into foreign hands.  Cardin eventually accepted the offer. Under his management, an Art Nouveau museum was later created on three floors of the building and a cabaret was established, which Cardin filled each night with songs from the beginning of the 20th century.
The chefs who worked at Maxim's included a young Wolfgang Puck. A New York location was opened in 1985, but it closed in 2000.


== Today ==

Today, the restaurant and the Maxim's brand belong to Pierre Cardin. Other Maxim's restaurants have been opened in Tokyo, London, Beijing, Monaco, Geneva, Brussels, and Doha. The Maxim's brand has been extended to a wide range of goods and services.


== Popular culture ==
Maxim's was featured in Franz Lehár's 1905 operetta, The Merry Widow.
It was mentioned in the 1937 Jean Renoir film, La Grande Illusion.
Two sequences in the 1958 musical film Gigi were filmed on location at Maxim's.
It was mentioned in the 1966 episode of I Dream of Jeannie "My Master, the Thief".
Maxim's was also mentioned in three episodes of Bewitched, "Paris Witches Style" "The Joker is a Card" "Serena Stops the Show".
Maxim's was mentioned in the 1982 Top 40 song I Predict, by Sparks.
It was featured in the 2011 Woody Allen film, Midnight in Paris.
It appears in a scene from the movie Sidney Sheldon's Bloodline, featuring Audrey Hepburn and Ben Gazzara.


== See also ==
Maxim's Art Nouveau "Collection 1900"


== References ==


== External links ==
Maxim's de Paris official website
Art Nouveau Museum official website
Pierre Cardin official website
Maxim's restaurant in Brussels