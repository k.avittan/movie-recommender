Chain of custody (CoC), in legal contexts, is the chronological documentation or paper trail that records the sequence of custody, control, transfer, analysis, and disposition of materials, including physical or electronic evidence. Of particular importance in criminal cases, the concept is also applied in civil litigation and more broadly in drug testing of athletes and in supply chain management, e.g. to improve the traceability of food products, or to provide assurances that wood products originate from sustainably managed forests. It is often a tedious process that has been required for evidence to be shown legally in court. Now however, with new portable technology that allows accurate laboratory quality results from the scene of the crime, the chain of custody is often much shorter which means evidence can be processed for court much faster.
The term is also sometimes used in the fields of history, art history, and archives as a synonym for provenance (meaning the chronology of the ownership, custody or location of a historical object, document or group of documents), which may be an important factor in determining authenticity.


== Criminal evidence ==
When evidence can be used in court to convict persons of crimes, it must be handled in a scrupulously careful manner to prevent tampering or contamination. The idea behind recording the chain of custody is to establish that the alleged evidence is in fact related to the alleged crime, rather than having, for example, been "planted" fraudulently to make someone appear guilty.
Establishing chain of custody is made of both a chronological and logical procedure, especially important when the evidence consists of fungible goods. In practice, this most often applies to illegal drugs which have been seized by law enforcement personnel. In such cases, the defendant at times disclaims any knowledge of possession of the controlled substance in question. Accordingly, the chain of custody documentation and testimony is presented by the prosecution to establish that the substance in evidence was in fact in the possession of the defendant.
An identifiable person must always have the physical custody of a piece of evidence. In practice, this means that a police officer or detective will take charge of a piece of evidence, document its collection, and hand it over to an evidence clerk for storage in a secure place. These transactions, and every succeeding transaction between the collection of the evidence and its appearance in court, should be completely documented chronologically in order to withstand legal challenges to the authenticity of the evidence. Documentation should include the conditions under which the evidence is gathered, the identity of all evidence handlers, duration of evidence custody, security conditions while handling or storing the evidence, and the manner in which evidence is transferred to subsequent custodians each time a transfer occurs (along with the signatures of persons involved at each step).
Maintaining a chain of custody is essential for the forensic scientist that is working on a specific criminal case. The documentation of evidence is key for maintaining a chain of custody because everything that is done to the piece of evidence must be listed and whoever came in contact with that piece of evidence is accountable for what happens to it. This prevents police officers and other law officials from contaminating the evidence or taking the piece of evidence.


=== Example ===
An example of chain of custody would be the recovery of a bloody knife at a murder scene:

Officer Andrew collects the knife and places it into a container, then gives it to forensics technician Bill.
Forensics technician Bill takes the knife to the lab and collects fingerprints and other evidence from the knife. Bill then gives the knife and all evidence gathered from the knife to evidence clerk Charlene.
Charlene then stores the evidence until it is needed, documenting everyone who has accessed the original evidence (the knife, and original copies of the lifted fingerprints).The chain of custody requires that from the moment the evidence is collected, every transfer of evidence from person to person be documented and that it be provable that nobody else could have accessed that evidence. It is best to keep the number of transfers as low as possible.
In the courtroom, if the defendant questions the chain of custody of the evidence it can be proven that the knife in the evidence room is the same knife found at the crime scene. However, if there are discrepancies and it cannot be proven who had the knife at a particular point in time, then the chain of custody is broken and the defendant can ask to have the resulting evidence declared inadmissible.
Chain of custody is also used in most chemical sampling situations to maintain the integrity of the sample by providing documentation of the control, transfer, and analysis of samples. Chain of custody is especially important in environmental work where sampling can identify the existence of contamination and can be used to identify the responsible party.


== Supply chain management ==
ISO standard 22095, Chain of custody – General terminology and models was published in 2020. The ISO describes this standard as "a simple solution" designed "to help boost manufacturer and consumer confidence, reducing supply chain costs by addressing issues like risk, loss of time and conditions of production".


== References ==


== See also ==
Evidence bag
Evidence management
Provenance
Traceability


== External links ==
Federal Rules of Evidence Rule 901 (Authentication and Identification Rule used for Chain of Custody)
Non Federal Chain of Custody