A Small Town Girl (aka The Small Town Girl) was a 1914 American silent drama film directed by Allan Dwan and featuring Lon Chaney, Pauline Bush and Rupert Julian. The film is now considered to be lost.Some sources list this as a 1915 film, but the Blake book on Chaney says it was released specifically on Nov. 7, 1914. A still from the film exists that shows Lon Chaney as the Pimp, seated second from left.  Chaney's co-star in this film, Rupert Julian, later directed him in The Phantom of the Opera (1925). 


== Plot ==
Ruth's uncle is the proprietor of the only hotel in Maplehurst, a small rural town. When she was orphaned years earlier, she was adopted by Dick, a young hotel clerk who now loves her dearly. Ruth cares for Dick, but feels he is too rustic. One day, a slick young Snob from the East arrives in Maplehurst and Ruth is taken in by his flashy clothes and fast car. Dick is crushed when his sweetheart goes off with the scoundrel. On one of her long drives with the snob, they stop at an inn, where he gets her drunk and takes advantage of her, impregnating her. 
When he moves back East, Ruth follows him, but he soon tires of her and throws her to the curb. She later gives birth to their child in a rundown big city boarding house, and the snob's family refuses to even see her when she goes to them for help. A local pimp (Lon Chaney) suggests that she become a prostitute, and Ruth starts to consider suicide; however, an elderly, childless couple take an interest in her and her baby. 
Ruth decides to write her uncle for help, and Dick intercepts the letter and sends her money. Her uncle soon after passes away, and Dick inherits the hotel. He searches for Ruth to bring her back home. Meanwhile the Snob has gone West, where the rugged environment has made a man out of him. He comes back to Ruth ready to accept his paternal responsibility, but she scorns him and refuses to even let him see their child.


== Cast ==
Pauline Bush as Ruth (Pauline in some sources)
Lon Chaney as The Pimp
William Lloyd as the Hotel Proprietor
Richard Rosson as Dick, the Boyfriend
Rupert Julian as The Snob
Murdock MacQuarrie as The Snob's Father
Martha Mattox


== Reception ==
Moving Picture World wrote: "A three-reel number, written by Beatrice Van and produced by Allan Dwan. Pauline Bush is featured as a country girl lured to the city by a visiting stranger, who soon deserts her. He returns later, after the baby is born, but she spurns him and goes to her country lover. This is appealingly acted and well photographed, the chief drawback being that all of the situations are very familiar. In a production of this length there should have been more novelty. At the same time the story is connected and well developed."
Motion Picture News wrote: "This drama featuring Pauline Bush and Rupert Julian contains just a plain story and nothing of the impossible. Bit in its plainness it is extraordinary....The photography is good, the scenes and settings that appear are well laid, but not much variety is given. The whole drama will appeal more to the serious and better educated of the average audience." 


== References ==


== External links ==
A Small Town Girl on IMDb