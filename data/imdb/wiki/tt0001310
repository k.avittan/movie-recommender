May to December is a British sitcom which ran for 39 episodes, from 2 April 1989 to 27 May 1994 on BBC1. The series was created by Paul Mendelson and produced by Cinema Verity.  The series was nominated for the BAFTA award for "Best Television Comedy Series" in 1991, but lost out to The New Statesman.Set in Pinner, London, it revolved around the romance between a widowed solicitor, Alec Callender (played by veteran television actor Anton Rodgers) and a much younger woman, Zoë Angell (played by Eve Matheson in series one and two, and by Lesley Dunlop in series three to six).The title of the show comes from the Anderson-Weill song "September Song", which is sung during the credits.
The titles of all of the episodes of May to December are taken from songs. Most are from musicals, reflecting Alec and Zoë's mutual interest, but some later ones are hits from the 50s and 60s.In keeping with a number of other BBC sitcoms, such as To the Manor Born, Yes, Minister, As Time Goes By and a number of others, in 1992 a radio adaptation of episodes was proposed. Episodes would have been adapted from the original television scripts, with Lesley Dunlop playing Zoë throughout. However, the radio version was dropped and never recorded, due to the changing demographic of BBC radio's listeners and output in the early 1990s.


== Characters ==
Alec Callender - (Anton Rodgers) - a widowed, Scottish solicitor who tries to balance his work, his family and his new romance with a much younger woman, Zoë.  Alec idolises Perry Mason, and has a "signed" photo reading "Cheers, Alec, let's crack open a case sometime. Perry." He wishes his cases could be more exciting, like Perry's, but instead the most excitement he sees is the occasional divorce.
Zoë Angell - (Eve Matheson, series 1-2) & (Lesley Dunlop, series 3-6) - a feisty PE teacher, who divorced her husband after discovering he'd been having an affair. She briefly dated the boys' PE teacher, Roy Morgan Jones, but the relationship is not serious and ends almost as quickly as it began.
Miles Henty (Clive Francis) - Alec's surviving partner (Semple died years earlier though his name is still on the door) who is a bit of a womaniser even though he is married to a sculptress named Annabelle. He initially encourages Alec to take Zoë out on a date. He is prominent in the first series and often takes Hillary to lunch, among other places.
Jamie Callender (Paul Venables) -  Alec's son, is an easy-going bachelor, who approves of his father's relationship with Zoë. He has a great sense of humour and seldom ever in a bad mood. He's a bit of a free spirit and lightens any scene he is in. He starts as a law student and takes over for Mr. Henty in as Alec's partner after he gets his licence. He is the polar opposite of his sister.
Simone Callender (Carolyn Pickles) - Alec's daughter. Simone is married to a vicar and has a high moral standard that borders on prudish. She is vehemently opposed to her father's relationship with Zoë, especially when they purchase a house a little too close for comfort to the vicarage, but slowly warms to her over the series.
Vera Flood - (Frances White) - The senior secretary who comes across as prim and bookish but has secretly written a romance novel using a pseudonym.  Her love life isn't as active as the other characters, especially in the beginning, but she eventually marries a man named Gerald Tipple.
Hillary - (Rebecca Lacey) - the ditzy and scatterbrained junior secretary. She is the exact opposite in every way to Miss Flood from her personality to the way she dresses. She has a long-standing relationship with boyfriend Derek, though she is briefly engaged to Miss Flood's nephew, Anthony. She is written out, offscreen, at the start of the sixth series, with the character said to have moved to the Isle of Wight, due to actress Rebecca Lacey choosing to focus on pursuing larger film roles. Writer Paul Mendelson pitched a spin-off series around the character of Hillary, both to allow Lacey more flexibility around potential film projects, and to capitalise on the character's popularity with viewers, but the project was not green lit.
Debbie - (Chrissie Cotterill) - Zoë's sister. While she doesn't completely approve of their relationship, she is more tolerant of it then most of the family. She works in her parents' greengrocery store and has a long term relationship with the much talked about, but never seen Trevor, who works nights. She is primarily Zoë's sounding board.
Dot Burgess - (Kate Williams) - Zoë's opinionated mother. She has quite a bit in common with Alec and likes him, but doesn't approve of her daughter's relationship with him. She has infrequent appearances. She owns a greengrocer's with Zoë's father Barry (Ronnie Stevens), from whom she later separates briefly before reconciling to the strains of "Cherry Pink and Apple Blossom White".
Fleur - (Sophie Louise Collinson) (episode 1), (Natalie Boonarec) (episode 2), and (Charlotte Perry) (episodes 3-6) - Alec and Zoë's daughter who is born in the first episode of the fifth series.
Rosie - Ashley Jensen - Replaces Hillary. A mad Scotswoman who is always asking: "Hoo's yer juices?"


== Episodes ==


=== Series 1 ===
Note: The original run of Series 1 had a different opening credits sequence, partly featuring an animated oak tree through the different seasons of the year, as well as a slightly different recording the theme song. A repeat run of the first series that began in August 1990 replaced these with the opening visuals and audio from the second series (which had also incorporated a number of shots from the first series). All later repeats and subsequent DVD releases continue to use the second series version of the opening.


=== Series 2 ===


=== Christmas Special ===


=== Series 3 ===


=== Series 4 ===


=== Series 5 ===


=== Series 6 ===


== DVD release ==
May to December series one and series two are both available on DVD in the UK, distributed by Acorn Media UK.


== References ==


== External links ==
May to December on IMDb
May to December at British Comedy Guide
Detailed episode guide to May to December at Newton's laws of television
May to December - UK TV Schedules and Episode Guide