Maid to Order is a 1987 American comedy/fantasy film starring Ally Sheedy, Beverly D'Angelo, Michael Ontkean, Valerie Perrine, Dick Shawn, Tom Skerritt and Merry Clayton.
This film is a variation on the Cinderella formula: the fairy godmother is not the means to a better life for the heroine but rather the nemesis. However, rather than doing so out of malice, Stella hopes to teach the heroine there is more to life than financial security.


== Plot ==
Jessie Montgomery (Ally Sheedy) is a spoiled rich girl in her mid-20s whose hard partying lifestyle and lack of self-respect as well as a lack of respect for others is starting to wear thin on her single father Charles (Tom Skerritt), a wealthy philanthropist, and on her boyfriend Brent (Jason Beghe), who breaks up with her after finally getting frustrated with her immature and self-destructive behavior. When Jessie is arrested for drunk driving and drug possession, she finally pushes Charles beyond his limits. He blames himself, as many years ago his wife died of cancer at a time when Jessie was a small child. Feeling it would help her cope with the death of her mother, Charles seldom laid down the law, often spoiling Jessie. While in the company of family retainer Woodrow (Theodore Wilson), he says the one thing he thought he would never say...he wishes he had never had a daughter. Stella Winston (Beverly D'Angelo), a fairy godmother who has been assigned to the Montgomery family, to keep Jessie from ruining her life. Stella casts a spell "erasing" Jessie's life as it is, as if Charles did never have a daughter. Then, she bails Jessie out of jail.
When Jessie tries to go home, her father doesn't recognize her and claims that he has no daughter. Stella appears and tells her that she's getting what she deserves. She tells Jessie that if she wants to eat and have a place to sleep, she will have to find employment. A college dropout who has never worked a day in her life, Jessie is forced to find work as a live-in maid for an eccentric couple named Starkey (Valerie Perrine and Dick Shawn) who got rich by winning the lottery some years back who are trying to make it in the music industry as talent agents.
Jessie has to interact with the other mansion staff, consisting of former singer-turned-cook Audrey (Merry Clayton), Hispanic servant Maria (Begoña Plaza), and chauffeur Nick (Michael Ontkean), a struggling songwriter. Jessie learns the true meaning of love, friendship, hard work, and self-respect. When she chooses the happiness of her new friends over her own, she is rewarded with having her old life returned to her, and being reunited with her father. However, her attitude is now much improved.


== Cast ==
Ally Sheedy – Jessie Montgomery
Beverly D'Angelo – Stella Winston
Michael Ontkean – Nick McGuire
Valerie Perrine – Georgette Starkey
Dick Shawn – Stan Starkey
Tom Skerritt – Charles Montgomery
Merry Clayton – Audrey James
Begoña Plaza – Maria
Rain Phoenix – Brie Starkey (as Rainbow Phoenix)
Theodore Wilson - Woodrow
Jason Beghe - Brent
Katey Sagal - Louise
Khandi Alexander - Hooker in Jail
Henry Woolf - Jailer


== Reception ==
Reviews were negative. Roger Ebert of the Chicago Sun-Times "found it too easy to anticipate most of the big moments and too hard to believe that Sheedy was really a spoiled, mean-spirited rich bitch." Janet Maslin in The New York Times praised Sheedy, saying her "petulant manner and her air of faint distaste for her surroundings are just right for this role. And she shows herself to be an able physical comedienne."


== Filming locations ==
Jessie's father's mansion is located at 365 S Hudson Street, Los Angeles, California. The Starkey mansion, where Jessie worked, is located at 32596 Pacific Coast Hwy, Malibu, California. The gas station where Jessie kicks the vending machine is located on Sunset and Swathmore in the Pacific Palisades.


== Availability ==
The movie was released on VHS by International Video Entertainment in 1988 and again in 1991 by Avid Home Entertainment. In 2002, Artisan Entertainment released the film on DVD without bonus features and was presented only in full screen.


== Soundtrack ==
"Spirit in the Sky"
Performed by Doctor and The Medics
Music and Lyrics by Norman Greenbaum
"I'm On My Own"
Performed by Craig Thomas
Music and Lyrics by Ralph Jones and Claudette Raiche
"Clean Up Woman"
Performed by Bekka Bramlett
Music and Lyrics by Clarence Reid and Willie Clark
"I Can Still Shine"
Performed by Merry Clayton
Music and Lyrics by Ashford and Simpson
"It's in His Kiss"
Performed by Merry Clayton
Music and Lyrics by Rudy Clark
"976-Self Service"
Music by Ralph Jones and Claudette Raiche
"Fernando the Champ"
Music and Lyrics by Rudy Regaiado


== External links ==
Maid to Order on IMDb
Maid to Order at AllMovie
Maid to Order at Rotten Tomatoes
Maid to Order at Box Office Mojo
Maid to Order is available for free download at the Internet Archive


== References ==