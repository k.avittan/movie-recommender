My Fair Lady is a musical based on George Bernard Shaw's 1913 play Pygmalion, with book and lyrics by Alan Jay Lerner and music by Frederick Loewe. The story concerns Eliza Doolittle, a Cockney flower girl who takes speech lessons from professor Henry Higgins, a phonetician, so that she may pass as a lady. The original Broadway and London shows starred Rex Harrison and Julie Andrews.
The musical's 1956 Broadway production was a notable critical and popular success. It set a record for the longest run of any show on Broadway up to that time. It was followed by a hit London production, a popular film version, and many revivals.  My Fair Lady has been called "the perfect musical".


== Plot ==


=== Act I ===
In Edwardian London, Eliza Doolittle is a Cockney flower girl with a thick, unintelligible accent. The noted phonetician Professor Henry Higgins encounters Eliza at Covent Garden and laments the vulgarity of her dialect ("Why Can't the English?"). Higgins also meets Colonel Pickering, another linguist, and invites him to stay as his houseguest. Eliza and her friends wonder what it would be like to live a comfortable life ("Wouldn't It Be Loverly?").
Eliza's father, Alfred P. Doolittle, stops by the next morning searching for money for a drink ("With a Little Bit of Luck"). Soon after, Eliza comes to Higgins's house, seeking elocution lessons so that she can get a job as an assistant in a florist's shop. Higgins wagers Pickering that, within six months, by teaching Eliza to speak properly, he will enable her to pass for a proper lady.
Eliza becomes part of Higgins's household. Though Higgins sees himself as a kindhearted man who merely cannot get along with women ("I'm an Ordinary Man"), to others he appears self-absorbed and misogynistic. Eliza endures Higgins's tyrannical speech tutoring. Frustrated, she dreams of different ways to kill him ("Just You Wait"). Higgins's servants lament the stressful atmosphere ("The Servants' Chorus").
Just as Higgins is about to give up on her, Eliza suddenly recites one of her diction exercises in perfect upper-class style ("The Rain in Spain"). Though Mrs Pearce, the housekeeper, insists that Eliza go to bed, she declares she is too excited to sleep ("I Could Have Danced All Night").
For her first public tryout, Higgins takes Eliza to his mother's box at Ascot Racecourse ("Ascot Gavotte"). Though Eliza shocks everyone when she forgets herself while watching a race and reverts to foul language, she does capture the heart of Freddy Eynsford-Hill. Freddy calls on Eliza that evening, and he declares that he will wait for her in the street outside Higgins' house ("On the Street Where You Live").
Eliza's final test requires her to pass as a lady at the Embassy Ball. After more weeks of preparation, she is ready. All the ladies and gentlemen at the ball admire her, and the Queen of Transylvania invites her to dance with the prince ("Embassy Waltz"). A Hungarian phonetician, Zoltan Karpathy, attempts to discover Eliza's origins. Higgins allows Karpathy to dance with Eliza.


=== Act II ===
The ball is a success; Karpathy has declared Eliza to be a Hungarian princess. Pickering and Higgins revel in their triumph ("You Did It"), failing to pay attention to Eliza. Eliza is insulted at receiving no credit for her success, packing up and leaving the Higgins house. As she leaves she finds Freddy, who begins to tell her how much he loves her, but she tells him that she has heard enough words; if he really loves her, he should show it ("Show Me").
Eliza and Freddy return to Covent Garden but she finds she no longer feels at home there. Her father is there as well, and he tells her that he has received a surprise bequest from an American millionaire, which has raised him to middle-class respectability, and now must marry his lover. Doolittle and his friends have one last spree before the wedding ("Get Me to the Church on Time").
Higgins awakens the next morning. He finds himself out of sorts without Eliza. He wonders why she left after the triumph at the ball and concludes that men (especially himself) are far superior to women ("A Hymn to Him"). Pickering notices the Professor's lack of consideration, and also leaves the Higgins house.
Higgins despondently visits his mother's house, where he finds Eliza. Eliza declares she no longer needs Higgins ("Without You"). As Higgins walks home, he realizes he's grown attached to Eliza ("I've Grown Accustomed to Her Face"). At home, he sentimentally reviews the recording he made the day Eliza first came to him for lessons, hearing his own harsh words. Eliza suddenly appears in his home. In suppressed joy at their reunion, Professor Higgins scoffs and asks, "Eliza, where the devil are my slippers?"


== Characters and original Broadway cast ==
The original cast of the Broadway stage production:
Eliza Doolittle, a young Cockney flowerseller – Julie Andrews
Henry Higgins, a professor of phonetics – Rex Harrison
Alfred P. Doolittle, Eliza's father, a dustman – Stanley Holloway
Colonel Hugh Pickering, Henry Higgins's friend and fellow phoneticist – Robert Coote
Mrs. Higgins, Henry's socialite mother – Cathleen Nesbitt
Freddy Eynsford-Hill, a young socialite and Eliza's suitor – John Michael King
Mrs. Pearce, Higgins's housekeeper – Philippa Bevans
Zoltan Karpathy, Henry Higgins's former student and rival – Christopher Hewett


== Musical numbers ==


== Background ==
In the mid-1930s, film producer Gabriel Pascal acquired the rights to produce film versions of several of George Bernard Shaw's plays, Pygmalion among them. However, Shaw, having had a bad experience with The Chocolate Soldier, a Viennese operetta based on his play Arms and the Man, refused permission for Pygmalion to be adapted into a musical. After Shaw died in 1950, Pascal asked lyricist Alan Jay Lerner to write the musical adaptation. Lerner agreed, and he and his partner Frederick Loewe began work. But they quickly realised that the play violated several key rules for constructing a musical: the main story was not a love story, there was no subplot or secondary love story, and there was no place for an ensemble. Many people, including Oscar Hammerstein II, who, with Richard Rodgers, had also tried his hand at adapting Pygmalion into a musical and had given up, told Lerner that converting the play to a musical was impossible, so he and Loewe abandoned the project for two years.During this time, the collaborators separated and Gabriel Pascal died. Lerner had been trying to musicalize Li'l Abner when he read Pascal's obituary and found himself thinking about Pygmalion again. When he and Loewe reunited, everything fell into place. All of the insurmountable obstacles that had stood in their way two years earlier disappeared when the team realised that the play needed few changes apart from (according to Lerner) "adding the action that took place between the acts of the play". They then excitedly began writing the show. However, Chase Manhattan Bank was in charge of Pascal's estate, and the musical rights to Pygmalion were sought both by Lerner and Loewe and by Metro-Goldwyn-Mayer, whose executives called Lerner to discourage him from challenging the studio. Loewe said, "We will write the show without the rights, and when the time comes for them to decide who is to get them, we will be so far ahead of everyone else that they will be forced to give them to us." For five months Lerner and Loewe wrote, hired technical designers, and made casting decisions. The bank, in the end, granted them the musical rights.
Lerner settled on the title My Fair Lady, relating both to one of Shaw's provisional titles for Pygmalion, Fair Eliza, and to the final line of every verse of the nursery rhyme "London Bridge Is Falling Down".  Recalling that the Gershwins' 1925 musical Tell Me More had been titled My Fair Lady in its out-of-town tryout, and also had a musical number under that title, Lerner made a courtesy call to Ira Gershwin, alerting him to the use of the title for the Lerner and Loewe musical.Noël Coward was the first to be offered the role of Henry Higgins, but he turned it down, suggesting the producers cast Rex Harrison instead. After much deliberation, Harrison agreed to accept the part. Mary Martin was an early choice for the role of Eliza Doolittle, but declined the role. Young actress Julie Andrews was "discovered" and cast as Eliza after the show's creative team went to see her Broadway debut in The Boy Friend. Moss Hart agreed to direct after hearing only two songs. The experienced orchestrators Robert Russell Bennett and Philip J. Lang were entrusted with the arrangements, and the show quickly went into rehearsal.The musical's script used several scenes that Shaw had written especially for the 1938 film version of Pygmalion, including the Embassy Ball sequence and the final scene of the 1938 film rather than the ending for Shaw's original play. The montage showing Eliza's lessons was also expanded, combining both Lerner's and Shaw's dialogue. The artwork on the original Playbill (and the sleeve of the cast recording) is by Al Hirschfeld, who drew the playwright Shaw as a heavenly puppetmaster pulling the strings on the Henry Higgins character, while Higgins in turn attempts to control Eliza Doolittle.


== Productions ==


=== Original Broadway production ===

The musical had its pre-Broadway tryout at New Haven's Shubert Theatre. At the first preview Rex Harrison, who was unaccustomed to singing in front of a live orchestra, "announced that under no circumstances would he go on that night...with those thirty-two interlopers in the pit".  He locked himself in his dressing room and came out little more than an hour before curtain time. The whole company had been dismissed but were recalled, and opening night was a success. My Fair Lady then played for four weeks at the Erlanger Theatre in Philadelphia, beginning on February 15, 1956.
The musical premiered on Broadway March 15, 1956, at the Mark Hellinger Theatre in New York City. It transferred to the Broadhurst Theatre and then The Broadway Theatre, where it closed on September 29, 1962, after 2,717 performances, a record at the time. Moss Hart directed and Hanya Holm was choreographer. In addition to stars Rex Harrison, Julie Andrews and Stanley Holloway, the original cast included Robert Coote, Cathleen Nesbitt, John Michael King, and Reid Shelton. Harrison was replaced by Edward Mulhare in November 1957 and Sally Ann Howes replaced Andrews in February 1958. By the start of 1959, it was the biggest grossing Broadway show of all-time with a gross of $10 million.The Original Cast Recording, released on April 2, 1956, went on to become the best-selling album in the country in 1956.


=== Original London production ===
The West End production, in which Harrison, Andrews, Coote, and Holloway reprised their roles, opened on April 30, 1958, at the Theatre Royal, Drury Lane, where it ran for five and a half years (2,281 performances). Edwardian musical comedy star Zena Dare made her last appearance in the musical as Mrs. Higgins. Leonard Weir played Freddy. Harrison left the London cast in March 1959, followed by Andrews in August 1959 and Holloway in October 1959.


=== 1970s revivals ===
The first Broadway revival opened at the St. James Theatre on March 25, 1976, and ran there until December 5, 1976; it then transferred to the Lunt-Fontanne Theatre, running from December 9, 1976, until it closed on February 20, 1977, after a total of 377 performances and 7 previews. The director was Jerry Adler, with choreography by Crandall Diehl, based on the original choreography by Hanya Holm. Ian Richardson starred as Higgins, with Christine Andreas as Eliza, George Rose as Alfred P. Doolittle and Robert Coote recreating his role as Pickering. Both Richardson and Rose were nominated for the Tony Award for Best Actor in a Musical, with the award going to Rose.
A London revival opened at the Adelphi Theatre in October 1979, with Tony Britton as Higgins, Liz Robertson as Eliza, Dame Anna Neagle as Higgins' mother, Peter Bayliss, Richard Caldicot and Peter Land. The revival was produced by Cameron Mackintosh and directed by the author, Alan Jay Lerner. A national tour was directed by Robin Midgley. Gillian Lynne choreographed.  Britton and Robertson were both nominated for Olivier Awards.


=== 1981 and 1993 Broadway revivals ===
Another Broadway revival of the original production opened at the Uris Theatre on August 18, 1981, and closed on November 29, 1981, after 120 performances and 4 previews.  Rex Harrison recreated his role as Higgins, with Jack Gwillim, Milo O'Shea, and Cathleen Nesbitt, at 93 years old reprising her role as Mrs. Higgins. The revival co-starred Nancy Ringham as Eliza. The director was Patrick Garland, with choreography by Crandall Diehl, recreating the original Hanya Holm dances.A new revival directed by Howard Davies opened at the Virginia Theatre on December 9, 1993, and closed on May 1, 1994, after 165 performances and 16 previews. The cast starred Richard Chamberlain, Melissa Errico and Paxton Whitehead. Julian Holloway, son of Stanley Holloway, recreated his father's role of Alfred P. Doolittle. Donald Saddler was the choreographer.


=== 2001 London revival; 2003 Hollywood Bowl production ===
Cameron Mackintosh produced a new production on March 15, 2001, at the Royal National Theatre, which transferred to the Theatre Royal, Drury Lane on July 21. Directed by Trevor Nunn, with choreography by Matthew Bourne, the musical starred Martine McCutcheon as Eliza and Jonathan Pryce as Higgins, with Dennis Waterman as Alfred P. Doolittle. This revival won three Olivier Awards: Outstanding Musical Production, Best Actress in a Musical (Martine McCutcheon) and Best Theatre Choreographer (Matthew Bourne), with Anthony Ward receiving a nomination for Set Design. In December 2001, Joanna Riding took over the role of Eliza, and in May 2002, Alex Jennings took over as Higgins, both winning Olivier Awards for Best Actor and Best Actress in a Musical respectively in 2003. In March 2003, Anthony Andrews and Laura Michelle Kelly took over the roles until the show closed on August 30, 2003.A UK tour of this production began September 28, 2005. The production starred Amy Nuttall and Lisa O'Hare as Eliza, Christopher Cazenove as Henry Higgins, Russ Abbot and Gareth Hale as Alfred Doolittle, and Honor Blackman and Hannah Gordon as Mrs. Higgins. The tour ended August 12, 2006.In 2003 a production of the musical at the Hollywood Bowl starred John Lithgow as Henry Higgins, Melissa Errico as Eliza Doolittle, Roger Daltrey as Alfred P. Doolittle and Paxton Whitehead as Colonel Pickering.


=== 2018 Broadway revival ===
A Broadway revival produced by Lincoln Center Theater and Nederlander Presentations Inc. began previews on March 15, 2018, at the Vivian Beaumont Theater and officially opened on April 19, 2018. It was directed by Bartlett Sher with choreography by Christopher Gattelli, scenic design by Michael Yeargan, costume design by Catherine Zuber and lighting design by Donald Holder. The cast included Lauren Ambrose as Eliza, Harry Hadden-Paton as Professor Henry Higgins, Diana Rigg as Mrs. Higgins, Norbert Leo Butz as Alfred P. Doolittle, Allan Corduner as Colonel Pickering, Jordan Donica as Freddy, and Linda Mugleston as Mrs. Pearce. Replacements included Rosemary Harris as Mrs. Higgins, Laura Benanti, as Eliza, and Danny Burstein, then Alexander Gemignani, as Alfred P. Doolittle.The revival closed on July 7, 2019, after 39 previews and 509 regular performances. A North American tour of the production, starring Shereen Ahmed and Laird Mackintosh as Eliza and Higgins, opened in December 2019 and is scheduled to run through August 2020.


=== Other major productions ===


==== Berlin, 1961 ====
A German translation of My Fair Lady opened on October 1, 1961, at the Theater des Westens in Berlin, starring Karin Hübner and Paul Hubschmid (and conducted, as was the Broadway opening, by Franz Allers). Coming at the height of Cold War tensions, just weeks after the closing of the East Berlin–West Berlin border and the erection of the Berlin Wall, this was the first staging of a Broadway musical in Berlin since World War II. As such it was seen as a symbol of West Berlin's cultural renaissance and resistance. Lost attendance from East Berlin (now no longer possible) was partly made up by a "musical air bridge" of flights bringing in patrons from West Germany, and the production was embraced by Berliners, running for two years.


==== 2007 New York Philharmonic concert and US tour ====
In 2007 the New York Philharmonic held a full-costume concert presentation of the musical. The concert had a four-day engagement lasting from March 7–10 at Lincoln Center's Avery Fisher Hall. It starred Kelsey Grammer as Higgins, Kelli O'Hara as Eliza, Charles Kimbrough as Pickering, and Brian Dennehy as Alfred Doolittle. Marni Nixon played Mrs. Higgins; Nixon had provided the singing voice of Audrey Hepburn in the film version.A U.S. tour of Mackintosh's 2001 West End production ran from September 12, 2007, to June 22, 2008. The production starred Christopher Cazenove as Higgins, Lisa O'Hare as Eliza, Walter Charles as Pickering, Tim Jerome as Alfred Doolittle and Nixon as Mrs. Higgins, replacing Sally Ann Howes.


==== 2008 Australian tour ====
An Australian tour produced by Opera Australia commenced in May 2008. The production starred Reg Livermore as Higgins, Taryn Fiebig as Eliza, Robert Grubb as Alfred Doolittle and Judi Connelli as Mrs Pearce. John Wood took the role of Alfred Doolittle in Queensland, and Richard E. Grant played the role of Henry Higgins at the Theatre Royal, Sydney.


==== 2010 Paris revival ====
A new production was staged by Robert Carsen at the Théâtre du Châtelet in Paris for a limited 27-performance run, opening December 9, 2010, and closing January 2, 2011. It was presented in English. The costumes were designed by Anthony Powell and the choreography was by Lynne Page. The cast was as follows: Sarah Gabriel / Christine Arand (Eliza Doolittle), Alex Jennings (Henry Higgins), Margaret Tyzack (Mrs. Higgins), Nicholas Le Prevost (Colonel Pickering), Donald Maxwell (Alfred Doolittle), and Jenny Galloway (Mrs. Pearce).


==== 2012 Sheffield production ====
A new production of My Fair Lady opened at Sheffield Crucible on December 13, 2012. Dominic West played Henry Higgins, and Carly Bawden played Eliza Doolittle. Sheffield Theatres' Artistic Director Daniel Evans was the director. The production ran until January 26, 2013.


==== 2016 Australian production ====
The Gordon Frost Organisation, together with Opera Australia, presented a production at the Sydney Opera House from August 30 to November 5, 2016. It was directed by Julie Andrews and featured the set and costume designs of the original 1956 production by Smith and Beaton. The production sold more tickets than any other in the history of the Sydney Opera House. The show's opening run in Sydney was so successful that in November 2016, ticket pre-sales were released for a re-run in Sydney, with the extra shows scheduled between August 24 and September 10, 2017, at the Capitol Theatre. In 2017, the show toured to Brisbane from March 12 and Melbourne from May 11.The cast featured Alex Jennings as Higgins (Charles Edwards for Brisbane and Melbourne seasons), Anna O'Byrne as Eliza, Reg Livermore as Alfred P. Doolittle, Robyn Nevin as Mrs. Higgins (later Pamela Rabe), Mark Vincent as Freddy, Tony Llewellyn-Jones as Colonel Pickering, Deidre Rubenstein as Mrs. Pearce, and David Whitney as Karpathy.


== Critical reception ==
According to Geoffrey Block, "Opening night critics immediately recognized that My Fair Lady fully measured up to the Rodgers and Hammerstein model of an integrated musical...Robert Coleman...wrote 'The Lerner-Loewe songs are not only delightful, they advance the action as well. They are ever so much more than interpolations, or interruptions.'"  The musical opened to "unanimously glowing reviews, one of which said 'Don't bother reading this review now. You'd better sit right down and send for those tickets...' Critics praised the thoughtful use of Shaw's original play, the brilliance of the lyrics, and Loewe's well-integrated score."A sampling of praise from critics, excerpted from a book form of the musical, published in 1956.
"My Fair Lady is wise, witty, and winning. In short, a miraculous musical." Walter Kerr, New York Herald Tribune.
"A felicitous blend of intellect, wit, rhythm and high spirits. A masterpiece of musical comedy ... a terrific show." Robert Coleman, New York Daily Mirror.
"Fine, handsome, melodious, witty and beautifully acted ... an exceptional show." George Jean Nathan, New York Journal American.
"Everything about My Fair Lady is distinctive and distinguished." John Chapman, New York Daily News.
"Wonderfully entertaining and extraordinarily welcomed ... meritorious in every department." Wolcott Gibbs, The New Yorker.
"One of the 'loverliest' shows imaginable ... a work of theatre magic." John Beaufort, The Christian Science Monitor.
"An irresistible hit." Variety.
"One of the best musicals of the century." Brooks Atkinson, The New York Times.The reception from Shavians was more mixed, however. Eric Bentley, for instance, called it "a terrible treatment of Mr. Shaw's play, [undermining] the basic idea [of the play]", even though he acknowledged it as "a delightful show".


== Principal roles and casting history ==


== Awards and nominations ==


=== Original Broadway production ===
Sources: BroadwayWorld TheatreWorldAwards


=== 1976 Broadway revival ===
Sources: BroadwayWorld Drama Desk


=== 1979 London revival ===
Source: Olivier Awards


=== 1981 Broadway revival ===
Source: BroadwayWorld


=== 1993 Broadway revival ===
Source: Drama Desk


=== 2001 London revival ===
Source: Olivier Awards


=== 2018 Broadway revival ===


== Adaptations ==


=== 1964 film ===

The film version was made in 1964, directed by George Cukor and with Harrison again in the part of Higgins. The casting of Audrey Hepburn instead of Julie Andrews as Eliza was controversial, partly because theatregoers regarded Andrews as perfect for the part and partly because Hepburn's singing voice was dubbed (by Marni Nixon). Jack L. Warner, the head of Warner Bros., which produced the film, wanted "a star with a great deal of name recognition", but since Andrews did not have any film experience, he thought it would be more successful to cast a movie star. (Andrews went on to star in Mary Poppins that same year for which she won both the Academy Award and the Golden Globe for Best Actress.) Lerner in particular disliked the film version of the musical, thinking it did not live up to the standards of Moss Hart's original direction. He was also unhappy with the casting of Hepburn as Eliza Doolittle and that the film was shot in its entirety at the Warner Bros. studio rather than, as he would have preferred, in London. Despite the controversy, My Fair Lady was considered a major critical and box office success, and won eight Oscars, including Best Picture of the Year, Best Actor for Rex Harrison, and Best Director for George Cukor.


=== Unrealized 2008 film ===
Columbia Pictures announced a new adaptation in 2008. The intention was to shoot on location in Covent Garden, Drury Lane, Tottenham Court Road, Wimpole Street and the Ascot Racecourse. John Madden was signed to direct the film, and Colin Firth and Carey Mulligan were possible choices for the leading roles. Emma Thompson wrote a new screenplay adaptation for the project, but the studio shelved it.


== Notes ==


== References ==
Citron, David (1995). The Wordsmiths: Oscar Hammerstein 2nd and Alan Jay Lerner, Oxford University Press. ISBN 0-19-508386-5
Garebian, Keith (1998). The Making of My Fair Lady, Mosaic Press. ISBN 0-88962-653-7
Green, Benny, Editor (1987). A Hymn to Him : The Lyrics of Alan Jay Lerner, Hal Leonard Corporation. ISBN 0-87910-109-1
Jablonski, Edward (1996). Alan Jay Lerner: A Biography, Henry Holt & Co. ISBN 0-8050-4076-5
Lees, Gene (2005). The Musical Worlds of Lerner and Loewe, Bison Books.  ISBN 0-8032-8040-8
Lerner, Alan Jay (1985). The Street Where I Live, Da Capo Press. ISBN 0-306-80602-9
McHugh, Dominic. Loverly: The Life and Times of "My Fair Lady"  (Oxford University Press; 2012) 265 pages; uses unpublished documents to study the five-year process of the original production.
Shapiro, Doris (1989). We Danced All Night: My Life Behind the Scenes With Alan Jay Lerner, Barricade Books. ISBN 0-942637-98-4


== External links ==
My Fair Lady at the Internet Broadway Database
Lincoln Center production
Ovrtur Page