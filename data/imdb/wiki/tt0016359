The Sleuth Kit (TSK) is a library and collection of Unix- and Windows-based utilities for extracting data from disk drives and other storage so as to facilitate the forensic analysis of computer systems. It forms the foundation for Autopsy, a better known tool that is essentially a graphical user interface to the command line utilities bundled with The Sleuth Kit. 
The collection is open source and protected by the GPL, the CPL and the IPL. The software is under active development and it is supported by a team of developers. The initial development was done by Brian Carrier who based it on The Coroner's Toolkit. It is the official successor platform.The Sleuth Kit is capable of parsing NTFS, FAT/ExFAT, UFS 1/2, Ext2, Ext3, Ext4, HFS, ISO 9660 and YAFFS2 file systems either separately or within disk images stored in raw (dd), Expert Witness or AFF formats.  The Sleuth Kit can be used to examine most Microsoft Windows, most Apple Macintosh OSX, many Linux and some other UNIX computers.
The Sleuth Kit can be used:

Via the included command line tools; or
As a library embedded within a separate digital forensic tool such as Autopsy or log2timeline/plaso.


== Tools ==
Some of the tools included in The Sleuth Kit include:

ils lists all metadata entries, such as an Inode.
blkls displays data blocks within a file system (formerly called dls).
fls lists allocated and unallocated file names within a file system.
fsstat displays file system statistical information about an image or storage medium.
ffind searches for file names that point to a specified metadata entry.
mactime creates a timeline of all files based upon their MAC times.
disk_stat (currently Linux-only) discovers the existence of a Host Protected Area.


== Applications ==
The Sleuth Kit can be used  

for understanding what data is stored on a disk drive, even if the operating system has removed all meta data.
for recovering deleted image files 
summarizing all deleted files
search for files by name or included keyword 


== See also ==

Autopsy (software) — A graphical user interface to The Sleuth Kit.
CAINE Linux − Includes The Sleuth Kit
List of free and open-source software packages


== References ==


== External links ==
Official website