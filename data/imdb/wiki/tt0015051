The Last Man on Earth is a 1964 black-and-white post-apocalyptic science fiction horror film based on the 1954 novel I Am Legend by Richard Matheson. The film was produced by Robert L. Lippert and directed by Ubaldo Ragona and Sidney Salkow, and stars Vincent Price and France Bettoia. The screenplay was written in part by Matheson, but he was dissatisfied with the result and chose to be credited as "Logan Swanson". William Leicester, Furio M. Monetti, and Ubaldo Ragona finished the script.
The Last Man on Earth was filmed in Rome, with scenes being completed at Esposizione Universale Roma. It was released in the United States and the United Kingdom by American International Pictures. In the 1980s the film entered the public domain. MGM Home Video, the current owners of the AIP film catalog, released a digitally remastered widescreen print of the film on DVD in September 2005.


== Plot ==

It is 1968, and Dr. Robert Morgan (Vincent Price) lives in a world where everyone else has been infected by a plague that has turned them into undead, vampiric creatures that cannot stand sunlight, fear mirrors, and are repelled by garlic. They would kill Morgan if they could, but they are weak and unintelligent. Every day Morgan carries out the same routine:  he wakes up, marks another day on the calendar, gathers his weapons, and then goes hunting for vampires, killing as many as he can and then burning the bodies to prevent them from coming back.  At night, he locks himself inside his house.
A flashback sequence explains that, three years earlier, Morgan's wife Virginia and daughter Kathy had succumbed to the plague before it was widely known by the public that the dead would return to life. Instead of taking his wife to the same public burn pit used to dispose of his daughter's corpse, Morgan buried her without the knowledge of the authorities. When his wife returned to his home and attacked him, Morgan became aware of the need to kill the plague victims with a wooden stake. Morgan hypothesizes that he is immune to the bacteria from a bite by an infected vampire bat when he was stationed in Panama, which may have introduced a diluted form of the plague into his blood.
One day, a dog appears in the neighborhood. Desperate for companionship, Morgan chases after the dog but does not catch it. Sometime later the dog appears, wounded, at Morgan's doorstep. He takes the dog into his home and treats its wounds, looking forward to having company for the first time in three years. He quickly discovers, however, that it, too, has become infected with the plague. Morgan is seen burying the dog, which he has impaled with a wooden stake. Morgan sinks further into depression and loneliness.
After burying the dog, Morgan spots a woman in the distance. The woman, Ruth, is terrified of Morgan at first sight and runs from him. Morgan convinces her to return to his home, but he is suspicious of her true nature. Ruth becomes ill when Morgan waves garlic in her face, who claims that she has a weak stomach.  Morgan's suspicion that Ruth is infected is confirmed when he discovers her attempting to inject herself with a combination of blood and vaccine that holds the disease at bay. Ruth initially draws a gun on Morgan but ultimately surrenders it to him. She tells him that she is part of a group of people like her — infected, but under treatment — and was sent to spy on Morgan. The vaccine allows the people to function normally with the drug in the bloodstream, but once it wears off, the infection takes over the body again. Ruth explains that her people are planning to rebuild society as they destroy the remaining humans, and that many of the vampires Morgan killed were still alive. Ruth desperately urges Morgan to flee, but he inexplicably refuses.
While Ruth is asleep, Morgan transfuses his own blood into her. She is immediately cured, and Morgan sees hope that, together, they can cure the rest of her people. Moments later, however, Ruth's people attack. Morgan takes the gun and flees his home while the attackers kill the vampires gathered around Morgan's home.  Ruth's people spot Morgan and chase him. He exchanges gunfire with them and picks up tear gas grenades from a police station armory along the way. While the tear gas delays his pursuers somewhat, Morgan is wounded by gunfire and retreats into a church. Despite Ruth's protests to let Morgan live, his pursuers finally impale him on the altar with a spear. In his final moments, Morgan denounces his pursuers as "freaks" and, as Ruth cradles him, declares that he is the last true man on Earth. As Ruth walks away from Morgan's body, she notices a baby crying and tries to assure the child that everyone is safe now.


== Cast ==
Vincent Price as Dr. Robert Morgan
Franca Bettoia as Ruth Collins
Carolyn De Fonseca dubbed for Franca Bettoia's voice in the English release of the film. She was uncredited.
Emma Danieli as Virginia Morgan
Giacomo Rossi Stuart as Ben Cortman
Umberto Raho (billed as Umberto Rau) as Dr. Mercer
Christi Courtland as Kathy Morgan
Antonio Corevi (billed as Tony Corevi) as the Governor
Ettore Ribotta (billed as Hector Ribotta) as the TV Reporter
Rolando De Rossi
Giuseppe Mattei, as the leader of the survivors, was also uncredited.


== Production ==


=== Development ===
Producer Anthony Hinds purchased the rights to Matheson's novel for Hammer Film Productions. Matheson wrote a script, and Hammer announced in 1958 that they would make it. However, British censors would not allow the film to be produced, so Hinds resold the script to American producer Robert L. Lippert.
Lippert had wanted to make a "last man on Earth" type film for a while. In the late 1950s Charles Marquis Warren and Robert Stabler optioned a novel by science fiction writer George R. Stewart called Earth Abides. Harry Spalding, who worked for Lippert, said the release of The World, the Flesh and the Devil (1959) killed off plans for that project. Spalding then read Matheson's novel and suggested Lippert film that book instead. The project was announced in August 1962.
Lippert originally told Matheson that Fritz Lang would direct the film, and Matheson thought that would be "wonderful". Eventually, however, Sidney Salkow was chosen to direct. Matheson made the follow-up comment: "Well, there's a bit of a drop."


=== Shooting ===
To save money, the film was shot in Italy with a predominantly Italian cast and crew.Matheson later said the film was the most faithful adaptation of his book, but he also called the result "inept" and used a pen name for his screenplay. (He later said he thought Harrison Ford as star and George Miller as director would have been the ideal creative combination".)


== Differences from the novel ==
There are several differences between the film and the novel I Am Legend from which it is based:

The protagonist of the novel is named Robert Neville, not Robert Morgan.
The protagonist's profession is changed from a plant worker to scientist.
The vampires are almost zombie-like, whereas in the novel, they are fast and capable of running and climbing.
The dog that shows up on Neville's doorstep in the novel is timid and comes and goes as it pleases, in contrast to the dog in the film.
In the novel the relationship with Ruth differs slightly, in that no transfusion takes place; a cure seems implausible, even as Neville hopes he will find one; and Ruth escapes after Neville discovers that she is infected.
Neville is not captured in the novel until many months later; even then he barely fights back.
The novel ends shortly before Neville is to be executed; Ruth returns to give him suicide pills and finds it ironic that he has become as much of a legend to the new society as vampires once were to Neville's world (hence the title).
The novel implies that the vampire plague resulted from a biological disease; the origin of the disease is never explained in The Last Man on Earth (and is altered in the subsequent adaptations).


== Release ==
Although the film was not considered a success upon its release, it later gained a more favorable reputation as a classic of the genre. As of August 2020, The Last Man on Earth holds an 80% rating at the film review aggregator Web site Rotten Tomatoes. Phil Hall of Film Threat called The Last Man on Earth "the best Vincent Price movie ever made".Among the less favorable reviews, Steve Biodrowski of Cinefantastique felt the film was "hampered by an obviously low budget and some poorly recorded, post-production dubbing that creates an amateurish feel, undermining the power of its story", while Jonathan Rosenbaum of the Chicago Reader remarked, "Some would consider this version better than the 1971 remake with Charlton Heston, The Omega Man, but that isn't much of an achievement".Among the film's creators, Price "had a certain fondness for the film" and felt it was better than The Omega Man. Richard Matheson co-wrote the film's screenplay, but was unhappy with the results. To keep receiving residual income from the film, though, he had to be credited, so he used the name "Logan Swanson", a combination of his wife's mother's maiden name and his mother's maiden name. Matheson said: "I was disappointed in the film, even though they more or less followed my story. I think Vincent Price, whom I love in every one of his pictures that I wrote, was miscast. I also felt the direction was kind of poor. I just didn’t care for it".


== See also ==
List of American films of 1964
Vampire films
I Am Legend (2007 film) Will Smith
The Omega Man (1971) Charlton Heston


== References ==


== External links ==
The Last Man on Earth on IMDb
The Last Man on Earth is available for free download at the Internet Archive
The Last Man on Earth at AllMovie
Audio transcription of the film (Part 1) (Part 2) at the Internet Archive.
Audio commentary of the film (Voices in the Dark Commentaries) at the Internet Archive.
Essay on film at Den of Geek
Article on film at Bright Lights Film Journal