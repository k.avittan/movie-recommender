"Woman in Chains" is a song by the English band Tears for Fears, released as the second single from their 1989 album The Seeds of Love. It has been described as a "feminist anthem".It was an international success, reaching the Top 40 in the United Kingdom, United States, Australia, Belgium, Ireland, Italy and New Zealand, and the Top 20 in Canada (just missing the Top 10, at number 11), France, the Netherlands and in Poland (where it was particularly successful, reaching number five).The studio cut features Phil Collins on drums. "Tears for Fears just wanted me to do that big drum thing from 'In the Air Tonight'…" Collins recalled. "'We want you to come in here in a big way.'"The song prominently features vocals by Oleta Adams, who went on to achieve a successful solo career. It was re-released in 1992 – with a different B-side and now credited to "Tears For Fears featuring Oleta Adams" – to capitalise on the singer's solo success and to promote the Tears for Fears compilation Tears Roll Down (Greatest Hits 82–92). This time, it reached number 57 in the UK.


== Content ==
"Woman in Chains" was recorded as a duet. Orzabal explained the impetus for the lyric to Melody Maker: "I was reading some feminist literature at the time and I discovered that there are societies in the world still in existence today that are non-patriarchal. They don't have the man at the top and the women at the bottom. They're matricentric—they have the woman at the centre and these societies are a lot less violent, a lot less greedy and there's generally less animosity... but the song is also about how men traditionally play down the feminine side of their characters and how both men and women suffer for it.... I think men in a patriarchal society are sold down the river a bit—okay, maybe we're told that we're in control but there are also a hell of a lot of things that we miss out on, which women are allowed to be".
The song was sampled in the S.A.S. single "So Free" featuring Cam'ron and on Uneasy Listening Vol. 1 by DJ Z-Trip and DJ P. It also appears on the soundtrack of the 1993 film Boxing Helena.


== Music video ==
The video, directed by Andy Morahan, was filmed in black and white. It focuses on the abusive relationship between a man (a boxer) and a woman (a pole-dancer, played by Angela Alvarado); interspersed with shots of the band and guest vocalist Oleta Adams performing the song. It also features Chris Hughes playing the drums.


== Personnel ==
Roland Orzabal – vocals, guitar, keyboards
Neil Taylor – guitar arpeggio
Curt Smith – bass guitar
Oleta Adams – vocals, keyboards
Manu Katché (start to 3:32) and Phil Collins (from 3:32) – drums
Luís Jardim – percussion
Tessa Niles – backing Vocals
Carol Kenyon – backing Vocals


== Charts ==


== References ==


== External links ==
Lyrics of this song at MetroLyrics