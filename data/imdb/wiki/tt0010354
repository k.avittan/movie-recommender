A Pyrrhic victory ( (listen) PIRR-ik) is a victory that inflicts such a devastating toll on the victor that it is tantamount to defeat. Winning a Pyrrhic victory takes a heavy toll that negates any true sense of achievement or damages long-term progress. 
The phrase originates from a quote from Pyrrhus of Epirus, whose triumph against the Romans in the Battle of Asculum in 279 BC destroyed much of his forces and—while a tactical victory—forced the end of his campaign.


== Etymology ==
Pyrrhic victory is named after King Pyrrhus of Epirus, whose army suffered irreplaceable casualties in defeating the Romans at the Battle of Heraclea in 280 BC and the Battle of Asculum in 279 BC, during the Pyrrhic War. After the latter battle, Plutarch relates in a report by Dionysius:

The armies separated; and, it is said, Pyrrhus replied to one that gave him joy of his victory that one other such victory would utterly undo him. For he had lost a great part of the forces he brought with him, and almost all his particular friends and principal commanders; there were no others there to make recruits, and he found the confederates in Italy backward. On the other hand, as from a fountain continually flowing out of the city, the Roman camp was quickly and plentifully filled up with fresh men, not at all abating in courage for the loss they sustained, but even from their very anger gaining new force and resolution to go on with the war.
In both Epirote victories, the Romans suffered greater casualties but they had a much larger pool of replacements, so the casualties had less impact on the Roman war effort than the losses of King Pyrrhus.
The report is often quoted as

Ne ego si iterum eodem modo uicero, sine ullo milite Epirum reuertar.
Another such victory and I come back to Epirus alone.
or

If we are victorious in one more battle with the Romans, we shall be utterly ruined.
The term entered the English vernacular due to popular misconceptions of the magnitude of Pyrrhus's losses: beginning before the 1800s, Latin history teaching books said that Pyrrhus suffered losses in the tens of thousands.


== Examples ==


=== Battles ===
This list comprises examples of battles that ended in a Pyrrhic victory. It is not intended to be complete but to illustrate the concept.

Battle of Asculum (279 BC), Pyrrhus of Epirus and Italian allies against the Romans: the Romans, though suffering twice as many casualties, could easily replenish their ranks. Pyrrhus lost most of his commanders and a great part of the forces he had brought to Italy, and he withdrew to Sicily.
Battle of Avarayr (451), Vardan Mamikonian and Christian Armenian rebels against the Sassanid Empire: the Persians were victorious but the battle proved to be a strategic victory for Armenians, as Avarayr paved the way to the Nvarsak Treaty (484 AD), which assured Armenian autonomy and religious freedom.
Siege of Szigetvár (1566), Ottoman–Habsburg wars: although the Ottomans won the siege, it can be seen as a Pyrrhic victory because of the heavy Ottoman casualties, the death of Sultan Suleiman, and the resulting delay to the Ottoman push for Vienna that year which suspended Ottoman expansion in Europe.
Siege of Ostend (1601–04), Eighty Years' War: for three years the Spanish attempted to capture this port from Dutch and English defenders, even as the Dutch expanded their territory further east – including capturing the port of Sluis to replace Ostend before surrendering. The vast cost and casualties of the siege were compounded by Spain's subsequent campaign to recapture the Dutch gains, which achieved little, and by 1607 Spain was bankrupt. The resultant Twelve Years' Truce effectively made the Dutch Republic an independent state.
Battle of Malplaquet (1709), War of the Spanish Succession: the battle was an Allied victory because Marlborough's army kept possession of the battlefield, but it had suffered double the French casualties and could not pursue. The French army withdrew in good order and relatively intact, and it remained a potent threat to further Allied operations.
Battle of Bunker Hill (1775), American Revolutionary War: after mounting three assaults on the colonial forces, the British won control of the Boston peninsula in the early stages of the war, but the engagement cost them many more casualties than the Americans had incurred (including a large number of officers) and led them to adopt more cautious methods, which helped American rebel forces; the political repercussions increased colonial support for independence.
Battle of Guilford Court House (1781), American Revolutionary War: in this short battle, the British force defeated a superior American army; the British lost a considerable number of men and their drive to conquer the southern colonies changed course.
Battle of Chancellorsville (1863), American Civil War: General Robert E. Lee split his army in the face of Hooker's larger Union force; the audacious strategy allowed the Confederate army to win the day against a numerically superior foe. However, 20% of Lee's army was injured or killed, including General Stonewall Jackson, and his losses were difficult to replace. Lee's weakened army went on the offensive, but less than two months later was defeated and forced to retreat after the Battle of Gettysburg.
Battle of the Santa Cruz Islands (1942), World War II, Solomon Islands Campaign: Japanese and Allied naval forces met during the struggle for Guadalcanal and nearby islands. After an exchange of carrier air attacks, U.S. surface ships retreated with one carrier sunk and another severely damaged. The Japanese carrier forces achieved a tactical victory, as none of their ships were sunk, but the heavy loss of irreplaceable veteran aircrews was to the strategic advantage of the Allies.
Battle of Chosin Reservoir (1950), Korean War: the Chinese army attempted to encircle and destroy the UN forces but in a 17-day battle in freezing weather, the UN forces inflicted crippling losses on the Chinese while making a fighting withdrawal. The Chinese occupied northeast Korea but they did not recover until the spring, and the UN maintained a foothold in Korea.
Battle of Vukovar (1991), Croatian War of Independence: the Yugoslav People's Army (JNA) laid siege to the city of Vukovar, held by the Croatian National Guard and civilian volunteers. After 87 days, the ruined city fell to the JNA. The siege exhausted the JNA and was a watershed moment in the Croatian War.


=== Other uses ===
The term is used as an analogy in business, politics and sport to describe struggles that end up ruining the victor. Theologian Reinhold Niebuhr commented on the necessity of coercion in preserving the course of justice by warning,

Moral reason must learn how to make coercion its ally without running the risk of a Pyrrhic victory in which the ally exploits and negates the triumph.
In Beauharnais v. Illinois, a 1952 U.S. Supreme Court decision involving a charge proscribing group libel, Associate Justice Black alluded to Pyrrhus in his dissent,

If minority groups hail this holding as their victory, they might consider the possible relevancy of this ancient remark: "Another such victory and I am undone".


== Related terms ==


=== Win the battle but lose the war ===
A related expression is "winning the battle but losing the war". This describes a poor strategy that wins a lesser objective, but overlooks and loses the larger objective. In less militaristic terms, this phrase is applied to situations where a small victory may be achieved but the "overarching goal" is lost. Examples include:

An employee pushes his idea to the point that his boss finally acknowledges its merits but fires the employee for arguing.
Attack on Pearl Harbor, 1941: the Imperial Japanese Navy Air Service bombed American military bases in Hawaii, in an effort to slow or prevent US interference in Southeast Asia and the Pacific theater. The "battle" was a victory for the Japanese: 16 ships sunk or damaged (including 8 battleships), 188 aircraft destroyed, 2,403 Americans killed, and another 1,178 wounded, at the light cost to Japan of 5 midget submarines, 29 aircraft, 64 servicemen killed, and 1 captured. In hindsight, however, the strategy failed. At the time, the United States was a declared neutral country and half of its population opposed a war with Japan; instead, in reaction to Pearl Harbor, the US declared war the next day and the Allies forced Japan to unconditional surrender and occupation within four years.
Banks handle an economic downturn by implementing policies that appear to solve that problem but in reality cause a deeper financial crisis in the future.
Homestead strike, 1892: a pivotal event in US labor history, when a union of iron and steel workers clashed with the owner and the manager of a major Pittsburgh-area steel mill over a new collective bargaining agreement. The union called a strike; management responded with a lockout; and events escalated to a violent clash involving strikers, their families, workers from nearby mills, and Pinkerton agents hired to break the picket line. The Pinkertons surrendered and left town, so the strikers could claim victory in the "battle". However, the mill reopened using newly hired non-union workers under the protection of state militia, and the strikers were forced to accept the owner's working terms. The union collapsed because of its failure to secure a favorable contract and because of public horror at the violence. In the long-term "war", the labor movement lost: over the next decade, every steel mill in the Midwest became de-unionized and essentially remained that way until the National Industrial Recovery Act of 1933.
Friends or couples may concentrate on winning disagreements or on being more important than the other, to the detriment of their relationship.


=== Hollow victory ===
A "hollow victory" or "empty victory" is one in which the victor gains little or nothing. Examples include:

In a murder trial, where a guilty verdict brings justice for the victim, but the family is still bereft.
A court-martial clears an officer of blame in a military accident, but the death and damage cannot be undone.
A civil case is decided in favor of the plaintiff, but the awarded amount of money or property is less than was spent to bring the lawsuit.
Victory in a battle or war which, by winning, caused additional problems in the future.
A campaign that did not achieve its goals despite the claims of victory.


== See also ==


== References ==