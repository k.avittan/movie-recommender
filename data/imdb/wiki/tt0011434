I was a Rich Man's Plaything is a 1947 artwork by Eduardo Paolozzi.  The collage was made from cuttings from American magazines and advertisements, mounted on card.  The work of juxtaposed found objects is considered a seminal piece of pop art: it was the first to include the word "pop" in its design, years before Lawrence Alloway coined the term "pop art".


== Summary ==
The collage measures 35.9 cm × 23.8 cm (14.1 in × 9.4 in).  It takes its title from words printed on the cover of Intimate Confessions magazine, the largest clipping incorporated into the design.  Over that, Paolozzi pasted images of a gun emitting the word "pop" cut from the packaging of a toy pop gun, a cherry with a slice of cherry pie, and a "Real Gold" logo from a brand of California lemon juice.  Also mounted on the card is an image of a Lockheed Hudson or Lockheed Ventura bomber, and part of a Coca-Cola advertisement.  Paolozzi took clippings from some magazines that he was given by American ex-servicemen, and from other that he acquired from shops in London .


== Inspirations ==
The original collage was one of ten from Paolozzi's "BUNK!" series, compiled from 1941 to 1952, that were exhibited in the Paolozzi retrospective held at the Tate Gallery in 1971, and which the artist presented to the gallery. They were also included in the 45 "BUNK!" collages reproduced as screenprints in a collection published in 1972.  Paolozzi's "BUNK!" series was inspired by technology, popular culture, glamour and consumerism, and took its name from a quotation of Henry Ford, that "History is more or less bunk … We want to live in the present …".  The word "BUNK!" is printed on one of the ten pieces held by the Tate Gallery, taken from "Evadne in Green Dimension" (1952).  Paolozzi gave an illustrated lecture entitled "Bunk" at the Institute of Contemporary Arts in 1952, the first event presented by the newly formed Independent Group, mainly comprising found images from magazines and other sources, projected using an epidiascope.


== References ==