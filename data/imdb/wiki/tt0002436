The "Battle Hymn of the Republic", also known as "Mine Eyes Have Seen the Glory" outside of the United States, is a popular American patriotic song by the abolitionist writer Julia Ward Howe.
Howe wrote her lyrics to the music of the song "John Brown's Body" in November 1861 and first published them in The Atlantic Monthly in February 1862. The song links the judgment of the wicked at the end of the age (through allusions to biblical passages such as Isaiah 63 and Revelation 19) with the American Civil War. 


== History ==


=== Oh! Brothers ===
The "Glory, Hallelujah" tune was a folk hymn developed in the oral hymn tradition of camp meetings in the southern United States and first documented in the early 1800s. In the first known version, "Canaan's Happy Shore," the text includes the verse "Oh! Brothers will you meet me (3×)/On Canaan's happy shore?" and chorus "There we'll shout and give him glory (3×)/For glory is his own." This developed into the familiar "Glory, glory, hallelujah" chorus by the 1850s. The tune and variants of these words spread across both the southern and northern United States.


=== As the "John Brown's Body" song ===
At a flag-raising ceremony at Fort Warren, near Boston, Massachusetts, on Sunday, May 12, 1861, the John Brown song, using the well known "Oh! Brothers" tune and the "Glory, Hallelujah" chorus, was publicly played "perhaps for the first time." The American Civil War had begun the previous month.
In 1890, George Kimball wrote his account of how the 2nd Infantry Battalion of the Massachusetts militia, known as the "Tiger" Battalion, collectively worked out the lyrics to "John Brown's Body." Kimball wrote:

We had a jovial Scotchman in the battalion, named John Brown. ... [A]nd as he happened to bear the identical name of the old hero of Harper's Ferry, he became at once the butt of his comrades. If he made his appearance a few minutes late among the working squad, or was a little tardy in falling into the company line, he was sure to be greeted with such expressions as "Come, old fellow, you ought to be at it if you are going to help us free the slaves," or, "This can't be John Brown—why, John Brown is dead." And then some wag would add, in a solemn, drawling tone, as if it were his purpose to give particular emphasis to the fact that John Brown was really, actually dead: "Yes, yes, poor old John Brown is dead; his body lies mouldering in the grave."
According to Kimball, these sayings became by-words among the soldiers and, in a communal effort — similar in many ways to the spontaneous composition of camp meeting songs described above — were gradually put to the tune of "Say, Brothers":

Finally ditties composed of the most nonsensical, doggerel rhymes, setting for the fact that John Brown was dead and that his body was undergoing the process of decomposition, began to be sung to the music of the hymn above given. These ditties underwent various ramifications, until eventually the lines were reached,—

And,—

These lines seemed to give general satisfaction, the idea that Brown's soul was "marching on" receiving recognition at once as having a germ of inspiration in it. They were sung over and over again with a great deal of gusto, the "Glory, hallelujah" chorus being always added.
Some leaders of the battalion, feeling the words were coarse and irreverent, tried to urge the adoption of more fitting lyrics, but to no avail. The lyrics were soon prepared for publication by members of the battalion, together with publisher C. S. Hall. They selected and polished verses they felt appropriate, and may even have enlisted the services of a local poet to help polish and create verses.The official histories of the old First Artillery and of the 55th Artillery (1918) also record the Tiger Battalion's role in creating the John Brown Song, confirming the general thrust of Kimball's version with a few additional details.


=== Creation of the "Battle Hymn" ===

Kimball's battalion was dispatched to Murray, Kentucky, early in the Civil War, and Julia Ward Howe heard this song during a public review of the troops outside Washington, D.C., on Upton Hill, Virginia. Rufus R. Dawes, then in command of Company "K" of the 6th Wisconsin Volunteer Infantry, stated in his memoirs that the man who started the singing was Sergeant John Ticknor of his company. Howe's companion at the review, The Reverend James Freeman Clarke, suggested to Howe that she write new words for the fighting men's song. Staying at the Willard Hotel in Washington on the night of November 18, 1861, Howe wrote the verses to the "Battle Hymn of the Republic." Of the writing of the lyrics, Howe remembered:

I went to bed that night as usual, and slept, according to my wont, quite soundly. I awoke in the gray of the morning twilight; and as I lay waiting for the dawn, the long lines of the desired poem began to twine themselves in my mind. Having thought out all the stanzas, I said to myself, "I must get up and write these verses down, lest I fall asleep again and forget them." So, with a sudden effort, I sprang out of bed, and found in the dimness an old stump of a pencil which I remembered to have used the day before. I scrawled the verses almost without looking at the paper.
Howe's "Battle Hymn of the Republic" was first published on the front page of The Atlantic Monthly of February 1862. The sixth verse written by Howe, which is less commonly sung, was not published at that time. The song was also published as a broadside in 1863 by the Supervisory Committee for Recruiting Colored Regiments in Philadelphia.
Both "John Brown" and "Battle Hymn of the Republic" were published in Father Kemp's Old Folks Concert Tunes in 1874 and reprinted in 1889. Both songs had the same Chorus with an additional "Glory" in the second line: "Glory! Glory! Glory! Hallelujah!"Julia Ward Howe was married to Samuel Gridley Howe, the famed scholar in education of the blind. Samuel and Julia were also active leaders in anti-slavery politics and strong supporters of the Union. Samuel Howe was a member of the Secret Six, the group who funded John Brown's work.


== Score ==
"Canaan's Happy Shore" has a verse and chorus of equal metrical length and both verse and chorus share an identical melody and rhythm. "John Brown's Body" has more syllables in its verse and uses a more rhythmically active variation of the "Canaan" melody to accommodate the additional words in the verse. In Howe's lyrics, the words of the verse are packed into a yet longer line, with even more syllables than "John Brown's Body." The verse still uses the same underlying melody as the refrain, but the addition of many dotted rhythms to the underlying melody allows for the more complex verse to fit the same melody as the comparatively short refrain.

One version of the melody, in C major, begins as below. This is an example of the mediant-octave modal frame.


== Lyrics ==
Howe submitted the lyrics she wrote to The Atlantic Monthly, and it was first published in the February 1862 issue of the magazine.


=== First published version ===

* Many modern recordings of the Battle Hymn of the Republic use the lyric "As He died to make men holy, let us live to make men free" as opposed to the lyric originally written by Julia Ward Howe: "let us die to make men free."


=== Other versions ===
Howe's original manuscript differed slightly from the published version. Most significantly, it included a final verse:

In the 1862 sheet music, the chorus always begins:


== Recordings and public performances ==
In 1953, Marian Anderson sang the song before a live television audience of 60 million persons, broadcast live over the NBC and CBS networks, as part of The Ford 50th Anniversary Show.
In 1960 the Mormon Tabernacle Choir won the Grammy Award for Best Performance by a Vocal Group or Chorus. The 45 rpm single record, which was arranged and edited by Columbia Records and Cleveland disk jockey Bill Randle, was a commercial success and reached #13 on Billboard's Hot 100 the previous autumn. It is the choir's only Top 40 hit in the Hot 100.
It's included along with her performance of "We Shall Overcome" on Joan Baez in Concert, Part 2, live material recorded during Joan Baez' concert tours of early 1963.
Judy Garland performed this song on her weekly television show in December 1963. She originally wanted to do a dedication show for President John F. Kennedy upon his assassination, but CBS would not let her, so she performed the song without being able to mention his name.
At Winston Churchill's funeral January 30th, 1965. Churchill's favourite hymns were sung, including the "Battle Hymn of the Republic".
Andy Williams experienced commercial success in 1968 with an a cappella version recorded at Senator Robert Kennedy's funeral. Backed by the St. Charles Borromeo choir, his version reached #11 on the adult contemporary chart and #33 on the Billboard Hot 100.
In the movie Kelly's Heroes, Oddball is playing it (in the rain) as his tanks meet up with Kelly and the rest of the troops.
Anita Bryant performed it January 17, 1971, at the halftime show of Super Bowl V.
Mormon Tabernacle Choir performed this song at the inaugural parade of President Ronald Reagan on January 20, 1981.
The song is one of the three American songs included in "An American Trilogy", a 1971 song medley written and performed by country composer Mickey Newbury. Newbury's song was popularized by Elvis Presley, who included it as a showstopper in his concerts. Presley recorded and issued "An American Trilogy" several times.
Track One on the album The Real Ale and Thunder Band "At Vespers", recorded at St. Laurence's Parish Church, Downton by BBC Radio Solent, 18 November 1984.
It was performed in St. Paul's Cathedral on September 14, 2001, as part of a memorial service for those lost in the September 11, 2001 attacks.
The Brooklyn Tabernacle Choir also sang this song at President Barack Obama's Second Presidential Inauguration Ceremony on January 21, 2013.
The Mother Bethel AME Church Choir from Philadelphia performed this song during the opening day of the Democratic National Convention on July 25, 2016.
A U.S. military choir and band performed this song at the pre-inauguration ceremony of President-Elect Donald Trump on January 19, 2017, at the Lincoln Memorial.
The Naval Academy Glee Club performed this song on September 1, 2018 at the funeral of Sen. John McCain at the Washington National Cathedral.


== Influence ==


=== Popularity and widespread use ===
In the years since the Civil War, "The Battle Hymn of the Republic" has been used frequently as an American patriotic song.


=== In association football ===

The refrain "Glory, glory, hallelujah!" has been adopted by fans of a number of sporting teams, most notably in the English and Scottish Premier Leagues. The popular use of the tune by Tottenham Hotspur can be traced to September 1961 during the 1961–62 European Cup. Their first opponents in the competition were the Polish side Górnik Zabrze, and the Polish press described the Spurs team as "no angels" due to their rough tackling. In the return leg at White Hart Lane, some fans then wore angel costumes at the match holding placards with slogans such as "Glory be to shining White Hart Lane", and the crowded started singing the refrain "Glory, glory, hallelujah" as Spurs beat the Poles 8–1, starting the tradition at Tottenham. It was released as the B-side to "Ozzie's Dream" for the 1981 Cup Final.
The theme was then picked up by Hibernian, with Hector Nicol's release of the track "Glory, glory to the Hibees" in 1963. "Glory, Glory Leeds United" was a popular chant during Leeds' 1970 FA Cup run. Manchester United fans picked it up as "Glory, Glory Man United" during the 1983 FA Cup Final. As a result of its popularity with these and other British teams, it has spread internationally and to other sporting codes. An example of its reach is its popularity with fans of the Australian Rugby League team, the South Sydney Rabbitohs (Glory, Glory to South Sydney) and to A-League team Perth Glory.


=== Cultural influences ===
Words from the first verse gave John Steinbeck's wife Carol Steinbeck the title of his 1939 masterpiece The Grapes of Wrath. The title of John Updike's In the Beauty of the Lilies also came from this song, as did Terrible Swift Sword and Never Call Retreat, two volumes in Bruce Catton's Centennial History of the Civil War. Terrible Swift Sword is also the name of a board wargame simulating the Battle of Gettysburg.The lyrics of "The Battle Hymn of the Republic" appear in Dr. Martin Luther King, Jr.'s sermons and speeches, most notably in his speech "How Long, Not Long" from the steps of the Alabama State Capitol building on March 25, 1965, after the 3rd Selma March, and in his final sermon "I've Been to the Mountaintop", delivered in Memphis, Tennessee on the evening of April 3, 1968, the night before his assassination. In fact, the latter sermon, King's last public words, ends with the first lyrics of the "Battle Hymn": "Mine eyes have seen the glory of the coming of the Lord."
Bishop Michael B. Curry of North Carolina, after his election as the first African American Presiding Bishop of The Episcopal Church, delivered a sermon to the Church's General Convention on July 3, 2015, in which the lyrics of The Battle Hymn framed the message of God's love. After proclaiming "Glory, glory, hallelujah, His truth is marching on", a letter from President Barack Obama was read, congratulating Bishop Curry on his historic election. Curry is known for quoting The Battle Hymn during his sermons.
The tune has played a role in many movies where patriotic music has been required, including the 1970 World War II war comedy Kelly's Heroes, and the 1999 sci-fi western Wild Wild West. The inscription, "Mine eyes have seen the glory of the coming of the Lord", is written at the feet of the sculpture of the fallen soldier at the American Cemetery in Normandy, France.


=== Other songs set to this tune ===
Some songs make use of both the melody and elements of the lyrics of "The Battle Hymn of the Republic", either in tribute or as a parody:

"Marching Song of the First Arkansas" is a Civil War-era song that has a similar lyrical structure to "The Battle Hymn of the Republic". It has been described as "a powerful early statement of black pride, militancy, and desire for full equality, revealing the aspirations of black soldiers for Reconstruction as well as anticipating the spirit of the civil rights movement of the 1960s".
The tune has been used with alternative lyrics numerous times. The University of Georgia's rally song, "Glory Glory to Old Georgia", is based on the patriotic tune, and has been sung at American college football games since 1909. Other college teams also use songs set to the same tune. One such is "Glory, Glory to Old Auburn" at Auburn University. Another is "Glory Colorado", traditionally played by the band and sung after touchdowns scored by the Colorado Buffaloes. "Glory Colorado" has been a fight song at the University of Colorado (Boulder) for more than one hundred years. The University of Georgia's rivals, including the Georgia Institute of Technology, sing a parody of "Glory, Glory to Old Georgia" called "To Hell with Georgia" set to the same tune.
In 1901 Mark Twain wrote "The Battle Hymn of the Republic, Updated", with the same tune as the original, as a comment on the Philippine–American War. It was later recorded by the Chad Mitchell Trio.
"The Burning of the School" is a well-known parody of the song.
The United States Army paratrooper song, "Blood on the Risers", first sung in World War II, includes the lyrics "Gory, gory" in the lyrics, based on the original's "Glory, glory".
A number of terrace songs (in association football) are sung to the tune in Britain. Most frequently, fans chant "Glory, Glory..." plus their team's name: the chants have been recorded and released officially as songs by Hibernian, Tottenham, Leeds United and Manchester United. The 1994 World Cup official song "Gloryland" interpreted by Daryl Hall and the Sounds of Blackness has the tune of "The Battle Hymn of the Republic". In Argentina the St. Alban's former Pupils Assn (Old Philomathian Club) used the tune for its "Glory Glory Philomathians" as well. Not heard often nowadays it is still a cherished song for the Old Philomathians.
In Australia, the most famous version of the song is used by the South Sydney Rabbitohs, an Australian rugby league club – "Glory Glory to South Sydney". The song mentions all the teams in the competition when the song was written, and says what Souths did to them when they played. Each verse ends with, "They wear the Red and Green".
The parody song "Jesus Can't Play Rugby", popular at informal sporting events, uses the traditional melody under improvised lyrics. Performances typically feature a call-and-response structure, wherein one performer proposes an amusing reason why Jesus Christ might be disqualified from playing rugby—e.g. "Jesus can't play rugby 'cause his dad will rig the game"—which is then repeated back by other participants (mirroring the repetitive structure of "John Brown's Body"), before ending with the tongue-in-cheek proclamation "Jesus saves, Jesus saves, Jesus saves". A chorus may feature the repeated call of "Free beer for all the ruggers", or, after concluding the final verse, "Jesus, we're only kidding".
A protest song titled "Gloria, Gloria Labandera" (lit. "Gloria The Laundrywoman") was used by supporters of former Philippine president Joseph Estrada to mock Gloria Macapagal-Arroyo after the latter assumed the presidency following Estrada's ouster from office, further deriving the "labandera" parallels to alleged money laundering. While Arroyo did not mind the nickname and went on to use it for her projects, the Catholic Church took umbrage to the parody lyrics and called it "obscene".Other songs simply use the melody, i.e. the melody of "John Brown's Body", with no lyrical connection to "The Battle Hymn of the Republic":

"Solidarity Forever", a marching song for organized labor in the 20th century.
The anthem of the American consumers' cooperative movement, "The Battle Hymn of Cooperation", written in 1932.
The tune has been used as a marching song in the Finnish military with the words Kalle-Kustaan muori makaa hiljaa haudassaan, ja yli haudan me marssimme näin ("Carl Gustaf's hag lies silently in her grave, and we're marching over the grave like this").
The Finnish Ice Hockey fans can be heard singing the tune with the lyrics "Suomi tekee kohta maalin, eikä kukaan sille mitään voi" (Finland will soon score, and no one can do anything about it").
The popular folk dance "Gólya" ("Stork"), known in several Hungarian-speaking communities in Transylvania (Romania), as well as in Hungary proper, is set to the same tune. The same dance is found among the Csángós of Moldavia with a different tune, under the name "Hojna"; with the Moldavian melody generally considered original, and the "Battle Hymn" tune a later adaptation.
The melody is used in British nursery rhyme "Little Peter Rabbit".
The melody is used in French Canadian Christmas carol called "Glory, Alleluia", covered by Ginette Reno and others.
The melody is used in the marching song of the Assam Regiment of the Indian Army: "Badluram ka Badan", or "Badluram's Body", its chorus being "Shabash Hallelujah" instead of "Glory Hallelujah". The word "Shabash" in Hindusthani means "congratulations" or "well done".
The Song "Belfast Brigade" using alternate lyrics is sung by the Lucky4 in support of the Irish Republican Army.
The Discordian Handbook Principia Discordia has a version of the song called Battle Hymn of the Eristocracy. It has been recorded for example by Aarni.
The Subiaco Football Club, in the West Australian Football League, uses the song for their team song. Also, the Casey Demons in the Victorian Football League also currently use the song. The words have been adjusted due to the song mainly being written during the period of time they were called the Casey Scorpions and the Springvale Football Club. As well as these two clubs, the West Torrens Football Club used the song until 1990, when their successor club, Woodville-West Torrens, currently use this song in the South Australian National Football League.
The Brisbane Bears, before they merged with the Fitzroy Football Club, used the Battle Hymn of the Republic in experiment mode before eventually scrapping it in favour of the original song.
The melody is used in the well-known Dutch children's song "Lief klein konijntje". The song is about a cute little rabbit that has a fly on his nose.
The melody is used as the theme for the Japanese electronics chain Yodobashi Camera
The melody is used as a nursery rhyme in Japan as ともだち讃歌 (tomodachi sanka)
The melody has been used as a fight song in Queen's University, named Oil Thigh.


=== Other settings of the text ===
Irish composer Ina Boyle set the text for solo soprano, mixed choir and orchestra; she completed her version in 1918.


== Media ==


== See also ==
Belfast Brigade
John Brown's Body
Battle Cry of Freedom
Solidarity Forever
William Weston Patton
"Glory, Glory" (Georgia fight song)
Blood on the Risers
Children's street culture


== References ==


== Further reading ==
Claghorn, Charles Eugene, "Battle Hymn: The Story Behind The Battle Hymn of the Republic". Papers of the Hymn Society of America, XXIX.
Clifford, Deborah Pickman. 'Mine Eyes Have Seen the Glory: A Biography of Julia Ward Howe. Boston: Little, Brown and Co., 1978. ISBN 0316147478
Collins, Ace. Songs Sung, Red, White, and Blue: The Stories Behind America's Best-Loved Patriotic Songs. HarperResource, 2003. ISBN 0060513047
Hall, Florence Howe. The story of the Battle hymn of the republic (Harper, 1916) online
Hall, Roger Lee. Glory, Hallelujah: Civil War Songs and Hymns, Stoughton: PineTree Press, 2012.
Jackson, Popular Songs of Nineteenth-Century America, note on "Battle Hymn of the Republic", pp. 263–64.
Kimball, George (1890), "Origin of the John Brown Song", New England Magazine, new, Cornell University, 1.
McWhirter, Christian. Battle Hymns: The Power and Popularity of Music in the Civil War. Chapel Hill, NC: University of North Carolina Press, 2012. ISBN 1469613670
Scholes, Percy A. "John Brown's Body", The Oxford Companion of Music. Ninth edition. London: Oxford University Press, 1955.
Snyder, Edward D. "The Biblical Background of the 'Battle Hymn of the Republic,'" New England Quarterly (1951) 24#2 pp. 231–238 in JSTOR
Stauffer, John, and Benjamin Soskis, eds. The Battle Hymn of the Republic: A Biography of the Song That Marches On (Oxford University Press; 2013) ISBN 978-0-19-933958-7. 380 pages; Traces the history of the melody and lyrics & shows how the hymn has been used on later occasions
Stutler, Boyd B. Glory, Glory, Hallelujah! The Story of "John Brown's Body" and "Battle Hymn of the Republic." Cincinnati: The C. J. Krehbiel Co., 1960. OCLC 3360355
Vowell, Sarah. "John Brown's Body," in The Rose and the Briar: Death, Love and Liberty in the American Ballad. Ed. by Sean Wilentz and Greil Marcus. New York: W. W. Norton, 2005. ISBN 0393059545


== External links ==


=== Sheet music ===
Free sheet music of The Battle Hymn of the Republic from Cantorion.org
1917 Sheet Music at Duke University as part of the American Memory collection of the Library of Congress
The Battle Hymn of the Republic. Facsimile of first draft


=== Audio ===
"The Battle Hymn of the Republic", Stevenson & Stanley (Edison Amberol 79, 1908)—Cylinder Preservation and Digitization Project.
MIDI for The Battle Hymn of the Republic from Project Gutenberg
The Battle Hymn of the Republic sung at Washington National Cathedral, mourning the September 11, 2001 attacks.
The short film A NATION SINGS (1963) is available for free download at the Internet Archive