The Silent Command is a 1923 American silent drama film directed by J. Gordon Edwards featuring Bela Lugosi as a foreign saboteur in his American film debut. The film, written by Anthony Paul Kelly and Rufus King, also stars Edmund Lowe, Alma Tell, and Martha Mansfield. The film depicts the story of Benedict Hisston (Lugosi), who is part of a plot to destroy the Panama Canal. Initially unable to obtain necessarily intelligence from Richard Decatur (Lowe), a captain in the United States Navy, he enlists the aid of femme fatale Peg Williams (Mansfield). Decatur pretends to be seduced into the conspiracy, costing him his career and estranging him from his wife (Tell), but he ultimately betrays the saboteurs in Panama and stops their plan. He returns home to the Navy and his wife, and to popular acclaim for his heroics.
The film was produced in cooperation with the Navy and was intended as a propaganda film to encourage support for a larger navy. The Silent Command was shown at the opening of several Fox Theatres locations and was sometimes marketed in conjunction with naval recruitment efforts. It received generally positive reviews from contemporary film critics, although modern appraisals consider the film mediocre.
The Silent Command began Lugosi's career in the American film industry. The film's focus on his eyes, at times in extreme close-up, helped to establish his image for later roles. Unlike most Fox Film productions of the silent era, several copies of The Silent Command have survived. It has been released in multiple home video formats, and is now in the public domain and available online.


== Plot ==
Benedict Hisston is a foreign agent, part of a conspiracy to destroy the Panama Canal and the US Navy's Atlantic Fleet. He attempts to acquire information about mine placement in the Canal Zone from Captain Richard Decatur but fails. That information is essential to the conspiracy's success and so he then hires vamp Peg Williams to obtain the intelligence through seduction.
Decatur is not fooled and obeys the "silent command" of the Chief of Naval Intelligence to play along with the spies without revealing his purpose to friends or family. He is court-martialed, stripped of rank, and dismissed from the Navy after he strikes an admiral. His association with Williams estranges him from his wife but earns him the trust of Hisston and the other spies. When the conspirators are ready to enact their plan, he travels to Panama with them. He thwarts their attempt at sabotage, saving the canal and the fleet. He is then reinstated into the Navy, reunited with his wife, and honored by the nation for his heroism.


== Cast ==


== Production ==

Rufus King, later known for his detective novels, wrote the original story for The Silent Command, which was adapted into a screenplay by Anthony Paul Kelly. It was intended as a propaganda film to encourage popular support for expansion of the United States Navy and was made with the Navy's cooperation and support. Fox Film's publicity promoter, Wells Hawks, may have been responsible for this arrangement, as he had previously worked in a publicity role for the Navy. Quotes praising the film were provided by several prominent members of the military for use in advertising, including General John J. Pershing and Theodore Roosevelt, Jr., the Assistant Secretary of the Navy. Years later, in a publicity interview for The Return of Chandu, Bela Lugosi commented on the irony of being a propagandist for naval expansion when his native country, landlocked Hungary, "has no navy nor needs any!"In December 1922, Lugosi had starred in a Greenwich Village play, The Red Poppy. The play performed poorly, but Lugosi, in the role of a thuggish Spaniard, received critical praise. He was cast as Hisston in The Silent Command based on the strength of that performance. Extreme close-ups of Lugosi's eyes were used throughout the film to reinforce perception of this character as evil. Film historian Gary Rhodes traced the origin of this technique, intended to suggest the evil eye or hypnosis, to characters in Weimar cinema inspired by Svengali. Lugosi had played one such character, Professor Mors, in the 1919 German film Sklaven fremdes Willens; Edwards himself had portrayed Svengali in a run of Trilby on the St. Louis stage.The Silent Command was released in at least two editions. As shown at its New York premiere, it was an eight-reel film with a 91-minute run time. However, the version screened earlier in Chicago had been 18 minutes shorter, which reduced the film to seven reels; this cut was used for most subsequent releases. H.T. Hodge claimed to have shown a nine-reel version at the Palace Theatre in Abilene, Texas. Additionally, some prints were released as part-color films with tinted scenes.The Silent Command was also released internationally, including Australia and Cuba in 1924, and was retitled His Country for distribution in France.


== Release and reception ==

In 1923, William Fox was expanding his Fox Theatres chain of movie theaters. The Silent Command was shown at the opening or re-opening of several such locations. On August 25, 1923, the first Fox Theatre on the Pacific opened in Oakland, California. The Silent Command was the first feature-length film screened at the Fox Oakland, during an elaborate opening gala that also included Tom Mix riding Tony, the Wonder Horse into the theater. Attendees included members of the film industry, city officials from Oakland and San Francisco, and faculty and students from the University of California, Berkeley The following week, on August 31, it was also the first film shown at the grand opening of Fox's Monroe Theatre in Chicago, in an invitation-only showing involving studio executives and members of the film industry. To encourage attendance at further showings at the Monroe, the studio partnered with the Navy's local recruitment office to produce one-sheets encouraging men to see the film before enlisting. In Springfield, Massachusetts and St. Louis,  it was also the debut film shown at theaters re-opening after renovations.Despite the earlier showings in Oakland and Chicago, Fox advertised the film's world premiere at New York's Central Theatre on September 2. As part of the Navy's support for the film, the audience included Rear Admiral Charles Peshall Plunkett and other officers. The film played at the Central for four weeks before being replaced with Monna Vanna.The Silent Command was generally praised by contemporary reviewers. Laurence Reid and C. S. Sewell, writing for Motion Picture News and Moving Picture World respectively, both offered acclaim for the film, despite what Reid described as a slow start. Variety was slightly less complimentary, suggesting the film was better suited for "regular neighborhood" theaters than prestigious first-run houses and criticizing the cinematography in Mansfield's vamp scenes. Most newspaper critics also gave positive reviews, although the Evening Journal wrote that the plot "taxes the credulity of even a generous picture fan." Modern reviews have been less enthusiastic. Lugosi biographer Arthur Lennig considered the film "turgid", and AllMovie awarded it ​2 1⁄2 out of 5 stars.


== Legacy ==

The Silent Command was Lugosi's first American film, and influenced the direction of his career in cinema. Lugosi had expressed an interest in playing Latin lover characters in the model of Rudolph Valentino, but his performance as Hisston revealed him to be convincing in more villainous roles. According to Rhodes, critics considered the focus on Lugosi's eyes cliché or even unintentionally humorous, but some filmgoers did find the shots genuinely frightening. This technique, showing Lugosi's eyes in extreme close-up, would be revisited in many of his later films,  such as The Midnight Girl, Dracula, and White Zombie. Images of Lugosi's eyes were eventually even used in advertising.Unlike most of Edwards's films, copies of The Silent Command are preserved in three archives: the George Eastman Museum, the Department of Film at the Museum of Modern Art, and Brussels's Cinematek. Grapevine Video made the film available on VHS in 1999, and released a DVD edition in 2005. On January 1, 2019, the film entered the public domain in the United States and became freely available at the Internet Archive.


== See also ==
Bela Lugosi filmography


== Notes ==


== References ==


== Bibliography ==
Bojarski, Richard (1980). Films of Bela Lugosi. Citadel Press. ISBN 978-0-8065-0716-3.CS1 maint: ref=harv (link)
Bordman, Gerald (1995). American Theatre: A Chronicle of Comedy and Drama, 1914–1930. Oxford University Press. ISBN 978-0-19-509078-9.CS1 maint: ref=harv (link)
Brownlow, Kevin (1976) [1968]. The Parade's Gone By.... University of California Press. ISBN 978-0-520-03068-8.CS1 maint: ref=harv (link)
Kabatchnik, Amnon (2010). Blood on the Stage 1925–1950: Milestone Plays of Crime, Mystery, and Detection. Scarecrow Press. ISBN 978-0-8108-6963-9.CS1 maint: ref=harv (link)
Lennig, Arthur (2003). The Immortal Count: The Life and Films of Bela Lugosi. The University Press of Kentucky. ISBN 978-0-8131-2273-1.CS1 maint: ref=harv (link)
Mank, Gregory William (2009) [1990]. Bela Lugosi and Boris Karloff: The Expanded Story of a Haunting Collaboration (Revised ed.). McFarland & Company. ISBN 978-0-7864-3480-0.CS1 maint: ref=harv (link)
Rhodes, Gary D. (1997). Lugosi: His Life in Films, on Stage, and in the Hearts of Horror Lovers. McFarland & Company. ISBN 978-0-7864-2765-9.CS1 maint: ref=harv (link)
Rhodes, Gary D. (2001). White Zombie: Anatomy of a Horror Film. McFarland & Company. ISBN 978-0-7864-2762-8.CS1 maint: ref=harv (link)
Solomon, Aubrey (2011). The Fox Film Corporation, 1915–1935: A History and Filmography. McFarland & Company. ISBN 978-0-7864-6286-5.CS1 maint: ref=harv (link)
Tillmany, Jack; Dowling, Jennifer (2006). Theatres of Oakland. Arcadia Publishing. ISBN 978-0-7385-4681-0.CS1 maint: ref=harv (link)


== External links ==
The Silent Command at AllMovie
The Silent Command on IMDb
The Silent Command is available for free download at the Internet Archive (79 min)