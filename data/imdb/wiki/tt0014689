The Average Woman is a 1924 silent American melodrama film directed by Christy Cabanne and starring Pauline Garon, David Powell, and Harrison Ford. It was released on March 1, 1924.


== Plot ==
Sally Whipple is the daughter of the local judge, who dotes on her and gives in to almost her every whim.  While at the library, she catches the interest of a newspaper man, Jimmy Munroe, who is writing an article on "the average woman".  Sally fits his idea of what represents that woman, and he begins to follow her around, hoping to collect information for his story. His stalking is noticed, and he is arrested.  Judge Whipple, finding him harmless, yet wanting to teach him a lesson, sentences him to regularly visit Sally. During these visits a romance develops between the two.
Rudolph Van Alten is the owner of a local notorious speakeasy, although he keeps his involvement in the club secret. He also begins courting Sally, although he has an ulterior motive. He has come into the possession of some letters written by the Judge's wife, which he thinks will embarrass the judge if they become public. He and his lover Mrs. La Rose, who is also the hostess at his nightclub, intend to use the letters to blackmail the judge. When Sally finds out of his plans, she offers to marry him if he will agree to turn over the letters.
When La Rose overhears Van Alten seducing the young girl, she becomes jealous and calls the police, who raid the club.  The Judge and Jimmy also arrive at the club, saving Sally from the raid, and in the chaos which ensues La Rose shoots and kills Van Alten, and then makes her escape. Sally and Jimmy marry.


== Cast list ==
Pauline Garon as Sally Whipple
David Powell as Rudolph Van Alten
Harrison Ford as Jimmy Munroe
Burr McIntosh as Judge Whipple
De Sacia Mooers as Mrs. La Rose
William H. Tooker as Colonel Crosby
Russell Griffin as "Tike" La Rose
Coit Albertson as Bill Brennon


== Production ==
C. C. Burr announced in late February 1923 that it had secured the rights to Dorothy De Jagers' short story, "The Average Woman". The story had appeared in the edition of April 8, 1922 of The Saturday Evening Post.  In November 1923 it was announced that film would be one of the films produced during the winter of 1923-24. On December 9, 1923 Burr revealed that the following actors had been cast in the picture: Harrison Ford, David Powell, Pauline Garon, De Sacia Mooers, Burr McIntosh, William Tucker, and Russell Griffin. It was Powell's first appearance in an independent film in several years. The movie began filming in early December 1923, with Christy Cabanne at the helm. In early January 1924, it was announced that Pauline Garon had been selected to play the lead in the film. It was also learned that Coit Albertson was in the cast. The picture was filmed at Burr's Glendale studio, which was located in Glendale, Queens. By February 23 the editing on the film had been completed. While the American Film Institute's film catalogue gives a release date of March 1, 1924, The Film Daily has a release date of February 3.Burr had originally intended to distribute the film as part of a several picture deal through Associated Exhibitors, but in October 1923 he terminated that deal, agreeing to distribute the films through the state rights market. The distribution rights for South America were sold to Sociedad General Cinematografica, while the Liberty Film Company were awarded the distribution rights for Cuba. In May, the distribution rights for France, Switzerland, and Belgium were sold to Societie Cinematographique for France, Belgium and Switzerland.


== Reception ==
The Film Daily gave the film a positive review, although they felt the story line was trite.  They enjoyed Garon's performance, and also gave good marks to Cabanne's direction and the cinematography of Brown and Sullivan.  While they felt the story trite, they called the screenplay by Harris "very good". Motion Picture News gave the film a slightly positive review. They enjoyed Cabanne's direction, and called the production a "simple story, treated with considerable discretion, smoothly and logical told." They highlighted the acting work of Powell and McIntosh, and especially praised Mooers work as the vengeful hostess (Mrs. LaRose).  They felt Ford's work was flat, and thought that Garon's performance worked at times, particularly during the romantic sequences, but left something to be desired in other places.


== Notes ==
After the film's release, the author of the short story, Dorothy De Jagers, sued C. C. Burr over a disagreement regarding her rights to have a say in the final cut of the film. A court awarded her $900, which would have been her pay for work on six reels of film. During the film's production, rumors began to spread that Garon had become engaged to Gene Sarazen, the professional golfer. In March 1924 she issued a complete denial of the rumors.


== References ==


== External links ==
The Average Woman on IMDb
The Average Woman at the TCM Movie Database
The Average Woman at the American Film Institute Catalog