Idiot Prayer: Nick Cave Alone at Alexandra Palace is a concert film  and live album by Australian musician Nick Cave. It was streamed globally to ticket holders online on 23 July 2020. It was filmed by cinematographer Robbie Ryan and features Cave performing solo on piano at Alexandra Palace in London. Although initially intended to be a one-time-only event, Idiot Prayer will be released in extended form in cinemas on 5 November 2020 and as a live album on 20 November 2020.
Idiot Prayer serves as the final film in a trilogy—along with 20,000 Days on Earth (2014) and One More Time with Feeling (2016)—and was described by Cave as "its luminous and heartfelt climax."


== Background ==
Nick Cave and the Bad Seeds released its seventeenth studio album Ghosteen on 4 October 2019. The band announced a 33-date European and UK tour, due to begin on 19 April 2020 in Lisbon, Portugal and conclude on 17 June in Tel Aviv, Israel. An 18-date North American tour, beginning on 16 September in Minneapolis, Minnesota, US and concluding on 17 October in Vancouver, British Columbia, Canada, was also later announced. A month prior to its commencement, it was announced that the European and UK tour was cancelled in response to the COVID-19 pandemic, with all shows rescheduled to later dates in 2021; three months later the North American tour was also cancelled. In lieu of the cancelled shows, Cave recorded a solo performance that June at Alexandra Palace's West Hall in London. The full performance was streamed globally as a concert film, Idiot Prayer: Nick Cave Alone at Alexandra Palace, on 23 July. It was filmed by cinematographer Robbie Ryan. The production involved significant precautions against the spread of COVID-19.

The film evolved from Cave's 2019 solo Conversations tour, in which he performed stripped down versions of his songs on piano with more of a focus on the lyrics. During the tour, Cave began to consider recording the Conversations versions of his songs in a studio. During the pandemic, he decided to record and film the songs and enlisted cinematographer Robbie Ryan, sound engineer Dom Monks and editor Nick Emerson. Idiot Prayer serves as the final film in a trilogy—along with 20,000 Days on Earth (2014) and One More Time with Feeling (2016)—and was described by Cave as "its luminous and heartfelt climax." It features performances of songs from across the Bad Seeds discography, as well as a song from Grinderman and a new song entitled "Euthanasia".
Speaking with NME, Robbie Ryan revealed the performance was recorded in one take with two cameras. Ryan also described his role, saying, "It was an interesting process for me, because there was no director really. Nick was the director of sorts, but he wouldn't call himself that. Normally in my world, you get picked by the director and then they run the show. This was different, so Nick was very curious about how we went about it and was very collaborative as to what I thought."Idiot Prayer was initially marketed as one-time-only event for paying viewers, with no later availability for viewing. However, on 3 September 2020, it was announced that Idiot Prayer will be released as a live album and a concert film. The film is an extended version and features four songs not shown during the livestream. It will be released in cinemas on 5 November 2020. The live album will be released on 20 November 2020. The live recording of "Galleon Ship" was released as a single on 3 September 2020. The live recording of "Euthanasia" was released as a single on 16 October 2020.


== Stream ==
Many viewers of the stream experienced buffering and freezing glitches. Organisers emailed ticket holders to apologise and announced the performance would be put online for those who purchased tickets to view from 24–26 July.


== Critical reception ==
In his review for Rolling Stone, Kory Grow praised Cave's arrangements for bringing "new depth to the songs" and also felt the "stark lighting" complemented each song.


== Album ==
All songs written by Nick Cave, except where noted.


== References ==


== External links ==
Idiot Prayer on IMDb