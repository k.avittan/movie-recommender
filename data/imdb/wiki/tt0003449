Third-degree murder is a category of murder defined in the laws of three states in the United States: Florida, Minnesota, and Pennsylvania. It was also formerly defined in New Mexico (which once had five degrees of murder) and Wisconsin. 
Depending on the state, third-degree murder may include felony murder regardless of the underlying felony, felony murder only where the underlying felony is non-violent, or depraved-heart murder, meaning that intent to kill is not an element of the offense of third-degree murder in any state which defines it. It is punishable by a maximum of 40 years of imprisonment in Florida (in the case of a violent career criminal) and Pennsylvania, and 25 years' imprisonment in Minnesota.


== Background and history ==
The first division of the general crime of murder into graded subcategories was enacted into the law of Pennsylvania in 1794. This enactment is often explained in terms of a desire to narrow the scope of application of capital punishment in that state and in the other states which subsequently graded murder into "first" and "second" degrees. The English common law, which had been received into the laws of the U.S. states, at the time applied capital punishment to a large number of crimes; as a result, states statutorily divided the crime of murder into first and second degrees, and began applying capital punishment only to criminals convicted of first-degree murder. By 1953 three states—namely Florida, Minnesota, and Wisconsin—had further created the subcategory of third-degree murder.


== By jurisdiction ==


=== Overview ===

United States federal law, and the laws of the majority of states and territories, divide murder into separate crimes known as first-degree murder and second-degree murder, while the rest use other names for different categories of murder or do not divide murder into separate categories at all. However, as of 2017 only three states have a crime called third-degree murder.


=== Florida ===

Florida law divides murder into three degrees. First-degree murder comprises premeditated murders and felony murders where the underlying felony belongs to an enumerated list of violent or drug-related felonies; second-degree murder is depraved-heart murder; third-degree murder is felony murder where the underlying felony is not one of the enumerated felonies falling under first-degree felony murder.The exact statutory definition of third-degree murder is "[t]he unlawful killing of a human being, when perpetrated without any design to effect death, by a person engaged in the perpetration of, or in the attempt to perpetrate, any felony other than" nineteen enumerated categories of felonies. It constitutes a second-degree felony. Second-degree felonies are punishable by a maximum of 15 years' imprisonment ordinarily, a maximum of 30 years for a habitual felony offender, or 30 to 40 years for a violent career criminal.The nineteen enumerated categories of felonies falling under first-degree murder rather than third-degree murder are drug trafficking; arson; sexual battery; robbery; burglary; kidnapping; prison escape; aggravated child abuse; aggravated abuse of an elderly person or disabled adult; aircraft piracy; unlawful distribution of cocaine, opium, or other controlled substances when such drug is proven to be the proximate cause of the death of the user; carjacking; home-invasion robbery; aggravated stalking; murder of another human being; unlawful throwing, placing, or discharging of a destructive device or bomb; aggravated fleeing or eluding with serious bodily injury or death; resisting an officer with violence to his or her person; or terrorism or an act in furtherance of terrorism.


=== Minnesota ===
Minnesota law originally defined third-degree murder solely as depraved-heart murder ("without intent to effect the death of any person, caus[ing] the death of another by perpetrating an act eminently dangerous to others and evincing a depraved mind, without regard for human life"). In 1987, an additional drug-related provision ("without intent to cause death, proximately caus[ing] the death of a human being by, directly or indirectly, unlawfully selling, giving away, bartering, delivering, exchanging, distributing, or administering a controlled substance classified in Schedule I or II") was added to the definition of third-degree murder. Up until the early 2000s, prosecutions under that provision were rare, but they began to rise in the 2010s. Some reports linked this increase in prosecutions to the opioid epidemic in the United States.Minnesota law also defines the crime of third-degree murder of an unborn child, with the same elements of depraved mind and lack of intent to kill distinguishing it from first- or second-degree murder of an unborn child. Both third-degree murder and third-degree murder of an unborn child are punishable by a maximum of 25 years' imprisonment.


=== New Mexico ===
New Mexico once divided the crime of murder into five different degrees. A legal scholar writing in 1953 (by which time this level of division had been abolished) described this as the "all-time 'record'" for dividing murder into degrees. The definitions were as follows:

first degree: premeditated killing (punished by death)
second-degree murder was further divided into two kinds
killing while committing a felony (punished by 7 to 14 years' imprisonment)
killing with an extremely reckless state of mind (punished by life imprisonment)
third degree: assisting suicide, killing of an unborn child, and other acts of that nature (punished by 3 to 10 years' imprisonment)
fourth degree: killing in the heat of passion, killing while committing a misdemeanour (punished by 1 to 7 years' imprisonment)
fifth degree: "every other killing" that is not justifiable (punished by a maximum fine of $1,000, up to 10 years' imprisonment, or some combination these)In the 1884 Compiled Laws of New Mexico, third-degree murder included assisting a suicide (§ 696), killing of an unborn child by injury to the mother (§ 697), administration of abortifacient causing death of an unborn child or its mother (§ 698), unintentional killing of a human being in the heat of passion in a cruel or unusual manner (§ 699), and unintentional death caused by an intoxicated physician (§ 701).


=== Pennsylvania ===
Pennsylvania law defines third-degree murder as a murder which is neither a first-degree murder ("criminal homicide ... committed by an intentional killing") nor a second-degree murder ("committed while defendant was engaged as a principal or an accomplice in the perpetration of a felony"). For purposes of that section, "felony" is specifically defined as "engaging in or being an accomplice in the commission of, or an attempt to commit, or flight after committing, or attempting to commit robbery, rape, or deviate sexual intercourse by force or threat of force, arson, burglary or kidnapping." There are also parallel crimes of first-degree, second-degree, and third-degree murder of an unborn child. There does not exist the crime of third-degree murder of a law-enforcement officer, only first-degree and second-degree. Third-degree murder and third-degree murder of an unborn child are punishable by a maximum of 40 years' imprisonment.Third-degree murder was introduced to Pennsylvania law in a 1974 amendment, at the same time as second-degree murder was redefined as felony murder; prior to that, second-degree murder had been defined as any murder not a first-degree murder. The common-law definition of murder as homicide "with malice aforethought" remains in force in Pennsylvania. A conviction for third-degree murder does not require intent to kill as in first-degree murder, but it still requires malice. In general, Pennsylvania courts have ruled that the standard of "malice" required for a conviction of third-degree murder is the same as that required for aggravated assault: not just "ordinary negligence" nor "mere recklessness", but "a higher degree of culpability, i.e., that which considers and then disregards the threat necessarily posed to human life by the offending conduct". A defense of diminished capacity may reduce first-degree murder to third-degree murder.The crime known as drug delivery resulting in death had originally been classified as another form of third-degree murder under Pennsylvania law. In Commonwealth v. Ludwig (2005), the Supreme Court of Pennsylvania ruled that this meant that conviction for the crime required the same element of malice as in any other third-degree murder. In response to this ruling, the Pennsylvania General Assembly amended the definition of the crime in 2011 to reclassify it as general criminal homicide rather than specifically as third-degree murder, thus removing the requirement of malice. However, the maximum sentence remained the same 40 years' imprisonment as for third-degree murder.


=== Wisconsin ===

Soon after statehood, Wisconsin enacted statutes repealing the common law crime of murder, creating the statutory crime of murder and dividing the statutory crime of murder into three degrees, with the third encompassing felony murder. For example, the 1849 Revised Statutes defined third-degree murder as a killing "perpetrated without any design to effect the death, by a person engaged in the commission of any felony". The 1956 Criminal Code in § 940.03 defined third-degree murder as causing the death of another "in the course of committing or attempting to commit a felony ... as a natural and probable consequence of the commission of or attempt to commit the felony", and provided that the sentence for the underlying felony could thus be extended by 15 years. This was described by some commentators as a "hybrid" between the common-law felony murder rule and the civil law approach of treating an unintentional death as a "penalty-enhancer" to the punishment for the underlying felony. The 1988 revision of § 940.03 removed the term "third-degree murder" entirely and re-entitled the section as "felony murder".


== See also ==

Criminal negligence
Crime of passion


== References ==


== Further reading ==
"A Comparative Review of States' Recognition of Reduced Degrees of Felony Murder". Washington and Lee Law Review. 40 (4). 1983. Retrieved 2018-03-13.