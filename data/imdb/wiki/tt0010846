In Taoism, the Five Precepts (Chinese: 五戒; pinyin: Wǔ Jiè; Jyutping: Ng5 Gaai3) constitute the basic code of ethics undertaken mainly by lay practitioners. For monks and nuns, there are more advanced and stricter precepts. The Five Precepts are nearly the same as the Five Precepts of Buddhism; however, there are minor differences to fit in with Chinese society.
According to the Zhengtong daozang (1445), the five basic precepts are:

The first precept: No Killing;
The second precept: No Stealing;
The third precept: No Sexual Misconduct;
The fourth precept: No False Speech;
The fifth precept: No Taking of Intoxicants.Their definitions can be found in an excerpt of Zhengtong daozang (1445):

Laozi said: "The precept against killing is: All living beings, including all kinds of animals, and those as small as insects, worms, and so forth, are containers of the uncreated energy, thus one should not kill any of them."
Laozi said: "The precept against stealing is: One should not take anything that he does not own and is not given to him, whether it belongs to someone or not."
Laozi said: "The precept against sexual misconduct is: If a sexual conduct happens, but it is not with your married spouse, it is a Sexual Misconduct. As for a monk or nun, he or she should never marry or practice sexual intercourse with anyone."Laozi said: "The precept against false speech is: If one did not witness what happened himself but telling something to others, or if one lies with knowing it's a lie, this constitutes False Speech."
Laozi said: "The precept against taking of intoxicants is: One should not take any alcoholic drinks, unless he has to take some to cure his illness, to regale the guests with a feast, or to conduct religious ceremonies."
Laozi had said: "These five precepts are the fundamentals for keeping one's body in purity, and are the roots of the upholding of the holy teachings. For those virtuous men and virtuous women who enjoy the virtuous teachings, if they can accept and keep these precepts, and never violate any of them till the end of their lifetimes, they are recognized as those with pure faith, they will gain the Way to Tao, will gain the holy principles, and will forever achieve Tao — the Reality."


== See also ==
Buddhist Five Precepts
Ten Precepts (Taoism)


== Notes ==


== References ==
 太上老君戒經 [Supreme Laozi's Precepts]. Zhengtong daozang 正統道藏 [Zhengtong-era Daoist Canon]. c. 1445.
Chinese Taoism Association (1994).  道教大辭典 [The Great Dictionary of Taoism]. ISBN 7-5080-0112-5./B.054