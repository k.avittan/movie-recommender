A fixer is a person who carries out assignments for someone else or who is good at solving problems for others. The term has different meanings in different contexts. In British usage the term is neutral, meaning "the sort of person who solves problems and gets things done". In journalism, a fixer is a local person who expedites the work of a correspondent working in a foreign country. In American usage, to describe a person as a fixer implies that their methods may be of questionable legality. A fixer who disposes of bodies or "cleans up" physical evidence of crime is often more specifically called a cleaner. In sports, a fixer is someone  who makes (usually illegal) arrangements to fix, i.e., manipulate or pre-arrange the outcome of a sporting contest.


== Facilitator ==
Fixers may primarily use legal means, such as lawsuits and payoffs, to accomplish their ends, or they may carry out unlawful activities. The White House Plumbers have been described as fixers for Richard Nixon; their methods included break-ins and burglary. Fixers who specialize in disposing of evidence or bodies are called "cleaners", like the character of Victor "The Cleaner" in the film La Femme Nikita, or the fictional Jonathan Quinn, subject of the Brett Battles novel The Cleaner.In Britain, a fixer is a commercial consultant for business improvement, whereas in an American context a fixer is often an associate of a powerful person who carries out difficult, undercover, or stealth actions, or extricates a client out of personal or legal trouble. A fixer may freelance, like Judy Smith, a well-known American public relations "crisis consultant" whose career provided inspiration for the popular 2012 television series Scandal. More commonly a fixer works for a single employer, under a title such as "attorney" or "bodyguard", which does not typically describe the kinds of services that they provide. For example, Michael Cohen was officially Donald Trump's personal attorney, but press accounts commonly describe him as Trump's fixer. Cohen later stated that it was his "duty to cover up [Trump]'s dirty deeds".


== Sports match fixer ==

In sport, when a match fixer arranges a preordained outcome of a sporting or athletic contest, the motivation is often gambling, and the fixer is often employed by organized crime. In the Black Sox Scandal, for instance, Major League Baseball players became involved with a gambling syndicate and agreed to lose the 1919 World Series in exchange for payoffs. In another example, in 1975, Boston mobster Anthony "Fat Tony" Ciulla of the Winter Hill Gang was identified as the fixer who routinely bribed jockeys to throw horse races. Other insiders may also be fixers, as in the case of veterinarian Mark Gerard, who, in September 1978, was convicted of fraud for "masterminding a horse-racing scandal that involved switching two thoroughbreds" so that he could cash in on a long-shot bet.


== Journalism aide ==
In journalism, a fixer is someone, often a local journalist, hired by a foreign correspondent or a media company to help arrange a story. Fixers will most often act as a translator and guide, and will help to arrange local interviews that the correspondent would not otherwise have access to. They help to collect information for the story and sometimes play a crucial role in the final outcome. Fixers are rarely credited, and often put themselves in danger, especially in regimes where they might face consequences from an oppressive government for exposing iniquities the state may want to censor.In modern journalism, these aides are often the prime risk mitigators within a journalist's team, making crucial decisions for the reporter. According to journalist Laurie Few, "You don’t have time not to listen (to the fixer)", and anybody who disregards a fixer's advice "is going to step on a landmine, figurative or actual". Throughout the last 20 years, fixers have ranged from civilians to local journalists within the regions of conflict. They are rarely credited and paid menially, which has begun a conversation for the compensation rights of these individuals. According to statistics gathered from the Global Investigative Journalism Network, the base pay for a fixer's time ranged from $50-400 USD per day.A map based on publicly accessible research data shows a visual representation of data collected from various studies conducted on both fixers and their journalist counterparts from over 70 countries. Gathered from the Global Reporting Centre, the survey demographic map had 132 respondents from North America, 101 from Europe, 23 from South America, Africa and Eurasia, 63 from Asia and 9 from Australia.


== In popular culture ==
Numerous films and several songs have been named The Fixer, and, as a genre, illustrate the different meanings of the term. Most commonly, they refer to the kind of person who carries out illicit activities on behalf of someone else. For example, the 2008 British television series The Fixer is about "a renegade group acting outside the law to bring order to the spiraling criminal activity in the country".A BBC Two documentary Alex Polizzi: The Fixer features a fixer in the benign British sense – a consultant who helps to turn around failing businesses.In the ABC drama Scandal, the main character Olivia Pope (portrayed by Kerry Washington) was a fixer and head of Pope and Associates, an organization that fixed political scandals and cleaned up crimes. Kerry Washington's character, Olivia Pope, is partially based on former George H.W. Bush administration press aide Judy Smith, who serves as a co-executive producer.In the AMC TV series Breaking Bad, the character Mike Ehrmantraut played by Jonathan Banks was the cleaner for Gustavo Fring's operations, later reprising the role in the series' prequel spin off, Better Call Saul.The 2016 Romanian drama The Fixer and the 2009 documentary Fixer: The Taking of Ajmal Naqshbandi are each about journalistic fixers.In the Ubisoft videogame Watch Dogs, enemy players are known as fixers, and players can get contracts to eliminate other players, or carry out illegal jobs in game. 


== Notable fixers ==


=== Business ===
Alex Polizzi
Michael Cohen


=== Entertainment ===
Eddie Mannix
Fred Otash
Anthony Pellicano
Howard Strickling


=== Journalism ===
Acquitté Kisembe - Agence France-Presse in the Democratic Republic of Congo (missing since 2003) Almigdad Mojalli - Independent Freelance Fixer/Journalist in Yemen (killed in action, 2016) Bakhtiyar Haddad - Iraqi fixer for French reporter Stephan Villeneuve (Both killed in action in Mosul, 2014) Zabihullah Tamanna - Translator for US National Public Radio in Afghanistan (killed in action, 2016) Ajmal Naqshbandi - Journalist/Fixer in Afghanistan. Killed by Taliban. (Killed in action, 2011) Sayed Agha - Driver/Fixer in Afghanistan. Killed by Taliban. (Killed in action, 2011) 


=== Organized crime ===
Sidney Korshak
Arnold Rothstein
Yoshio Kodama
Hisayuki Machii


=== Politics and business ===
Roy Cohn
Konstantin Kilimnik
Keith Schiller
Roger Stone


=== Public relations ===
Mike Sitrick
Judy Smith


== See also ==
Cleaner role in arts and entertainment
Henchman
Hitman
Contract killing
Contract killing in popular culture


== References ==