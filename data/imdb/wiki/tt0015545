Business hours are the hours during the day in which business is commonly conducted. Typical business hours vary widely by country. By observing common informal standards for business hours, workers may communicate with each other more easily and find a convenient divide between work life and home life.
In the United States, Canada, United Kingdom, and Australia, the hours between 9 am and 5 pm (the traditional "9 to 5") are typically considered to be standard business hours.
In Mexico, the standard business hours are from 7 am to 2 pm and 4 pm to 6 pm. (At least in Mexico City most offices open between 8 and 10 am and close around 6 or 7 pm. Some offices close early on Friday.  Many offices have their employees work Saturdays until lunch time (usually 2 pm).
In Finland, government agencies and other institutions follow the hours 8:00am to 4:15pm. Banks are usually open to 4:30pm. Common business is done from Monday to Friday, but major shops are usually open on Saturdays 9:00am – 6:00pm and on Sundays 12:00pm – 9:00pm, with exceptions.
Other countries have different business hour patterns. Many workers in warmer climates observe siesta during the afternoon, between the hours of 2 pm and 5 pm, effecting a pause in business hours, and resuming business in the evenings. La siesta is a Spanish language term which refers to a short nap of 15–30 minutes.Business hours usually occur on weekdays. However, the days of the week on which business is conducted also varies from region to region in the world.


== Non-traditional business hours ==
Many businesses and organizations have extended or unlimited business hours if their business takes place continuously. For example, hotels, airports, and hospitals are open 24 hours a day, and transactions can take place at any point in time, thus requiring staffing and management availability at all times.
Recent communications technology such as smartphones has also extended the working day.


== Restrictions ==
In some jurisdictions, legislation or regulations may require businesses to limit their trading hours. For example, some Western countries prohibit certain businesses from trading on Sundays, this being considered a day of rest in Christianity. Businesses which serve hot food or alcoholic drinks are also frequently restricted in their trading hours.


== United States ==
In the United States, the U.S. Department of Labor has mandated that companies must pay overtime to individuals who work over 40 hours a week. Businesses often do not schedule their employees to work more than 40 hours per week because they want to avoid paying their employees overtime.


== Greece ==
In Greece, business hours on Mondays, Wednesdays, and Saturdays are usually from 9 am to 3 pm, while on Tuesdays, Thursdays, and Fridays are usually from 9 am to 2 pm and after the siesta from 5.30 pm to 9 pm. Large stores and supermarkets follow an extended business hours schedule and are open Monday to Friday from 9 am to 9 pm and on Saturdays from 9 am to 8 pm. All stores are normally closed on Sundays, but there are several exceptions throughout the year. Banking and civil services hours are usually Monday to Friday from 8 am to 2 pm, with minor variations but are always closed at weekends. All stores and offices are closed on public holidays.


== See also ==
Shopping hours
Trading day
List of stock exchange trading hours
Working time


== References ==