The Pied Piper of Hamelin (German: Rattenfänger von Hameln, also known as the Pan Piper or the Rat-Catcher of Hamelin) is the titular character of a legend from the town of Hamelin (Hameln), Lower Saxony, Germany. The legend dates back to the Middle Ages, the earliest references describing a piper, dressed in multicolored ("pied") clothing, who was a rat-catcher hired by the town to lure rats away with his magic pipe. When the citizens refuse to pay for this service as promised, he retaliates by using his instrument's magical power on their children, leading them away as he had the rats. This version of the story spread as folklore and has appeared in the writings of Johann Wolfgang von Goethe, the Brothers Grimm, and Robert Browning, among others.
There are many contradictory theories about the Pied Piper. Some suggest he was a symbol of hope to the people of Hamelin, which had been attacked by plague; he drove the rats from Hamelin, saving the people from the epidemic.

The earliest known record of this story is from the town of Hamelin itself, depicted in a stained glass window created for the church of Hamelin, which dates to around 1300. Although the church was destroyed in 1660, several written accounts of the tale have survived.


== Plot ==
In 1284, while the town of Hamelin was suffering from a rat infestation, a piper dressed in multicolored ("pied") clothing appeared, claiming to be a rat-catcher. He promised the mayor a solution to their problem with the rats. The mayor, in turn, promised to pay him for the removal of the rats (according to some versions of the story, the promised sum was 1,000 guilders). The piper accepted and played his pipe to lure the rats into the Weser River, where they all drowned.Despite the piper's success, the mayor reneged on his promise and refused to pay him the full sum (reputedly reduced to a sum of 50 guilders) even going so far as to blame the piper for bringing the rats himself in an extortion attempt. Enraged, the piper stormed out of the town, vowing to return later to take revenge. On Saint John and Paul's day, while the adults were in church, the piper returned dressed in green like a hunter and playing his pipe. In so doing, he attracted the town's children. One hundred and thirty children followed him out of town and into a cave and were never seen again. Depending on the version, at most three children remained behind: one was lame and could not follow quickly enough, the second was deaf and therefore could not hear the music, and the last was blind and therefore unable to see where he was going. These three informed the villagers of what had happened when they came out from church.Other versions relate that the Pied Piper led the children to the top of Koppelberg Hill, where he took them to a beautiful land, or a place called Koppenberg Mountain, or Transylvania, or that he made them walk into the Weser as he did with the rats, and they all drowned. Some versions state that the Piper returned the children after payment, or that he returned the children after the villagers paid several times the original amount of gold.The Hamelin street named Bungelosenstrasse ("street without drums") is believed to be the last place that the children were seen.  Ever since, music or dancing is not allowed on this street.


== Background ==

The earliest mention of the story seems to have been on a stained-glass window placed in the Church of Hamelin c. 1300. The window was described in several accounts between the 14th and 17th centuries. It was destroyed in 1660. Based on the surviving descriptions, a modern reconstruction of the window has been created by historian Hans Dobbertin. It features the colorful figure of the Pied Piper and several figures of children dressed in white.This window is generally considered to have been created in memory of a tragic historical event for the town. Also, Hamelin town records apparently start with this event. The earliest written record is from the town chronicles in an entry from 1384 which reportedly states: "It is 100 years since our children left."Although research has been conducted for centuries, no explanation for the historical event is universally accepted as true. In any case, the rats were first added to the story in a version from c. 1559 and are absent from earlier accounts.


=== Natural causes ===

A number of theories suggest that children died of some natural causes such as disease or starvation and that the Piper was a symbolic figure of Death.  Analogous themes which are associated with this theory include the Dance of Death, Totentanz or Danse Macabre, a common medieval trope. Some of the scenarios that have been suggested as fitting this theory include that the children drowned in the river Weser, were killed in a landslide or contracted some disease during an epidemic. Another modern interpretation reads the story as alluding to an event where Hamelin children were lured away by a pagan or heretic sect to forests near Coppenbrügge (the mysterious Koppen "hills" of the poem) for ritual dancing where they all perished during a sudden landslide or collapsing sinkhole.


=== Emigration theory ===
Speculation on the emigration theory is based on the idea that, by the 13th century, overpopulation of the area resulted in the oldest son owning all the land and power (majorat), leaving the rest as serfs. It has also been suggested that one reason the emigration of the children was never documented was that the children were sold to a recruiter from the Baltic region of Eastern Europe, a practice that was not uncommon at the time. In her essay "Pied Piper Revisited", Sheila Harty states that surnames from the region settled are similar to those from Hamelin and that selling off illegitimate children, orphans or other children the town could not support is the more likely explanation. She states further that this may account for the lack of records of the event in the town chronicles. In his book The Pied Piper: A Handbook, Wolfgang Mieder states that historical documents exist showing that people from the area including Hamelin did help settle parts of Transylvania. Transylvania had suffered under lengthy Mongol invasions of Central Europe, led by two grandsons of Genghis Khan and which date from around the time of the earliest appearance of the legend of the piper, the early 13th century.In the version of the legend posted on the official website for the town of Hamelin, another aspect of the emigration theory is presented:

Among the various interpretations, reference to the colonization of East Europe starting from Low Germany is the most plausible one: The "Children of Hameln" would have been in those days citizens willing to emigrate being recruited by landowners to settle in Moravia, East Prussia, Pomerania or in the Teutonic Land. It is assumed that in past times all people of a town were referred to as "children of the town" or "town children" as is frequently done today. The "Legend of the children's Exodus" was later connected to the "Legend of expelling the rats". This most certainly refers to the rat plagues being a great threat in the medieval milling town and the more or less successful professional rat catchers.
The theory is provided credence by the fact that family names common to Hamelin at the time "show up with surprising frequency in the areas of Uckermark and Prignitz, near Berlin."
Historian Ursula Sautter, citing the work of linguist Jürgen Udolph, offers this hypothesis in support of the emigration theory:

"After the defeat of the Danes at the Battle of Bornhöved in 1227," explains Udolph, "the region south of the Baltic Sea, which was then inhabited by Slavs, became available for colonization by the Germans." The bishops and dukes of Pomerania, Brandenburg, Uckermark and Prignitz sent out glib "locators," medieval recruitment officers, offering rich rewards to those who were willing to move to the new lands. Thousands of young adults from Lower Saxony and Westphalia headed east. And as evidence, about a dozen Westphalian place names show up in this area. Indeed there are five villages called Hindenburg running in a straight line from Westphalia to Pomerania, as well as three eastern Spiegelbergs and a trail of etymology from Beverungen south of Hamelin to Beveringen northwest of Berlin to Beweringen in modern Poland.

Udolph favors the hypothesis that the Hamelin youths wound up in what is now Poland. Genealogist Dick Eastman cited Udolph's research on Hamelin surnames that have shown up in Polish phonebooks:

Linguistics professor Jürgen Udolph says that 130 children did vanish on a June day in the year 1284 from the German village of Hamelin (Hameln in German). Udolph entered all the known family names in the village at that time and then started searching for matches elsewhere. He found that the same surnames occur with amazing frequency in the regions of Prignitz and Uckermark, both north of Berlin. He also found the same surnames in the former Pomeranian region, which is now a part of Poland.
Udolph surmises that the children were actually unemployed youths who had been sucked into the German drive to colonize its new settlements in Eastern Europe. The Pied Piper may never have existed as such, but, says the professor, "There were characters known as lokators who roamed northern Germany trying to recruit settlers for the East." Some of them were brightly dressed, and all were silver-tongued.
Professor Udolph can show that the Hamelin exodus should be linked with the Battle of Bornhöved in 1227 which broke the Danish hold on Eastern Europe. That opened the way for German colonization, and by the latter part of the thirteenth century there were systematic attempts to bring able-bodied youths to Brandenburg and Pomerania. The settlement, according to the professor's name search, ended up near Starogard in what is now northwestern Poland. A village near Hamelin, for example, is called Beverungen and has an almost exact counterpart called Beveringen, near Pritzwalk, north of Berlin and another called Beweringen, near Starogard.
Local Polish telephone books list names that are not the typical Slavic names one would expect in that region. Instead, many of the names seem to be derived from German names that were common in the village of Hamelin in the thirteenth century. In fact, the names in today's Polish telephone directories include Hamel, Hamler and Hamelnikow, all apparently derived from the name of the original village.


=== 14th-century Decan Lude chorus book ===
Decan Lude of Hamelin was reported c. 1384, to have in his possession a chorus book containing a Latin verse giving an eyewitness account of the event.


=== 15th-century Lüneburg manuscript ===
The Lüneburg manuscript (c. 1440–50) gives an early German account of the event, rendered in the following form in an inscription on a house known as Rattenfängerhaus (English: "Rat Catcher's House" or Pied Piper's House) in Hamelin:
According to author Fanny Rostek-Lühmann this is the oldest surviving account. Koppen (High German Kuppe, meaning a knoll or domed hill) seems to be a reference to one of several hills surrounding Hamelin. Which of them was intended by the manuscript's author remains uncertain.


=== 16th- and 17th-century sources ===
Somewhere between 1559 and 1565, Count Froben Christoph von Zimmern included a version in his Zimmerische Chronik. This appears to be the earliest account which mentions the plague of rats.  Von Zimmern dates the event only as "several hundred years ago" (vor etlichen hundert jarn  [sic]), so that his version throws no light on the conflict of dates (see next paragraph). Another contemporary account is that of Johann Weyer in his De praestigiis daemonum (1563).


=== Other ===
Some theories have linked the disappearance of the children to mass psychogenic illness in the form of dancing mania. Dancing mania outbreaks occurred during the 13th century, including one in 1237 in which a large group of children travelled from Erfurt to Arnstadt (about 20 km), jumping and dancing all the way, in marked similarity to the legend of the Pied Piper of Hamelin, which originated at around the same time.Others have suggested that the children left Hamelin to be part of a pilgrimage, a military campaign, or even a new Children's crusade (which is said to have occurred in 1212) but never returned to their parents. These theories see the unnamed Piper as their leader or a recruiting agent.  The townspeople made up this story (instead of recording the facts) to avoid the wrath of the church or the king.William Manchester's A World Lit Only by Fire places the events in 1484, 100 years after the written mention in the town chronicles that "It is 100 years since our children left", and further proposes that the Pied Piper was a psychopathic paedophile, although for the time period it is highly improbable that one man could abduct so many children undetected.  Furthermore, nowhere in the book does Manchester offer proof of his description of the facts as he presents them.  He makes similar assertions regarding other legends, also without supporting evidence.


== Adaptations ==

In 1803, Johann Wolfgang von Goethe wrote a poem based on the story that was later set to music by Hugo Wolf. Goethe also incorporated references to the story in his version of Faust. (The first part of the drama was first published in 1808 and the second in 1832.)
Jakob and Wilhelm Grimm, known as the Brothers Grimm, drawing from 11 sources, included the tale in their collection Deutsche Sagen (first published in 1816). According to their account, two children were left behind, as one was blind and the other lame, so neither could follow the others. The rest became the founders of Siebenbürgen (Transylvania).
Using the Verstegan version of the tale (1605: the earliest account in English) and adopting the 1376 date, Robert Browning wrote a poem of that name which was published in his Dramatic Lyrics (1842). Browning's retelling in verse is notable for its humour, wordplay, and jingling rhymes.
Viktor Dyk's Krysař (The Rat-Catcher), published in 1915, retells the story in a slightly darker, more enigmatic way. The short novel also features the character of Faust.
In Marina Tsvetaeva's long poem liricheskaia satira, The Rat-Catcher (serialized in the émigré journal Volia Rossii in 1925–1926), rats are an allegory of people influenced by Bolshevik propaganda.
The Pied Piper (16 September 1933) is a short animated film based on the story, produced by Walt Disney Productions, directed by Wilfred Jackson, and released as a part of the Silly Symphonies series. It stars the voice talents of Billy Bletcher as the Mayor of Hamelin.
The Town on the Edge of the End, a comic-book version, was published by Walt Kelly in his 1954 Pogo collection Pogo Stepmother Goose.
Van Johnson starred as the Piper in NBC studios' adaptation: The Pied Piper of Hamelin (1957).
The Pied Piper is a 1972 British film directed by Jacques Demy and starring Jack Wild, Donald Pleasence, and John Hurt and featuring Donovan and Diana Dors.
The paperback horror novel Come, Follow Me by Philip Michaels (Avon Books, 1983) is based on the story.
In 1986, Jiří Bárta made an animated movie The Pied Piper based more on the above-mentioned story by Viktor Dyk; the movie was accompanied by the rock music by Michal Pavlíček.
In 1989, W11 Opera premiered Koppelberg, an opera they commissioned from composer Steve Gray and lyricist Norman Brooke; the work was based on the Robert Browning poem.
In Atom Egoyan's The Sweet Hereafter (1997), the myth of the Pied Piper is a metaphor for a town’s failure to protect its children.
China Miéville's 1998 London-set novel King Rat centers on the ancient rivalry between the rats (some of whom are portrayed as having humanlike characteristics) and the Pied Piper, who appears in the novel as a mysterious musician named Pete who infiltrates the local club-music scene.
The cast of Peanuts did their own version of the tale in the direct-to-DVD special It's the Pied Piper, Charlie Brown (2000), which was the final special to have the involvement of original creator Charles Schulz, who died before it was released.
Demons and Wizards' first album, Demons and Wizards (2000), includes a track called "The Whistler" which recounts the tale of the Pied Piper.
Terry Pratchett's 2001 young-adult novel, The Amazing Maurice and His Educated Rodents, parodies the legend from the perspective of the rats, the piper, and their handler.
The 2003 television film The Electric Piper, set in the United States in the 1960s, depicts the piper as a psychedelic rock guitarist modeled after Jimi Hendrix.
The Pied Piper of Hamelin was adapted in Happily Ever After: Fairy Tales for Every Child where it uses jazz music. The episode featured Wesley Snipes as the Pied Piper and the music performed by Ronnie Laws as well as the voices of Samuel L. Jackson as the Mayor of Hamelin, Grant Shaud as the Mayor's assistant Toadey (pronounced toe-day), John Ratzenberger and Richard Moll as respective guards Hinky and Dinky.
The Pied Piper, voiced by Jeremy Steig, has a small role in the 2010 Dreamworks animated film Shrek Forever After.
In the anime adaptation of the Japanese light novel series, Problem Children Are Coming from Another World, Aren't They? (2013), a major story revolves around the "false legend" of Pied Piper of Hamelin. The adaptation speaks in great length about the original source and the various versions of the story that sprang up throughout the years. It is stated that Weser, the representation of Natural Disaster, was the true Piper of Hamelin (meaning the children were killed by drowning or landslides).
In 2015, a South Korean horror movie title The Piper was released. It is a loose adaptation of the Brothers Grimm tale where the Pied Piper uses the rats for his revenge to kill all the villagers except for the children whom he traps in a cave.
The short story "The Rat King" by John Connolly, first included in the 2016 edition of his novel The Book of Lost Things, is a fairly faithful adaptation of the legend, but with a new ending. It was adapted for BBC Radio 4 and first broadcast on 28 October 2016.
In 2016, Victorian Opera presented The Pied Piper, an Opera by Richard Mills. At the Playhouse the Art Centre, Melbourne, Victoria, Australia.
In the American TV series Once Upon a Time, the Pied Piper is revealed to be Peter Pan, who is using pipes to call out to "lost boys" and take them away from their homes.
In the Netflix series The Society (TV series), a man named Pfeiffer removes a mysterious smell from the town of West Ham, but is not paid. Two days later he takes the kids on field trip in a school bus and returns them to an alternate version of the town where the adults are not present.
In the song "Pied Piper" by the boy group BTS, dedicated to their fans to remind them not to get distracted by said group.
In 2019, the collectible card game Magic: The Gathering, introduced a new set based on European Folk and Fairy Tails.  This set contained the first direct reference to the Piper, by being named "Piper of the Swarm."[1]
In the 1995 anime film Sailor Moon SuperS: The Movie, one of the film's antagonists named Poupelin was very similar to the Pied Piper, using his special flute to hypnotize children and follow him into his ship away from their homes where they sailed off to his home planet.
In Teenage Mutant Ninja Turtles there is a villain called the Rat King who uses rats as troops; like the Pied Piper he uses a flute to charm them and even turns Master Splinter on his prized students.
The HBO series Silicon Valley centres around a compression company called Pied Piper.  The denouement of the series depicts the company as benevolent and self-sacrificing as opposed to the extortionist depiction in the fable.  One of the characters refers to the company's eponymous inspiration as "a predatory flautist who murders children in a cave."


== As metaphor ==
Merriam-Webster definitions of pied pipera charismatic person who attracts followers
a musician who attracts mass
a leader who makes irresponsible promises


== Allusions in linguistics ==
In linguistics, pied-piping is the common name for the ability of question words and relative pronouns to drag other words along with them when brought to the front, as part of the phenomenon called Wh-movement. For example, in "For whom are the pictures?", the word "for" is pied-piped by "whom" away from its declarative position ("The pictures are for me"), and in "The mayor, pictures of whom adorn his office walls" both words "pictures of" are pied-piped in front of the relative pronoun, which normally starts the relative clause.
Some researchers believe that the tale has inspired the common English phrase "pay the piper", although the phrase is actually a contraction of the English proverb "he who pays the piper calls the tune" which simply means that the person paying for something is the one who gets to say how it should be done.


== Modernity ==
The present-day city of Hamelin continues to maintain information about the Pied Piper legend and possible origins of the story on its website.  Interest in the city's connection to the story remains so strong that, in 2009, Hamelin held a tourist festival to mark the 725th anniversary of the disappearance of the town's earlier children. The Rat Catcher's House is popular with visitors, although it bears no connection to the Rat-Catcher version of the legend.  Indeed, the Rattenfängerhaus is instead associated with the story due to the earlier inscription upon its facade mentioning the legend.  The house was built much later, in 1602 and 1603.  It is now a Hamelin City-owned restaurant with a Pied Piper theme throughout. The city also maintains an online shop with rat-themed merchandise as well as offering an officially licensed Hamelin Edition of the popular board game Monopoly which depicts the legendary Piper on the cover.In addition to the recent milestone festival, each year the city marks 26 June as "Rat Catcher's Day". In the United States, a similar holiday for Exterminators based on Rat Catcher's Day has failed to catch on and is marked on 22 July.


== See also ==

Hamelen (TV series)
List of literary accounts of the Pied Piper
Pied Piper of Hamelin in popular culture


== References ==


== Further reading ==
Marco Bergmann: Dunkler Pfeifer – Die bisher ungeschriebene Lebensgeschichte des "Rattenfängers von Hameln", BoD, 2. Auflage 2009, ISBN 978-3-8391-0104-9.
Hans Dobbertin: Quellensammlung zur Hamelner Rattenfängersage. Schwartz, Göttingen 1970.
Hans Dobbertin: Quellenaussagen zur Rattenfängersage. Niemeyer, Hameln 1996 (erw. Neuaufl.). ISBN 3-8271-9020-7.
Stanisław Dubiski: Ile prawdy w tej legendzie? (How much truth is there behind the Pied Piper Legend?). [In:] "Wiedza i Życie", No 6/1999.
Radu Florescu: In Search of the Pied Piper. Athena Press 2005. ISBN 1-84401-339-1.
Norbert Humburg: Der Rattenfänger von Hameln. Die berühmte Sagengestalt in Geschichte und Literatur, Malerei und Musik, auf der Bühne und im Film. Niemeyer, Hameln 2. Aufl. 1990. ISBN 3-87585-122-6.
Peter Stephan Jungk: Der Rattenfänger von Hameln. Recherchen und Gedanken zu einem sagenhaften Mythos. [In:] "Neue Rundschau", No 105 (1994), vol.2, pp. 67–73.
Ullrich Junker: Rübezahl – Sage und Wirklichkeit. [In:] „Unser Harz. Zeitschrift für Heimatgeschichte, Brauchtum und Natur". Goslar, December 2000, pp. 225–228.
Wolfgang Mieder: Der Rattenfänger von Hameln. Die Sage in Literatur, Medien und Karikatur. Praesens, Wien 2002. ISBN 3-7069-0175-7.
Aleksander R. Michalak: Denar dla Szczurołapa, Replika 2018. ISBN 978-83-7674-703-3.
Heinrich Spanuth: Der Rattenfänger von Hameln. Niemeyer Hameln 1951.
Izabela Taraszczuk: Die Rattenfängersage: zur Deutung und Rezeption der Geschichte. [In:] Robert Buczek, Carsten Gansel, Paweł Zimniak, eds.: Germanistyka 3. Texte in Kontexten. Zielona Góra: Oficyna Wydawnicza Uniwersytetu Zielonogórskiego 2004, pp. 261–273. ISBN 83-89712-29-6.
Jürgen Udolph: Zogen die Hamelner Aussiedler nach Mähren? Die Rattenfängersage aus namenkundlicher Sicht. [In:] Niedersächsisches Jahrbuch für Landesgeschichte 69 (1997), pp. 125–183. ISSN 0078-0561


== External links ==
D. L. Ashliman of the University of Pittsburgh quotes the Grimm's "Children of Hamelin" in full, as well as a number of similar and related legends.
An 1888 illustrated version of Robert Browning's poem (Illustrated by Kate Greenaway)
The 725th anniversary of the Pied Piper in 2009
The Pied Piper of Hamelin From the Collections at the Library of Congress
A Translation of Grimm's Saga No. 245 "The Children of Hameln"
A version of the legend from Howel's Famous Letters
Uri Avnery, "The Pied Piper of Zion", Gush Shalom website. 30 January 2016. About Israeli Prime Minister Binyamin Netanyahu and contemporary Israel.