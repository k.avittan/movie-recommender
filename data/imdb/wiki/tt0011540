John Dean "Jeff" Cooper (May 10, 1920 – September 25, 2006) was a United States Marine, the creator of the "modern technique" of handgun shooting, and an expert on the use and history of small arms.


== Early life and education ==
Cooper was born in Los Angeles where he enrolled in the Junior Reserve Officers' Training Corps at Los Angeles High School. He graduated from Stanford University with a bachelor's degree in political science. He received a regular commission in the United States Marine Corps (USMC) in September 1941. During World War II he served in the Pacific theatre with the Marine Detachment aboard USS Pennsylvania. By the end of the war he had been promoted to major. He resigned his commission in 1949, but returned to active duty during the Korean War, where he was involved in irregular warfare, and was promoted to lieutenant colonel. After the Korean War, the Marine Corps declined his application to remain on active duty. In the mid-1960s, he received a master's degree in history from the University of California, Riverside. From the late 1950s through the early 1970s, he was a part-time high school and community college history teacher.


== Career ==
In 1976, Cooper founded the American Pistol Institute (API) in Paulden, Arizona (later the Gunsite Academy). Cooper began teaching shotgun and rifle classes to both law enforcement and military personnel, as well as civilians, and did on-site training for individuals and groups from around the world. He sold the firm in 1992, but continued living on the Paulden ranch. He was known for his advocacy of large caliber handguns, especially the Colt 1911 and the .45 ACP cartridge.


=== The Modern Technique of the Pistol ===
Cooper's modern technique defines pragmatic use of the pistol for personal protection. The modern technique emphasizes two-handed shooting using the Weaver stance, competing with and eventually supplanting the once-prevalent one-handed shooting. The five elements of the modern technique are:

A large caliber pistol, preferably a semi-auto
The Weaver stance
The draw stroke
The flash sight picture
The compressed surprise trigger break


==== Firearm conditions of readiness ====
Cooper favored the Colt M1911 pistol and its variants.  There are several conditions of readiness in which such a weapon can be carried. Cooper promulgated most of the following terms:

Condition 4: Chamber empty, empty magazine, hammer down.
Condition 3: Chamber empty, full magazine in place, hammer down.
Condition 2: A round chambered, full magazine in place, hammer down.
Condition 1: A round chambered, full magazine in place, hammer cocked, safety on.
Condition 0: A round chambered, full magazine in place, hammer cocked, safety off.Condition 0 is considered "ready to fire;" as a result, there is a risk of accidental or negligent discharge carrying in Condition 0.


==== Bren Ten ====
Cooper conceived and designed the Bren Ten pistol around the 10mm Auto, based on the Czech CZ 75 design. The cartridge was more powerful than both the 9×19mm Parabellum and the .45 ACP round.


=== Combat mindset and the Cooper color code ===
The most important means of surviving a lethal confrontation, according to Cooper, is neither the weapon nor the martial skills.  The primary tool is the combat mindset, set forth in his book, Principles of Personal Defense.The color code, as originally introduced by Jeff Cooper, had nothing to do with tactical situations or alertness levels, but rather with one's state of mind.  As taught by Cooper, it relates to the degree of peril you are willing to do something about and which allows you to move from one level of mindset to another to enable you to properly handle a given situation.  Cooper did not claim to have invented anything in particular with the color code, but he was apparently the first to use it as an indication of mental state.The following is from The Carry Book: Minnesota Edition, 2011:
White: Unaware and unprepared.  If attacked in Condition White, the only thing that may save you is the inadequacy or ineptitude of your attacker.  When confronted by something nasty, your reaction will probably be "Oh my God! This can't be happening to me."
Yellow: Relaxed alert.  No specific threat situation.  Your mindset is that "today could be the day I may have to defend myself".  You are simply aware that the world is a potentially unfriendly place and that you are prepared to defend yourself, if necessary.  You use your eyes and ears, and realize that "I may have to shoot today".   You don't have to be armed in this state, but if you are armed you should be in Condition Yellow.  You should always be in Yellow whenever you are in unfamiliar surroundings or among people you don't know.  You can remain in Yellow for long periods, as long as you are able to "Watch your six."  (In aviation 12 o'clock refers to the direction in front of the aircraft's nose.  Six o'clock is the blind spot behind the pilot.)  In Yellow, you are "taking in" surrounding information in a relaxed but alert manner, like a continuous 360 degree radar sweep. As Cooper put it, "I might have to shoot."
Orange: Specific alert.  Something is not quite right and has your attention. Your radar has picked up a specific alert.  You shift your primary focus to determine if there is a threat (but you do not drop your six). Your mindset shifts to "I may have to shoot that person today", focusing on the specific target which has caused the escalation in alert status. In Condition Orange, you set a mental trigger: "If that person does "X", I will need to stop them".  Your pistol usually remains holstered in this state.  Staying in Orange can be a bit of a mental strain, but you can stay in it for as long as you need to. If the threat proves to be nothing, you shift back to Condition Yellow.
Red: Condition Red is fight.  Your mental trigger (established back in Condition Orange) has been tripped. "If 'X' happens I will shoot that person" — 'X' has happened, the fight is on.The USMC uses "Condition Black," although it was not originally part of Cooper's color code. According to Massad Ayoob, "Condition Black," in Cooper's youth, meant "combat in progress." "Condition Black" is also used to mean "immobilized by panic" or "overwhelmed by fear".In short, the color code helps one "think" in a fight.  As the level of danger increases, one's willingness to take certain actions increases.  If one ever does go to Condition Red, the decision to use lethal force has already been made —  the "mental trigger" has been tripped.The following are some of Cooper's additional comments on the subject.
Considering the principles of personal defense, we have long since come up with the color code.  This has met with surprising success in debriefings throughout the world.  The color code, as we preach it, runs white, yellow, orange, and red, and is a means of setting one's mind into the proper condition when exercising lethal violence, and is not as easy as I had thought at first.
There is a problem in that some  students insist upon confusing the appropriate color with the amount of danger evident in the situation.  As I have long taught, you are not in any color state because of the specific amount of danger you may be in, but rather in a mental state which enables you to take a difficult psychological step. Now, however, the government has gone into this and is handing out color codes nationwide based upon the apparent nature of a peril.  It has always been difficult to teach the Gunsite color code, and now it is more so.

We cannot say that the government's ideas about colors are wrong, but that they are different from what we have long taught here. The problem is this: your combat mind-set is not dictated by the amount of danger to which you are exposed at the time.  Your combat mind-set is properly dictated by the state of mind you think appropriate to the situation.  You may be in deadly danger at all times, regardless of what the Defense Department tells you.  The color code which influences you does depend upon the willingness you have to jump a psychological barrier against taking irrevocable action.  That decision is less hard to make since the jihadis have already made it.
He further simplified things in 2005:

In White you are unprepared and unready to take lethal action. If you are attacked in White you will probably die unless your adversary is totally inept.
In Yellow you bring yourself to the understanding that your life may be in danger and that you may have to do something about it.
In Orange you have determined upon a specific adversary and are prepared to take action which may result in his death, but you are not in a lethal mode.

In Red you are in a lethal mode and will shoot if circumstances warrant.


=== Firearms safety ===
Cooper advocated four basic rules of gun safety:

All guns are always loaded.  Even if they are not, treat them as if they are.
Never let the muzzle cover anything you are not willing to destroy.  (For those who insist that this particular gun is unloaded, see Rule 1.)
Keep your finger off the trigger till your sights are on the target.  This is the Golden Rule.  Its violation is directly responsible for about 60 percent of inadvertent discharges.
Identify your target, and what is behind it.  Never shoot at anything that you have not positively identified.


=== Rifle concepts ===
Cooper is best known for his work in pistol training, but he favored the rifle for tactical shooting. He often described the handgun as a convenient-to-carry stopgap weapon, allowing someone the opportunity to get to a rifle.

Personal weapons are what raised mankind out of the mud, and the rifle is the queen of personal weapons.
The rifle is a weapon. Let there be no mistake about that. It is a tool of power, and thus dependent completely upon the moral stature of its user. It is equally useful in securing meat for the table, destroying group enemies on the battlefield, and resisting tyranny. In fact, it is the only means of resisting tyranny, since a citizenry armed with rifles simply cannot be tyrannized.

The rifle itself has no moral stature, since it has no will of its own. Naturally, it may be used by evil men for evil purposes, but there are more good men than evil, and while the latter cannot be persuaded to the path of righteousness by propaganda, they can certainly be corrected by good men with rifles.


==== Scout rifle ====
Greatly influenced by the life and writings of Frederick Russell Burnham, Cooper published an article in the 1980s describing his ideal of a general-purpose rifle, "... a short, light, handy, versatile, utility rifle", which Cooper dubbed a "scout rifle". This was a bolt-action carbine chambered in .30 caliber (7.62 mm), less than 1 meter in length, less than 3 kilograms in weight, with iron sights, a forward mounted optical sight (long eye relief scope), and fitted with a practical sling (such as Ching sling).  Cooper defined his goal: "... a general-purpose rifle is a conveniently portable, individually operated firearm, capable of striking a single decisive blow, on a live target of up to 200 kilos in weight, at any distance at which the operator can shoot with the precision necessary to place a shot in a vital area of the target". Cooper felt the scout rifle should be suited to a man operating like the scout Burnham, either alone or in a two- or three-man team.In late 1997, Steyr Mannlicher produced a rifle to his "Scout" specifications, with Cooper's oversight during the engineering and manufacturing process. While not a spectacular sales success, these rifles nevertheless sold quite well and are still being produced.  Cooper considered the Steyr Scout "perfect" and often made the point that "I've got mine!"  Riflemen regard Cooper's development of the Scout Rifle concept, and his subsequent work on the evolution of the Steyr-Mannlicher Scout rifle, as his most significant and enduring contributions to riflecraft.  Ruger (Gunsite Scout Rifle), Savage Arms, Springfield Armory, and Mossberg have made versions of the Scout Rifle as well.


=== Ammunition concepts ===
Cooper was dissatisfied with the small-diameter 5.56×45mm NATO (.223 Remington) of the AR-15, and envisioned a need for a large bore (.44 cal or greater) cartridge in a semi-automatic rifle to provide increased stopping power and one-shot kills on big-game animals at 250 yards. This became known as the Thumper concept, inspiring the development of the .450 Bushmaster, .50 Beowulf and the .458 SOCOM, among other cartridges, all of them suitable for integration into preferably the AR-15/M16 rifle/M4 carbine or alternatively the AR-10/M14 rifle platforms.
Along the lines of the Thumper concept, Tim LeGendre of LeMag Firearms developed .45 Professional, the predecessor of the .450 Bushmaster cartridge, and later built and delivered an AR-15 in .45 Professional to Cooper  while Bill Alexander of Alexander Arms developed the .50 Beowulf.Encountering similar lack of stopping power issues as Cooper, the need for a large bore round for the AR-15 platform came about from informal discussion of members of the special operations command, specifically Task Force Ranger's experience in the Battle of Mogadishu (1993) that multiple shots were required to incapacitate members of the opposing force. Many Somalis would chew the drug Khat all day and the effects of the drug would both curb their appetite and increase their pain tolerance. Consequently Marty ter Weeme of Teppo Jutsu designed the .458 SOCOM cartridge in 2000 and Tony Rumore of Tromix was contracted to build the first .458 SOCOM rifle in February 2001.
Among the Thumper rounds requiring the use of the AR-10 platform due to their larger cartridge dimensions are the .45 Raptor, .50 Krater, .500 Auto Max, .500 Phantom, .500 Whisper, .510 Whisper and .510 Winchester Short Magnum cartridges.
The .50 Alaskan developed by Harold Johnson and Harold Fuller of the Kenai Peninsula of Alaska in the 1950s based on the .348 Winchester as well as the .45 Alaskan, .457 Wild West Magnum, and the .510 Kodiak Express are in some sense predecessors of Jeff Cooper's Thumper concept originating from the time of hunting large bears in Alaska using lever action rifles.


=== Writing ===
In 1997, Cooper wrote that he coined the term hoplophobia in 1962 "in response to a perceived need for a word to describe a mental aberration consisting of an unreasoning terror of gadgetry, specifically, weapons."In addition to his books on firearms and self-defense, Cooper wrote several books recounting his life adventures plus essays and short stories, including Fire Works (1980); Another Country: Personal Adventures of the Twentieth Century (1992); To Ride, Shoot Straight and Speak the Truth (1988); and C Stories (2004).  His daughter Lindy Wisdom published a biography, Jeff Cooper: the Soul and the Spirit (1996).
Jeff Cooper was regarded as one of the world's foremost authorities on big-game hunting.
Some of the comments from his "Gunsite Gossip" newsletter were printed in Guns & Ammo magazine as "Cooper's Corner" and later were compiled into The Gargantuan Gunsite Gossip.  These were his thoughts on firearms interleavened with his wide-ranging musings on many other subjects, and acquired a large U.S. and international following from the 1980s up to his death.  The firearms guru famous quotes became known as "Cooperism" and reflects on his philosophy and doctrine that has shaped the modern firearms world. Additionally Cooper wrote extensively and advocated firearms rights.
A complete bibliography of Jeff Cooper's writings from 1947 onwards is available at the Jeff Cooper Bibliography Project.Cooper was the Founding President and Honorary Lifetime Chairman of the International Practical Shooting Confederation. However, he was critical of the way he believed IPSC departed from the original focus on practical weapons toward what he called rooney guns highly modified pistols that he believed were not appropriate for practical daily service.  Numerous of his "Gunsite Gossip" and "Cooper's Corner" articles dealt with this issue and the "gamesmen" who he believed had caused IPSC to deteriorate.


== Personal life ==
Cooper was married to his wife Janelle for 64 years. They had three daughters. He died at his home on September 25, 2006, at the age of 86.


== Political views ==
Cooper held strongly conservative political opinions. In 1991, he wrote in Guns & Ammo magazine that "no more than five to ten people in a hundred who die by gunfire in Los Angeles are any loss to society. These people fight small wars amongst themselves. It would seem a valid social service to keep them well-supplied with ammunition." In 1994, Cooper said "Los Angeles and Ho Chi Minh City have declared themselves sister cities. It makes sense: they are both Third World metropolises formerly occupied by Americans." Cooper had expressed support of Jonas Savimbi, the governments of Rhodesia and apartheid-era South Africa, and 
Francisco Franco.


== See also ==

Handgun hunting
Jack Weaver
Mel Tappan
Self-defense
Thell Reed


== References ==
Abbreviations:

CC: Cooper's Commentaries
GG: Gunsight Gossip
GGG1: The Gargantuan Gunsight Gossip, Gunsight Press, Paulden, Arizona, USA, 1990, ISBN 0962134228, contains Gunsight Gossip Volumes 1 to 9, 1981 to 1989.
GGG2: Gargantuan Gunsight Gossip 2, Gunsight Press, Paulden, Arizona, USA, 2001, ISBN 0962134252, contains Gunsight Gossip Volumes 10 to 20, 1990 to 2000.
GGG3: Gargantuan Gunsight Gossip 3, Gunsight Press, Paulden, Arizona, USA, 2010, ISBN 0965540987, contains Gunsight Gossip Volumes 21 to 26, 2001 to 2006.Cooper's Commentaries is an unedited superset of Gunsight Gossip, with CC Vol. 1, No. 1 corresponding to GG Vol. XIII, No. 9, and an edited version of these were published as "Cooper's Corner" in Guns & Ammo magazine starting in 1986.


== Further reading ==
Campbell, Robert K. (2011). Gun Digest Shooter's Guide to the 1911. Gun Digest Books. p. 60. ISBN 9781440218880. Retrieved March 10, 2015.
Lake, Peter A. (February 1981). "Shooting to Kill". Esquire: 79–83.


== External links ==

Official Website of The Jeff Cooper Legacy Foundation
Origins of Have a Gun at The Gun Zone
Jeff Cooper's Commentaries
Jeff Cooper Books official site of Jeff Cooper and Wisdom Publishing (per Lindy Cooper Wisdom)