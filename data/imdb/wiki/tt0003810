Bess Meredyth (born Helen Elizabeth MacGlashen, February 12, 1890 – July 13, 1969) was a screenwriter and silent film actress. The wife of film director Michael Curtiz, Meredyth wrote The Affairs of Cellini (1934) and adapted The Unsuspected (1947). She was one of the 36 founders of the Academy of Motion Picture Arts and Sciences.


== Early life ==
Meredyth began her involvement in performing and writing from an early age. Her father was the manager at a local theatre, and she studied piano throughout her childhood. After encouragement from her English teacher, Meredyth also pursued fiction writing. At the age of 13, she approached the local newspaper editor about writing a fiction column. Each story she wrote for the paper earned her a dollar, making this her first paid work as a writer.Meredyth began her career in show business in vaudeville as a comedian. She most often sang or performed monologues while accompanying herself on the piano, a form she referred to as a "pianologue." 


== Acting career ==
Meredyth began her screen career as an extra at D.W. Griffith's Biograph Studios in New York, before moving to Los Angeles in 1911. Meredyth worked as an actress for five years, subsidizing her income with screenwriting. While most of this work was as an extra, her most prominent role was the titular character in the 4-reel Bess the Detectress (1914) serials.


== Relationship With Wilfred Lucas ==
Meredyth met Wilfred Lucas in 1911 when he encouraged her to pursue screen acting. The year after, the two worked together on the film A Sailor's Heart (1913), the first of many artistic collaborations. They were eventually given their production unit at Universal Studios, in which they produced the 30-reel long Trey of Hearts (1914) serials.
Meredyth and Lucas had one child together, television writer John Meredyth Lucas. They divorced in 1927, following her return from supervising Ben Hur (1925)


== Work with Snowy Baker ==
In 1918, Meredyth and Lucas traveled to Australia to work with  Australian sportsman Snowy Baker. They made three films together, The Man from Kangaroo (1920), The Jackeroo of Coolabong (1920) and The Shadow of Lightning Ridge (1921), the first two of which Meredyth co-directed. She was arguably the first professional screenwriter to work in Australia.


== Relationship with Michael Curtiz ==
Meredyth and director Michael Curtiz met soon after his arrival in the United States, while both were working at Warner Brothers Studios. They were married in 1929 and unsuccessfully attempted to start a production unit at MGM studios in 1946.Though often uncredited, Meredyth contributed to several of Curtiz's projects. Most notably, Curtiz reportedly called Meredyth for input several times a day while working on his most successful film, Casablanca (1942). 
Meredyth and Curtiz separated twice; once in 1941, and again in 1960. However, they remained in contact after this separation, and Curtiz included Meredyth in his will upon his death in 1962.


== Book ==
In 1934, Covei-Friede published The Mighty Barnum by Meredyth and Gene Fowler. In a review in The New York Times, John Chamberlain wrote that the book "marks the first time that a motion-picture scenario, or 'shooting script,' has been published in book form."


== Retirement ==
Throughout her time at MGM studios, Meredyth had mainly worked under Irving Thalberg. Upon his death in 1936, the new MGM executives dropped Meredyth's contract. Rather than re-entering as a "junior writer," as the new executives offered, Meredyth decided to retire from professional screenwriting. Despite this announcement, she has three credits after her alleged retirement, The Mark of Zorro (1940), That Night in Rio (1941), and The Unsuspected (1947).


== Death ==
On July 13, 1969, Meredyth died at the Motion Picture Country Hospital at age 79.


== Filmography ==


=== As writer ===


=== As actress ===


== References ==


== External links ==
Bess Meredyth on IMDb
Bess Meredyth at the Women Film Pioneers Project