Cathy is an American gag-a-day comic strip, drawn by Cathy Guisewite from 1976 until 2010. The comic is about a woman, Cathy, who struggles through the "four basic guilt groups" of life—food, love, family, and work. The strip gently pokes fun at the lives and foibles of modern women. The strip debuted on November 22, 1976, and appeared in over 1,400 newspapers at its peak. The strips have been compiled into more than 20 books. Three television specials were also created. Guisewite received the National Cartoonists Society Reuben Award in 1992 for the strip.


== History ==
Initially, the strip was based largely on Guisewite's own life as a single woman. "The syndicate felt it would make the strip more relatable if the character's name and my name were the same," Guisewite said in an interview. "They felt it would make it a more personal strip, and it would help people to know it was a real woman who was going through these things. I hated the idea of calling it 'Cathy.'" Guisewite had Cathy's longtime boyfriend Irving propose marriage on Valentine's Day 2004. The two characters married in the February 5, 2005 strip. That same year, Cathy appeared in the 75th anniversary party of Blondie and Dagwood.
On August 11, 2010, Cathy Guisewite announced the decision to end the run of Cathy. On October 3, 2010, the final strip ran, with the revelation that Cathy is pregnant with a girl.On June 1, 2020, Cathy Commiserations began on GoComics. The earliest strip was dated March 15, 2020.


== Main characters ==
Cathy Andrews Hillman (born Katharine Marianna Martucci): the protagonist of the strip and the only one not shown with a nose (although a small one was occasionally seen in side views). Cathy is also the only character that does not have beady eyes; instead, hers are drawn in an inverted heart-shape.
Andrea: initially Cathy's feminist foil and best friend and really her younger sister (Cathy was the eldest of four children, all girls), Andrea's role was reduced in the later years. Andrea is married to Luke, whom she met during an on-line chat, and has two children, Zenith and Gus, who grew from infants to uninhibited teens (not seen in the strips), illustrating—especially Zenith—that the characters do age, albeit very slowly. With Zenith, Andrea was obsessed with motherhood, to the point of eating special diets while pregnant and playing music on headphones into her pregnant womb to ensure her baby was born with a high IQ, and then bonding so tightly with Zenith that she understood her baby babble perfectly. Gus, however, entered a playgroup and she did not have as strong a connection with him (which initially left her distraught).
Electra: Cathy's dog—rescued from the animal shelter as a puppy.
Vivian: Irving's dog—also a rescue dog from the shelter. Both Cathy and Irving said that the dogs rescued them.
Anne (Annamaria) Andrews (née Martucci): Cathy's mother. Just as likely to provide frustration as inspiration. Refused to throw anything out, even online. Was highly marriage obsessed and was constantly either asking Cathy when she was getting married or trying to fix her up.
Bill Andrews (born Willy Baba): Cathy's father. More often than not, a helpless bystander to Cathy's mom. Cathy went to him when she needed advice about work, something Cathy's mom (even though she had created jobs—M.O.Ms, Mothers on the Move, or Granny Nannies, the daycare center in Flo's house) said she knows little about.
Charlene McGuire (née Markota): one of Cathy's best friends and really her cousin, receptionist at her office. Charlene's role expanded greatly once Andrea got married. Charlene announced her pregnancy on Sunday, May 20, 2007.
Simon: Charlene's husband. He and Cathy dated ten times before she decided that he wasn't for her. But (at first) Cathy was adamant that Charlene should have adhered to the "woman's rules" one of which is to never date a woman's old boyfriend. He and Charlene broke up briefly, which led to both her and Cathy signing up for video dating, but they eventually got back together.
Irving Hillman: former on-again-off-again love interest, golf and electronics addict and her husband as of February 5, 2005. The more complex the machinery, the better he liked it—even if he did not understand how to set it up.
Mr. Earl Pinkley: Cathy's boss, who often managed to make what little control he actually had of his work environment go a very long way. At one point, he sexually harassed Cathy by coming on to her in her apartment. Cathy solved the problem by telling Charlene what really happened and the gossip about her boss "spending the night" (he forgot to mention it was due to the fact that he was knocked out when Cathy punched him in the nose).
Mrs Hillman (née Polanski): Irving's mother who treated him like a "king". Did everything in her power to aggravate Cathy, especially regarding the 2007 Hillman family reunion.
Mr. Hillman: Irving's father. He seemed content to let his wife do all the talking.
Mabel represented multiple people (she was usually seen as the salesperson at any given store). She seemed to work at every single store, bank, travel agency etc. that Cathy or other characters went to (including Andrea's nurse when she had Zenith) and always had a pencil in her hair. She was easily aggravated by Cathy and vice versa. She sometimes had a co-worker with her. In at least one strip, the co-worker was referred to as Edith.
Emerson was one of the first boyfriends introduced (Irving was first) when the strip debuted. He pursued Cathy who figured she could do better and irritated Irving. He was eventually dropped in favor of Irving, but is often mentioned in Cathy's list of those she dated.
Many, many men that Cathy dated briefly.


== Animated specials ==

Three animated specials were made from the strip: Cathy, Cathy's Last Resort and Cathy's Valentine. All aired on CBS, and the former won Guisewite an Emmy Award.


== The four basic guilt groups ==
Defined by Cathy Guisewite, the four basic guilt groups are four types of temptation that the character Cathy faces in her daily life.


=== Food ===
Cathy had a love/hate affair with food (especially carbs). She loved it, but hated what it did to her thighs. She was visibly overweight, but not obese; she was often shown in a department store fitting room trying to stuff herself into a bathing suit. She was constantly on a diet, weighed herself obsessively and many mornings feared to get up, believing that she had ballooned overnight. Cathy was particularly fond of chocolate, pizza, and her mother's cooking (including her aunt's homemade nut rolls every Christmas, evidence of Cathy being of part Croatian descent, her mother's maiden name being Martucci).


=== Love ===
Cathy had dated extensively, but was unable to find "Mr. Right". Although a number of love interests had come and gone over the years, none had come back like Irving, who had left her initially due to her dislike of cats. Later in the series, they finally marry.
At the end of the comic series, Irving and Cathy find out that they are expecting a child.


=== Mom ===
Although well-meaning, Mom's advice often frustrated Cathy, whether or not Mom was right. Cathy and her mother were from two different generations: Cathy grew up in the era of feminism, women's rights, and the sexual revolution. Mom was from the earlier, more conservative World War II—1950s era. Although an equal in her marriage to Cathy's Dad, Mom held many old-fashioned ideas. Prior to Cathy's marriage, she seemed to have an obsession with seeing Cathy married, right down to keeping a current copy of Bride magazine in her purse and going as far as trying to send Christmas cards to some of Cathy's ex-boyfriends, one of whom was incarcerated for swindling investors, until Cathy stopped her.


=== Work ===
Cathy had to juggle many tasks at Product Testing, Inc. Her boss, Mr. Pinkley, often asked the impossible, and Cathy always seems to pull through in the end and give him and the client exactly what they wanted, albeit with quite a bit of drama. Cathy also worked at Jamba Juice. She was fired for bringing home smoothies for her cats. She also had numerous other jobs including as a nurse, social worker, pop music artist (her other job when she finally married Irving) and even occasional presidential candidate.


== Books ==
Following are books featuring Cathy, illustrated by Guisewite. The chronological strips and special collections lists are believed to be complete; the other sections are not.
Note: capitalization appears according to the copyright page of the book.


=== Chronological strips ===
The following books are collections of strips in the order they were published.

Cathy Chronicles (1979)
"What do you mean, I still don't have equal rights??!" (1980)
What's a Nice Girl Like You Doing With a Double Bed (March 1981, ISBN 0-553-01316-5)
I think I'm having a Relationship with a Blueberry Pie! (1981, ISBN 0-553-01337-8)
Another Saturday Night of Wild and Reckless Abandon (September 1982, ISBN 0-8362-1201-0)
A Mouthful of Breath Mints and No One to Kiss (August 1983, ISBN 0-8362-1120-0)
Men should come with instruction booklets (August 1984, ISBN 0-8362-2055-2)
Wake me up when I'm a size 5 (July 1985, ISBN 0-8362-2069-2)
Thin thighs in thirty years (July 1986, ISBN 0-8362-2081-1)
A hand to hold, an opinion to reject (August 1987, ISBN 0-8362-2092-7)
Why do the right words always come out of the wrong mouth? (August 1988, ISBN 0-8362-1808-6)
My Granddaughter Has Fleas!! (August 1989, ISBN 0-8362-1855-8)
$14 in the Bank and a $200 Face in My Purse (August 1990, ISBN 0-8362-1820-5)
Only Love Can Break a Heart, But a Shoe Sale Can Come Close (August 1992, ISBN 0-8362-1893-0)
Revelations From a 45-Pound Purse (August 1993, ISBN 0-8362-1722-5)
The Child Within Has Been Awakened But the Old Lady on the Outside Just Collapsed (September 1994, ISBN 0-8362-1761-6)
Understanding the "Why" Chromosome (August 1995, ISBN 0-8362-0423-9)
Abs of Steel, Buns of Cinnamon (August 1997, ISBN 0-8362-3683-1)
I Am Woman, Hear Me Snore (August 1998, ISBN 0-8362-6821-0)
I'd Scream Except I Look So Fabulous (August 1999, ISBN 0-7407-0006-5)
Shoes: Chocolate for the Feet (August 2000, ISBN 0-7407-0555-5)


=== Special collections ===
The following books include strips already published in earlier books.

Reflections: A Fifteenth Anniversary Collection (January 1991, ISBN 0-8362-1877-9)
Cathy Twentieth Anniversary Collection (September 1996, ISBN 0-8362-2523-6)
Food (December 2001, ISBN 0-7407-2112-7)
Love (December 2001, ISBN 0-7407-2061-9)
Mom (December 2001, ISBN 0-7407-2060-0)
Work (December 2001, ISBN 0-7407-2062-7)
The Wedding of Cathy and Irving : A Cathy Collection (July 2005, ISBN 0-7407-2668-4)


=== Gift books ===
The following books are smaller than the works above. They may have a short storyline and are intended as gifts.


==== Hardback ====
Commiserations (September 1993, ISBN 0-8362-3048-5)
Like Mother, Like Daughter (September 1993, ISBN 0-8362-3049-3)
Confessions to My Mother (March 1999, ISBN 0-8362-8788-6)
Affirmations (May 1996, ISBN 0-8362-1058-1)
Shop till you drop, then sit down and buy shoes (May 1996, ISBN 0-8362-1068-9)


==== Paperback ====
Paperback gift books.

How to Get Rich, Fall in Love, Lose Weight, and Solve All Your Problems by Saying "No" (1983, ISBN 0-8362-1986-4)
Eat Your Way to a Better Relationship (1983, ISBN 0-8362-1987-2)


=== Other books ===
The following books feature Cathy illustrations by Guisewite, but are not authored by her.

Dancing Through Life in a Pair of Broken Heels, written by Mickey Guisewite, Cathy Guisewite's sister (April 1994, ISBN 0-553-37377-3)
Lifestyles of the Trim & Healthy, written by Matthew Bennett (June 1994, ISBN 0-9629502-9-7)
Girl Food written by Barbara Albright & Cathy Guisewite (1997, ISBN 0-8362-3173-2)
The Complete Cathy: A Collection of Comics for Every Published for Sexy Superstar (April 2014 HarperCollins Publishers)


== In popular culture ==


=== Comics ===
The June 18, 2017 strip of the relaunched Bloom County featured a dismayed Cathy awakening in bed alongside Steve Dallas. She also appeared in the strip the following day.The comic strip Luann featured Mabel (in the October 5, 2016 strip) as a worker of the 'Bridal Barn' wedding gown store where Toni Daytona went to purchase her wedding gown with the help of Nancy and Luann DeGroot.


=== Television ===
Caroline in the City centers around fictional cartoonist Caroline Duffy (Lea Thompson), who makes a rival strip. The relationship between Caroline and Cathy is described as "oily rag, lit cigarette". In the season 1 episode "Caroline and the Bad Back", Caroline hurts her back and is unable to meet a deadline. To prevent the newspapers from double running Cathy strips, Del (Eric Lutes) and Richard (Malcolm Gets) create one for Caroline. The strip is presented as being only funny to men. Upon seeing the strip, Caroline declares that: "Cathy is gonna have a field day with this".In Sex and the City season 3, "Cock-a-doodle-doo", Miranda compares her lonely and repetitive take-out orders to a Cathy cartoon, and Carrie asks her never to refer to Cathy again, indicating Cathy's status by 2000 as a symbol of an earlier generation's sad, contemptuous view of single women. (Miranda: "I'm sitting home with my cat, ordering the same thing almost every night. The only thing sadder would be a Cathy comic pasted on my refrigerator door." Carrie: "Never say Cathy comic to me again").On the series 30 Rock, Cathy has been referenced more than once. On the episode "Don Geiss, America and Hope",  Liz Lemon's boyfriend Wesley compares her obsession with food to being like "a Cathy cartoon that just won't end". In another episode, Liz exclaims "Chocolate, chocolate,  chocolate, aack!", which had been exclaimed by Cathy herself in an earlier cartoon.Broad City season 3, episode 8: "Burning Bridges" featured a reference to the comic strip.
On Saturday Night Live, Andy Samberg would portray Cathy on Weekend Update (Season 34, Episode 17; aired March 7, 2009).
In the 2010 The Big Bang Theory episode "The Wheaton Recurrence", Sheldon, having studied the comic, brings Penny some ice cream after she gets into a fight with her boyfriend Leonard, saying, "If you were a cat, I would have brought you a lasagna."
In The Simpsons episode "Girls Just Wanna Have Sums", a painting of Cathy is shown as part of a hall of famous female artists; in the painting, Cathy is shown in a two-piece swimsuit, saying "I'm finally able to fit into my bathing suit.... and it's September!".  In a Simpsons comic strip, Comic Book Guy owns a rare Cathy cartoon - the only one that men find funny.


== See also ==

Cathy Guisewite
List of women in comics
Reuben Award


== References ==


== External links ==
Cathy at goComics.com
Cathy on IMDb
Cathy's Last Resort on IMDb
Cathy's Valentine on IMDb
An Interview with Cathy Guisewite: Cartoonist enters 25th year of creating nationally syndicated comic strip Cathy from Universal Press Syndicate
Cathy Guisewite profile at National Cartoonist Society Awards (Reuben) Web site
Retrospective on Cathy's cultural influence in Jezebel, 2012