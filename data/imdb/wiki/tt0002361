The Midnight Wedding is a 1912 Australian silent film directed by Raymond Longford based on a popular Ruritanian stage play in which Longford had appeared. It is considered a lost film.
It should not be confused with a 1912 film from the Gaumont Company of the same name.


== Plot ==
In the fictitious European country of Savonia, the dashing Paul Valmar (Augustus Neville) enlists in the Hussars after the death of his mother. After five years of service he is becomes a lieutenant and is appointed sword master to the regiment, causing jealousy amongst other others, notably the young Prince Eugene von Strelsburg (George Parke) and the wealthy Captain von Scarsbruck (D.L. Dalziel). Von Scarsbruck has been rejected by Eugene's sister, the Princess Astrea (Lottie Lyell), and he gets Eugene involved in gambling. Eugene taunts Valmar about his parentage, resulting in a fight in which Eugene is injured. Valmar seeks refuge in a church.
Under the terms of her father's will, the Princess Astrea must marry, but is given the option of von Scarsbruck or a nunnery. Father Gerard conceives of the idea of uniting Valmar with the princess. He blindfolds the officer and marries him to her on midnight. Valmar is subsequently captured and brought towards the Crown Prince. Valmar informs him that he is the Crown Prince's own son.
Von Scarsbruck is still intent on forcing a marriage with Astrea by destroying her reputation. He visits her chamber late one night, and is discovered by Valmar who challenges him to a duel. Valmar is injured, and Astrea confesses to her brother that she is married to him. Valmar recovers from his wounds and fights another duel with Von Scarsbruck. During the fight the cowardly Eugene tries to strike up Valmar's sword and Valmar runs him through. Astrea succeeds in stopping the fight, then the Crown Prince intervenes and puts Eugene and von Scarsbruck under arrest, banning all women from officer's quarters. Astrea disguises herself as an officer and sneaks into see the injured Valnar. She is discovered by the Crown Prince and confesses she is married to him.
Three months later, Valmar has recovered from his wounds and duels von Scarsburck again. Astrea hears about this and rides to the duel just in time to see Valmar mortally wound his opponent. Valmar and Astrea marry again, this time in a large ceremony.


== Cast ==
Cast (in alphabetical order) 

J. Barry ... Rev. A. Cette
D.L. Dalziel ... Capt. Rudolph von Scarsbruck
Jack Goodall ... Father Gerard
Robert Henry ... Maj. Donelli
Tim Howard ... Cpl. Otto
Dorothy Judge ...Kathie
Nellie Kemberman ... Stephanie
Tom Leonard ... Pvt. Bobo
Victor Loydell ... Sgt. Max
Lottie Lyell ... Princess Astrea
Augustus Neville ... Paul Valmar
George Parke ... Lt. Prince Eugene
Harry Saville ... Innkeeper
Arthur Smith ... Dr. Eitel
Fred Twitcham ... Crown Prince of Savonia


== Original Play ==
The play had was first performed in Australia in 1906 and was enormously popular. Longford had appeared in it playing the role of Von Scarsbruck although he did not act in the film. The cast largely came from the stage production produced by Clarke and Clyde Meynell.


== Production ==
The movie was the first to be shot at Charles Cozens Spencer's new studio at Rushcutters Bay in Sydney, enabling it to feature elaborate sets.A duel sequence shot outside a Sydney church was one of the first duels ever put on film.


== Release ==
The movie is sometimes confused with a British film of the same name that was released in Australia around the same time. It received good reviews and was a popular success at the box office.


== References ==


== External links ==
The Midnight Wedding on IMDb
The Midnight Wedding at National Film and Sound Archive
Digital copy of original play at National Archives of Australia (registration required)