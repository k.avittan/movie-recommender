Perry Mason is a fictional character, an American criminal defense lawyer who is the main character in works of detective fiction written by Erle Stanley Gardner. Perry Mason features in 82 novels and 4 short stories, all of which involve a client being charged with murder, usually involving a preliminary hearing or jury trial. Typically, Mason establishes his client's innocence by finding the real murderer.
The character of Perry Mason was adapted for motion pictures and a long-running radio series. These were followed by the well-known adaptation, the CBS television series Perry Mason (1957–1966) starring Raymond Burr. A second television series, The New Perry Mason starring Monte Markham, ran from 1973 to 1974; and 30 Perry Mason television films ran from 1985 to 1995, with Burr reprising the role of Mason in 26 of them prior to his death in 1993. A third television series, HBO's Perry Mason starring Matthew Rhys, started airing in 2020.
The Perry Mason series ranks third in the top ten  best selling book series. In 2015, the American Bar Association's publishing imprint, Ankerwycke, began reissuing Gardner's Perry Mason books, which had been out of print in the United States.


== Character ==
As a child, Gardner was a reader of the magazine Youth's Companion, published by the Perry Mason Company; Gardner later borrowed the name for his fictional attorney.  Gardner provided more information about Mason's character in the earlier novels; knowledge of his character is largely taken for granted in the later works, the television series and movies. In the first novel (The Case of the Velvet Claws, 1933), Mason describes himself:

"You'll find that I'm a lawyer who has specialized in trial work, and in a lot of criminal work...I'm a specialist on getting people out of trouble.  They come to me when they're in all sorts of trouble, and I work them out ... If you look me up through some family lawyer or some corporation lawyer, he'll probably tell you that I'm a shyster.  If you look me up through some chap in the District Attorney's office, he'll tell you that I'm a dangerous antagonist but he doesn't know very much about me."
Gardner depicts Mason as a lawyer who fights hard on behalf of his clients and who enjoys unusual, difficult or nearly-hopeless cases.  He frequently accepts clients on a whim based on his curiosity about their problem, for a minimal retainer, and finances the investigation of their cases himself if necessary. In The Case of the Caretaker's Cat (1935), his principal antagonist, District Attorney Hamilton Burger, says: "You're a better detective than you are a lawyer.  When you turn your mind to the solution of a crime, you ferret out the truth." In The Case of the Moth-Eaten Mink (1952), a judge who has just witnessed one of the lawyer's unusual tactics says: "Mr. Mason...from time to time you seem to find yourself in predicaments from which you extricate yourself by unusual methods which invariably turn out to be legally sound.  The Court feels you are fully capable of looking after your own as well as your clients' interests."
Another frequent antagonist, Lieutenant Arthur Tragg of the homicide squad, has a discussion with Mason about his approach to the law. Mason is recovering from having been poisoned, and Tragg is investigating.  In The Case of the Drowsy Mosquito (1943) he says:

"How does it feel to be the victim for once? … You've been sticking up for criminals and now you can see the other side of the picture.""Not 'sticking up for criminals,'" (Mason) protested indignantly. "I have never stuck up for any criminal. I have merely asked for the orderly administration of an impartial justice ... Due legal process is my own safeguard against being convicted unjustly. To my mind, that's government. That's law and order."
Other than what is learned of his character from the novels themselves, very little is known about Perry Mason.  His family, personal life, background, and education are not depicted, although according to the first chapter of The Case of the Sleepwalker's Niece (1935), his astrological sign was Leo. Mason has a professional relationship with Paul Drake. Della Street is Mason's only evident (though not sharply delineated) romantic interest. It is known that he lives in an apartment because he is occasionally awakened from sleep to go to his office; he does not entertain anyone at home. His tastes in food are known because many scenes take place in restaurants, and that he is an excellent driver as shown by his participation in the occasional car chase. Other than those sketchy facts, there is so little physical description of him that the reader is not even sure what he looks like.The 1930s films were not closely based on the character of Perry Mason as revealed in the books, and contain plot and character developments which are not accepted as canonical in the remainder of the books and adaptations. For instance, in one film, Mason marries his longtime secretary Della Street, while Paul Drake turns into comic sidekick Spudsy Drake.Likewise the TV series diverges at times significantly from the books, which was a practical necessity considering that there were only about 80 Perry Mason novels written altogether and over 270 episodes of the TV series.  Thus there was a need for a great deal of invented material, background, plots, and characters – none of which material Gardner incorporated into his ongoing series of Perry Mason novels. In fact, Gardner would write over 30 more Perry Mason novels from between 1957 when the TV series began up until his death in 1970.The television series contains some hints of what Mason did in the past. In The Case of the Misguided Missile, he says that he served in the Navy on Ulithi atoll during WWII. In The Case of the Travelling Treasure, he says that he served aboard a minesweeper.The HBO series presents him as being a private detective, becoming a lawyer by necessity in order to salvage the case he's working on. In this series, he lives on what remains of a dairy farm which has been in his family for at least two prior generations. He is also a veteran of WWI, having been discharged with a "blue ticket" (i.e. with negative connotations), probably because he mercy-killed some comrades who were about to die from a poison gas attack which they were too severely wounded to escape. Mason is also an alcoholic, divorced father who is struggling to maintain ownership of his deceased parents' farm.


== Novels ==

Julian Symons has noted that Erle Stanley Gardner "had spent more than twenty years practicing law in California, and the knowledge he gained was put to good use in the Perry Mason stories, which hinge on points of law, forensic medicine or science as clever as a watch mechanism … and also the total lack of characterization".While the Mason novels were largely a form of pulp fiction of the sort that began Gardner's writing career, they are unusual in that the whodunit mysteries usually involved two solutions: one in which the authorities believed (wherein Mason's client was guilty) and an alternative explanation (wherein Mason's client was innocent). Almost always, the second half of each novel is devoted to a courtroom scene, during which Mason arrives at the alternative explanation and proves it to the satisfaction of the court. "It is perfectly true that our author works to formula; in one sense, the plot never varies," wrote Jacques Barzun. "Having said this, one must add that the variety of persons and circumstances and the ingenuity in contriving the details that Gardner dreamed up in his dozens of cases are astonishing and entrancing."A hallmark of the stories is that Perry Mason (with the assistance of his secretary Della Street and private investigator Paul Drake), once embarked on a case, will juggle the evidence using unusual (even bizarre) tactics to mislead the police – but (except for the very earliest novels) always in an ethical fashion:

It's my contention, Della, that an attorney doesn't have to sit back and wait until a witness gets on the stand and then test his recollection simply by asking him questions. If facts can be shuffled in such a way that it will confuse a witness who isn't absolutely certain of his story, and if the attorney doesn't suppress, conceal, or distort any of the actual evidence, I claim the attorney is within his rights.
The influence of the television series has given the general public the impression that Mason is highly ethical. In the earliest novels, however, Mason was not above skulduggery to win a case. In The Case of the Counterfeit Eye (1935) he breaks the law several times, including manufacturing false evidence (glass eyes). Mason manipulates evidence and witnesses, resulting in the acquittal of the murderer in The Case of the Howling Dog (1934). The Case of the Curious Bride (1934) is … a good Perry Mason except for one great flaw, which the author would scarcely have been guilty of later on: he tampers with the evidence, by having a friend move into an apartment and testify to the state of the doorbells. … One is left with the uncomfortable idea that maybe the murder did not take place as Mason reconstructs it.
In the later novels, the only crime which he can be seen to commit might be illegal entry, when he and Paul Drake are searching for evidence. And even then, he would expect to put up a strong and effective defense leading to an acquittal. Hamilton Burger is constantly under the impression that Mason has done something illegal, but is never able to prove it. Gardner prefaced many of his later novels with tributes to coroners and forensic pathologists whose work was instrumental to solving cases. Gardner inserts his ideas about the importance of proper autopsies into many of his Mason novels. In The Case of the Fugitive Nurse, for instance, close scrutiny of dental records in the identification of burned bodies is a key point. In that same story, the possible use of additives to track illegal resale of medical narcotics is examined.
Critic Russel B. Nye saw pattern in Gardner's novels, calling them as formal as Japanese Noh drama. He described fairly rigid plot points:

Attorney Perry Mason's case is introduced.
Mason and his crew investigate.
Mason's client is accused of a crime.
Further investigations ensue.
The trial begins.
In a courtroom coup, Mason introduces new evidence and often elicits a confession from the lawbreaker.The Perry Mason series ranks third in the top ten  best selling book series, with sales of 300 million. R. L. Stine's Goosebumps series is ranked second, with 350 million; J. K. Rowling's Harry Potter series is first, with 450 million.In June 2015, the American Bar Association announced that its new publishing imprint, Ankerwycke, would reissue Gardner's Perry Mason novels. The Case of the Velvet Claws, The Case of the Sulky Girl, The Case of the Lucky Legs, The Case of the Howling Dog and The Case of the Curious Bride were the first five novels announced for trade paperback release. The Perry Mason books had been out of print in the United States.


== Adaptations ==


=== Film ===
Warner Bros. released a series of six Perry Mason films in the 1930s.

The Case of the Howling Dog (1934), with Warren William as Perry Mason and Helen Trenholme as Della Street.
The Case of the Curious Bride (1935), with Warren William and Claire Dodd as Della Street.  Notable for the first-released American screen appearance of Errol Flynn as the corpse, who is seen alive but not speaking in a brief flashback.
The Case of the Lucky Legs (1935), with Warren William and Genevieve Tobin as Della Street.
The Case of the Velvet Claws (1936), with Warren William and Claire Dodd as Della Street Mason.
The Case of the Black Cat (1936) (from The Case of the Caretaker's Cat [1935]), with Ricardo Cortez as Perry Mason and June Travis as Della Street.
The Case of the Stuttering Bishop (1937), with Donald Woods as Perry Mason and Ann Dvorak as Della Street.The six Perry Mason films are available on DVD as a single-set release from the Warner Bros. Archive Collection.The 1940 Warner Bros. film, Granny Get Your Gun, was loosely based on the 1937 Perry Mason novel The Case of the Dangerous Dowager. May Robson stars as Minerva Hatton. The film does not include Perry Mason or any of the regular characters.


=== Radio ===

Perry Mason was adapted for radio as a 15-minute daily crime serial that aired 1943–55 on CBS Radio. It had little in common with the usual portrayal of Mason, so much so that Gardner withdrew his support for a TV version of the daytime serial that began airing on CBS in 1956. The general theme of the radio serial was continued, with a different title and characters, as The Edge of Night.


=== Television ===


==== Perry Mason (1957–1966) ====

The best-known incarnation of Perry Mason came in the form of a CBS TV series simply titled Perry Mason which ran from 1957 to 1966, with Raymond Burr in the title role. Also starring were Barbara Hale as Della Street, William Hopper as Paul Drake, William Talman as Hamilton Burger, and Ray Collins as Lt. Tragg. The entire series has been released on DVD and reruns are a staple in syndication.
CBS All Access has posted full 60-minute episodes from the first eight seasons on its website for viewing.


==== The New Perry Mason (1973–1974) ====

Several years after Perry Mason was cancelled, a new series, The New Perry Mason, aired in 1973 featuring Monte Markham in the title role. A total of 15 episodes aired before being cancelled halfway through its first season.


==== Television films (1985–1995) ====

American television producers Dean Hargrove and Fred Silverman resurrected the Perry Mason character in a series of television films for NBC beginning in 1985. The two surviving stars of the CBS-TV series, Raymond Burr and Barbara Hale, reprised their roles as Mason and Della Street. In the first telefilm, Perry Mason Returns, Mason is an appellate court judge who resigns his position to successfully defend his secretary Della on murder charges. William Katt, Hale's son, was cast as Paul Drake, Jr. William Hopper, who played private investigator Paul Drake in the original TV series, had died years earlier; Hopper's photograph appears on Paul Drake Jr.'s desk. In the later TV movies, Mason used the services of attorney Ken Malansky, played by William R. Moses.
The Perry Mason series of TV movies continued until Burr's death from kidney cancer in 1993. The Case of the Killer Kiss was Burr's final portrayal of Mason. The film aired after his death, and was dedicated to Burr's memory. Thereafter, the title of the series was changed to A Perry Mason Mystery and starred either Paul Sorvino or Hal Holbrook as lawyers and friends of Mason. Hale and Moses continued in their roles; Mason was ostensibly out of town.


==== Perry Mason (2020) ====

In August 2016, HBO announced a potential new series. In August 2017 a change in the writing staff for the project was announced, with Rolin Jones and Ron Fitzgerald taking over for Nic Pizzolatto. In January 2019, Robert Downey Jr. announced on his Twitter page that Matthew Rhys would be portraying Perry Mason in the new production; Downey was originally going to portray Mason, but was forced to bow out due to scheduling conflicts. The HBO revival and reboot adapted its setting to Great Depression-era Los Angeles, some twenty years earlier than the CBS show (but in line with the earliest novels by Gardner). It features John Lithgow and Tatiana Maslany in additional roles.
This miniseries was created with a budget of around $74.3 million and released its first episode on June 21, 2020. In July 2020, HBO announced that the mini-series had been picked up for a second season, and that the show would become a regular series.


=== Other adaptations ===
The Perry Mason character has appeared in comic books and a short-lived (October 16, 1950 – June 21, 1952) comic strip. He was also the inspiration for The Whole Truth (1986) by James Cummins, a book-length collection of sestinas.
In 2008, The Colonial Radio Theatre on the Air began producing a series of full-cast audio theater dramatizations of Gardner's Perry Mason novels, adapted by M. J. Elliott.


== Regular characters ==
Recurring characters in the Perry Mason stories include the following:

Perry Mason: Los Angeles attorney introduced in the 1933 novel, The Case of the Velvet Claws.
Della Street: Mason's confidential secretary introduced in the 1933 novel, The Case of the Velvet Claws.
Paul Drake: Private investigator introduced in the 1933 novel, The Case of the Velvet Claws.
Hamilton Burger: District attorney introduced in the 1935 novel, The Case of the Counterfeit Eye.
Lt. Arthur Tragg: Police homicide investigator introduced in the 1940 novel, The Case of the Silent Partner.
Gertie Lade: Mason's switchboard operator, an "incurable romantic" introduced in the 1939 novel, The Case of the Rolling Bones, and occasionally appearing in the CBS-TV series.
Sergeant Holcomb: Homicide detective often featured in the novels but in only two episodes of the CBS-TV series.
Carl Jackson: Junior attorney in Mason's law firm, appearing in the novels and a few episodes of the CBS-TV series Perry Mason.
David Gideon: Young legal assistant working with Mason in nine episodes of the CBS-TV series.
Lt. Andy Anderson: Police homicide investigator in the CBS-TV series.
Lt. Steve Drumm: Police homicide investigator in the CBS-TV series.
Terrance Clay: Restaurateur and friend of Mason in the CBS-TV series.
Paul Drake, Jr.: Paul Drake's son, also a private investigator, in the first nine Perry Mason television films.
Ken Malansky: Attorney who replaced Paul Drake, Jr., in 21 of the television films.
Lieutenant Ed Brock: Police commander in several of the television films.
Michael Reston: District attorney in eight of the television films.
Amy Hastings: Ken Malansky's girlfriend and assistant to Mason in three of the television films.
Elinor Harrelson: Judge in seven of the Perry Mason television films.
Barbara August: District attorney in two of the Perry Mason television films.


== Title listings ==


== Influence ==
In her confirmation hearings before the Senate Judiciary Committee in July 2009, Supreme Court nominee Sonia Sotomayor prefaced her remarks on the role of the prosecutor by saying that she was inspired by watching the Perry Mason television series as a child:

I was influenced so greatly by a television show in igniting the passion that I had as being a prosecutor, and it was Perry Mason … In one of the episodes, at the end of the episode … Perry said to the prosecutor, "It must cause you some pain having expended all that effort in your case to have the charges dismissed." And the prosecutor looked up and said, "No. My job as a prosecutor is do justice and justice is served when a guilty man is convicted and when an innocent man is not." And I thought to myself that's quite amazing to be able to serve that role …
The Perry Mason novels inspired Robert M. Bell, former Chief Judge of the Maryland Court of Appeals, to become a lawyer. "I used to read those growing up", he recalled in 2012. "I got the sense that a lawyer could do good things for folk and was important to our community. That’s what I wanted to do."


== Cultural references ==


== Notes ==


== References ==


== External links ==
Perry Mason at the Museum of Broadcast Communications Encyclopedia of Television
Database and cover gallery for the Dell Comic book based on the TV show
The Perry Mason TV Show Book by Brian Kelleher and Diana Merrill
Perry Mason (radio) at the Internet Archive
Perry Mason TV Series Wiki