Freckles are clusters of concentrated melaninized cells which are most easily visible on people with a fair complexion. Freckles do not have an increased number of the melanin-producing cells, or melanocytes, but instead have melanocytes that overproduce melanin granules (melanosomes) changing the coloration of the outer skin cells (keratinocytes). As such, freckles are different from lentigines and moles, which are caused by accumulation of melanocytes in a small area.
Freckles can appear on all types of skin tones. Of the six Fitzpatrick skin types, they are most common on skin tones 1 and 2, which usually belong to North Europeans. However, it can be found in all ethnicities.


== Biology ==
The formation of freckles is caused by exposure to sunlight. The exposure to UV-B radiation activates melanocytes to increase melanin production, which can cause freckles to become darker and more visible. This means that one who has never developed freckles may develop them suddenly following extended exposure to sunlight.
Freckles are predominantly found on the face, although they may appear on any skin exposed to the sun, such as arms or shoulders. Heavily distributed concentrations of melanin may cause freckles to multiply and cover an entire area of skin, such as the face.  Freckles are rare on infants, and more commonly found on children before puberty.
Upon exposure to the sun, freckles will reappear if they have been altered with creams or lasers and not protected from the sun, but do fade with age in some cases.
Freckles are not a skin disorder, but people with freckles generally have a lower concentration of photo-protective melanin, and are therefore more susceptible to the harmful effects of UV radiation. It is suggested that people whose skin tends to freckle should avoid overexposure to sun and use sunscreen.


=== Genetics ===

The presence of freckles is related to rare alleles of the MC1R gene, though it does not differentiate whether an individual will have freckles if they have one or even two copies of this gene. Also, individuals with no copies of the MC1R do sometimes display freckles. Even so, individuals with a high number of freckling sites have one or more of variants of the MC1R gene. Of the variants of the MC1R gene Arg151Cys, Arg160Trp, and Asp294His are the most common in the freckled subjects.  The MC1R gene is also associated with red hair more strongly than with freckles. Most red-haired individuals have two variants of the MC1R gene and almost all have one.  The variants that cause red hair are the same that cause freckling.  Freckling can also be found in areas, such as Japan, where red hair is not seen. These individuals have the variant Val92Met which is also found in white people, although it has minimal effects on their pigmentation. The R162Q allele has a disputed involvement in freckling.The variants of the MC1R gene that are linked with freckles started to emerge in the human genotype when humans started to leave Africa. The variant Val92Met arose somewhere between 250,000 and 100,000 years ago, long enough for this gene to be carried by humans into central Asia. Arg160Trp is estimated to have arisen around 80,000 years ago while Arg151Cys and Asp294His have been estimated to arise around 30,000 years ago. The wide variation of the MC1R gene exists in people of European descent because of the lack of strong environmental pressures on the gene.  The original allele of MC1R is coded for dark skin with a high melanin content in the cells. The high melanin content is protective in areas of high UV light exposure.  The need was less as humans moved into higher latitudes where incoming sunlight has lower UV light content.  The adaptation of lighter skin is needed so that individuals in higher latitudes can still absorb enough UV for the production of vitamin D.  Freckled individuals tend to tan less and have very light skin, which would have helped the individuals that expressed these genes absorb vitamin D.


== Types ==
Ephelides describes a freckle that is flat and light brown or red and fades with a reduction of sun exposure.  Ephelides are more common in those with light complexions, although they are found on people with a variety of skin tones. The regular use of sunblock can inhibit their development.
Liver spots (also known as sunspots and lentigines) look like large freckles, but they form after years of exposure to the sun. Liver spots are more common in older people.


== See also ==
Beauty mark
Mole
List of Mendelian traits in humans
Melanocortin 1 receptor


== References ==


== External links ==
MedicineNet.com: Freckles
Ephelides (Freckles) at eMedicine