In 1916, Bud Fisher licensed the production of Mutt and Jeff for animation with pioneers Charles Bowers and Raoul Barré of the Barré Studio. This resulted in 292 animated Mutt and Jeff shorts, making it the longest running theatrical animated short series of the silent era, second to Krazy Kat in terms of years. Series ran from 1916-1923 and 1925-1926. A few shorts of the second run were re-shot in 1930 with the Kromocolor process and reissued with a sound effects and music track. The following is a list of animated films in the series separated by years.


== 1916 ==
Jeff's Toothache
Domestic Difficulties
Mutt And Jeff In The Submarine
The Indestructible Hats
Cramps
The Dog Pound
The Hock Shop
The Promoters
Two for Five
Wall Street


== 1917 ==
The Submarine Chasers
A Chemical Calamity
A Day In Camp
A Dog's Life
Cows And Caws
In The Theatrical Business
Preparedness
Revenge Is Sweet
The Bell Hops
The Boarding House
The Chamber Of Horrors
The Cheese Tamers
The Interpreters
The Janitors
The Prospectors


== 1918 ==

A Lot of Bull
An Ace and a Joker
Around the World in Nine Minutes
Bulling the Bolshevik
Efficiency
At the Front
Saving Russia
Hitting the High Spots
Hotel De Mutt
Joining the Tanks
Landing a Spy
Our Four Days in Germany
Pot Luck in the Army
The Accident Attorney
The Doughboy
The Draft Board
The Kaiser's New Dentist
The New Champion
The Side Show
Throwing the Bull
To the Rescue
Revenge Is Sweet
Back To The Balkans
The Leak
Freight Investigation
On Ice
Helping McAdoo
A Fisherless Cartoon
Occultism
Superintendents
The Tonsorial Artists
The Tale Of A Pig
Hospital Orderlies
Life Savers
Meeting Theda Bara
The Seventy-Mile Gun
The Burglar Alarm
The Extra-Quick Lunch
Hunting The U-Boats


== 1919 ==

Here And There
The Hula Hula Cabaret
Dog-Gone Tough Luck
Landing An Heiress
The Bearded Lady
500 Miles On A Gallon Of Gas
The Pousse Cafe
Fireman Save My Child
Wild Waves And Angry Woman
William Hohenzollern Sausage Maker
Out an' In Again
The Cow's Husband
Mutt The Mutt Trainer
Subbing For Tom Mix
Pigtails And Peaches
Seeing Things
The Cave Man's Bride
Sir Sidney
Left at The Post
The Shell Game
Oh Teacher
Hands Up
Sweet Papa
Pets And Pests
A Prize Fight
Look Pleasant Please
Downstairs And Up
A Tropical Eggs-pedi-tion
West Is East
Sound Your 'A'
Hard Lions
Mutt And Jeff In Switzerland
All That Glitters Is Not Goldfish
Everybody's Doing It
Mutt And Jeff In Spain
The Honest Book Agent
Bound In Spaghetti
In The Money
The Window Cleaners
Confessions Of A Telephone Girl
The Plumbers
The Chambermaid's Revenge
Why Mutt Left The Village
Cutting Out His Nonsense
For Bitter Or For Verse
He Ain't Done Right by Our Nell
Another Man's Wife
New York Night Life
Oil's Well That Ends Well
The Frozen North
The Jazz Instructors


== 1920 ==
Dead Eye Jeff
Fishing
I'm Ringing Your Party
Putting On The Dog
The Chemists
The Mint Spy
The Pawnbrokers
The Plumbers
The Soul Violin
Hula Hula Town
The Beautiful Model
The Chewing Gum Industry
The Great Pickle Robbery
The Honest Jockey
The Price Of A Good Sneeze
Nothing But Girls
The Bicycle Race
The Bowling Alley
The Paper Hangers
The Private Detectives
The Wrestlers
A Trip To Mars
One Round Jeff
The Tango Dancers
The Toy Makers
Departed Spirits
The Breakfast Food Industry
The Mystery Of Galvanized Iron Ash Can
Three Raisins And A Cake Of Yeast
The Bare Idea
Hot Dogs
In Wrong
The Merry Cafe
The Politicians
The Yacht Race
Home Sweet Home
Napoleon
The Cowpunchers
The Song Birds
A Tightrope Romance
Flapjacks
The Brave Toreador
The High Cost Of Living
The League Of Nations
The Tailor Shop
A Hard Luck Santa Claus
All Stuck Up
Farm Efficiency
Gum Shoe Work
Home Brew
The Medicine Man
Cleopatra
On The Hop
Sherlock Hawkshaw And Company
The Hypnotist
The North Woods
The Papoose
The Parlor Bolshevist
A Glutton For Punishment
A Rose by Any Other Name
Fisherman's Luck
His Musical Soup
Mutt And Jeff In Iceland
Mutt And Jeff's Nooze Weekly
On Strike
Pretzel Farming
Shaking The Shimmy
The Berth Of A Nation
The Latest In Underwear
The Rum Runners


== 1921 ==
The Lion Hunters
The Ventriloquist
Dr. Killjoy
Factory To Consumer
A Crazy Idea
The Naturalists
Gathering Coconuts
It's A Bear
Mademoiselle Fifi
The Vacuum Cleaner
A Hard Shell Game
A Rare Bird
White Meat
Flivvering
Cold Tea
The Glue Factory
The Gusher
Watering The Elephants
The Far East
A Shocking Idea
The Far North
Touring
Training Woodpeckers
Crows And Scarecrows
Darkest Africa
Not Wedded But A Wife
The Painter's Frolic
The Stampede
The Tong Sandwich
Shadowed
The Turkish Bath
A Messy Christmas
Fast Freight
The Village Cutups
The Stolen Snooze
A Link Missing
Getting Ahead


== 1922 ==
Bony Parts
Stuck In The Mud
The Crystal Gazer
The Hole Cheese
The Phoney Focus
The Last Shot
Any Ice Today
The Cashier
Too Much Soap
Around The Pyramids
Getting Even
Golfing
Hoot Mon
Tin Foiled
Hop, Skip And Jump
Hither And Thither
Modern Fishing
Court Plastered
Falls Ahead
Riding The Goat
The Fallen Archers
Cold Turkey
The Wishing Duck
Bumps And Things
Nearing The End
Gym Jams
A Ghostly Wallop
Beside The Cider
Long Live The King
The Last Laugh
The Bull Fight


== 1923 ==
Down In Dixie


== 1925 ==
A Kick For Cinderella
Accidents Won't Happen
Soda Jerks
The Invisible Revenge
Where Am I?
The Bear Facts
Mixing in Mexico
Oceans Of Trouble
A Link Missing
Thou Shalt Not Pass
Aroma Of The South Seas
The Globe Trotters


== 1926 ==
Lots of Water
When Hell Freezes Over
Westward Whoa
Slick Sleuths
Ups And Downs
Playing With Fire
Dog Gone
The Big Swim
Mummy o' Mine
A Roman Scandal
Bombs and Bums
Skating Instructors
Set 'em Up Again
A Stretch in Time


== See also ==
Mutt and Jeff live-action filmography


== References ==