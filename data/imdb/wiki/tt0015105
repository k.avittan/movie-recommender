The Madonnina, commonly known as the Madonna of the Streets, was a painting created by Roberto Ferruzzi (1854–1934) that won the second Venice Biennale in 1897. 
The models for this painting were Angelina Cian (age 11) and her younger brother. Although not originally painted as a religious picture, this painting became popularized as an image of the Virgin Mary holding her infant son, and has become the most renowned of Ferruzzi's works.


== Fate of the original painting ==
The original painting made its first appearance at an art exhibition in Venice in 1897. John George Alexander Leishman, steel millionaire and diplomat, who died in 1924 in France, bought the painting but not the reproduction rights; he is the last known owner. It is possible that the image entered a private art collection in Pennsylvania in the 1950s, but the current location of the original is unknown.  An original oil painting entitled Madonna and Child from Florence, Italy was bequeathed to the Sisters of St. Casimir by a Dr. Edgar W. Crass in 1950 and bears a striking resemblance to the current prints of the missing La Madonnina by Roberto Feruzzi.
A second possibility is that it was lost at sea in the Atlantic Ocean on a voyage from Europe to the United States.An oil painting appeared recently that may be the original painting, but it has not been guaranteed by the agency possessing it.


== Popular usage ==
Although the original has disappeared, it has not hindered the great popularity and usage of the image. Copies of the original are frequently featured on holycards, portraits, and greeting cards. 
The following are several notable uses of the image:

The Madonna of the Streets is featured as a mosaic in Sts. Peter and Paul Church, San Francisco, California.
Members of the Sisters of Life receive a medal of the Madonna of the Streets at their first profession.


== See also ==
Madonna della Strada


== Footnotes ==


== External links ==
Madonnina Painting
The original painting was painted by Francisco Goya, the name 
is La Virgen de la Encarnacion