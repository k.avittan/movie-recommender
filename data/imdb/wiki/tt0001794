Pygmalion (; Ancient Greek: Πυγμαλίων Pugmalíōn, gen.: Πυγμαλίωνος) is a legendary figure of Cyprus in Greek mythology who was a king and a sculptor. Though Pygmalion is the Greek version of the Phoenician royal name Pumayyaton, he is most familiar from Ovid's narrative poem Metamorphoses, in which Pygmalion was a sculptor who fell in love with a statue he had carved.


== In Ovid ==
In book 10 of Ovid's Metamorphoses, Pygmalion was a Cypriot sculptor who carved a woman out of ivory. According to Ovid, after seeing the Propoetides prostituting themselves, Pygmalion declared that he was "not interested in women", but then found his statue was so beautiful and realistic that he fell in love with it.
In time, Aphrodite's festival day came, and Pygmalion made offerings at the altar of Aphrodite. There, too scared to admit his desire, he quietly wished for a bride who would be "the living likeness of my ivory girl." When he returned home, he kissed his ivory statue, and found that its lips felt warm. He kissed it again, and found that the ivory had lost its hardness. Aphrodite had granted Pygmalion's wish.
Pygmalion married the ivory sculpture which changed to a woman under Aphrodite's blessing. In Ovid's narrative, they had a daughter, Paphos, from whom the city's name is derived.
In some versions Paphos was a son, and they also had a daughter, Metharme.Ovid's mention of Paphos suggests that he was drawing on a more circumstantial account than the source for a passing mention of Pygmalion in Pseudo-Apollodorus' Bibliotheke, a Hellenic mythography of the 2nd-century AD. Perhaps he drew on the lost narrative by Philostephanus that was paraphrased by Clement of Alexandria. In the story of Dido, Pygmalion is an evil king.


== Parallels in Greek myth ==
The story of the breath of life in a statue has parallels in the examples of Daedalus, who used quicksilver to install a voice in his statues; of Hephaestus, who created automata for his workshop; of Talos, an artificial man of bronze; and (according to Hesiod) of Pandora, who was made from clay at the behest of Zeus.
The moral anecdote of the "Apega of Nabis", recounted by the historian Polybius, described a supposed mechanical simulacrum of the tyrant's wife, that crushed victims in her embrace.
The trope of a sculpture so lifelike that it seemed about to move was a commonplace with writers on works of art in antiquity. This trope was inherited by writers on art after the Renaissance.


== Reinterpretations ==
The basic Pygmalion story has been widely transmitted and re-presented in the arts through the centuries. At an unknown date, later authors give as the name of the statue that of the sea-nymph Galatea or Galathea. Goethe calls her Elise, based upon the variants in the story of Dido/Elissa.
A variant of this theme can also be seen in the story of Pinocchio, in which a wooden puppet is transformed into a "real boy", though in this case the puppet possesses sentience prior to its transformation; it is the puppet and not its creator, the woodcarver Geppetto, who beseeches the divine powers for the miracle.
In the final scene of William Shakespeare's The Winter's Tale, a statue of Queen Hermione which comes to life is revealed as Hermione herself, so bringing the play to a conclusion of reconciliations.
In George Bernard Shaw's 1913 play Pygmalion, a modern variant of the myth with a subtle hint of feminism, the underclass flower-girl Eliza Doolittle is metaphorically "brought to life" by a phonetics professor, Henry Higgins, who teaches her to refine her accent and conversation and otherwise conduct herself with upper-class manners in social situations. This play in turn inspired the 1938 film Pygmalion, as well as the 1956 play My Fair Lady and the 1964 film My Fair Lady.
The 2007 film Lars and the Real Girl tells the story of a man who purchases a doll and treats her as a real person in order to reconnect with the rest of the world.  Although she never comes to life, he believes she is real, and in doing so develops more connections to his community.  When he no longer needs her, he lets her go.  This is a reversal of the myth of Pygmalion.


=== Paintings ===

The story has been the subject of notable paintings by Agnolo Bronzino, Jean-Léon Gérôme (Pygmalion and Galatea), Honoré Daumier, Edward Burne-Jones (four major works from 1868–1870, then again in larger versions from 1875–1878 with the title Pygmalion and the Image), Auguste Rodin, Ernest Normand, Paul Delvaux, Francisco Goya, Franz von Stuck, François Boucher, and Thomas Rowlandson, among others. There have also been numerous sculptures of the "awakening".


=== Literature ===
Ovid's Pygmalion has inspired many works of literature, some of which are listed below. The popularity of the Pygmalion myth surged in the 19th century.


==== Poems ====


===== England =====
John Marston's "Pigmalion", in "The Argument of the Poem" and "The Authour in prayse of his precedent Poem" (1598)
John Dryden's poem "Pygmalion and the Statue" (1697–1700)
Thomas Lovell Beddoes's "Pygmalion, or the Cyprian Statuary" (1823–25)
William Cox Bennett's poem "Pygmalion" from his work Queen Eleanor's Vengeance and Other Poems (1856)
Arthur Henry Hallam's poem "Lines Spoken in the Character of Pygmalion" from his work Remains in verse and prose of Arthur Henry Hallam: With a preface and memoir (1863)
Robert Buchanan's poem "Pygmalion the Sculptor" in his work Undertones (1864)
William Morris's poem "Earthly Paradise" in which he includes the section "Pygmalion and the Image" (1868)
William Bell Scott's "Pygmalion"
Thomas Woolner's long poem "Pygmalion" (1881)
Frederick Tennyson's "Pygmalion" from Daphne and Other Poems (1891)
Robert Graves' "Pygmalion to Galatea" (1926) and "Galatea and Pygmalion"


===== Scotland =====
Andrew Lang's "The New Pygmalion or the Statue's Choice" (1911)
Carol Ann Duffy's poem "Pygmalion's Bride" (1999)


===== Ireland =====
Emily Henrietta Hickey's A Sculptor and Other Poems (1881)
Patrick Kavanagh's "Pygmalion" (1938)
Eiléan Ní Chuilleanáin's "Pygmalion's Image" (1991)


===== Germany =====
Friedrich Schiller's poem "The Ideals" (Die Ideale) (1795-6)


===== Romania =====
Nichita Stănescu's poem "Către Galateea" (Dreptul la timp) (1965)


===== United States =====
Sara Jane Lippincott (Grace Greenwood)'s "Pygmalion" (1851)
Elizabeth Stuart Phelps' "Galatea" from Harper's Weekly (1884)
Edward Rowland Sill's "The Lost Magic" (1900)
H.D.'s "Pygmalion" (1913–17)
Genevieve Taggard's "Galatea Again" (1929)
Katha Pollitt's "Pygmalion" (1979)
Joseph Brodsky's "Encore" (1983)
Katherine Solomon's "Galatea" (1999)


===== Nicaragua =====
Claribel Alegría's "Galatea Before the Mirror" (1993)


==== Short stories ====
Nathaniel Hawthorne's short story "The Birth-Mark" and his similar novella, Rappaccini's Daughter.
H.P. Lovecraft's "Herbert West–Reanimator"
Tommaso Landolfi's  "La moglie di Gogol" ('The Wife of Gogol')
John Updike's "Pygmalion"
E. T. A. Hoffmann's "The Sandman"
Jorge Luis Borges's "Las Ruinas Circulares" (Argentina)
Isaac Asimov's short story Galatea (in his collection Azazel is a parody of the story, where a woman sculptor sculpts her idea of the ideal man)
Madeline Miller's short story 'Galatea'


==== Novels and plays ====
William Hazlitt's Liber Amoris: or, the New Pygmalion (1823)
Lloyd C. Douglas's novel "Invitation To Live" (1940)
Richard Powers's novel Galatea 2.2
Amanda Filipacchi's novel Vapor
Edith Wharton's The House of Mirth
Henry James' The Portrait of a Lady (1880–81)
George MacDonald's Phantastes
George Bernard Shaw's play Pygmalion
Tawfiq el-Hakim's play Pygmalion
William Schwenck Gilbert's play Pygmalion and Galatea
Willy Russell's play Educating Rita
Rousseau's play Pygmalion, scène lyrique, the first full melodrama
Villiers de l'Isle-Adam's novel Tomorrow's Eve
Jacinto Grau's play El Señor de Pigmalión (1921)
William Shakespeare's play The Winter's Tale (1611)


==== Other ====
Pete Wentz's comic series Fall Out Toy Works
Grant Morrison's Professor Pyg, who appears in Batman and Robin
William Moulton Marston's origin of Wonder Woman was inspired by the myth of Pygmalion and Galatea, as she was sculpted by her mother Hippolyta from clay and given life by Aphrodite's breath. The sculpture represents the creative power of a mother's love for a child, passing some of her qualities on to her daughter.


=== Opera, ballet, and music ===
The story of Pygmalion is the subject of Jean-Philippe Rameau's 1748 opera, Pigmalion.
It was also the subject of Georg Benda's 1779 monodrama, Pygmalion.
Ramler's poem Pygmalion was set to music as an aria by J.C.F.Bach in 1772, and as a cantata by Friedrich Benda in 1784.
Pygmalion was the subject of Gaetano Donizetti's first opera, Il Pigmalione.
Fromental Halévy wrote an opera Pygmalion in the 1820s, but it was not performed.
Franz von Suppe composed an operetta Die schöne Galathée which is based on the characters of Pygmalion and Galatea.
The ballet Coppélia, about an inventor who makes a life-sized dancing doll, has strong echoes of Pygmalion.
The choreographer Marius Petipa and the composer Prince Nikita Trubetskoi created a four-act ballet on the subject called Pygmalion, ou La Statue de Chypre. The ballet was revived in 1895 with the great ballerina Pierina Legnani.
The English progressive rock group Yes composed  "Turn of the Century" (1977); it tells the story of the sculptor Roan who, in the grief of his wife's death, "molds his passion into clay."  The sculpture of his wife comes to life and they fall in love.
British shoegaze  band Slowdive named their third LP Pygmalion in 1995.
The song "Trial By Fire" by darkwave/gothic band ThouShaltNot recreates the idea of a modern-day Pygmalion with lyrics such as "I sculpt your nature within, I am your Pygmalion" and "I dust away the plaster from off your breathing body...You'll never be the same."
Lunatic Soul's 2014 album Walking on a Flashlight Beam includes the track "Pygmalion's Ladder".
The progressive house artist Hellberg (Jonathan Hellberg) released a song called 'The Girl' featuring vocalist Cozi Zuehlsdorff in 2015. They have both admitted to having been inspired by the Pygmalion myth when creating the track.
Wonderbound Ballet Company in Denver, Colorado premiered the piece "Patterns" in 2018 as part of their work Aphrodite's Switchboard. The piece centers on a reinterpretation of the Pygmalion story in which Aphrodite falls in love with Pygmalion's sculpture herself.
My Fair Lady stage musical


=== Stage plays ===

Though it is not based on the story of Pygmalion, Shakespeare's play Measure for Measure references Pygmalion in a line spoken by Lucio in Act 3, Scene 2: "What, is there none of Pygmalion's images, newly made woman, to be had now, for putting the hand in the pocket and extracting it clutch'd?"There have also been successful stage-plays based upon the work, such as W. S. Gilbert's Pygmalion and Galatea  (1871). It was revived twice, in 1884 and in 1888. The play was parodied the musical 1883 burlesque Galatea, or Pygmalion Reversed, which was performed at the Gaiety Theatre with a libretto by Henry Pottinger Stephens and W. Webster, and a score composed by Wilhelm Meyer Lutz.
In January, 1872, Ganymede and Galatea opened at the Gaiety Theatre.  This was a comic version of Franz von Suppé's Die schöne Galathee, coincidentally with Arthur Sullivan's brother, Fred Sullivan, in the cast.
In March 1872, William Brough's 1867 play Pygmalion; or, The Statue Fair was revived, and in May of that year, a visiting French company produced Victor Massé's Galathée.
George Bernard Shaw's Pygmalion (1912, staged 1913) owes something to both the Greek Pygmalion and the legend of "King Cophetua and the beggar maid"; in which a king lacks interest in women, but one day falls in love with a young beggar-girl, later educating her to be his queen.  Shaw's comedy of manners in turn was the basis for the Broadway musical My Fair Lady (1956), as well as numerous other adaptations.
P. L. Deshpande's play Ti Fulrani ("Queen of Flowers") is also based on Shaw's Pygmalion. The play was a huge success in Marathi theater and has earned many accolades. Madhu Rye adapted Pygmalion in Gujarati as Santu Rangili (1976) which was successful.


=== Television ===
The Man from U.N.C.L.E. 3rd-season episode "The Galatea Affair" from 1966 is a spoof of My Fair Lady. A crude barroom entertainer (Joan Collins) is taught to behave like a lady. Noel Harrison, son of Rex Harrison, star of the My Fair Lady film, is the guest star.
The Japanese anime series Bubblegum Crisis: Tokyo 2040 includes a character named Galatea, an artificial life form designed to be the next evolution of the human race.
In Disney's Hercules: The Animated Series, Pygmalion was Hercules' art teacher. His success in crafting a perfect wife for himself prompted Hercules to do the same to create a date for a school dance, naming her Galatea.
The science-fiction franchise Star Trek explores the Pygmalion theme in episodes such Star Trek: The Next Generation's "Inheritance" (Episode 7x10), where Data's creator Dr. Soong constructs a female android to replace his deceased wife, and Star Trek: The Original Series' "Requiem for Methuselah" (Episode 3x19), where an immortal human builds a presumably immortal android as a life partner. The character of Data, himself an android "sculpted" by man and his longing to become more "human," are a recurrent arc of the series.
The 1897 flashback of the Gothic TV serial Dark Shadows includes a painter, Charles Delaware Tate (portrayed by Roger Davis), whose portraits come to life. The character of Amanda Harris is one of Tate's creations, falling in love with Quentin Collins.


=== Films ===
Pygmalion (1935 film), a German film based on the George Bernard Shaw play
Pygmalion (1937 film), a Dutch film based on the George Bernard Shaw play
Pygmalion (1938 film), a British film starring Leslie Howard and Wendy Hiller
Pygmalion (1948 film), a British television film starring Margaret Lockwood
Surjo Konna, a Bangladeshi film based on Pygmalion starring Bulbul Ahmed
Pygmalion (1983 film), a television film starring Peter O'Toole and Margot Kidder
Alfred Hitchcock's Vertigo (1958) is a variation on the Pygmalion theme.
The comedy movie Mannequin (1987) is based on Pygmalion.
My Fair Lady (film), a 1964 American musical film, starring Rex Harrison and Audrey Hepburn.
Bicentennial Man (1999 film),starring Robin Williams and Sam Neill based on story by Isaac Asimov (female android named Galatea) 


=== Interactive fiction ===
The text adventure Galatea, by Emily Short, is based on the myth of Galatea.


=== Audio drama/podcasts ===
One of the protagonists of science fiction/horror podcast Dining In The Void is a model named Galatea Ivory, known for her white skin and unparalleled beauty. One of her main arcs in the show revolves around her physical beauty and how it gets compromised by the show's villain Jo. In episode six, "Aligning Their Goals," Galatea reveals that her manager is called Pygmalion and that he sometimes tells her to "be quiet" on set. However, she defends him when Aveline Lion asks if that's controlling by saying he's looking out for her career.


== See also ==
Agalmatophilia
Golem
Hidari Jingorō
Narcissus
Pinocchio
Prometheus
Pygmalion and the Image series
Pygmalion effect
Pygmalion of Tyre
Uncanny valley
Waifu


== Notes ==


== References ==


== Further reading ==
Essaka Joshua. (2001). Pygmalion and Galatea: The History of a Narrative in English Literature. Ashgate.
Kenneth Gross. (1992). The Dream of the Moving Statue. Cornell University Press. (A wide-ranging survey of 'living statues' in literature and the arts).
Jack Burnham. Beyond Modern Sculpture (1982). Allan Lane. (A history of 'living statues' and the fascination with automata - see the introductory chapter: "Sculpture and Automata").
Ernst Buschor. Vom Sinn der griechischen Standbilder (1942). (Clear discussion of attitudes to sculptural images in classical times).
John J. Ciofalo. "The Art of Sex and Violence - The Sex and Violence of Art." The Self-Portraits of Francisco Goya. Cambridge University Press, 2001.
John J. Ciofalo. "Unveiling Goya's Rape of Galatea." Art History (December 1995), pp. 477–98.
Gail Marshall. (1998). Actresses on the Victorian Stage: Feminine Performance and the Galatea Myth. Cambridge University Press.
Alexandra K. Wettlaufer.  (2001). Pen Vs. Paintbrush: Girodet, Balzac, and the Myth of Pygmalion in Post-Revolutionary France. Palgrave Macmillan.
Danahay, Martin A. (1994) "Mirrors of Masculine Desire: Narcissus and Pygmalion in Victorian Representation". Victorian Poetry, No. 32, 1994: pages 35–53.
Edward A. Shanken. (2005) "https://web.archive.org/web/20060622174528/http://artexetra.com/Hot2Bot.pdf Hot 2 Bot:  Pygmalion's Lust, the Maharal's Fear, and the Cyborg Future of Art]", Technoetic Arts 3:1: 43-55.
(2005). Almost Human: Puppets, Dolls and Robots in Contemporary Art, Hunterdon Museum of Art, Clinton, New Jersey. (Catalogue for a group exhibition March 20 - June 12, 2005)
Morford, Mark. (2007). "Classical Mythology Eighth Edition". Oxford University Press
Hersey, George L (2009). "Falling in love with statues: artificial humans from Pygmalion to the present", Chicago, 2009, ISBN 978-0-226-32779-2
Law, Helen H. (1932). "The Name Galatea in the Pygmalion Myth", The Classical Journal, Vol. 27 No. 5 (Feb. 1932), published by The Classical Association of the Middle West and South, JSTOR 3290617
d'Huy, Julien. (2012). Le motif de Pygmalion: origine afrasienne et diffusion en Afrique.. Sahara. 23. pp. 49-58.
d'Huy, Julien. (2013). Il y a plus de 2000 ans, le mythe de Pygmalion existait en Afrique du nord.. Préhistoires Méditerranéennes.


== External links ==
English translation of Ovid's poem
English translation of Ovid's poem
Latin original, lines 243–297, at The Latin Library.com
Shakespeare reference