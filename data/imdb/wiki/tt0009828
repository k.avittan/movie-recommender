Between Friends is a 1924 American silent melodrama film based on the eponymous 1914 novel by  Robert W. Chambers. The film was directed by J. Stuart Blackton and produced by Albert E. Smith. It stars Lou Tellegen, Anna Q. Nilsson, and Norman Kerry. The feature was distributed by Vitagraph Studios, which was founded by Blackton and Smith in 1897 in Brooklyn, New York. The film is lost.


== Synopsis ==
Jack Greylock runs away with the wife of his lifelong friend, David Drene, a sculptor. Drene only knows that his wife has fled; he does not know the identity of the man with whom she went. The wife kills herself from remorse and Greylock returns to keep up the semblance of his ancient friendship with the sculptor, who buries his sorrow in his work. Cecile Waite, a model, poses for Drene and Greylock, upon meeting her, falls honestly in love with her. She, however, loves Drene, who is apparently oblivious. Drene learns through another friend that Greylock was responsible for his wife's downfall. He first intends to kill him, but perceiving Greylock's love for the model, resolves to work his vengeance through Cecile. Drene threatens her. Greylock pleads for mercy for the girl and eventually agrees to kill himself at midnight Christmas Eve if Drene will spare her. Drene's brooding overturns his reason for a time and after a night of delirium, he collapses. Cecile finds him and nurses him back to health. The sculptor regains consciousness on Christmas Eve, his disposition improved. He sends Cecile to tell Greylock of his changed mood, but she cannot get into the house where Greylock is slowly preparing to keep his pledge. Drene then telepathically draws Greylock to his home and absolves him of his promise.

American Silent Horror, Science Fiction and Fantasy Feature Films


== Cast ==
Lou Tellegen as David Drene
Anna Q. Nilsson as Jessica Drene
Norman Kerry as Jack Greylock
Alice Calhoun as Cecile White
Stuart Holmes as Quair
Henry Barrows as Guilder
Richard Billings as Little Boy (uncredited)


== Background ==

The film adaption was written and credited to screenwriter and director Charles Gaskill. However, in her book J. Stuart Blackton: A Personal Biography by His Daughter (June 1985), Marian Blackton Trimble writes that her father was unhappy with Gaskill's adaption of Chamber's novel, saying: "The script proved too involved, too scholarly, top heavy with long, platitudinous titles, the cardinal sin of the silent movie, and soggy with prolonged, unclimaxed scenes" (p. 150). Trimble says in her book that while en route by train from New York to Los Angeles, she penned a script that was acceptable to her father and actor Lou Tellegen as well, but Trimble received no credit for her work. Trimble went on to receive credit for other features under the pseudonym Marian Constance, mainly in films where she was working with her father. In a review of the film in The Film Daily, dated April 20, 1924, a list of credits for the film include Marion Constance along with Chas. L Gaskill for scenario. Trimble also states in her book (p. 152) that her father "was one of the first, if not the first", to utilize the technique of montage shots in his films.When the film played at the Forum Theatre in Los Angeles, the silent film The Haunted Hotel (1907), was shown in connection with Blackton's film. The showing of the old film proved to be a hit and other exhibitors started requesting early-day pictures from Vitagraph as well. The Exhibitors Herald reported in 1924 that Blackton had purportedly "invented a new technical arrangement of lights by which he has procured effects in photography which never before have been shown on the screen...the experiments were conducted in Los Angeles and used for the first time in the studio set of Between Friends, where a model poses for a statue".


=== Other adaptions ===
In February 1918, Vitagraph released The Woman Between Friends, a silent film which featured Robert Walker, Edith Speare and Marc McDermott. During filming, Walker accidentally shot at himself, but wasn't injured in the mishap.


== Reviews and reception ==
When the film premiered at the Rivoli Theater in New York, local newspapers were mixed in their reviews. The New York Times review said "there are black cats, apples handed to the hero by the pretty model and an utterly overdone bit of suspense toward the end of the picture, together with scenic long-distance hypnotism". The American considered the film "the best thing J. Stuart Blackton has made in a long, long time". The Daily News was harsh in their criticism of the feature saying, "just as the characters in this Robert W. Chambers story were cheap and unreal, so are they in the film".  The Morning Telegraph praised the film stating that "it is many a day since Mr. Blackton has made a better picture, not only has he handled a strong story with intelligent dignity and repression, but he has skillfully avoided an interpretation which easily could have made it objectionable to the censors and the censorious". A reviewer for the New York Evening Post blasted the film saying, "How anything so frightfully poor, so hilariously ridiculous and generally terrible could ever be screened at all, much less shown at a Broadway house, passeth comprehension...it's so thoroughly atrocious that the problem of trying to review it staggers the mind of ye reviewer".Henriette Sloane gave the film a positive review in the Exhibitors Trade Review, stating that it was "armed with an unusually strong cast of noted stars...and was a very satisfactory and decidedly entertaining photoplay which should provide the average audience with a very pleasant matinee or evening". She also noted that the "story is alive with romance, thrills and suspense". A review in The Film Daily was also positive stating the film had "some new twists in this latest eternal triangle plot and there's a splendid production and fine cast that also help to make the picture interesting".


== See also ==
Lost film
List of lost films


== References ==


== External links ==
Between Friends (1924) on IMDb
Between Friends at the American Film Institute Catalog
Between Friends at the TCM Movie Database
Synopsis at AllMovie