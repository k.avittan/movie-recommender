Bumping Into Broadway is a 1919 American short comedy film featuring Harold Lloyd. A print of the film survives in the film archive of the UCLA Film and Television Archive. This film is notable as Lloyd's first two-reeler featuring his "glasses" character.


== Plot ==
The film opens with a quick glimpse into the glamorous life of Broadway and the hubris often associated with its players. The film then shifts to the story of "The Girl" and "The Boy," she an aspiring actress and he an unpublished playwright. They are both humble artists struggling to make it big, and each are behind in their rent at a boarding house run by a stern landlady and a large, thuggish "bouncer." Having romantic feelings for the girl, the boy gives her all of his money so she can pay the back rent. Now penniless, the boy must find different ways to elude the landlady and bouncer. He finally escapes the menacing duo by hopping into a moving car.
Later, the eager playwright sneaks into the theater where the girl works a chorus girl to try and sell his play to the manager. He is unsuccessful, and after being kicked out of the manager's office, he's physically thrown into the street. Meanwhile, the girl has been fired from the show, and as a consolation, accepts an offer from the handsome "Stage-door Johnnie" to accompany him to a posh nightclub.
The couple, followed by the boy, arrive at the Sky Limit Club, an underground gambling establishment. While searching for the girl inside the club, the boy accidentally starts winning at roulette when he unwittingly places some found money on the table. Just as he bankrupts the casino, the place is raided by the police. After a series of chases and clever maneuvers, the boy is able to evade the police and is reunited with the girl. The film ends with the two engaged in a romantic kiss.


== Cast ==


== Background ==
Bumping Into Broadway was the first of a nine two-reel picture deal Harold Lloyd made with Pathé Exchange in April 1919. And even though Pathé was eager to release his first two-reel comedy featuring the "glasses" character, the company wanted to release all of the remaining one-reelers first.When the film was finally released in November 1919, it was met with great fanfare, partially due to the recent publicity surrounding its star after a prop bomb exploded during a photo shoot, damaging his eyes and severing his right thumb and forefinger. Miraculously, Lloyd managed to fully regain his eyesight and was able to attend the premiere of Bumping Into Broadway in New York City. In the end, the picture not only received great reviews, it broke several house records in theaters across the country, solidifying Lloyd's place as a top-billing comedy star.


== See also ==
List of American films of 1919
Harold Lloyd filmography


== References ==


== External links ==
Bumping Into Broadway on IMDb