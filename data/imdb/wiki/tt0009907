"The Battler" is a short story written by Ernest Hemingway, published in the 1925 New York edition of In Our Time,  by Boni & Liveright. The story is the fifth in the collection to feature Nick Adams, Hemingway’s autobiographical alter ego.


== Plot ==
"The Battler" begins as Nick Adams is thrown off a train, caught as a stowaway. Nick stumbles into the forest, to make his way to the next town, where he sees a fire in the darkness. A man next to the fire greets him, asking how he got the black eye. Nick answers that he was punched off a train. The man says should hit the man on the train with a rock the next time he passes. Nick replies, "I’ll get him". 
When the man compliments Nick on his toughness, he replies that "You got to be tough." After this, Nick realizes that the man’s face is misshapen with a smashed nose, permanently swollen lips, and one missing ear. The man then asks Nick whether he has ever been crazy and admits that "I’m not quite right" followed by "I’m crazy". 
Nick starts to feel uncomfortable and considers leaving until the man reveals himself to be Ad Francis, a former boxing champion. Later, Ad's Negro friend Bugs comes back to the camp with their dinner. While preparing dinner, Ad asks Nick for a knife, and Bugs tells Nick to not hand it to Ad. Ad is greatly offended and becomes very aggressive, saying, "You come in here where nobody asks you and eat a man’s food and when he asks to borrow a knife you get snotty." He threatens to hit Nick, but Bugs knocks him out with a blackjack. 
Nick asks how Ad became so crazy, and Bugs explains that Ad took too many beatings in the ring and that his wife–who the media falsely claimed to be his sister–abandoned him. Shortly after, Ad became depressed and could no longer financially support himself. 
After this revelation, Nick leaves the camp and heads towards the next town.


== Characterisation of Negro character ==
Bugs, the Negro character, is referred to throughout the story via the pejorative term "nigger". This racist terminology is used casually in a third-person limited narration, which seems to reflect the perspective of Nick Adams. At the same time, Bugs' actions and speech indirectly reveal that he is generous, intelligent, and the character most in control of the situation at hand.


== References ==


== Sources ==
Oliver, Charles. (1999). Ernest Hemingway A to Z: The Essential Reference to the Life and Work. New York: Checkmark Publishing. ISBN 978-0-8160-3467-3
Tetlow, Wendolyn E. (1992). Hemingway's "In Our Time": Lyrical Dimensions. Cranbury NJ: Associated University Presses. ISBN 978-0-8387-5219-7


== External links ==
Ernest Hemingway Collection, JFK Library