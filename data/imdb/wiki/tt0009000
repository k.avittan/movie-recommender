Debt of Honor is a techno-thriller novel, written by Tom Clancy and released on August 17, 1994. A direct sequel to The Sum of All Fears (1991), Jack Ryan becomes the National Security Advisor when a secret cabal of Japanese industrialists seize control of their country’s government and wage war on the United States. The book debuted at number one on The New York Times bestseller list. The novel was later noted as containing plot elements which were similar to the circumstances of the September 11, 2001, terrorist attacks and the hijacking of United Airlines Flight 93.


== Plot summary ==
Japanese industrialist Raizo Yamata has been plotting to bring back his country to a position of greatness for years, partly as revenge for the death of his family at the hands of American forces invading the island of Saipan during World War II. His opportunity comes when a car accident in eastern Tennessee, caused by faulty gas tanks made in Japan, results in the deaths of six American people. The incident leads to the swift passage of a law allowing the U.S. to mirror trade practices of the countries from which it imports goods, cutting off the American export markets upon which the Japanese economy depends. Facing an economic crisis, Japan’s ruling zaibatsu, led by Yamata, decides to take economic and military action against the United States. Along with covert support from China and India, they plot to curtail the U.S. presence in the Pacific and re-establish the Greater East Asia Co-Prosperity Sphere. After the Japanese Prime Minister Mogataru Koga resigns in disgrace due to the economic situation, the zaibatsu installs Hiroshi Goto, an aggressive nationalist and critic of the U.S., to succeed him. Meanwhile, Japan has covertly developed nuclear weapons, and with SS-18 designs bought from the former Soviet Union, has fabricated and deployed several ICBMs.
Japan launches the first phase of its assault, sending Self-Defense Force units to occupy the Mariana Islands, specifically Saipan and Guam, without casualties. However, during a joint military exercise, Japanese ships "accidentally" launch torpedoes at the U.S. Pacific Fleet, destroying two submarines and crippling two aircraft carriers. An immediate retaliation is forestalled by the second phase of the Japanese offensive: an economic attack, where Japan engineers the collapse of the U.S. stock market by hiring a programmer who is a consultant for an exchange firm to insert a logic bomb into the system, which when triggered blocks the storage of all trade records made after noon on Friday. They also assassinate the chairman of the Federal Reserve. The Japanese government then immediately sue for peace, offering international talks and seemingly free elections in the Marianas to delay a U.S. response.
Meanwhile, Jack Ryan is pulled out of retirement from government service by U.S. President Roger Durling, who appoints him as National Security Advisor. Despite his typical focus on military issues, he advises the president to deal with the economic crisis first, realizing that Japan's deletion of trade records could be an advantage in responding to the economic threat. He engineers a "do-over", where all of the transactions that were deleted on the day of the mass deletion are ignored and all trade information is restored to its condition at noon of that day. The U.S. stock market is successfully restored with only minor disruption, and a group of U.S. investment banks start a massive economic unloading of Japanese investment products, effectively eliminating any gains made by the zaibatsu.
The United States military then proceeds to launch a counterattack on their Japanese counterparts using whatever resources they have. In a staged accident, CIA operatives John Clark and Domingo Chavez blind two incoming Japanese E-767 pilots with a Dazzler that causes them to crash upon landing. The U.S. Air Force then proceeds to eliminate the rest of Japan's AWACS system through low-profile military attacks using widely dispersed U.S. assets, allowing B-2 bombers to destroy the hidden ICBM silos. They later use an attack by stealthy F-22 fighters to further damage Japan's air defenses. An Army special operations team is airdropped into Japan to support covertly inserted Comanche helicopters.  One helicopter is used to attack another AWACS plane with air-to-air missiles while several others use Hellfire missiles to kill members of Yamata's cabal. Meanwhile, Admiral Robby Jackson liberates the Marianas with few casualties by using a combination of cruise missiles and carrier air attacks to severely damage the Japanese aircraft stationed on the islands, which forces the Japanese commander to surrender his troops.
Outmaneuvered and cornered by the United States's military and economic response, Goto resigns, ceding power to his predecessor Koga, who was rescued earlier by Clark and Chavez from Yamata. Yamata and his surviving conspirators are arrested for treason, and the new Japanese government accepts the generous U.S. offer of status quo ante.
Throughout the book, President Durling faces another political crisis: Vice President Ed Kealty is forced to resign after being accused of rape. With the crisis over, President Durling nominates Ryan as vice president for successfully handling the crisis. However, an embittered Japan Air Lines pilot, driven mad by the deaths of his son and brother during the conflict, flies his Boeing 747 directly into the U.S. Capitol during a special joint session of Congress. The president, as well as nearly the entire Congress, the Supreme Court, and many other members of the federal government are killed in the attack. Ryan, who is on his way to be sworn as vice president after being confirmed, narrowly escapes the explosion. He becomes the President of the United States and takes his oath of office before a district judge in the CNN studios in Washington.


== Characters ==


=== The United States government ===
Jack Ryan: National Security Advisor to President Durling. Previously worked at the CIA. Later appointed as Vice President of the United States for successfully handling the crisis between the United States and Japan, as well as to serve as Pres. Durling's running mate for the upcoming presidential elections. However, he becomes President after his superior was killed in the attack on the United States Capitol.
Roger Durling: President of the United States. Succeeded J. Robert Fowler after his resignation in the previous novel. Dies in the Capitol attack.
Edward Jonathan Kealty: Vice President of the United States who later resigns from his post after it was publicly revealed that he was under FBI investigation for sexual misconduct.
Brett Hanson: Secretary of State. Dies in the Capitol attack.
Bosley "Buzz" Fiedler: Secretary of the Treasury. Dies in the Capitol attack.
Scott Adler: Deputy Secretary of State who later handles the negotiations with the Japanese over trying to recover the Mariana Islands by diplomatic means.
Christopher Cook: Deputy assistant secretary of state who unwittingly passes secret information to Japan in an effort to influence the diplomatic negotiations between the two countries.
Dan Murray: Deputy assistant director of the Federal Bureau of Investigation and lead investigator in the criminal case against Kealty
Alan Trent: Chairman of the House Select Committee on Intelligence. A vocal critic of the Japanese, he becomes instrumental in the swift passage of the Trade Reform Act, which would later cause war between the United States and Japan.


=== The Central Intelligence Agency ===
John Clark: Operations officer
Domingo "Ding" Chavez: Operations officer
Mary Pat Foley: Deputy Director (Operations)
Chet Nomuri: Field officer of Japanese descent working in Japan


=== The United States military ===
Bart Mancuso: Rear admiral and commander of submarine forces in the Pacific Fleet (COMSUBPAC)
Robby Jackson: Rear admiral (lower half) working as Deputy J-3 in the Joint Chiefs of Staff, later Commander of Task Force 77 (CTF-77) during the retaking of the Marianas
Rear Admiral Mike Dubro: Commander of the USS Dwight D. Eisenhower and USS Abraham Lincoln carrier battle groups in the Indian Ocean
Rafael "Bud" Sanchez: Commander of the Carrier Air Group (CAG) on USS John C. Stennis
Lieutenant Commander Wally Martin "Dutch" Claggett: Commander of the USS Tennessee, an Ohio-class submarine
Sandy Richter: Chief Warrant Officer 4 and one of the test pilots for the U.S. Army Comanche helicopter program that later becomes central to the plan to retake the Marianas
Captain Diego Checa: Leader of a squad of United States Army Rangers who establish a mountaintop helicopter base in Japan
Julio Vega: Senior non-commissioned officer in the squad of Rangers assigned to Japan


=== Japan ===
Raizo Yamata: Chairman of a large conglomerate in Japan and leader of the zaibatsu. He is the sole architect behind the war between the U.S. and Japan, which is motivated by revenge for the suicide of his family on Banzai Cliff in Saipan in an effort to evade capture by American forces during World War II.
Hiroshi Goto: Japanese politician who is aggressive towards the U.S. Controlled by Yamata, he becomes prime minister of Japan for most of the war between his country and the U.S.
Mogataru Koga: Prime minister of Japan who resigns in disgrace due to his country's economic situation, which paves the way for Goto's ascension into power. He becomes a bargaining chip in the diplomatic negotiations, as he is a potential opposition leader to Goto, but he is kidnapped by Yamata in an effort to silence him. After being rescued by Clark and Chavez from Yamata's men, he resumes his position as Japan's leader at the conclusion of the hostilities.
Rear Admiral Yusuo Sato: JMDSF fleet commander associated with Yamata. Later dies after Tennessee sinks his destroyer Mutsu with torpedoes.
Seiji Nagumo: Japanese intelligence officer covered as an embassy official, who is Cook's de facto case officer.
Torajiro Sato: A captain flying 747s for Japan Air Lines. His brother Yusuo and his son Major Shiro, who is an F-15 Eagle pilot, are killed in the military conflict. Embittered by his ordeal, he exacts revenge by crashing his aircraft into a joint session of Congress in the U.S. Capitol.


=== Other characters ===
V. K. Chandraskatta: Fleet commander in the Indian Navy who is allied with Yamata. He is in command of the Indian carrier battle group that tries to provoke Admiral Dubro in the Indian Ocean, who are stationed there to prevent India from invading the neighboring country of Sri Lanka and annex it.
Zhang Han San: Chinese career diplomat allied with Yamata
George Winston: Founder of the Columbus Group, a mutual funds group. He initially sells his company to Yamata, but after the stock market crash, he retakes control of his company after an emergency board meeting, and finds out that the Japanese businessman had caused the incident.
Mark Gant: Senior executive of the Columbus Group and a trusted aide to Winston
Sergey Golovko: Chairman of the Russian Foreign Intelligence Service (SVR)
Major Boris Il'ych Scherenko: Deputy rezident of Station Tokyo for the SVR
Oleg Lyalin: Former KGB "illegal" who is now an instructor at the Defense Language Institute, based in Presidio of Monterey, California. He tells Clark and Chavez about THISTLE, his well-placed network of agents in Japan, and how to reactivate it.
Portagee Oreza: Former Master Chief Quartermaster for the United States Coast Guard who lives with his wife in Saipan. He feeds information about the Japanese presence in the island to the National Military Command Center (NMCC) through Admiral Jackson, and later has a reunion with Clark, whom he knew as John Kelly about twenty years ago in previous novel Without Remorse.
Kimberly Norton: American national who is Goto's mistress. Clark and Chavez are sent to Japan to contact Norton and offer her a free ticket home, but she is killed by Yamata's men after Goto becomes prime minister since she is deemed to be a security risk.
Barbara Linders: A former aide to Vice President Kealty who was raped by her boss three years prior
Cathy Ryan: Associate professor with Johns Hopkins Hospital, Jack Ryan's wife


== Themes ==
Debt of Honor was released during a time of American resentment towards the Japanese, which was further popularized by the publication of Michael Crichton's crime thriller novel Rising Sun in 1992. In this case, Clancy avoided "Japan bashing" that is evident in Crichton's work, and instead presented a balanced picture of Japanese society, its government, business practices, and values. In the novel, he also discussed currency trading, governmental corruption, car manufacturing and distribution, and power politics in the country.The novel also reinforces Clancy's belief that the recent downsizing of the military establishment after the Cold War "has so depleted our military resources that the country is vulnerable to aggression that can arise anywhere, anytime", according to Publishers Weekly's review of the book.


== Reception ==
The novel received positive reviews. Publishers Weekly praised Clancy, who "spins out story threads in a rich but bewildering tangle of plot and setting, then vigorously weaves them together. Here, the heart-stopping climax is unexpected, but oddly appropriate." However, Christopher Buckley, in a review for The New York Times, called the book a "herniating experience" and criticized its "racist" depictions of Japanese characters.


== Legacy ==
In later years, the novel was noted for its similarity to the circumstances surrounding United Airlines Flight 93, especially regarding its climax, where an embittered Japanese pilot crashes his 747 on a joint session of Congress in the Capitol. While researching for the novel's ending, Clancy consulted an Air Force officer and described his reaction: "I ran this idea past him and all of a sudden this guy's eyeballing me rather closely and I said, 'Come on General, I know you must have looked at this before, you've got to have a plan for it.'  And the guy goes, 'Mr. Clancy, to the best of my knowledge, if we had a plan to deal with this, it would be secret, I wouldn't be able to talk to you about it, but to the best of my knowledge we've never looked at this possibility before.'"On April 1995, United States senator Sam Nunn outlined a scenario similar to the novel's ending, in which terrorists attack the Capitol on the night of a State of the Union address by crashing a radio-controlled airplane filled with chemical weapons into it. Nunn concluded that the scenario is "not far-fetched" and that the required technology is readily available. However, the 9/11 Commission Report revealed that national security officials did not consider the possibility: “[Counterterror official] Richard Clarke told us that he was concerned about the danger posed by aircraft in the context of protecting the Atlanta Olympics of 1996, the White House complex, and the 2001 G-8 summit in Genoa. But he attributed his awareness more to Tom Clancy novels than to warnings from the intelligence community.”In the aftermath of the attacks, Clancy was called into CNN and commented on the similarity between a plane crash depicted in the novel and the crash of United Flight 93. CNN anchor Judy Woodruff later remarked: "People in our newsroom have been saying today that what is happening is like right out of a Tom Clancy novel."


== References ==


== Further reading ==

Gallagher, Mark. Action figures: Men, action films, and contemporary adventure narratives (Springer, 2006).
Griffin, Benjamin. "The good guys win: Ronald Reagan, Tom Clancy, and the transformation of national security" (MA thesis , U of Texas, 2015).  online
Hixson, Walter L. "Red Storm Rising: Tom Clancy Novels and the Cult of National Security." Diplomatic History 17.4 (1993): 599-614.
Outlaw, Leroy B. "Red Storm Rising-A Primer for a Future Conventional War in Central Europe"" (Army War College, 1988). online
Payne, Matthew Thomas. Playing war: Military video games after 9/11 (NYU Press, 2016).
Terdoslavich, William. The Jack Ryan Agenda: Policy and Politics in the Novels of Tom Clancy: An Unauthorized Analysis (Macmillan, 2005). excerpt