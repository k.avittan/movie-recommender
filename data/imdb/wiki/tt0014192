The Leavenworth Case (1878), subtitled A Lawyer's Story, is an American detective novel and the first novel by Anna Katharine Green.  Set in New York City, it concerns the murder of a retired merchant, Horatio Leavenworth, in his New York mansion. The popular novel introduced the detective Ebenezer Gryce, and was influential in the development of the detective novel.  In her autobiography, Agatha Christie cited it as an influence on her own fiction.


== Plot Summary ==
The novel begins when a wealthy retired merchant named Horatio Leavenworth is shot and killed in his library. When investigator Ebenezer Gryce and lawyer Everett Raymond look into the case, it is revealed that no one could have left the Manhattan Mansion before the body was discovered the next day. As the story progresses, Leavenworth's orphaned nieces Mary and Eleanore, Hannah the maid, and a mysterious gentleman who appears on the scene all factor into the investigation.


== Reception ==
The Leavenworth Case was an immediate bestseller, making Green famous. It was popular both in America and in Europe, and according to one critic, it was popular among different age ranges and genders. The novel seems to have accrued generally positive reviews. Green's New York Times obituary calls The Leavenworth Case her most famous novel, and claimed after the author's death in 1935 that many copies were sold and that it was still popular. Despite its initial popularity, however, The Leavenworth Case (and the rest of Green's work) is largely forgotten today.In order to publish the novel, Green had to write secretly and then read what she had written to Rossiter Johnson who then notified George Putnam to publish. The book was an instant success, and was so popular that it began to be pirated in England. Three different publishers (Alexander Strahan, Ward, Lock & Co. and George Routledge & Sons) pirated the book in 1884, and this continued to be a problem throughout Green's career.


== Major Themes ==
The Leavenworth Case is a detective novel in terms of genre. Its major themes are in some cases typical of the genre and in others innovative. In writing this book, Green began, notably, what would become one of the first detective series. This is thematically significant and sets the novel apart from previous works in the genre that most often were stand-alone; Green instead extended the themes and other elements of the novel, such as its detective and his methods, across a series of novels. While thematically and structurally the novel is typical of the genre in some regards in terms of being a “whodunit,” Green's characterization of Detective Gryce adds additional intrigue, setting up the series of Gryce novels that would follow.The atmospheric and suspenseful aspects of the novel make it notable. Green used her familiarity with criminal and legal matters to create a novel that is characterized by technical accuracy and realistic procedural details. Green's use in the novel of aspects that include a coroner's inquest, expert testimony, scientific ballistic evidence, a schematic drawing of the crime scene, a reconstructed letter, and the first suspicious butler “anticipates many of the features used by subsequent mystery novels." Further, her innovative techniques and creative thematic elements were demonstrative of what would become standard elements in the detective novel. These aspects include: “a murder in a library, a narrator who is an assistant to the detective, newspaper accounts of the case, wills and a large inheritance, a second murder that heightens the mystery, and a final confrontation scene that prompts a confession." Certainly, the major themes of the novel and how Green presents them situate it as an influential text within the genre.


== Significance ==
The Leavenworth Case is a significant work of nineteenth-century American detective fiction. The publication and subsequent success of Green's text marked the entry of a female author into the predominantly male literary genre . In addition to the conventions of detective fiction, Green's novel has some aspects of sentimental literature.  Green's investigator, Ebeneezer Gryce, was introduced nine years prior to the publication of Sir Arthur Conan Doyle's Sherlock Holmes stories . By locating the action of her stories in America, Green distinguished herself from earlier authors of detective fiction who had set their works in Europe . 


== Adaptations ==
Green adapted the novel into a play first performed in 1891.  A later revival of the play starred Green's husband Charles Rohlfs.The story was filmed in movies of the same name in 1923 and 1936.


== References ==


== External links ==
Text of The Leavenworth Case provided by Project Gutenberg