An officer of two-star rank is a senior commander in many of the armed services holding a rank described by the NATO code of OF-7. The term is also used by some armed forces which are not NATO members. Typically, two-star officers hold the rank of rear admiral, counter admiral, major general, or in the case of those air forces with a separate rank structure, air vice-marshal.


== Australian two-star ranks ==

In the Australian Defence Force the following ranks of commissioned officers are awarded two-star ranks:

Rear admiral (Royal Australian Navy two-star rank)
Major general (Australian Army two-star rank)
Air vice-marshal (Royal Australian Air Force two-star rank)


== Brazil two-star ranks ==

General de Brigada (Brazilian Army two-star rank)
Contra Almirante (Brazilian Navy two-star rank)
Brigadeiro (Brazilian Air Force two-star rank)The two-star rank in Brazil is the first rank in a general career. The officers in this position are normally brigade commanders.


== Canadian two-maple-leaves ranks ==
Rear-admiral (French: contre-amiral; Royal Canadian Navy two-star-equivalent rank)
Major general (French: major-général; Canadian Army and Royal Canadian Air Force two-star-equivalent rank)Rather than stars, the Canadian Forces insignia use maple leaves. The maple leaves appear with St. Edward's crown and crossed sabre and baton. Before unification, air vice marshal was the two-star rank for the RCAF.


== Germany ==
The equivalent modern German two-star ranks (OF-7) of the Bundeswehr are as follows:

Generalmajor and Konteradmiral
Generalstabsarzt and AdmiralstabsarztNot to be confused with Generalmajor and Vizeadmiral (one-star ranks; OF-6) of the Wehrmacht until 1945 and of the National People's Army of East Germany until German reunification in 1990.


== Indian two-star ranks ==

Air vice-marshal (Indian Air Force two-star rank)
Major-general (Indian Army two-star rank)
Rear admiral (Indian Navy two-star rank)
Inspector-general (Indian Police Service two-star rank)


== Indonesian two-star ranks ==
Major Jendral (Major General) - Indonesian Army and Indonesian Marine Corps two-star rank
Laksamana Muda (Rear Admiral) - Indonesian Navy and Indonesian Maritime Security Agency two-star rank
Marsekal Muda (Air vice marshal) - Indonesian Air Force two-star rank
Inspektur Jenderal (Inspector-general of police) - Indonesian National Police two-star rank


== Pakistani two-star ranks ==
Major-general (Pakistan Army two-star rank)
Air vice-marshal (Pakistan Air Force two-star rank)
Rear admiral (Pakistan Navy two-star rank)
Additional inspector general of police (Pakistan Police two-star rank)
Inspector General of Prisons, (Punjab Prisons two-star rank)


== Bangladesh two-star ranks ==
Additional inspector general of police (Bangladesh Police two-star rank)
Air vice-marshal (Bangladesh Air Force two-star rank)
Major-general (Bangladesh Army two-star rank)
Rear admiral (Bangladesh Navy two-star rank)


== Philippine two-star ranks ==
Major General (Philippine Army two-star rank)
Major General (Philippine Air Force two-star rank)
Rear Admiral (Philippine Navy two-star rank)
Rear Admiral (Philippine Coast Guard two-star rank)
Police Major General (Philippine National Police two-star rank)
Fire Director (Bureau of Fire Protection two-star rank)
Jail Director (Bureau of Jail Management and Penology two-star rank)


== United Kingdom two-star ranks ==

Rear admiral (Royal Navy two-star rank)
Major general (British Army and Royal Marines two-star rank)
Air vice marshal (Royal Air Force two-star rank)


== United States two-star ranks ==
Rear admiral (United States Navy, Coast Guard, Public Health Service Commissioned Corps and National Oceanic and Atmospheric Administration Commissioned Corps two-star rank)
Major general (United States Army, Marine Corps and Air Force two-star rank)


== USSR / Russian Federation ==
The introduction of general ranks in the USSR took place in 1940. The lowest general rank, Major-General, had two stars on the buttonholes. With the introduction of the new insignia in 1942, the two-star general becomes a lieutenant-general (major-general began to wear one star on shoulder straps).
In the Russian and Soviet armies, the rank wearing two stars is lieutenant-general (Russian: Генерал-лейтенант), however the general in charge of a unit equivalent to the one led by a NATO two-star general (a division) is major-general (генерал-майор). This also applies to the air force, MVD, police, FSB and some others, and is caused by a Russian brigades being commanded by colonel, with the smallest unit commanded by a general being a division. In the navy, the equivalent rank is kontr-admiral (контр-адмирал).


== See also ==
Ranks and insignia of NATO
Three-star rank
One-star rank


== References ==