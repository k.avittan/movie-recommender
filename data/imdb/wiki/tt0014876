Fast & Furious (originally The Fast and the Furious) is a media franchise centered on a series of action films that are largely concerned with illegal street racing, heists and spies. The franchise also includes short films, a television series, live shows, video games and theme park attractions. It is distributed by Universal Pictures.
The first film was released in 2001, which began the original trilogy of films focused on illegal street racing, and culminated in the  film The Fast and the Furious: Tokyo Drift (2006).  The series went under a change with Fast & Furious (2009), which transitioned the series toward heists and spying, and was followed by four sequels. F9 is set to be released in 2021, with a tenth and eleventh film planned. The main films are collectively known as The Fast Saga.
Universal expanded the series to include the spin-off film Fast & Furious Presents: Hobbs & Shaw (2019), while its subsidiary DreamWorks Animation followed this with the animated web television series Fast & Furious Spy Racers. Soundtrack albums have been released for all the films, as well as compilation albums containing existing music heard in the films. Two short films that tie into the series have also been released.
The series has been commercially successful and is Universal's biggest franchise, and ranks as the tenth highest-grossing film series ever, with a combined gross of over $5.8 billion. Critical reception for the first film was mixed, and mostly negative until the fifth and later films, which were more positively received. Outside of the films, Fast & Furious has been the focus of other media, including attractions at Universal Studios Hollywood, live shows, commercials, many video games and toys. It is considered the vehicle that propelled lead actors Paul Walker and Vin Diesel to stardom.


== Production ==


=== Development ===

In 2000, actor Paul Walker had worked with director Rob Cohen on The Skulls. Cohen secured a deal with producer Neal H. Moritz for an untitled action film for Universal Pictures, and approached Walker and asked him to suggest his "dream" action film; Walker suggested a mash-up of the films Days of Thunder (1990) and Donnie Brasco (1997). Soon thereafter, Cohen and Moritz brought him a Vibe magazine article published in May 1998, which detailed an undercover street racing circuit operating in New York City, and suggested a story that was to be a re-imagined version of the film Point Break (1991), but set to follow Walker as an undercover cop tasked with infiltrating the world of underground street racing in Los Angeles. Upon hearing this, Walker signed on immediately; finding his co-star proved more difficult. The studio warmed toward the idea of Timothy Olyphant in the role of Dominic Toretto, due to the success of the blockbuster Gone in 60 Seconds (2000), but he declined. Moritz then persisted on Vin Diesel following his performance in Pitch Black (2000), with Diesel accepting after proposing several script changes. Upon release in June 2001, the film shattered box office expectations, and a 2002 sequel was green-lit by September.Diesel declined to return for the sequel, saying that the screenplay was inferior to its predecessor. Cohen also declined the sequel, opting to develop the film xXx (2002), which starred Diesel in the lead role. To account for these changes, Universal commissioned the writers to create a standalone sequel with Walker in the lead, and brought in John Singleton as the new director. Filming was delayed by a year, and the production location shifted to Miami. Tyrese Gibson, who worked with Singleton on the film Baby Boy (2001), was hired as Walker's new co-star, and was the first entry in the series to feature long-running cast member Ludacris.Universal attempted to bring back Diesel for the third installment, but he again declined due to other projects and a dislike for the script. After failing to secure the returns of Walker or any other member of the original cast, Universal ordered a reboot of the franchise. Screenwriter Chris Morgan subsequently attempted to revive the series primarily for car enthusiasts, introducing new characters, focusing on a car-related subculture, and moving the series to Tokyo; Japan contains one of the world's largest automotive industries. It is the first film in the series to start its tradition of filming in locations outside the United States. Moritz returned and hired director Justin Lin, having been impressed with Lin's work for the film Better Luck Tomorrow (2002), which shared similar elements with Tokyo Drift. Moreover, the series were able to bring Diesel in for a cameo appearance, in exchange for letting the actor's production company acquire the rights to the Riddick character. The third film was the least financially successful of the franchise, received lukewarm reception, and left the future of the franchise in limbo.Away from the franchise, Diesel had made a string of box office or critical flops, including The Chronicles of Riddick (2004), The Pacifier (2005), and Find Me Guilty (2006). After discussions with Universal, the pair shared an interest in reviving the series. After signing Diesel and confirming the return of Lin, Universal worked to track the first film's original co-stars, and re-signed Walker, Michelle Rodriguez, and Jordana Brewster in mid-2008. Walker was initially reluctant to rejoin the franchise after six years, but Diesel assured him that film would be considered the first "true" sequel. Morgan returned to write after the critical praise for the character Han Lue. Given the apparent death of the character in the third film, the timeline of the franchise was altered to account for his appearance. Considered a soft reboot as emphasis on car culture was toned down, the fourth film, Fast & Furious, was a commercial success. Although critical reception was mixed, it reinvigorated the franchise, as well as the star power of Diesel and Walker.

In 2011, Fast Five was released. While developing the film, Universal completely departed from any street racing elements prevalent in previous films, to transform the franchise into a heist action series involving cars. By doing so, they hoped to attract wider audiences that might otherwise be put off by a heavy emphasis on cars and car culture. Fast Five is considered the transitional film in the series, featuring only one car race and giving more attention to action set pieces such as gun fights, brawls, and the heist. Fast Five was initially conceived to conclude the franchise, but following strong box office performance and high critical praise, Universal proceeded to develop a sixth installment. Furthermore, the film is noted for the addition of Dwayne Johnson to the cast, whose performance was critically praised.In late 2011, the Los Angeles Times reported that Universal was approaching the sixth and seventh installment with a single storyline running through both films, with Morgan envisaging themes of freedom and family, but later shifted to account for the studio's wishes to incorporate elements of espionage. Lin revealed that he had, after discussions with Diesel, storyboarded, previsualized, and began editing a twelve-minute finale for Fast & Furious 6, before filming was completed on Fast Five. The suggestion was discussed to shoot the films back-to-back, in order to break the traditional two-year gap between installments, but this notion was abandoned at Lin's request. Upon release, the sixth film became the highest-grossing film in the series.
Universal lacked a major event film for 2014, and rushed Furious 7 into pre-production in mid-2013, due to its status as a bankable asset. Lin decided not to return to direct the seventh film, as he was still performing post-production on Fast & Furious 6. James Wan, primarily known for horror films, took over directorial duties. On November 30, 2013, Walker died in a single-vehicle crash, with filming only half-completed. Following Walker's death, filming was delayed for script rewrites, and his brothers, Caleb and Cody, were used as stand-ins to complete his remaining scenes. The script rewrites completed the story arcs of both Walker and Brewster's characters. Visual effects company Weta Digital was hired to re-create Walker's likeness. The film also introduced Nathalie Emmanuel to the cast. Ultimately, the film's delays saw it being released in April 2015, where it became the highest-grossing film in the franchise, grossing $1.5 billion. It was also the most critically successful, with praise being aimed at the film's action sequences and its emotional tribute to Walker.
The toll of directing with additional re-shoots dissuaded Wan from returning to the franchise, and Universal hired F. Gary Gray to helm the eighth film. This film was to begin a new trilogy, which will conclude the franchise. Diesel announced that introducing Kurt Russell and Charlize Theron as characters in Furious 7 would help to reach this. The film was released in 2017, and received mixed reviews from critics, many of whom praised the performances and action sequences, but criticized the storyline and the long running time. It was an unabashed commercial success, grossing over $1.2 billion worldwide. Universal later announced that final two films will be released in May 2020 and April 2021, with Lin returning to direct. It was announced that Brewster would reprise her role as Mia Toretto, while screenwriter Daniel Casey was hired for the ninth film; F9 is the first film since Tokyo Drift not to be written by Morgan. Pre-production began in February 2019 in London, and filming began in June and concluded in November. John Cena was cast as the film's villain, portraying Jakob Toretto, Dom's brother. Moreover, Sung Kang returned as Han, while the film is the first to star Helen Mirren and saw Lucas Black reprise his role as Sean Boswell from Tokyo Drift. F9 was originally scheduled to be theatrically released on May 22, 2020, but was pushed back a year to April 2, 2021, due to the COVID-19 pandemic.


==== Spin-off films ====
In 2015, Diesel announced that potential spin-offs were in the early stages of development. In 2019, Diesel announced a film that will focus on the female characters from the Fast & Furious, and mentioned that there are a total of three spin-off films in development. Nicole Perlman, Lindsey Beer and Geneva Robertson-Dworet will serve as co-screenwriters on the project.The first spin-off, Fast & Furious Presents: Hobbs & Shaw, was announced in 2018, and starred Johnson and Jason Statham. In late 2017, Variety reported Morgan had written the script, while David Leitch would direct. Originally, the ninth film in the main series was supposed to be released in April 2019, followed by the tenth in April 2021. Instead, Universal opted to proceed with the spin-off, to occupy the 2019 release date. This caused tensions between Johnson, Diesel and Gibson, with Gibson responding through an Instagram post, criticizing Johnson for causing the ninth film to be delayed. In October 2018, long-term producer Neal H. Moritz filed a lawsuit against Universal Pictures for breach of oral contract and committing promissory fraud after the distributor removed him as lead producer for Hobbs & Shaw. Furthermore, it was revealed in May 2019 that Universal dropped Moritz from all future Fast & Furious installments.


==== Better Luck Tomorrow ====
2006's The Fast and the Furious: Tokyo Drift, directed by Justin Lin, marked the first appearance in The Fast Saga of Han Lue, portrayed by Sung Kang, who had already portrayed a character with the same name in Lin's 2002 film Better Luck Tomorrow; Han subsequently became one of the main recurring characters in the franchise. Although the relation between Better Luck Tomorrow's Han and The Fast Saga's Han was originally left unaddressed, both Lin and Kang repeatedly confirmed during the following years that it was the same character, and that Better Luck Tomorrow doubled as Han's origin story, retroactively making the film part of The Fast Saga continuity.


=== Web series ===
In April 2016, DreamWorks Animation was acquired by NBCUniversal for $3.8 billion, with the acquisition including a first look deal with the company to produce animated film and series based on or with films under the Universal Pictures banner. In April 2018, streaming service Netflix green-lit the series Fast & Furious Spy Racers, with Bret Haaland, Diesel, Tim Hedrick, and Morgan set to be the executive producers, while Hedrick and Haaland are expected to act as showrunners. The first season premiered on December 26, 2019, with eight episodes.


== Films ==


=== The Fast Saga ===


==== The Fast and the Furious (2001) ====

The first installment in the series follows Brian O'Conner (Paul Walker), an undercover cop, who is tasked with discovering the identities of a group of unknown automobile hijackers led by Dominic Toretto (Vin Diesel).


==== 2 Fast 2 Furious (2003) ====

Brian O'Conner and Roman Pearce (Tyrese Gibson) team up to go undercover for the U.S. Customs Service to bring down drug lord Carter Verone (Cole Hauser) in exchange for the erasure of their criminal records.


==== The Fast and the Furious: Tokyo Drift (2006) ====

High school car enthusiast Sean Boswell (Lucas Black) is sent to live in Tokyo with his father, and finds solace in the city's drifting community. Although this is the third film released in the franchise, it has been retroactively placed as the sixth, with the subsequent three installments being set between 2 Fast 2 Furious and Tokyo Drift.


==== Fast & Furious (2009) ====

Federal Bureau of Investigation (FBI) agent Brian O'Conner and Dominic Toretto are forced to work together to avenge the murder of Toretto's lover Letty Ortiz (Michelle Rodriguez) and apprehend drug lord Arturo Braga (John Ortiz). The film is set five years after the events of The Fast and the Furious, and before Tokyo Drift.


==== Fast Five (2011) ====

Dominic Toretto, Brian O'Conner and Mia Toretto (Jordana Brewster) plan a heist to steal $100 million from corrupt businessman Hernan Reyes (Joaquim de Almeida) while being pursued for arrest by U.S. Diplomatic Security Service (DSS) agent Luke Hobbs (Dwayne Johnson).


==== Fast & Furious 6 (2013) ====

Dominic Toretto, Brian O'Conner and their team are offered amnesty for their crimes by Luke Hobbs, in exchange for helping him take down a skilled mercenary organization led by Owen Shaw (Luke Evans), one member of which is Toretto's former lover Letty Ortiz. It is the last film to be set before Tokyo Drift.


==== Furious 7 (2015) ====

Deckard Shaw (Jason Statham), a rogue special forces assassin seeking to avenge his comatose younger brother Owen, puts the team of Dominic Toretto and Brian O'Conner in danger once again.


==== The Fate of the Furious (2017) ====

Cyberterrorist Cipher (Charlize Theron) coerces Dom into working for her and turns him against his team, forcing them to find Dominic and take down Cipher.


==== F9 (2021) ====

Dominic Toretto and his family must face Dominic's younger brother Jakob (John Cena), a deadly assassin, who is working with their old enemy Cipher, and who holds a personal vendetta against Dominic.


==== Untitled tenth film ====
A tenth film is planned, with Lin set to return to direct and Chris Morgan to write, respectively. It was scheduled to be released on April 2, 2021 before F9 took its release date, thus causing the film to be postponed to a later date and due to continuous delays with the ongoing COVID-19 pandemic, the postponed No Time to Die took its release date.


==== Untitled eleventh film ====
An eleventh film is planned to end the franchise, with Lin and Morgan set to return to direct and write, respectively.


=== Spin-off films ===


==== Fast & Furious Presents: Hobbs & Shaw (2019) ====

Luke Hobbs and Deckard Shaw are forced to team up with Shaw's sister Hattie (Vanessa Kirby) to battle cybernetically-enhanced terrorist Brixton Lore (Idris Elba) threatening the world with a deadly virus. The film is set after the events of The Fate of the Furious.


==== Untitled female-led film ====
An untitled female-led spin-off film is in development, with Nicole Perlman, Lindsey Beer and Geneva Robertson-Dworet serving as writers.


==== Untitled Hobbs & Shaw sequel ====
In November 2019, producer Hiram Garcia confirmed that all creatives involved have intentions in developing a sequel, with conversations regarding the project ongoing. Garcia confirmed unresolved plot-points would be expanded upon in the next film. By March 2020, Johnson confirmed that a sequel was in active development, though a screenwriter and director had not yet been hired. Later that month, he announced that Chris Morgan was hired as screenwriter, with a plot that includes various new characters written to be introduced in the sequel. The production is under development with Seven Bucks Productions co-producing the film with Johnson and Hiram Garcia returning in their positions as producers.


== Short films ==
The short films were either released direct-to-video or saw limited theatrical distribution by Universal. They were mostly included as special features for The Fast and the Furious (2001), 2 Fast 2 Furious (2003), and Fast & Furious (2009), as part of the DVD releases. The films, which range from 10 to 20 minutes, are designed to be self-contained stories that provide backstory for characters or events introduced in the films. They were also designed to bridge the chronological gap that was created as the initial leads departed the series.


=== The Turbo Charged Prelude for 2 Fast 2 Furious (2003) ===

The film follows Brian O'Conner (Paul Walker), and details his escape from Los Angeles and avoidance of law enforcement, which culminates in his eventual arrival to Miami.
The short film is set between The Fast and the Furious and 2 Fast 2 Furious. It was directed by Philip G. Atwell and written by Keith Dinielli, and was released on June 3, 2003.


=== Los Bandoleros (2009) ===

Dominic Toretto (Vin Diesel) lives as a wanted fugitive in the Dominican Republic. He eventually reunites with Letty (Michelle Rodriguez) and other associates to plan the hijacking of a gasoline shipment to help an impoverished neighborhood.
The film is set after The Fast and the Furious and before Fast & Furious. It was directed by Diesel, and written by T.J. Mancini, from a story by him and Diesel. It was released on July 28, 2009.


== Television ==


=== Fast & Furious Spy Racers ===
Tony Toretto (voiced by Tyler Posey), Dominic Toretto's cousin, is recruited by a government agency together with his friends to infiltrate an elite racing league serving as a front for a crime organization called SH1FT3R that is bent on world domination.
Fast & Furious Spy Racers is an animated series based on the film franchise. Vin Diesel reprises his role as Dominic Toretto, voicing the character in brief appearances. It is produced executively by Tim Hedrick, Bret Haaland, Diesel, Neal Moritz, and Chris Morgan. Hedrick and Haaland also serve as the show's showrunners. The series' first season was released on Netflix on December 26, 2019. Its second season was released on October 9, 2020.


== Cast and crew ==


=== Principal cast ===

List indicator(s)
This section shows characters who will appear or have appeared in multiple Fast & Furious films and related media.

An empty, dark grey cell indicates the character was not in the media, or that the character's official presence has not yet been confirmed.


=== Additional crew & production details ===


== Reception ==


=== Box office performance ===


=== Critical and public response ===


== Music ==


=== Soundtracks ===


=== Singles ===


== Other media ==


=== Universal theme park attractions ===
After the release of Tokyo Drift in 2006, Universal began to market the franchise by introducing theme park attractions. From 2006 to 2013, the attraction The Fast and the Furious: Extreme Close-Up was included as part of the Studio Tour at Universal Studios Hollywood. The tour's tram would enter a small arena, which featured a demonstration of prop vehicles being manipulated by articulated robotic arms.A new attraction, Fast & Furious: Supercharged, opened as part of the Studio Tour at Universal Studios Hollywood in 2015. The tour's tram passes the black Dodge Charger used in the fifth film, as riders are shown a video of Luke Hobbs, who informs them a high-valued witness sought by Owen Shaw is on the tram. The tram enters a warehouse party, where the cast appear via a Pepper's ghost effect, before the party is shut down by the FBI and the tram moves into a motion simulator where a chase sequence ensues, led by Roman Pearce, Letty Ortiz, and Dominic Toretto. A similar attraction opened at Universal Studios Florida in 2018. In the queue, guests pass through a garage with memorabilia from the films before getting a video call from Tej Parker and Mia Toretto inviting them to a party. Guests board "party busses", where they get the video message from Hobbs and the ride proceeds as it does in the Hollywood version.


=== Tour ===
In 2018, Universal announced the Fast & Furious Live tour, a series of live shows which combine stunt driving, pyrotechnics, and projection mapping to recreate scenes from the films and perform other stunts. During production, thousands of stunt performers and drivers auditioned and were required to undergo a four-month training camp if selected. Additionally, parkour athletes, and stunts requiring both drivers and parkour practitioners, also featured.Fast & Furious Live had two preview shows between January 11–12, 2018 at Liverpool's Echo Arena, before officially beginning a European tour a week later.
After the primary leg of the tour concluded, Fast & Furious Live was extended in September 2018 for five additional shows, with two encore shows held at the Pala Alpitour in Turin from September 7–8, a show at the Ziggo Dome in Amsterdam on September 15, and two shows at the O2 Arena in Prague from September 21–22.
The tour was panned by critics. Ryan Gilbey of The Guardian wrote on his review that "Large sections of seating in the O2 were closed off; entire rows in the rest of it were empty" and "The only danger in Fast & Furious Live is that the audience might die of carbon monoxide poisoning. Or boredom." Adam White of The Daily Telegraph gave the show a two out of five rating, commenting that "Fast & Furious Live often feels like an elaborate if lethargic playground game, one hinging almost entirely on imagination."The tour was a financial failure, with the show's production company entering administration in the summer of 2018 and all of the cars and equipment auctioned off in 2019.


=== Video games ===
Fast & Furious has spawned several racing video games tied into the series, or has served as inspiration for other games playable on various systems, notably Need For Speed and Midnight Club. The game The Fast and the Furious was released in 2004 for mobile phone and Arcade, and was based on the first installment, and their sequel 2 Fast 2 Furious was released in the same year exclusively for mobile phone, and was based on the second installment. The game The Fast and the Furious was released in 2006 for the PlayStation 2 and PlayStation Portable, and drew heavy inspiration from Tokyo Drift. The game sold moderately and launched to mixed reviews. Originally, a Fast and the Furious video game was about to release on 2003 by Genki but was cancelled for unknown reasons, presumably due to the graphics.
Notably, several games have been released for mobile gaming, with a number available for iOS and Android devices, with the unlicensed tie-ins The Fast and the Furious: Pink Slip, Fast & Furious, Fast Five and Fast & Furious: Adrenaline. For the sixth installment, Universal helped develop a tie-in titled Fast & Furious 6: The Game, and aided development for Fast & Furious: Legacy.
Fast & Furious: Showdown was released in 2013 for Microsoft Windows, Xbox 360, PlayStation 3, Wii U and Nintendo 3DS. It marked the second game available for consoles, and the player controls multiple characters to help bridge the narrative gap between the fifth and sixth film. It opened to negative reviews and middling financial success. various cars, locations and characters from the series have appeared in the Facebook game Car Town.
In 2015, in a deal with Microsoft Studios, a standalone expansion of Forza Horizon 2 for Xbox One and Xbox 360 was released titled Forza Horizon 2 Presents Fast & Furious. It was released to promote Furious 7, and received generally positive reception, although, some critics lamented the limited involvement from the titular characters. In 2017, vehicular soccer game Rocket League released a downloadable content (DLC) pack in promotion for The Fate of the Furious, where players would be able to purchase the Dodge Charger from the film as well as its exclusive wheels, and six other new customizations.Fast & Furious Crossroads was announced at The Game Awards 2019. It was developed by Slightly Mad Studios, who worked on Need for Speed: Shift and the Project CARS series, and published by Bandai Namco Entertainment. The game was originally scheduled for release in May 2020, but it was almost complete but had been indefinitely delayed due to logistical problems caused by the COVID-19 pandemic. Fast & Furious Crossroads was released worldwide for Microsoft Windows, PlayStation 4, and Xbox One on August 7, 2020.


=== Toys ===
In 2002, RadioShack stocked and sold ZipZaps micro radio-controlled car versions of the cars from the first film, while diecast metal manufacturer Racing Champions released replicas of cars from the first two installments in different scales from 1/18 to 1/64, in 2004.AMT Ertl rivaled the cars released by Racing Champions by producing 1/24-scale plastic model kits of the hero cars in 2004, while Johnny Lightning, under the JL Full Throttle Brand, released 1/64 and 1/24 models of the cars from Tokyo Drift. These models were designed by renowned diecast designer Eric Tscherne. In 2011, Universal licensed the company Greenlight to sell model cars from all films in anticipation for Fast Five. Since 2013, Hot Wheels has released 1/64 models of every car from and since the sixth installment.In 2020, LEGO produced a set in their Technic line of Dom's Dodge Charger.


== See also ==
List of highest-grossing film franchises
Initial D (1995 debut), a Japanese street racing media franchise with similarities to Fast & Furious (particularly Tokyo Drift)
Thunderbolt (1995 film), a Jackie Chan racing action film with similarities to Fast & Furious
Torque, a similar film but involving high speed performance motorcycles.


== Notes ==


== References ==


== External links ==
Official website
The Fast and the Furious on IMDb