Paige O'Hara (born Donna Paige Helmintoller; May 10, 1956), is an American actress, voice actress, singer and painter. O'Hara began her career as a Broadway actress in 1983 when she portrayed Ellie May Chipley in the musical Showboat. In 1991, she made her motion picture debut in Disney's Beauty and the Beast, in which she voiced the film's heroine, Belle. Following the critical and commercial success of Beauty and the Beast, O'Hara reprised her role as Belle in the film's two direct-to-video follow-ups, Beauty and the Beast: The Enchanted Christmas (1997) and Belle's Magical World (1998), and for a cameo appearance in Ralph Breaks the Internet (2018).


== Early life ==
O'Hara was born in Fort Lauderdale, Florida, and attended Nova High School, in Davie, Florida, and Parkway Middle School of The Arts, also in Florida. She performed in shows with the Fort Lauderdale Children's Theatre.
While her mother is of Irish ancestry, her father was born in Ireland to a family of Irish, British, Dutch and German background.
O'Hara began acting at the age of four, attending acting classes in her home state of Florida.  It was not until she was 12 years old that she developed an interest in singing and enrolled in a performing arts high school.  O'Hara cites American actress and singer Judy Garland as one of her idols.


== Career ==


=== Broadway and stage ===
O'Hara made her first appearance on the Broadway stage as Ellie May Chipley in the revival of Showboat in 1983 starring Donald O'Connor. She repeated the role for the Houston Grand Opera's 1989 production and continued with them when the show was moved to the Cairo Opera House in Egypt. Continuing her legacy as Ellie, she also sang the part on the 1989 Grammy-nominated recording of the musical with Jerry Hadley, Frederica von Stade, and Teresa Stratas, conducted by John McGlinn on the Angel EMI label. Her other American stage credits include the title role in The Mystery of Edwin Drood (Broadway and national tour) and Ado Annie in a national tour of Oklahoma! directed by William Hammerstein. In 1995, she joined the Broadway production of Les Misérables, where she played the role of Fantine.
Internationally, O'Hara has played the role of Nellie Forbush in South Pacific (Australia).
In April 2011, O'Hara played the role of Judy Garland in From Gumm to Garland: JUDY, The Musical at the Tempe Center for the Arts in Tempe, Arizona.


=== Transition to film and Beauty and the Beast ===
A longtime fan of Walt Disney Pictures, O'Hara auditioned for Beauty and the Beast at the age of 30 after reading about the film in The New York Times.In Season 2 of The Legend of Prince Valiant (which starred her Beauty and the Beast co-star Robby Benson), O'Hara had a recurring role as Princess Aleta (who was later promoted to queen). Benson's character Prince Valiant fell in love with Aleta at first sight.
O'Hara also starred as Venus in the BBC's recorded broadcast of the live presentation of Kurt Weill's "One Touch of Venus" and in tribute to her Belle character from Beauty and the Beast, she portrayed Angela, a character in a fictional soap opera, for Disney's 2007 live action/traditional 2-D animated movie Enchanted.
For her work as Belle, O'Hara was honored with a Disney Legend Award on August 19, 2011.As of 2011, O'Hara was replaced by Julie Nathanson as the voice of Belle due to her voice changing significantly over the course of twenty years. Despite this, she still paints Belle for Disney Fine Art and also continues to do promotional appearances for Disney. In 2016, O'Hara appeared at numerous special screenings of Beauty and the Beast in honor of the film's 25th anniversary. O'Hara reprised her role as Belle in the 2018 film Ralph Breaks the Internet.


== Personal life ==
O'Hara is married to actor Michael Piontek. The couple first met in 1989; after six years of dating, they married in 1995. O'Hara identifies herself as a Catholic.


== Filmography ==


=== Film ===


=== Television ===


=== Video games ===


== Theatre ==


== Discography ==
Jerome Kern: Show Boat, conducted by John McGlinn, EMI, 1988


== References ==


== External links ==
Official website
Paige O'Hara at the Internet Broadway Database 
Paige O'Hara on IMDb
Paige O'Hara at the Disney Legends Website
Q&A With Paige O'Hara