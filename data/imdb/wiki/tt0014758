Tommy Cannon (born Thomas Derbyshire, 27 June 1938) and Bobby Ball (born Robert Harper, 28 January 1944 – 28 October 2020), known collectively as Cannon and Ball, were an English comedy double act best known for their comedy variety show The Cannon and Ball Show, which lasted for nine years on ITV. The duo met in the early 1960s while working as welders in Oldham, Lancashire. They started out as singers working the pubs and clubs of Greater Manchester and switched to comedy after being told comics earned an extra £3 a night. They continued to work as a comic duo on television and in theatre and pantomime. Ball died on 28 October 2020 following a COVID-19 diagnosis.


== TV and film ==
Their first TV appearance was as contestants in talent show Opportunity Knocks. In 1974 they appeared in variety series The Wheeltappers and Shunters Social Club before, in 1978, landing slots on Bruce Forsyth's Big Night. In 1979, LWT offered them their own series, The Cannon and Ball Show, which premiered on ITV on 28 July 1979. Further series followed each year through to 1988, along with Christmas and Easter specials.
They were the subjects of This Is Your Life in 1981 when they were surprised by Eamonn Andrews.In 1982, they appeared in a feature film, The Boys in Blue, based loosely on the Will Hay film, Ask a Policeman. The Boys in Blue was regarded critically as weak in comparison and was their only cinema outing.They also featured in a comic strip Rock on Tommy, which was published in the magazine Look-in.Their popularity coincided with the rise of alternative comedy, with its emphasis on more socially relevant and political concerns. As time passed, Cannon and Ball's popularity began to decline, though they were not the only comedy act to suffer as comic tastes shifted. During the 1980s, Greg Dyke, the then Head of Programming at ITV station TVS and later to hold a similar position at LWT expressed a concern that northern comedy shows may not suit southern tastes.
By the 1990s, the duo were seeking a change in direction and appeared in their own sitcom Cannon and Ball's Playhouse, the spin-off series Plaza Patrol and their game show Cannon and Ball's Casino. Plaza Patrol saw them play security guards in a shopping mall.


== Later years ==
In the later years of their career, they continued to find success as a comic duo in theatre and pantomime, along with numerous cameo appearances on TV. In late 2005, they appeared in the British reality TV series I'm a Celebrity...Get Me Out of Here!.
They admitted that, during their hey-day of huge popularity in the 1980s, they were barely on speaking terms and would avoid each other completely when not on stage or rehearsing. These tensions—which lasted for years—were later resolved and the two became very close once again.In 2010, they were panellists on BBC Radio 4 comedy panel show Act Your Age radio series. They appeared in a celebrity edition of Coach Trip on Channel 4 in 2012. In 2018 they appeared in ITV's Last Laugh in Vegas.


== Tours ==
The pair revived a touring version of the theatrical farce Big Bad Mouse, originally a highly successful vehicle for Jimmy Edwards and Eric Sykes in the 1960s and 1970s. The latest incarnation of the show featured Cannon and Ball in the starring roles, supported by 'Allo 'Allo!'s Sue Hodge as Lady Chesapeake and newcomer Emily Trebicki as secretary Miss Spencer. The show opened in Hull during May 2008 and toured six other cities, before ending in August at the Theatre Royal, Windsor, to mixed reviews.


== Religion ==
The pair became devout Christians and published a book called Christianity for Beginners. Ball became a born-again Christian in 1986 and Cannon in 1992, their conversions having a lot to do with the re-kindling of their broken friendship. They regularly featured in their own gospel and "an audience with..." show in churches around the country.


== Other work ==
Also in 2008, the pair's career continued as they appeared on television as the faces of Safestyle UK, a Bradford-based double glazing firm. The pair starred in pantomimes at the Theatre Royal, Lincoln including Robin Hood, Dick Whittington, Cinderella and in 2014 appeared in Jack and the Beanstalk. They also made several appearances in the sitcom Last of the Summer Wine.


== References ==


== External links ==
Official website 
Cannon and Ball discography at Discogs
Comedykings – The unofficial Cannon and Ball website
Interview with Tommy Cannon and Bobby Ball – British Library sound recording