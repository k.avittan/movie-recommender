Tillie Lerner Olsen (January 14, 1912 – January 1, 2007) was an American writer associated with the political turmoil of the 1930s and the first generation of American feminists.


== Biography ==

Olsen was born to Russian Jewish immigrants in Wahoo, Nebraska, and moved to Omaha while a young child. There she attended Lake School in the Near North Side through the eighth grade, living among the city's Jewish community. At age 15, she dropped out of Omaha High School to enter the work force. Over the years Olsen worked as a waitress, domestic worker, and meat trimmer. She was also a union organizer and political activist in the Socialist community. In 1932, Olsen began to write her first novel Yonnondio, the same year she gave birth to Karla, the first of four daughters.In 1933, Olsen moved to California, where she continued her union activities. In the 1930s she joined the American Communist party. She was briefly jailed in 1934 while organizing a packing house workers' union (the charge was "making loud and unusual noise"), an experience she wrote about in The Nation, The New Republic, and The Partisan Review. She later moved to San Francisco, California, where in 1936 she met and lived with Jack Olsen, who was an organizer and a longshoreman. In 1937, she gave birth to her second child, her first child with her future husband Jack Olsen, whom she married in 1944, on the eve of his departure for service in World War II. San Francisco remained her home until her 85th year when she moved to Berkeley, California, to a cottage behind the home of her youngest daughter.Olsen died on January 1, 2007, in Oakland, California, aged 94.


== Writing ==
During the 1930s as a writer she attempted to introduce the challenges of her own life and contemporary social/political circumstances into a novel which she had begun writing when she was only 19. Although only an excerpt of the first chapter was published in The Partisan Review in 1934, it led to a contract for her with Random House. Olsen abandoned the book, however, due to work, child rearing, and household responsibilities. Decades later in 1974, her unfinished novel was published as Yonnondio: From the Thirties.
During the early 1930s Tillie published a number of pieces of what is now referred to as "reportage". Reportage was defined by Joseph North at the 1935 National Writers Conference held in New York City of as "three-dimensional reporting...both an analysis and an experience, culminating in a course of action." Tillie returned to this form more 50 years later when she wrote "A Vision of Fear and Hope" for Newsweek, in 1994.Olsen first published a book in 1961, Tell Me a Riddle, a collection of four short stories, most linked by the characters in one family. Three of the stories were from the point of view of mothers. "I Stand Here Ironing" is the first and shortest story in the collection, about a woman who is grieving about her daughter's life and about the circumstances that shaped her own mothering. "O Yes" is the story of a white woman whose young daughter's friendship with a black girl is narrowed and ended by the pressures of junior high school.  "Hey Sailor, What Ship?", is told by an aging merchant marine sailor whose friendship with a San Francisco family (relatives of the main character in "Tell Me a Riddle") is becoming increasingly strained due to his alcoholism. (In later editions of the book, "Hey Sailor, What Ship?" appears as the second story in the collection). The title story is really a novella, and tells the story of an elderly Jewish immigrant couple facing the wife's death and trying to make sense out of the world in which they find themselves.
All four stories in Tell Me a Riddle were featured in Best American Short Stories, in the year each was first published in a literary magazine. The title story was awarded the O. Henry Award in 1961 for best American short story.
In 1968, Olsen signed the "Writers and Editors War Tax Protest" pledge, vowing to refuse to pay taxes in protest against the Vietnam War.Olsen's non-fiction volume, titled Silences, published in 1978, presented an analysis of authors' silent periods, including writer's blocks, unpublished work, and the problems that working-class writers, and women in particular, have in finding the time to concentrate on their art. One of her observations was that prior to the late 20th century, all the great women writers in Western literature either had no children or had full-time housekeepers to raise the children. The second part of the book was a study of the work of little-known writer Rebecca Harding Davis. Olsen researched and wrote the book in the San Francisco Public Library.

Once her books were published, Olsen became a teacher and writer-in-residence at numerous colleges, such as Amherst College, Stanford University, MIT, and Kenyon College. She was the recipient of nine honorary degrees, National Endowment for the Arts fellowships, and a Guggenheim Fellowship. Also among the honors bestowed upon Olsen was the Distinguished Contributions to American Literature Award from the American Academy and the Institutes of American Arts and Letters, in 1975, and the Rea Award for the Short Story, in 1994, for a lifetime of outstanding achievement in the field of short story writing. Tillie was invited to record her work at the Library of Congress in 1996.


== Legacy ==
Though she published little, Olsen was very influential for her treatment of the lives of women and the poor. She drew attention to why women have been less likely to be published authors (and why they receive less attention than male authors when they do publish). Her work received recognition in the years of much feminist political and social activity. It contributed to new possibilities for women writers. Olsen's influence on American feminist fiction has caused some critics to be frustrated at simplistic feminist interpretations of her work. In particular, several critics have pointed to Olsen's Communist past as contributing to her thought. Olsen's fiction awards, and the ongoing attention to her work, is often focused upon her unique use of language and story form, a form close to poetry in compression and clarity, as well as upon the content.
Reviewing Olsen's life in The New York Times Book Review, Margaret Atwood attributed Olsen's relatively small output to her full life as a wife and mother, a "grueling obstacle course" experienced by many women writers. Her book Silences "begins with an account, first drafted in 1962, of her own long, circumstantially enforced silence," Atwood wrote. "She did not write for a very simple reason: A day has 24 hours. For 20 years she had no time, no energy and none of the money that would have bought both."Tillie Olsen: A Heart in Action is a 2007 documentary film directed and produced by Ann Hershey on the life and literary influence of Olsen.


== Major works ==
Tell Me a Riddle, Lippincott, 1961. Reprinted, Rutgers University Press, 1995
Yonnondio: From the Thirties, Delacorte, 1974. Reprinted, Dell, 1989.
Silences, Delacorte, 1978. Reprinted, Dell, 1989. Reprinted, The Feminist Press, 2003.
Mothers to Daughter, Daughter to Mother: Mothers on Mothering: A Daybook and Reader, The Feminist Press, 1989.
Mothers & Daughters: That Special Quality: An Exploration in Photographs with Estelle Jussim, Aperture, 1995.
The Riddle of Life And Death with Leo Tolstoy, The Feminist Press, 2007.
Tell Me a Riddle, Requa I and Other Works, University of Nebraska Press, 2013.


== References ==


== Further reading ==
Coiner, Constance. Better Red: The Writing and Resistance of Tillie Olsen and Meridel Le Sueur. Oxford University Press, 1995.
Frye, Joanne S., Tillie Olsen: A Study of the Short Fiction, Twayne Publishers, ISBN 0805708634, 1997
Dawahare, Anthony. "'That Joyous Certainty': History and Utopia in Tillie Olsen's Depression-Era Literature", Twentieth Century Literature, Vol. 44, No. 3. (Autumn, 1998), pp. 261–75.
Hedges, Elaine and Shelley Fisher Fishkin, eds. Listening to Silences: New Essays in Feminist Criticism. New York: Oxford University Press, 1994.
Rosenfelt, Deborah. "From the Thirties: Tillie Olsen and the Radical Tradition." Feminist Studies, Vol. 7, No. 3. (Autumn, 1981), pp. 371–406.
Schultz, Lydia A. "Flowing against the Traditional Stream: Consciousness in Tillie Olsen's 'Tell Me a Riddle.'" MELUS, Vol. 22, No. 3, Varieties of Ethnic Criticism. (Autumn, 1997), pp. 113–31.


== Research resources ==
Tillie Olsen Papers, 1930-1990(call number M0667; ca. 62 linear ft.) are housed in the Department of Special Collections and University Archives at Stanford University Libraries


== External links ==
Tillie Olsen Film Project
Biography on GradeSaver
Bibliography from Creighton University
Obituary/appreciation by Anthony Dawahare in Reconstruction 8.1, 2008
"Tillie Olsen" by Abigail Martin in the Western Writers Series Digital Editions at Boise State University
Better Red: The Writing and Resistance of Tillie Olsen and Meridel Le Sueur by Constance Coiner