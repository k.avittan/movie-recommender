The Reign of Terror, commonly The Terror (French: la Terreur), was a period of the French Revolution when, following the creation of the First French Republic, a series of massacres and numerous public executions took place in response to revolutionary fervour, anticlerical sentiment, and spurious accusations of treason by Maximilien Robespierre and the Committee of Public Safety.
There is disagreement among historians over when exactly "the Terror" began. Some consider it to have begun only in 1793, giving the date as either 5 September, June or March, when the Revolutionary Tribunal came into existence. Others, however, cite the earlier time of the September Massacres in 1792, or even July 1789, when the first killing of the revolution occurred. There is a consensus that it ended with the fall of Maximilien Robespierre in July 1794 and resulting Thermidorian Reaction. By then, 16,594 official death sentences had been dispensed throughout France since June 1793, of which 2,639 were in Paris alone; and an additional 10,000 died in prison, without trial, or under both of these circumstances.


== Barère and Robespierre glorify "terror" ==

There was a sense of emergency among leading politicians in France in the summer of 1793 between the widespread civil war and counter-revolution. Bertrand Barère exclaimed on 5 September 1793 in the Convention: "Let's make terror the order of the day!" They were determined to avoid street violence such as the September Massacres of 1792 by taking violence into their own hands as an instrument of government.Robespierre in February 1794 in a speech explained the necessity of terror:

If the basis of popular government in peacetime is virtue, the basis of popular government during a revolution is both virtue and terror; virtue, without which terror is baneful; terror, without which virtue is powerless. Terror is nothing more than speedy, severe and inflexible justice; it is thus an emanation of virtue; it is less a principle in itself, than a consequence of the general principle of democracy, applied to the most pressing needs of the patrie [homeland, fatherland].
Some historians argue that such terror was a necessary reaction to the circumstances. Others suggest there were additional causes, including ideological and emotional.


== Influences ==


=== Enlightenment thought ===

Enlightenment thought emphasized the importance of rational thinking and began challenging legal and moral foundations of society, providing the leaders of the Reign of Terror with new ideas about the role and structure of government.Rousseau's Social Contract argued that each person was born with rights, and they would come together to form a government that would then protect those rights. Under the social contract, the government was required to act for the general will, which represented the interests of everyone rather than a few factions. Drawing from the idea of a general will, Robespierre felt that the French Revolution could result in a Republic built for the general will but only once those who fought this ideal were expelled. Those who resisted the government were deemed "tyrants" fighting against the virtue and honor of the general will. The leaders felt that their ideal version of government was threatened from the inside and outside of France, and terror was the only way to preserve the dignity of the Republic created from French Revolution.The writings of Baron de Montesquieu, another Enlightenment thinker of the time, greatly influenced Robespierre as well. Montesquieu's Spirit of the Laws defines a core principle of a democratic government: virtue—described as "the love of laws and of our country." In Robespierre's speech to the National Convention on 5 February 1794, titled "Virtue & Terror", he regards virtue as being the "fundamental principle of popular or democratic government." This was, in fact, the same virtue defined by Montesquieu almost 50 years prior. Robespierre believed that the virtue needed for any democratic government was extremely lacking in the French people. As a result, he decided to weed out those he believed could never possess this virtue. The result was a continual push towards Terror. The Convention used this as justification for the course of action to "crush the enemies of the revolution…let the laws be executed…and let liberty be saved."Though some members of the Enlightenment greatly influenced revolutionary leaders, cautions from other Enlightenment thinkers were blatantly ignored. Voltaire's warnings were often overlooked, though some of his ideas were used for justification of the Revolution and the start of the Terror. He protested against Catholic dogmas and the ways of Christianity, stating, "of all religions, the Christian should, of course, inspire the most toleration, but till now the Christians have been the most intolerant of all men." These criticisms were often used by Robespierre and other leaders as justification for their anti-religious reforms. In his Philosophical Dictionary, Voltaire states, "we are all steeped in weakness and error; let us forgive each other our follies; that is the first law of nature" and "every individual who persecutes a man, his brother, because he is not of his opinion, is a monster."


=== Threats of foreign invasion ===
After the beginning of the French Revolution, the surrounding monarchies did not show great hostility towards the rebellion. Though mostly ignored, Louis XVI was later able to find support in Leopold II of Austria (brother of Marie Antoinette) and Frederick William II of Prussia. On 27 August 1791, these foreign leaders made the Pillnitz Declaration, saying they would restore the French monarch if other European rulers joined. In response to what they viewed to be the meddling of foreign powers, France declared war on 20 April 1792. However, at this point, the war was only Prussia and Austria against France. France began this war with a large series of defeats, which set a precedent of fear of invasion in the people that would last throughout the war. 
Massive reforms of military institutions, while very effective in the long run, presented the initial problems of inexperienced forces and leaders of questionable political loyalty. In the time it took for officers of merit to use their new freedoms to climb the chain of command, France suffered. Many of the early battles were definitive losses for the French. There was the constant threat of the Austro-Prussian forces which were advancing easily toward the capital, threatening to destroy Paris if the monarch was harmed. This series of defeats, coupled with militant uprisings and protests within the borders of France, pushed the government to resort to drastic measures to ensure the loyalty of every citizen, not only to France but more importantly to the Revolution.
While this series of losses was eventually broken, the reality of what might have happened if they persisted hung over France. The tide would not turn from them until September 1792 when the French won a critical victory at Valmy preventing the Austro-Prussian invasion. While the French military had stabilized and was producing victories by the time the Reign of Terror officially began, the pressure to succeed in this international struggle acted as justification for the government to pursue its actions. It was not until after the execution of Louis XVI and the annexation of the Rhineland that the other monarchies began to feel threatened enough to form the First Coalition. The Coalition, consisting of Russia, Austria, Prussia, Spain, Holland, and Sardinia began attacking France from all directions, besieging and capturing ports and retaking ground lost to France. With so many similarities to the first days of the Revolutionary Wars for the French government, with threats on all sides, unification of the country became a top priority. As the war continued and the Reign of Terror began, leaders saw a correlation between using terror and achieving victory. Well phrased by Albert Soboul, "terror, at first an improvised response to defeat, once organized became an instrument of victory." The threat of defeat and foreign invasion may have helped spur the origins of the Terror, but the timely coincidence of the Terror with French victories added justification to its growth.


=== Popular pressure ===
During the Reign of Terror, the sans-culottes and the Hébertists put pressure on the National Convention delegates and contributed to the overall instability of France. The National Convention was bitterly split between the Montagnards and the Girondins. The Girondins were more conservative leaders of the National Convention, while the Montagnards supported radical violence and pressures of the lower classes. Once the Montagnards gained control of the National Convention, they began demanding radical measures. Moreover, the sans-culottes, the urban workers of France, agitated leaders to inflict punishments on those who opposed the interests of the poor. The sans-culottes’ violently demonstrated, pushing their demands and creating constant pressure for the Montagnards to enact reform. The sans-culottes fed the frenzy of instability and chaos by utilizing popular pressure during the Revolution. For example, the sans-culottes sent letters and petitions to the Committee of Public Safety urging them to protect their interests and rights with measures such as taxation of foodstuffs that favored workers over the rich. They advocated for arrests of those deemed to oppose reforms against those with privilege, and the more militant members would advocate pillage in order to achieve the desired equality. The resulting instability caused problems that made forming the new Republic and achieving full political support critical.


=== Religious upheaval ===
The Reign of Terror was characterized by a dramatic rejection of long-held religious authority, its hierarchical structure, and the corrupt and intolerant influence of the aristocracy and clergy. Religious elements that long stood as symbols of stability for the French people, were replaced by reason and scientific thought. The radical revolutionaries and their supporters desired a cultural revolution that would rid the French state of all Christian influence. This process began with the fall of the monarchy, an event that effectively defrocked the State of its sanctification by the clergy via the doctrine of Divine Right and ushered in an era of reason.Many long-held rights and powers were stripped from the church and given to the state. In 1789, church lands were expropriated and priests killed or forced to leave France. A Festival of Reason was held in the Notre Dame Cathedral, which was renamed "The Temple of Reason", and the old traditional calendar was replaced with a new revolutionary one. The leaders of the Terror tried to address the call for these radical, revolutionary aspirations, while at the same time trying to maintain tight control on the de-Christianization movement that was threatening to the clear majority of the still devoted Catholic population of France. The tension sparked by these conflicting objectives laid a foundation for the "justified" use of terror to achieve revolutionary ideals and rid France of the religiosity that revolutionaries believed was standing in the way.


== Major events during the Terror ==

On 10 March 1793 the National Convention set up the Revolutionary Tribunal. Among those charged by the tribunal, about half were acquitted (though the number dropped to about a quarter after the enactment of the Law of 22 Prairial on 10 June 1794). In March rebellion broke out in the Vendée in response to mass conscription, which developed into a civil war. Discontent in the Vendée lasted – according to some accounts—until after the Terror.
On 6 April 1793 the National Convention established the Committee of Public Safety, which gradually became the de facto war-time government of France. The Committee oversaw the Reign of Terror. "During the Reign of Terror, at least 300,000 suspects were arrested; 17,000 were officially executed, and perhaps 10,000 died in prison or without trial."On 2 June 1793 the Parisian sans-culottes surrounded the National Convention, calling for administrative and political purges, a low fixed-price for bread, and a limitation of the electoral franchise to sans-culottes alone. With the backing of the national guard, they persuaded the Convention to arrest 29 Girondist leaders. In reaction to the imprisonment of the Girondin deputies, some thirteen departments started the Federalist revolts against the National Convention in Paris, which were ultimately crushed.
On 24 June 1793 the Convention adopted the first republican constitution of France, the French Constitution of 1793. It was ratified by public referendum, but never put into force.
On 13 July 1793 the assassination of Jean-Paul Marat—a Jacobin leader and journalist—resulted in a further increase in Jacobin political influence. Georges Danton, the leader of the August 1792 uprising against the king, was removed from the Committee of Public Safety on 10 July 1793. On 27 July 1793 Robespierre became part of the Committee of Public Safety.
On 23 August 1793 the National Convention decreed the levée en masse:On 9 September the Convention established paramilitary forces, the "revolutionary armies", to force farmers to surrender grain demanded by the government. On 17 September, the Law of Suspects was passed, which authorized the imprisonment of vaguely defined "suspects". This created a mass overflow in the prison systems. On 29 September, the Convention extended price fixing from grain and bread to other essential goods, and also fixed wages.
On 10 October the Convention decreed that "the provisional government shall be revolutionary until peace." On 24 October the French Republican Calendar was enacted. The trial of the Girondins started on the same day, they were executed on 31 October.
Anti-clerical sentiments increased during 1793 and a campaign of dechristianization occurred. On 10 November (20 Brumaire Year II of the French Republican Calendar), the Hébertists organized a Festival of Reason.

On 14 Frimaire (5 December 1793) the National Convention passed the Law of Frimaire, which gave the central government more control over the actions of the representatives on mission.
On 16 Pluviôse (4 February 1794), the National Convention decreed the abolition of slavery in all of France and in French colonies.
On 8 and 13 Ventôse (26 February and 3 March 1794),  Saint-Just proposed decrees to confiscate the property of exiles and opponents of the revolution, known as the Ventôse Decrees.
By the end of 1793, two major factions had emerged, both threatening the Revolutionary Government: the Hébertists, who called for an intensification of the Terror and threatened insurrection, and the Dantonists, led by Georges Danton, who demanded moderation and clemency. The Committee of Public Safety took actions against both. The major Hébertists were tried before the Revolutionary Tribunal and executed on 24 March. The Dantonists were arrested on 30 March, tried on 3 to 5 April and executed on 5 April.
On 20 Prairial (8 June 1794) the Festival of the Supreme Being was celebrated across the country; this was part of the Cult of the Supreme Being, a deist national religion. On 22 Prairial (10 June), the National Convention passed a law proposed by Georges Couthon, known as the Law of 22 Prairial, which simplified the judicial process and greatly accelerated the work of the Revolutionary Tribunal. With the enactment of the law, the number of executions greatly increased, and the period from this time to the Thermidorian Reaction became known as "The Great Terror" (French: la Grande Terreur).
On 8 Messidor (26 June 1794), the French army won the Battle of Fleurus, which marked a turning point in France's military campaign and undermined the necessity of wartime measures and the legitimacy of the Revolutionary Government.


== Thermidorian Reaction ==

The fall of Robespierre was brought about by a combination of those who wanted more power for the Committee of Public Safety (and a more radical policy than he was willing to allow) and the moderates who completely opposed the revolutionary government. They had, between them, made the Law of 22 Prairial one of the charges against him, so that, after his fall, to advocate terror would be seen as adopting the policy of a convicted enemy of the republic, putting the advocate's own head at risk. Between his arrest and his execution, Robespierre may have tried to commit suicide by shooting himself, although the bullet wound he sustained, whatever its origin, only shattered his jaw. Alternatively, he may have been shot by the gendarme Merda. The great confusion that arose during the storming of the municipal Hall of Paris, where Robespierre and his friends had found refuge, makes it impossible to be sure of the wound's origin. In any case, Robespierre was guillotined the next day.The reign of the standing Committee of Public Safety was ended. New members were appointed the day after Robespierre's execution, and limits on terms of office were fixed (a quarter of the committee retired every three months). The Committee's powers were gradually eroded.


== See also ==
Bals des victimes
Great Purge
Infernal columns
Tricoteuse
Drownings at Nantes
State terrorism


== Notes ==


== References ==


=== Citations ===


=== Works cited ===


== Further reading ==


=== Primary sources ===
Cléry, Jean-Baptiste; Henry Essex Edgeworth (1961) [1798].  Sidney Scott (ed.). Journal of the Terror. Cambridge: Cambridge University Press. OCLC 3153946.


=== Secondary sources ===


=== Historiography ===


== External links ==
"The Terror" from In Our Time (BBC Radio 4)