Lombard Street is an east–west street in San Francisco, California that is famous for a steep, one-block section with eight hairpin turns. Stretching from The Presidio east to The Embarcadero (with a gap on Telegraph Hill), most of the street's western segment is a major thoroughfare designated as part of U.S. Route 101. The famous one-block section, claimed to be "the crookedest street in the world", is located along the eastern segment in the Russian Hill neighborhood. It is a major tourist attraction, receiving around two million visitors per year and up to 17,000 per day on busy summer weekends, as of 2015.San Francisco surveyor Jasper O'Farrell named the road after Lombard Street in Philadelphia.


== Route description ==
Lombard Street's west end is at Presidio Boulevard inside The Presidio; it then heads east through the Cow Hollow neighborhood. For 12 blocks, between Broderick Street and Van Ness Avenue, it is an arterial road that is co-signed as U.S. Route 101. Lombard Street continues through the Russian Hill neighborhood and to the Telegraph Hill neighborhood. At Telegraph Hill it turns south, becoming Telegraph Hill Boulevard to Pioneer Park and Coit Tower. Lombard Street starts again at Winthrop Street and ends at The Embarcadero as a collector road.Lombard Street is known for the one-way block on Russian Hill between Hyde and Leavenworth Streets, where eight sharp turns are said to make it the most crooked street in the world. The design, first suggested by property owner Carl Henry and built in 1922, was intended to reduce the hill's natural 27 percent grade, which was too steep for most vehicles. The crooked block is about 600 feet (180 m) long (412.5 feet (125.7 m) straightline), is one-way (downhill) and is paved with red bricks. The sign at the top recommends 5 mph (8 km/h). 
The segment normally sees around 250 vehicles per hour, with average daily traffic reaching 2630 vehicles in 2013. During peak times, vehicles have to wait up to 20 minutes to enter the Crooked Street segment, in a queue that can reach Van Ness Avenue.
To reduce habitual congestion and delays, future visitors may be required to reserve a time and pay a fee to drive down the crooked street.The Powell-Hyde cable car stops at the top of the block on Hyde Street.By 2017, the area around the curved segment had become a hot-spot of what has been described as "San Francisco's car break-in epidemic." This may in part have been due to its heavy traffic and association with tourism.
The Academy of Art University owns and operates a building called Star Hall on the street for housing purposes.Past residents of Lombard Street include Rowena Meeks Abdy, an early California painter who worked in the style of Impressionism.
Chase scenes in many films have been filmed on the street, including Good Neighbor Sam, Dr. Goldfoot and the Bikini Machine, What's Up, Doc?, Magnum Force, and Ant-Man and the Wasp. Lombard Street is also portrayed in the 2015 Pixar film Inside Out.


== Gallery ==

		
		
		
		


== See also ==

Vermont Street, the other San Francisco street claimed to be the "most crooked" has seven turns instead of eight, but its hill is steeper than Lombard's.
Snake Alley in Burlington, Iowa, once recognized by Ripley's Believe It or Not! as "The Crookedest Street in the World". Like Lombard Street, it has eight turns but over a shorter distance.


== References ==


== External links ==
Tourist Trapped: The Crookedest Street In The World, SFGate Culture Blog
Lombard Street, SF GuideLines (includes early images)
Lombard Street on San Francisco To Do