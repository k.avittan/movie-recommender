Dancing Girl is a prehistoric bronze sculpture made in lost-wax casting about c. 2300-1750 BCE in the Indus Valley Civilisation city of Mohenjo-daro (in modern-day Pakistan), which was one of the earliest cities. The statue is 10.5 centimetres (4.1 in) tall, and depicts a naked young woman or girl with stylized proportions standing in a confident, naturalistic pose. Dancing Girl is well-regarded as a work of art, and is a cultural artefact of the Indus Valley Civilisation.
The statuette was discovered by British archaeologist Ernest Mackay in the "HR area" of Mohenjo-daro in 1926. It is held by the National Museum, New Delhi, and the ownership of the statue is disputed by Pakistan.


== Description ==
This is one of 2 bronze art works found at Mohenjo-daro that show more flexible features when compared to other more formal poses. The girl is naked, wears a number of bangles and a necklace and is shown in a natural standing position with one hand on her hip. She wears 24 to 25 bangles on her left arm and 4 bangles on her right arm, and some object was held in her left hand, which is resting on her thigh; both arms are unusually long. Her necklace has three big pendants. She has her long hair styled in a big bun that is resting on her shoulder.


== Expert opinions ==

In 1973, British archaeologist Mortimer Wheeler described the item as his favourite statuette:

"She's about fifteen years old I should think, not more, but she stands there with bangles all the way up her arm and nothing else on. A girl perfectly, for the moment, perfectly confident of herself and the world. There's nothing like her, I think, in the world." 
John Marshall, the archeologist at Mohenjo-daro who found the figure, described the figure as "a young girl, her hand on her hip in a half-impudent posture, and legs slightly forward as she beats time to the music with her legs and feet". He is known to reacted with surprise when he saw this statuette. He said "When I first saw them I found it difficult to believe that they were prehistoric."  The archaeologist Gregory Possehl described Dancing Girl as "the most captivating piece of art from an Indus site" and qualified the description of her as a dancer by stating that, "We may not be certain that she was a dancer, but she was good at what she did and she knew it."The statue led to two important discoveries about the civilization: first that they knew metal blending, casting and other sophisticated methods, and secondly that entertainment, especially dance was part of the culture. The bronze girl was made using the lost-wax casting technique and shows the expertise of the people in making bronze works during that time.A similar bronze statuette was found by Mackay during his final full season of 1930–31 at DK-G area in a house at Mohenjo-daro. The preservation, as well as quality of craftsmanship, is inferior to that of the well known Dancing Girl. This second bronze female figure is displayed at Karachi Museum, Pakistan.An engraving on a piece of red potsherd, discovered at Bhirrana, India, a Harappan site in Fatehabad district in Haryana, shows an image that is evocative of Dancing Girl. The excavation team leader, L. S. Rao, Superintending Archaeologist, Excavation Branch, ASI, remarked that, “... the delineation [of the lines in the potsherd] is so true to the stance, including the disposition of the hands, of the bronze that it appears that the craftsman of Bhirrana had first-hand knowledge of the former”.


== Ownership dispute ==
Some Pakistani politicians and experts have demanded that the Dancing Girl be "returned" to Pakistan. In 2016 Pakistani barrister, Javed Iqbal Jaffery, petitioned the Lahore High Court for the return of the statue, claiming that it had been "taken from Pakistan 60 years ago on the request of the National Arts Council in Delhi but never returned." According to him, the Dancing Girl was to Pakistan what Da Vinci's Mona Lisa was to Europe.Another version of events, however, "suggests the statue was taken to Delhi before Partition by Mortimer Wheeler".


== References ==

General references

Craddock PT. 2015. The metal casting tradiitons of South Asia: Continuity and innovation. Indian Journal of History of Science 50(1):55-82.
During Caspers ECL. 1987. Was the dancing girl from Mohenjo-daro a Nubian?Annali, Istituto Oriental di Napoli 47(1):99-105.
Kenoyer JM. 1998. Seals and sculpture of the Indus cities. Minerva 9(2):19-24.
Possehl GL. 2002. The Indus Civilization: A Contemporary Perspective. Walnut Creek, California: Altamira Press.
Prakash B. 1983. Metallurgy in India through the ages. Bulletin of the Metals Museum of the Japan Institute of Metals 8:23-36.
Sadasivan B. 2011. The Dancing Girl: A History of Early India. Singapore: Institute of Southeast Asian Studies.