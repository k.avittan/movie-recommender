Riders of the Purple Sage is a Western novel by Zane Grey, first published by Harper & Brothers in 1912. Considered by scholars to have played a significant role in shaping the formula of the popular Western genre, the novel has been called "the most popular western novel of all time."


== Plot ==
Riders of the Purple Sage is a story about three main characters, Bern Venters, Jane Withersteen, and Jim Lassiter, who in various ways struggle with persecution from the local Mormon community ("Mormon" is the informal term for the Church of Jesus Christ of Latter-day Saints), led by Bishop Dyer and Elder Tull, in the fictional town of Cottonwoods, Utah.
Jane Withersteen, a born-and-raised Mormon, provokes Elder Tull because she is attractive, wealthy, and befriends "Gentiles" (non-Mormons), namely, a little girl named Fay Larkin, a man she has hired named Bern Venters, and another hired man named Lassiter. Elder Tull, a polygamist with two wives already, wishes to have Jane for a third wife, along with her estate.
The story involves cattle-rustling, horse-theft, kidnapping and gunfights.


== Setting ==
Southern Utah canyon country, 1871. The influx of Mormon settlers from 1847 to 1857 serves as a backdrop for the plot. The Mormons had been living in Kirtland, Ohio in the 1830s, but ventured west to escape persecution, Mormons being unpopular.


== Point-of-view ==
The story is told from an anonymous third-person, omniscient point-of-view. The narrator reports what the characters say and how they feel, even when they are alone. For example: "On this night the same old loneliness beset Venters..." 


== Characters ==
Jane WithersteenWealthy owner and operator of the considerable Withersteen ranch, her father having founded and established the estate. Miss Withersteen sympathizes with both Mormons (her own people) and Gentiles, which gets her into trouble with the local bishop and elder.

Bern VentersVenters is a non-Mormon in the employ of Miss Withersteen. As the story opens he is in a very poor state, being persecuted by the local Mormons. However, Venters is very able with firearms and horses, and he is determined to not be beat.

Jim LassiterLassiter is a gunfighter on a mysterious mission which brings him to Cottonwoods and Miss Withersteen. He is a non-Mormon and furthermore has no creed except his own way.

Bess/Elizabeth ErneBess has been raised by Oldring and his band of rustlers; she has very little memory of her mother. She is known as the Masked Rider.

Elder TullTull practices "plural marriage" and desires to marry Jane Withersteen. He also wants to drive Bern Venters and Lassiter out of town and out of the region.


== Sequel ==
The Rainbow Trail, a sequel to Riders of the Purple Sage that reveals the fate of Jane and Lassiter and their adopted daughter, was published in 1915. Both novels are notable for their protagonists' strong opposition to Mormon polygamy, but in Rainbow Trail this theme is treated more explicitly. The plots of both books revolve around the victimization of women in the Mormon culture: events in Riders of the Purple Sage are centered on the struggle of a Mormon woman who sacrifices her wealth and social status to avoid becoming a junior wife of the head of the local church, while Rainbow Trail contrasts the fanatical older Mormons with the rising generation of Mormon women who will not tolerate polygamy and Mormon men who will not seek it.


== Adaptations ==


=== Films ===
Riders of the Purple Sage has been adapted to film five times. The first film version of the novel was the silent film Riders of the Purple Sage (1918) starring William Farnum as Lassiter and Mary Mersch as Jane. In later film versions the villains are corrupt judges or lawyers, not polygamous Mormons. A second silent film version was released in 1925, starring Tom Mix as Lassiter and Mabel Ballin as Jane. The first sound version appeared in 1931, starring George O'Brien as Lassiter and Marguerite Churchill as Jane. In 1941, a fourth film version was released, starring George Montgomery as Jim Lassiter and Mary Howard as Jane. Fifty-five years later, a television film, Riders of the Purple Sage (1996), was released, starring Ed Harris as Lassiter and Amy Madigan as Jane.
Riders of the Purple Sage  (1918), starring William Farnum and Mary Mersch
Riders of the Purple Sage (1925), starring Tom Mix and Mabel Ballin
Riders of the Purple Sage (1931), starring George O'Brien and Marguerite Churchill
Riders of the Purple Sage (1941), starring George Montgomery and Mary Howard
Riders of the Purple Sage (1996), starring Ed Harris and Amy Madigan


=== Other media ===
During World War II the novel was rejected for publication as an Armed Services Editions paperback provided to US servicemen due to perceived bias against Mormonism. 
In 1952, Dell released a comic book version of the novel (Dell # 372).Riders of the Purple Sage was adapted into an opera by composer Craig Bohmler and librettist Steven Mark Kohn. It had its world premiere in February and March 2017 by the Arizona Opera in Tucson and Phoenix. The opera was broadcast nationwide on November 25, 2017 on the WFMT Radio Network's American Opera Series, and broadcast internationally in 2018 via distribution to the European Broadcasting Union.


== In popular culture ==
Riders of the Purple Sage has inspired a number of homages, including:

Three separate western bands have used the name Riders of the Purple Sage, one of which further inspired the name of country rock band, New Riders of the Purple Sage.
Author Philip José Farmer's 1967 science fiction novella, Riders of the Purple Wage, and his 1980 Thieves' World short story, "Spiders of the Purple Mage."
A two part scenario for the Dungeons & Dragons role-playing game titled "Tortles of the Purple Sage" appeared in Dungeon Adventures #6 and #7 (1987).


== References ==
Citations
Bibliography


== External links ==

 Riders of the Purple Sage at Project Gutenberg
 Riders of the Purple Sage public domain audiobook at LibriVox