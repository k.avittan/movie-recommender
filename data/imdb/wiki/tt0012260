Agatha Gregson, née Wooster, later Lady Worplesdon, is a recurring fictional character in the Jeeves stories of British comic writer P. G. Wodehouse, being best known as Bertie Wooster's Aunt Agatha. Haughty and overbearing, Aunt Agatha wants Bertie to marry a wife she finds suitable, though she never manages to get Bertie married,  thanks to Jeeves's interference.
She is often mentioned in the stories as being Bertie's fearsome aunt, in contrast to her sister Aunt Dahlia, Bertie's genial aunt.


== Inspiration ==
The character of Aunt Agatha was inspired by Wodehouse's aunt Mary Bathurst Deane, his mother's older sister. In a 1955 letter to his biographer Richard Usborne, Wodehouse wrote "Aunt Agatha is definitely my Aunt Mary, who was the scourge of my childhood." According to Richard Usborne, "His Aunt Mary (Deane) harried and harassed him a good deal, and blossomed later into Bertie's Aunt Agatha. Aunt Mary honestly considered that her harrying and harassing of the young Pelham was for his good; and she may have been right."Aunt Agatha's country home Woollam Chersey was inspired by Hunstanton Hall, the home of Wodehouse's friend Charles Le Strange.


== Life and character ==
Generally formidable in appearance, Aunt Agatha is five-foot-nine, with a beaky nose, an eagle eye, and a lot of grey hair. Agatha had at first been affianced to Percy Craye, though upon reading in the papers of his behaviour at a Covent Garden ball, she had ended the engagement. She then married Spenser Gregson, though he dies before The Code of the Woosters. About eighteen months before Joy in the Morning, she marries Percy Craye who had by then become Lord Worplesdon, whereupon she becomes Lady Worplesdon. She has one son, Thomas "Thos" Gregson.Aunt Agatha once portrayed Boadicea in an amateur pageant held at her home, Woollam Chersey. Bertie was obliged to play King Edward III in the same pageant. She lives at Woollam Chersey in Hertfordshire as Agatha Gregson and later, after marrying Percy Craye, becomes Lady Worplesdon and lives at the Craye home of Bumpleigh Hall near Steeple Bumpleigh in Hampshire.Generally, Aunt Agatha tries to make Bertie marry or tells him to do some task that she feels will benefit the family's prestige. Though Bertie is not financially dependent on her, he has been intimidated by her since he was young and, particularly in the early stories, feels compelled to obey her wishes. She has never liked Jeeves, and thinks that Bertie is too dependent on him, calling Jeeves Bertie's keeper. She disapproves of Bertie talking to Jeeves about private matters. On one occasion when she hears Bertie ask Jeeves for advice, she tells Jeeves to leave and then scolds Bertie, making remarks to him about "what she thought of a Wooster who could lower the prestige of the clan by allowing menials to get above themselves".Aunt Agatha first appears in "Extricating Young Gussie". This early story is usually not included in the main Jeeves canon (Bertie's surname appears to be Mannering-Phipps), though events in the story are referenced in future stories. In this story, Aunt Agatha tells Bertie to go to New York to keep his cousin, Gussie, away from the girl he has fallen in love with, Ray Denison. Aunt Agatha disapproves of Ray because she is a vaudeville performer, and feels that Gussie would disgrace the family by marrying her. Bertie only manages to make the problem (as Aunt Agatha sees it) worse, and decides to avoid her for a while.In "Aunt Agatha Takes the Count", she tries to make Bertie marry Aline Hemmingway, and later pushes Bertie to marry Honoria Glossop in "Scoring off Jeeves" and "Sir Roderick Comes to Lunch". She also appears in "The Delayed Exit of Claude and Eustace". These stories are in The Inimitable Jeeves.
Aunt Agatha wants to get Bertie a job in "Jeeves and the Impending Doom", tries to get Bertie engaged to Honoria again in "Jeeves and the Yule-tide Spirit", and instructs Bertie to keep his uncle, George Wooster, Lord Yaxley, from marrying an unsuitable woman in "Indian Summer of an Uncle". Her pet dog McIntosh, an Aberdeen terrier, is featured in "Episode of the Dog McIntosh". These stories are collected in Very Good, Jeeves,
Though she is frequently mentioned, Aunt Agatha does not directly appear in the novels. At the end of The Mating Season, Bertie is heading downstairs to see her, intending to finally stand up to her. Nonetheless, he is afraid of what will happen if information about his misadventures reaches Aunt Agatha in Much Obliged, Jeeves.


== Appearances ==
Aunt Agatha appears in:

The Man with Two Left Feet (1917)
"Extricating Young Gussie" (1915)
The Inimitable Jeeves (1923)
"Aunt Agatha Takes the Count" (1922)
"Scoring off Jeeves" (1922)
"Sir Roderick Comes to Lunch" (1922)
"The Delayed Exit of Claude and Eustace"(1922)
Very Good, Jeeves (1930)
"Jeeves and the Impending Doom" (1926)
"Jeeves and the Yule-tide Spirit" (1927)
"Indian Summer of an Uncle" (1930)Aunt Agatha is mentioned in many stories, including:

The Inimitable Jeeves (1923)
"Jeeves and the Chump Cyril" (1918)
Carry On, Jeeves (1925)
"Jeeves and the Unbidden Guest" (1916)
Very Good, Jeeves (1930)
"Episode of the Dog McIntosh" (1929)
"The Love That Purifies" (1929)
Thank You, Jeeves (1934)
Right Ho, Jeeves (1934)
The Code of the Woosters (1938)
Joy in the Morning (1946)
The Mating Season (1949)
Jeeves and the Feudal Spirit (1954)
Jeeves in the Offing (1960)
Stiff Upper Lip, Jeeves (1963)
Much Obliged, Jeeves (1971)
Aunts Aren't Gentlemen (1974)


== Quotes ==
Bertie describes his imperious Aunt Agatha in the following quotes, often mentioning her as a counterpoint to Aunt Dahlia:

"My experience is that when Aunt Agatha wants you to do a thing you do it, or else you find yourself wondering why those fellows in the olden days made such a fuss when they had trouble with the Spanish Inquisition."
"I was looking forward with bright anticipation to the coming reunion with this Dahlia – she, as I may have mentioned before, being my good and deserving aunt, not to be confused with Aunt Agatha, who eats broken bottles and wears barbed wire next to the skin."
"Well, this Dahlia is my good and deserving aunt, not to be confused with Aunt Agatha, the one who kills rats with her teeth and devours her young..."
"Aunt Agatha is cold and haughty, though presumably unbending a bit when conducting human sacrifices at the time of the full moon, as she is widely rumoured to do, and her attitude towards me has always been that of an austere governess, causing me to feel as if I were six years old and she had just caught me stealing jam from the jam cupboard; whereas Aunt Dahlia is as jovial and bonhomous as a pantomime dame in a Christmas pantomime."
"The aunt to whom I alluded was my good and deserving Aunt Dahlia, not to be confused with my Aunt Agatha who eats broken bottles and is strongly suspected of turning into a werewolf at the time of the full moon."


== Influence ==
The term "Aunt Agatha" has come to mean a "formidable aunt" or, more generally, "any older woman of fearsome disposition".


== Adaptations ==
TelevisionIn the 1965–1967 television series The World of Wooster, Aunt Agatha was portrayed by Fabia Drake.
In the 1990–1993 television series Jeeves and Wooster, Aunt Agatha was portrayed by Mary Wimbush in the first three series, and Elizabeth Spriggs in the fourth series. Unlike in the original stories, she goes to America and is interested in running an art gallery. She is also the subject of a portrait used for a soup company's advertisements, whereas in the original Jeeves canon, a portrait of Bertie Wooster was used.RadioJudith Furse voiced Aunt Agatha in the 1958 Caedmon recording of "Indian Summer of an Uncle".
In the 1973–1981 radio drama series What Ho! Jeeves, Joan Sanderson voiced Aunt Agatha.


== See also ==
List of Jeeves characters, an alphabetical list of Jeeves characters
List of P. G. Wodehouse characters in the Jeeves stories, a categorized outline of Jeeves characters
List of Jeeves and Wooster characters, a list of characters in the television series


== References ==
Notes
BibliographyCawthorne, Nigel (2013). A Brief Guide to Jeeves and Wooster. Constable & Robinson. ISBN 978-1-78033-824-8.
Garrison, Daniel H. (1991) [1989]. Who's Who in Wodehouse (Revised ed.). Constable & Robinson. ISBN 1-55882-087-6.
Ring, Tony; Jaggard, Geoffrey (1999). Wodehouse in Woostershire. Porpoise Books. ISBN 1-870-304-19-5.
Taves, Brian (2006). P. G. Wodehouse and Hollywood: Screenwriting, Satires and Adaptations. McFarland & Company. ISBN 978-0786422883.
Wodehouse, P. G (1997). "Extricating Young Gussie". Enter Jeeves: 15 Early Stories. Dover Publications. ISBN 978-0486297170.
Wodehouse, P. G. (2008) [1923]. The Inimitable Jeeves (Reprinted ed.). Arrow Books. ISBN 978-0099513681.
Wodehouse, P. G. (2008) [1925]. Carry On, Jeeves (Reprinted ed.). Arrow Books. ISBN 978-0099513698.
Wodehouse, P. G. (2008) [1930]. Very Good, Jeeves (Reprinted ed.). Arrow Books. ISBN 978-0099513728.
Wodehouse, P. G. (2008) [1934]. Right Ho, Jeeves (Reprinted ed.). Arrow Books. ISBN 978-0099513742.
Wodehouse, P. G. (2008) [1938]. The Code of the Woosters (Reprinted ed.). Arrow Books. ISBN 978-0099513759.
Wodehouse, P. G. (2008) [1949]. The Mating Season (Reprinted ed.). Arrow Books. ISBN 978-0099513773.
Wodehouse, P. G. (2008) [1954]. Jeeves and the Feudal Spirit (Reprinted ed.). Arrow Books. ISBN 978-1-78033-824-8.
Wodehouse, P. G. (2008) [1971]. Much Obliged, Jeeves (Reprinted ed.). Arrow Books. ISBN 978-0099513964.
Wodehouse, P. G. (2008) [1974]. Aunts Aren't Gentlemen (Reprinted ed.). Arrow Books. ISBN 978-0099513971.