"The Face upon the Barroom Floor", aka "The Face on the Floor" and "The Face on the Barroom Floor", is a poem originally written by the poet John Henry Titus in 1872. A later version was adapted from the Titus poem by Hugh Antoine d'Arcy in 1887 and first published in the New York Dispatch.  


== History ==
According to d'Arcy, the poem was inspired by an actual happening at Joe Smith's saloon at Fourth Avenue and 14th Street in Manhattan. When it was reprinted in a d'Arcy collection, he wrote a preface explaining the confusion of the two titles:

My only excuse for offering this little book is the fact that my friends want to get a few stories out of my scrap book—so here they are. One popular mistake I desire to rectify. When I wrote "The Face Upon the Floor," which was in 1887, I had no idea that it would receive the favor which it has. The popularity of the story induced the publisher of a Bowery Song Sheet to issue a song which was a bad plagiarism and, to get away from my copyright, called it "The Face on the Barroom Floor." Strange to say, the public has accepted the latter title, which is not correct. This book contains the true and original story. D'Arcy.


== Poem ==
Written in ballad form, the poem tells of an artist ruined by love; having lost his beloved Madeline to another man, he has turned to drink. Entering a bar, the artist tells his story to the bartender and to the assembled crowd. He then offers to sketch Madeline's face on the floor of the bar but falls dead in the middle of his work. Here is the full text:

'Twas a balmy summer's evening and a goodly crowd was there,
Which well-nigh filled Joe's barroom on the corner of the square,
And as songs and witty stories came through the open door
A vagabond crept slowly in and posed upon the floor.“Where did it come from?” someone said, “The wind has blown it in.”
“What does it want?” another cried, “Some whiskey, rum or gin?”
“Here Toby, sic him, if your stomach is equal to the work —
I wouldn't touch him with a fork, he’s as filthy as a Turk.”This badinage the poor wretch took with stoical good grace;
In fact, he smiled as though he thought he'd struck the proper place.
“Come boys, I know there's kindly heart among so good a crowd —
To be in such good company would make a deacon proud.”“Give me a drink — that’s what I want — I'm out of funds you know;
When I had cash to treat the gang, this hand was never slow.
What? You laugh as though you thought this pocket never held a sou:
I once was fixed as well my boys, as anyone  of you.”“There thanks, that’s braced me nicely; God Bless you one and all;
Next time I pass this good saloon, I'll make another call.
Give you a song? No, I can't do that, my singing days are past;
My voice is cracked, my throat's worn out, and my lungs are going fast.“Say, give me another whiskey, and I'll tell you what I'll do —
I'll tell you a funny story and a fact I promise too.
That I was ever a decent man, not one of you would think;
But I was, some four or five years back. Say, give me another drink.“Fill 'er up, Joe, I want to put some life into my frame —
Such little drinks, to a bum like me are miserably tame;
Five fingers! — there, that's the scheme — and corking whiskey too.
Well, here's luck, boys; and landlord, my best regards to you.“You’ve treated me pretty kindly, and I'd like to tell you how
I came to be the dirty sot, you see before you now.
As I told you once, was a man with muscle, frame and health,
And, but for a blunder, ought to have made considerable wealth.“I was a painter — not one that daubed on bricks or wood,
But an artist, and for my age I was rated pretty good,
I worked hard at my canvas and was bidding fair to rise,
For gradually I saw the star of fame before my eyes.“I made a picture, perhaps you've seen, 'tis called the 'Chase of Fame.'
It brought me fifteen hundred pounds and added to my name.
And then I met a woman — now comes the funny part —
With eyes that petrified my brain, and sunk into my heart.“Why don't you laugh? 'Tis funny, that the vagabond you see
Could ever love a woman and expect her love for me;
But 'twas so, and for a month or two, her smiles were freely given,
And when her loving lips touched mine it carried me to heaven.“Did you ever see a woman for whom your soul you'd give,
With a form like the Milo Venus, too beautiful to live;
With eyes that would beat the Koh-i-noor, and a wealth of chestnut hair?
If so, 'twas she, for there never was another half so fair.“I was working on a portrait, one afternoon in May,
Of a fair haired boy, a friend of mine, who lived across the way.
And Madeline admired it, and much to my surprise,
Said she'd like to know the man that had such dreamy eyes.“It didn't take long to know him, and before the month had flown
My friend had stolen my darling, and I was left alone.
And, ere a year of misery had passed above my head.
The jewel I had treasured so had tarnished, and was dead.“That's why I took to drink, boys. Why, I never saw you smile,
I thought you'd be amused, and laughing all the while.
Why, what's the matter friend? There's a teardrop in your eye.
Come, laugh like me; 'tis only babes and women that should cry.“Say boys, if you give me just another whiskey, I'll be glad,
And I'll draw right here a picture, of the face that drove me mad.
Give me that piece of chalk with which you mark the baseball score —
And you shall see the lovely Madeline upon the barroom floor.Another drink, and with chalk in hand, the vagabond began,
To sketch a face that well might buy the soul of any man.
Then, as he placed another lock upon that shapely head,
With a fearful shriek, he leaped and fell across the picture — dead!


== Adaptations and cultural legacy ==
Keystone Studios used the poem as the basis for the 1914 short film The Face on the Barroom Floor starring Charles Chaplin. 
In the 1941 film Louisiana Purchase, Bob Hope conducts a Senate filibuster which ends with his reading the entire poem while drawing a picture on the floor.
In Mad #10 (April 1954), the poem was illustrated by Jack Davis and Basil Wolverton, the latter doing a face that is kin to his "Lena the Hyena" . 
The poem was put to song by country music stars Tex Ritter for his 1959 Blood on the Saddle album and Hank Snow on his Tales of the Yukon album (1968). 
The poem was the inspiration for The Face on the Barroom Floor painting by Herndon Davis in the Teller House Bar in Central City, Colorado, and that painting inspired a chamber opera by Henry Mollicone. 
The title is mentioned in the lyrics of the Paul Francis Webster's song "It's Harry I'm Planning to Marry" (from the 1953 Warner Bros. musical Calamity Jane), despite the fact that it is set in Deadwood, 1876, which actually predates the poem by some 11 years. The song is delivered by the character Katie Brown (Allyn McLerie), who sings:

He's the one that I truly adore.
I'm numb,
I succumb
When he renders the face on the barroom floor.The poem is specifically referenced in the last verse of "The Mount Holyoke Drinking Song":

The face upon the barroom floor
I'd rather be than dull once more.
We're here,
Bring on the beer.
Who cares, we'll be sober tomorrow.
Here's a toast to old MHC!This version of the poem was also performed by the vocal group The Blazers on their album Drinking Songs Sung Under the Table released by ABC/Paramount in 1959In the 1970 rock tour film Mad Dogs & Englishmen, starring Joe Cocker and Leon Russell, the band, family, friends, and crew have a large picnic in a field in or near Tulsa, Oklahoma. Road manager Sherman "Smitty" Jones recites the last several verses of "The Face Upon the Barroom Floor" from memory for the surprised picnickers (those shown on camera including Carl Radle, Bobby Keys, and Joe Cocker), without telling them what it is. When he is done the camera follows him off to the side of the gathering, where one woman names the poem. Smitty says, "Glad someone recognized it." (In the song Leon Russell wrote memorializing the tour, "The Ballad of Mad Dogs & Englishmen," which plays during the film credits, Russell sings, "The bus is here, bring the beer. Sherman's reading Shakespeare," though the poem is of course not by Shakespeare.)


== References ==


== Listen to ==
"THE FACE ON THE BARROOM FLOOR" by THOMAS J. McCARTHY JR
The Face on the Barroom Floor read by Franklyn MacCormack
The Face Upon the Barroom Floor performed by storyteller Chris Abair
"The Face Upon the Barroom Floor" recited in part in the film Mad Dogs & Englishmen"THE FACE ON THE BARROOM FLOOR" by THOMAS J. McCARTHY JR
"The Face On The Barroom Floor" Begum - 1997


== External links ==
Image of the face with text of poem