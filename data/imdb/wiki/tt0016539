Win, Lose or Draw is an American television game show that aired from 1987 to 1990 in syndication and on NBC. It was taped at CBS Television City (one of the few non-CBS game shows to tape there), often in Studios 31, 33, and 43 at various times. It was co-produced by Burt & Bert Productions (headed by Burt Reynolds and Bert Convy, the original host of the syndicated version) and Kline & Friends for Disney's Buena Vista Television. It has also had two versions on The Disney Channel: Teen Win, Lose or Draw from 1989 to 1992, and a revived version known as Disney's Win, Lose or Draw which aired in 2014.
The set for the original Win, Lose or Draw was modeled after Burt Reynolds' living room.


== Broadcast history ==
A pilot with Bert Convy as host was taped on November 2, 1986 at CBS Television City in Hollywood, California, and featured Loni Anderson, Betty White, Burt Reynolds, and Tony Danza playing the game, with Rod Roddy announcing. The pilot, produced by Reynolds and Convy, garnered the interest of both the NBC network and various local stations nationwide; thus, two separate editions of the program were sold and produced with production beginning in June 1987.
On September 7, 1987, Win, Lose or Draw was launched with Kline and Friends joining Burt & Bert Productions as a second production company. Vicki Lawrence hosted the edition produced for NBC, which inherited the 11:30 a.m. Eastern timeslot that had been occupied by Scrabble for three years prior to its premiere. The syndicated edition, premiering that same day, was hosted by Bert Convy. Bob Hilton announced for the daytime series, with Gene Wood performing those duties for the syndicated series, while Hilton occasionally filled in on the latter version, and Johnny Gilbert and Dean Goss did so on the daytime show.Lawrence, with the exception of a month in 1988 when Sally Struthers hosted in her place, hosted the daytime Win, Lose or Draw for its entire two-year run. The daytime version faced off against the second half of the hit CBS game show The Price Is Right and the ABC talk show Home (which expanded to an hour in January 1989). NBC canceled the daytime series in the summer of 1989, and its final episode aired on September 1 of that year. Meanwhile, Robb Weller began hosting the third season of the syndicated series later that month after Convy opted to host a new production for his company, 3rd Degree. The final new episode aired in May 1990; reruns of the series continued to air until the show was withdrawn from syndication on August 31, 1990.
Reruns of the syndicated version aired on the USA Network from January 1, 1991 to December 24, 1992, and on Game Show Network from April 15, 2002 to March 14, 2004.
During its run, the syndicated Win, Lose or Draw made several road trips including Hawaii, Central Park in New York City, Burt Reynolds' ranch in Jupiter, Florida, Walt Disney World and Disneyland.


== Gameplay ==
Two teams, men versus women, each composed of two celebrities and one contestant (or a celebrity and two college students in the College Tournament in 1988), took turns guessing a phrase, title or thing that one teammate was drawing on a large pad of paper with markers, each team sat on a couch on a set designed after Burt Reynolds' actual living room at the time. The team member doing the drawing could not speak about the subject in his or her drawing and could not use letters, numbers, or symbols. If one of these illegal clues was used, any money won in that puzzle was split between the two teams. However, if a non-drawing team member mentioned a word that was part of the answer, their teammate at the sketch pad was then allowed to write it down.
In the first three rounds, each team had one minute to solve a puzzle, earning $200 for a correct guess. At the thirty-second mark, a doorbell sounded (or fanfare during the College Tournament in 1988), and the drawing player had the option of handing the marker off to one of his/her teammates, but the puzzle value would then be cut in half. If the team did not guess within the time limit, the opposing team was given one chance to confer and guess. If they gave the correct answer, they were awarded the money; if not, no money was awarded. By 1989, the first round was later changed to have the drawing contestant sketch a series of clues to a puzzle, one clue at a time. If the team guessed the puzzle from the identified clues, they scored $200.
Following round three, one player for each team was nominated to draw clues in a 90-second speed round. The topics for drawing were simpler for this round compared to those in previous rounds. Each correct guess was worth $100, and the team could only pass twice. The speed round started with the team that was ahead. The team with the most money at the end of the game won, and the contestant on the winning team received $1,000 in addition to the money they had already earned. If both teams were tied at the end of the speed round, each contestant earned $500. By the end of the daytime version's run, the speed round was decreased to 60-seconds and $50 a word.


=== Bonus Round ===
The daytime series underwent another significant change towards the end of its run, which was eliminating the $1,000 bonus and instead replacing it with a bonus round, which the series had not seen in either iteration to this point. This change coincided with another change added at the same time that involved two contestants and a celebrity on each team.
The bonus round was played similar to the speed round, except that players were allowed to pass multiple times. The first word was worth $50 and each correct answer that followed doubled the bank. Passing resulted in the bank resetting to zero.
Originally, the bonus was played with as many words as possible within the time limit. Under this format a team was able to win $25,600 in one round, guessing ten words without a miss. Afterwards, the scoring format was adjusted, this time, if the winning team correctly identified seven words within the time limit of 90 seconds, regardless of how many passes were used, they won $5,000.


=== Audience game ===
If there was extra time at the end of the show, an audience member would be called on stage and given the opportunity to sketch a subject for either the men's or women's team to guess in 60 seconds, much like the main rounds, with $100 awarded if the chosen team was able to identify the subject.


=== 1989–1990 changes ===
The third and last season of the syndicated Win, Lose or Draw saw a significant format overhaul itself, as the front game was altered, and both the shortened speed round and bonus round were added.
The object of the first two rounds was the same. However, the drawing team was not allowed to switch partners at any point. A guess could not be made until twenty-five of the sixty seconds had elapsed, and if the team guessed the puzzle before thirty seconds had elapsed the civilian contestant won $200. If not, the puzzle was played for $100. A successful steal by the opposing team was only worth $50.
If the game ended in a tie, a tie-breaker was played with the last team to play the speed round going first. The player at the board was given the choice of two words and began drawing, trying to convey the word as fast as he/she could. Once the word was guessed, the opposing team had to guess their word in a faster time. Doing so won the game; if not, the first team won the game.
At this point, the show began to use returning champions, who stayed on for ten days or until defeated, whichever came first.


== Disney Channel versions ==


=== Teen Win, Lose or Draw ===
From April 29, 1989 to April 28, 1990, and again from September 10, 1990 to September 26, 1992, Disney Channel aired a version called Teen Win, Lose or Draw. This version was hosted by Marc Price. Jay Wolpert produced the first season, which taped at the Disney-MGM Studios in Orlando, with Stone-Stanley Productions taking over for the rest of the run, at which time production also moved to the CBS Studio Center in Los Angeles. Originally the show aired on Saturdays at 6:30 p.m. and Sunday mornings at 11:30 a.m. Rotating as announcers during the first season were Brandy Brown, Chase Hampton and Tiffini Hale from The Mickey Mouse Club, with Mark L. Walberg taking over that duty for the final two seasons. Teams were made up of two teenage contestants (one each of two boys and two girls), and a teenage celebrity. Gameplay was largely identical to the original run, with the following differences:

Round 1 – The Clue Round: A player from each team draw as many words within 60 seconds. Each of the words was a clue to a puzzle – a person, place, thing, event, etc. The team in control had the first chance to answer, if they were unable to give the correct answer, the opposing team could guess to win the points.
Round 2 – The Phrase Round: The team is given a category, with the phrase based in that category. As in the original, the clue-giver could hand off to a teammate after 30 seconds.
Round 3 – The Speed Round: Played identically to the adult version, with the trailing team going first (or the team that went first in round one playing first if the score was tied). A grand prize was given to the winning team, with the losing team getting a consolation gift.
If the score was tied following the Speed Round, each team would play another speed round with 20 seconds on the clock. The team that got more in the 20 seconds won. If the tie persisted after this overtime round, each team would try to guess one word as fast as possible (with a maximum of 60 seconds), and the team that was faster won.


=== Disney's Win, Lose or Draw ===

In April 2013, the Disney Channel announced a new version of Win, Lose or Draw, to be hosted by Justin Willman, which premiered on January 17, 2014. As with Teen Win, Lose or Draw, the two teams on each program are made up of two young contestants plus a teenage celebrity (this time, from a Disney Channel or Disney XD program). New motion-control technology is featured.The 2014 version featured the following rounds (all 90 seconds):

Get a Clue – For each word, Willman announces a clue, leading to the answer, with additional clues provided every 10 seconds. Each correct answer is worth 10 points, with team members alternating after each word is either guessed or passed.
Draw-obstacle Course – The clue givers endured a variety of challenges while drawing, such as using an oversized stylus, drawing on a spinning touch-screen board, the current drawing disappearing if the clue giver lifts his/her finger, or drawing while standing on a vibrating stool. The challenges rotated from episode to episode, and three were presented per round. As before, correct answers were worth 10 points.
Fill in the Blank – Adapted from the original final round, Willman read a pun-styled clue with a blank, with the clue-giver drawing the word that filled in the blank. As in all previous versions, the team that was trailing went first (or the team that went first in round one going first if the score was tied), and clues were worth 20 points, with as many clues played as possible within the time limit.Passing at any time on a word ended play on that word.
The team in the lead after three rounds advanced to the 90-second bonus round. Both celebrities were clue-givers, and drew one- and two-word phrases, much like the original version. As in the Draw-stacle Course round, clue-givers might have to draw clues using a wand-type stylus while having his/her back turned to the board, draw while on a revolving chair and drawings moving from screen to screen at certain intervals (this changed from episode to episode). Contestants could pass at any time on a word, but could not score on it, even if they later came up with the correct answer. Each word or phrase guessed correctly won a gift package (each concealing a prize, where said item was awarded to both contestants), and a grand prize was awarded for guessing all four clues correctly.
If the game ended in a tie, host Willman drew one picture himself; as soon as a team was able to identify the subject, they had to buzz-in. A right answer won the game, but a wrong answer gave the opponents the win.


== Home versions ==


=== Board game ===
Milton Bradley Company created its version in 1987. It could be played like the TV show, or a variation of the game with pawns and a game board. Party, Junior, and Travel Junior editions were produced, plus a Refill Pack for the game. All contestants who appeared on the show received a copy of the Party edition, as did select audience members.


=== Computer and video games ===
Hi Tech Expressions released two editions of the DOS version of the game in 1988, as well as a "Junior" version, followed by a version for the Nintendo Entertainment System in 1989. Both versions of this party game featured a scene set in a living room, with the game contestants (representing real-life players) seated on opposite couches, much like the television show. While the game system drew a picture on the screen, one of the players would have a limited amount (60 seconds for the main game, and 90 for the speed round) of time to type in the word or phrase represented by the image. If the player typed in the incorrect answer, a player on the opposing team would have an opportunity to type the correct answer (in single-player games, the game system would type a random incorrect answer). The team that typed the correct answer would win money for that round, and the team that earned the most money at the end of the game won.
A "plug-and-play" console version was released by Senario in 2005, unlike the earlier computer and console adaptations, this one allowed players to actually draw the subjects, using an electronic pen, for their teammates to guess.


== Other versions ==


=== Canadian version ===
Canadian channel TVA aired a French language version called Fais-moi un dessin (Make me a drawing) hosted by Yves Corbeil from 1988 to 1991.


=== French version ===
French channel Antenne 2 aired their version of WLOD called Dessinez c'est gagné! (Draw is Won!) hosted by Patrice Laffont from 1989 to 1990, they even had a kids version of the show called Dessinez c'est gagné! junior (Draw is Won! junior) hosted by Éric Galliano from 1991 to 1993.


=== Scotland (Gaelic) version ===
A Scottish Gaelic language version aired on STV (not the same version as Win, Lose or Draw (UK game show) under the name De Tha Seo? (What's This?) with Neen Mackay followed by Cathy Macdonald and lastly, Norm Maclean as hosts from 1990 to 1994.


=== UK version ===


== International versions ==


== References ==


== External links ==
Official website
Win, Lose or Draw on IMDb
Win, Lose or Draw at MobyGames
Template:WinDrawWin