The following is a list of major characters in The Jetsons, an American animated comic science fiction sitcom produced by Hanna-Barbera Productions and first broadcast in prime-time on ABC as part of the 1962–63 United States network television schedule. Additional episodes were produced from 1985 to 1987 in syndication, with the same cast of characters.


== The Jetsons family ==


=== George Jetson ===

George J. Jetson (voiced by George O'Hanlon in the TV series, Jeff Bergman since 1990, Wally Wingert in Harvey Birdman, Attorney at Law) is a fictional character and the 40-year-old head of the Jetson family. He is the husband of Jane Jetson and the father of teenage daughter Judy and elementary school aged son Elroy.George resides with his family in the Skypad Apartments in Orbit City, in a future with the trappings of science fantasy depictions of American life in the future, such as robot servants, flying saucer-like cars, and moving sidewalks. All the apartment buildings are set on giant poles, resembling Seattle's Space Needle; the ground is almost never seen, though in Jetsons: The Movie, it is suggested that the earth is extremely polluted, thus everyone lives in the sky.
When George was a kid he had to fly through ten miles of asteroid storms to go to Orbit High School, where he was the star pitcher of its Spaceball team. At Orbit High School, He was labeled as an outsider and a geek. George is now an employee at Spacely's Space Sprockets, a manufacturer of "sprockets" and other high tech equipment. His job title is "digital index operator." His boss is Cosmo G. Spacely, noted for being short in both height and temper. Spacely usually treats his employees (particularly George) in a rather tyrannical fashion. George's job primarily requires him to repeatedly push a single button (or on occasion a series of buttons) on a computer (named RUDI {Short for: Referential Universal Digital Indexer} in the 1980s series of Jetsons episodes). George complains of his heavy work load: pushing a button as many as five times for three hours, three days a week. Often, Mr. Spacely will fire George in a fit of anger, only to hire him back by the end of the same episode.
Physically, George is a rather slim man of average height with short red hair and a cartoonishly large nose. His personality is that of a well-meaning, caring father, but he is often befuddled and stressed out by the problems of both his work and family lives. As The Jetsons was partially based on the comic strip Blondie, George himself was probably based on that strip's lead character, Dagwood Bumstead. His wife Jane is voiced by Penny Singleton who played the movie version of Blondie in the 1930s to 1950s.George's most famous catchphrase is "Jane! Stop this crazy thing!" seen at the end credits of the 1960s Jetsons episodes, but is also known for frequently uttering the phrase "Hooba-dooba-dooba!" to express wonder or astonishment (probably inspired by Fred Flintstone's similar-sounding "Yabba Dabba Doo!!").
O'Hanlon was known for his portrayal of everyman characters in film and television. O'Hanlon once said of his character: "George Jetson is an average man. He has trouble with his boss, he has problems with his kids, and so on. The only difference is that he lives in the next century." O'Hanlon last did the voice for George Jetson in Jetsons: The Movie, which was released posthumously.
In the Harvey Birdman, Attorney at Law episode "Back to the Present", George leads the Jetsons in returning to the past to sue the planet for causing global warming.
George Jetson also appeared in a cameo appearance on The Powerpuff Girls episode "Mime for a Change."
In The Jetsons & WWE: Robo-WrestleMania!, George Jetson is assigned by Mr. Spacely to supervise a project that involves robots drilling through the surface of the Earth.
George Jetson also appears at the Cedar Fair Entertainment Company and formerly Universal Studios Florida as a meetable character seen in 1996 video called "Kids for Character".
He also appears on Kanye West's music video "Heartless". He is also seen along with his family, Rosie, and Mr. Spacely in a MetLife commercial that aired in 2012.
The inspiration for George's name sake (aside from its voice actor) is believed to be a tribute to the prolific Danish designer, Georg Jensen, who defined modern design in the early and mid twentieth century.


=== Jane Jetson ===
Jane Jetson (voiced by Penny Singleton in the TV series, B. J. Baker singing voice, Lauri Fraser in The Jetsons: The Best Son, Diane Michelle in Harvey Birdman, Attorney at Law, Grey DeLisle in The Jetsons & WWE: Robo-WrestleMania!) is George's 33-year-old wife, mother of their two children, and a homemaker. Because she is so much younger than George she would have been only 17 at the time of Judy's birth. Jane is obsessed with fashion and new gadgetry. Her favorite store is Mooning Dales. She is also a dutiful wife who always tries to make life as pleasant as possible for her family. Although she is usually pleasant natured, she is quite protective of her family and she can be angered whenever they are threatened. She is also unafraid to stand up to Mr. Spacely, especially when he antagonizes her family and her husband.
Outside of the home, she is a member of the Galaxy Women Historical Society and is a fan of Leonardo de Venus and Picasso Pia. Although this wife of the future has both a robot maid and automated apartment appliances, even pushing buttons for housework gets to be too much for her and she has to take a vacation ("Dude Planet"). She has terrible driving skills, turning her driving instructor, a gangster and her husband into nervous wrecks ("Jane's Driving Lesson").
Jane appears, with Wilma Flintstone and Velma Dinkley, in a commercial for Dove shampoo. She even appears on Kanye West's music video "Heartless". In one episode of Animaniacs, a female space alien resembling Jane appears twice; once as a receptionist, and then again when a male alien trapped on a treadmill calls out the famous catchphrase "Jane! Stop this crazy thing!" where she comes to his aid.
She was included in Yahoo!'s Top 10 TV Moms from Six Decades of Television.


=== Judy Jetson ===

Judy Jetson (voiced by Janet Waldo in the TV series, Tiffany in the film, Danica McKellar in The Jetsons & WWE: Robo-WrestleMania!) is the 16-year-old daughter of George and Jane Jetson who acts just like a normal teenage girl only with more futuristic tastes.
Despite Judy still keeping modern-day "teenage girl" likes and dislikes, such as seemingly never-ending conversations on the telephone and shopping for futuristic outfits, she does not live exactly like a modern teenager.  For example, she enjoys taking advantage of the many technology gadgets at her grasp such as controllable zero-gravity switches (made for accomplishing popular dance moves of the show's time or for other uses). She also gives a summary of how her day went including her problems to a floating robotic diary, appropriately named, "DiDi" (voiced by Brenda Vaccaro).
Judy is also very fond of a futuristic rock singer named "Jet Screamer", whom George at first despised until Judy won a date with the heartthrob singer in a contest.  In the contest, girls who wanted to win a date with Jet Screamer were to write songs, and the writer of the winning song would win the date. Judy won because George replaced her original song with her younger brother Elroy's secret code. George came along to try to stop the date but failed and wound up as the drummer for the song and soon took a liking for Jet Screamer and his music.
Judy is depicted wearing a deep pinkish-purple, futuristic outfit and, like a few other Hanna-Barbara cartoon characters (such as Bamm-Bamm Rubble from The Flintstones and some background characters in various cartoons) has white hair that is colored to resemble platinum blonde or bleached blonde hair, which she wears in a ponytail on top of her head. She attends Orbit High School, which her father had also attended.
Judy went unvoiced in her appearance in Harvey Birdman, Attorney at Law, due to the fact a voice actress could not be secured for her (Waldo by this point was in her 90s and retired from voice acting).


=== Elroy Jetson ===
Elroy Jetson (voiced by Daws Butler in the TV series, Billy West in a WBCN commercial, Patric Zimmerman in the film, Jeff Bergman in two RadioShack commercials, The Jetsons: Father & Son Day and The Jetsons: The Best Son, Tom Kenny in Harvey Birdman, Attorney at Law, and Trevor Devall in The Jetsons & WWE: Robo-WrestleMania!, 2017-present) is a 6 1/2-year-old (self-reported in the first episode) boy who is the younger of the two children in the Jetson family. He is highly intelligent and an expert in all space sciences. Elroy attends Little Dipper School where he studies space history, astrophysics and star geometry. He is a mild mannered and good child.


== Jetson family household ==


=== Astro ===
Astro (voiced by Don Messick in the TV series, Scott Innes in several RadioShack commercials, Wally Wingert in Harvey Birdman, Attorney at Law, Frank Welker in The Jetsons & WWE: Robo-WrestleMania, 2017-present) is the Jetson family's Great Dane. He was designed by Iwao Takamoto, and originally voiced by Don Messick. Despite the stress he tends to cause him, George often regards Astro as his genuine best friend. Astro, despite being clumsy and dim-witted, was very loyal (to a fault) to the Jetsons, particularly George and Elroy. He was more advanced than present-day dogs, in that he had a rudimentary grasp of the English language. In a recurring gag, while George is walking Astro on a gravity-suspended treadmill, Astro begins chasing a cat that stumbles onto it, eventually forcing it to go at speeds too fast for George to keep up with, trapping him. This led to George's trademark phrase "Jane! Stop this crazy thing!".
Don Messick would usually give Astro a speech pattern involving replacing the first letter of any word with an R, such as "I love you, George" becoming "I ruv roo, Reorge". Astro is similar to Scooby-Doo in the cartoon Scooby-Doo, Where Are You!; which was also voiced by Messick (of the two voices, Astro's was slightly higher and raspier than Scooby's).

Astro was introduced in the season one episode, "The Coming of Astro". When Jane, Judy, and Elroy proposed keeping him to George, he was against it, claiming an apartment is no place for a dog. In an effort to make his family happy, he gets an electronic dog, 'Lectronimo. Through mishaps, 'Lectronimo fails to catch a burglar whom Astro inadvertently stops. The family gives 'Lectronimo to the police and keeps Astro. In Season 1, Episode 16, a millionaire named J.P. Gottrockets shows up and claims to be Astro's original owner and his original name was Tralfaz. Astro had no memory of his past life with the millionaire, but understood why he ran away, with Elroy rescuing him from the dog catcher - he didn't want wealth and privilege, he wanted a real family. After seeing how much he loved the Jetsons, Gottrockets gives Astro back to them. Despite this, Astro makes an appearance in the ending credits prior to the episode. Astro makes five cameo appearances as a portrait in the music video for the 2008 Kanye West song "Heartless", which takes place in West's actual apartment den. Astro was also in 11 episodes of a 1981 spin-off called Astro and the Space Mutts.


=== Rosie ===

Rosie   (voiced by Jean Vander Pyl in the TV series, Tress MacNeille in The Jetsons & WWE: Robo-WrestleMania!, 2008-present) is the Jetson family's robotic maid and housekeeper. Rosie is depicted as wearing a frilly apron, and was often seen using a separate vacuum cleaner. Her torso is mounted atop a single leg and she rolls about on a set of caster wheels. She frequently calls George Jetson "Mr. J". In seasons 1 and 3 her name  is spelled as Rosey 
on the episode title cards.
Rosie was an old demonstrator model hired by the Jetson family from U-Rent a Maid. The series' first episode, "Rosey the Robot", gives her model number as XB-500. She was in fact outdated when introduced, and her obsolescence was the focus of several episodes focused on her. In the episode "Rosie's Boyfriend," we learn she has a boyfriend, the robot Mac, a helper for Henry Orbit. Rosie clearly cares a great deal for Mac, but is obviously the more intelligent between them, often chiding him for his childish behavior.
She was spoofed in the Futurama movie Bender's Game, with a robot that looked like her saying "Everything must be clean. Very clean. That's why the dog had to die. He was a dirty dog. Also that boy Elroy. Dirty. Dirty." She was voiced by Tress MacNeille in this parody.
In 2017, a Jetsons comic was published by DC Comics. In it, Rosie is re-imagined as George's mother who was placed in a robotic body by Nexlyfe. She only informed Judy that she was going through with the procedure prior to it.


=== Orbitty ===
Orbitty (voiced by Frank Welker in the TV series, Diane Michelle in Harvey Birdman, Attorney at Law) is the 2nd pet of the Jetson family. Orbitty is an alien with spring-like legs and suction-cup feet. He has the ability to express his emotions by changing color. He is also excellent in fixing and repairing things. This character was introduced in the 1980s version of the series, but didn't appear in the third season (aside from one cameo) or any of the movies that came after it.


== Other characters ==


=== Mr. Spacely ===
Cosmo G. Spacely (voiced by Mel Blanc in the TV series, Frank Welker as a teenager in The Jetsons Christmas Carol, Jeff Bergman in the 1990s-Present; and Greg Burson in the "Bloopers of the Cartoon Stars" (1997) reel) is George's moneygrubbing and arrogant boss.
Spacely is president and owner of 'Spacely Space Sprockets', where George Jetson works. He frequently yells at George for perceived poor work performance, or just when he feels like it. A typical videophone call from Spacely starts with him yelling "JETSON!!!" He attempts to fire George for the slightest offense, though Spacely gives George his job back at the end of the episode or the beginning of the next episode. Though money is often the sole objective of his life, Spacely is not completely heartless, and will at times empathize or go out of his way to genuinely help George (albeit rarely). Spacely also often finds excuses to get involved in George's personal life.
He appears as a antihero in the TV series


=== Mrs. Spacely ===
Mrs. Petunia Spacely (voiced by Jean Vander Pyl) is Mr. Spacely's wife whom he loves, but lives in constant fear of. In episode 1 of season 1, Mr. Spacely calls her by the name Stella.


=== Arthur Spacely ===
Arthur Spacely (voiced by Dick Beals in earlier episodes, Catherine Thompson in season 3) is Mr. Spacely's son who is extremely snobbish and is a rival to Elroy.


=== R.U.D.I. ===
R.U.D.I. (voiced by Don Messick) is George's work computer as well as his best friend in the workplace. R.U.D.I. is sentient, free thinking and openly fond of George, recognizing his value as an employee and friend. His name is an acronym for Referential Universal Digital Indexer. He has a human personality and is a member of the Society Preventing Cruelty to Humans. Though capable and loyal, R.U.D.I is implied to be antiquated technology, as George mentions his model is no longer made.
In the episode "Family Fallout" the Jetsons are up against the Spacelys on a game show. The last question to come up was "what does R.U.D.I. stand for?" George's response was Referential Universal Differential Indexer – this was accepted by the game show host as the correct answer, even though earlier episodes had it as Digital instead of Differential.


=== Mr. Cogswell ===
Mr. Cogswell (voiced by Daws Butler in the TV series, Jeff Bergman in the 1990s-Present) is Spacely's big competitor. He owns Cogswell's Cosmic Cogs company and causes a lot of trouble for Spacely and George. To a lesser extent, Cogswell is another of the series' antagonists. He and Spacely are always finding ways to bring each other's businesses down. Cogswell has often tried to steal Spacely's ideas and make them his own to gain an advantage (only for it to backfire on both bosses). Just like Spacely, he is also cheap and greedy and not above firing his employees when any little thing goes wrong. Mr. Cogswell's first name of "Spencer" is revealed in the 1980s version of The Jetsons. He bears something of a resemblance to Fred Flintstone's boss, Mr. Slate.


==== Harlan ====
Harlan (voiced by Howard Morris) is Mr. Cogswell's sycophant right-hand man.
His name may be a nod to science-fiction writer Harlan Ellison.


==== Sentro ====
Sentro is a robotic dog used by Mr. Cogswell.
In his first appearance in "Solar Snoops," Sentro was operated by the Galactic Sneak where he and Cogswell controlled Sentro into stealing the prototype Microchip Cookie that Mr. Spacely was working on where Sentro posed as a security guard dog that was delivered to Mr. Spacely.
In The Jetsons Meet the Flintstones, Sentro is used again by Mr. Cogswell to steal the information about the prehistoric car that Mr. Spacely plans to market. Sentro is thwarted by both the Flintstones and the Jetsons.
Sentro later appeared as a boss in the video game The Jetsons: Cogswell's Caper.


=== Henry Orbit ===
Henry Orbit (voiced by Daws Butler) is the apartment building superintendent of the Skypad Apartments. He has an assistant robot M.A.C.C. who is also dating Rosie the Jetson's housekeeper robot. He is known for giving George advice and is a part-time inventor, although both more often than not tend to backfire on him.


==== M.A.C.C. ====
M.A.C.C. (voiced by Don Messick) is the robotic assistant of Henry Orbit who started dating Rosie after Elroy figured out that they fell in love.


=== Montague Jetson ===
Montague Jetson (voiced by Howard Morris) is the 110-year-old grandfather of George Jetson and the great-grandfather of Elroy and Judy.


=== Marcia Van Marsdale ===
Marcia Van Marsdale (voiced by Catherine Thompson) is Judy's high school rival.  Marcia is a snobbish girl who tries to outdo Judy, be it by trying to get a boy that Judy fancies, or beating Judy and her father at the Father-Daughter Dance.


== References ==


== Further reading ==
Hanna-Barbera Cartoons, by Michael Mallory, 1998, published by Hugh Lauter Levin Associates, Inc., distributed by Publishers Group West. ISBN 0-88363-108-3


== External links ==
The Jetsons at Toonopedia