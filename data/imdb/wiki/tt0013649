A superstition is "a belief or practice resulting from ignorance, fear of the unknown, trust in magic or chance, or a false conception of causation" or "an irrational abject attitude of mind toward the supernatural, nature, or God resulting from superstition." Often, it arises from ignorance, a misunderstanding of science or causality, a belief in fate or magic, or fear of that which is unknown. It is commonly applied to beliefs and practices surrounding luck, prophecy, and certain spiritual beings, particularly the belief that future events can be foretold by specific (apparently) unrelated prior events. The word superstition is often used to refer to a religion not practiced by the majority of a given society regardless of whether the prevailing religion contains alleged superstitions.

Identifying something as superstition is generally pejorative. Items referred to as such in common parlance are commonly referred to as folk belief in folkloristics.


== Etymology ==
The word superstition was first used in English in the 15th century, borrowed from French superstition which continues Latin superstitio.
The earliest known use as an English noun is found in Friar Daw's Reply (ca. 1420), where the foure general synnes are enumerated as Cediciouns, supersticions, þe glotouns, & þe proude.
While the formation of the Latin word is clear, from the verb super-stare, "to stand over, stand upon; survive", its original intended sense is less clear. It can be interpreted as "‘standing over a thing in amazement or awe", but other possibilities have been suggested, e.g. the sense of excess, i.e. over scrupulousness or over-ceremoniousness in the performing of religious rites, or else the survival of old, irrational religious habits.The earliest known use as a noun is found in Plautus, Ennius and later by Pliny, with the meaning of art of divination. From its use in the Classical Latin of Livy and Ovid, it is used in the pejorative sense that it holds today, of an excessive fear of the gods or unreasonable religious belief; as opposed to religio, the proper, reasonable awe of the gods. Cicero derived the term from  superstitiosi, lit. those who are "left over", i.e. "survivors", "descendants", connecting it with excessive anxiety of parents in hoping that their children would survive them to perform their necessary funerary rites. While Cicero distinguishes between religio and superstitio, Lucretius uses only the word religio (only with pejorative meaning). Throughout all of his work, he distinguished only between ratio and religio.
The Latin verb superstare itself is comparatively young, being "perhaps not ante-Augustan", first found in Livy, and the meaning "to survive" is even younger, found in late or ecclesiastical Latin, for the first time in Ennodius. The use of the noun by Cicero and Horace thus predates the first attestation of the verb. It doesn't exclude that the verb might have been used after the name.
The word superstitio, or superstitio vana "vain superstition", was applied in the 1st century to the officially outlawed religious cults in the Roman Empire. This concerned the religion of the druids in particular, which was described as a superstitio vana by Tacitus, and Early Christianity, outlawed as a superstitio Iudaica in AD 80 by Domitian.


== Superstition and religion ==

Greek and Roman polytheists, who modeled their relations with the gods on political and social terms, scorned the man who constantly trembled with fear at the thought of the gods, as a slave feared a cruel and capricious master. Such fear of the gods was what the Romans meant by "superstition" (Veyne 1987, p. 211).
Diderot's Encyclopédie defines superstition as "any excess of religion in general", and links it specifically with paganism.In his Prelude on the Babylonian Captivity of the Church, Martin Luther (who called the papacy "that fountain and source of all superstitions") accuses the popes of superstition:

For there was scarce another of the celebrated bishoprics that had so few learned pontiffs; only in violence, intrigue, and superstition has it hitherto surpassed the rest. For the men who occupied the Roman See a thousand years ago differ so vastly from those who have since come into power, that one is compelled to refuse the name of Roman pontiff either to the former or to the latter.
The current Catechism of the Catholic Church considers superstition sinful in the sense that it denotes "a perverse excess of religion", as a demonstrated lack of trust in divine providence (¶ 2110), and a violation of the first of the Ten Commandments.
The Catechism is a defense against the accusation that Catholic doctrine is superstitious:

Superstition is a deviation of religious feeling and of the practices this feeling imposes. It can even affect the worship we offer the true God, e.g., when one attributes an importance in some way magical to certain practices otherwise lawful or necessary. To attribute the efficacy of prayers or of sacramental signs to their mere external performance, apart from the interior dispositions that they demand is to fall into superstition. Cf. Matthew 23:16–22 (¶ 2111)


== Superstition and psychology ==


=== Origins ===


==== Behaviorism perspective ====
In 1948, behavioral psychologist B.F. Skinner published an article in the Journal of Experimental Psychology, in which he described his pigeons exhibiting what appeared to be superstitious behaviour. One pigeon was making turns in its cage, another would swing its head in a pendulum motion, while others also displayed a variety of other behaviours. Because these behaviors were all done ritualistically in an attempt to receive food from a dispenser, even though the dispenser had already been programmed to release food at set time intervals regardless of the pigeons' actions, Skinner believed that the pigeons were trying to influence their feeding schedule by performing these actions. He then extended this as a proposition regarding the nature of superstitious behavior in humans.Skinner's theory regarding superstition being the nature of the pigeons' behaviour has been challenged by other psychologists such as Staddon and Simmelhag, who theorised an alternative explanation for the pigeons' behaviour.Despite challenges to Skinner's interpretation of the root of his pigeons' superstitious behaviour, his conception of the reinforcement schedule has been used to explain superstitious behaviour in humans. Originally, in Skinner's animal research, "some pigeons responded up to 10,000 times without reinforcement when they had originally been conditioned on an intermittent reinforcement basis." Compared to the other reinforcement schedules (e.g., fixed ratio, fixed interval), these behaviours were also the most resistant to extinction. This is called the partial reinforcement effect, and this has been used to explain superstitious behaviour in humans. To be more precise, this effect means that, whenever an individual performs an action expecting a reinforcement, and none seems forthcoming, it actually creates a sense of persistence within the individual. This strongly parallels superstitious behaviour in humans because the individual feels that, by continuing this action, reinforcement will happen; or that reinforcement has come at certain times in the past as a result of this action, although not all the time, but this may be one of those times.


==== Evolutionary/cognitive perspective ====
From a simpler perspective, natural selection will tend to reinforce a tendency to generate weak associations or heuristics that are overgeneralized. If there is a strong survival advantage to making correct associations, then this will outweigh the negatives of making many incorrect, "superstitious" associations. It has also been argued that there may be connections between OCD and superstition. This may be connected to hygiene.
A recent theory by Jane Risen proposes that superstitions are intuitions that people acknowledge to be wrong, but acquiesce to rather than correct when they arise as the intuitive assessment of a situation. Her theory draws on dual-process models of reasoning. In this view, superstitions are the output of "System 1" reasoning that are not corrected even when caught by "System 2".


=== Mechanisms ===
People seem to believe that superstitions influence events by changing the likelihood of currently possible outcomes rather than by creating new possible outcomes. In sporting events, for example, a lucky ritual or object is thought to increase the chance that an athlete will perform at the peak of their ability, rather than increasing their overall ability at that sport. Consequently, people whose goal is to perform well are more likely to rely on "supernatural assistance" - lucky items and rituals - than are people whose goal is to improve their skills and abilities and learn in the same context.
Psychologist Stuart Vyse has pointed out that until about 2010, "[m]ost researchers assumed superstitions were irrational and focused their attentions on discovering why people were superstitious." Vyse went on to describe studies that looked at the relationship between performance and superstitious rituals. Preliminary work has indicated that such rituals can reduce stress and thereby improve performance, but, Vyse has said, "...not because they are superstitious but because they are rituals.... So there is no real magic, but there is a bit of calming magic in performing a ritualistic sequence before attempting a high-pressure activity.... Any old ritual will do."


=== Occurrence ===
People tend to attribute events to supernatural causes (in psychological jargon, "external causes") most often under two circumstances. 

People are more likely to attribute an event to a superstitious cause if it is unlikely than if it is likely. In other words, the more surprising the event, the more likely it is to evoke a supernatural explanation. This is believed to stem from an effectance motivation - a basic desire to exert control over one's environment. When no natural cause can explain a situation, attributing an event to a superstitious cause may give people some sense of control and ability to predict what will happen in their environment.
People are more likely to attribute an event to a superstitious cause if it is negative than positive. This is called negative agency bias. Boston Red Sox fans, for instance, attributed the failure of their team to win the world series for 86 years to the curse of the bambino: a curse placed on the team for trading Babe Ruth to the New York Yankees so that the team owner could fund a Broadway musical. When the Red Sox finally won the world series in 2004, however, the team's success was attributed to the team's skill and the rebuilding effort of the new owner and general manager. More commonly, people are more likely to perceive their computer to act according to its own intentions when it malfunctions than functions properly.


== Superstition and politics ==
Ancient Greek historian Polybius in his Histories uses the word superstition explaining that in ancient Rome that belief maintained the cohesion of the empire, operating as an instrumentum regni.


== Opposition to superstition ==
Opposition to superstition was first recorded in ancient Greece, where philosophers such as Protagoras and the Epicureans exhibited agnosticism or aversion to religion and myths, and Plato – especially his Allegory of the Cave –  and Aristotle both present their work as parts of a search for truth.
In the classical era, the existence of gods was actively debated both among philosophers and theologians, and opposition to superstition arose consequently. The poem De rerum natura, written by the Roman poet and philosopher Lucretius further developed the opposition to superstition. Cicero’s work De natura deorum also had a great influence on the development of the modern concept of superstition as well as the word itself. Where Cicero distinguished superstitio and religio, Lucretius used only the word religio. Cicero, for whom superstitio meant “excessive fear of the gods” wrote that “superstitio, non religio, tollenda est ”, which means that only superstition, and not religion, should be abolished. The Roman Empire also made laws condemning those who excited excessive religious fear in others.During the Middle Ages, the idea of God's influence on the world's events went mostly undisputed. Trials by ordeal were quite frequent, even though Frederick II (1194 – 1250 AD) was the first king who explicitly outlawed trials by ordeal as they were considered “irrational”.The rediscovery of lost classical works (The Renaissance) and scientific advancement led to a steadily increasing disbelief in superstition. A new, more rationalistic lens was beginning to see use in exegesis. Opposition to superstition was central to the Age of Enlightenment. The first philosopher who dared to criticize superstition publicly and in a written form was Baruch Spinoza, who was a key figure in the Age of Enlightenment.


== Regional and national superstitions ==
Most superstitions arose over the course of centuries and are rooted in regional and historical circumstances, such as religious beliefs or the natural environment. For instance, geckos are believed to be of medicinal value in many Asian countries, but not in regions where geckos are not found.
In China, Feng shui is a belief system that is said to have a negative effect on different places, e.g. that a room in the northwest corner of a house is "very bad". Similarly, the number 8 is a "lucky number" in China, so that it is more common than any other number in the Chinese housing market.


== See also ==
Auguste Comte
Anthropology
Atheism
Conscience
Culture
Cultural heritage
Exorcism
Exorcism in Christianity
Exorcism in Islam
Faith
Folklore
God of the gaps
Healing
Health
Heritage science
Heritage studies
List of destroyed heritage
Irreligion
James Randi
Kuai Kuai culture
Logic
Logical fallacies
Metaphor
Occult
Paranormal
Positivism
Pseudoscience
Rationality
Rationalism
Reason
Relationship between religion and science
Sacred mysteries

Synchronicity
Tradition
Urban legend


== References ==


== External links ==
Where Superstitions Come From: slideshow by Life magazine
Superstitions in Russia