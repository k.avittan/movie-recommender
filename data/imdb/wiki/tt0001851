The Scavenger's daughter was a type of torture device invented during the reign of King Henry VIII of England. 


== History ==

The Scavenger's Daughter (or Skevington's Daughter) was invented as an instrument of torture in the reign of Henry VIII by Sir Leonard Skevington, Lieutenant of the Tower of London, a son of Sir William Skeffington, Lord Deputy of Ireland, and his first wife, Margaret Digby. It was an A-frame shaped metal rack; the head was strapped to the top point of the A, the hands at the midpoint, and the legs at the lower spread ends. The frame could fold, swinging the head down and forcing the knees up into a sitting position, compressing the body so as to force the blood from the nose and ears.
The Scavenger's Daughter was conceived as the perfect complement to the Duke of Exeter's Daughter (the rack) because it worked according to the opposite principle by compressing the body rather than stretching it.
The best-documented use is that on the Irishman Thomas Miagh, charged with being in contact with rebels in Ireland. It may be in connection with the Scavenger's Daughter that Miagh carved on the wall of the Beauchamp Tower in the Tower of London, "By torture straynge my truth was tried, yet of my libertie denied. 181. Thomas Miagh."
Another victim of the Scavenger's Daughter was Thomas Cottam, an English Catholic priest and martyr from Lancashire, who suffered it twice before being released. Cottam would eventually be executed during the reign of Henry's successor, Elizabeth I. Likewise, on 10 December 1580, the priest Luke Kirby was subjected to it.It is also known as Skevington's gyves, as iron shackle, as the Stork (as in Italian cicogna) or as Spanish A-frame. Further it is known as Skevington's daughter, from which the more commonly known folk etymology using "Scavenger" is derived. There is a Scavenger's daughter on display in the Tower of London museum.


== See also ==
Captain's daughter, referring to the naval Cat o' nine tails


== Notes ==


== Sources ==
Encyclopædia Britannica Eleventh Edition, "Duke of Exeter's Daughter".