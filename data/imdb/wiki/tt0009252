The Kaiser, the Beast of Berlin (also known as The Beast of Berlin and The Kaiser) was a 1918 American silent war propaganda melodrama film written by, directed by, and starring Rupert Julian. The film's supporting cast included Elmo Lincoln, Nigel De Brulier, and Lon Chaney.The germanophobic film contains a propagandist view of the First World War, showing the political greed of the German Kaiser Wilhelm II, the resistance of some of his own soldiers, and fanciful prediction of the nature of the war's end.  The film is now considered lost.


== Synopsis ==

Kaiser Wilhelm II of Hohenzollern (Rupert Julian) is a vain and arrogant tyrant eager for conquest. When Belgium is invaded by the German army during World War I, Marcas, the blacksmith (Elmo Lincoln), although wounded, is able to save his daughter from the clutches of a German soldier. Soon after this, the RMS Lusitania is sunk by Captain von Neigle (Nigel De Brulier), who ultimately is driven mad with remorse. After the United States declares war, the Allied generals turn the Kaiser over to Albert I of Belgium. Incarcerated, the Kaiser faces his jailer, Marcas the blacksmith.


== Cast ==
Rupert Julian as The Kaiser
Elmo Lincoln as Marcas, the Blacksmith
Nigel De Brulier as Captain von Neigle
Lon Chaney as Bethmann-Hollweg
Harry von Meter as Captain von Hancke
Harry Carter as General von Kluck
Joseph W. Girard as Ambassador Gerard
Harry Holden as General Joffre
Alfred Allen as General John Pershing
C.E. Anderson as Captain Kovich
W.H. Bainbridge as Colonel Schmiedcke
Henry A. Barrows as General Douglas Haig
F. Beauregard as General von Weddingen
Walter Belasco as Admiral von Pliscott
Betty Carpenter as Bride
Edward Clark as General Erich von Falkenhagen
Ruth Clifford as Gabrielle
Wallace Coburn as General Rüdiger von der Goltz
F. Corcoran as General von Hoetzendorf
Orlo Eastman as President Woodrow Wilson
Mark Fenton as Admiral von Tirpitz
Robert Gordon as Louis Lomenie
Winter Hall as Dr. Von Gressler
Wadsworth Harris as General von Ruesselheim
Georgie Hupp as Little Jean
Ruby Lafayette as Grandmother Marcas
Gretchen Lederer as Bertha von Neigle
Frankie Lee as Hansel
Jack MacDonald as King Albert
K. Painter as General Hans von Beseler
Zoe Rae as Gretel
Allan Sears as Captain von Wohlbold
Jay Smith as Marshal von Hindenburg
Pedro Sose as General Porfirio Díaz


== Production notes ==
Although frequently listed as a Universal Studios production, the film was an independent production produced by Rupert Julian for Renowned Pictures. Julian licensed the distribution rights to Renowned, who in turn sold the rights to Universal Jewel for worldwide distribution.


== Reception ==
The Kaiser, the Beast of Berlin was an enormous hit when it was released, and Universal spared no expense in advertising the film. Universal studio head Carl Laemmle pushed the film to the theater owners as hard as he sold it to the viewing public. "A whirlwind of Applause - A Landslide of Money," "Unparalleled Receipts," and "The Picture That Blocked Traffic on Broadway" were some of the headlines for ads that ran in trade publications in an attempt to get theater owners to book the picture.
According to a report in Exhibitor's Trade Review on the film's success in Omaha, 14,000 saw the film there in a single week, a record for that city. "Wild cheering marked every show when the young captain socked the Kaiser on the jaw. Patriotic societies boosted the picture because of its aid in stirring up the country to war. Street car signs were used; huge street banners swung over the crowds in the downtown district, and a truck paraded the streets with the Kaiser hanging in effigy and a big sign 'All pro-Germans will be admitted free.' None availed himself of the invitation."Rupert Julian received rave reviews for his portrayal of the Kaiser and later reprised the role in many subsequent films.


== Preservation status ==
No known prints of the film survive. The Kaiser, the Beast of Berlin is one of the films included on the American Film Institute's list of the "Ten Most Wanted" lost films.


== In popular culture ==
In 1919, a short (two-reel) parody of the film was released titled The Geezer of Berlin.


== See also ==
List of lost films


== References ==


== External links ==
The Kaiser, the Beast of Berlin on IMDb
The Kaiser, The Beast of Berlin at silentera.com
Period advertisements for the film: