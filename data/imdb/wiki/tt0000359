The Man with the Rubber Head (French: L'Homme à la tête en caoutchouc), also known as A Swelled Head, is a 1901 French short silent film by Georges Méliès. The film stars Méliès himself as an apothecary who blows a copy of his own head up to enormous dimensions, but who is unable to get his assistant to perform the stunt as expertly. The special effect of the inflated head, made with a novel combination of purpose-built moving equipment and multiple exposures, evokes the modern close-up and enjoys an iconic place among Méliès's works.


== Plot ==
In a laboratory, an apothecary plans a novel experiment. Putting a living duplicate of his own head on a table, he uses a bellows to swell the head up to gigantic size. The head enjoys itself by making faces and laughing with the apothecary, who then completes the experiment by letting the air out and returning the head to normal.
The apothecary then attempts to have his assistant reproduce the experiment, but the assistant inflates the head too much, and it explodes in a bang of smoke. The angry apothecary throws the assistant out of the room.


== Production ==
Méliès appears in the film as the apothecary and as his duplicate head. To create the illusion of the expanding head, Méliès surrounded himself with a black background and used a specially built ramp to move himself gradually closer to the stationary camera. The head was then superimposed onto the rest of the image using the multiple exposure technique. The other special effects used are substitution splices and pyrotechnics.


== Release and reception ==
The Man with the Rubber Head was released by Méliès's Star Film Company and is numbered 382–383 in its catalogues, where it was advertised as a grande nouveauté. The film was sold in American catalogues as The Man With the Rubber Head and in British catalogues as A Swelled Head.The film is one of Méliès's best known works, and some historians have argued (though others have disagreed) that it represents a very early version of the close-up.


== References ==


== External links ==
L'homme à la tête de caoutchouc on IMDb
Interactive display about the film from the Cinémathèque française