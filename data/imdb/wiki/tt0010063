The desert rat-kangaroo (Caloprymnus campestris), also called the buff-nosed rat-kangaroo, plains rat-kangaroo or oolacunta, is an extinct small hopping marsupial endemic to desert regions of Central Australia. It was discovered in the early 1840s and described by John Gould in London in 1843, on the basis of three specimens sent to him by George Grey, the governor of South Australia at the time.


== Description ==
It was formed like a kangaroo, but had the bulk of a small rabbit, and was described as having a delicate and slender form. The length of the head and body combined is estimated to be about 254–282 mm in addition to a 307 to 377 mm long tail.
Its head was short, blunt, and wide, different from that of any kangaroo or wallaby with a naked nose, short and rounded ears.The color of its dense, straight, soft fur was appropriate for its desert surroundings. It was very pale yellowish brown, the hairs tipped with sooty brown; interspersed with the under fur were many long brownish white hairs. Its underbelly was described as white with very pale yellowish-brown feet and tail.A distinguishing feature of this species was the difference in size between the fore and hind limbs. Its fore limbs were quite delicate with bones weighing 1 gram, while its hind limbs are large with bones weighing 12 grams. This difference is related to saltation. Other characteristics related to hopping locomotion include a long, but rather thin tail.


== Distribution and habitat ==
Caloprymnus campestris was thought to occupy a relatively small area in South Australia, extending just over the borders of southwestern Queensland and Northern Territory. It was last seen in 1935 in the eastern Lake Eyre basin of northern Southern Australia
The desert rat-kangaroo lived in the desert regions of Australia, including clay pans, loamy flats, sand ridges, and gibber plain habitats. Its native habitat was very arid, cover is sparse, and consists of saltbush and other chenopods and emu bush.


== Ecology and behaviour ==
Caloprymnus campestris was solitary except for mothers with young offspring. It lived in nests built over shallow depressions in the ground. These nests were excavated or found and are crucial in the desert, where temperatures can be high, while relatively little brush or foliage is available for cover. The "pits" were lined with grass, which females carried to the nest with their tails. The nest would then be covered with twigs to provide cover from the scorching sun. Often, the desert rat kangaroo was found peeking out of the top of the nest to observe its surroundings. This species would spend most of the day taking cover in the nest, and emerge at dusk to feed. Thus, it was at least partially nocturnal.


=== Diet ===
The desert rat-kangaroo was mainly herbivorous, feeding on foliage and stems of desert vegetation, but has also been found to eat insects such as beetles and weevils. It was so independent of water, it even shunned the succulent plants of the sand hills. It was able to survive without any surface water while feeding on green plants.


=== Locomotion ===
It had a distinct method of hopping. Its posture was forward and the long tail was extended when it moved at high speeds. Unlike other marsupials, Caloprymnus would land with the right foot in front of the left foot. It showed great endurance while being chased on horseback at high speeds (Finlayson reported chasing an individual over 12 miles), and "paused only to die".


=== Reproduction ===
Females reached sexual maturity at about 11 months, while males reached maturity some two months later. Marked sexual dimorphism was apparent,  with females being larger. Females went through estrus at three-week intervals and could mate throughout the year. Although able to mate all year, they had an irregular breeding season when most mating took place. Females with pouched joeys had been found between June and December. Young were born very undeveloped, as is typical of marsupials. Gestation was probably around one to two months, with a pouch period of two to three months. All females were found with only one young at a time. Young remained dependent for over a month after leaving the pouch and soon after would leave permanently.


== Rediscovery and extinction ==
The desert rat-kangaroo was discovered in the early 1840s. However, after these early sightings, it was no longer recorded for 90 years (aside from an unconfirmed report in 1878), and was widely believed to be extinct. This species, even before European colonisation, was apparently never abundant.
Following the relief of drought conditions which improved the local habitat, the animal was rediscovered in 1931 when Hedley Finlayson found a thriving colony of them. He made multiple returns, but after a few years, the population disappeared. The last confirmed record of the species came in 1935 from near Ooroowilanie, east of Lake Eyre.Caloprymnus campestris was well-adapted to the extremely barren and arid regions it inhabited; these traits saved it from competition by introduced species like the European rabbit or domestic sheep. However, as early as the 1930s, the red fox had spread to the areas inhabited by the desert rat kangaroo. Thus, the rapid decline of the desert rat-kangaroo began shortly after its recovery in 1931 correlates with the invasion of its habitat by the red fox. Predation by the red fox and feral cats, as well as variable seasonal patterns and overhunting by indigenous Australians, were blamed for the extinction of this species.
No reliable reports of the species have been made since 1935, but unconfirmed sightings in Queensland followed periods of rain in 1956-1957 and 1974-1975. Also, recent remains of this species have been found in the mid-1980s inside caves.The desert rat-kangaroo was declared extinct in 1994, making it the only mammal species to be rediscovered and then lost again.


=== Possible survival ===
In view of its amazing recovery following a 90-year period when it was not seen, the extinction of the desert rat-kangaroo is not certain; thus, sightings of this animal would not fall into the cryptozoology category. In similar cases, the broad-faced potoroo was last seen in the late 19th century and is considered extinct, Gilbert's potoroo was considered extinct for 120 years prior to its rediscovery in 1994, and the long-footed potoroo was only discovered in 1967.
The finding of recent remains in the 1980s casts some doubt on the extinction of this species. Professor Ronald Nowak stated in his 2005 book, "perhaps a small population still survives, awaiting the time when it again may increase in response to proper conditions."


=== 2011 Peake Station sighting ===
An animal was sighted in May 2011 off Peake Station and subsequently identified from a museum skin as a desert rat-kangaroo. The area was then surveyed in August the same year. An old nest with some small macropod like scats was found and some tracks were also found near a waterhole that might have been made by the desert rat-kangaroo. However DNA analysis of the scats failed to yield any usable DNA. No DNA was found in dingo and cat scats in the area as well. As such the sighting remains unconfirmed, but researchers Tony Robinson and Tiana Forrrest confirmed the possibility "that a small population of Caloprymnus, generally considered to be extinct throughout its former range across the Lake Eyre Basin, may have been present in this area in May 2011".


== References ==

14. SG Carr, AC Robinson (1997) The present status and distribution of the desert rat-kangaroo Caloprymnus campestris (Marsupialia: Potoroidae) The South Australian Naturalist, 72(1) 1997