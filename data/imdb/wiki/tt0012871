Little Fires Everywhere is an American drama streaming television miniseries, based on the 2017 novel of the same name by Celeste Ng. It premiered on Hulu on March 18, 2020 and consists of eight episodes. The series stars Reese Witherspoon and Kerry Washington, both of whom were also executive producers, alongside Liz Tigelaar, Lauren Neustadter, and Pilar Savone. Set in the Cleveland suburb of Shaker Heights, Ohio during the late 1990s, it features Witherspoon and Washington as mothers from different socioeconomic backgrounds.


== Premise ==
Little Fires Everywhere follows "the intertwined fates of the picture-perfect Richardson family and an enigmatic mother and daughter who upend their lives. The story explores the weight of secrets, the nature of art and identity, the ferocious pull of motherhood – and the danger in believing that following the rules can avert disaster."


== Cast and characters ==


=== Main ===
Reese Witherspoon as Elena Richardson, a journalist, landlady, and mother of four teenagers.
Kerry Washington as Mia Warren, a talented artist who works part time as a waitress, and is Pearl's mother.
Joshua Jackson as Bill Richardson, Elena's husband and a lawyer.
Rosemarie DeWitt as Linda McCullough,  Elena's childhood friend.
Jade Pettyjohn as Lexie Richardson, Elena's and Bill's eldest daughter, and a straight A student.
Lexi Underwood as Pearl Warren, Mia's daughter, gifted student and budding poet.
Megan Stott as Izzy Richardson, Elena's and Bill's youngest daughter, an artist and the black sheep of the family.
Gavin Lewis as Moody Richardson, Elena's and Bill's younger son.
Jordan Elsass as Trip Richardson, Elena's and Bill's older son, a popular jock.


=== Recurring ===
SteVonté Hart as Brian Harlins, Lexie's boyfriend
Paul Yen as Scott
Huang Lu as Bebe Chow, Mia's co-worker and friend, an illegal immigrant and Mei-Ling's birth mother.
Geoff Stults as Mark McCullough, Linda's husband
Jaime Ray Newman as Elizabeth Manwill
Obba Babatundé as George Wright
Melanie Nicholls-King as Regina Wright
Jesse Williams as Joe Ryan
Sarita Choudhury as Anita Rees
Austin Basis as Principal Peters
Byron Mann as Ed Lan


=== Guest ===
AnnaSophia Robb as Young Elena*
Violet Skye as Baby Lexie
Tiffany Boone as Young Mia
Alona Tal as Young Linda
Nicole Beharie as Mrs Ryan
Matthew Barnes as Young Bill
Andy Favreau as Young Mark
Luke Bracey as Jamie Caplan
Anika Noni Rose as Pauline Hawthorne
Britt Robertson as Rachel
Aubrey Joseph as Warren Wright
Ozie "Zmny" Nzeribe as 2Pac Shirt Ozie Nzeribe


== Episodes ==


== Production ==


=== Development ===
The book was discovered by Reese Witherspoon and Lauren Neustadter before it was published. Witherspoon went on to choose the book as her September 2017 book club pick and soon after it was published it became a bestseller. Witherspoon then brought the book to Kerry Washington, and together the pair approached Liz Tigelaar to adapt and showrun the novel as a limited series. The project began its development at ABC Signature, the cable/streaming division of ABC Studios where Witherspoon's Hello Sunshine has a network-only deal and Washington's Simpson Street has an overall deal.On March 2, 2018, the production was officially announced but a network had yet to be determined. The series was set to be written and showrun by Tigelaar who will also executive produce alongside Witherspoon, Washington, Lauren Neustadter, and Pilar Savone. The author of the novel, Ng, would act as producer. Production companies involved in the series included Hello Sunshine, Simpson Street, ABC Signature Studios. On March 12, 2018, Hulu gave the production an eight episode order. This series order came after a multiple-outlet bidding war involving various networks and streaming services.
In April 2019, Lynn Shelton was chosen to direct the series and serve as an executive producer. Shelton died of a blood disorder shortly after the final episode of the series aired. 


=== Casting ===
Alongside the initial series announcement, it was reported that in addition to executive producing the series Reese Witherspoon and Kerry Washington had been cast in the series' lead roles. In April 2019, Rosemarie DeWitt, Jade Pettyjohn, Jordan Elsass, Gavin Lewis, Megan Stott and Lexi Underwood
joined the cast of the series. In May 2019, Joshua Jackson also joined the cast as the husband of Witherspoon's character. In June 2019, Paul Yen, Huang Lu, and Geoff Stults had been cast in recurring roles. In July 2019, Jaime Ray Newman joined the cast in a recurring role. In September 2019, Obba Babatundé and Byron Mann were cast in recurring capacities, with AnnaSophia Robb, Tiffany Boone, Alona Tal, Matthew Barnes, Andy Favrea, Luke Bracey, and Anika Noni Rose cast in guest roles. In October 2019, Jesse Williams, Britt Robertson, Kristoffer Polaha, Austin Basis and Reggie Austin joined the cast of the series, in recurring capacities; however, Polaha and Austin were not credited in the series.


=== Filming ===
Principal photography took place in Los Angeles, California and ran from May 31 to October 23, 2019. The exterior of the Warren home is located in Pasadena, with the exterior of the Richardson home in Hancock Park.


=== Music ===
The score was composed by Mark Isham and Florence & the Machine keyboardist Isabella Summers. The series' official soundtrack was released on April 17, 2020 by Hollywood Records and featured cover songs recorded for the drama and performed by Judith Hill, BELLSAINT, Ruby Amanfu, Lauren Ruth Ward  and an original song written by Ingrid Michaelson. 


== Release ==
Little Fires Everywhere premiered on March 18, 2020 on Hulu. Hulu's corporate sibling Disney+ Hotstar premiered the series in India concurrent with US telecast. Internationally, the series premiered on Amazon Prime Video in Europe, Latin America, Canada and Australia on May 22, 2020..


== Reception ==
On review aggregator website Rotten Tomatoes, the series holds an approval rating of 79% based on 76 reviews, with an average rating of 7.09/10. The website's critics consensus reads: "Though Little Fires Everywhere at times plays it too safe, sparks fly when it lets well-matched leads Kerry Washington and Reese Witherspoon dig into the difficult questions it does dare to ask." On Metacritic, it has a weighted average score of 70 out of 100, based on 30 critics, indicating "generally favorable reviews".According to Hulu, in its first 60 days online Little Fires Everywhere was the most-watched drama ever on the streaming service.


=== Awards and nominations ===


== See also ==
List of original programs distributed by Hulu


== References ==


== External links ==
Little Fires Everywhere on IMDb
Little Fires Everywhere at Rotten Tomatoes