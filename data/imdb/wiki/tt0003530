Dreams Come True (ドリームズ・カム・トゥルー, Dorīmuzu Kamu Turū) are a Japanese pop band formed in 1988, comprising Miwa Yoshida (lead vocals) and Masato Nakamura (bass). Founding member Takahiro Nishikawa (keyboards) left in 2003 to pursue a solo career. The band has sold more than 50 million records worldwide.


== History ==
Vocalist Miwa Yoshida, bassist Masato Nakamura, and keyboardist Takahiro Nishikawa formed Dreams Come True in 1988. The band is commonly known as DCT (Dreams Come True) and sometimes referred to as "Dorikamu" (ドリカム). Their first album sold more than one million copies in Japan. The Swinging Star (1992) was the first Japanese album to sell over three million copies, and for several years was the best-selling Japanese-language album of all time. Nakamura composed the music for the Sega Genesis games Sonic the Hedgehog (1991) and Sonic the Hedgehog 2 (1992).In 1993, Dreams Come True recorded "Winter Song" for the opening theme of the Japanese version of the film Sleepless in Seattle and the following year recorded the song "Eternity" for the animated film The Swan Princess by New Line Cinema. In Japan, they recorded theme songs for programs produced by Tokyo Broadcasting System. The Walt Disney Company hired the band to create themes for their television shows, and attractions at the Tokyo Disney Resort. In 2001, the band performed "Crystal Vine," the theme song for the Japanese version of Atlantis: The Lost Empire, and Yoshida was hired to dub over Audrey Rocio Ramirez in the Japanese version.
Yoshida started a solo music career in 1995, and the following year appeared on the cover of Time. She has also appeared in advertisements for Sony, Visa, Honda, Shiseido, Lotte, Vodafone, and Coca-Cola. Nakamura was hired to compose the music for several television commercials, and around the same time compiled various "Dreams Come True" songs for Konami to be used in the game Dancing Stage featuring Dreams Come True, a spin-off of the Dance Dance Revolution series.
On April 1, 1996, they released Love Unlimited, their final album for Sony Music. News of their departure would cause Sony Music shares to drop severely on the Tokyo Stock Exchange. They later signed with Virgin Music America and Toshiba-EMI in Japan, due to that label's promises to break the group into the American market—something Sony was unwilling to do. Their first album under the new contract, Sing Or Die (a title that half-jokingly described the band's relationship with their former label, according to Nakamura), was released in an English-language version, but received very little promotion and even less airplay. It would be the band's final album as a trio, and Takahiro Nishikawa would depart the band shortly after its release. Dreams Come True left Virgin after the failure of Sing or Die.
Part of the failure of the Virgin-DCT relationship can be laid at the label's attempt to renovate the band into something the American audiences could relate to. Yoshida took the stage name "Miwa", Nakamura became "King Masa," but Nishikawa remained the same, and only received a passing mention on Sing or Die's liner notes. Musically, the album veered away from the classic DCT formula and deeper into the hip-hop dance theme begun on Delicious and Love Unlimited. The combination was not well received by American audiences, nor DCT's legion of loyal Japanese fans.There are conflicting accounts concerning Nishikawa's departure. Nishikawa himself states both he and longtime DCT producer Mike Pela were forced out by Virgin because they did not fit into the label's makeover for the band. However, he also adds that prior to that, he had not been touring with DCT for some time, which supports the official account. At present, there seems to be little chance of Nishikawa playing with his former bandmates again.
Following Nishikawa's departure from DCT, he was arrested for assault and drug possession, but received a suspended sentence. However, after a second arrest for possession in 2006, he was sentenced to prison.Following 2001's Monkey Girl Odyssey, DCT severed all ties with Virgin and Toshiba-EMI, and set up shop with Universal Music Japan. More recent albums such as The Love Rocks and And I Love You have returned to the classic DCT sound, and have been received more warmly.The band released one more English-language album on Universal called Love Overflows, which were re-recorded versions of songs from Love Goes On to Monkey Girl Odyssey.
They have also been involved in charity events. Including the collaboration "Zero Landmine" with Ryuichi Sakamoto in 2001 to help raise funds for land mine removal agencies, and taking part in the Live 8 benefit concert in 2005.
On November 12, 2008, their single "Tsuretette Tsuretette" was released. "Tsuretette Tsuretette" debuted a top on the Oricon weekly charts, nine years ten months since their last number-one single "Asa ga Mata Kuru". The song also appears on the band's most recent album, Do You Dreams Come True?. The album is available in three versions: A single disc version, containing the main album; a second that adds Greatest Hits: The Soul 2, a sequel to their 2000 greatest-hits package; and a third that adds a live DVD, "Winter Fantasia 2008."
Their DVD and Blu-ray of their 20th Anniversary tour is one of the first concert videos to be filmed with the RED One high-definition camera.In 2010, they released their download only single "Godspeed", where the title is meant to be an even stronger wish of "Good Luck" for everyone to listen to it. In November of that same year, they released their 23rd album, Love Central, their second on Universal Music Japan's Nayutawave label.
In 2016, they released their first complete greatest hits album, Dreams Come True The Ura Best! Watashi Dake no Dorikamu.


== Reception ==
In a 2006 survey of people between 10 and 49 years of age in Japan, Oricon Style found the number one selling song Love Love Love (2,488,630 copies) to be the second most popular Valentine's Day song in Japan. The most popular song was Sayuri Kokushō's 1986 debut single Valentine Kiss, which sold only 317,000 copies. The other songs in the top five were Valentine's Radio from Yumi Matsutoya (1,606,780 copies), Happy Happy Greeting from Kinki Kids (608,790 copies), and My Funny Valentine by Miles Davis.


== Band members ==
Current members

Miwa Yoshida – vocal, songwriter
Masato Nakamura – bass guitar, composer, record producerFormer members

Takahiro Nishikawa – keyboards, synthesizer programmer (1988–2003)


== Discography ==


== See also ==
List of best-selling music artists in Japan


== References ==


== External links ==
Official website
Dreams Come True Garden
Dreams Come True Wonderland 2007 Official Site