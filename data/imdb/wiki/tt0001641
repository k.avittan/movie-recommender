Geranium is a genus of 422 species of annual, biennial, and perennial plants that are commonly known as geraniums or cranesbills. They are found throughout the temperate regions of the world and the mountains of the tropics, but mostly in the eastern part of the Mediterranean region.
The palmately cleft leaves are broadly circular in form. The flowers have five petals and are coloured white, pink, purple or blue, often with distinctive veining. Geraniums will grow in any soil as long as it is not waterlogged. Propagation is by semiripe cuttings in summer, by seed, or by division in autumn or spring.
Geraniums are eaten by the larvae of some Lepidoptera species including brown-tail, ghost moth, and mouse moth. At least several species of Geranium are gynodioecious. The species Geranium viscosissimum (sticky geranium) is considered to be protocarnivorous.


== Description ==

The genus name is derived from the Greek γέρανος (géranos) or γερανός (geranós) ‘crane’. The English name ‘cranesbill’ derives from the appearance of the fruit capsule of some of the species.  Species in the genus Geranium have a distinctive mechanism for seed dispersal. This consists of a beak-like column which springs open when ripe and casts the seeds some distance. The fruit capsule consists of five cells, each containing one seed, joined to a column produced from the centre of the old flower. The common name ‘cranesbill’ comes from the shape of the unsprung column, which in some species is long and looks like the bill of a crane. However, many species in this genus do not have a long beak-like column.


== Confusion with Pelargonium ==

Confusingly, "geranium" is also the common name of members of the genus Pelargonium, which are also in the family Geraniaceae and are widely grown as horticultural bedding plants. Linnaeus originally included all the species in one genus, Geranium, but they were later separated into two genera by Charles L’Héritier in 1789. Other former members of the genus are now classified in Erodium, including the plants known as filarees in North America.
The term "hardy geranium" is often applied to horticultural Geraniums to distinguish them from the Pelargoniums, which are not winter-hardy in temperate horticulture. However, not all Geranium species are winter-hardy (see below).
The shape of the flowers offers one way of distinguishing between the two genera Geranium and Pelargonium. Geranium flowers have five very similar petals, and are thus radially symmetrical (actinomorphic), whereas Pelargonium (and also Erodium) flowers have two upper petals which are different from the three lower petals, so the flowers have a single plane of symmetry (zygomorphic).


== Cultivation ==
A number of geranium species are cultivated for horticultural use and for pharmaceutical products.
Some of the more commonly grown species include:

All the above species are perennials and generally winter-hardy plants, grown for their attractive flowers and foliage.  They are long-lived and most have a mounding habit, with palmately lobed foliage. Some species have spreading rhizomes. They are normally grown in part shade to full sun, in well-draining but moisture retentive soils, rich in humus. Other perennial species grown  for their flowers and foliage include: G. argenteum, G. eriostemon, G. farreri, G. nodosum, G. procurrens, G. pylzowianum, G. renardii, G. traversii, G. tuberosum, G. versicolor, G. wallichianum and G. wlassovianum. Some of these are not winter-hardy in cold areas and are grown in specialized gardens like rock gardens. Geranium 'Johnson's Blue' is a hybrid between G. himalayense (southwestern China), with G. pratense (European meadow cranesbill).


=== Cultivars ===
The following hybrid cultivars have gained the Royal Horticultural Society's Award of Garden Merit (other cultivars are dealt with under their species name - see above):-


== Gallery ==

		
		
		
		
		
		
		
		
		
		


== See also ==
List of Geranium species


== References ==


== Bibliography ==


== External links ==
ITIS list of Geranium species
Geranium Taxonomic Information System
Preparing Geraniums for Winter