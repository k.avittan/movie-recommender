A hell ship is a ship with extremely inhumane living conditions or with a reputation for cruelty among the crew. It now generally refers to the ships used by the Imperial Japanese Navy and Imperial Japanese Army to transport Allied prisoners of war (POWs) and romushas (Asian forced slave laborers) out of the Dutch East Indies, the Philippines, Hong Kong and Singapore in World War II. These POWs were taken to Japan, Taiwan, Manchukuo, Korea, the Moluccas, Sumatra, Burma, or Siam to be used as forced labor.


== Early use of the term ==
The term was coined much earlier than its use in the Second World War. During the American Revolution, HMS Jersey and other prison hulks in New York Harbor were used by the British to house American prisoners of war in terrible conditions. More than 11,000 prisoners died on these ships. HMS Jersey in particular was referred to as "Hell."
In the context of World War 2, whilst the term was more commonly used to describe Japanese prisoner of war transports, the term was also used for German prisoner of war transports, such as the German tanker Altmark. The Royal Navy destroyer HMS Cossack boarded Altmark in a Norwegian fjord on 16 February 1940 and released some 300 British merchant sailors picked up from ships sunk by the pocket battleship Admiral Graf Spee. In reporting the Altmark incident, British newspapers frequently called Altmark "Hitler's hell-ship" or the "Nazi hell-ship".


== Japanese hell ships ==
In May 1942, the Japanese began transferring its captured POWs by sea. Similar to the treatment of prisoners on the Bataan Death March, prisoners were often crammed into cargo holds with little air, ventilation and food or water, for journeys that would last weeks. Many died due to asphyxia, starvation or dysentery. Some POWs became delirious and unresponsive in their environment of heat, humidity and lack of oxygen, food, and water. These transports carried a mix of POWs and regular Japanese troops and cargo, and thus were not eligable to be marked as non-combatents and could be attacked by Allied submarines and aircraft meaning they were at risk of being sunk before they even reached their destination. More than 20,000 Allied POWs died at sea when the transport ships carrying them were attacked by Allied submarines and aircraft.


== List of ships sunk ==


=== Lisbon Maru ===
Lisbon Maru was carrying 2,000 British POWs from Hong Kong to Japan in appalling conditions when torpedoed by USS Grouper on 1 October 1942.  800 POWs died when the ship sank the following day. Many were shot or otherwise killed by the ship's Japanese guards.


=== Rakuyo Maru and Kachidoki Maru ===
Rakuyo Maru was torpedoed on 12 September 1944 by USS Sealion which later realized the ship carried 1,317 Australian and British prisoners of war (POWs) from Singapore to Formosa (Taiwan). A total of 1,159 POWs died.  The 350 who were in lifeboats were bombarded and all killed by a Japanese navy vessel the next day when they were rowing towards land. On 15 September, the three submarines returned to the area and rescued 63 surviving POWs who were on rafts. Four of them died before they could be landed at Tanapag Harbor, Saipan, in the Mariana Islands. 
In the same convoy and on the same day, Kachidoki Maru with another 950 British POWs on board, was also sunk by USS Pampanito. 431 of them were killed.


=== Suez Maru ===
Suez Maru was a 4,645-ton freighter with passenger accommodation. She sailed on 25 November 1943 with 548 POWs (415 British and 133 Dutch) from Ambon bound for Surabaya. The POWs were all sick men from the work-camps on the Moluccas and Ambon. Twenty were stretcher cases. On 29 November 1943 the ship was torpedoed by USS Bonefish (SS-223) near Kangean Island east of Madoera Island. Most of the POWs drowned in the holds of the ship. The crew of Bonefish was unaware that Suez Maru was carrying POWs. Those who escaped from the holds and left the ship were shot by the Japanese. Minesweeper W-12 picked up the Japanese survivors although recently released documents state that W-12 machine-gunned the surviving POWs (a minimum of 250) in the water. There were no POW survivors.


=== Buyo Maru ===
Buyo Maru was a 5,446 ton transport carrying mainly Indian POWs. It was torpedoed by USS Wahoo (SS-238), commanded by Commander Dudley W. Morton, on 26 January 1943. Morton was responsible for ordering the machine gunning of some of the shipwrecked survivors, in the water and in boats, including the POWs. The Hague Convention of 1907 bans the killing of shipwreck survivors under any circumstances. Whatever the case, Morton and his executive officer Richard O'Kane had misidentified the survivors as Japanese. In fact, they were mainly Indian POWs of the 2nd Battalion, 16th Punjab Regiment, plus escorting forces from the Japanese 26th Field Ordnance Depot. Of 1,126 men aboard Buyo Maru, 195 Indians and 87 Japanese died in all, including those killed in the initial sinking. On the next day, 27 January 1943, the Choku Maru (No. 2) rescued the remaining survivors and took them to Palau (the exact number of dead varied according to sources).


=== Shinyo Maru ===

Shinyo Maru was attacked by the submarine USS Paddle on 7 September 1944. Two torpedo hits sank the ship and killed several hundred US, Dutch and Filipino servicemen. Japanese guarding the prisoners opened fire on them while they were trying to abandon ship or swim to the nearby island of Mindanao. 47 Japanese and 687 Allied POWs were killed.


=== Junyō Maru ===
The 5,065-ton tramp steamer Junyo Maru sailed from Batavia (Tandjoeng Priok) on 16 September 1944 with about 4,200 romusha slave labourers and 2,300 POWs aboard. These Dutch POWs included 1,600 from the 10th Battalion camp and 700 from the Kampong Makassar camp. This 23rd transport of POWs from Java was called Java Party 23. Java Party 23 included about 6,500 men bound for Padang on the west coast of Sumatra to work on the Sumatra railway (Mid-Sumatra).
On 18 September 1944 the ship was 15 miles off the west coast of Sumatra near Benkoelen when HMS Tradewind hit her with two torpedoes, one in the bow and one in the stern. About 4,000 romushas and 1,626 POWs died when the ship sank in 20 minutes. About 200 romushas and 674 POWs were rescued by Japanese ships and taken to the Prison in Padang, where eight prisoners died.


=== Maros Maru ===
The 600-ton Maros Maru (The SS Maros was renamed Haruyoshi Maru by the Japanese) sailed from Ambon on 17 September 1944 routed along the south-coast of Celebes with about 500 British and Dutch POWs bound for Surabaya. On 21 September 1944 the ship arrived at Muna Island (south of Celebes) to embark 150 POWs. The ship required engine repairs upon arrival in Makassar. Here 159 POWs died in the holds in the 40 days required to complete repairs. They got a seaman's grave in the harbour of Makassar. Only 327 POWs survived when the ship reached Surabaya on 26 November 1944. They were transported by train to the Kampong Makassar camp in Batavia (Meester Cornelis), and arrived on 28 November 1944.


=== Oryoku Maru ===
Oryoku Maru was a 7,363-ton passenger cargo liner transporting 1,620 survivors of the Bataan Death March, Corregidor, and other battles, mostly American, packed in the holds, and 1,900 Japanese civilians and military personnel in the cabins. She left Manila on 13 December 1944, and over the next two days was bombed and strafed by U.S. airplanes. As she neared the naval base at Olongapo in Subic Bay, U.S. Navy planes from USS Hornet attacked the unmarked ship, causing it to sink on December 15. About 270 died aboard ship. Some died from suffocation or dehydration. Others were killed in the attack, drowned or were shot while escaping the ship as it sank in Subic Bay, where the 'Hell Ship Memorial' is located. A colonel, in his official report, wrote:

Many men lost their minds and crawled about in the absolute darkness armed with knives, attempting to kill people in order to drink their blood or armed with canteens filled with urine and swinging them in the dark. The hold was so crowded and everyone so interlocked with one another that the only movement possible was over the heads and bodies of others.


=== Enoura Maru ===
About 1,000 of the survivors of the Oryoku Maru, which sank on 15 December 1944, were loaded on the Enoura Maru while the rest boarded the smaller Brazil Maru. Both ships reached Takao (Kaohsiung) harbor in Taiwan on New Year's Day. On 6 January 1945, the smaller group of prisoners was transferred from Brazil Maru to Enoura Maru, and 37 British and Dutch were taken ashore.  However, on January 9, the Enoura Maru was bombed and disabled by aircraft from USS Hornet while in harbor, killing about 350 men.


=== Brazil Maru ===
Survivors of the Oryoku Maru, which sank on 15 December 1944, were loaded on the Enoura Maru and the Brazil Maru. Both ships reached Takao (Kaohsiung) harbor in Taiwan on New Year's Day. On 6 January 1945, the smaller group of prisoners were transferred from Brazil Maru to Enoura Maru.  However, on January 9, the Enoura Maru was bombed and disabled by U.S. aircraft.Brazil Maru transported the last surviving Allied POWs to Moji, Japan, on 29 January 1945. There the Japanese medics were shocked at the wasted condition of the POWs and used triage to divide them. The 110 most severe cases were taken to a primitive military hospital in Kokura where 73 died within a month. Four other groups were sent to Fukuoka POW camps 1, 3, 4 and 17. Of 549 men alive when the ship docked, only 372 survived the war. Some eventually went to a POW camp in Jinsen, Korea, where they were given light duty, mainly sewing garments for the Japanese Army.


=== Arisan Maru ===
On October 24, 1944, the Arisan Maru was transporting 1,781 U.S. and Allied military and civilian POWs when she was hit by a torpedo from a U.S. submarine (either USS Shark or USS Snook), at about 5:00 p.m.; she finally sank about 7:00 p.m. No POWs were killed by the torpedo strikes and nearly all were able to leave the ship's holds but the Japanese did not rescue any of the POWs that day. Only nine of the prisoners aboard survived the event. Five escaped and made their way to China in one of the ship's two life boats. They were reunited with U.S. forces and returned to the United States. The four others were later recaptured by Imperial Japanese naval vessels, where one died shortly after reaching land.


=== Montevideo Maru ===
Montevideo Maru was a Japanese auxiliary ship that was sunk in World War II, resulting in the drowning of a large number of Australian prisoners of war and civilians being transported from Rabaul. Prior to the war, it operated as a passenger and cargo vessel, traveling mainly between Asia and South America.


== Media appearances ==
In 2012 film producer Jan Thompson created a film documentary on the hell ships, Death March, and POW camps titled Never the Same: The Prisoner-of-War Experience. The film reproduced scenes of the camps and ships, showed drawings and writings of the prisoners, and featured Loretta Swit as the narrator.


== See also ==
List of Japanese hell ships
Slave ship
Prison ship
SS Arandora Star
RMS Nova Scotia — a UK liner sunk in November 1942 while carrying interned Italian civilians and prisoners of war
SS Shuntien (1934)
Laconia incident


== References ==

Jones, Allan (2002). The Suez Maru atrocity: Justice denied!: the story of Lewis Jones, a victim of a WW2 Japanese hell-ship. Hornchurch: privately published. ISBN 0954272501.


== External links ==
"American POWs on Japanese Ships Take a Voyage into Hell". Prologue Magazine. Retrieved December 20, 2005.
"Hell Ships". Britain at War. Retrieved January 16, 2008.
Rolls of those who died on Hell Ships
Documentary Hell Ships to Flores and the Moluccas. 2012. Kees Maaswinkel, at youtube.com
Documentary Hell ships to Sumatra. 2012. Kees Maaswinkel, at youtube.com