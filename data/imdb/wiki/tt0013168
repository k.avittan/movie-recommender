Getting Even with Dad is a 1994 American comedy film starring Macaulay Culkin and Ted Danson.


== Plot ==
Timmy Gleason is the estranged son of ex-con Ray Gleason and has been living with his aunt Kitty and her fiancee since the death of his mother some years earlier. Ray works as a baker designing cakes for a bakery. When Kitty goes on her honeymoon, she dumps Timmy on a reluctant Ray, leaving him to look after his son in San Francisco for the next week.
Timmy is hoping to spend time with his father, but is largely ignored by Ray, who is the midst of planning a rare-coin heist with his two cronies Bobby and Carl. The robbery is successful, but Timmy learns of it and hides the stolen coins from them. He uses it to blackmail Ray into spending time with him, promising that he will return the coins afterwards. Thus father and son spend the next few days fishing, playing miniature golf and visiting amusement parks, with an amiable Carl and angry Bobby tagging along.
The police are suspicious of Ray, so Detective Theresa Walsh is assigned by her superior to go undercover and surveil him. By chance, Ray and Timmy get talking to Theresa, unaware of who she really is, and invite her for a coffee and then to dinner. Theresa and Ray develop a mutual attraction, causing her boss concern over her willingness to do her job. Timmy and Ray have also gotten closer, so Timmy decides that he wants to stay with his dad permanently. He urges Ray to forget about the stolen coins, because he will probably be caught and sent back to prison. Ray refuses, so Timmy prepares to return home.
At the last moment, Ray has a change of heart. Bobby, however, appears at the bus station, where at gunpoint he forces Ray to open the locker containing the coins. Ray and Bobby are set upon by the waiting police and arrested. Ray is crushed to discover that Theresa is a cop. However, it turns out that the bag in the locker was full of pennies, so Ray is released again. At Timmy's prompting, Theresa finds the rare coins in a gym bag in a department store held by a mannequin. The coins are returned and all charges against Ray are dropped. Father and son then prepare for a new life together.


== Cast ==
Macaulay Culkin as Timmy Gleason
Ted Danson as Ray Gleason
Glenne Headly as Detective Theresa Walsh
Saul Rubinek as Robert "Bobby" Drace
Gailard Sartain as Carl
Hector Elizondo as  Lt. Romayko
Sam McMurray as Alex
Ron Canada as Zinn
Sydney Walker as Mr. Wankmueller
Kathleen Wilhoite as Kitty
Dann Florek as Wayne, Kitty's new husband.
Scott Beach as Wino


== Production ==
Macaulay Culkin's character was supposed to have a short haircut in this movie, but Culkin, who had let his hair grow at the time, liked his looks and did not want to cut it. His father, Kit Culkin, demanded his son be allowed to keep his hair the way it was, pointing out that his character was a working-class boy and not a clean-cut, prep school one. He got to keep his long hair.


=== Locations ===
The theme park featured in the movie was Paramount's Great America located in Santa Clara, California. Ray's apartment in the movie is located at the corner of Jackson and Mason St. in Chinatown, San Francisco. The miniature golf course was filmed at Scandia Family Fun Center located in Rohnert Park, California. Principal photography took place between July and October 1993.


== Reception ==
Review aggregation website Rotten Tomatoes retrospectively collected reviews to give a score of 3% based on reviews from 29 film critics, with a rating average of 3.5 out of 10. The site's consensus reads: "Overly formulaic and tonally inconsistent, Getting Even with Dad tries for a sentimental conclusion it doesn't earn and winds up a slapsticky cash grab aimed at fans of Home Alone."  Audiences surveyed by CinemaScore gave the film a grade "B" on scale of A to F.Roger Ebert of the Chicago Sun-Times gave the film two out of four stars saying, "It wants to be a caper, a comedy, a romance, and a showcase for Macaulay Culkin. That's too much of a stretch."Desson Howe of The Washington Post also gave the film a negative review explaining that "after plying the audience with formulaic predictability, Getting Even doesn't even have the decency to end quickly. Minutes away from sending the audience home, it chooses to fall asleep on the job."
Culkin's performance in the film earned him a Razzie Award & a Stinkers Bad Movie Award nomination for Worst Actor (also for The Pagemaster and Richie Rich).


== Year-end lists ==
Dishonorable mention – Glenn Lovell, San Jose Mercury News


== References ==


== External links ==
Getting Even with Dad on IMDb
Getting Even with Dad at Rotten Tomatoes
Getting Even with Dad at Box Office Mojo