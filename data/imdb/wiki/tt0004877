All for Peggy is a 1915 American silent drama short film directed by Joe De Grasse, written by his wife Ida May Park and featuring Lon Chaney and Pauline Bush. The film is now considered to be lost. Lon Chaney had a very small role in the film. A still exists showing Lon Chaney in the role of Seth, the stable groom.


== Plot ==
Peggy Baldwin is engaged to Will Brandon, whose wealthy father James Brandon does not approve of his son marrying Peggy because he considers her to be of low station. The senior Brandon is the owner of a race horse named Ladybird, and Peggy's father Seth Baldwin (Lon Chaney) is the stable groom for the horse. Will makes a bet with his father that if Ladybird wins the race the following day, he will break off his engagement to Peggy. But if Ladybird loses the race, Will is free to marry Peggy.
Will finds out that his father has inside information however that Ladybird is a sure winner and feels like he was tricked. So Will plans to fix the race by asking Peggy's brother Ted (the horse's jockey) to feign being intoxicated that day and not show up to ride Ladybird. Peggy is alarmed when her brother fails to show up for the race, so she dons his jockey suit and rides the horse in his place. Ladybird wins, and Peggy almost faints from exhaustion afterwards. After the race, Will and his father learn that it was Peggy who rode the horse. The elder Brandon is so amused by Peggy's amazing feat, he allows the two young people to marry.


== Cast ==
Pauline Bush as Peggy Baldwin
William C. Dowlan as Will Brandon, Peggy's fiance
T. D. Crittenden as James Brandon, Ladybird's owner
Lon Chaney as Seth Baldwin, the stable groom
Harry Gleizer as Ted Baldwin, Ladybird's jockey
Anna Thompson as Mrs. Brandon


== Reception ==
"A pleasing number in which Pauline Bush appears as the daughter of a groom. The scenes lack the suspense they might have had, but the characterizations and action are good." --- Moving Picture World"This is an enjoyable comedy-drama produced by Joseph De Grasse. Pauline Bush is Peggy and she shows herself an expert horsewoman in the race. William C. Donlan is her lover, while Lon Chaney has a small part as the girl's father.--- Motion Picture News


== References ==


== External links ==
All for Peggy on IMDb
Reed, Inez (April 1915). "All for Peggy". Photoplay. Chicago, Ill: Photoplay Publishing Co. VII (5): 132ff. Retrieved December 26, 2013.