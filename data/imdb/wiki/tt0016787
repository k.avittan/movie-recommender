The penal colony of Cayenne (French: Bagne de Cayenne), commonly known as Devil's Island (Île du Diable), was a French penal colony that operated in the 19th and 20th century in the Salvation's Islands of French Guiana. Opened in 1852, the Devil's Island system received convicts from the Prison of St-Laurent-du-Maroni. All had been deported from all parts of the Second French Empire, and it was infamous for its harsh treatment of detainees, with a death rate of 75% at the worst, until it was closed down in 1953. Devil's Island was notorious for being used for the internal exile of French political prisoners, with the most famous being Captain Alfred Dreyfus.


== Organization ==
The prison system stretched over several locations, on the mainland and in the off-shore Salvation's Islands. Île Royale was the reception centre for the general population of the penal colony; they were housed in moderate freedom due to the difficulty of escape from the island. Saint-Joseph Island was the Reclusion, where inmates were sent to be punished by solitary confinement in silence and darkness for escapes or offences committed in the penal colony. Devil's Island was for political prisoners. In the 19th century, the most famous such prisoner was Captain Alfred Dreyfus. In addition to the prisons on each of the three islands in the Salut island group, the French constructed three related prison facilities on the South American mainland, just across the straits at Kourou; 50 kilometres (30 mi) east in Cayenne, which later became the capital of French Guiana; and 160 km (100 mi) west at St. Laurent.


== Early penal system ==

Prisoners convicted of felonies in the 17th and 18th centuries were sentenced to serve as oarsmen in the French Mediterranean galley fleet. Given the harsh conditions, this was virtually a death sentence. Following the decommissioning of the Mediterranean galley fleet in 1666, the majority of prisoners were paired together in chains aboard galley hulks (bagnes) moored in French harbours until the barges rotted and sank. The prisoners were moved to live on the adjacent pontoons. Prisoners relied on charity or their families for food, bedding and clothing. They were required to work 12 hours a day in the docks, earning 10-15 centimes, which they could spend on food and wine. Other prisoners were housed in prisons onshore, where conditions were apparently so bad that many prisoners would beg to be transferred to the hulks.
By the early 19th century, the French urban population had increased from under six million to over 16 million, with a commensurate increase in crime. In 1832 legislation was passed mandating the state's provision of basic necessities to prisoners; however, prison reform changed the previous reliance on corporal punishment to imprisonment with a goal of vengeance and deterrence, with imprisonment considered a way to remove offenders from society. Recidivism of up to 75% had become a major problem; unemployed released prisoners began flooding the cities. In the 1840s, the state set up internal agricultural penal colonies as a place to receive prisoners, thereby removing them from urban environments and giving them employment. Prisoners were commonly sentenced under doublage by which, on completion of their sentence, they were required to work as employees at the penal colony for an additional period equal to their original sentence.The French Navy, which had been tasked with managing the prison hulks, complained strongly about the cost of guarding the hulks and the disruption they caused to the shipyards. Following his coup in 1851, Emperor Napoleon III ordered that the hulks be permanently closed and that civil law convicts be transferred overseas to colonies. Debate over where the convicts would be sent was prolonged. Algeria was ruled out by the Navy as it was controlled by the French Army; Haiti, Cuba, the Dominican Republic and Texas were considered, but the government eventually chose French Guiana.
France had repeatedly failed since 1604 to colonize French Guiana. The last attempt at colonization was in 1763, and 75% of the 12,000 colonists that had been sent there died in their first year. By the 1850s, the declining number of survivors were on the brink of extinction. In 1852, Napoleon called for volunteer prisoners from the hulks to transfer to the new Bagne de Cayenne (Cayennes penal colony) at French Guiana; 3,000 convicts applied. Two categories of prisoners were eligible for transportation: transportés, those civil-law prisoners sentenced under doublage, and déportés, prisoners convicted of political crimes, such as espionage or conspiracy. The hulks continued to be used, housing an average of 5,400 prisoners at a time, until they were finally closed around the end of the 19th century. The agricultural penal colonies continued to be used for juveniles until the last was closed in 1939.


== Use as penal colony ==
The islands were part of a penal colony from 1852 onwards for criminals of France, who were convicted by juries rather than magistrates. The main part of the penal colony was a labor camp that stretched along the border with Dutch Guiana (present-day Suriname). This penal colony was controversial as it had a reputation for harshness and brutality. Prisoner-on-prisoner violence was common; tropical diseases were rife. Only a small minority of broken survivors returned to France to tell how horrible it was; they sometimes scared other potential criminals to go straight.  This system was gradually phased out and has been completely shut down since 1953. Since the late 20th century, the islands have been tourist destinations. The islands were featured in the book Papillon (1969), published as a memoir by Henri Charrière, a former prisoner who claimed to have escaped the island (but in reality had never visited there).
Devil's Island and associated prisons eventually became one of the most infamous prison systems in history. While the prison system was in use (1852–1953), inmates included political prisoners (such as 239 republicans who opposed Napoleon III's coup d'état in 1851) and the most hardened of thieves and murderers. The vast majority of the more than 80,000 prisoners sent to the Devil's Island prison system never made it back to France.  Many died due to disease and harsh conditions. Sanitary systems were limited, and the region was mosquito-infested, with endemic tropical diseases. The only exit from the island prisons was by water, and few convicts escaped.
Convicts who were lucky enough to have family or friends willing to send them money had to have it sent to them in care of a prison guard. The standard practice was for the guard to keep for himself a quarter of the amount sent and give the rest to the prisoner.
On 30 May 1854, France passed a new law of forced residency.  It required convicts to stay in French Guiana after completion of sentence for a time equal to their forced labour time. If the original sentence exceeded eight years, they were forced to stay as residents for the remainder of their lives and were provided land to settle on. In time, a variety of penal regimes emerged, as convicts were divided into categories according to the severity of their crimes and the terms of their imprisonment or "forced residence" regime.An 1885 law provided for repeat offenders for minor crimes to be sent to the French Guiana prison system, previously reserved for serious offenders and political prisoners. A limited number of convicted women were also sent to French Guiana, with the intent that they marry freed male inmates to aid in settlement and development of the colony. As the results were poor, the government discontinued the practice in 1907. On Devil's Island, the small prison facility did not usually house more than 12 persons.

The horrors of the penal settlement were publicized during the Dreyfus affair, as the French army captain Alfred Dreyfus was unjustly convicted of treason and sent to Devil's Island on 5 January 1895. In 1938 the penal system was strongly criticized in René Belbenoît's book Dry Guillotine.
Shortly after the release of Belbenoît's book, which aroused public outrage about the conditions, the French government announced plans to close the bagne de Cayennes. The outbreak of World War II delayed this operation but, from 1946 until 1953, one by one the prisons were closed. The Devil's Island facility was the last to be closed.
The cable car system that provided access to Devil's Island from Royale Island deteriorated and Devil's Island is now closed to public access.  It can be viewed from off shore by use of charter boats. The two larger islands in the Salut island group are open to the public; with some of the old prison buildings restored as museums, they have become tourist destinations.


== Alleged and successful escapes ==


=== Clément Duval ===
Clément Duval, an anarchist, was sent to Devil's Island in 1886. Originally sentenced to death, he later received a commuted sentence of hard labour for life. He escaped in April 1901 and fled to New York City, where he remained for the rest of his life. He eventually wrote a book about his imprisonment called Revolte.


=== François Frean, Paul Renuci, Raymond Vaude, and Giovanni Batistoti ===
Four escapees from Devil's Island arrived in St. Thomas, U.S. Virgin Islands on 18 October 1936. Their native boat was nearly wrecked on the reef and the convicts were initially entertained as guests and treated for injuries at the Municipal Hospital. The fugitives, François Frean, 37, Paul Renuci, 32, Raymond Vaude, 35, all French, and Giovanni Batistoti, 35, an Italian, were reported to have suffered hardships.


=== Henri Charrière and Sylvain ===
Henri Charrière's bestselling book Papillon (1969) describes his successful escape from Devil's Island, with a companion, Sylvain. They used two sacks filled with coconuts to act as lifeboat. According to Charrière, the two men leaped into heavy seas from a cliff and drifted to the mainland over a period of three days. Sylvain died in quicksand a short distance from the shore. From then, Charrière was to meet the man of the name Cuic-Cuic who would help him escape again to the freedom he always wanted. But he was caught again and served in the Bagne at El Dorado where he would soon become free for life and lived in Venezuela.
Charrière's account aroused considerable controversy. French authorities disputed it and released penal colony records that contradicted his account. Charrière had never been imprisoned on Devil's Island. He had escaped from a mainland prison. French journalists or prison authorities disputed other elements of his book and said that he had invented many incidents or appropriated experiences of other prisoners. Critics said he should have admitted his book was fiction.


=== Felix Milani ===
Felix Milani travelled on the same ship over as Henri Charrière and wrote a book about his experiences titled The Convict.


=== René Belbenoît ===
René Belbenoît is perhaps the most renowned escapee of the penal colony, who wrote about his experiences in two well-received memoirs: Hell on Trial and  The Dry Guillotine: Fifteen Years Among the Living Dead. After leaving the colony with permission, he made his way to the Panama Canal where he worked for nearly a year. Next he travelled to France to attempt to gain his freedom before being returned and imprisoned on the colony.


=== Francis Lagrange ===
Francis Lagrange was a painter and forger who wrote a book about his experiences on Devil's Island.


=== Bernard Carnot ===
According to the second memoir of American sailor and writer William Willis (Damned and Damned Again), a few days after New Years in 1938, he rented a room in New York City from a French immigrant named Madame Carnot. Her son, Bernard Carnot, had been sent to Devil's Island in 1922 for a murder that he did not commit, and the Carnot family had since moved to America. Out of compassion and a sense of adventure, Willis set out to the penal colony to effect Bernard Carnot's escape, which he eventually accomplished. The subtitle of the book indicates that it documents the 'true story of the last escape from Devil's Island'.
Having been smuggled to Brazil aboard a supply ship, Carnot would never reunite with his family, although they learned via Willis that he had gained his freedom. On the outbreak of WW2 he returned to Europe and joined the French forces. He is believed to have been killed in action shortly before the liberation of Strasbourg.


== Aftermath ==

In 1938, the French government stopped sending prisoners to Devil's Island. In 1953, the prison system was finally closed entirely. Most of the prisoners at the time returned to metropolitan France, although some chose to remain in French Guiana.
In 1965, the French government transferred the responsibility for most of the islands to its newly founded Guiana Space Centre. The islands are under the trajectory of the space rockets launched from the Centre eastward, toward the sea (to geostationary orbit). They must be evacuated during each launch. The islands host a variety of measurement apparatus for space launches.The CNES space agency, in association with other agencies, has restored buildings classified as historical monuments. Since tourism facilities have been added, the islands now receive more than 50,000 tourists each year.


== Cultural references ==

The bestselling memoir by Henri Charrière, Papillon (1969), described the extreme brutality and inhumane treatment of the penal colony.  The book was adapted as an American movie of the same name; released in 1973, it starred Steve McQueen and Dustin Hoffman. Later historical analysis of Charrière's "memoir", from the actual records of the penal colony, show that most of what he wrote never happened, were embellishments or were feats ascribed to others. Although prisoners were not treated well, conditions were not as bad as in Charrière's account. A remake of Papillon was released in 2017.
The Danish novel Helvede hinsides havet (1933) (Hell beyond the Sea), by Aage Krarup Nielsen, describes the life in the camp.
Devil's Island is featured in the plot of The Dain Curse (1928), a novel by Dashiell Hammett, the American mystery writer.In 1939, Boris Karloff was cast as Dr. Charles Gaudet in the film Devil's Island.
In the fifth season of Frasier, episode 23, "Party, Party", Niles extols the accomplishments of Seattle's exclusive Safari Club with "These are the people who introduced badminton to Devil's Island!"
In the 1944 film To Have and Have Not, Paul de Bursac (Walter Surovy) tells Harry Morgan (Humphrey Bogart): "Did you ever hear of Pierre Villemars?  .... He's on Devil's Island, they sent me here to get him, to bring him back here to Martinique."Season 2, episode 8 of The Wild Wild West (4 Nov. 1966) took place on Devil's Island.
Episode 9 of The Time Tunnel (11 Nov. 1966), appropriately titled "Devil's Island", was set on Devil's Island.
In season 1, episode 9 of Star Trek: The Original Series, "Dagger of the Mind", the Tantalus Penal Colony is nicknamed Devil's Island by episode antagonist Dr. Tristan Adams.
Devil's Island is the setting of Argentine author Adolfo Bioy Casares's Plan de Evasion (A Plan for Escape), published in 1969. The novella tells of a French military officer sent to the archipelago and his bizarre interactions with the island's governor and staff.
In the 2003 episode "Bend Her" of the animated comedy Futurama, Devil's Island is seen to have gained sufficient autonomy to enter the 3004 Olympics; the athletes appear to be wearing striped prison uniforms.
"Devils Island" is the title of a song by the band Megadeth on their 1986 album Peace Sells... but Who's Buying? The song expresses the thoughts of a prisoner on Devil's Island about to be executed. In the song, the prisoner's life is spared by God just as he is about to be killed, but he is condemned to spend the rest of his life on Devil's Island.In the 1925 film The Phantom of the Opera, starring Lon Chaney, it is mentioned that the Phantom had escaped Devil's Island, unlike the novel, in which he studied in Persia.
We're No Angels is a 1955 movie directed by Michael Curtiz which starred Humphrey Bogart, Aldo Ray, and Peter Ustinov as escapees from Devil's Island.
Devil's Island has one full episode of Dave Salmoni's Deadly Islands series dedicated to it. Aired on Animal Planet (Discovery Channel Network) in 2015, the episode documents Salmoni's exploration of the island together with talk of its flora & fauna, dangers, and past as host to a network of penitentiaries.
The life of Vere St. Leger Goold is the subject of a theatrical play called Love All: he was a top tennis player in the 19th century before being convicted of murder and being sent to Devil's Island, where he committed suicide.Isle of the Damned by G.J. Seaton, the story of an illegitimate son of a distant relative of the British Royal family sentenced to penal servitude.
The Man From Devil's Island by Colin Rickards tells the story of a German prisoner who escaped the colony.
Director Frank Borzage's 1940 film Strange Cargo stars Clark Gable, Joan Crawford, Ian Hunter, Peter Lorre, and Albert Dekker in the story of several convicts (and one woman) who escape from Devil's Island and are led by a man (Hunter) who may or may not be the personification of God. Lorre is a bounty-hunter in hot pursuit of the convicts and Crawford.Devil's Island is also the title of a song by musical group CocoRosie, featured as a hidden track on the album Tales of a GrassWidow.
In Tour de Farce, a short film starring The Inspector, the title character accompanies a prisoner to Devil's Island.
In the popular Hank the Cowdog book series by John R. Erickson, Hank refers to the Twitchell Dog Pound as "Devil's Island for dogs."
William Willis's adventure on Devil's Island was featured in the Season 4 premiere of Drunk History on Comedy Central.
In Neal Stephenson's 2015 novel "Seveneves", Devil's Island was mentioned when characters were discussing space flight launches out of Kourou.


== See also ==

Charles DeRudio


== References ==


== Further reading ==
Belbenoit, René. 1940.  @#!*%  on Trial. Translated from the French by Preston Rambo. E. P Dutton & Co. Reprint by Blue Ribbon Books, New York, 1941.
Belbenoit, René. 1938. Dry Guillotine: Fifteen Years among the Living Dead. Reprint: Berkley (1975). ISBN 0-425-02950-6. Reprint: Bantam Books, 1971.
W.E. Allison-Booth. 1931. Hell's Outpost: The True Story of Devil's Island By a Man Who Exiled Himself There. Minton, Balch & Company, 1931.
Seaton, George John. Isle of the Damned: Twenty Years in the Penal Colony of French Guinea.  Farrar, Straus and Young, 1951.  Also published in England as Scars Are My Passport.
Charrière, Henry. Papillon. Reprints: Hart-Davis Macgibbon Ltd. 1970. ISBN 0-246-63987-3 (hbk); Perennial, 2001. ISBN 0-06-093479-4 (sbk).
Godfroy Marion, Bagnards, Tallandier, 2008.
Godfroy Marion, Bagnards, édition du chêne, 2002 (Ranked as "Best coffee table book of the year" by Le Monde).
CNES, Dossier de presse Îles du Salut
Rickards, Colin. The Man From Devil's Island Peter Dawnay Ltd., London, 1968. Hardback
Nicol Smith, Black Martinique, Red Guiana, 1942.
Willis, William. 1959. Damned and Damned Again: The True Story of the Last Escape from Devil's Island. New York: St. Martin's Press.


== External links ==
"Devil's Island French Penal Colony", Salvation Army history
"Devil's Island" . New International Encyclopedia. 1905.