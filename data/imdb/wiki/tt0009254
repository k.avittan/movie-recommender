The power key, or power button, is a key found on many computer keyboards during the 1980s and into the early 2000s. They were introduced on the first Apple Desktop Bus keyboards in the 1980s and have been a standard feature of many Macintosh keyboards since then. They are also found on an increasing number of Microsoft Windows keyboards, sometimes supplanted with additional keys for sleep. The power key is becoming increasingly rare, as most modern personal computers using USB allow the system to be started up by pressing any key on the keyboard.


== Mac ==

The Apple Desktop Bus (ADB) was introduced on the Apple IIGS in 1986. This peripheral bus was intended to connect low-speed input devices like keyboards and computer mice. Looking for a low-cost connector, the design team selected the 4-pin mini-DIN connector, which is also used for S-Video. ADB only used one data pin and +5V and ground, leaving one pin free. This was used to implement the PSW connection, used to turn on the machine. A separate connection was required as the keyboard controller of machines of this era was not powered when the machine was powered down.Introduced along with the IIGS was the Apple Keyboard, which featured a rather large power key roughly centered above the main part of the keyboard. A power key became a standard feature of all ADB keyboards, notably the lauded Apple Extended Keyboard and its follow-ons. It began to become less common on later USB-based keyboards as these machines keep the keyboard powered when the machine is sleeping, so any key can act as a power key. Power keys remain on some modern Macs, including the MacBook Air, but most others have replaced it with an "eject" key, formerly used to open the now-non-existent CD drive.


== Windows ==

Early Windows PCs generally lacked the ability to control power through software, and power keys were not physically possible. The increasing use of USB connections allowed these, but the ability to use any key for "power on" generally negated their need in most cases. Power keys did become relatively common on some multimedia keyboards, where they were known as power management keys.Special keycodes are associated with these functions, e05e for Power, e05f for Sleep, and e063 Wake. These are supported in Microsoft Windows and various Unixen.


== References ==