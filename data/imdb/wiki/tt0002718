A brother is a man or boy who shares one or more parents with another. The female counterpart is a sister. Although the term typically refers to a familial relationship, it is sometimes used endearingly to refer to non-familial relationships. A full brother is a first degree relative.


== Overview ==

The term brother comes from the Proto-Indo-European *bʰréh₂tēr, which becomes Latin frater, of the same meaning. Sibling warmth, or sibling affect between male siblings has been correlated to some more negative effects. In pairs of brothers higher sibling warmth is related to more risk taking behaviour although risk taking behaviour is not related to sibling warmth in any other type of sibling pair. The cause of this phenomenon in which sibling warmth is only correlated with risk taking behaviours in brother pairs still is unclear. This finding does, however, suggest that although sibling conflict is a risk factor for risk taking behaviour, sibling warmth does not serve as a protective factor. Some studies suggest that girls having an older brother delays the onset of menarche by roughly one year. Research also suggests that the likelihood of being gay increases with the more older brothers a man has. Some analyzers have suggested that a man's attractiveness to a heterosexual woman may increase with the more he resembles her brother, while his unattractiveness may increase the more his likeness diverges from her brother. Females with a twin or very close-in-age brother, sometimes view him as their male alter ego, or what they would have been like, if they had a Y chromosomes.


== Fraternal relationship ==

The book Nicomachean Ethics, Book VIII written by Aristotle in 350 B.C.E., offers a way in which people should view the relationships between biological brothers. The relationship of brothers is laid out with the following quote: 
"The friendship of brothers has the characteristics found in that of comrades and in general between people who are like each other, is as much as they belong more to each other and start with a love for each other from their very birth, and in as much as those born to the same parents and brought up together and similarly educated are more akin in character; and the test of time has been applied most fully and convincingly in their case".
For these reasons, it is the job of the older brother to influence the ethics of the younger brother by being a person of good action. Aristotle says "by imitating and reenacting the acts of good people, a child becomes habituated to good action". Over time the younger brother will develop the good actions of the older brother as well and be like him. Aristotle also adds this on the matter of retaining the action of doing good once imitated: "Once the habits of ethics or immorality become entrenched, they are difficult to break." The good habits that are created by the influence of the older brother become habit in the life of the younger brother and turn out to be seemingly permanent. It is the role of the older brother to be a positive influence on the development of the younger brother's upbringing when it comes to the education of ethics and good actions. When positive characteristics are properly displayed to the younger brother by the older brother, these habits and characteristics are imitated and foster an influential understanding of good ethics and positive actions.


== Famous brothers ==

Gracchi, Ancient Roman reformers
George Washington Adams, John Adams II, and Charles Francis Adams Sr., politicians
Ben Affleck and Casey Affleck, actors
Alec Baldwin, William Baldwin, Stephen Baldwin, Daniel Baldwin, also known as the Baldwin brothers; actors
The Alexander Brothers; musicians
John and Lionel Barrymore, actors
Chang and Eng Bunker, the original Siamese twins
George W. Bush, Jeb Bush, Neil Bush and Marvin Bush, sons of George H. W. Bush
David Carradine, Keith Carradine, and Robert Carradine, American actors
Bill Clinton, 42nd President of the United States,  and Roger Clinton, Jr., his younger half-brother
Joel and Ethan Coen; filmmakers
Stephen Curry and Seth Curry; current NBA point guards in the Western Conference
Dizzy and Daffy Dean, Major League Baseball pitchers
Mark DeBarge, Randy DeBarge, El DeBarge, James DeBarge, and Bobby DeBarge, the male members of the singing group DeBarge
Emilio Estevez and Charlie Sheen, actors
Isaac Everly and Phil Everly, The Everly Brothers, singers
Liam Gallagher and Noel Gallagher, members of Oasis (band)
Barry Gibb, Robin Gibb, and Maurice Gibb, members of the Brothers Gibb or "Bee Gees" singing group
John Gotti, Eugene "Gene" Gotti, Peter Gotti and Richard V. Gotti, and Vincent Gotti, New York "made men" with the Gambino crime family
Jacob Grimm and Wilhelm Grimm, known as the Brothers Grimm, German academics and folk tale collectors
Matt Hardy and Jeff Hardy, professional wrestlers
Pau and Marc Gasol, professional basketball players
O'Kelly Isley, Jr., Rudolph Isley, and Ronald Isley, Ernie Isley, Marvin Isley, and Vernon Isley, members of The Isley Brothers singer-songwriting group and band, which also included their brother-in-law, Chris Jasper
Jackie Jackson, Tito Jackson, Jermaine Jackson, Marlon Jackson, Michael Jackson and Randy Jackson, members of The Jackson 5 and later, The Jacksons
Jesse and Frank James, Old West outlaws
John, Robert and Ted Kennedy, politicians
Terry Labonte and Bobby Labonte, race car drivers
Robert Todd Lincoln, Edward Baker Lincoln, William Wallace Lincoln and Tad Lincoln, sons of Abraham Lincoln
Eli and Peyton Manning, National Football League quarterbacks
Mario and Luigi, video game characters
Justin, Travis, and Griffin McElroy, podcasters
Billy Leon McCrary (December 7, 1946 – July 14, 1979) and Benny Loyd McCrary, wrestlers known as The McGuire Twins
Ringling brothers, circus performers, owners, and show runners.
Russo brothers, filmmakers, producers, and directors
Alan Osmond, Wayne Osmond, Merrill Osmond, Jay Osmond and Donny Osmond, members of The Osmonds
Logan Paul and Jake Paul, Youtubers, internet personalities, and actors
Neil and Ronald Reagan
John D. Rockefeller and William Rockefeller, co-founders of Standard Oil and members of the Rockefeller family
Thomas "Tommy" Smothers and Richard "Dick" Smothers, performing artists known as the Smothers Brothers
JJ Watt, TJ Watt, Derek Watt, National Football League Players
Damon Wayans, Dwayne Wayans, Keenan Ivory Wayans, Marlon Wayans, Shawn Wayans, performing artists, directors and producers
Bob Weinstein and Harvey Weinstein, film producers
Brian Wilson, Dennis Wilson, and Carl Wilson, members of The Beach Boys
Marvin Winans, Carvin Winans, Michael Winans, and Ronald Winans, members of The Winans, singers and musicians
Orville Wright and Wilbur Wright, known as the Wright brothers, pioneer aviators
Daniel Sedin and Henrik Sedin, professional hockey players
Wallace Shawn and Allen Shawn, writer and composer of The Fever


== Other works about brothers ==

In the Bible:
Cain and Abel, the sons of Adam and Eve
Jacob and Esau, the sons of Isaac and Rebecca
Moses and Aaron, prophet
Sts. Peter and Andrew, apostles
Sts. James and John, apostles
My Brother, My Brother, and Me, podcast
Saving Private Ryan (1998), film
Simon & Simon, television series
Supernatural, American television series
The Brothers Karamazov, novel
The Wayans Bros., television series
Bonanza (1959–1973), television series
In the Ramayana:
Rama, Lakshmana, Bharata, and Shatrughna
In the Mahabharata:
The Pandavas – Yudhishthira, Arjuna, Bhima, Sahadeva and Nakula
The Kauravas – One hundred brothers including Duryodhana, Dushasana and Vikarna, among others


== See also ==
Sister
Brotherhood (disambiguation)
Stepsibling


== References ==


== External links ==
 The dictionary definition of brother at Wiktionary