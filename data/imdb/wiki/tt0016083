"Man in the Box" is a song by the American rock band Alice in Chains. It was released as a single in January 1991 after being featured on the group's debut studio album Facelift (1990). It peaked at No. 18 on Billboard's Mainstream Rock chart and was nominated for a Grammy Award for Best Hard Rock Performance in 1992. The song was included on the compilation albums Nothing Safe: Best of the Box (1999), Music Bank (1999), Greatest Hits (2001), and The Essential Alice in Chains (2006). "Man in the Box" was the second most-played song of the decade on mainstream rock radio between 2010 and 2019.


== Origin and recording ==
In the liner notes of 1999's Music Bank box set collection, guitarist Jerry Cantrell said of the song; "That whole beat and grind of that is when we started to find ourselves; it helped Alice become what it was."The song makes use of a talk box to create the guitar effect. The idea of using a talk box came from producer Dave Jerden, who was driving to the studio one day when Bon Jovi's "Livin' on a Prayer" started playing on the radio.The original Facelift track listing credited only vocalist Layne Staley and Jerry Cantrell with writing the song. All post-Facelift compilations credited the entire band. It is unclear as to why the songwriter credits were changed.


== Composition ==
"Man in the Box" is widely recognized for its distinctive "wordless opening melody, where Layne Staley's peculiar, tensed-throat vocals are matched in unison with an effects-laden guitar" followed by "portentous lines like: 'Feed my eyes, can you sew them shut?', 'Jesus Christ, deny your maker' and 'He who tries, will be wasted' with Cantrell's drier, less-urgent voice," along with harmonies provided by both Staley and Cantrell in the lines 'Won't you come and save me'.


== Lyrics ==
In a 1992 interview with Rolling Stone, Layne Staley explained the origins of the song's lyrics:

I started writing about censorship. Around the same time, we went out for dinner with some Columbia Records people who were vegetarians. They told me how veal was made from calves raised in these small boxes, and that image stuck in my head. So I went home and wrote about government censorship and eating meat as seen through the eyes of a doomed calf.
Jerry Cantrell said of the song:

But what it's basically about is, is how government and media control the public's perception of events in the world or whatever, and they build you into a box by feeding it to you in your home, ya know.  And it's just about breaking out of that box and looking outside of that box that has been built for you.
In a recorded interview with MuchMusic in 1991, Staley stated that the lyrics are loosely based on media censorship, and "I was really really stoned when I wrote it, so it meant something different then", he said laughing.


== Release and reception ==
"Man in the Box" was released as a single in 1991. "Man in the Box" is widely considered to be one of the band's signature songs, reaching number 18 on the Billboard Album Rock Tracks chart at the time of its release. According to Nielsen Music's year-end report for 2019, "Man in the Box" was the second most-played song of the decade on mainstream rock radio with 142,000 spins.The song was number 19 on VH1's "40 Greatest Metal Songs", and its solo was rated the 77th greatest guitar solo by Guitar World in 2008. It was number 50 on VH1's "100 Greatest Songs of the 90s" in 2007. The song was nominated for the Grammy Award for Best Hard Rock Performance in 1992.Steve Huey of AllMusic called the song "an often overlooked but important building block in grunge's ascent to dominance" and "a meeting of metal theatrics and introspective hopelessness."


== Music video ==
The MTV music video for the track was released in 1991 and was directed by Paul Rachman, who later directed the first version of the "Sea of Sorrow" music video for the band and the 2006 feature documentary American Hardcore. The music video was nominated for Best Heavy Metal/Hard Rock Video at the 1991 MTV Video Music Awards. The video is available on the home video releases Live Facelift and Music Bank: The Videos. The video shows the band performing in what is supposedly a barn, where throughout the video, a mysterious man wearing a black hooded cloak is shown roaming around the barn. Then, after the unknown hooded figure is shown, he is shown again looking around inside a stable where many animals live where he suddenly discovers and shines his flashlight on a man (Layne Staley) that he finds sitting in the corner of the barnhouse. At the end of the video, the hooded man finally pulls his hood down off of his head, only to reveal that his eyelids were sewn together with stitches the whole time. This part of the video depicts on the line of the song, "Feed my eyes, now you've sewn them shut". The man with his eyes sewn shut was played by a friend of director Paul Rachman, Rezin, who worked in a bar parking lot in Los Angeles called Small's.The music video was shot on 16mm film and transferred to tape using a FDL 60 telecine. At the time this was the only device that could sync sound to picture at film rates as low as 6FPS. This is how the surreal motion was obtained. The sepia look was done by Claudius Neal using a daVinci color corrector.Layne Staley tattooed on his back the Jesus character depicted in the video with his eyes sewn shut.


== Live performances ==
At Alice in Chains' last concert with Staley on July 3, 1996, they closed with "Man in the Box". Live performances of "Man in the Box" can be found on the "Heaven Beside You" and "Get Born Again" singles and the live album Live. A performance of the song is also included on the home video release Live Facelift and is a staple of the band's live show due to the song's popularity.


== Personnel ==
Layne Staley – lead vocals
Jerry Cantrell – guitar, talkbox, backing vocals
Mike Starr – bass
Sean Kinney – drums


== Chart positions ==


=== Weekly charts ===
Facelift versionLive version


=== Decade-end charts ===


== Cover versions ==
Richard Cheese and Lounge Against the Machine covered "Man in the Box" in a lounge style on their 2005 album Aperitif for Destruction. Platinum-selling recording artist David Cook covered the song during his 2009 Declaration Tour. Angie Aparo recorded a cover version for his album Weapons of Mass Construction. Apologetix parodied the song as "Man on the Cross" on their 2013 album Hot Potato Soup. Metal artist Chris Senter released a parody titled "Cat in the Box" in March 2015, featuring a music video by animator Joey Siler. Les Claypool's bluegrass project Duo de Twang covered the song on their debut album Four Foot Shack.


== In popular culture ==
Professional wrestler Tommy Dreamer used the song as his entrance music in Extreme Championship Wrestling from 1995 to 2001, and with his own wrestling promotion, House of Hardcore, since 2012.
The song appeared as a playable track in the video games Rock Band 2 and Guitar Hero Live.
"Man in the Box" has been featured in films such as Lassie (1994), The Perfect Storm (2000), Funny People (2009) and Always Be My Maybe (2019).
The song has been featured in TV shows including Beavis and Butt-Head (1993), Dead at 21 (1994), Cold Case (season 2, episode 13, "Time to Crime" in 2005), and Supernatural (season 12, episode 6, "Celebrating the Life of Asa Fox" in 2016).


== References ==


== External links ==
Review of "Man in the Box" at Allmusic
"Man in the Box" official music video on YouTube
Lyrics of this song at MetroLyrics