The Headless Horseman is a mythical figure who has appeared in folklore around the world since the Middle Ages. The figure is traditionally depicted as a man upon horseback who is missing his head.


== Description ==
Depending on the legend, the Horseman is either carrying his head, or is missing his head altogether, and is searching for it. Examples include the dullahan from Ireland, who is a demonic fairy usually depicted riding a horse and carrying his head under his arm; the titular knight from the English tale Sir Gawain and the Green Knight; and "The Legend of Sleepy Hollow," a short story written in 1820 by American Washington Irving, which has been adapted into several other works of literature and film including the 1949 Disney cartoon "The Adventures of Ichabod and Mr. Toad" and the 1999 Tim Burton film Sleepy Hollow.


=== In American folklore ===

The Headless Horseman is a fictional character from the 1820 short story "The Legend of Sleepy Hollow" by American author Washington Irving. The story, from Irving's collection of short stories entitled The Sketch Book of Geoffrey Crayon, Gent., has worked itself into known American folklore/legend through literature and film, including the 1999 Tim Burton film Sleepy Hollow.The legend of the Headless Horseman (also known as "the Headless Hessian of the Hollow") begins in Sleepy Hollow, New York, during the American Revolutionary War. Traditional folklore holds that the Horseman was a Hessian trooper who was killed during the Battle of White Plains in 1776. He was decapitated by an American cannonball, and the shattered remains of his head were left on the battlefield while his comrades hastily carried his body away. Eventually they buried him in the cemetery of the Old Dutch Church of Sleepy Hollow, from which he rises as a malevolent ghost, furiously seeking his lost head and wielding a Jack-o'-Lantern as a temporary replacement and/or weapon. Modern versions of the story refer his rides to Halloween, around which time the battle took place.The Headless Horseman is also a novel by Mayne Reid, first published in monthly serialized form during 1865 and 1866, and subsequently published as a book in 1866, based on the author's adventures in the United States. "The Headless Horseman" or "A Strange Tale of Texas" was set in Texas and based on a south Texas folk tale.


=== In Irish folklore ===
The dullahan or dulachán ("dark man") is a headless, demonic fairy, usually riding a horse and carrying his head under his arm. He wields a whip made from a human corpse's spine. When the dullahan stops riding, a death occurs. The dullahan calls out a name, at which point the named person immediately dies. In another version, he is the headless driver of a black carriage, the Cóiste Bodhar. A similar figure, the gan ceann ("without a head"), can be frightened away by wearing a gold object or putting one in his path.


=== In Scottish folklore ===
The most prominent Scots tale of the headless horseman concerns a man named Ewen decapitated in a clan battle at Glen Cainnir on the Isle of Mull. The battle denied him any chance to be a chieftain, and both he and his horse are headless in accounts of his haunting of the area.


=== In English folklore ===
The 14th century poem Gawain and the Green Knight features a headless horseman, the titular giant knight. After he is beheaded by Gawain, the Green Knight lifts his head up with one hand and rides from the hall, challenging Gawain to meet him again one year later.


== In popular culture ==


=== Comics ===
The comic book series Chopper, written by Martin Shapiro, is a modern-day reimagining of the Headless Horseman. It features a headless outlaw biker on a motorcycle who collects the souls of sinners. The only people who can see him are those who have consumed a strange new Ecstasy-like drug that triggers their sixth sense and opens a gateway to the afterlife. During the hallucinogenic high, any characters who have committed significant sins are hunted by the headless ghost. Once the drug wears off the victim is safe and beyond the Headless Horseman's ghostly reach.


=== Television ===
The Kolchak: The Night Stalker episode "Chopper" (initially broadcast on January 31, 1975) features a headless motorcyclist who enacts revenge for the loss of his head on a rival biker gang, 20 years after his murder.In the Midsomer Murders episode "The Dark Rider", a killer lures several victims to their deaths by masquerading as a headless horseman from local legend.A third-season episode of Nickelodeon's Are You Afraid of the Dark? depicts the Horseman as a real ghost, unlike the original novel.


=== Film ===
The Headless Horseman appears in the "Legend of Sleepy Hollow" segment of The Adventures of Ichabod and Mr. Toad. Just like the story, the Headless Horseman pursues Ichabod Crane which ends with the Headless Horseman throwing his pumpkin head at him. While it was mentioned what happened to Ichabod's hat was found near the shattered pumpkin, a rumor was mentioned that he has married a wealthy widow in a distant county with children who look like him. This rendition of the Headless Horseman was also featured as one of the villain characters in Disney's House of Mouse.In the 1999 Tim Burton film Sleepy Hollow, the Headless Horseman is the ghost of a murderous Hessian mercenary (performed by Ray Park in Headless Horseman form and portrayed by Christopher Walken in his true form) summoned by Katrina Van Tassel's stepmother Lady Van Tassel to eliminate her enemies. After Ichabod Crane returns his skull, the Horseman returns to Hell, taking Lady Van Tassel with him.


=== Book ===
Washington Irving's gothic story "The Legend of Sleepy Hollow" features a character known as the Headless Horseman believed to be a Hessian soldier who was decapitated by a cannonball in battle.


=== Video Games ===
The Headless Horseman is the central character in multiple video games such as Dullahan and Headless Jack. The horseman also appears in many other games such as World of Warcraft, The Elder Scrolls V: Skyrim and Team Fortress 2, as a minor or secret and usually adversarial character.


=== Miscellaneous ===
The Headless Horseman mascot for Sleepy Hollow High School, in Westchester County, New York, has been referred to as "America's scariest high school mascot".


== See also ==
Cephalophore, beheaded Christian martyred saints
Saints Gemolo and Himerius of Bosto
Headless Mule, a character in Brazilian folklore
List of ghosts
List of ghost films
Headless men


== Notes ==