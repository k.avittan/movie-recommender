Samuel Wilson (September 13, 1766 – July 31, 1854) was a meat packer from Troy, New York, whose name is purportedly the source of the personification of the United States known as "Uncle Sam".


== Biography ==
Samuel was born in the historic town of Arlington, Massachusetts (known as Menotomy at the time, township of West Cambridge), to parents Edward and Lucy Wilson.  Samuel Wilson is a descendant of one of the oldest families of Boston.  Through direct heritance of his grandfather, Robert Wilson, originally from Greenock, Scotland, he was Scottish with a Massachusetts background. As a boy, he moved with his family to Mason, New Hampshire.


=== Continental Army ===
While living in Mason at the young age of fourteen, Samuel joined the Continental Army on March 2, 1781.  His duties while enlisted consisted of guarding and caring for cattle, and mending fences, as well as slaughtering and packaging meat.  Guarding meat was a priority during the war.  It was not uncommon for enemies to tamper with and poison food sources.  Samuel's service to the Army most likely came to an end around October 19, 1781, with the surrender of Cornwallis at the Siege of Yorktown.


=== Move to New York ===
In 1789, at the age of 22, Samuel and his older brother Ebeneezer, age 27, relocated, by foot, to Troy, New York.  The Wilson brothers were amongst the first pioneer settlers of the community.  Troy was attractive to earlier settlers for its proximity to the Hudson River.  Samuel and his brother Ebeneezer partnered together and built several successful businesses.  Both were employees of the city as well as successful entrepreneurs.
After his relocation to Troy, Samuel drew upon his prosperous location.  He purchased property on Mount Ida (now Prospect Park), closely located to the Hudson River.  The combination of natural elements produced exceptional clay, ideal for brick making.  This began a new business venture for Samuel Wilson.  His bricks were the first native bricks of Troy.  Many historical buildings in Troy include bricks made by Wilson.  This was revolutionary during the 18th century.  Many bricks during this period were imported from the Netherlands.
On March 8, 1793, Samuel and his brother Ebeneezer leased the western half of lot 112, now 43 Ferry Street, from Jacob Vanderhyden for 30 shillings a year.  This was the year the Wilson brothers began E & S Wilson, which was the Wilson brothers’ introduction to the profitable meat business.  Their slaughterhouse was located between Adams (now Congress) Street and Jefferson Street.  The brothers took advantage of their prime location and built a dock at the foot of what is now Ferry Street.  Having such access to the Hudson River enabled E & S Wilson to prosper.


=== Marriage and children ===
Samuel returned to Mason, New Hampshire, in 1797, to marry Betsey Mann, daughter of Captain Benjamin Mann.  Samuel and Betsey were parents to four children, Polly (1797–1805), Samuel (1800–07), Benjamin (1802–59), and Albert (1805–66). Benjamin Wilson was the only child to have children.  He married Mary Wood, and together they were parents to Sarah, Elizabeth, Emma, and Marion. Albert married Amanda, but they had no children.
Samuel Wilson not only was a pioneer of Troy, he was active in the community, as well as an employee of the city.  On April 12, 1808, he took an oath as Office Assessor.  Four days later he took an Oath of Office as Path Master (now more commonly known as road commissioner).


=== War of 1812 ===
Samuel Wilson's career role during the War of 1812 is what he is most noted for today.  The demand for a supply of meat for the troops had significantly increased.  E & S Wilson's location and dock made the business ready and ideal.  
Secretary of War William Eustis made a contract with Elbert Anderson Jr. of New York City to supply and issue all rations necessary for the United States forces in New York and New Jersey for one year.  Anderson ran an advertisement on October 6, 13, and 20 looking to fill the contract.  E & S Wilson secured the contract for 2,000 barrels of pork and 3,000 barrels of beef for one year.  The business held a staff of 200 men during this period.
Samuel Wilson was appointed meat inspector for the American army.  His duties included checking the freshness of meat and assuring that it was properly packaged and that the barrels were according to specification.   Each barrel was required to be labeled.  Each barrel was marked “E.A.-U.S.”  This marking indicated Elbert Anderson, United States.  The great majority of E & S Wilson's meat was shipped close by to a camp of 6,000 soldiers in Greenbush, New York.  Many soldiers stationed in Greenbush were locals of Troy.  They knew of or were acquainted with Sam Wilson and his nickname Uncle Sam, as well as his meat packing business.  These soldiers recognized the barrels being from Troy and made an association between the "U.S." stamp and Uncle Sam.  Over time, it is believed, anything marked with the same initials, as much Army property was, also became linked with his name.


== Historical legacy ==

Samuel Wilson died July 31, 1854, at the age of 87.  He was originally buried in Mt. Ida Cemetery, but later transferred to Oakwood Cemetery in Troy. Monuments mark his birthplace in Arlington, Massachusetts, and site of burial in Troy, New York. A New Hampshire historical marker (number 35) titled "Uncle Sam's House" marks his boyhood home in Mason, New Hampshire.

While Samuel Wilson may or may not have been the original "Uncle Sam", the 87th United States Congress adopted the following resolution on September 15, 1961:
Resolved by the Senate and the House of Representatives that the Congress salutes Uncle Sam Wilson of Troy, New York, as the progenitor of America's National symbol of Uncle Sam.


== Gallery ==

		
		


== References ==


== External links ==
Samuel Wilson at Find a Grave