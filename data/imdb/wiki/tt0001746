Lost in the Arctic is a lost 1911 American silent drama film that portrayed the Inuit or "Eskimo" culture in the northern coastal area of Labrador. Directed by William V. Mong and produced by the Selig Polyscope Company, the "one-reeler" costarred Columbia Eneutseak, J. C. Smith, and also Mong. The film was not, as advertised by Selig and in trade publications in 1911, shot in the Arctic or even farther south in Labrador. It and another Selig release, The Way of the Eskimo, were produced at the same time in the same location in the United States, in Escanaba, Michigan. Both "Arctic" stories were filmed there in less than two weeks during the early winter months of 1911, staged along the frozen shoreline of Little Bay de Noc that connects to Lake Michigan.
Released in September 1911, this 820-foot film was distributed on a "split-reel", attached on the same 1000-foot reel to an unrelated 180-foot "topical" short titled Noted Men. That accompanying film, which had a running time less than three minutes, offered "intimate views" of the vice president of the United States at that time as well as four other prominent national politicians.


== Plot ==
This film is described in 1911 trade publications and in Selig promotional material as a story of adventure in the Eskimos' "land of eternal ice". The motion picture, according to reviews and plot descriptions in those publications, opened with snow-covered scenes of a group of Eskimos on a seasonal tribal hunt for game. Soon they discover a teenage orphan girl (Columbia Eneutseak) living alone in a hut. Very sick and without food, she has little chance of survival, so the tribal members refuse to adopt her into their community. They instead invoke the Eskimos' "unwritten law" that imposes a death sentence on anyone who is judged to be "too ill or too aged to participate in the annual hunt". In accordance with that law, the orphan is allowed to choose one of two means of death: she may either wander into the surrounding wilderness and be devoured by wolves and bears, or she may be set adrift in an umiak or open canoe without any supplies. She chooses the latter.
After conducting a solemn "'Ceremony of the Walrus Skull'", calling upon the spirits to guide the teenager on her "journey into darkness", tribal members place her into the canoe and launch her into open water bordered by immense fields of solid ice. Elsewhere, at the same time, a white explorer named Davis (William Monk) is desperate for food and is trying to spear seals though holes in the ice. He is the lone survivor of a "lost polar expedition", and as he continues his hunt, he suddenly realizes that the large section of ice on which he is standing has broken off from the ice field and is drifting out to sea. Seemingly doomed, Davis is later elated to sight the orphan's canoe, which has miraculously drifted toward his ice floe. Davis manages to join the girl, whose weak condition has only worsened. Captain John Smith (J. C. Smith), another Arctic explorer, is out searching for the lost expedition across the ice-strewn sea. Overcoming his own "trials and suffering" in the search, Smith finally locates his colleague and saves him as well as his sick Eskimo companion.


== Cast ==

William V. Mong as Davis, Arctic explorer
Columbia Eneutseak [Smith] as "The Eskimo Orphan"
J. C. Smith as Captain John Smith, Arctic explorer
Zacharias Zad as "The Bear Hunter"
Chief Opetek as chief of the tribe
[Esther] Eneutseak [Smith], the chief's wife
Pearytok as Inuit tribe member
Lolituk as Inuit tribe member
Autosig as Inuit tribe member
Beasotuk as Inuit tribe member
Magook as Inuit tribe member
Tava as Inuit tribe member


== Production ==
In addition to directing and acting in this film, William Mong wrote its screenplay or "scenario". The production at the time was one of several that Selig Polygraph planned with an Arctic theme. The trade journal The Moving Picture World reported in July 1911 that the film was among a "series of pictures made by Selig in the far north last winter". That publication and Selig itself continued to promote Lost in the Arctic as an adventure that was actually filmed there or at least along the northern coast of Labrador. It was instead one of two films shot on location at the same time in the United States, in Escanaba, Michigan, located approximately 300 miles north of Chicago. Situated next to the Little Bay de Noc linked to Lake Michigan, Escanaba offered a winter environment that on screen proved to be a convincing alternative to Canada's northern frontier. Today, nearly a dozen photographs documenting the filming of Lost in the Arctic and The Way of the Eskimo are preserved in Escanaba by the Delta County Historical Society. With the exceptions of Mong and J. C. Smith nearly the entire cast of this film consisted of Inuit performers, most of whom had been born in either Labrador or Newfoundland. The actress portraying the wife of the chief was Columbia Eneutseak's biological mother, Esther Eneutseak Smith.To enhance the "Arctic" realism of filming in Michigan, Monk staged and filmed a hunting expedition and the killing of a nonindigenous polar bear in Escabana, an event documented among the photographs held by the noted historical society. Selig in 1911 maintained a growing menagerie of wild and domesticated animals for use in its productions, including three live bears and "10 eskimo dogs". Most likely, the company shipped to Escanaba one of those bears from its large studio "plant" in Chicago or possibly from the company's newer, rapidly expanding "Pacific Coast" facilities in Edendale, Los Angeles. A live walrus was apparently shipped as well to Escanaba for the same purposes. In its July 1911 issue, the Chicago-based trade journal Motography states, "A valuable polar bear is slain in one of these far north plays and an Eskimo is seen killing the wary walrus by his primitive methods." The telling word regarding the bear in that report is "valuable", a strong implication that the animal was from Selig's menagerie, one its more expensive purchased specimens. While it is possible that some footage of the bear roaming on the ice or its killing was also used in The Way of the Eskimo, it is more likely the footage was used entirely in Lost in the Arctic. Selig advertisements and trade reviews for the latter production refer specifically to the "harpooning" and "thrilling" death of the polar bear. In addition, cast member "Zachariah" (Zacharias Zad) is credited in 1911 publications as "The Bear Hunter" in this film.


== Release and reception ==
Upon its release in September 1911, the film generally received positive reviews like The Way of the Eskimo, which had been distributed by Selig two months earlier. The correspondent for the trade journal Motion Picture News reviewed Lost in the Arctic among a series of other recent releases and reported that he enjoyed it "immensely". The Moving Picture World was equally impressed with the film, predicting that it would "outrival" The Way of the Eskimo, describing it as "one of the season's notable offerings". Two weeks later, The Moving Picture World continued to compliment the production and in another concise review highly recommended it to theater owners as an educational offering with solid box-office potential:An intensely interesting picture of the far north. It has a well-designed and fairly dramatic, though slight, plot; but shows so much of Eskimo customs and also so vividly pictures the experiences of exploring parties on the ice fields that it will be a very pleasing number on any bill. It is also instructive. The photographs must have been taken under difficulties. Some of them are very good. One scene shows the death of a polar bear. It's a good film.


== "Lost" film status ==
No copy of this one-reel Selig production is preserved in Library of Congress, the UCLA Film Archives, in the Department of Film at the Museum of Modern Art, the George Eastman Museum, the Library and Archives Canada (LAC), or in other major film repositories in the United States, Canada, or Europe. The film is therefore classified or "presumed to be" a lost production.


== Notes ==


== References ==


== External links ==
Lost in the Arctic on IMDb