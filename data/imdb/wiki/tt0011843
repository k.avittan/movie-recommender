"Wedding Bell Blues" is a song written and recorded by Laura Nyro in 1966. The best known version was a number one hit for the 5th Dimension in 1969.
The lyrics are written from the perspective of a woman whose boyfriend has not yet proposed to her, and who wonders, "am I ever gonna see my wedding day?" The song carries dual themes of adoring love and frustrated lament.
The title subsequently became a popular phrase in American pop culture.


== Laura Nyro recording ==

Nyro wrote "Wedding Bell Blues" at the age of 18 as a "mini-suite". The song originally featured several dramatic rhythmic changes—a trait Nyro explored on future albums. It was recorded in 1966 for Verve Folkways on her debut album More Than a New Discovery. Producer Herb Bernstein did not allow Nyro to record her original arrangement, which led to the artist more or less disowning the entire album.
As released by Nyro, the song is similar in content and arrangement to the later 5th Dimension version, albeit with a somewhat more soulful vocal line. Nyro's recording was released as a single in September 1966 and remained on Billboard's "Bubbling Under" chart for several weeks, peaking at No. 103.


== The 5th Dimension version ==
The 5th Dimension had already found hits with Nyro's "Stoned Soul Picnic" and "Sweet Blindness" during 1968. When recording tracks for their upcoming album The Age of Aquarius, producer Bones Howe suggested recording another Nyro song.
5th Dimension member Marilyn McCoo was then engaged to another member of the group, Billy Davis Jr., though they had not decided on an actual wedding date. In May 1969 the album was released. The first single released ahead of the album, "Aquarius/Let the Sunshine In", was a tremendous hit, while the success of the second single, "Workin' On a Groovy Thing", was much more modest. When a disc jockey in San Diego began playing "Wedding Bell Blues" from the album, Soul City Records saw the song's potential, and in September 1969 it was released as a single.
"Wedding Bell Blues" quickly soared to No. 1 on the U.S. pop singles chart, spending three weeks there in November, 1969 and made one of the group's somewhat rare appearances on the U.S. R&B singles chart, where it peaked at No. 23.  It was the group's second of five #1 songs on the U.S. adult contemporary chart. It was a top five hit in Canada, and placed in the top 20 on the UK Singles Chart—and their only hit there except for the earlier "Aquarius/Let the Sunshine In". It became a platinum record.
In 1969 television appearances, McCoo sang lead vocal parts of the song to Davis, who would then respond with quizzical looks. The rest of the 5th Dimension's early hits had featured more ensemble singing, and McCoo's prominent vocal and stage role on "Wedding Bell Blues" might have led to her being more featured in the group's early 1970s productions.


== Morrissey version ==
Morrissey released a version of the song for his album California Son on April 10, 2019. It features Billie Joe Armstrong of Green Day and Lydia Night of The Regrettes.


== Chart history ==


== See also ==
Hot 100 No. 1 Hits of 1969 (USA)


== References ==