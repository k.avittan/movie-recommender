Stop Thief is an American-originated electronic board game published by Parker Brothers.  It was released in 1979. In 2016, Restoration Games announced that they were bringing back a restored version of the game, using a free smartphone companion app in place of the electronic device. On April 11, 2017, they successfully funded a Kickstarter campaign for the game.


== Overview ==
Players play as detectives tracking an invisible thief.  The game is played on a board with two six sided dice, plastic detective figures to represent positions on the board, and a Crime Scanner, a handheld calculator-like electronic and sound effects device.


=== Game Board ===
The game board depicts four buildings with what type establishment it is along with a number (Furs - 1, Jewelry - 2, Antiques - 3, Bank - 4) and also four streets between and around the buildings that are also numbered (5, 6, 7, 8). Inside each building on alternate squares are two-digit numbers that indicate where the thief can end his movement, with four or five of them being in red to indicate a potential crime scene. The streets are likewise numbered with one square in red (the Newsstand). There are also five Subway squares--one on each corner of the board and one in the center--that the thief or the players can use to move from one subway station to another.


=== The Crime Scanner ===
The Crime Scanner is powered by a 9-volt battery and has a red LED display and fifteen buttons. The display shows the building (1-4) or street (5-8) the thief is currently located along with two letters designating the last action the thief took; the scanner also plays a sound whenever the thief performs these actions:

Cr - Crime; alarm
Fl - Walking across the floor; two footsteps
dr - Opened a door; door creaking open
GL - Broke a window; breaking glass
St - Ran down the street; several rapid footsteps
St - Took subway, then returned to the street; subway train, then several rapid footstepsIn addition to the On and Off buttons and ten buttons numbered 0-9, there are three other buttons of importance:

T - Tip: Gives the exact location of the thief by building/street number and two-digit square
A - Arrest: Allows the player to attempt an arrest by entering the building/street number and two-digit square where he believes the thief to be currently located
C - Clue: The thief performs his next action with the display updating accordingly


=== Other Game Parts ===
In addition to the detective tokens, dice, board, and Crime Scanner, the game also contains eight Detective Licenses, ten Wanted Posters, play money (in denominations of $500, $100, and $50) and a deck of Sleuth Cards.


=== Game Play ===
All players choose a detective token and license and place their tokens on the street space in front of the Acme Detective Agency. They also receive $300 and three Sleuth Cards that are dealt face-down with the remainder stacked and placed face-down. The players also select a Banker who is responsible for awarding/taking money, handling the Sleuth Cards, and handling the Wanted Posters. The Wanted Posters are shuffled, placed face-down, and the top one drawn; the poster has the reward for capturing the thief on the front, and the Banker takes money from the bank in the reward amount and places it on top of the poster.
After a roll of the dice determines who goes first, the player who starts turns on the Crime Scanner by pressing the On button twice. The scanner will play a sound (alarm) that starts the game and displays either the building (1-4) or street (5-8) on which the crime was committed along with the letters "Cr" to show the thief's action. The player rolls the dice and moves his piece in the direction of what he believes to be the thief's current location. The player can move vertically, diagonally, or horizontally, and is not required to take all the steps to which he is entitled. If the player enters one of the Subway squares, he can take it to any of the other four Subway squares on the board, but he also gives up any remaining moves that his dice roll gave him and ends his turn there. Play then moves to the first player's left, where the next player presses the Clue button for the thief's next move (in addition to the actions above, the thief can choose not to move; this is indicated with a short "dot" sound and the scanner continues to reflect the previous clue). The player conducts his turn same as the first player, and when done play continues in the same manner.
During a player's turn, he can play one of his three Sleuth Cards. These cards can be used to take money from other players, cause another player to skip his turn, give additional clues (pressing the Clue button a determined number of times), get a Tip to the thief's location, or move to another space on the board. Once a card is played, it is returned to the bottom of the Sleuth Cards stack and the player draws another card from the top. Only one card can be played per turn.
If the player lands on or next to a space where he thinks the thief is, he can attempt an arrest by pressing the Arrest key and entering the three-digit code (building/street number plus two-digit square number); once the last number is pressed, a police siren followed by the sound of machine gun bursts plays. After a momentary pause, the result is given:

Success - a two-tone "hee haw" siren plays; the player who made the arrest is given the reward money and play restarts with a new Wanted Poster and reward being drawn, and all players returning their tokens to the detective agency
Escape - a "Na-na-na-na-NA-na" sound plays as the thief taunts the detective, which is followed by five or six additional clues (including possibly more crimes) as the thief runs away; play then continues as normal
False Arrest - a "raspberry" sounds plays which occurs if the arrest location entered by the player is incorrect; the player loses his license and indicates so by turning it over; on his next turn, he pays $100 (or whatever he has available, whichever is less) to regain his license and ends his turn without performing any other actionsIn addition to being used for entering an arrest code, the number buttons also can be pressed to hear up to the last ten actions the thief committed (choosing not to move is not included). Hearing these clues does not count against the player in any way and does not cause the thief to move from his current location.


=== The Thief ===
The thief has certain rules for movement; some things he can do that the players cannot, and likewise some things he cannot do that the player can:

The thief can only end any movement he makes on a numbered square
Once he breaks a window or opens a door, he must go through it with his next action; players cannot go through a window but can choose to land on a door space and then backtrack without going to the other side of it
If running after escaping an arrest attempt, the thief can take the subway and continue to move the remainder of his five or six actions that he takes from his escape
The thief cannot return immediately to a square he just moved from; he must make at least one other move to another square first; players do not have this restriction
Neither the thief nor the players can climb a wall
When a thief commits a crime, the crime square will become a normal floor space and will play as such on the Crime Scanner until the thief leaves and returns to the building; the Newsstand will shut down when it is robbed and play as a normal street square until the thief is arrested
Each additional crime the thief commits before being caught adds $100 to the reward, which the Banker takes from the bank and adds to the reward money on the Wanted Poster


=== Winning the Game ===
The first player to accumulate $2,500 is the winner.


== References ==


== External links ==
Boardgames.about.com
Restoration Games - Stop Thief