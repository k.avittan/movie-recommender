Escape to the Chateau is a British Channel 4 documentary series which follows the story of couple Dick Strawbridge and Angel Adoree along with their family as they buy, renovate and redecorate the 19th-century Château de la Motte-Husson in Martigné-sur-Mayenne, France, and simultaneously juggle growing both their business and family. The first season follows Dick and Angel's quest to restore the chateau from its uninhabitable state by installing running water, heating and electricity throughout the 45-room castle, which had not been lived in for the past forty years. They are working against a deadline: their own wedding, which is to be held at the property. The subsequent seasons continue their story as they grow their events and wedding business and continue restoration, renovations and maintenance of the castle, and document the family events and milestones going on around them.


== Cast information ==


=== Dick Strawbridge ===
Formerly a Lieutenant Colonel in the British Army, Dick Strawbridge has appeared on numerous television shows such as It's Not Easy Being Green (2006), Coast (2006), Scrapheap Challenge (2009), The Hungry Sailors and Saturday Farm (2011), Dirty Rotten Survival (2015), and many others. However, his engineering and food experience is highlighted in Escape to the Chateau, with his can-do attitude and engineering expertise at the fore, as the work on the chateau and its close-surrounding land continues through the seasons.


=== Angel Adoree ===
Angel Adoree, who runs a hospitality business called The Vintage Patisserie, for which she sought investment when she appeared on Dragon's Den in 2010, provides a creative factor and flair for the show, adding her style and interior-decoration skill to the château as well as sharing in the interest in hospitality and entertaining to make lasting memories for their guests. Adoree was born Angela Newman and grew up on Canvey Island, where her family runs a jewelers' shop. She and Dick Strawbridge have two children.


== Episode list ==


=== Series 1 ===


=== Series 2 ===


=== Series 3 ===


=== Series 4 ===


=== Series 5 ===


=== Series 6 ===


=== Variant series ===
A second version of series 1 was shown on More 4.

Episode 1 is called "Hole in the Wall" (26 June 2016).
Episode 2 is called "They're our Flies" (3 July 2016).
Episode 3 is called "Angel's Palace of Dreams" (10 July 2016).More 4 has shown these additional episodes as "an extended four-part version of series one". These are described as follows:

"S1 Ep1/4 - Dick Strawbridge and Angel buy a fairy tale French chateau and set about bringing the abandoned building back to life",
"S1 Ep2/4 - Dick, Angel and the family move into their crumbling chateau, which they find home to bats, birds and thousands of flies",
"S1 Ep3/4 - Wanting to create a stunning honeymoon suite, Angel asks Dick to smash a hole in the three-feet-thick castle wall", and
"S1 Ep3/3 [sic] - Dick and Angel are due to marry in just a few weeks, expecting 200 guests. But room after room still needs to be restored, decorated and furnished, and the moat needs damming."Episode 4 of the More 4 series seems to be the third episode of the original three-part Channel 4 series.


=== Retrospective Series ===
An additional three episode retrospective series began showing in late 2019. These are:

Episode 1 is called "Escape to the Chateau : Restoring the Dream" (26 November 2019).
Episode 2 is called "Escape to the Chateau : Designing the Dream" (2 December 2019).
Episode 3 is called "Escape to the Chateau : The Great Outdoors" (9 December 2019).These covered events during the first five years.


== Background and production ==
Dick Strawbridge invited Channel 4 to join them documenting their journey in searching for a house. The couple had already spent four years searching for a home to bring up their two young children, Arthur and Dorothy. In contrast to many other home shows, the television coverage was never planned, but Channel 4 paid to document the journey which lifted some of the weight that such a large task would cost. This was in the form of the presenter's fee, in addition to the exposure from the show's advertising, which has allowed the business to uphold its popularity as a wedding and events venue.The appeal of the show may be due to the affordability on face value and lack of demand for such French properties, which attracts many English house buyers. That was the same story with Dick and Angel, since when they first started house hunting in France, they were looking at more modest properties such as small farmhouses, until they realised that at such low prices, they could buy a castle. For £280,000, they purchased a chateau that lists forty-five rooms, a walled garden and surrounded by a moat on a twelve-acre property, ten times larger than the average British house. However, the show makes viewers aware of the large restoration and running costs of such projects, which can become costly for people trying to make a business out of these castles.With policies on rehabilitating older houses introduced in the 1970s, the National Housing Agency (ANAH) was created to promote the improvement of older private dwellings, which can either improve or hinder the process. Another way to gather funds is through shareholdings schemes. This was seen in Escape to the Chateau DIY with the couple renovating Chateau de Jalesnes, who used wealthy French nationals as shareholders. Another example was through a GoFundMe page that saved the 13th-century Château de la Mothe-Chandeniers by receiving €500,000, which now has thousands of shareholders invested in its restoration.


=== History of Château de la Motte-Husson ===

From the twelfth to fourteenth centuries, the site of the chateau was used as a fortified stronghold in the parish of La Motte. In 1406, the Husson family, seigneurs of Montgiroux, named the castle Château de la Motte-Husson.
The Baglion de la Dufferie family (a French branch of the Baglioni family of Perugia) acquired the estate in 1600. After that, it was rebuilt within the square moat during the period from 1868 to 1874. The current façade is reflective of the efforts of Countess Louise-Dorothée de Baglion de la Dufferie (1826–1902), who told her husband that she wanted a grand residence. The chateau served as a summer home for the family.


== Other series ==


=== Escape to the Chateau DIY ===
Escape to the Chateau DIY follows Dick and Angel giving their tips to other British immigrants in France trying to run a business from chateaux in a similar condition to their own. It follows their renovations and possibilities for generating profit. The show has four series. The initial broadcast was on 9 April 2018, and the third series concluded on 15 November 2019. The fourth series commenced broadcasting on 20 March 2020.


== References ==


== External links ==
Escape to the Chateau at Channel 4
Escape to the Chateau on IMDb
Escape to the Chateau DIY – châteaux contact list