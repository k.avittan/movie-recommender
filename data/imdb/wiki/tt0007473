The Trail of the Lonesome Pine is a 1936 American romance film based on the novel of the same name. The picture was directed by Henry Hathaway starring Fred MacMurray, Sylvia Sidney, and Henry Fonda. It was the second full-length feature film to be shot in three-strip Technicolor and the first in color to be shot outdoors, with the approval of the Technicolor Corporation. Much of it was shot at Big Bear Lake in southern California. The Trail of the Lonesome Pine was the fourth feature film adaptation of John Fox Jr.'s 1908 novel, including 1916 and 1923 silent versions.  As with the novel, the film makes extensive use of Appalachian English in the dialogue.


== Plot ==
Deep in the region of the Eastern Kentucky Coalfield, a feud between the Kentucky clans of the Tollivers and the Falins has been ongoing for as long as anyone can recall. After an engineer, Jack Hale, arrives with coal and railroad interests, he saves the life of Dave Tolliver, whose injury has developed gangrene.
Dave expects to marry a cousin, June, but she takes an immediate shine to the newcomer. Her younger brother Buddie is also impressed with Hale, who begins to educate him and take the boy under his wing. But others from both families do not give this outsider their trust.
Upset over the budding romance, Dave sets out after Hale with a rifle but is ambushed by the Falins. The latest round of violence causes June not to want to return home, so Hale sends her to Louisville to live with his sister.
A bridge is destroyed by the Falins, causing the accidental death of Buddie. A funeral is held and June returns, newly sophisticated from being in the big city. Family patriarch Buck Falin extends his apologies about her brother. Dave, however, is shot in the back by Wade Falin.
The families agree that the feud has gone too far. Hale is befriended by all, and will happily marry June.


== Cast ==
Fred MacMurray as Jack Hale
Sylvia Sidney as June Tolliver
Henry Fonda as Dave Tolliver
Fred Stone as Judd Tolliver
Nigel Bruce as Thurber
Beulah Bondi as Melissa
Robert Barrat as Buck Falin
George "Spanky" McFarland as Buddie Tolliver
Fuzzy Knight as Tater
Otto Fries as Corsey
Samuel S. Hinds as Sheriff
Alan Baxter as Clay Tolliver
Margaret Armstrong as Tolliver family member
Ricca Allen as Tolliver family member
Fern Emmett as Lena Tolliver


== Production ==
With principal on-location photography beginning in mid-October 1935 in Chatsworth, at Big Bear Lake (in the San Bernardino Mountains), and at the Santa Susana Pass in California, recreating the rural and mountain locale of the novel. The film was the first feature-length film to be shot in three-strip Technicolor on location.In an interview with James Bawden in 1976, Fonda remembered carving “HF LUVS SS” on a tree during the production of this film. Director Henry Hathaway found it when shooting Woman Obsessed there in 1959.Considered a technological success, The Trail of the Lonesome Pine was not the first film to utilize the new color process but integrated its use successfully, and was a harbinger of future developments. "The significance of this achievement is not to be minimized. It means that color need not shackle the cinema, but may give it fuller expression. It means that we can doubt no longer the inevitability of the color film or scoff at those who believe that black-and-white photography is tottering on the brink of that limbo of forgotten things which already has swallowed the silent picture."


== Reception ==
The Trail of the Lonesome Pine received positive critical acclaim, with Frank Nugent of The New York Times considering the film as significant yet not without flaws. "Paramount's new film is far from perfect, either as a photoplay or as an instrument for the use of the new three-component Technicolor process", although "a cast of unusual merit and a richly beautiful color production" were its redeeming qualities. Writing for The Spectator in 1936, Graham Greene gave the film a neutral review. While criticizing the use of Technicolor here as "just bad bright picture-postcard stuff", Greene praised the story as "quite a good one" and singled out Sylvia Sidney for her charming performance.The film made a profit of $522,620.


=== Awards and honors ===
Two original songs from the film, both written by composer Louis Alter and lyricist Sidney D. Mitchell and sung by Fuzzy Knight, gained national prominence. "A Melody from the Sky" was nominated for the 1937 Academy Award in the category of "Best Music, Original Song". The other song, "Twilight on the Trail", became a popular hit and eventually something of a classic. It inspired a 1941 cowboy film of the same name and has been recorded by numerous country, pop, rock and soul singers.
Trail of the Lonesome Pine was recognized at the 1936 Venice Film Festival for a "Special Recommendation" for the use of color film.


== References ==


=== Notes ===


=== Bibliography ===


== External links ==
The Trail of the Lonesome Pine at the TCM Movie Database
The Trail of the Lonesome Pine (1936) on IMDb
The Trail of the Lonesome Pine at Virtual History