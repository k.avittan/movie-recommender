"Jack and the Beanstalk" is an English fairy tale. It appeared as "The Story of Jack Spriggins and the Enchanted Bean" in 1734 and as Benjamin Tabart's moralized "The History of Jack and the Bean-Stalk" in 1807. Henry Cole, publishing under pen name Felix Summerly, popularized the tale in The Home Treasury (1845), and Joseph Jacobs rewrote it in English Fairy Tales (1890). Jacobs' version is most commonly reprinted today, and is believed to be closer to the oral versions than Tabart's because it lacks the moralizing."Jack and the Beanstalk" is the best known of the "Jack tales", a series of stories featuring the archetypal Cornish and English hero and stock character Jack.According to researchers at Durham University and Universidade Nova de Lisboa, the story originated more than five millennia ago, based on a wide-spread archaic story form which is now classified by folklorists as ATU 328 The Boy Who Stole Ogre's Treasure.


== Story ==
A fake work - Jack, a poor country boy, trades the family cow for a handful of magic beans, which grow into an enormous beanstalk reaching up into the clouds. Jack climbs the beanstalk and finds himself in the castle of an unfriendly giant. The giant senses Jack's presence and cries, "Fee, fie, fo, fum, I smell the blood of an Englishman! Be he alive, or be he dead, I'll grind his bones to make my bread!" Outwitting the giant, Jack is able to retrieve many goods once stolen from his family, including an enchanted goose that lays golden eggs. Jack then escapes by chopping down the beanstalk. The giant, who is pursuing him, falls to his death, and Jack and his family prosper.


== Origins ==

"The Story of Jack Spriggins and the Enchanted Bean" was published in the 1734 second edition of Round About Our Coal-Fire. In 1807, Benjamin Tabart published The History of Jack and the Bean Stalk, but the story is certainly older than these accounts.According to researchers at Durham University and the Universidade Nova de Lisboa, the tale type (AT 328, The Boy Steals Ogre's Treasure) to which the Jack story belongs  may have had a Proto-Indo-European language (PIE) origin (the same tale also has Proto-Indo-Iranian variants),  and so some think that the story would have originated millennia ago. (4500 BC to 2500 BC)In some versions of the tale, the giant is unnamed, but many plays based on it name him Blunderbore (one giant of that name appears in the 18th-century tale "Jack the Giant Killer"). In "The Story of Jack Spriggins" the giant is named Gogmagog.
The giant's catchphrase "Fee-fi-fo-fum! I smell the blood of an Englishman" appears in William Shakespeare's King Lear (c 1606) in the form "Fie, foh, and fum, I smell the blood of a British man." (Act 3, Scene 4), and something similar also appears in "Jack the Giant Killer".


== Analogies ==
"Jack and the Beanstalk" is an Aarne-Thompson tale-type 328, The Treasures of the Giant, which includes the Italian "Thirteenth" and the French "How the Dragon was Tricked" tales. Christine Goldberg argues that the Aarne-Thompson system is inadequate for the tale because the others do not include the beanstalk, which has analogies in other types (a possible reference to the genre anomaly).The Brothers Grimm drew an analogy between this tale and a German fairy tale, "The Devil With the Three Golden Hairs". The devil's mother or grandmother acts much like the giant's wife, a female figure protecting the child from the evil male figure."Jack and the Beanstalk" is unusual in some versions in that the hero, although grown up, does not marry at the end but returns to his mother. In other versions he is said to have married a princess. This is found in few other tales, such as some variants of "Vasilisa the Beautiful".


== Moral perspectives ==

The original story portrays a "hero" gaining the sympathy of a man's wife, hiding in his house, robbing him, and finally killing him. In Tabart's moralized version, a fairy woman explains to Jack that the giant had robbed and murdered his father justifying Jack's actions as retribution (Andrew Lang follows this version in the Red Fairy Book of 1890).
Jacobs gave no justification because there was none in the version he had heard as a child and maintained that children know that robbery and murder are wrong without being told in a fairy tale, but did give a subtle retributive tone to it by making reference to the giant's previous meals of stolen oxen and young children.Many modern interpretations have followed Tabart and made the giant a villain, terrorizing smaller folk and stealing from them, so that Jack becomes a legitimate protagonist. For example, the 1952 film starring Abbott and Costello the giant is blamed for poverty at the foot of the beanstalk, as he has been stealing food and wealth and the hen that lays golden eggs originally belonged to Jack's family. In other versions, it is implied that the giant had stolen both the hen and the harp from Jack's father.  Brian Henson's 2001 TV miniseries Jack and the Beanstalk: The Real Story not only abandons Tabart's additions but vilifies Jack, reflecting Jim Henson's disgust at Jack's unscrupulous actions.


== Adaptations ==


=== Film and TV ===


==== Live-action theatrical films ====
The first film adaptation was made in 1902 by Edwin S. Porter for the Edison Manufacturing Company.
The 1952 Abbott and Costello adaptation was not the only time a comedy team was involved with the story. The Three Stooges had their own five-minute animated retelling, titled Jack and the Beanstalk (1965).
Michael Davis directed the 1994 adaptation, titled Beanstalk, starring J. D. Daniels as Jack and Stuart Pankin as the giant. The film was released by Moonbeam Entertainment, the children's video division of Full Moon Entertainment.
Avalon Family Entertainment's 2009 Jack and the Beanstalk is a low-budget live-action adaptation starring Christopher Lloyd, Chevy Chase, James Earl Jones, Gilbert Gottfried, Katey Sagal, Wallace Shawn and Chloë Grace Moretz. Jack is played by Colin Ford.
The Warner Bros. film directed by Bryan Singer and starring Nicholas Hoult as Jack is titled Jack the Giant Slayer and was released in March 2013. In this tale, which is amalgamated with Jack the Giant Killer, Jack climbs the beanstalk to save a princess.
The story appears in the 2017 commercial for the British breakfast cereal Weetabix, where the giant is scared off by an English boy who has had a bowl of Weetabix.
In the 2014 film Into the Woods, and the musical of the same name, one of the main characters, Jack (Daniel Huttlestone) climbs a beanstalk, much like in the original version. He acquires a golden harp, a hen that lays golden eggs, and several gold pieces. The story goes on as it does in the original fairy tale, but continues afterwards showing what happens after you get your happy ending. In this adaptation, the giant's vengeful wife (Frances de la Tour) attacks the kingdom to find and kill Jack as revenge for him murdering her husband, where some characters were killed during her rampage. The giant's wife is eventually killed by the surviving characters in the story.


==== Live-action television films and series ====
Gilligan's Island did in 1965 an adaptation/dream sequence in the second-season episode "'V' for Vitamins" in which Gilligan tries to take oranges from a giant Skipper and fails. The part of the little Gilligan chased by the giant was played by Bob Denver's 7-year-old son Patrick Denver.
In 1973 the story was adapted, as The Goodies and the Beanstalk, by the BBC television series The Goodies.
In the Season 3 premiere 1995 episode of Barney and Friends titled "Shawn and the Beanstalk", Barney the Dinosaur and the gang tell their version of Jack and the Beanstalk, which was all told in rhyme.
Beanstalks and Bad Eggs an 1997, episode of Hercules: The Legendary Journeys episode
A Season 2 1999 episode of The Hughleys titled "Two Jacks & a Beanstalk" shows a retelling of the story where Jack Jr. (Michael, Dee Jay Daniels) buys magical beans as a means of gaining wealth and giving his family happiness and health. He & Jack Sr. (Darryl, D.L. Hughley) climb the beanstalk to see what prosperity awaits them.
The Jim Henson Company did a TV miniseries adaptation of the story as Jim Henson's Jack and the Beanstalk: The Real Story in 2001 (directed by Brian Henson) which reveals that Jack's theft from the giant was completely unmotivated, with the giant Thunderdell (played by Bill Barretta) being a friendly, welcoming individual, and the giant's subsequent death was caused by Jack's mother cutting the beanstalk down rather than Jack himself. The film focuses on Jack's modern-day descendant Jack Robinson (played by Matthew Modine) who learns the truth after the discovery of the giant's bones and the last of the five magic beans, Jack subsequently returning the goose and harp to the giants' kingdom.
In an episode of Tweenies (1999-2002) titled "Jake and the Beanstalk", the characters perform a pantomime based on the story with Jake as the role of Jack and Judy as the giant. The title "Jake and the Beanstalk" was also used for an episode of Jake and the Never Land Pirates.
ABC's Once Upon a Time (2011-2018) debuts their spin on the tale in the episode "Tiny" of Season Two,Tallahassee where Jack, now a female named Jacqueline (known as Jack) is played by Cassidy Freeman and the giant, named Anton, is played by Jorge Garcia. In this adaptation, Jack is portrayed as a villainous character. In Season Seven, a new iteration of Jack (portrayed by Nathan Parsons) is a recurring character and Henry Mills' first friend in the New Enchanted Forest. It was mentioned that he and Henry fought some giants. He debuts in "The Eighth Witch". In Hyperion Heights, he is cursed as Nick Branson and is a lawyer and Lucy's fake father. Later episodes revealed that his real name is Hansel, who is hunting witches.


==== Animated films ====
Jack and the Beanstalk - is a 1931 Fleischer Studios Talkartoon animated short film starring Bimbo and Betty Boop.
Jack and the Beanstalk - ComicColor cartoons directed by Ub Iwerks
Giantland is a 1933 animated short film produced by Walt Disney Productions and distributed by United Artists. The short is the first is an adaptation of the fairy tale by Disney with Mickey Mouse in the title role.  It was the 62nd Mickey Mouse short film, and the twelfth of that year.In 1947 Mickey and the Beanstalk was released  as part of Fun and Fancy Free. This the second adaptation of the story by Disney and put Mickey Mouse in the role of Jack, accompanied by Donald Duck and Goofy. The giant's named "Willie" in this version). This version of the fairy tale was narrated  by Edgar Bergen in the original feature, this segment was later re-released as part of Walt Disney anthology television series and narrated first by Sterling Holloway and then by Ludwig Von Drake.
Walt Disney Animation Studios wanted to do another adaptation of the fairy tale called Gigantic. Tangled director Nathan Greno was to direct and it was set to be released in late 2020, however, it was reported on October 10, 2017, that the studio had made the decision to cancel the film after struggling creatively with it.
Warner Bros. adapted the story into three Merrie Melodies cartoons.
Friz Freleng directed Jack-Wabbit and the Beanstalk (1943),
Chuck Jones directed Beanstalk Bunny (1955) where Elmer Fudd is the giant,
Freleng directed Tweety and the Beanstalk (1957).
The famous cartoon series The Pink Panther also features a mention of this plot, in "Cat and the Pinkstalk". (1978)
In the animated movie Puss in Boots, the classic theme appears again. The magic beans play a central role in that movie, culminating in the scene, in which Puss, Kitty and Humpty ride a magic beanstalk to find the giant's castle.
Warner Bros. Animation's direct-to-DVD film Tom and Jerry's Giant Adventure is based on the fairy tale.


==== Foreign animated films ====
Gisaburo Sugii directed a feature-length anime telling of the story released in 1974, titled Jack to Mame no Ki. The film, a musical, was produced by Group TAC and released by Nippon Herald. The writers introduced a few new characters, including Jack's comic-relief dog, Crosby, and Margaret, a beautiful princess engaged to be married to the giant (named "Tulip" in this version) due to a spell being cast over her by the giant's mother (an evil witch called Madame Hecuba). Jack, however, develops a crush on Margaret, and one of his aims in returning to the magic kingdom is to rescue her. The film was dubbed into English, with legendary voice talent Billie Lou Watt voicing Jack, and received a very limited run in U.S. theaters in 1976. It was later released on VHS (now out of print) and aired several times on HBO in the 1980s. However, it is now available on DVD with both English and Japanese dialogue.


==== Animated television series and films ====
In 1967, Hanna-Barbera produced a live action version of Jack and the Beanstalk, with Gene Kelly as Jeremy the Peddler (who trades his magic beans for Jack's cow), Bobby Riha as Jack, Dick Beals as Jack's singing voice, Ted Cassidy as the voice of the animated giant, Janet Waldo as the voice of the animated Princess Serena, Marni Nixon as Serena's singing voice, and Marian McKnight as Jack's mother. The songs were written by Sammy Cahn and Jimmy Van Heusen. Kelly also directed the Emmy Award-winning film.
An 1978 episode of Challenge of the Super Friends titled "Fairy Tale of Doom" has the Legion of Doom using the Toyman's newest invention, a projector-like device to trap the Super Friends inside pages of children's fairy tales. The Toyman traps Hawkman in this story.
An 1989 episode of The Super Mario Bros. Super Show!, entitled "Mario and the Beanstalk", does a retelling with Bowser as the giant (there is no explanation as to how he becomes a giant).
In Season 1 of Animaniacs (1993), an episode featured a parody of Jack and the Beanstalk titled "The Warners and the Beanstalk". All three Warners (Yakko, Wakko and Dot) take on Jack's role, while the giant is based on Ralph the Guard.
Wolves, Witches and Giants Episode 9 of Season 1, Jack and the Beanstalk, broadcast on 19 October 1995, has Jack's mother chop down the beanstalk and the giant plummet through the earth to Australia. The hen that Jack has stolen fails to lay any eggs and ends up "in the pot by Sunday", leaving Jack and his mother to live in reduced circumstances for the rest of their lives.
Jack and Beanstalk were featured in Happily Ever After: Fairy Tales for Every Child (1995-2000) where Jack is voiced by Wayne Collins and the giant is voiced by Tone Loc. The story is told in an African-American style.
In The Magic School Bus 1996 episode "Gets Planted", the class put on a school production of Jack and the Beanstalk, with Phoebe starring as the beanstalk after Ms. Frizzle turned her into a bean plant.
In a Rugrats: Tales From the Crib  episode 2006 named "Three Jacks and a Beanstalk" where Angelica plays the giant.
In a Happy Tree Friends 2006 episode called "Dunce Upon a Time", there was a strong resemblance as Giggles played a Jack-like role and Lumpy played a giant-like role.
In an 2006 episode of Mickey Mouse Clubhouse called "Donald and the Beanstalk", Donald Duck accidentally swapped his pet chicken with Willie the Giant for a handful of magic beans.
The story was adapted in 2014 by Family Guy in the 10th episode of its 12th season, Grimm Job, where Peter Griffin takes his own spin on various fairy tales while reading bedtime stories to Stewie.
In the 2016 a television adaptation of Revolting Rhymes based on Rohald Dahl's modernization of the tale was released, were Jack lives next door to Cinderella and is in love with her.


=== Literature ===
The story is the basis of the similarly titled traditional British pantomime, wherein the Giant is certainly a villain, Jack's mother the Dame, and Jack the Principal Boy.
Jack of Jack and the Beanstalk is the protagonist of the comic book Jack of Fables, a spin-off of Fables, which also features other elements from the story, such as giant beanstalks and giants living in the clouds. The Cloud Kingdoms first appear in issue #50 and is shown to exist in their own inter-dimensional way, being a world of their own but at the same time existing over all of the other worlds.
Roald Dahl rewrote the story in a more modern and gruesome way in his book Revolting Rhymes (1982), where Jack initially refuses to climb the beanstalk and his mother is thus eaten when she ascends to pick the golden leaves at the top, with Jack recovering the leaves himself after having a thorough wash so that the giant cannot smell him. The story of Jack and the Beanstalk is also referenced in Dahl's The BFG, in which the evil giants are all afraid of the "giant-killer" Jack, who is said to kill giants with his fearsome beanstalk (although none of the giants appear to know how Jack uses it against them, the context of a nightmare that one of the giants has about Jack suggesting that they think that he wields the beanstalk as a weapon).
James Still published Jack and the Wonder Beans (1977, republished 1996) an Appalachian variation on the Jack and the Beanstalk tale. Jack trades his old cow to a gypsy for three beans that are guaranteed to feed him for his entire life. It has been adapted as a play for performance by children.
Snips, Snails, and Dragon Tails, an Order of the Stick print book, contains an adaptation in the Sticktales section. Elan is Jack, Roy is the giant, Belkar is the golden goose, and Vaarsuvius is the wizard who sells the beans. Haley also appears as an agent sent to steal the golden goose, and Durkin as a dwarf neighbor with the comic's stereotypical fear of tall plants.
A children's book, What Jill Did While Jack Climbed the Beanstalk, was published in 2020 by Edward Zlotkowski. It takes place at the same time as Jack's adventure, but it tells the story of what his sister encounters when she ventures out to help the family and neighbors.
In the One Piece Skypiea Arc, there is a huge twisted beanstalk that connects Upper Yard and God's Shrine, which is called "Giant Jack".


=== Video games ===
An arcade video game, Jack the Giantkiller, was released by Cinematronics in 1982 and is based on the story. Players control Jack, and must retrieve a series of treasures – a harp, a sack of gold coins, a golden goose and a princess – and eventually defeat the giant by chopping down the beanstalk.
Jumpin' Kid: Jack to Mame no Ki Monogatari was released 1990 in Japan for the Family Computer. A North American release was planned but ultimately scrapped. The game was known in Poland, Russia and other non-NES countries via Famiclones
Bart Simpson plays the role of the main character in a Simpsons video game: The Simpsons: Bart & the Beanstalk.
Tiny Toon Adventures: Buster and the Beanstalk   is the only Tiny Toon Adventures-related video game released for MS-DOS and various other systems. It was developed and published by Terraglyph Interactive Studios in 1996
Tiny Toon Adventures: The Great Beanstalk (also known as Tiny Toon Adventures: Buster and the Beanstalk in Europe) is the first Tiny Toon Adventures game released on the PlayStation.  It was developed by Terraglyph Interactive Studios and published by NewKidCo on October 27, 1998.
The story was adapted in 2012 by software maker Net Entertainment and made into a slot machine game.
The AWS service Elastic Beanstalk, which allows developers to provision websites, is a reference to Jack and the Beanstalk.


=== Music ===
Stephen Sondheim's 1986 musical Into the Woods  features Jack, originally portrayed by Ben Wright, along with several other fairy tale characters. In the second half of the musical, the giant's wife climbs down a second (inadvertently planted) beanstalk to exact revenge for her husband's death, furious at Jack's betrayal of her hospitality. The Giantess then causes the deaths of Jack's mother and other important characters before being finally killed by Jack.
Swedish melody hardcore band Venerea adapts the story in the song "Beanstalk" on their 1997 album Both ends burning.
Mark Knopfler sang a song, "After the Beanstalk", in his 2012 album Privateering.


== See also ==

"Jack the Giant Killer"


== References ==


== External links ==
Pantomime based on the fairytale of "Jack and the Beanstalk"
Jack and the Beanstalk Felt Story at Story Resources
"Jack and the Beanstalk" at SurLaLune Fairy Tales — Annotated version of the fairy tale.
Adult Pantomime based on the fairytale of "Jack and the Beanstalk"
Jack tales in Appalachia — including "Jack and the Bean Tree"
Children's audio story of Jack and the Beanstalk at Storynory
Kamishibai (Japanese storycard) version — in English, with downloadable Japanese translation
The Disney version of Jack and the Beanstalk at The Encyclopedia of Disney Animated Shorts
Full text of Jack And The Bean-Stalk from "The Fairy Book"
Jack et le Haricot Magique - The Rock Musical by Georges Dupuis & Philippe Manca
Jack And The Beanstalk - Animation movie in 4K at Geetanjali Audios in collaboration with Film Art Music Entertainment Productions FAME Productions