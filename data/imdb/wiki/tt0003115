A Man and a Woman (French: Un homme et une femme) is a 1966 French film written and directed by Claude Lelouch and starring Anouk Aimée and Jean-Louis Trintignant. Written by Lelouch and Pierre Uytterhoeven, the film is about a young widow and widower who meet by chance at their children's boarding school and whose budding relationship is complicated by the memories of their deceased spouses. The film is notable for its lush photography, which features frequent segues among full color, black-and-white, and sepia-toned shots, and for its memorable musical score by Francis Lai.
A Man and a Woman had a total of 4,272,000 admissions in France and was also the 6th highest-grossing film of the year. In the United States, the film earned $14,000,000. The film won several awards, including the Palme d'Or at the 1966 Cannes Film Festival, two Golden Globe Awards for Best Foreign Language Film and Best Actress - Drama (for Aimée), and two Academy Awards for Best Foreign Language Film and Best Original Screenplay. A sequel, A Man and a Woman: 20 Years Later (Un Homme et une Femme, 20 Ans Déjà) was released in 1986, followed by The Best Years of a Life, which was released in 2019.


== Plot ==
A young widow, Anne Gauthier (Anouk Aimée), is raising her daughter Françoise (Souad Amidou) alone following the death of her husband (Pierre Barouh) who worked as a stuntman and who died in a movie set accident that she witnessed. Still working as a film script supervisor, Anne divides her time between her home in Paris and Deauville in northern France where her daughter attends boarding school. A young widower, Jean-Louis (Jean-Louis Trintignant), is raising his son Antoine (Antoine Sire) alone following the death of his wife Valerie (Valerie Lagrange) who committed suicide after Jean-Louis was in a near fatal crash during the 24 Hours of Le Mans. Still working as a race car driver, Jean-Louis divides his time between Paris and Deauville where his son also attends boarding school.
One day Anne and Jean-Louis meet at the Deauville boarding school after Anne misses the last train back to Paris. Jean-Louis offers her a lift and the two become acquainted during the drive home, enjoying each other's company. When he drops her off, he asks if she would like to drive up together the following weekend, and she gives him her phone number. After a busy week at the track preparing for the next race, Jean-Louis calls and they meet early Sunday morning and drive to Deauville in the rain. Clearly attracted to each other, they enjoy a pleasant Sunday lunch with their children who get along well. Later that afternoon they go for a boat ride followed by a walk on the beach at sunset.  
Jean-Louis spends the following week preparing for and driving in the Monte Carlo Rally in southeast France. Every day, Anne closely follows news reports of the race, which takes place in poor weather conditions along the icy roads of the French Riviera. Of the 273 cars that started the race, only 42 were able to finish, including Jean Louis's white Mustang, number 145. Watching the television coverage of the conclusion of the race, Anne sends Jean-Louis a telegram that reads, "Bravo! I love you. Anne."
That night at a dinner for the drivers at the Monte Carlo Casino, Jean-Louis receives the telegram and leaves immediately. He jumps into the other Mustang (number 184) used during the race and drives through the night to Paris, telling himself that when a woman sends a telegram like that, you go to her no matter what. Along the way he imagines what their reunion will be like. At her Paris apartment, Jean-Louis learns that Anne is in Deauville, so he continues north. Jean-Louis finally arrives in Deauville and finds Anne and the two children playing on the beach. When they see each other, they run into each other's arms and embrace.
After dropping their children off at the boarding school, Jean-Louis and Anne drive into town where they rent a room and begin to make love with passionate tenderness. While they are in each other's arms, however, Jean-Louis senses that something is not right. Anne's memories of her deceased husband are still with her and she feels uncomfortable continuing. Anne says it would be best for her to take the train back to Paris alone. After dropping her off at the station, Jean-Louis drives home alone, unable to understand her feelings. On the train Anne can only think of Jean-Louis and their time together. Meanwhile, Jean-Louis drives south through the French countryside to the Paris train station, just as her train is arriving. As she leaves the train, she spots Jean-Louis and is surprised, hesitates briefly, and then walks toward him and they embrace.


== Cast ==


== Production ==


=== Story and script ===
According to director Claude Lelouch, the story originated from an experience following his disappointment trying to get a distribution deal for his film Les Grands Moments. As was his habit during troubling times, he went for a long drive and ended up on the shore at Deauville at 2:00 am. After a few hours sleep in the car, he was awakened by the sunrise and saw a woman walking on the beach with her daughter and a dog. This sparked his creativity which led to the story and script which he co-wrote with Pierre Uytterhoeven within a month.


=== Casting ===
A key casting decision for Lelouch was Jean-Louis Trintignant.

I think Jean-Louis is the actor who taught me how to direct actors. We really brought each other a lot. He changed his method of acting while working with me, and I began to truly understand what directing actors was all about, working with him. I think the relationship between a director and actor is the same relationship as in a love story between two people. One cannot direct an actor if you do not love him or her. And he cannot be good if he or she does not love you in turn.
For the female lead, Trintignant asked Lelouch who his ideal woman would be, and Lelouch indicated Anouk Aimée, who had appeared in Fellini's La Dolce Vita (1960) and 8½ (1962). Trintignant happened to be a close friend of hers and told him to call her. When he did, she accepted without reading the script. Although early disagreements and the low-budget skeleton crew caused initial tension between the director and actress, they quickly resolved their differences and the two went on to become close friends.


=== Filming ===
Once the script was drafted, the film was made relatively quickly, with one month of preproduction work, three weeks of principal photography, and three weeks editing. Due to budget constraints, he used an older handheld camera that was not soundproof, so blankets were frequently employed to dampen the camera noise.Lelouch is considered a pioneer in mixing different film stocks: black-and-white with color, and 35mm with 16mm and super 8. For years film critics debated the symbolism of the mixed film stocks, but Lelouch acknowledged that the primary reason was that he was running out of money, and black and white stock was cheaper. His original plan involved shooting strictly in black and white, but when an American distributor offered him $40,000 to film in color, he filmed the outdoor sequences in color, and the indoor scenes in black and white.The music soundtrack was recorded prior to filming, and Lelouch would play the music on the set to inspire the actors. Lelouch encouraged his actors to improvise some of the dialogue, and several key scenes were improvised. The climactic scene at a train station was not scripted at the time of shooting, and Aimée did not know that director Lelouch had decided on the two main characters reuniting at the end. The look of surprise on Aimée's face is genuine.The film was shot, among others, in Paris, Monte Carlo and Deauville. The love scene was shot in the Hotel Barrière Le Normandy Deauville which, in memory of the film, has a suite entitled "A Man and a Woman". (Info by the Dizionario del Turismo Cinematografico)


== Reception ==


=== Box office ===
The film earned theatrical rentals of $4.6 million in the United States and Canada and $4 million internationally during its initial theatrical release. It played for 83 weeks in Boston, 73 weeks in Syracuse and 61 weeks in Seattle. An English language version of the film was shown in the United States and Canada from July 1, 1968 and the film eventually earned rentals of $6.3 million in the US and Canada.It was the sixth most popular film at the French box office in 1966, after La Grande Vadrouille, Dr Zhivago, Is Paris Burning?, A Fistful of Dollars and Lost Command.


=== Critical response ===
Upon its theatrical release in the United States, A Man and a Woman received mostly positive reviews. In his review in The New York Times, Bosley Crowther wrote, "For a first-rate demonstration of the artfulness of a cameraman and the skill at putting together handsome pictures and a strongly sentimental musical score, there is nothing around any better than Claude Lelouch's A Man and a Woman." Crowther lauded the "beautiful and sometimes breath-taking exposition of visual imagery intended to excite the emotions" and praised the director for his ability to create something unique from the commonplace:

Mr. Lelouch, who was his own script writer as well as director and cameraman, has a rare skill at photographing clichés so that they sparkle and glow with poetry and at generating a sense of inspiration in behavior that is wholly trivial.
The review in Variety noted the performances of the lead actors: "Anouk Aimee has a mature beauty and an ability to project an inner quality that helps stave off the obvious banality of her character, and this goes too for the perceptive Jean-Louis Trintignant as the man."On the review aggregator web site Rotten Tomatoes, the film holds a 75% positive rating from top film critics based on 14 reviews, and an 87% positive audience rating based on 5,480 reviews.The film was selected for screening as part of the Cannes Classics section at the 2016 Cannes Film Festival.


=== Awards and nominations ===
1966 Cannes Film Festival Palme d'Or (Claude Lelouch) –  Won
1967 Academy Award for Best Foreign Language Film –  Won
1967 Academy Award for Best Writing (Claude Lelouch, Pierre Uytterhoeven) –  Won
1967 Academy Award Nomination for Best Actress in a Leading Role (Anouk Aimée)
1967 Academy Award Nomination for Best Director (Claude Lelouch)
1967 Blue Ribbon Award for Best Foreign Language Film (Claude Lelouch) –  Won
1967 Cinema Writers Circle Award for Best Foreign Film –  Won
1967 Directors Guild of America Award for Outstanding Directorial Achievement (Claude Lelouch) – 
1967 Golden Globe Award for Best Foreign-Language Foreign Film –  Won
1967 Golden Globe Award for Best Motion Picture Actress (Anouk Aimée) –  Won
1967 Golden Globe Award Nomination for Best Motion Picture Director (Claude Lelouch)
1967 Golden Globe Award Nomination for Best Original Score (Francis Lai)
1967 Golden Globe Award Nomination for Best Original Song in a Motion Picture (Francis Lai, Pierre Barouh)
1967 Italian National Syndicate of Film Journalists Silver Ribbon for Best Director, Foreign Film (Claude Lelouch) –  Won
1967 Laurel Award Nomination for Female Dramatic Performance (Anouk Aimée)
1967 National Board of Review of Motion Pictures Award for Top Foreign Film –  Won
1968 BAFTA Award for Best Foreign Actress (Anouk Aimée) –  Won
1968 BAFTA Award Nomination for Best Film (Claude Lelouch)
1968 Mexican Cinema Journalists Silver Goddess Award for Best Foreign Actress (Anouk Aimée) –  Won


== Soundtrack ==
The soundtrack was written by Francis Lai and earned "Best Original Score" nominations at both the BAFTA Awards and Golden Globe Awards in 1967.  The film's theme song, with music by Francis Lai and lyrics by Pierre Barouh, was also nominated for "Best Original Song in a Motion Picture" at the Golden Globe Awards. In Finland it has become one of the most easily recognizable TV advertisement themes, having been used for decades by the cruise ferry brand Silja Line.
Pierre Barouh, who plays the deceased husband in the film, also sings the songs in the soundtrack.  In a sequence of the film, he makes a brief reappearance singing "Samba Saravah", a French version with lyrics by Barouh himself of the Brazilian song "Samba da Benção" written by Baden Powell with original lyrics by Vinicius de Moraes.The song "Aujourd'hui C'est Toi" is used as the theme for the BBC's Panorama current affairs program, plus Rede Globo's Jornal Hoje midday newscast, and YLE's Ajankohtainen kakkonen weekly current affairs television program in Finland on TV2 from 1969 to 2015.  Harry James recorded a version of the film's theme song on his album For Listening And Dancing, released in 1981 on Reader's Digest RD4A 213.


=== Track listing ===
Un homme et une femme (A Man and a Woman) performed by Nicole Croisille and Pierre Barouh (2:40)
Samba Saravah by Pierre Barouh (4:30)
Aujourd'hui c'est toi (Today It's You) by Nicole Croisille (2:06)
Un homme et une femme (A Man and a Woman) (2:37)
Plus fort que nous (Stronger Than Us) (3:15)
Aujourd'hui c'est toi (Today It's You) (2:35)
A l'ombre de nous (In Our Shadow) by Pierre Barouh (4:55)
Plus fort que nous (Stronger Than Us) by Nicole Croisille and Pierre Barouh (3:43)
A 200 a l'heure (124 Miles An Hour) (2:30)


== See also ==
List of submissions to the 39th Academy Awards for Best Foreign Language Film
List of French submissions for the Academy Award for Best Foreign Language Film


== References ==


== External links ==
A Man and a Woman on IMDb
A Man and a Woman at AllMovie
A Man and a Woman at Rotten Tomatoes