DC One Million is a comic book crossover storyline that ran through a self-titled, weekly miniseries and through special issues of almost all of the "DCU" titles published by American company DC Comics in November 1998. It featured a vision of the DC Universe in the 853rd century (85,201–85,300 AD), chosen because that is the century in which DC Comics would have published issue #1,000,000 of their comics if they had maintained a regular publishing schedule. The miniseries was written by Grant Morrison and drawn by Val Semeiks.


== Setup ==
The core of the event was a four-issue miniseries, in which the 20th-century Justice League of America and the 853rd-century Justice Legion Alpha cooperate to defeat a plot by the supervillain Vandal Savage (who, being practically immortal, exists in both centuries as well as all the ones in between) and future Superman nemesis Solaris the Living Sun. Thirty-four other series then being published by DC also put out a single issue numbered #1,000,000, which either showed its characters' involvement in the central plot or gave a glimpse of what its characters' descendants/successors would be doing in the 853rd century. Hitman #1,000,000 was essentially a parody of the entire storyline. A trade paperback collection was subsequently published, consisting of the four-issue mini-series, and tie-in issues necessary to follow the main plot. The series was then followed by a one-shot titled DC One Million 80-Page Giant #1,000,000 (1999), which was a collection of further adventures in the life of the future heroes.


== Plot ==
In the 853rd century, the original Superman ("Superman-Prime One Million") still lives, but has spent over fifteen thousand years in a self-imposed exile in his Fortress of Solitude in the heart of the Sun in order to keep it alive, during which everyone he knew and loved died. One of his descendants is "Kal Kent", the Superman of the 853rd century.
The galaxy is protected by the Justice Legions, which were inspired by the 20th-century Justice League and the 31st-century Legion of Super-Heroes, among others. Justice Legion Alpha, which protects the solar system, includes Kal Kent and future analogues of Wonder Woman, Hourman, Starman, Aquaman, the Flash and Batman. Advanced terraforming processes have made all the Solar System's planets habitable, with the ones most distant from the Sun being warmed by Solaris, a "star computer" which was once a villain but was reprogrammed by one of Superman's descendants.
Superman-Prime announces that he will soon return to humanity and, to celebrate, Justice Legion Alpha travels back in time to the late 20th century to meet Superman's original teammates in the JLA, and bring them and Superman to the future to participate in games and displays of power as part of the celebration.
Meanwhile, in Russia, Vandal Savage single-handedly defeats the Titans (Arsenal, Tempest, Jesse Quick and Supergirl) when they attempt to stop him purchasing nuclear-powered Rocket Red suits. He then launches four Rocket Red suits (with a Titan trapped inside each of the four) in a nuclear strike on Washington D.C., Metropolis, Brussels and Singapore.
One member of the Justice Legion Alpha (the future Starman) has been bribed into betraying his teammates by Solaris, who has returned to its old habits. Before the original heroes can be returned to their own time the future Hourman, android, collapses and releases a virus programmed by Solaris to attack machines and humans.
The virus affects the guidance systems of the Rocket Red suits and causes one of them to instead detonate over Montevideo, killing over one million people. Tempest (the Titan inside) had escaped long before the suit exploded by using the ice that formed on the suit at high altitude, although he subsequently blacked out and fell into the sea. The virus also drives humans insane, causing an increase in anger and paranoia worldwide. Believing that this was deliberately planned by the JLA to stop him, Savage launches an all-out war on superhumans using "blitz engines" he had created and hidden while allied with Hitler during World War II. The paranoia caused by the virus also leads the Justice Legion Alpha and the contemporary heroes to attack each other, although the Justice Legion Alpha manage to coordinate themselves enough to stop the other Rocket Red suits from hitting their targets.
The remnants of the JLA that stayed in the present and the Justice Legion Alpha overcome their paranoia when the future Superman and Steel realize the significance of the symbol they both wear; as Huntress had pointed out to Steel earlier, wearing the 'S' means that he has to make the hard choices. The two JLAs are eventually able to stop the virus when it is discovered that it is a complex computer program looking for appropriate hardware. To provide this hardware, the heroes are forced to build the body of Solaris (including in it a DNA sample of Superman's wife Lois Lane) and the virus flees from the Earth to this body, bringing Solaris to life. In a final act of repentance, the future Starman sacrifices himself to banish Solaris from the solar system. The future Superman forces himself through time using confiscated time travel technology he finds in the Watchtower, almost dying in the process due to the drain on his powers.
Meanwhile, in the 853rd century, the original JLA are fighting an alliance between Solaris and Vandal Savage. Savage has found a sample of kryptonite on Mars (where it was left by the future Starman back in the 20th century), which he gives to Solaris. Savage has also hired Walker Gabriel to steal the time travel gauntlets of the 853rd century Flash (John Fox) to ensure the Justice Legion Alpha remains trapped in the past. However, he ultimately double-crosses Gabriel.
Solaris, in a final attack, slaughters thousands of superhumans so that it can fire the kryptonite into the sun and kill Superman-Prime before he emerges. The JLA's Green Lantern — a hero who uses a power that Solaris has never encountered before — causes Solaris to go supernova and he and the 853rd century Superman contain the resulting blast — but not before the kryptonite is released.
The future Vandal Savage teleports from Mars to Earth using the stolen Time-Gauntlets. It turns out, however, that Walker Gabriel and Mitch Shelley, the Resurrection Man (an immortal who had become Savage's greatest foe through the millennia), had sabotaged the Gauntlets so that Savage, instead of travelling only in space, also travels through time, arriving in Montevideo moments before the nuclear blast he caused centuries earlier, finally bringing his life to an end.
It is then revealed that a secret conspiracy—forewarned by the trouble in the 20th century, mainly in that Huntress, inspired by the time capsules which students in her class were currently making, realized they had centuries to foil the plot—has spent the intervening centuries coming up with a foolproof plan for stopping Solaris. Their actions included replacing the hidden kryptonite with a disguised Green Lantern power ring, with which the original Superman emerges from the Sun and finishes off Solaris.
In the aftermath, the original Superman and the future Hourman use the DNA sample to recreate Lois Lane, complete with superpowers. Superman then also recreates Krypton, along with all its deceased inhabitants, in Earth's solar system, and live happily ever after with Lois.


== Crossovers ==
Alongside the main DC One Million miniseries and the accompanying 80-Page Giant issue, the following ongoing DC Comics books also partook in the event:

Action Comics
Adventures of Superman
Aquaman
Azrael
Batman
Batman: Shadow of the Bat
Booster Gold
Catwoman
Chase
Chronos
Creeper
Detective Comics
Flash
Green Arrow
Green Lantern
Hitman
Impulse
JLA
Legion of Super-Heroes
Legionnaires
Lobo
Martian Manhunter
Nightwing
Power of Shazam
Resurrection Man
Robin
Starman
Superboy
Supergirl
Superman (vol. 2)
Superman: The Man of Steel
Superman: The Man of Tomorrow
Wonder Woman
Young Heroes in Love
Young Justice


== The Justice Legions ==
There are twenty-four Justice Legions, each based on 20th- and 30th-century superhero teams. Those featured include:

Justice Legion A is based on the Justice League. See main article Justice Legion Alpha.
Justice Legion B is based on the Titans. Members include Nightwing (a batlike humanoid), Aqualad (a humanoid made from water), Troy (a younger version of the 853rd century Wonder Woman), Arsenal (a robot) and Joto (killed in teleporter accident).
Justice Legion L is based on the Legion of Super-Heroes and protects an artificially created planetary system (all that remains of the 30th Century United Planets). Members include Cosmicbot (a cyborg based on magnetism, modelled on Cosmic Boy), Titangirl (the combined psychic energy of all Titanians, based on Saturn Girl), Implicate Girl (who contains the abilities of all three trillion Carggites in her "third eye", very loosely based on Triplicate Girl), Brainiac 417 (a disembodied intelligence, based on Brainiac 5 and Apparition), the M'onelves (who combine the powers of M'onel and Shrinking Violet) and barely humanoid versions of Umbra and Chameleon.
Justice Legion S consists of numerous Superboy clones, all with different powers. Members include Superboy 820 (with aquatic powers), Superboy 3541 (who can increase his size) and Superboy One Million (who can channel any of their powers through "the Eye"). They all (most notably One Million) resemble OMAC as much as Superboy. This was an intentional pun, as the title of the story was "One Million And Counting", which referred to the million clones and formed the OMAC acronym.
Justice Legion T is based on Young Justice. Members include Superboy One Million (as referred to above), Robin the Toy Wonder (optimistic robot sidekick to the 853rd century Batman) and Impulse (the living embodiment of random thoughts lost in the Speed Force).
Justice Legion Z (for Zoomorphs) is based on the Legion of Super-Pets. Members include Proty One Million and Master Mind. A version of Comet the Super-Horse is also a member.


== Other characters ==
Several other futuristic versions of DC characters appeared in the crossover, including:

Atom
Azrael
Booster Gold
Catwoman
Charade City
Gunfire
Lex Luthor
Supergirl
Captain Marvel


== Later references ==
In 2008, 10 years after the crossover, an issue of Booster Gold (vol. 2) was published as Booster Gold #1,000,000 and was announced as an official DC One Million tie-in by DC Comics. This comic introduced Peter Platinum, the Booster Gold of the 853rd century.
Grant Morrison's All-Star Superman miniseries made several references to the DC One Million miniseries. The Superman from DC One Million makes an appearance and the series ends with Superman becoming an energy being who resides in the Sun after his body has been supercharged with yellow solar energy (similar in appearance to Superman-Prime) and Solaris makes an appearance as well.
Morrison's Batman #700 also briefly shows the One Million Batman and his sidekick—Robin, the Toy Wonder—alongside a number of future iterations of Batman.
The One Million Batman, Robin the Toy Wonder and One Million Superman play a significant role in Superman/Batman #79–80, in which Epoch battles Batmen and Supermen from various time periods.
By signing into WBID account in the video game Batman: Arkham Origins, the costume of the One Million version of Batman will be unlocked for use.


== Awards ==
The original miniseries was a top vote-getter for the Comics Buyer's Guide Fan Award for Favorite Limited Series for 1999.  The storyline was a top vote-getter for the Comics Buyer's Guide Award for Favorite Story for 1999.


== Collected editions ==
DC One Million, later reprinted with the title JLA: One Million (208 pages, DC Comics, June 1999, ISBN 1-56389-525-0, Titan Books, June 1999, ISBN 1-84023-094-0, DC Comics, June 2004, ISBN 1-4012-0320-5) collects:
DC One Million (by Grant Morrison, with pencils by Val Semeiks and inks by Prentis Rollins/Jeff Albrecht/Del Barras, four-issue miniseries)
Green Lantern #1,000,000 (by Ron Marz, with pencils by Bryan Hitch and inks by Andy Lanning/Paul Neary)
Resurrection Man #1,000,000 (by Dan Abnett/Andy Lanning, with art by Jackson Guice)
Starman #1,000,000 (by James Robinson, with pencils by Peter Snejbjerg and inks by Wade Von Grawbadger)
Superman: The Man of Tomorrow #1,000,000 (by Mark Schultz, with pencils by Georges Jeanty and inks by Dennis Janke/Denis Rodier)
DC One Million Omnibus (1,080 pages, DC Comics, October 2013, ISBN 978-1-4012-4243-5) collects:DC One Million #1–4, plus the #1,000,000 issues of Action Comics, Adventures Of Superman, Aquaman, Azrael, Batman, Batman: Shadow Of The Bat, Catwoman, Chase, Chronos, The Creeper, Detective Comics, The Flash, Green Arrow, Green Lantern, Hitman, Impulse, JLA, Legion Of Super-Heroes, Legionnaires, Lobo, Martian Manhunter, Nightwing, Power Of Shazam, Resurrection Man, Robin, Starman, Superboy, Supergirl, Superman (vol. 2), Superman: The Man Of Steel, Superman: The Man Of Tomorrow, Wonder Woman and Young Justice; as well as Booster Gold #1,000,000, DC One Million 80-Page Giant #1 and Superman/Batman #79–80. (Omnibus did not include Young Heroes in Love as it was a creator-owned series.)


== Notes ==


== References ==


== External links ==
Comics Buyer's Guide Fan Awards
Sequart on DC One Million