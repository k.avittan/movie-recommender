A bellhop (North America), or hotel porter (international), is a hotel porter who helps patrons with their luggage while checking in or out. Bellhops often wear a uniform (see bell-boy hat), like certain other page boys or doormen. This occupation is also called bellman and bellboy (pronunciation ) in North America.


== Duties ==
The job's name is derived from the fact that the hotel's front desk clerk rang a bell to summon an employee, who would "hop" (jump) to attention at the desk to receive instructions. The term "porter" is used in the United Kingdom and much of the English-speaking world. "Bellboy" or "bellhop" is an American English term.
This employee traditionally was a boy or adolescent male, hence the term bellboy. Today's bellhops must be quick-witted, good with people, and outgoing. Bellhops will meet a variety of different people each day and must have the social skills to deal with them. Duties often include opening the front door, moving luggage, valeting cars, calling cabs, transporting guests, giving directions, performing basic concierge work, and responding to the guest's needs. They must be able to escort guests into their rooms while carrying luggage, or help move any baggage a customer needs.In many countries, such as the United States, it is customary to tip such an employee for their service.


== Famous bellboys ==

Brandon Flowers, the frontman, keyboardist and primary lyricist of the Las Vegas-based rock band The Killers, served as a bellhop at the Gold Coast Hotel and Casino in Las Vegas.
Ted Serios was a Chicago bellboy who gained notoriety in the 1960s by producing "thoughtographs" on Polaroid film, which he claimed were produced using psychic powers.
Karl Ernst was a Sturmabteilung Gruppenführer who, in early 1933, was the Sturmabteilung leader in Berlin. Before joining the Nazi Party he had been a hotel bellboy and a bouncer at a gay nightclub.


== Bellhops in popular culture ==
The Belgian comic strip character Spirou was originally a bellboy. Throughout many of his albums he always wore a red bellhop suit. In later stories this was reduced to him just wearing his bellhop cap.
In the 1918 comedy short The Bell Boy Roscoe Arbuckle and Buster Keaton play bell boys.
In the classic 1955 "I Love Lucy" episode "Hollywood at Last", Bobby the Bellboy, portrayed by Bob Jellison, makes his first appearance.
The Bellboy is a 1960 comedy film starring Jerry Lewis, in which he plays the titular character.
The 1962 film The Bellboy and the Playgirls also features a bellboy.
The 1973 song Bell Boy by The Who has the character Jimmy discover that someone he looked up to is now a bell boy.
The 1992 film Blame It on the Bellboy features a hapless bellboy.
In the 1995 film Four Rooms Tim Roth plays a bellhop who goes through some rough times.
The 1997 film Tower of Terror The ghost bellhop "Dewey Todd Jr." is portrayed by John Franklin.
The 2014 film The Grand Budapest Hotel The Lobby Boy "Zero Moustafa." is portrayed by Tony Revolori.
The 2020 book The House in the Cerulean Sea by T.J. Klune features a green slime-monster-child named Chauncey whose greatest ambition is to be a bellhop.


== See also ==
Porter (carrier)
Skycap


== References ==


== External links ==
 Media related to Bellhops at Wikimedia Commons