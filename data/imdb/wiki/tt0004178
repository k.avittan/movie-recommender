In the Hebrew Bible, the coat of many colors (Hebrew: כְּתֹנֶת פַּסִּים‎‎ ketonet passim) is the name for the garment that Joseph owned, which was given to him by his father Jacob.


== The problem of translation ==

According to the King James Version, Genesis 37:3 reads, "Now Israel loved Joseph more than all his children, because he was the son of his old age: and he made him a coat of many colours."

The Septuagint translation of the passage uses the word ποικίλος (poikilos), which indicates "many colored"; the Jewish Publication Society of America Version also employs the phrase "coat of many colors". On the other hand, the Revised Standard Version translates ketonet passim as "a long robe with sleeves" while the New International Version notes the translation difficulties in a footnote, and translates it as "a richly ornamented robe".Aryeh Kaplan in The Living Torah gives a range of possible explanations:

Kethoneth passim in Hebrew. It was a royal garment; 2 Samuel 13:18 (cf. Ralbag ad loc.). The word passim can be translated as 'colorful' (Radak; Septuagint), embroidered (Abraham ibn Ezra; Bahya ibn Paquda; Nachmanides on Exodus 28:2), striped (Jonah ibn Janah; Radak, Sherashim), or with pictures (Targum Jonathan). It can also denote a long garment, coming down to the palms of the hands (Rashbam; Ibn Ezra; Tosafot; Genesis Rabbah 84), and the feet (Lekach Tov). Alternatively, the word denotes the material out of which the coat was made, which was fine wool (Rashi) or silk (Ibn Janach). Hence, kethoneth passim, may be translated as "a full-sleeved robe", "a coat of many colors", "a coat reaching to his feet", "an ornamented tunic", "a silk robe", or "a fine woolen cloak".James Swanson suggests that the phrase indicates a "tunic or robe unique in design for showing special favor or relationship" and that "either the robe was very long-sleeved and extending to the feet, or a richly-ornamented tunic either of special color design or gold threading, both ornamental and not suitable for working."The phrase is used one other time in the Hebrew Scriptures, to describe the garment worn by Tamar, daughter of David, in 2 Samuel 13:18-19.


== In Genesis ==

Joseph's father Jacob (also called Israel) favored him and gave Joseph the coat as a gift; as a result, he was envied by his brothers, who saw the special coat as an indication that Joseph would assume family leadership. His brothers' suspicion grew when Joseph told them of his two dreams (Genesis 37:11) in which all the brothers bowed down to him. The narrative tells that his brothers plotted against him when he was 17, and would have killed him had not the eldest brother Reuben interposed. He persuaded them instead to throw Joseph into a pit and secretly planned to rescue him later. However, while Reuben was absent, the others planned to sell him to a company of Ishmaelite merchants. When the passing Midianites arrived, the brothers dragged Joseph up and sold him to the merchants for 20 pieces of silver. The brothers then dipped Joseph's coat in goat blood and showed it to their father, who assumed that Joseph had been torn apart by wild beasts.

The envy of his brothers may have stemmed from the fact that Joseph was the son of Rachel, Jacob's first love. However, Joseph's brothers were the sons of Rachel's older sister Leah and the sons of the handmaidens, who were given to Jacob during a time when Rachel could not conceive. There was a battle between Leah and Rachel to compete for Jacob's attention. Jacob had told Joseph, when he was seventeen years old, to go check on his brothers. Joseph would report back to his father of their evil deeds. In addition to this he shares his dreams of them bowing down to him. Their anger towards him only increased.


== In popular culture ==
The coat is featured in the musical Joseph and the Amazing Technicolor Dreamcoat.
In 1997, Anita Diamant's novel The Red Tent, Dinah mentions that Rachel is making a colorful garment for her son Joseph; she also mentions that it will make him the target of his brothers' taunting.
In the 1997 video game Castlevania: Symphony Of The Night for PlayStation, a cloak called "Joseph's cloak" can be found in the American localization of the game. It also features adjustable colors in the equip screen of the game as further reference from the Bible.
The story is referenced in the 1971 Dolly Parton song "Coat of Many Colors".


== References ==