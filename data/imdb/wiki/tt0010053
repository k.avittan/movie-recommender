Neptune's Daughter  is a 1949 Technicolor musical romantic comedy film released by Metro-Goldwyn-Mayer starring Esther Williams, Red Skelton, Ricardo Montalbán, Betty Garrett, Keenan Wynn, Xavier Cugat and Mel Blanc. It was directed by Edward Buzzell, and features the debut of the Academy Award–winning song "Baby, It's Cold Outside" by Frank Loesser.
This was the third movie that paired Williams and Ricardo Montalbán, the other two being Fiesta (1947) and On an Island with You (1948), and the second Williams film to co-star Red Skelton (after 1944's Bathing Beauty). This is one of the first films to depict television use.


== Plot summary ==
Although she initially rejects an offer by Joe Backett to become his business partner at the Neptune swimming suit design company, aquatic ballet dancer Eve Barrett changes her mind when she considers the publicity potential of the job. One day, Joe learns that a South American polo team will be playing a big match in town, and he and Eve begin planning a swimming spectacle for the event. Eve tells her man-crazy sister and roommate Betty about the South American team, and Betty immediately seizes upon the idea of finding herself a date among the players. Meanwhile, Jose O'Rourke, the handsome playboy captain of the polo team, seeks relief for his injured arm from Jack Spratt, a bumbling masseur, who complains to Jose about his lack of success with women. During the massage, Jose gives Jack advice on how to attract women, stressing the importance of speaking to women in Spanish, which he calls the "language of love." Later, while looking for the famed South American team captain, Betty accidentally mistakes Jack for Jose. Jack does not reveal his identity to Betty and accepts her invitation to visit her at her house.
On their date, Jack secretly plays a Spanish language instruction record while pretending that he is speaking romantic Spanish phrases to Betty. At the end of the evening, Betty tells Eve about her date, and Eve tries to dissuade her from pursuing a romance with any of the visiting polo players. The following day, while giving a tour of the Neptune bathing suit factory, Eve meets Jose and warns him to stay away from her sister. Jose is confused by the warning but because he is attracted to Eve, he pretends to understand and agrees to break his presumed date with Betty. When Jose asks Eve to go on the date with him, she reluctantly consents and does so only to prevent him from pursuing her sister. Despite her best attempts to make her date with Jose a failure, Eve finds him attractive and enjoys her evening. Confusion abounds the following day, when Eve's maid, Matilda, tells her that Betty has gone on another date with Jose. Furious at the news, Eve goes to Jose's apartment and demands to see her sister. She searches Jose's apartment to no avail and does not understand why Betty is not there. Later, when crooked nightclub owner Lukie Luzette learns that a man named Jose is the polo team's most valuable player, he decides to kidnap Jose and keep him out of the game to ensure that his bet against his team will pay off. Lukie sends one of his henchmen to abduct Jose, but the henchman mistakenly abducts Jack instead. Jose, meanwhile, proposes marriage to Eve, and she, having found no evidence of further wrongdoings, accepts.
However, just as Eve is about to tell Betty that she intends to marry Jose, Betty informs her that she and Jose are now engaged. When Jose shows up at Eve and Betty's house, Eve, convinced that he has deceived her, shuts the door in his face. Moments later, Jose is abducted by Lukie's men and placed in captivity. Jack, meanwhile, manages to escape from his captors just as the big polo match begins. Betty, who still believes that Jack is Jose, insists that he save his team from defeat and helps him mount a horse. While Jack inadvertently scores a victory for the South American team, the police find Jose and free him. Jose arrives at the polo field in time to accept the team's trophy and to clear up Eve's confusion. Jack admits to Betty that he is an impostor, but she forgives him and assures him of her love. All ends happily as a double wedding is planned for both couples.


== Cast ==
Esther Williams as Eve Barrett
Ricardo Montalbán as José O'Rourke
Red Skelton as Jack Spratt
Betty Garrett as Betty Barrett
Keenan Wynn as Joe Backett
Mel Blanc as Pancho
Xavier Cugat as himself
Ted de Corsia as Lukie Luzette
Mike Mazurki as Mac Mozolla
George Mann as Tall Wrangler
Frank Mitchell as Little Wrangler


== Production ==
Although the story states off-hand that Jack Spratt is a resident of the fictional town "Clayport, California," the movie was filmed partly on location in Los Angeles, California and at Weeki Wachee Springs in Florida. Some of the action in the film is depicted as taking place at Casa Cugat, Xavier Cugat's Mexican restaurant which was located at 848 North La Cienega Boulevard in West Hollywood.Williams discovered she was pregnant with her first child with husband Ben Gage early into shooting.Williams contracted with Cole of California to appear as their spokesperson before making the film. As a result, the company created the suits that Williams and the aqua chorus wore throughout the film. After filming was done, Williams's doctor ordered her to stay away from activity, and she instead gave swimming lessons to blind children. "Teaching those children how to swim was "one of the most gratifying experiences I've ever had," wrote Williams in her autobiography.Williams played a swimsuit designer in this film, and went on to copy several swimsuit designs from this film in the line of swimwear she would later create.


== Musical numbers ==
The song "Baby It's Cold Outside"  was not planned to be in this film. It took the place of Frank Loesser's "(I'd Like to Get You on a) Slow Boat to China," which the Hays Office thought inappropriate.
"Baby, It's Cold Outside" - Esther Williams and Ricardo Montalbán, Betty Garrett and Red Skelton
Loesser wrote the duet in 1944 and premiered the song with his wife at their Navarro Hotel housewarming party. It went on to win the Academy Award for Best Original Song at the 22nd Academy Awards.
"I Love Those Men" - Betty Garrett with Red Skelton and Xavier Cugat and his Orchestra
"My Heart Beats Faster" - Ricardo Montalbán
"Jungle Rhumba" - Xavier Cugat and his Orchestra


== Release ==
According to MGM records the film was a big hit and earned $3,477,000 in the US and Canada and $2,296,000 overseas, resulting in profit of $1,473,000.


=== Critical reception ===
A 1949 New York Times review of the film called it a "great big beautiful musical, full of slickness and Technicolored plush, models and Xavier Cugat rhythm."


=== Accolades ===
The film is recognized by American Film Institute in these lists:

2004: AFI's 100 Years...100 Songs:
"Baby, It's Cold Outside" – Nominated
2006: AFI's Greatest Movie Musicals – Nominated


=== Home media ===
On July 17, 2007, Turner Entertainment released Neptune's Daughter on DVD as part of the Esther Williams Spotlight Collection, Volume 1. The 5 disc set contained digitally remastered versions of four other Williams's films: Bathing Beauty (1944), Easy to Wed (1946), On an Island with You (1948) and Dangerous When Wet (1953).


== References ==


== External links ==
Neptune's Daughter on IMDb
Neptune's Daughter at AllMovie
Neptune's Daughter at the TCM Movie Database
Neptune's Daughter at the American Film Institute Catalog