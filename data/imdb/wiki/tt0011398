Playing with Fire is a 2019 American family comedy film directed by Andy Fickman from a screenplay by Dan Ewen and Matt Lieberman based on a story by Ewen. The film stars John Cena, John Leguizamo, Keegan-Michael Key, Brianna Hildebrand, Dennis Haysbert, and Judy Greer, and follows a group of smokejumpers who must watch over three children who have been separated from their parents following an accident.
The film was theatrically released by Paramount Pictures on November 8, 2019, in the United States. It is the third Walden Media film from Nickelodeon Movies, after Charlotte's Web and Dora and the Lost City of Gold (the latter was also released in 2019). Despite receiving generally negative reviews from critics, the film was a box office success, grossing $68 million worldwide with production budget of $29 million.


== Plot ==
Superintendent Jake Carson is the commanding officer of a group of smokejumpers in remote California woodlands. Carson is capable in a crisis and takes tremendous pride in his work, diving into dangerous situations to rescue civilians alongside his team - overtly loyal Mark Rogers, nervy and neurotic Rodrigo Torres and "Axe", a huge mute who carries his fireman's axe everywhere. After rescuing three children from a burning cabin, Carson is contacted by the universally admired division commander Richards. Richards praises Carson's work and shortlists Carson for his replacement, Carson's dream job.
Rogers advises Carson that they are responsible for the welfare of the children Brynn, Will, and Zoey under the "SafeHaven Laws", which require law enforcement and first responders to care for children until they are released to a parent or guardian. Carson leaves a voicemail for the children's mother who texts back saying that they are on their way. Carson's attempts to complete his application for division commander are undermined by the children running haywire around the station, and the arrival of Carson's ex-girlfriend, environmental Doctor Amy Hicks, a local who protests the smokejumpers taking water from endangered toad habitats to fight fires. Carson tries to offload the children onto Hicks, who refuses.
Despite the mayhem, the rest of the smokejumpers begin to bond with the children, with toddler Zoey warming up the brutish Axe and Torres teaching Will how to navigate dangerous situations. Brynn pays lip service to Rogers' admiration of Carson, but then stages an escape on the station's ATV, spilling oil and slashing tires to prevent chase. Carson catches them by off-roading on a little girl's bike and corners the children on a dirt road. At Will's prompting, Brynn admits that they are orphans on the run from foster care, fearing that they will be separated. The text messages were from Brynn herself. The group camp out overnight and Carson promises to hold off calling Child Services until after Zoey's birthday in two days. 
The group go all out in preparing for Zoey's birthday, and the four smokejumpers buy presents for Brynn and Will as well. Carson then tells Will a bedtime story about a yeti who was married to his job, had a son, and then died on the job because he was distracted by having a family. Brynn and Hicks are both touched by the story, which fits in with the circumstances of Carson's father's death and Carson's inability to form relationships. The overboard birthday party is interrupted by the unexpected arrival of Richards and Child Services. The children flee in Richards' car and run off the road right on a cliff. While driving, Brynn gets trapped in her seat belt. Carson parachutes down to rescue them and with Will's help, he frees Brynn before the car can roll off the cliff. 
Back at the station, Hicks and the other smokejumpers bid an emotional farewell to the children. Richards tells Carson that family can be a source of support and that there is more to life than working. Inspired, Carson refuses to release the children to Child Services under the Safe Haven laws and proposes a plan to adopt all three of them. Some time later, Carson and Hicks get married with the smokejumpers and their adopted children in attendance.


== Cast ==
John Cena as Jake "Supe" Carson
Keegan-Michael Key as Mark Rogers
John Leguizamo as Rodrigo Torres
Brianna Hildebrand as Brynn
Dennis Haysbert as Commander Richards
Judy Greer as Dr. Amy Hicks
Tyler Mane as Axe
Paul Potts as Axe's opera voice
Christian Convery as Will
Finley Rose Slater as Zoey


== Production ==
The film was announced in October 2018 when John Cena was cast to star in the film. Andy Fickman was hired to direct the next month.By January 2019, Brianna Hildebrand, Judy Greer, Keegan-Michael Key, Edoardo Carfora, Christian Convery, and John Leguizamo joined the cast.  Rob Gronkowski was offered a role in the film but turned it down due to scheduling conflicts.Filming began on February 4, 2019, in Burnaby, British Columbia, and concluded on March 29. Visual effects and animation were done in post-production by Industrial Light & Magic.


== Release ==
The film was originally set to be released on March 20, 2020, but it was later moved up to November 8, 2019, taking over Sonic the Hedgehog's original release date.


== Reception ==


=== Box office ===
Playing with Fire grossed $44.5 million in the United States and Canada, and $24.2 million in other territories, for a worldwide total of $68.6 million.In the United States and Canada, the film was released alongside Doctor Sleep, Midway and Last Christmas, and was projected to gross $7–10 million from 3,125 theaters in its opening weekend. It made $3.6 million on its first day, including $500,000 from Thursday night previews. It went on to debut to $12.8 million, beating projections, and also finishing third at the box office. In its second weekend the film made $8.6 million, finishing fourth behind Ford v Ferrari, Midway and Charlie's Angels.


=== Critical response ===
On review aggregator website Rotten Tomatoes, the film holds an approval rating of 22% based on 73 reviews, with an average rating of 3.93/10. The website’s critics consensus reads, "Playing with Fire, sadly, is incinerated by the catastrophic inferno of its own sickening existence." Metacritic assigned the film a weighted average score of 24 out of 100, based on 14 critics, indicating "generally unfavorable reviews." Audiences polled by CinemaScore gave the film an average grade of "B+" on an A+ to F scale, while those surveyed at PostTrak gave it an average 2.5 out of 5 stars.Wendy Ide  for The Observer gave the film one star, describing the premise as "beyond inept" and calling it "(a) late contender for the worst movie of the year".


== References ==


== External links ==
Playing with Fire on IMDb