Workers' compensation or workers' comp (formerly workmen's compensation until the name was changed to make it gender-neutral) is a form of insurance providing wage replacement and medical benefits to employees injured in the course of employment in exchange for mandatory relinquishment of the employee's right to sue his or her employer for the tort of negligence. The trade-off between assured, limited coverage and lack of recourse outside the worker compensation system is known as "the compensation bargain". One of the problems that the compensation bargain solved is the problem of employers becoming insolvent as a result of high damage awards. The system of collective liability was created to prevent that, and thus to ensure security of compensation to the workers. Individual immunity is the necessary corollary to collective liability.
While plans differ among jurisdictions, provision can be made for weekly payments in place of wages (functioning in this case as a form of disability insurance), compensation for economic loss (past and future), reimbursement or payment of medical and like expenses (functioning in this case as a form of health insurance), and benefits payable to the dependents of workers killed during employment.
General damage for pain and suffering, and punitive damages for employer negligence, are generally not available in workers' compensation plans, and negligence is generally not an issue in the case.


== Origin and international comparison ==
Laws regarding workers compensation vary by country, but the Workers' Accident Insurance system put into place by Prussian Chancellor Otto von Bismarck in 1881 with the start of the Sickness and Accident Laws, is often cited as a model for Europe and later the United States.


=== Statutory no-fault compensation ===
Workers' compensation statutes are intended to eliminate the need for litigation and the limitations of common law remedies by having employees give up the potential for pain- and suffering-related awards, in exchange for not being required to prove tort (legal fault) on the part of their employer. The laws provide employees with monetary awards to cover loss of wages directly related to the accident as well as to compensate for permanent physical impairments and medical expenses.
The laws also provide benefits for dependents of those workers who are killed in work-related accidents or illnesses. Some laws also protect employers and fellow workers by limiting the amount an injured employee can recover from an employer and by eliminating the liability of co-workers in most accidents. US state statutes establish this framework for most employment. US federal statutes are limited to federal employees or to workers employed in some significant aspect of interstate commerce.The exclusive remedy provision states that workers compensation is the sole remedy available to injured workers, thus preventing employees from also making tort liability claims against their employers.


=== Common law remedies ===
In common law nations, the system was motivated by an "unholy trinity" of tort defenses available to employers, including contributory negligence, assumption of risk, and the fellow servant rule.Common law imposes obligations on employers to provide a safe workplace, provide safe tools, give warnings of dangers, provide adequate co-worker assistance (fit, trained, suitable "fellow servants") so that the worker is not overburdened, and promulgate and enforce safe work rules.Claims under the common law for worker injury are limited by three defenses afforded employers:

The Fellow Servant Doctrine is that employer can be held harmless to the extent that injury was caused in whole or in part by a peer of the injured worker.
Contributory negligence allows an employer to be held harmless to the extent that the injured employee failed to use adequate precautions required by ordinary prudence.
Assumption of risk allows an employer to be held harmless to the extent the injured employee voluntarily accepted the risks associated with the work.


== By nation ==


=== Australia ===
As Australia experienced a relatively influential labour movement in the late 19th and early 20th century, statutory compensation was implemented very early in Australia. Each territory has its own legislation and its own governing body.
A typical example is  Work Safe Victoria, which manages Victoria's workplace safety system. Its responsibilities include helping employees avoid workplace injuries occurring, enforcement of Victoria's occupational and safety laws, provision of reasonably priced workplace injury insurance for employers, assisting injured workers back into the workforce, and managing the workers' compensation scheme by ensuring the prompt delivery of appropriate services and adopting prudent financial practices.Compensation law in New South Wales has recently (2013) been overhauled by the state government. In a push to speed up the process of claims and to reduce the amount of claims, a threshold of 11% WPI (whole person impairment) was implemented for physical injuries and 15% for psychiatric injuriesWorkers' compensation regulators for each of the states and territories are as follows:
Australian Capital Territory – Work Safe Act
New South Wales – State Insurance Regulatory Authority (formerly WorkCover NSW)
Northern Territory – NT Work Safe
Queensland – The Workers' Compensation Regulator (formerly Q-COMP)
South Australia – ReturnToWork SA (from 1 July 2015)
Tasmania – WorkCover Tasmania
Victoria – WorkSafe Victoria
Western Australia – WorkCover WAEvery employer must comply with the state, territory or commonwealth legislation, as listed below, which applies to them:

Federal legislation – Safety, Rehabilitation and Compensation Act 1988
New South Wales – Workers Compensation Act 1987 and the Workplace Injury Management and Workers Compensation Act 1998
Northern Territory – Work Health and Safety (National Uniform Legislation) Regulations
Australian Capital Territory – Workers Compensation Act 1951
Queensland – Workers Compensation and Rehabilitation Act 2003
South Australia – Workers Rehabilitation and Compensation Act 1986
Tasmania – Workers Rehabilitation and Compensation Act 1988
Victoria – Workplace Injury Rehabilitation and Compensation Act 2013
Western Australia – Workers Compensation and Injury Management Act 1981


=== Brazil ===
The National Social Insurance Institute (in Portuguese, Instituto Nacional do Seguro Social – INSS) provides insurance for those who contribute. It is a public institution that aims to recognize and grant rights to its policyholders. The amount transferred by the INSS is used to replace the income of the worker taxpayer, when he or she loses the ability to work, due to sickness, disability, age, death, involuntary unemployment, or even pregnancy and imprisonment. During the first 15 days the worker's salary is paid by the employer and after that by the INSS, as long as the inability to work lasts. Although the worker's income is guaranteed by the INSS, the employer is still responsible for any loss of working capacity, temporary or permanent, when found negligent or when its economic activity involves risk of accidents or developing labour related diseases.


=== Canada ===
Workers' compensation was Canada's first social program to be introduced as it was favoured by both workers' groups and employers hoping to avoid lawsuits. The system arose after an inquiry by Ontario Chief Justice William Meredith who outlined a system in which workers were to be compensated for workplace injuries, but must give up their right to sue their employers. It was introduced in the various provinces at different dates. Ontario and Nova Scotia was first and second in 1915, Manitoba in 1916, British Columbia in 1917, Alberta and New Brunswick in 1918, Saskatchewan adopted in 1930. It remains a provincial responsibility and thus the rules vary from province to province. In some provinces, such as Ontario's Workplace Safety and Insurance Board, the program also has a preventative role ensuring workplace safety. In British Columbia, the occupational health and safety mandate (including the powers to make regulation, inspect and assess administrative penalties) is legislatively assigned to the Workers' Compensation Board of British Columbia WorkSafeBC. In most provinces the workers' compensation board or commission remains concerned solely with insurance. The workers' compensation insurance system in every province is funded by employers based on their payroll, industry sector and history of injuries (or lack thereof) in their workplace  (usually referred to as "experience rating").


=== Germany ===

The German worker's compensation law of 6 July 1884, initiated by Chancellor Otto von Bismarck, was passed only after three attempts and was the first of its kind in the world. Similar laws passed in Austria in 1887, Norway in 1894, and Finland in 1895.The law paid indemnity to all private wage earners and apprentices, including those who work in the agricultural and horticultural sectors and marine industries, family helpers and students with work-related injuries, for up to 13 weeks. Workers who are totally disabled get continued benefits at 67 percent after 13 weeks, paid by the accident funds, financed entirely by employers.
The German compensation system has been taken as a model for many nations.


=== India ===
Main article : The Workmen's Compensation Act 1923The Indian worker's compensation law 1923 was introduced on 5 March 1923. It includes Employer's liability compensation, amount of compensation.  Workmen Compensation Insurance covers employees under Workmen Compensation Act, Fatal Accident Act and common law.


=== Italy ===

In Italy workers' compensation insurance is mandatory and is provided by INAIL.


=== Japan ===

Workers' accident compensation insurance is paired with unemployment insurance and referred to collectively as labour insurance. Workers' accident compensation insurance is managed by the Labor Standards Office.


=== Malaysia ===
The Workmen's Compensation Act 1952 is modelled on the United Kingdom's Workmen's Compensation Act 1906. Adopted before Malaysia's independence from the UK, it is now used only by non-Malaysian workers, since citizens are covered by the national social security scheme.


=== Mexico ===

The Mexican Constitution of 1917 defined the obligation of employers to pay for illnesses or accidents related to the workplace. It also defined social security as the institution to administer the right of workers, but only until 1943 was the Mexican Social Security Institute created (IMSS). Since then, IMSS manages the Work Risks Insurance in a vertically integrated fashion: registration of workers and firms, collection, classification of risks and events, and medical and rehabilitation services. A reform in 1997 defined that contributions are related to the experience of each employer. Public sector workers are covered by social security agencies with corporate and operative structures similar to those of IMSS.


=== New Zealand ===
In New Zealand, all companies that employ staff and in some cases others, must pay a levy to the Accident Compensation Corporation, a Crown entity, which administers New Zealand's universal no-fault accidental injury scheme. The scheme provides financial compensation and support to citizens, residents, and temporary visitors who have suffered personal injuries.


=== United Kingdom ===
Great Britain followed the German model. Joseph Chamberlain, leader of the Liberal Unionist party and coalition with the Conservatives, designed a plan that was enacted under the Salisbury government in 1897. the Workmen's Compensation Act 1897 was a key domestic achievement. It served its social purpose at no cost to the government, since compensation was paid for by insurance which employers were required to take out. The system operated from 1897 to 1946. It was expanded to include industrial diseases by the Workmen's Compensation Act 1906 and replaced by a state compensation scheme under the National Insurance (Industrial Injuries) Act 1946. Since 1976, this state scheme has been set out in the UK's Social Security Acts.Work related safety issues in the UK are supervised by the Health and Safety Executive (HSE) who provide the framework by which employers and employees are able to comply with statutory rules and regulations.With the exception of the following, all employers are obliged to purchase compulsory Employer's Liability Insurance in accordance with the Employer's Liability (Compulsory Insurance) Act 1969. The current minimum limit of indemnity required is £5,000,000 per occurrence. Market practice is to usually provide a minimum £10,000,000 with inner limits to £5,000,000 for certain risks, e.g. workers on oil rigs and acts of terrorism.
These employers do not require Employer's Liability Insurance:

local authorities (other than parish councils)
joint boards or committees whose members include members of local authorities
police authorities
nationalised industries or their subsidiaries
certain bodies which are financed out of public funds
employers of crews on offshore installations, ships or hovercraft, if they are covered instead with a mutual insurance association of ship owners or ship owners and others
a health service body or NHS Trust"Employees" are defined as anyone who has entered into or works under a contract of service or apprenticeship with an employer. The contract may be for manual labour, clerical work or otherwise, it may be written or verbal and it may be for full-time or part-time work.
These persons are not classed as employees and, therefore, are exempt:

persons who are not employees (for example independent contractors who are not the employees of the person engaging them)
people employed in any activity which is not a business (such as domestic servants)
people who are related to the employer – husband, wife, father, mother, grandfather, grandmother, stepfather, stepmother, son, daughter, grandson, granddaughter, stepson, stepdaughter, brother sister, half-brother or half-sister
people who are not normally resident in the United Kingdom and who are working there for fewer than 14 consecutive days.Employees need to establish that their employer has a legal liability to pay compensation. This will principally be a breach of a statutory duty or under the tort of negligence. In the event that the employer is insolvent or no longer in existence, compensation can be sought directly from the insurer under the terms of the Third Parties (Rights against Insurers) Act 2010.
For the history of worker's compensation in the UK, see Workmen's Compensation Act 1897 and following acts.


=== United States ===

In the United States, some form of workers' compensation is typically compulsory for almost all employers in most states (depending upon the features of the organization), with the notable exception of Texas as of 2018. Regardless of compulsory requirements, businesses may purchase insurance voluntarily, and in the United States policies typically include Part One for compulsory coverage and Part Two for non-compulsory coverage. By 1949, every state had enacted a workers' compensation program.In most states, workers' compensation claims are handled by administrative law judges, who often act as triers of fact.Workers' compensation statutes which emerged in the early 1900s were struck down as unconstitutional until 1911 when Wisconsin passed a law that was not struck down; by 1920, 42 states had passed workers' compensation laws.


== See also ==

Workers Compensation Act 1987
Workers' compensation employer defense


== References ==


== External links ==
United States Department of Labor
Center for Workers' Compensation Studies at the U.S. National Institute for Occupational Safety and Health
Chisholm, Hugh, ed. (1911). "Employers' Liability and Workmen's Compensation" . Encyclopædia Britannica. 9 (11th ed.). Cambridge University Press. pp. 356–361. This contains a detailed survey of the basis and international applications of the concept as of the early 20th century.