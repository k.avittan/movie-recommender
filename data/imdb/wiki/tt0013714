Super Trouper is the seventh studio album by the Swedish pop group ABBA, first released in 1980. It features the No.1 singles "The Winner Takes It All" and "Super Trouper". The album became the biggest-selling album of 1980 in the UK.


== Overview ==
Led by the international hit "The Winner Takes It All", Super Trouper was the group's sixth chart-topping album in the UK. It was also the best-selling album in Britain for 1980. Super Trouper was first released on CD in 1983 by Polar Music International, in the early 1980s by Polydor, and in the late 1980s by Atlantic Records for the US. The album has been reissued in digitally remastered form four times; first in 1997, then in 2001, again in 2005 as part of The Complete Studio Recordings box set and as a Deluxe Edition (containing a bonus DVD) in 2011. The previous year's divorce between Björn and Agnetha was explored in "The Winner Takes It All", and the members' lives in Stockholm high society circles coloured the lyrics for "On and On and On". Other well-known songs on the album include the hit single title track "Super Trouper", as well as the electro-dance of "Lay All Your Love on Me".
Probably due to the disco backlash of the time, the album saw ABBA returning to a more straightforward pop sound, as opposed to the preceding (and noticeably more dance-oriented) Voulez-Vous album. The album closed with "The Way Old Friends Do", which was recorded live a year earlier during their 1979 concert tour. Although not released as a single with this album, the song was later released as a single in 1992 to promote the compilation More ABBA Gold: More ABBA Hits. The song was later covered by The Alexander Brothers, The Kingston Trio, Philomena Begley and Faryl Smith.


== Album cover and title ==
Super Trouper is a registered trademark owned by Strong Entertainment Lighting, for their brand of followspots, i.e., directional spotlights used to follow a performer on stage.  Album cover designer, Rune Söderqvist, decided to use the spotlight theme and photograph the group, surrounded by circus performers, at Piccadilly Circus, London.  After discovering that there was a law preventing any entertainers or animals appearing in central London, they instead invited the members of two local circuses to Europa Film Studios, Stockholm to take the photograph there. Several of ABBA's friends were also invited to take part and the following also appear on the cover: Görel Hanser (vice-president of Polar Music who subsequently married the band's photographer Anders Hanser), Berka Bergkvist (another Polar Music employee), Tomas Ledin, and Anders Anderson (ABBA's manager's son).
At the same time, Lasse Hallström also filmed scenes that were eventually used in the videos for "Happy New Year", "Felicidad" and "Super Trouper" even though the latter had not even been composed at the time.


== Track listing ==
All tracks written by Benny Andersson and Björn Ulvaeus except where noted.


== Non-album tracks ==
"Elaine"A  song recorded in 1980 and released as B-side "The Winner Takes It All" single.

"Put On Your White Sombrero"A song based on waltz with a Latin American sound recorded in 1980 with lead vocal by Lyngstad. The track was replaced at a late stage by this album's title track, "Super Trouper". The arrangement of "Put On Your White Sombrero" was later remodelled into a new ABBA song, "Cassandra", which became the B-Side to "The Day Before You Came". The song remained unreleased until 1994.


== Personnel ==
ABBA

Agnetha Fältskog - lead vocals (2, 6, 9, 11), co-lead vocals (3, 8, 10, 12), backing vocals
Anni-Frid Lyngstad - lead vocals (1, 4, 5, 7, 13), co-lead vocals (3, 8, 10, 12), backing vocals
Björn Ulvaeus - acoustic guitar, co-lead vocals (10), backing vocals
Benny Andersson - synthesizers, keyboards, co-lead vocals (10), backing vocalsAdditional personnel

Ola Brunkert – drums
Lars Carlsson – horn
Rutger Gunnarsson – bass, guitar
Janne Kling – flute, saxophone
Per Lindvall – drums
Janne Schaffer – guitar
Åke Sundqvist – percussion
Mike Watson – bass
Lasse Wellander – guitar
Kajtek Wojciechowski – saxophone
Producers: Benny Andersson, Björn Ulvaeus
Engineer: Michael B. Tretow
Arranger: Benny Andersson, Björn Ulvaeus
Design: Rune Söderqvist
Mastering for 1980's USA Atlantic CD version by Zal Schreiber of Atlantic Studios, NYC
Remastered for the 1997 Remasters by Jon Astley and Tim Young with Michael B. Tretow
Remastered for the 2001 Remasters by Jon Astley with Michael B. Tretow
Remastered for the 2005 Complete Studio Recordings Box Set by Henrik Jonsson


== Charts ==


== Sales and certifications ==


== References ==


== External links ==
abba4therecord.com – webpage showing Voulez-Vous releases worldwide