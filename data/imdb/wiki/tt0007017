Gort is a fictional humanoid robot that appeared first in the 1951 20th Century Fox American science fiction film The Day the Earth Stood Still and later in its 2008 remake. His depiction varies between film adaptations, however, the original character was loosely based on the character Gnut, from "Farewell to the Master", a 1940 Astounding Science Fiction short story written by Harry Bates, used as the basis for Andrew Phillips's screenplay. In that story, Gnut is a moving green statue that is apparently attendant upon Klaatu, but in the terminus of the story is identified as the eponymous "master" over Klaatu.


== Farewell to the Master depiction ==
Gnut is described with a much more human appearance, in general, as well as emotional expressions.  He is described in the fourth paragraph of the short story as an 8 foot tall giant man made of greenish metal, including musculature:

For Gnut had almost exactly the shape of a man – a giant, but a man – with greenish metal for man's covering flesh, and greenish metal for man's bulging muscles. 
Comparisons are made to robots, but only as a means of differentiating from the crudity of those built by humans while Gnut appears human in several respects.  Additionally, the muscles are described as fully functional:

[Gnut's] neck and shoulders made Cliff a seat hard as steel, but with the difference that their underlying muscles with each movement flexed, just as would those of a human being.
Gnut's movement is supposed to be smooth, walking with an "almost jerkless rhythm which only he among robots possessed", and his hands have "tough metal fingers" while 1951's Gort had none.
Gnut also wears a loincloth, though it isn't explained why.
Particular attention is given to describing Gnut's eyes, which are "internally illuminated red eyes [that] were so set that every observer felt they were fixed on himself alone", and the face in general is described with "a look of sullen, brooding thought."  Toward the end of the short story Gnut's face is described with "metal muscles".  This differs dramatically from both movie depictions which use a visor to represent Gort's eyes, but very few other facial features.  Gnut speaks to the main character at the end of the short story, while the movie depictions remove his ability to speak at all.
Gnut is also able to raise his internal temperature high enough to vaporize "Glasstex", a fictional thermoplastic material used to imprison him partway through the story.  His body is also invulnerable to "ray" weapons, gases, acids, heat, and electrical currents.


== 1951 depiction ==
Gort is an eight-foot tall, seamless robot apparently constructed from a single piece of "flexible metal". He is but one member of a "race of robots" invented by an interplanetary confederation (described as "A sort of United Nations on a Planetary level" by Klaatu, who is a representative of that confederation) to protect their citizens against all aggression by destroying any aggressors. Klaatu describes "him" as one of an interstellar police force, holding irrevocable powers to "preserve the peace" by destroying any aggressor. The fear of provoking these robots acts as a deterrent against aggression. To that end, Gort accompanies Klaatu on his mission to deliver an ultimatum to the people of Earth: the interplanetary confederation is not concerned with internal human politics; however, if humanity threatens to use atomic weapons against the other planets, the Earth will be destroyed to ensure the safety of the other planets.
Gort does not speak, but he can receive and follow verbal commands (including the famous dialog line "Klaatu barada nikto", spoken by actor Patricia Neal's character toward the end of the film), as well non-verbal commands: at one point, Klaatu communicates with him using reflected signals from a borrowed flashlight. This is not the end of his capability, though, as Gort is also shown acting entirely on his own, both to protect Klaatu from harm and to free himself from encasement in a block of plastic. Gort can also operate highly complex machinery, and is both the pilot and captain of the ship that delivers Klaatu to Earth – all of his "race" have similar ships that they use to patrol the planets.Seemingly unarmed, Gort is in fact armed with a laserlike weapon that is projected from beneath a visor on his head. While the weapon can apparently vaporize any physical object, its effect is variable at Gort's discretion, and is precise enough to destroy a single object without harming anything around it. In its first onscreen use, it vaporized several soldier's guns from their hands while not harming the soldiers at all, and seconds later was used to vaporize tanks and artillery without harming their occupants or any surrounding objects. Gort is also continually aware of Klaatu's physical condition and location without Klaatu needing to wear a tracking device of any sort. Gort is not known to be damageable by any means available to mankind, and can – despite resistance – destroy the Earth itself if he is sufficiently provoked. During most of the film, Gort remains motionless in front of his ship, which rests in a baseball park in central Washington, D.C., near the White House. Scientists and military researchers attempt to examine both the robot and the ship.He was portrayed by seven-foot, seven-inch (231 cm)-tall actor Lock Martin wearing a thick foam-rubber suit designed and built by Addison Hehr. Two suits were created, fastened alternately from the front or back so that the robot would appear seamless from any angle in the completed scenes. A fiberglass statue of Gort was also used for the close-ups of the firing of his energy beam weapon or when a scene did not require that he move. To maximize the height of the robot, the Gort suit was made with lifts in the boots. Martin could see forward through the suit's visor area during certain shots, and air holes were provided for him under the robot's wide chin and jaw, and these can be seen in several close-ups of Gort's head.


== 2008 depiction ==

The nature of Gort was almost completely altered in the 2008 remake. Gort's name was revised as an acronym assigned to the robot by the United States' military and scientists: G.O.R.T., which stands for "Genetically Organized Robotic Technology". The acronym is only used once. G.O.R.T. is significantly taller than Gort, being about twenty-eight feet tall as opposed to eight feet. Much like the original Gort, G.O.R.T does not speak. Unlike the original Gort, G.O.R.T. is on Earth specifically to save it from humanity: if the human race doesn't change its ecological destructiveness by a certain time, it will destroy humanity to save the Earth's biosphere. He is neutralized by Klaatu at the end of the film with a massive EMP that also shuts down all of humanity's electrical technology.
G.O.R.T. is made up of a vast swarm of microscopic insect-like devices that can self-replicate through the consumption of matter and energy: these nano-machines are capable of consuming any substance they touch, regardless of the hardness or density of the material being consumed, and can fly as well as crawl. These nanomachines can be used as a doomsday weapon, potentially consuming all material on a planet. In addition to this mode of attack, G.O.R.T. conceals a laser weapon under a visor-like slit in his "face", which can also be used to hack Unmanned aerial vehicles (such as General Atomics MQ-1 Predator drones). It can also generate debilitating high-frequency sound and an electromagnetic field that disrupts all electrical devices in a wide radius.
Unlike the 1951 version, this newer G.O.R.T. is an all-CGI effect, and has 5 digits on each hand, instead of the mitten-style hands of the 1951 robot; his feet, however, have no digits. Features such as the cuffs, belt, visor, and boots of the first Gort are gone. The 2008 G.O.R.T has a more simplistic overall surface design that in close-up appears to "move" due to its nanorobotic composition.


== Comparing 1951 and 2008 performances ==
Owen Gleiberman writes that "Gort isn't so lovey-dovey" in the remake; rather, "he's like a super-tall, obsidian Oscar statue wreaking havoc." At the insistence of Keanu Reeves, the phrase "Klaatu barada nikto" was included in the remake (Keanu says the words when Gort reacts to his shooting, although the words are very distorted).


== Cultural references ==
A life-size replica of the 1951 Gort is on display at the Science Fiction Museum and Hall of Fame in Seattle. Collectors can also own a screen-accurate Gort by visiting The Robot Man website, a company offering many different feature film and television prop robot replicas.A photo of Gort, with Ringo Starr (as Klaatu), appears on the cover of Starr's 1974 Goodnight Vienna album.
Experimental pop artist Eric Millikin created a large mosaic portrait of Gort out of Halloween candy as part of his "Totally Sweet" series in 2013.At 26 minutes and 15 seconds into the science fiction film Tron (1982), there is a reference to The Day the Earth Stood Still (1951) written on the wall of a cubicle: "Gort Klaatu barada nikto".
Gort appears in the FBI lineup in "The Springfield Files", an episode of The Simpsons. With him are other TV aliens, such as Gordon Shumway aka Alf, Chewbacca, Marvin the Martian, and one of the Kang and Kodos team.
Gort is also referenced in Season 7, episode 9 of Futurama- A Clockwork Origin.  The Planet Express crew are tried for crimes against science on a planet inhabited by rapidly evolving robots.  The front of the court building reads "Superior Gort".
The Star Wars film Return of the Jedi contains an homage to the phrase used to deactivate Gort, "Klaatu barada nikto". Two characters are named Klaatu and Barada, and the Klaatu character's alien race are known as the Nikto.
The phrase "Klaatu barada nikto" is used as a magic spell associated with the Necronomicon ex Mortis in the 1992 movie Army of Darkness.
In the season 3 episode of "Super Robot Monkey Team Hyperforce Go!", "Season of the Skull", the phrase used to deactivate Gort, "Klaatu barada nikto" is referenced in the form of a spell from a book.


== See also ==
List of fictional robots and androids


== References ==


=== Citations ===


=== Bibliography ===