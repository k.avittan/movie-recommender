Steven R. Gundry is an American doctor and author. He is a former cardiac surgeon and currently runs his own clinic, investigating the impact of diet on health. Gundry conducted cardiac surgery research in the 1990s and was a pioneer in infant heart transplant surgery, and is a New York Times best-selling author of The Plant Paradox: The Hidden Dangers in "Healthy" Foods That Cause Disease and Weight Gain.He is best known for his disputed claims that lectins, a type of plant protein found in numerous foods, cause inflammation resulting in many modern diseases. The Plant Paradox diet suggests avoiding all foods containing lectins. Scientists and dieticians have classified Gundry's claims about lectins as pseudoscience. He sells supplements that he claims protect against or reverse the supposedly damaging effects of lectins.


== Career ==


=== Cardiothoracic Surgeon ===
Gundry graduated from Yale University with a B.A. in 1972 and went on to earn a medical degree at the Medical College of Georgia in 1977.People reported in 1990 that an infant boy's heart spontaneously healed itself while waiting weeks on life support for a transplant from Gundry and Dr. Leonard Bailey. The boy's recovery made the need for a heart transplant unnecessary, and he received a successful four-hour surgery from Gundry to repair the mitral valve. During his career as a cardiothoracic surgeon, Gundry published three hundred articles and registered several patents for medical devices.In 2002 Gundry began transitioning from Clinical Professor of Cardiothoracic Surgery at the Seventh-day Adventist Loma Linda University School of Medicine to private practice by starting The International Heart & Lung Institute in Palm Springs, California.


=== Nutritionist ===
By mid-2000s Gundry had augmented his career as a cardiothoracic surgeon by providing dietary consulting through The Center for Restorative Medicine, a branch of his private surgery practice. While not an accredited dietitian, Gundry's advice focused on heart health and followed conventional wisdom of western diets such as drinking a glass of red wine per day, increase intake of plants and nuts, reduce simple carbohydrates, and consume fish and grass-fed meats.He has since authored two books focused on food-based health interventions, recommending a mostly plant-based diet. Although not mentioned in his first book, Dr. Gundry’s Diet Evolution: Turn Off the Genes That Are Killing You and Your Waistline (2008), his second book, The Plant Paradox (2017), advocates avoiding lectins, a class of proteins found in numerous plants. In 2018 he published an accompanying recipe book.T. Colin Campbell, a biochemist and advocate for plant-based diets, states that The Plant Paradox contains numerous unsupported claims and denies that it makes a "convincing argument that lectins as a class are hazardous." Robert H. Eckel, an endocrinologist and past president of the American Heart Association, argues that Gundry's diet advice contradicts "every dietary recommendation represented by the American Cancer Society, American Heart Association, American Diabetes Association and so on" and that it is not possible to draw any conclusions from Gundry's own research due to the absence of control patients in his studies. Writing in New Scientist, food writer and chef Anthony Warner notes that Gundry's theories "are not supported by mainstream nutritional science" and that evidence of the benefits of high-lectin containing diets "is so overwhelming as to render Gundry’s arguments laughable".Gundry sells supplements that he claims protect against the damaging effect of lectins. Although Today's Dietician acknowledges evidence that consuming lectins in some raw foods like kidney beans can be harmful, it concludes that "preliminary studies have revealed potential health benefits of lectin consumption and minute evidence of harm."


=== Books ===
Gundry, Steve (2009). Dr. Gundry's Diet Evolution: Turn Off the Genes That Are Killing You and Your Waistline. New York: Harmony Books. ISBN 9780307352118.
Gundry, Steve; Bell Buehl, O. (2017). The Plant Paradox: The Hidden Dangers in "Healthy" Foods That Cause Disease and Weight Gain. New York: Harper Collins. ISBN 9780062966629.
Gundry, Steve (2018). The Plant Paradox Cookbook: 100 Delicious Recipes to Help You Lose Weight, Heal Your Gut, and Live Lectin-Free. New York: Harper Wave. ISBN 9780062843371.
Gundry, Steve (2019). The Plant Paradox Quick and Easy: The 30-Day Plan to Lose Weight, Feel Great, and Live Lectin-Free. New York: Harper Wave. ISBN 9781982625986.
Gundry, Steve (2019). The Longevity Paradox: How to Die Young at a Ripe Old Age. New York: Harper Collins. ISBN 9780062843395.
Gundry, Steve (2019). The Plant Paradox Family Cookbook: 80 One-Pot Recipes to Nourish Your Family Using Your Instant Pot, Slow Cooker, or Sheet Pan. New York: Harper Collins. ISBN 9780062911841.


== References ==


== External links ==
Works by or about Steven Gundry in libraries (WorldCat catalog)