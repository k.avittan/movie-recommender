The Better Half is an American comic strip created by Bob Barnes. It follows the lives of a married couple, Stanley and Harriet Parker, and the usual annoyances couples have with one another after years of marriage. In 1958, the strip won Barnes the National Cartoonist Society Newspaper Panel Cartoon Award.
James Coco and Lily Tomlin made a sitcom pilot based on the strip for ABC in the early 1970s, but no series was ever made.


== Publication history ==
The Better Half was distributed by the Register and Tribune Syndicate in 1956–1986 and King Features Syndicate from 1986 to 2014. 
Creator Bob Barnes produced the panel from its debut on June 25, 1956 until his death in 1970. His wife Ruth Barnes and illustrator Dick Rogers continued the strip until September 30, 1979. It then passed to Vinnie Vinson (October 1, 1979 to October 3, 1982) and Randy Glasbergen (October 10, 1982 to November 30, 2014).Between 1982 and 1992, Glasbergen did the strip under the pseudonym "Jay Harris," so as not to confuse publishers who were familiar with his different style of humor and character design. ("Harris" was his wife's maiden name.) As he was able to transform the characters to his own style, he began using his own name. In the process, Stanley became much shorter than Harriet and lost his scruffy mustache.
At the end of syndication, The Better Half was appearing seven days a week in approximately 150 print and online newspapers around the world. The strip ended on November 30, 2014, after a 58-year run.


== Characters ==
Stanley Parker is a stocky, middle-aged office manager who tends to be lazy, especially at home.
Harriet Parker is Stanley's thin (and thin-tempered) wife. Her cooking does not always agree with Stanley.
Bert is the proprietor and head cook of Bert's Beanery, a greasy spoon which Stanley frequents.


== References ==


=== Notes ===


=== Sources ===
Strickler, Dave. Syndicated Comic Strips and Artists, 1924-1995: The Complete Index. Cambria, California: Comics Access, 1995. ISBN 0-9700077-0-1
"NCS Awards: Newspaper Panel". Retrieved 6 December 2008.


== See also ==
Andy Capp