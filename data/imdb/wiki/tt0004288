Making a Living (also known as Doing His Best, A Busted Johnny, Troubles and Take My Picture) is the first film starring Charlie Chaplin. It premiered on February 2, 1914. Chaplin plays charming swindler who runs afoul of the Keystone Cops.  It was written and directed by Henry Lehrman.


== Plot ==
Chaplin's character attempts to convince a passerby (director Henry Lehrman) to give him money.  Chaplin is then shown flirting with a woman and proposes to her, which she accepts. Lehrman enters to present the woman with flowers and a ring, which the woman refuses citing she's engaged. Lerhman sees Chaplin and a slapstick fight between the two ensues.  Later, Lehrman's character takes a photograph of an automobile accident; Chaplin's character steals the camera whilst the journalist is helping a trapped motorist and rushes back to the paper with it to claim the photograph as his own. A short pursuit with the Keystone Kops follows.


== Chaplin's performance ==
Chaplin wore a large moustache and a top hat in this film, he also carries a walking cane. Chaplin's famed screen persona of "The Little Tramp" did not appear until his next film, Kid Auto Races at Venice, but his character in this film is somewhat similar, having hat, cane, moustache and baggy trousers.  In later accounts Chaplin lamented that the best of his performance had been left out of the final cut.  Lehrman later admitted to deliberately mishandling the cutting of the film out of spite for Chaplin.
The Fremont Hotel, Los Angeles is shown briefly in the background of a fighting scene in the road. This is one of only a couple of films in which Chaplin and the Keystone Cops both appear.


== Review ==
A review in Moving Picture World stated: "The clever player who takes the role of the nervy and very nifty sharper in this picture is a comedian of the first water, who acts like one of Nature's own naturals.  It is so full of action that it is indescribable, but so much of it is fresh and unexpected fun that a laugh will be going on all the time almost.  It is foolish-funny stuff that will make even the sober-minded laugh, but people out for an evening's good time will howl."


== Cast ==
Charlie Chaplin as Swindler
Emma Clifton as jealous husband's wife
Virginia Kirtley as Daughter
Alice Davenport as Mother
Henry Lehrman as Reporter
Minta Durfee as Woman
Chester Conklin as Policeman / Bum
Charles Inslee as Newspaper Editor (uncredited)


== See also ==
Charlie Chaplin filmography


== References ==


== External links ==
Making a Living on IMDb
Making a Living at Rotten Tomatoes
The short film Making a Living is available for free download at the Internet Archive