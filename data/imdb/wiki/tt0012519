The Old Oaken Bucket is a traveling trophy awarded in American college football as part of the rivalry between the Indiana Hoosiers football team of Indiana University and Purdue Boilermakers football team of Purdue University. It was first awarded in 1925.
Indiana and Purdue first met on the gridiron in 1891. The rivalry has been renewed annually every year in peacetime with some exceptions. Purdue leads the overall series 74–42–6. Purdue won the 2018 contest 28–21 in Bloomington, Purdue's second straight win in the rivalry. Indiana ended the streak in 2019, when they won 44–41 in overtime.


== History of the Trophy ==
The concept of a trophy for football games played annually between Purdue University and Indiana University was first proposed during a joint meeting of the Chicago chapters of the Indiana and Purdue alumni organizations in 1925:

"discuss the possibility of undertaking worthy joint enterprises in behalf of the two schools."During that meeting Indiana alumnus Dr. Clarence Jones and Purdue alumnus Russel Gray were appointed to propose a suitable trophy. At a subsequent meeting in Chicago Jones and Gray recommended some oaken bucket be that trophy and the chapters drafted the resolution that:

"an old oaken bucket as the most typical Hoosier form of trophy, that the bucket should be taken from some well in Indiana, and that a chain to be made of bronze block "I" and "P" letters should be provided for the bucket. The school winning the traditional football game each year should have possession of the "Old Oaken Bucket" until the next game and should attach the block letter representing the winning school to the bail with the score engraved on the latter link."Purdue alumnus Fritz Ernst and Indiana alumnus Whiley J. Huddle were appointed to find a suitable oak bucket. They found such a bucket at the then Bruner family farm between Kent and Hanover in southern Indiana.[1]  Although the bucket might have been used at an open well on the Bruner family farm that had been settled during the 1840s, the Bruner family lore indicates that the bucket might have been used by General John Hunt Morgan and his "Raiders" during their jaunt through southeastern Indiana during the Civil War.
In accordance with the Chicago alumni organization's resolution, the winner of the bucket gets a "P" or "I" link added to the chain of the bucket with the score, date and the city where the game was played engraved on the link. In case of a tie, an "I–P" link was added. The inaugural Old Oaken Bucket Game ended in a 0–0 deadlock on November 21, 1925, in Bloomington resulting in the very first and most visible link, an "I–P" link, being added to the handle of the bucket.
When Indiana and Purdue moved to separate divisions for the 2014 season—Indiana to the East and Purdue to the West—the Old Oaken Bucket was the only inter-divisional rivalry protected under the new alignment.


== The poem "The Old Oaken Bucket" ==
The name of the trophy refers to a sentimental poem written in 1817 by a successful printer and publisher, Samuel Woodworth (1784–1842) which begins:

"How dear to this heart are the scenes of my childhood,
When fond recollection presents them to view!
The orchard, the meadow, the deep-tangled wild-wood,
And every loved spot which my infancy knew!...And e'en the rude bucket that hung in the well—
The old oaken bucket, the iron-bound bucket,
The moss-covered bucket which hung in the well.Although Samuel Woodworth was not from Indiana, the poem exemplifies the sentiment felt by the people of Indiana towards their home state. The poem was set to music in 1826 by G. F. Kiallmark (1804–1887) and memorized or sung by generations of American schoolchildren; it made the poet's unpretentious childhood home in Scituate, Massachusetts the goal of sentimental tourists in the late 19th century.
Bing Crosby recorded a musical version of the poem on a Decca record on June 14, 1941 with John Scott Trotter and His Orchestra.


== Series statistics ==


== Game results ==
The 1903 contest, scheduled to be played on October 31 at Washington Park in Indianapolis, Indiana, was canceled after one of the trains carrying the Purdue football team collided with a coal train near 18th Street on the north side of Indianapolis. In all, 17 Purdue football players, coaches, alumni, and team supporters were killed in the Purdue Wreck.


== See also ==
List of NCAA college football rivalry games
List of most-played college football series in NCAA Division I


== References ==


== External links ==
History of the Bucket (Purdue)