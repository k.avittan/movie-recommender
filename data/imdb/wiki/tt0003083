The Lost Boys is a 1987 American teen horror film directed by Joel Schumacher, produced by Harvey Bernhard with a screenplay written by Jeffrey Boam. Janice Fischer and James Jeremias wrote the film's story. The film's ensemble cast includes Corey Haim, Jason Patric, Kiefer Sutherland, Jami Gertz, Corey Feldman, Dianne Wiest, Edward Herrmann, Billy Wirth, Brooke McCarter, Alex Winter, Jamison Newlander, and Barnard Hughes. 
The title is a reference to the Lost Boys in J. M. Barrie's stories about Peter Pan and Neverland, who, like the vampires, never grow up. Most of the film was shot in Santa Cruz, California.
The Lost Boys was released and produced by Warner Bros. Pictures on July 31, 1987 and was a critical and commercial success, grossing over $32 million against a production budget of $8.5 million. 
The success of the film has spawned a franchise with two sequels (Lost Boys: The Tribe and Lost Boys: The Thirst), two comic book series and a future television series.


== Plot ==
Michael Emerson and his younger brother Sam travel with their recently divorced mother Lucy to the small beach town of Santa Carla, California to live with her eccentric father, referred to simply as Grandpa. 
Michael and Sam begin hanging out at the boardwalk, which is plastered with flyers of missing people, while Lucy gets a job at a video store run by the local bachelor Max. Michael becomes fascinated by Star, a young woman he spots on the boardwalk, though she seems to be in a relationship with the mysterious David, the leader of a young biker gang. 
In the local comic book store, Sam meets brothers Edgar and Alan Frog, a pair of self-proclaimed vampire hunters, who give him horror comics to teach him about the threat they claim has infiltrated the town.
Michael finally talks to Star and is approached by David, who goads him into following them by motorcycle along the beach until they reach a dangerous cliff, which Michael almost goes over. At the gang's hangout, a sunken luxury hotel beneath the cliff, David initiates Michael into the group. Star warns Michael not to drink from an offered bottle, telling him it is blood, but Michael ignores her advice. Later on, David and the others, including Michael, head to a railroad bridge where they hang off the edge over a foggy gorge; one by one they fall, Michael falling after them.
Michael wakes up at home the next day, unaware of how he got there. His eyes are sensitive to sunlight and he develops a sudden thirst for blood, which leads him to impulsively attack Sam. Sam's dog, Nanook, retaliates, causing Sam to realize that Michael is turning into a vampire by his brother's semi-transparent reflection. Sam is initially terrified but Michael convinces him that he is not yet a vampire and that he desperately needs his help. 
Michael begins to develop supernatural powers and asks Star for help, but has sex with her shortly afterwards. Sam deduces that, since Michael has not killed anyone, he is a half-vampire and his condition can be reversed upon the death of the head vampire. Sam and the Frog brothers test whether Max is the head vampire during a date with Lucy but Max passes every test and the boys decide to focus on David.
To provoke him into killing, David takes Michael to stalk a group of beach goers and instigates a feeding frenzy. Horrified, Michael escapes and returns home to Sam. Star then arrives and reveals herself as a half-vampire who is looking to be cured. It emerges that David had intended for Michael to be Star's first kill, sealing her fate as a vampire. 
The next day, a weakening Michael leads Sam and the Frog brothers to the gang's lair. They impale one of the vampires, Marko, with a stake, awakening David and the two others but the boys escape, rescuing Star and Laddie, a half-vampire child and Star's companion.
That evening, while Lucy is on a date with Max, the teens arm themselves with holy-water-filled water guns, a longbow and stakes; barricading themselves in the house. When night falls, David's gang attack the house. The Frog brothers and Nanook manage to kill Paul by pushing him into a bathtub filled with garlic and holy water, dissolving him to the bone. Sam is attacked by Dwayne, another vampire, before he shoots an arrow through his heart and into the stereo behind him, electrocuting him and causing parts of his body to explode. 
Michael is then attacked by David, forcing him to use his vampire powers. He manages to overpower David and impales him on a set of antlers. However, Michael, Star and Laddie do not transform back to normal as they had hoped. Lucy then returns home with Max, who is revealed to be the head vampire. He informs the boys that to invite a vampire into one's house renders one powerless over said vampire, leaving them unable to exploit any weaknesses that the vampire has while there, explaining why their earlier assumption appeared to be incorrect. Max reveals he had instructed David to turn Sam and Michael into vampires so that Lucy could not refuse to be transformed herself, as his objective had been to get Lucy to be a mother for his lost boys. 
As Max pulls Lucy to him, preparing to transform her, he is killed when Grandpa crashes his jeep through the wall of the house and impales Max on a wooden fence post, causing him to explode. Michael, Star and Laddie then return to normal. Amongst this carnage and debris, Grandpa casually retrieves a drink from the refrigerator and declares: "One thing about living in Santa Carla I never could stomach: all the damn vampires."


== Cast ==


== Production ==


=== Background ===
A March 5, 1985 Variety news item announced that the independent production company Producers Sales Organization (PSO) bought first-time screenwriters Janice Fischer and James Jeremias's Lost Boys script for $400,000 on February 20, 1986. PSO announced their acquisition of the project at American Film Market 1985. Later Warner Bros. joined the project, taking over domestic distribution and some foreign territories.The film's title is a reference to the characters featured in J.M. Barrie's Peter Pan stories, who — like vampires — never grow old. According to Day, the central theme of The Lost Boys, "organised around loose allusions to Peter Pan", is the tension surrounding the Emerson family and the world of contemporary adolescence. The film was originally set to be directed by Richard Donner and the screenplay, written by Janice Fischer and James Jeremias, was modelled on Donner's recent hit The Goonies (1985). In this way the film was envisioned as more of a juvenile vampire adventure with 13 or 14 year old vampires, while the Frog brothers were "chubby 8 year-old Cub Scouts" and the character of Star was a young boy. When Donner committed to other projects, Joel Schumacher was approached to direct the film. He insisted on making the film sexier and more adult, bringing on screenwriter Jeffrey Boam to retool the script and raise the ages of the characters.


=== Casting ===
Director Joel Schumacher said he had "one of the greatest [casts] in the world. They are what make the film." Most of the younger cast members were relatively unknown. Schumacher and Marion Dougherty met with many candidates. Jason Patric was approached early on by Schumacher to play Michael, but Patric had no interest in doing a vampire film and turned it down "many times". Eventually he was won over by Schumacher's vision and his promise to allow the cast a lot of "creative input" in making the film. According to Kiefer Sutherland, Patric "was really instrumental" in adapting the script with Schumacher and shaping the film.Schumacher envisioned the character of Star as being a waifish blonde, similar to Meg Ryan, but he was convinced by Jason Patric to consider Jami Gertz, who had just worked with Patric in Solarbabies (1986). Schumacher was impressed, but only at Patric's insistence did he finally cast Gertz. Schumacher was surprised when his first choice for the role of Lucy, Dianne Wiest, accepted the role, as she had just recently won the Academy Award for Best Supporting Actress for Hannah and Her Sisters (1986).After seeing Kiefer Sutherland's portrayal of Tim in At Close Range, Schumacher arranged a reading with him at which they got on very well. Sutherland had just completed work on Stand by Me when he was offered the role of David. Schumacher said Sutherland "can do almost anything. He's a born character actor. You can see it in The Lost Boys. He has the least amount of dialogue in the movie, but his presence is extraordinary."


=== Principal photography ===
Most of the film was shot in Santa Cruz, California; locations include the Santa Cruz Boardwalk, the Pogonip open space preserve, and the surrounding Santa Cruz Mountains. Other locations included a cliffside on the Palos Verdes Peninsula in Los Angeles County, used for the entrance to the vampire cave, and a valley in Santa Clarita near Magic Mountain, where introductory shots were shot for the scene where Michael and the Lost Boys hang from a railway bridge. Stage sets included the vampire cave, built on Stage 12 of the Warner Bros. lot, and a recreation of the interior and exterior of the Pogonip clubhouse on Stage 15, which stood in for Grandpa's house.Sutherland broke his right wrist while doing a wheelie on his motorcycle and had to wear gloves on camera to conceal the cast. His motorcycle for the movie was adapted so he could operate it with his left hand only.


== Reception ==


=== Box office ===
The Lost Boys opened at #2 during its opening weekend, with a domestic gross of over $5.2 million. It went on to gross a domestic total of over $32.2 million against an $8.5 million budget.


=== Critical response ===
Critical reception was generally positive. Roger Ebert gave the film two-and-a-half out of four stars, praising the cinematography and "a cast that's good right down the line," but ultimately describing Lost Boys as a triumph of style over substance and "an ambitious entertainment that starts out well but ends up selling its soul." Caryn James of The New York Times called Dianne Wiest's character a "dopey mom" and Barnard Hughes's character "a caricature of a feisty old Grandpa." She found the film more of a comedy than a horror and the finale "funny". Elaine Showalter comments that "the film brilliantly portrays vampirism as a metaphor for the kind of mythic male bonding that resists growing up, commitment, especially marriage." Variety panned the film, calling it "a horrifically dreadful vampire teensploitation entry that daringly advances the theory that all those missing children pictured on garbage bags and milk cartons are actually the victims of bloodsucking bikers."On Rotten Tomatoes, the film has an approval rating of 76% based on 67 reviews. The site's critics consensus reads, "Flawed but eminently watchable, Joel Schumacher's teen vampire thriller blends horror, humor, and plenty of visual style with standout performances from a cast full of young 1980s stars." On Metacritic it has a rating of 63% based on reviews from 16 critics, indicating "Generally favorable reviews". Audiences surveyed by CinemaScore gave the film a grade of "A−" on scale of A+ to F.It won a Saturn Award for Best Horror Film in 1987.


=== Cultural influence ===
The mythographer A. Asbjørn Jøn wrote that The Lost Boys helped shift popular culture depictions of vampires. The film is often credited with bringing a more youthful appeal to the vampire genre by making the vampires themselves sexy and young. This inspired subsequent films like Buffy the Vampire Slayer. The scene in which David transforms noodles into worms was directly referenced in the 2014 vampire mockumentary film What We Do in the Shadows. The film inspired the song of the same name by the Finnish gothic rock band The 69 Eyes.The music video for Into the Summer, a song released by American rock band Incubus on August 23, 2019, pays homage to the film.Event organizers Monopoly Events created "the biggest Lost Boys reunion ever" in 2019 at their annual horror fan convention, For the Love of Horror, which included Kiefer Sutherland, Jason Patric, Alex Winter, Jamison Newlander, and Billy Wirth along with musicians from the film, G Tom Mac, and Tim Cappello, who all appeared at the event and were reunited for the first time in over 30 years. Both G Tom Mac and Tom Cappello performed separate live music sets on the event stage to a vast crowd of fans on both days of the event, while Cappello performed a third time at the event after-party. All of the celebrities posed together for photographs in a purpose-built "cave" set modeled on the vampire cave seen in The Lost Boys original movie which was complete with a poster of Jim Morrison, a bottle of fake blood and David the vampire's wheelchair.


== Novelization ==
Due to his past fantasy novels and horror short stories, Craig Shaw Gardner was given a copy of the script and asked to write a novelization to accompany the film's release. At the time, Gardner was, like the Frog Brothers, managing a comic book store as well as writing.The novelization was released in paperback by Berkley Publishing and is 220 pages long. It includes several scenes later dropped from the film, such as Michael working as a trash collector for money to buy his leather jacket. It expands the roles of the opposing gang, the Surf Nazis, who were seen as nameless victims of the vampires in the film. It also includes several tidbits of vampire lore, such as not being able to cross running water and salt sticking to their forms.


== Sequels ==
Kiefer Sutherland's character, David, was impaled on antlers but does not explode or dissolve as do the other vampires. He was intended to have survived, which would be picked up in a sequel, The Lost Girls. Scripts for this and other sequels circulated over the years; Joel Schumacher made several attempts at a sequel during the 1990s, but nothing came to fruition.David makes a reappearance in the 2008 comic book series, Lost Boys: Reign of Frogs, which serves as a sequel to the first film and a prequel to Lost Boys: The Tribe.
A direct-to-DVD sequel, Lost Boys: The Tribe, was released more than 20 years after the release of the original film. Corey Feldman returned as Edgar Frog, with a cameo by Corey Haim as Sam Emerson. Kiefer Sutherland's half-brother Angus Sutherland played the lead vampire, Shane Powers.In March 2009, MTV reported that work had begun on a third film entitled Lost Boys: The Thirst, with Feldman serving as an executive producer in addition to playing Edgar Frog, and Newlander returning as Alan Frog.  Haim, who was not slated to be part of the cast, died in March 2010. The film was released on DVD on October 12, 2010. A fourth film was discussed as well as a Frog Brothers television show but with the dissolution of Warner Premiere, the projects evaporated.In July 2016, Vertigo stated for a release of a miniseries comics starting on October 12, 2016, where Michael, Sam and the Frog Brothers must protect Star from her sisters, the Blood Belles.


== Music ==
Thomas Newman wrote the original score as an eerie blend of orchestra and organ arrangements, while the music soundtrack contains a number of notable songs and several covers, including "Good Times", a duet between INXS and former Cold Chisel lead singer Jimmy Barnes which reached No. 2 on the Australian charts in early 1987. This cover version of a 1960s Australian hit by the Easybeats was originally recorded to promote the Australian Made tour of Australia in early 1987, headlined by INXS and Barnes.
Tim Cappello's cover of The Call's "I Still Believe" was featured in the film as well as on the soundtrack. Cappello makes a small cameo appearance in the film playing the song at the Santa Cruz boardwalk, with his saxophone and bodybuilder muscles on display.
The soundtrack also features a cover version of The Doors' song "People Are Strange" by Echo & the Bunnymen. The song as featured in the film is an alternate, shortened version with a slightly different music arrangement.
Lou Gramm, lead singer of Foreigner, also recorded "Lost in the Shadows" for the soundtrack, along with a video which featured clips from the film.The theme song, "Cry Little Sister", was originally recorded by Gerard McMahon (under his pseudonym Gerard McMann) for the soundtrack, and later re-released on his album "G Tom Mac" in 2000. In the film's sequel Lost Boys: The Tribe, "Cry Little Sister" was covered by a Seattle-based rock band, Aiden and appeared again in the closing credits of Lost Boys: The Thirst.


=== Soundtrack ===
"Good Times" by Jimmy Barnes and INXS – 3:49 (The Easybeats)
"Lost in the Shadows (The Lost Boys)" by Lou Gramm – 6:17
"Don't Let the Sun Go Down on Me" by Roger Daltrey – 6:09 (Elton John/Bernie Taupin)
"Laying Down the Law" by Jimmy Barnes and INXS – 4:24
"People Are Strange" by Echo & the Bunnymen – 3:36 (The Doors)
"Cry Little Sister (Theme from The Lost Boys)" by Gerard McMann – 4:46
"Power Play" by Eddie & the Tide – 3:57
"I Still Believe" by Tim Cappello – 3:42 (The Call)
"Beauty Has Her Way" by Mummy Calls – 3:56
"To the Shock of Miss Louise" by Thomas Newman – 1:21


== Charts ==


== References ==


== Further reading ==
Patrick Day, William (2002). Vampire Legends in Contemporary American Culture: What Becomes a Legend Most. United States: UPK. ISBN 0813122422.


== External links ==
The Lost Boys on IMDb
The Lost Boys at AllMovie
The Lost Boys at Box Office Mojo
"The Story Behind The Lost Boys"