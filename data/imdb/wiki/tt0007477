Close Enough is an American adult animated sitcom created by J. G. Quintel for HBO Max. Originally, the series was created for TBS in 2017, but was shelved. After the AT&T purchase of Time Warner, the show was put on hold. It was released exclusively to the HBO Max streaming service on July 9, 2020. The series has received positive reviews, with several critics comparing it favorably to Quintel's previous series, Regular Show. On August 6, 2020, the series was renewed for a second season. On September 14, 2020, the series was broadcast internationally through Netflix and Adult Swim.


== Plot ==
The series revolves around a couple who are in their early thirties with their five-year-old daughter and their divorced friends who live with them in a Los Angeles duplex. As in Regular Show, what seem like normal domestic crises usually escalate in surreal, often science fictional, ways.


== Cast ==


=== Main characters ===
J. G. Quintel as Joshua "Josh" Singleton, Emily's husband and Candice's father who is a video game developer and works for a company based on Geek Squad. He is modeled and based after his voice actor and creator J. G. Quintel.
Gabrielle Walsh as Emily Ramirez Singleton, Josh's wife and Candice's mother who works as an assistant for a food corporation and part-time in a comedy singer-songwriter band with Bridgette. Her character is based on J. G. Quintel's wife Cassia.
Jessica DiCicco as Candice Singleton-Ramirez, Josh and Emily's five-year-old daughter who struggles with school work.
Jason Mantzoukas as Alex Dorpenberger, Josh's best friend and Bridgette's ex-husband who works as a community college professor.
Kimiko Glenn as Bridgette Yoshida, Emily's best friend and Alex's ex-wife. She is a Japanese-American social media influencer.
Danielle Brooks as Pearle Watson, a retired LAPD cop who is the landlord of the duplex and the adoptive mother of Randy.
James Adomian as Randy Watson, Pearle's adopted son and the building's property manager.


=== Recurring characters ===
John Early as Mr. Timothy Brice Campbell, Candice's teacher at Chamomile Elementary.
Fred Stoller as Mr. Salt, Emily's boss


=== Additional voices ===
William Salyers
Max Mittelman
Roger Craig Smith
Erica Lindbeck
Fred Tatasciore
Jeff Bennett
Matthew Mercer
Grey Griffin
Ali Hillis
Mo Collins
Eric Bauza
Kate Micucci
Skyler Gisondo
Marc Evan Jackson
Tom Kenny
John DiMaggio
Corey Burton
Maurice LaMarche
Richard Steven Horvitz


=== Guest stars ===
Weird Al Yankovic as himself
David Hasselhoff as himself
Noel Fielding as Snailathan Gold
Dave Foley as Dr. Ferguson
George Lopez as Wurst Bros. leader
Matt Besser as Bush Guy
Cheri Oteri as Dr. Glandz
Rachel Dratch as Meredith Breedmore
Seth Morris as Robot Guard
Lauren Lapkus as Forever 23 employee
Suzy Nakamura as Bridgette's mom
Chris Parnell as Ron
Eugene Cordero as Dante
Rich Sommer as Keith Nash
Brent Weinbach as Lee
Steve Agee as Davey Wegman
Judy Greer as Nikki
Diamond White as Caitlin Olsman
Jessica St. Clair as Joy
Lea DeLaria as Prisoner
Horatio Sanz as Raoul
Lennon Parham as Toluca Lake
James Urbaniak as Commercial Bot
Whitmer Thomas as The Gooch


== Release ==
The series was announced in May 2017, four months after Quintel's previous series, Regular Show, concluded, and was originally planned to air on TBS, but it was delayed several times, and TBS's plans for the animation block to premiere it fell through when production on The Cops was shut down due to Louis C.K.’s admitted sexual misconduct. On October 29, 2019, it was announced that the series would instead make its home on HBO Max. The second half of the third episode was premiered at the Annecy International Animated Film Festival on June 15, 2020. On September 14, 2020, it was released worldwide through Netflix.


== Episodes ==
221 minutes of content (meaning 10 half-hours) was supposed to be released for the first season. When the series launched, the first season only had 8 episodes released.


== Reception ==
Review aggregator Rotten Tomatoes reported an approval rating of 100% based on 18 reviews, with an average rating of 7.7/10. The website's critical consensus reads, "Completely absurd and yet, utterly relatable, Close Enough captures the strange experience that is being an adult." On Metacritic, it has a weighted average score of 74 out of 100 based on 6 reviews, indicating "generally favorable reviews".


== References ==


== External links ==
Close Enough on HBO Max
Close Enough on IMDb