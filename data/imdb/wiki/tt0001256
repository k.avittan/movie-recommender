The House with Closed Shutters is a 1910 American silent drama film directed by  D.W. Griffith and released by the Biograph Company.


== Plot ==
During the American Civil War a young soldier loses his nerve in battle and runs away to his home to hide; his sister puts on his uniform, takes her brother's place in the battle, and is killed. Their mother, not wanting the shameful truth to become known, closes all the shutters (hence the film's title) and keeps her son's presence a secret for many years, until two boyhood chums stumble upon the truth.


== Cast ==
Henry B. Walthall as The Confederate soldier
Grace Henderson as His mother
Dorothy West as His sister
Joseph Graybill as Her suitor
Charles West as Her suitor
William J. Butler as The colored servant (credited as W. J. Butler)
Edwin August
Verner Clarges as In Lee's Tent
John T. Dillon as On Porch / At Farewell / In Lee's Tent (credited as Jack Dillon)
Gladys Egan as On Porch / At Farewell
Frank Evans as In Lee's Tent
Francis J. Grandon as In Lee's Tent
Alfred Paget as On Porch / At Farewell
Mabel Van Buren as On Porch / At Farewell


== Production ==
The House with Closed Shutters was filmed between June 25 and July 2, 1910, at the Biograph studios and on location in Fort Lee, New Jersey.


== Commentary ==
Unlike the portrayal of women in film serials where they tend to be in peril and need to be saved, West's "sister" character, while dressed in her brother's uniform, was in control of her situation as she rode over the same dangerous route that her brother had taken in his flight. Her horsemanship is emphasized over the other male riders, as soon as she is on the horse she rides at full speed and she alone among the riders has her horse rear up on its hind legs.


== Preservation ==
Prints of The House with Closed Shutters exist in the film archives of the Museum of Modern Art, George Eastman House, and the Library of Congress.


== See also ==
List of American films of 1910
1910 in film


== References ==


== External links ==
The House with Closed Shutters on IMDb
The House with Closed Shutters is available for free download at the Internet Archive