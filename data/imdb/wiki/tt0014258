The faun (Latin: faunus, Ancient Greek: φαῦνος, phaunos, pronounced [pʰaunos]) is a mythological half human–half goat creature appearing in Roman mythology.
The goat men, more commonly affiliated with the Satyrs of Greek mythology than the fauns of Roman, are bipedal creatures with the legs and tail of a goat and the head, torso, and arms of a man, and are often depicted with goat's horns and pointed ears. These creatures borrowed their appearance from the satyrs, who in turn borrowed their appearance from the god Pan of the Greek pantheon. They were symbols of peace and fertility, and their chieftain, Silenus, was a minor deity of Greek mythology.


== Origins ==

Romans believed fauns inspired fear in men traveling in lonely, remote or wild places. They were also capable of guiding humans in need, as in the fable of The Satyr and the Traveller, in the title of which Latin authors substituted the word Faunus. Fauns and satyrs were originally quite different creatures: whereas fauns are half-man and half-goat, satyrs originally were depicted as stocky, hairy, ugly dwarves or woodwoses with the ears and tails of horses or asses. Satyrs also were more woman-loving than fauns, and fauns were rather foolish where satyrs had more knowledge.
Ancient Roman mythological belief also included a god named Faunus often associated with enchanted woods and the Greek god Pan and a goddess named Fauna who were goat people.


== In art ==

		
		
		
		
		
		
		
		
The Barberini Faun (located in the Glyptothek in Munich, Germany) is a Hellenistic marble statue from about 200 BCE, found in the Mausoleum of the Emperor Hadrian (the Castel Sant'Angelo) and installed at Palazzo Barberini by Cardinal Maffeo Barberini (later Pope Urban VIII). Gian Lorenzo Bernini restored and refinished the statue.The House of the Faun in Pompei, dating from the 2nd century BCE, was so named because of the dancing faun statue that was the centerpiece of the large garden. The original now resides in the National Museum in Naples and a copy stands in its place.The French symbolist Stéphane Mallarmé's famous masterpiece L'après-midi d'un faune (published in 1876) describes the sensual experiences of a faun who has just woken up from his afternoon sleep and discusses his encounters with several nymphs during the morning in a dreamlike monologue. The composer Claude Debussy based his symphonic poem Prélude à l'après-midi d'un faune (1894)  on the poem, which also served as the scenario for a ballet entitled L'après-midi d'un faune (or Afternoon of a Faun) choreographed to Debussy's score in 1912 by Vaslav Nijinsky.


== In fiction ==
The Marble Faun (1860) is a romance set in Italy by Nathaniel Hawthorne. It was said to have been inspired after viewing the Faun of Praxiteles in the Capitoline Museum.
In H.G. Wells' The Time Machine (1895), in the year 802,701 A.D., the Time Traveller sees "a statue—a Faun, or some such figure, minus the head."
Mr. Tumnus, in C. S. Lewis's The Chronicles of Narnia (1949), is a faun. Lewis said that the famous Narnia story, The Lion, the Witch and the Wardrobe, all came to him from a single picture he had in his head of a faun carrying an umbrella and parcels through a snowy wood. In the film series, fauns are distinct from satyrs, which are more goat-like in form.
In Lolita, the protagonist is attracted to pubescent girls whom he dubs "nymphets"; "faunlets" are the male equivalent.
In the 1981 film My Dinner with Andre it is related how fauns befriend and take a mathematician to meet Pan.
In Guillermo del Toro's 2006 film Pan's Labyrinth (El Laberinto del Fauno), a faun guides the film's protagonist, Ofelia, to a series of tasks, which lead her to a wondrous netherworld.
Don, in Rick Riordan's The Son of Neptune (2011), is a faun. In the book, several fauns appear, begging for money. Due to his memory of the Greek satyrs, Percy Jackson feels like there should be more to fauns. Also, in the prequel to The Son of Neptune, The Lost Hero, Jason Grace calls Gleeson Hedge a faun upon learning that he is a satyr. In the third instalment in the series, The Mark of Athena, Frank Zhang calls Hedge a faun.
In The Goddess Within, a visionary fiction novel written by Iva Kenaz, the main heroine falls in love with a faun.
In the Spyro video game series, Elora is a faun from Avalar, who helps Spyro the dragon navigate the world around him.


== See also ==


== Notes ==


== References ==
Wells, H. G. (1961) [First published 1895], The Time Machine, New York: Dolphin Books