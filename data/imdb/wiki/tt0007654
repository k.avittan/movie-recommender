Married at First Sight is an American reality television series based on a Danish series of the same name titled Gift Ved Første Blik. The series first aired in the United States on FYI. Beginning with season two, it aired in simulcast on sister network A&E. The series features three to five couples, paired up by relationship experts, who agree to marry when they first meet. For the first three seasons, the experts were clinical psychologist Dr. Joseph Cilona, sexologist Dr. Logan Levkoff, sociologist Dr. Pepper Schwartz, and humanist chaplain Greg Epstein. Starting with the fourth season, the experts were Schwartz, pastor and marriage counselor Calvin Roberson, and communication and relationship expert Rachel DeAlto. Rachel DeAlto was replaced with Dr. Jessica Griffin beginning with the sixth season. Dr. Griffin stayed until the ninth season and was replaced by Dr. Viviana Coles in seasons ten and eleven. The couples spend their wedding night in a hotel before leaving for a honeymoon. Upon returning home, they live together as a married couple for the remainder of the eight weeks. Thereafter they must choose to divorce or stay married. On October 25, 2016 FYI renewed the show for a fifth season. In 2017, for the fifth season, the show moved to the Lifetime channel. The following year, two spinoffs were announced to premiere that October, Married at First Sight: Honeymoon Island and Married at First Sight: Happily Ever After.The eleventh season premiered on July 15, 2020 following a kickoff and matchmaking special on July 8, 2020.


== Synopsis ==
Over the ten completed seasons of MAFS, 34 couples have been chosen for the experiment. Out of these 34, 21 chose to stay married on Decision Day, at the end of the six- or eight-week experiment. As of mid-April 2020, however, only nine couples are still married, making for an overall success rate of 26%.


=== Season 1 ===
The first season took place in New York City and northern New Jersey. The couples were:

Cortney and Jason divorced after five years of marriage. Jason married actress Roxanne Pallett in January 2020.Jamie and Doug are now a family of four. The couple lost their first son, Johnathan Edward, at 17 weeks' gestation in 2016 before going on to have a daughter, Henley Grace in 2017. They have spoken about subsequent struggles to conceive, experiencing a chemical pregnancy in 2018 and a miscarriage at 10 weeks along in 2019. Their son, Hendrix Douglas was born in May 2020.


=== Season 2 ===
The second season, just like the first, took place in New York City and Northern New Jersey. The couples were:


=== Season 3 ===
The third season took place in Atlanta, Georgia. The couples were:


=== Season 4 ===
The fourth season of Married at First Sight premiered on Tuesday, July 26, 2016. The setting was South Florida (particularly Miami) and in this season, two new specialists joined sociologist Dr. Pepper Schwartz: communication and relationship expert Rachel DeAlto, and marriage counselor Pastor Calvin Roberson. Doctors Joseph Cilona and Logan Levkoff exited the series after season three. The couples were:

1 Heather and Derek left in the middle of the experiment.


=== Season 5 ===
The fifth season of Married at First Sight was renewed on October 25, 2016.  For this season, the show moved to the Lifetime channel.  The episodes aired in 2017 and featured couples in Chicago. The couples were:

Ashley Petta and Anthony D'Amico announced in July of 2020 that they’re expecting daughter No. 2 after welcoming their first, Mila, in January of 2019.


=== Season 6 ===
The sixth season of Married at First Sight was renewed on the Lifetime channel.  The first of the episodes aired on January 2, 2018 and featured couples in Boston. Relationship expert Dr. Jessica Griffin joined the show as a specialist, replacing Rachel DeAlto who left after Season 5. The couples were:

Shawniece Jackson and Jephte Pierre welcomed their first child, a baby girl named Laura Denise Pierre, in October of 2018.Jonathan Francetic and relationship expert Dr. Jessica Griffin revealed they had struck up a romance following his time on the show when Griffin announced she would not be returning to Married At First Sight as an expert for Season 9. They began dating more than five months after the season wrapped. The pair became engaged in April of 2019. Their original wedding date was set for October of 2020 but was postponed due to the COVID-19 pandemic.


=== Season 7 ===
The seventh season of Married at First Sight was renewed on the Lifetime channel, and featured couples from Dallas, with the first episode airing on July 10, 2018. The couples were:

Danielle Bergman-Dodd and Bobby Dodd are expecting their second child together, due January 2021. Their first, Olivia Nicole, was born in February 2019.


=== Season 8 ===
The eighth season of Married at First Sight featured couples from Philadelphia, and premiered on January 1, 2019. The couples were:


=== Season 9 ===
The ninth season of Married at First Sight featured couples from Charlotte, North Carolina and premiered on June 12, 2019. Dr. Jessica Griffin did not return after revealing she was in a relationship with Season 6 cast member Jon Francetic, with the couple later getting engaged and going into business together. She was replaced by relationship expert Dr. Viviana Coles. The couples were:

Deonna McNeill and Greg Okotie announced they are expecting their first baby together in September of 2020.


=== Season 10 ===
The tenth season of Married at First Sight featured couples from Washington D.C. The first episode aired on January 1, 2020. The couples were:


=== Season 11 ===
The eleventh season of Married at First Sight will feature couples from New Orleans. The first episode was set to premiere on July 15, 2020. The couples are:


=== Season 12 ===
The twelfth season of Married at First Sight will feature couples again from Atlanta.


=== Season 13 ===
The thirteenth season of Married at First Sight will feature couples from Houston.


== Timeline of Cast ==


== Spinoffs ==
Married at First Sight has had eight spinoffs:

Married at First Sight: The First Year
Married Life
Married at First Sight: Second Chances
Jamie and Doug Plus One
Married at First Sight: Honeymoon Island
Married at First Sight: Happily Ever After
Married at First Sight: Couples' Cam
Married at First Sight: Unmatchables


=== Married at First Sight: The First Year ===
Married at First Sight: The First Year follows the lives of the two Season 1 couples that remained married from the six-month mark to the one-year anniversary and beyond. The couples are Jamie Otis-Hehner and Doug Hehner, and Cortney Hendrix-Carrion and Jason Carrion. The first episode premiered on January 13, 2015. A total of two seasons have been shown for this spinoff.


=== Married Life ===
Married Life continues to follow the daily lives of two married couples from Season 1, Jamie Otis-Hehner and Doug Hehner and Cortney Hendrix-Carrion and Jason Carrion. The first season premiered May 5, 2015. The second season premiered January 24, 2017.


=== Married at First Sight: Second Chances ===
Two participants from Season 3, Vanessa Nelson and David Norton, both took part in the first season of the second spinoff Married at First Sight: Second Chances. Both David and Vanessa selected from a group of men (for Nelson) and women (for Norton) to date in a format similar to that of The Bachelor or The Bachelorette, ultimately ending with each of them choosing someone to marry. The groups were narrowed down from one hundred (in a speed dating-like situation) to twenty-five, to ten. After Vanessa and David each picked their top ten, they eliminated one person each episode (with the exception of one week's double elimination) until finding 'the one'. The first season premiered on 27 April 2017 and concluded on 6 July 2017 with Vanessa becoming engaged to Andre, although they ultimately ended their relationship, and David choosing to marry no one.


=== Jamie and Doug Plus One ===
Jamie and Doug Plus One continues to follow the lives of Jamie Otis-Hehner and Doug Hehner from the time right before the birth of their daughter Henley, through the first months of new parenthood. The series shows the couple's struggles with caring for their newborn while trying to maintain intimacy in their marriage.


=== Married at First Sight: Honeymoon Island ===
Married at First Sight: Honeymoon Island is an eight-episode series about sixteen singles on an exotic island attempting to match themselves. Casting includes unmatched singles from earlier seasons of the original, fan favorites, and new candidates. Married at First Sight: Honeymoon Island premiered on October 23, 2018, and the participants are:
At the end of the season, the following participants remain:


=== Married at First Sight: Happily Ever After ===
Married at First Sight: Happily Ever After follows the lives of couples from previous seasons who still remain married and are expecting children, including Ashley Petta and Anthony D'Amico from Season 5, Shawniece Jackson and Jephte Pierre from Season 6, and Danielle Bergman and Bobby Dodd from season 7. The first episode premiered on October 30, 2018.


=== Married at First Sight: Couples' Cam ===
Married at First Sight: Couples' Cam premiered on May 20, 2020.


=== Married at First Sight: Unmatchables ===
Married at First Sight: Unmatchables will premiere in 2021.


== International versions ==
The format created in Denmark has been adapted in several countries.


== External links ==
Married at First Sight on IMDb


== References ==