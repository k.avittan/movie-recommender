The Man Who Would Be King is a 1975 Technicolor adventure film adapted from the 1888 Rudyard Kipling novella of the same name. It was adapted and directed by John Huston and starred Sean Connery, Michael Caine, Saeed Jaffrey, and Christopher Plummer as Kipling (giving a name to the novella's anonymous narrator). The film follows two rogue ex-soldiers, former non-commissioned officers in the British Army, who set off from late 19th-century British India in search of adventure and end up in faraway Kafiristan, where one is taken for a god and made their king.


== Plot ==
In 1885 in India, while working late at night in his newspaper office, the journalist Rudyard Kipling is approached by a ragged, seemingly crazed derelict who reveals himself to be Peachy Carnehan, an old acquaintance. Carnehan tells Kipling the story of how he and his comrade-in-arms Danny Dravot, ex-sergeants of the British Army who had become adventurers, travelled far beyond India into the remote land of Kafiristan.
Three years earlier, Dravot and Carnehan had met Kipling under less than auspicious circumstances. After stealing Kipling's pocket-watch, Carnehan found a masonic tag on the chain and, realising he had robbed a fellow Freemason, had to return it. At the time, he and Dravot were working on a plot to blackmail a local rajah, which Kipling foiled by getting the British district commissioner to intervene. In a comic relief turn, Carnehan obliquely blackmails the commissioner in order to avoid deportation.
Frustrated at the lack of opportunities for lucrative criminal mischief in an India becoming more civilised and regulated, partly through their own hard efforts as soldiers, and with little to look forward to in England except petty jobs, the two turn up at Kipling's office with an audacious plan. Forsaking India, they will head with twenty rifles and ammunition to Kafiristan, a country virtually unknown to Europeans since its conquest by Alexander the Great. There they will offer their services to a ruler and then help him to conquer his neighbours, but proceed to overthrow him and loot the country. Kipling, after first trying to dissuade them, gives Dravot his masonic tag as a token of brotherhood.
After signing a contract pledging mutual loyalty and forswearing drink and women, the two set off on an epic overland journey north beyond the Khyber Pass. Over the next few weeks, they travel through Afghanistan, fighting off bandits, blizzards, and avalanches as they make their way into the unknown land of Kafiristan. They chance upon a Gurkha soldier, Billy Fish, the sole survivor of a years-before British expedition. Speaking English as well as the local language, Billy smooths their way as they begin their rise, first offering their services to the chief of a much-raided village. When a force has been trained in modern weapons and tactics, they lead it out against some hated neighbours. During the battle, an arrow pierces Dravot's jacket but he is unharmed.
Both sides take him to be a god, though in fact the missile was stopped by his leather bandolier. Victory follows victory, with the defeated added to the ranks of the swelling army. Finally, nobody is left to stand in their way and they are summoned to the holy city of Sikandergul by the high priest of the region. He sets up a re-enactment of the arrow incident, to determine whether Dravot is a man or a god by seeing whether or not he bleeds. When his shirt is torn open, they are amazed to see the masonic tag around his neck. It contains the sacred symbol left by Sikander (their name for Alexander the Great), who had promised to send a son to rule over them.
Hailing Dravot as king as well as god, they show him the royal treasury, which is full of unimaginable amounts of gold and jewels that are now all his. Carnehan suggests that they leave with as much loot as they can carry as soon as the snows have melted on the mountain passes. Dravot, however, is beginning to enjoy the adulation of the locals, settling their disputes and issuing laws, and even dreams of visiting Queen Victoria as an equal. He is also struck by the beauty of a girl called Roxana, the name of Alexander's wife, and cancels their pact to avoid women, saying he will marry her in order to leave the people an heir. When she is reluctantly brought to him, he tries to kiss her, but she, terrified that the touch of a god means death to a mortal, bites his cheek. Seeing him bleed, the people realise he is only human and try to grab the English impostors.
Outnumbered in the ensuing battle and captured, Dravot is made to walk onto a rope bridge, where he lustily sings the hymn "The Son of God Goes Forth to War". When the ropes are cut, he falls thousands of feet to his death. Carnehan is crucified between two pine trees, but upon being found still alive the next morning, is freed. Crippled in body and unhinged in mind from his ordeal, he eventually makes his way back to India as a beggar. Finishing his story, he leaves Kipling's office after putting a bundle on the desk. When Kipling opens it, he finds Dravot's severed head, still wearing a golden crown.


== Cast ==


== Production ==
The Man Who Would Be King had been a pet project of John Huston's for many years after he had read the book as a child.  Huston had planned to make the film since the 1950s, originally with Clark Gable and Humphrey Bogart in the roles of Daniel and Peachy. He was unable to get the project off the ground before Bogart died in 1957; Gable followed in 1960. Burt Lancaster and Kirk Douglas were then approached to play the leads, followed by Richard Burton and Peter O'Toole. In the 1970s, Huston approached Robert Redford and Paul Newman for the roles. Newman advised Huston that British actors should play the roles, and it was he who recommended Connery and Caine. Caine was very keen to appear especially after he was told that his part had originally been written for Humphrey Bogart, his favorite actor as a young man.Christopher Plummer was cast as Rudyard Kipling as a last minute replacement for Richard Burton.The role of Roxanne (the only listed female character in the movie) was originally slated for Tessa Dahl, the daughter of Roald Dahl and actress Patricia Neal. Dahl, excited to take the role, had prepared for the part by losing weight and capping her teeth. However, at the last minute, director Huston had decided to cast someone whose appearance was more in keeping with natives of Kafiristan. "We've got to find an Arab princess somewhere", he is recounted as saying over dinner with Caine. At that same dinner, Caine's ethnic-Indian wife Shakira was present, so Huston and Caine persuaded her to take on the role.The film was shot at Pinewood Studios and at locations in France and Ouarzazate, Morocco.While on location, both Connery and Caine strongly objected to the racist treatment of Saeed Jaffrey, who had been cast to play the Gurkha guide Billy Fish.Stuntman Joe Powell doubled for Sean Connery and it was he who performed the fall from the rope bridge at the film's climax. Execution involved a potentially fatal fall of 80 feet onto a pile of cardboard boxes and mattresses. Huston was so impressed with Powell's performance he stated "That's the darndest stunt I've ever seen!" Michael Caine stated that Sean Connery did not like heights and was not fond of the final scene in which he had to walk to the middle of the bridge. Caine recalled: 'There was a day when we shooting on the rope bridge and Sean turned to John and said "Do you think the bridge looks safe?" John lowered his eyes and said, "Sean, the bridge looks the way it always has. The only difference is that today, you're going to be standing in the middle of it." ' 


== Differences from the novella ==
The movie is a generally faithful adaptation of the novella, but for some small narrative differences.  In the movie:

Kipling, a journalist, is presented as the principal narrator, only implied in the novella.
Dravot and Carnehan are shown the treasures of Alexander, not in the novella.
Billy Fish is an ex-Gurkha soldier, the sole survivor of an expedition, rather than a native chief loyal to the Englishmen.
Billy Fish interprets for the pair; in the novella, Dravot speaks the local native languages.
Dravot is smitten by Roxanne's beauty, and then decides to marry her, rather than first seeing her at their wedding.
Dravot's head is left on Kipling's desk by Carnehan; in the novella he takes it with him, and is later found insane and dying by Kipling, without the head.


== Reception ==
John Simon of New York magazine considered the film to be Huston's best work since The African Queen, twenty-three years earlier. Jay Cocks of Time commented "John Huston has been wanting to make this movie for more than twenty years. It was worth the wait."Roger Ebert gave the film four stars and wrote: "It's been a long time since there's been an escapist entertainment quite this unabashed and thrilling and fun."Some critics felt that the film was too long and that Caine had overplayed his part. A review in Variety was critical of the film mostly because of Caine's performance, stating "Whether it was the intention of John Huston or not, the tale of action and adventure is a too-broad comedy, mostly due to the poor performance of Michael Caine."This film has a 97% rating on Rotten Tomatoes from 29 reviews.Both Connery and Caine considered the movie their favorite of all they had worked on.


== Award nominations ==
The film was nominated for four Academy Awards:
Best Art Direction – Alexandre Trauner, Tony Inglis, Peter James
Best Writing – John Huston, Gladys Hill
Best Costume Design – Edith Head
Best Editing – Russell LloydMaurice Jarre was nominated for the Golden Globe Award for Best Original Score.
Head was nominated for the BAFTA Award for Best Costume Design, and Oswald Morris for the BAFTA Award for Best Cinematography.


== Music ==
Maurice Jarre scored the film and invited classical Indian musicians to participate in the recording sessions with a traditional European symphony orchestra. A key song, which figures within the plot of the movie, is a fusion of the music of the Irish song "The Minstrel Boy" with the lyrics of Reginald Heber's "The Son of God Goes Forth to War". This song is heard at key moments in the score, notably being sung by Dravot as he is being executed and as he tumbles to his death. The film's performance of The Minstrel Boy is by William Lang, late of the Black Dyke Band and the London Symphony Orchestra.


== Home media ==
The Man Who Would Be King was released by Warner Home Video on DVD in Region 1 on 19 November 1997, and was re-issued on 9 November 2010, followed by a Region A Blu-ray release on 7 June 2011. In Region 2, the film was released on DVD by Sony Pictures Home Entertainment on 27 August 2007, with a re-issue on 17 May 2010.


== See also ==
White savior narrative in film


== References ==


=== Bibliography ===


== External links ==
The Man Who Would Be King on IMDb
The Man Who Would Be King at AllMovie
The Man Who Would Be King at the TCM Movie Database
The Man Who Would Be King at the American Film Institute Catalog
The Man Who Would Be King at Box Office Mojo
The Man Who Would Be King at Rotten Tomatoes