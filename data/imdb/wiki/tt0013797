Yosemite Valley ( yoh-SEM-i-tee) is a glacial valley in Yosemite National Park in the western Sierra Nevada mountains of Central California. The valley is about 7.5 miles (12 km) long and approximately 3000–3500 feet deep, surrounded by high granite summits such as Half Dome and El Capitan, and densely forested with pines. The valley is drained by the Merced River, and a multitude of streams and waterfalls flow into it, including Tenaya, Illilouette, Yosemite and Bridalveil Creeks. Yosemite Falls is the highest waterfall in North America and is a big attraction especially in the spring, when the water flow is at its peak. The valley is renowned for its natural environment and is regarded as the centerpiece of Yosemite National Park, attracting visitors from around the world.
The valley is the main attraction in the park for the majority of visitors and a bustling hub of activity during tourist season in the summer months. Most visitors enter the valley from roads to the west and pass through the Tunnel View entrance. Visitor facilities are in the center of the valley. There are both hiking trail loops that stay within the valley and trailheads that lead to higher elevations, all of which afford glimpses of the park's many scenic wonders.


== Descriptions ==
Yosemite Valley is on the western slope of the Sierra Nevada mountains, 150 miles (240 km) due east of San Francisco. It stretches for 7.5 miles (12 km) in a roughly east–west direction, with an average width of about 1 mile (1.6 km).

Yosemite Valley represents only one percent of the park area, but this is where most visitors arrive and stay. More than half a dozen creeks tumble from hanging valleys at the top of granite cliffs that can rise 3000–3500 feet (914–1067 m) above the valley floor, which itself is 4000 ft (1219 m) above sea level.  These streams combine into the Merced River, which flows out from the western end of the valley, down the rest of its canyon to the San Joaquin Valley.  The flat floor of Yosemite Valley holds both forest and large open meadows, which have views of the surrounding crests and waterfalls.
Below is a description of these features, looking first at the walls above, moving west to east as a visitor does when entering the valley, then visiting the waterfalls and other water features, returning east to west with the flow of water.
The first view of Yosemite Valley many visitors see is the Tunnel View. So many paintings were made from a viewpoint nearby that the National Park Service named that viewpoint Artist Point. The view from the lower (western) end of the Valley contains the great granite monolith El Capitan on the left, and Cathedral Rocks on the right with Bridalveil Fall.  Just past this spot the Valley suddenly widens with the Cathedral Spires, then the pointed obelisk of Sentinel Rock to the south.  Across the Valley on the northern side are the Three Brothers, rising one above the other like gables built on the same angle – the highest crest is Eagle Peak, with the two below known as the Middle and Lower Brothers.

To this point the Valley has been curving gently to the left (north).  Now a grand curve back to the right begins, with Yosemite Falls on the north, followed by the Royal Arches, topped by North Dome.  Opposite, to the south, is Glacier Point, 3,200 feet (975 m) above the Valley floor. At this point the Valley splits in two, one section slanting northeast, with the other curving from south to southeast. Between them, at the eastern end of the valley, is Half Dome, among the most prominent natural features in the Sierra Nevada.  Above and to the northeast of Half Dome is Clouds Rest; at 9926 feet (3025 m), the highest point around Yosemite Valley.


== Water ==
Snow melting in the Sierra forms creeks and lakes.  In the surrounding region, these creeks flow to the edge of the Valley to form cataracts and waterfalls.
A fan of creeks and forks of the Merced River take drainage from the Sierra crest and combine at Merced Lake.  The Merced then flows down to the end of its canyon (Little Yosemite Valley), where it begins what is often called the Giant Staircase.  The first drop is Nevada Fall, which drops 594 feet (181 m), bouncing off the granite slope below it.  Below is Vernal Fall, 317 feet (97 m) high, one of the most picturesque waterfalls in the Valley.  The Merced then descends rapids to meet Illilouette Creek, which drops from the valley rim to form Illilouette Fall.  They combine at the base of the gorges that contain each stream, and then flow around the Happy Isles to meet Tenaya Creek at the eastern end of Yosemite Valley proper.
Tenaya Creek flows southwest from Tenaya Lake and down Tenaya Canyon, finally flowing between Half Dome and North Dome before joining the Merced River.  The following falls tumble from the Valley rim to join it at various points:

Yosemite Falls 2,425 feet (739 m) Upper Yosemite Fall 1,430 feet (440 m), the middle cascades 670 feet (200 m), and Lower Yosemite Fall 320 feet (98 m). (Yosemite Creek)
Snow Creek Falls 2,140 feet (650 m)
Sentinel Falls 1,920 feet (590 m)
Ribbon Fall 1,612 feet (491 m)
Royal Arch Cascade 1,250 feet (380 m)
Lehamite Falls 1,180 feet (360 m)
Staircase Falls 1,020 feet (310 m)
Bridalveil Fall 620 feet (190 m). (Bridalveil Creek)
Nevada Fall 594 feet (181 m)
Silver Strand Falls 574 feet (175 m)
Vernal Fall 318 feet (97 m)


== Natural Yosemite Valley ==


=== Geology ===

The features in Yosemite Valley are made of granitic rock emplaced as plutons miles deep during the late Cretaceous. Over time the Sierra Nevada was uplifted, exposing this rock to erosion at the surface.
The oldest of these granitic rocks, at 114 million years, occur along the Merced River Gorge west of the valley. The El Capitan pluton intruded the valley, forming most of the granitic rock that makes up much of the central part of the valley, including Cathedral Rocks, Three Brothers, and El Capitan. The youngest Yosemite Valley pluton is the 87-million-year-old Half Dome granodiorite, which makes up most of the rock at Glacier Point, the Royal Arches, and its namesake, Half Dome.

For the last 30 million years, glaciers have periodically filled much of the valley. The most current glaciation, the Wisconsinian was not, however, the most severe. Ice ages previous to the Wisconsinian were colder and lasted longer. Their glaciers were huge and covered nearly all the landmarks around Yosemite Valley except Half Dome, Eagle Peak, Sentinel Dome, and the top of El Capitan. Wisconsinian glaciers, however, only reached Bridalveil Fall in the valley. The glaciers widened the valley, but much of its width is in fact due to previous stream erosion and mass wasting along vertical joints in the valley's walls.
After the retreat of many of these glaciers, a stand of Ancient Lake Yosemite developed. The valley floor owes its flatness to sediment deposited by these stands (the last glaciers in the valley were small and did not remove much old lake sediment). The last stand of Lake Yosemite was about 5.5 miles (8.9 km) long and was impounded by a terminal moraine near the base of El Capitan. It was later filled by sediment, becoming a swampy meadow.
The parallel Tenaya Canyon and Little Yosemite Canyon glaciers were, at their largest, 2,000 feet (600 m) deep where they flowed into the Yosemite Valley near the base of Half Dome. They also formed Cloud's Rest behind Half Dome as an arête.
Near Glacier Point there are 2,000 feet (600 m) of mostly glacial sediment with at least six separate sequences of Lake Yosemite sediments. Here, huge and highly erosive pre-Wisconsinian glaciers excavated the bedrock valley floor, and much smaller Wisconsinian glaciers deposited glacial debris.


=== Ecology ===

The biological community on the floor of Yosemite Valley is a diverse one, with more than 400 species of grasses and wildflowers and thousands of species of insects having been identified there.  At the most general level, the valley can be classified as a dry yellow pine forest with a number of large open meadows. Plant and animal species that make up a significant part of this natural community include:

Trees – ponderosa pine, lodgepole pine, sugar pine, white fir, incense-cedar, California black oak, interior live oak, coast Douglas-fir, California laurel, bigleaf maple, Scouler's willow, Pacific dogwood, white alder, western balsam poplar
Shrubs – whiteleaf manzanita, mountain misery, western azalea, American dogwood, buckbrush, deer brush, sierra gooseberry
Wildflowers – Indian pink, soap plant, California poppy, miner's lettuce, purple Chinese houses, purple milkweed, Pacific starflower, western buttercup, pineapple weed
Mammals – California ground squirrel, western gray squirrel, chickaree, mule deer, American black bear, bobcat, coyote
Birds – dark-eyed junco, mountain chickadee, black-headed grosbeak, white-headed woodpecker, Steller's jay, American dipper, common raven
Reptiles – Gilbert's skink, northern alligator lizard, rattlesnake
Amphibians – Sierra Nevada salamander


== Tourism ==
Yosemite National Park had a record number of 5 million visitors in 2016.On July 24, 2018, several areas of the park, including the Valley, were closed due to wildfires in the area; Officials expected the Valley area to reopen on August 7 but Wawona, Mariposa Grove and the South Entrance would remain closed for an indefinite period.


=== Hiking ===

Several trails lead out of the Valley, including

The John Muir Trail --- running 211 miles (340 km) to Mount Whitney
The Mist Trail --- with views of Vernal Fall and Nevada Fall
The Four Mile Trail --- leading to Glacier Point.
The Yosemite Falls Trail --- to the top of Yosemite Falls


=== Climbing ===
Yosemite is now a world rock climbing attraction.  The massive 'big walls' of granite have been climbed countless times since the 1950s and have pushed climbers' abilities to new heights.  While climbers traditionally take several days to climb the monoliths, bivvying on the rock faces, modern climbing techniques help climbers ascend the cliffs in mere hours.  Many climbers stay at Camp 4 before beginning big wall climbs.
Half Dome figures prominently on the reverse side of the California state quarter. Hiking to the top of Half Dome is one of the most popular hikes from the valley, and very crowded. The park now requires permits to use the trail, and in 2011 the permits sold out very quickly after becoming available. The park now uses a lottery system for hikers to apply for permits.


== History ==


=== Native Americans in Yosemite ===

Habitation of the Yosemite Valley proper can be traced to about 3,000 years ago when vegetation and game in the region was similar to that present today; the western slopes of the Sierra Nevada had acorns, deer, and salmon, while the eastern Sierra had pinyon nuts and obsidian.
The prehistory of the area is divided into three cultural phases on archaeological grounds, the "Crane Flat" phase, (1000 BCE to 500 CE)  is marked by hunting with the atl atl and the use of grinding stones., the "Tarmarack" phase (500 to 1200 CE), marked by a shift to using smaller rock points, indicating development and use of the bow and arrow, and the "Mariposa", from 1200 until European contact in the mid-19th century.In the 19th century, it was inhabited by a Miwok band  who called the Valley "Ah-wah-nee" and themselves the Ahwahnechee.  
This group had trading and family ties to Mono Lake Paiutes from the eastern side of the Sierra Nevada. They annually burned the vegetation on the Valley floor, which promoted the black oak and kept the meadows and forests open. This protected the supply of their principal food, acorns, and reduced the chance of ambush. At the time of first European contact, this band was led by Chief Tenaya (Teneiya), who was raised by his mother among the Mono Lake Paiutes.


=== The Mariposa Battalion and the first tourists ===
The first non-Native Americans to see Yosemite Valley were probably members of the 1833 Joseph Walker Party, which was the first to cross the Sierra Nevada from east to west. The first descriptions of Yosemite, however, came nearly 20 years later. The 1849 California Gold Rush led to conflicts between miners and Native Americans, and the state formed the volunteer Mariposa Battalion as a punitive expedition against the Native Americans living in the Yosemite area.  In 1851, the Battalion was led by Major Jim Savage, whose trading post on the Merced River the Awaneechee had raided.  This and other missions (the Mariposa Wars) resulted in Chief Teneiya and the Awaneechee spending months on a reservation in the San Joaquin Valley.  The band returned the next year to the Valley but took refuge among the Mono Paiutes after further conflicts with miners.  Most of the Awaneechee (along with Teneiya) were chased back to the Valley and killed by the Paiutes after violating hospitality by stealing horses.
While the members of that first expedition of the Mariposa Battalion had heard rumors of what could be found up the Merced River, none were prepared for what they saw March 27, 1851, from what is now called Old Inspiration Point (close to the better-visited Tunnel View).  Dr. Lafayette Bunnell later wrote:

The grandeur of the scene was but softened by the haze that hung over the valley -- light as gossamer -- and by the clouds which partially dimmed the higher cliffs and mountains. This obscurity of vision but increased the awe with which I beheld it, and as I looked, a peculiar exalted sensation seemed to fill my whole being, and I found my eyes in tears with emotion.
Camping that night on the Valley floor, the group agreed with the suggestion of Dr. Bunnell to call it "Yo-sem-i-ty", mistakenly believing that was the native name. The term is from the Southern Sierra Miwok word Yohhe'meti, meaning "they are killers," which referred to the inhabitants of the place .  Bunnell was also the first of many to underestimate the height of the Valley walls. A San Francisco newspaper demanded that he cut his estimate of the rim height—at 1500 feet (450 m), already under by half—in half again before they would publish it.

James Hutchings—who organized the first tourist party to the Valley in 1855—and artist Thomas Ayers generated much of the earliest publicity about Yosemite, creating articles and entire magazine issues about the Valley. Ayres' highly detailed angularly exaggerated artwork and his written accounts were distributed nationally and an art exhibition of his drawings was held in New York City.
Two of Hutchings' first group of tourists, Milton and Houston Mann, built the first toll route into the valley, with the development of the first hotels in the area and other trails quickly following.  Orchards were planted and livestock grazed in Valley meadows, with damage to native ecosystems as the result.


=== Yosemite: The first park ===

The work of Ayres gave easterners an appreciation for Yosemite Valley and started a movement to preserve it. Influential figures such as Galen Clark, clergyman Thomas Starr King and leading landscape architect Frederick Law Olmsted were among those who urged Senator John Conness of California to try to preserve Yosemite.President Abraham Lincoln signed a bill on June 30, 1864 granting Yosemite Valley and the Mariposa Grove of giant sequoias to the State of California "for public use, resort and recreation," the two tracts "shall be inalienable for all time". This was the first time in history that a federal government had set aside scenic lands simply to protect them and to allow for their enjoyment by all people.
Simply designating an area a park isn't sufficient to protect it.  California did not set up an administration for the park until 1866 when the state-appointed Galen Clark as the park's guardian.  An 11-year struggle followed to resolve homesteading claims in the valley.  The challenge of increasing tourism, with the need to first build stagecoach roads, then the Yosemite Valley Railroad, along with hotels and other facilities in and around the Valley was met during the rest of the 19th century.  But much environmental damage was caused to the valley itself at that time.  The problems that Yosemite Park had under state control was one of the factors in establishing Yellowstone National Park as the first completely national park in 1872.

Due to the difficulty of traveling there, early visitors to the valley came for several weeks to a couple of months, often as entire families with many possessions. Early hotels were therefore set up for extended stays and catered primarily to wealthy patrons who could spend extended periods away from home. One of these hotels—the Wawona Hotel, built in the 1880s—still operates.
After the Valley became a park, the surrounding territory was still subject to logging, mining, and grazing.  John Muir publicized the damage to the subalpine meadows that surround the Valley and in 1890, the government created a national park that included a much larger territory—enclosing Yosemite Valley and Mariposa Grove.


=== 20th century ===

As with Yellowstone, the new federal park was under U.S. Army jurisdiction until 1914.  In 1906, the state ceded the Valley and Mariposa Grove to the federal government.  The National Park Service, on its creation in 1916, took over Yosemite.

Yosemite Valley is listed as a National Historic District and as a California Historical Landmark. After the creation of the Park Service, many separate hotel owners held separate concession contracts. The Yosemite Park Company had built the Yosemite Lodge and Yosemite Village had its own group of merchants. Fire had destroyed a number of the original valley hotels and concession owners came and went until Park Service forced the two largest companies to merge in order that one single concession contract could be given. In 1925 the two family-run companies became the Yosemite Park and Curry Company and went on to build and run the Ahwahnee Hotel as the company headquarters for years, introducing a number of traditions, including the Bracebridge dinner.
Curry Village was the site from where villagers and visitors watched the Yosemite Firefall. This "fall" was large batches of red hot embers dropped from Glacier Point. The Park Service stopped this practice in 1969 as part of their long process of de-emphasizing artificial park attractions.
On July 6, 1996, a massive rock slide, weighing an estimated 60,000–80,000 tons, crashed 1800 feet (550 m) into the valley from the east side of Glacier Point, traveling at over 160 mph (260 km/h). Dust blanketed that part of the valley for days, and the wind speed in front of the slide is estimated to have been 300 mph (480 km/h). One person was killed in the slide.


=== Merced River Plan ===
In 1987 Congress designated 122 miles of the Merced as a Wild and Scenic River. Yosemite National Park contains 81 of these miles, and the valley contains 8 of those miles. This designation will "... preserve the Merced River in free-flowing condition and to protect the water quality and the outstandingly remarkable values (ORVs) that make the river worthy of designation."In March 2014, the park system released the Merced Wild and Scenic River Comprehensive Management Plan/EIS to address the preservation of the river, safety, and to improve the visitor experience in the park. The plan will restore meadows and river bank areas and remove non-essential roads. Camping capacity will increase by 37%, and recreational services will be expanded. The plan calls for an 8% increase in parking for day use visitors to Yosemite Valley, including a new 300-car parking lot. The plan will allow the valley to accommodate a peak of 20,100 visitors per day.The plan has been criticized for prioritizing park visitors over the preservation of the river and the valley. Some believe there should be further limitations to the number of cars and parking lots in the valley, and more focus on public transportation. On busy summer days there can be long delays and traffic gridlock at the entrance to Yosemite.


== See also ==
Geology of the Yosemite area
History of the Yosemite area
Julia Parker (basketmaker)
Yosemite West, California


== Notes ==


== References ==
Schoenherr, Allan A. (1995). A Natural History of California. UC Press. ISBN 0-520-06922-6.CS1 maint: ref=harv (link)
Bunnell, Lafayette (1892). The Discovery of the Yosemite (3rd ed.). New York: F.H. Revell Company. ISBN 0-8369-5621-4.CS1 maint: ref=harv (link)
Harris, Ann G.; Tuttle, Esther; Tuttle, Sherwood D. (1997). Geology of National Parks (5th ed.). Iowa: Kendall/Hunt Publishing. ISBN 0-7872-5353-7.CS1 maint: ref=harv (link)
Wuerthner, George (1994). Yosemite: A Visitors Companion. Stackpole Books. ISBN 0-8117-2598-7.CS1 maint: ref=harv (link)
Schaffer, Jeffrey P. (1999). Yosemite National Park: A Natural History Guide to Yosemite and Its Trails. Berkeley: Wilderness Press. ISBN 0-89997-244-6.CS1 maint: ref=harv (link)
Roper, Steve (1994). Camp 4: Recollections of a Yosemite Rockclimber. The Mountaineers. ISBN 0-89886-587-5.CS1 maint: ref=harv (link)
Clarke, W. A. (August 1902). "Automobiling In The Yosemite Valley". Overland Monthly, and Out West Magazine. XL (2): 104–110. Retrieved 2009-08-15. 


== External links ==
 Yosemite National Park travel guide from Wikivoyage
The Geologic Story of Yosemite Valley by N. King Huber (USGS, 1987) authoritative and up-to-date summary of Yosemite's geology
Origin of Yosemite Valley, Chapter 4, "Glaciers of California", by Bill Guyton
Historic Yosemite Indian Chiefs – with photos
Daily updating time-lapse movies of Yosemite Valley
Yosemite Extreme Panoramic Imaging Project aiming at stitching 10,000 high resolution images