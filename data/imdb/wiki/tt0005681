Sister Elizabeth Barton (1506 – 20 April 1534), known as "The Nun of Kent", "The Holy Maid of London", "The Holy Maid of Kent" and later "The Mad Maid of Kent", was an English Catholic nun. She was executed as a result of her prophecies against the marriage of King Henry VIII of England to Anne Boleyn.


== Early life ==
Little is known about Barton's early life.  She was born in 1506 in the parish of Aldington, about twelve miles from Canterbury, and she appears to have come from a poor background.  She was working as a servant when her visions began in 1525.


== Visions ==
On Easter of 1525, at the age of 19, while working as a domestic servant in the household of Thomas Cobb, a farmer of Aldington, Barton suffered from a severe illness and claimed to have received divine revelations that predicted events, such as the death of a child living in her household or, more frequently, pleas for people to remain in the Roman Catholic Church. She also urged people to pray to the Blessed Virgin Mary and to undertake pilgrimages. Thousands believed in her prophecies and both Archbishop William Warham and Bishop John Fisher attested to her pious life.When some events that Barton foretold apparently happened, her reputation spread. Barton's revelations became publicly known and matters were brought up by Archbishop William Warham. The parish priest, Richard Masters, referred the matter to Warham, who appointed a commission to ensure that none of her prophecies were at variance with Catholic teaching. This commission was led by the Benedictine monk, Edward Bocking, Barton's spiritual advisor. The commission decided favorably, Warham arranged for Barton to be received in the Benedictine St Sepulchre's Priory, Canterbury.Barton's life became very public, nothing unorthodox was found in her case and her public healing from the Virgin Mary at Court-at-Street increased attention and gave fame to the Holy Maid of Kent and the Marian Shrine. Barton became a nun, known as the Holy Maid of Kent at the Benedict house of St. Sepulchre' Priory, Canterbury under Bocking's spiritual direction.In 1527, Robert Redman published A marueilous woorke of late done at Court of Streete in Kent which discussed all of Barton's "miracles, revelations, and prophecies" and the controversies leading up to the arrests and executions. In 1528, Barton held a private meeting with Cardinal Thomas Wolsey, the second most powerful man in England after Henry VIII, and she soon thereafter met twice with Henry himself. Henry accepted Barton because her prophecies then still supported the existing order. She also consulted with Richard Reynolds, a Birgittine monk of Syon Abbey. He arranged a meeting between Barton and Thomas More, who was impressed by her fervour. Her prophecies warned against heresy and condemned rebellion at a time when Henry was attempting to stamp out Lutheranism and was afraid of possible uprising or even assassination by his enemies.However, when the King began the process of obtaining an annulment of his marriage to Catherine of Aragon and seizing control of the Church in England from Rome, Barton turned against him. Barton strongly opposed the English Reformation and, in around 1532, began prophesying that if Henry remarried, he would die within a few months. She said that she had even seen the place in Hell to which he would go (Henry actually lived for a further 15 years).Remarkably, probably because of her popularity, Barton went unpunished for nearly a year. The King's agents spread rumours that she was engaged in sexual relationships with priests and that she suffered from mental illness. Many prophecies, as Thomas More thought, were fictitiously attributed to her.


== Arrest and execution ==
With her reputation undermined, the Crown arrested Barton in 1533 and forced her to confess that she had fabricated her revelations. However, all that is known regarding her confession comes from Thomas Cromwell, his agents and other sources on the side of the Crown.
Friar John Laurence of the Observant Friars of Greenwich gave evidence against Barton and against fellow Observants, Friars Hugh Rich and Richard Risby. Laurence then requested to be named to one of the posts left vacant by their imprisonment. She was condemned by a bill of attainder (25 Henry VIII, c. 12); an Act of Parliament authorizing punishment without trial.
Barton was attainted for treason by Parliamentary Act, on the basis that she had maliciously opposed Henry VIII's divorce from Catherine of Aragon, and had prophesied that the King would lose his kingdom. Although Barton claimed God had revealed to her that he no longer recognized Henry VIII's monarchy, the Act of Attainder argued that Barton was at the centre of a conspiracy against the King. Barton was viewed as a false prophet who was encouraged to profess fake revelations to persuade others to go against the monarchy.On 20 April 1534 Elizabeth Barton was hanged at Tyburn for treason. She was 28 years old. Five of her chief supporters were executed alongside her:

Edward Bocking, Benedictine monk of Christ Church, Canterbury
John Dering, Benedictine monk;
Henry Gold, priest;
Hugh Rich, Franciscan friar;
Richard Risby, Franciscan friar.Barton was buried at Greyfriars Church in Newgate, but her head was put on a spike on London Bridge, the only woman in history dealt that dishonour.


== Legacy ==
Churches such as the Anglican Catholic Church of St Augustine of Canterbury continue to venerate Barton.


== Popular culture ==
Barton's case is dealt with in the 2009 historical novel Wolf Hall by Hilary Mantel, and in its television adaptation, where she is played by Aimee-Ffion Edwards. Barton and her prophecies are also mentioned in Philippa Gregory's 2014 novel The King's Curse; the sixth and final book in The Cousins' War series.
In A Man for all Seasons, Barton is referred to during the interrogation of Thomas More as having been executed (Barton was executed about 15 months before More).


== References ==


== Bibliography ==
McKee, John (1925), Dame Elizabeth Barton OSB, the Holy Maid of Kent, London: Burns, Oates and Washbourne.
Neame, Alan (1971), The Holy Maid of Kent: The Life of Elizabeth Barton: 1506–1534, London: Hodder and Stoughton, ISBN 978-0-340-02574-1.
Shagan, Ethan H (2003), "Chapter 2: The Anatomy of opposition in early Reformation England; the case of Elizabeth Barton, the holy maid of Kent", Popular Politics in the English Reformation, Cambridge, UK: Cambridge University Press, pp. 61–88.
Watt, Diane (1997), Secretaries of God, Cambridge, UK: DS Brewer.


== External links ==
Watt, Diane (2004), "Barton, Elizabeth (c. 1506–1534)", Dictionary of National Biography, Oxford, doi:10.1093/ref:odnb/1598.