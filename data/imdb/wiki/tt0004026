In Jewish folklore, a golem ( GOH-ləm; Hebrew: גולם‎) is an animated anthropomorphic being that is created entirely from inanimate matter (usually clay or mud). The word was used to mean an amorphous, unformed material in Psalms and medieval writing.The most famous golem narrative involves Judah Loew ben Bezalel, the late-16th-century rabbi of Prague. Many tales differ on how the golem was brought to life and afterward controlled. According to Moment Magazine, "the golem is a highly mutable metaphor with seemingly limitless symbolism. It can be a victim or villain, Jew or non-Jew, man or woman—or sometimes both. Over the centuries it has been used to connote war, community, isolation, hope, and despair."


== Etymology ==
The word golem occurs once in the Bible in Psalm 139:16, which uses the word גלמי (golmi; my golem), that means "my light form", "raw" material, connoting the unfinished human being before God's eyes. The Mishnah uses the term for an uncultivated person: "Seven characteristics are in an uncultivated person, and seven in a learned one," (שבעה דברים בגולם) (Pirkei Avot 5:10 in the Hebrew text; English translations vary). In Modern Hebrew, golem is used to mean "dumb" or "helpless". Similarly, it is often used today as a metaphor for a mindless lunk or entity who serves a man under controlled conditions but is hostile to him under others. "Golem" passed into Yiddish as goylem to mean someone who is lethargic or beneath a stupor.


== History ==


=== Earliest stories ===
The oldest stories of golems date to early Judaism. In the Talmud (Tractate Sanhedrin 38b), Adam was initially created as a golem (גולם) when his dust was "kneaded into a shapeless husk." Like Adam, all golems are created from mud by those close to divinity, but no anthropogenic golem is fully human. Early on, the main disability of the golem was its inability to speak. Sanhedrin 65b describes Rava creating a man (gavra). He sent the man to Rav Zeira. Rav Zeira spoke to him, but he did not answer. Rav Zeira said, "You were created by the sages; return to your dust".
During the Middle Ages, passages from the Sefer Yetzirah (Book of Creation) were studied as a means to create and animate a golem, although there is little in the writings of Jewish mysticism that supports this belief. It was believed that golems could be activated by an ecstatic experience induced by the ritualistic use of various letters of the Hebrew Alphabet forming a "shem" (any one of the Names of God), wherein the shem was written on a piece of paper and inserted in the mouth or in the forehead of the golem.A golem is inscribed with Hebrew words in some tales (for example, some versions of Chełm and Prague, as well as in Polish tales and versions of Brothers Grimm), such as the word emet (אמת, "truth" in Hebrew) written on its forehead. The golem could then be deactivated by removing the aleph (א) in emet, thus changing the inscription from "truth" to "death" (met מת, meaning "dead"). 
Rabbi Jacob Ben Shalom arrived at Barcelona from Germany in 1325 and remarked that the law of destruction is the reversal of the law of creation.One source credits 11th century Solomon ibn Gabirol with creating a golem, possibly female, for household chores.Joseph Delmedigo informs us in 1625 that "many legends of this sort are current, particularly in Germany."The earliest known written account of how to create a golem can be found in Sodei Razayya by Eleazar ben Judah of Worms of the late 12th and early 13th century.


=== The Golem of Chełm ===

The oldest description of the creation of a golem by a historical figure is included in a tradition connected to Rabbi Eliyahu of Chełm (1550–1583).A Polish Kabbalist, writing in about 1630–1650, reported the creation of a golem by Rabbi Eliyahu thus: "And I have heard, in a certain and explicit way, from several respectable persons that one man [living] close to our time, whose name is R. Eliyahu, the master of the name, who made a creature out of matter [Heb. Golem] and form [Heb. tzurah] and it performed hard work for him, for a long period, and the name of emet was hanging upon his neck until he finally removed it for a certain reason, the name from his neck and it turned to dust." A similar account was reported by a Christian author, Christoph Arnold, in 1674.
Rabbi Jacob Emden (d. 1776) elaborated on the story in a book published in 1748: "As an aside, I'll mention here what I heard from my father's holy mouth regarding the Golem created by his ancestor, the Gaon R. Eliyahu Ba'al Shem of blessed memory. When the Gaon saw that the Golem was growing larger and larger, he feared that the Golem would destroy the universe. He then removed the Holy Name that was embedded on his forehead, thus causing him to disintegrate and return to dust. Nonetheless, while he was engaged in extracting the Holy Name from him, the Golem injured him, scarring him on the face."
According to the Polish Kabbalist, "the legend was known to several persons, thus allowing us to speculate that the legend had indeed circulated for some time before it was committed to writing and, consequently, we may assume that its origins are to be traced to the generation immediately following the death of R. Eliyahu, if not earlier."


=== The classic narrative: The Golem of Prague ===

The most famous golem narrative involves Judah Loew ben Bezalel, the late 16th century rabbi of Prague, also known as the Maharal, who reportedly "created a golem out of clay from the banks of the Vltava River and brought it to life through rituals and Hebrew incantations to defend the Prague ghetto from anti-Semitic attacks" and pogroms. Depending on the version of the legend, the Jews in Prague were to be either expelled or killed under the rule of Rudolf II, the Holy Roman Emperor. The Golem was called Josef and was known as Yossele. It was said that he could make himself invisible and summon spirits from the dead. Rabbi Loew deactivated the Golem on Friday evenings by removing the shem before the Sabbath (Saturday) began, so as to let it rest on Sabbath. One Friday evening Rabbi Loew forgot to remove the shem, and feared that the Golem would desecrate the Sabbath. A different story tells of a golem that fell in love, and when rejected, became the violent monster seen in most accounts. Some versions have the golem eventually going on a murderous rampage.The rabbi then managed to pull the shem from his mouth and immobilize him in front of the synagogue, whereupon the golem fell in pieces. The Golem's body was stored in the attic genizah of the Old New Synagogue, where it would be restored to life again if needed. According to legend, the body of Rabbi Loew's Golem still lies in the synagogue's attic. When the attic was renovated in 1883, no evidence of the Golem was found. Some versions of the tale state that the Golem was stolen from the genizah and entombed in a graveyard in Prague's Žižkov district, where the Žižkov Television Tower now stands. A recent legend tells of a Nazi agent ascending to the synagogue attic during World War II and trying to stab the Golem, but he died instead. The attic is not open to the general public.Some Orthodox Jews believe that the Maharal did actually create a golem. The evidence for this belief has been analyzed from an Orthodox Jewish perspective by Shnayer Z. Leiman.


=== Sources of the Prague narrative ===
The general view of historians and critics is that the story of the Golem of Prague was a German literary invention of the early 19th century. According to John Neubauer, the first writers on the Prague Golem were:

1837: Berthold Auerbach, Spinoza
1841: Gustav Philippson, Der Golam, eine Legende
1841: Franz Klutschak, Der Golam des Rabbi Löw
1842: Adam Tendlau Der Golem des Hoch-Rabbi-Löw
1847: Leopold Weisel, Der GolemHowever, there are in fact a couple of slightly earlier examples, in 1834 and 1836.All of these early accounts of the Golem of Prague are in German by Jewish writers. It has been suggested that they emerged as part of a Jewish folklore movement parallel with the contemporary German folklore movement.The origins of the story have been obscured by attempts to exaggerate its age and to pretend that it dates from the time of the Maharal. It has been said that Rabbi Yudel Rosenberg (1859–1935) of Tarłów (before moving to Canada where he became one of its most prominent rabbis) originated the idea that the narrative dates from the time of the Maharal. Rosenberg published Nifl'os Maharal (Wonders of Maharal) (Piotrków, 1909) which purported to be an eyewitness account by the Maharal's son-in-law, who had helped to create the Golem. Rosenberg claimed that the book was based upon a manuscript that he found in the main library in Metz. Wonders of Maharal "is generally recognized in academic circles to be a literary hoax". Gershom Sholem observed that the manuscript "contains not ancient legends but modern fiction". Rosenberg's claim was further disseminated in Chayim Bloch's (1881–1973) The Golem: Legends of the Ghetto of Prague (English edition 1925).
The Jewish Encyclopedia of 1906 cites the historical work Zemach David by David Gans, a disciple of the Maharal, published in 1592. In it, Gans writes of an audience between the Maharal and Rudolph II: "Our lord the emperor ... Rudolph ... sent for and called upon our master Rabbi Low ben Bezalel and received him with a welcome and merry expression, and spoke to him face to face, as one would to a friend. The nature and quality of their words are mysterious, sealed and hidden." But it has been said of this passage, "Even when [the Maharal is] eulogized, whether in David Gans' Zemach David or on his epitaph …, not a word is said about the creation of a golem. No Hebrew work published in the 16th, 17th, and 18th centuries (even in Prague) is aware that the Maharal created a golem." Furthermore, the Maharal himself did not refer to the Golem in his writings. Rabbi Yedidiah Tiah Weil (1721–1805), a Prague resident, who described the creation of golems, including those created by Rabbis Avigdor Kara of Prague (died 1439) and Eliyahu of Chelm, did not mention the Maharal, and Rabbi Meir Perils' biography of the Maharal published in 1718 does not mention a golem.


== The Golem of Vilna ==
There is a similar tradition relating to the Vilna Gaon or "the saintly genius from Vilnius" (1720–1797). Rabbi Chaim Volozhin (Lithuania 1749–1821) reported in an introduction to Sifra de Tzeniuta that he once presented to his teacher, the Vilna Gaon, ten different versions of a certain passage in the Sefer Yetzira and asked the Gaon to determine the correct text. The Gaon immediately identified one version as the accurate rendition of the passage. The amazed student then commented to his teacher that, with such clarity, he should easily be able to create a live human. The Gaon affirmed Rabbi Chaim's assertion and said that he once began to create a person when he was a child, under the age of 13, but during the process, he received a sign from Heaven ordering him to desist because of his tender age.


== Hubris theme ==

The existence of a golem is sometimes a mixed blessing. Golems are not intelligent, and if commanded to perform a task, they will perform the instructions literally. In many depictions, Golems are inherently perfectly obedient. In its earliest known modern form, the Golem of Chełm became enormous and uncooperative. In one version of this story, the rabbi had to resort to trickery to deactivate it, whereupon it crumbled upon its creator and crushed him. There is a similar hubris theme in Frankenstein, The Sorcerer's Apprentice, and some other stories in popular culture, such as The Terminator. The theme also manifests itself in R.U.R. (Rossum's Universal Robots), Karel Čapek's 1921 play which coined the term robot; the play was written in Prague, and while Čapek denied that he modeled the robot after the Golem, there are many similarities in the plot.


== Culture of the Czech Republic ==
The Golem is a popular figure in the Czech Republic. There are several restaurants and other businesses whose names make reference to the creature, a Czech strongman (René Richter) goes by the nickname "Golem", and a Czech monster truck outfit calls itself the "Golem Team."Abraham Akkerman preceded his article on human automatism in the contemporary city with a short satirical poem on a pair of golems turning human.


== Clay Boy variation ==
A Yiddish and Slavic folktale is the Clay Boy, which combines elements of the Golem and The Gingerbread Man, in which a lonely couple makes a child out of clay, with disastrous or comical consequences. In one common Russian version, an older couple, whose children have left home, makes a boy out of clay and dries him by their hearth. The Clay Boy comes to life; at first, the couple is delighted and treats him like a real child, but the Clay Boy does not stop growing and eats all their food, then all their livestock, and then the Clay Boy eats his parents. The Clay Boy rampages through the village until he is smashed by a quick-thinking goat.


== Golem in popular culture ==


=== Science ===
The Golem: What You Should Know About Science, by Harry Collins and Trevor Pinch, is a book first published in 1993. It posits that science is neither inherently good or evil and it is therefore down to its human masters to steer science in the right direction. Through the use of a series of case studies, it explores the nature and resolution of controversy in cutting-edge scientific discovery.


=== Literature ===
Mainstream European society adopted the golem in the early 20th century. Most notably, Gustav Meyrink's 1914 novel Der Golem is loosely inspired by the tales of the golem created by Rabbi Loew. Another famous treatment from the same era is H. Leivick's 1921 Yiddish-language "dramatic poem in eight sections", The Golem. Nobel Prize winner Isaac Bashevis Singer also wrote a version of the legend, and Elie Wiesel wrote a children's book on the legend.
Marge Piercy's 1991 Arthur C. Clarke Award-winning novel He, She and It features a substantial subplot that retells the story of Rabbi Loew and his golem.
In the 1992 fantasy short story "In This Season" by Harry Turtledove, a golem named "Emes" helps three Jewish families escape from Puck, Poland during the week of Hanukkah shortly after the start of World War II in 1939.
The novels of Terry Pratchett in the fictional setting of Discworld also include several golems as characters. They are introduced in the 19th Discworld novel Feet of Clay (1996). All of them are named for Yiddish words and are allotted "holy days" to rest (a reference to the rabbi's care for the Sabbath). The golems are frequently used to explore themes of free will, self-determination, and the meaning and value of personhood.
In The Wheel of Time series by Robert Jordan, a creature called a "gholam", a superhuman assassin animated by magic, appears throughout. Gholams are bound to a single master and obey their commands, yet the gholam depicted in the story also begins to exhibit individual personality and desires.
In Cynthia Ozick's 1997 novel The Puttermesser Papers, a modern Jewish woman, Ruth Puttermesser, creates a female golem out of the dirt in her flowerpots to serve as the daughter she never had. The golem helps Puttermesser become elected Mayor of New York before it begins to run out of control.
Kyt Wright's 2020 novella The Journals of Professor Guthridge has a golem as a plot device.
Pete Hamill's 1997 novel Snow in August includes a story of a rabbi from Prague who has a golem.
Michael Chabon's 2000 novel, The Amazing Adventures of Kavalier & Clay, features one of the protagonists, escape artist Josef Kavalier, smuggling himself out of Prague along with the Golem. Petrie describes the theme of escape in the novel, culminating in Kavalier's drawing of a modern graphic novel centered on a golem.
Ted Chiang's a short story, "Seventy-Two Letters", published in 2000, explores the connection between a golem and the name that animates it.
In the Michael Scott novel The Alchemyst, the immortal Dr. John Dee attacked Nicholas Flamel with two golems, which, along with being made of mud, each had a pair of shiny stone "eyes".
Helene Wecker's 2013 novel The Golem and the Jinni features an intelligent female golem created by a malevolent Jewish magician in Danzig in 1899. Designed to be her master's bride, the golem becomes masterless when he dies of appendicitis aboard a ship to New York. Eventually named Chava, she is taken under the wing of a Hasidic rabbi and befriends a jinni, who has recently been liberated after being trapped by a wizard for a thousand years.
Jonathan Stroud's children's fantasy book The Golem's Eye centers on a golem created by magicians in an alternative London. The story depicts the golem as being impervious to magical attacks. The golem is finally destroyed by removing the parchment of creation from its mouth.
In Byron L. Sherwin's 2006 novel The Cubs and the Kabbalist, rabbis create a golem named Sandy Greenberg to help the Chicago Cubs win the World Series.
In Neil Gaiman's American Gods, on the way to the climax a "small, dark-bearded man with a dusty black derby on his head, curling payees at his temples" walks ahead of his companion, "who was twice his height and was the blank gray color of good Polish clay: the word inscribed on his forehead meant truth." This is a clear reference to the mythological Golem of Prague.
Jonathan Kellerman and Jesse Kellerman's 2014 novel The Golem of Hollywood combines the stories of Los Angeles detective Jacob Lev and the life of a golem from the golem's point of view.
In Patricia Briggs' 2017 novel Silence Fallen, the main protagonist Mercedes Thompson Hauptman awakes the spirit of the Golem of Prague, which purges the city of vampires.
In Alice Hoffman's 2019 novel The World That We Knew set in World War II Germany and France, a mother pays a rabbi's daughter (who has learned the secret ritual to creating a golem from eavesdropping on her father) to create a golem to protect her young daughter as she sends her away from Berlin to Paris to escape the Nazis. Told partly through the eyes of Ava, the female golem, the story explores the question of what it means to be human in a world full of inhumanity.


=== Film and television ===
Inspired by Gustav Meyrink's novel was a classic set of expressionistic silent movies (1915–1920), Paul Wegener's Golem series, of which The Golem: How He Came into the World (also released as The Golem, 1920, US 1921: the only surviving film of the trilogy) is especially famous. In the first film, released in 1915, the Golem is revived in modern times before falling from a tower and shattering; in the second film, The Golem and the Dancing Girl (1917), Wegener (playing himself) puts on his Golem costume and makeup to impress a female fan.
Julien Duvivier's Le Golem (1936) (alternative title: The Man of Stone), a French/Czechoslovakian film, is a sequel to the 1920 Wegener film.
A two-part Czechoslovakian color film The Emperor and the Golem was produced in 1951.
It! (alternative titles: Anger of the Golem and Curse of the Golem) is a 1967 British horror film made by Seven Arts Productions and Gold Star Productions, Ltd. featuring the Golem of Prague as its main subject. It stars Roddy McDowall as the mad assistant museum curator Arthur Pimm who brings the Golem to life.
The Golem (1967) is a French TV movie directed by Jean Kerchbron adapted from Gustav Meyrink's novel.
Golem is a 1979 Polish film directed by Piotr Szulkin.
Faust, a 1994 film by Jan Švankmajer based on the legend of the same name, features a stop-motion animated Czech golem using clay.
Rabín a jeho Golem (1995) is a Czech TV film recounting the Golem of Prague tale, with Vladimír Ráz as Rabbi Jahuda Löw and Pavel Nový as the Golem "Josef".
"Golem", the 41st episode of Gargoyles, originally aired on December 14, 1995, involves a man trying to achieve immortality by transferring his soul into the Golem of Prague.
The 1997 The X-Files episode, "Kaddish", featured a golem.
The Golem (2000) is a TV movie re-make of the original 1920 film, written and directed by Scott Wegener (not related to Paul Wegener).
A golem (played by John DeSantis) appears as a major plot point in the 13th episode of season 8 of the American TV show Supernatural entitled "Everybody Hates Hitler".
In The Simpsons 2006 episode "Treehouse of Horror XVII" (season 15, episode 8), the Golem of Prague (voiced by Richard Lewis) is the key character in the second segment, "You, Gotta, Know When to Golem", where the Simpson family also creates a second, female golem made of Play-Doh (voiced by Fran Drescher).
A giant golem appears as the creation of an evil female mage in Episodes 5, 6 and 13 of the anime series The Familiar of Zero (2006); another, called the "Jörmungandr" (a name taken from the monster in Norse mythology), appears in Episodes 10, 11 and 12 of The Familiar of Zero: Rondo of Princesses (2008).
A Golem created to protect Ichabod Crane's son appears in "The Golem", Episode 10 of Season One of the TV series Sleepy Hollow.
In Episode 4 ("Dyin' on a Prayer," 2014) of Season Four of the mythology-themed TV show Grimm, a rabbi (David Julian Hirsh) summons a Golem out of clay as a supernatural guardian to protect his son David (Jakob Salvati). He later attempts to defeat the Golem by putting a shem in its mouth but fails, and the Golem is defeated when struck multiple times by David.
Malivore, played by actor Douglas Tait, is the name of a golem character in the 2018 CW's spin-off series Legacies, and is one of the series' main antagonists. Malivore was created from black magic by the combined blood of a witch, a werewolf, and a vampire in order to consume supernatural beings. Creatures consumed by Malivore become wiped from the collective conscience and likely become myths and folklore, for example dragons, unicorns, and gargoyles. The series is a spin-off of The Originals (2013), and The Vampire Diaries (2009).
A widely touted and critically acclaimed theatre production called Golem - a dystopian fable about over-reliance on machines in the context of malign corporate agendas and social control - combining live performance and music with highly stylized animation and projection, was filmed at artsdepot, London and broadcast in November 2018 on BBC FOUR in the UK.
In the 2018 movie The Golem, a woman creates a golem to protect her village from a group of thugs. The child golem reminds the woman of her drowned son.


=== Audio ===
In 1974, CBS Radio Mystery Theater aired an episode entitled "The Golem", which takes place during the Holocaust.
A futuristic version of the Prague Golem story adapted by Michelene Wandorform from Marge Piercy's novel He, She and It (see "Literature" above) was broadcast on BBC Radio 4 on 10 June 1995 under the title Body of Glass starring Eleanor Bron and Ciaran Hinds.
The Mysterious Golem of Prague: A Dramatic Passover Story, written by Chaim Clorfene and Simcha Gottlieb and starring Leonard Nimoy, was broadcast as part of JewishKids.org's  Jewish Story Time.
In Brute Force, an Actual Play Podcast, a Golem named Ezra is a central character.


=== Music ===
There have been a number of scores written to accompany or based on the 1920 film, including by Daniel Hoffman and performed by the San Francisco-based ensemble Davka and by Karl-Errnst Sasse.
In 1923, Romanian composer Nicolae Bretan wrote the one-act opera The Golem, first performed the following year in Cluj and later revived in Denver, Colorado, in 1990.
Eugen D'Albert's Der Golem, a music drama in 3 acts with libretto by Ferdinand Lion, premiered in Frankfurt on December 14, 1026.
In 1962, Abraham Ellstein's opera The Golem, commissioned by the New York City Opera, premiered at City Opera, New York.
In 1980, Larry Sitsky composed the opera The Golem with libretto by Gwen Harwood and premiered by The Australian Opera.
In 1988, The german thrash/death metal band Protector created the album Golem to leave one of the most influencing extreme metal albums ever created.
In 1994, composer Richard Teitelbaum composed Golem, based on the Prague legend and combining music with electronics.


=== Comics ===
Marvel Comics resurrected the Golem of Prague as a heroic character created by Len Wein and John Buscema, first appearing in #134 of the second series of The Incredible Hulk, then in issue #174 of Strange Tales in 1974, lasting for only two more issues (#176 & 177), then reappearing sporadically in other Marvel comics series.
Another Marvel Comics Golem appeared as a character in Issue #13 of The Invaders when Jacob Goldstein created a golem to help free The Invaders, who had been captured by the Nazis, but while bringing the clay figure to life became fused with it after they were both struck by lightning. He made a second and final appearance in The Invaders Vol. 2 Issue #2.
The Golem of Prague is an antihero in DC Comics' 1991-92 reboot of the superhero the Ragman. In the eight-issue miniseries, it serves as the predecessor to the Ragman suit itself, replaced by the Rabbis as a protector when it becomes uncontrollable. When the main character, Rory Regan, gains control of the Ragman suit, the Golem reawakens and hunts him down, but falls in love with a human woman along the way. The Golem later returned in the 1993-94 sequel miniseries, Ragman: Cry of the Dead #1-5.
Fullmetal Alchemist, a Japanese manga series, follows the story of a young alchemist who, after a failed attempt to revive his mother through alchemy, must attach the soul of his younger brother to a suit of armor. This attachment is performed through the inscription of an alchemic circle on the suit of armor, which, if smudged or broken, would end the younger brother's life.
In James Sturm's 2001 graphic novel The Golem's Mighty Swing, a Jewish baseball team in the 1920s creates a golem to help them win their games.
In 2012, Studio 407 published the graphic novel The Golem written by Scott Barkman and Alex Leung and drawn by Mark Louie Vuycankiat, with a traditional Golem being created in modern-day Sarajevo.


=== Tabletop and video games ===
Golems appear in the fantasy role-playing game Dungeons & Dragons (first published in the 1970s), and the influence of Dungeons & Dragons has led to the inclusion of golems in other tabletop role-playing games, as well as in video games.
Golem is also the name of one of the 151 Generation I Pokémon species that debuted in Pokémon Red and Blue in 1996. The Pokémon Golett and Golurk, however, are more similar to traditional golems.
The video game Diablo II, released by Blizzard Entertainment in 2000, features a character class called Necromancer who can raise various types of golems for battle.
In Heroes of Might and Magic, various golems appear as controllable units, such as stone golem, iron golem, steel golem, gold golem, diamond golem, and sandstone golem.
The sci-fi shooter Warframe features a gigantic boss known as the Jordas Golem, which is an amalgamation of Infested flesh merged with ship technology from the Corpus faction, controlled by a corrupted digital being known as a Cephalon.
In Heroes of the Storm, grave golems are large and powerful creatures made of skulls and brambles who serve as the mercenary bosses on several battlegrounds.
In Warcraft series, War golems are golems that are a dwarven invention.
In the video game Minecraft, golems are found as mobs, which are divided into two types: the Iron Golem and the Snow Golem, with the Iron Golem, more resembling the traditional golem, and the snow golem resembling a snowman. Iron Golems can usually be found in towns and are harmless unless a villager is attacked, in which case they will become hostile towards the attacker.
Golems are featured in the Dragon Age series of dark fantasy role-playing games by Canadian developer BioWare. They were created by the dwarvish Paragon Cardin to fight the Darkspawn. Shale is a golem who features as a companion in Dragon Age: Origins.
The MMORPG RuneScape has a quest called "The Golem", featuring a Golem still obeying his original command.
In the video game adaptation of I Have No Mouth And I Must Scream, the player controls Nimdok to build a golem as part of his scenario.
In Fantasy Flight Games' Android universe of board games and role-playing games, both humanoid robots ("bioroids") and artificially gestated human clones are collectively given the epithet of golem, implying that they lack the spark of the divine within them because they are manufactured by man, and not born.
Golems appear as enemies in the RPG Infinity Blade.
In 2016, the Golem appeared in the mobile game Clash Royale which was to be unlocked in Arena 3: Barbarian Bowl. It costs 8 Elixir. When its hit points are used up, it explodes into two smaller Golemites. There is another Golem in Arena 8: Frozen Peak, but this Golem is made from ice and is named Ice Golem. It has lower hitpoints with lower damage from the Golem as well as costing only 2 Elixir. After the Ice Golem's hitpoints end, it explodes, slowing down nearby enemies, but both of them hit buildings only.
One of the bosses in the 2011 sandbox-adventure game Terraria is depicted as a golem and named Golem.
The fifth generation Pokemon Golett and Golurk are Ground/Ghost types shaped after Golems. The latter has a seal in its chest similar to the Prague Golem.
In Magic: The Gathering, "Karn" is a time-travelling silver golem planeswalker from Dominaria. Golem is also a creature type describing artificial life-forms created by magic, typically with "powerstones" providing them with energy.


== See also ==


== References ==


== Further reading ==
Baer, Elizabeth R. (2012). The Golem Redux: From Prague to Post-Holocaust Fiction. Detroit, MI: Wayne State University. ISBN 978-0814336267.
Bilski, Emily B. (1988). Golem! Danger, Deliverance and Art. New York: The Jewish Museum. ISBN 978-0873340496.
Bloch, Chayim; tr. Schneiderman, H. (1987). The Golem: Mystical Tales of the Ghetto of Prague (English translation from German. First published in 'Oestereschischen Wochenschrift' 1917). New York: Rudolf Steiner Publications. ISBN 0833400258.
Bokser, Ben Zion (2006). From the World of the Cabbalah. New York: Kessinger. ISBN 9781428620858.
Chihaia, Matei (2011). Der Golem-Effekt. Orientierung und phantastische Immersion im Zeitalter des Kinos. Bielefeld: transcript. ISBN 978-3-8376-1714-6.
Faucheux, Michel (2008). Norbert Wiener, le golem et la cybernétique. Paris: Editions du Sandre.
Dennis, Geoffrey (2007). The Encyclopedia of Jewish Myth, Magic, and Mysticism. Woodbury (MN): Llewellyn Worldwide. ISBN 978-0-7387-0905-5.
Winkler, Gershon (1980). The Golem of Prague: A New Adaptation of the Documented Stories of the Golem of Prague. New York: Judaica Press. ISBN 0-910818-25-8.
Goldsmith, Arnold L. (1981). The Golem Remembered 1909–1980: Variations of a Jewish Legend. Detroit: Wayne State University Press. ISBN 0814316832.
Montiel, Luis (30 June 2013). "Proles sine matre creata: The Promethean Urge in the History of the Human Body in the West". Asclepio. 65 (1): 001. doi:10.3989/asclepio.2013.01.
Idel, Mosche (1990). Golem: Jewish Magical and Mystical Traditions on the Artificial Anthropoid. Albany (NY): State University of New York Press. ISBN 0-7914-0160-X.
Rosenberg, Yudl; tr. Leviant, Curt (2008). The Golem and the Wondrous deeds of the Maharal of Prague (first English translation of original in Hebrew, Pietrkow, Poland, 1909). Yale University Press. ISBN 978-0-300-12204-6.
Salfellner, Harald (2016). The Prague Golem: Jewish Stories of the Ghetto. Prague: Vitalis. ISBN 978-80-7253-188-2.
Tomek, V.V. (1932). Pražské židovské pověsti a legendy. Prague: Končel. Translated (2008) as Jewish Stories of Prague, Jewish Prague in History and Legend. ISBN 1-4382-3005-2.


== External links ==
 Media related to Golem at Wikimedia Commons

rabbiyehudahyudelrosenberg.com
Background on the Golem legends
yutorah.org
Historical figures in the golem legends
Essay about the golem and Jewish identity
Listen to "The Mysterious Golem of Prague" on Jewish Story Time
See Rabín a jeho Golem on YouTube
See It! (aka Curse of the Golem) on Internet Archive
See Myths of Mankind: The Golem of Prague on YouTube