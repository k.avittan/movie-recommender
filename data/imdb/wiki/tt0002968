Best: His Mother's Son is a 2009 comedy-drama television film that chronicles the life of the late footballer George Best's mother, Ann, and her battles against alcoholism that would ultimately lead to her death at the age of 54. The film was originally broadcast on BBC Two and BBC Northern Ireland on 26 April 2009, receiving ratings of 2.74 million viewers.Best: His Mother's Son was filmed at County Antrim and Belfast in Northern Ireland, the country where George Best was born in 1946. It starred Tom Payne as the title character, with Michelle Fairley playing his mother Ann and Lorcan Cranitch appearing as his father Dickie. Des McAleer was also featured as the Manchester United manager during George's playing career, Matt Busby.


== Background ==
The film looks at the later life of George Best's mother, Ann, who became an alcoholic despite having never touched alcohol before the age of 40. However she became so addicted that it killed her within 10 years. The film focuses upon how much this was influenced by her son's fame and also whether she had an effect on her son, who died in 2005 from organ failure.


== Cast and characters ==
Tom Payne as George Best
Michelle Fairley as Ann Best
Lorcan Cranitch as Dickie Best
Lisa Hogg as Carol Best
Laura Donnelly as Barbara Best
Des McAleer as Matt Busby
Stephen Hagan as Steve Fullaway
Bronagh Taggart as Ruby Emerson
Niamh McGrady as Martha Devine


== Controversy ==
The film has been met with controversy, with claims from the Best family that the writers and producers had not been given permission to make the film. George Best's sister gave an interview to the Belfast Telegraph in which she said "Everyone declined to participate in [the filming]. The family was asked but we weren't prepared to give the BBC any information on my mum's alcoholism." Julia told the newspaper that she was also a recovering alcoholic and would have found it very difficult to allow the film to be made. She went on to say "As a recovering alcoholic myself I am totally aware of the need for publicity for this subject, but I totally object to anyone using my family to do it — especially since the three main characters in this drama are dead and can’t defend themselves." Barbara McNarry, the sister of George Best, said that the film's portrayal of alcoholism showed a "simplistic and limited understanding of this complex disease".There was also controversy over how accurately events are portrayed, despite the fact that real game footage and interviews are used within the film. Tom Payne was quoted in an interview saying "It's meant to be based on real events but there are some parts which are made for TV". He also said that he had done much reading to prepare for the role but "as far as the family are concerned, they were unconnected to the film".


== Reception ==


=== Critical reception ===
The film received a mixed response from television critics. Andrew Billen, writing in The Times, gave it 3 stars out of 5 said of Tom Payne's performance "Tom Payne as George surprised early on by emphasising his shyness and naivety but he did not grow sufficiently to convince us that he had the charisma and stature of a fifth Beatle". Sky Television wrote that it was a "much hyped drama, but one thing is for sure that the compelling story of the boy from Belfast who went on to become a world class footy player - will always be in the nation's hearts." The Daily Telegraph's Damian Thompson wrote that it was a "gloomy drama" in terms of the material it was dealing with but "the recreation of late-Sixties Belfast was accurate and...intelligently subdued" and described Michelle Fairley's portrayal of Ann Best as "magnificent, pulling off the difficult trick of conveying the hidden drunkenness of the respectable woman alcoholic." Tom Sutcliffe, writing in The Independent, drew comparisons with Five Minutes of Heaven, a recent drama that was also set in Northern Ireland. He said that the film "remained penned in by biographical fidelity" and "it somehow seemed smaller than the tale it was telling...it consciously offered the back-story to a larger and better-known melodrama...it never seemed entirely sure what to do with the material it had."The Belfast Telegraph was extremely critical of the film, calling it "an intrusive film that failed to tell the full story".


== References ==


== External links ==
Best: His Mother's Son on IMDb
Review, Leicester Mercury