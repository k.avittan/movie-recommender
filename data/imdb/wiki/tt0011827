The Village Sleuth is a 1920 American silent comedy drama film directed by Jerome Storm and written by Agnes Christine Johnston. The film stars Charles Ray, Winifred Westover, Dick Rush, Donald MacDonald, George Hernandez, and Betty Schade. The film was released on September 12, 1920, by Paramount Pictures. A copy of the film is in the Gosfilmofond film archive.


== Plot ==
As described in a film magazine, William Wells (Ray), a farm boy with a consuming desire to be like Sherlock Holmes, takes his first "detective" commission from his father Pa Wells (Morrison) and seeks to discover the identity of some watermelon thieves. Discovering the culprits among his own gang of friends and his father finding this out, he goes on a wider path to become a detective. Obtaining work as a hired man for a health resort, William begins an untiring hunt for a mystery. He gets a taste of the real thing when a robbery and murder come rapidly racing one over the other. In the end the man supposedly murdered makes his appearance and the sleuth uncovers the robbery culprit in an ex-convict guest of the resort. Pinky (Westover), a chorus girl in cahoots with the "murdered" man, gives William a lively time in keeping faith in her, but proves her trust in the end.


== Cast ==
Charles Ray as William Wells
Winifred Westover as Pinky Wagner
Dick Rush as David Keene
Donald MacDonald as Dr. Roberts
George Hernandez as Mr. Richley  (credited as George F. Hernandez)
Betty Schade as Mrs. Richley
Louis Morrison as Pa Wells (credited as Lew Morrison)


== Promotion ==
From a newspaper advertisement for the film:

"As a country boy, with aspirations to become a great detective, Charles Ray is said to afford considerable laughter and a few thrills in A Village Sleuth . . . After his attempts to round up some melon thieves in his dad's apple orchard, have gotten him into hot water, Charlie goes out and gets a real job in a private sanitarium. There he encounters a real mystery and, his detective instincts aroused starts to unravel it. The results are surprising in the extreme. Charlie is revealed not only as the logical successor to Sherlock Holmes but wins a pretty girl in the bargain. A Village Sleuth was written by Agnes Christine Johnston, scenarist of Twenty-three and a Half Hours Leave, and produced for Paramount release by Thomas H. Ince. Winifred Westover is the leading woman. Jerome Storm directed." 


== Preservation status ==
Copies of The Village Sleuth are held at the Library of Congress, Gosfilmofond, UCLA Film and Television Archive, Academy Film Archive, and Jugoslovenska Kinoteka.


== References ==


== External links ==
The Village Sleuth on IMDb
synopsis at AllMovie