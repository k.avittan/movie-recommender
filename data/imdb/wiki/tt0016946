Hands Across America was a public fundraising event on Sunday, May 25, 1986, in which approximately 5 to 6.5 million people held hands for fifteen minutes in an (ostensible) attempt to form a continuous human chain across the contiguous United States.Despite the claimed intent, there was never any realistic chance of forming an unbroken human chain from coast to coast, as hundreds of miles of low-population areas along the route through the vast rural Midwest and Desert Southwest would have required hundreds of thousands of volunteers and millions of dollars in transportation, provision and coordination costs to keep the chain intact. Regardless, the event was declared a major success by the participants, particularly in larger urban areas where there was greater enthusiasm and a successfully large amount of fundraising.
Many participants donated ten dollars to reserve their place in line. The proceeds were donated to local charities to fight hunger and homelessness and help those in poverty.
The event raised about $15 million for charities after operating costs.


== Cities ==

Cities along the route included the following:

New York, New York with Brooke Shields as well as Liza Minnelli, Cardinal John O'Connor, Susan Anton, Gregory Hines, and Edward James Olmos, Yoko Ono, and Harry Belafonte anchoring the George Washington Bridge.
Trenton, New Jersey (with Dionne Warwick and Tony Danza)
Philadelphia, Pennsylvania (with Jerry Lewis and Scott Baio)
Baltimore, Maryland (with R2-D2 (Kenny Baker) and Emmanuel Lewis.) The first break in the chain west of New York was reported to be in Maryland.
Washington, D.C. (with President Ronald Reagan at the White House and Speaker of the House Tip O'Neill at the United States Capitol)
Pittsburgh, Pennsylvania (with Fred Rogers and the Pittsburgh Pirates Parrot)
Youngstown, Ohio (with Michael Jackson)
Cleveland, Ohio (with David Copperfield)
Toledo, Ohio (with Jamie Farr)
Columbus, Ohio (with Michael J. Fox)
Cincinnati, Ohio (with Chewbacca the Wookiee)
Indianapolis, Indiana (occurred in the rain, scheduled side-by-side with the Indy 500, which was rained-out that day)
Champaign, Illinois (with Walter Payton)
Chebanse, Illinois: A cornfield in central Illinois served as center-point of the nation with 16,000 people in attendance
Springfield, Illinois (with 50 Abraham Lincoln impersonators)
St. Louis, Missouri (with Kathleen Turner under the St. Louis Arch)
Memphis, Tennessee (with 54 Elvis Presley impersonators)
Little Rock, Arkansas (with Governor Bill Clinton)
Amarillo, Texas (with Kenny Rogers, Renegade, Lee Greenwood and Tony Dorsett at the TX-NM border)
Albuquerque, New Mexico (with Don Johnson)
Phoenix, Arizona (with Ed Begley, Jr., however desert areas were mostly empty, dotted with one-mile (1.6 km)-long chains of people. Truck drivers sounded their horns during the appointed time.)
San Bernardino, California (with Bob Seger and Charlene Tilton)
Santa Monica, California (with George Burns, Jack Youngblood, Dudley Moore, Richard Dreyfuss, and Donna Mills)
Long Beach, California (with Mickey Mouse, Goofy, Reverend Robert Schuller, Kenny Loggins, Joan Van Ark, John Stamos, Robin Williams, and C-3PO (Anthony Daniels) backed by Papa Doo Run Run.)The event was conceived and organized by Ken Kragen. Event implementation was through USA for Africa under the direction of Marty Rogol, the founding Executive Director. A theme song, titled "Hands Across America," was played simultaneously on hundreds of radio stations at 3:00 p.m. Eastern time (noon Pacific time). The song was written by Marc Blatte, John Carney, and Larry Gottlieb, and featured lead vocals by session singers Joe Cerisano and Sandy Farina, and the band Toto. The song peaked at #65 on the Billboard Hot 100 in 1986.
Hands Across America was a project of USA for Africa. USA for Africa produced "We Are the World" and the combined revenues raised by both events raised almost $100 million to fight famine in Africa and hunger and homelessness in the United States.
The date and time chosen for the event inadvertently conflicted with another charity fundraiser, Sport Aid, which was organized by USA for Africa on the same day. Since Hands Across America was much better publicized in the United States, only 4000 runners participated in New York City for Sport Aid.


== Continuity of the chain ==
In order to allow the maximum number of people to participate, the path linked major cities and meandered back and forth within the cities. Just as there were sections where the "line" was six to ten people deep, there were also undoubtedly many breaks in the chain. However, enough people participated that if an average of all the participants had been taken and spread evenly along the route standing four feet (1.2 m) apart, an unbroken chain across the 48 contiguous states would have been able to be formed.


== Legacy ==
Hands Across America raised $34 million. According to The New York Times, only about $15 million was distributed after deducting operating costs.


== References ==


== Further reading ==

Hands Across America, May 25, 1986. Pocket Books. 1986. ISBN 978-0671631185. OCLC 14525428.
USA for Africa's Hands Across America pages
ABC News: Great Shakes: 'Hands Across America' 20 Years Later
Time: American Notes Charity, December 1, 1986