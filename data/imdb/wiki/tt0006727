Gloriana, Op. 53, is an opera in three acts by Benjamin Britten to an English libretto by William Plomer, based on Lytton Strachey's 1928 Elizabeth and Essex: A Tragic History.
The first performance was presented at the Royal Opera House, London, in 1953 during the celebrations of the coronation of Queen Elizabeth II. Gloriana was the name given by the 16th-century poet Edmund Spenser to his character representing Queen Elizabeth I in his poem The Faerie Queene. It became the popular name given to Elizabeth I. It is recorded that the troops at Tilbury hailed her with cries of "Gloriana, Gloriana, Gloriana", after the defeat of the Spanish Armada in 1588.
The opera depicts the relationship between Queen Elizabeth and the Earl of Essex, and was composed for the coronation of Queen Elizabeth II in June 1953. Several in the audience of its gala opening were disappointed by the opera, which presents the first Elizabeth as a sympathetic, but flawed, character motivated largely by vanity and desire. The premiere was one of Britten's few critical failures, and the opera was not included in the series of complete Decca recordings conducted by the composer. However, a symphonic suite extracted from the opera by the composer (Opus 53a), which includes the Courtly Dances, is often performed as a concert piece.


== Performance history ==
On 22 November 1963, the composer's 50th birthday, Bryan Fairfax conducted a concert performance, which was the opera's first performance in any form since its inaugural production in 1953. When the production toured in 1954 to Manchester and Birmingham, Joan Sutherland sang the role of Penelope. The second staging of Gloriana was undertaken by Sadler's Wells Opera in 1966 with Sylvia Fisher in the title role, directed by Colin Graham.In 1973, at The Proms in London, a concert version under conductor Charles Mackerras was performed and recorded, based on Sadlers' Wells Opera's revival of the Colin Graham production, with Ava June and David Hillman as Elizabeth and Essex.Another eleven years were to pass before a recording appeared, that of 1984 under Mark Elder by the English National Opera (formerly Sadlers' Wells Opera). This was also based on a revival of Colin Graham's production , with Sarah Walker and Anthony Rolfe-Johnson as Elizabeth and Essex, and the production also toured to the United States in New York, New Orleans and Texas. Other productions of the opera in Britain have been undertaken by Welsh National Opera in 1992 and by Opera North in 1994 . At Opera North the production was directed by Phyllida Lloyd with Josephine Barstow and Tom Randle as Elizabeth and Essex, conducted by Paul Daniel, the production was extensively toured, revived and was recorded in 1999.Central City Opera (in Colorado) presented the North American premiere production of the opera in 2001, starring Joyce Castle as Elizabeth I. The Opera Theatre of Saint Louis mounted the work in 2005, with Christine Brewer as Elizabeth I.
The Royal Opera House in London presented a performance in June–July 2013 to celebrate both the 60th anniversary of the opera, and the centenary of Britten's birth. The performance starred Susan Bullock, Toby Spence, Patricia Bardon and Mark Stone.


== Roles ==


== Synopsis ==
Time: The late 16th century.
Place: England.


=== Act 1 ===
Scene 1: A tournament
Lord Mountjoy wins a jousting tournament. Robert Devereux, Earl of Essex, provokes Mountjoy into fighting with him and is slightly wounded. Queen Elizabeth arrives and scolds the men for their jealousy. She requests that they attend her at court as friends. Mountjoy and Essex make peace and the crowd praise Elizabeth.
Scene 2: The Queen's apartment, Nonsuch Palace
Elizabeth and Cecil discuss the rivalry between Mountjoy and Essex. Cecil warns Elizabeth about the threat of another Armada from Spain and cautions her that it would be dangerous to show too much affection to the impulsive Essex. After Cecil has gone, Essex himself enters and sings to the Queen to take her mind off political problems. He asks her to let him go to Ireland to counter the rebellion led by the Earl of Tyrone. He grows impatient when the Queen shows reluctance, and accuses Cecil and Walter Raleigh of plotting against him. Elizabeth sends him away and prays for strength to rule her people well.


=== Act 2 ===
Scene 1: Norwich
The Queen, accompanied by Essex, visits Norwich, and talks with the Recorder of Norwich. A masque celebrating Time and Concord is given in her honour.
Scene 2: Essex's house
Essex's sister Lady Penelope Rich meets Mountjoy for an illicit tryst in the garden. Essex and his wife Frances join them, and Essex denounces the Queen for thwarting his plans to go to Ireland. He, Mountjoy and Lady Rich imagine gaining power as the Queen gets older, but Frances urges caution.
Scene 3: The Palace of Whitehall
A ball is in progress at the Palace. Frances, Lady Essex, is wearing a beautifully ornate dress, which is much admired by members of the court. The Queen commands the musicians to play an energetic melody; the courtiers dance a set of five energetic "Courtly Dances". The ladies retire to change their linen. Lady Essex enters, wearing a plainer dress than before and tells Lady Rich that her original dress has gone missing. The Queen arrives wearing Lady Essex's dress, which is far too short and tight for her. She mocks Lady Essex and withdraws again. Mountjoy, Essex and Lady Rich comfort the humiliated Lady Essex. Essex expresses his fury at the Queen's behaviour, but calms down when Elizabeth returns, in her own clothes. She appoints Essex Lord Deputy of Ireland. Everyone celebrates.


=== Act 3 ===
Scene 1: Nonsuch Palace
The Queen's maids gossip about Essex's failure to control the Irish rebellion. Essex bursts in and insists on seeing the Queen immediately, even though she is wigless and in her dressing gown. Elizabeth sadly admits to Essex that she is an old woman. She receives him kindly and is initially sympathetic to his troubles, but grows impatient as he complains about his enemies at court. When he has left, her maids dress her and make up her face. Cecil arrives and warns her that the Irish rebels and the hot-headed Essex both pose a threat to her reign. Elizabeth agrees that Essex should be kept under house arrest.
Scene 2: A street in the City of London
A ballad singer recounts Essex's attempts to incite rebellion, while Essex's followers try to gather new recruits. A herald announces that Essex is branded a traitor, and that anyone who supports him will be guilty of treason.
Scene 3: The Palace of Whitehall
Essex has been sent to the Tower of London. Cecil, Raleigh and other councillors try to persuade the Queen to sentence Essex to death, but she is reluctant. Alone, she muses on her continued fondness for Essex. Lady Essex, Lady Rich and Lord Mountjoy arrive to beg for mercy for Essex. The Queen treats the gentle Lady Essex kindly and reassures her that she and her children will not suffer. However, she becomes angry when the proud Lady Rich implies that the Queen needs Essex to rule effectively. Elizabeth refuses to listen to further entreaties and signs Essex's death warrant. Alone again, she reflects on her relationship with Essex and her own mortality.


== Recordings ==
1984 Sarah Walker (Queen Elizabeth I), Anthony Rolfe Johnson (Earl of Essex), Jean Rigby (Countess of Essex), Neil Howlett (Lord Mountjoy), Alan Opie (Sir Robert Cecil), Elizabeth Vaughan (Lady Rich), Richard Van Allan (Sir Walter Raleigh), Malcolm Donnelly (Henry Cuffe); English National Opera Chorus and Orchestra; Mark Elder (conductor). Live recording, London 1984 (also broadcast on the BBC). Label: ArtHaus Musik DVD
1993 Josephine Barstow, Philip Langridge, Alan Opie, Jonathan Summers, Willard White, Jenevora Williams, Welsh National Opera Orchestra & Chorus, Charles Mackerras (Argo/Decca)
2013 Royal Opera House, London, Paul Daniel (Opus Arte)
2018 Teatro Real, Madrid, conductor: Ivor Bolton, starring: Anna Caterina Antonacci, Leonardo Capalbo, Paula Murrihy, Duncan Rock and others


== References ==
Notes

Sources

Blyth, Alan, "Review: Gloriana", Gramophone, December 2006.
Erb, Jane, Gloriana, ClassicalNet, 1996
Rosenthal, Harold: "Fisher, Sylvia" in Sadie, Stanley (ed), The New Grove Dictionary of Opera, Oxford: Oxford University Press ISBN 978-0-19-522186-2
Saberton, Roy, Programme Notes: Gloriana – The Courtly Dances, The Burgess Hill Symphony Orchestra, May 1998Other sources

Whittall, Arnold, "Gloriana"  in Stanley Sadie, (Ed.),  The New Grove Dictionary of Opera, Vol. Two, pp. 451– 452. London: Macmillan Publishers, Inc. 1998  ISBN 0-333-73432-7  ISBN 1-56159-228-5