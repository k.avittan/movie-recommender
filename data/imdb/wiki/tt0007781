The Chamber of Horrors was an original exhibition at Madame Tussauds in London, being an exhibition of waxworks of notorious murderers and other infamous historical figures. The gallery first opened as a 'Separate Room' in Marie Tussaud's 1802 exhibition in London and quickly became a success as it showed historical personalities and artifacts rather than the freaks of nature popular in other waxworks of the day. It closed permanently in April 2016.


== Early days ==

The forerunner of Tussaud's Chamber of Horrors was the Caverne des Grands Voleurs (the Cavern of the Great Thieves) which had been founded by Dr Philippe Curtius as an adjunct to his main exhibition of waxworks in Paris in 1782. Here Curtius displayed wax figures of notorious French criminals who had been executed, as well as members of the French royal family and aristocracy who had been guillotined during the Revolution.
When Marie Tussaud moved to London in 1802 to set up her own exhibition at the Lyceum Theatre she brought some of these figures with her and set them up in a separate gallery; and when later she toured her exhibits around the country she maintained this division in her exhibition using a 'Separate Room' to display them in. The exhibits at this time included the heads of King Louis XVI and Marie Antoinette, as well as Madame du Barry, Marat, Robespierre, Hébert, Carrier and Fouquier-Tinville in addition to models of a guillotine and the Bastille and the Egyptian mummy from Curtius' collection.

In 1835 Madame Tussaud set up a permanent exhibition in London, and here the 'Separate Room' became the 'Chamber of Horrors'. At this time her exhibits included Colonel Despard, Arthur Thistlewood, William Corder and Burke and Hare, in addition to those listed above. The name 'Chamber of Horrors' is often credited to a contributor to Punch in 1845, but Marie Tussaud appears to have originated it herself, using it in advertising as early as 1843. Visitors were charged an extra sixpence to enter the 'Separate Room'.
In 1886 the exhibits included Burke and Hare, James Bloomfield Rush, Charles Peace, William Marwood, Percy Lefroy Mapleton, Mary Ann Cotton, Israel Lipski, Franz Muller, William Palmer and Marie Manning.Other exhibits have included George Chapman, John Reginald Halliday Christie, William Corder, Dr Hawley Harvey Crippen, Colonel Despard, John Haigh, Neville Heath, Bruno Hauptmann, Henri Landru, Charles Manson, Florence Maybrick, Donald Neilson, Dennis Nilsen, Mary Pearcey, Buck Ruxton, George Joseph Smith and Arthur Thistlewood.


== The Chamber ==
This part of the exhibition was in the basement of the building and included wax heads made from the death masks of victims of the French Revolution including Marat, Robespierre, King Louis XVI and Marie Antoinette, who were modelled by Marie Tussaud herself at the time of their deaths or execution, and more recent figures of murderers and other notorious criminals.
The Chamber of Horrors was renovated in 1996 at a cost of $1.5 million, bringing to life the history of crime and punishment over the last 500 years and including items from Newgate Prison and featuring replicas of instruments of torture displayed amid a recording of actors' groans and screams. An innovation in recent years was to have actors in macabre make-up and costumes lurch at customers from the dark shadows and recesses of prison cells, where some cells were occupied with waxwork figures and others had the doors ajar, giving the impression that a dangerous maniac was on the loose. Historical characters displayed included Vlad the Impaler, Genghis Khan, Guy Fawkes and Adolf Hitler. There was no waxwork figure of Jack the Ripper in the Chamber of Horrors, in accordance with Madame Tussaud's policy of not modelling persons whose likeness is unknown. Instead, he was portrayed as a shadow. Figures of disgraced entertainers Jimmy Savile and Gary Glitter were destroyed rather than being relocated to the Chamber of Horrors.Visiting the exhibit was optional and not recommended for young children or pregnant women.
The chamber closed on 11 April 2016 and has since been replaced by a new attraction named the Sherlock Holmes Experience.
From an educational perspective the chamber catalogued various methods of execution from around the world and after closure, the public was no longer afforded this opportunity.

		
		
		
		
		
		
		
		
		


== References ==


== External links ==
Madame Tussaud's 'Chamber of Horrors' on Pathé News (1947)
The Chamber of Horrors on the Madame Tussauds website
'A Proximate Violence: Madame Tussaud's Chamber of Horrors' - Nineteenth-Century Art Worldwide