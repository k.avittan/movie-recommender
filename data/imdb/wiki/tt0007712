Blue Streak is a 1999 American buddy cop comedy film  directed by Les Mayfield and starring Martin Lawrence, Luke Wilson, Dave Chappelle, Peter Greene, Nicole Ari Parker, and William Forsythe. It is a remake of the British film The Big Job (1965). The film was shot on location in California. The prime shooting spot was Sony Pictures Studios, which is located in Culver City, California.
The film was released in September 1999 and opened as the number one movie in North America. It went on to gross nearly US$120 million at the worldwide box office. The film's soundtrack album, featuring a number of popular urban/hip-hop artists, was certified platinum.


== Plot ==
Jewel thief Miles Logan participates in a $17 million diamond heist in Los Angeles. One of his accomplices, Deacon, turns on Miles and kills Eddie, Miles' best friend and another member of the team, before attempting to take the stone from Miles. The police arrive and Miles is forced to hide the diamond in the ductwork of a building being constructed.  Deacon flees and Miles is arrested, and as Miles is being taken away he is horrified to discover that Eddie has been killed.
Two years later, Miles is released from prison and attempts to reconnect with his girlfriend. She dumps him for lying to her about his criminal life, and Miles decides to go retrieve the diamond. He is dismayed to find that the building he hid the diamond in is now an LAPD police station. He goes inside and discovers that the stone is hidden in what is now the Robbery/Homicide detective bureau, which requires a key card to access.
Miles returns later on disguised as an eccentric pizza deliveryman. While unable to gain access to the ducts, he does manage to steal an access card. Miles visits his forger Uncle Lou, who creates a fake badge and transfer papers that allow Miles to enter the building posing as a newly transferred Detective Malone. While trying to access the ducts, Miles inadvertently foils a prisoner escape and is teamed up with Detective Carlson. The pair are sent out on a burglary call, where Miles quickly solves it as a fraud perpetrated by the owner. On the ride back, they stumble upon an armed robbery being committed by Miles' good friend and former getaway driver Tulley. Miles intervenes and arrests Tulley before the police can shoot him, but Tulley demands $50,000 to keep quiet about who Miles really is.  Miles makes another attempt to locate the diamond but is interrupted by Carlson, who has discovered that Miles isn't who he claims to be. Miles convinces Carlson that he is from Internal Affairs. Miles tries to get back to searching for the diamond but he and Carlson are sent out on another call. While out, they capture a truckload of heroin belonging to a major dealer. Miles locates the diamond in the vent inside the evidence locker and finally retrieves it, but accidentally drops it into the load of heroin they seized. The FBI arrives and demands the heroin be turned over to them for testing.
A panicked Miles suggests the FBI and his cops unit use the heroin as bait in a sting. He arranges to be with the heroin in the delivery truck, but is soon joined by Tulley (who he secretly set free from holding) and Deacon. During the drug deal, Deacon tries to expose Miles as a cop to the drug runners. While Miles and Tulley attempt to distract the dealers, the police and FBI raid the deal. Deacon escapes with the diamond in an armored truck and the police and FBI follow as he approaches the border to Mexico. The police and FBI are forced to halt their pursuit at the border, but Miles steals a patrol car and continues after Deacon. Miles forces Deacon to wreck the truck and then offers him a deal: Deacon gives Miles the diamond and allows Miles to arrest him and in exchange Miles takes him back to the United States and cuts him back in on the diamond. Deacon agrees, and Miles immediately double-crosses him by handcuffing him to the wrecked truck for the Federales to find. Deacon draws a gun to shoot Miles but Miles turns and shoots him first, killing him and avenging Eddie's death.
Miles walks back the US side of the border where the FBI demands he explain his actions. The police also want to know who he's working for as his fake credentials didn't check out.  Miles tells them he's an undercover Mexican officer, and has to get back to Mexico to explain everything to his fellow Federales. Miles gets a few inches over the border when Carlson and Hardcastle stop him and reveal that they know who Miles really is. However, they don't arrest him as they are grateful for all of his help and look at him as a friend. They jokingly state the FBI are strict about integrating people over international borders although Miles is only a few inches over the border. The three men share a bittersweet goodbye, before Miles heads off into Mexico with the diamond.


== Cast ==
Martin Lawrence as Miles Logan/Detective Malone
Luke Wilson as Detective Carlson
Dave Chappelle as Tulley
Peter Greene as Deacon
William Forsythe as Detective Hardcastle
Graham Beckel as Lieutenant Rizzo
Robert Miranda as Detective Glenfiddish
Olek Krupa as Jean LaFleur
Saverio Guerra as Benny
Richard C. Sarafian as Uncle Lou
Tamala Jones as Janiece
Julio Oscar Mechoso as Detective Diaz
Steve Rankin as FBI Agent Gray
Carmen Argenziano as Captain Penelli
John Hawkes as Eddie
Nicole Ari Parker as Melissa Green
J. Kenneth Campbell as FBI Section Commander Peterson


== Reception ==


=== Box office ===
The film opened at #1 with a weekend gross of $19,208,806 from 2,735 theaters for a per venue average of $7,023. It ended its run with $68,518,533 in North America, and $49,239,967 internationally for a total of $117,758,500 worldwide.


=== Critical reception ===
Blue Streak had received mostly mixed reviews. Rotten Tomatoes gives the film a 36% "Rotten" rating based on reviews from 69 critics. The critical consensus reads: "Martin Lawrence lends his comedic touch, but the movie isn't much more than standard action-comedy fare." Gene Seymour of the Los Angeles Times described the film by saying that "it starts out like a caper flick that shifts, almost by accident, into an episode from the old 'Martin' TV series [until] eventually, it settles for being a bleached, cluttered photostat of 'Beverly Hills Cop,' if only a bit more clever than the original." Lawrence Van Gelder of The New York Times also compared the film to Beverly Hills Cop, and stated that "in this instance, the buoyancy is only intermittent."
 Audiences polled by CinemaScore gave the film an average grade of "A" on an A+ to F scale.Roger Ebert praised the film, giving it 3 stars out of 4.


== Cancelled sequel ==
There were plans for a sequel. However, the sequel was never made.  Its screenplay was re-purposed and turned into the film Bad Company.


== See also ==
The Big Job, a 1965 movie with a similar plot


== References ==


== External links ==
Blue Streak on IMDb
Blue Streak at Rotten Tomatoes
Blue Streak at Box Office Mojo