A lovers' lane is a secluded area where people kiss, make out, or engage in sexual activity. These areas range from parking lots in secluded rural areas to places with extraordinary views of a cityscape or other feature.
"Lovers' lanes" are typically found in cultures built around the automobile—lovers often make out in a car or van for privacy.
Lovers' lanes have existed for centuries, sometimes as places for secret meetings with a forbidden lover or as a euphemism for red-light districts and other areas of prostitution.The Oxford English Dictionary records use of the phrase "lovers' lane" from 1853.


== Examples ==

There are several streets called Lovers Lane, including those at Oriskany, New York; Whitmire, South Carolina; Manitou Springs, Colorado; Baton Rouge, Louisiana; Kersey, Pennsylvania; Boonville, New York; Greenfield, Massachusetts; Southborough, Massachusetts; Northfield, Vermont; Riverton, Utah; Steubenville, Ohio; Bowling Green, Kentucky; Portage, Michigan; Excelsior Springs, Missouri; Springfield, Missouri; Charlestown, New Hampshire; Sugar Hill, New Hampshire; Princeton, New Jersey; Slatington, Pennsylvania; Adliya, Bahrain; Dallas, Texas; Texarkana, Texas; Ravenna Township, Portage County, Ohio; Visalia, California; El Segundo, California, Milwaukee, Wisconsin; Ancaster, Ontario; Newark-on-Trent; Ludham (both in England) and Thurso, Scotland.

Lovers Lane station is a DART Light Rail station in Dallas, Texas.
Lover's Lane Bridge is a historic bridge spanning the Dog River in Berlin, Vermont.
Lovers Lane is a famous road in St. Joseph, Missouri of which a popular poem by Eugene Field was written.
The area on the south-east side of Fullers Bridge, which crosses the Lane Cove River, and is located near Chatswood, New South Wales, Australia, is a well-known Lovers' Lane.The bowdlerised version Love Lane is sometimes seen. Jowett Walk, Oxford, once had this name.


== Crime ==
Due to the typically isolated location of most lovers' lanes, they have occasionally been the setting for violent crime. For example:

A series of unsolved murders and violent crimes in 1946, dubbed the Texarkana Moonlight Murders, began with two attacks which targeted couples at lovers' lanes in the Texarkana area.In Palos Verdes, California, a gang of teens robbed multiple cars on a lover's lane in October 1955, and were caught raping a thirteen-year-old girl.In 1963, a lovers' lane site at Fuller's Bridge, Sydney became notorious as the location of the bodies of CSIRO scientist Dr. Gilbert Stanley Bogle and Mrs. Margaret Olive Chandler, the wife of one of his colleagues. The cause of death, while indicative of poisoning, could not be definitively determined, and apart from Mrs. Chandler's husband, Geoffrey, who was considered the prime suspect by the New South Wales Police, no one to-date has been charged. The Bogle-Chandler case has baffled law enforcement and forensic experts up to present day..Several of the Zodiac Killer's victims were murdered in lovers' lanes in northern California.All victims of the Monster of Florence were couples murdered in lovers' lanes near Florence, ItalySeveral attacks perpetrated by the Son of Sam serial killer also took place in such settings.Two Mercer University students were killed by Andy Cook at a lovers' lane location in Georgia on January 2, 1995.


== In popular culture ==
A lovers' lane is typically the setting of the urban legend "The Hook," about a young couple menaced by a hook-handed killer.Lovers' lanes have featured in numerous popular songs; tracks with that title have been released by Georgio (1987), FireHouse (1990), The Other (2006) and Hunx and His Punx (2011). The Go-Betweens' 1988 sixth album was titled 16 Lovers Lane, and the 1992 debut album by M.C. Brains was similarly titled.Films titled Lovers' Lane have been released in 1924, 1999, and 2005. Episodes of the television series 77 Sunset Strip (in 1964) and Roseanne (in 1988) also had this title.


== See also ==
Gropecunt Lane
Lover's Leap
Cruising for sex


== References ==