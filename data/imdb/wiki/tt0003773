Circumstantial evidence is evidence that relies on an inference to connect it to a conclusion of fact—such as a fingerprint at the scene of a crime. By contrast, direct evidence supports the truth of an assertion directly—i.e., without need for any additional evidence or inference.
On its own, circumstantial evidence allows for more than one explanation. Different pieces of circumstantial evidence may be required, so that each corroborates the conclusions drawn from the others. Together, they may more strongly support one particular inference over another. An explanation involving circumstantial evidence becomes more likely once alternative explanations have been ruled out.
Circumstantial evidence allows a trier of fact to infer that a fact exists. In criminal law, the inference is made by the trier of fact in order to support the truth of an assertion (of guilt or absence of guilt).
Reasonable doubt is tied into circumstantial evidence as circumstantial evidence is evidence that relies on an inference, and reasonable doubt was put in place so that the circumstantial evidence against someone in a criminal or civil case may not be enough to convict someone fairly. Reasonable doubt is described as the highest standard of proof used in court and means that a juror can find the defendant guilty of the crime to a moral certainty. Therefore, the circumstantial evidence against someone may not be enough to convict, but it can contribute to other decisions made concerning the case.Testimony can be direct evidence or it can be circumstantial. For example, a witness saying that she saw a defendant stab a victim is providing direct evidence. By contrast, a witness who says that she saw the defendant enter a house, that she heard screaming, and that she saw the defendant leave with a bloody knife gives circumstantial evidence. It is the necessity for inference, and not the obviousness of a conclusion, that determines whether evidence is circumstantial.
Forensic evidence supplied by an expert witness is usually treated as circumstantial evidence. For example, a forensic scientist may provide results of ballistic tests proving that the defendant’s firearm fired the bullets that killed the victim, but not necessarily that the defendant fired the shots.
Circumstantial evidence is especially important in civil and criminal cases where direct evidence is lacking.


== Civil law ==
Circumstantial evidence is used in civil courts to establish or refute liability. It is usually the most common form of evidence, for example in product liability cases and road traffic accidents. Forensic analysis of skid marks can frequently allow a reconstruction of the accident. By measuring the length of such marks and using dynamic analysis of the car and road conditions at the time of the accident, it may be found that a driver underestimated his or her speed. Forensic science and forensic engineering are common as much in civil cases as in criminal.


== Criminal law ==
Circumstantial evidence is used in criminal courts to establish guilt or innocence through reasoning. 
With obvious exceptions (immature, incompetent, or mentally ill individuals), most criminals try to avoid generating direct evidence. Hence, the prosecution usually must resort to circumstantial evidence to prove the existence of mens rea, or intent. The same goes for the plaintiff's establishing the negligence of tortfeasors in tort law to recover damages from them.
One example of circumstantial evidence is the behavior of a person around the time of an alleged offense. In the case of someone charged with theft of money, were the suspect seen in a shopping spree purchasing expensive items shortly after the time of the alleged theft, the spree might prove to be circumstantial evidence of the individual's guilt.


=== Forensic evidence ===
Other examples of circumstantial evidence are fingerprint analysis, blood analysis or DNA analysis of the evidence found at the scene of a crime. These types of evidence may strongly point to a certain conclusion when taken into consideration with other facts—but if not directly witnessed by someone when the crime was committed, they are still considered circumstantial. However, when proved by expert witnesses, they are usually sufficient to decide a case, especially in the absence of any direct evidence. Owing to subsequent developments in forensic methods, old undecided cases (or cold cases) are frequently resolved.


=== Validity of circumstantial evidence ===
A popular misconception is that circumstantial evidence is less valid or less important than direct evidence. This is only partly true: direct evidence is popularly assumed to be the most powerful. Many successful criminal prosecutions rely largely or entirely on circumstantial evidence, and civil charges are frequently based on circumstantial or indirect evidence. 
Indeed, the common metaphor for the strongest possible evidence in any case—the "smoking gun"—is an example of proof based on circumstantial evidence. Similarly, fingerprint evidence, videotapes, sound recordings, photographs, and many other examples of physical evidence that support the drawing of an inference, i.e., circumstantial evidence, are considered very strong possible evidence.
In practice, circumstantial evidence can have an advantage over direct evidence in that it can come from multiple sources that check and reinforce each other. Eyewitness testimony can be inaccurate at times, and many persons have been convicted on the basis of perjured or otherwise mistaken testimony. Thus, strong circumstantial evidence can provide a more reliable basis for a verdict. Circumstantial evidence normally requires a witness, such as the police officer who found the evidence, or an expert who examined it, to lay the foundation for its admission. This witness, sometimes known as the sponsor or the authenticating witness, is giving direct (eyewitness) testimony, and could present credibility problems in the same way that any eyewitness does.
Eyewitness testimony is frequently unreliable, or subject to conflict or outright fabrication. For example, the RMS Titanic sank in the presence of approximately 700 witnesses. For many years, there was vigorous debate on whether the ship broke into two before sinking. It was not until the ship was found, in September 1985, that the truth was known.
However, there is often more than one logical conclusion naturally inferred from the same set of circumstances. In cases where one conclusion implies a defendant's guilt and another his innocence, the "benefit of the doubt" principle would apply.  Indeed, if the circumstantial evidence suggests a possibility of innocence, the prosecution has the burden of disproving that possibility.


==== Examples ====
Much of the evidence against convicted Timothy McVeigh was circumstantial. Speaking about McVeigh's trial, Robert Precht said, "the prosecution's use of indirect evidence is no cause for worry."  The 2004 murder trial of Scott Peterson was another high-profile conviction based heavily on circumstantial evidence. Another case that relied on circumstantial evidence was that of Nelson Serrano. The 2015 murder trial of Ivan Chan Man-sum was a conviction based solely on circumstantial evidence without finding the body.A famous aphorism on the probity of circumstantial evidence was penned by Henry David Thoreau: "Some circumstantial evidence is very strong, as when you find a trout in the milk."


== See also ==
Expert witness
Forensic engineering
Forensic science
Hearsay
Inculpatory evidence


== References ==