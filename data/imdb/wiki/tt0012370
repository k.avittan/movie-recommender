A lamplighter is a person employed to light and maintain candle or, later, gas street lights. Very few exist today as most gas street lighting has long been replaced by electric lamps.


== Function ==
Lights were lit each evening, generally by means of a wick on a long pole. At dawn, they would return to put them out using a small hook on the same pole. Early street lights were generally candles, oil, and similar consumable liquid or solid lighting sources with wicks.


== Other duties ==
Another lamplighter duty was to carry a ladder and renew the candles, oil, or gas mantles.
In some communities, lamplighters served in a role akin to a town watchman; in others, it may have been seen as little more than a sinecure.
In the 19th century, gas lights became the dominant form of street lighting. Early gaslights required lamplighters, but eventually  systems were developed which allowed the lights to operate automatically.


== Today ==
Today a lamplighter is an extremely rare job. In Brest as a tourist attraction a lamplighter has been employed since 2009 to light up the kerosene lamps in the shopping street every day.A small team of lamplighters still operate in London, England where gas lights have been installed by English Heritage.In Waikiki, Hawaii, lamplighters in traditional Hawaiian costumes run along the shore and light gas torches in the evening.
Frank Serpico, an NYPD whistleblower, prefers to use the term "lamp-lighter" to describe the whistleblower's role as a watchman.Tinker Tailor Soldier Spy, a spy novel written by John le Carré, refers to Lamplighters as a section of British Intelligence that provided surveillance and couriers. Lamplighter's Serenade  is a song written by Hoagy Carmichael. It was recorded by Frank Sinatra during his first session as a solo artist, on 19 January 1942. Bing Crosby also recorded a version of the song just days later.
The Old Lamp-Lighter is another song; its music was written by Nat Simon, the lyrics by Charles Tobias. The song was published in 1946.


== Modern outdoors usage ==

In the late-19th and 20th centuries, most cities with gas streetlights replaced them with new electric streetlights. For example, Baltimore, the first US city to install gas streetlights, removed nearly all of them in 1957.  A gas lamp is located at N. Holliday Street and E. Baltimore Street as a monument to the first gas lamp in America, erected at that location.However, gas lighting of streets has not disappeared completely from some cities, and the few municipalities that retained gas lighting now find that it provides a pleasing nostalgic effect. Gas lighting is also seeing a resurgence in the luxury home market for those in search of historical authenticity.
The largest gas lighting network in the world is that of Berlin. With about 37,000 lamps (2014), it holds more than half of all working gas street lamps in the world. In central London around 1500 gas lamps still operate, lighting the Royal Parks, the exterior of Buckingham Palace and almost the entire Covent Garden area. The Park Estate in Nottingham retains much of its original character, including the original gas lighting network.
In the United States, more than 2800 gas lights in Boston operate in the historic districts of Beacon Hill, Back Bay, Bay Village, Charlestown, and parts of other neighborhoods. In Cincinnati, Ohio, more than 1100 gas lights operate in areas that have been named historic districts. Gas lights also operate in parts of the famed French Quarter and outside historic homes throughout the city in New Orleans.
South Orange, New Jersey, has adopted the gaslight as the symbol of the town, and uses them on nearly all streets. Several other towns in New Jersey also retain gas lighting: Glen Ridge, Palmyra, Riverton, and some parts of Orange, Cape May and Cherry Hill. The village of Riverside, Illinois, still uses its original gas street lights that are an original feature of the  Frederick Law Olmsted planned community. Manhattan Beach, California, has a gas lamp section in which all the sidewalks are lit by public gas lamps. Disneyland has authentic 19th century gas lamps from Baltimore along the "Main Street, U.S.A." section of the theme park.
Many gas utility companies will still quote a fixed periodic rate for a customer-maintained gas lamp, and some homeowners still use such devices. However, the high cost of natural gas lighting at least partly explains why a large number of older gas lamps have been converted to electricity. Solar-rechargeable battery-powered gas light controllers can be easily retrofitted into existing gas lamps to keep the lights off during daylight hours and cut energy consumption and green-house gas carbon emissions by 50%.

Lamplighters in art
		
		
		
		


== Further reading ==
Carl Benedikt Frey. 2019. The Technology Trap: Capital, Labor, and Power in the Age of Automation. Princeton University Press.


== Notes ==


== External links ==
 Media related to Lamplighters at Wikimedia Commons