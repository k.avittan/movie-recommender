A bowling alley (also known as a bowling center, bowling lounge, bowling arena, or historically bowling club) is a facility where the sport of bowling is played. Bowling alleys contain long and narrow synthetic or wooden lanes. The number of lanes inside a bowling alley is variable. With 116 lanes, the Inazawa Grand Bowl in Japan is the largest bowling alley in the world. Before World War II, manual pinsetters were used at bowling alleys to set up the pins for ten-pin lanes. Modern ten-pin bowling alleys have automatic mechanical pinsetters. Bowling alleys are predominantly used by families for recreation and are found worldwide, often in suburban shopping centers and urban areas. Modern bowling alleys usually offer other games (often billiard tables, darts and arcade games) and may serve food or beverages, usually via vending machines or an integrated bar or restaurant.


== History ==
In 1840, the first indoor bowling alley opened—Knickerbocker Alleys in New York City. Instead of wood, this indoor alley used clay for the bowling lane. By 1850, there were more than 400 bowling alleys in New York City, which earned it the title "bowling capital of North America". Because early versions of bowling were difficult and there were concerns about gambling, the sport faltered. Several cities in the United States regulated bowling due to its association with gambling.In the late 19th century, bowling was revived in many U.S. cities. Alleys were often located in saloon basements and provided a place for working-class men to meet, socialize, and drink alcohol. Bars were and still are a principal feature of bowling alleys. The sport remained popular during the Great Depression and, by 1939, there were 4,600 bowling alleys across the United States. New technology was implemented in alleys, including the 1952 introduction of automatic pinsetters (or pinspotters), which replaced pin boys who manually placed bowling pins. Today, most bowling alley facilities are operated by Bowlero Corporation or Brunswick Bowling & Billiards.
In 2015, over 70 million people bowled in the United States.


== Modern day bowling alleys ==

Upon entering a bowling alley, patrons stop off at a cashier to purchase games and rent bowling shoes. The shoe rental is often located near the main entrance. Vending machines, bar areas, billiards tables, arcade games, pro shops and party rooms are common features of modern bowling alleys. Each lane has an overhead monitor/television screen to display bowling scores as well as a seating area and tables for dining and socializing. Some bowling alleys offer live music, often performed by a disc jockey, a small band, or (rarely) a solo pianist.


== References ==


== External links ==
U.S. Bowling Alleys Database