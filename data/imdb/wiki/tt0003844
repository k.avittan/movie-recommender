James Adam Belushi (; born June 15, 1954) is an American actor, comedian, singer and musician. He played the role of Jim on the sitcom According to Jim (2001–2009). His other television roles include Saturday Night Live (1983–1985), Wild Palms (1993), Aaahh!!! Real Monsters (1994–1997), Show Me a Hero (2015) and Twin Peaks (2017).
Belushi has appeared in films such as Thief (1981), Trading Places (1983), About Last Night (1986), Salvador (1986), The Principal (1987), Red Heat (1988), K-9 (1989), Mr. Destiny (1990), Curly Sue (1991), Jingle All the Way (1996), Gang Related   (1997), Joe Somebody (2001), Underdog (2007), The Ghost Writer (2010) and Katie Says Goodbye (2019).
He is the younger brother of comic actor John Belushi and the father of actor Robert Belushi.


== Early life ==
Belushi was born in Chicago, to Adam Anastos Belushi, an Albanian from Qytezë, Korçë, and Agnes Demetri (Samaras) Belushi, who was born in Ohio to Albanian immigrants from Korçë. He was raised in Wheaton, a Chicago suburb, along with his three siblings: older brother John, older sister Marian, and younger brother Billy. After graduating from Wheaton Central High School, Jim Belushi attended the College of DuPage, and graduated from Southern Illinois University Carbondale with a degree in Speech and Theater Arts.


== Career ==

From 1977 to 1980, Belushi, like his older brother John Belushi, worked with the Chicago theater group The Second City. During this period, Belushi made his television debut in 1978's Who's Watching the Kids and also had a small part in Brian De Palma's The Fury. His first significant role was in Michael Mann's Thief (1981). After his elder brother John's death, from 1983 to 1985 he appeared on Saturday Night Live; he portrayed characters such as Hank Rippy from "Hello, Trudy!" and "That White Guy". Belushi also appeared in the film Trading Places as a drunk man in a gorilla suit during a New Year's Eve party. He made a guest appearance in Faerie Tale Theatre's third-season episode Pinocchio, starring Paul Reubens as the titular puppet.
Belushi rose to greater prominence with his supporting roles in The Man with One Red Shoe (1985), About Last Night..., Salvador and Little Shop of Horrors (as Patrick Martin) (all 1986), which opened up opportunities for lead roles. He has starred in films including Real Men, The Principal, Red Heat, Homer and Eddie, K-9, Dimenticare Palermo, Taking Care of Business, Mr. Destiny, Only the Lonely, Curly Sue, Once Upon A Crime, Wild Palms, Race the Sun, Jingle All The Way, Separate Lives, Retroactive, Gang Related, Angel's Dance and Joe Somebody (2001).
His voice work includes The Mighty Ducks, The Pebble and the Penguin, Babes in Toy land, Gargoyles and Hey Arnold!, and Hoodwinked, Scooby-Doo! and the Goblin King and The Wild. He also lent his vocal talents for 9: The Last Resort (a PC game released in 1995), in which he portrayed "Salty", a coarse yet helpful character. In 1997, he portrayed the "Masked Mutant" in the Goosebumps PC video game, alongside Adam West as "The Galloping Gazelle". On January 4, 2001, Belushi appeared on the ER episode "Piece of Mind". The episode focused on both Dr. Mark Greene's life-or-death brain surgery in New York and Belushi's character, who had been in a car accident with his son in Chicago. Belushi's performance contributed to his re-emergence in the public eye, and the following year he was cast as the title role in ABC's According to Jim. His first animation voice-over was as a pimple on Krumm's head in Aaahh!!! Real Monsters on Nickelodeon. That performance led him to be cast in the continuing role as Simon the Monster Hunter in that series, where he ad-libbed much of his own dialogue.

In 2003, Belushi and Dan Aykroyd released the album Have Love, Will Travel, and participated in an accompanying tour. The concert was made available on video on demand by Bob Gold & Associates. He also performs at various venues nationwide as Zee Blues in an updated version of The Blues Brothers. He released his first book, Real Men Don't Apologize, in May 2006. Belushi was a narrator of an NFL offensive linemen commercial. Belushi also introduced the starting lineups for the University of Illinois football team during ABC's telecast of the 2008 Rose Bowl.
He appeared in MC Hammer's video "Too Legit to Quit" in 1991 (in the extended full-length version). He also hosted a celebration rally for the Chicago Cubs playoff series in Chicago prior to the 2008 World Series.
Steve Dahl has dubbed him "The Funniest Living Belushi."
In 2010, Belushi was cast in a pilot for CBS called The Defenders a series about defense lawyers. The one-hour series premiered on September 22, 2010. In two episodes in 2011, Belushi was paired with Blues Brothers partner Dan Aykroyd. On May 15, 2011, The Defenders was canceled by CBS. In 2011, he was cast as corrupt businessman Harry Brock in Born Yesterday, which opened on Broadway in late April.In August 2020, Belushi started a series about his life at his cannabis farm in Oregon, called Growing Belushi.


== Personal life ==
Belushi has been married three times. On May 17th, 1980, he married Sandra Davenport, who gave birth to his son, Robert James, on October 23, 1980. Belushi and Davenport divorced in 1988. Belushi was married to actress Marjorie Bransfield from 1990 to 1992. He married Jennifer Sloan on May 2, 1998, the couple have a daughter and a son. On March 5, 2018, Jennifer Sloan filed for divorce from Belushi. The two have since reconciled. Belushi is closely linked to his Albanian heritage and received honorary Albanian citizenship from the President of Albania, Bamir Topi. He is Eastern Orthodox Christian, visiting with the Ecumenical Patriarchate of Constantinople in 2010. Belushi is an avid fan of the Chicago Blackhawks, Chicago Bears, Chicago Cubs, Chicago Bulls and the Chicago Fire.He had a legal battle and publicized feud with his neighbor, actress Julie Newmar. She claimed their conflicts stemmed from Belushi's attempt to "build a second house in the back", which she claimed was illegal in their R-1 neighborhood, since there can be only one house per lot. In 2004, Belushi filed a $4 million lawsuit against Newmar, alleging "she has harassed and defamed him". He also claimed she destroyed his fence, which Newmar denied. They ended the feud in 2006 and Belushi invited Newmar to guest-star on According to Jim on an episode which satirized their conflict.In 2011, Belushi announced that he suffered from gout, and became a spokesman for Savient Pharmaceuticals' educational campaign "Check Out Your Gout". He has also appeared on the cover of and been interviewed by Cigar Aficionado magazine.Belushi endorsed the re-election campaign of Democratic President Barack Obama in 2012. On a Fox News interview, he explained "When you talk to the President in private, he's a cool guy, who knows what he's doing. Besides, I'm from Chicago too."Belushi built a holiday home in Eagle Point, Oregon in 2015 where he now grows cannabis. By 2018, the size of his Eagle Point property had been expanded from 13 acres (5.3 ha) to 93 acres (38 ha). He has been involved with fundraising for projects in Eagle Point and elsewhere in Southern Oregon, including the planned rebuilding of the Butte Creek Mill and the restoration of the Holly Theatre, in Medford.  In 2018, he was still living in Los Angeles "most of the time", The Oregonian reported, and living in Oregon only part of the year. Per a 2018 article, Belushi indicated he planned on opening a pop-up cannabis dispensary in downtown Portland.


== Filmography ==


=== Film ===


=== Television ===


=== Video games ===


== Characters on Saturday Night Live ==
Hank Rippy (Hello, Trudy!)
Man on the Street Jesse Donnelly
That White Guy, a stereotypical Caucasian man who can rap


=== Celebrity impersonations ===
Rosemary Clooney
Joe Cocker
Joan Collins
Bob Guccione
Hulk Hogan
Pope John Paul II
Bob Keeshan as Captain Kangaroo
Josef Mengele
Willie Nelson
Thomas Noguchi
Michael Reagan
Babe Ruth
Arnold Schwarzenegger


== References ==


== External links ==
Jim Belushi official website
Jim Belushi on IMDb
Jim Belushi at AllMovie
Jim Belushi at the Internet Broadway Database