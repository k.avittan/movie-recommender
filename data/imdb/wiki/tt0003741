Tom Mix (1880–1940) was an American motion picture actor, director, and writer whose career spanned from 1910 to 1935.  During this time he appeared in 270 films and established himself as the screen's most popular cowboy star.   Mix's flair for showmanship set the standard for later cowboy heroes such as Gene Autry and Roy Rogers.  His horse Tony also became a celebrity who received his own fan mail.Born in Pennsylvania, Mix served in the United States Army before moving to the Oklahoma Territory in 1902.  Three years later, after working as a physical fitness instructor, bartender, and peace officer, he was hired as a full-time cowboy for the Miller Brothers 101 Ranch.  Soon after, Mix established himself as the star attraction of the Millers' Wild West Show. In early 1910 Mix agreed to work as an actor and wrangler for the Selig Polyscope Company. Mix alternated between working in films and in Wild West shows until 1913 when he went into the film business full-time. The following year he established his own production unit and became a director and writer as well as an actor.  His films  for Selig were usually one and two-reel shorts that initially emphasized humor in the tradition of Will Rogers but eventually moved into action-oriented stories that displayed Mix's riding and stunting prowess.In 1917 Mix left Selig and signed a contract with the Fox Film Corporation. His earliest films for them were two-reel shorts but within a year he switched to features.  During his peak period in the 1920s Mix appeared in action-packed westerns filled with fights and chases which often showed him doing his own stunt work. His films were also known for their location work in places such as Arizona's Grand Canyon and Colorado's Royal Gorge. Among the notable directors that he worked with during this time were John Ford, Sidney Franklin, Jack Conway, George Marshall, and Roy William Neill.  His leading ladies included Louella Maxam, Colleen Moore, Esther Ralston, Laura La Plante, Billie Dove and Clara Bow. In 1928, after eleven years and 86 films with Fox, Mix moved to FBO Pictures for a series of five films. With the advent of sound, Mix abandoned his film career and returned to working in Wild West shows. He came back to films in 1932 for a series of nine sound features for Universal. In 1935, he appeared in his final film, a 15-chapter serial for Mascot Pictures. Mix died in an automobile accident in Arizona five years later.On February 8, 1960, Mix was awarded a star on the Hollywood Walk of Fame. In 1998, Mix's film Sky High was added to the National Film Registry by the Library of Congress.


== Selig Polyscope Company ==
In 1910, Will A. Dickey, owner of the Circle D Ranch Wild West Show and Indian Congress, signed a deal to provide stock and wranglers for the Selig motion picture company. Dickey had seen Mix perform with the Miller Brothers 101 Ranch Wild West Show and asked him if he would be interested in appearing in films. Mix agreed and soon after joined the Selig unit in Flemington, Missouri. Mix's earliest films were made as part of one of Selig's traveling units. Location work for these films was done in Missouri and Oklahoma while at least one film was made in Chicago.  Otis Turner, the director of these films, was impressed with Mix's screen image and wanted to keep him working as a film actor. Mix, however, was not interested in remaining in films and signed with Zack Mulhall's Wild West Show to help organize the Appalachian Exposition in Knoxville, Tennessee, which was scheduled to run in October and November.  After this Mix rejoined the Miller Brothers 101 Ranch Show.In the spring of 1911 Mix left the Miller Brothers 101 Ranch Show and resumed his career as an actor with the Selig Polyscope Company. Early in 1912 Mix left the film industry and joined Guy Weadick, one of his former 101 Ranch associates, to stage the first Calgary Stampede in Alberta, Canada. After this Mix toured Canada with the Buffalo Ranch Wild West Show. Following the show's closure Mix returned to Dewey, Oklahoma and accepted the position of night marshal. All of Mix's 1912 films are one-reel shorts directed by Otis Turner. 
In January 1913 Mix accepted an offer from Selig to return to acting in films as part of a production unit in Prescott, Arizona, until the supervision of actor-director William Duncan. During his time with this unit Duncan persuaded Mix to write some of the scenarios. All of Mix's 1913 films were directed by Duncan and, except where noted, are one-reel shorts. The majority of these films co-starred Myrtle Stedman and Lester Cuneo. The last films that Mix made with Duncan were released in early 1914.  By that time Mix had been reassigned work with Colin Campbell, one of Selig's top directors, to appear in films produced in Truckee, California. These films were largely two or three reels in length and attracting more attention than Selig's average releases. Later in 1914 Selig gave Mix his own unit, which allowed him to write, direct and star in films made in Glendale, California.Mix returned to Arizona in 1915. The Selig Company began to experience a decline in its profits around this time, due in part to World War I cutting off its foreign market. As a result, Selig began trimming its production schedule. All films were directed by Mix and, except where noted, are one-reel shorts. In 1916 Mix moved to Las Vegas, New Mexico, which, at the time, was the last remaining open range in the West. In June 1916 he moved again, this time back to Glendale. With pressure from Selig to reduce costs in his unit, Mix began searching for a new producer. By the end of the year he had signed with the Fox Film Corporation. Mix's final films with Selig were released in early 1917.
All of Mix's Selig films are one-reel shorts except where noted. Most of these films are now lost.  Those that are known to survive are listed in the notes section.


== Fox Film Corporation ==
Mix moved from Selig to the Fox Film Corporation in 1917, starting at a salary of $350 per week.  His earliest films for the studio were two-reel shorts similar to the ones he made at Selig.  Within a year, however he switched to feature films.  Mix's popularity soared at Fox and his salary eventually escalated to $17,000 per week.Initially Mix worked as a writer and director as well as an actor but eventually restricted his work largely to being in front of the cameras.  As his Fox films often teamed him with notable directors (such as John Ford, Sidney Franklin, Jack Conway, George Marshall, and Roy William Neill) an extra column now appears to list them.  As the survival rate of Mix's Fox films is higher than his Selig films another column is added to display their survival status.
Except where noted all films are five reel features.


== FBO Pictures ==
By 1927, numerous low-budget imitations of Mix's films were flooding the cinema market.  This, along with Mix's salary, the high rental fees for his films, and Fox Films' commitment to sound films made the studio decide not to renew his contract. Following his departure from Fox, Mix went on a vaudeville tour with the Keith-Albee-Orpheum circuit.  In 1928, Mix signed with the Film Booking Offices of America (FBO) studios to appear in six silent westerns.  By the time his first film, The Son of the Golden West, was released FBO had merged with RKO Pictures.  Mix's FBO films were not as well received by the public and press as his Fox films.  As a result, after making five films for the studio production of the proposed sixth film, The Dude Ranch, was cancelled. Prints of all five of Mix's FBO films survive.


== Universal Pictures ==
Following his departure from FBO Mix returned to the vaudeville circuit, followed by two years with the Sells Floto Circus.  In November 1931, Mix received an offer from Carl Laemmle of Universal Studios to star in a series of sound westerns.  The resulting nine films (plus a cameo appearance in a tenth) proved to be popular at the box office. In December 1932, however, Mix ended his association with Universal due to injuries and a bout of influenza.  All of Mix's Universal films survive (see references for each film).


== Mascot ==
Following his departure from Universal, Mix returned to live performances.  In 1934, he joined with showman Sam Gill to form the "Tom Mix Wild West and Sam Gill Circus (Combined)".  Following Gill's death from a heart attack Mix bought out his late partner's ownership.  To help finance this deal Mix signed with film producer Nat Levine's Mascot Pictures to appear in a western serial.  Although a major box office hit, the resulting effort was Mix's final film.


== Other film appearances ==
Mix appeared as himself in two early sound short films that were part of the "Voice of Hollywood" series. Both were produced by Tiffany-Stahl Productions and released in 1930.


== Archives with Mix films ==
The following film archives have prints of Tom Mix films.  The abbreviations are used in the survival status section for the Fox films and in the notes section for all other films.


== References ==


=== Citations ===


=== Bibliography ===
Main source

Birchard, Robert S. (1993). King Cowboy: Tom Mix and the Movies. Burbank, CA: Riverwood Press. ISBN 978-1-880756-05-8.CS1 maint: ref=harv (link)Secondary sources

Barbour, Alan G. (1971). The Thrill of It All. New York, NY: Collier Books. ISBN 978-0-02-012040-7.CS1 maint: ref=harv (link)
Brownlow, Kevin (February 12, 1979). The War, the West and the Wilderness (1st ed.). New York, NY: Knopf. ISBN 978-0-394-48921-6.CS1 maint: ref=harv (link)
Buscombe, Edward, ed. (October 13, 1988). The BFI Companion to the Western. New York, NY: Da Capo Press. ISBN 978-0-233-98332-5.CS1 maint: ref=harv (link)
Dickens, Homer (August 1971). The Films of Gary Cooper. Secaucus, New Jersey: Citadel Press. ISBN 978-0-8065-0010-2.CS1 maint: ref=harv (link)
Everson, William K. (1992). The Hollywood Western. Secaucus, New Jersey: Citadel Press. ISBN 978-0-8065-1256-3.CS1 maint: ref=harv (link)
Layton, James; Pierce, Davis (February 24, 2015). The Dawn of Technicolor 1915–1935. Rochester, New York: George Eastman Museum. ISBN 978-0-935398-28-1.CS1 maint: ref=harv (link)


== External links ==
Tom Mix at IMDb