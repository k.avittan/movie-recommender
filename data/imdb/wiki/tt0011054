The Children Nobody Wanted is a 1981 American made-for-television drama film based on the true story of child advocate Tom Butterfield (1940–1982), the youngest bachelor to become a legal foster parent in the state of Missouri, and his creation of the Butterfield Ranch.


== Plot ==
Working in a state mental hospital, nineteen-year-old Tom Butterfield (Fredric Lehne), befriends an abandoned eight-year-old boy for whom authorities cannot find a foster family. Compelled by the child’s plight, Butterfield and supportive girlfriend Jennifer Williams (Michelle Pfeiffer) transform a dilapidated country club in Marshall, Missouri, into the Butterfield Ranch for homeless boys.


== Cast ==


== Tom Butterfield ==
Butterfield began the lengthy process of developing his story for the screen, moving to Los Angeles in 1978. By this stage, his ranch was a non-for-profit organisation and received almost $1 million a year for four homes in Marshall. "When I was in college," he once said, "I didn't intend to be a foster parent or get into social work and start a boys home." He added at the time of the telemovie's production, "I didn’t really start out twenty years ago with the intent to make a movie."He continued to work in Los Angeles to raise funds for Butterfield Youth Services Inc., until being hospitalised on 15 November 1982 for acute pneumonia and respiratory problems. He died at Cedars-Sinai Medical Center in Los Angeles on 13 December 1982 following a biopsy to clean out a congested lung, which caused his body to go into shock. He was forty-two.


== Production significance ==
The movie was Michelle Pfeiffer's first starring role and added to her acting resume and small screen presence. Agent John LaRocca "landed her roles in three television films" in 1981. Audiences were exposed to Pfeiffer in Callie and Son (13 October on CBS) and the remake of Splendor in the Grass (26 October on NBC) as well as The Children Nobody Wanted. Pfeiffer would continue to appear on television in one-off performances of established programmes, but The Children Nobody Wanted was her final made-for-television movie and signified the end of her association with LaRocca. Switching agents secured Pfeiffer’s leading role in Grease 2 (1982).The production also marked Megan Mullally's acting debut.
The film is available on Region 1 DVD, upon whose cover a solitary Pfeiffer appears.


== References ==


== External links ==
The Children Nobody Wanted on IMDb