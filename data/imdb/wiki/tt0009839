Noriyuki "Pat" Morita  (June 28, 1932 – November 24, 2005) was a Japanese-American actor, voice actor, and comedian. He was known for his roles as Matsuo "Arnold" Takahashi on Happy Days (1975–1983), Mr. Miyagi in The Karate Kid film series, Mike Woo in The Mystery Files of Shelby Woo, and The Emperor of China in Mulan and Mulan II. Morita was nominated for the 1985 Academy Award for Best Supporting Actor for his portrayal of Mr. Miyagi in The Karate Kid. Morita also portrayed Ah Chew in Sanford and Son (1974–1976). Morita was the series lead actor in the television program Mr. T and Tina (1976) and in Ohara (1987–1988), a police-themed drama. The two shows made history for being among the few TV shows with an Asian American series lead.


== Early life ==
Morita was born in Isleton, California in 1932. Morita's father Tamaru, born in 1897, had immigrated to California from Kumamoto Prefecture on the Japanese island of Kyushu in 1915. Tamaru's wife Momoe, born in 1903, had emigrated to California in 1913. Noriyuki, as Pat was named, had a brother named Hideo (Harry) who was twelve years older.Morita developed spinal tuberculosis (Pott disease) at the age of two and spent the bulk of the next nine years in the Weimar Institute in Weimar, California and later at the Shriners Hospital in San Francisco. For long periods he was wrapped in a full-body cast and was told that he would never walk. During his time at a sanatorium near Sacramento, Morita befriended a visiting priest who would often joke that, if Morita ever converted to Catholicism, the priest would rename him to "Patrick Aloysius Ignatius Xavier Noriyuki Morita". Released from the hospital at age 11 after undergoing extensive spinal surgery and learning how to walk, Morita was transported from the hospital directly to the Gila River camp in Arizona to join his interned family. After about a year and a half, he was transferred to the Tule Lake War Relocation Center.After World War II ended, Morita moved back to the Bay Area and he graduated from Armijo High School in Fairfield, California in 1949. For a time after the war, the family operated Ariake Chop Suey, a restaurant in Sacramento, California, jokingly described by Morita years later as "a Japanese family running a Chinese restaurant in a black neighborhood with a clientele of blacks, Filipinos and everybody else who didn’t fit in any of the other neighborhoods".  Morita would entertain customers with jokes and serve as master of ceremonies for group dinners.  After Morita’s father was killed in 1956 by a brutal hit-and-run accident while walking home from an all-night movie, Morita and his mother kept the restaurant going for another three or four years.  Needing a regular job to support his wife and a newly born child, Morita became a data processor in the early 1960s with the Department of Motor Vehicles and other state agencies, graduating to a graveyard shift job at Aerojet General.  In due time he was a department head at another aerospace firm Lockheed, handling the liaison between the engineers and the programmers who were mapping out lunar eclipses for Polaris and Titan missile projects.However, Morita suffered from occupational burnout and decided to quit his job and try show business.  He began working as a stand-up comedian at small clubs in Sacramento and San Francisco, and took the stage name "Pat Morita", in part due to the presence of comedians including Pat Henry and Pat Cooper, and in part due to memories of the priest he had befriended as a boy.  Morita struggled for many years in comedy, until fellow performer — ventriloquist Hank Garcia — told him to try his luck in Los Angeles.  Sally Marr, Lenny Bruce's mother, acted as his agent and manager after he moved to Los Angeles, and booked him in the San Fernando Valley and at the Horn nightclub in Santa Monica.  Morita sometimes worked as the opening act for singers Vic Damone and Connie Stevens and for his mentor, the comedian Redd Foxx.  Foxx later gave him a role on his sitcom Sanford and Son in the early 1970s.


== Television and movie career ==


=== Early work ===
Morita's first movie roles were as a henchman in Thoroughly Modern Millie (1967) and a similar role in The Shakiest Gun In The West (1968), starring Don Knotts. Later, a recurring role as South Korean Army Captain Sam Pak on the sitcom M*A*S*H (1973, 1974) helped advance the comedian's acting career. He also was cast as Rear Admiral Ryunosuke Kusaka in the war film Midway (1976).

He had a recurring role on the show Happy Days as Matsuo "Arnold" Takahashi, owner of the diner Arnold's for the show's third season (1975–1976) and made guest appearances in 1977 and 1979, during the show's fourth and sixth season, respectively. After the season's end, he left the show to star as inventor Taro Takahashi in his own show Mr. T and Tina, the first Asian-American sitcom on network TV. The sitcom was placed on Saturday nights by ABC and was quickly canceled after a month in the fall of 1976. Morita revived the character of Arnold on Blansky's Beauties in 1977 and eventually returned to Happy Days during the tenth season (1982-1983), and appeared in one episode during the final season. Morita had another notable recurring television role on Sanford and Son (1974–1976) as Ah Chew, a good-natured friend of Lamont Sanford.


=== The Karate Kid ===
Morita gained particular fame playing wise karate teacher Mr. Miyagi, who taught young "Daniel-san" (Ralph Macchio) the art of Goju-ryu karate in The Karate Kid. He was nominated for an Academy Award for Best Supporting Actor and a corresponding Golden Globe Award, reprising his role in three sequels: The Karate Kid Part II (1986), The Karate Kid Part III (1989) and The Next Karate Kid (1994), the last of which starred Hilary Swank instead of Macchio. Though he was never a student of karate, he learned all that was required for the films. Although he had been using the name Pat for years, producer Jerry Weintraub suggested that he be billed with his given name to sound "more ethnic." Morita put this advice into practice and was recognized as Noriyuki "Pat" Morita at the 57th Academy Awards ceremony. Weintraub did not want to cast Morita for the part of Mr. Miyagi, wanting a dramatic actor for the part and labeling Morita a comedic actor. Morita eventually tested five times before Weintraub himself offered him the role.


=== Post-Karate Kid ===

Morita went on to play Tommy Tanaka in the Kirk Douglas-starring television movie Amos, receiving his first Primetime Emmy Award nomination and second Golden Globe Award nomination for the role. He then starred in the ABC detective show Ohara (1987–1988); it was cancelled after two seasons due to poor ratings. He then wrote and starred in the World War II romance film Captive Hearts (1987). Morita hosted the educational home video series Britannica's Tales Around the World (1990–1991). Later in his career Morita starred on the Nickelodeon television series The Mystery Files of Shelby Woo (1996–1998), and had a recurring role on the sitcom The Hughleys (2000). He also made a guest appearance on a 1996 episode of Married... with Children. He went on to star in the short film Talk To Taka as a sushi chef who doles out advice to anyone who will hear him. Morita voiced the Emperor of China in Disney's 36th animated feature Mulan (1998) and reprised the role in Mulan II (2004), a direct-to-video sequel and Kingdom Hearts II.Morita had a cameo appearance in the 2001 Alien Ant Farm music video "Movies". Morita's appearance in the video spoofed his role in The Karate Kid. In 2002, he made a guest appearance on an episode of Spy TV.  In 2003, he had a cameo on an episode of Yes, Dear, as an unnamed karate teacher, potentially being Miyagi. He would also reprise his role (to an extent) in the stop-motion animated series Robot Chicken in 2005.
One of Morita's last television roles was as Master Udon on the 2006 SpongeBob SquarePants Season 4 episode, "Karate Island". The episode was dedicated to him, airing about 6 months after his death. 
One of his last film roles was in the independent feature film Only the Brave (2006), about the 442nd Regimental Combat Team, where he plays the father of lead actor (and director) Lane Nishikawa. About this time he also starred in a Michael Sajbel movie called Remove All Obstacles (2010) as a cold storage guru. This was a 9-minute industrial short advertising doors used for cold storage warehouses. Pat also took a small role in the independent film Act Your Age, filmed in central Illinois and released in April 2011. His last movie was Royal Kill (2009), starring Eric Roberts, Gail Kim, and Lalaine, directed by Babar Ahmed.


== Death ==
Morita died of complications of alcoholism on November 24, 2005, at his home in Las Vegas, Nevada, at the age of 73. Throughout his life, Morita had battled alcoholism. He is survived by Evelyn, his wife of 11 years, and three daughters from his previous marriage.Morita was cremated at Palm Green Valley Mortuary and Cemetery in Las Vegas, Nevada.


== Dedicated TV episodes ==
The SpongeBob SquarePants Season 4 episode "Karate Island" (original air date May 12, 2006), for which he voiced Master Udon, was dedicated in his memory at the end of the episode before the end credits.
The fifth episode of the series Cobra Kai was also dedicated to his memory.


== Filmography ==


=== Posthumous credits ===


== References ==


== External links ==

Pat Morita on IMDb
Pat Morita at the TCM Movie Database 
Pat Morita at AllMovie
Pat Morita at Find a Grave
"Pat Morita, 73, Actor Known for 'Karate Kid' and 'Happy Days,' Dies", The New York Times, November 26, 2005
Pat Morita on People.com
Pat Morita's hip, but no hippie at the Wayback Machine (archived August 8, 2009)
Pat Morita at The Interviews: An Oral History of Television