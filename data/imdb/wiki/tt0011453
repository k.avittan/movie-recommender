A medicine man or medicine woman is a traditional healer and spiritual leader who serves a community of indigenous people of the Americas. Individual cultures have their own names, in their respective Indigenous languages, for the spiritual healers and ceremonial leaders in their particular cultures.


== The medicine man and woman in North America ==


=== Cultural context ===

In the ceremonial context of Indigenous North American communities, "medicine" usually refers to spiritual healing. Medicine men/women should not be confused with those who employ Native American ethnobotany, a practice that is very common in a large number of Native American and First Nations households.The terms "medicine people" or "ceremonial people" are sometimes used in Native American and First Nations communities, for example, when Arwen Nuttall (Cherokee) of the National Museum of the American Indian writes, "The knowledge possessed by medicine people is privileged, and it often remains in particular families."Native Americans tend to be quite reluctant to discuss issues about medicine or medicine people with non-Indians. In some cultures, the people will not even discuss these matters with Indians from other tribes. In most tribes, medicine elders are prohibited from advertising or introducing themselves as such. As Nuttall writes, "An inquiry to a Native person about religious beliefs or ceremonies is often viewed with suspicion." One example of this is the Apache medicine cord or Izze-kloth whose purpose and use by Apache medicine elders was a mystery to nineteenth century ethnologists because "the Apache look upon these cords as so sacred that strangers are not allowed to see them, much less handle them or talk about them."The 1954 version of Webster's New World Dictionary of the American Language reflects the poorly-grounded perceptions of the people whose use of the term effectively defined it for the people of that time: "a man supposed to have supernatural powers of curing disease and controlling spirits." In effect, such definitions were not explanations of what these "medicine people" are to their own communities but instead reported on the consensus of socially and psychologically remote observers when they tried to categorize the individuals. The term "medicine man/woman," like the term "shaman," has been criticized by Native Americans, as well as other specialists in the fields of religion and anthropology.
While non-Native anthropologists sometimes use the term "shaman" for Indigenous healers worldwide, including the Americas, "shaman" is the specific name for a spiritual mediator from the Tungusic peoples of Siberia and is not used in Native American or First Nations communities.
The term "medicine man/woman" has also frequently been used by Europeans to refer to African traditional healers, along with the offensive term "witch-doctors".


== Frauds and scams ==
There are many fraudulent healers and scam-artists who pose as Cherokee "shamans", and the Cherokee Nation has had to speak out against these people, even forming a task force to handle the issue. In order to seek help from a Cherokee medicine person a person needs to know someone in the community who can vouch for them and provide a referral. Usually one makes contact through a relative who knows the healer.


== See also ==


== Notes ==


== External links ==
New Age Frauds & Plastic Shamans, an organization devoted to discussing fraudulent medicine-people