The right to keep and bear arms (often referred to as the right to bear arms) is a right for people to possess weapons (arms) for their own defense. Only a few countries support the idea that their people have a right to keep and bear arms and protect it on a statutory level, and even fewer protect such a right on a constitutional level.


== Background ==
The Bill of Rights 1689 allowed Protestant citizens of England to "have Arms for their Defence suitable to their Conditions and as allowed by Law" and restricted the ability of the English Crown to have a standing army or to interfere with Protestants' right to bear arms "when Papists were both Armed and Imployed contrary to Law" and established that Parliament, not the Crown, could regulate the right to bear arms.Sir William Blackstone wrote in the 18th century that the right to have arms was auxiliary to the "natural right of resistance and self-preservation" subject to suitability and allowance by law. The term arms as used in the 1600s, the term refers to the process of equipping for war. It is commonly used as a synonym for weapon.Inclusion of this right in a written constitution is uncommon. In 1875, 17 percent of constitutions included a right to bear arms. Since the early twentieth century, "the proportion has been less than 9 percent and falling". In an article titled "U.S. Gun Rights Truly Are American Exceptionalism," a historical survey and comparative analysis of constitutions dating back to 1789, Tom Ginsburg and colleagues "identified only 15 constitutions (in nine countries) that had ever included an explicit right to bear arms. Almost all of these constitutions have been in Latin America, and most were from the 19th century".


== Americas ==


=== Colombia ===
Colombians can not bear arms unless the government allows it. The 1993 gun law states that "only the government can import, export, manufacture, and market firearms, ammunition, explosives and their components, arms making equipment, and all activities thereto pertaining". The state agency in charge of weapons and explosives manufacture, and main supplier to the Colombian Armed Forces is Indumil.


=== Guatemala ===

The right to own weapons for personal use, not prohibited by the law, in the place of in habitation, is recognized. There will not be an obligation to hand them over, except in cases ordered by a competent judge.
While protecting the right to keep arms, Guatemalan constitution specifies that this right extends only to "weapons not prohibited by law".


=== Honduras ===
Although not explicitly protected by the constitution, the right to keep and bear arms is conditionally guaranteed by Honduran Statute law.
Every person, in the exercise of their civil rights, may request a maximum of five (5) licences for the possession and carrying of up to five (5) firearms by submitting an application with the following information:
1) Form with personal information and residence;
2) Brand, model, serial number, identification of modification of calibre, if any; as well as any other characteristics of the weapon;
3) Proof of having undertaken a ballistic test;
4) Payment of municipal matriculation and criminal background check; and, 

5) Identification documents.


=== Mexico ===
The inhabitants of the United Mexican States have the right to possess arms within their domicile, for their safety and legitimate defense, except those forbidden by Federal Law and those reserved for the exclusive use of the Army, Militia, Air Force and National Guard. Federal law shall provide in what cases, conditions, under what requirements and in which places inhabitants shall be authorized to bear arms.

Mexican constitution of 1857 first included right to be armed. In its first version, the right was defined in similar terms as it is in the Second Amendment to the United States Constitution. A new Mexican constitution of 1917 relativized the right, stating that its utilization must be in line with local police regulations.
Another change was included in 1971 Constitution. Since then, Mexicans have the right to be armed only within their home and further utilization of this right is subject to statutory authorization in Federal law.


=== United States ===

A well regulated militia, being necessary to the security of a free state, the right of the people to keep and bear arms, shall not be infringed. 
Generally, where modern constitutions refer to arms at all, the purpose is "to allow the government to regulate their use or to compel military service, not to provide a right to bear them". Constitutions which historically guaranteed a right to bear arms are those of Bolivia, Colombia, Costa Rica, Guatemala, Honduras, Liberia, Mexico, Nicaragua and the United States of America. Nearly all of the Latin American examples were modeled on that of the United States. At present, out of the world's nearly 200 constitutions, three still include a right to bear arms: Guatemala, Mexico, and the United States; of these three, only the last does not include explicit restrictive conditions. Until 2008, the U.S. Supreme Court had never struck down a law aimed at regulating guns. The first ruling by the U.S. Supreme Court on the second amendment, United States v. Cruikshank in 1876, permitted states to restrict firearm ownership by minorities, ruling the first and second amendment only applied to the federal government, not state governments and laws. This ruling was overturned in 2008, as a result of the case of District of Columbia v. Heller and, two years later, McDonald v. Chicago, the U.S. Supreme Court held that states could not restrict the ownership of handguns for self-defense.


== Europe ==


=== Czech Republic ===
	
(1) Everyone has the right to life. Human life is worthy of protection even before birth. (2) Nobody may be deprived of her life. (3) The death penalty is prohibited. (4) Deprivation of life is not inflicted in contravention of this Article if it occurs in connection with conduct which is not criminal under the law. The right to defend own life or life of another person also with arms is guaranteed under conditions set out in the law.

	
Historically, the Czech lands were at the forefront of the spreading of civilian firearms ownership. In the 1420s and 1430s, firearms became indispensable tools for the mostly peasant Hussite armies, whose amateur combatants, including women, fended off a series of invasions of professional crusader armies of well-armored warriors with cold weapons. Throughout and after the Hussite wars, firearms design underwent fast development and their possession by civilians became a matter of course. First firearms regulation was enacted in 1517 as a part of general accord between the nobles and burghers and later in 1524 as a standalone Enactment on Firearms (zřízení o ručnicích). The 1517 law explicitly stated that "all people of all standing have the right to keep firearms at home" while at the same time enacting a universal carry ban. The 1524 enactment set out a process of issuing of permits for carrying of firearms and detailed enforcement and punishment for carrying without such a permit. Carrying later became again permitless until 1852, when Imperial Regulation No. 223 reintroduced carry permits. This law remained in force until 1939 German invasion.Since inception through the Hussite revolution, the right to keep firearms endured for over half-millennia until the Nazi gun ban during the German occupation in the 20th century. Firearms possession later became subject to government permission during the communist dictatorship with only those deemed loyal to the communist party being able to be armed. After return of liberty, the Czech Republic instated shall issue permitting process, under which all residents can keep and bear arms subject to fulfillment of regulatory conditions.In the Czech Republic, every resident that meets conditions laid down in Act No. 119/2002 Coll. has the right to have firearms license issued and can then obtain a firearm. Holders of D (exercise of profession) and E (self-defense) licenses, which are also shall-issue, can carry up to two concealed firearms for protection. The right to be armed is statutorily protected, however it is not listed in the constitution.
A proposal to have right to keep and bear arms included in the constitution was entered in the Czech Parliament in December 2016. The proposal was approved by vote of 139 to 9 on 28 June 2017 by the Chamber of Deputies. It later failed to reach necessary support in Senate, where only 28 out of 59 Senators present supported it (constitutional majority being 36 votes ). A new proposal was entered by 35 Senators in September 2019.


=== Switzerland ===
 
The Swiss have a statutory right to bear arms under Article 3 of the 1997 Weapons Act.Switzerland practices universal conscription, which requires that all able-bodied male citizens keep fully automatic firearms at home in case of a call-up. Every male between the ages of 20 and 34 is considered a candidate for conscription into the military, and following a brief period of active duty will commonly be enrolled in the militia until age or an inability to serve ends his obligation. Until December 2009, these men were required to keep their government-issued selective fire combat rifles and semi-automatic handguns in their homes as long as they were enrolled in the armed forces. Since January 2010, they have had the option of depositing their personal firearm at a government arsenal. Until September 2007, soldiers received 50 rounds of government-issued ammunition in a sealed box for storage at home; after 2007 only about 2,000 specialist troops are allowed to keep the ammunition at home.In a referendum in February 2011, voters rejected a citizens' initiative that would have obliged members of the armed services to store their rifles and pistols on military compounds and required that privately owned firearms be registered.


=== United Kingdom ===
the subjects which are Protestants may have arms for their defence suitable to their conditions and as allowed by law		

The right to keep and bear arms is not legally or constitutionally protected in the United Kingdom. While citizens may possess certain firearms on an appropriate licence, All handguns, automatic, and centerfire semi-automatic weapons are illegal to possess without special provisons.The English Bill of Rights 1689 allowed:

That the Subjects which are Protestants may have Arms for their Defence suitable to their Conditions and as allowed by Law.
The first serious control on firearms was established with the passing of the Firearms Act 1920.Since 1953, it has been a criminal offence in the United Kingdom to carry a knife (with the exception of non-locking folding knives with a cutting edge of 3 inches (7.62 centimetres) or less) or any "offensive weapon" in a public place without lawful authority or reasonable excuse. The cutting edge of a knife is separate to the blade length. The only manner in which an individual can carry arms is on private property or any property which the public does not have a lawful right of access as the law only creates the offence when it occurs in public e.g. a person's own home, private land, the area in a shop where the public have no access, etc. Furthermore, Section 141 Criminal Justice Act 1988 specifically lists all offensive weapons that cannot technically be owned - even on private property - by way of making it illegal to sell, trade, hire, etc. an offensive weapon to another person.Furthermore, the law does not allow an offensive weapon or ordinary item intended or adapted as an offensive weapon to be carried in public before the threat of violence arises. This would only be acceptable in the eyes of the law if the person armed themselves immediately preceding or during an attack (in a public place). This is known as a "weapon of opportunity" or "instantaneous arming".


== Other ==


=== Sharia law ===
Under Sharia law, there is an intrinsic freedom to own arms. However, in times of civil strife or internal violence, this right can be temporarily suspended to keep peace and prevent harm, as mentioned by Imam ash-Shatibi in his works on Maqasid ash-Shari'ah (The Intents and Purposes of Shari'ah) Citizens not practicing Islam are prohibited from bearing arms and are required to be protected by the military, the state for which they pay the jizyah. In exchange they do not need to pay the zakat.


=== Yemen ===

The Law Regulating Carrying Firearms, Ammunition & their Trade states that:
The citizens of the Republic shall have the right to hold the necessary rifles, machine guns, revolvers, and hunting rifles for their personal use with an amount of ammunition for the purpose of legitimate defense.
Firearms are both easily and legally accessible.


== Gun violence and the politics of the right to bear arms ==
Legal restrictions on the right to keep and bear arms are usually put in place by legislators because they believe that they will reduce gun related violence.  Their actions are frequently the result of political pressure for such controls. The Brady, Snowdrop Campaigns, and the Million Mom March are examples of campaigns calling for tighter restrictions on the right to keep and bear arms.
Accident statistics are hard to obtain, but much data is available on the issue of gun ownership and gun related deaths. The United Nations Interregional Crime and Justice Research Institute (UNICRI) has made comparisons between countries with different levels of gun ownership and investigated the correlation between gun ownership levels and gun homicides, and between gun ownership levels and gun suicides. A strong correlation is seen in both.

During the 1989 and 1992 International Crime Surveys, data on gun ownership in eighteen countries have been collected on which WHO data on suicide and homicide committed with guns and other means are also available. The results presented in a previous paper based on the fourteen countries surveyed during the first ICS and on rank correlations (Spearman's rho), suggested that gun ownership may increase suicides and homicides using firearms, while it may not reduce suicides and homicides with other means. In the present analysis, four additional countries covered by the 1992 ICS only have been included, and Pearson's correlation coefficients r have been used. The results confirm those presented in the previous study.

UNICRI also investigated the relationship between gun ownership levels and other forms of homicide or suicide to determine whether high gun ownership added to or merely displaced other forms of homicide or suicide. They reported that "widespread gun ownership has not been found to reduce the likelihood of fatal events committed with other means. Thus, people do not turn to knives and other potentially lethal instruments less often when more guns are available, but more guns usually means more victims of suicide and homicide." Speculating on possible causes the researchers concluded that "all we know is that guns do not reduce fatal events due to other means, but that they go along with more shootings. Although we do not know why exactly this is so, we have a good reason to suspect guns to play a—fatal—role in this".The research reporter found that guns were the major cause of homicides in 3 of the 14 countries it studied; Northern Ireland, Italy, and the United States. Although the data seem to indicate that reducing the availability of one significant type of arms—firearms—leads to reductions both in gun crimes and gun suicides and in overall crimes and overall suicides, the author did caution that "reducing the number of guns in the hands of the private citizen may become a hopeless task beyond a certain point", citing the American example.In contrast to the 1993 study however, a more recent study by UNICRI researchers from 2001 examined the link between household gun ownership and overall homicide, overall suicide, as well as gun homicide and gun suicide rates amongst 21 countries. Significant correlations between household gun ownership and rates of gun suicides for both genders, and gun homicide rates involving female victims were found. There were no significant correlations detected for total homicide and suicide rates, as well as gun homicide rates involving male victims.Public health critic and gun rights proponent Miguel Faria writing in Surgical Neurology International contends that to keep and bear arms have not only constitutional protection but also that firearms have beneficial aspects that have been ignored by the public health establishment in which he played a part; that guns are beneficial in personal self-defense, collective defense, as well as in protecting life and property., 


== See also ==
Index of gun politics articles
List of countries by gun ownership
Overview of gun laws by nation
Right of self-defense
Knife legislation


== Notes ==


== References ==


== Further reading ==
Baker, Dennis (2009). "Collective Criminalization and the Constitutional Right to Endanger Others". Criminal Justice Ethics. 28 (2): 168–200. doi:10.1080/07311290903181200. S2CID 144553546.
Cramer, Clayton E. (1994). For the Defense of Themselves and the State: The Original Intent and Judicial Interpretation of the Right to Keep and Bear Arms. Praeger Publishers. ISBN 0-275-94913-3.
Dizard, Jan E.; Muth, Robert Merrill; Andrews, Stephen P., Jr. (1999). Guns in America: A Reader. New York University Press. ISBN 0-8147-1878-7.
Halbrook, Stephan P. (1989). A Right to Bear Arms: State and Federal Bills of Rights and Constitutional Guarantees. Greenwood Press. ISBN 0-313-26539-9.
Malcolm, Joyce (1996). To Keep and Bear Arms: The Origins of an Anglo-American Right. Harvard University Press. ISBN 978-0674893078.
Malcolm, Joyce (2004). Guns and Violence: The English Experience. Harvard University Press. ISBN 978-0674016088.
Spitzer, Robert J. (1998). The Politics of Gun Control. Chatham House Publishers. ISBN 1-56643-021-6.
Uviller, H. Richard; William G. Merkel (2002). The Militia and the Right to Arms. Duke University Press. ISBN 0-8223-3017-2.