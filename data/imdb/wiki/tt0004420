Red coat (also spelled as "redcoat") or scarlet tunic is a military garment used widely, though not exclusively worn, by most regiments of the British Army, Royal Marines, and some colonial units within the British Empire, from the 17th to the 20th centuries. The scarlet tunic continues to be used into the 21st century, with several armed forces of the Commonwealth of Nations adopting them as their full dress and mess dress uniforms. The term redcoat may have originated in 16th century Tudor Ireland as a derogatory term for the British, as British soldiers in  Lord Lieutenant of Ireland's army wore red coats, the first time British soldiers collectively had a red uniform, the term was then brought to America and Europe by Irish emigrants.From the mid-17th century to the 19th century, the uniform of most British soldiers (apart from artillery, rifles and light cavalry) included a madder red coat or coatee. From 1873 onwards, the more vivid shade of scarlet was adopted for all ranks, having previously been worn only by officers, sergeants and all ranks of some cavalry regiments.


== History within the British Empire ==


=== Earlier instances ===
There had been instances of red military clothing pre-dating its general adoption by the New Model Army. The uniforms of the Yeoman of the Guard (formed 1485) and the Yeomen Warders (also formed 1485) have traditionally been in Tudor red and gold. The Gentlemen Pensioners of James I (now the Gentlemen-at-Arms) had worn red with yellow feathers. At Edgehill, the first battle of the Civil War, the King's people had worn red coats, as had at least two Parliamentary regiments". However none of these examples constituted the national uniform that the red coat was later to become.


=== 16th century ===
In Ireland during the reign of Elizabeth, the soldiers of the queen's Lord Lieutenant of Ireland were on occasion referred to as "red coats" by the native Irish, from the colour of their clothing. As early as 1561 the Irish named a victory over these royal troops as Cath na gCasóga Dearga, literally meaning 'The Battle of the Red Cassocks' but usually translated as the Battle of the Red Sagums – sagum being a cloak. Note the Irish word is casóg ("cassock") but the word may be translated as coat, cloak, or even uniform, in the sense that all of these troops were uniformly attired in red.

That the term "redcoat" was brought to Europe and elsewhere by Irish emigrants is evidenced by Philip O'Sullivan Beare, one of the many thousands of fugitives from Tudor and early Stuart Ireland, who mentions the 'Battle of the Redcoats' event in his 1621 history of the Tudor conquest, written in Latin in Spain. He wrote of it as "that famous victory which is called 'of the red coats' [illam victoriam quae dicitur 'sagorum rubrorum'] because among others who fell in battle were four hundred soldiers lately brought from England and clad in the red livery of the viceroy".O'Sullivan alludes to two other encounters in which the Irish won the day against English 'red coats'. One concerns an engagement, twenty years later in 1581, during the Second Desmond Rebellion, in which he says "a company of English soldiers, distinguished by their dress and arms, who were called "red coats" [Vestibus et armis insignis erat cohors Anglorum quae "Sagorum rubrorem" nominabantur], and being sent to war [in Ireland] by the Queen were overwhelmed near Lismore by John Fitzedmund Fitzgerald, the seneschal". The other relates to a rout by William Burke, Lord of Bealatury in 1599 of "English recruits clad in red coats" (qui erant tyrones Angli sagis rubris induti).English sources confirm that Crown troops in Ireland wore red coats/cloaks/uniforms/clothing. In 1584 the Lords and Council informed the Sheriffs and Justices of Lancashire who were charged with raising 200-foot for service in Ireland that they should be furnished with "a cassocke of some motley, sad grene coller, or russett". Seemingly, russet was chosen. Again, in the summer of 1595, the Lord Deputy William Russell, 1st Baron Russell of Thornhaugh writing to William Cecil, 1st Baron Burghley about the relief of Enniskillen, mentions that the Irish rebel Hugh O'Neill, Earl of Tyrone had "300 shot in red coats like English soldiers" – the inference being that English soldiers in Ireland were distinguished by their red uniforms.During the Anglo-Spanish war from 1585 to 1604 English pike men and arquebusiers fighting with their Dutch ally, were also clad in red cassocks. This was noted during the Siege of Ostend where 1,600 Englishman under the command of Sir Francis Vere arrived as reinforcements there in July 1601.


=== 17th century ===

The Red Coat has evolved from being the British infantryman's normally worn uniform to a garment retained only for ceremonial purposes. Its official adoption dates from February 1645, when the Parliament of England passed the New Model Army ordinance. The new English Army was formed of 22,000 men, paper strength, comprising eleven regiments of cavalry each of 600 men for a total of 6,600, twelve regiments of infantry each of 1,200 men for a total of 14,400, and one regiment of 1,000 dragoons and the artillery, consisting of 900 men. The infantry regiments wore coats of Venetian red with white, blue or yellow facings. A contemporary comment on the New Model Army dated 7 May 1645 stated: "the men are Redcoats all, the whole army only are distinguished by the several facings of their coats".Outside of Ireland, the English Red Coat made its first appearance on a European continental battlefield at the Battle of the Dunes in 1658. A Protectorate army had been landed at Calais the previous year and "every man had a new red coat and a new pair of shoes". The English name from the battle comes from the major engagement carried out by the "red-coats". To the surprise of continental observers they stormed sand-dunes 150 feet (46 m) high fighting experienced Spanish soldiers from their summits with musket fire and push of pike.The adoption and continuing use of red by most British/English soldiers after The Restoration (1660) was the result of circumstances rather than policy, including the relative cheapness of red dyes. Red was by no means universal at first, with grey and blue coats also being worn. There is no known basis for the myth that red coats were favoured because they did not show blood stains. Blood does in fact show on red clothing as a black stain.


=== 18th century ===

Prior to 1707 colonels of regiments made their own arrangements for the manufacture of uniforms under their command. This ended when a royal warrant of 16 January 1707 established a Board of General Officers to regulate the clothing of the army. Uniforms supplied were to conform to the "sealed pattern" agreed by the board. The style of the coat tended to follow those worn by other European armies. From an early stage red coats were lined with contrasting colours and turned out to provide distinctive regimental facings (lapels, cuffs and collars). Examples were blue for the 8th Regiment of Foot, green for the 5th Regiment of Foot, yellow for the 44th Regiment of Foot and buff for the 3rd Regiment of Foot.
In 1747, the first of a series of clothing regulations and royal warrants that set out the various facing colours and distinctions to be borne by each regiment. The long coat worn with a white or buff coloured waistcoat was discontinued in 1797 in favour of a tight-fitting coatee fastened with a single row of buttons, with white lace loops on either side.


==== American War of Independence ====

In the United States, "Redcoat" is associated in cultural memory with the British soldiers who fought against the Patriots during the American Revolutionary War: the Library of Congress possesses several examples of the uniforms the British Army used during this time. Most soldiers that fought the Patriots wore the red coat though the Hessian mercenaries and some locally recruited Loyalist units had blue or green clothing.
Accounts of the time usually refer to British soldiers as "Regulars" or "the King's men", however, there is evidence of the term "red coats" being used informally, as a colloquial expression. During the Siege of Boston, on 4 January 1776, General George Washington uses the term "red coats" in a letter to Joseph Reed. In an earlier letter dated 13 October 1775, Washington used a variation of the expression, stating, "whenever the Redcoat gentry pleases to step out of their Intrenchments." Major General John Stark of the Continental Army was purported to have said during the Battle of Bennington (16 August 1777), "There are your enemies, the Red Coats and the Tories. They are ours, or this night Molly Stark sleeps a widow!"Other pejorative nicknames for British soldiers included "bloody backs" (in a reference to both the colour of their coats and the use of flogging as a means of punishment for military offences) and "lobsters" (most notably in Boston around the time of the Boston Massacre, The earliest reference to the association with the lobster appears in 1740, just before the French and Indian War).


=== 19th–20th century ===
Following the discomfort experienced by troops in the Crimean War, a more practical tunic was introduced in 1855, initially in the French double breasted style, but replaced by a single breasted version in the following year. An attempt at standardisation was made following the Childers Reforms of 1881, with English and Welsh regiments having white facings (collar and cuffs), Scottish yellow, Irish green and Royal regiments dark blue. However some regiments were subsequently able to obtain the reintroduction of historic facing colours that had been uniquely theirs.

British soldiers fought in scarlet and blue uniforms for the last time at the Battle of Gennis in the Sudan on 30 December 1885. They formed part of an expeditionary force sent from Britain to participate in the Nile Campaign of 1884-85, wearing the "home service uniform" of the period. This included scarlet "frocks" (plain jackets in harder-wearing material designed for informal wear), although some regiments sent from India were in khaki drill. A small detachment of infantry which reached Khartoum by steamer on 28 January 1885 were ordered to fight in their red coats in order to let the Mahdist rebels know that the real British forces had arrived.Even after the adoption of khaki Service Dress in 1902, most British infantry regiments (81 out of 85) and some cavalry regiments (12 out of 31) continued to wear scarlet tunics on parade and for off-duty "walking out dress", until the outbreak of the First World War in 1914. While nearly all technical and support branches of the army wore dark blue, the Royal Engineers had worn red since the Peninsular War in order to draw less fire when serving amongst red-coated infantry.Scarlet tunics ceased to be general issue upon British mobilisation in August 1914. The Brigade of Guards resumed wearing their scarlet full dress in 1920 but for the remainder of the army red coats were only authorised for wear by regimental bands and officers in mess dress or on certain limited social or ceremonial occasions (notably attendance at court functions or weddings). The reason for not generally reintroducing the distinctive full dress was primarily financial, as the scarlet cloth requires expensive cochineal dye.
As late as 1980, consideration was given to the reintroduction of scarlet as a replacement for the dark blue "No. 1 dress" and khaki "No. 2 dress" of the modern British Army, using cheaper and fadeless chemical dyes instead of cochineal. Surveys of serving soldiers' opinion showed little support for the idea and it was shelved.


=== History with the Royal Marines ===

Red coats were first worn by British sea-going regiments when adopted by The Prince of Denmark's Regiment in 1686. Thereafter red coatees became the normal parade and battle dress for marine infantry, although the staining effects of salt spray meant that white fatigue jackets and subsequently blue undress tunics were often substituted for shipboard duties. The Royal Marine Artillery wore dark blue from their creation in 1804. The scarlet full-dress tunics of the Royal Marine Light Infantry were abolished in 1923 when the two branches of the Corps were amalgamated and dark blue became the universal uniform colour for both ceremonial and ordinary occasions.


=== Colonial forces throughout the Empire ===
Red and scarlet uniforms were widely worn by British organised or allied forces during the Imperial period. This included the presidency armies of the East India Company from 1757 onwards (along with the succeeding British Indian Army), and colonial units from Canada.


== Modern use in the Commonwealth ==
The scarlet tunic has been retained as the full dress, band or mess uniforms by several armed forces of the Commonwealth of Nations. These include the Australian, British, Canadian, Fijian, Ghanaian, Indian, Jamaican, Kenyan, New Zealand, Pakistani, Singaporean, and the Sri Lankan armies.


=== Canada ===

Usage of the scarlet tunic originates with the Canadian Militia, a militia raised to support the British Army in British North America, as well as the Canadian government following Confederation in 1867. Present dress regulations relating to the scarlet tunic originated from a simplified system ordered by the sovereign in 1902, and later promulgated in the Canadian Militia Dress Regulations 1907, and Militia Order No. 58/1908. The dress regulations, including the scarlet tunic, was maintained after the Canadian Militia was reorganized into the Canadian Army in 1940.
The Canadian Army's universal full dress uniforms includes a scarlet tunic. Although scarlet is the primary colour of the tunic, its piping is white, and the unit's facing colours appear on the tunic's collar, cuffs, and shoulder straps. The universal design also features a trefoil-shaped Austrian knot embroidered atop the facing on the tunic's cuff. However, some units in the Canadian Army are authorized regimental differences from the Army's universal full dress. As a result, some armoured regiments and artillery units substitute dark blue, Canadian-Scottish regiments "archer green", and all rifle/Voltigeur regiments "rifle green" for scarlet tunics as a part of their full dress.
In addition to the full dress uniform, a scarlet-coloured mess jacket is a part of the authorized mess dress for members of the Canadian Army. The full dress uniform for cadets of the Royal Military College of Canada is similar to the universal full dress uniform of the Canadian Army, also incorporating the scarlet tunic. The dress uniform of the Royal Canadian Mounted Police, a federal law enforcement agency, also incorporates elements of a red coat, referred to as the Red Serge.


=== New Zealand ===

During the 19th century, several volunteer militias in New Zealand wore a variety of scarlet, dark blue, or green tunics, closely following the contemporary uniforms of the British Army. Presently however, the New Zealand Army Band and the Officer Cadet School are the only units of the New Zealand Army that use the scarlet tunic as part of their ceremonial full dress uniforms.
In addition to full dress, the standard mess dress for the New Zealand Army includes a scarlet jacket with dark blue/black lapels.


=== United Kingdom ===

The scarlet tunic remains in the current British Army Dress Regulations. The scarlet tunic is one of three coloured tunics used by the British Army, alongside dark green tunics (used by The Rifles), and dark blue tunics (used by several units, such as the Royal Artillery). The scarlet tunic is presently used as part of the full dress uniforms for the Life Guards and several other cavalry units, the Foot Guards, the Royal Engineers, line infantry regiments, generals, and most army staff officers of the British Army. The locally recruited Royal Gibraltar Regiment also uses a scarlet tunic as part of its winter ceremonial dress.
In addition, the scarlet tunic is still used by some regimental bands or drummers for ceremonial purposes. Officers and NCOs of those regiments which previously wore red retain scarlet as the colour of their "mess" or formal evening jackets. Some regiments turn out small detachments, such as colour guards, in scarlet full dress at their own expense. e.g. the Yorkshire Regiment before amalgamation.


== Rationale for red ==
From the modern perspective, the retention of a highly conspicuous colour such as red for active service appears inexplicable and foolhardy, regardless of how striking it may have looked on the parade ground. However, in the days of the musket (a weapon of limited range and accuracy) and black powder, battle field visibility was quickly obscured by clouds of smoke. Bright colours provided a means of distinguishing friend from foe without significantly adding risk. Furthermore, the vegetable dyes used until the 19th century would fade over time to a pink or ruddy-brown, so on a long campaign in a hot climate the colour was less conspicuous than the modern scarlet shade would be. As formal battles of the time commonly involved deployment in columns and lines, the individual soldier was not likely to be a target by himself.


=== Within the British Empire ===
As noted above, no historical basis can be found for the suggestion that the colour red was favoured because of the supposedly demoralising effect of blood stains on a uniform of a lighter colour. In his book British Military Uniforms (Hamylyn Publishing Group 1968), the military historian W.Y. Carman traces in considerable detail the slow evolution of red as the English soldier's colour, from the Tudors to the Stuarts. The reasons that emerge are a mixture of financial (cheaper red, russet or crimson dyes), cultural (a growing popular sense that red was the sign of an English soldier), and simple chance (an order of 1594 is that coats "be of such colours as you can best provide").
Before the Tudor period, red frequently appeared in the cloth livery provided for the household personnel—including guard troops—of many European royal houses and Italian or Church principalities. Red or purple had provided a rich distinction for senior clerics through the Middle Ages in the hierarchy of colours distinguishing the Roman Church.
During the English Civil War red dyes were imported in large quantities for use by units and individuals of both sides, though this was the beginning of the trend for long overcoats. The ready availability of red pigment made it popular for military clothing and the dying process required for red involved only one stage. Other colours required the mixing of dyes in two stages and accordingly involved greater expense; blue, for example, could be obtained with woad, but more popularly it became the much more expensive indigo. In financial terms the only cheaper alternative was the grey-white of undyed wool—an option favoured by the French, Austrian, Spanish and other Continental armies. The formation of the first English standing army (Oliver Cromwell's New Model Army in 1645) saw red clothing as the standard dress. As Carman comments "The red coat was now firmly established as the sign of an Englishman".

On traditional battlefields with large engagements, visibility was not considered a military disadvantage until the general adoption of rifles in the 1850s, followed by smokeless powder after 1880. The value of drab clothing was quickly recognised by the British Army, who introduced khaki drill for Indian and colonial warfare from the mid-19th century on. As part of a series of reforms following the Second Boer War, (which had been fought in this inconspicuous clothing of Indian origin) a darker khaki serge was adopted in 1902 for service dress in Britain itself. From then on, the red coat continued as a dress item only, retained for reasons both of national sentiment and its value in recruiting. The British military authorities were more practical in their considerations than their French counterparts, who incurred heavy casualties by retaining highly visible blue coats and red trousers for active service until several months into World War I.


==== As a symbol ====
The epithet "redcoats" is familiar throughout much of the former British Empire, even though this colour was by no means exclusive to the British Army. The entire Danish Army wore red coats up to 1848 and particular units in the German, French, Austro-Hungarian, Russian, Bulgarian and Romanian armies retained red uniforms until 1914 or later. Amongst other diverse examples, Spanish hussars, Japanese Navy and United States Marine Corps bandsmen, and Serbian generals had red tunics as part of their gala or court dress during this period. In 1827 United States Artillery company musicians were wearing red coats as a reversal of their branch facing colour. However the extensive use of this colour by British, Indian and other Imperial soldiers over a period of nearly three hundred years made red uniform a veritable icon of the British Empire. The significance of military red as a national symbol was endorsed by King William IV (reigned 1830–1837) when light dragoons and lancers had scarlet jackets substituted for their previous dark blue, hussars adopted red pelisses and even the Royal Navy were obliged to adopt red facings instead of white. Most of these changes were reversed under Queen Victoria (1837–1901). A red coat and black tricorne remains part of the ceremonial and out-of-hospital dress for in-pensioners at the Royal Hospital Chelsea.


== Material used ==
Whether scarlet or red, the uniform coat has historically been made of wool with a lining of a loosely woven wool known as bay to give shape to the garment. The modern scarlet wool is supplied by Abimelech Hainsworth and is much lighter than the traditional material, which was intended for hard wear on active service.The cloth for private soldiers used up until the late 18th century was plain weave broadcloth weighing 16 ounces per square yard (540 g/m2), made from coarser blends of English wool. The weights often quoted in contemporary documents are given per running yard, though; so for a cloth of 54 inches (140 cm) width a yard weighed 24 ounces (680 g). This sometimes leads to the erroneous statement that the cloth weighed 24 oz per square yard.

Broadcloth is so called not because it is finished wide, 54 inches not being particularly wide, but because it was woven nearly half as wide again and shrunk down to finish 54 inches. This shrinking, or milling, process made the cloth very dense, bringing all the threads very tightly together, and gave a felted blind finish to the cloth. These factors meant that it was harder wearing, more weatherproof and could take a raw edge; the hems of the garment could be simply cut and left without hemming as the threads were so heavily shrunk together as to prevent fraying.
Officers' coats were made from superfine broadcloth; manufactured from much finer imported Spanish wool, spun finer and with more warps and wefts per inch. The result was a slightly lighter cloth than that used for privates, still essentially a broadcloth and maintaining the characteristics of that cloth, but slightly lighter and with a much finer quality finish. The dye used for privates' coats of the infantry, guard and line, was rose madder. A vegetable dye, it was recognised as economical, simple and reliable and remained the first choice for lower quality reds from the ancient world until chemical dyes became cheaper in the latter 19th century.
Infantry sergeants, some cavalry regiments and many volunteer corps (which were often formed from prosperous middle-class citizens who paid for their own uniforms) used various mock scarlets; a brighter red but derived from cheaper materials than the cochineal used for officers coats. Various dye sources were used for these middle quality reds, but lac dye, extracted from a kind of scale insect "lac insects" which produce resin shellac, was the most common basis.
The noncommissioned officer's red coat issued under the warrant of 1768, was dyed with a mixture of madder-red and cochineal to produce a "lesser scarlet"; brighter than the red worn by other ranks but cheaper than the pure cochineal dyed garment purchased by officers as a personal order from military tailors. Officers' superfine broadcloth was dyed true scarlet with cochineal, a dye derived from insects. This was a more expensive process but produced a distinctive colour that was the speciality of 18th-century English dyers.
The most notable centre for dying "British scarlet" cloth was Stroud in Gloucestershire, which also dyed cloth for many foreign armies. An 1823 recipe for dying 60 pounds (lbs) - about 27 kg - of military woollen cloth lists: 1 lb of cochineal, 3 lbs madder, 6 lbs argol (potassium tartrate), 3 lbs alum, 4 pints tin liquor (stannous chloride), 6 lbs cudbear (orcein) and two buckets of urine. The alum, argol and tin liquor, which acted as mordants or dye fixatives were boiled together for half an hour, the madder and cochineal was added for another ten minutes. The cloth was added and boiled for two hours; after that, the cloth was drained and immersed in cudbear and urine for another two hours. The cloth was stretched out to dry on tenters, then finally brushed with teasels and tightly rolled to produce a sheen.During the 18th and much of the nineteenth centuries the cheaply made coats of other ranks in the British army were produced by a variety of contractors, using the laborious process of dyeing described above. Accordingly, even when new, batches of garments sent to regiments might be issued in different shades of red. This tendency towards variations in appearance, commented on by contemporary observers, would subsequently be compounded by weather bleaching and soaking.)


== Use outside the Commonwealth ==


=== Bolivia ===
The Bolivian Colorados Regiment wear red tunics on ceremonial occasions - colorado means red in Spanish.


=== Brazil ===
The Brazilian Marine Corps also wear red coats as part of their ceremonial uniforms.


=== Denmark-Norway ===
The combined Danish-Norwegian army wore red uniforms from the 17th century until Norway entered union with Sweden in 1814. Most Danish Army infantry, cavalry and artillery regiments continued to wear red coats until they were replaced by dark blue service tunics in 1848. The modern Danish Royal Life Guards still wears the historic red tunics on special ceremonial occasions.


=== France ===
The Irish Brigade of the French Army (1690-1792) wore red coats supposedly to show their origins and continued loyalty to the cause of Jacobitism. Red coats were also worn by the Swiss Guard and other Swiss mercenary regiments in the French Army from the mid-17th to early 19th centuries. The North African spahi regiments wore red jackets until disbanded in 1962.


=== Indonesia ===

In Indonesia, the honor guard which is presented during a state visit and during other state-level ceremonies is assigned to a detachment from the Presidential Security Unit (Paspampres) wearing red full dress uniforms, with a white buff belt worn on the upper waist, white trousers with white parade boots and a black shako as the headdress.
This is the only unit from the Indonesian National Armed Forces which wears red as their full dress uniform.


=== Italy ===
Redshirts (Italian Camicie rosse) or Red coats (Italian Giubbe Rosse) is the name given to the volunteers who followed Giuseppe Garibaldi. The name derived from the colour of their shirts or loose fitting blouses, as complete uniforms were beyond the financial resources of the Italian patriots.


=== Netherlands ===

The Garderegiment Fuseliers Prinses Irene of the Royal Netherlands Army wear red tunics based on the British style, in commemoration of the unit's foundation in exile in the United Kingdom during World War II.


=== Paraguay ===
All branches of the Paraguayan Army wore red jackets or blouses during the War of the Triple Alliance 1864-70.


=== Poland ===
The Royal Polish Guards (Polish: Gwardia Piesza Koronna), during the times of the Polish-Lithuanian Commonwealth, wore a red cloth jacket with white lapels and a blue or turquoise vest. During the colder seasons, all soldiers were given red coats, similar to those of the contemporary British army, made of wool.


=== United States ===

Members of the United States Marine Band and the United States Marine Drum and Bugle Corps wear red coats for performances at the White House and elsewhere. This is a rare survival of the common 18th-century practice of having military bandsmen wear coats in reverse colours to the rest of a given unit (United States Marines wear blue/black tunics with red facings so United States Marine bandsmen wear red tunics with blue/black facings). 
Members of the United States Old Guard Fife and Drum Corps also wear red coats, closely modelled on those worn by the Continental Army during the American Revolution.


=== Venezuela ===

At the beginning of the 19th century, the Ejército Libertador (the Army of Liberation), inherited from the British Legion the red hussar cavalry uniforms used by the Company of Honor Guard of the Liberator Simon Bolivar.
In present-day Venezuela the red coat is part of the parade uniforms of the Regimiento de Guardia de Honor (Regiment of Presidential Guards); the Compañia de Honor "24 de Junio" (Company of Honor "24 de Junio") and the new National Militia Bolivariana.


== Gallery ==
17th–18th century
		
		
		
		
		
		
19th century
		
		
		
		
		
		
20th century
		
		
		
		
		
		
21st century
		
		
		
		
		
		
		
Units outside the Commonwealth
		
		
		
		
		
		


== See also ==

Thin Red Line


== References ==


== Sources ==
Barnes, Major R. M. (1951). History of the Regiments & Uniforms of the British Army. Seeley Service & Co.CS1 maint: ref=harv (link)
Barthorp, Michael (1982). British Infantry Uniforms Since 1660. Blandford Press. ISBN 978-1-85079-009-9.CS1 maint: ref=harv (link)
Carman, W.Y. (1968). British Military Uniforms from Contemporary Pictures. Hamlyn Publishing Group.CS1 maint: ref=harv (link)