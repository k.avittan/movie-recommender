The right of conquest is a former historically legitimate right of ownership to land after immediate possession via force of arms. It was recognized as a principle of international law that gradually deteriorated in significance until its proscription in the aftermath of World War II following criminalization of a war of aggression as first codified in the Nuremberg Principles. Further definition of aggression was recommended by the United Nations General Assembly to the Security Council via the non-binding United Nations General Assembly Resolution 3314.


== History and arguments ==
Proponents state that this right acknowledges the status quo, and that denial of the right is meaningless unless one is able and willing to use military force to deny it. Further, the right was traditionally accepted because the conquering force, being by definition stronger than any lawfully entitled governance which it may have replaced, was, therefore, more likely to secure peace and stability for the people, and so the right of conquest legitimizes the conqueror towards that end.The completion of colonial conquest of much of the world (see the Scramble for Africa), the devastation of World War I and World War II, and the alignment of both the United States and the Soviet Union with the principle of self-determination led to the abandonment of the right of conquest in formal international law. The 1928 Kellogg–Briand Pact, the post-1945 Nuremberg Trials, the UN Charter, and the UN role in decolonization saw the progressive dismantling of this principle. Simultaneously, the UN Charter's guarantee of the "territorial integrity" of member states effectively froze out claims against prior conquests from this process.


== Conquest and military occupation ==
After the attempted conquests of Napoleon and up to the attempted conquests of Hitler, the disposition of territory acquired under the principle of conquest had to, according to international law, be conducted according to the existing laws of war.  This meant that there had to be military occupation followed by a peace settlement, and there was no reasonable chance of the defeated sovereign regaining the land.  While a formal peace treaty "makes good any defects in title", it was not required.  Recognition by the losing party was not a requirement: "the right of acquisition vested by conquest did not depend on the consent of the dispossessed state". However, the alternative was annexation (part or in whole) which if protested as unlawful, a peace treaty was the only means to legitimize conquest in a time of war.  Essentially, conquest itself was a legal act of extinguishing the legal rights of other states without their consent.  Under this new framework, it is notable that conquest and subsequent occupation outside of war were illegal.In post-World War II times, when the international community frowned on wars of aggression, not all wars involving territorial acquisitions ended in a peace treaty. For example, the fighting in the Korean War paused with an armistice, without any peace treaty covering it. North Korea is still technically at war with South Korea and the United States as of 2020.


== See also ==


== References ==


=== Works cited ===
Korman, Sharon (1996). The Right of Conquest: The Acquisition of Territory by Force in International Law and Practice. Oxford University Press. ISBN 0-19-828007-6.CS1 maint: ref=harv (link)