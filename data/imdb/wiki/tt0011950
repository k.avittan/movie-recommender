Beach House is an American dream pop duo formed in Baltimore, Maryland in 2004. The band consists of vocalist and keyboardist Victoria Legrand and guitarist, keyboardist, and backup vocalist Alex Scally.
Their self-titled debut album was released in 2006 to critical acclaim and has been followed by Devotion (2008), Teen Dream (2010), Bloom (2012), Depression Cherry (2015), Thank Your Lucky Stars (2015), B-sides and Rarities (2017), and 7 (2018).


== History ==


=== Formation and Beach House ===
Vocalist and organist Victoria Legrand, who graduated from Vassar College in 2003, and guitarist Alex Scally, who graduated from Oberlin College in 2004, formed the band in 2004 after meeting in Baltimore's indie rock scene, producing music composed largely of organ, programmed drums, and slide guitar. Of the origins of the band name Scally said: "We’d been writing music, and we had all these songs, and then there was that moment where you say ‘what do we call ourselves?’ We tried to intellectualize it, and it didn’t work. There were different plant-names, Wisteria, that kind of thing. Stupid stuff. But, once we stopped trying, it just came out, it just happened. And it just seemed perfect." In an interview with Pitchfork, Legrand addressed their being a two-member status thus: "[I]t's a way to challenge ourselves: What do you do when it's just the two of you? ... [O]ne of the reasons this has been such a fulfilling experience for me is that with two people, it's so much easier to achieve things that feel exciting and new."In August 2006, their song "Apple Orchard" was featured on a Pitchfork MP3 mixtape. By October 2006 the band's self-titled debut album, Beach House, was released through Carpark Records, and was ranked 16th on Pitchfork's Best Albums of that year. The album was recorded on a 4-track over a two day period in Scally's basement.


=== Devotion and other projects ===

Beach House's second album, Devotion, was released on February 26, 2008. It was received with similar acclaim as the first album and was likewise included in Pitchfork's Best Albums of 2008 list. On October 21, 2008, the group released the single "Used to Be".
Beach House also recorded a cover of Queen's "Play the Game" for the iTunes Store release of the Red Hot Organization's 2009 compilation Dark Was The Night.
In 2009, Legrand provided backing vocals on the song "Two Weeks" by the indie rock band Grizzly Bear. She later collaborated with the band again by providing vocals to "Slow Life", the band's contribution to the soundtrack for the film Twilight: New Moon.
In October 2010, the band contributed a charity T-shirt for the Yellow Bird Project to raise money for the House of Ruth women's shelter in Maryland for victims of domestic violence.


=== Teen Dream ===

Teen Dream, the duo's "dynamic and intense" third album, was released on Sub Pop on January 26, 2010. It was released in the UK by Bella Union and in Mexico by Arts & Crafts. It contains a newer version of their 2008 single "Used to Be". Meanwhile, "Norway", the first single off ''Teen Dream'', was made available as a free download on the band's website on November 17, 2009; it was later promoted on iTunes as the Free Single of the Week starting January 12, 2010. The album was produced and engineered by Chris Coady (Yeah Yeah Yeahs, TV on the Radio, Grizzly Bear). Music videos were made both for songs, "Silver Soul" and "Real Love", created by famed collective, The Masses. The album's unanimously positive reviews garnered the band a larger fan base, with Jay-Z and Beyoncé being spotted at the band's shows.Teen Dream was listed as  number 5 on Pitchfork's Top 50 Albums of 2010 with the following notes:

Teen Dream did little to alter Beach House's core characteristics---slow-motion beats layered with hazy keyboard drones, rippling guitar figures, and Victoria Legrand's melancholic melodies---but greatly amplified them to the point of redefining the band's essence, from that of introverted knee-gazers into an assured, emotionally assertive force. -- Stuart BermanOf the success of the album and it being dubbed the group's "breakout" record by numerous publications, Legrand stated: "I see this as just another step in a direction. I would not want to say that 2010 will be our year, necessarily, I hope it’s just another year in which we do good work. I don’t want to be defined by this year, I want it to just be a beginning."The album was also included in the book 1001 Albums You Must Hear Before You Die.


=== Bloom ===

On March 7, 2012 the band streamed a new song, "Myth", from their website. The album Bloom was released on May 15, 2012 via Sub Pop. A second song from the album, "Lazuli", was released on April 13, 2012. Beach House was featured on the cover of Issue #80 of the Fader. A music video for "Lazuli" was released on June 6, 2012. It was directed by Allen Cordell, who also directed the video for "Walk in the Park" from Teen Dream. A music video for the track "Wild" has also been released. A music video for "Wishes" directed by Eric Wareheim and starring Ray Wise was released on March 7, 2013.The band released a short film, Forever Still, on February 4, 2013. The film, directed by the band and Max Goldman, was inspired by Pink Floyd's Live at Pompeii and features the band performing songs from Bloom at various sites around Tornillo, Texas, where the album was recorded. The idea for the film came from the band's desire to make quality promotional content they could control artistically: "We had previously been involved in too many live sessions, radio tapings, photoshoots, etc., where the outcome was far below our personal artistic standards. We also felt a need to distance ourselves from the 'content' culture of the internet that rewards quantity over quality and shock over nuance."


=== Depression Cherry and Thank Your Lucky Stars ===

On May 26, 2015, the band announced their fifth album Depression Cherry. It was released on August 28 via Sub Pop (on Bella Union in the UK) and the band announced a world tour in support. Talking about the direction of the new album, the band said, "In general, this record shows a return to simplicity, with songs structured around a melody and a few instruments, with live drums playing a far lesser role. With the growing success of Teen Dream and Bloom, the larger stages and bigger rooms naturally drove us towards a louder, more aggressive place; a place farther from our natural tendencies. Here, we continue to let ourselves evolve while fully ignoring the commercial context in which we exist." "I think, this album continues to change meaning for me. But if anything, it's full of many things. Love, pain, getting older, dealing with loss, letting go. It's really ultimately whatever the listener feels in response to it," has Legrand described. "The name Depression Cherry is like a dance between these two energies. That very abstract feeling summed up the energy field of the record for us," specifies Victoria. This LP received praise from critics.On October 7, 2015, the band announced a sixth album, entitled Thank Your Lucky Stars, which was released on October 16, 2015.On September 28, 2015, popular music webzine Spin reported that the band had performed a new song called "Helicopter Dream (I'm Awake)" on Flaming Lips frontman Wayne Coyne's new podcast. Other music webzines also reported on the purported new track, including Consequence of Sound, Fact, and Stereogum However, according to the band's representatives, the song was a fake. In an email to Spin, their representatives state, "It is not a new song. Not even their voices on that podcast. Sorry!" This led to a thinkpiece from the gossip blog Gawker, which used the hoax as an example of "how easy it is to fool a blogger".Notably, reaction to the fake track was largely favorable, with Spin calling it a "fuzzy space-odyssey" and Consequence of Sound describing it as "a reverb-soaked number." Even the band itself gave a positive review, tweeting, "To whoever made that fake podcast interview, we like your helicopter dream song." The fake song and podcast hoax was named by Vice's Noisey as one of "The Best Trolls of 2015."


=== B-Sides and Rarities ===
On January 31, 2017, the band announced a North American tour. In addition, they announced that they were working on a compilation of B-Sides and rarities. The compilation, B-Sides and Rarities, was eventually released on June 30, 2017, and was supported by a new song, "Chariot", which served as the lead single of the compilation and one of the two previously unreleased songs on it. Band describes that it felt like a good step for them. It helped them to clean the creative closet, put the past to bed, and start anew.


=== 7 ===
On February 15, 2018, the band released a new song, "Lemon Glow," and announced it as the lead single from their then-upcoming album which they said would be released "later this spring." On March 7, the band released another single from the album, "Dive", alongside an announcement of the album's title.
7 is the seventh studio album by Beach House, released on May 11, 2018, through Sub Pop. It follows the B-Sides and Rarities compilation album released in 2017, which served as a proverbial "cleaning out the closet" to pave the way for a new creative process.The album saw the group's departure from longtime producer Chris Coady. 7 was co-produced by Beach House and Pete “Sonic Boom” Kember. His work with Spacemen 3 and Spectrum impressed the band to reach out and see if he might like to co-produce the album with them. The record also features drums from James Barone, the band's longtime live drummer.
Throughout the process of recording 7, the band's goal was rebirth and rejuvenation. They wanted to rethink old methods and shed some self-imposed limitations. In the past, they've often limited their writing to parts that they can perform live. On 7, they decided to follow whatever came naturally. As a result, there are some songs with no guitar, and some without keyboard. There are songs with layers and production that they could never recreate live, and that was exciting to them. Basically, letting creative moods, instead of instrumentation, dictate the album’s feel.‘’This album felt really kinetic. We tend to make pretty still music, and this record felt like we were really excited by the kind of bubbling, chaotic, discordant energy field. … The vibe and the show are getting more energetic and messy, but in what I think is a cool way. Maybe a little bit more “rock and roll,” to use the old term,’’ adds Scally.On October 23, 2018, the band released a limited edition 7-inch vinyl of "Lose Your Smile" from 7 as the A-side and a new track from the recording sessions of 7 titled "Alien" as the B-side. The vinyl was originally sold on the band's European tour in dates from September to October. In addition, "Alien" was released as a standalone single to digital download and streaming services.


== Musical style and influences ==
Singer Victoria Legrand's vocals have often been compared to those of Nico. Some music outlets have also compared Legrand's vocals to 1980s psychedelic rock vocalist Kendra Smith of the band Opal. Guitarist Alex Scally plays a Fender Stratocaster guitar in an E♭ Tuning. The group's influences include This Mortal Coil, Cocteau Twins, The Zombies, Brian Wilson, Françoise Hardy, Neil Young, Big Star, Tony, Caro and John and Chris Bell.Beach House re-recorded the Tony, Caro and John song "Snowdon Song" and renamed it "Lovelier Girl". The song was released on the band's self-titled album with no attribution. Initially, Tony Dore of Tony, Caro and John stated, "I haven't seen much yet in terms of royalties or acknowledgement of authorship". Several months later, however, Dore disclosed that discussions had taken place to apply proper attribution on re-releases of the self-titled album.Legrand has described creating new music and releasing albums as a necessity for the band, saying, "When we decide that a record is finished, I think it's really about coming to terms with saying 'OK, I think we've done everything that we could do, and now it's time to let these things go.' Because if we hold onto them, we might destroy them or they might never come out. There are all different types of artists and I think that some people like to hold onto things and they get very perfectionistic. But perfectionism is kind of a synonym for destruction. It can really be a deterrent for one's evolution. Alex and I have always felt that putting records out is very necessary. It's a gut feeling we have."


== Live performances ==
The band has toured extensively worldwide. Of touring, Legrand stated: "[W]e love touring. That's when you get to get into a rhythm, playing every night. It can be really fun. And I think you learn where you want to go with your music."Beach House is famous for its visual-heavy live shows that elevate their sound to a new level. Scally believes that playing live music is the best way to connect with fans. The band’s innovation in tour décor began with lightboxes with disco balls in them that Scally and Legrand made for their first tour.In March 2009, the band was featured at the SXSW festival in Austin, Texas. In April 2010, the band performed at the Coachella Festival in Indio, California. In June 2010, the band surmounted "technical difficulties due to too much MDMA" while playing a set on The Park Stage at Glastonbury. In August 2010, the band joined Vampire Weekend on tour as one of their two opening acts, the other being the Dum Dum Girls. In October 2010, Beach House played at the Austin City Limits Music Festival. On December 20, 2010 the band performed on Conan.
The band was chosen by Animal Collective to perform at the All Tomorrow's Parties festival they curated in May 2011, and also by Portishead to perform at the ATP I'll Be Your Mirror festival that they curated in July 2011 at London's Alexandra Palace. On May 29, 2011 the band played at the Sasquatch! Music Festival. On July 31, 2011, the band played at the Fuji Rock music festival in Niigata, Japan. On October 16, 2011, the band played at the Treasure Island Music Festival. On May 18, 2012, they appeared on the Late Show with David Letterman and on Sunday, July 15, 2012, they performed the closing set on the Red Stage during the Pitchfork Music Festival.
In May 2012, the band appeared on Later... with Jools Holland playing "Myth" and "Lazuli" from their 2012 album, Bloom.
On July 26, 2012, they performed the Bloom cuts "Wild" and "Wishes" on Late Night with Jimmy Fallon.
On October 17, 2015, the band performed "One Thing" from their album Thank Your Lucky Stars (which debuted the same night) on The Late Show with Stephen Colbert.
Beach House performed at Coachella 2016 as well as Pickathon 2016 outside of Portland, Oregon and FYF Fest 2016 in Los Angeles, California. On July 15, 2016, the band headlined the Pitchfork Music Festival in Chicago, Illinois. On August 13 the band played at the Eaux Claires music festival in Eau Claire, Wisconsin.“I believe that our show now with 7 material, visually and musically, is the best it’s ever been. It evolved over a long period of time and it’s a vision now. At first, it was just an idea, but now it’s an experience…. We want to make people leave themselves. Any good artistic experience is like that,” adds Scally.In March 2018, the band announced an extensive tour itinerary that started at the end of April in the States and took them through the fall to Europe. In August 2019, Beach House added new tour dates in the US, including stops in Burlington and Cleveland, as well as Grand Rapids and Milwaukee, appearing at Osheaga festival in Montreal, Psycho Fest in Las Vegas, and Bellwether Music Festival in Waynesville, OH.


== Band members ==


== Discography ==


=== Studio albums ===


=== Compilation albums ===


=== Extended plays ===


=== Singles ===


== Awards and nominations ==


=== Sweden GAFFA Awards ===
Delivered since 2010, the GAFFA Awards (Swedish: GAFFA Priset) are a Swedish award that rewards popular music awarded by the magazine of the same name.


== References ==


== External links ==
Official website
Beach House on IMDb
Beach House at Sub Pop