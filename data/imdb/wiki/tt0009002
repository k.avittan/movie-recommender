A decoy (derived from the duck-coy, "duck cage") is usually a person, device, or event meant as a distraction, to hide what an individual or a group might be looking for. Decoys have been used for centuries most notably in game hunting, but also in wartime and in the committing or resolving of crimes.

The term decoy may refer to two distinct devices, both used for hunting wildfowl. One is a long cone-shaped wickerwork tunnel installed on a small pond to catch wild ducks. After the ducks settled on the pond, a small, trained dog would herd the birds into the tunnel. The catch was formerly sent to market for food, but now these are used only by ornithologists to catch ducks to be ringed and released. The word decoy, also originally found in English as "coy", derives from the Dutch de Kooi (the cage) and dates back to the early 17th century, when this type of duck trap was introduced to England from the Netherlands. As "decoy" came more commonly to signify a person or a device than a pond with a cage-trap, the latter acquired the retronym "decoy pool".The other form of duck decoy, otherwise known as a hunting decoy or wildfowl decoy, is a life-size model of the creature. The hunter places a number about the hunting area as they will encourage wild birds to land nearby, hopefully within the range of the concealed hunter. Originally carved from wood, they are now typically made from plastic.
Wildfowl decoys (primarily ducks, geese, shorebirds, and crows, but including some other species) are considered a form of folk art. Collecting decoys has become a significant hobby both for folk art collectors and hunters. The world record was set in September 2007 when a pintail drake and Canada goose, both by A. Elmer Crowell sold for 1.13 million dollars apiece.


== Military decoy ==

The decoy in war is a low-cost device intended to represent a real item of military equipment. They may be deployed in amongst their real counterparts, to fool enemy forces into attacking them and so protect the real items of equipment by diverting fire away from them.
Alternatively, large numbers of military decoys, or dummys, may be deployed as an aspect of military deception. Their purpose is to fool the enemy into believing forces in a particular area are much stronger than they really are. One notable example are Quaker Guns.
For a defense system, decoys and chaff for ICBMs would mainly work in mid-course: during the boost phase they would be inside the rocket, because separate rockets for each of many decoys would not be practical, while at atmospheric reentry light decoys and chaff considerably slow down and/or are destroyed in the atmosphere.


=== Bomb decoy ===
In irregular warfare, improvised explosive devices are commonly used as roadside bombs to target military patrols. Some guerillas also use imitation IEDs to intimidate civilians, to waste bomb disposal resources, or to set up an ambush. Some terrorist groups use fake bombs during a hostage siege, in order to limit hostage rescue efforts.


=== Sonar decoy ===

A sonar decoy is a device designed to create a misleading reading on sonar, such as the appearance of a false target.


== In biochemistry ==
In biochemistry, there are decoy receptors, decoy substrates and decoy RNA. In addition, digital decoys are used in protein folding simulations.


=== Decoy receptor ===
Decoy receptors, or sink receptors, are receptors that bind a ligand, inhibiting it from binding to its normal receptor. For instance, the receptor VEGFR-1 can prevent vascular endothelial growth factor (VEGF) from binding to the VEGFR-2 The TNF inhibitor etanercept exerts its anti-inflammatory effect by being a decoy receptor that binds to TNF.


=== Decoy substrate ===
A decoy substrate or pseudosubstrate is a protein that has similar structure to the substrate of an enzyme, in order to make the enzyme bind to the pseudosubstrate rather than to the real substrate, thus blocking the activity of the enzyme. These proteins are therefore enzyme inhibitors.
Examples include K3L produced by vaccinia virus, which prevents the immune system from phosphorylating the substrate eIF-2 by having a similar structure to eIF-2. Thus, the vaccinia virus avoids the immune system.


=== Digital decoys ===
In protein folding simulations, a decoy is a computer-generated protein structure which is designed so to compete with the real structure of the protein. Decoys are used to test the validity of a protein model; the model is considered correct only if it is able to identify the native state configuration of the protein among the decoys.
Decoys are generally used to overcome a main problem in protein folding simulations: the size of the conformational space. For very detailed protein models, it can be practically impossible to explore all the possible configurations to find the native state.
To deal with this problem, one can make use of decoys. The idea behind this is that it is unnecessary to search blindly through all possible conformations for the native conformation; the search can be limited to a relevant sub-set of structures. To start with, all non-compact configurations can be excluded.
A typical decoy set will include globular conformations of various shapes, some having no secondary structures, some having helices and sheets in different proportions.
The computer model being tested will be used to calculate the free energy of the protein in the decoy configurations. The minimum requirement for the model to be correct is that it identifies the native state as the minimum free energy state (see Anfinsen's dogma).


== Decoys as folk art ==

Ever since Joel Barber, the first known decoy collector, started in 1918, decoys have become increasingly viewed as an important form of North American folk art. Barber's book Wild Fowl Decoys, was the first book on decoys as collectible objects. It was followed in 1965 by folk art dealer Adele Earnest's "The Art of the Decoy" and "American Bird Decoys" by collector Wm. J. Mackey.
William J. Mackey made many trips to Chincoteague Island for the great flounder fishing as well as hunting for Chincoteague decoys. On his trips to the island he called Snug Harbor Marina home. He would send out locals to search for great finds of Chincoteague history. Cigar Daisey was one of the local Chincoteaguers that would help Mackey find all the best decoys that made his collection world-famous. Cigar has told many stories of the many truck loads of decoys he rounded up for his good friend.
By that time a milestone in collecting had already occurred with the publication of "Decoy Collectors Guide", a small magazine created by hobbyists Hal & Barbara Sorenson of Burlington, Iowa. The 'Guide' helped foster a sense of community and provided a forum for collectors to share their research.
By the 1970s decoys were becoming big business, at least by previous standards. The death of Wm. F. Mackey brought his decoys to market in a series of auctions in 1973 and 1974, with the star of his collection, a Long-billed Curlew by Wm. 'Bill' Bowman selling for a record US$10,500.
Since the 1960s numerous collectors organizations have been created, specialist books and magazines published, with specialist dealers, and special interest shows around the US and Canada.
The largest collectors organization is the Midwest Decoy Collectors Association which despite its name is an international group. It is a non-profit, [501(c)(3)] organization which sponsors the biggest show of the year. There are numerous state and regional groups as well.
The current world record was set when two decoys (Canada goose and a preening pintail drake) by A. Elmer Crowell of East Harwich, MA were sold for $1.13 million each on September 19, 2007 by Stephen O'Brien Jr. Fine Arts, in what O'Brien describes as "the largest private sale of decoys ever." The decoys were part of a private sale of 31 decoys for $7.5 million.  Joe Engers, Editor of Decoy Magazine, noted that O'Brien is one of the top dealers of decoys in the country.Among other admired makers were the Ward brothers, Lemuel (1896–1984) and Steven, of Crisfield, Maryland. Their career output is estimated at between 27,000 and 40,000 birds, working and decorative.
One of the most famous decoy makers in recent times is Delbert "Cigar" Daisey from Chincoteague Va. Cigar decoys are in high demand all over the country. The best decoy he ever made was a pintail that he made for his wife in 1973. This decoy was featured in National Geographic in June 1980 on page 826. This decoy is estimated to be worth between $100,000 - $150,000.
Fish decoy collecting is also quite popular. Especially ice fishing decoys. See also fishing lures.


== See also ==
Boarstall Duck Decoy
Decoy effect
Hale Duck Decoy
Honeypots – decoy resources for computer network security
Maskirovka
Military dummies
Mobile submarine simulator
Penetration aid
Quaker gun
Red herring
Sting operation
Ward Museum of Wildfowl Art
XGAM-71 Buck Duck


== References ==


== External links ==
Decoy Magazine, Joe Engers - The ultimate publication for decoy lovers and collectors
The Midwest Decoy Collectors Association – The de facto international collectors association
The Book of Duck Decoys – Sir Ralph Payne-Gallwey, 1886 (full text)
British Duck Decoys of To-Day, 1918 – Joseph Whitaker (full text)