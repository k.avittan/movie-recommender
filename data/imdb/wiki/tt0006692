The Flying Torpedo is a 1916 American silent drama directed by John B. O'Brien and Christy Cabanne. It was produced by the Fine Arts Film Company and distributed by the Triangle Film Corporation. The film was written by John Emerson (who also stars), Robert M. Baker and D. W. Griffith (who was not credited). The film is now considered lost.


== Plot ==

In 1921, novelist Winthrop Clavering (Emerson), known as "The World's Greatest Detective", befriends young inventor Bartholomew Thompson (Spottiswoode Aitken), has just invented a radio controlled flying bomb (weaponry that would come to be known as guided missiles). Bartholomew is soon murdered by spies, described as "yellow men from the East" in the film, who steal his new invention. Clavering and his Swedish maid Hulda (Bessie Love) set out to find the spies who have been invading the United States. Clavering and Hulda catch up with spies just as they invade California and force them out of the country with the same device they stole.


== Cast ==

Eric von Stroheim played a small supporting role as an evil German officer.


== Production notes ==

John Emerson had previously portrayed the role of Winthrop Clavering in the play The Conspiracy, from December 1912 to March 1914.

The film was produced by D. W. Griffith's film production company Fine Arts and was distributed by Triangle Film Corporation. Griffith also helped to write the film's scenario with lead John Emerson and Robert Baker. Griffith was also responsible for casting a teenage Bessie Love in the film whom he discovered and cast in several of his films in 1915. Filming began in July 1915 under the working title The Scarlet Band. Christy Cabanne directed the battle sequences.


== References ==
Citations
Works citedSoister, John T. (2012). American Silent Horror, Science Fiction and Fantasy Feature Films, 1913–1929. McFarland & Company. ISBN 978-0-7864-8790-5.CS1 maint: ref=harv (link)


== External links ==
The Flying Torpedo on IMDb
The Flying Torpedo at AllMovie
The Flying Torpedo at the American Film Institute Catalog
The Flying Torpedo at the British Film Institute
The Flying Torpedo at the TCM Movie Database
Lobby poster