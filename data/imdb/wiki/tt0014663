The Alice Comedies are a series of Animated/live-action shorts created by Walt Disney in the 1920s, in which a live action little girl named Alice (originally played by Virginia Davis) and an animated cat named Julius have adventures in an animated landscape.
On January 1, 2019, the earliest Alice Comedies became the first Disney properties to enter the public domain in the United States.


== Alice's Wonderland ==
Disney, Ub Iwerks, and their staff made the first Alice Comedy, a one-reel (ten-minute) 1923 short subject titled Alice's Wonderland, while still heading the failing Laugh-O-Gram Studio in Kansas City, Missouri.Alice's Wonderland begins with Alice entering a cartoon studio to witness cartoons being created. Alice is amazed by what she sees: the cartoon characters come to life and play around. After heading to bed that night, she dreams of being in the cartoon world, welcomed by all of the characters. Alice plays with them until a group of lions break free from a cage and chase her.
Though never released, this short helped set the stage for what was to come in the later Alice Comedies, as it established the world as a playful dream and also introduced the elements which would soon define the series. The idea of setting a real-world girl in an animated world was at this point in film history still unique. The design and voice of the later series were all set by this original film.


== The Alice Comedies series begins ==

After completing the film, the studio went bankrupt and was forced to shut down. After raising money by working as a freelance photographer, Disney bought a one-way train ticket to Los Angeles, California to live with his uncle Robert and his brother Roy. In California, Disney continued to send out proposals for the Alice series, in hopes of obtaining a distribution agreement. A deal was finally arranged through Winkler Pictures, run by Margaret Winkler and her fianceé, Charles Mintz. Because of a recent falling out with Pat Sullivan, the studio needed a quick replacement for their centerpiece Felix the Cat animated series. Disney convinced Davis's family to bring her from Missouri to Los Angeles to star in the series.


== Shorts and subsequent releases ==
Walt Disney both directed and produced all 57 films in this series. Animation was done by Walt Disney, sometimes assisted by Rollin "Ham" Hamilton. Over the course of the series, four actresses played Alice: Virginia Davis (15), Margie Gay (31), Dawn O'Day (1) and Lois Hardwick (10). The film Alice in the Jungle contains only archival footage of Virginia Davis.
The shorts in this series are now all in the public domain. In 2000, Inkwell Images released Alice in Cartoonland – The Original Alice Comedies by Walt Disney in VHS, and in 2007 in DVD as part of the Golden Classics series with ten of the films as well a documentary, poster gallery, and DVD ROM. In 2007, Kit Parker Films released another DVD called Alice in Cartoonland: The 35mm Collector's Set. In 2005 and again in 2007, ten shorts in the series were released as part of the Walt Disney Treasures series. Seven were part of the Disney Rarities that was released in 2005, while three more were released as part of The Adventures of Oswald the Lucky Rabbit, released in 2007.
In October 2016 it was announced that the Alice shorts held by the EYE Film Institute in Amsterdam had been restored for global re-release.


== See also ==
Animation in the United States during the silent era
Oswald the Lucky Rabbit


== References ==


== External links ==
Alice Comedies on IMDb
The Alice Comedies at Don Markstein's Toonopedia. Archived from the original on April 4, 2012.
Alice in Cartoonland-The Original Alice Comedies by Walt Disney [1]