A marker pen, fineliner, marking pen, felt-tip pen, flow marker, vivid (in New Zealand), texta (in Australia), sketch pen (in South Asia) or koki (in South Africa), is a pen which has its own ink source and a tip made of porous, pressed fibers such as felt. A permanent marker consists of a container (glass, aluminum or plastic) and a core of an absorbent material. This filling serves as a carrier for the ink. The upper part of the marker contains the nib that was made in earlier times of a hard felt material, and a cap to prevent the marker from drying out. Until the early 1990s, the most common solvents that were used for the ink were toluene and xylene. These two substances are both harmful and characterized by a very strong smell. Today, the ink is usually made on the basis of alcohols (e.g. 1-Propanol, 1-butanol, diacetone alcohol and cresols). Markers may be waterproof, dry-erase, wet-erase (e.g. transparency markers), or permanent.


== History ==
Lee Newman patented a felt-tipped marking pen in 1910. In 1926, Benjamin Paskach patented a "fountain paintbrush", as he called it, which consisted of a sponge-tipped handle containing various paint colors. Markers of this sort began to be popularized with the sale of Sidney Rosenthal's Magic Marker (1953), which consisted of a glass tube of ink with a felt wick. By 1958, use of felt-tipped markers was commonplace for a variety of applications such as lettering, labeling, and creating posters. The year 1962 brought the development of the modern fiber-tipped pen (in contrast to the marker, which generally has a thicker point) by Yukio Horie of the Tokyo Stationery Company (which later became Pentel).
In 1993 the Copic Sketch markers were released, popularising markers for professional illustration.


== Parts ==
The marker reservoir, which holds the ink, is formed from polyester. The "felt" used for the tip is usually made of highly compressed synthetic fibers or porous ceramics. Toluol and xylol were used as solvents for the dye and are still used for the indelible ink in permanent markers. Due to their toxicity, they have often been replaced with less critical substances such as alkyl or cyclic alkylene carbonates (like propylene carbonate) in other types of markers. Water content of the ink can be up to 10%. Besides solvents and the dye itself, the ink may contain additives (e.g. nonylphenylpolyglycol ether, alkylpoly-glycol ether, fatty acid polyglycol ester, or fatty alcohol ethoxalates) and preservatives (e.g. 2-Phenylphenol and its sodium salt, 6-acetoxy-2,4-dimethyl-m-dioxane).


== Types ==


=== Permanent marker ===
Permanent markers are porous pens that can write on surfaces such as glass, plastic, wood, metal, and stone. The marks made by such pens are however, not permanent on some plastics like Teflon, polypropylene etc., and can be erased easily. The ink is generally resistant to rubbing and water, and can last for many years. Depending on the surface and the marker used, however, the marks can often be removed with either vigorous scrubbing or chemicals such as acetone. They are highly regarded by colorists and artists around the world.


=== Highlighters ===
Highlighters are a form of marker used to highlight and cover over existing writing while still leaving the writing readable. They are generally produced in neon colours to allow for colour coding, as well as attract buyers to them.


=== Whiteboard markers ===

A whiteboard marker, or a dry-erase marker in some locations, uses an erasable ink, made to be used on a slick (or matte-finished), non-porous writing surface, for temporary writing with overhead projectors, whiteboards, and the like. They are designed so that the user is able to easily erase the marks using either a damp cloth, tissue, handkerchief, baby wipe, or other easily cleaned or disposable items. Generally, people use fabrics to do so, but others use items like paper, clothing items, some even use their bare hands to wipe it clear. The erasable ink does not contain the toxic chemical compounds xylene and/or toluene as have been used in permanent markers, being less of a risk to being used as a recreational drug.
Wet-wipe markers are another version that are used on overhead projectors, signboards, whiteboards, and other non-porous surfaces.


=== Security marker ===
Special "security" markers, with fluorescent but otherwise invisible inks, are used for marking valuables in case of burglary. The owner of a stolen, but recovered item can be determined by using ultraviolet light to make the writing visible.


=== Election marker ===
Marker pens with election ink (an indelible dye and often a photosensitive agent such as silver nitrate) used to mark the finger, and especially the cuticle, of voters in elections in order to prevent electoral fraud such as double voting. The stain stays visible for a week or two and may also be used to assist in vaccinations.


=== Porous point pen ===
A porous point pen contains a point that is made of some porous material such as felt or ceramic. Draftsman's pens usually have a ceramic tip since this wears well and does not broaden when pressure is applied while writing.


== Dialectal variations ==
The use of the terms "marker" and "felt-tipped pen" varies significantly among different parts of the world. This is because most English dialects contain words for particular types of marker, often generic brand names, but there are no such terms in widespread international use.


=== Asia ===
In some parts of India, water-based felt-tip pens are referred to as "sketch pens" because they are mainly used for sketching and writing on paper or cardboard. The permanent ink felt-tip markers are referred to as just "markers". In Malaysia and Singapore, marker pens are simply called markers. In the Philippines, a marker is commonly referred to as a "Pentel pen", regardless of brand. In Indonesia, a marker pen is referred to as "Spidol". In South Korea and Japan, marker pens are referred to as "sign pens", "name pens", or "felt pens". In Japan, permanent pens are also referred to as "Magic" (from a famous pen brand name). In Iran, felt-tip pens are referred to as "Magic" or "Highlight" regardless of its brand.


=== Australia ===
In Australia, the term "marker" usually refers only to large-tip markers, and the terms "felt-tip" and "felt pen" usually refer only to fine-tip markers. Markers in Australia are often generically called "texta", after a brand name of a type of permanent marker. Some variation in naming convention occurs between the states, for example in Queensland the brand name "nikko" has been commonly adopted.


=== New Zealand ===
The generic terms for fine-tipped markers are usually "felt pen" or "felts". Large permanent markers are called 'vivids' after a popular brand sold there, the Bic Stephens Vivid


=== South Africa ===
In South Africa, the term "Koki" is used for both felt pens and markers, by South Africans, as well as the standard "marker".


=== Canada and United States ===
In the United States, the word "marker" is used as well as "magic marker", the latter being a genericized trademark. The word "sharpie" is also now used as a genericized trademark; Sharpie is a popular brand of permanent markers used for labeling. Markers are also sometimes referred to as felt-pens or felts in some parts of Canada.


== See also ==

Highlighter
List of pen types, brands and companies
Paint marker
Sharpie


== Notes and references ==