The Night Riders was the name given by the press to the militant faction of tobacco farmers during a popular resistance to the monopolistic practices of the American Tobacco Company of James B. Duke. On September 24, 1904, the tobacco planters of western Kentucky and the neighboring counties of West Tennessee formed the Dark Fired Tobacco District, or Black Patch District Planters' Protective Association of Kentucky and Tennessee (called the Association or PPA). It urged farmers to boycott the American Tobacco Company and refuse to sell at the ruinously low prices being offered in a quasi-monopoly market. 
Groups of a more militant faction of farmers, trained and led by Dr. David A. Amoss of Caldwell County, Kentucky, resorted to physical intimidation or burning the crops to enforce compliance among the growers. Becoming known as the Night Riders because of their night-time activities, they also targeted and destroyed the tobacco warehouses of the ATC. Their largest raid of this type was their occupation and attack on areas of Hopkinsville, Kentucky, in 1907.


== Background ==
The Black Patch Tobacco War (or the Great Tobacco strike) in southwestern Kentucky and northern Tennessee extended from 1904 to 1909. It was the longest and most violent conflict between the end of the Civil War and the civil rights struggles of the mid 1960s. Originally known as the Silent Brigade, The Night Riders were a vigilante force opposed to the American Tobacco Company because it priced tobacco so low that farmers could not make any profit from their work.The head of the Night Riders was David Amoss, a medical doctor and farmer. The Amoss House in Caldwell County, Kentucky, is now a museum dedicated to preserving the history of Dr. Amoss and the Night Riders. However, the building is in danger of being sold. Other museums in the area house numerous artifacts and personal histories regarding the era of the Night Riders.


== Causes ==
The major cause of the Black Patch Wars was the drastic reduction in price that the American Tobacco Company offered tobacco farmers for their crops. In the last decade of the nineteenth century, farmers had earned a profit of from eight to twelve cents a pound, which was more than enough for a comfortable lifestyle. This changed with the turn of the twentieth century, due to the development of a virtual monopoly by the American Tobacco Company. After eliminating competition, the ATC paid an average of four cents a pound from 1901 to 1903. This was two cents per pound less than the cost of producing tobacco. Farmers were losing money just by planting their crops. In some areas the price fell as low as three, two or even one cent a pound.The farmers formed the Planters' Protective Association to oppose the monopoly. Under leader Felix G. Ewing's plan, they would grow tobacco, but store it in Ewing's warehouses until the market price increased. Ewing stressed that his plan would require the cooperation of all tobacco growers. Breaking the boycott was not allowed.


== Planter's Protective Association and its conflicts ==
The association was created in 1904 so farmers could sell their crop for a set price (originally set at eight cents per pound and two cents per pound over the cost of production). The Association would keep the tobacco in their own warehouses and pay the farmers when they sold its holdings.   At the beginning of their work, between 70% to 95% of farmers in the county were signing contracts to deliver their crop only to the association (percentages varied in some regions). However, in the first year of the Association, many non member producers, and even Association members, ignored their pledges. They undermined the attempt to meet the Tobacco Trust (Monopoly) on an economic basis. They chose to seek personal profit, as the Trust then paid ten to twelve cents per pound in an attempt to destroy the Association. For example, in 1906, non-members were selling to the Trust at ten to twelve cents a pound while Association members were receiving seven and one-eighth cents.In order to solve this problem, farmers in the association met at Stainback School House; where they decided that some of the members should become "Possum Hunters". Possum Hunters were told to visit non-members in groups of not less than five men, but not more than 2,000, to put pressure on non-member farmers to join and increase the strength of the Association. These nightly meetings eventually led to the violence of the Masked Silent Brigade (or the Night Riders).


== American Tobacco Company (ATC) ==
The American Tobacco Company, also known as the Tobacco Trust, was one of the most sophisticated and highly financed industrial monopolies in the late 1890s. When the burley tobacco crop of 1906 and 1907 was boycotted by the ATC, farmers resorted to desperate measures. In the year 1908, more than 35,000 farmers in more than 30 counties did not plant tobacco, and an entire year's worth of crop was lost. As a result, the ATC agreed to the farmers' demands in November 1908.


== Attacks ==
The Night Riders began to attack individual farms and crops if the farmers did not support the Association. Large groups of armed Night Riders even took over some towns in actions in which they destroyed tobacco crops stored in Trust warehouses, as well as the buildings and machinery. They also attacked individuals who supported the Trust. The Night Riders were known to be an efficient association. 
In 1908, the association exerted its peak of control, having gained nearly complete control of the Dark Leaf tobacco crop. The Night Riders achieved their success through violence and illegal, vigilante actions.  In order to protect themselves from the government, some Night Riders gained election into office in affected Dark Patch regions. They gained control of elected judicial positions and officers of the counties. Attorneys for some victims began to move plaintiffs out of Kentucky to establish residency elsewhere and qualify for a lawsuit in the federal courts. This broke the power of the Night Riders in local courts and brought them under the judicial process.The vigilante actions of the Night Riders have been compared to those of the Ku Klux Klan, and later white paramilitary groups against black and white Republicans and sympathizers during Reconstruction. Other historians disagree, as their motives were different.


== Related 1920s actions ==
In the 1920s, the Burley [Bright Leaf] Tobacco Growers Cooperative Association tried to compete with a monopoly buyer. They also faced a deep decline in the price of Bright Leaf Tobacco as the Dark Patch growers had encountered. They had a large number of voluntary members who pledged to sell only to the Burley Association, in order to consolidate the season's harvest for price negotiation with the monopoly.  The Burley Association would keep the crop and sell it at an acceptable price, then use the money to pay the producers. This action was initially successful, but the monopoly later reduced the price it paid to the Association and its members.


== See also ==
Trial of David Amoss


== References ==


== External links ==
Night Riders and the Kentucky Black Patch
Amoss House (scroll down)