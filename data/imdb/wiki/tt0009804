Freedom to Marry was the national bipartisan organization dedicated to winning marriage for same-sex couples in the United States.  Freedom to Marry was founded in New York City in 2003 by Evan Wolfson. Wolfson served as president of the organization through the June 2015 victory at the Supreme Court, until the organization's official closing in February 2016.Freedom to Marry drove the national strategy - what Freedom to Marry called the "Roadmap to Victory" - that led to the nationwide victory. The strategy aimed at a Supreme Court win bringing the country to national resolution, once advocates had succeeded in creating the climate for the court by working on three tracks: winning marriage in a critical mass of states, growing national majority support for marriage, and ending marriage discrimination by the federal government.


== History ==
In 1983, at a time when same-sex couples had no country- or state-level recognition anywhere in the world, Evan Wolfson wrote his Harvard Law School thesis on the constitutional right to marriage for same-sex couples. He believed that by claiming the vocabulary of marriage, same-sex couples could transform the country's understanding of who gay people were and, as a result, why exclusion and discrimination are wrong. The thesis outlined the arguments that ultimately became a national conversation and a legal and political set of battles that led to a transformation of public understanding and a triumph in the Supreme Court.Wolfson went on to serve full-time as the Marriage Director of Lambda Legal throughout the 1990s. He worked as co-counsel in Hawaii’s landmark Baehr case, which launched the ongoing international freedom to marry movement. The Hawaii case foreshadowed the pattern ahead: a legal breakthrough followed by political defeat, because of insufficient progress in changing hearts and minds. When, in 2000, Wolfson was approached by leaders of the Evelyn & Walter Haas Jr. Fund, he successfully proposed that the foundation make a $2.5 million challenge grant investment in 2001 – then the largest foundation award in the history of the LGBT movement from a highly respected, non-LGBT foundation – to help Wolfson build a new campaign to win marriage. The campaign was officially launched in 2003, the birth of Freedom to Marry.
Wolfson knew from studying the history of civil rights movements that marriage for same-sex couples would become the law of the land only when one of the country's two national actors, Congress or the U.S. Supreme Court, brought national resolution to the cause. But a smart, strategic campaign was integral to creating the climate necessary to get to that point and to avoid seeing gains snatched away.
As in Hawaii (and even the earliest rounds of the marriage battle in the 1970s), litigation was central – but it wasn't enough. Wolfson called for the creation of a campaign that reflected what he called the "4 multi's": it would be multi-year (not expected to win overnight), multi-state (not watching as victories were picked off one by one), multi-partner (no one organization could do it all), and multi-methodology (it would combine litigation, lobbying, public education, organization, direct action, fund-raising, and even, eventually, electoral). Wolfson knew that marriage advocates didn't have to win every state, but they had to win enough states – and not every American had to be persuaded to support the freedom to marry, but enough Americans needed to be supportive before elected officials and judges, including the justices of the Supreme Court, would do the right thing.
Over two decades, the marriage movement built from only 27% support among the American people in 1993 to 63% in 2015; and from 0 states issuing marriage licenses in 2002 to 37 states and the District of Columbia in 2015, when the victories created the powerful momentum and energy that enabled the Supreme Court Justices to finish the job and strike down marriage discrimination once and for all.


== Areas of work ==


=== Messaging ===
Beginning in 2010, Freedom to Marry created and coordinated a research collaborative, dubbed the Marriage Research Consortium, drawing together state and national partners such as the Movement Advancement Project, Basic Rights Oregon, and Third Way to "crack the code" on how to reach the next segment of the American public who were not yet part of the majority the campaign had achieved. Through cutting-edge research, focus groups, collaboration with the partners, and compilation of the experience gained in multiple campaigns (and losses), Freedom to Marry pioneered a new messaging playbook for the marriage movement called "Why Marriage Matters".  The playbook shifted from a previous focus on the rights and benefits that come with marriage towards values-based frames of love, commitment, freedom, family, and the Golden Rule. These were the messages that caused "middle" voters – those who Wolfson called the "reachable but not yet reached"—to move towards support, and helped contribute to the historic first victories for marriage at the ballot in 2012, to winning a majority of Americans in support of marriage, and to the movement's ultimate victory in 2015.


=== Advertising and media ===
Through an aggressive and sophisticated media strategy, Freedom to Marry's "Press Room" drove the national narrative around marriage and combated marriage opponents' fear-based messaging. Freedom to Marry funded, directed, and created ads showcasing unexpected surrogates and stories of Americans' journeys to supporting marriage. Compelling messengers included veterans, Republicans, and family members of same-sex couples, and helped make the case in the court of public opinion that all of America is ready for the freedom to marry.


=== Digital Action Center ===
Freedom to Marry launched its Digital Action Center to leverage the national organization's successful online work and apply it across more than two dozen state digital campaigns. Freedom to Marry's award-winning digital team developed cutting edge online tools and innovative content strategies to engage marriage supporters in effective action, advance the organization's message online, raise funds, and tell the stories of people directly affected by marriage discrimination. In effect, Freedom to Marry was the digital "back end" of virtually all the key state campaigns in the last several years of the push. Freedom to Marry won numerous honors for its pioneering use of video and social work for advocacy such as the Silver 2015 Pollie Award.


=== State campaigns ===
Freedom to Marry's work in the states almost always consisted of quickly and efficiently establishing state campaign coalitions, promoting teamwork, providing and enhancing expertise and a playbook of effective tactics, and generating needed funding. Freedom to Marry played a role in nearly every legislative and ballot win, and worked closely with its partners to create the climate for victory in states with pending litigation, enlisting expertise, resources, and infrastructure to support local advocates.
"Why Marriage Matters" was Freedom to Marry's national public education campaign. The campaign was launched on February 14, 2011 The Why Marriage Matters project includes videos and stories from real people and real facts about why marriage matters, and was a critical part of long-term efforts in the states.


=== President Obama and Democrats "Say, 'I Do'" ===
In March 2011, Freedom to Marry launched an open letter calling on Barack Obama to support marriage for same-sex couples. Over 122,000 people signed their names to the letter, including numerous celebrities, civic leaders, and entrepreneurs. The campaign ended on May 9, 2012, when President Obama became the first sitting president of the United States to say he supports marriage for same-sex couples.In February 2012, Freedom to Marry also launched a campaign to persuade the Democratic Party to include marriage for same-sex couples as a plank in the party platform at the 2012 Democratic National Convention. The campaign contributed to outspoken support from 22 Democratic Senators, House Democratic Leader Nancy Pelosi, Chair of the Democratic National Convention Rep. Debbie Wasserman-Schultz, Caroline Kennedy and nine other co-chairs of President Obama's reelection campaign, and more than 40,000 Americans who added their names to Freedom to Marry's online petition.
On July 29, the Democratic Party Platform Drafting Committee included a freedom to marry plank in the draft of the platform. The draft was ratified at the Democratic National Convention in September, making the Democratic Party the first major U.S. political party to officially support marriage for same-sex couples in the national party platform.


=== Mayors for the Freedom to Marry ===
In January 2012, Freedom to Marry launched the Mayors for the Freedom to Marry campaign (also known as Mayors for Marriage), encouraging mayors of cities throughout the United States to endorse marriage equality for their localities. Over 500 mayors from nearly all 50 states had joined the campaign by the time marriage was won in 2015. On January 13, 2012, San Antonio Mayor Julián Castro, Baltimore Mayor Stephanie Rawlings-Blake, and West Sacramento Mayor Christopher Cabaldon published a column, "Gay marriage a question of justice", in USA Today.


=== Fundraising ===
As the largest funder of the marriage movement, Freedom to Marry played a pivotal role in attracting broad foundation and individual donors. Donors spanned the political spectrum, from hedge fund giant Paul Singer, the founder and CEO of Elliott Management and a major Republican bundler, to acclaimed philanthropists Jon Stryker and Tim Gill, major supporters of Democratic candidates.
For example, in March 2012, Freedom to Marry launched the "Win More States Fund," which identified battleground states where funding was most critically needed. The 2012 states included New Hampshire, Maine, Washington, Minnesota, New Jersey, and later Maryland. The goal was to raise at least $3 million to funnel into these state campaigns. The organization met this initial goal in early August 2012 and continued fundraising through the rest of the year, becoming the largest out-of-state funder in three of the state ballot-measure victories. All six of the states in the Win More States Fund won in 2012 - with Maine, Maryland, and Washington passing marriage for the first time at the ballot in the November 2012 election. Minnesota became the first state to block an anti-marriage amendment at the ballot (and passed a proactive marriage bill in the next legislative session).  New Hampshire successfully blocked a measure to repeal marriage, and New Jersey passed marriage in the state legislature.In February 2013, Freedom to Marry launched the second round of states for the Win More States Fund with a goal of raising and investing $2 million into campaigns to win marriage in Delaware, Hawaii, Illinois, Minnesota, New Jersey and Rhode Island.In total, Freedom to Marry raised over $60 million to win marriage nationwide.  This money was invested into state, national, and federal programs, and directly into campaigns on the ground.


=== Young Conservatives for the Freedom to Marry ===
Young Conservatives for the Freedom to Marry is a campaign to highlight and build support for the freedom to marry among young conservatives across America. They represent young conservatives across the country that agree all Americans should be able to share in the freedom to marry. Notable members of Young Conservatives Leadership Committee include S.E. Cupp, Abby Huntsman, and Meghan McCain. The effort is managed by conservative activist Tyler Deaton.On June 4, 2014, the campaign launched a national effort to "reform the RNC platform." The "reform the platform" campaign launched in New Hampshire, consisting of a plan focused on the presidential primary states and "leading up to the Republican National Convention in 2016."


== See also ==

LGBT rights in the United States
List of LGBT rights organizations


== References ==


== Further reading ==
The New York Times, "Evan Wolfson: ‘I Believed We Could Win’," July 2015
The Washington Post, "Freedom to Marry is going out of business. And everybody’s thrilled.," July 2015
The Atlantic Monthly, "The Marriage Plot: Inside This Year's Epic Campaign for Gay Equality," December 2012
New York Times, "Evan Wolfson and Cheng He: Vows," October 2011
The Advocate, "Op-Ed: For Evan Wolfson, an 'I Do' Filled With 'I Did', October 2011
U.S. News & World Report, "Without Nationwide Gay Marriage, U.S. Government Discriminates, October 2011
TIME Magazine, "After New York: The (Near) Future of Gay Marriage, July 2011
Huffington Post, "The Freedom to Marry: What's Next After New York?, June 2011
The Advocate, "Freedom to Marry Announces Expansion Plans," January 2011
The Economist, "Single-Sex Marriage: This house believes that gay marriage should be legal," January 2011
Pacific Daily News, "Civil rights attorney promotes gay marriage", December 2010
Los Angeles Times, "Gains outweigh setback in landmark year for gay rights," December 2010
The Sunday Business Post, "A Very Civil Defense", December 2010
The Advocate, "The Message from Iowa", November 2010
Fox News, "Judges Ouster Stirs Debate", November 2010
New Jersey Record, "Seizing the Moment", October 2010
Anderson Cooper 360, "Same-Sex Marriage Ban Overturned", August 2010
The New York Times, "Ted Olson's Supreme Court Adventure", August 2009


== External links ==
Official website
Mayors For The Freedom To Marry
Young Conservatives for the Freedom to Marry