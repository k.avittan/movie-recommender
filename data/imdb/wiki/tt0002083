The Fall of Troy is an American post-hardcore band from Mukilteo, Washington. The band is a trio consisting of Thomas Erak (guitars, vocals, keyboards), Andrew Forsman (drums, percussion) and Tim Ward (bass, screamed vocals) who was later replaced by Frank Ene following his departure from the band in late 2007. Ene would remain in the band until their initial break-up in 2010, but Ward rejoined the band in Ene's place for their reunion in 2013. Drew Pelisek, formerly of CHON, has been touring with the band on bass and backing vocals since 2017 and both he and Ward are considered to be official members.
The trio is known for their technical and dynamic style, unorthodox song structures and energetic stage presence. The group has released six full-length albums, two EPs, and one single. Prior to The Fall of Troy's formation, when each member was about 17 years old, all three founding members were in another group named The 30 Years War, who released two EPs.


== History ==


=== The 30 Years War (2002) ===
Late in his freshman year at Kamiak High School, Erak provided vocals and guitar for the band The Tribune. The band recorded one EP but disbanded by mid-2002. Munro and Erak then started a four-piece hardcore band under the name The 30 Years War. The band was rounded out with drummer Andrew Forsman. The group had originally intended a much mellower sound than employed. Erak stayed on guitar and vocals, Munro moved to second guitar, and bass and drums were filled by Tim Ward and Forsman respectively. During the life of The 30 Years War, two EPs were independently released, entitled Martyrs Among the Casualties and Live at the Paradox.
Just as The 30 Years War were about to go into the studio to record again, Munro quit, stating that school commitments rendered him unable to continue playing with the band. After Munro left, the band changed its name to "The Fall of Troy" using the same method with which they had chosen their previous name: "opening a history textbook and pointing at a random location until [they] found a selection they liked".


=== Self-titled debut album (2003–2004) ===
In May 2003, the trio entered The Hall of Justice in Seattle, Washington with producer Joel M. Brown to record their first full length. They were all about 17 and a half years of age, and the album was recorded in one take over their spring break in one week. The album was released on November 4, 2003 (see 2003 in music) on Lujo Records, and reissued on August 22, 2006 (see 2006 in music) by Equal Vision Records.
The trio also recorded their independently released Ghostship EP shortly after the release of the self-titled, in 2004. Early versions of the demos included keyboards by Jesse Erickson of Mukilteo, WA and had no vocals. The demos on the Ghostship EP; Part I, II, IV, and V, showcase the Phantom on the Horizon concept album, while "Macaulay McCulkin" is on 2005's Doppelgänger.


=== Doppelgänger and mainstream success (2005–2006) ===
The band was invited to submit a new demo for Equal Vision, which saw the band hooking up with Ghostship producers Gordon Edward Greenwood III and Dustin Kochel once again. The resulting two tracks were "Tom Waits" and "Laces Out, Dan". Equal Vision used these tracks as promos for the yet to be recorded album.
In March 2005, the band entered the studio  to record their second album, titled Doppelgänger. It was released on August 16, 2005 in Compact Disc and vinyl format, which also had alternate artwork. The trio started touring extensively from the release of Doppelgänger through the first quarter of 2006.
"F.C.P.R.E.M.I.X." was released as the only single from the album and has led to some mainstream success. The music video was released June 6, 2006 and received massive airplay on MTV and FUSE. The song was featured in Saints Row for Xbox 360 and in MLB 2K6 for Xbox, PlayStation 2, GameCube, PSP and Xbox 360. The song is also featured in Guitar Hero III: Legends of Rock.


=== Manipulator (2006–2007) ===
In mid-December 2006, the band entered the studio  with producer Matt Bayles (Minus the Bear, Botch, Pearl Jam, Mastodon) to begin work on their third full-length album Manipulator. The album achieved 4 out of 5 K's in Kerrang! magazine and 7 out of 10 from Metal Hammer magazine.
The album features Nouela Johnston of the Seattle band People Eating People contributing vocal and keyboard parts to many songs. The album includes the previously written, "Seattlantis," and mostly all new material, including songs such as, "Problem!?," "Cut Down All the Trees and Name the Streets After Them," and a song with the palindromic title, "A Man. A Plan. A Canal. Panama." Before its release date the band stated that the first single from Manipulator would be "Cut Down All the Trees and Name the Streets After Them," which was also the first song released to the public on the band's MySpace. The music video for this song was released August 8, 2007, also on the band's MySpace.
To support Manipulator, the band went on tour with and opened for Deftones in the summer of 2007. Footage of this tour was used for the single, "Ex-Creations," which was released later on January 16, 2008 on MySpace as well.
In late November 2007, during their tour with Coheed and Cambria, Timothy took a break from the band. It was later confirmed by the band that he had left the band due to stress. He was replaced by Frank Ene of the band "...Of Stalwart Fads".
The trio went on tour spring 2008 with Foxy Shazam, The Dear Hunter and Tera Melos.


=== Phantom on the Horizon (2008) ===
On November 28, 2008, Phantom on the Horizon was released. The EP features all five Ghostship parts, with interludes. Erak has described the album as "one song separated by tracks." The album was played in its entirety on their West Coast and East Coast tours, followed by some "deep cuts" from the group's catalogue each night of the tour. Only 3,000 copies have been pressed (despite the misprint reverse side of the Phantom on the Horizon CD casing numbering the CD's out of a total of 3,300) and were sold online and at shows. After the 1500 physical copies held aside for online ordering sold out on December 1, an MP3 version of the album became available online retailers such as iTunes. A vinyl edition followed in 2009, with 1,000 clear copies sold through Hot Topic stores and 1,000 orange copies sold directly from the band's website.


=== In the Unlikely Event, break up and new bands (2009–2013) ===
On February 26, 2009, The Fall of Troy confirmed on their website and their MySpace profile that they were entering the studio that week with producer Terry Date to record the follow-up to Manipulator, which will include "a dozen or so songs" and should be in stores this summer.  It was revealed on April 30, 2009 that Rody Walker, lead vocalist for the progressive metal band, Protest The Hero, would make a guest appearance on the record. In the Unlikely Event, their fourth studio album, was released on October 6, 2009.
On February 26, 2010, The Fall of Troy announced that they would disband after completing a spring US tour.The trio did one of their final interviews with Mario Trevizo of Lexington Music Press.Following Fall of Troy's breakup, Thomas Erak went on to form Just Like Vinyl. In January 2013, it was announced that Erak joined the post-hardcore band Chiodos. Andrew Forsman replaced Erak as drummer of the local band The Monday Mornings in 2010. Tim Ward relocated to Idaho following his removal from The Fall of Troy and has been recording and releasing demos online under the monikers Messed Up Coyote, Cool Timmy, Trash Kids, Dorothey Valens, and Stranger Danger. Frank Ene formed the band Chineke in 2010 as lead guitarist and vocalist.
On February 21, 2011, Thomas announced the releasing of a live video named The Fall of Troy: Live at the Glasshouse.  It was filmed in Pomona, CA, on October 12, 2009.
In August 2011, Enjoy The Ride Records reissued The Fall of Troy's Doppelganger on vinyl. Only 1,500 copies were pressed, 1,000 of these featuring a black and red split coloring and sold at Hot Topic. The other 500 feature a black and red split with added "bone splatter" coloring and were sold online. In November 2011, Enjoy The Ride Records reissued 2007's Manipulator on vinyl as a double LP. Only 1,000 copies were pressed with 500 featuring a blue, pink, and orange split on LP1 and a black, white, and grey split on LP2 exclusive to Enjoy The Ride Records' website. The other 500 were once again sold at Hot Topic featuring the same colors in a spiral swirl.


=== Reunion, OK and Mukiltearth (2013–present) ===
The original lineup of The Fall of Troy reunited for three nights in December 2013 in Austin, Texas. Each night, the band performed one of their first three albums (The Fall of Troy, Doppelgänger and Manipulator) in their entirety. During the VIP reunion show, the band announced they would continue making music in 2014 by releasing an album free of charge. In December 2014, Thomas Erak announced that he left Chiodos to focus on The Fall of Troy's new album and his family. The Fall of Troy went on a 10th anniversary U.S. tour of their second album Doppelgänger in September and November 2015 with And So I Watch You from Afar and Kylesa as direct support for each respective leg.After two years of teases and updates, The Fall of Troy formally announced its fifth studio album and follow-up to 2009's In the Unlikely Event. The new album, titled OK, was self-released on April 20, 2016 (three weeks after its formal announcement) through the band's website for a pay what you want model in addition to a limited-edition vinyl version. Coinciding with the album's announcement, the band posted the track "401k" for online streaming followed by a music video for the track "Inside Out". The group further released OK#2, OK#3.1, and OK#3.2, all of which are alternate/instrumental versions of OK. In July 2020, The Fall of Troy announced a new album entitled Mukiltearth. Released in August of that year, the album consists of six re-recorded tracks from Martyrs Among the Casualties (an EP released when the band was known as The 30 Years War), alongside four newly written tracks. Two songs were posted to YouTube and streaming services following the announcement of the album, titled "We Are The Future" and "Chain Wallet Nike Shoes", with the latter being a re-recording from Martyrs Among the Casualties.


== Musical style and influences ==
The Fall of Troy is characterized by a technical, intricate style, unorthodox song structures and energetic live performances. Thomas Erak's alternating guitar riffs and rhythmic chord work, paired with odd time signatures set the band apart from other post-hardcore bands. They also include abrupt transitions between melody and dissonance, as well as interchanging clean vocals and screams which was often split between Erak and Ward, respectively. Its music has been described as "danceable", however, and the band members have stated to pay as much attention to the groove as to complex structures. The Fall of Troy is usually categorized as mathcore, post-hardcore and screamo.At the beginning, the main influences of The Fall of Troy were local bands from the Seattle area, including Botch, The Blood Brothers, Sunny Day Real Estate, Raft of Dead Monkeys and Unwed Sailor, in addition to the Texan post-hardcore act At the Drive-In. They have expressed admiration for New Jersey mathcore group The Dillinger Escape Plan as well: Andrew Forsman singled out drummer Billy Rymer as an inspiration, while Thomas Erak lauded their 1999 debut album, Calculating Infinity. Forsman has been particularly influenced by electronic music artists such as Aphex Twin.Among the artists who have cited The Fall of Troy as an influence are Chon, and Closure in Moscow.


== Band members ==

Timeline


== Discography ==


=== Studio albums ===


=== Extended plays ===


=== Compilation contributions ===
"Mr. Moustache" (originally by Nirvana; tribute album Doused in Mud, Soaked in Bleach) (2016, Robotic Empire)


== Videography ==
"Tom Waits (Demo Version)" (Equal Vision, March 11, 2005)
"Whacko Jacko Steals the Elephant Man's Bones" (Equal Vision, October 21, 2005)
"F.C.P.R.E.M.I.X." (Equal Vision, May 26, 2006)
"Cut Down All the Trees and Name the Streets After Them" (Equal Vision, August 8, 2007)
"Ex-Creations" (Equal Vision, January 16, 2008)
"The Fall of Troy: Live At The Glasshouse" (Equal Vision, February 21, 2011)


== References ==


== External links ==
Official website