Ko Yong-hui (Korean: 고용희; Hanja: 高容姬; 26 June 1952 – 13 August 2004), also spelled Ko Young-hee, was the North Korean supreme leader Kim Jong-il's consort and the mother of North Korea's leader, Kim Jong-un. Within North Korea she is only referred to by titles, such as "The Respected Mother who is the Most Faithful and Loyal 'Subject' to the Dear Leader Comrade Supreme Commander", "The Mother of Pyongyang", and "The Mother of Great Songun Korea."


== Biography ==
Born in Osaka, Japan, to a Japanese mother and a Korean father, Ko's birth date and Japanese name in Japanese official records are 26 June 1952 and Takada Hime, respectively. Her father, Ko Gyon-tek, worked in an Osaka sewing factory run by Japan's ministry of war. She, along with her family, moved to North Korea in May 1961 or in 1962 as part of a repatriation program. In the early 1970s, she began working as a dancer for the Mansudae Art Troupe in Pyongyang. Her younger sister Ko Yong-suk sought asylum from the U.S. embassy in Bern, Switzerland, while she was living there taking care of Kim Jong-un during his school days, according to South Korea's National Intelligence Service; U.S. officials arranged Ko Yong-suk's departure from the country without consulting South Korean officials.It is thought that Ko and Kim Jong-il first met in 1972. In 1981, Ko gave birth to son Kim Jong-chul, her first child with Kim. It was Kim's fourth child, after daughter Kim Hye-gyong (born 1968 to Hong Il-chon), son Kim Jong-nam (born 1971 to Song Hye-rim), and daughter Kim Sol-song (born 1974 to Kim Young-sook). Kim Jong-il's second child with Ko, the present North Korean supreme leader Kim Jong-un, followed one to three years after Jong-chul. Their third child, Kim Yo-jong, a daughter, was believed to be about 23 in 2012. However, the birth year of Kim Yo-jong is also given as 1987.On 27 August 2004, various sources reported that she had died in Paris from unspecific illness, probably of breast cancer. However, there is another report, stating that she was treated in Paris in the spring of 2004 and flown back to Pyongyang where she fell into a coma and died in August 2004.In 2012, Kim Jong-un built a grave for Ko on Mount Taesong.


== Cult of personality ==
Under North Korea's songbun ascribed status system, Ko's Korean-Japanese heritage would make her part of the lowest "hostile" class. Furthermore, her father worked in a sewing factory for the Imperial Japanese Army, which would give her the "lowest imaginable status qualities" for a North Korean.Prior to an internal propaganda film released after the ascension of Kim Jong-un, there were three attempts made to idolize Ko, in a style similar to that associated with Kang Pan-sok, mother of Kim Il-sung, and Kim Jong-suk, mother of Kim Jong-il and the first wife of Kim Il-sung. These previous attempts at idolization failed and were stopped after Kim Jong-il's 2008 stroke.The building of a cult of personality around Ko encounters the problem of her bad songbun, even though it is usually passed on by the father. Making her identity public would undermine the Kim dynasty's pure bloodline, and after Kim Jong-il's death, her personal information, including name, became state secrets. Ko's real name and other personal details have not been publicly revealed in North Korea, and she is referred to as "Mother of Great Songun Korea" or "Great Mother".


== See also ==

List of Korea-related topics
List of Koreans
Politics of North Korea
History of North Korea
Songbun


== References ==


== External links ==
BBC News obituary