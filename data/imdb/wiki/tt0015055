In microeconomics, the law of demand is a fundamental principle which states that, "conditional on all else being equal, as the price of a good increases (↑), quantity demanded will decrease (↓); conversely, as the price of a good decreases (↓), quantity demanded will increase (↑)". The only factor which influences the quantity demanded is the price. The law of demand is the inverse relationship between demand and price.  It also “works with the law of supply to explain how market economies allocate resources and determine the prices of goods and services that we observe in everyday transactions”  The law of demand describes an inverse relationship between price and quantity demanded of a good. Alternatively, other things being constant, quantity demanded of a commodity is inversely related to the price of the commodity. For example, a consumer may demand 2 kgs of apples at $70 per kg; he may, however, demand 1 kg if the price rises to $80 per kg. This has been the general human behaviour on relationship between the price of the commodity and the quantity demanded. The factors held constant refer to other determinants of demand, such as the prices of other goods and the consumer's income. There are, however, some possible exceptions to the law of demand, such as Giffen goods and Veblen goods.


== Demand vs quantity demanded ==
It is very important to apprehend the difference between demand and the quantity demanded as they are completely different. Demand refers to the demand curve (demand schedule), while quantity demanded refers to a specific point located on the demand curve which corresponds to a specific price. Therefore, a change in quantity demanded reflects a change in the price.


== Mathematical description ==
Consider the function 
  
    
      
         
        
          Q
          
            x
          
        
        =
        f
        (
        
          P
          
            x
          
        
        ;
        
          Y
        
        )
      
    
    {\displaystyle \ Q_{x}=f(P_{x};\mathbf {Y} )}
  , where 
  
    
      
        
          Q
          
            x
          
        
      
    
    {\displaystyle Q_{x}}
   is the quantity demanded of good 
  
    
      
        x
      
    
    {\displaystyle x}
  , 
  
    
      
        f
      
    
    {\displaystyle f}
   is the demand function, 
  
    
      
        
          P
          
            x
          
        
      
    
    {\displaystyle P_{x}}
   is the price of the good and 
  
    
      
        
          Y
        
      
    
    {\displaystyle \mathbf {Y} }
   is the list of parameters other than the price.
The law of demand states that 
  
    
      
        
          
            
              ∂
              f
            
            
              ∂
              
                P
                
                  x
                
              
            
          
        
        <
        0
      
    
    {\displaystyle {\frac {\partial f}{\partial P_{x}}}<0}
  . Here 
  
    
      
        ∂
        
          /
        
        ∂
        
          P
          
            x
          
        
      
    
    {\displaystyle \partial /\partial P_{x}}
   is the partial derivative operator.The above equation, when plotted with quantity demanded (
  
    
      
        
          Q
          
            x
          
        
      
    
    {\displaystyle Q_{x}}
  ) on the 
  
    
      
        x
      
    
    {\displaystyle x}
  -axis and price (
  
    
      
        
          P
          
            x
          
        
      
    
    {\displaystyle P_{x}}
  ) on the 
  
    
      
        y
      
    
    {\displaystyle y}
  -axis, gives the demand curve, which is also known as the demand schedule. The downward sloping nature of a typical demand curve illustrates the inverse relationship between quantity demanded and price. Therefore, a downward sloping demand curve embeds the law of demand.


== Terminology ==

Note that "demand" and "quantity demanded" are used to mean different things in economic jargon. On the one hand, "demand" refers to the entire demand curve, which is the relationship between quantity demanded and price. Changes in demand are due to changes in other determinants (
  
    
      
        
          Y
        
      
    
    {\displaystyle \mathbf {Y} }
  ), such as the income of consumers. Therefore, "change in demand" is used to mean that the relationship between quantity demanded and price has changed. Alfred Marshall worded this as: When then we say that a person's demand for anything increases, we mean that he will buy more of it than he would before at the same price, and that he will buy as much of it as before at a higher price. Changes in demand are depicted graphically by a shift in the demand curve.  On the other hand, "quantity demanded" refers to the quantity of goods consumers want for a given price, conditional on the other determinants. "Changes in quantity demanded" is depicted graphically by a movement along the demand curve.


== Economic history and theory ==
The law of demand was documented as early as 1892 by economist Alfred Marshall. Due to the law's general agreement with observation, economists have come to accept the validity of the law under most situations. Furthermore, researchers found that the success of the law of demand extends to animals such as rats, under laboratory settings.


== Exceptions to the law of demand ==
Generally the amount demanded of a good increases with a decrease in price of the good and vice versa.  In some cases, however, this may not be true. There are certain goods which do not follow this law. These include Veblen goods, Giffen goods, stock exchanges  and expectations of future price changes. Further exception and details are given in the sections below.


== Giffen goods ==
Initially proposed by Sir Robert Giffen, economists disagree on the existence of Giffen goods in the market. A Giffen good describes an inferior good that as the price increases, demand for the product increases. As an example, during the Great Famine of Ireland of the 19th century, potatoes were considered a Giffen good. Potatoes were the largest staple in the Irish diet, so as the price rose it had a large impact on income. People responded by cutting out on luxury goods such as meat and vegetables, and instead bought more potatoes. Therefore, as the price of potatoes increased, so did the quantity demanded.


=== Expectation of change in the price of commodity ===
If an increase in the price of a commodity causes households to expect the price of a commodity to increase further, they may start purchasing a greater amount of the commodity even at the presently increased price.  Similarly, if the household expects the price of the commodity to decrease, it may postpone its purchases. Thus, some argue that the law of demand is violated in such cases. In this case, the demand curve does not slope down from left to right; instead it presents a backward slope from the top right to down left. This curve is known as an exceptional demand curve. Prestigious goods also fail law of demand.


=== Basic or necessary goods ===
The goods which people need no matter how high the price is are basic or necessary goods. Medicines covered by insurance are a good example. An increase or decrease in the price of such a good does not affect its quantity demanded


== See also ==

Revealed preference
Aggregation problem
Representative agent
Methodological individualism
Demand (economics)
Second law of demand (price elasticity over time)
Third Law of Demand Alchian–Allen effect
Supply and demand
Law of supply


== References ==