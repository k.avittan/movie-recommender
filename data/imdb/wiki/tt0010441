A marriage of convenience  is contracted for reasons other than that of love and commitment. Instead, such a marriage is entered into for personal gain or some other sort of strategic purpose, such as political marriage. There are some cases in which those married do not intend to live together as a couple, and typically got married only for one of them to gain the right to reside in a country.
In many cultures it is usual for parents to decide their adult children's marriages; this is called arranged marriage.
A marriage of convenience that is neither a sham marriage entered into for fraudulent purposes or a forced marriage is not against the law.


== Legal loophole ==

Marriages of convenience are often contracted to exploit legal loopholes of various sorts. A couple may wed for one of them to gain citizenship or right of abode (This has been depicted in TV shows such as On the Wings of Love; where marriage is purposely undertaken to gain residency), for example, as many countries around the world will grant such rights to anyone married to a resident citizen. In the United States, this practice is known as a green card marriage. In Australia, there have been marriages of convenience to bring attention to the government's Youth Allowance laws. On 31 March 2010 two students were publicly and legally married on the University of Adelaide's lawn so that they could both receive full Youth Allowance.
In the United States during the era of the Vietnam War, some couples were wed during the man's time of exposure to the military draft; the couple agreed to no contact, followed by an annulment at the end of the (typically one year) marriage. Advertisements were commonly placed in student newspapers to this effect.
Because they exploit legal loopholes, sham marriages of convenience often have legal consequences. For example, U.S. Immigration (USCIS) can punish this with a US$250,000 fine and five-year prison sentence.


== Homosexuality ==
Another common reason for marriages of convenience is to hide one partner's homosexuality in places where being openly gay is punishable or potentially detrimental. A sham marriage of this type, sometimes called a lavender marriage, is intended to hide the appearance of homosexuality. Such marriages may have one heterosexual and one gay partner, or two gay partners: a lesbian and a gay man married to each other. In the case where a gay man marries a woman, the woman is said to be his "beard". In recent years, such marriages are conducted to make a political point about the absence of same-sex marriage in a particular country.


== Metaphorical usage ==
The phrase "marriage of convenience" has also been generalized to mean any partnership between groups or individuals for their mutual (and sometimes illegitimate) benefit, or between groups or individuals otherwise unsuited to working together. An example would be a "national unity government", as existed in Israel during much of the 1980s or in Great Britain during World War II. More specifically, cohabitation refers to a political situation which can occur in countries with a semi-presidential system (especially France), where the president and the prime minister belong to opposed political camps.


== Political marriage ==
Marriages of convenience, often termed marriages of state, have always been commonplace in royal, aristocratic, and otherwise powerful families, to make alliances between two powerful houses. Examples include the marriages of Agnes of Courtenay, her daughter Sibylla, Jeanne d'Albret, and Catherine of Aragon (twice).


== See also ==
Abuse
Basic Allowance for Housing
Child abuse
Child neglect
Child marriage
Forced marriage
Lavender marriage
Marriage of state
Arranged marriage


== References ==


== Further reading ==
Jones, James A., "The Immigration Marriage Fraud Amendments: Sham Marriages or Sham Legislation?", Florida State University Law Review, 1997
Seminara, David, "Hello, I Love You, Won’t You Tell Me Your Name: Inside the Green Card Marriage Phenomenon", Center for Immigration Studies, Washington, D.C., November 2008
Winston, Ali, "Marrying For Love?: You'll Have To Prove It", City Limits News, New York, Monday, Jul 28, 2008
Winter, Jana, "EXCLUSIVE: Aide to Harry Reid Lied to Feds, Submitted False Documents About Sham Marriage", Fox News, October 25, 2010
Academic article on political discourse & policies on forced and fraudulent marriages in the Netherlands: Bonjour&De Hart 2013, "A proper wife, a proper marriage. Constructions of 'us' and 'them' in Dutch family migration policy", European Journal of Women's Studies
Hill, S. "The European Economic Area and Marriages of Convenience", Thomas Bingham Chambers, London, Thursday, April 2, 2015
Eli Coleman PhD (1989) The Married Lesbian, Marriage & Family Review, 14:3-4, 119-135, DOI: 10.1300/J002v14n03_06


== External links ==