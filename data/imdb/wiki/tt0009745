Under the Yoke (Bulgarian: Под игото - Pod Igoto]), with subtitle A Romance of Bulgarian Liberty is a historical novel by Bulgarian author Ivan Vazov written in 1887-1888 and published in parts 1889-1890 in a magazine The Collection of Folk Tales and in a single book 1894. It is set in a small town in Central Bulgaria during the months leading up to the April Uprising in 1876 and is the most famous piece of classic Bulgarian literature. Under the Yoke has been translated into more than 30 languages. The English translation was made in 1894 by William Morfill and published by the London publishing house William Heinemann.


== Plot ==
The tranquility in a Bulgarian village under Ottoman rule is only superficial: the people are quietly preparing for an uprising. The plot follows the story of Boycho Ognyanov, who, having escaped from a prison in Diarbekir, returns to the Bulgarian town of Byala Cherkva (White Church, fictional representation of Sopot) to take part in the rebellion. There he meets old friends, enemies, and the love of his life. The plot portrays the personal drama of the characters, their emotions, motives for taking part in or standing against the rebellion, betrayal and conflict.
Historically, the April Uprising of 1876 failed due to bad organization, limited resources, and betrayal. The brutal way in which the Ottomans broke down the uprising became the pretext for the Russian-Turkish war, that brought about Bulgarian independence.
The book has many autobiographical elements: Sopot is the writer's hometown, and he did take a personal part in the uprising described.


== Reactions ==
Mary C. Neuburger has described the novel's coffeehouse scenes as a "social panorama" of Ganko's kafene (small Bulgarian coffeehouse) featuring a "parade of archetypal characters from a Balkan mountain town in Ottoman Bulgaria who drink bitter coffee, ruminate, and debate, laugh and observe, within a 'dense fog of tobacco smoke'". She writes that Vazov "skillfully paints a late Ottoman landscape" leading to the April Uprising of 1876.


== Adaptation into films ==
Under the Yoke has been twice adapted into films :

1952 : Under the Yoke (Под игото) by Dako Dakovski  . Sylvie Vartan, then a seven year little girl, plays the role of a schoolgirl under tensely patriotic end-of-year examination.
1990 : Under the Yoke (Под игото) by Yanko Yankov . A production in nine parts for Bulgarian television.


== References ==


== External links ==
Text of Under the Yoke at the Internet Archive