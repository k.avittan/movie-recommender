Danny Phantom is an American animated action adventure television series created by Butch Hartman for Nickelodeon. The series follows a teenage boy who, after an accident with an unpredictable portal between the human world and the "Ghost Zone", becomes a human-ghost hybrid and takes on the task of saving his town (and the world) from subsequent ghost attacks using an evolving variety of supernatural powers. He is aided in his quest by his two best friends Sam Manson and Tucker Foley, and later, his sister Jazz, who for most of the series' run are among the only people who know of his double life.Throughout its run, Danny Phantom received five Annie Award nominations and generally positive reviews from critics and audiences, with praise primarily directed at its ensemble cast and comic book-influenced themes and storyline.  Additionally, Danny Phantom has spawned video games, home video releases, toys, and various other merchandise.


== Premise ==
Danny Phantom centers on the life and adventures of Danny Fenton, a fourteen-year-old boy living in the small town of Amity Park. He lives with his ghost-hunting parents, Jack and Maddie, and his overprotective but caring sixteen-year-old sister, Jazz. Upon pressure from his two best friends, Sam Manson and Tucker Foley, Danny decides to explore the Ghost Portal created by his parents in their attempt to bridge the human world and the Ghost Zone (the parallel universe in which ghosts reside), that when plugged in, failed to work. Once inside, he inadvertently presses the "On" button (which his parents naively failed to do), thus activating the Portal and infusing his DNA with ectoplasm, transforming him into a half-ghost.Danny, who calls himself "Danny Phantom" in ghost form, develops the ability to fly, to become invisible, to become intangible, and to "overshadow" (possess and control) people after first learning how to switch back and forth at will between his ghost and human forms. Over time, he develops much stronger abilities, such as his Ghost Ray (a concentrated blast of energy he fires from his hand), his Ghostly Wail (an intensely powerful scream with sonic capabilities that knocks back anything caught in its path), and even cryokinesis. Danny is initially frightened by his new abilities and has little control over them, but he soon learns to use them to protect his town from malevolent ghosts. Danny turns to the life of a superhero, using his powers to rid his hometown from the various ghosts who begin to plague it and are almost always brought into the world thanks to the sporadic activation of the Fentons' Ghost Portal. Sam, Tucker, and Jazz are Danny's primary allies in his ghost-fighting activities, and help him keep his ghost-half a secret.
Danny's ghost form is a polarization of what he looked like when he first entered the Ghost Portal. When he "goes ghost", his black hair turns white, his blue eyes turn green, and the black-and-white jumpsuit he had put on before the accident appears in negative color, with the originally white areas of the suit appearing black, and vice versa. In the premiere episode of season two, a ghost grants Sam's inadvertent wish that she and Danny had never met; in consequence, Danny loses not only memories, but his ghost powers as well, as Sam had primarily been the one to persuade Danny to investigate the Portal in the first place, which led to the accident. Luckily, however, Sam had been protected from the wish by the ghost-hunting tech of Danny's parents, allowing her to persuade the now fully human Danny to regain his powers by re-enacting the accident. This time, before Danny enters the Portal, Sam replaces the logo of his father's face on the jumpsuit, which she also had removed the first time (if she hadn't, it would have been part of Danny's ghost form), with her newly designed "DP" fused-letter logo on the chest so that it appears when he goes ghost from then on.
Danny faces threats of many kinds, including vengeful ghost hunter Valerie Gray (Cree Summer) who, for a short period of time, becomes his love interest, an enemy half-ghost Vlad Masters (Martin Mull), an old college friend of his father's and considered to be Danny's true arch-rival, and even his own parents who, as ghost hunters, view Danny Phantom (as they would and do to any ghosts) as nothing but a menace to society. In addition, Danny tries to keep his secret safe from his classmates, teachers, and family. Throughout the progression of the series, Danny slowly realizes his own potential and purpose, while both worlds slowly begin to accept him as their defender.


== Episodes ==


== Characters ==

Daniel "Danny" Fenton / Danny Phantom is the series' titular protagonist. Danny is a fourteen-year-old boy who gains ghost powers in a lab accident when he steps into and activates his parents' Ghost Portal. Now a human-ghost hybrid, he chooses to use his powers to fight against malevolent scary ghosts who have begun to regularly escape the mysterious Ghost Zone and plague his hometown of Amity Park. A rather unpopular student in high school along with his friends, Danny also faces the typical hardships of living a normal teenager's life, but with the additional challenges of protecting Amity Park from frequent ghost attacks, learning to control his ghost powers, and maintaining his secret identity as "Danny Phantom." An ongoing story arc is his struggle to use his powers for the benefit of others rather than abuse them for his own needs, though he ultimately makes the right decision with the help of his friends. Despite frequently struggling with self-confidence, Danny values his altruism and comes to appreciate his own self-worth.
Samantha "Sam" Manson is Danny's female best friend and eventually girlfriend at the end of the series; she is also responsible for the accident in which Danny gains his powers. She is a self-proclaimed Goth who is also a practitioner of a dramatized form of vegetarianism called "Ultra Recyclo-Vegetarianism" (often generalized as "not eating anything that had a face") and is an amateur activist, often protesting about environmental issues and animal rights. At first, she is Danny's closest friend; however, her feelings for Danny strengthen over time. She eventually falls in love with him but cannot bring herself to tell him for fear of ruining their friendship. Her patience pays off, as Danny secretly falls in love with her as well; they ultimately share their true feelings with each other and become a couple.
Tucker Foley is Danny's male best friend, a nerd who is obsessed with technology and carries a PDA at all times. When not obsessing over gadgets, he obsesses over girls. Like Sam, he shares in Danny's secret and often helps battle ghosts and send them back into the Ghost Zone. He generally provides comic relief. Tucker's gadgets are sometimes redundant but work well in the Ghost Zone or against ghosts. Tucker and Sam frequently handle the toils and triumphs of aiding Danny, especially when he's "going ghost."
Jasmine "Jazz" Fenton is Danny's somewhat overprotective but loving older sister, an intelligent and highly sociable overachiever who considers herself an adult. Jazz views her parents' obsession with ghosts as a sign of needing psychological help. She eventually learns about Danny's powers, but chooses not to reveal her knowledge until he is ready to talk about them with her.
Jack Fenton  is Danny and Jazz's father and Maddie's husband. Jack expresses an obsession with destroying ghosts, blindly holding the belief that all ghosts are evil and must be destroyed, including Danny Phantom. He is generally incompetent in nature, but can be an effective fighter when provoked. Jack cares about his family but does not know about Danny's powers. He is almost never seen without his orange jumpsuit.
Maddie Fenton is Danny and Jazz's mother and Jack's wife. She is a gifted genius and dedicated hunter of ghosts, though she usually aims to dissect and study them rather than destroy them. A ninth-degree black belt, she is an excellent, competent fighter from whom Danny probably inherited his own talent for combat. Like her husband, she does not know about Danny's powers and is almost never seen without her blue jumpsuit. She also wears a jumpsuit hood that makes her look somewhat like Catwoman from DC comics.
Vlad Masters / Vlad Plasmius is Danny's nemesis throughout the series. Vlad had attended college at the University of Wisconsin–Madison with Jack and Maddie until Jack's prototype Ghost Portal beamed ecto-acne into Vlad's face, giving Vlad ghost powers and ruining his social life. Half-ghost for 20 years, Vlad has much more experience with his ghost powers than Danny. Vlad serves as the main antagonist throughout the entire series as he always tries to steal Maddie away from Jack and to persuade Danny to join his side and destroy his friends. He first appears in "Bitter Reunions".


== Production ==


=== Music ===
Hartman has confirmed that the bassline in the Danny Phantom theme song was inspired heavily by Queen's 1989 hit song "The Invisible Man".


== Broadcast ==
Danny Phantom premiered on April 3, 2004 at 9:30 p.m. with its first episode airing after the 2004 Kids' Choice Awards, two days after Nickelodeon's 25th Anniversary. The series aired its final episode on August 24, 2007. After the show ended, reruns continued to air on Nicktoons until December 25, 2016, and returned to premiere on NickRewind for the first time on January 16, 2019. The series aired on CBC and YTV in Canada.


== Merchandise ==


=== Video games ===
There have been two video games released for the main series. Danny Phantom: The Ultimate Enemy (for the Game Boy Advance) was made to promote the then upcoming special "The Ultimate Enemy" with the main gameplay consisting of events from the TV movie. It is a 2D platformer and was released September 8, 2005. Danny Phantom: Urban Jungle for both the Game Boy Advance and Nintendo DS was to promote the Danny Phantom episode "Urban Jungle". It is loosely based on the episode and is strictly a shooter game. It was released September 19, 2006.Danny is one of the main heroes in the Nicktoons Unite! series, appearing in all four games across multiple platforms, Nicktoons Unite!, Nicktoons: Battle for Volcano Island, Nicktoons: Attack of the Toybots, and SpongeBob SquarePants featuring Nicktoons: Globs of Doom.Danny Phantom and other characters and locations from the series have also been featured in other Nickelodeon crossover video games including: Nicktoons: Summer Camp, Nicktoons Basketball (PC), Nicktoons: Freeze Frame Frenzy (Game Boy Advance), Nicktoons Movin' (PlayStation 2), Nicktoons Winners Cup Racing (PC), Nicktoons Nitro (Arcade), Nicktoons MLB, Nickelodeon Super Brawl Universe (Android, iOS), and Nicktoons: Android Invasion (LeapFrog Didj). Danny Phantom characters and locations are featured in Nickelodeon Kart Racers 2: Grand Prix (PlayStation 4, Xbox One, Nintendo Switch, Microsoft Windows), which was released in 2020.


=== Print media ===
In October 2005, Scholastic Corporation published a Nick Zone chapter book, Stage Fright, with an original Danny Phantom story written by Erica David and illustrated by Victoria Miller and Harry Moore. Danny Phantom also made several appearances in Nickelodeon Magazine, including original comics "Brat's Entertainment!" (featuring Youngblood) and "Seeing Red" (featuring Undergrowth).


=== Home media ===


==== Nick Picks releases ====


==== CreateSpace releases ====


==== Shout! Factory releases ====


==== Other releases ====


=== Toys ===
Little official merchandise has been produced for Danny Phantom; however in 2005, Burger King released a line of Danny Phantom kids meal toys.During 2012, a company called Jazwares Toys released a 6-inch tall action figure of Danny Phantom as part of their Nicktoons toy line.In 2020 it was announced that during New York Comic Con 2020 which will be held on online Funko and Target will release a exclusive funko pop of Danny Phantom.


=== Apparel ===
As of 2020, a line of Danny Phantom shirts is available at Kohl's as part of their licensed Nickelodeon merchandise collection.


== Reception ==


=== Critical reception ===
Danny Phantom was well received by both television critics and audiences. The series gathered a cult following.
Sean Aitchison from CBR said “Danny Phantom might have a few elements that firmly place it in the 2000s, but the storytelling and design still feel fresh and fun in modern day. The show was full of action and humor, and the characters felt real and layered. If you're looking for an old Nickelodeon cartoon to rewatch, Danny Phantom should be on your list.” Eric McInnis writing for Study Breaks Magazine said, “The show offered fun comedy, memorable characters, and fantastic character designs for the enemies Danny had to fight in each episode.” Joly Herman of Common Sense Media wrote more negatively of Danny Phantom, saying that, “This cartoon can be funny, and the characters are unique. But, as is the case with so many contemporary cartoons, the rush to violence overshadows the good aspects of the series. Death threats, torture, knives, and violence against women are commonplace. There's no opportunity to work things out. Danny is either a coward or a hero -- there's no in between. He either fights or perishes, which is a heavy choice for a sensitive guy.”


=== Awards and nominations ===


== References ==


== External links ==
Danny Phantom on IMDb
Danny Phantom at TV.com
Danny Phantom at The Big Cartoon DataBase
Danny Phantom on Twitter