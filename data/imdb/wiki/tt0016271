Panache (French pronunciation: ​[panaʃ]) is a word of French origin that carries the connotation of flamboyant manner and reckless courage, derived from the helmet-plume worn by cavalrymen in the Early Modern period.The literal translation is a plume, such as is worn on a hat or a helmet; the reference is to King Henry IV of France (13 December 1553 – 14 May 1610), a pleasure-loving and cynical military leader, famed for wearing a striking white plume in his helmet and for his war cry: "Follow my white plume!" (French: "Ralliez-vous à mon panache blanc!").


== Cyrano de Bergerac ==
The epitome of panache and the reason for its establishment as a virtue are found in Edmond Rostand's depiction of Cyrano de Bergerac, in his 1897 play of that name. Prior to Rostand, panache was not necessarily a good thing and was seen by some as a suspect quality.
Panache is referred to explicitly at two points in the play but is implicit throughout: Cyrano's challenges to Montfleury, Valvert, and, at one point, the whole audience at the theatre (Act I), and his nonchalant surrender of a month's salary to pay for the damages; his duel with a hundred footpads at the Porte de Nesle (Act II), as well as his dismissal of the exploit when talking to Roxane ("I've been much braver since then"); his crossing the Spanish lines daily to deliver Roxane's letters (Act IV); and his leaving his deathbed to keep his appointment with her in Act V.
The explicit references bring in the double entendre: first, in Act IV, when sparring with de Guiche over the loss of de Guiche's white sash, he says: "I hardly think King Henry would have doffed his white panache in any danger." A second instance is in Cyrano's last words, which were: "yet there is something still that will always be mine, and when I go to God's presence, there I will doff it and sweep the heavenly pavement with a gesture: something I'll take unstained out of this world... my panache."


== Current use ==
In Quebec, the word panache may also refer to antlers, such as those of a moose or deer. The Panache River is a tributary of the east bank of the Wetetnagami River flowing into Senneterre in the La Vallée-de-l'Or Regional County Municipality, in the administrative region of Abitibi-Témiscamingue, in Quebec, in Canada.
In Wes Anderson's 2014 film The Grand Budapest Hotel, the main character's ubiquitous perfume is called 'L'air de Panache'.
Panache is a loan word that remains in use across English dialects, denoting a style that is confident and flamboyant.


== Notes ==


== References ==
Cyrano de Bergerac (Penguin translation by Carol Clark) ISBN 978-0-14-044968-6


== External links ==
 The dictionary definition of panache at Wiktionary