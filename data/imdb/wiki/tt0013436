Ninety-Nine Nights, (Korean: 나인티-나인 나이츠, Japanese: ナインティ ナイン ナイツ), is a 2006 fantasy hack and slash video game developed for the Xbox 360 by an alliance between Q Entertainment and Phantagram; video game designer Tetsuya Mizuguchi served as producer for the game. The game features hundreds of enemies onscreen at any given time, and borrows heavily from other video games of the genre, most notably from the Dynasty Warriors and Kingdom Under Fire series.
The game was released in Japan on April 20, 2006, in North America on August 15, 2006, and in Europe on August 25, 2006. A demo of Ninety-Nine Nights was released on a DVD-ROM as a pre-order bonus in Japan, and on July 28, 2006, one was released on the Xbox Live Marketplace. 
A sequel, Ninety-Nine Nights II was announced at Microsoft's Tokyo Game Show press conference in 2008, and released in 2010.


== Gameplay ==
This game is of the crowd combat subgenre, in which players battle hundreds of foes simultaneously. Combo moves are performed by using various combinations of the two main attack buttons, while the jump and dash buttons can initiate other actions or specialty attacks. Individual enemies are generally quite weak, typically being unable to perform any combos or block with any effectiveness.
There are seven different characters with different play styles, although only one character, Inphyy, is unlocked at the beginning. Successfully completing each character's story will unlock one or two new characters, until they have all been revealed.
After completing levels in Ninety-Nine Nights, a player's performance is scored, with both a letter grade and 'points' being awarded, depending on how well the player did. Points can be spent to unlock extras, such as concept art and character bios.
The title has limited role-playing elements, with characters gaining levels and being able to select which weapons and accessories to equip. These performance-enhancing items can be found in the different stages or are awarded for excellent performance, providing benefits such as increased attack power. As the characters gain levels they learn additional attack combinations, but there is no opportunity for skill customization.
Another key component of the title is the "Orb Attack" / "Orb Spark" element. Killing enemies yields red orbs that are stored up until the "Orb Attack" bar is full. Once the bar is full, a player may press B to enter "Orb Attack" mode, where the character can use powerful attacks to slay groups of enemies. Enemies killed while in this mode drop blue, not red, orbs. Once a player has stored up enough blue orbs (which usually require several "Orb Attacks"), he or she can unleash a super-devastating, screen-clearing "Orb Spark" attack. Killing enemies yields the occasional equipment drop, which you can equip any time during a map as long as you are not retaliating from an attack, in midair, or in the middle of an attack.
The orb collection mechanic is not unlike that found in Onimusha: Warlords except that it is automatic.  The orb attacks are comparable to "Musou attacks" in the Dynasty Warriors series, in that the player is invulnerable while making them, although in Ninety-Nine Nights, such attacks are significantly more powerful. Each character has their own unique attacks, weapons and orb attacks, as well having their own questline (of around four stages on average).
The gameplay differs somewhat from previous games in this genre as enemy soldiers put up virtually no resistance. Players will routinely mow down thousands of such troops per level using various attack combinations. The Orb attacks effectively act as "nuke buttons", destroying vast formations of enemy soldiers, with only boss characters and some formation leaders unaffected. Each level takes the form of a series of smaller battles which are often interspersed with cutscenes. Although there are usually several objectives during a level, progress through levels and the game is mostly linear. In a stark contrast to the cannon fodder foes, the boss characters are typically quite challenging and can inflict massive amounts of damage in a short period of time.


== Development ==
Kingdom Under Fire: Heroes developer Youn-Lee was involved in creating the game; the game had only six months development time - development kits were received in September 2005, and the game was released in March 2006 in Japan.


== Reception ==
Ninety-Nine Nights received mixed reviews from various media outlets. On the review aggregator GameRankings, the game had an average score of 63% based on 81 reviews. On Metacritic, the game had an average score of 61 out of 100, based on 66 reviews — indicating mixed or average reviews.IGN and GameSpot gave it relatively poor review scores of 5.6 out of 10 and a 5.9 out of 10, respectively. On the other hand, Play Magazine awarded it a score of 90% in its review, while the Official Xbox Magazine gave it a 7.5 out of 10. The Japanese magazine Famitsu awarded the game a score of 31/40 (8/8/8/7).


== Soundtrack ==


=== Track listing ===
Theme from Ninety-Nine Nights [2:25]
From the New World: Molto Vivace (Eternal Mix) [3:01]
Comes off Run There [3:35]
Hammerfall [3:51]
Spiral Maze! [3:33]
Carry Wind Out [3:18]
Destroys Evil Completely [4:33]
The Four Seasons "Summer" Presto (Eternal Mix) [2:55]
Ninety-Nine Nights (N3): The Defender of Truth [4:59]
Place Where They Live [4:44]
Eyes of Evil [4:06]
Ninety-Nine Nights (N3): Tokyo Remix [2:27]
Before the War [2:08]
Ninety-Nine Nights (N3): From a Distant Forest [2:05]
Ninety-Nine Nights (N3): The Arrival [3:10]
The Four Seasons "Winter" Allegro [3:09]The tracks were composed by:
Pinar Toprak (1, 9, 12, 14, 15)
Takayuki Nakamura (3, 6, 7, 10, 11, 13)
Shingo Yasumoto (4, 5)
Antonín Dvořák (2)
Antonio Vivaldi (8, 16). 


== References ==


== External links ==
Official website
Q Entertainment entry
Phantagram entry