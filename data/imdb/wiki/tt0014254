"It's a Man's Man's Man's World" is a song written by James Brown and Betty Jean Newsome. Brown recorded it on February 16, 1966, in a New York City studio and released it as a single later that year. It reached No. 1 on the Billboard R&B chart and No. 8 on the Billboard Hot 100. Its title is a word play on the 1963 comedy film It's a Mad, Mad, Mad, Mad World.


== Song ==
The song's lyrics, which Rolling Stone characterized as "biblically chauvinistic", attribute all the works of modern civilization (the car, the train, the boat ("Like Noah made the ark"),  and the electric light) to the efforts of men, but claim that it all would "mean nothing without a woman or a girl". The song also states that man made toys for the baby boys and girls, and comments about the fact that "Man makes money" to buy from other men. Before the song's fade, Brown states that man is lost in his bitterness and in the wilderness. Brown's co-writer and onetime girlfriend, Betty Jean Newsome, wrote the lyrics based on her own observations of the relations between the sexes. In later years, Newsome would claim that Brown didn't write any part of the song and argued in court that Brown sometimes forgot to pay her royalties. In May 1966, Record World magazine reported that Brown, King Records and Dynatone Publishing were being sued by Clamike Records for alleged copyright infringement of the Betty Newsome song "It's a Man's World (But What Would He Do Without a Woman)".The composition of "It's a Man's Man's Man's World" developed over a period of several years. Tammy Montgomery, better known as Tammi Terrell, recorded "I Cried", a Brown-penned song based on the same chord changes, in 1963. Brown himself recorded a demo version of the song, provisionally entitled "It's a Man's World", in 1964. This version later appeared on the CD compilations The CD of JB and Star Time.
The released version of "It's a Man's Man's Man's World" was recorded quickly, in only two takes, with a studio ensemble that included members of Brown's touring band and a string section arranged and conducted by Sammy Lowe. A female chorus was involved in the recording sessions, but their parts were edited out of the song's final master."It's a Man's Man's Man's World" became a staple of Brown's live shows for the rest of his career. Its slow, simmering groove and declamatory vocal line made it suitable for long, open-ended performances incorporating spoken ruminations on love and loss and sometimes interpolations from other songs. It appears on almost all of Brown's live albums starting with 1967's Live at the Garden. Brown also recorded a big band jazz arrangement of the song with the Louie Bellson Orchestra for his 1970 album Soul on Top.
In 2004, "It's a Man's Man's Man's World" was ranked number 123 on Rolling Stone magazine's list of the 500 greatest songs of all time.


== Personnel ==
James Brown – lead vocalwith studio band:

Dud Bascomb – trumpet
Waymon Reed – trumpet
Lamarr Wright – trumpet
Haywood Henry – baritone saxophone
Ian Bridle – piano
Billy Butler – guitar
Bernard "Pretty" Purdie – drumsOther players, including trombone, bass and strings, unknown
Arranged and conducted by Sammy Lowe


== Chart positions ==


== Renée Geyer version ==
Australian musician Renée Geyer recorded a version in 1974. The song was released in November 1974 as the second single from her second studio album, It's a Man's Man's World. The song peaked at number 44 on the Australian Kent Music Report, becoming her first Australian top 50 single.


=== Track listing ===
Australian 7" SingleSide A "It's a Man's Man's Man's World" - 3:30
Side B "Once in a Lifetime Thing" - 3:30


=== Charts ===


== Other cover versions ==
The song has been recorded by many artists in various idioms over the years.

Tom Jones recorded the song on his 1968 album The Tom Jones Fever Zone.
The Grateful Dead covered the song in concert, with Ron "Pigpen" McKernan singing the lead vocal.
Avant-garde music collective The Residents recorded the song as a companion single to their album George and James; the video for the song was frequently played on MTV.
Avant-garde music Czech band Už jsme doma and singer of The Residents Randy played and recorded the song as a part of the show in Olomouc (2010) and in 2020 released the song on CD Moravian Meeting Arrangement by Miroslav Wanek.
Dutch musician Waylon (singer) covered this song on his first audition of television show Holland's Got Talent. Later he signed at Motown.
Grand Funk released a version of the song entitled "It's a Man's World" on their 1983 album, What's Funk?
British band Brilliant recorded a cover of the song in 1985, peaking at #58 in the UK.
Christina Aguilera performed a critically acclaimed rendition of the song as a posthumous tribute to James Brown at the 2007 Grammy Awards. Her performance has been voted as the 3rd Most Memorable Grammy Performance of all time.
Concrete Blonde covered the song on their 1992 album, Walking in London.
Marla Glen recorded a version for her 1997 album Our World
Seal recorded the song for his 2008 album Soul and released it as the second single from the album.
British soul singer Joss Stone released a live version (recorded at Kennedy Center, Washington, D.C., December 7, 2003) in 2004 as both a vinyl and CD B-side of the A-side single, Super Duper Love. It was included in a commercial for Chanel’s Coco Mademoiselle in 2012 starring Keira Knightley.
Polish diva Edyta Górniak covered this song in the Jak Oni śpiewają Final on 21 November 2009.
Polish musician Czeslaw Niemen was inspired by It's a Man's Man's Man's World and recorded Dziwny jest ten świat (Strange Is This World) in 1967, where he discusses mankind's vices and hopes for peaceful future (similarly to John Lennon's Imagine.
The cast of Glee, featuring Quinn Fabray (Dianna Agron) covered the song during season one's twenty-first episode, entitled "Funk", and released it as a single. Quinn was trying to represent the suppressed feeling women feel from men. The song charted in the Canadian Hot 100 chart at 73, UK Singles Chart at 94 and in the Billboard Hot 100 chart at 95.
Smash star, Katharine McPhee, covered the song during season one.
Joshua Ledet covered this song during the eleventh season of American Idol, and briefly during the finale of the series.
Van Morisson on A Night in San Francisco
Celine Dion on Taking Chances World Tour: The Concert in 2010.
Cher on It's a Man's World in 1995.
Patricia Kaas on Je te dis vous in 1993.
International singer Natacha Atlas recorded a cover for her 2003 album Something Dangerous.
American singer Etta James recorded the song for her 2006 album All The Way.
In season 2 of The Four: Battle for Stardom, Whitney Reign covered the song as a challenge.
12 year old Norwegian singer and Norway’s Got Talent winner, Angelina Jordan, recorded the song for her 2018 album It’s Magic (Angelina Jordan & Forsbarets Stabsmusikkorps).
Ventriloquist, Darci Lynne performed a cover with Petunia, her puppet, in Season 14 of America's Got Talent as a guest performer. Petunia dedicated the song to Preacher Lawson. 
Jurnee Smollett-Bell recorded a cover for 2020 film Birds of Prey soundtrack.


== Sampling ==
Michael Jackson samples the brass introduction from the song for "Bad".
"It's A Man's World" by Ice Cube from his 1990 album AmeriKKKa's Most Wanted.
The trumpet introduction was sampled by the Wu-Tang Clan for their 2000 song "Gravel Pit" and by Nigerian-Greek rapper MC Yinka in the song "Χαιρετισμός" (Chairetismos) from his album "Αλάνα" (Alana).
2Pac used a sample on the song "Tradin War Stories" from his 1996 album All Eyez on Me.
Rapper Beanie Sigel used a sample for his song "Man's World" from his 2001 album The Reason.
The song is also sampled on Alicia Keys' song "Fallin'".
Macy Gray sampled on her song "Ghetto Love" on her album "Big".
The song is also sampled on Guilty Simpson's song "Man's World" produced by J Dilla.
The song was most recently sampled in a live performance by Jennifer Hudson as the prelude to her cover of Aretha Franklin's classic, "Respect".
The hip hop band Heavy D & the Boyz, sampled the musical introduction as the same on their track "You Ain't Heard Nuttin' Yet" for the album titled Big Tyme.
Young The Giant covered the song released exclusively on a Spotify Sessions track.
BTS lead rapper SUGA sampled the song on the title track “Agust D” for his first solo mixtape also released under his moniker, Agust D.


== Answer songs ==
Neneh Cherry released the answer song "Woman" on her 1996 album Man in response to the chauvinism of the original.
The band Napalm Death released the song "It's a M.A.N.S World!", which attacks and parodies the ideas of chauvinism and patriarchy ideas, on their 1988 album From Enslavement to Obliteration.


== Uses in popular culture ==
The song was featured in the 1993 film A Bronx Tale, which starred actor Robert De Niro, who also directed the film.
The song was featured in the 1993 television film Portrait of a Young Girl at the End of the 60’s in Brussels, directed by Chantal Akerman
The song was also featured in the 2000 Liam Neeson/Sandra Bullock action comedy Gun Shy.
The song appeared in the opening of the 2012 comedy hit Think Like a Man, starring Kevin Hart, Gabrielle Union and Taraji P. Henson.
The song was featured in the 2014 James Brown biopic Get On Up, starring Chadwick Boseman, Dan Aykroyd, and Viola Davis.
The song was featured in the 2019 film The Kitchen.
Jurnee Smollett-Bell's character Black Canary sings a cover of this song in Birds of Prey: And the Fantabulous Emancipation of One Harley Quinn


== References ==


== External links ==
Song Review from Allmusic
Lyrics of this song at MetroLyrics