The Founding Fathers of the United States, or simply the Founding Fathers or Founders, were a group of American leaders who united the Thirteen Colonies, led the war for independence from Great Britain, and built a frame of government for the new United States of America upon republican principles during the latter decades of the 18th century.
Historian Richard B. Morris in 1973 identified the following seven figures as key Founding Fathers: John Adams, Benjamin Franklin, Alexander Hamilton, John Jay, Thomas Jefferson, James Madison, and George Washington based on the critical and substantive roles they played in the formation of the country's new government.  Adams, Jefferson, and Franklin were members of the Committee of Five that drafted the Declaration of Independence. Hamilton, Madison, and Jay were authors of The Federalist Papers, advocating ratification of the Constitution. The constitutions drafted by Jay and Adams for their respective states of New York (1777) and Massachusetts (1780) were heavily relied upon when creating language for the U.S. Constitution. Jay, Adams, and Franklin negotiated the Treaty of Paris (1783) that would end the American Revolutionary War. Washington was Commander-in-Chief of the Continental Army and was president of the Constitutional Convention. All held additional important roles in the early government of the United States, with Washington, Adams, Jefferson, and Madison serving as president. Jay was the nation's first chief justice, Hamilton was the first Secretary of the Treasury, and Franklin was America's most senior diplomat, and later the governmental leader of Pennsylvania.
The term Founding Fathers is sometimes more broadly used to refer to the Signers of the embossed version of the Declaration of Independence in 1776, although four significant founders – George Washington, John Jay, Alexander Hamilton, and James Madison – were not signers. Signers is not to be confused with the term Framers; the Framers are defined by the National Archives as those 55 individuals who were appointed to be delegates to the 1787 Constitutional Convention and took part in drafting the proposed Constitution of the United States. Of the 55 Framers, only 39 were signers of the Constitution. Two further groupings of Founding Fathers include: 1) those who signed the Continental Association, a trade ban and one of the colonists' first collective volleys protesting British control and the Intolerable Acts in 1774, and 2) those who signed the Articles of Confederation, the first U.S. constitutional document.The phrase Founding Fathers is a 20th-century appellation, coined by Warren G. Harding in 1916.


== Background ==

The First Continental Congress met briefly in Philadelphia, Pennsylvania in 1774, consisting of 56 delegates from all thirteen American colonies except Georgia. Among them was George Washington, who would soon be drawn out of military retirement to command the Continental Army during the American Revolutionary War. Also in attendance were Patrick Henry and John Adams, who, like all delegates, were elected by their respective colonial assemblies. Other delegates included Samuel Adams from Massachusetts, John Dickinson from Pennsylvania and New York's John Jay. This congress, in addition to formulating appeals to the British crown, established the Continental Association to administer boycott actions against Britain.
When the Second Continental Congress convened on May 10, 1775, it essentially reconstituted the First Congress. Many of the same 56 delegates who attended the first meeting participated in the second. New arrivals included Benjamin Franklin and Robert Morris of Pennsylvania, John Hancock of Massachusetts, John Witherspoon of New Jersey, and Charles Carroll of Carrollton of Maryland, who was named as a late delegate due to his being Roman Catholic. Hancock was elected Congress president two weeks into the session when Peyton Randolph was recalled to Virginia to preside over the House of Burgesses. Thomas Jefferson replaced Randolph in the Virginia congressional delegation. The second Congress adopted the Declaration of Independence. Witherspoon was the only active clergyman to sign the Declaration. He also signed the Articles of Confederation and attended the New Jersey (1787) convention that ratified the Federal Constitution.
The newly founded country of the United States had to create a new government to replace the British Parliament. The U.S. adopted the Articles of Confederation, a declaration that established a national government with a one-house legislature. Its ratification by all thirteen colonies gave the second Congress a new name: the Congress of the Confederation, which met from 1781 to 1789. The Constitutional Convention took place during the summer of 1787, in Philadelphia. Although the convention was called to revise the Articles of Confederation, the intention from the outset for some including James Madison and Alexander Hamilton was to create a new frame of government rather than amending the existing one. The delegates elected George Washington to preside over the convention. The result of the convention was the United States Constitution and the replacement of the Continental Congress with the United States Congress.


== Social background and commonalities ==

The Founding Fathers represented a cross-section of 18th-century U.S. leadership. According to a study of the biographies by Caroline Robbins:

The Signers came for the most part from an educated elite, were residents of older settlements, and belonged with a few exceptions to a moderately well-to-do class representing only a fraction of the population. Native or born overseas, they were of British stock and of the Protestant faith.
They were leaders in their communities; several were also prominent in national affairs. Virtually all participated in the American Revolution; at the Constitutional Convention at least 29 had served in the Continental Army, most of them in positions of command. Scholars have examined the collective biography of the Founders, including both the signers of the Declaration and of the Constitution.


=== Education ===
Many of the Founding Fathers attended or graduated from the colonial colleges, most notably Columbia known at the time as "King's College", Princeton originally known as "The College of New Jersey", Harvard College, the College of William and Mary, Yale College and University of Pennsylvania. Some had previously been home schooled or obtained early instruction from private tutors or academies. Others had studied abroad. Ironically, Benjamin Franklin who had little formal education himself would ultimately establish the College of Philadelphia based on European models (1740); "Penn" would have the first medical school (1765) in the thirteen colonies where another Founder, Benjamin Rush would eventually teach.
With a limited number of professional schools established in the U.S., Founders also sought advanced degrees from traditional institutions in England and Scotland such as the University of Edinburgh, the University of St. Andrews, and the University of Glasgow.


==== Colleges attended ====
College of William and Mary: Thomas Jefferson, Benjamin Harrison V
Harvard College: John Adams, Samuel Adams, John Hancock and William Williams
King's College (now Columbia): John Jay, Alexander Hamilton, Gouverneur Morris, Robert R. Livingston and Egbert Benson.
College of New Jersey (now Princeton): James Madison, Gunning Bedford Jr., Aaron Burr, Benjamin Rush and William Paterson
College of Philadelphia later merged into the University of Pennsylvania: Hugh Williamson
Yale College: Oliver Wolcott,  Andrew Adams
Queen's College (now Rutgers): James Schureman
James Wilson attended the University of St. Andrews, the University of Glasgow, and the University of Edinburgh though he never received a degree.


=== Advanced degrees and apprenticeships ===


==== Doctors of Medicine ====
University of Edinburgh: Rush 
University of Utrecht, Netherlands: Williamson


==== Theology ====
University of Edinburgh: Witherspoon (attended, no degree)
University of St. Andrews: Witherspoon (honorary doctorate)


==== Legal apprenticeships ====
Several like John Jay, James Wilson, John Williams and George Wythe were trained as lawyers through apprenticeships in the colonies while a few trained at the Inns of Court in London. Charles Carroll of Carrollton earned his law degree at Temple in London.


==== Self-taught or little formal education ====
Franklin, Washington, John Williams and Henry Wisner had little formal education and were largely self-taught or learned through apprenticeship.


=== Demographics ===
The great majority were born in the Thirteen Colonies. But at least nine were born in other parts of the British Empire:

England: Robert Morris, Button Gwinnett
Ireland: Butler, Fitzsimons, McHenry and Paterson
West Indies: Hamilton
Scotland: Wilson and WitherspoonMany of them had moved from one colony to another. Eighteen had already lived, studied or worked in more than one colony: Baldwin, Bassett, Bedford, Davie, Dickinson, Few, Franklin, Ingersoll, Hamilton, Livingston, Alexander Martin, Luther Martin, Mercer, Gouverneur Morris, Robert Morris, Read, Sherman, and Williamson.
Several others had studied or traveled abroad.


=== Occupations ===
The Founding Fathers practiced a wide range of high and middle-status occupations, and many pursued more than one career simultaneously. They did not differ dramatically from the Loyalists, except they were generally younger and less senior in their professions.
As many as thirty-five including Adams, Hamilton, Jefferson, Madison, and Jay were trained as lawyers though not all of them practiced law. Some had also been local judges.
Washington trained as a land surveyor before he became commander of a small militia.
At the time of the convention, 13 men were merchants: Blount, Broom, Clymer, Dayton, Fitzsimons, Shields, Gilman, Gorham, Langdon, Robert Morris, Pierce, Sherman and Wilson.
Broom and Few were small farmers.
Franklin, McHenry and Mifflin had retired from active economic endeavors.
Franklin and Williamson were scientists, in addition to their other activities.
McClurg, McHenry, Rush and Williamson were physicians.
Johnson and Witherspoon were college presidents.


=== Finances ===
Historian Caroline Robbins in 1977 examined the status of the Signers of the Declaration of Independence and concluded:

There were indeed disparities of wealth, earned or inherited: some Signers were rich, others had about enough to enable them to attend Congress. ... The majority of revolutionaries were from  moderately well-to-do or average income brackets. Twice as many Loyalists belonged to the wealthiest echelon. But some Signers were rich; few, indigent. ... The Signers were elected not for wealth or rank so much as because of the evidence they had already evinced of willingness for public service. A few of them were wealthy or had financial resources that ranged from good to excellent, but there are other founders who were less than wealthy. On the whole they were less wealthy than the Loyalists.Seven were major land speculators: Blount, Dayton, Fitzsimmons, Gorham, Robert Morris, Washington, and Wilson.
Eleven speculated in securities on a large scale: Bedford, Blair, Clymer, Dayton, Fitzsimons, Franklin, King, Langdon, Robert Morris, Charles Cotesworth Pinckney, and Sherman.
Many derived income from plantations or large farms which they owned or managed, which relied upon the labor of enslaved men and women particularly in the southern colonies: Bassett, Blair, Blount, Davie, Johnson, Butler, Carroll, Jefferson, Jenifer, Madison, Mason, Charles Pinckney, Charles Cotesworth Pinckney, Rutledge, Spaight, and Washington.
Eight of the men received a substantial part of their income from public office: Baldwin, Blair, Brearly, Gilman, Livingston, Madison, and Rutledge.


=== Prior political experience ===
Several of the Founding Fathers had extensive national, state, local and foreign political experience prior to the adoption of the Constitution in 1787. Some had been diplomats. Several had been members of the Continental Congress or elected president of that body.

Benjamin Franklin began his political career as a city councilman and then Justice of the Peace in Philadelphia. He was next elected to the Pennsylvania Assembly and was sent by them to London as a colonial agent which helped hone his diplomatic skills.
Jefferson, Adams, Jay and Franklin all acquired significant political experience as ministers to countries in Europe.
John Adams and John Jay drafted the Constitutions of their respective states, Massachusetts and New York, and successfully navigated them through to adoption.
Jay, Thomas Mifflin and Nathaniel Gorham had served as president of the Continental Congress.
Gouverneur Morris had been a member of the New York Provincial Congress.
John Dickinson, Franklin, Langdon, and Rutledge had been governors or presidents of their states.
Robert Morris had been a member of the Pennsylvania Assembly and president of Pennsylvania's Committee of Safety. He was also a member of the Committee of Secret Correspondence.
Roger Sherman had served in the Connecticut House of Representatives.
Elbridge Gerry was a member of the Massachusetts Provincial Congress.
Carroll served in the Maryland Senate.
Wythe's first exposure to politics was as a member of Virginia's House of Burgesses.
Read's entry into the political arena was as a commissioner of the town of Charlestown, Maryland.
Clymer was a member of the Philadelphia Committee of Safety and the Continental Congress.
Wilson's time as a member of the Continental Congress in 1776 was his introduction to colonial politics.Nearly all of the 55 Constitutional Convention delegates had some experience in colonial and state government, and the majority had held county and local offices. Those who lacked national congressional experience were Bassett, Blair, Brearly, Broom, Davie, Dayton, Alexander Martin, Luther Martin, Mason, McClurg, Paterson, Charles Pinckney, Strong, and Yates.


=== Religion ===

Franklin T. Lambert (2003) has examined the religious affiliations and beliefs of some of the Founders. Of the 55 delegates to the 1787 Constitutional Convention, 28 were Anglicans (i.e. Church of England; or Episcopalian, after the American Revolutionary War was won), 21 were other Protestants, and two were Roman Catholics (D. Carroll and Fitzsimons). Among the Protestant delegates to the Constitutional Convention, eight were Presbyterians, seven were Congregationalists, two were Lutherans, two were Dutch Reformed, and two were Methodists.A few prominent Founding Fathers were anti-clerical notably Jefferson.Historian Gregg L. Frazer argues that the leading Founders (John Adams, Jefferson, Franklin, Wilson, Morris, Madison, Hamilton, and Washington) were neither Christians nor Deists, but rather supporters of a hybrid "theistic rationalism".Many Founders deliberately avoided public discussion of their faith. Historian David L. Holmes uses evidence gleaned from letters, government documents, and second-hand accounts to identify their religious beliefs.


=== Ownership of slaves and position on slavery ===

The founding fathers were not unified on the issue of slavery. Many of them were opposed to it and repeatedly attempted to end slavery in many of the colonies, but predicted that the issue would threaten to tear the country apart and had limited power to deal with it.  In her study of Thomas Jefferson, historian Annette Gordon-Reed discusses this topic, "Others of the founders held slaves, but no other founder drafted the charter for freedom". In addition to Jefferson, George Washington, and many other of the Founding Fathers were slaveowners, but some were also conflicted by the institution, seeing it as immoral and politically divisive; Washington gradually became a cautious supporter of abolitionism and freed his slaves in his will.  John Jay led the successful fight to outlaw the slave trade in New York. Conversely, many founders such as Samuel Adams and John Adams were against slavery their entire lives. Benjamin Rush wrote a pamphlet in 1773 which harshly condemned slavery and besought the colonists to petition the king and put an end to the British African Company of Merchants which kept slavery and the slave trade going.  The Continental Association of 1774 contains a clause severely limiting the slave trade as part of the general boycott of British trade.Franklin, though he was a key founder of the Pennsylvania Abolition Society, originally owned slaves whom he later manumitted. While serving in the Rhode Island Assembly, Stephen Hopkins introduced one of the earliest anti-slavery laws in the colonies, and John Jay would try unsuccessfully to abolish slavery as early as 1777 in the State of New York. He nonetheless founded the New York Manumission Society in 1785, for which Hamilton became an officer. They and other members of the Society founded the African Free School in New York City, to educate the children of free blacks and slaves. When Jay was governor of New York in 1798, he helped secure and signed into law an abolition law; fully ending forced labor as of 1827. He freed his own slaves in 1798. Alexander Hamilton opposed slavery, as his experiences in life left him very familiar with slavery and its effect on slaves and on slaveholders, although he did negotiate slave transactions for his wife's family, the Schuylers. John Adams, Samuel Adams, and Thomas Paine never owned slaves.Slaves and slavery are mentioned only indirectly in the 1787 Constitution. For example, Article 1, Section 2, Clause 3 prescribes that "three-fifths of all other Persons" are to be counted for the apportionment of seats in the House of Representatives and direct taxes.  Additionally, in Article 4, Section 2, Clause 3, slaves are referred to as "persons held in service or labor". The Founding Fathers, however, did make important efforts to contain slavery. Many Northern states had adopted legislation to end or significantly reduce slavery during and after the American Revolution. In 1782 Virginia passed a manumission law that allowed slave owners to free their slaves by will or deed. As a result, thousands of slaves were manumitted in Virginia. Thomas Jefferson, in 1784, proposed to ban slavery in all the Western Territories, which failed to pass Congress by one vote. Partially following Jefferson's plan, Congress did ban slavery in the Northwest Ordinance of 1787, for lands north of the Ohio River.The international slave trade was banned in all states except South Carolina, by 1800. Finally in 1807, President Jefferson called for and signed into law a Federally-enforced ban on the international slave trade throughout the U.S. and its territories. It became a federal crime to import or export a slave. However, the domestic slave trade was allowed, for expansion, or for diffusion of slavery into the Louisiana Territory.


=== Attendance at conventions ===
In the winter and spring of 1786–1787, twelve of the thirteen states chose a total of 74 delegates to attend the Constitutional Convention in Philadelphia. Nineteen delegates chose not to accept election or attend the debates. Among them was Patrick Henry of Virginia, who in response to questions about his refusal to attend was quick to reply, "I smelled a rat." He believed that the frame of government convention organizers were intent on building would trample upon the rights of citizens. Also, Rhode Island's lack of representation at the convention was due to leader's suspicions of the convention delegates' motivations. As the colony was founded by Roger Williams as a sanctuary for Baptists, Rhode Island's absence at the convention in part explains the absence of Baptist affiliation among those who did attend. Of the 55 who did attend at some point, no more than 38 delegates showed up at one time.


=== Spouses and children ===
Only four (Baldwin, Gilman, Jenifer, and Alexander Martin) were lifelong bachelors. Many of the Founding Fathers' wives, like Eliza Schuyler Hamilton, Martha Washington, Abigail Adams, Sarah Livingston Jay, Dolley Madison, Mary White Morris and Catherine Alexander Duer, were strong women who made significant contributions of their own to the fight for liberty.Sherman fathered the largest family: 15 children by two wives. At least nine (Bassett, Brearly, Johnson, Mason, Paterson, Charles Cotesworth Pinckney, Sherman, Wilson, and Wythe) married more than once. George Washington, who became known as "The Father of His Country", had no biological children, though he and his wife raised two children from her first marriage and two grandchildren.


== Signatories to founding documents ==
Among the state documents promulgated between 1774 and 1789 by the Continental Congress, four are paramount: the Continental Association, the Declaration of Independence, the Articles of Confederation, and the United States Constitution. Altogether, 145 men signed at least one of the four documents. In each instance, roughly 50% of the names signed are unique to that document. Only a few people (6) signed three of the four, and only Roger Sherman of Connecticut signed all of them. The following persons signed one or more of these United States formative documents:

Notes:


=== Post-constitution life ===
Subsequent events in the lives of the Founding Fathers after the adoption of the Constitution were characterized by success or failure, reflecting the abilities of these men as well as the vagaries of fate. Washington, Adams, Jefferson, Madison, and Monroe served in highest U.S. office of president. Jay would be appointed as the first chief justice of the United States and later elected to two terms as Governor of New York. Alexander Hamilton would be appointed the first Secretary of the Treasury in 1789, and later Inspector General of the Army under President John Adams in 1798.
Seven (Fitzsimons, Gorham, Luther Martin, Mifflin, Robert Morris, Pierce, and Wilson) suffered serious financial reversals that left them in or near bankruptcy. Robert Morris spent three of the last years of his life imprisoned following bad land deals. Two, Blount and Dayton, were involved in possibly treasonous activities. Yet, as they had done before the convention, most of the group continued to render public service, particularly to the new government they had helped to create.


=== Youth and longevity ===

Many of the Founding Fathers were under 40 years old at the time of the signing of the Declaration of Independence in 1776: Aaron Burr was 20, Alexander Hamilton was 21, Gouverneur Morris was 24. The oldest were Benjamin Franklin, 70, and Samuel Whittemore, 81.A few Founding Fathers lived into their nineties, including: Paine Wingate, who died at age 98; Charles Carroll of Carrollton, who died at age 95; Charles Thomson, who died at 94; William Samuel Johnson, who died at 92; and John Adams, who died at 90. Among those who lived into their eighties were Benjamin Franklin, Samuel Whittmore, John Jay, Thomas Jefferson, James Madison, John Armstrong Jr., Hugh Williamson, and George Wythe. Approximately 16 died while in their seventies, and 21 in their sixties. Three (Alexander Hamilton, Richard Dobbs Spaight, and Button Gwinnett) were killed in duels. Two, John Adams and Thomas Jefferson, died on the same day, July 4, 1826.The last remaining founders, also poetically called the "Last of the Romans", lived well into the nineteenth century. The last surviving signer of the Declaration of Independence was Charles Carroll of Carrollton, who died in 1832. The last surviving member of the Continental Congress was John Armstrong Jr., who died in 1843. He gained this distinction in 1838 upon the death of the only other surviving delegate, Paine Wingate.


== Other notable patriots of the period ==
The following men and women also advanced the new nation through their actions.


== Legacy ==


=== Institutions formed by Founders ===
Several Founding Fathers were instrumental in establishing schools and societal institutions that still exist today:

Franklin founded the University of Pennsylvania, while Jefferson founded the University of Virginia.
George Washington supported the founding of Washington College by consenting to have the "College at Chester" named in his honor, through generous financial support, and through service on the college's Board of Visitors and Governors.Rush founded Dickinson College and Franklin College, (today Franklin and Marshall) as well as the College of Physicians of Philadelphia, the oldest medical society in America.
Hamilton founded the New York Post, as well as the United States Coast Guard.
Knox helped found the Society of the Cincinnati in 1783; the society was predicated on service as an officer in the Revolutionary War and heredity. Members included Washington, Hamilton and Burr. Other Founders like Sam Adams, John Adams, Franklin and Jay criticized the formation of what they considered to be an elitist body and threat to the Constitution. Franklin would later accept an honorary membership though Jay declined.


=== Scholarship on the Founders ===
Articles and books by twenty-first century historians combined with the digitization of primary sources like handwritten letters continue to contribute to an encyclopedic body of knowledge about the Founding Fathers.


==== Historians who focus on the Founding Fathers ====
Ron Chernow won the Pulitzer Prize for his biography of George Washington. His bestselling book about Alexander Hamilton inspired the blockbuster musical of the same name.
Joseph J. Ellis – According to Ellis, the concept of the Founding Fathers of the U.S. emerged in the 1820s as the last survivors died out. Ellis says "the founders", or "the fathers", comprised an aggregate of semi-sacred figures whose particular accomplishments and singular achievements were decidedly less important than their sheer presence as a powerful but faceless symbol of past greatness. For the generation of national leaders coming of age in the 1820s and 1830s – men like Andrew Jackson, Henry Clay, Daniel Webster, and John C. Calhoun – "the founders" represented a heroic but anonymous abstraction whose long shadow fell across all followers and whose legendary accomplishments defied comparison.

We can win no laurels in a war for independence," Webster acknowledged in 1825. "Earlier and worthier hands have gathered them all. Nor are there places for us ... [as] the founders of states. Our fathers have filled them. But there remains to us a great duty of defence and preservation.
Joanne B. Freeman – Freeman's area of expertise is the life and legacy of Alexander Hamilton as well as political culture of the revolutionary and early national eras. Freeman has documented the often opposing visions of the Founding Fathers as they tried to build a new framework for governance, "Regional distrust, personal animosity, accusation, suspicion, implication, and denouncement—this was the tenor of national politics from the outset." Annette Gordon-Reed is an American historian and Harvard Law School professor. She is noted for changing scholarship on Thomas Jefferson regarding his relationship with Sally Hemings and her children. She has studied the challenges facing the Founding Fathers particularly as it relates to their position and actions on slavery. She points out "the central dilemma at the heart of American democracy: the desire to create a society based on liberty and equality" that yet does not extend those privileges to all."David McCullough's Pulitzer Prize winning 2001 book, John Adams., focuses on the Founding Father, and his 2005 book, 1776, details George Washington's military history in the American Revolution and other independence events carried out by America's founders.
Peter S. Onuf – Thomas Jefferson
Jack N. Rakove – Thomas Jefferson


==== Noted collections of the Founding Fathers ====
Adams Papers Editorial Project
Founders Online – a searchable database of over 178,000 documents authored by or addressed to George Washington, John Jay, Benjamin Franklin, John Adams (and family), Thomas Jefferson, Alexander Hamilton and James Madison.
The Papers of Alexander Hamilton
The Selected Papers of John Jay at Columbia University
The Papers of Thomas Jefferson at Princeton University
The Papers of James Madison at University of Virginia
The Washington Papers at University of Virginia
The Franklin Papers at Yale University


=== In stage and film ===
The Founding Fathers were portrayed in the Tony Award–winning 1969 musical 1776, which depicted the debates over, and eventual adoption of, the Declaration of Independence. The stage production was adapted into the 1972 film of the same name.
The 1989 film A More Perfect Union, which was filmed on location in Independence Hall, depicts the events of the Constitutional Convention. The writing and passing of the founding documents are depicted in the 1997 documentary miniseries Liberty!, and the passage of the Declaration of Independence is portrayed in the second episode of the 2008 miniseries John Adams and the third episode of the 2015 miniseries Sons of Liberty. The Founders also feature in the 1986 miniseries George Washington II: The Forging of a Nation, the 2002-03 animated television series Liberty's Kids, the 2020 miniseries Washington, and in many other films and television portrayals.
Several Founding Fathers—Hamilton, Washington, Jefferson, Madison and Burr—were reimagined in Hamilton, a 2015 musical inspired by the 2004 biography Alexander Hamilton, with music, lyrics and book by Lin-Manuel Miranda. The musical won eleven Tony Awards and a Pulitzer Prize for Drama.


=== Children's books ===
In their 2015 children's book, The Founding Fathers author Jonah Winter and illustrator Barry Blitt categorized 14 leading patriots into two teams based on their contributions to the formation of America – the Varsity Squad (Washington, Franklin, Jefferson, John Adams, Madison, Jay, and Hamilton) and the Junior Varsity Squad (Sam Adams, Hancock, Henry, Morris, Marshall, Rush, and Paine).


== See also ==

Father of the Nation
History of the United States Constitution
History of the United States (1776–1789)
List of national founders (worldwide)
Military leadership in the American Revolutionary War
Patriot (American Revolution)
Rights of Englishmen
Sons of Liberty


== References ==


== Further reading ==


== External links ==
Founders Online: Correspondence and Other Writings of Six Major Shapers of the United States
Debunks – along with other fact finding sites – the Internet Myth of "What Happened to The Signers of the Declaration of Independence" (Published June 28, 2005) (Retrieved January 30, 2015)
What Would the Founding Fathers Do Today? at the Wayback Machine (archived January 14, 2007)
"Founding Father Quotes, Biographies, and Writings"