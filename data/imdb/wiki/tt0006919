Rubáiyát of Omar Khayyám is the title that Edward FitzGerald gave to his 1859 translation from Persian to English of a selection of quatrains (rubāʿiyāt) attributed to Omar Khayyam (1048–1131), dubbed "the Astronomer-Poet of Persia".
Although commercially unsuccessful at first, FitzGerald's work was popularised from 1861 onward by Whitley Stokes, and the work came to be greatly admired by the Pre-Raphaelites in England. FitzGerald had a third edition printed in 1872, which increased interest in the work in the United States. By the 1880s, the book was extremely popular throughout the English-speaking world, to the extent that numerous "Omar Khayyam clubs" were formed and there was a "fin de siècle cult of the Rubaiyat".FitzGerald's work has been published in several hundred editions and has inspired similar translation efforts in English and in many other languages.


== Sources ==

The authenticity of the poetry attributed to Omar Khayyam is highly uncertain. Khayyam was famous during his lifetime not as a poet but as an astronomer and mathematician. The earliest reference to his having written poetry is found in his biography by al-Isfahani, written 43 years after his death. This view is reinforced by other medieval historians such as Shahrazuri (1201) and Al-Qifti (1255). Parts of the Rubaiyat appear as incidental quotations from Omar in early works of biography and in anthologies. These include works of Razi (ca. 1160–1210), Daya (1230), Juvayni (ca. 1226–1283), and Jajarmi (1340). Also, five quatrains assigned to Khayyam in somewhat later sources appear in Zahiri Samarqandi's Sindbad-Nameh (before 1160) without attribution.The number of quatrains attributed to him in more recent collections varies from about 1,200 (according to Saeed Nafisi) to more than 2,000. Skeptical scholars point out that the entire tradition may be pseudepigraphic.
The extant manuscripts containing collections attributed to Omar are dated much too late to enable a reconstruction of a body of authentic verses.
In the 1930s, Iranian scholars, notably Mohammad-Ali Foroughi, attempted to reconstruct a core of authentic verses from scattered quotes by authors of the 13th and 14th centuries, ignoring the younger manuscript tradition. After World War II, reconstruction efforts were significantly delayed by two clever forgeries. De Blois (2004) is pessimistic, suggesting that contemporary scholarship has not advanced beyond the situation of the 1930s, when Hans Heinrich Schaeder commented that the name of Omar Khayyam "is to be struck out from the history of Persian literature".A feature of the more recent collections is the lack of linguistic homogeneity and continuity of ideas. Sadegh Hedayat commented that "if a man had lived for a hundred years and had changed his religion, philosophy, and beliefs twice a day, he could scarcely have given expression to such a range of ideas". Hedayat's final verdict was that 14 quatrains could be attributed to Khayyam with certainty. Various tests have been employed to reduce the quatrains attributable to Omar to about 100. Arthur Christensen states that "of more than 1,200 ruba'is known to be ascribed to Omar, only 121 could be regarded as reasonably authentic". Foroughi accepts 178 quatrains as authentic, while Ali Dashti accepts 36 of them.FitzGerald's source was transcripts sent to him in 1856–57, by his friend and teacher Edward B. Cowell, of two manuscripts, a Bodleian manuscript with 158 quatrains
and a "Calcutta manuscript".
FitzGerald completed his first draft in 1857 and sent it to Fraser's Magazine in January 1858.
He made a revised draft in January 1859, of which he privately printed 250 copies. This first edition became extremely sought after by the 1890s, when "more than two million copies ha[d] been sold in two hundred editions".


== Skepticism vs. Sufism debate ==
The extreme popularity of FitzGerald's work led to a prolonged debate on the correct interpretation of the philosophy behind the poems. FitzGerald emphasized the religious skepticism he found in Omar Khayyam. In his preface to the Rubáiyát, he describes Omar's philosophy as Epicurean and claims that Omar was "hated and dreaded by the Sufis, whose practice he ridiculed and whose faith amounts to little more than his own, when stripped of the Mysticism and formal recognition of Islamism under which Omar would not hide". Richard Nelson Frye also emphasizes that Khayyam was despised by a number of prominent contemporary Sufis. These include figures such as Shams Tabrizi, Najm al-Din Daya, Al-Ghazali, and Attar, who "viewed Khayyam not as a fellow-mystic, but a free-thinking scientist". The skeptic interpretation is supported by the medieval historian Al-Qifti (ca. 1172–1248), who in his The History of Learned Men reports that Omar's poems were only outwardly in the Sufi style but were written with an anti-religious agenda. He also mentions that Khayyam was indicted for impiety and went on a pilgrimage to avoid punishment.Critics of FitzGerald, on the other hand, have accused the translator of misrepresenting the mysticism of Sufi poetry by an overly literal interpretation. Thus, the view of Omar Khayyam as a Sufi was defended by Bjerregaard (1915). Dougan (1991) likewise says that attributing hedonism to Omar is due to the failings of FitzGerald's translation, arguing that the poetry is to be understood as "deeply esoteric".Idries Shah (1999) similarly says that FitzGerald misunderstood Omar's poetry.The Sufi interpretation is the view of a minority of scholars. Henry Beveridge states that "the Sufis have unaccountably pressed this writer [Khayyam] into their service; they explain away some of his blasphemies by forced interpretations, and others they represent as innocent freedoms and reproaches". Aminrazavi (2007) states that "Sufi interpretation of Khayyam is possible only by reading into his Rubaiyat extensively and by stretching the content to fit the classical Sufi doctrine".FitzGerald's "skepticist" reading of the poetry is still defended by modern scholars. Sadegh Hedayat (The Blind Owl 1936) was the most notable modern proponent of Khayyam's philosophy as agnostic skepticism. In his introductory essay to his second edition of the Quatrains of the Philosopher Omar Khayyam (1922), Hedayat states that "while Khayyam believes in the transmutation and transformation of the human body, he does not believe in a separate soul; if we are lucky, our bodily particles would be used in the making of a jug of wine". He concludes that "religion has proved incapable of surmounting his inherent fears; thus Khayyam finds himself alone and insecure in a universe about which his knowledge is nil". In his later work (Khayyam's Quatrains, 1935), Hedayat further maintains that Khayyam's usage of Sufic terminology such as "wine" is literal, and that "Khayyam took refuge in wine to ward off bitterness and to blunt the cutting edge of his thoughts."


== Editions ==

FitzGerald's text was published in five editions, with substantial revisions:

1st edition – 1859 [75 quatrains]
2nd edition – 1868 [110 quatrains]
3rd edition – 1872 [101 quatrains]
1878, "first American edition", reprint of the 3rd ed.
4th edition – 1879 [101 quatrains]
5th edition – 1889 [101 quatrains]Of the five editions published, four were published under the authorial control of FitzGerald. The fifth edition, which contained only minor changes from the fourth, was edited posthumously on the basis of manuscript revisions FitzGerald had left.
Numerous later editions were published after 1889, notably an edition with illustrations by Willy Pogany first published in 1909 (George G. Harrap, London). It was issued in numerous revised editions. This edition combined FitzGerald's texts of the 1st and 4th editions and was subtitled "The First and Fourth Renderings in English Verse".
A bibliography of editions compiled in 1929 listed more than 300 separate editions.  Many more have been published since.Notable editions of the late 19th and early 20th centuries include:
Houghton, Mifflin & Co. (1887, 1888, 1894);
Doxey, At the Sign of the Lark (1898, 1900),  illustrations by Florence Lundborg;
The Macmillan Company (1899);
Methuen (1900) with a commentary by H.M. Batson, and a biographical introduction by E.D. Ross;
Little, Brown, and Company (1900), with the versions of E.H. Whinfield and Justin Huntly McCart;
Bell (1901); Routledge (1904);
Foulis (1905, 1909);
Essex House Press (1905);
Dodge Publishing Company (1905);
Duckworth & Co. (1908);
Hodder and Stoughton (1909), illustrations by Edmund Dulac;
Tauchnitz (1910); 
East Anglian Daily Times (1909), Centenary celebrations souvenir; 
Warner (1913);
The Roycrofters (1913);
Hodder & Stoughton (1913), illustrations by René Bull;
Dodge Publishing Company (1914), illustrations by Adelaide Hanscom.
Sully and Kleinteich (1920).
Critical editions have been published by Decker (1997) and by Arberry (2016).


== Character of translation ==
FitzGerald's translation is rhyming and metrical, and rather free. Many of the verses are paraphrased, and some of them cannot be confidently traced to his source material at all. Michael Kearney claimed that FitzGerald described his work as "transmogrification". To a large extent, the Rubaiyat can be considered original poetry by FitzGerald loosely based on Omar's quatrains rather than a "translation" in the narrow sense.
FitzGerald was open about the liberties he had taken with his source material:

My translation will interest you from its form, and also in many respects in its detail: very un-literal as it is. Many quatrains are mashed together: and something lost, I doubt, of Omar's simplicity, which is so much a virtue in him. (letter to E. B. Cowell, 9/3/58)
I suppose very few People have ever taken such Pains in Translation as I have: though certainly not to be literal. But at all Cost, a Thing must live: with a transfusion of one's own worse Life if one can’t retain the Original's better. Better a live Sparrow than a stuffed Eagle. (letter to E. B. Cowell, 4/27/59)
For comparison, here are two versions of the same quatrain by FitzGerald, from the 1859 and 1889 editions:

This quatrain has a close correspondence in two of the quatrains in the Bodleian Library ms., numbers 149 and 155. In the literal prose translation of 
Edward Heron-Allen (1898):


== Other translations ==


=== English ===
Multilingual edition, published in 1955 by Tahrir Iran Co./Kashani Bros.
Two English editions by Edward Henry Whinfield (1836–1922) consisted of 253 quatrains in 1882 and 500 in 1883. This translation was fully revised and some cases fully translated anew by Ali Salami and published by Mehrandish Books.
Whinfield's translation is, if possible, even more free than FitzGerald's; Quatrain 84 (equivalent of FitzGerald's quatrain XI in his 1st edition, as above) reads:

John Leslie Garner published an English translation of 152 quatrains in 1888. His was also a free, rhyming translation.
Quatrain I. 20 (equivalent of FitzGerald's quatrain XI in his 1st edition, as above):

Justin Huntly McCarthy (1859–1936) (Member of Parliament for Newry) published prose translations of 466 quatrains in 1889.
Quatrain 177 (equivalent of FitzGerald's quatrain XI in his 1st edition, as above):

Richard Le Gallienne (1866–1947) produced a verse translation, subtitled "a paraphrase from several literal translations", in 1897. 
In his introductory note to the reader, Le Gallienne cites McCarthy's "charming prose" as the chief influence on his version. Some example quatrains follow:

Edward Heron-Allen (1861–1943) published a prose translation in 1898. He also wrote an introduction to an edition of the translation by Frederick Rolfe (Baron Corvo) into English from Nicolas's French translation. Below is Quatrain 17 translated by E. H. into English:

The English novelist and orientalist Jessie Cadell (1844–1884) consulted various manuscripts of the Rubaiyat with the intention of producing an authoritative edition. Her translation of 150 quatrains was published posthumously in 1899.A. J. Arberry in 1959 attempted a scholarly edition of Khayyam, based on thirteenth-century manuscripts. However, his manuscripts were subsequently exposed as twentieth-century forgeries. While Arberry's work had been misguided, it was published in good faith.
The 1967 translation of the Rubáiyat by Robert Graves and Omar Ali-Shah, however, created a scandal. The authors claimed it was based on a twelfth-century manuscript located in Afghanistan, where it was allegedly utilized as a Sufi teaching document. But the manuscript was never produced, and British experts in Persian literature were easily able to prove that the translation was in fact based on Edward Heron Allen's analysis of possible sources for FitzGerald's work.Quatrains 11 and 12 (equivalent of FitzGerald's quatrain XI in his 1st edition, as above):

John Charles Edward Bowen (1909–1989) was a British poet and translator of Persian poetry. He is best known for his translation of the Rubaiyat, titled A New Selection from the Rubaiyat of Omar Khayyam. Bowen is also credited as being one of the first scholars to question Robert Graves' and Omar Ali-Shah's translation of the Rubaiyat.A modern version of 235 quatrains, claiming to be "as literal an English version of the Persian originals as readability and intelligibility permit", was published in 1979 by Peter Avery and John Heath-Stubbs. 
Their edition provides two versions of the thematic quatrain, the first (98) considered by the Persian writer Sadeq Hedayat to be a spurious attribution.

In 1988, the Rubaiyat was translated by an Iranian for the first time. Karim Emami's translation of the Rubaiyat was published under the title The Wine of Nishapour in Paris. The Wine of Nishapour is the collection of Khayyam's poetry by Shahrokh Golestan, including Golestan's pictures in front of each poem.
Example quatrain 160 (equivalent to FitzGerald's quatrain XI in his first edition, as above):

In 1991, Ahmad Saidi (1904–1994) produced an English translation of 165 quatrains grouped into 10 themes. Born and raised in Iran, Saidi went to the United States in 1931 and attended college there. He served as the head of the Persian Publication Desk at the U.S. Office of War Information during World War II, inaugurated the Voice of America in Iran, and prepared an English-Persian military dictionary for the Department of Defense. His quatrains include the original Persian verses for reference alongside his English translations. His focus was to faithfully convey, with less poetic license, Khayyam's original religious, mystical, and historic Persian themes, through the verses as well as his extensive annotations. Two example quatrains follow:
Quatrain 16 (equivalent to FitzGerald's quatrain XII in his 5th edition, as above):

Quatrain 75:


=== German ===
Adolf Friedrich von Schack (1815–1894) published a German translation in 1878.
Quatrain 151 (equivalent of FitzGerald's quatrain XI in his 1st edition, as above):

Friedrich Martinus von Bodenstedt (1819–1892) published a German translation in 1881. The translation eventually consisted of 395 quatrains.
Quatrain IX, 59 (equivalent of FitzGerald's quatrain XI in his 1st edition, as above):


=== French ===
The first French translation, of 464 quatrains in prose, was made by J. B. Nicolas, chief interpreter at the French embassy in Persia in 1867.
Prose stanza (equivalent of Fitzgerald's quatrain XI in his 1st edition, as above):

The best-known version in French is the free verse edition by Franz Toussaint (1879–1955) published in 1924. This translation consisting of 170 quatrains was done from the original Persian text, while most of the other French translations were themselves translations of FitzGerald's work. The Éditions d'art Henri Piazza published the book almost unchanged between 1924 and 1979. Toussaint's translation has served as the basis of subsequent translations into other languages, but Toussaint did not live to witness the influence his translation has had.
Quatrain XXV (equivalent of FitzGerald's quatrain XI in his 1st edition, as above):


=== Russian ===
Many Russian-language translations have been undertaken, reflecting the popularity of the Rubaiyat in Russia since the late 19th century and the increasingly popular tradition of using it for the purposes of bibliomancy. The earliest verse translation (by Vasily Velichko) was published in 1891. The version by Osip Rumer published in 1914 is a translation of FitzGerald's version. Rumer later published a version of 304 rubaiyat translated directly from Persian. A lot of poetic translations (some based on verbatim translations into prose by others) were also written by German Plisetsky, Konstantin Bal'mont, Cecilia Banu, I. I. Tkhorzhevsky (ru), L. Pen'kovsky, and others.


=== Other languages ===
In Polish, several collections of Rubaiyat have appeared, including ones by Professor Andrzej Gawroński (1933, 1969),  regarded as the best.
The first translation of nine short poems into Serbo-Croatian was published in 1920, and was the work of Safvet beg Bašagić. In 1932, Jelena Skerlić-Ćorović re-published these nine, alongside 75 more poems. In 1964, noted orientalist Fehim Bajraktarević published his translation of the Rubaiyat.
In Icelandic Magnús Ásgeirsson translated the Rubaiyat in 1935. There was an earlier translation by Einar Benediktsson in 1921. Jochum M. Eggertsson (Skuggi) published a translation in 1946. All translations are of FitzGerald's version.
First Czech translator is Josef Štýbr. At first he translated from English (from FitzGerald's "translations") (1922), after that from original language (1931). Translation from the original can be found on Czech wikisource (770 poems). Subsequent translators are mentioned here.
The poet J. H. Leopold (1865–1925) rendered a number of rubaiyat into Dutch.
Eric Hermelin translated the Rubaiyat into Swedish in 1928.
Sir John Morris-Jones translated direct from Persian into Welsh in 1928.
In Finnish, the first translations were made by Toivo Lyy in 1929. More recently Jaakko Hämeen-Anttila (1999 and 2008) and Kiamars Baghban with Leevi Lehto (2009) have translated Khayyam into Finnish.
G. Sankara Kurup produced a translation into Malayalam (1932).
Duvvoori Ramireddy translated the Rubaiyat into Telugu in 1935.
Thomas Ifor Rees produced a Welsh translation, published in Mexico City in 1939.
Kantichandra Ghosh, Muhammad Shahidullah (in 1942), Kazi Nazrul Islam (in 1958) and Shakti Chattopadhyay (in 1978) produced translations into Bengali.
The earliest translation in Hungarian consisted of a few stanzas taken from the French version of Nicolas, by Béla Erődi in 1919–20. Lőrinc Szabó finalized his translation of the FitzGerald version in 1943.
Fraînque Le Maistre produced a Jèrriais version (based on FitzGerald's 1st edition) during the German occupation of the Channel Islands (1940–1945).
Srimadajjada Adibhatla Narayana Das (1864–1945) translated the original Persian quatrains and Edward FitzGerald's English translations into Sanskrit and pure-Telugu. Pandit Narayana Das claimed his translation was more literal than that of FitzGerald. (See Ajjada Adibhatla Narayana Dasu.)
Poet Cornelis Jacobus Langenhoven (1873–1932, author of "Die Stem van Suid-Afrika") produced the first translation in Afrikaans. Herman Charles Bosman wrote a translation into Afrikaans published in 1948.
In Japan, until 1949, more than 10 poets and/or scholars made translations into Japanese. The first complete translation from Persian into the modern Japanese language was made by Ryosaku Ogawa in 1949, which is still popular and has been published from Iwanami Shoten (it is now in the public domain and also freely available from Aozora Bunko). Historically, the first attempt was six poems translated by Kambara Ariake in 1908. In 1910, Kakise Hikozo translated 110 poems from the 5th edition of FitzGerald's translation. The first translation from Persian into the classical Japanese language was made by a linguist, Shigeru Araki, in 1920. Among various other translations, Ogawa highly evaluates Ryo Mori's (ja:森亮), produced in 1931.
D. V. Gundappa translated the work into Kannada as a collection of poems titled "Umarana Osage" in 1952.
Robert Bin Shaaban produced a version in Swahili (dated 1948, published 1952).
Gopal Chandra Kanungo illustrated and translated the book into Odia in 1954.
Devdas Chhotray adapted Edward FitzGerald's work into Oriya and recorded it in musical form in 2011.
The first translator into Slovene was Alojz Gradnik, his translation being published in 1955. It was translated again by Slovene translator and poet Bert Pribac in 2007 from the French Toussaint edition.
Maithili Sharan Gupt and Harivanshrai Bachchan translated the book into Hindi in 1959.
Francesco Gabrieli produced an Italian translation (Le Rubaiyyàt di Omar Khayyàm) in 1944. Alessandro Bausani produced another translation in 1965.
It was translated into Latvian by Andrejs Kurcijs in 1970.
Christos Marketis translated 120 rubaiyat into Greek in 1975.
172 rubaiyat were translated into Belarusian  by Ryhor Baradulin in 1989.
Thirunalloor Karunakaran translated the Rubaiyat into Malayalam in 1989.
In 1990, Jowann Richards produced a Cornish translation.
Scottish poet Rab Wilson published a version in Scots in 2004.
In 2015 it was translated into Romanian for the first time by orientalist philologist Gheorghe Iorga.
Kerson Huang based a Chinese version on FitzGerald's version.
Fan Noli produced an Albanian translation in 1927, the melody and poetics of which are highly regarded.
At least four versions exist in the Thai language. These translations were made from the text of FitzGerald. Their respective authors are HRH Prince Narathip Prapanpong, Rainan Aroonrungsee (pen name: Naan Gitirungsi), Pimarn Jamjarus (pen name: Kaen Sungkeet), and Suriyachat Chaimongkol.
Haljand Udam produced an Estonian translation.
Ahmed Rami, a famous late Egyptian poet, translated the work into Arabic. His translation was sung by Umm Kulthum.
The Kurdish poet Hajar translated the Rubaiyat in his Chwar Parchakani Xayam.
Armenian poet Kevork Emin has translated several verses of the Rubaiyat.
The Assyrian journalist and poet Naum Faiq translated the Rubaiyat into the Syriac language.
The Assyrian author Eshaya Elisha Khinno translated the Rubaiyat into Sureth (Assyrian Neo-Aramaic) in 2012
Hồ Thượng Tuy translated from English into Vietnamese (from FitzGerald's 1st edition) in 1990.
Nguyễn Viết Thắng produced a Vietnamese translation of 487 rubaiyat, translated from English and Russian in 1995, published in Hanoi in 2003.
Xabier Correa Corredoira published a Galician translation in 2010.
Hemendra Kumar Roy translated the Rubaiyat into Bengali.
Radha Mohan Gadanayak translated the Rubaiyat into Odia
Tadhg Ó Donnchadha (Torna) translated the Rubaiyat from English into Irish in 1920.
The Filipino poet and linguist Ildefonso Santos published his Tagalog translation in 1953.


== Influence ==
FitzGerald rendered Omar's name as "Omar the Tentmaker", and this name resonated in English-speaking popular culture for a while. Thus, Nathan Haskell Dole published a novel called Omar, the Tentmaker: A Romance of Old Persia in 1898. Omar the Tentmaker of Naishapur is a historical novel by John Smith Clarke, published in 1910. 
"Omar the Tentmaker" is a 1914 play in an oriental setting by Richard Walton Tully, adapted as a silent film in 1922.
US General Omar Bradley was given the nickname "Omar the Tent-Maker" in World War II, and the name has been recorded as a slang expression for "penis".
FitzGerald's translations also reintroduced Khayyam to  Iranians, "who had long ignored the Neishapouri poet".


=== Literature ===
The title of Rex Stout's Nero Wolfe novel Some Buried Caesar comes from one of the Tentmaker's quatrains (FitzGerald's XVIII), for example.
Eugene O'Neill's drama Ah, Wilderness! derives its title from the first quoted quatrain above.
Agatha Christie used The Moving Finger as a story title, as did Stephen King. See also And Having Writ….
Lan Wright used Dawn's Left Hand as the title of a science fiction story serialized in New Worlds Science Fiction (January–March 1963).
The title of Allen Drury's science fiction novel The Throne of Saturn comes from a quatrain which appears as the book's epigraph.
The title of Nevil Shute Norway's novel The Chequer Board is taken from Stanza LXIX, and that stanza appears as the book's epigraph.Equally noteworthy are these works likewise influenced:

The satirist and short story writer Hector Hugh Munro took his pen name of 'Saki' from Edward FitzGerald's translation of the Rubaiyat.
The American author O. Henry humorously referred to a book by "Homer KM" with the character "Ruby Ott" in his short story "The Handbook of Hymen. " O. Henry also quoted a quatrain from the Rubaiyat of Omar Khayyam in "The Rubaiyat of a Scotch Highball".
Oliver Herford released a parody of the Rubaiyat called "The Rubaiyat of a Persian Kitten" in 1904, which is notable for its charming illustrations of the kitten in question on his philosophical adventures.
The artist/illustrator Edmund Dulac produced some much-beloved illustrations for the Rubaiyat, 1909.
The play The Shadow of a Gunman (1923) by Seán O'Casey contains a reference to the Rubaiyat as the character Donal Davoren quotes "grasp this sorry scheme of things entire, and mould life nearer to the heart's desire".
The Argentinian writer Jorge Luis Borges discusses The Rubaiyat and its history in an essay, "The Enigma of Edward FitzGerald" ("El enigma de Edward FitzGerald") in his book "Other Inquisitions" ("Otras Inquisiciones", 1952). He also references it in some of his poems, including "Rubaiyat" in "The Praise of the Shadow" ("Elogio de la Sombra", 1969), and "Chess" ("Ajedrez") in "The Maker" ("El Hacedor", 1960). Borges' father Jorge Guillermo Borges was the author of a Spanish translation of the FitzGerald version of The Rubaiyat.
Science fiction author Paul Marlowe's story "Resurrection and Life" featured a character who could only communicate using lines from the Rubaiyat.
Science fiction author Isaac Asimov quotes The Moving Finger in his time-travel novel The End of Eternity  when a character discusses whether history could be changed.
Charles Schultz wrote a strip in which Lucy reads the Jug of Wine passage, and Linus asks "No blanket?".
Wendy Cope's poem "Strugnell's Rubiyat" is a close parody of the FitzGerald translation, relocated to modern day Tulse Hill.
One of the title pages of Principia Discordia (1965), a co-author of which went by the pen-name Omar Khayyam Ravenhurst, features its own spin on the quatrain most quoted above:A jug of wine,
A leg of lamb
And thou!
Beside me,
Whistling in
the darkness.The Lebanese writer Amin Maalouf based his novel Samarkand (1988) on the life of Omar Khayyam, and the creation of the Rubaiyat. It details the Assassin sect as well, and includes a fictional telling of how the (non-existent) original manuscript came to be on the RMS Titanic.
In the opening chapter of his book God is Not Great (2007), Christopher Hitchens quotes from Richard Le Gallienne's translation of Khayyam's famous quatrain:And do you think that unto such as you
A maggot-minded, starved, fanatic crew
God gave the secret, and denied it me?
Well, well—what matters it? Believe that, too!The title of Daphne du Maurier's memoir Myself when Young is a quote from quatrain 27 of Fitzgerald's translation:Myself when young did eagerly frequent
Doctor and Saint, and heard great Argument
About it and about: but evermore
Came out by the same Door as in I went.


=== Cinema ===
Filmmaker D.W. Griffith planned a film based on the poems as a follow-up to Intolerance in 1916. It was to star Miriam Cooper, but when she left the Griffith company the plans were dropped; he would ultimately film Broken Blossoms instead.
Text from the Rubaiyat appeared in intertitles of the lost film A Lover's Oath (1925)
The lines "When Time lets slip a little perfect hour, O take it—for it will not come again." appear in the intertitles of Torrent, the 1926 film starring Greta Garbo and Ricardo Cortez.
Part of the quatrain beginning "The Moving Finger writes ... " was quoted in Algiers, the 1938 movie starring Charles Boyer and Hedy Lamarr.
A canto was quoted and used as an underlying theme of the 1945 screen adaptation of The Picture of Dorian Gray: "I sent my soul through the invisible, some letters of that after-life to spell, and by and by my soul did return, and answered, 'I myself am Heaven and Hell.'"
The Rubaiyat was quoted in the 1946 King Vidor Western film Duel in the Sun, which starred Gregory Peck and Jennifer Jones: "Oh threats of hell and hopes of paradise! One thing at least is certain: This life flies. One thing is certain and the rest is Lies; The Flower that once is blown for ever dies."
The 1951 film Pandora and the Flying Dutchman, starring James Mason and Ava Gardner, opens with an illuminated manuscript of the quatrain beginning "The moving finger writes...".
In the film The Music Man (based on the 1957 musical), town librarian Marian Paroo draws down the wrath of the mayor's wife for encouraging the woman's daughter to read a book of "dirty Persian poetry". Summarizing what she calls the "Ruby Hat of Omar Kayayayayay...I am appalled!!", the mayor's wife paraphrases FitzGerald's Quatrain XII from his 5th edition: "People lying out in the woods eating sandwiches, and drinking directly out of jugs with innocent young girls."
The film Omar Khayyam, also known as The Loves Of Omar Khayyam, was released in 1957 by Paramount Pictures and includes excerpts from the Rubaiyat.
In Back to the Future the character Lorraine Baines, played by Lea Thompson, is holding a copy of the book in 1955 at the high school when her son Marty McFly is trying to introduce her to his father.
The Rubaiyat was quoted in the film 12 Monkeys (1995) around 11 minutes in.
In Adrian Lyne's Unfaithful a copy of the text in French is quoted in English: "Drink wine, this is life eternal //This, all that youth will give to you//It is the season for wine, roses//And drunken friends//Be happy for this moment//This moment is your life." The book is a gift given flirtatiously to Diane Lane's character by Olivier Martinez who plays rare book dealer Paul Martel in the film.


=== Music ===
The British composer Granville Bantock produced a choral setting of FitzGerald's translation 1906–1909.
Using FitzGerald's translation, the Armenian-American composer Alan Hovhaness set a dozen of the quatrains to music. This work, The Rubaiyat of Omar Khayyam, Op. 308, calls for narrator, orchestra, and solo accordion.
The Rubaiyat have also influenced Arabic music. In 1950 the Egyptian singer Umm Kulthum recorded a song entitled "Rubaiyat Al-Khayyam".
The Comedian Harmonists in "Wochenend und Sonnenschein".
Woody Guthrie recorded an excerpt of the Rubaiyat set to music that was released on Hard Travelin' (The Asch Recordings Vol. 3).
The Human Instinct's album Pins In It (1971) opens with a track called "Pinzinet", the lyrics of which are based on the Rubaiyat.
Elektra Records released a compilation album named Rubáiyát in 1990 to commemorate the 40th anniversary of the Elektra Records record label.
Coldcut produced an album with a song called "Rubaiyat" on their album Let us Play! (1997). This song contains what appears to be some words from the English translation.
Jazz-soul harpist Dorothy Ashby's 1970 album The Rubaiyat of Dorothy Ashby quotes from several of the poem's verses.
The famed "skull and roses" poster for a Grateful Dead show at the Avalon Ballroom done by Alton Kelley and Stanley Mouse was adapted from Edmund J. Sullivan's illustrations for The Rubaiyat of Omar Khayyam.
The work influenced the 2004 concept album The Rubaiyyat of Omar Khayyam by the Italian group Milagro acustico.
The song "Beautiful Feeling" by Australian singer-songwriter Paul Kelly, on 2004 album Ways and Means, includes the lyrics "A jug of wine, a loaf of bread and thee, lying on a blanket underneath that big old spreading tree." This song was used as the theme song in the 2004 Australian television drama, Fireflies.
The 1953 Robert Wright-George Forrest musical Kismet, adapted from a play by Edward Knoblock, contains a non-singing character, Omar (it is implied that he is the poet himself), who recites some of the couplets in the Fitzgerald translation.
The record label Ruby Yacht gets its namesake, in part, from the Rubáiyát of Omar Khayyám.
milo's album budding ornithologists are weary of tired analogies features a couple of references to the Rubaiyat.


=== Television ===
In one 6-episode story arc of The Rocky and Bullwinkle Show, Bullwinkle finds the "Ruby Yacht of Omar Khayyam" in the town of Frostbite Falls (on the shores of Veronica Lake).  This pun is deemed so bad, the characters groan, and narrator William Conrad quips, "Well, you don't come up with an awful thing like that, and not hit the front page!"
In the American television drama, Have Gun - Will Travel, the sixth episode of the sixth season is titled "The Bird of Time". The last lines are the main character, Paladin, quoting from Quatrain VII, "The Bird of Time has but a little way To flutter—and the Bird is on the Wing."
A copy of the Rubaiyat plays a role in an episode of the TV series New Amsterdam and is shown to be the inspiration for the name of one of the lead character's children, Omar York.
In the Australian 2014 television drama, Anzac Girls, Lieutenant Harry Moffitt reads from the Rubaiyat to his sweetheart, nurse Sister Alice Ross-King.
In "The Moving Finger" episode of  'I Dream of Jeannie' Jeannie tries out to be a movie star and her screen test is her reciting the Rubaiyat


=== Other media ===
In Cyberflix's PC game Titanic: Adventure Out of Time, the object is to save three important items, the Rubaiyat of Omar Khayyam, one of Adolf Hitler's paintings, and a notebook that proves German officials were attempting to gain geo-political advantage by instigating communist revolution. Finding the Rubaiyat will prevent World War I, as the book is used to fund the assassination of Archduke Franz Ferdinand. Two passages from the book are also included in the game as clues to progress the narrative.
Some versions of the computer game Colossal Cave Adventure feature a ruby-covered yacht called "Omar Khayyam" (a pun – the "ruby yacht" of Omar Khayyam).


=== Other ===
In Australia, a copy of FitzGerald's translation and its closing words, Tamam Shud ("Ended") were major components of the unsolved Tamam Shud case.
The Supreme Court of the Philippines, through a unanimous opinion written in 2005 by Associate Justice Leonardo Quisumbing, quoted "The Moving Finger" when it ruled that the widow of defeated presidential candidate Fernando Poe Jr. could not substitute her late husband in his pending election protest against President Gloria Macapagal Arroyo, thus leading to the dismissal of the protest.
There was a real jewel-encrusted copy of the book on the Titanic. It had been crafted in 1911 by the firm of Sangorski & Sutcliffe in London. It was won at a Sotheby's auction in London on 29 March 1912 for £405 (a bit over $2,000 in 1912) to Gabriel Weis, an American, and was being shipped to New York. The book remains lost at the bottom of the Atlantic to this day.


=== Anniversary events ===
2009 marked the 150th anniversary of Fitzgerald's translation, and the 200th anniversary of Fitzgerald's birth. Events marking these anniversaries included:

The Smithsonian's traveling exhibition Elihu Vedder's Drawings for the Rubaiyat at the Phoenix Art Museum, 15 November 2008 – 8 February 2009
The exhibition Edward Fitzgerald & The Rubaiyat from the collection of Nicholas B. Scheetz at the Grolier Club, 22 January – 13 March 2009.
The exhibition Omar Khayyám. Een boek in de woestijn. 150 jaar in Engelse vertaling at the Museum Meermanno, The Hague, 31 January – 5 April 2009
The exhibition The Persian Sensation: The Rubaiyat of Omar Khayyam in the West at the Harry Ransom Humanities Research Center at The University of Texas at Austin, 3 February – 2 August 2009
An exhibition at the Cleveland Public Library Special Collections, opening 15 February 2009
The joint conference, Omar Khayyam, Edward FitzGerald and The Rubaiyat, held at Cambridge University and Leiden University, 6–10 July 2009
The Folio Society published a limited edition (1,000 copies) of the Rubáiyát to mark the 150th anniversary.


== See also ==
Tamam Shud case
Ubi sunt § In Persian poetry


== References ==

William Mason, Sandra Martin, The Art of Omar Khayyam: Illustrating FitzGerald's Rubaiyat (2007).


== External links ==
FitzGeraldBibliography of editions (omarkhayyamnederland.com)
List of editions (WorldCat)
 The Rubáiyát of Omar Khayyám public domain audiobook at LibriVox
The illustrated Rubáiyát of Omar Khayyám, translated by Edward Fitzgerald, at Internet Archive.
The Persian Poet, contains the translations by Edward FitzGerald and a biography.
Project Gutenberg: etext#246 (translation by Edward FitzGerald)
The Rubáiyát of Omar Khayyám at Faded Page (Canada)
The entire book in DNL E-Book format.
The complete four edition translations by Edward FitzGerald, with illustrations by Blanche McManus at Kellscraft.com.OtherDatabase of manuscripts of the Rubáiyát of Omar Khayyám (cam.ac.uk)
Graves and Ali-Shah.
Rubaiyat of Omar Khayyam a collection of rubaiyat in Persian, accompanied by several translations into English and German.
A comparison between the translations by Heron-Allen and Talbot.
Syracuse University's Special Collections Research Center has in its Rare Books holdings more than 300 different editions of the Rubaiyat
The Harry Ransom Center at the University of Texas at Austin holds over 1,500 items related to the Rubaiyat, including two copies of the first edition, hundreds of editions, translations, and parodies, several Persian manuscripts containing rubaiyat, and ephemera, manuscripts and correspondence documenting the phenomenon of "Omariana"
Toussaint's Translation (French)