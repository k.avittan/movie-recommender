The Man of Destiny is an 1897 play by George Bernard Shaw, set in Italy during the early career of Napoleon. It was published as a part of Plays Pleasant, which also included Arms and the Man, Candida and You Never Can Tell. Shaw titled the volume Plays Pleasant in order to contrast it with his first book of plays, Plays Unpleasant.


== Characters ==
Napoleon Bonaparte
Sub-Lieutenant
Giuseppe Grandi
Strange Lady


== Plot ==
12 May 1796, an inn at Tavazzano. After his victory at the Battle of Lodi, Napoleon eats his meal, works on his plans and talks with the innkeeper Giuseppe Grandi. A lieutenant arrives with bad news. The dispatches he was carrying have been stolen by a youth who tricked him out of them. Napoleon says he will be arrested for dereliction of duty. The lieutenant says he can hear the voice of the youth who tricked him. A woman appears. She says that the dispatches were stolen by her brother. Napoleon is unconvinced. He orders the lieutenant out and tells the woman to hand over the dispatches. A battle of wits ensues between Napoleon and the woman, but she eventually concedes and hands over the documents. However she says he should not read one of the documents. It is a letter claiming that Napoleon's wife Josephine has been having an affair with Paul Barras. If he is known to have read the letter it will cause a duel. Napoleon, concerned about a public scandal, decides to pretend that the dispatches are still missing. He calls the lieutenant back, and tells him to go and find the missing documents or be court martialed. To save the lieutenant from disgrace, the lady leaves and switches to her male disguise. As soon as she reappears the lieutenant recognises the "brother" who robbed him. Pretending to have magical powers, she finds the dispatches in Napoleon's coat. Napoleon says he's been outwitted by an Englishwoman, and makes a series of comments about the English ability to constantly have things both ways ("As the great champion of freedom and national independence, he conquers and annexes half the world, and calls it Colonization"). He gives the woman the letter unopened; she burns it.


== Productions ==
The play was written for Ellen Terry and Richard Mansfield, but was first performed, on 1 July 1897, at the Grand theatre, Croydon with Murray Carson and Florence West in the principal roles.  The play was the first Shaw work performed in the German language when it was given in a translation by Siegfried Trebitsch at the Schauspielhaus in Frankfurt on 20 April 1903. On 28 March 1928, the play had its first radio broadcast with Esme Percy as Napoleon.
The play was revived at The Pentameters Theatre in Hampstead in April 2013, performed in a double bill with The Fascinating Foundling. The cast included Karl Niklas as Napoleon, Seamus Newham as Giuseppe, Lucas Livesey as Lieutenant and Emma West as The Strange Lady. Directed by Michael Friend and produced by Leonie Scott-Matthews.
A version of it aired on Australian television in 1963. Australian TV drama was relatively rare at the time.Playwright Jim Knable adapted the play into "Destiny and the Little Man," which was given a world premiere production as part of "Nightcap Riot: Mombucha" in New York City in January and February 2016. The production was directed by Bruce Levitt of Cornell University.
 A version aired on BBC television in 1939, and is believed to be lost.


== References ==


== External links ==
 The Man of Destiny at Project Gutenberg
The Man of Destiny at the Internet Broadway Database