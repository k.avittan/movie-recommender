7th Heaven is an American family drama television series created and produced by Brenda Hampton that centers on the Camden family and their lives in the fictional town of Glenoak, California. The series debuted on August 26, 1996, on The WB, where it aired for ten seasons. Following the shutdown of The WB and its merger with UPN to form The CW, the series aired on the new network on September 25, 2006, for its eleventh and final season, airing its final episode on May 13, 2007. 7th Heaven was the last series to be produced by Spelling Television (later produced by CBS Paramount Network Television for the eleventh and final season) before it was shut down and became an in-name-only unit of CBS Television Studios.


== Premise ==

The series follows the Reverend Eric Camden—a Protestant minister living in the fictional town of Glenoak, California—as well as Eric's wife Annie and their seven children. Except for Lucy, the children are all named after key biblical figures. Originally, there are five children (making it a family of seven). The twins are born in season three, in the episode "In Praise of Women". Four of the children, Matt, Mary, Lucy, and Simon, at different times, move away from home during the show's run. Simon goes to college, Mary goes to live with her grandparents and Matt marries and pursues his career as a doctor, far away from the family. Despite these three being absent from the Camden home, the house is always full. When Lucy marries, they move into the garage apartment. Their daughter is born while they are there. Later, they move into a home next door. Ruthie leaves for a short while in the final season to go to Scotland. The Camdens offer shelter to various house guests at different points in the show.


== Main cast and characters ==


== Episodes ==


== Production ==
Although originally produced for Fox in 1996, the show aired on the WB. It was produced by Spelling Television and distributed for syndication by CBS Television Distribution. Its producers, including Aaron Spelling, considered it wholesome family viewing, incorporating public service announcements into the show. The final season of 7th Heaven was shown on the inaugural season of The CW. The show wrapped production on the final episode March 8, 2007, about one month before most shows film their last episodes of the season. This was due largely to the fact that after ten years of working together, the actors, producers and crew had gotten production down to a steady pace, slashing costs repeatedly and routinely coming in well under budget. This resulted in 7th Heaven filming episodes in shorter time during the final seasons.


=== 2006 renewal ===
After much deliberation within the now-defunct WB network, it was made public in November 2005 that the tenth season would be the program's final season because of high costs, which were revealed to be due to a poorly negotiated licensing agreement by the WB network a few years earlier. The program's future was hanging in the balance and it was entirely in the hands of the newly established CW network whether to renew it for an eleventh seasonal run. In March 2006, the main cast of characters were approached about the possibility of returning for an eleventh season. After further consideration by the CW network, it was decided three days after the airing of its "series finale", that 7th Heaven would be picked up for an eleventh season, which would air on their network in the Monday-night slot that had helped make it famous. Originally the show was renewed for thirteen episodes, but on September 18, 2006, the renewal was extended to a full twenty-two episodes.
Along with the show's unexpected and last-minute renewal came some changes. The show's already-low budget was moderately trimmed, forcing cuts in the salaries of some cast members and shortened taping schedules (seven days per episode instead of the typical eight). David Gallagher, who played Simon, chose not to return as a regular. Furthermore, Mackenzie Rosman, who played youngest daughter Ruthie, did not appear in the first six episodes. Catherine Hicks missed three episodes in Season 11, as another cost-cutting move. Additionally, George Stults was absent for a few episodes at the beginning of season 11. Also, after airing Monday nights at 8/7c for ten seasons, plus the first two episodes of season 11, the CW unexpectedly moved 7th Heaven to Sunday nights as of October 15, 2006. The Sunday/Monday lineup swap was attributed to mediocre ratings of shows on both nights. While 7th Heaven did improve in numbers over the CW's previous Sunday night programming, it never quite hit its Monday-night momentum again.


== Reception ==


=== Critical reception ===
The Parents Television Council often cited 7th Heaven among the top ten most family-friendly shows. The show was praised for its positive portrayal of a cleric and for promoting honesty, respect for parental authority, and the importance of a strong family and a good education through its storylines. It was proclaimed the best show in 1998-1999 by the Parents Television Council. The council also explained "7th Heaven manages to provide moral solutions to tough issues facing teenagers without seeming preachy or heavy-handed. Additionally, unlike most TV series, 7th Heaven shows the consequences of reckless and irresponsible behavior." It was also noted that "While addressing topics such as premarital sex and peer pressure, these parents [Annie and Eric] are eager to provide wise counsel along with love and understanding."However, other critics feel different about the show, citing 7th Heaven as  "arguably one of the worst long-running shows on television". As reasons are stated for example the heavy-handed moralizing, Christian propaganda, and depiction of a caricature of a real family, that is "so clean it is obscene".Some criticize the predictable plotlines of each episode, that follow always the same pattern: 
"One of the Camden family has a problem and/or secret; some sort of "Three's Company"-esque misunderstanding ensues as a result of that problem and/or secret; a confrontation and/or intervention takes place, usually involving a minisermon by one of the Camden parents; and whoever stands at the center of the drama eventually figures out how to "do the right thing." On top of this, implausible scenarios are seen to be regularly included, such as the daughter Mary's absence from the show for several seasons being scarcely explained with the character  being busy, wayward or in New York.According to one critic, "the sappiness and sanctimony of the characters often made the moral lessons impossible to swallow".  Also, the show is said to show an obsession with premarital sex. In this regard, the parents and the oldest son Matt sometimes depict a sense of ownership of the sexuality of their daughters (respective sisters) Lucy and Mary, by threatening potential romantic interests or negotiating their daughters' romantic rights.


=== U.S. ratings ===
7th Heaven was the most watched TV series ever on the WB. It holds the record for the WB's most watched hour at 12.5 million viewers, on February 8, 1999; 19 of the WB's 20 most watched hours were from 7th Heaven. On May 8, 2006, it was watched by 7.56 million viewers, the highest rating for the WB since January 2005. When the show moved to the CW, ratings dropped. Possible reasons for the decline include an aired "Countdown to Goodbye" ad campaign for the last six months of the 2005–06 season, which promoted it as the final season ever; though the CW announced the series' unexpected renewal, it didn't promote the new season strongly via billboards, bus stops, magazine or on-air promos. Lastly, the network moved 7th Heaven from its long-established Monday night slot to Sunday nights, causing ratings to drop further. The series had a season average of just 3.3 million on the new network, losing 36% of the previous year's audience. It was the third most watched scripted show on the CW. Overall, it was the seventh most watched show.


=== Awards and nominations ===
Emmy Awards1997: Outstanding Art Direction for a Series (Patricia Van Ryker and Mary Ann Good) – Nominated
ASCAP Film and Television Music Awards2000: Top TV Series (Dan Foliart) – Won
2001: Top TV Series (Dan Foliart) – Won
Family Television Awards1999: Best Drama – Won
2002: Best Drama – Won
Kids' Choice Awards1999: Favorite Television Show – Nominated
2000: Favorite Animal Star (Happy the dog) – Nominated
2001: Favorite Television Show – Nominated
2002: Favorite Television Show – Nominated
2003: Favorite Television Show – Nominated
TV Guide Awards1999: Best Show You're not Watching – Won
2000: Favorite TV Pet (Happy the dog) – Nominated
Teen Choice Awards1999: TV Choice Actor (Barry Watson) – Nominated
1999: TV Choice Drama – Nominated
2000: TV Choice Drama – Nominated
2001: TV Choice Actor (Barry Watson) – Nominated
2001: TV Choice Drama – Nominated
2002: TV Choice Drama/Action Adventure – Won
2002: TV Choice Actor in Drama (Barry Watson) – Won
2002: TV Choice Actress in Drama (Jessica Biel) – Nominated
2003: TV Choice Drama/Action Adventure – Won
2003: TV Choice Actor in Drama/Action Adventure (David Gallagher) – Won
2003: TV Choice Breakout Star – Male (George Stults) – Won
2003: TV Choice Actress in Drama/Action Adventure (Jessica Biel) – Nominated
2003: TV Choice Breakout Star – Female (Ashlee Simpson) – Nominated
2004: TV Choice Breakout Star – Male (Tyler Hoechlin) – Nominated
2004: TV Choice Actor in Drama/Action Adventure (David Gallagher) – Nominated
2004: TV Choice Drama/Action Adventure – Nominated
2005: TV Choice Actor in Drama/Action Adventure (Tyler Hoechlin) – Nominated
2005: TV Choice Actress in Drama/Action Adventure (Beverley Mitchell) – Nominated
2005: TV Choice Parental Units (Stephen Collins and Catherine Hicks) – Nominated
2005: TV Choice Drama/Action Adventure – Nominated
2006: TV Choice Breakout Star – Female (Haylie Duff) – Nominated
2006: TV Choice Parental Units (Stephen Collins and Catherine Hicks) – Nominated
Young Artist Awards1997: Best Family TV Drama Series – Won
1997: Best Performance in a Drama Series – Young Actress (Beverley Mitchell) – Won
1997: Best Performance in a Drama Series – Young Actor (David Gallagher) – Nominated
1997: Best Performance in a TV Comedy/Drama – Supporting Young Actress Age Ten or Under (Mackenzie Rosman) – Nominated
1998: Best Family TV Drama Series – Won (tied with Promised Land)
1998: Best Performance in a TV Drama Series – Leading Young Actress (Beverley Mitchell) – Won (tied with Sarah Schaub)
1998: Best Performance in a TV Drama Series – Guest Starring Young Actor (Bobby Brewer) – Nominated
1998: Best Performance in a TV Drama Series – Guest Starring Young Actress (Danielle Keaton) – Nominated
1998: Best Performance in a TV Drama Series – Guest Starring Young Actress (Molly Orr) – Nominated
1998: Best Performance in a TV Drama Series – Leading Young Actor (David Gallagher) – Nominated
1998: Best Performance in a TV Drama Series – Leading Young Actress (Jessica Biel) – Nominated
1998: Best Performance in a TV Drama Series – Supporting Young Actress (Mackenzie Rosman) – Nominated
1999: Best Family TV Drama Series – Nominated
1999: Best Performance in a TV Drama Series – Guest Starring Young Actor (Craig Hauer) – Nominated
1999: Best Performance in a TV Series – Young Ensemble (Beverley Mitchell, Barry Watson, Jessica Biel, David Gallagher, Mackenzie Rosman) – Nominated
2000: Best Performance in a TV Drama Series – Guest Starring Young Actress (Kaitlin Cullum) – Won
2000: Best Performance in a TV Drama Series – Leading Young Actress (Beverley Mitchell) – Won
2000: Best Family TV Series – Drama – Nominated
2001: Best Performance in a TV Drama Series – Guest Starring Young Actress (Brooke Anne Smith) – Won
2001: Best Family TV Drama Series – Nominated
2001: Best Performance in a TV Drama Series – Guest Starring Young Actress (Jamie Lauren) – Nominated
2002: Best Family TV Drama Series – Nominated
2002: Best Performance in a TV Drama Series – Guest Starring Young Actress (Ashley Edner) – Nominated
2002: Best Performance in a TV Drama Series – Leading Young Actor (David Gallagher) – Nominated
2002: Best Performance in a TV Drama Series – Supporting Young Actress (Mackenzie Rosman) – Nominated
2004: Best Performance in a TV Series (Comedy or Drama) – Supporting Young Actress (Mackenzie Rosman) – Won
2005: Best Family Television Series (Drama) – Nominated
2005: Best Performance in a TV Series (Comedy or Drama) – Leading Young Actor (Tyler Hoechlin) – Nominated
2006: Best Performance in a TV Series (Comedy or Drama) – Young Actor Age Ten or Younger (Drake Johnston) – Nominated
2007: Best Family Television Series (Drama) – Nominated
2007: Best Performance in a TV Series (Comedy or Drama) – Supporting Young Actress (Mackenzie Rosman) – Nominated
2007: Best Performance in a TV Series (Comedy or Drama) – Young Actor Age Ten or Younger (Nikolas Brino and Lorenzo Brino) – Nominated
2008: Best Performance in a TV Series – Young Actor Ten or Under (Lorenzo Brino) – Nominated
2008: Best Performance in a TV Series – Young Actor Ten or Under (Nikolas Brino) – Nominated
Young Star Awards
1997: Best Performance by a Young Actor in a Drama TV Series (David Gallagher) – Nominated
1998: Best Performance by a Young Actress in a Drama TV Series (Beverley Mitchell) – Nominated
1998: Best Performance by a Young Actress in a Drama TV Series (Jessica Biel) – Nominated
1998: Best Performance by a Young Actor in a Drama TV Series (David Gallagher) – Won
1999: Best Performance by a Young Actor in a Drama TV Series (David Gallagher) – Nominated
2000: Best Performance by a Young Actor in a Drama TV Series (David Gallagher) – Nominated
2000: Best Young Ensemble Cast – Television (David Gallagher, Jessica Biel, Beverley Mitchell, Mackenzie Rosman) – Nominated


== Availability ==


=== Syndication ===
CBS Television Distribution handles the domestic and international distribution of the series. Season one episodes were retitled 7th Heaven Beginnings. Although the series did not receive a rating other than TV-G throughout its 11-season run, reruns on some cable/satellite channels have been given either a TV-PG or TV-14 rating (depending on the subject matter).
In the United States, the show began airing reruns in off-network syndication on September 25, 2000, but ceased to air in syndication in September 2002, while the series was still in first-run broadcast on The WB and later on The CW. The show then aired on the ABC Family channel from the fall of 2002 until 2008. Then, it was announced on April 1, 2010, that ABC Family had re-obtained the rights to the series, and would begin airing it at 11 a.m. (ET/PT) on weekdays beginning April 12, 2010. However, after one week, ABC Family abruptly pulled the show and replaced it with a third daily airing of Gilmore Girls.
It started airing on "superstation" WGN America on September 8, 2008, though it had previously aired on from 2000 to 2008 during its initial off-network syndication run. Incidentally, the series aired in first-run form on WGN from the show's 1996 debut on The WB until 1999, when WGN ceased to carry WB network programming on its national feed (7th Heaven, along with Sister, Sister, The Parent 'Hood and The Wayans Bros. are the only WB series to air in both first-run broadcast and off-network syndication on WGN America). Since September 2010, 7th Heaven no longer airs on WGN America.
The series also began airing on Hallmark Channel around the same time as when WGN America began to carry reruns of the series again. Hallmark Channel airings of the series, however, truncated the opening credit sequence removing the majority of the theme song except for the first stanza and the last few seconds of the theme. Both channels removed it in 2010.
As of 2010 Crossroads Television System aired the show in Canada. In August 2011, the show was dropped from the lineup.
It can now be seen on Joytv, as of 2012.
As of 2012, GMC (now known as UP) is the first network to air 7th Heaven in the United States since 2010 and began airing the series with a marathon on July 7, 2012. Due to allegations of child molestation against Stephen Collins, the network pulled the series from its schedule as of the afternoon of October 7, 2014. 7th Heaven briefly returned to UP in December 2014; however, it was quickly removed from the schedule. UP CEO Charley Humbard stated, "We brought the show back because many viewers expressed they could separate allegations against one actor from the fictional series itself. As it turns out, they cannot." However, in the summer of 2015, UP brought back the series, where it currently airs from 12PM to 3PM (EST). Previously, it aired on GetTV and Hallmark Drama.
In the United States, all eleven seasons of 7th Heaven are available to stream on Hulu and CBS All Access.In the United Kingdom, it aired on Sky One on a weekly basis as a part of its primetime slots at 8pm.
In Australia, 7th Heaven was originally broadcast on Network Ten. Reruns of the series have been aired weekdays on satellite channel FOX8 and Network Ten's digital channel Eleven.
It is on RTE (Radio Telefis Eireann) in Ireland.


=== Home media ===
CBS DVD (distributed by Paramount Home Entertainment) has released 7th Heaven on DVD. They have released all 11 seasons in Region 1. In region 2, seasons 1-7 have been released while in region 4 the first 6 seasons have been released on DVD.
On August 22, 2017, it was announced that the complete series would be released on DVD for November 14.


== References ==


== External links ==
Official website 
7th Heaven at Yahoo! TV
7th Heaven on IMDb
7th Heaven at TV.com