"Mary, Mary, Quite Contrary" is an English nursery rhyme. The rhyme has been seen as having religious and historical significance, but its origins and meaning are disputed. It has a Roud Folk Song Index number of 19626.


== Lyrics ==

The most common modern version is:

The oldest known version was first published in Tommy Thumb's Pretty Song Book (1744) with the lyrics that are shown here:

Several printed versions of the 18th century have the lyrics:

The last line has the most variation including:

Cowslips all in arow [sic].
and

With lady bells all in a row.


== Explanations ==
Like many nursery rhymes, it has acquired various historical explanations. One theory is that it is religious allegory of Catholicism, with Mary being Mary, the mother of Jesus, bells representing the sanctus bells, the cockleshells the badges of the pilgrims to the shrine of Saint James in Spain (Santiago de Compostela) and pretty maids are nuns, but even within this strand of thought there are differences of opinion as to whether it is lament for the reinstatement of Catholicism or for its persecution. Another theory sees the rhyme as connected to Mary, Queen of Scots (1542–1587), with "how does your garden grow" referring to her reign over her realm, "silver bells" referring to (Catholic) cathedral bells, "cockle shells" insinuating that her husband was not faithful to her, and "pretty maids all in a row" referring to her ladies-in-waiting – "The four Maries".Mary has also been identified with Mary I of England (1516–1558) with "How does your garden grow?" said to refer to her lack of heirs, or to the common idea that England had become a Catholic vassal or "branch" of Spain and the Habsburgs. It is also said to be a punning reference to her chief minister, Stephen Gardiner.  "Quite contrary" is said to be a reference to her unsuccessful attempt to reverse ecclesiastical changes effected by her father Henry VIII and her brother Edward VI. The "pretty maids all in a row" is speculated to be a reference to miscarriages or her execution of Lady Jane Grey. "Rows and rows" is said to refer to her executions of Protestants.No proof has been found that the rhyme was known before the 18th century, while Mary I of England (Mary Tudor) and Mary, Queen of Scots (Mary Stuart), were contemporaries in the 16th century.


== References ==