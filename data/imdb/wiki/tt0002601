This is a list of characters from the NBC and ABC sitcom Diff'rent Strokes.


== Main characters table ==


== Phillip Drummond ==
Phillip Drummond was portrayed by Conrad Bain. He is a friendly, wealthy white widower, who runs Trans-Allied, Incorporated. He was born December 3, 1931 in Manhattan, New York. (This made him Conrad Bain's junior by eight years.)
Phillip has a daughter, Kimberly, and two adopted African American sons, Willis and his younger brother Arnold Jackson. He also has an eccentric elder sister named Sophia (played by Dody Goodman). Arnold and Willis's mother, Lucy Jackson (portrayed by Todd Bridges' real-life mother), worked as a housekeeper for the Drummonds years ago; her death-bed wish was that Phillip would take care of her two sons. In the series pilot, Phillip welcomes Arnold and Willis into his home.
Phillip had dated several women, and would later get remarried to Maggie McKinney, a television aerobics instructor (played by Dixie Carter from 1983 to 1985 and Mary Ann Mobley from 1985 to 1986). Maggie subsequently introduced Sam McKinney (Danny Cooksey), her son from a previous marriage, to the family.
Phillip Drummond is the only character to appear in every episode of the series.
Phillip, along with Arnold Jackson, made a cameo in the Fresh Prince of Bel-Air series finale "I, Done," as a potential buyer of the Banks' mansion.
On The Facts of Life, Philip appears in Rough Housing.  


== Kimberly Drummond ==
Kimberly Drummond was portrayed by Dana Plato. She was the only biological child and daughter of wealthy widower, Phillip Drummond (Conrad Bain). She was born on Park Avenue in New York City on October 22, 1964. Kimberly was shown as a caring, loving big sister to both Willis and Arnold, but as a wealthy, coming of age teenager, she suffered from various problems. Several such instances took place in season 6, one of which involved a two-part "very special" episode on the dangers of hitchhiking, where she was nearly a victim of sexual assault by a deranged man named Bill (Woody Eney). In another episode, no one in the family knew that she was suffering from bulimia, though she later admitted to having a problem and agreed to seek help.
Kimberly wasn't in the opening credits in the 7th season nor the 8th season, however, in the 8th season, there were clips of her and she is shown in the last frame of the opening credits for the 8th season.
On The Facts of Life, Kimberly appears in Rough Housing.


== Arnold Jackson ==
Arnold Jackson was portrayed by Gary Coleman. He was the younger brother of Willis Jackson (Todd Bridges), and was born in Harlem, New York City on July 19, 1970. Arnold is a "precocious moppet," who was practically known for his catch phrase, "Whatchoo talkin' 'bout Willis?", which became a part of popular culture and in 2006 was included in TV Land's "The 100 Greatest TV Quotes and Catch Phrases" special.Arnold's father died in 1975, and his mother died in 1977. His mother worked as a housekeeper for a wealthy white widower, named Philip Drummond (Conrad Bain). Before her death, his mother expressed her wish for her two boys to be cared for by Mr. Drummond. He agreed, and in 1979, he officially adopted Willis and Arnold.Arnold is the main character in the series. In many episodes, he is shown as being a selfish younger brother, or coming up with or being suckered into some scheme to keep out of trouble or obtain his desire of the episode. When the boys first move in with Mr. Drummond, Willis wants to move back to Harlem, while Arnold is satisfied with their new surroundings. Willis eventually changes his mind, and they decide to stay with "Mr. D.," as the boys initially refer to him.In another episode, Arnold has to fight a school bully named "The Gooch," so that he will not pick on him anymore. However, Mr. Drummond does not want Arnold fighting the bully, and ultimately decides that Arnold must make peace with "The Gooch." Arnold, however, listens to his brother, Willis, who tells him to fight back. This ended with Arnold getting a black eye, and both boys getting in trouble.
Arnold's best friends is Dudley Ramsey (played by TV actor Shavar Ross), who, like Arnold, was adopted. Dudley appears in many episodes, and both are involved in various schemes throughout the series. Steven Mond played Robbie Jayson, Arnold's other best friend, who once pressured him to try drugs. In one episode, Arnold has to have an appendectomy, but is too scared to see the doctor.
Arnold, along with Phillip Drummond, made a cameo in the Fresh Prince of Bel-Air series finale, as potential buyers of the Banks' mansion
On The Facts of Life, Arnold appears in Rough Housing and in The New Girl: Part 1.


== Willis Jackson ==
Willis Jackson was portrayed by Todd Bridges. He was the older brother of Arnold. He was born in Harlem on April 27, 1965. The boys' late mother was a housekeeper named Lucy, for a wealthy White man, named Phillip Drummond, and her deathbed wish was that he would take care of her two kids, and Philip Drummond officially adopted them in 1979. Willis's catch phrase is "Say what?"
Willis was portrayed alternately as rebellious and responsible. In one episode, Willis joins a gang named "The Tarantulas."
Willis also had a girlfriend, named Charlene DuPrey, portrayed by Janet Jackson of both the television shows Good Times and Fame. Jackson appeared from the show's third season (1980–1981) until 1984, through the show's sixth season.
Bridges' role as Willis Jackson started to fade, because of casting changes in the 1984–1985 season, when Danny Cooksey was added as Sam McKinney, his and Arnold's new younger stepbrother. Bridges did not appear as often in the show's final season, though his name remained in the opening credits.
On The Facts of Life, Willis appears in Rough Housing and in Bought and Sold.


== Edna Garrett ==

Edna Garrett was portrayed by Charlotte Rae. She was Mr. Drummond's housekeeper from 1978 to 1979. She left the show midway through the second season to take a job as housemother and dietitian at Eastland School, the school Kimberly attends in Peekskill, New York. She returned as a guest star in the wedding episode.


== References ==