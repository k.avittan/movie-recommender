You Should Have Left  is a 2020 American psychological horror film written and directed by David Koepp, based on the 2017 book of the same name by Daniel Kehlmann. It stars Kevin Bacon and Amanda Seyfried. Jason Blum served as a producer through his Blumhouse Productions banner.
Originally intended to be theatrically released, the film was released digitally via Premium VOD on June 18, 2020, by Universal Pictures. 


== Plot ==
Theo Conroy (Kevin Bacon) is a retired banker married to a younger woman Susanna (Amanda Seyfried), an actress, and they share a daughter named Ella. One afternoon when Theo attempts to visit Susanna on set, he is denied entry. While waiting to be granted authorization, he hears Susanna doing a love-making scene and is visibly annoyed. Susanna later apologies to Theo, attributing his obstruction to a misunderstanding, and she tells him she has "put him on the [approved] list". Theo informs Susanna that the set security guard recognised him; Susanna teases Theo, telling him that "they think you're dangerous."
Soon after, they book a vacation in Wales, but there is something strange about the house—time passed unusually fast and they both experience bad nightmares. They also discover that neither one of them made the booking, each thinking the other did it. 
One night, Ella sees the shadow of a man on the wall. The next morning, while Theo writes in his meditation journal, she asks Susanna why people dislike Theo. Susanna reluctantly explains that Theo's first wife drowned in the bathtub and people suspected that he killed her, though he was acquitted at trial. While in town for supplies, the shopkeeper asks if he's met Stetler, who Theo presumes is the home owner. He mysteriously gives Theo a drafting triangle and tells him to measure the right angles, leaving Theo confused.
Theo wrestles with feelings of jealousy and mistrust toward Susanna. One evening while she is taking a bath, Theo checks the messages on her phone and laptop. Theo has a dream that night and sees someone has written in his journal: "You should leave. Go now." The following morning as he watches Susanna and Ella playing outside, he texts her. At the same time he sees Susanna look at her phone, he hears a text vibration on the kitchen counter and finds an identical phone with his messages on the screen. Realizing that she has a secret phone, he suspects she has been cheating on him. He confronts Susanna and she admits to an affair with another actor. Theo asks her to leave for the night and she goes into town to stay at an inn. 
He returns to his journal to see that someone has now written “You should have left. Now it’s too late.” Upon discovering an anomaly in the angle between the wall and floor, they measure the kitchen and find that it is larger inside than outside. Ella and Theo get separated; the two of them appear to be experiencing separate visions in the dream world. Once reunited, Theo calls Susanna, wanting her to come back and take him and Ella away from the house, but her phone is turned off. He then calls the shopkeeper, inquiring whether he knows of any cab services in the area. The shopkeeper replies that there are none and speaks of the house cryptically, saying that the devil collects souls from there.
Desperate to escape the house, Theo and Ella decide to go to town by foot, but see a shadowy figure observing them from inside as they walk away from it. After some time, they find they have circled back to the house. Seeing no other option, they stay there for the night, but Theo enters the dream world again and sees his and Susanna's past selves as they first arrived at the house. He then meets Stetler, who has taken Ella captive. He takes Theo's form to taunt him and says he will return Ella on the condition that Theo does "what he must." Ella is returned, relieving Theo. 
The next day, Susanna returns to the house and Theo gives her Ella. He finally confesses to the true circumstances surrounding the death of his first wife: he didn't directly kill her, but didn't help when she was drowning; he simply watched her die because he had been miserable with her for so long. He accepts that he belongs in the house. Theo's soul is then seen trapped inside the house, revealing he had been the figure watching himself and Ella leave the night before, having tried to warn his past self by writing the messages in his journal. The shopkeeper's voice says that some people don't leave the house, and "the place finds them."


== Cast ==
Kevin Bacon as Theo Conroy
Amanda Seyfried as Susanna
Avery Essex as Ella
Geoff Bell as Angus
Lowri-Ann Richards as Tiny Woman


== Production ==
In March 2018, it was announced Kevin Bacon would star in the film, with David Koepp directing and writing the film, based upon the novel of the same name by Daniel Kehlmann; Koepp and Bacon had previously collaborated on the 1999 film Stir of Echoes. Jason Blum will serve as a producer under his Blumhouse Productions banner. In June 2018, Amanda Seyfried joined the cast of the film.Filming took place at various locations in Wales, including at the Life House in Llanbister, Radnorshire.


== Music ==
The film's score was composed by Geoff Zanelli. Back Lot Music released the soundtrack on June 18, 2020, coinciding with the film's streaming release.


== Release ==
Originally scheduled for a theatrical release, Universal Pictures decided to release the film digitally in the United States and Canada through Premium VOD on June 18, 2020 due to the movie theater closures since mid March because of the COVID-19 pandemic restrictions.


== Reception ==


=== VOD sales ===
In its first weekend, the film was the second-most rented on FandangoNow and the iTunes Store. In its second weekend, it fell to number three and number six, respectively. In its third weekend the film placed sixth on FandangoNow, but ranked second on Spectrum's weekly chart, then the following weekend placed fifth on both services. Over the weekend of July 31, Universal lowered the price of the film from $19.99 to $5.99, and You Should Have Left finished second on FandangoNow and fifth at Apple TV.


=== Critical response ===
On Rotten Tomatoes, the film holds an approval rating of 40% based on 109 reviews, with an average rating of 4.90/10. The site's critics consensus reads: "You Should Have Left hints at a genuinely creepy experience, but never quite manages to distill its intriguing ingredients into a consistently satisfying whole." At Metacritic, the film has a weighted average score of 46 out of 100, based on 27 critics, indicating "mixed or average reviews".Kate Erbland of IndieWire gave the film a "B–" and said that "when You Should Have Left is at its best, it's unconcerned with mapping out such easy clarifications and leans into the raw madness of corrosive guilt and a house made to punish people who dare come inside its walls." Critic Terry Mesnard wrote that the larger-than-it-seems house in the film felt "ripped" from the 2000 novel House of Leaves by Mark Z. Danielewski.


== References ==


== External links ==
Official website
You Should Have Left on IMDb