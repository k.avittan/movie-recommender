A cramp is a sudden, involuntary muscle contraction or overshortening; while generally temporary and non-damaging, they can cause significant pain and a paralysis-like immobility of the affected muscle.  Muscle cramps are common and are often associated with pregnancy, physical exercise or overexertion, age (common in older adults), or may be a sign of a motor neuron disorders. Cramps may occur in a skeletal muscle or smooth muscle.  Skeletal muscle cramps may be caused by muscle fatigue or a lack of electrolytes such as sodium (a condition called hyponatremia), potassium (called hypokalemia), or magnesium (called hypomagnesemia). Some skeletal muscle cramps do not have a known cause. Cramps of smooth muscle may be due to menstruation or gastroenteritis. Motor neuron disorders (e.g., amyotrophic lateral sclerosis), metabolic disorders (e.g., liver failure), some medications (e.g., diuretics and inhaled beta‐agonists), and haemodialysis may also cause muscle cramps.A cramp usually starts suddenly and it also usually goes away on its own over a period of several seconds, minutes, or hours. Restless leg syndrome and rest cramps are not considered the same as muscle cramps.


== Pathophysiology ==
 

Muscle contraction begins with the brain setting off action potentials, which are waves in the electrical charges that extend along neurons. The waves travel to a group of cells in a muscle, letting calcium ions out from the cells' sarcoplasmic reticula (SR), which are storage areas for calcium. The released calcium lets myofibrils contract under the power of energy-carrying adenosine triphosphate (ATP) molecules. Meanwhile, the calcium is quickly pumped back into the SR by fast calcium pumps. Each muscle cell contracts fully; stronger contraction of the whole muscle requires more action potentials on more groups of cells in the muscle. When the action potentials stop, the calcium stops flowing from the SR and the muscle relaxes. The fast calcium pumps are powered by the sodium-potassium gradient. The sodium-potassium gradient is maintained by the sodium-potassium pump and their associated ion channels. A lack of potassium or sodium would prevent the sodium-potassium gradient from being strong enough to power the calcium pumps; the calcium ions would remain in the myofibrils, forcing the muscle to stay contracted and causing a cramp. The cramp eventually eases as slow calcium pumps, powered by ATP instead of the sodium gradient, push the calcium back into storage.Cramps can occur when muscles are unable to relax properly due to myosin proteins not fully detaching from actin filaments. In skeletal muscle, ATP levels must be large enough to bind to the myosin heads for them to attach or detach from the actin and allow contraction or relaxation; the absence of enough levels of ATP means that the myosin heads remains attached to actin. The muscle must be allowed to recover (resynthesize ATP), before the myosin proteins can detach and allow the muscle to relax. Skeletal muscles work as antagonistic pairs. Contracting one skeletal muscle requires the relaxation of the opposing muscle in the pair.


== Differential diagnosis ==
Causes of cramping include hyperflexion, hypoxia, exposure to large changes in temperature, dehydration, or low blood salt. Muscle cramps can also be a symptom or complication of pregnancy; kidney disease; thyroid disease; hypokalemia, hypomagnesemia, or hypocalcaemia (as conditions); restless legs syndrome; varicose veins; and multiple sclerosis.As early as 1965, researchers observed that leg cramps and restless legs syndrome can result from excess insulin, sometimes called hyperinsulinemia.


=== Skeletal muscle cramps ===

Under normal circumstances, skeletal muscles can be voluntarily controlled. Skeletal muscles that cramp the most often are the calves, thighs, and arches of the foot, and are sometimes called a "Charley horse" or a "corky".  Such cramping is associated with strenuous physical activity and can be intensely painful; however, they can even occur while inactive and relaxed. Around 40% of people who experience skeletal cramps are likely to endure extreme muscle pain, and may be unable to use the entire limb that contains the "locked-up" muscle group. It may take up to a week for the muscle to return to a pain-free state, depending on the person's fitness level, age, and several other factors.


==== Nocturnal leg cramps ====

Nocturnal leg cramps are involuntary muscle contractions that occur in the calves, soles of the feet, or other muscles in the body during the night or (less commonly) while resting. The duration of nocturnal leg cramps is variable, with cramps lasting anywhere from a few seconds to several minutes. Muscle soreness may remain after the cramp itself ends. These cramps are more common in older people. They happen quite frequently in teenagers and in some people while exercising at night. Besides being painful, a nocturnal leg cramp can cause much distress and anxiety. The precise cause of these cramps is unclear. Potential contributing factors include dehydration, low levels of certain minerals (magnesium, potassium, calcium, and sodium, although the evidence has been mixed), and reduced blood flow through muscles attendant in prolonged sitting or lying down. Nocturnal leg cramps (almost exclusively calf cramps) are considered "normal" during the late stages of pregnancy. They can, however, vary in intensity from mild to extremely painful.
A lactic acid buildup around muscles can trigger cramps; however, they happen during anaerobic respiration when a person is exercising or engaging in an activity where the heartbeat rises.  Medical conditions associated with leg cramps are cardiovascular disease, hemodialysis, cirrhosis, pregnancy, and lumbar canal stenosis. Differential diagnoses include restless legs syndrome, claudication, myositis, and peripheral neuropathy. All of them can be differentiated through careful history and physical examination.Gentle stretching and massage, putting some pressure on the affected leg by walking or standing, or taking a warm bath or shower may help to end the cramp. If the cramp is in the calf muscle, dorsiflexing the foot will stretch the muscle and provide almost immediate relief. There is limited evidence supporting the use of magnesium, calcium channel blockers, carisoprodol, and vitamin B12.Quinine is no longer recommended for treatment of nocturnal leg cramps due to potential fatal hypersensitivity reactions and thrombocytopenia. Arrhythmias, cinchonism, and hemolytic uremic syndrome can also occur at higher dosages.


=== Smooth muscle cramps ===
Smooth muscle contractions may be symptomatic of endometriosis or other health problems. Menstrual cramps may also occur during a menstrual cycle.


=== Cramps caused by treatments ===
Various medications may cause nocturnal leg cramps:
Diuretics, especially potassium sparing
Intravenous (IV) iron sucrose
Conjugated estrogens
Teriparatide
Naproxen
Raloxifene
Long acting adrenergic beta-agonists (LABAs)
Hydroxymethylglutaryl-coenzyme A reductase inhibitors (HMG-CoA inhibitors or statins)Statins may sometimes cause myalgia and cramps among other possible side effects. Raloxifene (Evista) is a medication associated with a high incidence of leg cramps.  Additional factors, which increase the probability for these side effects, are physical exercise, age, female gender, history of cramps, and hypothyroidism. Up to 80% of athletes using statins suffer significant adverse muscular effects, including cramps; the rate appears to be approximately 10–25% in a typical statin-using population. In some cases, adverse effects disappear after switching to a different statin; however, they should not be ignored if they persist, as they can, in rare cases, develop into more serious problems. Coenzyme Q10 supplementation can be helpful to avoid some statin-related adverse effects, but currently there is not enough evidence to prove the effectiveness in avoiding myopathy or myalgia.


== Treatment ==
Stretching, massage, and drinking plenty of liquids may be helpful in treating simple muscle cramps.


=== Medication ===
The antimalarial drug quinine is a traditional treatment, that may be slightly effective for reducing the number of cramps, the intensity of cramps, and the number of days a person experiences cramps. Quinine has not been shown to reduce the duration (length) of a muscle cramp. Quinine treatment may lead to haematologic and cardiac toxicity. Due to its low effectiveness and negative side effects, its use as a medication for treating muscle cramps is not recommended by the FDA.Magnesium is commonly used to treat muscle cramps. Moderate quality evidence indicates that magnesium is not effective for treating or preventing cramps in older adults. It is not known if magnesium helps cramps due to pregnancy, liver cirrhosis, other medical conditions, or exercising. Oral magnesium treatment does not appear to have significant major side effects, however, it may be associated with diarrhea and nausea in 11-37% of people who use this medicine.With exertional heat cramps due to electrolyte abnormalities (primarily potassium loss and not calcium, magnesium, and sodium), appropriate fluids and sufficient potassium improves symptoms. Vitamin B complex, naftidrofuryl, lidocaine, and calcium channel blockers may be effective for muscle cramps.


== Prevention ==
Adequate conditioning, stretching, mental preparation, hydration, and electrolyte balance are likely helpful in preventing muscle cramps.


== References ==


== External links ==
Muscle Cramps (of Skeletal Muscles)