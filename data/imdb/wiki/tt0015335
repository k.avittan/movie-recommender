"Sinners in the Hands of an  Angry God" is a sermon written by British Colonial Christian theologian Jonathan Edwards, preached to his own congregation in Northampton, Massachusetts, to profound effect, and again on July 8, 1741 in Enfield, Connecticut.  The preaching of this sermon was the catalyst for the First Great Awakening. Like Edwards' other works, it combines vivid imagery of Hell with observations of the world and citations of the scripture. It is Edwards' most famous written work, is a fitting representation of his preaching style, and is widely studied by Christians and historians, providing a glimpse into the theology of the First Great Awakening of c. 1730–1755.
This was a highly influential sermon of the Great Awakening, emphasizing God’s wrath upon unbelievers after death to very real, horrific, and fiery Hell.  The underlying point is that God has given humans a chance to confess their sins. It is the mere will of God, according to Edwards, that keeps wicked men from being overtaken by the devil and his demons and cast into the furnace of hell - “like greedy hungry lions, that see their prey, and expect to have it, but are for the present kept back [by God’s hand].” Mankind’s own attempts to avoid falling into the “bottomless gulf” due to the overwhelming “weight and pressure towards hell” are insufficient as “a spider's web would have to stop a falling rock“. This act of grace from God has given humans a chance to believe and trust in Christ. Edwards provides much varied and vivid imagery to illustrate this main theme throughout.


== Doctrine ==

"There is nothing that keeps wicked men at any one moment out of hell, but the mere pleasure of God."
Most of the sermon's text consists of ten "considerations":

God may cast wicked men into hell at any given moment.
The wicked deserve to be cast into hell. Divine justice does not prevent God from destroying the Wicked at any moment.
The wicked, at this moment, suffer under God's condemnation to Hell.
The wicked, on earth—at this very moment—suffer a sample of the torments of Hell. The wicked must not think, simply because they are not physically in Hell, that God (in Whose hand the wicked now reside) is not—at this very moment—as angry with them as He is with those miserable creatures He is now tormenting in hell, and who—at this very moment—do feel and bear the fierceness of His wrath.
At any moment God shall permit him, Satan stands ready to fall upon the wicked and seize them as his own.
If it were not for God's restraints, there are, in the souls of wicked men, hellish principles reigning which, presently, would kindle and flame out into hellfire.
Simply because there are not visible means of death before them at any given moment, the wicked should not feel secure.
Simply because it is natural to care for oneself or to think that others may care for them, men should not think themselves safe from God's wrath.
All that wicked men may do to save themselves from Hell's pains shall afford them nothing if they continue to reject Christ.
God has never promised to save us from Hell, except for those contained in Christ through the covenant of Grace.


== Purpose ==
One church in Enfield, Connecticut, had been largely unaffected during the First Great Awakening of New England. Edwards was invited by the pastor of the church to preach to them. Edwards's aim was to teach his listeners about the horrors of hell, the dangers of sin, and the terrors of being lost. Edwards described the position of those who do not follow Christ's urgent call to receive forgiveness. Edwards scholar John E. Smith notes that despite the apparent pessimism of the notion of an angry God, that pessimism is "overcome by the comforting hope of salvation through a triumphant, loving savior. Whenever Edwards preached terror, it was part of a larger campaign to turn sinners from their disastrous path and to the rightful object of their affections, Jesus Christ."


== Application ==
In the final section of "Sinners in the Hands of an Angry God," Edwards shows that his theological argument holds throughout scripture and biblical history. He invokes stories and examples throughout the whole Bible. Edwards ends the sermon with one final appeal: "Therefore let everyone that is out of Christ, now awake and fly from the wrath to come." According to Edwards, only by returning to Christ can one escape the stark fate he outlines.


== Effect and legacy ==
Edwards was interrupted many times during the sermon by people moaning and crying out, "What shall I do to be saved?". Although the sermon has received criticism, Edwards' words have endured and are still read to this day. Edwards' sermon continues to be the leading example of a First Great Awakening sermon and is still used in religious and academic studies.Since the 1950s, a number of critical perspectives were used to analyse the sermon. The first comprehensive academic analysis of "Sinners in the Hands of an Angry God" was published by Edwin Cady in 1949, who comments on the imagery of the sermon and distinguishes between the "cliché" and "fresh" figurative images, stressing how the former related to the colonial life. Lee Stuart questions that the message of the sermon was solely negative and attributes its success to the final passages in which the sinners are actually "comforted". Rosemary Hearn argues that it is the logical structure of the sermon that constitutes its most important persuasive element. Lemay looks into the changes in the syntactic categories, like grammatical tenses, in the text of the sermon. Lukasik stresses how in the sermon Edwards appropriates Newtonian physics, especially the image of the gravitational pull that would relentlessly bring the sinners down. Gallagher focuses on the "beat" of the sermon, and on how the consecutive structural elements of the sermon serve different persuasive aims. Choiński suggests that the rhetorical success of the sermon consists in the use of the "deictic shift" that transported the hearers mentally into the figurative images of hell.Ironically,  Jonathan Edwards wrote and spoke a great deal on heaven and angels writes John Gerstner in "Jonathan Edwards on Heaven and Hell,1998"and those theme are less remembered, namely  "Heaven is a World of Love".


== See also ==
Calvinism
Religious Affections
A Faithful Narrative
Freedom of the Will
American philosophy
Puritans
Redemption
The Justice of God in the Damnation of Sinners
Great Awakening, generally, the term used for three or four distinct periods of religious revival in American Christian history


== Notes ==


== References ==
Choiński, Michał (25 April 2016), Rhetoric of the Revival: The Language of the Great Awakening, Göttingen: Vandenhoeck & Ruprecht, ISBN 978-3-525-56023-5, retrieved 2016-07-04
Conforti, Joseph (1995), Jonathan Edwards, Religious Tradition, & American Culture, Chapel Hill: University of North Carolina Press, ISBN 978-0-8078-4535-6, retrieved 2013-01-04
Crocco, Stephen (20 November 2006), "Edwards's Intellectual Legacy",  in Stein, Stephen (ed.), The Cambridge Companion to Jonathan Edwards, New York: Cambridge University Press, pp. 300–324, ISBN 978-0-521-61805-2, retrieved 2013-01-04
Hart, Darryl; Lucas, Sean; Nichols, Stephen (1 August 2003), The Legacy of Jonathan Edwards, Grand Rapids: Baker Academic, ISBN 978-0-8010-2622-5, retrieved 2013-01-04
Kimnach, Wilson; Maskell, Caleb; Minkema, Kenneth (23 March 2010), Jonathan Edwards's Sinners in the Hands of an Angry God, New Haven: Yale University Press, ISBN 978-0-300-14038-5, retrieved 2013-01-04
Marsden, George (1 August 2004), Jonathan Edwards, New Haven: Yale University Press, ISBN 978-0-300-10596-4, retrieved 2013-01-04
Ostling, Richard (4 October 2003), "Theologian Still Relevant After 300 Years", Times Daily, Associated Press, retrieved 2013-01-04
Stout, Harry (20 November 2006), "Edwards as Revivalist",  in Stein, Stephen (ed.), The Cambridge Companion to Jonathan Edwards, New York: Cambridge University Press, pp. 125–143, ISBN 978-0-521-61805-2, retrieved 2013-01-04
Wilson, John, "A History of the Work of Redemption", WJE Online, 9, retrieved 2013-01-04


== External links ==
Sinners in the Hands of an Angry God, from DigitalCommons@University of Nebraska - Lincoln