The popular song "Low Bridge, Everybody Down" was written in 1905 by Thomas S. Allen after Erie Canal barge traffic was converted from mule power to engine power, raising the speed of traffic.  Also known as "Fifteen Years on the Erie Canal", "Fifteen Miles on the Erie Canal", "Erie Canal Song", "Erie Barge Canal", and "Mule Named Sal", the song memorializes the years from 1825 to 1880 when the mule barges made boomtowns out of Utica, Rome, Syracuse, Rochester, and Buffalo, and transformed New York into the Empire State.
The music cover published in 1913 depicts a boy on a mule getting down to pass under a bridge, but the song refers to travelers who would typically ride on top of the boats.  The low bridges would require them to get down out of the way to allow safe passage under a bridge.


== Recordings ==
Early 20th century recordings of the song include ones by Billy Murray, Vernon Dalhart, and Jack Narz.  The song has become part of the folk repertoire, recorded by folksingers like Glenn Yarborough, Pete Seeger and the Weavers, The Kingston Trio, the children's series VeggieTales, and artists like the Sons of the Pioneers. Dan Zanes included it on a children's album with Suzanne Vega singing lead.  Bruce Springsteen recorded the song on his 2006 album We Shall Overcome: The Seeger Sessions.  The cartoon series Animaniacs parodied "Low Bridge" with their song about the Panama Canal. The lyrics are also the text of the book The Erie Canal (1970), illustrated by Peter Spier.


== Traditional lyrics ==


== Variations ==
As with most folk songs, the lyrics have changed over time. The most obvious changes from Thomas Allen's original version has been changing the word "years" to "miles". Allen's original version commemorates 15 years of working along the canal with Sal. The new version using the word miles refers to the average distance a mule would tow a barge before resting or being relieved by another mule. Dave Ruch's research on this change has been documented in an extensive article.Another change is in the second verse. The current line "Git up there mule, here comes a lock" is a change from the original line "Get up there gal, we've passed that lock". The original refers to how mules would rest while waiting for barges to lock through, and then need to be instructed when to start again. The current implies speeding up when a lock is within sight — not a standard course of action.


=== Alternate lyrics ===


== References ==


== External links ==
Library of Congress National Jukebox: Low bridge! Everybody down! 
The Erie Canal Song: History, Origins, and Changes Over the Years - with all five original verses, downloadable music, early recordings, and more
The Erie Canal Song: Low Bridge - Lyrics, Sheet Music, Multiple Audio Performances and other classic Erie Canal songs.
Lyrics to The Erie Canal with MIDI audio
Sheet music to Low Bridge in PDF format
MP3 of 78RPM recording of Low Bridge Everybody Down by Billy Murray from a collection of his recordings at Internet Archive