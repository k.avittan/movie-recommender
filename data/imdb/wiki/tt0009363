Cleopatra is a 1917 American silent historical drama film based on H. Rider Haggard's 1889 novel Cleopatra, the 1890 play Cleopatre by Émile Moreau and Victorien Sardou, and the play Antony and Cleopatra by William Shakespeare. The film starred Theda Bara in the title role, Fritz Leiber, Sr. as Julius Caesar, and Thurston Hall as Mark Antony. The film is now considered lost, with only fragments surviving.


== Plot ==
Because the film has been lost, the following summary is reconstructed from a description in a contemporary film magazine.
Cleopatra (Bara), the Siren of Egypt, by a clever ruse reaches Caesar (Leiber) and he falls victim to her charms. They plan to rule the world together, but then Caesar falls. Cleopatra's life is desired by the church, as the wanton woman's rule has become intolerable. Pharon (Roscoe), a high priest, is given a sacred dagger to take her life. He gives her his love instead and, when she is in need of some money, leads her to the tomb of his ancestors, where she tears the treasure from the breast of the mummy. With this wealth she goes to Rome to meet Antony (Hall). He leaves the affairs of state and travels to Alexandria with her, where they revel. Antony is recalled to Rome and married to Octavia (Blinn), but his soul cries out for Cleopatra. He sends her a message to arm her ships and meet him at Actium, where they battle the opposing forces. They are overpowered, and flee to Alexandria. There they are captured by Octavius (De Vries), and Antony dies in Cleopatra's arms. Before Cleopatra is to be dragged behind the wheels of Octavius' chariot, Pharon the priest, who has never ceased to love her, brings her the serpent that she joyously brings to her breast, dying royally with her crown on her head and scepter in her hand as becomes Egypt.


== Cast ==
Theda Bara as Cleopatra
Fritz Leiber as Caesar
Thurston Hall as Antony
Albert Roscoe as Pharon
Herschel Mayall as Ventidius
Dorothy Drake as Charmian
Delle Duncan as Iras
Henri De Vries as Octavius Caesar
Art Acord as Kephren
Hector V. Sarno as Messenger
Genevieve Blinn as Octavia


== Production ==

Cleopatra was one of the most elaborate Hollywood films ever produced up to that time, with particularly lavish sets and costumes.  According to the studio, the film cost $500,000 (equivalent to around $9.98 million today) to make and employed 2,000 people behind the scenes. Theda Bara appeared in a variety of costumes, some quite risqué. The film was a great success at the time.
The picture was filmed on the Dominquez slough just outside Long Beach, California. The throne prop used in the film ended up, years later, in the possession of Leon Schlesinger Productions; its disposition after the acquisition of that company by Warner Bros. is unknown.


== Reception ==
Like many American films of the time, Cleopatra was subject to cuts by city and state film censorship boards. For example, the Chicago Board of Censors required the following cuts:

in Reel 1, three scenes of the Queen posing before Caesar with her navel exposed, ascending stairs to throne and suggestively leaning against him, two scenes of Queen lying on couch with Caesar standing near, Reel 2, Queen in objectionable costume turning as she embraces Caesar, first and last scenes of Queen at astrologer's table looking into crystal, Reel 3, first scene of Queen at harp and on couch before she goes to dais, two closeups of Queen on dais bending over, two full length views of Queen in chariot exposing her legs, two views of Queen on couch awakening from sleep, Reel 4, entire incident of Queen's meeting with Pharon except scene at beginning of conversation at point where she raises cloth as she starts towards balcony to where she leaves Pharon, all front views of Queen showing her breasts outlined by snake breast plates, closeup of Queen in spangle costume at doorway as she descends stairs and approaches Pharon, closeup kissing scene between Queen and Pharon and Queen's actions following, scene of Queen and Pharon before couch where she turns and exposes legs, three scenes of Queen in objectionable costume before and after Pharon raises knife, two closeups of stabbing guard, all scenes of Queen coming down stairs, two scenes of Queen on low couch, two scenes before and two scenes after taking parchment from Pharon, Queen on couch taking Pharon's hand and scene following embrace, Queen standing while Pharon reads parchment, Queen advancing towards Pharon, closeup of Queen seizing knife and all views of it descending, Reel 6, five closest views of Antony and Queen showing her breasts, Reel 7, Queen standing before Antony, Queen on couch after Antony leaves, three scenes of Queen in leopard skin costume with one breast exposed, full view of Queen in leopard skin costume on couch, Reel 8, the intertitle "Antony, one last word. Will nothing save you from this wanton?" etc., four scenes of Queen and Antony on couch before curtains are drawn aside, Reel 10, Queen walking to throne in costume exposing body.

After the Hays Code was implemented in Hollywood, Cleopatra was judged to be too obscene to be shown. However, despite its controversies, the film was a huge box office draw, becoming one of the most successful blockbusters of 1917.


== Preservation status ==

The last two prints known to exist were destroyed in fires at the Fox studios in 1937 (along with the majority of Bara's other films for Fox) and at the Museum of Modern Art in New York City, 
and the majority of the film is now considered lost.  Only brief fragments of footage are known to survive.Filmmaker and film historian Phillip Dye is reconstructing Cleopatra on video, titled Lost Cleopatra, editing together still picture montages combined with the surviving film clip. The script is based on the original scenario with modifications based on research into censorship reports, reviews of the film and synopses from period magazines. Dye screened the film at the Hollywood Heritage Museum on February 8, 2017.


== See also ==
Pre-Code sex films
List of American films of 1917
Cleopatra VII
List of incomplete or partially lost films
1937 Fox vault fire


== Notes ==


== External links ==
Cleopatra on IMDb
Cleopatra at AllMovie
Cleopatra (1917) surviving footage on YouTube