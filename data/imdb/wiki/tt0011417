Love Without Question is a 1920 silent mystery drama produced and directed by B.A. Rolfe. The film is based on the 1917 novel The Abandoned Room by Charles Wadsworth Camp and was adapted by Violet Clark. The film starred Olive Tell and James W. Morrison, and featured Mario Majeroni, Ivo Dawson, and Floyd Buckley. The film is currently lost.


== Plot ==
An old and wealth recluse named Silas Blackburn (Mario Majeroni) lives in his mansion and is cared for by his long-time butler as well as his ward, Katherine (Olive Tell). During a stormy night, Katherine finds Silas extremely agitated and nervous. The old man accuses Katharine of colluding with his wastrel grandson, Robert (James W. Morrison), to steal his money. Katharine denies his allegations and declares any romance between her and Robert long dead, but Silas threatens to remove her from his will.
Katherine retreats to her room but is unable to sleep. She eventually goes back to find Silas and sees him about to retire in an abandoned room. Katharine begs Silas not to spend the night in the room because it had not been used since three generations of Blackburns died sleeping there. Silas shrugs off her concern saying that he is too afraid to sleep anywhere else. Katharine leaves Silas and returns to her room. Later that night, a restless Katherine returns with the butler and finds Silas dead with a pin-like wound at the base of his skull. The two conclude the old man was murdered.
All while this is happening, Robert is drinking heavily at an actress's apartment. Carlos Paredes (Ivo Dawson), a family friend, urges him to sober up and go to his grandfather's home where he has an appointment. Unable to convince Robert to leave, Carlos departs alone while the young man remains with Maria (Peggy Parr), the actress. Robert wakes up the next morning in a derelict farmhouse near his uncle's mansion.
While Robert pulls himself together, a detective and coroner, Dr. Groome (Charles McKay) arrive at the mansion to investigate Silas's murder. The detective sharply interrogates Katherine and identifies Robert as the prime suspect. Just then, a disheveled Robert appears at the door and is questioned about his activities during the previous night. Robert explains that he was with Maria all night, but Carlos refused to support his alibi. All the people present begin to wonder if Robert had something to do with his grandfather's death. Even Robert, who cannot remember how he ended up in the farmhouse, questions his own innocence.
The detective continues his investigation and cannot understand how the murder was committed in the abandoned room with Silas securely locked inside. He decides to sleep in the room. Just before he retires to the room, he boasts that he will have enough evidence to arrest the killer in the morning. The detective is found dead in the room the following morning.
A few days later, after Silas is buried, a new detective is brought in to investigate what has grown into a double homicide. Meanwhile, Katherine learns that Carlos has been secretly communicating with Maria. She also learns that the two are planning a clandestine meeting, so she decides to spy on them. Robert spots Katherine skulking through the woods and begins wondering if she killed Silas. He keeps his suspicions to himself and announces to everyone that he has decided to spend the night in the abandoned room to uncover the truth behind the murders.
Instead of sleeping in the abandoned room, Robert conceals himself near a window and keeps watch over the bed. Late in the night, he sees a hand reaching through the wall at the head of the bed. Rushing forward, he seizes the arm and discovers it belongs to Katharine, who had entered the room through a secret door. Robert accuses her of the murders, but she protests and scolds him for believing she would have anything to do with such a crime.
The next night, Silas is found sitting in his favorite chair in front of the fireplace. He refuses to explain where he has been and scoffs at the suggestion that he was ever dead at all. Confusion ensues and the coroner suggests they excavated Silas's grave. When this is done, the casket is found to be empty. Suddenly, Maria arrives at the Blackburn mansion. At the sight of the actress, Silas draws a pistol from his jacket and shoots himself.
More confusion ensues, but the butler begins to tell a story that made sense of what just occurred. The butler explains that Silas had a twin brother in South America. At some point, Silas had stolen a vast sum of money from his twin and fled. The butler confessed that the twin had come to the house the night of Silas's death, and threatened to expose the old man as a thief and fraud. He recounted that the brothers finally agreed that the twin should stay overnight in the abandoned room to settle his feud with Silas the following morning. While Silas was at the door plotting his next move, Katherine had arrived and interrupted his scheming. He had intended to kill his twin and hide the body, but he was forced to let the body be mistaken for his own after he told Katherine that he was spending the night in the abandoned room.
The butler continues by explaining that Maria, the actress, was actually the twin's daughter, and that she had known that either her uncle or father was murdered. The reason she had been communicating with Carlos (who was also her secret lover) was that she wanted to find out if her was father was dead. Silas, the butler concludes, had killed himself because he feared Maria would expose him.
The mystery now revealed, Robert promises to mend his way and rekindles his romance with Katharine.


== Cast ==
Olive Tell as Katherine
James W. Morrison as Robert Blackburn
Mario Majeroni as Silas Backburn
Ivo Dawson as Carlos Paredes
Charles McKay as Dr. Groome
Gordon Hamilton as Robinson
Peggy Parr as Mario (Note: While the Peggy Parr's character is referred to as "Maria," the film credits and press-book misspelled the character's name as "Mario." )
George S. Stevens as Jenkins
Floyd Buckley as Howells


== Production ==
Love Without Question was the first production of Jans Pictures, Inc. The young company produced its films at Peerless Picture Studios in Fort Lee, New Jersey, and released them through States' Rights. It was also one of C. Wadsworth Camp (father of writer Madeleine L'Engle) and James W. Morrison's first works since returning from the front lines of World War I. During production of the film, Herman F. Jans and Fred C. Backer traveled throughout the western and southern United States securing contracts with film brokers and theaters to show their first movie. Production of the film completed on March 13, 1920.


== Release ==

It premiered at the Strand Theater in New York City on March 24, 1920. The film was heavily promoted in a 23-page spread in the Motion Picture News. Frank Backer, the general manager of Jans Pictures, was pleased with its reception. He reported that within six weeks of its release the film "had sold its territorial rights throughout the entire world except for 11% of the United States," adding that it marked "a new record for rapid selling of territorial rights on a States' Rights production."The movie's producers advised theaters showing Love Without Question to decorate their lobbies like the film's abandoned room. They also urged them to place a wax figure of Silas on a bed with a sinister wax hand holding a hat pin above his head. Furthermore, a woman wearing a veil and black cloak should walk around mysteriously and scurry away when approached by movie-goers.


== Reception ==
The film received positive reviews from critics. Joshua Lowe (writing as "Jolo") of Variety called the film "an intensely absorbing mystery yarn, fairly reeking with suspensive interest," although he thought the ending was "disappointing and commonplace." He noted that Tell, an established Broadway star, was underutilized and had "relatively little to do but look pretty and 'emote' occasionally." Agnes Smith of the New York Telegraph also reviewed the film positively, writing "It is a plot with many twists and turns and director Rolfe did his very best to give it the proper sort of spooky atmosphere. He has succeeded very well and there are several excellent 'scares' in the picture."


== References ==