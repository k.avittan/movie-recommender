The New Austrian tunneling method (NATM), also known as the sequential excavation method (SEM) or sprayed concrete lining method (SCL), is a method of modern tunnel design and construction employing sophisticated monitoring to optimize various wall reinforcement techniques based on the type of rock encountered as tunneling progresses. This technique first gained attention in the 1960s based on the work of Ladislaus von Rabcewicz, Leopold Müller, and Franz Pacher between 1957 and 1965 in Austria.  The name NATM was intended to distinguish it from earlier methods, with its economic advantage of employing inherent geological strength available in the surrounding rock mass to stabilize the tunnel wherever possible rather than reinforcing the entire tunnel.NATM/SEM is generally thought to have helped revolutionise the modern tunneling industry.  Many modern tunnels have used this excavation technique.
The works built by the Sequential Excavation Method are very attractive from the economic point of view and reasonable in karst conditions.


== Principles ==
The NATM integrates the principles of the behaviour of rock masses under load and monitoring the performance of underground construction during construction. The NATM has often been referred to as a "design as you go" approach, by providing an optimized support based on observed ground conditions. More correctly it can be described as a "design as you monitor" approach, based on observed convergence and divergence in the lining and mapping of prevailing rock conditions. It is not a set of specific excavation and support techniques.
NATM has seven elements:

Exploitation of the strength of native rock mass – Relies on the inherent strength of the surrounding rock mass being conserved as the main component of tunnel support. Primary support is directed to enable the rock to support itself.
Shotcrete protection – Loosening and excessive rock deformation must be minimised. This is achieved by applying a thin layer of shotcrete immediately after face advance.
Measurement and monitoring – Potential deformations of the excavation must be carefully monitored. NATM requires installation of sophisticated measurement instrumentation. It is embedded in lining, ground, and boreholes.  In the event of observed movements, additional supports are installed only when needed, with a resultant overall economy to the total cost of the project.
Flexible support – The primary lining is thin and reflects recent strata conditions. Active rather than passive support is used and the tunnel is strengthened by a flexible combination of rock bolts, wire mesh and steel ribs, not by a thicker concrete lining.
Closing of the invert – Especially crucial in soft ground, the quick closing of the invert (the bottom portion of the tunnel) which creates a load-bearing ring is important, and has the advantage of engaging the inherent strength of the rock mass surrounding the tunnel.
Contractual arrangements – Since the NATM is based on monitoring measurements, changes in support and construction method are possible, but only if the contractual system enables them.
Rock mass classification, ranging from very hard to very soft, determines the minimum support measures required and avoids economic waste that comes from needlessly strong support measures.  Support system designs exist for each of the main rock classes. These serve as the guidelines for tunnel reinforcement.Based on the computation of the optimal cross section, only a thin shotcrete protection is necessary. It is applied immediately behind the excavated tunnel face to create a natural load-bearing ring and minimize the rock's deformation. Geotechnical instruments are installed to measure the later deformation of excavation. Monitoring of the stress distribution within the rock is possible.
This monitoring makes the method very flexible, even if teams encounter unexpected changes in the geomechanical rock consistency, e.g. by crevices or pit water. Reinforcement is done by wired concrete that can be combined with steel ribs or lug bolts, not with thicker shotcrete,
The measured rock properties suggest the appropriate tools for tunnel strengthening. Since the turn of the 21st century, NATM has been used for soft ground excavations and making tunnels in porous sediments. NATM enables immediate adjustments in the construction details, but requires a flexible contractual system to support such changes.


== Variant names ==
NATM was originally developed for use in the Alps, where tunnels are commonly excavated at depth and in high in situ stress conditions. The principles of NATM are fundamental to modern-day tunnelling, and NATM fundamentally involves specifically addressing the specific soil conditions being encountered.  Most city tunnels are built at shallow depth and do not need to control the release of in situ stress as was the case with the original NATM in the Alps. Projects in cities place a higher priority on minimizing settlement therefore they tend to use different support methods from the original NATM. This has led to a confusion in terminology in that tunnelling engineers use "NATM" to mean different things. New terms have arisen and alternative names for certain aspects of NATM have been adopted as its use has spread. This is partly caused by an increased use of this tunneling method in the United States, particularly in soft ground shallow tunnels.
Other designations are seen for this modern tunneling style, e.g. Sequential Excavation Method (SEM) or Sprayed Concrete Lining (SCL) are often used in shallower tunnels. In Japan the terms Centre Dividing Wall NATM or Cross Diaphragm Method (both abbreviated to CDM), and Upper Half Vertical Subdivision method (UHVS) are used.
The Austrian Society of Engineers and Architects defines NATM as "a method where the surrounding rock or soil formations of a tunnel are integrated into an overall ring-like support structure. Thus the supporting formations will themselves be part of this supporting structure."Some engineers use NATM whenever proposing shotcrete for initial ground support of an open-face tunnel. The term NATM can be misleading in relation to soft-ground tunnels. As noted by Emit Brown, NATM can refer both to a design philosophy and a construction method.


== Key features ==
Key features of the NATM design philosophy are:

The strength of the ground around a tunnel is deliberately mobilized to the maximum extent possible.
Mobilization of ground strength is achieved by allowing controlled deformation of the ground.
Initial primary support is installed having load-deformation characteristics appropriate to the ground conditions, and installation is timed with respect to ground deformations.
Instrumentation is installed to monitor deformations in the initial support system, as well as to form the basis of varying the initial support design and the sequence of excavation.When NATM is seen as a construction method, the key features are:

The tunnel is sequentially excavated and supported, and the excavation sequences can be varied to address the specific rock conditions being encountered efficiently.
The initial ground support is provided by shotcrete in combination with fibre or welded-wire fabric reinforcement, steel arches (usually lattice girders), and sometimes ground reinforcement (e.g. soil nails, spiling).
The permanent support is typically a cast-in-place concrete lining placed over a waterproofing membrane.
There is a quick closure of the invert, that is, the bottom part of the tunnel, to create a structural ring that takes advantage of the rock or soil arc naturally created on the top part of the tunnel section.


== Safety ==
The 1994 Heathrow Airport tunnel collapse led to questions about the safety of the NATM. However, the subsequent trial blamed the collapse on poor workmanship and flaws in construction management, rather than on the NATM.


== See also ==
Geotechnical engineering
Rock mass classification
Tunnels
Analysis of controlled deformation in rocks and soils


== References ==


== Further reading ==
Johann Golser, The New Austrian Tunneling Method (NATM), Theoretical Background & Practical Experiences. 2nd Shotcrete conference, Easton, Pennsylvania (USA), 4-8 Oct 1976.