Recoil (often called knockback, kickback or simply kick) is the rearward thrust generated when a gun is being discharged.  In technical terms, the recoil is a result of conservation of momentum, as according to Newton's third law the force required to accelerate something will evoke an equal but opposite reactional force, which means the forward momentum gained by the projectile and exhaust gases (ejecta) will be mathematically balanced out by an equal and opposite momentum exerted back upon the gun.  In hand-held small arms, the recoil momentum will be eventually transferred to the ground, but will do so through the body of the shooter hence resulting in a noticeable impulse commonly referred to as a "kick".
In heavier mounted guns, such as heavy machine guns or artillery pieces, recoil momentum is transferred to the ground through the mounting platform on which the weapon is installed.  In order to bring the rearward moving gun to a halt, the momentum acquired by the gun is dissipated by a forward-acting counter-recoil force applied to the gun over a period of time after the projectile exits the muzzle. To apply this counter-recoiling force, modern mounted guns may employ recoil buffering comprising springs and hydraulic recoil mechanisms, similar to shock-absorbing suspension on automobiles.  Early cannons used systems of ropes along with rolling or sliding friction to provide forces to slow the recoiling cannon to a stop.  Recoil buffering allows the maximum counter-recoil force to be lowered so that strength limitations of the gun mount are not exceeded.  Gun chamber pressures and projectile acceleration forces are tremendous, on the order of tens of thousands of pounds per square inch and tens of thousands of times the acceleration of gravity (g's), both necessary to launch the projectile at useful velocity during the very short travel distance of the barrel.  However, the same pressures acting on the base of the projectile are acting on the rear face of the gun chamber, accelerating the gun rearward during firing.  Practical weight gun mounts are typically not strong enough to withstand the maximum forces accelerating the projectile during the short time the projectile is in the barrel, typically only a few milliseconds.  To mitigate these large recoil forces, recoil buffering mechanisms spread out the counter-recoiling force over a longer time, typically ten to a hundred times longer than the duration of the forces accelerating the projectile.  This results in the required counter-recoiling force being proportionally lower, and easily absorbed by the gun mount.  Modern cannons also employ muzzle brakes very effectively to redirect some of the propellant gasses rearward after projectile exit.  This provides a counter-recoiling force to the barrel, allowing the buffering system and gun mount to be more efficiently designed at even lower weight.
Recoilless guns also exist where much of the high pressure gas remaining in the barrel after projectile exit is vented rearward though a nozzle at the back of the chamber, creating a large counter-recoiling force sufficient to eliminate the need for heavy recoil mitigating buffers on the mount.
The same physics principles affecting recoil in mounted guns also applies to hand-held guns.  However, the shooter's body assumes the role of gun mount, and must similarly dissipate the gun's recoiling momentum over a longer period of time than the bullet travel-time in the barrel, in order not to injure the shooter.  Hands, arms and shoulders have considerable strength and elasticity for this purpose, up to certain practical limits.  Nevertheless, "perceived" recoil limits vary from shooter to shooter, depending on body size, the use of recoil padding, individual pain tolerance, the weight of the firearm, and whether recoil buffering systems and muzzle devices (muzzle brake or suppressor) are employed.  For this reason, establishing recoil safety standards for small arms remains challenging, in spite of the straightforward physics involved.


== Recoil: momentum, energy and impulse ==


=== Momentum ===
A change in momentum of a mass requires a force; according to Newton's first law, known as the law of inertia, inertia simply being another term for mass. That force, applied to a mass, creates an acceleration, which when applied over time, changes the velocity of a mass.  According to Newton's second law, the law of momentum -- changing the velocity of the mass changes its momentum, (mass multiplied by velocity). It is important to understand at this point that velocity is not simply speed.  Velocity is the speed of a mass in a particular direction.  In a very technical sense, speed is a scalar (mathematics), a magnitude, and velocity is a vector (physics), magnitude and direction.  Newton's third law, known as conservation of momentum, recognizes that changes in the motion of a mass, brought about by the application of forces and accelerations, does not occur in isolation; that is, other bodies of mass are found to be involved in directing those forces and accelerations.  Furthermore, if all the masses and velocities involved are accounted for, the vector sum, magnitude and direction, of the momentum of all the bodies involved does not change; hence, momentum of the system is conserved.  This conservation of momentum is why gun recoil occurs in the opposite direction of bullet projection -- the mass times velocity of the projectile in the positive direction equals the mass times velocity of the gun in the negative direction.  In summation, the total momentum of the system equals zero, surprisingly just as it did before the trigger was pulled. From a practical engineering perspective, therefore, through the mathematical application of conservation of momentum, it is possible to calculate a first approximation of a gun's recoil momentum and kinetic energy, and properly design recoil buffering systems to safely dissipate that momentum and energy, simply based on estimates of the projectile speed (and mass) coming out the barrel. To confirm analytical calculations and estimates, once a prototype gun is manufactured, the projectile and gun recoil energy and momentum can be directly measured using a ballistic pendulum and ballistic chronograph.
There are two conservation laws at work when a gun is fired: conservation of momentum and conservation of energy. Recoil is explained by the law of conservation of momentum, and so it is easier to discuss it separately from energy.
The nature of the recoil process is determined by the force of the expanding gases in the barrel upon the gun (recoil force), which is equal and opposite to the force upon the ejecta. It is also determined by the counter-recoil force applied to the gun (e.g. an operator's hand or shoulder, or a mount). The recoil force only acts during the time that the ejecta are still in the barrel of the gun. The counter-recoil force is generally applied over a longer time period and adds forward momentum to the gun equal to the backward momentum supplied by the recoil force, in order to bring the gun to a halt. There are two special cases of counter recoil force: Free-recoil, in which the time duration of the counter-recoil force is very much larger than the duration of the recoil force, and zero-recoil, in which the counter-recoil force matches the recoil force in magnitude and duration. Except for the case of zero-recoil, the counter-recoil force is smaller than the recoil force but lasts for a longer time. Since the recoil force and the counter-recoil force are not matched, the gun will move rearward, slowing down until it comes to rest. In the zero-recoil case, the two forces are matched and the gun will not move when fired. In most cases, a gun is very close to a free-recoil condition, since the recoil process generally lasts much longer than the time needed to move the ejecta down the barrel. An example of near zero-recoil would be a gun securely clamped to a massive or well-anchored table, or supported from behind by a massive wall.  However, employing zero-recoil systems is often neither practical nor safe for the structure of the gun, as the recoil momentum must be absorbed directly through the very small distance of elastic deformation of the materials the gun and mount are made from, perhaps exceeding their strength limits.  For example, placing the butt of a large caliber gun directly against a wall and pulling the trigger risks cracking both the gun stock and the surface of the wall.   
The recoil of a firearm, whether large or small, is a result of the law of conservation of momentum. Assuming that the firearm and projectile are both at rest before firing, then their total momentum is zero. Assuming a near free-recoil condition, and neglecting the gases ejected from the barrel, (an acceptable first estimate), then immediately after firing, conservation of momentum requires that the total momentum of the firearm and projectile is the same as before, namely zero. Stating this mathematically:

  
    
      
        
          p
          
            f
          
        
        +
        
          p
          
            p
          
        
        =
        0
        
      
    
    {\displaystyle p_{f}+p_{p}=0\,}
  where 
  
    
      
        
          p
          
            f
          
        
        
      
    
    {\displaystyle p_{f}\,}
   is the momentum of the firearm and 
  
    
      
        
          p
          
            p
          
        
        
      
    
    {\displaystyle p_{p}\,}
   is the momentum of the projectile. In other words, immediately after firing, the momentum of the firearm is equal and opposite to the momentum of the projectile.
Since momentum of a body is defined as its mass multiplied by its velocity, we can rewrite the above equation as:

  
    
      
        
          m
          
            f
          
        
        
          v
          
            f
          
        
        +
        
          m
          
            p
          
        
        
          v
          
            p
          
        
        =
        0
        
      
    
    {\displaystyle m_{f}v_{f}+m_{p}v_{p}=0\,}
  where:

  
    
      
        
          m
          
            f
          
        
        
      
    
    {\displaystyle m_{f}\,}
   is the mass of the firearm

  
    
      
        
          v
          
            f
          
        
        
      
    
    {\displaystyle v_{f}\,}
   is the velocity of the firearm immediately after firing

  
    
      
        
          m
          
            p
          
        
        
      
    
    {\displaystyle m_{p}\,}
   is the mass of the projectile

  
    
      
        
          v
          
            p
          
        
        
      
    
    {\displaystyle v_{p}\,}
   is the velocity of the projectile immediately after firingA force integrated over the time period during which it acts will yield the momentum supplied by that force. The counter-recoil force must supply enough momentum to the firearm to bring it to a halt. This means that:

  
    
      
        
          ∫
          
            0
          
          
            
              t
              
                c
                r
              
            
          
        
        
          F
          
            c
            r
          
        
        (
        t
        )
        
        d
        t
        =
        −
        
          m
          
            f
          
        
        
          v
          
            f
          
        
        =
        
          m
          
            p
          
        
        
          v
          
            p
          
        
      
    
    {\displaystyle \int _{0}^{t_{cr}}F_{cr}(t)\,dt=-m_{f}v_{f}=m_{p}v_{p}}
  where:

  
    
      
        
          F
          
            c
            r
          
        
        (
        t
        )
        
      
    
    {\displaystyle F_{cr}(t)\,}
   is the counter-recoil force as a function of time (t)

  
    
      
        
          t
          
            c
            r
          
        
        
      
    
    {\displaystyle t_{cr}\,}
   is duration of the counter-recoil forceA similar equation can be written for the recoil force on the firearm:

  
    
      
        
          ∫
          
            0
          
          
            
              t
              
                r
              
            
          
        
        
          F
          
            r
          
        
        (
        t
        )
        
        d
        t
        =
        
          m
          
            f
          
        
        
          v
          
            f
          
        
        =
        −
        
          m
          
            p
          
        
        
          v
          
            p
          
        
      
    
    {\displaystyle \int _{0}^{t_{r}}F_{r}(t)\,dt=m_{f}v_{f}=-m_{p}v_{p}}
  where:

  
    
      
        
          F
          
            r
          
        
        (
        t
        )
        
      
    
    {\displaystyle F_{r}(t)\,}
   is the recoil force as a function of time (t)

  
    
      
        
          t
          
            r
          
        
        
      
    
    {\displaystyle t_{r}\,}
   is duration of the recoil forceAssuming the forces are somewhat evenly spread out over their respective durations, the condition for free-recoil is 
  
    
      
        
          t
          
            r
          
        
        ≪
        
          t
          
            c
            r
          
        
      
    
    {\displaystyle t_{r}\ll t_{cr}}
  , while for zero-recoil, 
  
    
      
        
          F
          
            r
          
        
        (
        t
        )
        +
        
          F
          
            c
            r
          
        
        (
        t
        )
        =
        0
      
    
    {\displaystyle F_{r}(t)+F_{cr}(t)=0}
  .


=== Angular momentum ===
For a gun firing under free-recoil conditions, the force on the gun may not only force the gun backwards, but may also cause it to rotate about its center of mass or recoil mount. This is particularly true of older firearms, such as the classic Kentucky rifle, where the butt stock angles down significantly lower than the barrel, providing a pivot point about which the muzzle may rise during recoil.  Modern firearms, such as the M16 rifle, employ stock designs that are in direct line with the barrel, in order to minimize any rotational effects.  If there is an angle for the recoil parts to rotate about, the torque (
  
    
      
        τ
      
    
    {\displaystyle \tau }
  ) on the gun is given by:

  
    
      
        τ
        =
        I
        
          
            
              
                d
                
                  2
                
              
              θ
            
            
              d
              
                t
                
                  2
                
              
            
          
        
        =
        h
        F
        (
        t
        )
      
    
    {\displaystyle \tau =I{\frac {d^{2}\theta }{dt^{2}}}=hF(t)}
  where 
  
    
      
        h
      
    
    {\textstyle h}
   is the perpendicular distance of the center of mass of the gun below the barrel axis, 
  
    
      
        F
        (
        t
        )
      
    
    {\textstyle F(t)}
   is the force on the gun due to the expanding gases, equal and opposite to the force on the bullet, 
  
    
      
        I
      
    
    {\textstyle I}
   is the moment of inertia of the gun about its center of mass, or its pivot point, and 
  
    
      
        θ
      
    
    {\displaystyle \theta }
   is the angle of rotation of the barrel axis "up" from its orientation at ignition (aim angle). The angular momentum of the gun is found by integrating this equation to obtain:

  
    
      
        I
        
          
            
              d
              θ
            
            
              d
              t
            
          
        
        =
        h
        
          ∫
          
            0
          
          
            t
          
        
        F
        (
        t
        )
        
        d
        t
        =
        h
        
          m
          
            g
          
        
        
          V
          
            g
          
        
        (
        t
        )
        =
        h
        
          m
          
            b
          
        
        
          V
          
            b
          
        
        (
        t
        )
      
    
    {\displaystyle I{\frac {d\theta }{dt}}=h\int _{0}^{t}F(t)\,dt=hm_{g}V_{g}(t)=hm_{b}V_{b}(t)}
  where the equality of the momenta of the gun and bullet have been used. The angular rotation of the gun as the bullet exits the barrel is then found by integrating again:

  
    
      
        I
        
          θ
          
            f
          
        
        =
        h
        
          ∫
          
            0
          
          
            
              t
              
                f
              
            
          
        
        
          m
          
            b
          
        
        
          V
          
            b
          
        
        
        d
        t
        =
        2
        h
        
          m
          
            b
          
        
        L
      
    
    {\displaystyle I\theta _{f}=h\int _{0}^{t_{f}}m_{b}V_{b}\,dt=2hm_{b}L}
  where 
  
    
      
        
          θ
          
            f
          
        
      
    
    {\displaystyle \theta _{f}}
   is the angle above the aim angle at which the bullet leaves the barrel, 
  
    
      
        
          t
          
            f
          
        
      
    
    {\displaystyle t_{f}}
   is the time of travel of the bullet in the barrel (because of the acceleration 
  
    
      
        a
        =
        2
        x
        
          /
        
        
          t
          
            2
          
        
      
    
    {\displaystyle a=2x/t^{2}}
   the time is longer than 
  
    
      
        L
        
          /
        
        
          V
          
            b
          
        
      
    
    {\displaystyle L/V_{b}}
   : 
  
    
      
        
          t
          
            f
          
        
        =
        2
        L
        
          /
        
        
          V
          
            b
          
        
      
    
    {\displaystyle t_{f}=2L/V_{b}}
  ) and L is the distance the bullet travels from its rest position to the tip of the barrel. The angle at which the bullet leaves the barrel above the aim angle is then given by:

  
    
      
        
          θ
          
            f
          
        
        =
        
          
            
              2
              h
              
                m
                
                  b
                
              
              L
            
            I
          
        
      
    
    {\displaystyle \theta _{f}={\frac {2hm_{b}L}{I}}}
  


=== Including the ejected gas ===
Before the projectile leaves the gun barrel, it obturates the bore and "plugs up" the expanding gas generated by the propellant combustion behind it.  This means the gas is essentially contained within a closed system and acts as a neutral element in the overall momentum of the system's physics.  However, when the projectile exits the barrel, this functional seal is removed and the highly energetic bore gas is suddenly free to exit the muzzle and expand in the form of a supersonic shockwave (which can be often fast enough to momentarily overtake the projectile and affect its flight dynamics), creating a phenomenon known as the muzzle blast.  The forward vector of this blast creates a jet propulsion effect that exerts back upon the barrel, and creates an additional momentum on top of the backward momentum generated by the projectile before it exits the gun.
The overall recoil applied to the firearm is equal and opposite to the total forward momentum of not only the projectile, but also the ejected gas.  Likewise, the recoil energy given to the firearm is affected by the ejected gas.  By conservation of mass, the mass of the ejected gas will be equal to the original mass of the propellant (assuming complete burning).  As a rough approximation, the ejected gas can be considered to have an effective exit velocity of 
  
    
      
        α
        
          V
          
            0
          
        
      
    
    {\displaystyle \alpha V_{0}}
   where 
  
    
      
        
          V
          
            0
          
        
      
    
    {\displaystyle V_{0}}
   is the muzzle velocity of the projectile and 
  
    
      
        α
      
    
    {\displaystyle \alpha }
   is approximately constant. The total momentum 
  
    
      
        
          p
          
            e
          
        
      
    
    {\displaystyle p_{e}}
   of the propellant and projectile will then be:

  
    
      
        
          p
          
            e
          
        
        =
        
          m
          
            p
          
        
        
          V
          
            0
          
        
        +
        
          m
          
            g
          
        
        α
        
          V
          
            0
          
        
        
      
    
    {\displaystyle p_{e}=m_{p}V_{0}+m_{g}\alpha V_{0}\,}
  where: 
  
    
      
        
          m
          
            g
          
        
        
      
    
    {\displaystyle m_{g}\,}
   is the mass of the propellant charge, equal to the mass of the ejected gas.
This expression should be substituted into the expression for projectile momentum in order to obtain a more accurate description of the recoil process. The effective velocity may be used in the energy equation as well, but since the value of α used is generally specified for the momentum equation, the energy values obtained may be less accurate. The value of the constant α is generally taken to lie between 1.25 and 1.75. It is mostly dependent upon the type of propellant used, but may depend slightly on other things such as the ratio of the length of the barrel to its radius.
Muzzle devices can reduce the recoil impulse by altering the pattern of gas expansion.  For instance, muzzle brakes primarily works by diverting some of the gas ejecta towards the sides, increasing the lateral blast intensity (hence louder to the sides) but reducing the thrust from the forward-projection (thus less recoil).  Similarly, recoil compensators divert the gas ejecta mostly upwards to counteract the muzzle rise.  However, suppressors work on a different principle, not by vectoring the gas expansion laterally but instead by modulating the forward speed of the gas expansion.  By using internal baffles, the gas is made to travel through a convoluted path before eventually released outside at the front of the suppressor, thus dissipating its energy over a larger area and a longer time.  This reduces both the intensity of the blast (thus lower loudness) and the recoil generated (as for the same impulse, force is inversely proportional to time).


== Perception of recoil ==

For small arms, the way in which the shooter perceives the recoil, or kick, can have a significant impact on the shooter's experience and performance. For example, a gun that is said to "kick like a mule" is going to be approached with trepidation, and the shooter may anticipate the recoil and flinch in anticipation as the shot is released. This leads to the shooter jerking the trigger, rather than pulling it smoothly, and the jerking motion is almost certain to disturb the alignment of the gun and may result in a miss.  The shooter may also be physically injured by firing a weapon generating recoil in excess of what the body can safely absorb or restrain; perhaps getting hit in the eye by the rifle scope, hit in the forehead by a handgun as the elbow bends under the force, or soft tissue damage to the shoulder, wrist and hand; and these results vary for individuals.  In addition, as pictured on the right, excessive recoil can create serious range safety concerns, if the shooter cannot adequately restrain the firearm in a down-range direction.
Perception of recoil is related to the deceleration the body provides against a recoiling gun, deceleration being a force that slows the velocity of the recoiling mass.  Force applied over a distance is energy.  The force that the body feels, therefore, is dissipating the kinetic energy of the recoiling gun mass.  A heavier gun, that is a gun with more mass, will manifest lower recoil kinetic energy, and, generally, result in a lessened perception of recoil.  Therefore, although determining the recoiling energy that must be dissipated through a counter-recoiling force is arrived at by conservation of momentum, kinetic energy of recoil is what is actually being restrained and dissipated.  The ballistics analyst discovers this recoil kinetic energy through analysis of projectile momentum.
One of the common ways of describing the felt recoil of a particular gun-cartridge combination is as "soft" or "sharp" recoiling; soft recoil is recoil spread over a longer period of time, that is at a lower deceleration, and sharp recoil is spread over a shorter period of time, that is with a higher deceleration.  Like pushing softer or harder on the brakes of a car, the driver feels less or more deceleration force being applied, over a longer or shorter distance to bring the car to a stop.  However, for the human body to mechanically adjust recoil time, and hence length, to lessen felt recoil force is perhaps an impossible task.  Other than employing less safe and less accurate practices, such as shooting from the hip, shoulder padding is a safe and effective mechanism that allows sharp recoiling to be lengthened into soft recoiling, as lower decelerating force is transmitted into the body over a slightly greater distance and time, and spread out over a slightly larger surface.  
Keeping the above in mind, you can generally base the relative recoil of firearms by factoring in a small number of parameters:  bullet momentum (weight times velocity), (note that momentum and impulse are interchangeable terms), and the weight of the firearm. Lowering momentum lowers recoil, all else being the same.  Increasing the firearm weight also lowers recoil, again all else being the same.  The following are base examples calculated through the Handloads.com free online calculator, and bullet and firearm data from respective reloading manuals (of medium/common loads) and manufacturer specs:

In a Glock 22 frame, using the empty weight of 1.43 lb (0.65 kg), the following was obtained:
9 mm Luger: Recoil impulse of 0.78 lbf·s (3.5 N·s); Recoil velocity of 17.55 ft/s (5.3 m/s); Recoil energy of 6.84 ft⋅lbf (9.3 J)
.357 SIG: Recoil impulse of 1.06 lbf·s (4.7 N·s); Recoil velocity of 23.78 ft/s (7.2 m/s); Recoil energy of 12.56 ft⋅lbf (17.0 J)
.40 S&W: Recoil impulse of 0.88 lbf·s (3.9 N·s); Recoil velocity of 19.73 ft/s (6.0 m/s); Recoil energy of 8.64 ft⋅lbf (11.7 J)
In a Smith & Wesson .44 Magnum with 7.5-inch barrel, with an empty weight of 3.125 lb (1.417 kg), the following was obtained:
.44 Remington Magnum: Recoil impulse of 1.91 lbf·s (8.5 N·s); Recoil velocity of 19.69 ft/s (6.0 m/s); Recoil energy of 18.81 ft⋅lbf (25.5 J)
In a Smith & Wesson 460 7.5-inch barrel, with an empty weight of 3.5 lb (1.6 kg), the following was obtained:
.460 S&W Magnum: Recoil impulse of 3.14 lbf·s (14.0 N·s); Recoil velocity of 28.91 ft/s (8.8 m/s); Recoil energy of 45.43 ft⋅lbf (61.6 J)
In a Smith & Wesson 500 4.5-inch barrel, with an empty weight of 3.5 lb (1.6 kg), the following was obtained:
.500 S&W Magnum: Recoil impulse of 3.76 lbf·s (16.7 N·s); Recoil velocity of 34.63 ft/s (10.6 m/s); Recoil energy of 65.17 ft⋅lbf (88.4 J)In addition to the overall mass of the gun, reciprocating parts of the gun will affect how the shooter perceives recoil.  While these parts are not part of the ejecta, and do not alter the overall momentum of the system, they do involve moving masses during the operation of firing.  For example, gas-operated shotguns are widely held to have a "softer" recoil than fixed breech or recoil-operated guns.  (Although many semi-automatic recoil and gas-operated guns incorporate recoil buffer systems into the stock that effectively spread out peak felt recoil forces.)  In a gas-operated gun, the bolt is accelerated rearwards by propellant gases during firing, which results in a forward force on the body of the gun.  This is countered by a rearward force as the bolt reaches the limit of travel and moves forwards, resulting in a zero sum, but to the shooter, the recoil has been spread out over a longer period of time, resulting in the "softer" feel.


== Mounted guns ==
 

A recoil system absorbs recoil energy, reducing the peak force that is conveyed to whatever the gun is mounted on. Old-fashioned cannons without a recoil system roll several meters backwards when fired.   The usual recoil system in modern quick-firing guns is the hydro-pneumatic recoil system, first developed by Wladimir Baranovsky in 1872–5 and adopted by the Russian army, then later in France, in the 75mm field gun of 1897. In this system, the barrel is mounted on rails on which it can recoil to the rear, and the recoil is taken up by a cylinder which is similar in operation to an automotive gas-charged shock absorber, and is commonly visible as a cylinder mounted parallel to the barrel of the gun, but shorter and smaller than it. The cylinder contains a charge of compressed air, as well as hydraulic oil; in operation, the barrel's energy is taken up in compressing the air as the barrel recoils backward, then is dissipated via hydraulic damping as the barrel returns forward to the firing position. The recoil impulse is thus spread out over the time in which the barrel is compressing the air, rather than over the much narrower interval of time when the projectile is being fired. This greatly reduces the peak force conveyed to the mount (or to the ground on which the gun has been placed).
In a soft-recoil system, the spring (or air cylinder) that returns the barrel to the forward position starts out in a nearly fully compressed position, then the gun's barrel is released free to fly forward in the moment before firing; the charge is then ignited just as the barrel reaches the fully forward position. Since the barrel is still moving forward when the charge is ignited, about half of the recoil impulse is applied to stopping the forward motion of the barrel, while the other half is, as in the usual system, taken up in recompressing the spring. A latch then catches the barrel and holds it in the starting position. This roughly halves the energy that the spring needs to absorb, and also roughly halves the peak force conveyed to the mount, as compared to the usual system. However, the need to reliably achieve ignition at a single precise instant is a major practical difficulty with this system; and unlike the usual hydro-pneumatic system, soft-recoil systems do not easily deal with hangfires or misfires. One of the early guns to use this system was the French 65 mm mle.1906; it was also used by the World War II British PIAT man-portable anti-tank weapon.
Recoilless rifles and rocket launchers exhaust gas to the rear, balancing the recoil. They are used often as light anti-tank weapons.  The Swedish-made Carl Gustav 84mm recoilless gun is such a weapon.
In machine guns following Hiram Maxim's design – e.g. the Vickers machine gun – the recoil of the barrel is used to drive the feed mechanism.


== Misconceptions about recoil ==
Hollywood and video game depictions of firearm shooting victims being thrown several feet backwards are inaccurate, although not for the often-cited reason of conservation of energy, which would also be in error because conservation of momentum would apply.  Although energy (and momentum) must be conserved (in a closed system), this does not mean that the kinetic energy or momentum of the bullet must be fully deposited into the target in a manner that causes it to fly dramatically away.  
For example, a bullet fired from an M16 rifle (5.56x 45) has approximately 1763 joules of kinetic energy as it leaves the muzzle, but the recoil energy of the gun is less than 7 joules. Despite this imbalance, energy is still conserved because the total energy in the system before firing (the chemical energy stored in the propellant) is equal to the total energy after firing (the kinetic energy of the recoiling firearm, plus the kinetic energy of the bullet and other ejecta, plus the heat energy from the explosion). In order to work out the distribution of kinetic energy between the firearm and the bullet, it is necessary to use the law of conservation of momentum in combination with the law of conservation of energy.
The same reasoning applies when the bullet strikes a target. The bullet may have a kinetic energy in the hundreds or even thousands of joules, which in theory is enough to lift a person well off the ground. This energy, however, cannot be efficiently given to the target, because total momentum must be conserved, too. Approximately, the fraction of energy transferred to the target (energy transferred to the target divided by the total kinetic energy of the bullet) cannot be larger than the inverse of the ratio of the masses of the target and the bullet itself.
The rest of the bullet's kinetic energy is spent in the deformation or shattering of the bullet (depending on bullet construction), damage to the target (depending on target construction), and heat dissipation. In other words, because the bullet strike on the target is an inelastic collision, only a minority of the bullet energy is used to actually impart momentum to the target. This is why a ballistic pendulum relies on conservation of bullet momentum and pendulum energy rather than conservation of bullet energy to determine bullet velocity; a bullet fired into a hanging block of wood or other material will spend much of its kinetic energy to create a hole in the wood and dissipate heat as friction as it slows to a stop.
Gunshot victims frequently (but not always) simply collapse when shot, which is usually due to psychological expectation when hit, a direct hit to the central nervous system, or rapid blood pressure drop causing immediate unconsciousness (see stopping power), or the bullet shatters a leg bone, and not the result of the momentum of the bullet pushing them over.


== See also ==
Muzzle rise, a torque generated by recoil that tends to cause the muzzle to lift up and back
Power factor, a ranking system used in practical shooting competitions to reward cartridges with more recoil.
Recoil operation, the use of recoil force to cycle a weapon's action
Ricochet, a projectile that rebounds, bounces or skips off a surface, potentially backwards toward the shooter
Recoil buffer
Muzzle brake
Recoil pad


== References ==


== External links ==
Recoil Tutorial
Recoil Calculator and summary of equations at JBM.