Oliver Twist; or, the Parish Boy's Progress is Charles Dickens's second novel, and was published as a serial from 1837 to 1839 and released as a three-volume book in 1838, before the serialization ended. The story centres on orphan Oliver Twist, born in a workhouse and sold into apprenticeship with an undertaker. After escaping, Oliver travels to London, where he meets the "Artful Dodger", a member of a gang of juvenile pickpockets led by the elderly criminal Fagin.
Oliver Twist is notable for its unromantic portrayal of criminals and their sordid lives, as well as for exposing the cruel treatment of the many orphans in London in the mid-19th century. The alternative title, The Parish Boy's Progress, alludes to Bunyan's The Pilgrim's Progress, as well as the 18th-century caricature series by William Hogarth, A Rake's Progress and A Harlot's Progress.In this early example of the social novel, Dickens satirises the hypocrisies of his time, including child labour, domestic violence, the recruitment of children as criminals, and the presence of street children. The novel may have been inspired by the story of Robert Blincoe, an orphan whose account of working as a child labourer in a cotton mill was widely read in the 1830s. It is likely that Dickens's own experiences as a youth contributed as well.Oliver Twist has been the subject of numerous adaptations for various media, including a highly successful musical play, Oliver!, and the multiple Academy Award-winning 1968 motion picture. Disney also put its spin on the novel with the animated film called Oliver & Company in 1988.


== Publications ==
The novel was originally published in monthly instalments in the magazine Bentley's Miscellany, from February 1837 to April 1839. It was originally intended to form part of Dickens's serial, The Mudfog Papers. George Cruikshank provided one steel etching per month to illustrate each instalment. The novel first appeared in book form six months before the initial serialisation was completed, in three volumes published by Richard Bentley, the owner of Bentley's Miscellany, under the author's pseudonym, "Boz". It included 24 steel-engraved plates by Cruikshank.
The first edition was titled: Oliver Twist, or, The Parish Boy's Progress.

Serial publication dates:
I – February 1837 (chapters 1–2)
II – March 1837 (chapters 3–4)
III – April 1837 (chapters 5–6)
IV – May 1837 (chapters 7–8)
V – July 1837 (chapters 9-11)
VI – August 1837 (chapters 12–13)
VII – September 1837 (chapters 14–15)
VIII – November 1837 (chapters 16–17)
IX – December 1837 (chapters 18–19)
X – January 1838 (chapters 20–22)
XI – February 1838 (chapters 23–25)
XII – March 1838 (chapters 26–27)
XIII – April 1838 (chapters 28–30)
XIV – May 1838 (chapters 31–32)
XV – June 1838 (chapters 33–34)
XVI – July 1838 (chapters 35–37)
XVII – August 1838 (chapters 38–part of 39)
XVIII – October 1838 (conclusion of chapter 39–41)
XIX – November 1838 (chapters 42–43)
XX – December 1838 (chapters 44–46)
XXI – January 1839 (chapters 47–49)
XXII – February 1839 (chapter 50)
XXIII – March 1839 (chapter 51)
XXIV – April 1839 (chapters 52–53)


== Plot summary ==


=== Workhouse years ===

Oliver Twist is born into a life of poverty and misfortune, raised in a workhouse in the fictional town of Mudfog, located 70 miles (110 km) north of London. He is orphaned by his father's mysterious absence and his mother Agnes' death in childbirth, welcomed only in the workhouse and robbed of her gold name locket. Oliver is meagerly provided for under the terms of the Poor Law and spends the first nine years of his life living at a baby farm in the 'care' of a woman named Mrs. Mann. Oliver is brought up with little food and few comforts. Around the time of Oliver's ninth birthday, Mr. Bumble, the parish beadle, removes Oliver from the baby farm and puts him to work picking and weaving oakum at the main workhouse. Oliver, who toils with very little food, remains in the workhouse for six months. One day, the desperately hungry boys decide to draw lots; the loser must ask for another portion of gruel. This task falls to Oliver himself, who at the next meal comes forward trembling, bowl in hand, and begs Mr. Bumble for gruel with his famous request: "Please, sir, I want some more".A great uproar ensues. The board of well-fed gentlemen who administer the workhouse hypocritically offer £5 to any person wishing to take on the boy as an apprentice. Mr. Gamfield, a brutal chimney sweep, almost claims Oliver. However, when Oliver begs despairingly not to be sent away with "that dreadful man", a kindly magistrate refuses to sign the indentures. Later, Mr. Sowerberry, an undertaker employed by the parish, takes Oliver into his service. He treats Oliver better and, because of the boy's sorrowful countenance, uses him as a mourner at children's funerals. Mr. Sowerberry is in an unhappy marriage, and his wife looks down on Oliver and misses few opportunities to underfeed and mistreat him. He also suffers torment at the hands of Noah Claypole, an oafish and bullying fellow apprentice and "charity boy" who is jealous of Oliver's promotion to mute, and Charlotte, the Sowerberrys' maidservant, who is in love with Noah.
Wanting to bait Oliver, Noah insults the memory of Oliver's biological mother, calling her "a regular right-down bad 'un". Enraged, Oliver assaults and even gets the better of the much bigger boy. However, Mrs. Sowerberry takes Noah's side, helps him to subdue, punch, and beat Oliver, and later compels her husband and Mr. Bumble, who has been sent for in the aftermath of the fight, to beat Oliver again. Once Oliver is sent to his room for the night he breaks down and weeps. The next day Oliver escapes from the Sowerberrys' house and later decides to run away to London to seek a better life.


=== London, the Artful Dodger and Fagin ===

Nearing London, Oliver encounters Jack Dawkins, a pickpocket more commonly known by the nickname the "Artful Dodger", and his sidekick, a boy of a humorous nature named Charley Bates, but Oliver's innocent and trusting nature fails to see any dishonesty in their actions. The Dodger provides Oliver with a free meal and tells him of a gentleman in London who will "give him lodgings for nothing, and never ask for change". Grateful for the unexpected assistance, Oliver follows the Dodger to the "old gentleman's" residence. In this way Oliver unwittingly falls in with an infamous Jewish criminal known as Fagin, the gentleman of whom the Artful Dodger spoke. Ensnared, Oliver lives with Fagin and his gang of juvenile pickpockets in their lair at Saffron Hill for some time, unaware of their criminal occupations. He believes they make wallets and handkerchiefs.
Soon, Oliver naively goes out to "make handkerchiefs" with the Artful Dodger and Charley Bates, only to learn that their real mission is to pick pockets. The Dodger and Charley steal the handkerchief of an old gentleman named Mr Brownlow and promptly flee. When he finds his handkerchief missing, Mr Brownlow turns round, sees Oliver running away in fright, and pursues him, thinking he was the thief. Others join the chase, capture Oliver, and bring him before the magistrate. Curiously, Mr Brownlow has second thoughts about the boy – he seems reluctant to believe he is a pickpocket. To the judge's evident disappointment, a bookstall holder who saw the Dodger commit the crime clears Oliver, who, by now actually ill, faints in the courtroom. Mr Brownlow takes Oliver home and, along with his housekeeper Mrs Bedwin, cares for him.

Oliver stays with Mr Brownlow, recovers rapidly, and blossoms from the unaccustomed kindness. His bliss is interrupted when Fagin, fearing Oliver might tell the police about his criminal gang, decides that Oliver must be brought back to his hideout. When Mr Brownlow sends Oliver out to pay for some books, one of the gang, a young girl named Nancy, whom Oliver had previously met at Fagin's, accosts him with help from her abusive lover, the robber Bill Sikes, and Oliver is quickly bundled back to Fagin's lair. The thieves take the five-pound note Mr Brownlow had entrusted to him, and strip him of his fine new clothes. Oliver, shocked, flees and attempts to call for police assistance, but is dragged back by the Artful Dodger, Charley, and Fagin. Nancy, alone, is sympathetic towards Oliver and saves him from beatings by Fagin and Sikes.
In a renewed attempt to draw Oliver into a life of crime, Fagin forces him to participate in a burglary. Nancy reluctantly assists in recruiting him, all the while assuring the boy that she will help him if she can. Sikes, after threatening to kill him if he does not cooperate, puts Oliver through a small window and orders him to unlock the front door. The robbery goes wrong and Oliver is shot by people in the house and wounded in his left arm. After being abandoned by Sikes, the wounded Oliver makes it back to the house and ends up under the care of the people he was supposed to rob: Miss Rose and her guardian Mrs Maylie.


=== Mystery of a man called "Monks" ===

The mysterious man Monks plots with Fagin to destroy Oliver's reputation. Monks denounces Fagin's failure to turn Oliver into a criminal, and the two of them agree on a plan to make sure he does not find out about his past. Monks is apparently related to Oliver in some way. Back in Oliver's hometown, Mr Bumble has married Mrs Corney, the matron of the workhouse where the story first began, only to find himself in an unhappy marriage, constantly arguing with his domineering wife. After one such argument, Mr Bumble walks to a pub where he meets Monks, who questions him about Oliver. Bumble informs Monks that he knows someone who can give Monks more information for a price, and later Monks meets secretly with the Bumbles. After Mrs Bumble tells Monks all she knows for a price, Monks takes the locket and ring proving Oliver's parents, which had once belonged to Oliver's mother, and drops them into the river flowing under his place. Monks relates these events to Fagin, unaware that Nancy is eavesdropping on their conversations and plans to inform Oliver's benefactors. Mr Brownlow returns to London, where Oliver sees him, and brings him to meet the Maylies.
Now ashamed of her role in Oliver's kidnapping and worried for the boy's safety, Nancy goes to Rose Maylie, staying in London. She knows that Monks and Fagin are plotting to get their hands on the boy again, and offers to meet again any Sunday night on London bridge. Rose tells Mr Brownlow, and the two then make plans with all their party in London. The first Sunday night, Nancy tries to leave for her walk, but Sikes refuses permission when she declines to state exactly where she is going. Fagin realizes that Nancy is up to something, perhaps has a new boyfriend, and resolves to find out what her secret is.
Meanwhile, Noah has fallen out with the undertaker Mr Sowerberry, stolen money from him, and fled to London with Charlotte. Using the name "Morris Bolter", he joins Fagin's gang for protection and becomes a practicer of "the kinchin lay" (robbing of children), and Charlotte is put with the girls. Fagin sends Noah to watch the Artful Dodger on trial, after he is caught with a stolen silver snuff box; the Dodger is convicted while showing his style, with a punishment of transportation to Australia. Next, Noah is sent by Fagin to spy on Nancy, and discovers her meeting with Rose and Mr Brownlow on the bridge, hearing their discussion of why she did not appear the prior week and how to save Oliver from Fagin and Monks.
Fagin angrily passes the information on to Sikes, twisting the story to make it sound as if Nancy had informed on him, when she had not. Believing Nancy to be a traitor, Sikes beats her to death in a fit of rage that very night and flees to the countryside to escape from the police and his conscience. There, Sikes is haunted by visions of Nancy and alarmed by news of her murder spreading across the countryside. He returns to London to find a hiding place and intends to steal money from Fagin and flee to France, only to die by accidentally hanging himself while attempting to lower himself from a rooftop to flee from a mob angry at Nancy's murder.


=== Resolution ===

While Sikes is fleeing the mob, Mr Brownlow forces Monks to listen to the story connecting him, once called Edward Leeford, and Oliver as half brothers, or to face the police for his crimes. Their father, Edwin Leeford, was once friends with Brownlow. Edwin had fallen in love with Oliver's mother, Agnes, after Edwin and Monks' mother had separated. Edwin had to help a dying friend in Rome, and then died there himself, leaving Agnes, "his guilty love", in England. Mr Brownlow has a picture of Agnes and had begun making inquiries when he noticed a marked resemblance between her and Oliver. Monks had hunted his brother to destroy him, to gain all in their father's will. Meeting with Monks and the Bumbles in Oliver's native town, Brownlow asks Oliver to give half his inheritance to Monks to give him a second chance; Oliver is more than happy to comply. Monks moves to "the new world", where he squanders his money, reverts to crime, and dies in prison. Fagin is arrested, tried and condemned to the gallows. On the eve of Fagin's hanging, Oliver, accompanied by Mr Brownlow in an emotional scene, visits Fagin in Newgate Prison, in hope of retrieving papers from Monks. Fagin is lost in a world of his own fear of impending death.
On a happier note, Rose Maylie is the long-lost sister of Agnes, and thus Oliver's aunt. She marries her sweetheart Harry Maylie, who gives up his political ambitions to become a parson, drawing all their friends to settle near them. Oliver lives happily with Mr Brownlow, who adopts him. Noah becomes a paid, semi-professional police informer. The Bumbles lose their positions and are reduced to poverty, ending up in the workhouse themselves. Charley Bates, horrified by Sikes' murder of Nancy, becomes an honest citizen, moves to the country, and eventually becomes prosperous. The novel ends with the tombstone of Oliver's mother on which it is written only one name: Agnes.


== Characters ==


== Major themes and symbols ==

In Oliver Twist, Dickens mixes grim realism with merciless satire to describe the effects of industrialism on 19th-century England and to criticise the harsh new Poor Laws. Oliver, an innocent child, is trapped in a world where his only options seem to be the workhouse, a life of crime symbolised by Fagin's gang, a prison, or an early grave. From this unpromising industrial/institutional setting, however, a fairy tale also emerges. In the midst of corruption and degradation, the essentially passive Oliver remains pure-hearted; he steers away from evil when those around him give in to it, and in proper fairy-tale fashion, he eventually receives his reward – leaving for a peaceful life in the country, surrounded by kind friends. On the way to this happy ending, Dickens explores the kind of life an outcast, orphan boy could expect to lead in 1830s London.


=== Poverty and social class ===
Poverty is a prominent concern in Oliver Twist. Throughout the novel, Dickens enlarged on this theme, describing slums so decrepit that whole rows of houses are on the point of ruin. In an early chapter, Oliver attends a pauper's funeral with Mr. Sowerberry and sees a whole family crowded together in one miserable room.
This prevalent misery makes Oliver's encounters with charity and love more poignant. Oliver owes his life several times over to kindness both large and small. The apparent plague of poverty that Dickens describes also conveyed to his middle-class readers how much of the London population was stricken with poverty and disease. Nonetheless, in Oliver Twist, he delivers a somewhat mixed message about social caste and social injustice. Oliver's illegitimate workhouse origins place him at the nadir of society; as an orphan without friends, he is routinely despised. His "sturdy spirit" keeps him alive despite the torment he must endure. Most of his associates, however, deserve their place among society's dregs and seem very much at home in the depths. Noah Claypole, a charity boy like Oliver, is idle, stupid, and cowardly; Sikes is a thug; Fagin lives by corrupting children, and the Artful Dodger seems born for a life of crime. Many of the middle-class people Oliver encounters—Mrs. Sowerberry, Mr. Bumble, and the savagely hypocritical "gentlemen" of the workhouse board, for example—are, if anything, worse.
On the other hand, Oliver for a workhouse boy—proves to be of gentle birth. Although he has been abused and neglected all his life, he recoils, aghast, at the idea of victimising anyone else. This apparently hereditary gentlemanliness makes Oliver Twist something of a changeling tale, not just an indictment of social injustice. Oliver, born for better things, struggles to survive in the savage world of the underclass before finally being rescued by his family and returned to his proper place—a commodious country house.
The 2005 film Oliver Twist adaptation of the novel dispenses with the paradox of Oliver's genteel origins by eliminating his origin story completely, making him just another anonymous orphan like the rest of Fagin's gang.


=== Symbolism ===
Dickens makes considerable use of symbolism. The "merry old gentleman" Fagin, for example, has satanic characteristics: he is a veteran corrupter of young boys who presides over his own corner of the criminal world; he makes his first appearance standing over a fire holding a toasting-fork, and he refuses to pray on the night before his execution. The London slums, too, have a suffocating, infernal aspect; the dark deeds and dark passions are concretely characterised by dim rooms and pitch-black nights, while the governing mood of terror and brutality may be identified with uncommonly cold weather. In contrast, the countryside where the Maylies take Oliver is a bucolic heaven.The novel is also concerned with social class, and the stark injustice in Oliver's world. When the half-starved child dares to ask for more, the men who punish him are fat, and a remarkable number of the novel's characters are overweight.Toward the end of the novel, the gaze of knowing eyes becomes a potent symbol. For years, Fagin avoids daylight, crowds, and open spaces, concealing himself most of the time in a dark lair. When his luck runs out at last, he squirms in the "living light" of too many eyes as he stands in the dock, awaiting sentence. Similarly, after Sikes kills Nancy at dawn, he flees the bright sunlight in their room, out to the countryside, but is unable to escape the memory of her dead eyes. In addition, Charley Bates turns his back on crime when he sees the murderous cruelty of the man who has been held up to him as a model.


=== Characters ===

In the tradition of Restoration Comedy and Henry Fielding, Dickens fits his characters with appropriate names. Oliver himself, though "badged and ticketed" as a lowly orphan and named according to an alphabetical system, is, in fact, "all of a twist." However, Oliver and his name may have been based on a young workhouse boy named Peter Tolliver whom Dickens knew while growing up. Mr. Grimwig is so called because his seemingly "grim", pessimistic outlook is actually a protective cover for his kind, sentimental soul. Other character names mark their bearers as semi-monstrous caricatures. Mrs. Mann, who has charge of the infant Oliver, is not the most motherly of women; Mr. Bumble, despite his impressive sense of his own dignity, continually mangles the King's English he tries to use; and the Sowerberries are, of course, "sour berries", a reference to Mrs. Sowerberry's perpetual scowl, to Mr. Sowerberry's profession as an undertaker, and to the poor provender Oliver receives from them. Rose Maylie's name echoes her association with flowers and springtime, youth and beauty while Toby Crackit's is a reference to his chosen profession of housebreaking.
Bill Sikes's dog, Bull's-eye, has "faults of temper in common with his owner" and is an emblem of his owner's character. The dog's viciousness represents Sikes's animal-like brutality while Sikes's self-destructiveness is evident in the dog's many scars. The dog, with its willingness to harm anyone on Sikes's whim, shows the mindless brutality of the master. Sikes himself senses that the dog is a reflection of himself and that is why he tries to drown the dog. He is really trying to run away from who he is. This is also illustrated when Sikes dies and the dog immediately dies as well. After Sikes murders Nancy, Bull's-eye also comes to represent Sikes's guilt. The dog leaves bloody footprints on the floor of the room where the murder is committed. Not long after, Sikes becomes desperate to get rid of the dog, convinced that the dog's presence will give him away. Yet, just as Sikes cannot shake off his guilt, he cannot shake off Bull's-eye, who arrives at the house of Sikes's demise before Sikes himself does. Bull's-eye's name also conjures up the image of Nancy's eyes, which haunt Sikes until the bitter end and eventually cause him to hang himself accidentally.
Dickens employs polarised sets of characters to explore various dual themes throughout the novel; Mr. Brownlow and Fagin, for example, personify "good vs. evil". Dickens also juxtaposes honest, law-abiding characters such as Oliver himself with those who, like the Artful Dodger, seem more comfortable on the wrong side of the law. Crime and punishment is another important pair of themes, as is sin and redemption: Dickens describes criminal acts ranging from picking pockets to murder, and the characters are punished severely in the end. Most obviously, he shows Bill Sikes hounded to death by a mob for his brutal acts and sends Fagin to cower in the condemned cell, sentenced to death by due process. Neither character achieves redemption; Sikes dies trying to run away from his guilt, and on his last night alive, the terrified Fagin refuses to see a rabbi or to pray, instead asking Oliver to help him escape.
Nancy, by contrast, redeems herself at the cost of her own life and dies in a prayerful pose. She is one of the few characters in Oliver Twist to display much ambivalence. Her storyline in the novel strongly reflects themes of domestic violence and psychological abuse at the hands of Bill, who ultimately murders her. Although Nancy is a full-fledged criminal, indoctrinated and trained by Fagin since childhood, she retains enough empathy to repent her role in Oliver's kidnapping, and to take steps to try to atone. As one of Fagin's victims, corrupted but not yet morally dead, she gives eloquent voice to the horrors of the old man's little criminal empire. She wants to save Oliver from a similar fate; at the same time, she recoils from the idea of turning traitor, especially to Bill Sikes, whom she loves. When he was later criticised for giving a "thieving, whoring slut of the streets" such an unaccountable reversal of character, Dickens ascribed her change of heart to "the last fair drop of water at the bottom of a dried-up, weed-choked well".


== Allegations of antisemitism ==

Dickens has been accused of following antisemitic stereotypes because of his portrayal of the Jewish character Fagin in Oliver Twist. Paul Vallely writes that Fagin is widely seen as one of the most grotesque Jews in English literature, and the most vivid of Dickens's 989 characters. Nadia Valdman, who writes about the portrayal of Jews in literature, argues that Fagin's representation was drawn from the image of the Jew as inherently evil, that the imagery associated him with the Devil, and with beasts.The novel refers to Fagin 274 times in the first 38 chapters as "the Jew", while the ethnicity or religion of the other characters is rarely mentioned. In 1854, The Jewish Chronicle asked why "Jews alone should be excluded from the 'sympathizing heart' of this great author and powerful friend of the oppressed." Dickens (who had extensive knowledge of London street life and child exploitation) explained that he had made Fagin Jewish because "it unfortunately was true, of the time to which the story refers, that that class of criminal almost invariably was a Jew." Dickens commented that by calling Fagin a Jew he had meant no imputation against the Jewish faith, saying in a letter, "I have no feeling towards the Jews but a friendly one. I always speak well of them, whether in public or private, and bear my testimony (as I ought to do) to their perfect good faith in such transactions as I have ever had with them." Eliza Davis, whose husband had purchased Dickens's home in 1860 when he had put it up for sale, wrote to Dickens in protest at his portrayal of Fagin, arguing that he had "encouraged a vile prejudice against the despised Hebrew", and that he had done a great wrong to the Jewish people. While Dickens first reacted defensively upon receiving Davis's letter, he then halted the printing of Oliver Twist, and changed the text for the parts of the book that had not been set, which explains why after the first 38 chapters Fagin is barely called "the Jew" at all in the next 179 references to him.


== Film, television and theatrical adaptations ==


=== Film ===
Oliver Twist (1909), the first adaptation of Dickens' novel, a silent film starring Edith Storey and Elita Proctor Otis.
Oliver Twist (1912), a British silent film adaptation, directed by Thomas Bentley.
Oliver Twist (1912), an American silent film adaptation starring Nat C. Goodwin.
Oliver Twist (1916), a 1916 silent film adaptation, starring Marie Doro and Tully Marshall.
Oliver Twist (1919), a silent Hungarian film adaptation.
Oliver Twist (1922), silent film adaptation featuring Lon Chaney and Jackie Coogan.
Oliver Twist (1933), the first sound production of Dickens' novel.
Oliver Twist, a 1948 David Lean film adaptation starring Alec Guinness as Fagin.
Manik, a 1961 Bengali film directed by Bijalibaran Sen which was based on this novel. The film stars Pahari Sanyal, Chhabi Biswas, Sombhu Mitra and Tripti Mitra.
Oliver!, a 1968 British musical adaptation, winner in the Best Picture category at the 41st Academy Awards.
Oliver Twist (1974), a 1974 animation film co-written by Ben Starr.
Oliver Twist (1982), an Australian animated film.
Oliver & Company, a 1988 Disney full-length animated feature inspired by the story of Oliver Twist. The story takes place in modern-day New York City, with Oliver (voiced by Joey Lawrence) portrayed as an orphaned kitten, the Dodger as a street-wise mongrel (voiced by Billy Joel), and Fagin (voiced by Dom DeLuise) as a homeless bum who lives on the docks with his pack of stray dogs that he trains to steal so he can survive and repay his debt to loan shark Sykes (voiced by Robert Loggia).
Twisted, a 1996 independent film based on Charles Dickens' novel Oliver Twist set in the gay underground sub-culture of New York City in the 1990s and starring Emmy Award, Tony Award, Grammy Award winner Billy Porter and Academy Award nominee William Hickey (actor) directed by Seth Michael Donsky.
Oliver Twist (1997), directed by Tony Bill and starring Richard Dreyfuss and Elijah Wood.
Twist, a 2003 independent film loosely based on Charles Dickens' novel Oliver Twist
Oliver Twist (2005), directed by Roman Polanski and starring Barney Clark and Ben Kingsley.
August Rush is a 2007 film, set in the present day, which a few critics have suggested is essentially a musical adaptation of Oliver Twist.
Twist (2020) - Modern day version directed by Martion Owen, and starring Michael Caine as Fagin.


=== Television ===
Oliver Twist, a 1982 TV movie directed by Clive Donner, starring George C. Scott as Fagin and Tim Curry as Bill Sikes.
Oliver Twist, a 12 episode 1985 BBC One miniseries directed by Gareth Davies, starring Eric Porter and Michael Attwell.
Oliver Twist, 1999 ITV miniseries adaptation starring Andy Serkis and Keira Knightley.
Oliver Twist, a five episode 2007 BBC One miniseries directed by Coky Giedroyc, starring Timothy Spall and Tom Hardy.
Saban's Adventures of Oliver Twist, a 52 episode animated American-French co-production that aired between 1996 and 1997, where the story is downplayed for younger viewers, where Oliver loses his mother in a crowd rather than being dead and the characters are represented by anthropomorphic animals. Oliver in this version is a young dog.
Escape of the Artful Dodger, an Australian TV series set as a sequel, where Dodger and Oliver are sent to the colony of Australia.


=== Theatre ===
In 1838 Charles Zachary Barnett's adaptation, the three-act burletta Oliver Twist; or, The Parish Boy's Progress opened at the Marylebone Theatre in London .Oliver!, a West End theatre stage musical adaptation by Lionel Bart.
Oliver Twist is a 2017 stage adaptation of the novel written by Anya Reiss which premiered at the Regent's Park Theatre. The show was directed by Caroline Byrne.


== See also ==
 Literature portal
 Novels portal
Charles Dickens bibliography
Child labor


== References ==


== External links ==
Online versionsManuscript material and articles relating to Oliver Twist. From the British Library's Discovering Literature website.
Oliver Twist at Internet Archive

 Oliver Twist at Project Gutenberg
Oliver Twist at Standard Ebooks
 Oliver Twist public domain audiobook at LibriVox
Oliver Twist—easy to read HTML version
Oliver Twist, or, The Parish Boy's Progress Typeset PDF version, including the illustrations of James Mahoney (1871 Household Edition by Chapman & Hall).Critical analysisWhen Is a Book Not a Book? Oliver Twist in Context, a seminar by Robert Patten from the New York Public Library
Background information and plot summary for Oliver Twist, with links to other resources
Article in British Medical Journal on Oliver Twist's diet