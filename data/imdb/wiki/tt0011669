In architecture, a folly is a building constructed primarily for decoration, but suggesting through its appearance some other purpose, or of such extravagant appearance that it transcends the range of garden ornaments usually associated with the class of buildings to which it belongs. 
Eighteenth-century English landscape gardening and French landscape gardening often featured mock Roman temples, symbolising classical virtues. Other 18th-century garden follies represented Chinese temples, Egyptian pyramids, ruined abbeys, or Tatar tents, to represent different continents or historical eras. Sometimes they represented rustic villages, mills, and cottages to symbolise rural virtues. Many follies, particularly during times of famine, such as the Great Famine in Ireland, were built as a form of poor relief, to provide employment for peasants and unemployed artisans.
In English, the term began as "a popular name for any costly structure considered to have shown folly in the builder", the OED's definition, and were often named after the individual who commissioned or designed the project. The connotations of silliness or madness in this definition is in accord with the general meaning of the French word "folie"; however, another older meaning of this word is "delight" or "favourite abode".  This sense included conventional, practical, buildings that were thought unduly large or expensive, such as Beckford's Folly, an extremely expensive early Gothic Revival country house that collapsed under the weight of its tower in 1825, 12 years after completion.  As a general term, "folly" is usually applied to a small building that appears to have no practical purpose or the purpose of which appears less important than its striking and unusual design, but the term is ultimately subjective, so a precise definition is not possible.


== Characteristics ==

The concept of the folly is subjective and it has been suggested that the definition of a folly "lies in the eyes of the beholder". Typical characteristics include:

They have no purpose other than as an ornament. Often they have some of the appearance of a building constructed for a particular purpose, such as a castle or tower, but this appearance is a sham. Equally, if they have a purpose, it may be disguised.
They are buildings, or parts of buildings. Thus they are distinguished from other garden ornaments such as sculpture.
They are purpose-built. Follies are deliberately built as ornaments.
They are often eccentric in design or construction. This is not strictly necessary; however, it is common for these structures to call attention to themselves through unusual details or form.
There is often an element of fakery in their construction. The canonical example of this is the sham ruin: a folly which pretends to be the remains of an old building but which was in fact constructed in that state.
They were built or commissioned for pleasure.


== History ==

Follies began as decorative accents on the great estates of the late 16th century and early 17th century but they flourished especially in the two centuries which followed. Many estates had ruins of monastic houses and (in Italy) Roman villas; others, lacking such buildings, constructed their own sham versions of these romantic structures.
However, very few follies are completely without a practical purpose. Apart from their decorative aspect, many originally had a use which was lost later, such as hunting towers. Follies are misunderstood structures, according to The Folly Fellowship, a charity that exists to celebrate the history and splendour of these often neglected buildings.


=== Follies in 18th-century French and English gardens ===

Follies (French: fabriques) were an important feature of the English garden and French landscape garden in the 18th century, such as Stowe and Stourhead in England and Ermenonville and the gardens of Versailles in France.  They were usually in the form of Roman temples,  ruined Gothic abbeys, or Egyptian pyramids. Painshill Park in Surrey contained almost a full set, with a large Gothic tower and various other Gothic buildings, a Roman temple, a hermit's retreat with resident hermit, a Turkish tent, a shell-encrusted water grotto and other features.  In France they sometimes took the form of romantic farmhouses, mills and cottages, as in Marie Antoinette's Hameau de la Reine at Versailles. Sometimes they were copied from landscape paintings by painters such as Claude Lorrain and Hubert Robert. Often, they had symbolic importance, illustrating the virtues of ancient Rome,  or the virtues of country life. The temple of philosophy at Ermenonville, left unfinished, symbolised that knowledge would never be complete, while the temple of modern virtues at Stowe was deliberately ruined, to show the decay of contemporary morals.Later in the 18th century, the follies became more exotic, representing other parts of the world, including Chinese pagodas, Japanese bridges, and Tatar tents.


=== Famine follies ===
The Great Famine of Ireland of 1845–49 led to the building of several follies in order to provide relief to the poor without issuing unconditional handouts. However, to hire the needy for work on useful projects would deprive existing workers of their jobs. Thus, construction projects termed "famine follies" came to be built. These included roads in the middle of nowhere, between two seemingly random points, screen and estate walls, piers in the middle of bogs, etc.


== Examples ==

Follies are found worldwide, but they are particularly abundant in Great Britain.


=== Australia ===
Big Banana, Coffs Harbour, New South Wales


=== Austria ===

Roman ruin and gloriettes, in the park of Schönbrunn Palace, Vienna


=== Canada ===
Dundurn Castle in Hamilton, Ontario


=== Czech Republic ===
Series of buildings in Lednice–Valtice Cultural Landscape


=== France ===
Chanteloup Pagoda, near Amboise
Désert de Retz, folly garden in Chambourcy near Paris, France (18th century)
Parc de la Villette in Paris has a number of modern follies by architect Bernard Tschumi.
Ferdinand Cheval in Châteauneuf-de-Galaure, built what he called an Ideal Palace, seen as an example of naive architecture.
Hameau de la Reine, in the park of the Château de Versailles


=== Germany ===
Bergpark Wilhelmshöhe water features
Lighthouse in the park of Moritzburg Castle near Dresden
Mosque in the Schwetzingen Castle gardens
Pfaueninsel artificial ruin, Berlin
Ruinenberg near Sanssouci Park, Potsdam


=== Hungary ===
Bory Castle at Székesfehérvár
Taródi Castle at Sopron
Vajdahunyad vára in the City Park of Budapest


=== India ===
Overbury's Folly, Thalassery, Kerala
Rock Garden of Chandigarh


=== Ireland ===
Carden's Folly
Casino at Marino
Conolly's Folly and The Wonderful Barn on the same estate
Killiney Hill, with several follies
Larchill in County Kildare, with several follies
Powerscourt Estate, which contains the Pepperpot Tower
Saint Anne's Park, which contains a number of follies
Saint Enda's Park, former school of Patrick Pearse, contains several follies
The Jealous Wall at Belvedere House near Mullingar, Co. Westmeath


=== Italy ===
La Scarzuola, Montegabbione
The Park of the Monsters (Bomarzo Gardens)
Il Giardino dei Tarocchi near Capalbio


=== Jamaica ===
Three follies were built on Folly Estate, Port Antonio, in 1905. They are now in ruins. 


=== Malta ===

Lija Belvedere Tower


=== Poland ===

Roman aqueduct, Arkadia, Łowicz County
Temple of the Sibyl in Puławy


=== Romania ===
Iulia Hasdeu Castle


=== Russia ===
Ruined towers in Peterhof, Tsarskoe Selo, Gatchina, and Tsaritsino
Creaking Pagoda and Chinese Village in Tsarskoe Selo
Dutch Admiralty in Tsarskoe Selo


=== Spain ===

El Capricho, Comillas (Cantabria)


=== Ukraine ===

Ruins in Oleksandriia, Bila Tserkva


=== United Kingdom ===


==== England ====
Ashton Memorial, Lancaster
Beckford's Tower, Somerset
Broadway Tower, The Cotswolds
Bettison's Folly, Hornsea
Black Castle Public House, Bristol
Brizlee Tower, Northumberland
The Cage at Lyme Park, Cheshire
The Castle at Roundhay Park, West Yorkshire
Clavell Tower, Dorset
Faringdon Folly, Faringdon, Oxfordshire
Flounders' Folly, Shropshire
Forbidden Corner, North Yorkshire
Freston Tower, near Ipswich, Suffolk
Garrick's Temple to Shakespeare, Hampton
Gothic Tower at Goldney Hall, Bristol
The Great Pagoda at the Royal Botanic Gardens, Kew, London¨
Hadlow Tower, Hadlow, Kent
Hardwick Hall Country Park, County Durham contains several restored follies
Hawkstone Park, follies and gardens in Shropshire
Hiorne's Tower, Arundel Castle, West Sussex
Horton Tower, Dorset
King Alfred's Tower, Stourhead, Somerset
Mow Cop Castle, Staffordshire
Old John, Bradgate Park, Leicestershire
Painshill, Cobham, Surrey, an 18th-century landscape garden with several follies, some modern reconstructions
Penshaw Monument, Penshaw, Sunderland
Pelham's Pillar, Caistor, North Lincolnshire
Perrott's Folly, Birmingham
Pope's Grotto, Twickenham, South West London
Racton Monument, West Sussex
The Ruined Arch at the Royal Botanic Gardens, Kew, London
Rushton Triangular Lodge, Northamptonshire (16th century)
Severndroog Castle, Shooter's Hill, south-east London
Sham Castle, Bathwick Hill, Bath, Somerset
The Sledmere Cross takes the form of an Eleanor Cross and is a true 'folly' that was 'converted' to a World War I Memorial
Solomon's Temple, Buxton, Derbyshire
Stainborough Castle, South Yorkshire
Two of the follies in Staunton Country Park have survived until the present day
Stowe School has several follies in the grounds
Sway Tower, New Forest
Tattingstone Wonder, near Ipswich, Suffolk
Wainhouse Tower, the tallest folly in the world, Halifax, West Yorkshire
Wentworth Woodhouse, Wentworth, South Yorkshire
Williamson Tunnels, probably the largest underground folly in the world, Liverpool
Wilder's Folly, Sulham, Berkshire


==== Scotland ====
The Caldwell Tower, Lugton, Renfrewshire
Captain Frasers Folly (Uig Tower) Isle of Skye
Dunmore Pineapple, Falkirk
Hume Castle, Berwickshire
Kinnoull Hill Tower, Perth
McCaig's Tower, Oban, Argyll and Bute
National Monument, Edinburgh
The Temple near Castle Semple Loch, Renfrewshire


==== Wales ====

Castell Coch, Cardiff
Clytha Castle, Monmouthshire
Folly Tower at Pontypool
Paxton's Tower, Carmarthenshire
Portmeirion
Gwrych Castle, Conwy County Borough


=== United States ===
Bancroft Tower, Worcester, Massachusetts
Belvedere Castle, New York City
Bishop Castle, outside of Pueblo, Colorado
Chateau Laroche, Loveland, Ohio
Italian Barge, Villa Vizcaya, Miami, Florida
Kingfisher Tower, Otsego Lake (New York)
Lawson Tower, Scituate, Massachusetts
Coral Castle, Homestead, Florida
Summersville Lake Lighthouse, Mount Nebo, West Virginia
The Parthenon in Nashville, Tennessee
Hofmann Tower in Lyons, Illinois
Vessel, New York, New York
Watts Towers, Watts, Los Angeles


== See also ==

List of garden features
English garden
Folly Fellowship
French landscape garden
Garden hermit
Goat tower
Grotto
Novelty architecture
Ruin value


== References ==


== Bibliography ==


== External links ==
 Media related to Follies (architecture) at Wikimedia Commons