My Best Friend's Wedding is a 1997 American romantic comedy film directed by P.J. Hogan from a screenplay by Ronald Bass. The film stars Julia Roberts, Dermot Mulroney, Cameron Diaz and Rupert Everett.
The film received generally positive reviews from critics and was a global box-office hit. The soundtrack song "I Say a Little Prayer (For You)" was covered by singer Diana King and featured heavily in the film, making it a US Billboard Hot 100 hit. The soundtrack featured a number of Burt Bacharach/Hal David songs.


== Plot ==
Three weeks before her 28th birthday, New York City food critic Julianne “Jules” Potter receives a call from her lifelong friend Michael O'Neal, a Chicago sportswriter. Years ago, the two agreed that if unmarried by 28, they would marry each other. Michael tells her that in four days, he will marry Kimmy Wallace, a college student whose father owns the Chicago White Sox. Realizing she is in love with Michael, Jules resolves to sabotage his wedding. Arriving in Chicago, she reunites with Michael and meets Kimmy, who asks her to be the maid of honor. Jules schemes to break up the couple, but her attempt to humiliate Kimmy at a karaoke bar backfires. She manipulates Kimmy into asking her father to offer Michael a job, which Jules knows will anger Michael, but this fails as well.
Frustrated, Jules begs her friend George for help, and he flies to Chicago. On George’s advice, Jules prepares to confess her feelings to Michael, but instead tells him that she is engaged to George, hoping to make Michael jealous. George, who is gay, plays along but embarrasses Jules at lunch with the wedding party, singing “I Say a Little Prayer” as the whole restaurant joins in. George flies home, and Jules tells Michael that her “relationship” with George is over. Michael admits to feeling jealous and gives her the chance to confess her own feelings, but she lets the moment pass, and they share a dance as Michael sings “The Way You Look Tonight”.
The day before the wedding, at Kimmy's father's office, Jules uses his email account to forge a message from him to Michael's boss, asking that Michael be fired to allow Kimmy’s father to hire him at Kimmy’s insistence. She saves the message rather than send it, but later realizes that Kimmy’s father has unknowingly sent the email. Jules lies to enlist Michael’s help, but they find the office locked. Returning to Jules’ hotel, Michael receives a message from his boss notifying him of the email. Furious, he calls Kimmy, calling off the wedding.
The next morning, Jules discovers that neither Michael nor Kimmy have told anyone else that the wedding is off. She tries to manipulate the couple into breaking up for good, but Michael and Kimmy decide to get married after all. Jules finally confesses her love for Michael and kisses him. Kimmy witnesses this and drives away, pursued by Michael, who is followed by Jules in a caterer’s truck. Jules calls George, who assures her that Michael loves Kimmy. Finding Michael at Chicago Union Station, Jules confesses to everything. He forgives her and tells her that here at the station is where he proposed to Kimmy and she accepted, and they split up to look for Kimmy.
Jules finds Kimmy in the bathroom of Comiskey Park. Amid a crowd of onlookers, Kimmy confronts Jules for interfering with Michael. Jules apologizes, assuring Kimmy that Michael truly loves her, and they reconcile. The wedding proceeds, and at the reception, Jules gives a heartfelt speech as Kimmy's maid of honor. Jules and Michael share their goodbyes, both finally moving on. On the phone with George, Jules is surprised to see him at the reception, and they dance together.


== Cast ==
Julia Roberts as Julianne Potter, a 27-year-old food critic who realizes she's in love with her best friend Michael and tries to win him back after he decides to marry someone else.
Dermot Mulroney as Michael O'Neal, Julianne's best friend and a sportswriter who is engaged to Kimmy Wallace.
Cameron Diaz as Kimberly "Kimmy" Wallace, Michael's fiancée who comes from an affluent family.
Rupert Everett as George Downes, Julianne's gay friend and editor who pretends to be engaged to Julianne to make Michael jealous.
Philip Bosco as Walter Wallace, husband of Isabelle, father of Kimmy, and future father-in-law of Michael. He is a rich businessman who owns the Chicago White Sox baseball team.
M. Emmet Walsh as Joe O'Neal, father of Michael and Scotty O'Neal and future father-in-law of Kimmy. He suggested that Julianne be Michael's best man, but had to go with Scotty instead.
Rachel Griffiths as Samantha Newhouse, twin sister of Mandy and one of Kimmy's bridesmaids.
Carrie Preston as Mandy Newhouse, twin sister of Samantha and one of Kimmy's bridesmaids.
Susan Sullivan as Isabelle Wallace, wife of Walter, mother of Kimmy, and future mother-in-law of Michael.
Christopher Masterson as Scotty O'Neal, youngest son of Joe and younger brother of Michael and future brother-in-law of Kimmy. He serves as his brother's best man at his wedding.
Paul Giamatti as Richard, a bellman who encounters Julianne in a hotel hallway.


== Release ==


=== Box office ===
The film opened at No. 2 at the North American box office, making $21,678,377 USD in its opening weekend, behind Batman & Robin. It stayed in the top 10 weekly U.S. box-office for six consecutive weeks, and eventually earned $127,120,029. The worldwide gross total stands at $299,288,605 (listed as one of the 10 biggest films of 1997 both domestically and worldwide).


=== Critical reception ===
The film received generally positive reviews from critics. As of 27 February 2018, the film holds a 73% approval rating on review aggregator Rotten Tomatoes based on 59 reviews, with an average rating of 6.4/10. The website's critical consensus reads, "Thanks to a charming performance from Julia Roberts and a subversive spin on the genre, My Best Friend's Wedding is a refreshingly entertaining romantic comedy." On Metacritic, the film holds a score of 50 out of 100 based on 23 critical reviews, indicating "mixed or average reviews."Total Film praised the film, giving it four stars out of five and stating "Here she banishes all memories of Mary Reilly and I Love Trouble with a lively, nay sparkling, performance. Smiling that killer smile, shedding those winning tears, delivering great lines with effortless charm, Roberts is back where she rightly belongs - not in grey period costume, but as the sexy queen of laughs." The review also said that "My Best Friend's Wedding is a perfect date movie," and a film that "proves Roberts isn't as crap as we all thought she was."Peter Travers of Rolling Stone called it "the summer-date-film supreme for pretty women and the gay men they love," despite criticisms of the script. He praises Roberts as "riper, more dexterous with a comic line, slyer with modulation," concluding that "Roberts puts her heart into this one." Joanna Berry of Radio Times gave it four stars out of five, observing that this "sparkling comedy" proved to be a career-resurrecting movie for Julia Roberts.Roger Ebert of Chicago Sun-Times said, "One of the pleasures of Ronald Bass' screenplay is the way it subverts the usual comic formulas that would fuel a plot like this." CNN movie reviewer Carol Buckland said Roberts "lights up the screen," calling the film "fluffy fun."Andrew Johnson, writing in Time Out New York, observed, "The best scene occurs when Julianne's gay editor and confidant George (Everett) turns up in Chicago and poses as her fiancé, seizing control of the film for five delicious minutes. His devilish impersonation of a straight guy is priceless, and things only get better when he leads a sing-along at the rehearsal dinner. At times like this, when the film spins into pop culture overdrive that it stops being a star vehicle and flirts with genuine comic brilliance."


=== Awards and recognition ===
American Film Institute recognition:

AFI's 100 Years... 100 Laughs - Nominated


== Soundtrack ==
The soundtrack was released on June 17, 1997 with Stage and Screen genre. The soundtrack relied on covers of familiar songs. The soundtrack was praised by AllMusic to work "better than it should, since most of the vocalists... concentrate on the songs..."
"I Say a Little Prayer (For You)" – Diana King
"Wishin' and Hopin' " – Ani DiFranco
"You Don't Know Me" – Jann Arden
"Tell Him" – The Exciters
"I Just Don't Know What to Do with Myself" – Nicky Holland
"I'll Be Okay" – Amanda Marshall
"The Way You Look Tonight" – Tony Bennett
"What the World Needs Now Is Love" – Jackie Deshannon
"I'll Never Fall in Love Again" – Mary Chapin Carpenter
"Always You" – Sophie Zelmani
"If You Wanna Be Happy" – Jimmy Soul
"I Say a Little Prayer (For You)" – The Cast of My Best Friend's Wedding
"Suite From My Best Friend's Wedding" – James Newton HowardChart positions


=== Certifications ===


== Remakes ==
Three remakes with the same title have been released. 

A Chinese version was released in China on August 5, 2016.
A Mexican remake was presented in Mexico on February 14, 2019.


== References ==


== External links ==
My Best Friend's Wedding on IMDb
My Best Friend's Wedding at the TCM Movie Database
My Best Friend's Wedding at AllMovie
My Best Friend's Wedding at Box Office Mojo
My Best Friend's Wedding at Rotten Tomatoes
My Best Friend's Wedding at Metacritic
Make Your Best Friend Fall In Love With You at Bright Success