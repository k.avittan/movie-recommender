The Picture of Dorian Gray is a Gothic and philosophical novel by Oscar Wilde, first published complete in the July 1890 issue of Lippincott's Monthly Magazine. Fearing the story was indecent, prior to publication the magazine's editor deleted roughly five hundred words without Wilde's knowledge. Despite that censorship, The Picture of Dorian Gray offended the moral sensibilities of British book reviewers, some of whom said that Oscar Wilde merited prosecution for violating the laws guarding public morality. In response, Wilde aggressively defended his novel and art in correspondence with the British press, although he personally made excisions of some of the most controversial material when revising and lengthening the story for book publication the following year.
The longer and revised version of The Picture of Dorian Gray published in book form in 1891 featured an aphoristic preface—a defence of the artist's rights and of art for art's sake—based in part on his press defences of the novel the previous year. The content, style, and presentation of the preface made it famous in its own right, as a literary and artistic manifesto. In April 1891, the publishing firm of Ward, Lock and Company, who had distributed the shorter, more inflammatory, magazine version in England the previous year, published the revised version of The Picture of Dorian Gray.The Picture of Dorian Gray is the only novel written by Wilde.  It exists in several versions: the 1890 magazine edition (in 13 chapters), with important material deleted before publication by the magazine's editor, J. M. Stoddart; the "uncensored" version submitted to Lippincott's Monthly Magazine for publication (also in 13 chapters), with all of Wilde's original material intact, first published in 2011 by Harvard University Press; and the 1891 book edition (in 20 chapters). As literature of the 19th century, The Picture of Dorian Gray "pivots on a gothic plot device" with strong themes interpreted from Faust.


== Summary ==
Dorian Gray is the subject of a full-length portrait in oil by Basil Hallward, an artist impressed and infatuated by Dorian's beauty; he believes that Dorian's beauty is responsible for the new mood in his art as a painter. Through Basil, Dorian meets Lord Henry Wotton, and he soon is enthralled by the aristocrat's hedonistic world view: that beauty and sensual fulfilment are the only things worth pursuing in life.
Newly understanding that his beauty will fade, Dorian expresses the desire to sell his soul, to ensure that the picture, rather than he, will age and fade. The wish is granted, and Dorian pursues a libertine life of varied amoral experiences while staying young and beautiful; all the while, his portrait ages and records every sin.


== Plot ==

The Picture of Dorian Gray begins on a beautiful summer day in Victorian England, where Lord Henry Wotton, an opinionated man, is observing the sensitive artist Basil Hallward painting the portrait of Dorian Gray, a handsome young man who is Basil's ultimate muse. While sitting for the painting, Dorian listens to Lord Henry espousing his hedonistic world view and begins to think that beauty is the only aspect of life worth pursuing, prompting Dorian to wish that his portrait would age instead of himself.
Under Lord Henry's hedonistic influence, Dorian fully explores his sensuality. He discovers the actress Sibyl Vane, who performs Shakespeare plays in a dingy, working-class theatre. Dorian approaches and courts her, and soon proposes marriage. The enamoured Sibyl calls him "Prince Charming", and swoons with the happiness of being loved, but her protective brother, James, warns that if "Prince Charming" harms her, he will murder him.
Dorian invites Basil and Lord Henry to see Sibyl perform in Romeo and Juliet. Sibyl, too enamoured with Dorian to act, performs poorly, which makes both Basil and Lord Henry think Dorian has fallen in love with Sibyl because of her beauty instead of her acting talent. Embarrassed, Dorian rejects Sibyl, telling her that acting was her beauty; without that, she no longer interests him. On returning home, Dorian notices that the portrait has changed; his wish has come true, and the man in the portrait bears a subtle sneer of cruelty.
Conscience-stricken and lonely, Dorian decides to reconcile with Sibyl, but he is too late, as Lord Henry informs him that Sibyl has killed herself. Dorian then understands that, where his life is headed, lust and beauty shall suffice. Dorian locks the portrait up, and over the following eighteen years, he experiments with every vice, influenced by a morally poisonous French novel that Lord Henry Wotton gave him.
One night, before leaving for Paris, Basil goes to Dorian's house to ask him about rumours of his self-indulgent sensualism. Dorian does not deny his debauchery, and takes Basil to see the portrait. The portrait has become so hideous that Basil is only able to identify it as his by the signature he affixes to all of his portraits. Basil is horrified, and beseeches Dorian to pray for salvation. In anger, Dorian blames his fate on Basil and stabs him to death. Dorian then calmly blackmails an old friend, the scientist Alan Campbell, into using his knowledge of chemistry to destroy the body of Basil Hallward. Alan later kills himself.

To escape the guilt of his crime, Dorian goes to an opium den, where James Vane is unknowingly present. James had been seeking vengeance upon Dorian ever since Sibyl killed herself, but had no leads to pursue as the only thing he knew about Dorian was the name Sibyl called him, "Prince Charming". In the opium den, however, he hears someone refer to Dorian as "Prince Charming", and he accosts Dorian. Dorian deceives James into believing that he is too young to have known Sibyl, who killed herself eighteen years earlier, as his face is still that of a young man. James relents and releases Dorian, but is then approached by a woman from the opium den who reproaches James for not killing Dorian. She confirms that the man was Dorian Gray and explains that he has not aged in eighteen years. James runs after Dorian, but he has gone.
James then begins to stalk Dorian, causing Dorian to fear for his life. However, during a shooting party, a hunter accidentally kills James Vane, who was lurking in a thicket. On returning to London, Dorian tells Lord Henry that he will live righteously from now on. His new probity begins with deliberately not breaking the heart of the naïve Hetty Merton, his current romantic interest. Dorian wonders if his newly-found goodness has rescinded the corruption in the picture but when he looks at it, he sees only an even uglier image of himself. From that, Dorian understands that his true motives for the self-sacrifice of moral reformation were the vanity and curiosity of his quest for new experiences, along with the desire to restore beauty to the picture.
Deciding that only full confession will absolve him of wrongdoing, Dorian decides to destroy the last vestige of his conscience and the only piece of evidence remaining of his crimes; the picture. In a rage, he takes the knife with which he murdered Basil Hallward and stabs the picture. The servants of the house awaken on hearing a cry from the locked room; on the street, a passerby who also heard the cry calls the police. On entering the locked room, the servants find an unknown old man stabbed in the heart, his figure withered and decrepit. The servants identify the disfigured corpse by the rings on its fingers, which belonged to Dorian Gray. Beside him, the portrait is now restored to its former appearance of beauty.


== Characters ==
Oscar Wilde said that, in the novel The Picture of Dorian Gray (1891), three of the characters were reflections of himself:

Basil Hallward is what I think I am; Lord Henry is what the world thinks of me; Dorian is what I would like to be—in other ages, perhaps.

The characters of the story areDorian Gray – a handsome, narcissistic young man enthralled by Lord Henry's "new" hedonism. He indulges in every pleasure and virtually every 'sin', studying its effect upon him, which eventually leads to his death. It was purported that Wilde's inspiration for the character was the poet John Gray, but Gray distanced himself from the rumour.
Basil Hallward – a deeply moral man, the painter of the portrait, and infatuated with Dorian, whose patronage realises his potential as an artist. The picture of Dorian Gray is Basil's masterpiece. The character is supposed to have been formed after painter Charles Haslewood Shannon.
Lord Henry "Harry" Wotton – an imperious aristocrat and a decadent dandy who espouses a philosophy of self-indulgent hedonism. Initially Basil's friend, he neglects him for Dorian's beauty. The character of witty Lord Harry is a critique of Victorian culture at the Fin de siècle – of Britain at the end of the 19th century. Lord Harry's libertine world view corrupts Dorian, who then successfully emulates him. To the aristocrat Harry, the observant artist Basil says, "You never say a moral thing, and you never do a wrong thing."  Lord Henry takes pleasure in impressing, influencing, and even misleading his acquaintances (to which purpose he bends his considerable wit and eloquence) but appears not to observe his own hedonistic advice, preferring to study himself with scientific detachment.  His distinguishing feature is total indifference to the consequences of his actions. Scholars generally accept the character is partly inspired by Wilde's friend Lord Ronald Gower.
Sibyl Vane – a talented actress and singer, she is a beautiful girl from a poor family with whom Dorian falls in love. Her love for Dorian ruins her acting ability, because she no longer finds pleasure in portraying fictional love as she is now experiencing real love in her life. She kills herself on learning that Dorian no longer loves her; at that, Lord Henry likens her to Ophelia, in Hamlet.
James Vane – Sibyl's brother, a sailor who leaves for Australia. He is very protective of his sister, especially as their mother cares only for Dorian's money. Believing that Dorian means to harm Sibyl, James hesitates to leave, and promises vengeance upon Dorian if any harm befalls her. After Sibyl's suicide, James becomes obsessed with killing Dorian, and stalks him, but a hunter accidentally kills James. The brother's pursuit of vengeance upon the lover (Dorian Gray), for the death of the sister (Sibyl) parallels that of Laertes vengeance against Prince Hamlet.
Alan Campbell – chemist and one-time friend of Dorian who ended their friendship when Dorian's libertine reputation devalued such a friendship. Dorian blackmails Alan into destroying the body of the murdered Basil Hallward; Campbell later shoots himself dead.
Lord Fermor – Lord Henry's uncle, who tells his nephew, Lord Henry Wotton, about the family lineage of Dorian Gray.
Adrian Singleton – A youthful friend of Dorian's, whom he evidently introduced to opium addiction, which induced him to forge a cheque and made him a total outcast from his family and social set.
Victoria, Lady Henry Wotton – Lord Henry's wife, whom he treats disdainfully; she later divorces him.


== Allusions ==


=== Faust ===
About the literary hero, the author, Oscar Wilde, said, "in every first novel the hero is the author as Christ or Faust." As in the legend of Faust, in The Picture of Dorian Gray a temptation (ageless beauty) is placed before the protagonist, which he indulges. In each story, the protagonist entices a beautiful woman to love him, and then destroys her life. In the preface to the novel (1891), Wilde said that the notion behind the tale is "old in the history of literature", but was a thematic subject to which he had "given a new form".Unlike the academic Faust, the gentleman Dorian makes no deal with the Devil, who is represented by the cynical hedonist Lord Henry, who presents the temptation that will corrupt the virtue and innocence that Dorian possesses at the start of the story. Throughout, Lord Henry appears unaware of the effect of his actions upon the young man; and so frivolously advises Dorian, that "the only way to get rid of a temptation is to yield to it. Resist it, and your soul grows sick with longing." As such, the devilish Lord Henry is "leading Dorian into an unholy pact, by manipulating his innocence and insecurity."


=== Shakespeare ===
In the preface to The Picture of Dorian Gray (1891), Wilde speaks of the sub-human Caliban character from The Tempest. In chapter five, he writes: "He felt as if he had come to look for Miranda and had been met by Caliban". When Dorian tells Lord Henry about his new love Sibyl Vane, he mentions the Shakespeare plays in which she has acted, and refers to her by the name of the heroine of each play. Later, Dorian speaks of his life by quoting Hamlet, a privileged character who impels his potential suitor (Ophelia) to suicide, and prompts her brother (Laertes) to swear mortal revenge.


=== Joris-Karl Huysmans ===
The anonymous "poisonous French novel" that leads Dorian to his fall is a thematic variant of À rebours (1884), by Joris-Karl Huysmans. In the biography Oscar Wilde (1989), the literary critic Richard Ellmann said: Wilde does not name the book, but at his trial he conceded that it was, or almost [was], Huysmans's À rebours ... to a correspondent, he wrote that he had played a "fantastic variation" upon À rebours, and someday must write it down. The references in Dorian Gray to specific chapters are deliberately inaccurate.


== Literary significance ==


=== Possible Disraeli influence ===
Some commentators have suggested that The Picture of Dorian Gray was influenced by the British Prime Minister Benjamin Disraeli's (anonymously published) first novel Vivian Grey (1826) as, "a kind of homage from one outsider to another." The name of Dorian Gray's love interest, Sibyl Vane, may be a modified fusion of the title of Disraeli's best known novel (Sybil) and Vivian Grey's love interest Violet Fane, who, like Sibyl Vane, dies tragically. There is also a scene in Vivian Grey in which the eyes in the portrait of a "beautiful being" move when its subject dies.


=== Publication history ===

The Picture of Dorian Gray originally was a novella submitted to Lippincott's Monthly Magazine for serial publication. In 1889, J. M. Stoddart, an editor for Lippincott, was in London to solicit novellas to publish in the magazine. On 30 August 1889, Stoddart dined with Oscar Wilde, Sir Arthur Conan Doyle and T. P. Gill at the Langham Hotel, and commissioned novellas from each writer. Conan Doyle promptly submitted The Sign of the Four (1890) to Stoddart, but Wilde was more dilatory; Conan Doyle's second Sherlock Holmes novel was published in the February 1890 edition of Lippincott's Monthly Magazine, yet Stoddart did not receive Wilde's manuscript for The Picture of Dorian Gray until 7 April 1890, nine months after having commissioned the novel from him.The literary merits of The Picture of Dorian Gray impressed Stoddart, but, as an editor, he told the publisher, George Lippincott, "in its present condition there are a number of things an innocent woman would make an exception to. ..." Among the pre-publication deletions that Stoddart and his editors made to the text of Wilde's original manuscript were: (i) passages alluding to homosexuality and to homosexual desire; (ii) all references to the fictional book title Le Secret de Raoul and its author, Catulle Sarrazin; and (iii) all "mistress" references to Gray's lovers, Sibyl Vane and Hetty Merton.

The Picture of Dorian Gray was published on 20 June 1890, in the July issue of Lippincott's Monthly Magazine. British reviewers condemned the novel's immorality, causing such controversy that retailing chain W H Smith withdrew every copy of the July 1890 issue of Lippincott's Monthly Magazine from its bookstalls in railway stations.
Consequent to the harsh criticism of the 1890 magazine edition, Wilde ameliorated the homoerotic references, to simplify the moral message of the story. In the magazine edition (1890), Basil tells Lord Henry how he "worships" Dorian, and begs him not to "take away the one person that makes my life absolutely lovely to me." In the magazine edition, Basil focuses upon love, whereas, in the book edition (1891), he focuses upon his art, saying to Lord Henry, "the one person who gives my art whatever charm it may possess: my life as an artist depends on him."
The magazine edition of The Picture of Dorian Gray (1890) was expanded from thirteen to twenty chapters; the final chapter being divided into two,  which became the nineteenth and twentieth chapters in the book edition, The Picture of Dorian Gray (1891). Wilde's textual additions were about the "fleshing out of Dorian as a character" and providing details of his ancestry that made his "psychological collapse more prolonged and more convincing."The introduction of the James Vane character to the story develops the socio-economic background of the Sibyl Vane character, thus emphasising Dorian's selfishness and foreshadowing James's accurate perception of the essentially immoral character of Dorian Gray; thus, he correctly deduced Dorian's dishonourable intent towards Sibyl. The sub-plot about James Vane's dislike of Dorian gives the novel a Victorian tinge of class struggle. With such textual changes, Oscar Wilde meant to diminish the moralistic controversy about the novel The Picture of Dorian Gray.


=== Preface ===
Consequent to the harsh criticism of the magazine edition of the novel, the textual revisions to The Picture of Dorian Gray included a preface in which Wilde addressed the criticisms and defended the reputation of his novel. To communicate how the novel should be read, in the preface, Wilde explains the role of the artist in society, the purpose of art, and the value of beauty. It traces Wilde's cultural exposure to Taoism and to the philosophy of Chuang Tsǔ (Zhuang Zhou). Earlier, before writing the preface, Wilde had written a book review of Herbert Giles's translation of the work of Zhuang Zhou. The preface was first published in the 1891 edition of the novel; nonetheless, by June 1891, Wilde was defending The Picture of Dorian Gray against accusations that it was a bad book.In the essay The Artist as Critic, Oscar Wilde said:

The honest ratepayer and his healthy family have no doubt often mocked at the dome-like forehead of the philosopher, and laughed over the strange perspective of the landscape that lies beneath him. If they really knew who he was, they would tremble. For Chuang Tsǔ spent his life in preaching the great creed of Inaction, and in pointing out the uselessness of all things.


=== Criticism ===
In the 19th century, the critical reception of the novel The Picture of Dorian Gray (1890) was poor. The book critic of The Irish Times said, The Picture of Dorian Gray was "first published to some scandal." Such book reviews achieved for the novel a "certain notoriety for being 'mawkish and nauseous', 'unclean', 'effeminate' and 'contaminating'." Such moralistic scandal arose from the novel's homoeroticism, which offended the sensibilities (social, literary, and aesthetic) of Victorian book critics. Most of the criticism was, however, personal, attacking Wilde for being a hedonist with values that deviated from the conventionally accepted morality of Victorian Britain. In the 30 June 1890 issue of the Daily Chronicle, the book critic said that Wilde's novel contains "one element ... which will taint every young mind that comes in contact with it." In the 5 July 1890 issue of the Scots Observer, a reviewer asked "Why must Oscar Wilde 'go grubbing in muck-heaps?'" In response to such criticism, Wilde obscured the homoeroticism of the story and expanded the personal background of the characters.


=== Textual revisions ===
After the initial publication of the magazine edition of The Picture of Dorian Gray (1890), Wilde expanded the text from 13 to 20 chapters and obscured the homoerotic themes of the story. In the novel version of The Picture of Dorian Gray (1891), chapters 3, 5, and 15 to 18, inclusive, are new; and chapter 13 of the magazine edition was divided, and became chapters 19 and 20 of the novel edition. In 1895, at his trials, Oscar Wilde said he revised the text of The Picture of Dorian Gray because of letters sent to him by the cultural critic Walter Pater.Passages revised for the novel

(Basil about Dorian) "He has stood as Paris in dainty armour, and as Adonis with huntsman's cloak and polished boar-spear. Crowned with heavy lotus-blossoms, he has sat on the prow of Adrian's barge, looking into the green, turbid Nile. He has leaned over the still pool of some Greek woodland, and seen in the water's silent silver the wonder of his own beauty."
(Lord Henry describes "fidelity") "It has nothing to do with our own will. It is either an unfortunate accident, or an unpleasant result of temperament."
"You don't mean to say that Basil has got any passion or any romance in him?" / "I don't know whether he has any passion, but he certainly has romance," said Lord Henry, with an amused look in his eyes. / "Has he never let you know that?" / "Never. I must ask him about it. I am rather surprised to hear it."
(Basil Hallward described) "Rugged and straightforward as he was, there was something in his nature that was purely feminine in its tenderness."
(Basil to Dorian) "It is quite true that I have worshipped you with far more romance of feeling than a man usually gives to a friend. Somehow, I had never loved a woman. I suppose I never had time. Perhaps, as Harry says, a really grande passion is the privilege of those who have nothing to do, and that is the use of the idle classes in a country."
(Basil confronts Dorian) "Dorian, Dorian, your reputation is infamous. I know you and Harry are great friends. I say nothing about that now, but surely you need not have made his sister's name a by-word." (The first part of this passage was deleted from the 1890 magazine text; the second part of the passage was inserted to the 1891 novel text.)Passages added to the novel

"Each class would have preached the importance of those virtues, for whose exercise there was no necessity in their own lives. The rich would have spoken on the value of thrift, and the idle grown eloquent over the dignity of labour."
"A grande passion is the privilege of people who have nothing to do. That is the one use of the idle classes of a country. Don't be afraid."
"Faithfulness! I must analyse it some day. The passion for property is in it. There are many things that we would throw away, if we were not afraid that others might pick them up."The uncensored edition
In 2011, the Belknap Press published The Picture of Dorian Gray: An Annotated, Uncensored Edition. The edition includes text that was deleted by J. M. Stoddart, the story's initial editor, before its publication in Lippincott's Monthly Magazine in 1890.


== Adaptations ==


== Bibliography ==


=== Editions ===
The Picture of Dorian Gray (Oxford: Oxford World's Classics, 2008) ISBN 9780199535989. Edited with an introduction and notes by Joseph Bristow. Based on the 1891 book edition.
The Uncensored Picture of Dorian Gray (Belknap Press, 2011) ISBN 9780674066311. Edited with an introduction by Nicholas Frankel. This edition presents the uncensored typescript of the 1890 Lippincott edition.
The Picture of Dorian Gray (New York: Norton Critical Editions, 2006) ISBN 9780393927542. Edited with an introduction and notes by Michael Patrick Gillespie. Presents the 1890 Lippincott edition and the 1891 book edition side by side.
The Picture of Dorian Gray (Harmondsworth: Penguin Classics, 2006), ISBN 9780141442037. Edited with an introduction and notes by Robert Mighall. Included as an appendix is Peter Ackroyd's introduction to the 1986 Penguin Classics edition. It reproduces the 1891 book edition.
The Picture of Dorian Gray (Broadview Press, 1998) ISBN 978-1-55111-126-1. Edited with an introduction and notes by Norman Page. Based on the 1891 book edition.


== See also ==

Adaptations of The Picture of Dorian Gray
Dorian Gray (character)
Dorian Gray syndrome
The Happy Hypocrite – a thematic inversion of The Picture of Dorian Gray


== References ==


== External links ==
Replica of the 1890 Edition & Critical Edition at University of Victoria
The Picture of Dorian Gray to read on line on bibliomania site.
The Picture of Dorian Gray Audiobook on YouTube

 The Picture of Dorian Gray (13-chapter version) at Project Gutenberg

 The Picture of Dorian Gray (20-chapter version) at Project Gutenberg
The Picture of Dorian Gray at the Standard Ebooks site.
 The Picture of Dorian Gray public domain audiobook at LibriVox
The Picture of Dorian Gray title listing at the Internet Speculative Fiction Database