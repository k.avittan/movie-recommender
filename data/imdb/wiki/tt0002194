For Honor is a 2017 action video game developed and published by Ubisoft for Microsoft Windows, PlayStation 4, and Xbox One. The game allows players to play the roles of historical forms of soldiers and warriors, including knights, samurai, vikings and since October 2018 the Chinese Wu Lin, within a medieval setting, controlled using a third-person perspective. The game was developed primarily by Ubisoft Montreal and released worldwide in 2017.


== Gameplay ==

For Honor is an action fighting game set during a medieval, fantasy setting.  Players can play as a character from three different factions, namely the Iron Legion (Knights), the Warborn (Vikings), and the Dawn Empire (Samurai). A fourth faction, the Wu Lin, was added in the Marching Fire expansion in October 2018. The four factions represent knights, vikings, samurai, and Chinese warriors respectively. Each hero also has quotes in their own languages that will trigger when certain actions are performed. The Knights speak in Latin, the Vikings speak Icelandic, the Samurai speak Japanese, and the Wu Lin speak Mandarin. Each faction had four classes at launch, with two more being added at the beginning of every season of the Faction War. The Vanguard class is described as "well-balanced" and has excellent offense and defense. The Assassin class is fast and efficient in dueling enemies, but the class deals much less damage to multiple enemies. The Heavies (Tanks) are more resistant to damages and are suitable for holding capture points, though their attacks are slow. The last class, known as "Hybrid", is a combination of two of the three types, and is capable of using uncommon skills.
All heroes are unique and have their own weapons, skills, and fighting styles. Players fight against their opponents with their class-specific melee weapons. When players perform certain actions, such as killing multiple enemies consecutively, they gain Feats, which are additional perks. These perks allow players to gain additional points and strengths, call in a barrage of arrows or a catapult attack, or heal themselves. In most missions, players are accompanied by numerous AI minions. They are significantly weaker than the player character, and do not pose much threat.
A tactical combat system, known as "Art of Battle", is initiated when the player encounters other players or player-like AI in the multiplayer or higher health AI in the campaign. Players enter a dueling mode with them wherein players aim at their opponent with their weapon. Players then can choose how to place and position their weapons from three directions (from above, the right, and the left) when they are attacking their enemies. By observing on-screen hints and the movements of their opponents, which reflect their respective attack position, players are able to choose the correct position to block the other players' attacks. Players also have other special abilities, which vary depending on the character they choose, such as barging into enemies with their own shoulders and performing back-stepping swipes. The strength of each attack can also be decided by players. The system aims at allowing players to "feel the weight of the weapon in [their] hand".


=== Multiplayer ===
Similar to the single-player campaign, the multiplayer modes feature perks, AI minions, and the Art of Battle system. As the competitive multiplayer modes feature a structure similar to that of shooters, the creative director of the game called For Honor a "shooter with swords". Friendly fire is also featured in the game. Players can cause damage to their own teammates if they accidentally or intentionally hit them with their blades. The multiplayer aspect also allows players to customize their characters. For instance, the armor that the characters wear can be changed and modified. There are seven game modes: There is also a ranked duel game mode that is currently in beta:
Dominion: Dominion is a four-versus-four multiplayer mode in which players must capture and hold multiple zones in a battlefield. Points are earned through occupying the zones and killing enemy minions that fight at point B. Players earn double points for staying on points A and C. When one team earns 1000 points, the other team starts to ‘break’ meaning each player on that team cannot respawn unless revived by another teammate. Once one of the teams are breaking the opposing team must eliminate all of their players to secure victory.
Brawl: In this two-versus-two multiplayer mode, one duo must eliminate the other completely in order to win.
Duel: Duel is a one-versus-one multiplayer mode in which a player must successfully kill the opponent 3 times in order to win.
Ranked Duel: Ranked duel is a one-versus-one multiplayer mode in which players start in a qualifying stage, where they will have to complete 15 matches before they are placed into one of five rank tiers, Bronze, Silver, Gold, Platinum, and Diamond. Players placement depends on how many wins or losses they receive in the 20 qualifying matches. After players are placed within their respective rank tier, players will be pitted against other players within a similar rank tier.
Skirmish: Skirmish is a four-versus-four multiplayer mode in which players gain points while killing enemies. When one team earns enough points, they must eliminate the players from the other team and win the match.
Elimination: A team of players must eliminate the entire team of opponent players in this four-versus-four multiplayer mode. The team that still has remaining warriors will automatically win the match.
Tribute: A four-versus-four multiplayer mode where teams attempt to steal offerings and place them on their shrine. Each of the three offering gives the team a special power-up. The team to capture all three and defend them until the timer ends wins or the team with the most offerings at the end of the battle timer wins.
Breach: A four-versus-four multiplayer mode where the attacker’s goal is to kill the Commander while the defenders must successfully stop the attackers. The attackers must complete a series of objectives such as leading the battering ram to each of the two gates, breaking them both down and, ultimately, slaying the Commander; on the contrary, the defenders must prevent the attackers from completing any objectives.


=== The Faction War ===
Each online multiplayer match awards War Assets based on the outcome and the player's performance. These War Assets are then deployed in the Faction War – which stretches across all platforms – where they are used either to defend an allied territory or conquer a neighbouring one occupied by an enemy faction, with the most war assets deployed in a given territory determining the victor. Territories controlled are updated every six hours, while rounds last for two weeks and seasons last for ten weeks (five rounds). As the war progresses and territories change, the changing front will determine which maps that are played and their appearance (each map has variants depending on whether it is under Samurai, Knight or Viking control.) Players who have distinguished themselves and helped their faction gain and defend ground earn higher quality equipment as spoils of war after each round and each season. After a season ends, the map is reset and a new season begins after an off-season period, but the outcome of the previous season impacts the story background of the new season.


=== Heroes ===
There are currently four factions in For Honor: Knights, Vikings, and Samurai, with a fourth faction, the Wu Lin, having been added with the Marching Fire expansion. There are eight combatants in the Knight, seven in the Viking, and Samurai, and five in Wu Lin, which makes a total of twenty-seven heroes, each wielding their own unique weapon from history.
Knight faction features:

Warden, a Vanguard class knight wielding a longsword, an expert at keeping close distance and tackling the enemy;
Conqueror, a Heavy class warrior wielding a flail and using a heater shield to fend off attacks;
Peacekeeper, an Assassin equipped with a combination of shortsword and dagger, relying on quick attacks and bleeding out the opponent;
Lawbringer, an armor-clad Hybrid fighter using a poleaxe and combining shoves and throws to control the flow of the fight;
Centurion, a Roman-themed Hybrid warrior brandishing a gladius who is a fierce hand-to-hand combatant;
Gladiator, an Assassin class fighter reminiscent of retiarii of ancient Rome with a trident and a buckler at their disposal;
Black Prior, a Heavy class knight wielding a broadsword who is a master at using a kite shield in both defense and offense.
Warmonger, a Vanguard class warrior wielding a flamberge who once belonged to the knights until they were chosen by the god Horkos to finish the mission of Apollyon.Viking faction features:

Raider, a Vanguard class warrior fighting with a dane axe, making use of his heavy weapon to perform powerful strikes;
Warlord, a Heavy class warrior equipped with viking sword and performing quick attacks from behind his viking shield;
Berserker, an Assassin with hand axes in both of his hands, which, while sacrificing defense, allow him to perform rapid strikes;
Valkyrie, a Hybrid class fighter using spear and shield in a versatile fighting technique, constantly pushing at the enemy;
Highlander, a Hybrid stylised as a Scottish warrior with a claymore who switches between defensive and offensive stances;
Shaman, an Assassin class fighter with a feral fighting style utilizing a hatchet and a gutting knife;
Jormungandr, a Heavy class warrior using a powerful war hammer to brutally assault the opponents.Samurai faction features:

Kensei, a Vanguard class warrior wielding a nodachi in a steady, balanced stance;
Shugoki, a Heavy class combatant using his weight and a kanabo to crush attacking enemies;
Orochi, a katana-wielding Assassin who makes quick work of his opponents with a flurry of chained blows;
Nobushi, a Hybrid class fighter fighting with a naginata, who is an expert at keeping enemies at distance;
Shinobi, an Assassin class fighter with kusarigama in each hand, using elusive moves to always keep the enemy guessing;
Aramusha, a Hybrid class warrior with two katanas and a fast-paced, uncommon fighting technique;
Hitokiri, a Heavy class warrior and a former executioner wielding a masakari.Wu Lin faction features:

Tiandi, a Vanguard class warrior who wields a da-dao and specializes in swift combat based on dodges;
Jiang Jun, a Heavy class fighter with a guandao at his disposal, who is able to keep multiple enemies at bay;
Nuxia, an Assassin wielding dual hook swords to execute fast and skillful strikes while simultaneously dodging the opponent;
Shaolin, a Hybrid class fighter who relies on bō and martial arts expertise to overcome the enemy;
Zhanhu, a Hybrid class warrior whose dodges and masterful usage of changdao provide a worthy challenge.


== Plot ==


=== Setting ===
After a natural catastrophe pitted the most fearsome warriors against one another in a fight for resources and territory, the bloodthirsty warlord Apollyon believes the people of the Knights, Vikings, and Samurai have grown weak and want to create an age of all-out war through manipulation of each faction. To this end the perspectives of characters within each faction are shown as events are shaped, battles are waged, and agendas are created as Apollyon works to ensure continuous sparks of conflict between the Legion, the Warborn, and the Chosen from the Myre. With a later DLC, the Wu-Lin, based on Chinese culture, were added, while in-game lore links the Romans fighting for the Legion to a fifth Roman-based faction not present in the game.


=== Story ===
The warlord Apollyon takes control of the knights of the Blackstone Legion after murdering her rivals, who fight for the people of the land of Ashfeld, allowing her to sow the seeds of perpetual war and create stronger men to rule over the weak. During the Blackstone Legion's attempt to bring a dishonorable lord-turned-mercenary, Hervis Daubeny, to justice, his second-in-command, known as the Warden, helps to stop the Blackstone siege and battles the champion of the Blackstone knights. Upon defeating a Blackstone Legion captain, Ademar, the Warden is made a knight of the Legion by Holden Cross, Apollyon's lieutenant, and leaves with him. During his/her time in Apollyon's army, the Warden helps to defend against the Viking raiders of the Warborn, but soon realizes shortly after meeting with Apollyon that she cares nothing about protecting people and seeks to manipulate her enemies into endless battles. Starting with the Vikings, Apollyon and her warriors including Cross, the Warden and fellow lieutenants Stone and Mercy, attack their settlements and sack their strongholds in the northern land of Valkenheim, leaving only enough food and supplies to fight over, and sparing those who would eagerly fight for those scraps or are strong enough to do so.
Afterwards, in Valkenheim, the Viking clans fight among themselves, killing one another for the dwindling scraps left by Apollyon. This continues until a powerful warrior known as Raider comes down from the mountains, and begins uniting the warriors of the various clans under the Warborn banner, alongside Warlord friend Stigandr, Valkyrie warrior Runa and Berserker Helvar, first by killing the brutal raider Ragnar, who steals what little remains from those who cannot feed themselves, and then Siv the Ruthless, who seeks to conquer and plunder their own people. After killing their rivals, Raider's rapidly growing army retake a Warborn stronghold from knights of Apollyon's army, and then set out to the land of the Myre to raid the Dawn Empire of the Chosen, a group of powerful Samurai, to resupply and feed their people. The Raider then leads the assault on the Samurai, kills the Samurai General, Tozen, and causes the Samurai to retreat back to their greatest city. In the chaos, Apollyon kills the Dawn Empire's ruler and his daimyōs that refuse to fight.
Into this chaos is brought the Orochi warrior known as the Emperor's champion, the strongest and most fearsome warrior in the Dawn Empire. The champion was imprisoned for speaking out of turn and was freed during the chaos of the Viking raid. The Orochi helps to push back the Vikings, but fails to prevent Apollyon from riding through the chaos and murdering the Imperial family, forcing the Daimyos to fight one another for control of the Dawn Empire. After learning of the devastation the Viking raid caused, she/he, fellow samurai Ayu, the Shugoki Okuma and Nobushi Momiji attempt to reunite the Daimyos under one banner, using Apollyon as a common enemy to rally against. The Emperor's Champion infiltrates the Emperor's palace with Momiji and confronts Seijuro, the Daimyo who took Apollyon's offer to become Emperor. After defeating Seijuro, the champion convinces him to join him against Apollyon. It is also during this time that the Emperor's Champion learns of Apollyon's manipulations of the various factions and rallies allies to stop Apollyon, invading Ashfeld to attack Blackstone Fortress. During a scouting mission with Momiji, the Orochi is met by the Warden, now leading a rebellion against Apollyon with Holden, Stone and Mercy by his/her side and, after dueling him/her, realizes they are allied against the same enemy. Both armies besiege the castle on separate fronts, with the Orochi searching for Apollyon. After finding Apollyon, the Orochi duels with and kills her, but not before learning that she wanted to create eternal war to weed out the weak and create the strongest of men, making them evermore bloodthirsty. Despite her death, Apollyon got what she wanted: an age of wolves.
In the aftermath, the armies of all three factions attacking the Blackstone Fortress; Knight, Samurai and Viking alike all turn on each other, resulting in a war lasting seven years. Realizing the war's futility The Warden, now leader of the Knights, sends Holden to meet with leaders of the other factions, Ayu and Stigandr. Though all three realize that the prospect of peace may be futile, they all agree that peace is worth fighting for and striving for it will make for an unforgettable tale.


== History ==

For Honor was developed by Ubisoft Montreal. Blue Byte developed the game's PC version. It was announced during Ubisoft's E3 2015 press conference. A CGI trailer and a gameplay demo were shown during the conference. Development of the game began in 2012. For Honor was the company's first attempt at developing a strategy-action game. The structure of the game is inspired by shooter games. The game was released worldwide for Microsoft Windows, PlayStation 4, and Xbox One on February 14, 2017. The game's original score was written and produced by film composers Danny Bensi, Saunder Jurriaans and Owen Wallis. A 20-track original soundtrack released alongside the game on February 14. On 27 July 2018, the game was announced to be joining the Xbox Games With Gold program.The documentary Playing Hard shows the development of the game, from an idea of Jason Vandenberghe up to release.
The game features a Dolby Atmos soundtrack.The Warden was later included as a playable DLC character in SNK's 2019 fighting game Samurai Shodown on June 24, 2020.


== Reception ==
For Honor received "generally favorable" reviews, according to video game review aggregator Metacritic.PC Gamer awarded it a score of 74/100, saying "A tense, tactical medieval brawler that will reward anyone with the patience and will to master it."Eurogamer ranked the game 25th on their list of the "Top 50 Games of 2017". The game won the People's Choice Award for "Best Fighting Game" in IGN's Best of 2017 Awards.


=== Sales ===
In Japan, For Honor debuted as the top-selling video game during its first week of release (February 13 to February 19, 2017), selling 40,062 copies, according to Media Create. In the U.S., it was the top-selling video game of February 2017, according to The NPD Group's tracking of retail and some digital sales. In the UK, it was the best-selling game during the week ending February 18, 2017, according to Chart-Track data, which excludes digital sales. The game ranked seventh worldwide in digital sales of console games during February 2017, according to SuperData Research's digital sales report, selling over 700,000 digital copies for all three platforms.


=== Accolades ===


== Notes ==


== References ==


== External links ==
Official website
For Honor at Ubisoft