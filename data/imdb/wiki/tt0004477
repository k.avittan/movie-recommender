A police dog is a dog that is specifically trained to assist police and other law-enforcement personnel. Their duties include: searching for drugs and explosives, locating missing people, finding crime scene evidence, and attacking people targeted by the police. Police dogs must remember several verbal cues and hand gestures. The most commonly used breeds are the German Shepherd, Belgian Malinois, Bloodhound, Dutch Shepherd, and the retriever breeds. Recently, the Belgian Malinois has become the dog of choice for police and military work due to their intense drive and focus. Malinois are smaller and more agile than German Shepherd Dogs, and have fewer health issues. However, a well-bred working line German Shepherd Dog is just as successful and robust as a Malinois.In many countries, the intentional injuring or killing of a police dog is a criminal offense.In English-speaking countries, police dog units are often referred to as K-9 or K9, a name which is a pun upon the word canine.


== History ==


=== Early history ===
Dogs have been used in law enforcement since the Middle Ages. Wealth and money was then tithed in the villages for the upkeep of the parish constable's bloodhounds that were used for hunting down outlaws. In France, dogs were used in the 14th century in St. Malo. Bloodhounds used in Scotland were known as "Slough dogs" – the word "Sleuth", (meaning detective) was derived from this.The rapid urbanization of London in the 19th century increased public concern regarding growing lawlessness – a problem that was far too great to be dealt with by the existing law enforcement of the time. As a result, private associations were formed to help combat crime. Night watchmen were employed to guard premises, and were provided with firearms and dogs to protect themselves from criminals.


=== Modern era ===

One of the first attempts to use dogs in policing was in 1889 by the Commissioner of the Metropolitan Police of London, Sir Charles Warren. Warren's repeated failures at identifying and apprehending the serial killer Jack the Ripper had earned him much vilification from the press, including being denounced for not using bloodhounds to track the killer. He soon had two bloodhounds trained for the performance of a simple tracking test from the scene of another of the killer's crimes. The results were far from satisfactory, with one of the hounds biting the Commissioner and both dogs later running off, requiring a police search to find them.It was in Continental Europe that dogs were first used on a large scale. Police in Paris began using dogs against roaming criminal gangs at night, but it was the police department in Ghent, Belgium that introduced the first organized police dog service program in 1899. These methods soon spread to Austria-Hungary and Germany; in the latter the first scientific developments in the field took place with experiments in dog breeding and training. The German police selected the German Shepherd Dog as the ideal breed for police work and opened up the first dog training school in 1920 in Greenheide. In later years, many Belgian Malinois dogs were added to the unit. The dogs were systematically trained in obedience to their officers and tracking and attacking criminals. 
In Britain, the North Eastern Railway Police were among the first to use police dogs in 1908 to put a stop to theft from the docks in Hull. By 1910, railway police forces were experimenting with other breeds such as Belgian Malinois, Labrador Retrievers, and German shepherds.


== Training ==

Training of police dogs is a very lengthy process since it begins with the training of the canine handler. The canine handlers go through a long process of training to ensure that they will train the dog to the best of its ability. First, the canine handler has to complete the requisite police academy training and one to two years of patrol experience before becoming eligible to transfer to a specialty canine unit. This is because the experience as an officer allows prospective canine officers to gain valuable experience in law enforcement. However, having dog knowledge and training outside of the police academy is considered to be an asset, this could be dog obedience, crowd control, communicating effectively with animals and being approachable and personable since having a dog will draw attention from surrounding citizens.
For a dog to be considered for a police department, it must first pass a basic obedience training course. They must be able to obey the commands of their handler without hesitation. This allows the officer to have complete control over how much force the dog should use against a suspect. Dogs trained in Europe are usually given commands in the country's native language. Dogs are initially trained with this language for basic behavior, so, it is easier for the officer to learn new words/commands, rather than retraining the dog to new commands. This is contrary to the popular belief that police dogs are trained in a different language so that a suspect cannot command the dog against the officer.Dogs used in law enforcement are trained to either be "single purpose" or "dual purpose". Single-purpose dogs are used primarily for backup, personal protection, and tracking. Dual-purpose dogs, however, are more typical. Dual-purpose dogs do everything that single-purpose dogs do, and also detect either explosives or narcotics. Dogs can only be trained for one or the other because the dog cannot communicate to the officer if it found explosives or narcotics. When a narcotics dog in the United States indicates to the officer that it found something, the officer has reasonable suspicion to search whatever the dog alerted on (i.e. bag or vehicle) without a warrant.In suspect apprehension, having a loud barking dog is helpful and can result in suspects surrendering without delay.


== Specialized police dogs ==
Apprehension and attack dogs – This dog is used to locate, apprehend, and sometimes subdue suspects.
Detection dogs – Trained to detect explosives or drugs such as marijuana, heroin, cocaine, crack cocaine, or methamphetamines. Some dogs are specifically trained to detect firearms and ammunition.
Dual purpose dog – Also known as a patrol dog, these dogs are trained and skilled in tracking, handler protection, off-leash obedience, criminal apprehension, and article, area and building search.
Search and rescue dogs (SAR) – This dog is used to locate suspects or find missing people or objects. Belgian Malinois, German Shepherds, Golden Retrievers, Labrador Retrievers, and Bloodhounds can all be used.


== Popular breeds ==
Dog breeds used by law enforcement include the Airedale terrier, Belgian Shepherd (Malinois), Bloodhound, Border Collie, Boxer, Doberman Pinscher, German shepherd, Labrador, Rottweiler, and Spaniel.


== Retirement ==
Police dogs are retired if they become injured to an extent where they will not recover completely, pregnant, or raising puppies, or are too old or sick to continue working. Since many dogs are raised in working environments for the first year of their life and retired before they become unable to perform, the working life of a dog is 6–9 years.However, when police dogs retire in some countries they may have the chance to receive a pension plan for their contribution. Police dogs in Nottinghamshire, England, now have the opportunity to retire with a form of security since their government forces now offer £805 over the span of three years to cover any additional medical costs. Not only do they now receive a pension plan but they also get to retire and reside with their original handler.If these dogs are killed in the line of duty they get the same honors as their human partners.


== Usage by country and region ==


=== Australia ===
The Australian Federal Police and other law enforcement agencies are known to employ K9s for security priorities such as airport duties.


=== Bangladesh ===
Border Guards Bangladesh, Rapid Action Battalion and the Dhaka Metropolitan Police maintain several dog squads to assist in anti-narcotic and anti-bombing campaigns.


=== Belgium ===
The Belgian Canine Support Group is part of the country's federal police. It has 35 dog teams, most of which are Belgian Malinois. Some dogs are trained to detect drugs, human remains, hormones or fire accelerants.  About a third are tracker dogs trained to find or identify living people.  These teams are often deployed to earthquake areas to locate people trapped in collapsed buildings.  The federal police's explosive detector dogs are attached to the Federal Police Special Units.


=== Canada ===

Canadians started using police dogs occasionally in 1908. However, they used privately owned dogs until 1935 when the Royal Canadian Mounted Police (RCMP) saw the value of police dogs and created the first team in 1937. By the 1950s, the RCMP had German Shepherds, Schnauzers, and Doberman Pinschers in service.Many Canadian municipalities use dog squads as a means of tracking suspects. Most municipalities in Canada employ the bite and hold technique rather than the bark and hold technique meaning once the dog is deployed, it bites the suspect until the dog handler commands it to release. This often results in serious puncture wounds and is traumatic for suspects. A dog has the legal status of property in Canada. As such, developing case law is moving towards absolute liability for the handlers of animals deliberately released to intentionally maim suspects. The dog is effectively a weapon.
In 2010, an Alberta Court of Queen's Bench judge stayed criminal charges against Kirk Steele, a man who was near-fatally shot by a police officer while he stabbed the officer's police dog. The judge found that the shooting was cruel and unusual treatment and excessive force.Police require reasonable suspicion they will recover evidence in order to use a dog to sniff a person or their possessions in public. This is because using a dog to detect scents is considered a search.  The main exemption to that rule are the dogs of the Canada Border Services Agency who are allowed to make searches without warrants under s.98 of the Customs Act.
In 2017, it was reported that the Canadian forces now have approximately 170 RCMP dog teams across Canada and it is continuing to grow as more and more Canadian municipalities are seeing the value of police dogs.


=== Denmark ===
There are a total of 240 active police dogs in Denmark, each of which are ranked in one of three groups: Group 1, Group 2 and Group 3. Dogs in Group 1 are very experienced, and highly trained. Group 1 dogs are typically within the age range of four to eight years old and are used for patrolling, rescue, searching for biological evidence and major crime investigations. Group 2 dogs are employed for the same tasks as members of Group 1, but they do not participate in major crime investigations or searching for biological evidence. Group 3 is the beginner rank for police dogs, and are only employed for patrol operations.


=== Hong Kong ===

The Police Dog Unit (PDU; Chinese: 警犬隊) was established in 1949 and is a specialist force of the Hong Kong Police under the direct command of the Special Operations Bureau. Their roles are crowd control, search and rescue, and poison and explosive detection. In addition, the PDU works in collaboration with other departments for anti-crime operations.


=== Netherlands ===
The Dutch Mounted Police and Police Dog Service (DLHP) is part of the Korps landelijke politiediensten (KLPD; National Police Services Agency) and supports other units with horse patrols and specially trained dogs. The DLHP's dogs are trained to recognize a single specific scent. They specialize in identifying scents (identifying the scent shared by an object and a person), narcotics, explosives and firearms, detecting human remains, locating drowning people and fire accelerants.
The KLPD is just one of the 26 police regions in the Netherlands. Every other region has its own canine unit. For example, the canine unit of the regional police Amsterdam-Amstelland has 24 patrol dog handlers and six special dog handlers and four instructors.  The unit has 24 patrol dogs, three explosives/firearms dogs, three active narcotic dogs, two passive narcotic dogs, two scent identifying dogs, one crime scene dog and one USAR dog. They work on a 24/7 basis, every shift (07:00–15:00/15:00-23:00/23:00-07:00 local time), has a minimum of 2 patrol dog handlers on patrol. The special dog handlers work only in the dayshift or after a call.


=== India ===
In India, the National Security Guard inducted the Belgian Malinois into its K-9 Unit, Border Security Force and Central Reserve Police Force use Rajapalayam as guard dogs to support the Force in the borders of Kashmir. 
For regional security, the Delhi Police has recruited many of the city's street dogs to be trained for security purposes. The Bengal Police uses German Shepherds, Labrador Retrievers, and the Indian pariah dog in its bomb-sniffing squad.


=== Israel ===
Israel utilizes canine units for border patrols to track illegal persons or objects that pose a threat. Police dogs serve in the Israel Police and Israel Prison Service.


=== Italy ===

All the law enforcement in Italy (Carabinieri, Polizia di Stato and Guardia di Finanza) have in service many patrol dogs for Public Order, Anti-Drug, Anti-explosive, Search and Rescue. The first train centers for police dogs in Italy were established after World War I and in 1924, Italy purchased German Shepherds from Germany for border patrol operations in the Alps. The Carabinieri Kennel Club was formed in 1957 to produce police dogs and train handlers in Italy. German and Belgian shepherds are used for multiple purposes, Labradors for drug, weapons and explosive surveillance and Rottweilers serve for protection.


=== Japan ===
Japan is one of the few Asian countries that have dogs serving in law enforcement as other Asian nations dislike dogs due to cultural norms. In ancient times, samurai had Akita service companions between the 16th and 19th centuries that would defend samurai while they slept at night. In modern times, the German shepherd is the common police dog of the Tokyo Metropolitan Police Department.


=== Kenya ===
Police dogs began their service in Kenya in 1948 as part of the Kenya Police Criminal Investigation Department of the Kenya Police. Since the 1950s, the main police dog in service is the German shepherd, with Labradors, Rottweilers and English Springer Spaniels being used for specialized purposes. Since the 2000s, the Kenya Police has increased the breeding and adoption of police dogs with the long-term goal of having canines serving in each police station of Kenya.


=== Nepal ===
The Nepal Police first established a canine unit in 1975 due to increased crime rates and to help with investigations. Since then, police dogs are in service throughout various regions of Nepal and have been present at the Tribhuvan International Airport since 2009.


=== Pakistan ===
Pakistan Customs uses K-9 Unit for anti-smuggling operations. Pakistan's Sindh Police also have a specialized K-9 unit.


=== Peru ===

Peru recruits various canine units for various governmental, military and police operations. The National Service of Agrarian Health (SENASA) of the Ministry of Agriculture and Irrigation has the Canine Brigade of Plant Health that detects plants that may violate phytosanitary trade practices and to prevent the contraband importation of pests in plants and fruit. The brigade is present at Jorge Chávez International Airport and in Peruvian territory.For the National Police of Peru, they prefer the German Shepherd, Belgian Shepherd Malinois, Beagle, Weimaraner, Golden Retriever and Labrador Retriever breeds for their service and accept donations of dogs between the ages of 12 and 24 months. The National Police use canine units for drug surveillance in the country's main airport, Jorge Chávez International Airport, with the force receiving canine training from United States Customs and Border Protection.The Peruvian Army has canine units trained for search and rescue as well as disaster situations. During the COVID-19 pandemic in Peru, a limitation of gatherings and curfew was enforced with the assistance of canine units that served for law enforcement.


=== Russia ===
Police dogs have been used in Russia since 1909 in Saint Petersburg. Attack dogs are used commonly by police and are muzzled at all times unless ordered to apprehend a suspect. Police dogs have also been used to track fugitives, which has remained common in most Soviet Union Successor States.


=== Sweden ===
The Swedish Police Authority currently deploys around 400 police canines. There is however no requirement for the dogs to be purebred, as long as they meet mental and physical requirements set by the police. Dogs aged 18–48 months are eligible to take admission tests for the K9 training. The police dogs live with their operators, and after retirement at age 8–10 the operator often assumes the ownership of the dog.


=== United Kingdom ===

Police forces across the country employ dogs and handlers and dog training schools are available to cater for the ever-increasing number of dogs being used. The use of police dogs became popular in the 1930s and in 1938, Scotland Yard officially added dogs to its police force.There are over 2,500 police dogs employed amongst the various police forces in the UK, with the Belgian Malinois as the most popular breed for general purpose work. In 2008, a Belgian Malinois female handled by PC Graham Clarke won the National Police Dog Trials with the highest score ever recorded.
All British police dogs, irrespective of the discipline they are trained in, must be licensed to work operationally. To obtain the license they have to pass a test at the completion of their training, and then again every year until they retire, which is usually at about the age of 8. The standards required to become operational are laid down by the Association of Chief Police Officers (ACPO) sub-committee on police dogs and are reviewed on a regular basis to ensure that training and licensing reflects the most appropriate methods and standards.


=== United States ===

Police dogs are in widespread use across the United States. K-9 units are operated on the federal, state, county, and local level and are utilized for a wide variety of duties, similar to those of other nations. Their duties generally include drug, bomb, and weapon detection and cadaver searches. The most common police dogs used for everyday duties are the German Shepherd and the Belgian Malinois though other breeds may be used to perform specific tasks.
On the federal level, police dogs are rarely seen by the general public, though they may be viewed in some airports assisting Transportation Security Administration officials search for explosives and weapons or by Customs and Border Protection searching for concealed narcotics and people. Some dogs may also be used by tactical components of such agencies as the Bureau of Alcohol, Tobacco, Firearms, and Explosives, the Federal Bureau of Investigation, and the United States Marshals Service.
Most police agencies in the United States – whether state, county, or local – use K-9s as a means of law enforcement. Often, even the smallest of departments operates a K-9 team of at least one dog, while the officers of more metropolitan cities can be more used to working with dozens. In the former case, police dogs usually serve all purposes deemed necessary, most commonly suspect apprehension and narcotics detection, and teams are often on call; in the latter case, however, individual dogs usually serve individual purposes in which each particular animal is specialized, and teams usually serve scheduled shifts. In both cases, police dogs are almost always cared for by their specific handlers. K-9s are not often seen by the public, though specialized police vehicles used for carrying dogs may be seen from time to time.

It is a felony to assault or kill a federal law enforcement animal, and it is a crime in most states to assault or kill a police animal. Yet despite common belief, police dogs are not treated as police officers for the purpose of the law, and attacking a police dog is not punishable in the same manner as attacking a police officer. Though many police departments formally swear dogs in as police officers, this swearing-in is purely honorary, and carries no legal significance.Police dogs also play a major role in American penal systems. Many jails and prisons will use special dog teams as a means of intervening in large-scale fights or riots by inmates. Also, many penal systems will employ dogs – usually bloodhounds – in searching for escaped prisoners.
At the federal level, police dogs play a vital role in homeland security. Federal law enforcement officials use the dogs to detect explosives or narcotics at major U.S. transportation hubs, such as airports. L. Paul Waggoner of the Canine Performance Sciences Program at Auburn University and an expert on police dogs told Homeland Preparedness News, "It is my perspective that detector dogs are a critical component of national security – and they also provide a very visible and proven deterrent to terrorist activities."In October 2017, the U.S. House Oversight and Government Reform Intergovernmental Affairs Subcommittee held a hearing about whether there is a sufficient supply of dogs that can be trained as police dogs. Congressman Mike Rogers (R-AL) said that the continued ISIS-inspired attacks in the U.S. and all over the world "have driven demand through the roof" for police dogs. During testimony at the subcommittee hearing, a representative from the American Kennel Club said that between 80–90 percent of dogs purchased by the U.S. Department of Homeland Security and U.S. Department of Defense come from foreign vendors, mostly located in Europe.


==== U.S. Supreme Court cases ====
The United States Supreme Court is the highest federal court in the United States of America. Some U.S. Supreme Court cases that pertain to police dogs are:

United States v. Place: The court determined that the sniffing of personal items of a person in a public place by a dog for the purpose of finding contraband was not considered a "search" under the Fourth Amendment.
City of Indianapolis v. Edmond: It is unconstitutional to set up a checkpoint to detect evidence of "ordinary criminal wrongdoing". This case was due to a checkpoint for drugs using police dogs to sniff cars.
United States v. Sharp: A canine sniff of the exterior of a vehicle is not a search under the Fourth Amendment, but if the dog enters the vehicle to sniff, it is a search. This case was ruled in favor of the officer because the dog jumped into the car, however, it was not encouraged by the officer therefore it was the dog's natural instinct to get closer to the scent.
Florida v. Harris – US Supreme Court case involving an officer's assertions on the training/reliability of his dog, and their sufficiency to establish probable cause
Florida v. Jardines – US Supreme Court case to determine whether a dog sniff at the front door of a home requires probable cause and a search warrant


== See also ==
Detection dog
Dogs in warfare
Nosework


== References ==


== External links ==
National Police Canine Association (US)
United States Police Canine Association
The North American Police Work Dog Association
Los Angeles County Police Canine Association US
Virginia Police Canine Association US
American Working Dog Association