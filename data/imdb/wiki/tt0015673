Chess Fever (Russian: Шахматная горячка, romanized: Shakhmatnaya goryachka) is a 1925 Soviet silent comedy film directed by Vsevolod Pudovkin and Nikolai Shpikovsky. Chess Fever is a comedy about the Moscow 1925 chess tournament, made by Pudovkin during the pause in the filming of Mechanics of the Brain. The film combines acted parts with actual footage from the tournament.


== Plot ==
In Moscow during the international chess tournament of 1925, the hero (Vladimir Fogel) and heroine (Anna Zemtsova) of the story are engaged to be married. Caught up in a society-wide chess fever, the hero forgets about his marital obligations and must beg for her forgiveness. As he kneels before his dismayed fiancée on a checkered cloth, the hero becomes distracted and starts to play chess. Enraged, the heroine throws his chess themed belongings out of the window and forces him to leave. Now separated, the heroine finds herself at a pharmacy, intending to obtain poison to kill herself. Meanwhile, the hero dejectedly sits on a bridge above a river, throwing what's left of his chess possessions into the water. Rather than throwing himself off the bridge as well, he realizes the importance of love and resolves to find the heroine and apologize. It is at this time that the heroine raises what she thinks is a vial of poison to her lips. However, she is stopped when she realizes that she was mistakenly given a chess piece by the distracted chemist. The heroine's distress is interrupted by World Chess Champion José Raúl Capablanca, who tells her that, in the company of a beautiful woman, he too hates chess. The two become friends and drive off as the hero arrives. The hero, with nothing left to do but return to chess, attends the tournament. Looking into the crowd, he is shocked to find his fiancée excitedly watching the game. He runs to her and the two embrace, united by their love for chess, and the film ends with them playing the game together.


== Cast ==
José Raúl Capablanca – the World Champion
Vladimir Fogel – the hero
Anna Zemtsova – the heroine
Natalya Glan
Zakhar Darevsky
Boris Barnet
Frank Marshall – himself (cameo)
Richard Réti – himself (cameo)
Carlos Torre Repetto – himself (cameo)
Frederick Yates – himself (cameo)
Ernst Grünfeld – himself (cameo)
Mikhail Zharov – house painter
Anatoli Ktorov – tram passenger
Yakov Protazanov – chemist
Yuli Raizman – chemist's assistant
Ivan Koval-Samborsky – policeman
Konstantin Eggert
Fyodor Otsep – game spectator (uncredited)
Sergei Komarov – grandfather (uncredited)


== Production ==


=== Development ===
Chess Fever is the directorial debut of Vsevolod Pudovkin, who had previously worked as a screenwriter, actor, and art director, and as an assistant to Lev Kuleshov. Pudovkin and Shpikovsky made this short silent comedy film in less than a month. It combines acted scenes with actual footage from the chess tournament occurring at this time and includes many cameos from Chess Champions and grandmasters. The film also features many Russian and Soviet film directors, such as Boris Barnet, Fedor Ozep, Yuli Raizman, and Yakov Protazanov.


== Cultural Influence ==
Chess Fever influenced author Vladimir Nabokov's 1930 novel The Luzhin Defence, published in the United States as The Defense. There are parallels within the two works, such as the main characters' inward and outward dispositions and the central love story present in both. The hero in Chess Fever is similar to Aleksandr Ivanovich Luzhin, the protagonist in Nabokov's novel; both are oblivious and idiosyncratic in nature, with a similar attire of checkered clothing and high degree of self-absorption. They are overcome with their obsession of chess and have difficulty merging their romantic relationships with their love for chess. Pudovkin's short film is the basis for Nabokov's novel, which went on to be made into a film.


== See also ==
The Three Million Trial


== Footnotes ==


== References ==
Leyda, Jay (1960), Kino: A History of the Russian and Soviet Film, New York: Macmillan, OCLC 1683826.


== External links ==
Chess Fever on IMDb
Chess Fever, version with English subtitles on YouTube
Chess Fever, part 1 on YouTube
Chess Fever, part 2 on YouTube