James Elliott "Willie" Williams (November 13, 1930 – October 13, 1999) was a Cherokee Indian and an honorary United States Navy chief boatswain's mate who was awarded the Medal of Honor during the Vietnam War. Boatswain's Mate First Class Williams was one of 32 Native Americans to receive the medal and is considered to be the most decorated enlisted man in the history of the US Navy.


== Personal life ==
Chief Williams was born in Fort Mill, South Carolina and moved two months later with his parents to Darlington, South Carolina where he spent his early childhood and youth. He attended the local schools and graduated from St. John's high school. In 1949, Williams married the former Elaine Weaver and they had five children (daughter, Debbie, sons, James E. "Jr.", Stephen Michael, Charles E., and daughter, Gail) and seven grandchildren.
In 1999, Williams died on the Navy's birthday, October 13, and was buried at the Florence National Cemetery in Florence, South Carolina.


== Navy service ==
Williams enlisted in the United States Navy on August 8, 1947, at the age of 16, and completed basic training at Naval Training Center San Diego. He served for almost twenty years, retiring on April 26, 1967 as a boatswain's mate first class. During those years, he served in both the Korean War and Vietnam War. On May 14, 1968, Williams was presented the Medal of Honor by President Lyndon B. Johnson during the dedication ceremony of the "Hall of Heroes" in the Pentagon. In 1977, he received the honorary title of chief boatswain's mate.During the Korean War, Williams served aboard the destroyer USS Douglas H. Fox from November 1950 to June 1952. He served off the coast of Korea where he was detached off the destroyer to take raiding parties into North Korea on small boats from March to June 1952.Petty Officer Williams served aboard USS Little Rock from June 1960 through April 1963, reenlisting aboard Little Rock in April 1962.In Vietnam April 1966, with the enlisted rank of petty officer first class and the rating of boatswain's mate 1st class (BM1), Williams was assigned in May to the River Patrol Force, River Squadron Five, in command of River Patrol Boat 105 (PBR-105). The force's  mission was to intercept Viet Cong and People's Army of Vietnam arms shipments, supplies, and personnel on the waterways of South Vietnam's Mekong Delta and to keep innocent boat traffic on the river and canals safe.On October 31, 1966, Williams was commanding PBR 105 alongside another PBR searching for Viet Cong guerrillas operating in an isolated area of the Mekong Delta. Suddenly, Viet Cong manning two sampans opened fire on the Americans. While Williams and his men neutralized one sampan, the other one escaped into a nearby canal. The PBRs gave chase and soon found themselves in a beehive of enemy activity as the VC opened fire on them with rocket propelled grenades and small arms from fortified river bank positions.
Williams repeatedly led the PBRs against concentrations of enemy junks and sampans. He also called for support from the heavily armed UH-1B Huey helicopters of HA(L)-3. When that help arrived, he kicked off another attack in the failing light. As a result of the three-hour battle, the U.S. naval force killed 1,000 Viet Cong guerrillas, destroyed over fifty vessels, and disrupted a major enemy logistic operation. For his actions on that date he was awarded the Medal of Honor.


== U.S. Marshals Service ==
After receiving an honorable discharge from the Navy, he worked for the Wackenhut Corporation. In 1969, he was appointed U. S. Marshal for the District of South Carolina, where he served until May 1977. He was then transferred to the Federal Law Enforcement Training Center, Glynco, Georgia, as an instructor and National Armorer. He was called back to South Carolina in July 1979 under court appointment as U. S. Marshal for South Carolina and served in that position until April 1980. He was then transferred to U. S. Marshal Service Headquarters, Washington, D. C., as Programs Manager, Health and Safety and In-District Training Officer where he served until his retirement from the U. S. Marshals Service with the grade of GS-1811-15.


== Military awards ==
Williams' decorations and awards include:

4 gold service stripes.


=== Medal of Honor citation ===
Rank and organization: Boatswain's Mate First Class (PO1c.), United States Navy, River Section 531, My Tho, RVN, Place and date: Mekong River, Republic of Vietnam, October 31, 1966. Entered service at: Columbia, S.C. Born: June 13, 1930, Rock Hill, S.C.
CITATION:

For conspicuous gallantry and intrepidity at the risk of his life above and beyond the call of duty. BM1 Williams was serving as Boat Captain and Patrol Officer aboard River Patrol Boat (PBR) 105 accompanied by another patrol boat when the patrol was suddenly taken under fire by 2 enemy sampans. BM1 Williams immediately ordered the fire returned, killing the crew of 1 enemy boat and causing the other sampan to take refuge in a nearby river inlet. Pursuing the fleeing sampan, the U.S. patrol encountered a heavy volume of small-arms fire from enemy forces, at close range, occupying well-concealed positions along the river bank. Maneuvering through this fire, the patrol confronted a numerically superior enemy force aboard 2 enemy junks and 8 sampans augmented by heavy automatic weapons fire from ashore. In the savage battle that ensued, BM1 Williams, with utter disregard for his safety exposed himself to the withering hail of enemy fire to direct counter-fire and inspire the actions of his patrol. Recognizing the overwhelming strength of the enemy force, BM1 Williams deployed his patrol to await the arrival of armed helicopters. In the course of his movement he discovered an even larger concentration of enemy boats. Not waiting for the arrival of the armed helicopters, he displayed great initiative and boldly led the patrol through the intense enemy fire and damaged or destroyed 50 enemy sampans and 7 junks. This phase of the action completed, and with the arrival of the armed helicopters, BM1 Williams directed the attack on the remaining enemy force. Now virtually dark, and although BM1 Williams was aware that his boats would become even better targets, he ordered the patrol boats' search lights turned on to better illuminate the area and moved the patrol perilously close to shore to press the attack. Despite a waning supply of ammunition the patrol successfully engaged the enemy ashore and completed the rout of the enemy force. Under the leadership of BM1 Williams, who demonstrated unusual professional skill and indomitable courage throughout the 3 hour battle, the patrol accounted for the destruction or loss of 65 enemy boats and inflicted numerous casualties on the enemy personnel. His extraordinary heroism and exemplary fighting spirit in the face of grave risks inspired the efforts of his men to defeat a larger enemy force, and are in keeping with the finest traditions of the U.S. Naval Service.
 |S| Lyndon B. Johnson 


=== Navy Cross citation ===
Citation:

The President of the United States of America takes pleasure in presenting the Navy Cross to Boatswain's Mate First Class James Elliott Williams (NSN: 9908934), United States Navy, for extraordinary heroism on 15 January 1967 while serving with River Section 531, River Squadron FIVE, Task Force 116 (TF-116), and friendly foreign forces during combat operations against communist insurgent (Viet Cong) forces on the Mekong River in the Republic of Vietnam. As Patrol Officer of a combat River Patrol Boat (PBR) patrol, Petty Officer Williams interdicted a major enemy supply movement across the Nam Thon branch of the Mekong River. He directed his units to the suspected crossing area, and was immediately taken under intense hostile fire from fortified positions and from along the river banks. After coordinating Vietnamese artillery support and U. S. Air Force air strikes, Petty Officer Williams courageously led his three PBR's back into the hazardous river to investigate and destroy the enemy sampans and supplies. Blistering fire was again unleashed upon his forces. Frequently exposing himself to enemy fire, he directed his units in silencing several automatic-weapons positions, and directed one PBR to investigate several sampans which could be seen, while the other PBR's provided cover fire. Almost immediately, the enemy renewed their fire in an effort to force the PBR's away from the sampans. Petty Officer Williams ordered the destruction of the sampan and the extraction of all his units. During the fierce firefight following the temporary immobilization of one of the units, Petty Officer Williams was wounded. Despite his painful injuries, he was able to lead his patrol back through the heavy enemy fire. His patrol had successfully interdicted a crossing attempt of three heavy-weapons companies totaling nearly four hundred men, had accounted for sixteen enemy killed in action, twenty wounded, the destruction of nine enemy sampans and junks, seven enemy structures, and 2400 pounds of enemy rice. By his outstanding display of decisive leadership, his unlimited courage in the face of heavy enemy fire, and his utmost devotion to duty, Petty Officer Williams upheld the highest traditions of the United States Naval Service.


== Other honors ==

Navy Special Boat Unit 20 headquarters at Joint Expeditionary Base Little Creek-Fort Story, Virginia Beach, Virginia, was dedicated the BM1 James E. Williams Building in 1997.
Navy guided missile destroyer USS James E. Williams (DDG-95) was named and christened in his honor on June 28, 2003, at Pascagoula, Mississippi.


== See also ==

List of Medal of Honor recipients
List of Medal of Honor recipients for the Vietnam War


== References ==

James Elliott Williams, Claim to Fame: Medal of Honor recipients at Find a Grave
"James E. Williams, Medal of Honor recipient". Medal of Honor recipients, Vietnam (M–Z). United States Army Center of Military History. June 8, 2009. Retrieved 2007-10-07.
"James E. Williams, USS James E. Williams website, US Navy". Archived from the original on December 1, 2005. Retrieved September 29, 2010.
"USS Williams website". Archived from the original on December 1, 2005. Retrieved September 29, 2010.
"Williams Memorial Web Site". Archived from the original on May 25, 2011. Retrieved September 29, 2010.