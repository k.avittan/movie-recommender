Beggar on Horseback is a 1924 play by George S. Kaufman and Marc Connelly.
The play is a parody of the expressionistic parables that were popular at the time; its title derives from the proverb "Set a beggar on horseback, and he'll ride at a gallop," "Set a beggar on horseback, and he'll ride to hell," or "Set a beggar on horseback, and he will ride to the devil," meaning that if you give wealth to the undeserving, they will be the worse for it.  The play rails against the perils of trading one's artistic talents for commercial gain. At its core is Neil McRae, a poor, young classical composer. Concerned about how hard he is working at odd jobs to meet his financial obligations, his friends - a doctor visiting from back home and his neighbor, Cynthia Mason, in whom he has more than a passing interest - urge him to marry Gladys Cady, whose father is a wealthy industrialist. However, the man also favors the Tin Pan Alley school of musical composition, to which McRae is staunchly opposed. Conflict arises when he is offered a job making widgets at a substantial salary if he agrees to give up his "foolish" interest in the classics.
The original Broadway production opened on February 12, 1924 at the Broadhurst Theatre, where it ran for 223 performances. The cast included Roland Young, Osgood Perkins, Spring Byington, and Frederic Richard Sullivan.
The play was revived with most of the original cast a mere seven months later, opening on March 23, 1925, at the Shubert Theatre, where it ran for 16 performances.
Forty-five years later, after 13 previews, another revival opened on May 14, 1970 at Lincoln Center's Vivian Beaumont Theater, where it ran for 52 performances. The cast included Leonard Frey and Susan Watson.


== Film version ==
On August 24, 1925, Paramount Pictures released a silent film version directed by James Cruze and starring Edward Everett Horton. Only three reels of this seven-reel film exist.


== References ==


== External links ==
The full text of Beggar on Horseback at the Internet Archive
Beggar on Horseback at Internet Broadway Database