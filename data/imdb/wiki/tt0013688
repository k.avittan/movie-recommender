The Toll of the Sea is a 1922 American silent drama film directed by Chester M. Franklin, produced by the Technicolor Motion Picture Corporation, released by Metro Pictures, and featuring Anna May Wong in her first leading role. The film was written by Frances Marion and directed by Chester M. Franklin (brother of director Sidney Franklin), with the lead roles played by Wong and Kenneth Harlan. The plot was a variation of the Madama Butterfly story, set in China instead of Japan.
The film was the second Technicolor feature (after 1917's The Gulf Between), the first color feature made in Hollywood, and the first Technicolor color feature anywhere that did not require a special projector to be used for screenings.The film premiered on November 26, 1922 at the Rialto Theatre in New York City, and went into general release on January 22, 1923.


== Plot ==
When young Chinese woman Lotus Flower sees an unconscious man floating in the water at the seashore, she quickly gets help for him. The man is Allen Carver, an American. Soon the two have fallen in love, and they get married "Chinese fashion". Carver promises to take her with him when he returns home. Lotus Flower's friends warn her that he will leave without her, and one states she has been forgotten by four American husbands, but she does not believe them. However, Carver's friends discourage him from fulfilling his promise, and he returns to the United States alone.
Lotus Flower has a young son, whom she names Allen after his father. When the older Allen finally returns to China, Lotus Flower is at first overjoyed. She dresses in her elaborate Chinese bridal gown to greet him. However, he is accompanied by his American wife, Elsie. Allen has told Elsie about Lotus Flower, and it is Elsie who persuaded her husband to tell Lotus Flower the real situation. When the boy is brought to see his father, Lotus Flower pretends he is the child of her American neighbors. Later, though, she confides the truth to Elsie and asks her to take the boy to America. She tells the child that Elsie is his real mother. After Elsie takes the boy away with her, Lotus Flower says, "Oh, Sea, now that life has been emptied I come to pay my great debt to you." The sun is then shown setting over the water, and it is implied that Lotus Flower drowns herself.


== Cast ==
Anna May Wong as Lotus Flower
Kenneth Harlan as Allen Carver
Beatrice Bentley as Barbara 'Elsie' Carver
Priscilla Moran as Little Allen (as Baby Moran)
Etta Lee as Gossip
Ming Young as Gossip


== Production ==
Because the Technicolor camera divided the lens image into two beams to expose two film frames simultaneously through color filters, and at twice the normal frames per second, much higher lighting levels were required. All scenes of The Toll of the Sea were shot under "natural light" and outdoors, with the one "interior" scene shot in sunlight under a muslin sheet.


== Preservation status ==
Once believed to have been lost, the film was restored by the UCLA Film and Television Archive, under the supervision of Robert Gitt and Peter Comandini, from the 35mm nitrate film original camera negative in 1985. As the final two reels were missing, Gitt and Comandini used "an original two-color Technicolor camera" to shoot a sunset on a California beach, "much as the film's original closing must have looked."


== Availability ==
The restored version is available as one of the titles included in the 4-DVD box-set Treasures from American Film Archives, 50 Preserved Films.


== References ==


== External links ==
The Toll of the Sea on IMDb
The Toll of the Sea at Silent Era
The Toll of the Sea is available for free download at the Internet Archive
The Toll of the Sea at AllMovie
The Toll of the Sea at Widescreen Museum