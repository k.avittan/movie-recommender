"The Crocodile's Dilemma" is the pilot episode and series premiere of the FX anthology series Fargo. The episode aired on April 15, 2014 in the United States on FX. It was written by series creator and showrunner Noah Hawley and directed by Adam Bernstein.  The title refers to the paradox in logic known as the crocodile dilemma.In the episode, a strange man named Lorne Malvo (Billy Bob Thornton) arrives in Bemidji, Minnesota. Living their daily lives, the town's inhabitants, including zealous police Deputy Molly Solverson (Allison Tolman), police officer and single father Gus Grimly (Colin Hanks), and insecure insurance salesman Lester Nygaard (Martin Freeman), are unaware that this man will impact their lives forever.
"The Crocodile's Dilemma" received critical acclaim and was seen by 2.65 million viewers. It received seven Primetime Emmy Award nominations, including Outstanding Directing for a Limited Series, Movie, or Dramatic Special and Outstanding Writing for a Limited Series, Movie, or Dramatic Special.


== Plot ==
In January 2006, a drifter named Lorne Malvo (Billy Bob Thornton) crashes a car at night on a wintry rural highway outside Bemidji, Minnesota, after hitting a deer, and cuts his forehead after banging it into the steering wheel. A nearly naked man jumps from the popped trunk and bolts through the snow into the woods.  Malvo calmly watches as the man runs away and makes no attempt at pursuit, instead choosing to gaze upon the dying deer. In town, a put-upon insurance salesman named Lester Nygaard (Martin Freeman) runs into Sam Hess (Kevin O'Grady), a former high school bully who intimidates Lester into accidentally running into a window and breaking his nose. At the hospital, Lester meets Malvo and tells him about Sam, and Malvo offers to murder him.  Lester fails to respond, but Malvo goes to Sam's workplace, to get a glimpse of him, and later that night murders him at a strip club by throwing a knife into his head.
Lester meets Malvo in a motel restaurant and confronts him about the murder.  Malvo tells Lester that if Lester does not stand up to "the boss, [Lester] will get washed away". Meanwhile, Chief Vern Thurman (Shawn Doyle) and Deputy Molly Solverson (Allison Tolman) investigate the car wreck and find the nearly naked man's frozen body in the nearby woods. They also look into Sam's death. While they are questioning Sam's wife Gina (Kate Walsh), Malvo calls the elder son pretending to be an attorney, and claims the younger brother inherited everything, leading the elder son to beat his brother with a hockey stick before Molly tackles him.
The police learn that Lester was overheard discussing Sam with another man (Malvo), at the hospital.  Lester tries to impress his wife, Pearl (Kelly Holden Bashar), by repairing the couple's washing machine, but he fails at it, and Pearl mocks him.  Lester, reaching his breaking point after years of her psychological abuse, hits her with a hammer and kills her. A panicked Lester summons Malvo to help with the aftermath.  Vern arrives at Lester's house to question him about Sam, but discovers Pearl’s body in the basement. Vern radios for backup, but Malvo arrives soon afterward and fatally shoots him. Malvo disappears before Molly shows up, and Lester intentionally knocks himself out to make the killings look like a home invasion.  He eventually wakes up in the hospital and notices a gun pellet lodged in his hand.
Later, in Duluth, police officer Gus Grimly (Colin Hanks) converses over radio with his daughter Greta (Joey King), until he pulls Malvo over for running a stop sign. Malvo overhears the radio and threatens Gus to either press the issue and face death, or allow Malvo to leave and live. Malvo drives away as a shaken Gus returns to his patrol car.


== Reception ==


=== Ratings ===
In its initial premiere on FX, the first episode of Fargo had 2.65 million viewers. The show premiered in the UK on Channel 4 on April 20, 2014, and was seen by 1.6 million viewers.


=== Critical reception ===
The first episode of the series was critically acclaimed; it currently holds a perfect 100% rating on Rotten Tomatoes.The A.V. Club writer Emily VanDerWerff gave the episode an A− rating but stated: "as much as I like this first episode, it will be hard for anything in this series to match my love of the film, which is perfection". IGN reviewer Roth Cornet gave the series premiere a 9 out of 10 "Amazing" rating, saying: "with rich and zany characters, darkly comedic violence, and a tantalizing mystery on tap, Fargo is well-worth watching". Dan Snierson of Entertainment Weekly named it the second best television episode of 2014.


=== Accolades ===
For the 66th Primetime Emmy Awards, Noah Hawley was nominated for Outstanding Writing for a Miniseries, Movie or a Dramatic Special and Adam Bernstein was nominated for Outstanding Directing for a Miniseries, Movie or a Dramatic Special, for this episode.


== References ==


== External links ==
"The Crocodile's Dilemma" on IMDb
"The Crocodile's Dilemma" at TV.com