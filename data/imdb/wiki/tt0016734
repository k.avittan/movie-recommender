The Silent Generation is the demographic cohort following the Greatest Generation and preceding the baby boomers. The generation is generally defined as people born from 1928 to 1945. By this definition and U.S. Census data, there were 23 million Silents in the United States as of 2019.In the United States, the generation was comparatively small because the Great Depression of the 1930s and World War II in the early-mid 1940s caused people to have fewer children. It includes most of those who fought during the Korean War. They are noted as forming the leadership of the civil rights movement as well as comprising the "silent majority".


== Terminology ==
Time magazine first used the term "Silent Generation" in a November 5, 1951 article titled "The Younger Generation", although the term appears to precede the publication:
The most startling fact about the younger generation is its silence. With some rare exceptions, youth is nowhere near the rostrum. By comparison with the Flaming Youth of their fathers & mothers, today's younger generation is a still, small flame. It does not issue manifestoes, make speeches or carry posters. It has been called the "Silent Generation."
A reason later proposed for this perceived silence is that as young adults during the McCarthy Era, many members of the Silent Generation felt it was unwise to speak out.The cohort has been named the "Lucky Few" in the 2008 book The Lucky Few: Between the Greatest Generation and the Baby Boom, by Elwood D. Carlson PhD, the Charles B. Nam Professor in Sociology of Population at Florida State University. 
Australia's McCrindle Research uses the name Builders to describe the Australian members of this generation, born between 1925 and 1945, and coming of age to become the generation "who literally and metaphorically built [the] nation after the austerity years post-Depression and World War II".


== Date and age range definitions ==
The Pew Research Center uses 1928 to 1945 as birth years for this cohort. According to this definition, the oldest member of the Silent Generation is 92 years old and the youngest turns 75 years old in 2020.
Resolution Foundation, in a report titled Cross Countries: International comparisons of international trends, uses 1926 to 1945 as birth dates for the Silent Generation.Authors William Strauss and Neil Howe use 1925–1942 as birth years.


== Characteristics ==

The Silent Generation were children of the Great Depression whose parents, having revelled in the highs of the Roaring Twenties, now faced great economic hardship and struggled to provide for their families. Before reaching their teens they shared with their parents the horrors of World War II but through children's eyes. Many lost their fathers or older siblings who were killed in the war. They saw the fall of Nazism and the catastrophic devastation made capable by the nuclear bomb. When the Silent Generation began coming of age after World War II, they were faced with a devastated social order within which they would spend their early adulthood and a new enemy in Communism via the betrayal of post-war agreements and rise of the Soviet Union. Unlike the previous generation who had fought for “changing the system,” the Silent Generation were about “working within the system.” They did this by keeping their heads down and working hard, thus earning themselves the "silent" label. Their attitudes leaned toward not being risk-takers and playing it safe. Fortune magazine's story on the “College Class of ‘49” was subtitled “Taking No Chances." 
From their childhood experiences during the Depression and the insistence from their parents to be frugal, they tend to be thrifty and even miserly. They prefer to maximize the property's lifespan, i.e. "get their money's worth." This can lead to hoarding in the guise of "not being wasteful.”As with their own parents, Silents tended to marry and have children young. American Silents are noted as being the youngest of all American generations in marrying and raising families. As young parents, this generation gave birth primarily to the Baby Boomers while younger members of the generation and older members who held off raising a family until later in life gave birth to Generation X. Whereas divorce in the eyes of the previous generation was considered an ultimate sin, the Silents were the generation that reformed marriage laws to allow for divorce and lessen the stigma. This led to a wave of divorces among Silent Generation couples thereafter in the United States.As a birth cohort, they never rose in protest as a unified political entity. Because "following the rules" had proven to be successful for Silents and had led to incredible and stable wealth creation, it was common that their Boomer and Gen X children would become estranged from them due to their diametrically opposite rebellious nature, vocal social concerns, and economic hardship unknown to the Silents, creating a different generational consciousness. For example, the Boomer children were instrumental in bringing about the counterculture of the 1960s, and the rise of left wing, liberal views considered anti-establishment, those of which went directly against the "work within the system" methodology that the Silents worshipped. Gen X children grew up in the 1970s and 1980s with the threat of nuclear war hanging over them and a resultant bleak view of the future, contributing to their generational disaffection, in contrast to the optimistic outlook of their Silent Generation parents.The style of parenting known to the Silents and the generations before them originated in the late 1800s. Representative of this was the idea that "children should be seen but not heard." These ideas were ultimately challenged following the 1946 publication of the book The Common Sense Book of Baby and Child Care by Benjamin Spock which influenced some Boomers' views on parenting and family values when they became parents themselves. These conflicting views, seen as overly permissive by the Silents, further estranged those Boomers from their parents and, amongst other things, gave rise in the mid 1960s to the term generation gap to describe initially the conflict of cultural values between the Silents and their Boomer (and later Gen X) children.


== Demographics ==

Data is from the Pew Research Center. Some cohort sizes are greater than the number born due to immigration.


== See also ==

List of generations


== References ==


== External links ==
TIME Magazine, The Younger Generation, 1951
TIME Magazine, The Silent Generation Revisited, 1970
The Silent Generation