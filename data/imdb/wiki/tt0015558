Alice Solves the Puzzle is a 1925 animated short film directed by Walt Disney. It was the 15th film in the Alice Comedies series, and is notable for being the first film to feature Pete, the longest-recurring Disney character. The film is also notable for being one of the first animated films to have been heavily censored.


== Plot ==
A girl named Alice is stuck while solving a difficult crossword puzzle when her cat Julius tells her they should go to the beach. They swim in the ocean for a while then dry off and Alice continues her puzzle. Just as she begins, Pete (a collector of rare crossword puzzles who discovers that it is the one he is missing) demands she give him her puzzle. Alice refuses and smacks him in the face. She then runs into a lighthouse and locks the door. Pete breaks down the door and chases Alice around the lighthouse. Alice screams for help and Julius hears her. He climbs to the top and a fight breaks out. Julius wins the fight by knocking Pete off the lighthouse. Alice then discovers the last phrase in her puzzle, "The End." 


== Cast ==
Margie Gay - Alice
Walt Disney - Julius, Bootleg Pete, Police Dog (voices)


== Legacy ==


=== Pete ===
Alice Solves the Puzzle was the first film to feature the antagonist Pete. He would go on to become the longest running character of all the Disney animated creations. In this first installment he is referred to as “Bootleg Pete” because of his use of whiskey (at a time when alcohol was illegal). Because of his peg used for a right leg, he quickly gained the nickname Peg-Leg Pete. The early Pete was portrayed as a bear, which he would stay for most of his early appearances until the advent of Mickey Mouse, and subsequently became a large cat.


=== Censorship ===
When Russell Merritt examined a German print of Alice Solves the Puzzle, he was surprised to find an additional scene missing from American prints. In most prints of Pete's first scene, he is shown speeding in a boat being pulled by a pelican. He passes a police-dog, who blows a whistle and chases him. Pete simply turns and laughs. However, Merrit discovered in the German version Pete is stopped by a customs inspector who examines the boat, then lets him pass. Pete then opens the pelican's mouth and pulls out a bottle of bootleg whiskey. This scene was cut because the Pennsylvania Censorship Board asked Disney to cut the scene during its first release. Disney then directed Winkler Studios, his distributor, to cut the scene from any further U.S. releases.The only remnant of the scene in the United States is two frames in which Pete's whiskey bottle is still visible.


== Sources ==


== External links ==
Alice Solves the Puzzle on IMDb
Alice Solves the Puzzle at the Internet Animation Database
Alice Solves the Puzzle in Alice in Cartoonland at Google Photos