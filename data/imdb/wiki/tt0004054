Jawaani Jaaneman is a 2020 Indian Hindi-language comedy-drama film directed by Nitin Kakkar, and produced by Jackky Bhagnani, Deepshikha Deshmukh, Jay Shewakramani and lead actor Saif Ali Khan under the banners of Pooja Entertainment, Black Knight Films and Northern Lights Films. It stars Khan as a 40-year-old father and debutant Alaya Furniturewala as his daughter, alongside Tabu. The film revolves around Jazz, a property broker and party-animal in London, who has to confront a daughter he never knew he had, who is also pregnant.
Principal photography of the film took place from 14 June to 24 August 2019 in London, England. It was theatrically released in India on 31 January 2020 to mixed-to-positive reviews. It grossed ₹45 crore worldwide to emerge as one of Khan's more successful small-budget films.


== Plot ==
Jaswinder "Jazz" Singh (Saif Ali Khan) is a 40-year-old carefree womaniser who works in London as a property broker during the day with his brother Dimpy (Kumud Mishra) and parties at night in a bar owned by his friend Rocky (Chunky Pandey), usually taking whichever girl agrees to go to his home for a one-night stand. Jazz, along with his brother Dimpy, has to negotiate a multi-million-pound deal involving a property in Hounslow, but his aged landlady Malika (Kamlesh Gill) refuses to part with her home, and his repeated attempts to convince her to give it up for the new construction do not succeed.
One night, Jazz is partying as usual when a girl spills her drink on him; she apologises and he carries on. The next night, at the bar, she reveals herself to him as Tia (Alaya Furniturewala), a 21-year-old Amsterdammer who has come to London for some personal work, as she puts it. He agrees to take her to his home. There, Tia tells him that she is on a quest to find her long-lost father, and that Jazz is one of three possible options, based on a photograph of Tia's mother with him. When she tells him that she might be his daughter, he is aghast, but she advises him to take a DNA test to confirm the results. When the results arrive, it transpires that not only is Tia Jazz's daughter, but she also happens to be pregnant.
A shocked Jazz initially refuses to accept the truth; when Tia offers to stay with him and deliver her baby at his home, he refuses, saying that he cannot take on the responsibility of running a family. She moves in next door to his house. He consults his hairdresser friend Rhea (Kubbra Sait), who is his age, for advice. She congratulates him and advises him to confront his being not only a father but also an impending grandfather. His reckless partying ways continue at personal expense, as once he falls from a bar table, drunk, and breaks his leg. His feelings for Rhea grow, but when he tries to have sex with her at her home, she stops him and asks him to give up his lecherous ways, and instead to be a good friend and nothing more.
Meanwhile Tia visits Jazz at his home regularly, and silently resolves to endear herself to his family. One night Jazz's brother Dimpy and their parents visit his home. Despite Jazz warning her against it, Tia embraces everyone, and catches them unawares when she shows them her sonogram on Jazz's projector, forcing him to explain the situation. When they leave, an elated Tia tells Jazz that she had come in search of a father but instead found a family. Tia also convinces his landlady Malika to agree to the property deal and part with her house, which she accepts on the condition that a willow tree, that she has grown with since her childhood, not be touched.
The two happily live through her pregnancy, until one day when Tia's hippie mother Ananya (Tabu) and Tia's boyfriend Rohan turn up from Amsterdam unannounced. Ananya and Rohan's hippie, drug-fuelled, wanton ways are too much for Jazz to handle, and he fears that this may impact Tia's delivery. One day Jazz and Dimpy are celebrating the launch of their new prime property in Hounslow, when Tia arrives and notices in the model of the property that the willow tree is missing. Hurt, she leaves; when Jazz confronts her, and attempts to convince her that the millions to be gained from the property will more than compensate for the lost tree, she tells him tearfully that, instead of a father, she has found a property broker.
Tia, Ananya and Rohan proceed to leave Jazz's house to catch a train to Amsterdam, which leaves the next day. A despondent Jazz ponders over whether the deal is really worth aggrieving an old lady, before he finally decides to call off the Hounslow project, even at the expense of money, to which Dimpy agrees. Jazz rushes to Waterloo railway station where the three are waiting for their train; he proceeds to tell how he cancelled the deal and saved Malika's tree, when Tia's water breaks suddenly. The scene then cuts to the hospital where she has delivered a baby girl, and, for the first time, Jazz is genuinely happy at the prospect of having a family and being a grandfather.
The film ends with Jazz's friends Rhea and Rocky joining his family at his parents' home for Diwali celebrations. Jazz whispers into baby Kiara's ears that she will be the world's first baby to witness the wedding of both her parents and her grandfather, as Tia looks on happily.


== Cast ==
Saif Ali Khan as Jaswinder "Jazz" Singh
Tabu as Anaya
Alaya Furniturewala as Tia Singh
Kubbra Sait as Rhea
Chunky Pandey as Rajendra "Rocky" Sharma, Jazz's Best Friend
Kumud Mishra as Dimpy, Jazz's elder brother
Farida Jalal as Jazz's mother
Dante Alexander as Rohan, Tia's boyfriend
Kiku Sharda as Dr. Kriplani
Rameet Sandhu as Tanvi (cameo)


== Production ==


=== Development ===
Saif Ali Khan started a new production house in October 2018 to co-produce Jawaani Jaaneman with Jay Shewakramani. Saif wanted the avant-garde project  in an alternative genre. In a statement, he said, "this is going to be as exciting as it gets. Jay and I have been planning this for a while now and with Jawaani Jaaneman, we found just the right project to produce together.” Alaya Furniturewala, daughter of actress Pooja Bedi, making her debut is playing the role of his daughter. Tabu has been cast in an interesting role. Khan lost weight to fit into the character of a 40-year-old father.


=== Filming ===
The filming began in the second week of June 2019 with principal photography in London. Alaya finished the filming of the first schedule in August 2019. The film was eventually wrapped up on 24 August 2019.


== Release ==
The film was released on 31 January 2020.


=== Box office ===
Jawaani Jaaneman earned ₹3.24 crore at the domestic box-office on its opening day. On the second day, the film collected ₹4.55 crore. On the third day, the film collected ₹5.04 crore, taking its total opening weekend collection to ₹12.83 crore.As of 13 March 2020, with a gross of ₹34.50 crore in India and ₹10.27 crore overseas, the film has a worldwide gross collection of ₹44.77 crore.


== Soundtrack ==
This film’s soundtrack is composed by Gourov-Roshin, Tanishk Bagchi and Prem-Hardeep. The lyrics are written by Preet Harpal, Mumzy Stranger, Shabbir Ahmed, Devshi Khanduri and Shellee.
The song Gallan Kardi is a remake of the song Dil Luteya, written by Preet Harpal, composed by Sukshinder Shinda and sung by Jazzy B, Apache Indian.The song "Ole Ole 2.0" from Yeh Dillagi (1994) was originally composed by Dilip Sen and Sameer Sen, lyrics by Sameer Anjaan and sung by Abhijeet Bhattacharya then was recreated by Tanishk Bagchi.


== References ==


== External links ==
Jawaani Jaaneman on IMDb
Jawaani Jaaneman at Bollywood Hungama