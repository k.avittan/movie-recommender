Yesterday, Today and Tomorrow (Italian: Ieri, oggi, domani) is a 1963 comedy anthology film by Italian director Vittorio De Sica. It stars Sophia Loren and Marcello Mastroianni. The film consists of three short stories about couples in different parts of Italy. The film won the Academy Award for Best Foreign Language Film at the 37th Academy Awards.


== Plot ==


=== Adelina of Naples ===
Set in the poorer Naples in 1954, Adelina (Sophia Loren) supports her unemployed husband Carmine (Marcello Mastroianni) and child by selling black market cigarettes.  When she doesn't pay a fine, her furniture is to be repossessed.  However her neighbors assist her by hiding the furniture. A lawyer who lives in the neighborhood advises Carmine that, as the fine and furniture are in Adelina's name, she will be imprisoned.  However, Italian law stipulates that women cannot be imprisoned when pregnant or within six months after a pregnancy.  As a result, Adelina schemes to stay pregnant continuously.  After seven children in eight years, Carmine is seriously exhausted and Adelina must make the choice of being impregnated by their mutual friend Pasquale (Aldo Giuffrè) or be incarcerated.
She finally chooses to be incarcerated, and the whole neighborhood gathers money to free her and petition for her pardon, which finally comes and she is reunited with her husband Carmine and their children.


=== Anna of Milan ===
Anna (Sophia Loren dressed by Christian Dior), the wife of a mega-rich industrialist, has a lover named Renzo (Marcello Mastroianni).  Whilst driving together in her husband's Rolls-Royce, Anna must determine which is the most important to her happiness – Renzo or the Rolls. Renzo rethinks his infatuation with Anna when she expresses no concern when they nearly run over a child, and end up crashing the Rolls-Royce.
She is infuriated by the damage to her Rolls-Royce, and ends up getting another passing driver to take her home, leaving Renzo on the road.


=== Mara of Rome ===
Mara (Sophia Loren) works as a prostitute from her apartment, servicing a variety of high class clients including Augusto (Marcello Mastroianni), the wealthy, powerful and neurotic son of a Bologna industrialist.
Mara's elderly neighbor's grandson Umberto (Gianni Ridolfi) is a handsome and callow young man studying for the priesthood but not yet ordained. Umberto and Mara talk one night asking each other about their occupations. Embarrassed, Mara tells him she does manicures. Umberto's grandmother (Tina Pica) sees them talking and, knowing that Mara is a prostitute, interrupts their conversation telling Mara that she'll go to hell. Umberto protests, but Mara defends herself. Umberto falls in love with her. To the shrieking dismay of his grandmother, the young man wishes to leave his vocation to be with Mara, or to join the French Foreign Legion, if Mara rejects him.  Mara vows to set the young man on the path of righteousness back to the seminary and vows celibacy for a week, if she succeeds. For this, she enlists the reluctant Augusto. Mara then provides a striptease at the climax of the film.


== Cast ==
Sophia Loren as Adelina Sbaratti / Anna Molteni / Mara
Marcello Mastroianni as Carmine Sbaratti / Renzo / Augusto Rusconi
Aldo Giuffrè as Pasquale Nardella (segment "Adelina")
Agostino Salvietti as Dr. Verace (segment "Adelina")
Lino Mattera as Amedeo Scapece (segment "Adelina")
Tecla Scarano as Verace's sister (segment "Adelina")
Silvia Monelli as Elivira Nardella (segment "Adelina")
Carlo Croccolo as Auctioneer (segment "Adelina")
Pasquale Cennamo as Chief Police (segment "Adelina")
Tonino Cianci as (segment "Adelina") (as Antonio Cianci)
Armando Trovajoli as Giorgio Ferrario (segment "Anna")
Tina Pica as Grandmother Ferrario (segment "Mara")
Gianni Ridolfi as Umberto (segment "Mara") (as Giovanni Ridolfi)
Gennaro Di Gregorio as Grandfather (segment "Mara")


== Awards ==
1965 Academy Award for Best Foreign Language Film
1965 BAFTA Award for Best Foreign Actor – Marcello Mastroianni
1964 Golden Globes – Samuel Goldwyn Award - nomination
1964 David di Donatello Awards – David for Best Production – Carlo Ponti


== See also ==
List of submissions to the 37th Academy Awards for Best Foreign Language Film
List of Italian submissions for the Academy Award for Best Foreign Language Film


== References ==


== External links ==
Yesterday, Today and Tomorrow on IMDb
Yesterday, Today and Tomorrow at the TCM Movie Database
Yesterday, Today and Tomorrow at AllMovie