A half-truth is a deceptive statement that includes some element of truth. The statement might be partly true, the statement may be totally true but only part of the whole truth, or it may use some deceptive element, such as improper punctuation, or double meaning, especially if the intent is to deceive, evade, blame or misrepresent the truth.


== Purpose ==
The purpose and or consequence of a half-truth is to make something that is really only a belief appear to be knowledge, or a truthful statement to represent the whole truth, or possibly lead to a false conclusion. According to the justified true belief theory of knowledge, in order to know that a given proposition is true, one must not only believe in the relevant true proposition, but one must also have a good reason for doing so. A half-truth deceives the recipient by presenting something believable and using those aspects of the statement that can be shown to be true as a good reason to believe the statement is true in its entirety, or that the statement represents the whole truth. A person deceived by a half-truth considers the proposition to be knowledge and acts accordingly.


== Examples ==
In January 2018, U.S. President Donald Trump claimed on Twitter that "because of my policies, Black Unemployment has just been reported to be at the LOWEST RATE EVER RECORDED!" Although the unemployment rate for black Americans was indeed at a record low, the rate had been consistently decreasing since 2010, seven years before Trump took office.
Using a technicality: Former U.S. President Bill Clinton famously engaged in a half-truth when he gave the testimony of "I did not have sexual relations with that woman, Miss Lewinsky." Here he engaged in an equivocation fallacy to deliberately indicate one particular meaning of the phrase "sexual relations", while intending another meaning, in order to deliberately mislead the court while still being able to later claim that "my statements were technically correct."
"You should not trust Peter with your children. I once saw him smack a child with his open hand." In this example the statement could be true, but Peter may have slapped the child on the back because he was choking.
"I'm a really good driver. In the past thirty years, I have gotten only four speeding tickets"* This statement is true, but irrelevant if the speaker started driving a week ago.
After being stopped for drunk driving, the inebriated driver proclaims "I only had a couple of beers" in slurred speech. The driver may have also consumed alcoholic drinks other than beer, and the "beers" may have been large bottles as opposed to the usual contents of a normal-sized can, bottle, or glass
The classic story about blind men and an elephant. Each blind man touches a different part of the elephant and reaches a different conclusion about the nature of the elephant; while each man's experience of the elephant is accurate, none of them have a full understanding of the nature of the beast. One may be touching the tail and believe that the elephant is long and thin, another may be touching the belly and say that it is round and big.
"False dichotomy": the formal fallacy of false dilemma, also known as false choice, falsified dilemma, fallacy of the excluded middle, black and white thinking, false correlative, either/or fallacy, and bifurcation—involves a situation in which two alternative statements are held to be the only possible options, when in reality there exist one or more other options which have not been considered or presented to the listeners.


== Politics ==
Some forms of half-truths are an inescapable part of politics in representative democracies. The reputation of a political candidate can be irreparably damaged if they are exposed in a lie, so a complex style of language has evolved to minimise the chance of this happening. If someone has not said something, they cannot be accused of lying. As a consequence, politics has become a world where half-truths are expected, and political statements are rarely accepted at face value.William Safire defines a half-truth, for political purposes, as "a statement accurate enough to require an explanation; and the longer the explanation, the more likely a public reaction of half-belief".In his 1990 work The Magic Lantern: The Revolution of 1989 Witnessed in Warsaw, Budapest, Berlin, and Prague, Timothy Garton Ash responded to Václav Havel's call for "living in truth":

Now we expect many things of politicians in a well-functioning parliamentary democracy. But "living in truth" is not one of them. In fact the essence of democratic politics might rather be described as "working in half-truth". Parliamentary democracy is, at its heart, a system of limited adversarial mendacity, in which each party attempts to present part of the truth as if it were the whole.
Philosopher Alfred North Whitehead was quoted as saying: "There are no whole truths; all truths are half-truths. It is trying to treat them as whole truths that plays the devil". If this is true, statements, or truths, which according to Whitehead are all half-truths, are susceptible to creating deceptive and false conclusions.


== Meme theory ==
Richard Brodie links half-truths to memes "the truth of any proposition depends on the assumptions you make in considering it—the distinct memes you use in thinking about it". Brodie considers half-truths a necessary part of human interaction because they allow practical application of ideas when it is impractical to convey all the information needed to make a fully informed decision, although some half-truths can lead to a false conclusions or inferences in the world of logic.


== Quotations ==
The notion of half-truths has existed in various cultures, giving rise to several epigrammatic sayings.

Karl Kraus, an Austrian journalist, critic, playwright, and poet noted, "An aphorism can never be the whole truth; it is either a half-truth or a truth-and-a-half."
Arthur Koestler "Two half-truths do not make a truth, and two half-cultures do not make a culture."


== See also ==


== References ==


== External links ==
Lying with Statistics - Examples of abuse of statistical, mathematical and scientific principles
Half-Truths and the Development of Tax Policy