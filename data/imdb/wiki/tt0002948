The Holy Rosary (; Latin: rosarium, in the sense of "crown of roses" or "garland of roses"), also known as the Dominican Rosary, or simply the Rosary, refers to a set of prayers used in the Catholic Church and to the string of knots or beads used to count the component prayers. When referring to the prayer, the word is usually capitalized ("the Rosary", as is customary for other names of prayers, such as "the Lord's Prayer", and "the Hail Mary"); when referring to the beads, it is written with a lower-case initial letter ("a rosary bead").
The prayers that compose the Rosary are arranged in sets of ten Hail Marys, called decades. Each decade is preceded by one Lord's Prayer ("Our Father") and traditionally followed by only one Glory Be. Many Catholics also choose to recite the "O my Jesus" prayer after the Glory Be, which is the most well-known of the seven Fatima Prayers. During recitation of each set, thought is given to one of the Mysteries of the Rosary, which recall events in the lives of Jesus and of Mary. Five decades are recited per rosary. Rosary beads are an aid towards saying these prayers in the proper sequence.
Pope Pius V (in office 1566–1572) established a standard 15 Mysteries of the Rosary, based on long-standing custom. This groups the mysteries in three sets: the Joyful Mysteries, the Sorrowful Mysteries, and the Glorious Mysteries. In 2002 Pope John Paul II said that it is fitting that a new set of five be added, termed the Luminous Mysteries, bringing the total number of mysteries to 20. The Glorious mysteries are said on Sunday and Wednesday, the Joyful on Monday and Saturday, the Sorrowful on Tuesday and Friday, and the Luminous on Thursday. Usually five decades are recited in a session.
Over more than four centuries, several popes have promoted the Rosary as part of the veneration of Mary in the Catholic Church,
and consisting essentially in meditation on the life of Christ. The rosary also represents the Catholic emphasis on "participation in the life of Mary, whose focus was Christ", and the Mariological theme "to Christ through Mary".


== Devotions and spirituality ==

Devotion to the Rosary is one of the most notable features of popular Catholic spirituality. Pope John Paul II placed the Rosary at the very center of Christian spirituality and called it "among the finest and most praiseworthy traditions of Christian contemplation."Catholics believe the Rosary is a remedy against severe trials, temptations and the hardships of life, and that the Rosary is one of the great weapons given to believers in their battle against every evil.Saints and popes have emphasized the meditative and contemplative elements of the rosary and provided specific teachings for how the rosary should be prayed, for instance the need for "focus, respect, reverence and purity of intention" during rosary recitations and contemplations.From the sixteenth century onwards, Rosary recitations often involved "picture texts" that assisted meditation. Such imagery continues to be used to depict the mysteries of the rosary. Catholic saints have stressed the importance of meditation and contemplation. Scriptural meditations concerning the rosary are based on the Christian tradition of Lectio Divina, (literally divine reading) as a way of using the Gospel to start a conversation between the person and Christ. Padre Pio, a rosary devotee, said: "Through the study of books one seeks God; by meditation one finds him."References to the Rosary have been part of a number of reported Marian Apparitions spanning two centuries. The reported messages from these apparitions have influenced the spread of Rosary devotion worldwide. In Quamquam pluries Pope Leo XIII related Rosary devotions to Saint Joseph and granted indulgences for adding a prayer to St. Joseph to the Rosary during the month of October.Praying the Rosary may be prescribed by priests as a type of penance after confession. (Penance is not generally intended as a "punishment"; rather, it is meant to encourage meditation upon and spiritual growth from past sins.)


== History ==

Knotted prayer ropes were used in early Christianity; the Desert Fathers are said to have created the first such, using knots to keep track of the number of times they said the Jesus prayer.According to pious tradition, the concept of the Rosary was given to Saint Dominic in an apparition of the Virgin Mary during the year 1214 in the church of Prouille, though in fact it was known from the ninth century in various forms. This Marian apparition received the title of Our Lady of the Rosary. In the 15th century it was promoted by Alanus de Rupe (aka Alain de la Roche or Blessed Alan of the Rock), a Dominican priest and theologian, who established the "fifteen rosary promises" and started many rosary confraternities.
According to Herbert Thurston, it is certain that in the course of the twelfth century and before the birth of St. Dominic, the practice of reciting 50 or 150 Ave Marias had become generally familiar.  According to 20th century editions of the Catholic Encyclopedia, the story of St. Dominic's devotion to the Rosary and supposed apparition of Our Lady of the Rosary does not appear in any documents of the Church or Dominican Order prior to the writings of Blessed Alanus, some 250 years after Dominic. However recent scholarship by Donald H. Calloway, which has received the endorsement of some of the Church hierarchy, seeks to refute this claim.Leonard Foley claimed that although Mary's giving the Rosary to St. Dominic is recognized as a legend, the development of this prayer form owes much to the Order of Preachers.The practice of meditation during the praying of the Hail Mary is attributed to Dominic of Prussia (1382–1460), a Carthusian monk who termed it "Life of Jesus Rosary". The German monk from Trier added a sentence to each of the 50 Hail Marys, using quotes from scriptures (which at that time followed the name "Jesus," before the intercessory ending was added during the Counter-Reformation). In 1569, the papal bull Consueverunt Romani Pontifices by the Dominican Pope Pius V officially established the devotion to the rosary in the Catholic Church.From the 16th to the early 20th century, the structure of the Rosary remained essentially unchanged. There were 15 mysteries, one for each of the 15 decades. During the 20th century, the addition of the Fatima Prayer to the end of each decade became more common. There were no other changes until 2002, when John Paul II instituted five optional new Luminous Mysteries, although variations of these had already been proposed by the likes of St. Louis de Montfort and St. George Preca and were implemented during the mid-20th Century by figures such as Patrick Peyton.


=== Papal endorsements ===
During the 16th century, Pope Pius V associated the rosary with the General Roman Calendar by instituting the Feast of Our Lady of Victory (later changed to Our Lady of the Rosary), which is celebrated on 7 October.
Pope Leo XIII, known as "The Rosary Pope," issued twelve encyclicals and five apostolic letters concerning the rosary and added the invocation Queen of the most Holy Rosary to the Litany of Loreto. Pope Pius XII and his successors actively promoted veneration of the Virgin in Lourdes and Fatima, which is credited with a new resurgence of the rosary within the Catholic Church.Pope John XXIII deemed the rosary of such importance that on 28 April 1962, in an apostolic letter he appealed for the recitation of the Rosary in preparation for the Second Vatican Ecumenical Council.Pope John Paul II issued the Apostolic Letter Rosarium Virginis Mariae which emphasized the Christocentric nature of the Rosary as a meditation on the life of Christ. He said: "Through the Rosary the faithful receive abundant grace, as by the hands of the Mother of the Redeemer."On 3 May 2008, Pope Benedict XVI stated that the Rosary was experiencing a new springtime: "It is one of the most eloquent signs of love that the young generation nourish for Jesus and his Mother." To Benedict XVI, the Rosary is a meditation on all the important moments of salvation history.
The Congregation for Divine Worship's directory of popular piety and the liturgy emphasizes the Christian meditation/meditative aspects of the rosary, and states that the Rosary is essentially a contemplative prayer which requires "tranquility of rhythm or even a mental lingering which encourages the faithful to meditate on the mysteries of the Lord's life." The Congregation for Divine Worship points out the role the Rosary can have as a formative component of spiritual life.
The theologian Romano Guardini described the Catholic emphasis on the Rosary as "participation in the life of Mary, whose focus was Christ." This opinion was expressed earlier by Leo XIII who considered the rosary as way to accompany Mary in her contemplation of Christ.


=== Devotional growth ===

The Rosary has been featured in the writings of Catholic figures from saints to popes and continues to be mentioned in reported Marian apparitions, with a number of promises attributed to the power of the rosary.
According to Alan de la Roche, the Virgin Mary is reputed to have made 15 specific promises to Christians who pray the Rosary. The Fifteen rosary promises range from protection from misfortune to meriting a high degree of glory in heaven.John T. McNicholas says that during the time of the Penal Laws in Ireland when Mass was either infrequent or impossible, the Rosary became a substitute within the home. During the 18th century, the French priest Louis de Montfort elaborated on the importance of the rosary and its power in his book Secret of the Rosary. He emphasized the power of the rosary and provided specific instructions on how it should be prayed, e.g. with attention, devotion, and modesty (reverence), with reflective pauses.One of the forces that encouraged the spread of the rosary during the 19th century among Catholics was the influence of the Rosary Pope, a title given to Leo XIII (1878–1903) because he issued a record twelve encyclicals and five Apostolic Letters concerning the Rosary, instituted the Catholic custom of daily Rosary prayer during the month of October and, during 1883, added the invocation Queen of the most Holy Rosary to the Litany of Loreto.Leo XIII explained the importance of the Rosary as the one road to God from the faithful to the mother and from her to Christ and through Christ to the Father, and that the Rosary was a vital means to participate with the life of Mary and to find the way to Christ. This emphasis on the path through Mary to Christ has since been a key direction in Catholic Mariology, with Mariology being viewed as inherent in Christology.Rosary as a family prayer was endorsed by Pope Pius XII in his encyclical Ingruentium malorum: "The custom of the family praying of the Holy Rosary is a most efficacious means."


== Structure of prayers ==


=== Basic structure ===
The structure of the Rosary prayer, recited using the rosary beads, is as follows: The Rosary is begun on the short strand:

The sign of the cross on the Crucifix;
The prayer "O Lord, open my lips; O God, come to my aid; O Lord, make haste to help me", still on the Crucifix;
The Apostles' Creed, still on the Crucifix;
The Lord's Prayer at the first large bead (for the needs of the Church and for the intentions of the pope);
The Hail Mary on each of the next three beads (for the three theological virtues: faith, hope, and charity); and
The Glory Be on the next large bead.The praying of the decades then follows, repeating this cycle for each mystery:

Announce the mystery;
The Lord's Prayer on the large bead;
The Hail Mary on each of the ten adjacent small beads;
The Glory Be and the Fatima Prayer on the space before the next large bead; andTo conclude:

The Salve Regina;
The Loreto Litany;
Any further intentions; and
The sign of the cross.Instead of ending each decade with the Gloria Patri, Pope Pius IX would add "May the souls of the faithful departed through the mercy of God rest in peace." The Fatima Prayer is commonly said after the Gloria Patri as a pious addition, still on the large bead.


=== Variations and common pious additions ===

Common pious additions to the Rosary occur after each decade and after recitation of the Hail Holy Queen. Some Catholics recite the Fatima Decade Prayer at the end of each decade, preceding or following the Glory Be. Some add the Miraculous Medal prayer "O Mary, conceived without sin..." or the Fatima Ave refrain ("Ave, Ave, Ave Maria! Ave, Ave, Ave Maria!"). Others add a praying of the pious Eucharistic prayer "O Sacrament Most Holy, O Sacrament Divine, All praise and all thanksgiving be every moment Thine" at the end of each decade in honor of Jesus in the Blessed Sacrament. In the practice of the Brothers of the Christian Schools, there is an additional decade for the intentions of the students or the Virgin Mary.

After the Hail, Holy Queen, many Catholics add the prayer "O God, whose only begotten Son...", the prayer to Saint Michael, and a prayer for the intentions of the Pope. In some cases, the Litany of Loreto may be recited at the end.
In the practice of the Dominican Order, the beginning prayers of the rosary correspond to the beginning of the Divine Office:
In the Name of the Father, and of the Son, and of the Holy Spirit. Amen.
Hail Mary, full of grace, the Lord is with Thee.
Blessed art Thou among women, and Blessed is the Fruit of Thy Womb, Jesus.
O Lord, open my lips.
And my mouth will proclaim Your praise.
Incline Your aid to me, O God.
O Lord, make haste to help me.
Glory be to the Father, and to the Son, and to the Holy Spirit, as it was in the beginning, is now, and ever shall be, world without end. Amen.


=== Group recitation of the Rosary ===
When a group recites the Rosary, it is customary that the prayers that constitute the decades are divided into two parts. The second part of the Our Father begins with "Give us this day our daily bread ..."; the second part of the Hail Mary begins with "Holy Mary, Mother of God ..."; and the second part of the Glory Be to the Father with "As it was in the beginning...." This lends itself to antiphonal prayer.Sometimes a leader may recite the first half of the prayer while the other participants recite the rest. In another style of praying the Rosary, the recitation of the first part of the prayers is sometimes entrusted to different persons while still maintaining the traditional Leader versus Congregation style of praying.


== Mysteries of the Rosary ==
The Mysteries of the Rosary are meditations on episodes in the life and death of Jesus from the Annunciation to the Ascension and beyond. These are traditionally grouped by fives into themed sets known as the Joyful (or Joyous) Mysteries, the Sorrowful Mysteries, and the Glorious Mysteries.Typically, a spiritual goal known as a "fruit" is also assigned to each mystery. Below are listed from the appendix of Louis Marie de Montfort's book Secret of the Rosary for the original 15 mysteries, with other possible fruits being listed in other pamphlets bracketed:


=== Joyful Mysteries ===
Joyful MysteriesThe Annunciation. Fruit of the Mystery: Humility
The Visitation. Fruit of the Mystery: Love of Neighbour
The Nativity. Fruit of the Mystery: Poverty, Detachment from the things of the world, Contempt of Riches, Love of the Poor
The Presentation of Jesus at the Temple. Fruit of the Mystery: Gift of Wisdom and Purity of mind and body (Obedience)
The Finding of Jesus in the Temple. Fruit of the Mystery: True Conversion (Piety, Joy of Finding Jesus)


=== Sorrowful Mysteries ===
Sorrowful MysteriesThe Agony in the Garden. Fruit of the Mystery: Sorrow for Sin, Uniformity with the Will of God
The Scourging at the Pillar. Fruit of the Mystery: Mortification (Purity)
The Crowning with Thorns. Fruit of the Mystery: Contempt of the World (Moral Courage)
The Carrying of the Cross. Fruit of the Mystery: Patience
The Crucifixion and Death of our Lord. Fruit of the Mystery: Perseverance in Faith, Grace for a Holy Death (Forgiveness)


=== Glorious Mysteries ===
Glorious MysteriesThe Resurrection. Fruit of the Mystery: Faith
The Ascension. Fruit of the Mystery: Hope, Desire to Ascend to Heaven
The Descent of the Holy Spirit. Fruit of the Mystery: Love of God, Holy Wisdom to know the truth and share it with everyone, Divine Charity, Worship of the Holy Spirit
The Assumption of Mary. Fruit of the Mystery: Union with Mary and True Devotion to Mary
The Coronation of the Virgin. Fruit of the Mystery: Perseverance and an Increase in Virtue (Trust in Mary's Intercession)


=== Luminous Mysteries ===
Pope John Paul II, in his apostolic letter Rosarium Virginis Mariae (October 2002), recommended an additional set called the Luminous Mysteries (or the "Mysteries of Light").
Luminous MysteriesThe Baptism of Jesus in the Jordan. Fruit of the Mystery: Openness to the Holy Spirit, the Healer.
The Wedding at Cana. Fruit of the Mystery: To Jesus through Mary, Understanding of the ability to manifest-through faith.
Jesus' Proclamation of the Kingdom of God. Fruit of the Mystery: Trust in God (Call of Conversion to the Messiah)
The Transfiguration. Fruit of the Mystery: Desire for Holiness.
The Institution of the Eucharist. Fruit of the Mystery: Adoration.The original Mysteries of Light were written by George Preca, the only Maltese official Catholic Saint, and later reformed by the Pope.


=== Days of praying ===
The full Rosary consists of praying all 15 mysteries, with the Joyful, Sorrowful and Glorious considered the minimal amount. Alternatively, a single set of five mysteries can be prayed each day, according to the following convention:


== Rosary beads ==

Rosary beads provide a physical method of keeping count of the number of Hail Marys said as the mysteries are contemplated. The fingers are moved along the beads as the prayers are recited. By not having to keep track of the count mentally, the mind is free to meditate on the mysteries. A five-decade rosary contains five groups of ten beads (a decade), with additional large beads before each decade. The Hail Mary is said on the ten beads within a decade, while the Lord's Prayer is said on the large bead before each decade. A new mystery meditation commences at each of the large beads. Some rosaries, particularly those used by religious orders, contain fifteen decades, corresponding to the traditional fifteen mysteries of the rosary. Both five- and fifteen-decade rosaries are attached to a shorter strand, which starts with a crucifix, followed by one large bead, three small beads, and one large bead, before connecting to the rest of the rosary. A five-decade rosary consists of a "total" of 59 beads.Although counting the prayers on a string of beads is customary, the prayers of the Rosary do not require a set of beads, but can be said using any type of counting device, by counting on the fingers or by counting without any device at all.


=== Single-decade rosaries ===
Single-decade rosaries can also be used: the devotee counts the same ring of ten beads repeatedly for every decade. During religious conflict in 16th- and 17th-century Ireland severe legal penalties were prescribed against practising Catholics. Small, easily hidden rosaries were thus used to avoid identification and became known as Irish penal rosaries. Sometimes rather than a cross, other symbols of specific meanings were used, such as a hammer to signify the nails of the cross, cords to represent the scourging, a chalice to recall the Last Supper, or a crowing rooster signifying the denial of Peter.


=== Materials and distribution ===

The beads can be made from any materials, including wood, bone, glass, crushed flowers, semi-precious stones such as agate, jet, amber, or jasper, or precious materials including coral, crystal, silver, and gold. Beads may be made to include enclosed sacred relics or drops of holy water. Rosaries are sometimes made from the seeds of the "rosary pea" or "bead tree." Today, the vast majority of rosary beads are made of glass, plastic or wood. It is common for beads to be made of material with some special significance, such as jet from the shrine of St. James at Santiago de Compostela, or olive seeds from the Garden of Gethsemane. In rare cases beads are made of expensive materials, from gold and silver to mother of pearl and Swarovski black diamond designs. Early rosaries were strung on thread, often silk, but modern ones are more often made as a series of chain-linked beads. Catholic missionaries in Africa have reported that rosaries made of tree bark have been used there for praying due to the lack of conventional rosaries. Our Lady's Rosary Makers produce some 7 million rosaries annually that are distributed to those considered to be in economic and spiritual need.Most rosaries used in the world today have simple and inexpensive plastic or wooden beads connected by cords or strings. The major cost is labor for assembly. A large number of inexpensive rosary beads are manufactured in Asia, especially in China. Italy has a strong manufacturing presence in medium- and high-cost rosaries.
Rosaries are often made for sale; hundreds of millions have also been made and distributed free of charge by Catholic lay and religious apostolates worldwide. There are a number of rosary-making clubs around the world that make and distribute rosaries to missions, hospitals, prisons, etc. free of charge. To comply with safety precautions in prisons, special rosaries are donated using string that easily breaks.


=== Wearing the rosary ===
The Apostolate of Holy Motherhood writes that the Virgin Mary encourages the faithful to wear the rosary and scapular because "it will help them to love Jesus more" and serve as a "protection from Satan." In addition, Saint Louis-Marie Grignion de Montfort encouraged Christians to wear the rosary, stating that doing so "eased him considerably." Many religious orders wear the rosary as part of their habit. A rosary hanging from the belt often forms part of the Carthusian habit.Canon Law §1171 provides that sacred objects, which are designated for divine worship by dedication or blessing, are to be treated reverently and are not to be employed for profane or inappropriate use even if they are owned by private persons. As such, according to Edward McNamara, professor of liturgy at the Regina Apostolorum University:

If the reason for wearing a rosary is as a statement of faith, as a reminder to pray it, or some similar reason "to the glory of God," then there is nothing to object to. It would not be respectful to wear it merely as jewelry. This latter point is something to bear in mind in the case of wearing a rosary around the neck. In the first place, while not unknown, it is not common Catholic practice. ...While a Catholic may wear a rosary around the neck for a good purpose, he or she should consider if the practice will be positively understood in the cultural context in which the person moves. If any misunderstanding is likely, then it would be better to avoid the practice. ...Similar reasoning is observed in dealing with rosary bracelets and rings, although in this case there is far less danger of confusion as to meaning. They are never mere jewelry but are worn as a sign of faith.

A rosary ring is a ring worn around the finger with 10 indentations and a cross on the surface, representing one decade of a rosary. These and other kinds of religious rings were especially popular during the 15th and the 16th centuries. These rosary rings have been given to some Catholic nuns at the time of their solemn profession. A finger rosary is similar to a ring, but is somewhat larger. Rosaries like these are used by either rotating or just holding them between a finger and thumb while praying. A hand rosary is a decade in a complete loop, with one bead separated from ten other beads, this is meant to be carried while walking or running, so as not to entangle the larger type.
In addition to a string of beads, single-decade rosaries are made in other physical forms. A ring rosary, also known as a "Basque rosary," is a finger ring with eleven knobs on it, ten round ones and one crucifix.
A rosary bracelet is one with ten beads and often a cross or medal. Another form is the rosary card. A rosary card is either one with a "handle" that moves like a slide rule to count the decade, or it has a whole rosary with bumps similar to Braille and ancient counting systems. Some households that cannot afford Christian artwork or a crucifix hang up a rosary. In addition, many Christians hang rosaries from the rear-view mirror of their automobiles as a sign of their faith and for protection as they drive.


=== Rosary and Scapular ===

"The Rosary and the Scapular are inseparable" were words attributed to the Virgin Mary by Lucia Santos, one of the three children who reported the Marian apparitions of Our Lady of Fátima in 1917 and later the Pontevedra apparitions in 1925. In these apparitions, the Virgin Mary reportedly called herself The Lady of the Rosary and in one of the final Fátima appearances on 13 October 1917 had a Brown Scapular in one hand and a rosary in the other. The Lady of the Rosary reportedly encouraged the praying of the Rosary and the wearing of the Brown Scapular.Throughout history, the Rosary and the Scapular as objects for devotions and prayers have been encouraged and associated by a number of popes, and specific indulgences have been attached to them.


=== Click To Pray eRosary ===
In October 2019 the Vatican launched a US$109 "electronic rosary" with ten black agate and hematite beads, and a metal cross that detects movement. It is connected to the "Click to Pray eRosary" mobile telephone app, designed to help Catholic users pray for world peace and contemplate the gospel. The rosary can be worn as a bracelet; it is activated by making the sign of the cross. The app gives visual and audio explanations of the rosary..


== Rosary based devotions ==
The use of Novenas which include a rosary is popular among Catholics. As in other Novenas, the traditional method consists of praying the rosary on nine consecutive days, and submitting a petition along with each prayer. Indulgences are provided for rosary Novenas that include specific prayers, e.g. a prayer to Saint Catherine of Siena and Saint Dominic.The longer "54-day Rosary Novena" consists of two parts, 27 days each, i.e. three repetitions of the 9-day Novena cycle. It is an uninterrupted series of Rosaries in honor of the Virgin Mary, reported as a private revelation by Fortuna Agrelli in Naples, Italy, in 1884. The Novena is performed by praying five decades of the Rosary each day for twenty-seven days in petition. The second phase which immediately follows it consists of five decades each day for twenty-seven days in thanksgiving, and is prayed whether or not the petition has been granted. During the novena, the meditations rotate among the joyful, sorrowful and glorious mysteries.Rosary beads are at times used to say rosary-based prayers that do not primarily involve the Hail Mary and the mysteries of the Rosary. Some forms of the Catholic rosary are intended as reparation including the sins of others. An example is the Rosary of the Holy Wounds first introduced at the beginning of the 20th century by the Venerable Sister Marie Martha Chambon, a Catholic nun of the Monastery of the Visitation Order in Chambéry, France. This rosary is somewhat similar in structure to the Chaplet of Divine Mercy introduced by Saint Faustina Kowalska said on the usual rosary beads and intended as an Act of Reparation to Jesus Christ for the sins of the world. These prayers often use rosary beads, but their words and format do not correspond to the Mysteries. Both Kowalska and Chambon attributed these prayers to visions of Jesus.


== Recordings of the Rosary ==
Recordings of the Rosary prayers are sometimes used by devotees to help with aspects of prayer such as pacing, memorization, and by providing inspirational meditations. Some of the more well known include:

The Rosary is a Place, Fr. Benedict J. Groeschel, CFR and Simonetta, The Saint Philomena Foundation
The Rosary, Fr. Kevin Scallon and Dana, Heartbeat Records
Pray the Rosary with Servant of God, Fr. Patrick Peyton


== In non-Catholic Christianity ==
Many similar prayer practices exist in other Christian communities, each with its own set of prescribed prayers and its own form of prayer beads (known as the "Chotki"), such as the prayer rope in Eastern Orthodox Christianity. These other devotions and their associated beads are usually referred to as "chaplets." The rosary is sometimes used by other Christians, especially in Lutheranism, the Anglican Communion, and the Old Catholic Church.Another example of Rosary-based prayers includes the non-denominational Ecumenical Miracle Rosary, "a set of prayers and meditations which covers key moments in the New Testament."


=== Anglicanism ===

The use of the Catholic Rosary is fairly common among Anglicans of Anglo-Catholic churchmanship. Many Anglo-Catholic prayer books and manuals of devotion, such as Saint Augustine's Prayer Book contain the Catholic Rosary along with other Marian devotions. The public services of the Anglican churches, as contained in the Book of Common Prayer, do not directly invoke the Blessed Virgin or any other saint in prayer as the Thirty-Nine Articles reject the practice of praying to saints, but many Anglo-Catholics feel free to do so in their private devotions. Anglicans who pray the Catholic Rosary tend not to use the Luminous Mysteries or the Fátima decade prayer.Anglican prayer beads, also known informally as the "Anglican rosary," are a recent innovation created in the 1980s. They consist of four "weeks" (the equivalent of a decade) of seven beads each. The weeks are separated from each other by single beads termed "cruciform beads." A variety of different prayers may be said, the most common being the Jesus Prayer.
Anglican Prayer Beads are not a Marian devotion, and there are no appointed meditations. Although it is sometimes called the "Anglican rosary," it is distinct from the Rosary of Our Lady as prayed by Catholics, Anglicans, and other Western Christians.


=== Lutheranism ===
A small minority of Lutherans pray the Rosary. However, while using the Catholic format of the Rosary, each "Hail Mary" is replaced with the "Jesus Prayer." The only time the "Hail Mary" is said is at the end of the Mysteries on the medal, where it is then replaced with the "Pre-Trent" version of the prayer (which omits "Holy Mary, mother of God, pray for us sinners, now and at the hour of our death"). The final "Hail Mary" can also be replaced by reciting of either the prayer Magnificat, or Martin Luther's "Evangelical praise of the Mother of God." The Wreath of Christ is used in the Swedish Lutheran church.


== Churches named for the Holy Rosary ==

Catholic Marian church buildings around the world named in honor of the rosary include: the Shrine of the Virgin of the Rosary of Pompei in Italy, Our Lady of the Rosary Basilica in the archdiocesan seat of Rosario province, Argentina; the Sanctuary of Our Lady of the Rosary of San Nicolás in the neighboring suffragan diocese of San Nicolás de los Arroyos, Our Lady of Pompeii in New York City, which is named for the Our Lady of the Rosary of Pompei, the Rosary Basilica in Lourdes, Nossa Senhora do Rosário in Porto Alegre, Brazil, The Chapel of the Virgin of the Rosary (1531–1690) in Puebla City, Mexico.

		
		
		
		


== In Marian art ==
Since the 17th century, the rosary began to appear as an element in key pieces of Catholic Marian art. Key examples include Murrillo's Madonna with the Rosary at the Museo del Prado in Spain and the statue of Madonna with Rosary at the church of San Nazaro Maggiore in Milan. 

		
		
		
		
		
		
		
		
		
		
		
		
		
		


== See also ==


== Notes ==


=== Works cited ===


=== General references ===


== Further reading ==


== External links ==

"Interactive web application for the Holy Rosary" (in English and Latin).
"Multilanguage parallel text of the Rosarium Beatae Virginis Mariae" (in Latin, English, Spanish, and French). Archived from the original on 14 August 2011.
"The Holy Rosary" http://www.theholyrosary.org/
Nuns of the Perpetual Rosary
Pray the Rosary online with others around the world http://www.comepraytherosary.org