Hart of Dixie is an American comedy-drama television series that aired on The CW from September 26, 2011, to March 27, 2015. The series, created by Leila Gerstein, stars Rachel Bilson as Dr. Zoe Hart, a New Yorker who, after her dreams of becoming a heart surgeon fall apart, accepts an offer to work as a general practitioner in the fictional Gulf Coast town of Bluebell, Alabama.
Hart of Dixie premiered on September 26, 2011. The show was scheduled to move back to Mondays in the fall for its third season having been paired with Beauty & the Beast. The show's third season premiered on October 7, 2013.On May 8, 2014, the CW renewed the show for its fourth season. On July 18, 2014, CW president Mark Pedowitz announced that Hart of Dixie would have ten episodes for its fourth season, which premiered on December 15, 2014. On March 14, 2015, Leila Gerstein revealed that season four of Hart of Dixie would be its last. The CW officially canceled the show on May 7, 2015.


== Plot ==


=== Season 1 ===
The first season revolves around Zoe Hart adjusting to life in the small town of Bluebell, Alabama, after failing to secure a fellowship in New York because of her poor bedside manner. She has inherited half of the practice from a father she never knew, with whom Zoe's mother had an affair while engaged to the man Zoe knew as her dad. To keep her half of the inherited practice she must bring in 30% of the patients, which she finds difficult as she is at odds with most of the local residents due to her city persona. Zoe also struggles with her growing feelings for local attorney George Tucker, feelings of which his fiancée, Lemon Breeland is fully aware, leading her to make it her mission to ensure Zoe leaves Bluebell. Other storylines include neighbor Wade's feelings for Zoe (which may or may not be returned); Zoe's friendship with the mayor, Lavon, and his past with Lemon; and Zoe's unresolved issues regarding her family.


=== Season 2 ===
The season begins with Zoe confused with her feelings for both George and Wade (she slept with Wade the night of George and Lemon's aborted wedding at the end of season one). She later decides that George isn't ready for another relationship yet and decides to see Wade. Lavon's former high school sweetheart Ruby Jeffries (played by Golden Brooks) returns to Bluebell and reveals she is opposing for mayor. George begins dating again, first seeing newcomer Shelby Sinclair; however, he later dumps her, and Shelby begins dating Brick Breeland. George later starts a relationship with Wade's ex-wife, Tansy Truitt. As the season progresses, Ruby leaves after Lemon's jealousy destroys her and Lavon. Wade and Zoe continue to date, though they face their share of setbacks. At the end of the season, Zoe is faced deciding between a summer in New York working at a hospital at her dream job - only to have Wade confess his feelings for her, and she unable to return them. Meanwhile, Annabeth begins having feelings for Lavon, and later the two sleep together, leaving Lemon devastated by her best friend's betrayal.


=== Season 3 ===
The season begins with Zoe returning to Bluebell after a summer in NYC - along with her new boyfriend Joel (played by Josh Cooke). George struggles to rebuild his life following his break up with Tansy but finds love with Lavon's younger cousin. Lemon finds herself in a scandalous relationship, while Annabeth hopes her relationship with Lavon will grow into something more. Zoe learns more about her family roots in Bluebell, breaks up with Joel, and reunites with Wade.


=== Season 4 ===
The final season deals with Zoe's pregnancy and her relationship with Wade. George, Lemon, Lavon, and Annabeth enter a tumultuous love affair, while Brick has to deal with his past in order to move forward as Lemon and Magnolia meet their half-sister from their estranged mother who left the family. In the end, George and Annabeth move in together, George changes careers to become a music manager, Zoe and Wade get married (in a rushed ceremony, due to Zoe's inhibitions about marriage and having gone into labor - they recite their vows while rushing towards the delivery room - delivering a baby boy), Lemon and Lavon get married.
The final scene shows the town in harmony in the town square, Wade and Zoe with their son – and an old love triangle re-fueled among three elderly members of the community; Zoe asks if all small towns are like Bluebell, to which Wade answers that they probably aren't. Zoe agrees, and they get up to walk their son and join their friends.


== Cast and characters ==
Rachel Bilson as Zoe Hart
Scott Porter as George Tucker
Jaime King as Lemon Breeland
Cress Williams as Lavon Hayes
Wilson Bethel as Wade Kinsella
Tim Matheson as Bertram “Brick” Breeland
Kaitlyn Black as Annabeth Thibodaux
McKaley Miller as Rose Hattenbarger


== Development and production ==
On February 1, 2011, it was announced that The CW had ordered a pilot for Hart of Dixie. On May 17, 2011, the network officially picked up Hart of Dixie to series, set to air in fall 2011. The series marks the second time executive producer Josh Schwartz and series star Rachel Bilson have worked together on television. The first time the duo worked together was on the Fox teen drama The O.C., created by Schwartz. The show's executive producer, Josh Schwartz, compared the show to Felicity, Everwood, and Gilmore Girls.With the reveal of The CW's fall 2011 schedule, it was announced that Hart of Dixie would air on Monday at 9:00 pm Eastern/8:00 pm Central, following Gossip Girl. It premiered on Monday, September 26, 2011. On October 12, 2011, the series was picked up for a full season, which will consist of twenty-two episodes. Along with pick-up for all other CW dramas, Mark Pedowitz said "We believe in the creative strength of these dramas, and by giving them back nine orders we can give our audience the chance to enjoy complete seasons of all three of them." On May 11, 2012, The CW renewed the show for a second season, which premiered on October 2, 2012. The CW renewed the show for a third season on April 26, 2013.


=== Casting ===
On February 8, 2011, TVLine reported that Rachel Bilson was nearing a deal to star in the series. Her role was later confirmed by The CW in a press release. Soon after, Wilson Bethel joined the cast as Wade Kinsella, Zoe's "gorgeous bad-boy" neighbor. Scott Porter was cast as good-looking lawyer George Tucker, a potential love-interest for Bilson's character.On May 20, 2011, it was announced that Nancy Travis would not continue with the series due to her commitments with the 20th Century Fox-produced sitcom Last Man Standing. Travis was written out after the first two episodes. Meredith Monroe appeared in one episode as Lemon's estranged mother. JoBeth Williams appeared in three episodes as Candice Hart, the mother of Bilson's character. On July 26, 2013, it was announced that Kaitlyn Black was upgraded to series regular status for season three.


== Reception ==


=== Critical reception ===
Hart of Dixie's first season has received mixed reviews, scoring a 43 out of 100 on the review aggregator Metacritic.TVGuide.com described the show as "Southern Exposure" and, in a later review, stated that the actors are better than the "cutesy" material, although Bilson is not convincing as a heart surgeon. Both TVGuide.com and Robert Bianco of USA Today stated that the show is potentially offensive to the South. Bianco also wrote that the show is shallow and far-fetched, with Bilson giving an unconvincing performance, such as acting surprised when calling herself a doctor. Tim Goodman of The Hollywood Reporter stated that the series is predictable and superficial, but "surprisingly touching".Writing for The New York Times, critic Neil Genzlinger wrote "the premiere, at least, doesn’t find a convincing way to balance the clashing strands: the city-mouse disorientation, the medical emergencies, the girlfights, the daddy issues, the young-pretty-and-available stuff." Los Angeles Times reviewer Mary McNamara described the show as "a stack of familiar scenarios stitched together to form a pretty if not terribly substantial quilt." TVLine described the show as "Everwood-esque". TVLine later stated: "Beautifully filmed with warm, cozy tones, the Southern setting utterly envelops the glowing Bilson", adding that Porter's appearance "wins us over and makes you forget that clunky intro" and despite the "rom-coms clichés, the pilot is super-efficient at introducing us to those who will be the key players in Zoe’s story, laying the framework for storytelling places to go." Tv Times magazine gave Hart of Dixie its lowest score of 2011–2012: 12 out of 100.


=== Awards and accolades ===


=== Ratings ===


== International broadcasts ==


== References ==


== External links ==
Official website
Hart of Dixie on IMDb
Hart of Dixie at TV.com