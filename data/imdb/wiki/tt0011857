Whisper of the Heart (Japanese: 耳をすませば, Hepburn: Mimi o Sumaseba, literally "If You Listen Closely") is a 1995 Japanese animated romantic coming-of-age drama film directed by Yoshifumi Kondō and written by Hayao Miyazaki based on the 1989 manga of the same name by Aoi Hiiragi. It was animated by Studio Ghibli for Tokuma Shoten, Nippon Television Network and Hakuhodo. The film stars Yoko Honna, Issei Takahashi, Takashi Tachibana, Shigeru Muroi, Shigeru Tsuyuguchi and Keiju Kobayashi. It was the first theatrical Studio Ghibli film to be directed by someone other than Miyazaki or Isao Takahata.
Whisper of the Heart was Kondō's only film as director before his death in 1998. Studio Ghibli had hoped that Kondō would become the successor to Miyazaki and Takahata.It was the only theatrical Ghibli film not directed by Miyazaki or Takahata for seven years until The Cat Returns was released in 2002, which focused on a minor character of the film, Baron.


== Plot ==
Shizuku Tsukishima is a 14-year-old student at Mukaihara Junior High School, where she is best friends with Yūko Harada. Living in Tokyo with her parents Asako and Seiya, as well as sharing a room with older sister Shiho, Shizuku is a bookworm and is keen on creative writing. During an ordinary evening, she looks through the checkout cards in her library books. She discovers they were all checked out previously by someone named Seiji Amasawa.
Over the next few days, Shizuku encounters a young man, later revealed to be Seiji, who annoys her by teasing her about "Concrete Roads", a set of original lyrics describing Tama New Town that Shizuku has written to the tune of the song Take Me Home, Country Roads. Finding a cat riding a train, Shizuku follows it to discover an antique shop run by Shirō Nishi. In the shop is a cat statuette nicknamed "The Baron". Shizuku is ecstatic about finding "a place where stories begin" and asks Nishi if she may return.
Shizuku later finds her way back to the antique shop, where Seiji encounters her. Seiji shows Shizuku the workshop below the showroom, where she discovers that he is learning to make violins to pursue his dream of becoming a master luthier. Shizuku begs Seiji to play violin for her, but he only agrees on the condition that she sing along. Seiji and a mortified Shizuku perform "Take Me Home, Country Roads", accompanied by Nishi and his friends. Seiji is revealed to be the grandson of Nishi, and Shizuku and Seiji finally befriend each other.
Days after the two meet, Seiji leaves for Cremona, Italy, for a two-month study with a master violin-maker, but not before admitting that he admires Shizuku’s talents and that he had been checking out a large number of books in the hopes that she would eventually notice him. Inspired by Seiji pursuing his dream, Shizuku resolves to test her talents as a creative writer. She decides to pursue her writing seriously during the two months. She asks Nishi if she can write a story featuring the Baron, to which Nishi grants his consent in exchange for being the first to read her finished story.
Shizuku concocts a fantasy story featuring herself as the female protagonist, the Baron as the male hero who is looking for his lost love, Louise, and the cat she followed from the train (a neighborhood stray who is, among other names, known as "Moon" and "Muta") as the story's villain who took Louise from the Baron. Devoting her time to her writing, Shizuku eats snacks, stays up until early in the morning, and her school grades drop drastically. Shizuku argues with her family over her grades and whether she will even attend high school. As she continues to push herself into finishing the story before Seiji returns, her anxiety mounts and she begins to lose heart.
When her story is complete, Shizuku delivers the manuscript to Nishi. After Nishi reads Shizuku's writing and gives her his honest assessment, she breaks down in tears as the stress of the last two months finally turns into relief. Consoling her with udon, Nishi tells Shizuku the real-life story of the Baron. Nishi reveals that when he studied in Germany in his youth, he found his first love, a young woman named Louise. Nishi discovered the twin statuettes of the Baron and his female companion in a cafe, but as the female statuette was away for repairs, the shopkeeper would only allow Nishi to buy the Baron if Louise agreed to hold onto the Baron's companion so that they could be reunited once Nishi returned to Germany. However, the two lovers and their cat statues were separated during World War II and Nishi could find no trace of Louise afterward. In the English dub, Seiji tells Shizuku that the Baron has a missing partner during her second visit to the shop, but in the Japanese dialogue Seiji says his grandfather won't tell anyone the story behind the Baron, and it is through coincidence, intuition, or serendipity that Shizuku invents a missing partner for him (much to Nishi's surprise).
Deciding she needs to learn more about writing, and that she does want to attend high school after all, Shizuku is returned home by Nishi and announces to her mother that she will resume studying for her high school entrance exams full-time. Early in the next morning, she wakes up and looks outside her window to see Seiji on his bicycle. He has returned a day early. In the English dub, Seiji tells Shizuku that he decided to finish high school before returning to Cremona to become a luthier, but in the Japanese dialogue, Seiji says he will return to Cremona after middle school graduation as planned.
The two ride Seiji's bike to a lookout and watch the sun rise over the city, where Seiji professes his love for Shizuku and proposes that they marry at some point in the future; she happily accepts.


== Voice cast ==


== Background ==

Whisper of the Heart was based on the manga Mimi o Sumaseba which was originally created by Aoi Hiiragi. The manga was serialized in Shueisha's shōjo manga magazine Ribon between August and November 1989, and a single tankōbon volume was released on February 20, 1990. The volume was reprinted on July 15, 2005. A second manga by the same author titled Mimi o Sumaseba: Shiawase na Jikan was serialized in Shueisha's Ribon Original in August 1995 and released in a single volume on February 20, 1996. A spiritual sequel to this film adaption, The Cat Returns, was turned back into a manga by Aoi Hiiragi, under the name Baron: Neko no Danshaku.


== Production ==
During production, the backgrounds in the fantasy sequences of the film were drawn by Naohisa Inoue and the woodcut of the imprisoned violin-maker was created by Miyazaki's son Keisuke Miyazaki, a professional engraver. Japanese musical duo Chage and Aska's short music video, titled "On Your Mark", by Studio Ghibli was released along with Whisper of the Heart.


== Music ==

The film score of Whisper of the Heart was composed by Yuji Nomi. At times during the film, Shizuku translates John Denver's song "Take Me Home, Country Roads" to Japanese for her school's chorus club. She writes her own humorous Japanese version of the song, called "Concrete Road," about her hometown in western Tokyo. The songs were actually translated by producer Toshio Suzuki's daughter Mamiko with Hayao Miyazaki writing supplemental lyrics. These songs play a role at points in the story. A recording of "Take Me Home, Country Roads," performed by Olivia Newton-John, plays during the film's opening sequence. The song was also performed by Shizuku's voice actress Yoko Honna.


== Filming location ==
The movie is set around Seiseki-Sakuragaoka station in Tama city, Tokyo, where Shizuku goes up and down stairs and where she and Seiji declare their love on top of the hill near the station. There are paper fortunes at the shrine where this scene takes place. There are three shops where fans of the movie go to meet.


== Release ==
Whisper of the Heart was released in Japan on July 15, 1995, as the first film in the country to use the Dolby Digital sound format. An English dub of this film was released by Walt Disney Pictures on March 7, 2006. Turner Classic Movies televised both the dubbed and subbed versions on January 19, 2006 as part of their month-long celebration of Miyazaki in honor of his birthday, January 5. The English title, Whisper of the Heart, was created by Studio Ghibli and used on several officially licensed "character goods" released around the same time as the film was released in theaters in Japan. The North American Blu-ray was released by Walt Disney Studios Home Entertainment on May 22, 2012, alongside Castle in the Sky and The Secret World of Arrietty. GKIDS re-issued the film on Blu-ray and DVD on January 16, 2018 under a new deal with Studio Ghibli.


== Reception ==
Whisper of the Heart  was the highest-grossing Japanese film on the domestic market in 1995, earning ¥1.85 billion in distribution income, and grossing ¥3.15 billion in total box office revenue.Whisper of the Heart received very positive reviews from film critics. The review aggregator Rotten Tomatoes reported that 94% of critics have given the film a positive review based on 16 reviews, with an average rating of 7.59/10. On Metacritic, the film has a weighted average score of 75 out of 100 based on 4 critic reviews, indicating "generally favorable reviews". Time Out London included Whisper of the Heart in their Top 50 Animated Film list. It was also included in Film4's Top 25 Animated Film list. On Anime News Network, Michael Toole gave it an overall grade of A−, calling it "beautiful and evocative; a fine tale of adolescent yearning and aspiration."General producer and screenwriter Hayao Miyazaki defended the film's ending, saying that it was his idea. Miyazaki wanted Shizuku and Seiji to "commit to something."


== Spin-off ==
Over the course of the film, Shizuku is working on a fantasy novel that revolves around a cat figurine, named The Baron, which she discovers in Mr. Nishi's antique store. In 2002, Studio Ghibli produced a spin-off film The Cat Returns, directed by Hiroyuki Morita and again featuring The Baron, and the stray cat, Muta, in the film. Later on, Muta and the crow (Toto, who is friends with him and the Baron) seem to appear in The Secret World of Arrietty as two skirmishing animals.


== Sequel ==
In January 2020, Sony Pictures Entertainment announced that there will be a live-action sequel. The film will star Nana Seino as Shizuku and Tori Matsuzaka as Seiji. Yūichirō Hirakawa will direct.


== References ==


== External links ==
Official website
Whisper of the Heart on IMDb
Mimi o Sumaseba (Whisper of the Heart) at The Big Cartoon DataBase
Whisper of the Heart (film) at Anime News Network's encyclopedia