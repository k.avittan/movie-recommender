The Masked Rider is the primary mascot of Texas Tech University. It is the oldest of the university's mascots still in existence today. Originally called "Ghost Rider", it was an unofficial mascot appearing in a few games in 1936 and then became the official mascot with the 1954 Gator Bowl. The Masked Rider has led the team onto the field at nearly every football game since. It is the nation's first school mascot to feature a live horse at a football game, ahead of  Florida State's Chief Osceola and Renegade and 25 years before USC's Traveler and all other such mascots in existence today.
After learning of the Masked Rider, other schools emulated the idea of a mounted mascot.  Florida State began their tradition in 1978, immediately after seeing Texas Tech's live mascot at the 1977 Tangerine Bowl that pitted the two. The Oklahoma State Cowboys copied the Masked Rider in 1984 when Eddy Finley, a Texas Tech alumnus became an Oklahoma State University agricultural education professor, and started the Spirit Rider Program when both schools were still in separate conferences.The Masked Rider is adorned from head to toe in black, including a black gaucho hat and a black mask. The only other color present is the scarlet rider's cape. The current horse is also black, a tradition for the last 40 years, although horses prior to the 70s were on occasion other colors.
Students must pass a rigorous interview and testing process in order to be selected for this honor by the Masked Rider Advisory Committee. The Masked Rider is available for public appearances for no fee, though a donation is encouraged.
In 2000, the Masked Rider tradition was commemorated with the unveiling of a sculpture outside Frazier Alumni Pavilion on Texas Tech's campus. The Grant Speed crafted sculpture is 25 percent larger than life. In August 2013, the statue was wrapped in black Crêpe paper to mourn the death of the first Masked Rider, Joe Kirk Fulton.


== History ==

In 1936, the first rider, George Tate (class of 1937), led the football team onto the football field then left the field. Tate, whose identity was kept a secret at the time, was wearing a scarlet satin cape made by the Home Economics Department. He had borrowed a horse from the Tech barn as a prank. Tate was quoted in the November 4, 1984, issue of The Dallas Morning News as saying that Arch Lamb, who was then the head yell leader of the Saddle Tramps, "dreamed up this Red Raider thing." The prank was pulled a few more times that season but didn't surface again until the 1950s, when another Tech student was approached about creating a mascot.In 1953, Texas Tech football coach DeWitt Weaver approached a student named Joe Kirk Fulton about becoming the Masked Rider. DeWitt's Red Raiders were 10-1-0 in football and headed to Jacksonville, Florida for the Gator Bowl. At the time, Texas Tech was hoping to be invited to join the Southwest Conference. All the other teams had a mascot, and it is thought that DeWitt believed creating a mascot for Texas Tech might aid the school's chances for admission into the conference. Fulton agreed to ride a horse named Blackie in the bowl game.
Texas Tech's Center for Campus Life explains:

According to reports from those present at the 1954 Gator Bowl, the crowd sat in stunned silence as they watched Fulton and Blackie rush onto the football field, followed by the team. After a few moments of stunned disbelief, the silent crowd burst into cheers. Ed Danforth, a writer for the Atlanta Journal and a press box spectator later wrote, "No team in any bowl game ever made a more sensational entrance."

Beginning in the early 1960s, the Texas Tech marching band has played a composition written expressly for the mascot. "Ride, Raider, Ride" (commonly known as The Horse Music) is performed in quick time as the horse is galloped around the stadium sidelines. The highly spirited tune was composed by faculty member Richard Tolley, Professor of trumpet and Associate Director of Bands, 1959-1991.


=== Incidents ===

In 1963, the horse, Tech Beauty, was kidnapped and spray-painted with the letters "AMC" prior to Tech's football game against rival Texas A&M.
In 1974, the selection of the first female Masked Rider, Ann Lynch, caused widespread controversy.
In 1975, the horse was kidnapped and received chemical burns after being painted with orange paint prior to Tech's football game against Texas.
In 1982, the Masked Rider was involved in injuring an opposing school's cheerleader. Ten years later, the Masked Rider was involved in the injuring of a referee.
On September 3, 1994, an accident involving the Masked Rider resulted in the death of Texas Tech's animal mascot, a black American Quarter Horse named Double T during a Lubbock football game between Texas Tech and the New Mexico Lobos. After a 3rd quarter score by Tech, then Masked Rider, Amy Smart, fell from the horse after the horse's saddle broke during the customary post-score gallop around the stadium field, and the horse eventually ran unaccompanied towards the exit tunnel where it accidentally collided with the stadium wall dying instantly.
In 2001, the Masked Rider horse trailer was involved in a car accident. The horse, Black Phantom Raider, sustained serious injuries which led to his euthanization.
In 2006, the Masked Rider appeared as No. 24 on the CollegeFootballNews.com list of "College Football's 25 Greatest Mascots".
The horse, Midnight Matador, served as the Masked Rider's mount from 2002–2012. Midnight Matador was retired during the 2012 season following a leg injury, having been the longest serving at the position in school history.


== Raider Red ==

Around the 1971 football season, the Southwest Conference created a rule forbidding the bringing of live animal mascots to away games unless the host school allowed it. Since the Masked Rider's horse might be prohibited from traveling to some games under this rule, an alternate mascot named Raider Red was created; Raider Red is a person wearing a normal mascot costume.


== References ==


== External links ==
Texas Tech Center for Campus Life: The Masked Rider
Texas Tech Official Athletics Site: The Masked Rider Tradition
Film of The Masked Rider at the 1954 Gator Bowl