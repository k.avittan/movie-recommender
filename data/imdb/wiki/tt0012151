Mary Loretta Philbin (July 16, 1902 – May 7, 1993) was an American film actress of the silent film era, who is best known for playing the roles of Christine Daaé in the 1925 film The Phantom of the Opera  opposite Lon Chaney, and as Dea in The Man Who Laughs. Both roles cast her as the beauty in Beauty and the Beast-type stories.


== Early life ==
Philbin was born on July 16, 1902 in Chicago, Illinois, into a middle-class Irish American family and raised Catholic. Her father, John Philbin, was born in Ballinrobe, County Mayo, Ireland.


== Career ==
Philbin began her acting career after winning a beauty contest sponsored by Universal Pictures in Chicago. After she moved to California, Erich von Stroheim signed her to a contract with Universal, deeming her a "Universal Super Jewel."She made her screen debut in 1921, and the following year was honored at the first WAMPAS Baby Stars awards, a promotional campaign sponsored by the Western Association of Motion Picture Advertisers in the United States, which annually honored young women whom they believed to be on the threshold of movie stardom.
During the 1920s, Philbin starred in a number of high-profile films, most notably in D. W. Griffith's 1928 film Drums of Love. In 1927, she appeared in the horror film Surrender, though her most celebrated role was in the Universal horror film The Phantom of the Opera in 1925. Philbin's ethereal screen presence was noted in a 1924 edition of Motion Picture Classic, in which she was referred to as "one of the astonishing anomalies of motion pictures...Pat O'Malley once said of her: "If I were superstitious I would think that the spirit of some great tragedienne of a forgotten past slipped into Mary's soul."Philbin played a few parts during the early talkie era and most notably dubbed her own voice when The Phantom of the Opera was given sound and re-released. She retired from the screen in the early 1930s and devoted her life to care for her aging parents.


== Later life and death ==
Philbin spent the remainder of her life after leaving the film industry as a recluse, living in the same home in Huntington Beach, California. She never married and rarely made public appearances. One rare public appearance by Philbin occurred in her later years at the Los Angeles opening of the Andrew Lloyd Webber musical The Phantom of the Opera.
She died of pneumonia at age 90 in 1993 and was buried at the Calvary Cemetery in east Los Angeles, California.


== Filmography ==


== Notes and references ==


=== Notes ===


=== References ===
Beck, Calvin Thomas (1978). Scream Queens: Heroines of the Horrors. Macmillan. ISBN 978-0-020-12140-4.CS1 maint: ref=harv (link)
Sanchez, Nellie Van de Grift (1930). California and Californians. 3. The Lewis Publishing Co.CS1 maint: ref=harv (link)
Slide, Anthony (2002). Silent Players: A Biographical and Autobiographical Study of 100 Silent Film Actors and Actresses. University Press of Kentucky. ISBN 978-0-813-12249-6.CS1 maint: ref=harv (link)


== External links ==

Mary Philbin on IMDb
Mary Philbin at the American Film Institute catalog
Mary Philbin at Golden Silents
Mary Philbin at Find a Grave
Photographs and literature