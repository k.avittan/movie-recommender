In Greek mythology, Niobe (; Greek: Νιόβη [ni.óbɛː]) was a daughter of Tantalus and of either Dione, the most frequently cited, or of Eurythemista or Euryanassa, the wife of Amphion and the sister of Pelops and Broteas.
Her father was the ruler of a city located near Manisa in today's Aegean Turkey that was called "Tantalis" or "the city of Tantalus", or "Sipylus". The city was located at the foot of Mount Sipylus and its ruins were reported to be still visible in the beginning of the 1st century AD, although few traces remain today. Pliny reports that Tantalis was destroyed by an earthquake and the city of Sipylus (Magnesia ad Sipylum) was built in its place.Niobe's father is referred to as "Phrygian" and sometimes even as "King of Phrygia", although his city was located in the western extremity of Anatolia where Lydia was to emerge as a state before the beginning of the first millennium BC, and not in the traditional heartland of Phrygia, situated more inland. References to his son and Niobe's brother as "Pelops the Lydian" led some scholars to the conclusion that there would be good grounds for believing that she belonged to a primordial house of Lydia.
Niobe's husband, Amphion was a son of Zeus and Antiope, twin brother to Zethus, a ruler of Thebes. Amphion became a great singer and musician after his lover Hermes taught him to play and gave him a golden lyre. Amphion married Niobe, the daughter of Tantalus, the Lydian king. 
She was already mentioned in Homer's Iliad which relates her proud hubris, for which she was punished by Leto, who sent Apollo and Artemis to slay all of her children, after which her children lay unburied for nine days while she abstained from food. Once the gods interred them, she retreated to her native Sipylus, "where Nymphs dance around the River Acheloos, and though turned to stone, she broods over the sorrows sent by the Gods". Later writers asserted that Niobe was wedded to Amphion, one of the twin founders of Thebes, where there was a single sanctuary where the twin founders were venerated, but in fact no shrine to Niobe.


== Central theme ==

Niobe boasted of her fourteen children, seven male and seven female (the Niobids), to Leto who only had two children, the twins Apollo and Artemis. The number varies in different sources. Her speech which caused the indignation of the goddess was rendered in the following manner:

It was on occasion of the annual celebration in honor of Latona and her offspring, Apollo and Diana [i.e Artemis] when the people of Thebes were assembled, their brows crowned with laurel, bearing frankincense to the altars and paying their vows, that Niobe appeared among the crowd. Her attire was splendid with gold and gems, and her face as beautiful as the face of an angry woman can be. She stood and surveyed the people with haughty looks. "What folly," said she, "is this! to prefer beings whom you never saw to those who stand before your eyes! Why should Latona be honored with worship rather than I? My father was Tantalus, who was received as a guest at the table of the gods; my mother was a goddess. My husband built and rules this city, Thebes; and Phrygia is my paternal inheritance. Wherever I turn my eyes I survey the elements of my power; nor is my form and presence unworthy of a goddess. To all this let me add, I have seven sons and seven daughters, and look for sons-in-law and daughters-in-law of pretensions worthy of my alliance. Have I not cause for pride? Will you prefer to me this Latona, the Titan's daughter, with her two children? I have seven times as many. Fortunate indeed am I, and fortunate I shall remain! Will any one deny this?

Using arrows, Artemis killed Niobe's daughters and Apollo killed Niobe's sons. According to some versions, at least one Niobid (usually Meliboea, along with her brother Amyclas in other renderings) was spared. Their father, Amphion, at the sight of his dead sons, either killed himself or was killed by Apollo for having sworn revenge. Devastated, Niobe fled back to Mount Sipylus and was turned into stone, and, as she wept unceasingly, waters started to pour from her petrified complexion. Mount Sipylus indeed has a natural rock formation which resembles a female face, and it has been associated with Niobe since ancient times and described by Pausanias. The rock formation is also known as the "Weeping Rock" (Turkish: Ağlayan Kaya), since rainwater seeps through its porous limestone.
After Niobe's overweening pride in her children, offending Apollo and Artemis, brought about her children's deaths, Amphion commits suicide out of grief; according to Telesilla, Artemis and Apollo murder him along with his children. Hyginus, however, writes that in his madness he tried to attack the temple of Apollo, and was killed by the god's arrows.
The only Niobid spared stayed greenish pale from horror for the rest of her life, and for that reason she was called Chloris (the pale one).


== Within Greek culture ==
In his archaic role as bringer of diseases and death, Apollo with his poison arrows killed Niobe's sons and Artemis with her poison arrows killed Niobe's daughters. This is related to the myth of the seven youths and seven maidens who were sent every year to the king Minos of Crete as an offering sacrifice to the Minotaur. Niobe was transformed into a stone on Mount Sipylus in her homeland of Phrygia, where she brooded over the sorrows sent by the gods. In Sophocles' Antigone the heroine believes that she will have a similar death.
The iconic number "seven" often appears in Greek legends, and represents an ancient tradition because it appears as a lyre with seven strings in the Hagia Triada sarcophagus in Crete during the Mycenean age. Apollo's lyre had also seven strings.


== In literature and fine arts ==


=== Literature ===

The story of Niobe, and especially her sorrows, is an ancient one. The context in which she is mentioned by Achilles to Priam in Homer's Iliad is as a stock type for mourning. Priam is not unlike Niobe in the sense that he was also grieving for his son Hector, who was killed and not buried for several days.
Niobe is also mentioned in Sophocles's Antigone where, as Antigone is marched toward her death, she compares her own loneliness to that of Niobe. Sophocles is said to have also contributed a play titled Niobe that is lost.
The Niobe of Aeschylus, set in Thebes, survives in fragmentary quotes that were supplemented by a papyrus sheet containing twenty-one lines of text. From the fragments it appears that for the first part of the tragedy the grieving Niobe sits veiled and silent.
Furthermore, the conflict between Niobe and Leto is mentioned in one of Sappho's poetic fragments ("Before they were mothers, Leto and Niobe had been the most devoted of friends.").In Latin language sources, Niobe's account is first told by Hyginus in his collection of stories in brief and plain Fabulae.
Parthenius of Nicaea records a rare version of the story of Niobe, in which her father is called Assaon and her husband Philottus. The circumstances in which Niobe loses her children are also different, see Niobids#Variant myth.
Niobe's iconic tears were also mentioned in Hamlet's soliloquy (Act 1, Scene 2), in which he contrasts his mother's grief over the dead King, Hamlet's father – "like Niobe, all tears" – to her unseemly hasty marriage to Claudius.The quotation from Hamlet is also used in Dorothy L. Sayers' novel Murder Must Advertise, in which an advertising agency's client turns down an advertisement using the quotation as a caption.In William Faulkner's novel Absalom, Absalom! Faulkner compares Ellen, the wife of Sutpen and father of Henry and Judith, to Niobe, "this Niobe without tears, who had conceived to the demon [Sutpen] in a kind of nightmare" (Chapter 1).
Among works of modern literature which have Niobe as a central theme, Kate Daniels' Niobe Poems can be cited.


=== Arts ===
The subject of Niobe and the destruction of the Niobids was part of the repertory of Attic vase-painters and inspired sculpture groups and wall frescoes as well as relief carvings on Roman sarcophagi.
The subject of the Attic calyx-krater from Orvieto conserved in the Musée du Louvre has provided the name for the Niobid Painter.A lifesize group of marble Niobids, including one of Niobe sheltering one of her daughters, found in Rome in 1583 at the same time as the Wrestlers, were taken in 1775 to the Uffizi in Florence where, in a gallery devoted to them, they remain some of the most prominent surviving sculptures of Classical antiquity (see below). New instances come to light from time to time, like one headless statue found in early 2005 among the ruins of a villa in the Villa dei Quintili just outside Rome.In painting, Niobe was painted by post-Renaissance artists from varied traditions (see below). An early appearance, The Death of Niobe's Children by Abraham Bloemaert, was painted in 1591 towards the start of the Dutch Golden Age. The English artist Richard Wilson gained great acclaim for his The Destruction of the Children of Niobe, painted in 1760. Three notable works, all dating from the 1770s, Apollo and Diana Attacking Niobe and her Children by Anicet-Charles-Gabriel Lemonnier, The Children of Niobe Killed by Apollo and Diana by Pierre-Charles Jombert and Diana and Apollo Piercing Niobe’s Children with their Arrows by Jacques-Louis David belong to the tradition of French Baroque and Classicism.
Niobe is an abstract painting by Károly Patkó.In classical music, Italian composer Agostino Steffani (1654 – 1728) dedicated his opera "Niobe, Queen of Saba" to her myth.Benjamin Britten based one of his Six Metamorphoses after Ovid on Niobe.
In modern music, Caribou called the last track on his 2007 album Andorra "Niobe".
In modern dance, José Limón named a section of his dance theater work Dances for Isadora as "Niobe". The section is a solo for a woman mourning the loss of her children.
A marble statue of Niobe is a female lead character in a long-running 1892 farce Niobe (play) by Harry Paulton. In the play she is bought to life by a quaint electrical storm and brings the Edwardian values and relationships in the household to disarray. The season at the London Royal Strand Theatre enjoyed more than five hundred performances. The play is the subject of a musical dedication by Australian composer Thomas Henry Massey. The play was filmed in 1915.


=== Examples in painting and sculpture ===

		
		
		
		
		
		
		


== Related terms ==
The choice of "Niobe" simply as a name in works of art and literature is not uncommon either. Two minor characters of Greek mythology have the same name (see Niobe (disambiguation)) and the name occurs in several works of the 19th century. More recently, one of the characters in the films The Matrix Reloaded and The Matrix Revolutions was also named Niobe. A character named Niobe also appeared in the Rome TV series.
The element niobium was so named as an extension of the inspiration which had led earlier to the naming of the element tantalum by Anders Gustaf Ekeberg. On the basis of his argument according to which there were two different elements in the tantalite sample, Heinrich Rose named them after children of Tantalus—niobium and pelopium—although the argument was later contested as far as pelopium was concerned.
A mountain in British Columbia, Canada is named Mount Niobe.
Four successive ships of the British Royal Navy were called HMS Niobe.


== See also ==
Aedon
Cassiopeia (disambiguation)


== Sources ==


=== Modern scholarship ===


=== Classical authors ===


=== General reading ===


== References ==