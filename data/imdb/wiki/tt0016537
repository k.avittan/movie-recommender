Susan Ellis Wild (born June 7, 1957) is an American attorney and politician from the commonwealth of Pennsylvania. A Democrat, she is a member of the United States House of Representatives from Pennsylvania's 7th congressional district. The district is located in the heart of the Lehigh Valley, and includes Allentown, Bethlehem, and Easton. She spent the last two months of 2018 as the member for Pennsylvania's 15th congressional district after Charlie Dent resigned in 2018.


== Early life and career ==
Wild is the daughter of Norman Leith and Susan Stimus Ellis. Wild's mother was a journalist. Her father served in the United States Air Force during World War II and the Korean War. She was born in Wiesbaden Air Force Base, West Germany, while her father was stationed there. She also lived in France, California, New Mexico, and Washington D.C.Wild volunteered on Jimmy Carter's 1976 presidential campaign. She graduated from American University in 1978. She earned her Juris Doctor at the George Washington University Law School in 1982. She studied under John Banzhaf. Wild became a partner at the law firm Gross McGinley in 1999.


== U.S. House of Representatives ==


=== Elections ===


==== 2018 general election ====
Wild ran for Lehigh County Commissioner in 2013, but lost. She was appointed the first female solicitor of Allentown, Pennsylvania in January 2015. She served as Solicitor of Allentown from January 7, 2015, when she was confirmed by the Allentown City Council, until December 31, 2017, when she resigned from office to pursue her candidacy for the United States House of Representatives to succeed retiring U.S. Rep. Charlie Dent (R) in 2018.In the 2018 elections, Wild ran for the United States House of Representatives in Pennsylvania's 7th congressional district. That district had previously been the 15th, represented by seven-term Republican Charlie Dent. She won the Democratic Party primary election and faced Republican Lehigh County commissioner Marty Nothstein in the November 6 general election. She defeated Nothstein in the general election. When the final precincts were counted, Wild received 53.4% of the vote.


==== 2018 special election ====
On the same day, Wild also ran in a separate special congressional election for the balance of Dent's term; he had resigned in May after announcing the previous fall that he would not run for reelection. On November 15, 2018, it was announced that Wild had won the 15th congressional district's special election, receiving 130,353 votes to Nothstein's 129,593 votes.The closer margin in the special election came because it was run under the old lines that had been thrown out by the Pennsylvania Supreme Court in February 2018. The old 15th had stretched from the Lehigh Valley into heavily Republican territory between Lebanon and Harrisburg, by way of a tendril in Berks County. The new 7th is a somewhat more compact district centered in the Lehigh Valley, and includes a sliver of the Poconos.


=== Tenure ===
Upon taking office, Wild became the first Democrat to represent the Lehigh Valley since 1999. She had two months' more seniority than the rest of the large Democratic freshman class of 2018. She was one of four Democratic women elected from Pennsylvania in 2018. The others were Mary Gay Scanlon, Madeleine Dean and Chrissy Houlahan. The state's congressional delegation had previously been all male.Wild has been critical of Brazil's president Jair Bolsonaro, a regular target of such left-leaning publications such as Vox for views characterized as 'far-right', 'misogynistic', 'homophobic' and 'anti-immigrant'. In March 2019, Wild and 29 other Democratic lawmakers wrote a letter to Secretary of State Mike Pompeo. The letter read in part, "Since the election of far-right candidate Jair Bolsonaro as president, we have been particularly alarmed by the threat Bolsonaro’s agenda poses to the LGBTQ+ community and other minority communities, women, labor activists, and political dissidents in Brazil. We are deeply concerned that, by targeting hard-won political and social rights, Bolsonaro is endangering Brazil’s long-term democratic future."


=== Donald Trump impeachment ===
On December 10, 2019, Democrats on the House Judiciary Committee advanced two articles of impeachment against Republican President Donald Trump. On December 18, 2019, Wild voted 'Yes' on the first article of impeachment, "abuse of power", and 'Yes' on the second article of impeachment, "obstruction of Congress".


=== Committee assignments ===
Committee on Foreign Affairs
Subcommittee on Africa, Global Health, Global Human Rights and International Organizations (Vice Chair)
Subcommittee on Europe, Eurasia, Energy, and the Environment
Committee on Education and Labor
Subcommittee on Health, Employment, Labor, and Pensions
Committee on Ethics


=== Caucus memberships ===
New Democrat Coalition
Climate Change Task Force(Co-Chair)
Servicewomen and Women Veterans Congressional Caucus
House Sustainable Energy and Environment Coalition
Bipartisan Heroin and Opioid Task Force
Freshman Working Group on Addiction
Blue Collar Caucus
Congressional Labor and Working Families Caucus
Middle Class Jobs Caucus
Congressional Homelessness Caucus
Congressional Caucus on Foster Youth
Bipartisan Task Force for Combatting Anti-Semitism
Congressional LGBT Equality Caucus
Congressional Caucus on Maternity Care
Black Maternal Health Caucus
Congressional Baby Caucus
House Nursing Caucus
Congressional Autism Caucus
Congressional Diabetes Caucus
Congressional Native American Caucus
National Heritage Area Caucus
Congressional Animal Protection Caucus
Congressional Ukrainian Caucus
Congressional Hellenic-Israel Alliance (CHIA) Caucus
Congressional Humanities Caucus
Bipartisan Public Broadcasting Caucus
House Small Brewers Caucus
Congressional Candy Caucus
Congressional Freethought Caucus


== Electoral history ==


== Personal life ==
Wild and her husband, Russell Wild, divorced in 2003 after 22 years of marriage. They have two adult children, Clay and Adrienne. Following her divorce, Wild reunited with her law school boyfriend, Kerry Acker, who remained her life partner until his death on May 25, 2019. She lives in South Whitehall Township, west of Allentown. She is Jewish.


== See also ==
List of Jewish members of the United States Congress
Women in the United States House of Representatives


== References ==


== External links ==
Congresswoman Susan Wild official U.S. House website
Susan Wild for Congress official campaign websiteBiography at the Biographical Directory of the United States Congress
Profile at Vote Smart
Financial information (federal office) at the Federal Election Commission
Legislation sponsored at the Library of Congress
Appearances on C-SPAN