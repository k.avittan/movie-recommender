Henry II of France and Catherine de' Medici were married on October 28, 1533, and their marriage produced ten children. Henry and Catherine became the ancestors of monarchs of several countries.


== Background, pedigree and family ties ==
Both Henry and Catherine were from illustrious families and had notable people in their respective family trees.


=== Henry II of France ===
Henry's father was Francis I of France, the patron of Leonardo da Vinci and a member of the Valois-Angoulême branch of the House of Valois. His maternal grandfather was Louis XII of France, the conqueror of the Neapolitan Kingdom of Naples and the Duchy of Milan. Both Kings of France were descendants (in the male line) of Charles V, King of France, and as such represented, as the only extant line of the House of Valois, the descent of Charles, Count of Valois, fourth son of King Philip III of France, from the House of Capet.
Henry's maternal grandmother was Anne of Brittany, the defiant Duchess Regent of Brittany and Queen of France twice over. Her grandmother was Eleanor of Navarre, the daughter of John II of Aragon and half-sister of Ferdinand II of Aragon. Therefore, this made Henry a distant half-cousin of his son-in-law Philip II of Spain. This also made him a second cousin once-removed of his other son-in-law, Henry IV of France, who was also a distant cousin through their common male-line ancestor, St. Louis of France.


=== Catherine de' Medici ===
Catherine's father and grandfather were Lorenzo II de' Medici, Duke of Urbino and Piero the Unfortunate respectively. The latter was a son of Lorenzo il Magnifico, the great Renaissance ruler of Florence. This made Catherine a scion of the illustrious House of Medici. Also her guardian till her marriage was Pope Clement VII, formerly Giulio De'Medici, illegitimate son of Giuliano De'Medici, brother of Lorenzo il Magnifico. Therefore, from her father's side, Catherine had high Italian ancestry.
Her mother was from the House of La Tour d'Auvergne, her father being the last Count of Boulogne from the aforementioned House. Her maternal grandmother Jeanne de Bourbon-Vendôme was a direct patrilineal descendant of Louis, the first duke of Bourbon. This made Catherine a direct descendant of Louis IX of France, and of the House of Capet, albeit through a female line. Also this made her a 7th cousin, once removed of her husband Henry and a 6th cousin, 3 times removed of Henri of Navarre, her son-in-law.


== Children ==
During their marriage, the royal couple faced health problems with their children: their fourth child, Louis, died when he was a baby and Catherine almost died giving birth to her youngest children who were the twins, Joan and Victoria. After their birth the doctors told Henry and Catherine that their marriage should not produce more children.
The following table lists all the children of Henry and Catherine.


== Descendants of Elizabeth of Valois ==


=== Progeny of Isabella Clara Eugenia of Spain ===


=== Progeny of Catherine Michelle of Spain ===


== Descendants of Claude of Valois ==


=== Progeny of Henry II, Duke of Lorraine ===


=== Progeny of Christine of Lorraine ===


=== Progeny of Francis II, Duke of Lorraine ===


== See also ==
Descendants of Louis XIV of France
Descendants of Philippe I, Duke of Orléans
Descendants of Philip V of Spain
Descendants of Charles III of Spain
Descendants of Charles I of England