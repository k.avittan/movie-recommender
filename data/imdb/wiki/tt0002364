Eugenie Besserer (December 25, 1868 – May 28, 1934) was an American actress who starred in silent films and features of the early sound motion-picture era, beginning in 1910. Her most prominent role is that of the title character's mother in the first talkie film, The Jazz Singer.


== Early life ==
Born in Watertown, New York, to French Canadian parents, she was taken by her parents to Ottawa, Ontario, as a girl, and spent her childhood there. She was left an orphan and escaped from her guardians at the age of 12. She came to New York City and arrived at Grand Central Station with only 25 cents (Canadian currency, equivalent to US$0.34 at the time) in her pocket. She managed to locate a former governess, with the assistance of a street car conductor, who helped Eugenie locate an uncle, with whom she lived. She continued her education there.


== Career ==
Besserer's initial theatrical experience came with McKee Rankin when the producer had Nance O'Neill as a star. Soon, she appeared with stage luminaries such as Frank Keenan and Wilton Lackaye. As a youth, she played a juvenile part with Maurice Barrymore. She performed a season at Pike's Opera House in Portland, Oregon. Another season, Eugenie acted in a drama opposite Henry Kolker. The illness of her sister brought her to the West Coast, and she came to Hollywood in 1910 when films were just starting to be made.
In motion pictures, Eugenie was usually cast in mother roles, most famously as the mother of Al Jolson's character in The Jazz Singer. Eugenie became associated with the Selig Polyscope Company. A significant part for the actress was her role as Aunt Ray Innis in The Circular Staircase (1915), based on the novel by Mary Roberts Rinehart.


== Personal life ==
She shared a home with her husband, Albert W. Hegger, an art dealer, from the time she came to Los Angeles. They lived in a hilltop home above Silver Lake. They had one daughter.


== Death ==
Eugenie Besserer died in 1934, aged 65, from a heart attack at her home. A funeral mass was held at St. Theresa's Church, with a rosary service at Edwards Brothers Colonial Mortuary, Venice Boulevard, in Los Angeles. She is buried in Calvary Cemetery, East Los Angeles.


== Filmography ==


=== 1930s ===
To the Last Man (1933) as Granny Spelvin
Six Hours to Live (1932) (uncredited) in undetermined role
Scarface (1932) (uncredited) as citizens committee member
Du Barry, Woman of Passion (1930) (uncredited) as Rosalie/prison matron
In Gay Madrid (1930) (as Eugenia Besserer) as Doña Generosa
A Royal Romance (1930) as Mother


=== 1920s ===
Seven Faces (1929) as Madame Vallon
Mister Antonio (1929) as Mrs. Jorny
Illusion  (1929) as Mrs. Jacob Schmittlap
Fast Company (1929) as Mrs. Kane
Speedway (1929) as Mrs. MacDonald
Whispering Winds (1929) as Jim's mother
Madame X (1929) as Rose, Floriot's servant
Thunderbolt (1929) as Mrs. Morgan
The Bridge of San Luis Rey (1929) as a nun
Mister Antonio (1929) as Mrs. Jorny
A Lady of Chance (1928) as Mrs. 'Ma' Crandall
Lilac Time (1928) as Madame Berthelot
Yellow Lily (1928) as Archduchess
Two Lovers (1928) as Madame Van Rycke
Drums of Love (1928) as Duchess de Alvia
The Jazz Singer (1927) as Sara Rabinowitz
Slightly Used (1927) as Aunt Lydia
Captain Salvation (1927) as Mrs. Buxom
When a Man Loves (1927) as the landlady
The Night of Love (1927) (uncredited) as gypsy
Wandering Girls (1927) as Peggy's mother
Flesh and the Devil (1926) as Leo's mother
The Fire Brigade (1926) as Mrs. O'Neil
The Millionaire Policeman (1926) as Mrs. Gray
Winning the Futurity (1926) as Mary Allen
Kiki (1926) (uncredited) as landlady
The Skyrocket (1926) as wardrobe mistress
Bright Lights (1925) as Patsy's mother
Wandering Footsteps (1925) as Elizabeth Stuyvesant Whitney
The Circle (1925) as Lady Catherine "Kitty" Cheney
The Coast of Folly (1925) as nanny
Confessions of a Queen (1925) as Elanora
Friendly Enemies (1925) as Marie
A Fool and His Money (1925) as Mother
The Price She Paid (1924) as Mrs. Elton Gower
Bread (1924) as Mrs. Sturgis
Enemies of Children (1923)
Anna Christie (1923) as Marthy
The Rendezvous (1923) as Nini
Her Reputation (1923) as Madame Cervanez
The Lonely Road (1923) as Martha True
The Strangers' Banquet (1922) as Mrs. McPherson
June Madness (1922) as Mrs. Whitmore
The Hands of Nara (1922) as Mrs. Claveloux
Kindred of the Dust (1922) as Mrs. McKaye
Penrod (1922)
The Rosary (1922) as Widow Kathleen Wilson
The Light in the Clearing (1921)
Molly O' (1921) as Antonia Bacigalupi
The Sin of Martha Queed (1921) as Alicia Queed
Good Women (1921) as Mrs. Emmeline Shelby
The Breaking Point (1921) as Mrs. Janeway
What Happened to Rosa (1920) as Madame O'Donnelly
The Scoffer (1920) as Boorman's wife
45 Minutes from Broadway (1920) as Mrs. David Dean
Seeds of Vengeance (1920) as Judith Cree
Fickle Women (1920) as Mrs. Price
The Brand of Lopez (1920) as Señora Castillo
For the Soul of Rafael (1920) as Dona Luisa
The Gift Supreme (1920) as Martha Vinton
The Fighting Shepherdess (1920) as Jezebel


=== 1910s ===
The Greatest Question (1919) as Mrs. Hilton
Scarlet Days (1919) as Rosie Nell
Turning the Tables (1919) as Mrs. Feverill
Ravished Armenia (1919)
The Sea Flower (1918) as Kealani
The Road Through the Dark (1918) as Aunt Julie
The Eyes of Julia Deep (1918) as Mrs. Lowe
A Hoosier Romance (1918) as the squire's wife
The Still Alarm (1918) in undetermined role
The City of Purple Dreams (1918)
Little Orphant Annie (1918) as Mrs. Goode
Who Shall Take My Life? (1917) as Mrs. Munroe
The Curse of Eve (1917) as the Mother
The Witness for the State (1917)
Her Salvation (1917)
Little Lost Sister (1917) as Mrs. Welcome
The Crisis (1917) as Mrs. Brice
Beware of Strangers (1917) as Mary DeLacy
In After Years (1917)
The Garden of Allah (1916) as Lady Rens
Twisted Trails (1916) as Martha, the housekeeper
The Temptation of Adam (1916)
The Woman Who Did Not Care (1916)
A Social Deception (1916)
The Grinning Skull (1916)
Thou Shalt Not Covet (1916) as my wife
The Devil-in-Chief (1916)
I'm Glad My Boy Grew Up to Be a Soldier (1915) as Mrs. Warrington
Just as I Am (1915)
The Bridge of Time (1915)
The Circular Staircase (1915) as Aunt Ray
The Rosary (1915) as Widow Kelly
Ingratitude of Liz Taylor (1915)
The Carpet from Bagdad (1915) as Mrs. Chedsoye
Poetic Justice of Omar Khan (1915)
The Vision of the Shepherd (1915)
The Story of the Blood Red Rose (1914) as Queen of Urania
The Tragedy That Lived (1914)
Hearts and Masks (1914)
Ye Vengeful Vagabonds (1914)
The Man in Black (1914)
His Fight (1914)
Me an' Bill (1914)
The Fire Jugglers (1914)
The Salvation of Nance O'Shaughnessy (1914)
Elizabeth's Prayer (1914) as Hilda Crosby, an unscrupulous actress
Memories (1914) as Mary, Professor Scott's sweetheart
The Master of the Garden (1913)
Phantoms (1913) as Natalie Storm
The Probationer (1913) as Granny
Fate Fashions a Letter (1913)
The Acid Test (1913)
The Unseen Defense (1913)
In God We Trust (1913)
The Fighting Lieutenant (1913)
Woman: Past and Present (1913) as Grandmother America
A Flag of Two Wars (1913)
The Girl and the Judge (1913)
Wamba A Child of the Jungle (1913)
Indian Summer (1913)
Lieutenant Jones (1913)
In the Days of Witchcraft (1913)
Dollar Down, Dollar a Week (1913)
Love Before Ten (1913) as Mrs. Walters
Diverging Paths (1913)
The Spanish Parrot Girl (1913) as Mrs. Avery
The Governor's Daughter (1913)
The Ne'er to Return Road (1913)
The Last of Her Tribe (1912)
Sammy Orpheus; or, The Pied Piper of the Jungle (1912)
The Millionaire Vagabonds (1912) as Mrs. Knobhill
Opitsah: Apache for Sweetheart (1912)
The Vintage of Fate (1912)
Old Songs and Memories (1912)
His Wedding Eve (1912)
Monte Cristo (short) (1912) as Mercedes
The Substitute Model (1912)
The Indelible Stain (1912)
Sergeant Byrne of the Northwest Mounted Police (1912)
The Little Indian Martyr (1912)
His Masterpiece (1912)
The Lake of Dreams (1912)
In Exile (1912)
A Child of the Wilderness (1912)
The Hand of Fate (1912)
The End of the Romance (1912) as Alice Gray
Me an' Bill (1912)
The Junior Officer (1912)
As Told by Princess Bess (1912)
The Danites (1912)
Disillusioned (1912)
Bunkie (1912)
The Cowboy's Adopted Child (1912)
George Warrington's Escape (1911) as Madame Esmond Warrington
An Evil Power (1911)
The Bootlegger (1911)
Old Billy (1911)
The Blacksmith's Love (1911)
The Regeneration of Apache Kid (1911)
Their Only Son (1911)
Slick's Romance (1911)
The Profligate (1911)
It Happened in the West (1911)
The Craven Heart (1911)
A Sacrifice to Civilization (1911)
One of Nature's Noblemen (1911)
Stability vs. Nobility (1911)
The Still Alarm (1911)
The Mother (1911)
The Other Fellow (1911)
The Wonderful Wizard of Oz (1910) - (undetermined)


== References ==
The Los Angeles Times, "Eugenie Besserer Dead", May 31, 1934, p. A2
The Monessen, Pennsylvania Daily Independent, "Miss Eugenie Besserer Arrived In New York City With Just 25 Cents-Now A Star In Pictures", September 16, 1915, p. 3


== External links ==
Eugenie Besserer on IMDb
Eugenie Besserer at the Internet Broadway Database