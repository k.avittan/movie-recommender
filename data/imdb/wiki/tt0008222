A love letter is an expression of love in written form. However delivered, the letter may be anything from a short and simple message of love to a lengthy explanation of feelings. 


== History ==
One of the oldest references to a love letter dates to Indian mythology of more than 5000 years ago.  Mentioned in the Bhagavatha Purana, book 10, chapter 52, it is addressed by princess Rukmini to king Krishna and carried to him by her Brahmin messenger Sunanda.Examples from Ancient Egypt range from the most formal, and possibly practical – "The royal widow . . . Ankhesenamun wrote a letter to the king of the Hittites, Egypt's old enemy, begging him to send one of his sons to Egypt to marry her" – to the down-to-earth: Let me "bathe in thy presence, that I may let thee see my beauty in my tunic of finest linen, when it is wet". A fine expression of literary skill can be found in Imperial China: when a heroine, faced with an arranged marriage, wrote to her childhood sweetheart, he exclaimed, "What choice talent speaks in her well-chosen words . . . everything breathes the style of a Li T'ai Po. How on earth can anyone want to marry her off to some humdrum clod?"In Ancient Rome, "the tricky construction and reception of the love letter" formed the center of Ovid's Ars Amatoria or Art of Love: "The love letter is situated at the core of Ovidian erotics".
The Middle Ages saw the formal development of the Ars dictaminis, including the art of the love letter from opening to close. For salutations, "the scale in love letters is nicely graded from 'To the noble and discreet lady P., adorned with every elegance, greeting' to the lyrical fervors of 'Half of my soul and light of my eyes . . . greeting, and that delight which is beyond all word and deed to express'". The substance similarly "ranges from doubtful equivoque to exquisite and fantastic dreaming", rising to appeals for "the assurance 'that you care for me the way I care for you'".The love letter continued to be taught as a skill at the start of the eighteenth century, as in Richard Steele's Spectator. Perhaps in reaction, the artificiality of the concept came to be distrusted by the Romantics: "'A love-letter? My letter – a love-letter? It . . . came straight from my heart'".


=== 20th Century ===
The love letter continued to flourish in the first half of the twentieth-century – F Scott Fitzgerald gives us a 1920s Flapper 'absorbed in composing one of those non-committal, marvellously elusive letters that only a young girl can write'.Before the development of widespread means of telecommunications, letters were one of the few ways for a distant couple to remain in contact, particularly in wartime.  The strains on either end of such a relationship could intensify emotions and lead to letters going beyond simple communication to expressions of love, longing and desires.  It is claimed that the very act of writing can trigger feelings of love in the writer. Secrecy, delays in transit, and the exigencies of maneuvers could further complicate the communication between two parties, whatever their degree of involvement.  So precious could love letters be that even already read ones would even be brought into battle and read again for solace during a break in the action.  Others would defer, compartmentalizing their feelings and leaving a letter folded away where it would cause no pain.In the second half of the century, with the coming of a more permissive society - and the immediacy afforded by the technology of the Information Age, the nuanced art of the love letter became old-fashioned, even anachronistic.


== Today ==
However, with the arrival of the Internet and its iconic AOL notice "You've Got Mail", written expressions of love made a partial comeback - and provided the plot device for a highly successful 1998 romantic comedy by that name, You've Got Mail.  So far out of fashion had written love letters fallen that in the 2000s one can find websites where advice is given on how to write one.As ever, an advantage of written communication - being able to express thoughts and feelings as they come to a writer's mind - remains.  For some this is easier than doing so face to face.  Also, the very act of communicating a permanent expression of feelings to another conveys the importance of the writer's emotions to the recipient. 
In contrast, in mobile, Twitter or Tweet, "telegraphese" is widespread, and a sign-off such as "LOL! B cool B N touch bye" could have the ring of being composed by a "disinterested young mother".


== Form ==
A love letter has no specific form.  It can be lengthy, elaborate, and composed on scented stationary, or a few poignant words penciled on a scrap of paper or piece of bark.  What is communicated and how determine whether it is one or not.
The range of emotions expressed can span adulation to obsession, and include devotion, disappointment, grief and indignation, self-confidence, ambition, impatience, self-reproach and resignation.A love letter may take another literary form than simple prose.  A historically popular one was the poem, particularly in the form of a sonnet. William Shakespeare's are particularly cited. Structure and suggestions of love letters been examined in works such as the anthology Love Letters of Great Men and The Book of Love. Cathy Davidson, author of the latter, confesses that after reading hundreds of love letters for her collection, "The more titles I read, the less I was able to generalize about female versus male ways of loving or expressing that love".After the end of a relationship, returning love letters to the sender or burning them can be a release to their recipient, or intended to hurt their author. In the past return could also be a matter of honor, as a love letter, particularly from a lady, could be compromising or embarrassing, to the point where the use of "compromising letters...for blackmailing or other purposes" became a Victorian cliche.
While scented stationary for love letters is commercially available, some women prefer to use their own perfume to trigger emotions specifically associated with the being with them.


== See also ==
Hate mail
Poison pen letter
Sexting
Text messaging
Valentine's Day
Victorian letter writing guides


== References ==


== Further reading ==
Ursula Doyle, Love Letters of Great Women (2009)
Bill Shapiro, Other People's Love Letters (2007)


== External links ==