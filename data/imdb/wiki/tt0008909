"Annie Laurie" is an old Scottish song based on a poem said to have been written by William Douglas (1682?–1748) of Dumfriesshire, about his romance with Annie Laurie (1682–1764). The words were modified and the tune was added by Alicia Scott in 1834/5. The song is also known as "Maxwelton Braes".


== William Douglas and Annie Laurie ==
William Douglas became a soldier in the Royal Scots and fought in Germany and Spain and rose to the rank of captain. He also fought at least two duels. He returned to his estate at Fingland in 1694.
Annie Laurie was born Anna, on 16 December 1682, about 6 o'clock in the morning at Barjarg Tower, in Keir, near Auldgirth, Scotland, the youngest daughter of Robert Laurie, who became first baronet of Maxwellton in 1685.
Traditionally it is said that Douglas had a romance with Annie Laurie, but that her father opposed a marriage. This may have been because Anna was very young; she was only in her mid-teens when her father died.  It may also have been because of Douglas's aggressive temperament or more likely because of his Jacobite allegiances. It is known for certain that they knew of each because in a later letter by Anna she says in reply to news about Douglas, "I trust that he has forsaken his treasonable opinions, and that he is content."
Douglas recovered from this romance and eloped with a Lanarkshire heiress, Elizabeth Clerk of Glenboig. They married in Edinburgh in 1706. Douglas's political beliefs forced him into exile. He became a mercenary soldier and sold his estate at Fingland in the 1720s, though eventually he received a pardon.


== Anna Laurie's later life ==
In Edinburgh on 29 August 1709 Anna married Alexander Fergusson, 14th Laird of Craigdarroch. (Early editions of Brewer's are in error claiming her husband was James Ferguson, who was in fact her son.) She lived at Craigdarroch for 33 years.  Under her directions the present mansion of Craigdarroch was built, and a relic of her taste is still preserved in the formal Georgian gardens at the rear of the house. She died on 5 April 1764, at Friars' Carse, Dumfriesshire, Scotland, and some sources say she was buried at Craigdarroch.  Portraits of her exist at Maxwelton and at Mansfield, the seat of the Stuart-Monteiths.  The portraits show that she had blue eyes.


== Doubts about authorship ==
There has been some doubt that Douglas composed the poem.  The words of the second verse of the song may be based on an old version of John Anderson My Jo, to the tune of which song Annie Laurie was sometimes sung. The words were first recorded in 1823 in Sharpe's "Ballad Book", quite a long time after 1700.  The song therefore may have been written by Allan Cunningham, who invented contributions to Sharpe's book.  However Douglas is known to have written other verses and he also knew an Anna Laurie of Maxwelton. This seems to indicate he was the originator of some of the first verse at least.


== Lady John Scott's additions ==
In February 1890 Lady John Scott (1810–1900) (née Alicia Ann Spottiswoode) wrote to the editor of the Dumfries Standard, claiming that she had composed the tune and had written most of the modern words.  She said that around 1834-5 she encountered the words in collection of the Songs of Scotland (1825) by Allan Cunningham in a library.  She adapted the music she had composed for another old Scottish poem, Kempye Kaye. She also amended the first verse slightly, the second verse greatly, which she thought was unsuitable, and wrote a new third verse.  In the 1850s Lady John published the song with some other songs of hers for the benefit of the widows and orphans of the soldiers killed in the Crimean War. The song became popular and was closely associated with Jenny Lind.


=== Lady John Scott version ===
The earliest known version by Lady John was published by James Lindsay of Glasgow and is:

Notes
braes (a brae is a sloping bank of a river or sea-shore; a hill-slope)
bonnie means pretty
fa's means falls
gi'ed means gave
dee means die
snaw means snow
e'e means eye
gowans are daisies
o is of
simmer means summer
a is all


=== Original ===
The earliest known version, one that may be closest to what Douglas wrote, follows:

Notes
She's backit means "She's endowed with a back(side)"
She's breistit means "She's endowed with a breast"
jimp means elegant or slender
ye weel may span means that her waist could be encompassed with the span of two hands
a rolling eye is a 'come hither' lookThe song "Annie Laurie" also is mentioned in a poem, The Song of the Camp, by Bayard Taylor (1825–1878).


== Trivia ==
Haymarket Square martyr Albert Parsons sang his favorite song "Annie Laurie" in his prison cell on the day of his execution.
Winifred Bonfils (1863 – May, 1936.) Reporter, columnist writing as Winifred Black for Hearst's syndicate and as "Annie Laurie" for the San Francisco Examiner.
Annie Laurie's Kirk or Wee Kirk o' the Heather, Forest Lawn Memorial Park, Glendale, LA, California, is based on the now ruined old church in Glencairn near Moniave.
Annie Laurie is sung by the father in Betty Smith's novel A Tree Grows in Brooklyn. He sings it after he and his family moves to his last home and sees a piano of the previous owner of the flat. His wife later names their youngest daughter Annie Laurie after the song.  It is sung by actor James Dunn in the film version.
Annie Laurie is used as a distinguishing feature by the fictional main character Richard Hannay in John Buchan's novel The Thirty-Nine Steps (1915).
Bing Crosby included the song in a medley on his album 101 Gang Songs (1961)
Annie Laurie is the song recorded by Doberman in the episode of The Phil Silvers Show 'Doberman The Crooner'
Annie Laurie sung by the Red Army Choir was chosen by the Irish American writer J. P. Donleavy as one of his Desert Island Discs on 4 March 2007.
The song appears prominently as a plot point in the 1998 Takashi Miike film The Bird People in China.
An instrumental version of the song is played at 10pm every day over the PA system of Ngee Ann City shopping centre in Singapore, to announce the closing of the mall for the day.
Swedish band The Radio Dept. cover the song on their 2002 EP of the same name.
Songwriter Rood Adeo covered the song Annie Laurie on his 2012 CD Mindful Indifference.
The song is played in a flute throughout the Little Lord Fauntleroy (Little Prince Cedie) TV series from Nippon Animation.
The satirical song "Transport of Delight" by Flanders and Swann contains the couplet:Some people like a Motorbike, some say, 'A Tram for me!'Or for a Bonny Army Lorry they wad lay them doon and dee.
However the version on their early LP, At the Drop of a Hat, is:
Some talk of a Lagonda, some like a smart MG;For a bonny Army lorry they'd lay them doon and dee.American march composer John Philip Sousa wrote a march that used the melody in the trio called "Bonnie Annie Laurie" from 1883.
John Steinbeck wrote about this song (Maxwellton's brae are bonnie) in his novel "To a God Unknown" (chapter VIII).
John Lennon mentions this song in passing in his short story Up Yours that appears in his book Skywriting By Word of Mouth, posthumously published in 1986.
Annie Laurie is part of the soundtrack from the 1954 Japanese movie Twenty-Four Eyes.


== References ==


== External links ==
 Works related to Annie Laurie at Wikisource
Works by or about Annie Laurie at Internet Archive
 Annie Laurie public domain audiobook at LibriVox
Free scores of Annie Laurie in the Choral Public Domain Library (ChoralWiki)
Page 716 [1]