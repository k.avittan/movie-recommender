The term Fourth Estate or fourth power refers to the press and news media both in explicit capacity of advocacy and implicit ability to frame political issues. Though it is not formally recognized as a part of a political system, it wields significant indirect social influence.The derivation of the term fourth estate arises from the traditional European concept of the three estates of the realm: the clergy, the nobility, and the commoners. The equivalent term "fourth power" is somewhat uncommon in English, but it is used in many European languages, including German (Vierte Gewalt), Spanish (Cuarto poder), and French (Quatrième pouvoir), to refer to a government's separation of powers into legislative, executive, and judicial branches.


== Origins ==
Thomas Carlyle attributed the origin of the term to Edmund Burke, who used it in a parliamentary debate in 1787 on the opening up of press reporting of the House of Commons of Great Britain. Earlier writers have applied the term to lawyers, to the British queens consort (acting as free agents independent of their husbands), and to the proletariat.


== The press ==
In modern use, the term is applied to the press, with the earliest use in this sense described by Thomas Carlyle in his book On Heroes and Hero Worship: "Burke said there were Three Estates in Parliament; but, in the Reporters' Gallery yonder, there sat a Fourth Estate more important far than they all."Burke's 1787 coining would have been making reference to the traditional three estates of Parliament: The Lords Spiritual, the Lords Temporal and the Commons. If, indeed, Burke did make the statement Carlyle attributes to him, the remark may have been in the back of Carlyle's mind when he wrote in his French Revolution (1837) that "A Fourth Estate, of Able Editors, springs up; increases and multiplies, irrepressible, incalculable." In this context, the other three estates are those of the French States-General: the church, the nobility and the townsmen. Carlyle, however, may have mistaken his attribution: Thomas Macknight, writing in 1858, observes that Burke was merely a teller at the "illustrious nativity of the Fourth Estate". If Burke is excluded, other candidates for coining the term are Henry Brougham speaking in Parliament in 1823 or 1824 and Thomas Macaulay in an essay of 1828 reviewing Hallam's Constitutional History: "The gallery in which the reporters sit has become a fourth estate of the realm." In 1821, William Hazlitt (whose son, also named William Hazlitt, was another editor of Michel de Montaigne—see below) had applied the term to an individual journalist, William Cobbett, and the phrase soon became well established.Oscar Wilde wrote:

In old days men had the rack. Now they have the Press. That is an improvement certainly. But still it is very bad, and wrong, and demoralizing. Somebody — was it Burke? — called journalism the fourth estate. That was true at the time no doubt. But at the present moment it is the only estate. It has eaten up the other three. The Lords Temporal say nothing, the Lords Spiritual have nothing to say, and the House of Commons has nothing to say and says it. We are dominated by Journalism.
In United States English, the phrase "fourth estate" is contrasted with the "fourth branch of government", a term that originated because no direct equivalents to the estates of the realm exist in the United States. The "fourth estate" is used to emphasize the independence of the press, while the "fourth branch" suggests that the press is not independent of the government.


=== The networked Fourth Estate ===
Yochai Benkler, author of the 2006 book The Wealth of Networks, described the "Networked Fourth Estate" in a May 2011 paper published in the Harvard Civil Liberties Review. He explains the growth of non-traditional journalistic media on the Internet and how it affects the traditional press using WikiLeaks as an example. When Benkler was asked to testify in the United States vs. PFC Bradley E. Manning trial, in his statement to the morning 10 July 2013 session of the trial he described the Networked Fourth Estate as the set of practices, organizing models, and technologies that are associated with the free press and provide a public check on the branches of government. It differs from the traditional press and the traditional fourth estate in that it has a diverse set of actors instead of a small number of major presses. These actors include small for-profit media organizations, non-profit media organizations, academic centers, and distributed networks of individuals participating in the media process with the larger traditional organizations.


== Alternative meanings ==


=== In European law ===
In 1580 Montaigne proposed that governments should hold in check a fourth estate of lawyers selling justice to the rich and denying it to rightful litigants who do not bribe their way to a verdict:
What is more barbarous than to see a nation [...] where justice is lawfully denied him, that hath not wherewithall [sic] to pay for it; and that this merchandize hath so great credit, that in a politicall government there should be set up a fourth estate [tr. French: quatriesme estat (old orthography), quatrième état (modern)] of Lawyers, breathsellers and pettifoggers [...].


=== The proletariat ===
An early citation for this is Henry Fielding in The Covent Garden Journal (1752):

None of our political writers ... take notice of any more than three estates, namely, Kings, Lords, and Commons ... passing by in silence that very large and powerful body which form the fourth estate in this community ... The Mob.
This sense has prevailed in other countries: In Italy, for example, striking workers in 1890s Turin were depicted as Il quarto stato—The Fourth Estate—in a painting by Giuseppe Pellizza da Volpedo. A political journal of the left, Quarto Stato, published in Milan, Italy, in 1926, also reflected this meaning.Far-right theorist Julius Evola saw the Fourth Estate as the final point of his historical cycle theory, the regression of the castes: 

[T]here are four stages: in the first stage, the elite has a purely spiritual character, embodying what may be generally called "divine right." This elite expresses an ideal of immaterial virility. In the second stage, the elite has the character of warrior nobility; at the third stage we find the advent of oligarchies of a plutocratic and capitalistic nature, such as they arise in democracies; the fourth and last elite is that of the collectivist and revolutionary leaders of the Fourth Estate.


=== British queens consort ===
In a parliamentary debate of 1789 Thomas Powys, 1st Baron Lilford, MP, demanded of minister William Pitt, 1st Earl of Chatham that he should not allow powers of regency to "a fourth estate: the queen". This was reported by Burke, who, as noted above, went on to use the phrase with the meaning of "press".


=== U.S. Department of Defense ===
In the United States government's Department of Defense, the "fourth estate" (also called the "back office") refers to 28 agencies that do not fall under the Departments of the Army, Navy, and Air Force. Examples include the Defense Technology Security Administration, Defense Technical Information Center, and Defense Information Systems Agency.


== Maintaining transparency ==

Due to the massive increase in centralized political powers, emerges a need for the fourth estate of democracy, where transparency is maintained regarding information, news and the public sphere. This fourth estate, being the news media, contributes greatly and is used as a tool for the unbiased dispersion of news. Addressing important information that may often showcase the dark side of political parties or corporations.During the American Revolution, this fourth estate was crucial for the process of distributing information, with the medium being newspapers. This demand for information had carried on even after the American Revolution, where it was heavily utilized for ideological dispersion. News media plays an important role in keeping the populace notified, and actively engages in distributing the truth, which has brought to light various issues. A popular example being the exposing of President Nixon's criminal activities, which ultimately led to his resignation, showcasing the political capability of this fourth estate. More recently, news media and journalists have played an extremely huge role in exposing the illegal monitoring done by the NSA, where a large percentage of the population is closely surveilled, and their disclosed information is accessible by the government.These events showcase the capability of news media as a fourth estate of the truth, which serves the people. Furthermore, creates a sense of balance and transparency in society.


== Fiction ==
In his novel The Fourth Estate, Jeffrey Archer wrote: "In May 1789, Louis XVI summoned to Versailles a full meeting of the 'Estates General'. The First Estate consisted of three hundred clergy. The Second Estate, three hundred nobles. The Third Estate, commoners." The book is fiction based on the lives of two real-life Press Barons, Robert Maxwell and Rupert Murdoch.


== See also ==
Fourth branch of government
Freedom of the press
Estates of the realm
First Estate
Second Estate
Third Estate
Fifth Estate
Fourth Estate Public Benefit Corporation
List of newspapers in the United States
The Fourth Estate (2018 TV series)
Videocracy


== References ==


== External links ==
"The Fourth Estate", Section V of French Revolution by Thomas Carlyle, as posted in the online library of World Wide School