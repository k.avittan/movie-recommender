The Story of the Kelly Gang is a 1906 Australian bushranger film that traces the exploits of 19th-century bushranger and outlaw Ned Kelly and his gang. It was directed by Charles Tait and shot in and around the city of Melbourne. The original cut of this silent film ran for more than an hour with a reel length of about 1,200 metres (4,000 ft), making it the longest narrative film yet seen in the world. It premiered at Melbourne's Athenaeum Hall on 26 December 1906 and was first shown in the United Kingdom in January 1908. A commercial and critical success, it is regarded as the origin point of the bushranging drama, a genre that dominated the early years of Australian film production. Since its release, many other films have been made about the Kelly legend.
As of 2020, approximately 17 minutes of the film are known to have survived, which, together with stills and other fragments, have undergone restoration for theatrical and home video releases. In 2007, The Story of the Kelly Gang was inscribed on the UNESCO Memory of the World Register for being the world's first full-length narrative feature film.


== Plot ==

Film historian Ina Bertrand suggests that the tone of The Story of the Kelly Gang is "one of sorrow, depicting Ned Kelly and his gang as the last of the bushrangers." Bertrand identifies several scenes that suggest considerable film making sophistication on the part of the Taits. One is the composition of a scene of police shooting parrots in the bush. The second is the capture of Ned, shot from the viewpoint of the police, as he advances. A copy of the programme booklet has survived, containing a synopsis of the film, in six 'scenes'. The latter provided audiences with the sort of information later provided by intertitles, and can help historians imagine what the entire film may have been like.
According to the synopsis given in the surviving programme, the film originally comprised six sequences. These provided a loose narrative based on the Kelly gang story.
Scene 1: Police discuss a warrant for Dan Kelly's arrest. Later, Kate Kelly rebuffs the attentions of a Trooper.
Scene 2: The killings of Kennedy, Scanlon and Lonigan at Stringybark Creek by the gang.
Scene 3: The hold-up at Younghusband's station and a bank hold–up.
Scene 4: Various gang members and supporters evade the police and the gang killing of Aaron Sherritt.
Scene 5: The attempt to derail a train and scenes at the Glenrowan Inn. The police surround the hotel, Dan Kelly and Steve Hart "die by each other's hands" after Joe Byrne is shot dead.
Scene 6: The closing scenes. Ned Kelly fights hard but is shot in the legs. "He begs the Troopers to spare his life, thus falls the last of the Kelly Gang…"Some confusion regarding the plot has emerged as a result of a variant poster dating from the time the film was re-released in 1910. The similar (but different) photos suggest that either the film was being added to for its re-release, or an entirely new version was made by Johnson and Gibson, as the poster proclaims. In addition, a film fragment (" the Perth fragment ") exists, showing Aaron Sherritt being shot in front of an obviously painted canvas flat. This is now thought to be from a different film altogether, perhaps a cheap imitation of The Story of the Kelly Gang made by a theatrical company, keen to cash in on the success of the original, or an earlier bushranger short.


== Origins ==
Australian bushranger Ned Kelly had been executed only twenty-six years before The Story of the Kelly Gang was made, and Ned's mother Ellen and younger brother Jim were still alive at the time of its release. The film was made during an era when plays about bushrangers were extremely popular, and there were, by one estimate, six contemporaneous theatre companies giving performances of the Kelly gang story. Historian Ian Jones suggests bushranger stories still had an "indefinable appeal" for Australians in the early 20th century. Stephen Vagg wrote that "bushranger films are their own, uniquely Australian genre, deriving from local history and literary tradition rather than simply copying American tropes... Kelly Gang... , was adapted from an Australian stage play, based on an Australian historical event, and featured many traditions and tropes that are grounded more in Australian than American literary traditions – miscarriage of justice, Protestant-Catholic sectarianism, class warfare, feisty “squatter’s daughters”, etc."


== Cast ==
There is considerable uncertainty over who appeared in the film and a number of unsubstantiated claims have been made regarding participation. According to the Australian National Film and Sound Archive, the only actors positively identified are; 

John Forde as Dan Kelly
Elizabeth Tait as the stunt double for the actress playing Kate KellyOthers thought to be in the film include

Frank Mills, as the title character Ned Kelly
John and Frank Tait, Harriet Tait, members of Charles Tait's family.
J. (Jack) Ennis, as Steve Hart
Will Coyne, as Joe ByrneIn her memoirs, Viola Tait claimed the part of Ned was played by a Canadian stunt actor, who deserted the project part way through.A 1944 article said the actors came from Cole's Dramatic Company. This article claims the Taits were not in the film.


== Production ==

Shooting of the film reportedly involved a budget variously estimated between £400 (Gibson) and £1,000 (Tait) and took six months. While it is now commonly accepted that the Tait's experienced older brother Charles directed the film, only ten years after it was made, pioneer Australian director W. J. Lincoln claimed it was actually "directed by Mr Sam Crews [sic], who... worked without a scenario, and pieced the story together as he went along." Lincoln also claimed that "the principal characters were played by the promoters and their relatives, who certainly made no pretensions to any great histrionic talent."Viola Tait's memoirs, published in the early 1970s, identifies Charles as being chosen as director because of his theatrical experience. Her account confirmed that many of the extended Tait family and their friends appeared in scenes.Much of the film was shot on the property of Elizabeth Tait's family (Charles' wife) at Heidelberg, now a suburb of Melbourne. Other scenes in the film may have been shot in the suburbs of St Kilda (indoor scenes), and possibly Eltham, Greensborough, Mitcham, and Rosanna. The Victoria Railways Department assisted by providing a train.Costumes were possibly borrowed from E. I. Cole's Bohemian Company, and members of the troupe may have also performed in the film. According to Viola Tait, Sir Rupert Clarke loaned the suit of Kelly armour his family then owned for use in the film.The Story of the Kelly Gang was made by a consortium of two partnerships involved in theatre—entrepreneurs John Tait and Nevin Tait, and pioneering film exhibitors Millard Johnson and William Gibson. The Tait family owned the  Melbourne Athenaeum Hall and part of their concert program often included short films. Melbourne film exhibitors Johnson and Gibson also had technical experience, including developing film stock. Credit for writing the film scenario is generally given to brothers Frank, John and sometimes Charles Tait. At a time when films were usually shorts of five to ten minutes duration, their inspiration for making a film of at least sixty minutes in length, and intended as a stand-alone feature, was undoubtedly based on the proven success of stage versions of the Kelly story.Film historians Andrew Pike and Ross Cooper have noted that at the time, the filmmakers were unaware of the historical importance of the film they were making, and only much later "poured forth their memories." Unfortunately, "with the passage of time and the desire to make a good story of it" they "created a maze of contradictory information."For example, in later years, William Gibson claimed that while touring through New Zealand showing the bio-pic Living London, he noticed the large audiences attracted to Charles McMahon's stage play The Kelly Gang. Film historian Eric Reade claimed the Taits themselves owned the stage rights to a Kelly play, while actors Sam Crewes and John Forde later also claimed to have thought of the idea of a making a film of the Kelly Gang's exploits, inspired by the success of stage plays.There is evidence that at least one other bushranging film had been made before 1906. This was Joseph Perry's 1904 short Bushranging in North Queensland, made by the Salvation Army's Limelight Department in Melbourne, one of the world's first film studios.


== Release and reception ==

The film was given a week of trial screenings in country towns in late 1906. This proved enormously successful and the movie recouped its budget for these screenings alone.Its Melbourne debut was made at the Athenaeum Hall on 26 December 1906. It ran for five weeks to full houses, local papers noting the extraordinary popularity of the film. Although the country screenings had been silent, when the film was screened in Melbourne it was accompanied by live sound effects, including blank cartridges as gunshots and coconut shells beaten together to simulate hoofbeats. At later screenings a lecturer would also narrate the action. These additions were well-received by the theatre critic for Melbourne Punch, who stated that they greatly enhance the film's realism. He went on to say:
All the notable features of the story of the Kellys are reproduced, and with the dialogue make up a sensational and realistic series dealing with the murders, robberies and misdeeds which are not the air-created fancies of a penny-dreadful writer, but actual facts which are well within the memory of our citizens.
Comparing the film to other artistic depictions of the Kelly saga, one Adelaide critic wrote that it conveys "a far more vivid impression of the actual life and deeds of the Kellys than letterpress and stagecraft combined."Many groups at the time, including some politicians and the police, interpreted the film as a glorification of criminality. Scenes depicting the gang's chivalrous conduct towards women received criticism, with the The Bulletin stating that such a portrayal "justifies all Ned Kelly’s viciousness and villainies". The film was banned in "Kelly Country"—regional centres such as Benalla and Wangaratta—in April 1907, and in 1912 bushranger films were banned across New South Wales and Victoria.Despite the bans, the film toured Australia for over 20 years and was also shown in New Zealand, Ireland and Britain. When Queen's Royal Theatre was rebuilt in Dublin in 1909, it opened with a program headed by The Story of the Kelly Gang. The backers and exhibitors made "a fortune" from the film, perhaps in excess of £25,000.


== Restoration ==

The film was considered lost until 1976, when five short segments totalling a few seconds of running time were found. In 1978 another 64 metres (210 ft) of the film was discovered in a collection belonging to a former film exhibitor. In 1980, further footage was found at a rubbish dump. The longest surviving single sequence, the scene at Younghusband's station, was found in the UK in 2006. In November 2006, the National Film and Sound Archive released a new digital restoration which incorporated the new material and recreated some scenes based on existing still photographs.
The restoration is now 17 minutes long and includes the key scene of Kelly's last stand and capture.


== In popular culture ==
In director Warwick Thornton's 2017 film Sweet Country, a "travelling picture show" in 1920s Northern Territory shows The Story of the Kelly Gang to residents of an outback town, who cheer for the bushranger. The film's protagonist, an Aboriginal outlaw, is also named Kelly, but vilified and hunted by the same townspeople. According to The Australian, Thornton, an Aboriginal, "has little time" for depictions of Ned Kelly as a larrikin folk hero and Irish victim of British colonisation. "When there was a raiding party," said Thornton, "and a massacre happened, the Scottish, the Irish and the British were all shooting us. It doesn’t equate for me.”


== Other Ned Kelly films ==
The Kelly Gang (1920)
When the Kellys Were Out (1923)
When the Kellys Rode (1934)
A Message to Kelly (1947)
The Glenrowan Affair (1951)
Stringybark Massacre (1967)
Ned Kelly (1970)
Reckless Kelly (1993) (satire)
Ned Kelly (2003)
Ned (2003) (satire)
True History of the Kelly Gang (2019)


== See also ==
Ned Kelly in popular culture
List of Australian films before 1910
List of incomplete or partially lost films


== References ==


== External links ==
The Story of the Kelly Gang on IMDb
The Story of the Kelly Gang at AllMovie
The Story of the Kelly Gang at the National Film and Sound Archive
The Story of the Kelly Gang preserved and released on DVD in Australia
The Story of the Kelly Gang at Australian Screen Online
Sally Jackson and Graham Shirley describe the restoration of The Story of the Kelly Gang
The Story of the Kelly Gang (1906) on YouTube