A Manic Pixie Dream Girl (MPDG) is a stock character type in films. Film critic Nathan Rabin, who coined the term after observing Kirsten Dunst's character in Elizabethtown (2005), said that the MPDG "exists solely in the fevered imaginations of sensitive writer-directors to teach broodingly soulful young men to embrace life and its infinite mysteries and adventures." MPDGs are said to help their men without pursuing their own happiness, and such characters never grow up; thus, their men never grow up.The Manic Pixie Dream Girl has been compared to another stock character, the Magical Negro, a Black character who seems to exist only to provide spiritual or mystical help to the white savior protagonist. In both cases, the stock character has no discernible inner life and usually exists only to provide the protagonist some important life lessons.


== Examples ==
MPDGs are usually static characters who have eccentric personality quirks and are unabashedly girlish. They invariably serve as the romantic interest for a (most often brooding or depressed) male protagonist. Examples of an MPDG are described below:

Natalie Portman's character in the movie Garden State (2004), written and directed by Zach Braff. In his review of Garden State, Roger Ebert also described this kind of rather unbelievable "movie creature" as "completely available" and "absolutely desirable".
The A.V. Club points to Katharine Hepburn's character in Bringing Up Baby (1938) as one of the earliest examples of the archetype.


=== Counterexamples ===
The titular character of Annie Hall (1977) is often called a MPDG but is arguably not one, as she has her own goals independent of the male lead.
Kate Winslet's character Clementine in Eternal Sunshine of the Spotless Mind (2004) acknowledges the trope of the Manic Pixie Dream Girl and rejects the type, in a remark to Jim Carrey's Joel: "Too many guys think I'm a concept, or I complete them, or I'm gonna make them alive. But I'm just a fucked-up girl who's lookin' for my own peace of mind; don't assign me yours."
Although Zooey Deschanel's Summer in 500 Days of Summer (2009) is often identified as a MPDG, the movie can be seen as a deconstruction of the trope because it shows the dangers of idealising women as things, rather than respecting them as real people with their own complex outlooks. Director Marc Webb stated, "Yes, Summer has elements of the manic pixie dream girl – she is an immature view of a woman. She's Tom's view of a woman. He doesn't see her complexity and the consequence for him is heartbreak. In Tom's eyes, Summer is perfection, but perfection has no depth. Summer's not a girl, she's a phase."
Eve, the lead character of Stuart Murdoch's musical film, God Help the Girl (2014), has also been noted as a subversion of the trope, with actress Emily Browning approaching the character as "the anti-manic pixie dream girl" and describing her as having "her own inner life" and being "incredibly self-absorbed; [...] Olly wants her to be his muse and she's like, 'No, I'm not having that. I'm gonna go do my own shit.'"
Margo Roth Spiegelman in Paper Towns (2015) is a deconstruction of the trope, the author of the book, John Green, has stated   "Paper Towns is devoted IN ITS ENTIRETY to destroying the lie of the manic pixie dream girl... I do not know how I could have been less ambiguous about this without calling the novel The Patriarchal Lie of the Manic Pixie Dream Girl Must Be Stabbed in the Heart and Killed."


== Criticism and debate ==
In an interview with Vulture, the entertainment section of New York, about her film Ruby Sparks, actress and screenwriter Zoe Kazan criticized the term as reductive, diminutive, and misogynistic. She disagreed that Hepburn's character in Bringing Up Baby is a MPDG: "I think that to lump together all individual, original quirky women under that rubric is to erase all difference."In a December 2012 video, AllMovie critic Cammila Collar embraced the term as an effective description of one-dimensional female characters who only seek the happiness of the male protagonist, and who do not deal with any complex issues of their own. The pejorative use of the term, then, is mainly directed at writers who do not give these female characters more to do than bolster the spirits of their male partners.In December 2012, Slate's Aisha Harris posited that "critiques of the MPDG may have become more common than the archetype itself", suggesting that filmmakers had been forced to become "self-aware about such characters" in the years since Rabin's coining of the phrase and that the trope had largely disappeared from film.In July 2013, Kat Stoeffel, for The Cut, argued that the use of the term had become sexist, in that "it was levied, criminally at Diane Keaton in Annie Hall and Zooey Deschanel, the actual person. How could a real person's defining trait be a lack of interior life?"Similar sentiments were elucidated by Monika Bartyzel for The Week in April 2013, who wrote "this once-useful piece of critical shorthand has devolved into laziness and sexism". Bartyzel argues that "[The term] 'Manic Pixie Dream Girl' was useful when it commented on the superficiality of female characterizations in male-dominated journeys, but it has since devolved into a pejorative way to deride unique women in fiction and reality."


== Retraction of the term ==
In July 2014, for Salon, Rabin prompted a retraction of the term "Manic Pixie Dream Girl". He argued that in "giving an idea a fuzzy definition", he inadvertently gave the phrase power it was not intended to have. The trope's popularity, Rabin suggested, led to discussions of a more precise definition, a reduction of the critic's all-encompassing classification of MPDG. While he coined the term to expose the sexist implications in modern culture, the "phrase was increasingly accused of being sexist itself". Backlash occurred when many well-loved female characters were placed under this trope. In response, Rabin suggested that nuanced characters cannot be classified in such a restricted nature, and thus he apologized to pop culture for "creating this unstoppable monster".


== Manic Pixie Dream Boy ==
A possible male version of this trope, the Manic Pixie Dream Boy or Manic Pixie Dream Guy, was found in Augustus Waters from the film version of The Fault in Our Stars (2014); he was given this title in a 2014 Vulture article, in which Matt Patches stated, "he's a bad boy, he's a sweetheart, he's a dumb jock, he's a nerd, he's a philosopher, he's a poet, he's a victim, he's a survivor, he's everything everyone wants in their lives, and he's a fallacious notion of what we can actually have in our lives".The Manic Pixie Dream Boy trope has also been pointed out in sitcoms such as Parks and Recreation and 30 Rock. The female protagonists of these shows are married to men (Adam Scott's Ben Wyatt and James Marsden's Criss Chros, respectively), who, according to a 2012 Grantland article, "patiently [tamp] down her stubbornness and temper while appreciating her quirks, helping her to become her best possible self".


== Similar tropes ==


=== Algorithm-defined fantasy girl ===

Another version of the Manic Pixie Dream Girl is the algorithm-defined fantasy girl. The difference is that the latter is not human, but a robot or artificial intelligence of some sort. The function is the same one: to fulfill the desires of the male character and to help him in his journey without having any desires or journey of her own. Some examples are Joi in Blade Runner 2049 and Samantha in Spike Jonze's Her.


== See also ==

Damsel in distress
Foil (literature)
Gamine
Golden fantasy
Johanson analysis
Mary Sue
Smurfette principle


== References ==