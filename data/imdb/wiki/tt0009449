The One Woman: A Story of Modern Utopia is a 1903 novel by Thomas Dixon Jr.


== Plot summary ==
Reverend Joseph Gordon, a preacher in New York City, clashes with church elders because of his socialist views. Despite being a socialist, his best friend, Mark Overman, is a millionnaire Wall Street banker.Meanwhile, Gordon grows apart from his wife, Ruth, who disapproves of his politics. After he starts a relationship with Kate Ransom, a wealthy female parishioner, he divorces his wife. Kate Ransom donates a million dollars for him to start a new church and thus get rid of the disapproving church elders. The new church is called the "Temple of Man".Unfortunately, Kate Ransom falls in love with his friend Mark Overman. The two men have a fight over the woman, and Gordon kills Overman. Ransom tells the police about the murder and Gordon is sentenced to the death penalty. Meanwhile, his faithful ex-wife asks her childhood lover, now the Governor of New York, to grant him a pardon, which he does. Gordon is rescued from execution at the last minute.


== Main themes ==
The novel's primary theme is socialism, and it has been described as an 'anti-socialist novel.'Another theme is feminism. However, biographer Anthony Slide explains that it is construed as a by-product of socialism.


== Critical reception ==
The book was widely reviewed and became a best-seller.It has been interpreted as an attack on socialist clergyman George D. Herron, who had recently divorced.


== Theatrical and cinematic adaptations ==
The novel was adapted as a play in 1906. The first performance took place in Norfolk, Virginia, October of that year. It was performed on a tour in the American South. The main character, Frank Gordon, was played by D. W. Griffith. His wife, Linda Arvidson, also acted in the play. Two months later, they were replaced with cheaper actors.The novel was adapted into a film in 1918. The screenwriters were Harry Chandlee and E. Richard Schayer. It was directed by Reginald Barker. It was shot in May–June 1918 at Paralta Studio on Melrose Avenue in Los Angeles. Actors included W. Lawson Butt, Clara Williams and Adda Gleason. The film, which is now lost, was reviewed in Variety.


== References ==


== External links ==
The One Woman, at Internet Archive
The One Woman, at Hathi Trust
The One Woman, at Project Gutenberg