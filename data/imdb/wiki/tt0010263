The Wanted are a British-Irish boy band consisting of members Max George, Siva Kaneswaran, Jay McGuiness, Tom Parker and Nathan Sykes. They formed in 2009 and were signed worldwide to Universal Music subsidiaries Island Records and Mercury Records, and managed by Scooter Braun.The band's debut album, The Wanted, was released on 25 October 2010 and peaked at number four on the UK Albums Chart. The album spawned three UK top 20 singles: their debut single, "All Time Low", which debuted at number one, "Heart Vacancy", which reached number two, and "Lose My Mind", which peaked at number 19. Follow-up album Battleground was released on 4 November 2011 and hit number five in the UK and number four in Ireland. The lead single from the album, "Gold Forever", was released in aid of Comic Relief and reached number three in the UK. Their second number-one hit, "Glad You Came", topped the singles chart in the United Kingdom for two weeks and in Ireland for five weeks. The third and fourth singles, "Lightning" and "Warzone", hit number two and number 21, respectively, in the UK.
In early 2012, the Wanted began to achieve success in the United States and Canada: their hit single "Glad You Came" sold 3 million copies in the US and hit number three on the Billboard Hot 100 and number two on the Canadian Hot 100. Their follow-up singles "Chasing the Sun" and "I Found You" reached the top spot on the Billboard Hot Dance Club Songs chart. In 2013, it was announced that the group would be starring in their own reality series on E! entitled The Wanted Life. The series would document the group's stay in Los Angeles while they recorded their third worldwide album and prepare for a world tour, as well as their day-to-day antics. The show promised an honest glimpse into the lives of the band members.According to the British Phonographic Industry (BPI), the Wanted has been certified for 600,000 albums and 2 million singles in the UK.


== History ==


=== 2009–2010: Formation and debut album ===
The Wanted were formed in 2009 through mass auditions held by Jayne Collins, who also put together the Saturdays and Parade. The audition process, which saw more than 1,000 people attend, took over nine months. Just George, Parker and Sykes were involved at the start, and Kaneswaran and McGuiness joined later through the process. The group revealed in their autobiography that after they were informed of the line-up, seven more people were re-auditioned before they were signed to Maximum Artist Management and Geffen Records.
The group were credited with co-writing five of the thirteen tracks on the album. On 25 July 2010, they released their debut single, "All Time Low". The song was co-written by Steve Mac (who also produced it), Wayne Hector and Ed Drewett. It hit number one in the UK and spent 17 weeks in the UK top 40. The second single "Heart Vacancy", again written with Hector, was released on 17 October 2010. It peaked at number two on the UK Singles Chart. The group's third single from the album was "Lose My Mind", which reached number 19 on the UK Singles Chart following a performance on The X Factor. The group's eponymous debut album was released on 25 October 2010 and peaked at number four in the UK and has since been certified Platinum in the UK. Between 28 March and 15 April 2010, the group embarked on the fifteen-date Behind Bars theatre tour, which saw them play in twelve UK cities. They were supported by Lawson, Twenty Twenty and Starboy Nathan.


=== 2011–2012: Battleground and The Wanted: The EP ===
In January 2011, the group began to work on their second studio album. The album featured writing credits from the band members on ten of the fifteen tracks, and contributions from Steve Mac, Wayne Hector, Ed Drewett, Diane Warren and Guy Chambers. The album's lead single, "Gold Forever", was released in aid of Comic Relief and debuted at number three on the UK Singles Chart. In July 2011, the group released the album's second single, "Glad You Came", which became their second number-one single. It was number one for two weeks, and stayed in the top ten of the chart for six weeks. The single also reached number one in Ireland and remained there for five weeks. The song also charted in some European territories, and was very successful in Australia. The third single from the album, "Lightning", was released on 16 October 2011, peaking at number 2 in the UK and number 5 in Ireland. The fourth single, "Warzone", was released on 26 December 2011, with the music video and radio debut coming in early November 2011. On 7 November 2011, the group's second album Battleground was released. It debuted at number 5 in the UK and number 4 in Ireland. It has since been certified Platinum in the UK. In addition to shows in the United Kingdom, including T4 on the Beach, V Festival, and the iTunes Festival, the group supported Canadian singer Justin Bieber in Brazil on 8 and 9 October 2011 and Britney Spears on her Femme Fatale Tour at the Manchester Evening News Arena on 6 November 2011.
Between 15 February and 9 March 2012, the group embarked on "The Code" tour, to promote the release of the album. The 16-date arena tour saw the group play in thirteen cities in England, Wales, Scotland, Ireland and Northern Ireland. Known as #TheCodeTour, each concert featured an interactive element that linked to the group's official Facebook and Twitter pages.
In a bid to break into the United States market, the Wanted went on a short promotional radio and club tour of the United States, touring between 17 January to 8 February 2012. The success of the 10-date tour led to the group's official release of their debut American single, "Glad You Came". It peaked at number three on the Billboard Hot 100 and has since sold over 3 million copies there. Their debut American EP, The Wanted: The EP, was released in April 2012, debuting at number seven in the US and number eight in Canada. "Chasing the Sun" was released as the band's second official single there, peaking at number 50 on the BillboardHot 100 whilst also reaching number one on the Hot Dance Club Songs chart. "Chasing the Sun" was released on 21 May 2012. On 22 May 2012, the band opened and performed at the first Q102's Springle Ball concert. On 14 June 2012 the band performed a concert at New York City's Beacon Theater, which was also broadcast live on music television station Fuse. The group visited Australia and New Zealand for the promotion of their extended play in August 2012. Following their promotional tour of Australia, the group headed to the US for a number of promotional concerts.


=== 2012–2014: Word of Mouth, The Wanted Life and indefinite hiatus ===
The Wanted began working on their third studio album shortly after the release of Battleground on 4 November 2011. In May 2012, "Chasing the Sun" was released as the album's lead single, and the third single from the band's The Wanted: The EP in the United States. Around this time, the band also premiered a brand new track called "Satellite", which was co-written by Ryan Tedder of OneRepublic. In August 2012, the band filmed a music video in Los Angeles, reportedly for their brand new single, "I Found You". However, when the video for "I Found You" was released in October 2012, it was a different one. The band later revealed that they had reshot the video in London, not being satisfied with the first one. The original was later released as the "fan version" on Vevo. "I Found You" was released on 5 November 2012 as the album's second single.The band confirmed in September 2012 that the album would contain a number of collaborations, including tracks with Pitbull, LMFAO, Chris Brown, Rita Ora and Dappy. In November 2012, the band announced that the album had been pushed back from the original release of 3 December 2012 to February 2013 at their request; the reason stated for this delay was to perfect the album, and "tweak out a few insecurities involving certain tracks". It was confirmed that the album would feature a track co-written by fellow label-mate Justin Bieber.
In early February 2013, it was announced that the Wanted would star in their own reality series called The Wanted Life which premiered internationally in Autumn 2013. The show is produced by Ryan Seacrest. In the United States, it premiered on E! on 2 June 2013.In April 2013, the band announced the release of the third single from their third album, titled "Walks Like Rihanna", named after Barbadian singer Rihanna. It was released on 23 June 2013 and debuted and peaked at number 4 on the UK Singles Chart. On 9 September 2013, the band announced that their new album Word of Mouth would be released on 4 November. It was preceded by the release of "We Own the Night" and "Show Me Love (America)".
On 25 July 2013 the group was spoken to by police after champagne was thrown out of a hotel window, soaking poet Todd Swift. A police statement said "Police were called at around 12.20pm on Thursday, July 25, to a report of a man having been verbally threatened and having had a liquid substance thrown over him at a hotel in Marylebone."On 7 October 2013, it was announced that the Wanted would embark on their first world tour, the Word of Mouth World Tour, in 2014. The tour included shows in the UK, mainland Europe, the US, and Canada.On 22 January 2014, the band announced that they would be taking a hiatus in order to pursue individual solo projects.


== Members ==

Max George – lead and backing vocals (2009–2014)
Siva Kaneswarm – lead and backing vocals (2009–2014)
Jay McGuiness – lead and backing vocals (2009–2014)
Tom Parker – lead and backing vocals (2009–2014)
Nathan Sykes – lead and backing vocals (2009–2014)


== Discography ==

The Wanted (2010)
Battleground (2011)
Word of Mouth (2013)


== Media ==


=== Wanted Wednesday ===
The group run a weekly online podcast, #WantedWednesday, featuring backstage footage, social activities and exclusive performances.


=== Wanted World ===
In 2012, the group launched an interactive fan site titled Wanted World, where the fans can get exclusive access to new videos, merchandise, VIP tour tickets, and live web chats. Platinum membership costs £100 per year and gold membership £40 per year. Since 2013, fans have been able to buy a £5 monthly membership that gives them the same access as gold membership.


== Tours ==
The Behind Bars Tour was the band's first theatre tour which sold out within weeks of going on sale. It began on 26 March 2011 and concluded on until 15 April. The tour featured 12 different cities with a total of sixteen dates.

It was announced on Friday 16 December 2011 that the group would embark on their first US tour, with 10 dates to be held over January and February 2012.

The Wanted announced that they would embark on their first arena tour across the UK and Ireland in February and March 2012. Known as the Code Tour and T.W.A.T., each concert featured an interactive element that links to their official Facebook and Twitter pages.


== Filmography ==


== Awards and nominations ==


== References ==


== External links ==
Official website (archived version)
The Wanted on IMDb