The Ranch is an American streaming television comedy/drama series. It reunites That '70s Show  co-stars Ashton Kutcher and Danny Masterson as brothers Colt and Rooster Bennett, respectively, helping run their father Beau (Sam Elliott)'s cattle ranch in Colorado. It also stars Debra Winger as their mother Maggie, a local bar owner, and Elisha Cuthbert as Colt's love interest Abby, a local school teacher. Other cast members from That 70s Show who have recurring roles include Wilmer Valderrama, Kurtwood Smith and Debra Jo Rupp. It debuted in 2016 on Netflix and ran for four seasons in eight parts.The Ranch was filmed on a sound stage in front of a live audience in Burbank, California. The opening sequence shows scenes from Norwood, and Ouray, Colorado and surrounding Ouray, and San Miguel Counties. The exterior shot of Maggie's bar is a bar located in Naturita, Colorado in Montrose County, Each season consists of 20 episodes broken up into two parts, each containing 10 episodes, the episodes are approximately 30 minutes in length. All episodes are named after American country music songs.On October 31, 2018, Netflix renewed the series for a fourth and final season. The first part of the final season (Part 7) premiered on September 13, 2019. The second part of the final season (part 8) was released on January 24, 2020.


== Premise ==
The show takes place on the fictional Iron River Ranch, near the fictitious small town of Garrison, Colorado; detailing the life of the Bennetts, a dysfunctional family consisting of two brothers, their rancher father, and his estranged wife, a local bar owner.


== Cast ==


=== Main ===
Ashton Kutcher as Colt Reagan Bennett, a former high school and college football star who pursued a professional career with mixed success, never finding regular playing time or stardom, but hanging on in backup roles and on practice squads.  He returns to his hometown after a fifteen-year absence so that he can take part in a tryout with a semi-pro team, and decides to stay and help his father and older brother on their family ranch. Colt is frequently the butt of their jokes, particularly when it comes to his lack of ranching skills and his short-lived pro football career.  He is a heavy drinker and has a habit of not using common sense, but deep down he is a friendly young man and cares about other people. Based on his various T-shirts and dialogue throughout the first season, it's implied that Colt has played for the Barcelona Dragons, San Jose SaberCats, Spokane Shock, Orlando Predators, Nebraska Danger, Sioux City Bandits, Iowa Barnstormers, Green Bay Blizzard, Minnesota Axemen, Portland Forest Dragons and Philadelphia Soul. Dialogue and team gear also indicate that he was a member of teams in Canada, notably the Ottawa Redblacks and fictional Saskatoon Cold ("Like the Miami Heat... only cold").  He has also played on teams in Alaska and was a member of the Buffalo Bills practice squad. In the first episode of the series, Colt reveals that he was "the back up to the back up to the back up" for the Florida State team that won the college football championship in 1999. He later marries Abby, his high school sweetheart, and they become the parents of a daughter they name Peyton.
Danny Masterson as Jameson "Rooster" Ford Bennett (seasons 1–3), Colt's older brother. He has lived and worked on the ranch since Colt left to follow his football career, at times showing a bitter attitude for having to accept the responsibility while Colt pursued his dream. Despite living in Colt's shadow he is far more competent on the ranch as well as a more rational thinker. He has, however, been just as immature as Colt, drinking too much and slacking off. In the first half of part 5, Rooster gets into a rivalry with Mary's intimidating boyfriend, and in the second episode of Part 6, it is discovered that Rooster's motorcycle went off the side of a dangerous road; he is missing and presumed dead.
Debra Winger as Maggie Bennett, owner of Maggie's bar, and Colt and Rooster's mother. Maggie is at first separated and later divorced from Beau and lives in a trailer behind the bar she owns. Much more patient and laid-back than Beau, her company and advice are often sought out by the boys on how to deal with problems, especially those concerning their father. Maggie is a hippie and an avid marijuana smoker.
Sam Elliott as Beau Roosevelt Bennett, Maggie's ex-husband, and Colt and Rooster's father. A Vietnam War veteran, he has worked the ranch since returning from the war and taking it over after his father died. He is a curmudgeon, constantly annoyed with everyone and everything. He has a strained relationship with Rooster and Colt. He hates modern amenities and is easily angered by his sons' antics. Beau appears to be a Republican with a soft-spot for Ronald Reagan but at times seems to despise all politicians regardless of affiliation.
Elisha Cuthbert as Abby Phillips-Bennett (seasons 2–4; recurring, season 1), a Garrison High School history teacher and Colt's high school sweetheart. As the series begins, Abby has been in a five-year relationship with Kenny Ballard and briefly becomes engaged to him. As the series progresses, she breaks off her engagement to Kenny and ultimately marries Colt, having a baby girl, Peyton, down the line.


=== Recurring ===
Barry Corbin as Dale Rivers, a hearing-impaired veterinarian and close friend of Beau's
Grady Lee Richmond as Hank McGinty, a regular Maggie's patron
Bret Harrison as Kenny Ballard, a Courtyard by Marriott manager and Abby's ex-fiancé
Megyn Price as Mary Roth, a Cracker Barrel waitress and Rooster's ex-girlfriend
Kelli Goss as Heather Roth, Mary's younger daughter and Colt's ex-girlfriend, was pregnant with their child.
Molly McCook as Darlene Roth, Mary's older daughter
Kathy Baker as Joanne, Beau's girlfriend and Mary's friend/coworker at the Cracker Barrel
Ethan Suplee as "Beer Pong" Billy Tompkins, a Sheriff's Deputy, and high school friend of Colt and Rooster's.
Justin Mooney as Deputy Wilkerson, a former schoolmate of Colt and Rooster's who Colt has an antagonistic rapport with.
Aimee Teegarden as Nikki, Heather's friend and Billy's fiancée
Chasty Ballesteros as Tanya Showers, a sexy weather reporter and Kenny's current girlfriend
Laura Vallejo as Maria, a Maggie's waitress
Sharon Lawrence as Brenda Sanders, a widowed hairdresser who befriended Beau when he and Maggie separated
Maggie Lawson as Jen, an engineer who wants to build a pipeline underneath the Bennett Ranch and Rooster's love interest
Wendie Malick as Lisa Neumann, owner of the Neumann's Hill ranching corporation
Dax Shepard as Luke Matthews, the son of Beau's late brother Greg who arrives after Rooster's disappearance.
Stephen Saux as Mike, a motel manager, first seen at the Thomas Rhett concert
Van Epperson as Father McGinty, a Priest who performs the wedding ceremony for Beau and Joanne; and later Peyton's baptism
Travis Case as Toby, Colt's high school classmate, who runs the neighboring Henderson Ranch
Josh Burrow as Nick, Mary's abusive ex-boyfriend, who ran Rooster out of town at gun point. Eventually Nick was killed.


=== Special guests ===
Jon Cryer as Bill Jensen, a loan officer and former high school football referee (Seasons 1–2)
Wilmer Valderrama as Umberto, a former Iron River ranch hand, former employee of Neumann's Hill at the My Little Pony Ranch. He is Rooster and Colt's friend. He was deported after an arrest showed he was in the country illegally. (Seasons 1–2)
Martin Mull as Jerry, Maggie's sometimes acid-tripping attorney
John Amos as Ed Bishop, a longtime family friend of the Bennetts and a Neumann's Hill employee (Seasons 1–2)
Thomas F. Wilson as Coach Fitzgerald, the Garrison High School head football coach (Season 1)
Debra Jo Rupp as Janice Phillips, Abby's mother (Seasons 2–4)
Jim Beaver as Chuck Phillips, Abby's father, who like Janice is disdainful of Abby's relationship with Colt (Seasons 2–4)
Conchata Ferrell as Shirley (Season 2)
Lou Diamond Phillips as Clint, a traveling musician attracted to Maggie (Season 2)
Kurtwood Smith as Sam Peterson, neighbor of the Iron River Ranch (Seasons 2–4)
Nancy Travis as Karen, Maggie's estranged sister (Season 3)Joe Firstman, Pearl Charles, Kurt Neumann, Bukka Allen, Sam Hawksley and Thomas Rhett all make cameo appearances.


== Episodes ==


== Release ==
The first ten episodes premiered on April 1, 2016, the second batch of ten episodes premiered on October 7, 2016. In April 2016, Netflix renewed The Ranch for a second season of 20 episodes, the first half of which premiered on June 16, 2017, and the second half was released on December 15, 2017.On July 4, 2017, Netflix announced through its official The Ranch Twitter account that the show had been renewed for a third season of 20 episodes, the first ten of which aired on June 15, 2018. In December 2017, it was announced that Masterson had been written out of the show following multiple sexual assault allegations made against him, and appeared in only the first 10 episodes of the third season.On October 31, 2018, Netflix renewed the series for a fourth and final season with the series to conclude in 2020. On August 21, 2019, it was announced that the first part of the final season (Part 7) was set to premiere on September 13, 2019. On December 9, 2019, it was reported that the second part of the final season (part 8) was set to be released on January 24, 2020.


== Reception ==
The Ranch has earned mixed to positive reviews from critics. The review aggregator website Rotten Tomatoes gives the series an average approval rating of 63% (60% for season 1, 67% for season 2) based on 141 reviews, with an average rating of 4.5/10. The site's critical consensus reads, "A formulaic set-up and predictable plotting are elevated by The Ranch's surprising sensitivity and strong performances." Metacritic gave the series a score of 56 out of 100, based on 20 critics, indicating "mixed or average reviews".Writing for Slate in a positive review, television critic Willa Paskin wrote of the show, "The Ranch is a red-state sitcom, though it takes place in the swing state of Colorado, and is good enough to be watched by people of any political affiliation" and "The goodness sneaks up on you." Los Angeles Times wrote: "[Elliott and Winger's] scenes together, as restrained as they are, are the show's most emotionally resonant. You will want to check them out."


== References ==


== External links ==
The Ranch on IMDb