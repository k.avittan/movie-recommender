The Man with the Iron Heart (released as HHhH in France and Killing Heydrich in Canada) is a 2017 English-language French-Belgian biographical action-thriller film directed by Cédric Jimenez and written by David Farr, Audrey Diwan, and Jimenez. It is based on French writer Laurent Binet's 2010 novel HHhH, and focuses on Operation Anthropoid, the assassination of Nazi leader Reinhard Heydrich in Prague during World War II.
The film stars Jason Clarke, Rosamund Pike, Jack O'Connell, Jack Reynor, and Mia Wasikowska. It was shot in Prague and Budapest from September 2015 until February 2016.


== Cast ==
Jason Clarke as Reinhard Heydrich
Rosamund Pike as Lina Heydrich
Stephen Graham as Heinrich Himmler
Jack O'Connell as Jan Kubiš
Jack Reynor as Jozef Gabčík
Mia Wasikowska as Anna Novak
Gilles Lellouche as Václav Morávek
Tom Wright as Josef Valcik
Enzo Cilenti as Adolf Opalka
Adam Nagaitis as Karel Čurda
Geoff Bell as Heinrich Müller
Volker Bruch as Walter Schellenberg
Barry Atsma as Karl Hermann Frank
Delaet Ignace as Klaus Heydrich
Noah Jupe as Ata Moravec
David Rintoul as Eduard Wagner
Vernon Dobtcheff as Emil Hácha


== Production ==
The film is based on Laurent Binet's novel HHhH about Operation Anthropoid, the assassination of Nazi leader Reinhard Heydrich in Prague. The title is an acronym for Himmlers Hirn heißt Heydrich ("Himmler's brain is called Heydrich"), a quip about Heydrich said to have circulated in Nazi Germany. Cédric Jimenez directed the film based on the script he co-wrote with David Farr and Audrey Diwan, which was financed by Légende Films, Adama Pictures, Echo Lake Entertainment and FilmNation Entertainment. Alain Goldman and Simon Istolainen produced the film. On 28 October 2015, The Weinstein Company came on board to handle the United States distribution rights to the film.Principal photography on the film began on 14 September 2015 in Prague and Budapest, which ended on 1 February 2016.


== Reception ==
On Rotten Tomatoes, the film holds an approval rating of 67% based on 9 reviews, with an average rating of 5.83/10.Boyd van Hoeij of The Hollywood Reporter wrote: "Finally less a two-stories-for-the-price-of-one situation than essentially two films of about an hour each, this is nonetheless a visually impressive Hollywood calling card for Jimenez, who almost manages to overcome the material’s structural weaknesses with impressive directorial verve."


== See also ==
Operation AnthropoidOther films on this subject

Hangmen Also Die! (1943)
Hitler's Madman (1943)
The Silent Village (1943)
Atentát (1964)
Operation Daybreak (1975)
Conspiracy (2001)
Lidice (2011)
Anthropoid (2016)


== References ==


== External links ==
The Man with the Iron Heart on IMDb
The Man with the Iron Heart at Rotten Tomatoes