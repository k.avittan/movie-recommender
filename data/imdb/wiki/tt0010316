Júbilo Iwata (Japanese: ジュビロ磐田, Hepburn: Jubiro Iwata) is a professional Japanese association football team that currently play in the J2 League. The team name Júbilo means 'joy' in Spanish and Portuguese. The team's hometown is Iwata, Shizuoka prefecture and they play at Yamaha Stadium. For big fixtures such as the Shizuoka Derby with Shimizu S-Pulse and against some of the top teams in J1, Júbilo play at the much larger Ecopa Stadium in Fukuroi City, a venue built specifically for the 2002 FIFA World Cup finals. They practice at Okubo Ground in Iwata and Iwata Sports Park Yumeria.Between 1997 and 2003 Iwata were one of the most successful teams in the J. League. Over this seven-year spell Jubilo finished outside the top two of J1 just once, winning the league title on three occasions. This period also saw a number of cup final appearances, including winning the Emperor’s Cup, the J. League Cup, and the Asian Champions League once each.


== History ==


=== Origins and rise to the top ===
The team started out as the company team for Yamaha Motor Corporation in 1970. After making its way through the Shizuoka and Tōkai football leagues, it played in the Japan Soccer League until it reorganized as the J.League at the end of 1992.
Their first glory happened when they won both the Emperor's Cup and promotion as champions of the JSL Division 2 in 1982. They won their first Japanese league title in the 1987/88 season. Due to problems in the upcoming professionalization, Yamaha decided to relegate themselves and not be one of the J.League founder members.
They finished in 2nd place of the JFL 1st division, a division below the top flight, in 1993 and were promoted to the J1 league for 1994. The team welcomed Marius Johan Ooft as its manager, as well as the Brazilian national team captain Dunga and a number of foreign players to build a winning team. Dunga's football philosophy deeply influenced the club, initially as a player and currently as an advisor.


=== Glory years ===
In a seven-year period between 1997 and 2003, the club won a number of titles relying on Japanese players instead of foreigners who may leave on a transfer during the middle of the season. Within this period Júbilo won the J.League title three times, finished second three more and won each of the domestic cup competitions once. In 1999 they were also crowned Champions of Asia after winning the final match against Esteghlal F.C. and
121.000 spectators in Azadi Stadium.
In one of the most fruitful periods in J.League history, Júbilo broke several records and created some new ones. Amongst these are the most goals scored in a season (107 in 1998); the fewest goals conceded in a season (26 in 2001); the biggest goal difference (plus 68 goals in 1998); and the largest win (9–1 against Cerezo Osaka in 1998). In 2002, the team won both stages of the championship, a first in J.League history, and the same year the team had a record seven players selected for the J.League Team of the Year. All of these records still stand today.


=== Today ===

Since their last cup triumph in the 2003 Emperor's Cup, the squad which took them to such heights began to age. Without similarly skilled replacements coming through the youth team or from outside, Júbilo's power started to fade, and in 2007 the club ended the season in a record worst position of 9th. Perhaps more concerning to Júbilo supporters is their eclipse in recent seasons by bitter local rivals Shimizu S-Pulse who, in ending the season above Júbilo every year since 2006, have become Shizuoka prefecture's premier performing team. In 2008 they finished 16th out of 18 – their lowest position in the 18-club table – but kept their J1 position by defeating Vegalta Sendai in the promotion/relegation playoff.
In 2013 season, it took them until 8th week to make their first win in the league matches, and never move up higher than 16th since they were ranked down to 17th as of the end of 5th week. Then eventually suffered their first relegation to 2014 J.League Division 2 after they were defeated by Sagan Tosu at their 31st week match.
Júbilo were promoted back to J1 in 2015 after finishing runners-up.


== Honours ==


== Rivalries ==
Júbilo's closest professional rivals are S-Pulse from Shizuoka. Júbilo also has rivalries with Kashima Antlers and Yokohama Marinos, with whom they traded the Japanese league championship since the late 1980s. During the Japan Soccer League days they had a more local derby with Honda, across the Tenryu in Hamamatsu, but as Honda has long resisted professionalism, competitive matches between them since 1994 are a rarity.


== Record as J.League member ==
KeyTms. = Number of teams
Pos. = Position in league
Attendance/G = Average league attendance
Source: J.League Data Site


== Players ==


=== Current squad ===
As of 28 June 2020
Note: Flags indicate national team as defined under FIFA eligibility rules. Players may hold more than one non-FIFA nationality.


=== Out on loan ===
Note: Flags indicate national team as defined under FIFA eligibility rules. Players may hold more than one non-FIFA nationality.


=== World Cup players ===
The following players have been selected by their country in the World Cup, while playing for Júbilo Iwata:

 Dunga (1998)
 Hiroshi Nanami (1998)
 Masashi Nakayama (1998, 2002)
 Toshihiro Hattori (1998, 2002)
 Takashi Fukunishi (2002, 2006)
 Kim Jin-Kyu (2006)
 Yūichi Komano (2010)
 Masahiko Inoha (2014)


=== Award winners ===
The following players have won the awards while at Júbilo Iwata:

J.League Player of the Year
 Dunga (1997)
 Masashi Nakayama (1998)
 Toshiya Fujita (2001)
 Naohiro Takahara (2002)
J.League Top Scorer
 Masashi Nakayama (1998, 2000)
 Naohiro Takahara (2002)
 Ryoichi Maeda (2009, 2010)
J.League Best XI
 Hiroshi Nanami (1996, 1997, 1998, 2002)
 Dunga (1997, 1998)
 Tomoaki Ōgami (1997)
 Masashi Nakayama (1997, 1998, 2000, 2002)
 Daisuke Oku (1998)
 Toshiya Fujita (1998, 2001, 2002)
 Makoto Tanaka (1998)
 Takashi Fukunishi (1999, 2001, 2002, 2003)
 Arno van Zwam (2001)
 Toshihiro Hattori (2001)
 Go Oiwa (2001)
 Hideto Suzuki (2002)
 Makoto Tanaka (2002)
 Naohiro Takahara (2002)
 Yoshikatsu Kawaguchi (2006)
 Ryoichi Maeda (2009, 2010)
 Yūichi Komano (2012)
J.League Rookie of the Year
 Robert Cullen (2005)
J.League Cup MVP
 Nobuo Kawaguchi (1998)
 Ryoichi Maeda (2010)
J.League Cup New Hero Award
 Hiroshi Nanami (1996)
 Naohiro Takahara (1998)
 J2 League Top Scorer 
 Jay Bothroyd (2015)


=== Club captains ===
 Morishita Shinichi 1994
 Mitsunori Yoshida 1995
 Masashi Nakayama 1996-1998
 Toshihiro Hattori 1999-2005
 Takashi Fukunishi 2006
 Hideto Suzuki 2007
 Yoshikatsu Kawaguchi 2008
 Takano Takano 2009
 Nasu Daisuke 2010-2011
 Yamada Daiki 2012-2013
 Daisuke Matsui 2014
 Ryoichi Maeda 2014
 Kota Ueda 2015-2016
 Kentaro Oi 2017
 Sakurauchi Nagisa 2018-


=== Former players ===
Players with senior international caps:


== Managers ==


== In popular culture ==
In the Captain Tsubasa manga series, three characters was players of Júbilo Iwata. The midfielders Taro Misaki and Hanji Urabe, and the defender Ryo Ishizaki.


== References ==


== External links ==
Official Jubilo Iwata site
(in Japanese) Official Jubilo Iwata site