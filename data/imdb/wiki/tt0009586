Scrambling (also known as alpine scrambling) is "a walk up steep terrain involving the use of one's hands". It is an ambiguous term that lies somewhere between hiking, hillwalking, mountaineering, and rock climbing. Canyoning often involves scrambling.
Alpine scrambling is scrambling in high mountains like the Alps and the Rockies of North America, and may not follow a defined or waymarked path. The Mountaineers climbing organization defines alpine scrambling as follows:

Alpine Scrambles are off-trail trips, often on snow or rock, with a 'non-technical' summit as a destination. A non-technical summit is one that is reached without the need for certain types of climbing equipment (body harness, rope, protection hardware, etc), and not involving travel on extremely steep slopes or on glaciers. However, this can mean negotiating lower angle rock, traveling through talus and scree, crossing streams, fighting one's way through dense brush, and walking on snow-covered slopes.


== Overview ==

Although ropes may be sensible on harder scrambles, sustained use of rope and belay counts as climbing; typically, the use of ropes in scrambling is limited to basic safety uses.
While much of the enjoyment of scrambling depends on the freedom from technical apparatus, unroped scrambling in exposed situations is potentially one of the most dangerous of mountaineering activities. For this reason, mountain walkers are advised to carry a rope on harder scrambles. Scramblers are also advised to know their limits and to turn back before they get into difficulties.Some mountain tops may be reached by walking or scrambling up their least-steep side.


== Hazards and safety ==

Many easy scrambles become serious climbs in bad weather. Black ice or verglas is a particular problem in cold weather, and mist or fog can disorient scramblers very quickly.
Hypothermia is an insidious danger for all hikers and especially inexperienced hikers. Weather does not need to be very cold to be dangerous since ordinary rain or mist has a strong cooling effect. In mild weather, hikers may neglect to bring appropriate clothing or gear for keeping dry and warm enough for safety.
Scramblers normally travel equipped with a waterproof jacket and other protective clothing, as well as emergency supplies of food and drink. A large-scale map is also needed, so that the route can be followed with accuracy, and escape made via recognised paths in the case of bad weather or injury.The ten essentials, and a companion are recommended for any scramble, as well as the need to leave details of the planned trip with a responsible person. Where snow conditions exist, a helmet, ice axe, crampons, and the knowledge to use them, are also recommended for mountain hikes.


== Classification systems ==

In the U.S., scrambling is Class 3 in the Yosemite Decimal System of climb difficulties. In the British system it is Easy with some of the harder scrambles incorporating moves of Moderate or even Difficult standard.Some guidebooks on scrambling may rate the routes as follows:

easy — generally, just off-trail hiking with minimal exposure (if at all) and perhaps a handhold or two. UIAA Class I.
moderate — handholds frequently needed, possible exposure, route-finding skills helpful. UIAA Class II.
difficult — almost constant handholds, fall distance may be fatal, route-finding skills needed, loose and downsloping rock. Less-experienced parties may consider using a rope for short sections. YDS class 3, 4, and possibly 5.In Britain scrambles are usually rated using Ashton's system of either Grade 1, 2, 3 or 3S (S for serious), with the grade being based around technical difficulty and exposure. The North Ridge of Tryfan in Snowdonia, or Striding Edge on Helvellyn in the Lake District, are classic Grade 1 scrambles. At the other end of the scale, Broad Stand on Scafell is usually considered Grade 3 or 3S. Some of the older Scottish guidebooks used a system of grades 1 to 5, leading to considerable confusion and variation over grades 1, 2 and 3 in Scotland.


== By country ==


=== Canada ===

A guide to 175 scrambling routes in the Canadian Rockies can be found in Scrambles in the Canadian Rockies by Alan Kane. Backpacker magazine has twice featured the book as an expedition guide. The Canadian Alpine Journal referred to it as a "scree gospel".


=== Italy ===

Via ferrata is "a mountain route equipped with fixed ladders, cables, and bridges in order to be accessible to climbers and walkers" common in the Dolomites of Italy. This form of scrambling has climbing aids built in on the route that help make it safe. The essence of a modern via ferrata is a steel cable which runs along the route and is periodically (every 3 to 10 metres (9.8 to 32.8 ft)) fixed to the rock. Using a via ferrata kit, climbers can secure themselves to the cable, limiting any fall. The cable can also be used as an aid to climbing, and additional climbing aids, such as iron rungs (stemples), pegs, carved steps and even ladders and bridges are often provided. Thus, via ferratas allow otherwise dangerous routes to be undertaken without the risks associated with unprotected scrambling and climbing or the need for climbing equipment such as ropes. Such routes allow the relatively inexperienced a means of enjoying dramatic positions and accessing difficult peaks, normally the preserve of the serious mountaineer; although, as there is a need for some equipment, a good head for heights and basic technique, the via ferrata can be seen as a distinct step up from ordinary mountain walking.


=== United Kingdom ===
Scrambling has become an increasingly popular form of mountaineering in Britain; the English Lake District, the Scottish Highlands, and the north of Snowdonia in Wales being the chief regions of interest. Popular Scrambling guidebooks exist for these areas, outlining a wide spectrum of routes in terms of both difficulty and style. UK scrambles come in many forms: the ascents, descents and traverses of ridges, gully and ghyll scrambles, rakes (lines of weakness that penetrate large, and often sheer, rock faces), and the ascents of buttresses and faces. UK scrambles vary enormously in length; from as little height gain as 30 metres, as with many crag rock climbs, up to the 700+ metres of vertical height gain encountered on Tower Ridge, a famous 3S grade scramble that ascends Ben Nevis via its north face (Tower Ridge is also graded as a 'difficult' rock climb - arguably Britain's longest.). Scrambles in Snowdonia typically feature between 60 (e.g. Milestone Gully, Tryfan.) and 500 metres (e.g. Bryant's Gully, Glyder Fawr) of height gain.
Ridge routes that involve some scrambling are especially popular in Britain, including Crib Goch on Snowdon, Bristly Ridge on Glyder Fach, Striding Edge on Helvellyn, and Sharp Edge on Blencathra, both in the English Lake District, as well as numerous routes in Scotland, such as the Aonach Eagach ridge in Glencoe. Many of these routes include a "bad step", where the scrambling suddenly becomes much more serious. The bad step on Crib Goch for example, involves only 20 feet (6.1 m) or so of climbing, but the position is exposed. The rock face here is well polished by countless boots, but there are many holds which offer firm support. By contrast, the traverse of the Cuillin Ridge on Skye demands use of a rope at one point at least. The ridge routes of Liathach and Beinn Eighe in Wester Ross are easier to traverse but are extremely exposed. Descent from such ridges is very limited, so once committed, the scrambler must continue to the end. An Teallach to the north offers scrambling, as does Stac Pollaidh  further north in Sutherland, which includes a bad step.
One resource for scramblers in Britain are the guides by W A Poucher (1891–1988), though these are now dated and more recent guide books exist.
A community project consisting of a comprehensive list of scrambles in the United Kingdom is available at UKscrambles.com.


=== Poland and Slovakia ===

Scrambling has grown ever so popular in recent years with more experienced mountain hikers in Tatras, the highest mountain range within the 1500 km-long chain of Carpathians, located on the border between Poland and Slovakia. A hiker who has scaled all possible marked tourist trails will in time typically start looking for more ambitious goals, namely those peaks that by law are only accessible via hiring a licensed mountain guide (for a fee). More adventurous individuals, however, or those on a tight budget (often college/university students) endeavour to climb those harder accessible summits without assistance, especially since very accurate descriptions of (typically "normal", i.e. the easiest) routes are readily available on the Internet. Apart from dedicated websites, guidebooks for rock climbers by pl:Witold Henryk Paryski or pl:Władysław Cywiński (Poland) and sk:Arno Puškáš (Slovakia) are particularly popular. There is an informal peak bagging challenge called pl:Wielka Korona Tatr / Velka Korona Tatier (or Great Crown of Tatras), involving climbing Tatras' all fourteen 8-thousanders (in feet), only 3 of which are accessible by marked hiking trails. Typically, they are graded at most I or II in Tatra climbing difficulty scale (equivalent to British Grade 3/3s).


== See also ==
Peak bagging
Sure-footedness
Walking in the United Kingdom


== References ==


== External links ==
British Mountain Council: Hill skills (Scrambling)
GoXplore Guides article on Scrambling