"Poor Little Fool" is a rock and roll song written by Sharon Sheeley and first recorded by Ricky Nelson in 1958.


== Background ==
Sheeley wrote the song when she was fifteen years old. She had met Elvis Presley, and he encouraged her to write. It was based on her disappointment following a short-lived relationship with Don Everly of The Everly Brothers. Sheeley sought Ricky Nelson to record the tune. She drove to his house, and claimed her car had broken down. He came to her aid, and she sprang the song on him. Her version was at a much faster tempo than his recording.
The song was recorded by Ricky Nelson on April 17, 1958, and released on Imperial Records through its catalog number: 5528. On August 4, 1958 it became the first number-one song on Billboard magazine's then-new Hot 100 chart, replacing the magazine's Jockeys and Top 100 charts. It spent two weeks at the number-one spot. It also reached the top ten on the Billboard Country and Rhythm and Blues charts. Following its success, Sheeley worked with Eddie Cochran.
"Poor Little Fool" became a radio hit when it was released as part of a four-song Extended Play 45 rpm disc which was excerpted from the artist's second LP, Ricky Nelson. Responding to the buzz, Lew Chudd, the founder and head of Imperial Records, rushed out a single version (on both 45 and 78 rpm). Nelson objected, however, believing that the move would hurt sales of the EP. Under his contract with Imperial, the singer had approval rights for all picture-sleeve art and to express his displeasure with Chudd's decision, he chose not to select a photograph for the "Poor Little Fool" single. As a result, "Poor Little Fool" was the only Ricky Nelson single released by Imperial to be issued in the United States without a photo in a plain label-cut-out sleeve.


== Charts ==


== Other versions ==
The "Dodgers" and Johnny Angel released a cover version of the song in 1958 on Skyway 45-119-AA.
The Fleetwoods recorded it in 1962.
Terry Black released a version of the song in 1965 on his debut album, Only 16, and it reached #6 in Canada.


== See also ==
List of Billboard Hot 100 number-one singles from 1958 to 1969


== References ==