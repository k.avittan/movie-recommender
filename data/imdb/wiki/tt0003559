"Michael, Row the Boat Ashore" (also called "Michael Rowed the Boat Ashore", "Michael, Row Your Boat Ashore", or "Michael Row That Gospel Boat") is an African-American spiritual first noted during the American Civil War at St. Helena Island, one of the Sea Islands of South Carolina. The best-known recording was released in 1960 by the U.S. folk band The Highwaymen; that version briefly reached number-one hit status as a single. 
It was sung by former slaves whose owners had abandoned the island before the Union navy arrived to enforce a blockade. Charles Pickard Ware was an abolitionist and Harvard graduate who had come to supervise the plantations on St. Helena Island from 1862 to 1865, and he wrote down the song in music notation as he heard the freedmen sing it. Ware's cousin William Francis Allen reported in 1863 that the former slaves sang the song as they rowed him in a boat across Station Creek.The song was first published in 1867 in Slave Songs of the United States by Allen, Ware, and Lucy McKim Garrison. Folk musician and educator Tony Saletan rediscovered it in 1954 in a library copy of that book. The song is cataloged as Roud Folk Song Index No. 11975.


== Lyrics ==
One of the oldest published versions of the song runs in a series of unrhymed couplets:

The same source attests another version in rhyme:

This song originated in oral tradition, and there are many versions of the lyrics. It begins with the refrain, "Michael, row the boat ashore, Hallelujah." The lyrics describe crossing the River Jordan, as in these lines from Pete Seeger's version:

The River Jordan was where Jesus was baptised and can be viewed as a metaphor for deliverance and salvation, but also as the boundary of the Promised Land, death, and the transition to Heaven.
According to William Francis Allen, the song refers to the Archangel Michael. In the Roman Catholic interpretation of Christian tradition, Michael is often regarded as a psychopomp or conductor of the souls of the dead.The spiritual was also recorded on Johns Island during the 1960s by American folk musician and musicologist Guy Carawan and his wife, Candie Carawan. Janie Hunter, former singer of the Moving Star Hall singers, noted that her father, son of former slaves, would sing the spiritual when he rowed his boat back to the shore after catching fish.

(repeated thus until end)
A similar version was collected by Guy Carawan on an unspecified Sea Island.

(repeated thus until end)
Harry Belafonte sang a rather different rendition on his album Midnight Special which combines elements drawn from Christianity, American slavery, and Civil Rights Movement.  The lyrics work their way through different parts of the Biblical narrative before concluding with the following verses:


== Recordings ==
The version of "Michael, Row the Boat Ashore" that is widely known today was adapted by Boston folksinger and teacher Tony Saletan, who taught it to Pete Seeger in 1954. Saletan, however, never recorded it. Seeger taught it to the Weavers, who performed it at their Christmas Eve 1955 post-blacklist reunion concert. A recording of that performance was released in 1957 on an album titled The Weavers on Tour. In the same year, folksinger Bob Gibson included it on his Carnegie Concert album. The Weavers included an arrangement in The Weavers' Song Book, published in 1960. Similarly, Seeger included it in his 1961 songbook, American Favorite Ballads, with an attribution to Saletan. The American folk quintet the Highwaymen had a #1 hit in 1961 on both the pop and easy listening charts in the U.S. with their version, under the simpler title of "Michael", recorded and released in 1960.   On Top 40 radio station WABC in New York City, the song was #1 for three weeks in August 1961.  This recording also went to #1 in the United Kingdom. Billboard ranked the record as the No. 3 song of 1961.The Highwaymen version that went to #1 on the Billboard charts had these lyrics:

The recording begins and ends with one of the singers whistling the tune a cappella, later accompanied by simple instruments, in a slow, ballad style.  All the Highwaymen sang and harmonized on the Michael lines but individual singers soloed for each set of additional lyrics.  This version differs from the Pete Seeger version by changing "meet my mother on the other side" to "milk and honey on the other side."  "Milk and honey" is a phrase used in the Book of Exodus during Moses' vision of the burning bush.  The original Negro spiritual mentions the singer's mother but the hit version does not.
Lonnie Donegan reached #6 in the UK Singles Chart with his cover version in 1961. Harry Belafonte recorded a popular version of it for his 1962 Midnight Special album.  Pete Seeger included it in his Children's Concert at Town Hall in 1963.  The Lennon Sisters recorded a version which was later featured as a bonus track from their album "The Lennon Sisters Sing Great Hits".
Trini Lopez had a minor hit with it in 1964. The Israeli-French singer Rika Zaraï also recorded a French version under the title "Michaël" in 1964.
The African-American gospel/folk duo Joe & Eddie recorded it for their "Walking Down the Line" album in 1965. The Carawans' recording from St. Johns Island of "Jane Hunter and three Moving Star Hall singers" of a traditional "Row, Michael, Row," was released by Smithsonian Folkways Records in 1967 on the album, Been in the Storm So Long. Later, the song was recorded by The Beach Boys for their 1976 15 Big Ones album but was left off the final running order. Brian Wilson rearranged the song, giving it a rich arrangement with sound similar to the many other covers recorded during this period, including a complex vocal arrangement. Mike Love sang lead vocals. Richard Jon Smith's version spent nine weeks in mid-1979 at #1 in South Africa.The counselors sing the song, along with "Down in the Valley" in the opening scene of the 1980 horror film, Friday the 13th.
A German version is "Michael, bring dein Boot an Land" by Ronny(de). A German gospel version is "Hört, wen Jesus glücklich preist" (A song of the Beatitudes).  German disco group Dschinghis Khan recorded a version of it in 1981.
The Smothers Brothers did a fairly straightforward version of the song on their album It Must Have Been Something I Said!, before turning it into a comic sing-along on Golden Hits of the Smothers Brothers, Vol. 2 (which is also included on their album Sibling Revelry: The Best of the Smothers Brothers).
Sule Greg Wilson produced a version based upon Allen/Ware/Garrison, as well as Row, Michael Row, by Jane Hunter and Moving Star Hall singers.  Featuring Tuscarora vocalist Pura Fé (with Wilson on instruments and background vocals), this version was used for the end credits of The Librarian and the Banjo, Jim Carrier's 2013 film on Dena Epstein, author of the book, Sinful Tunes and Spirituals.
Raffi sings this song on his Bananaphone album.
Peter, Paul and Mary sing this song on their Around the Campfire album.
The melody of this song also appears in a hymn entitled Glory be to God on High.


== See also ==
Christian child's prayer § Spirituals


== References ==


== External links ==
Audio sample of the song performed by the German choir Outta Limits
Free MP3 Download for use in services performed by Richard Irwin