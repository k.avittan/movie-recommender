Lieutenant Colonel Herbert Jones,  (14 May 1940 – 28 May 1982), known as H. Jones, was a British Army officer and posthumous recipient of the Victoria Cross (VC). He was awarded the VC after being killed in action during the Battle of Goose Green for his actions as commanding officer of the 2nd Battalion, Parachute Regiment during the Falklands War.


== Background ==

Jones was born in Putney, the eldest of three sons of Herbert Jones (1888–1957), an American artist, and his Welsh wife, Olwen Pritchard (1902–1990), a nurse. He attended St. Peter's Preparatory School in Seaford, Sussex, Eton College and DLD College London. He joined the British Army on leaving school and on graduation from the Royal Military Academy, Sandhurst on 23 July 1960, was commissioned into the Devonshire and Dorset Regiment as a second lieutenant.He was promoted to lieutenant on 23 January 1962, captain on 23 July 1966, and major on 31 December 1972, At this time he was brigade major at HQ 3rd Infantry Brigade in Northern Ireland. As such he was responsible for the efforts to find Captain Robert Nairac who had been abducted by the Provisional IRA. Nairac and Jones had become friends and would sometimes go to the Jones household for supper. After a four-day search, the Garda Síochána confirmed that Nairac had been shot and killed in the Republic of Ireland after being smuggled over the border. On 13 December 1977 he was appointed a Member of the Order of the British Empire (MBE) for his services in Northern Ireland that year.  On 30 June 1979 he was promoted lieutenant colonel, and on 1 December 1979, he was transferred to the Parachute Regiment.  In the 1981 New Year Honours he was appointed an Officer of the Order of the British Empire (OBE).


== Falklands War ==

Jones was on holiday in France when he heard the news of the invasion of the Falklands. He had just returned from training in Kenya, and his battalion, 2nd Battalion The Parachute Regiment (2 PARA), was earmarked for deployment for Belize.
Jones' battalion was attached, alongside 3 PARA, to reinforce 3 Commando Brigade Royal Marines, the first major infantry formation to be sent south. During the Battle of Goose Green, an attack against entrenched Argentinian positions, with his unit pinned down by heavy fire from MAG machine guns and FAL automatic rifles, he led a charge against the nearest position. He was killed while doing so but the Argentinian unit surrendered shortly afterwards.  For his actions he was posthumously awarded the Victoria Cross.Command of 2 PARA passed to Major Chris Keeble. Jones' body was buried in a battlefield grave at Ajax Bay on 30 May. After the war it was exhumed and re-buried at the Blue Beach War Cemetery in San Carlos on 25 October 1982.
Ex-TA Para officer and military theorist Spencer Fitz-Gibbon wrote in 1995 that despite his undoubted courage H. did more to hinder than to help 2 Para, losing sight of the overall battle picture and failing to allow his sub-unit commanders to exercise Mission Command, before his fatal attempt to lead "A" Company forward from the position where they had become bogged down.Margaret Thatcher said "his life was lost, but his death was the turning point in the battle." The battle demonstrated the UK's increasingly unquestionable military superiority, quelling concerns about possible defeat, and led to the release of 112 civilians who had been imprisoned in the local community hall for the best part of a month.


=== VC citation ===
On 28th May 1982 Lieutenant Colonel Jones was commanding 2nd Battalion The Parachute Regiment on operations on the Falkland Islands. The Battalion was ordered to attack enemy positions in and around the settlements of Darwin and Goose Green.  During the attack against an enemy who was well dug in with mutually supporting positions sited in depth, the Battalion was held up just South of Darwin by a particularly well-prepared and resilient enemy position of at least eleven trenches on an important ridge. A number of casualties were received. In order to read the battle fully and to ensure that the momentum of his attack was not lost, Colonel Jones took forward his reconnaissance party to the foot of a re-entrant which a section of his Battalion had just secured. Despite persistent, heavy and accurate fire the reconnaissance party gained the top of the re-entrant, at approximately the same height as the enemy positions. From here Colonel Jones encouraged the direction of his Battalion mortar fire, in an effort to neutralise the enemy positions. However, these had been well prepared and continued to pour effective fire onto the Battalion advance, which, by now held up for over an hour and under increasingly heavy artillery fire, was in danger of faltering.  In his effort to gain a good viewpoint, Colonel Jones was now at the very front of his Battalion. It was clear to him that desperate measures were needed in order to overcome the enemy position and rekindle the attack, and that unless these measures were taken promptly the Battalion would sustain increasing casualties and the attack perhaps even fail. It was time for personal leadership and action. Colonel Jones immediately seized a sub-machine gun, and, calling on those around him and with total disregard for his own safety, charged the nearest enemy position. This action exposed him to fire from a number of trenches. As he charged up a short slope at the enemy position he was seen to fall and roll backward downhill. He immediately picked himself up, and again charged the enemy trench, firing his sub-machine gun and seemingly oblivious to the intense fire directed at him. He was hit by fire from another trench which he outflanked, and fell dying only a few feet from the enemy he had assaulted. A short time later a company of the Battalion attacked the enemy, who quickly surrendered. The display of courage by Colonel Jones had completely undermined their will to fight further.
Thereafter the momentum of the attack was rapidly regained, Darwin and Goose Green were liberated, and the Battalion released the local inhabitants unharmed and forced the surrender of some 1,200 of the enemy.
The achievements of 2nd Battalion The Parachute Regiment at Darwin and Goose Green set the tone for the subsequent land victory on the Falklands. The British achieved such a moral superiority over the enemy in this first battle that, despite the advantages of numbers and selection of battle-ground, the Argentinian troops never thereafter doubted either the superior fighting qualities of the British troops, nor their own inevitable defeat.

This was an action of the utmost gallantry by a Commanding Officer whose dashing leadership and courage throughout the battle were an inspiration to all about him.
His Victoria Cross is displayed at the National Army Museum in Chelsea, London.


== Memorials ==
Jones' grave at Blue Beach War Cemetery is marked with a headstone engraved with the Parachute Regiment's insignia and that of the Victoria Cross. The headstone includes the quotation: "He is not the beginning but the continuing of the same unto the end." A street in Stanley was named H Jones Road in his memory in addition to Jones Avenue in Mount Pleasant air base. A memorial stone to all those killed at the scene of the battle, near Darwin, also bears his name. His name is also on the South Atlantic Task Force Memorial in St Paul's Cathedral, London, on the wall with the names of the fallen in the Falklands Memorial Chapel at Pangbourne College, and the Parachute Regiment Memorial at their headquarters in Aldershot; he also has a memorial in the cloisters of Eton College and a plaque on a footpath at Kingswear, Devon. The memorial board from St Peter's School,  carved with the name of Jones can be seen in Seaford Museum. In addition the 'Colonel H' Public house in Great Yarmouth, Norfolk is named in his honour. There is a wooden plaque memorial in Kingswear parish church and a copy of the citation is on view near the memorial.


== Family ==
Jones's widow, Sara, was appointed a Commander of the Order of the British Empire (CBE) for charity work (she is involved with a number of charities related to the armed forces), and since 2003 has been a Deputy Lieutenant of Wiltshire. Both of their sons, Rupert and David, served as infantry officers in the Devon and Dorsets (now merged into The Rifles). Rupert commanded the 1st Mechanised Brigade from 2012 to 2014, and was promoted to major general in 2016, making him, at 47, the youngest general officer in the British Army at the time.


== Notes ==


== References ==


== Further reading ==
H. Jones VC: The Life and Death of an Unusual Hero (John Wilsey, Hutchinson, London, 2002, ISBN 0-09-179355-6)
2 Para's Battle for Darwin Hill and Goose Green by David J Kenney ISBN 0-9660717-1-9


== External links ==
VCs of the Falklands War (citation details)
H. Jones at Find a Grave
Malvinas, Soldado Ledesma, abatió en combate al Tnte. Coronel Jones on YouTube (in Spanish)