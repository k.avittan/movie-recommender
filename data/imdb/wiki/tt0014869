The Enchanted Cottage is a 1945 romantic fantasy starring Dorothy McGuire, Robert Young, and Herbert Marshall, with Mildred Natwick.
It was based on the 1923 play by Arthur Wing Pinero.  The Enchanted Cottage was first adapted for the silent screen in 1924, with Richard Barthelmess and May McAvoy as the newlyweds.  A third adaptation appeared in 2016.


== Plot ==
When pilot Oliver Bradford is disfigured by war wounds, he hides from his family, including his mother, after his fiancée is too jarred by his disfigurement to accept it readily. He lives in bitter seclusion in the seaside New England cottage he had rented from its current owner, Mrs. Minnett, for his originally planned honeymoon, while blind concert pianist John Hillgrove who lives nearby befriends him gradually.
Laura Pennington is a shy, homely maid who has hired on as the cottage's caretaker and befriends an initially reluctant Oliver after he admires her wood-carving talents. Oliver and Laura gradually fall in love and marry, but after Oliver and Laura fear their marriage is one of mutual pity, the couple discovers that their feelings for each other have mysteriously transformed them. He appears handsome to her and she seems beautiful to him. This "transformation" is perceived only by the two lovers (and the audience). Laura believes that the cottage is "enchanted" because it was rented to honeymoon couples, and in time, the widowed Mrs. Minnett reveals the true story behind the cottage's legend.


== Cast ==
Dorothy McGuire as Laura Pennington
Robert Young as Oliver Bradford
Herbert Marshall as Major John Hillgrove
Mildred Natwick as Mrs. Abigail Minnett
Spring Byington as Violet Price
Hillary Brooke as Beatrice Alexander
Richard Gaines as Frederick 'Freddy' Price
Alec Englander as Danny 'Taxi' Stanton
Robert Clarke as Marine Corporal
Eden Nichols as Soldier


== Production ==
According to Jeremy Arnold of TCM, Robert Young told Leonard Maltin in a 1986 interview that he considered The Enchanted Cottage to be "the best love story that's ever been written. [It] was one of those films I hated to see end. I wanted it to go on and on and on. It was such a joy to do." Young later named a home he built in California “The Enchanted Cottage.”Arthur Wing Pinero's play, written in 1921 and first performed in 1922, was filmed in 1924 as a timely story involving physical and emotional disabilities following the First World War.  In the stage directions, Oliver is described as an emaciated wreck of a man, broken by the war. The dialogue reveals that he was wounded in the neck and that it is painful to straighten his left leg, but he says his “head,” meaning his mind,  is the worst.  In the 1924 film, his body is contorted, he walks with a cane, and he cannot put his right foot to the ground. In the 1945 film, most of Oliver's face is scarred, and he has completely lost the use of his right arm and hand. He can walk without aids and there is no visible impairment of his lower body.
The original play and film were set in England, and the history of the “honeymoon” couples extended back into the Tudor period. The “shadows” of three of the historical couples appeared onstage. Elaborate fantasy sequences, representing Laura's dreams and fears, played key roles in the story. They were eliminated from the 1945 version.
In the play, Mrs. Minnett's uncanny gifts are made more explicit: She comes from a long line of witches (witches can be good, Laura hastens to say). She is more damaged by her grief. The cast of the play is larger, including the local rector and his wife, and the couple's blind friend, Major Hillgrove, believes in the transformation unlike the character portrayed by Herbert Marshall.RKO Producer Harriet Parsons acquired the rights for her studio for an updated World War II version set in New England. When RKO management took the movie away from her and gave it to producer-writer Dudley Nichols, Hollywood columnist Hedda Hopper wrote a strongly worded newspaper editorial criticizing RKO for gender bias. The outcry from the column led RKO to change its plan and give the property back to Harriet Parsons.Parsons wrote an outline of the updated story about a World War II veteran. She engaged DeWitt Bodeen for the screenplay, and the two became lifelong friends. Parsons also selected John Cromwell as director. David O. Selznick lent RKO Dorothy McGuire for the movie, and MGM lent Robert Young, who re-teamed with McGuire after her debut in Claudia.
Parsons contributed to the screenplay along with Herman J. Mankiewicz, who was hired by Cromwell to touch up Bodeen's screenplay.Composer Roy Webb wrote a piano concerto for the movie that a blinded World War I veteran (Herbert Marshall) uses as a tone poem to describe the story of the two protagonists to a gathering of their friends. Webb was nominated for an Academy Award for Best Original Score in 1945 and performed the concerto at the Hollywood Bowl later that year. Marshall, who had lost a leg in World War I, played his role as a blind man with the help of special contact lenses.
Dorothy McGuire insisted her character show her plainness with no makeup, ill-fitting clothes and a drab hairstyle. When McGuire was filmed looking appealing to Robert Young, her character had similar costumes that were well tailored.


== Reception ==
The movie was released in April 1945. Not all the critics were enthusiastic. Writing for The New York Times, critic Bosley Crowther observed
[The play and first film] concerned the illusion of beauty and the moral courage which was mutually found by a homely girl and a maimed war veteran when they viewed each other through the eyes of love. It is a theme which is quite as appealing today as it was back then and one which can bear repeating in modern and practical detail. However, the current film version of the wistful drama... contemporizes the subject in peculiarly obsolete terms. Despite all the marvelous advances in plastic surgery, it assumes that a shattered Air Force pilot would be returned to society with a face very badly disfigured and frightening to behold. It forgets that a casualty quite as bitter as the American hero in this case would be studiously rehabilitated through modern treatment before being dismissed. And it violates an obvious tenet of feminine beauty culture today—which is that a girl of moderate features (and fair intelligence) can make herself look very sweet.
[T]he deep and studied poignance of this elaborately heart-torturing film appears not only unreasonable but very plainly contrived. It is hard to believe that a depressed veteran's entire recuperation would be allowed to devolve upon a fustrated [sic] girl, an intuitive blind man and a honeymoon cottage possessing charm. And it is fair to insist that no young lady with a face and figure such as that of Dorothy McGuire would permit herself to look so dingy and woebegone as she does in this film.
A review in Variety on December 31, 1944 stated: “Sensitive love story of a returned war veteran with ugly facial disfigurements, and the homely slavey—both self-conscious of their handicaps—is sincerely told both in the script [based on the play by Arthur Wing Pinero] and outstanding direction of John Cromwell.”In 2018, Jeremy Arnold wrote for TCM that

The Enchanted Cottage is a movie with its heart in the right place. Anyone who has ever been in love can relate to the sensation that one's partner becomes more beautiful as one's love deepens. The Enchanted Cottage illustrates this phenomenon to full and lovely effect, with its allegorical yet delicate story of the power of love to physically transform a couple.On Rotten Tomatoes the film hold a rating of 83% among critics and 82% among viewers.


=== Box office ===
The movie made a profit of $881,000.


=== Accolades ===
The film is recognized by American Film Institute in these lists:

2002: AFI's 100 Years...100 Passions – Nominated


== Adaptations to other media ==
The Enchanted Cottage was adapted as a radio play on the September 3, 1945 broadcast of Lux Radio Theater with Robert Young and Dorothy McGuire reprising their movie roles, and on the December 11, 1946 broadcast of Academy Award Theater, starring Peter Lawford and Joan Lorring.
It was broadcast live on the General Electric Theater radio program on September 24, 1953. Joan Fontaine had the starring role.
The movie also forms the basis of one of the films that Manuel Puig used to compose his novel Kiss of the Spider Woman. The movie is the basis of chapter 5 in which Molina tells a film to himself, one in which he imagines the advantages of lovers' bodies being adapted to the "true love" of their souls, a reality that would make possible supposedly impossible romantic possibilities for himself and his married love interest, Gabriel.
On her variety show, Carol Burnett performed a parody of the film titled "The Enchanted Hovel", with herself in Dorothy McGuire's role and Dick Van Dyke in Robert Young's role.


== See also ==
Gothic romance film


== References ==


== External links ==
The Enchanted Cottage on IMDb 
The Enchanted Cottage at the TCM Movie Database 
The Enchanted Cottage at AllMovie 
The Enchanted Cottage at the American Film Institute Catalog