Hoodoo was created by enslaved Africans after they arrived to the New World. It is the product of enslaved people within the boundaries of North America who faced suffering on a daily basis, yet refused to relinquish all of their power and identity. Hoodoo is a mixture of African and Native American practices as well as European Christian folk practices. Hoodoo, in addition to being a body of botanical and esoteric knowledge, is a rebellion against mental and spiritual domination by Europeans.  Hoodoo has a long history of being tied to class struggle, hardship, and looking to one's ancestors in trying times. It is also described as being a counter-culture of the down-trodden and the underdog. Also known as Lowcountry Voodoo in the Gullah South Carolina Lowcountry, Hoodoo is an amalgamation of spiritual practices, traditions, and beliefs that were held in secret away from white slaveholders. Following the Great Migration, Hoodoo spread throughout the United States. 

Regional synonyms for hoodoo include conjuration, witchcraft, or rootwork.


== History ==


=== Origins ===
Approximately 388,000 African people from various ethnic groups were shipped to British North America (present day Canada and The United States of America) between the seventeenth and nineteenth centuries as a result of the transatlantic slave trade. They were Kongo, Igbo, Akan, Mandé, Yoruba, Fon, Ewe, and Fulbe, among many others. Hoodoo began not long after Indigenous Americans, enslaved Africans, and Europeans came into daily contact with one another during the colonial era. The most widely accepted narrative categorizes Hoodoo as a primarily African retention with Native American and European influences. However that is only a theory and a difficult one to prove. One reason is because some of the elements of rootwork that are classified as African retentions (such as bone divination, quartered ideographic religious symbols, medicine bundles, etc.) are ubiquitous to many Indigenous American cultures as well. The extent to which Hoodoo could be practiced varied by region and the temperament of the slave owners. Enslaved Africans of the Southeast, known as the Gullah, experienced an isolation and relative freedom that allowed for retention of the practices of their West African ancestors. Root work or Hoodoo, in the Mississippi Delta where the concentration of enslaved African-Americans was dense, was practiced under a large cover of secrecy. Hoodoo was first documented in American English in 1875 and was used as a noun (the practice of hoodoo) or as a transitive verb, as in "I hoodoo you," an action carried out by varying means. The hoodoo could be manifest in a healing potion, or in the exercise of a parapsychological power, or as the cause of harm which befalls the targeted victim. In African-American Vernacular English (AAVE), Hoodoo is often used to refer to a paranormal consciousness or spiritual hypnosis, a spell, but Hoodoo may also be used as an adjective for a practitioner, such as in "Hoodoo man". Known hoodoo spells date back to the 1800s. Spells are dependent on the intention of the practitioner and "reading" of the client.


=== Zora Neale Hurston ===
Zora Neale Hurston, an African-American cultural anthropologist and Hoodoo initiate, reports in her essay Hoodoo in America, that conjure, had its highest development along the Gulf Coast, particularly in the city of New Orleans and in the surrounding country. It was those regions that were settled by Haitian immigrants at the time of the overthrow of the French rule in Haiti by Toussaint Louverture. Thirteen-hundred Haitians, (of African descent along with their White ex-masters) were driven out, and the nearest French refuge was the province of Louisiana. They brought with them their conjure rituals modified by contact with European culture, such as the Catholic church. While some retained Haitian Vodou, others began practicing Hoodoo. Unlike the North American slaves who were traded like livestock or any other commodity with no thought given to family ties, island slaves were encouraged to make themselves as much at home as possible in their bondage, and thus retained more of their West African background, customs, and language than the continental slaves. Thirteen-hundred Haitians (including former slaves and their slave masters) settled in Louisiana from 1791 to 1809 their French West African practices.


=== Development ===
The mobility of black people from the rural South to more urban areas in the North is characterized by the items used in Hoodoo. White pharmacists opened their shops in African American communities and began to offer items both asked for by their customers, as well as things they themselves felt would be of use. Examples of the adoption of occultism and mysticism may be seen in the colored wax candles in glass jars that are often labeled for specific purposes such as "Fast Luck" and "Love Drawing".
The Sixth and Seventh Books of Moses is a grimoire made popular by Europeans that is purportedly based on Jewish Kabbalah. It contains numerous signs, seals, and passages in Hebrew that are supposed to be related to Moses' ability to work wonders. Though its authorship is attributed to Moses, the oldest manuscript dates to the mid-19th century. Its importance in hoodoo among a few practitioners is summarized as follows:

I read de "Seven Books of Moses" seven or eight yeah a'ready ... de foundation of hoodooism came from way back yondah de time dat Moses written de book "De Seven Book of Moses".
 Hoodoo spread throughout the United States as African-Americans left the delta during the Great Migration. 


== Divination ==
There are several forms of divination traditionally used in Hoodoo.


=== Cleromancy ===
Involves the casting of small objects (such as shells, bones, stalks, coins, nuts, stones, dice, sticks,  etc.)


=== Cartomancy ===
Divination by means of interpreting cards.


=== Natural or Judicial Astrology ===
The study of positions and motions of celestial bodies in the belief that they have an influence over nature and human affairs.


=== Augury ===
The deciphering of phenomena (omens) that are believed to foretell the future, often signifying the advent of change. 


=== Oneiromancy ===
A form of divination based upon dreams.


== Practices ==


=== "Seeking" process ===

In a process known as "seeking" a Hoodoo practitioner will ask for salvation of your soul in order for a Gullah church to accept them. A spiritual leader will assist in the process and after believing the follower is ready they will announce it to the church. A ceremony will commence with much singing, and the practice of a ring shout.


=== Spirit mediation ===
The purpose of Hoodoo was to allow people access to supernatural forces to improve their lives. Hoodoo is purported to help people attain power or success ("luck") in many areas of life including money, love, health, and employment. As in many other spiritual and medical folk practices, extensive use is made of herbs, minerals, parts of animals' bodies, an individual's possessions.
Contact with ancestors or other spirits of the dead is an important practice within the conjure tradition, and the recitation of psalms from the Bible is also considered spiritually influential in Hoodoo. Due to Hoodoo's great emphasis on an individual's spiritual power to effect desired change in the course of events, Hoodoo's principles are believed to be accessible for use by any individual of faith. Hoodoo practice does not require a formally designated minister.


=== Spiritual supplies ===
Homemade powders, mojo hands, oils (van van oil, dragon's blood, etc.), and talismans form the basis of much rural Hoodoo, but there are also some successful commercial companies selling various Hoodoo products to urban and town practitioners. These are generally called spiritual supplies, and they include herbs, roots, minerals, candles, incense, oils, floor washes, sachet powders, bath crystals, icons, aerosols, and colognes. Many patent medicines, cosmetics, and household cleaning supplies for mainstream consumers have been aimed also at Hoodoo practitioners. Some products have dual usage as conventional and spiritual supplies, examples of which include the Four Thieves Vinegar, Florida Water, and Red Devil Lye.


=== Bottle tree ===

Hoodoo is linked to a popular tradition of bottle trees in the United States. According to gardener and glass bottle researcher Felder Rushing, the use of bottle trees came to the Old South from Africa with the slave trade. The use of blue bottles is linked to the "haint blue" spirit specifically. Glass bottle trees have become a popular garden decoration throughout the South and Southwest.


== Cosmology ==


=== God ===
Since the 19th century there has been Christian influence in Hoodoo thought. This is particularly evident in relation to God's providence and his role in retributive justice.  For example, though there are strong ideas of good versus evil, cursing someone to cause their death might not be considered a malignant act.  One practitioner explained it as follows:According to Carolyn Morrow Long, "At the time of the slave trade, the traditional nature-centered religions of West and Central Africa were characterized by the concept that human well-being is governed by spiritual balance, by devotion to a supreme creator and a pantheon of lesser deities, by veneration and propitiation of the ancestors, and by the use of charms to embody spiritual power. ...In traditional West African thought, the goal of all human endeavor was to achieve balance." Several African spiritual traditions recognized a genderless supreme being who created the world, was neither good nor evil, and which did not concern itself with the affairs of mankind. Lesser spirits were invoked to gain aid for humanity's problems.


=== God as conjurer ===
Not only is Yahweh's providence a factor in Hoodoo practice, but Hoodoo thought understands the deity as the archetypal Hoodoo doctor. On this matter Zora Neale Hurston stated, "The way we tell it, Hoodoo started way back there before everything. Six days of magic spells and mighty words and the world with its elements above and below was made." From this perspective, biblical figures are often recast as Hoodoo doctors and the Bible becomes a source of spells and is, itself, used as a protective talisman. This can be understood as a syncretic adaptation for the religion. By blending the ideas laid out by the Christian Bible, the faith is made more acceptable. This combines the teachings of Christianity that Africans brought to America were given and the traditional beliefs they brought with them.

A recent work on hoodoo lays out a model of hoodoo origins and development.  Mojo Workin: The Old African American Hoodoo System by Katrina Hazzard-Donald discusses what the author calls the ARC or African Religion Complex which was a collection of eight traits which all the enslaved Africans had in common and were somewhat familiar to all held in the agricultural slave labor camps known as plantations communities. Those traits included naturopathic medicine, ancestor reverence, counter clockwise sacred circle dancing, blood sacrifice, divination, supernatural source of malady, water immersion and spirit possession. These traits allowed Culturally diverse Africans to find common culturo-spiritual ground. According to the author, hoodoo developed under the influence of that complex, the African divinities moved back into their natural forces, unlike in the Caribbean and Latin America where the divinities moved into Catholic saints.This work also discusses the misunderstood "High John the Conqueror root" and myth as well as the incorrectly-discussed "nature sack".


=== Moses as conjurer ===
Hoodoo practitioners often understand the biblical figure Moses in similar terms. Hurston developed this idea in her novel Moses, Man of the Mountain, in which she calls Moses, "the finest Hoodoo man in the world." Obvious parallels between Moses and intentional paranormal influence (such as magic) occur in the biblical accounts of his confrontation with Pharaoh. Moses conjures, or performs magic  "miracles" such as turning his staff into a snake. However, his greatest feat of conjure was using his powers to help free the Hebrews from slavery. This emphasis on Moses-as-conjurer led to the introduction of the pseudonymous work the Sixth and Seventh Books of Moses into the corpus of hoodoo reference literature.


=== Bible as talisman ===
In Hoodoo, "All hold that the Bible is the great conjure book in the world." It has many functions for the practitioner, not the least of which is a source of spells. This is particularly evident given the importance of the book Secrets of the Psalms in hoodoo culture. This book provides instruction for using psalms for things such as safe travel, headache, and marital relations. The Bible, however, is not just a source of spiritual works but is itself a conjuring talisman. It can be taken "to the crossroads", carried for protection, or even left open at specific pages while facing specific directions. This informant provides an example of both uses:

Whenevah ah'm afraid of someone doin' me harm ah read the 37 Psalms an' co'se ah leaves the Bible open with the head of it turned to the east as many as three days.


=== Spirits ===

It is believed one's soul returns to God after death, however their spirit may still remain on Earth. Spirits can interact with the world by providing good fortune or bringing bad deeds. A spirit that torments the living is known as a Boo Hag.


== Differences from voodoo religions ==
Hoodoo shows evident links to the practices and beliefs of Fon and Ewe Vodun spiritual folkways. The folkway of Vodun is a more standardized and widely dispersed spiritual practice than Hoodoo. Vodun's modern form is practiced across West Africa in the nations of Benin, Togo, and Burkina Faso, among others. In the Americas, the worship of the Vodoun loa is syncretized with Roman Catholic saints. The Vodou of Haiti, Voodoo of Louisiana, Vodú of Cuba, and the Vudú of the Dominican Republic are related more to Vodun than to Hoodoo.


== See also ==
 Traditional African religion portal


== References ==

https://www.jstor.org/stable/535394?seq=1
https://www.simonandschuster.com/books/The-Hoodoo-Tarot/Tayannah-Lee-McQuillar/9781620558737
https://www.scribd.com/book/394271695/365-Days-of-Hoodoo-Daily-Rootwork-Mojo-Conjuration
http://www.mamiwata.com/hoodoointerview/hoodoointerview.html
http://www.mamiwata.com/hoodoo/hoodoo.html
http://www.mamiwata.com/hoodoo/page2.html
http://www.mamiwata.com/hoodoo/page3.html
Hoodoo, Rootwork, Conjure, Obeah at Curlie