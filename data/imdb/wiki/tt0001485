Prince George, Duke of Kent,  (George Edward Alexander Edmund; 20 December 1902 – 25 August 1942) was a member of the British royal family, the fourth son of King George V and Queen Mary. He was the younger brother of Edward VIII and George VI. 
Prince George served in the Royal Navy in the 1920s and then briefly as a civil servant. He became Duke of Kent in 1934. In the late 1930s he served as an RAF officer, initially as a staff officer at RAF Training Command and then, from July 1941, as a staff officer in the Welfare Section of the RAF Inspector General's Staff. He was killed in a military air-crash on 25 August 1942.
Both before and during his marriage, Prince George allegedly had a string of affairs, from socialites to Hollywood celebrities.


== Early life ==

Prince George was born on 20 December 1902 at York Cottage on the Sandringham Estate in Norfolk, England. His father was the Prince of Wales (later King George V), the only surviving son of King Edward VII and Queen Alexandra. 
His mother was the Princess of Wales (later Queen Mary), the only daughter and eldest child of the Duke and Duchess of Teck. At the time of his birth, he was fifth in the line of succession to the throne, behind his father and three older brothers Edward, Albert and Henry.
George was baptised in the Private Chapel at Windsor Castle on 26 January 1903 by Francis Paget, Bishop of Oxford.


== Education and career ==
Prince George received his early education from a tutor and then followed his elder brother, Prince Henry, to St Peter's Court, a preparatory school at Broadstairs, Kent. At the age of 13, like his brothers, the Prince of Wales (later King Edward VIII) and Prince Albert (later King George VI), before him, he went to naval college, first at Osborne and, later, at Dartmouth. He was promoted to sub-lieutenant on 15 February 1924, and was promoted to lieutenant on 15 February 1926.  He remained on active service in the Royal Navy until March 1929, serving on HMS Iron Duke and later HMS Nelson.After leaving the navy, he briefly held posts at the Foreign Office and later the Home Office, becoming the first member of the royal family to work as a civil servant. He continued to receive promotions after leaving active service: to commander on 15 February 1934 and to captain on 1 January 1937.From January to April 1931, Prince George and his elder brother the Prince of Wales travelled 18,000 miles on a tour of South America. Their outward voyage was on the ocean liner Oropesa. In Buenos Aires they opened a British Empire Exhibition. They continued from Río de la Plata to Rio de Janeiro on the liner Alcantara and returned from Brazil to Europe on the liner Arlanza, landing at Lisbon. The princes returned via Paris and an Imperial Airways flight from Paris–Le Bourget Airport that landed specially in Windsor Great Park.On 23 June 1936, George was appointed a personal aide-de-camp to his eldest brother, the new King Edward VIII. Following the abdication of Edward VIII, he was appointed a personal naval aide-de-camp to his elder brother, now George VI. On 12 March 1937, he was commissioned as a Colonel in the British Army and in the equivalent rank of group captain in the Royal Air Force (RAF). He was also appointed as the Colonel-in-Chief of the Royal Fusiliers from the same date.In October 1938 George was appointed Governor-General of Australia in succession to Lord Gowrie with effect from November 1939. On 11 September 1939 it was announced that, owing to the outbreak of the Second World War, the appointment was postponed.On 8 June 1939, George was promoted to the ranks of rear admiral in the Royal Navy, major-general in the British Army and air vice-marshal in the Royal Air Force. At the start of the Second World War, George returned to active military service with the rank of rear admiral, briefly serving in the Intelligence Division of the Admiralty.


== Marriage and issue ==

On 12 October 1934, in anticipation of his forthcoming marriage to his second cousin, Princess Marina of Greece and Denmark, he was created Duke of Kent, Earl of St Andrews, and Baron Downpatrick. The couple married on 29 November 1934 at Westminster Abbey. They had three children:

Prince Edward, Duke of Kent (9 October 1935): he married Katharine Lucy Mary Worsley on 8 June 1961. They have three children.
Princess Alexandra, The Hon. Lady Ogilvy (25 December 1936): she married Rt. Hon. Sir Angus James Robert Bruce Ogilvy, son of David Lyulph Gore Wolseley Ogilvy, 12th Earl of Airlie and Lady Alexandra Marie Bridget Coke, on 24 April 1963. They had two children.
Prince Michael of Kent (4 July 1942): he married Marie Christine von Reibnitz on 30 June 1978. They have two children.


== Personal life ==

There were "strong rumours" that he had affairs with musical star Jessie Matthews, writer Cecil Roberts and Noël Coward, a relationship which Coward's long-term boyfriend, Graham Payn, denied.The Duke of Kent is rumoured to have been addicted to drugs, especially morphine and cocaine, a rumour that reputedly originated with his friendship with Kiki Preston, whom he first met in the mid-1920s. Reportedly, Prince George shared Kiki in a ménage à trois with Argentine Jorge Ferrara. Other alleged sexual liaisons include José Uriburu, son of the Argentine ambassador to the Court of St James's, José Uriburu Tezanos.In his attempt to rescue his cocaine-addicted brother from the influence of Kiki, Edward, Prince of Wales, attempted for a while to persuade both George and Kiki to break off their contact, to no avail. Eventually, Edward forced George to stop seeing Kiki and also forced Kiki to leave England, while she was visiting George there in the summer of 1929. For years afterwards, Edward feared that George might relapse to drugs if he maintained his contact with Kiki. Indeed, in 1932, Prince George ran into Kiki unexpectedly at Cannes and had to be removed almost by force.It has been alleged for years that American publishing executive Michael Temple Canfield (1926–1969) was the illegitimate son of the Duke of Kent and Kiki Preston. According to various sources, both Kent's brother, the Duke of Windsor, and Laura, Duchess of Marlborough, Canfield's second wife, shared this belief. Canfield was the adopted son of Cass Canfield, American publisher of Harper and Row. In 1953, Canfield married Caroline Lee Bouvier the younger sister of Jacqueline Bouvier who married U.S. Senator and future U.S. president John F. Kennedy the same year. Canfield and Bouvier divorced in 1958, and the marriage was annulled by the Roman Catholic Church in November 1962.


== RAF career ==

As a young man the Duke came to the opinion that the future lay in aviation. It became his passion, and in 1929, the Duke earned his pilot's licence. He was the first of the royal family to cross the Atlantic Ocean by air. Before his flying days, he entered the Royal Navy, and was trained in intelligence work while stationed at Rosyth.In March 1937, he was granted a commission in the Royal Air Force as a group captain. He was also made the Honorary Air Commodore of No. 500 (County of Kent) Squadron Auxiliary Air Force in August 1938. He was promoted to air vice-marshal in June 1939, along with promotions to flag and general officer rank in the other two services.In 1939 he returned to active service as a rear admiral in the Royal Navy, but in April 1940, transferred to the Royal Air Force. He temporarily relinquished his rank as an air officer to assume the post of staff officer at RAF Training Command in the rank of group captain, so that he would not be senior to more experienced officers. On 28 July 1941, he assumed the rank of air commodore in the Welfare Section of the RAF Inspector General's Staff. In this role, he went on official visits to RAF bases to help boost wartime morale.


== Freemasonry ==
Prince George was initiated into freemasonry on 12 April 1928 in Navy Lodge No 2612.  He subsequently served as master of Navy Lodge in 1931, and was also a member of Prince of Wales's Lodge No 259, and Royal Alpha Lodge No 16, of which he served as master in 1940.  He was appointed senior grand warden of the United Grand Lodge of England in 1933, and served as provincial grand master of Wiltshire from 1934, until he was elected grand master of the United Grand Lodge of England in 1939; a position he held until his death in 1942.


== Death ==

On 25 August 1942, George and 14 others took off in a RAF Short Sunderland flying boat W4026 from Invergordon, Ross and Cromarty, to fly to Iceland on non-operational duties. The aircraft crashed on Eagle's Rock, a hillside near Dunbeath, Caithness, Scotland. All but one were killed, including George, who was 39 years old.Lynn Picknett and Clive Prince have written about the crash in their book Double Standards, which however has been criticised for its "implausible inaccuracy". 
They alleged that Kent had a briefcase full of 100-krona notes, worthless in Iceland, handcuffed to his wrist, leading to speculation the flight was a military mission to Sweden, the only place where krona notes were of value.His death in RAF service marked the first time in more than 450 years that a member of the royal family died on active service. The prince's body was transferred initially to St. George's Chapel, Windsor, and he was buried in the Royal Burial Ground, Frogmore, directly behind Queen Victoria's mausoleum. His elder son, six-year-old Prince Edward, succeeded him as Duke of Kent. Princess Marina, his wife, had given birth to their third child, Prince Michael, only seven weeks before Prince George's death.
One RAF crew member survived the crash: Flight Sergeant Andrew Jack, the Sunderland's rear gunner. Flight Sergeant Jack's niece has claimed that Jack told his brother that the Duke had been at the controls of the plane; that Jack had dragged him from the pilot's seat after the crash; and that there was an additional person on board the plane whose identity has never been revealed.


== In popular culture ==
The Duke's early life is dramatised in Stephen Poliakoff's television serial The Lost Prince (2003), a biography of the life of the Duke's younger brother John. In the film, the teenage Prince 'Georgie' is portrayed as sensitive, intelligent, artistic and almost uniquely sympathetic to his brother's plight. He is shown as detesting his time at the Royal Naval College and as having a difficult relationship with his austere father.
Much of George's later life was outlined in the documentary film The Queen's Lost Uncle. He is a recurring character in the revival of Upstairs, Downstairs (2010/2012), played by Blake Ritson. He is portrayed as a caring brother, terrified of the mistakes that his family is making; later, he is portrayed as an appeaser of the German regime, but also as a supportive friend of Hallam Holland.George and his eldest brother the Prince of Wales, later King Edward VIII, are shown in Stephen Poliakoff's BBC television serial Dancing on the Edge (2013), in which they are portrayed as supporters of jazz and encouragers of Louis Lester's Jazz Band. A sexual attraction to Louis on George's part is also insinuated.


== Titles, styles, honours and arms ==


=== Titles and styles ===
1902–1910: His Royal Highness Prince George of Wales
1910–1934: His Royal Highness The Prince George
1934–1942: His Royal Highness The Duke of Kent
in Scotland from 1935: His Grace The Lord High CommissionerHe was Patron of the Society for Nautical Research (1926-1942).


=== Honours ===
KG: Knight of the Garter, 1923
KT: Knight of the Thistle, 1935
GCMG: Knight Grand Cross of the Order of St Michael and St George, 1934
GCVO: Knight Grand Cross of the Royal Victorian Order, 1924
Royal Victorian Chain, 1936
Knight of the Order of the Elephant, 20 September 1922
Knight of the Order of the Seraphim, 1 October 1932


=== Appointments ===
1932: Royal Bencher of the Honourable Society of Lincoln's Inn


=== Arms ===
Around the time of his elder brother Prince Henry's twenty-first birthday, Prince George was granted the use of the Royal Arms, differenced by a label argent of three points, each bearing an anchor azure.


== Ancestry ==


== References ==


== Further reading ==
Hunt, Leslie (1972). Twenty-one Squadrons: History of the Royal Auxiliary Air Force, 1925–57. London: Garnstone Press. ISBN 0-85511-110-0.CS1 maint: ref=harv (link)(New edition in 1992 by Crécy Publishing, ISBN 0-947554-26-2.)
Millar, Peter. "The Other Prince". The Sunday Times (26 January 2003).
Warwick, Christopher. George and Marina, Duke and Duchess of Kent. London: Weidenfeld and Nicolson, 1988. ISBN 0-297-79453-1.


== External links ==
Hansard 1803–2005: contributions in Parliament by the Duke of Kent
Portraits of Prince George from the National Portrait Gallery
Newspaper clippings about Prince George, Duke of Kent in the 20th Century Press Archives of the ZBW