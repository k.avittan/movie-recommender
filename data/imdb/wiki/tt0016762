Dancing Mothers is a 1926 American black and white silent drama film produced by Paramount Pictures. The film was directed by Herbert Brenon, and stars Alice Joyce, Conway Tearle, and making her debut appearance for a Paramount Pictures film, Clara Bow. Dancing Mothers was released to the general public on March 1, 1926. The film survives on 16mm film stock and is currently kept at the UCLA Film and Television Archive.


== Synopsis ==
The film tells the story of a pretty mother, who was almost cheated out of life by a heartless husband and a thoughtless daughter.


== Cast ==
Alice Joyce as Ethel "Buddy" Westcourt (played by Mary Young in play)
Norman Trevor as Hugh Westcourt (played by Henry Stephenson in play)
Clara Bow as Catherine "Kittens" Westcourt (played by Helen Hayes in play)
Conway Tearle as Gerald "Jerry" Naughton (played by John Halliday in play)
Eleanor Lawson as Irma (played also by Lawson in play; under the name Elsie Lawson)
Dorothy Cumming as Mrs. Mazzarene
Donald Keith as Kenneth Cobb
Leila Hyams as Birdey Courtney
Spencer Charters as Butter and Egg Man


== Production notes ==
The film was adapted from a successful Broadway stage play by Edgar Selwyn and Edmund Goulding, and Paramount reportedly bought the rights for $45,000. On Broadway the principal parts had been played by Helen Hayes as the daughter, John Halliday as the father, and Mary Young as the mother. Shooting began at Paramount's Astoria Studio in November 1925, after actress Betty Bronson, the star of Peter Pan (1924), was cast for the role of Katherine "Kittens" Westcourt by the studio, but was rejected after director Herbert Brennon reported to studio executives that "when she tried to be sexy, she looked like a little girl who wanted to go to the bathroom."  After production ended in December 1925, Brennon reported to Paramount's top officials that Clara was not only very talented as an actress, but that she took direction very well.


== Reception ==
"A splendid picture containing mother appeal, flapper appeal and well balanced with comedy and a climax that's different, since 'they don't live happy ever after'".
"...it is an effective drama, well acted and Clara Bow is a real little modern."
"It is a picture that strikes home to the adult mind and is a tremendous indictment to every age."


== References ==


== External links ==
Dancing Mothers on IMDb
Synopsis at AllMovie
Lobby poster