Lion hunting is the act of hunting lions. Lions have been hunted since antiquity.


== History ==


=== Ancient Egypt ===
In Ancient Egypt, lion hunts were usually reserved for pharaohs. These hunts nearly resulted in the extermination of lion populations in North Africa by 1100 BC. Commemorative artwork has been found telling of how during a single hunt, pharaoh Amenhotep III killed more than 100 lions.


=== Assyria ===
In ancient Assyria, lion hunting was a ritualized activity reserved for kings. These hunts were symbolic of the ruling monarch's duty to protect and fight for his people. The Lion Hunt of Ashurbanipal, a sequence of Assyrian palace reliefs from the North Palace at Nineveh dating from about 645 BC in the British Museum in London show King Ashurbanipal hunting lions.In fact the "royal lion hunt", was the staged and ritualized killing by the king of lions already captured and released into an arena.  The realism of the lions has always been praised, although the pathos modern viewers tend to feel was perhaps not part of the Assyrian response.The Assyrian kings hunted lions for political and religious purposes, to demonstrate their power.  To get the lion out of his cage and onto the Syrian plains, a servant would raise a door and start running. Then the lion would get beaten by dogs and beaters, so that the lion would go to the king. The king would kill the lion from a chariot with his bow and arrow or spear. Sometimes the king would kill it on foot with a sword. He would do this by holding it by the mane and then thrusting the sword into the lion's throat.   Then, when the lion was killed, the king would pour a Liquid over it and give a speech to the cities(?) god to thank him, so the lion's evil spirit wouldn't come back and haunt him.  Expert spearmen and archers would protect the king. Sometimes the king would invite some nobles to accompany him.


=== Caucasus ===

Before the end of the 10th century, Asiatic lions were hunted by 'Shirvanshakhs' in South Caucasia.


=== Greece ===

Lions were present in the Greek peninsula until classical times; the prestige of lion hunting was shown in Heracles' first labour, the killing of the Nemean lion, and lions were depicted as prominent symbols of royalty, as for example in the Lion Gate to the citadel of Mycenae.


=== Republic of South Africa ===
The first South African legislation on the killing of predators was established in 1656. Six realen were awarded to those who shot or captured lions. In the 1890s, over 4000 lions were killed both inside and outside Kruger National Park in an effort to boost game populations. Between 1903 and 1927, no fewer than 1,272 lions were killed by park personnel.


== Maasai lion hunting ==
The Maasai people have traditionally viewed the killing of lions as a rite of passage. Historically, lion hunts were done by individuals, however, due to reduced lion populations, lion hunts done solo are discouraged by elders. Most hunts are now partaken by groups of 10 warriors. Group hunting, known in Maasai as olamayio, gives the lion population a chance to grow. However, in Kenya, where this practice is illegal, olamayio may be used as a reason for retaliatory killing against lions suspected of killing livestock.  Maasai customary laws prohibit killing a sick or infirm lion. The killing of lionesses is also prohibited unless provoked.
At the end of each age-set, usually after a decade, the warriors count all of their lion kills to compare them with those hunted by the former age-set in order to measure accomplishment.


=== Group hunting ===

Empikas (warrior delegation) plan a lion hunt in advance in secret. Only the warriors are permitted to know about the day of the hunt. The secret is considered so important that Ilbarnot (young warriors) from the same age-set are denied information regarding the hunt, due to the older warriors fearing discovery from anti-hunt groups. If a warrior is found guilty of spreading rumours, he is punished through beating. In addition, the guilty warrior will be looked down upon throughout his entire age group's cycle.


=== Solo hunting ===
Solo lion hunting requires confidence and advanced hunting skills, requiring a dedicated warrior. Unlike group hunting, solo lion hunting is usually not an organised event, sometimes occurring when a warrior is out herding cattle.


=== The journey ===
The lion hunt starts at dawn, when elders and women are still asleep. The warriors meet discreetly at a nearby landmark where they depart to predetermined areas. Before departing, the Ilmorijo (older warriors) filter out the group in order that only the bravest and strongest warriors take part. The resulting group is known as Ilmeluaya (fearless warriors). The rejected young warriors are commanded by older warriors to keep the information of the hunt confidential, until the return of their favoured colleagues. There have been cases whereby older warriors have forced warriors to give up their excess weaponry, seeing as it is considered insulting to bring more than a spear which is sufficient to kill a lion.After a successful hunt, a one-week celebration takes place throughout the community. The warrior who struck the first blow is courted by the women and receives an Imporro, a doubled-sided beaded shoulder strap. The warrior wears this ornament during ceremonies. The community will honor Olmurani lolowuaru (the hunter) with much respect throughout his lifetime.


=== Body parts ===
The Maasai do not eat game meat, and use the bodies of their killed lions for three products; the mane, tail and claws. The mane is beaded by women of the community, and given back to the hunter, who wears it over his head on special occasions. 
After the meat ceremony, when a warrior becomes a junior elder, the mane is thrown away and greased with a mixture of sheep oil and ochre. This sacrificial event is done to avoid evil spirits.The lion's tail is stretched and softened by the warriors, then handed over to the women for beading. The warriors keep the tail in their manyatta (warriors camp), until the end of warriorhood. The lion tail is considered the most valuable product and after graduation, the warriors must gather to pay their last special respect to the tail before it is disposed of.


== Hunting methods ==


=== Spot & stalk ===
This method is typically employed by sport hunters with the assistance of a professional hunter and at least one native tracker.
A walk-and-stalk is very action packed when the encounter occurs. Lions are unpredictable, nimble and fast, meaning that if it was not killed in the first shot, it can be very vindictive. If wounded, it can lie in wait and potentially charge at the hunter, targeting just one person in the hunting party with the intent to kill.
Lions largely prefer to inhabit wooded savannah grasslands, meaning that hunting them during the summer is a very difficult task, as the mane camouflages well with the underlying undergrowth. Most people use a .375 caliber rifle to kill instantly.


=== Hounding ===
Throughout history all manner of pariahs, hounds, terriers and mongrels have been used by man in the pursuit of lion in Africa and (in ancient times) the Middle East. Most notably among these lion hunting dogs is the Rhodesian Ridgeback. Hounding for lion (and subsequent use of the Rhodesian Ridgeback for this purpose) saw its greatest popularity and participation levels during the "Great White Hunter" period, essentially the second half of the 19th century and earliest decades of the 20th century. As hounding for lion has been on the decline since that time, hounding for leopard has seen an inversely proportional increase in popularity. This transition may be due at least in part to the increasing popularity of American and Continental scent hounds among African houndsmen and P.H.'s, as leopard are much less likely to make their stand on solid ground (as lion tend to do), and are much more likely to bay to tree. Although, commensurate with their increasing popularity, several packs of such scenthounds are in regular use today by P.H.s and guides as lion hunters throughout southern Africa.
This method of lion hunting involves the hunter releasing a pack of dogs bred for this purpose in an area known for high levels of lion activity or over fresh spoor/tracks. The pack will track the lion and then hold the lion at bay until the hunter can close the distance and kill the lion at close range.


=== Baiting ===
This common method involves the hunter lying in ambush from a constructed blind about 30-50 yards off from a bait, usually an ungulate carcass, after hanging or fastening the carcass to a tree in a likely area. The bait is then checked every day until there is evidence of a hungry lion present in the area. A large spoor or long black-tipped hairs on the bait signal the building of a blind. The blind is built on the ground or in a tree nearby where the hunter will lie in wait usually in mid-afternoon or early mornings.


== Controversy ==

Lion hunting is a subject of controversy in modern times. Currently, the lion is listed as a vulnerable species by the IUCN, and some subspecies are listed as endangered. Fewer than 20,000 survive in the wild, a reduction of 60% in the last two decades. There were estimated to be 1.2 million lions in 1880.This decline is mainly due to poaching of them and their prey, further influenced by excessive legal trophy hunting and habitat destruction. In addition to the direct population loss from trophy hunting, opponents argue that  trophy hunting of lions primarily kills large males, leading to a smaller and potentially less healthy lion population. Also, when a dominant male is killed it often leads to other deaths when male lions fight for the slain lion's pride and then kill its cubs to eliminate genetic competition. Opponents note that tourism to look at live animals contributes much more to the local economy than trophy hunting of lions does. It is estimated that only 3% of revenue generated by lion hunting actually goes to the communities affected by it.Proponents of trophy hunting argue that it is a tool that can be used to raise money for local communities and conservation organizations, and that a limited amount of trophy off-take will not harm animals at a population level. It is estimated that trophy hunting generates at least $201 million USD per year in the 23 sub-Saharan African countries that allow it. The mean trophy fee for hunting one lion in Namibia is approximately 22,000 USD, and hunters also spend money on services such as safari packages, lodging, and tour guides.In the summer of 2015, the killing of Cecil, a popular lion in Zimbabwe, by an American tourist created a significant international backlash against the hunter and of the practice of hunting lions.
The majority of trophy body parts from lion hunting go to the United States.


== See also ==
Cecil (lion)
Tiger hunting
Elephant hunting in Kenya


== References ==