A lighthouse keeper or lightkeeper is a person responsible for tending and caring for a lighthouse, particularly the light and lens in the days when oil lamps and clockwork mechanisms were used. Lighthouse keepers were sometimes referred to as "wickies" because of their job trimming the wicks.


== Duties and functions ==
Historically, lighthouse keepers were needed to trim the wicks, replenish fuel, wind clockworks and perform maintenance tasks such as cleaning lenses and windows. They were also responsible for the fog signal, the weather station and played a major role in search and rescue at sea.Because most lighthouses are located in remote, isolated or inaccessible areas on islands and coastlines, it was typical for the work of lighthouse keeper to remain within a family, passing from parents to child, all of whom lived in or near the lighthouse itself. "Stag light" was an unofficial term given to some isolated lighthouses in the United States Lighthouse Service. It meant stations that were operated solely by men, rather than accommodating keepers and their families.Electrification and other technological advancements such as remote monitoring and automatic bulb changing began to appear in the 1960s and over the course of the late 20th century made paid resident keepers at the lights unnecessary in certain areas, while simply altering their responsibilities elsewhere. Those who continue to work as lighthouse keepers today perform building maintenance, repair work to broken and blind buoys, geographic realignment of wayward navigational aids off the coast, and technical maintenance on automated systems. In most countries, the training of lighthouse keepers falls within the jurisdiction of the Navy or Coast guard.  In the US, periodic maintenance of the lights is now performed by visiting Coast Guard Aids to Navigation teams.


== History ==
The earliest record of a named individual in a formal capacity as a lighthouse keeper was William, a member of the now famous Knott family, who was appointed to the South Foreland lighthouse near Dover, England in 1730.George Worthylake served as the first lighthouse keeper in the United States.  He served at Boston Harbor Lighthouse from 1716 until his death in 1718.  In 1776, Hannah Thomas became the first female lighthouse keeper in the United States when she became keeper of Plymouth (Gurnet) Lighthouse in Massachusetts following the death of her husband, John Thomas. Both Hannah and her husband received $200 per year for their service.


== Current status ==


=== North America ===
According to Fisheries and Oceans Canada, there are 51 staffed lighthouses left in Canada, as of October 2017: one in New Brunswick, 23 in Newfoundland and Labrador, and 27 in British Columbia. All of these lighthouses are staffed for operational reasons, except for the light on Machias Seal Island, in New Brunswick. This lighthouse, manned by the Canadian Coast Guard, is kept occupied for sovereignty purposes due to the disputed status of the island with the US.The last civilian keeper in the United States, Frank Schubert, died in 2003.  The last officially manned lighthouse, Boston Light, was manned by the Coast Guard until 1998.  It now has volunteer Coast Guard Auxiliary "keepers" whose primary role is to serve as interpretive tour guides for visitors.The lighthouses of Mexico are managed by the General Directorate for Ports and Merchant Marine, a government agency within the larger Secretariat of Communications and Transportation. Automation is not as common in Mexico as in other countries and many of the larger lights are therefore still staffed by resident civilian keepers.


=== South America ===
The first lighthouse in Brazil is recorded to have been lit in 1698 in Santo Antonio, Bahia province, and they maintain a rich lighthouse keeping tradition. Of the 200 active lighthouses still maintained by the Navigational Aid Center, 33 are staffed, as of March 2020.As of 2013, the National Geospatial-Intelligence Agency (NGA) listed 650 lighthouses in Chile and approximately 20 of those were inhabited as of 2003. The southernmost lighthouse in the world, located on the northeast coast of Isla Gonzalo, in the Diego Ramirez Islands, remains a continuously staffed lighthouse and meteorological station administered by the Chilean Navy. In October 2017, the lighthouse keeper Marcelo Escobar had to be airlifted to Punta Arenas after he suffered a broken arm.


=== Europe ===
Sweden began systematically automating their lighthouses as early as the 1930s. Their transformation was so efficient that of the over 100 inhabited lighthouses operating at the start of the 20th century, only nine remained by the year 1980, and only three remained staffed into the 21st century: Kullen Lighthouse, the oldest lighthouse in Scandinavia, was automated in 2003, having been only inhabited periodically since 1996, and then with the retirement of Lighthouse Master Per-Erik Broström on February 25, 2003 Holmogadd lighthouse on the southern end of the Holmoarna islands, became Sweden's final inhabited lighthouse. Holmogadd Light was decommissioned five years later.The last manned lighthouse in Finland was deserted in 1987, and the last Norwegian lighthouse keeper moved out of Runde Lighthouse in 2002.Most French lighthouses are automated, though approximately a dozen were still staffed as of 2011. The French Department of Lighthouses and Beacons recommends that at least two isolated lighthouses at sea remain staffed regularly, if not constantly, for the benefit of training personnel in the work of maritime signal maintenance and safety. This highlights the ongoing role that lighthouse-keeping can play in modern society.As of 2011, there were also 62 staffed lighthouses in Italy and two staffed lights in the Netherlands - one at Schiermonnikoog and the other on the island of Terschelling. The last lighthouse keeper in Iceland was terminated in 2010.


==== The United Kingdom and the British Isles ====
Automation of lighthouses in the British Isles began in the late 1960s, but the majority of lighthouses remained staffed by resident keepers until the 1980s and 1990s. On the Isle of Man, the final lighthouses to become automated and therefore lose their keepers were the Calf of Man and Langness lighthouses, which were converted in 1995 and 1996 respectively. The Langness Lighthouse was purchased in 2008 by British television personality Jeremy Clarkson, and his ex-wife currently operates the property as tourist accommodations.The Hook Head Lighthouse, at the eastern entrance to Ireland's Waterford Harbour, is the second oldest operating lighthouse in the world. It was manned continuously from at least 1207 until 1996, when it was finally automated. From the time of its construction in the 13th century, until the mid-17th century, the lighthouse was even home to an early Christian monastery.Following the automation of Hook Head Lighthouse, Baily Lighthouse became the last Irish lighthouse to have a resident keeper, but it too was automated in 1997, and the lighthouse keeper was no longer needed.A couple of months later, on March 31, 1998, the keepers left the Fair Isle South Lighthouse in Shetland, and with that the final Scottish lighthouse to be staffed became automated.The last manned lighthouse in England, and the United Kingdom, was the North Foreland Lighthouse in Kent. The last six keepers Dave Appleby, Colin Bale, Dermot Cronin, Tony Homewood, Barry Simmons and Tristan Sturley completed their service in a ceremony attended by the Prince Philip, on 26 November 1998. In an interview with the BBC, Dermot Cronin remarked, "I had no idea I would be closing the door of the last manned lighthouse in the British Isles."


=== Asia ===
There are five lighthouses along the coasts of the Malay Peninsula, all managed by the Maritime and Port Authority of Singapore. Of those five, two were still regularly staffed by lighthouse keepers as of the end of 2015. Raffles Lighthouse, on Singapore's southernmost island, and Pulau Pisang Lighthouse, which is technically located within the neighbouring country of Malaysia, are both crewed by a rotating staff of eight lighthouse keepers who work 10-day shifts in pairs.In 2006 Meshima Lighthouse became the last lighthouse in Japan to become automated.


=== Oceania ===
The last staffed lighthouse in Australia was the Sugarloaf Point Light, otherwise known as Seal Rocks Lighthouse. Although the lighthouse was electrified in 1966, and automated in 1987, a caretaker and lighthouse keeper remained on site until 2007, when the lighthouse keeper's cottages were renovated into tourist accommodations.All lighthouses in New Zealand have been automated since 1990.


== Recognition ==


=== In popular culture ===
The character of the lighthouse keeper has been popular throughout history for their associated air of adventure, mystery, isolation and their rugged lifestyle. The heroic role that lighthouse keepers can sometimes play when shipwrecks occur also feeds into their popularity.
The following books, TV shows and films draw heavily upon the life of the lighthouse keeper:

Hello Lighthouse (2018 book)
Ladies of the Lights (2010 book)
The Lighthouse at the End of the World (Jules Verne novel, 1905)
The Light at the Edge of the World (film adaptation of the above, 1971)
The Lighthouse Keepers (1929 film)
The Lighthouse Keepers (Three novels, 2006-2008)
The Lighthouse-Keeper's Daughter a.k.a. Manina, the Girl in the Bikini (1952 film)
The Light Between Oceans (2012 novel) / The Light Between Oceans (2016 film)
Lighthouse Keeping Loonies (1975 TV series episode)
A Plague of Lighthouse Keepers (1971 song)
Cape Forlorn (1931 film) / Menschen im Käfig (1930 film) / Le cap perdu (1931 film)
Times of Joy and Sorrow (1957 film)
The Lighthouse (2019 film)
Dr Who - The Horror of Fang Rock (1977)I Want To Marry A Lighthouse Keeper is a song written by musician Erika Eigen and performed by her band Sunforest on the soundtrack to the 1971 film A Clockwork Orange.


=== In Marine Safety and National Coast Guards ===
To recognize the role of lighthouse keepers in the nation's maritime safety, the US Coast Guard named a class of 175-foot (53 m) USCG Coastal Buoy Tenders after famous US Lighthouse Keepers.  Fourteen ships in the "Keeper" class were built between 1996 and 2000 and are used to maintain aids to navigation, including lighthouses:
USCGC Ida Lewis (WLM-551); Newport, Rhode Island
USCGC Katherine Walker (WLM-552); Bayonne, New Jersey
USCGC Abbie Burgess (WLM-553); Rockland, Maine
USCGC Marcus Hanna (WLM-554); South Portland, Maine
USCGC James Rankin (WLM-555); Baltimore, Maryland
USCGC Joshua Appleby (WLM-556); St. Petersburg, Florida
USCGC Frank Drew (WLM-557); Portsmouth, Virginia
USCGC Anthony Petit (WLM-558);Ketchikan, Alaska
USCGC Barbara Mabrity (WLM-559); Mobile, Alabama
USCGC William Tate (WLM-560); Philadelphia, Pennsylvania
USCGC Harry Claiborne (WLM-561); Galveston, Texas
USCGC Maria Bray (WLM-562); Atlantic Beach, Florida
USCGC Henry Blake (WLM-563); Everett, Washington
USCGC George Cobb (WLM-564); San Pedro, California


== See also ==
Grace Darling
Marcus Hanna
Ida Lewis


== References ==


== Further reading ==


== External links ==
Association of Lighthouse Keepers
Bibliography on lighthouse keepers.
Bibliography on Michigan and other lighthouses.
Ode to the lighthouse keeper by Andrew Tremaine.
Life as a Lighthouse Keeper - United States Lighthouses
Scott T. Price. "U. S. Coast Guard Aids to Navigation: A Historical Bibliography". United States Coast Guard Historian's Office.
Wagner, John L. "Beacons Shining in the Night: The Lighthouses of Michigan". Clarke Historical Library, Central Michigan University.
Pepper, Terry. "Seeing the Light: Lighthouses on the western Great Lakes". Archived from the original on 2008-01-30.
"Lighthouses in the United Kingdom | US Lighthouse Society". uslhs.org.