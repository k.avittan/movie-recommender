Tyrant Fear is a 1918 American silent drama film directed by Roy William Neill, written by R. Cecil Smith, and starring Dorothy Dalton, Thurston Hall, Melbourne MacDowell, William Conklin, Lou Salter, and Carmen Phillips. It was released on April 29, 1918, by Paramount Pictures. A print of the film is held by the Library of Congress.


== Plot ==
As described in a film magazine, Allaine Grandet (Dalton) is sold to Jules Latour (Conklin) by her father Paula Grandet. As a climax to his constant brutality, Latour turns her over to James Dermot (MacDowell), who offers to pay the husband well for his wife's appearance at the Northern Star dance hall. At the dance hall she is put up as the stake in a card game between Dermot and a miner, with the dance hall proprietor being the looser. She resists her new owner and is assisted by the piano player, Harley Dane (Hall), who is shot down by Dermot. She, in turn, shoots Dermot and she escapes with the piano player, whom she nurses back to health. He wants to marry her, but she appraises him of her husband. Later, they find Latour's body in the snow, which simplifies matters for the couple.


== Cast ==
Dorothy Dalton as Allaine Grandet
Thurston Hall as Harley Dane
Melbourne MacDowell as James Dermot
William Conklin as Jules Latour
Lou Salter as Theodore De Coppee
Carmen Phillips as Marie Courtot


== Preservation status ==
A fragment, or reel 1, exists of this film in the Library of Congress collection.


== Reception ==
Like many American films of the time, Tyrant Fear was subject to cuts and restrictions by city and state film censorship boards. For example, the Chicago Board of Censors cut, in Reel 1, the intertitles "At Dermot's Settlement are girls to warm a man's heart" and "That is the life — drink, cards and women of fire", two scenes of bridegroom passionately embracing Marie after the dance, Reel 2, the intertitles "Wife, hell, you're not a wife for a real man — you're as cold as the snow outside" and "A magnet which draws men's souls from their bodies where unbridled indulgence holds sway", scene in which Dermot sends woman over to Allaine to include the twisting of her arm, Reel 3, flash one gambling scene, girl on bar falling back into man's arms, girls at bar, girl throwing arms around man's neck, three intertitles "One whirl for the girl" etc., "She is your woman, Dermot", and "By God, I'm through — I'll play no more for your dirty swine", entire incident of gambling for girl to include man throwing bag of money on gambling table, two closeups of wheel, and Dermot pushing girl towards man, four scenes of man dragging girl except for scene in which she breaks away and runs to man at the piano, all scenes of man dragging girl upstairs, three struggle scenes with man on balcony and girl throwing him over balustrade, and, Reel 5, the three intertitles containing the words "The lustful lights of the North Star", "What will you do if I consent", and "Damn you, you're made of ice".


== References ==


== External links ==
Tyrant Fear on IMDb
synopsis at AllMovie