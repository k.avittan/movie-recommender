The Winding Stair Mountains is a mountain ridge located within the state of Oklahoma in Le Flore County, north of Talihina.


== Description ==
The ridge is part of a larger mountain range, the Ouachita Mountains, which is itself a subsection of the U.S. Interior Highlands. Some of the peaks within this ridge have elevations reaching about 2,400 feet (730 m) above sea level.
Tree species native to this area include the following: shortleaf pine, loblolly pine, southern red oak, white oak and flowering dogwood.


== History ==
In prehistoric times (8000 B.C. - 1 A.D.), hunter-gatherer tribes roamed the area. By the time the first Europeans visited the region, it had been populated by the Wichita people. When the United States Government expelled the Choctaw people from their previous homeland in the Southeastern States after 1820, the land became part of the Choctaw Nation in Indian Territory.  During territorial times the mountains served as the boundary between Sugar Loaf County and Wade County in the Choctaw Nation.  The Choctaw government was dissolved in 1906, just before the present state of Oklahoma was formed on November 16, 1907. In 1971, it gained Self-Determination status. Those Choctaw who removed to the Indian Territory in the early 20th century during the Trail of Tears are federally recognized as the Choctaw Nation of Oklahoma.


== Tourism ==
The mountain range is a popular tourist destination, due to the designated protected areas in the vicinity.


=== Federally protected areas ===
The mountain range is almost entirely located in the Ouachita National Forest. The forest comprises the Winding Stair Mountain National Recreation Area, designated by President Ronald Reagan in 1988. The recreation area covers approximately 26,445 acres.


=== Scenic Byway ===

The Talimena National Scenic Byway crosses the mountains from Talihina, Oklahoma to Mena, Arkansas. It was designated in 2005 as a National Scenic Byway. The byway, which travels through the Ouachita National Forest, is often described as one of the prettiest drives in the country, situated along some of the highest peaks in the Winding Stair Mountains. The byway is a popular destination for many tourists in the area.


== See also ==
Kiamichi Country
Kiamichi River
Ouachita Mountains


== References ==