Sweet Kitty Bellairs is a 1930 American historical musical comedy film directed by Alfred E. Green. The film is based on the 1900 novel, The Bath Comedy by Agnes Castle and Egerton Castle. Shot entirely in Technicolor, the film stars Claudia Dell, Ernest Torrence and, Walter Pidgeon and is set in Bath, England in 1793.
The novel was first adapted for the stage by David Belasco in 1903 and starred Henrietta Crosman. A silent film adaptation starring Mae Murray followed in 1916.


== Plot ==
Kitty Bellairs (Claudia Dell), a famous flirt of her day, comes to Bath for the season. Early on in the film she declares that "in spite of her thirty or forty affairs, I've lost not a bit of my virtue." Her path is strewn with a number of conquests, including an enamored highwayman, a lord and some others who hang on her every word. A highwayman stops her coach as she is on her way to Bath and is immediately raptured by Kitty Bellairs. He trades the loot from the passengers for a kiss from Kitty who feels she should "yield" in order to save the life of Lord Varney (Walter Pidgeon), who has gallantly come to defend her honor.
In spite of this, Lord Varney draws his sword and ends up losing the fight when he loses his sword, upon which the highwayman declares, "Blood is not a pretty sight for tender eyes, Retrieve your sword while I go about my business." He proceeds to kiss Kitty who declares she considers herself not to have been kissed at all, upon which the highwayman kisses her several times and slips a ring on her finger leaving her enraptured. Lord Varney, however, is in love with Kitty himself but is extremely bashful and shy. The film then progresses to the city of Bath, where the inhabitants sing an amusing song about their daily lives, and the proceeds to a dance which Kitty is attending. She meets Captain O'Hara (Perry Askam) who declares his love for her. When Lord Varney approaches and asks for his dance from Kitty, Captain O'Hara declares that "it 'was' his dance" and whisks her away. Lord Varney is approached by his friend who laughs at his shyness.
Nevertheless, Lord Varney declares his love for her and decides to write a love poem to Kitty. The film then proceeds to the next day and we see Kitty being tended to by her maid while chatting with her hairdresser about her three lovers. She describes them and asks his opinion on whom she should choose. The film then proceeds to the house of Lady Julia Standish (June Collyer) on whom Kitty is paying a call. Lady Julia's husband is neglecting her and Kitty gives her advice on how to make her husband interested once again. Her husband, Sir Jasper Standish (Ernest Torrence) arrives from a trip to find her dressed elegantly as if expecting a caller. Meanwhile, Kitty places a love note addressed to her in a conspicuous place with a lock of red hair and leaves the house. Through a welter of songs into which the principals break at short intervals she at length decides on a lord instead of a highwayman.
Lord Varney, hearing that Kitty was visiting Lady Standish, comes to call on Kitty at Lord Standish's house. Lord Standish immediately assumes that he is fooling around with his wife and insults him so that he must fight a duel "according to the code" in order to uphold his honor. The report of the scandal soon flies through the town and we are taken to a bath where everyone is talking about the supposed affair. Kitty happens to be there and as soon as she hears the story she begins to fear for the life of Lord Varney, whom she now realizes is the one she really loves. Through a welter of songs into which the principals break at short intervals, as well as outrageous Pre-Code comedy, satire and drama,  Kitty and Lord Varney are at length united.


=== Pre-Code sequences ===
The film contains several examples of Pre-Code humor. In one scene, an obviously gay hairdresser is talking to Kitty Bellairs about her love affairs. Kitty asks him which man she should choose and the hairdresser says she should choose the highwayman because he prefers "a manly man."
In another scene, Kitty teaches her friend how to get her husband to pay attention to her. Her instructions include wearing Parisian negligee and finding another lover.


== Cast ==


== Songs ==
"You, I Love But You" (Sung by Claudia Dell)
"I've Been Waiting For You" (Sung by Walter Pidgeon)
"Drunk Song" (Sung by Ernest Torrence, Lionel Belmore, Edgar Norton)
"Duelling Song" (Sung by Ernest Torrence, Perry Askam, Lionel Belmore, Edgar Norton, Douglas Gerrard)
"Peggy's Leg" (Sung by Ernest Torrence, Perry Askam, Lionel Belmore, Edgar Norton, Douglas Gerrard, Arthur Edmund Carewe)
"Highwayman Song" (Sung by Perry Askam and Claudia Dell)
"Pump Room Song" (Sung by Claudia Dell)
"Song of the Town of Bath" (Sung by Extras)
"Tally Ho" (Sung by Claudia Dell, Walter Pidgeon, Lionel Belmore)


== Preservation status ==
The film survives in a black-and-white nitrate copy. No copies of the film are known to exist in the original Technicolor. The color work on the film was highly praised by the film reviewers of the day.


== See also ==
List of early color feature films


== References ==


== External links ==
Sweet Kitty Bellairs on IMDb
Sweet Kitty Bellairs at AllMovie