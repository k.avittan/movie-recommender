Matinee is a 1993 American period comedy film directed by Joe Dante. It is about a William Castle-type independent filmmaker, with the American home front during the Cuban Missile Crisis as a backdrop. The film stars John Goodman, Cathy Moriarty, Simon Fenton, Omri Katz, Lisa Jakub, Robert Picardo, Kellie Martin, and Jesse White (in his final theatrical film role). It was written by Jerico Stone and Charles S. Haas, the latter portraying Mr. Elroy, a schoolteacher.


== Plot ==
In Key West, Florida in October 1962, Gene Loomis and his younger brother, Dennis, live on a military base with their mother while their father is away on a United States Navy submarine. At a local movie theater one afternoon, Gene and Dennis see a promo for an exclusive engagement of producer Lawrence Woolsey's sensational new horror film, entitled Mant! Woolsey is scheduled to make an in-person appearance at the cinema in Key West the following Saturday. After the boys return home to the base, the Loomis family watches as President Kennedy delivers a speech confirming the presence of Soviet missiles in Cuba. Meanwhile, Woolsey arrives in Florida with his actress girlfriend, Ruth Corday. He finds the fearful atmosphere created by the ongoing missile crisis to be the perfect environment in which to host Mant!'s premiere.
Woolsey has brought along two of his actors — Herb Denning, a former hired thug, and Bob, a victim of the Hollywood blacklist forced to find work in cheap, independent "B" movies — to impersonate outraged protestors who publicly object to Mant!'s exhibition. However, Jack and Rhonda, a local couple, make a strong argument for allowing the premiere based on First Amendment rights. Later, Gene reads an issue of Famous Monsters of Filmland and recognizes Denning from his appearance in an earlier Woolsey film.
At school, Gene gradually befriends one of his classmates, Stan, and becomes infatuated with Jack and Rhonda's daughter, Sandra, after she gets a week of detention for protesting against uselessness of a "duck and cover" air raid drill, telling the other students that it would be preferable to die from the immediate effects of an atom bomb instead of acute radiation syndrome caused by fallout. Stan has a crush on another girl, Sherry, whose former boyfriend, Harvey Starkweather, is a violent juvenile delinquent and would-be poet. After Harvey threatens Stan for pursuing Sherry, Stan lies to her to call off their first date.
Woolsey continues to devote himself to the promotion of Mant!, hiring Harvey to dress as the mutated half-man, half-ant creature from the film, and installing buzzers in theater seats as part of a gimmick he calls "Rumble-Rama," although the cinema's manager, Howard, is concerned about the effects "Rumble-Rama" could have on the fragile balcony area, which has a maximum capacity of just 100 people. At the Saturday matinee, Sherry encounters Stan, who is attending the screening with Gene and Dennis; she is upset that he deceived her, but later reconciles with him. Sandra attends the premiere with her parents, but leaves them to watch the movie with Gene. When Harvey (costumed as the Mant! monster) sees Sherry and Stan kissing during the film, he attacks Stan in a rage, then punches Woolsey after Woolsey tries to intervene, and a chase ensues. Stan takes a rifle from a nuclear fallout shelter located inside the movie theater and uses it to frighten Harvey away, but Sandra and Gene are locked inside the shelter after the door is accidentally closed. While trapped, the two comfort each other and eventually share their first kiss.
Woolsey helps rescue Sandra and Gene from the shelter before their oxygen supply runs out; however, Harvey reappears and holds a switchblade to Ruth's throat, demanding money from Woolsey before fleeing with Sherry. Howard calls the police, and Harvey is arrested after crashing his car outside the movie theater as Sherry and Stan reunite. Woolsey also realizes that Harvey has turned the "Rumble-Rama" machinery up so high that the overcrowded balcony in the cinema is starting to collapse. Assisted by Gene, Woolsey projects trompe l'oeil footage of a mushroom cloud that appears to blast a hole through the screen and outside wall of the theater, thus evacuating the panicked audience.
Once the Cuban Missile Crisis has ended, Ruth and Woolsey leave for another premiere in Cleveland, Ohio, bidding goodbye to Sandra and Gene. Woolsey has grown fond of them, telling Ruth he might like to have children of his own some day. The film ends as Sandra and Gene watch helicopters fly over the beaches in Key West, waiting for Gene's father to return.


== Cast ==
John Goodman as Lawrence Woolsey
Cathy Moriarty as Ruth Corday / Carole
Simon Fenton as Gene Loomis
Omri Katz as Stan
Lisa Jakub as Sandra
Kellie Martin as Sherry
Jesse Lee as Dennis Loomis
Lucinda Jenney as Anne Loomis
James Villemaire as Harvey Starkweather
Robert Picardo as Howard, the Theater Manager
Jesse White as Mr. Spector
Dick Miller as Herb Denning
John Sayles as Bob
David Clennon as Jack
Lucy Butler as Rhonda
Belinda Balaski as Stan's Mom
Naomi Watts as Shopping Cart Starlet


== Production ==
Joe Dante says the financing of the movie was difficult.

Matinee got made through a fluke. The company that was paying for us went out of business and didn't have any money. Universal, which was the distributor, had put in a little money, and we went to them and begged them to buy into the whole movie, and to their everlasting sorrow they went ahead and did it. [Laughs.]
Principal photography began on April 13, 1992. Filming took place in and around Florida, including the towns of Cocoa, Maitland, and Key West in the Florida Keys. The interior sequences in the school and the theater were filmed on set at Universal Studios Florida in Orlando. The street scenes were filmed at Oxnard, California. Production was completed on June 19, 1992.


=== Music ===
The original score was composed by Jerry Goldsmith. Several cues from previous films were also used, arranged and conducted by Dick Jacobs, including "Main Title" from Son of Dracula (1943); "Visitors" from It Came from Outer Space (1953); "Main Title" from Tarantula (1955); "Winged Death" from The Deadly Mantis (1957); two cues from This Island Earth (1955), "Main Title" and "Shooting Stars"; and three cues from the Creature from the Black Lagoon trilogy: "Monster Attack" from Creature from the Black Lagoon (1954); "Main Title" from Revenge of the Creature (1955); and "Stalking the Creature" from The Creature Walks Among Us (1956).


=== Casting ===
Joe Dante has cast character actor Dick Miller in each of his movies to date, casting him here as one of the men protesting the monster movie's release, and as a soldier holding a sack of sugar. Also appearing in supporting roles are William Schallert and Robert O. Cornthwaite (who both appeared in scores of low-budget films of all genres); Kevin McCarthy (perhaps best remembered for his role in Invasion of the Body Snatchers) as well as Robert Picardo, both of whom appeared in several of Dante's movies. John Sayles, who collaborated with Dante on earlier movies, appears as one of the men protesting the monster movie's release.


=== Films within the film ===


==== Mant! ====
Woolsey's low-budget Mant! is a parody morphing of several low-budget science-fiction horror films of the 1950s (many in black & white) that fused radioactivity with mad science and mutation: In particular Tarantula (1955), wherein a scientist is injected with an atomic isotope formula with disastrous results, and in general the films Them! (1954); The Beast with a Million Eyes (1955) The Deadly Mantis (1957); The Black Scorpion (1957); The Amazing Colossal Man (1957); Monster That Challenged the World (1957); Beginning of the End (1957); War of the Colossal Beast (1958); The Fly (1958) and The Alligator People (1959). The depiction of Mant!'s use of "Rumble-Rama" is a riff on William Castle's many in-theatre gimmicks ("Emergo", "Percepto", "Illusion-O", "Shock Sections" etc.), however, the only "monster movie" produced or directed by William Castle before 1970 was 1959's The Tingler, which did not use a radiation theme.  "Rumble-Rama" is also a nod to Sensurround, Universal's sound process of the 1970's. Matinee also mentions some of Woolsey's earlier "films": Island of the Flesh Eaters, The Eyes of Doctor Diablo, and The Brain Leeches (not to be confused with the real-world 1977 film of the same name).


==== The Shook-Up Shopping Cart ====
Although Matinee is set in October 1962, its other film within a film, the family-oriented gimmick comedy The Shook-Up Shopping Cart (featuring an anthropomorphic shopping cart), is a reference to some color Disney comedies that came later in the decade: The Love Bug (1969) in particular, and The Ugly Dachshund (1966); Monkeys, Go Home! (1967); Blackbeard's Ghost (1968); The Horse in the Gray Flannel Suit (1968); The Million Dollar Duck (1971); Snowball Express (1972) and The Shaggy D.A. (1976) in general. The film features a then-unknown Naomi Watts.


== Release ==
Matinee was released on January 29, 1993 in 1,143 theatres. It ranked at #6 at the box office, grossing $3,601,015 in its opening weekend. The film went on to gross $9,532,895 in its theatrical run.


== Reception ==
Matinee was well received by film critics and has a 92% approval rating at the film review aggregator website Rotten Tomatoes, based on 36 reviews with an average rating of 7.61/10. The website's critical consensus reads, "Smart, funny, and disarmingly sweet, Matinee is a film that film buffs will love -- and might even convert some non-believers." Roger Ebert gave the film three and half out of four stars and wrote, "There are a lot of big laughs in Matinee, and not many moments when I didn't have a wide smile on my face". In her review for The New York Times, Janet Maslin wrote, "Matinee, which devotes a lot of energy to the minor artifacts of American pop culture circa 1962, is funny and ingenious up to a point. Eventually, it becomes much too cluttered, with an oversupply of minor characters and a labored bomb-and-horror-film parallel that necessitates bringing down the movie house". Entertainment Weekly gave the film a "B+" rating and Owen Gleiberman wrote, "In Matinee, Dante has captured the reason that Cold War trash like Mant struck such a nerve in American youth: The prospect of atomic disaster was so fanciful and abstract that it began to merge in people's imaginations with the very pop culture it had spawned. In effect, it all became one big movie. Matinee is a loving tribute to the schlock that fear created".In his review for the Los Angeles Times, Peter Rainer wrote of Dante's film: "He pulls out his bag of tricks and even puts in an animated doodle; he's reaching not only for the flagrant awfulness of movies like MANT but also for the zippy ardor of the classic Warner Bros. cartoons. He does everything but put a buzzer under your seat". In his review for the Chicago Reader, Jonathan Rosenbaum wrote, "At the same time that Dante has a field day brutally satirizing our desire to scare ourselves and others, he also re-creates early-60s clichés with a relish and a feeling for detail that come very close to love". In her review for The Washington Post, Rita Kempley wrote, "In this funny, philosophical salute to B-movies and the B-moguls who made them, Dante looks back fondly on growing up with the apocalypse always on your mind and atomic mutants lurking under your bed". In his review for the USA Today, Mike Clark wrote, "Part spoof, part nostalgia trip and part primer in exploitation-pic ballyhoo, Matinee is a sweetly resonant little movie-lovers' movie".


== See also ==
Cuban Missile Crisis
List of films featuring fictional films


== References ==


== External links ==
Matinee on IMDb
Matinee at Rotten Tomatoes
Matinee at Box Office Mojo
Joe Dante interview at DVD Savant