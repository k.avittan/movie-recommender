Brewster's Millions is a comedic novel written by George Barr McCutcheon in 1902, originally under the pseudonym of Richard Greaves. It was adapted into a play in 1906, which opened at the New Amsterdam Theatre on Broadway, and the novel or play has been adapted into films thirteen times, four of which were produced in India. The plot concerns a young man whose eccentric grandfather leaves him a fortune in a will, with the qualification that he must spend $1 million in the first year or lose the remainder of his inheritance (approx US$25 million in 2020 dollars). 


== Plot summary ==
The novel revolves around Montgomery Brewster, a young man who inherits one million dollars from his rich grandfather. Shortly after, a rich uncle who hated Brewster's grandfather (a long-held grudge stemming from the grandfather's disapproval of the marriage of Brewster's parents) also dies. The uncle will leave Brewster seven million dollars, but only under the condition that he keeps none of the grandfather's money. Brewster is required to spend every penny of his grandfather's million within one year, resulting in no assets or property held from the wealth at the end of that time. If Brewster meets these terms, he will gain the full seven million; if he fails, he remains penniless.
Brewster finds that spending so much money within the course of a year is incredibly difficult under the strict conditions imposed by his uncle's will.  Brewster is required to demonstrate business sense by obtaining good value for the money he spends, limiting his donations to charity, his losses to gambling, and the value of his tips to waiters and cab drivers. Moreover, Brewster is sworn to secrecy, and cannot tell anyone why he is living to excess. Working against him are his well-meaning friends, who try repeatedly to limit his losses and extravagance even as they share in his luxurious lifestyle.
Brewster's challenge is compounded by the fact that his attempts to lose money through stock speculation and roulette prove to increase his funds rather than decrease them. He throws large parties and balls, and charters a cruise lasting several months to Europe and Egypt for his large circle of friends and employees; the press lampoons him as a spendthrift. Despite his loose purse strings, Brewster repeatedly demonstrates a strong moral character. At one point, he uses his funds to bail out a bank to save his landlady's account, despite risking his eligibility for the will. At another, he jumps overboard to save a drowning sailor from his cruise even as his rich friends choose not to.
Brewster's would-be wife Barbara Drew turns down his marriage proposal early in the year, believing him to be financially irresponsible and bound to a life of poverty, and his attempts to win her back repeatedly fail as his attention is entirely absorbed by the requirement to spend so much money. At the conclusion of the year, he succeeds in spending the last of his funds, which he has meticulously documented, and confesses his love to another woman, Peggy Gray, who has been sympathetic to his lifestyle despite knowing nothing about his challenge. Tragedy strikes the night before the deadline, as his lawyers informed him that the executor of his uncle's will has vanished after liquidating all of the assets.  Brewster convinces himself that he is doomed to poverty, but marries Peggy Gray, who accepts him despite the lack of wealth. Shortly after the wedding, the executor of his uncle's will arrives to inform him that he has successfully met the challenge and that he has come to deliver the money to Brewster in person.


== Film, theatrical, television, and radio adaptations ==


=== Stage adaptation ===

The novel was adapted into a Broadway play of the same name by Winchell Smith and Byron Ongley. The play debuted at the New Amsterdam Theatre on December 31, 1906.
Opening night principal cast:
Edward Abeles – 	Montgomery Brewster
Leslie Bassett –	 	 Fred Gardner
Gaston Bell –	 	 Horace Pettingill
Cecile Breton –	 	 Trixie Clayton
George Clare –	 	 Rawles
Jack Devereaux –	Subway Smith
Sumner Gard –	 	 Archibald Vanderpool
Willard Howe –	 	 Frank Bragdon
Nestor Lennon –  Colonel Drew
Emily Lytton –	 	 Mrs. Dan De Mille
Arthur Morris – 	 Thomas
Olive Murray –	 	 Barbara Drew
Josephine Park –	 Janice Armstrong
George Probert –	 Nopper Harrison
Eugene Redding –	 Monsieur Bargie
Albert Sackett – 	 Mr. Grant
Joseph Woodburn –	 Joseph MacCloudThe play was later adapted into a musical, Zip Goes a Million.


=== Film versions ===
The novel Brewster's Millions has been adapted into many films:


=== Television adaptation ===
In the TV episode "Punky's Millions", from the animated version of Punky Brewster, Punky and her father Henry appear on a TV show trying to win the $40 million prize. In order to claim the grand prize, they must spend one million dollars in 48 hours. To make sure that the money would not be spent all at once on something very expensive (e.g., a villa as suggested by one of Punky's friends), a rule states the money cannot be used to buy any single item for more than $10,000. Also, every single item purchased must be donated. When the deadline passes, Punky and Henry seem to have won, but one of Punky's friends, Allen, forgot to spend the 98 cents he got as change when he bought chocolate, so they lose the game. Fortunately, they had been donating their purchases to a local orphanage, and one of the dollars they spent was on a lottery ticket, which wins $100,000 for the orphanage.


=== Radio adaptation ===
On February 15, 1937, the Lux Radio Theatre presented a version of the play starring Jack Benny. The show was modified for Benny: the title character in this version is named Jack Benjamin Brewster; and the character opens the play's first scene by playing Benny's theme song, "Love in Bloom", on the violin. 
Playing opposite Benny is his real-life wife and the co-star of his long-running radio show, Mary Livingstone. Livingstone plays Brewster's girlfriend, here called Mary Gray. 
The casting of Benny as a character who must spend money was considered humorous in itself, as it contrasted sharply with Benny's well-known radio persona as a miser.


== See also ==

"The Million Pound Bank Note", an 1893 short story with a similar premise, written by Mark Twain.


== References ==


== External links ==

 Brewster's Millions at Project Gutenberg
Brewster's Millions at the Internet Broadway Database
 Brewster's Millions public domain audiobook at LibriVox