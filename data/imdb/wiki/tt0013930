"The Cloud Minders" is the twenty-first episode of the third season of the American science fiction television series Star Trek. Written by Margaret Armen (based on a story by David Gerrold and Oliver Crawford) and directed by Jud Taylor, it was first broadcast on February 28, 1969.
In the episode, Captain Kirk races against time to acquire plague-fighting minerals from a world suffering from a grievous social class disparity.


== Plot ==
The Federation starship Enterprise arrives at the planet Ardana to take on a shipment of zenite, needed elsewhere to halt a botanical plague. Captain Kirk and First Officer Spock beam directly to the zenite mines, where a group of miners, led by a young woman, attempt to take them hostage. High Advisor Plasus arrives with a security force, driving the miners off, and invites Kirk and Spock to return with him to the floating city of Stratos.
Kirk and Spock are entertained as guests until the zenite can be recovered. They learn Ardanan society is divided between the Troglytes, who perform all physical labor, and city-dwellers who live in luxury. A group of Troglytes known as the Disruptors are rebelling against the elites.
While Kirk is resting, Vanna, leader of the Disrupters, attempts to take him hostage. Kirk overpowers her, recognizes her as the woman he saw at the mine and questions her. She accuses the Stratos city-dwellers of using the Enterprise to intimidate the Troglytes. Spock and a Stratos security guard arrive, and Vanna is subdued. She is then taken for interrogation under the "rays", which induce intense pain as a form of torture. Kirk is outraged by Plasus's actions and demands the interrogation be stopped. Plasus instead orders Kirk and Spock to leave Stratos immediately and forbids them to return.
The two return to the Enterprise where Chief Medical Officer Dr. McCoy reports that unprocessed zenite emits an odorless, invisible gas which diminishes mental capacity and heightens emotions. Spock believes that Troglytes serving aboard the floating city have been spared the effects of the gas, enabling them to organize the rebellion.
Kirk contacts Plasus and offers special masks that filter the zenite gas. Plasus, however, regards Kirk's proposal as unwarranted interference in his government's affairs. Kirk then beams down to Vanna's holding cell, informs her of the effects of zenite gas, and makes his offer to her. Vanna is skeptical at first but appears to trust him, and she and Kirk escape and return to the zenite mines. Once there, however, she orders Kirk seized by two other Disruptors.
Vanna's friends depart, and she puts Kirk to work digging for zenite. After a short while Kirk attacks her, regains his phaser, and fires at the ceiling of the mine, causing a cave-in that traps them. He then contacts the Enterprise and orders Plasus to be beamed to his location. An indignant Plasus is beamed in, and Kirk forces him and Vanna to dig for zenite. The effects of the zenite gas become apparent as Kirk and Plasus become increasingly irritable. A brawl between them breaks out, and Vanna, finally believing Kirk's story of the gas, signals the Enterprise for help.
Back in the city, the zenite consignment is prepared for transport. Plasus has grudgingly allowed the distribution of the filter masks, and Vanna declares that her people's struggle for equality will continue.


== Production and reception ==
Zack Handlen of The A.V. Club gave the episode a 'C+' rating, noting that it "had enough story that it didn't drag too terribly near the end" but that "philosophy-wise, it chickened out in a way that's highly unusual for the show".


== Influence ==
Manny Coto, showrunner of Star Trek: Enterprise, stated in an interview for StarTrek.com that he had wanted to feature the cloud city of Stratos in the fourth season of that series, saying, "There was one I really wanted to do set on Stratos, the Cloud City [from "The Cloud Minders"]. I really wanted to do a two-parter on that location, to see Stratos in its earlier stages."


== References ==


== External links ==
"The Cloud Minders" at StarTrek.com
"The Cloud Minders" on IMDb
"The Cloud Minders" at Memory Alpha (a Star Trek wiki)
"The Cloud Minders" at TV.com
"The Cloud Minders" Review of the remastered version at TrekMovie.com