The Grand Passion is a 1918 American silent western film directed by Ida May Park and starring Dorothy Phillips, Jack Mulhall, and Lon Chaney. It is not known whether the film currently survives.


== Plot ==
As described in a film magazine, Dick Evans (Stowell), boss of Powderville, decides to start a newspaper and support it through coerced advertising from the businesses in the town. He hires Jack Ripley (Mulhall), a New York newspaperman, to be its editor. Viola (Phillips), niece of Paul Argos (Chaney), arrives on the same train as Ripley. Forming a relationship with her, Evans decides to clean up the town. In the meantime, Viola has been kidnapped and hidden in a roadhouse on the other side of the tracks. Evans and Ripley rescue her but incur the enmity of the denizens of the district. They attack the newspaper office and, in the face of defeat, Evans orders Ripley to escape with Viola. When she discovers that Evans is missing, she returns to the burning town and discovers him wounded. She declares her love and indications are that he will survive to claim it.


== Cast ==
Dorothy Phillips as Viola Argos
Jack Mulhall as Jack Ripley
Lon Chaney as Paul Argos
William Stowell as Dick Evans
Bert Appling as Red Pete Jackson
Evelyn Selbie as Boston Kate
Alfred Allen as Ben Mackey


== Reception ==
Like many American films of the time, The Grand Passion was subject to cuts by city and state film censorship boards. For example, the Chicago Board of Censors required cuts, in Reel 1, of Pete lunging at reporter with knife, women in background being pulled over the balustrade, two views of young women dancing on platform, Reel 2, two scenes of Pete throwing Paul across table, young women throwing arms around Dick's neck, Reel 3, view of dance hall as seen by young woman looking through widow, putting intoxicated woman into taxicab, Reel 4, shooting man through window, woman doing wiggle dance on platform, two intertitles "I've come for her to kill if she's been wronged" and "Shoot the word around", all scenes of young women in Boston Kate's house after the intertitle "Thank God, a woman" to include all scenes of Kate and her locking door, Reel 5, the intertitle "She's at Boston Kate's in the valley", two scenes of young woman in room looking at barred window, all scenes of man in hall and at young woman's door, two scenes of Viola at window in gown falling from shoulders, woman shooting Dick, printer shooting Pete, three scenes of women rushing down street, Reel 6, two scenes of women rushing down street, two dance hall scenes at beginning of reel, all scenes of women and men in automobile carrying torches to include the intertitle "To the jail, we'll turn Red Pete loose", all scenes of riot in street showing rioters shooting and people falling to include killing of boy, man falling at crossing and dragging the body away, and the intertitle "You're marked. Good-night for Red Pete."


== References ==


== External links ==
The Grand Passion on IMDb
The Grand Passion at AllMovie