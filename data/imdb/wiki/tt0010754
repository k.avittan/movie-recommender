Natalia Mikhailovna Vodianova (Russian: Ната́лья Миха́йловна Водяно́ва, IPA: [nɐˈtalʲjə mʲɪˈxajləvnə vədʲɪˈnovə]; born 28 February 1982), nicknamed Supernova, is a Russian model, philanthropist, entrepreneur, and public speaker.
She is well known for her rags-to-riches life story and for her eight-season, seven-figure contract with Calvin Klein. In 2012, she came in third on the Forbes top-earning models list, estimated to have earned $8.6 million in one year. Vodianova is currently ranked by models.com as one of the New Supers in the fashion industry.Vodianova is the founder of the Naked Heart Foundation, a philanthropic organization committed to helping children with special needs and their families. In 2015, Vodianova launched "Elbi", a digital platform supporting philanthropy by allowing people to make micro-donations.
In 2010, Vodianova was awarded by Harper’s Bazaar as Inspiration of the Year. In 2014 Glamour recognized her as Woman of the Year and awarded her its The Voice for Children award. In 2016, Mattel created the first Russian Barbie under her name. She received Vogue India's Women of The Year award 2017.
Vodianova is a mother of 5. She is also a member of the Special Olympics International Board of Directors.


== Early life ==
Born in Gorky, Soviet Union (now Nizhny Novgorod, Russia), Vodianova grew up in a poor district of the city (Avtozavod, GAZ car factory) with her mother and two half-sisters. One of her sisters was disabled. Natalia knew first hand what it meant living and caring for a disabled person as well as the struggles they go through in life. As a teenager, Vodianova helped her mother sell fruit on the street and later set up her own fruit stand with a friend to help her family out of poverty. Vodianova's father walked out on the family when she was a toddler, and she did not have any further contact with him until after she had become famous. Her grandmother is Mordovian (Erzya).


== Career ==


=== Modelling ===
At the age of 15, Vodianova enrolled in a modelling agency. By the age of 17, she had moved to Paris, and signed with Viva Models. Vodianova has achieved success as a runway, editorial, and advertising campaign model. She has walked in more than 200 runway shows for American and European based designers' ready-to-wear and haute couture collections, and has completed advertising campaigns for Guerlain, Givenchy, Prada, Calvin Klein, Louis Vuitton, Chanel, Gucci, Nina Ricci, Versace, Yves Saint Laurent, L'Oréal, H&M, David Yurman, Marc Jacobs, Stella McCartney, Diane von Fürstenberg, Hugo Boss, Miss Sixty, Stuart Weitzman, Etam and others.She has worked with all the prolific fashion photographers such as Mario Testino, Annie Leibovitz, Steven Meisel, Patrick Demarchelier, Paolo Roversi, Bruce Weber, Peter Lindbergh, Craig McDean, Inez and Vinoodh, Mario Sorrenti, Mert Alas and Marcus Piggott, Alasdair McLellan, David Sims, Juergen Teller, Helmut Newton, Bruno Aveillan, Hedi Slimane, Ellen von Unwerth, Terry Richardson, Mariano Vivanco, Sølve Sundsbø, Steven Klein, and Karl Lagerfeld.Vodianova has also appeared on the covers of major international magazines including Vanity Fair; American, Taiwan, British, French, Italian, Chinese, Russian, Australian, Spanish, German, Indian, Turkish, Japanese, Greek, Polish, South Korean, and Portuguese Vogue; W; Numéro; i-D; Marie Claire; GQ; Elle; Allure; Glamour; Pop; Love; Cosmopolitan; Harper's Bazaar; L'Officiel; Grazia; Esquire; Tatler; Hello!, Porter, and Interview.
Photographed by Steven Meisel, Vodianova made her first appearance on the cover of American Vogue in the September 2004 edition alongside Gisele Bündchen and Daria Werbowy as one of the "Models of the Moment". She then appeared as the solo cover subject of the July 2007 edition of the magazine. During this time period, other covers of American Vogue have all featured non-model celebrities with only three other exceptions—Linda Evangelista, Liya Kebede, and Bündchen. In May 2009, Vodianova was again featured on the cover of American Vogue as one of the "Model Faces of the Moment". As of 2014, Vodianova has appeared on the cover of British Vogue ten times; the first was the September 2003 issue. As of December 2015, she has appeared on a total of 70 international Vogue covers.

 In Spring 2009, Vodianova entered a three-year agreement to be a brand ambassador for the French lingerie company Etam and will design a lingerie collection each season during the term of the agreement. The collections will be marketed under the brand Natalia pour Etam. Vodianova has a contract with Guerlain, a French perfume house and cosmetics line, which was acquired in 1994 by the LVMH group, a multinational investment corporation specializing in luxury brands. She is also a regular campaign and poster model for Stella McCartney. She featured as a 'Face of the Moment' in May 2009's US Vogue.
In 2014, Vodianova appeared on the cover of the September issue of Vanity Fair shot by photographer Mario Testino. She was one of fifty models on the September 2014 cover of Italian Vogue, the magazine's fiftieth anniversary issue. She has also appeared on a solo cover for French Vogue in the September issue, her sixth cover for the magazine. In the November issue, Vodianova graced of the cover of American Vogue shot by Annie Leibovitz; this was her fourth American Vogue cover and second solo American Vogue cover since her appearance in July 2007.
In 2015, Vodianova was photographed by Steven Meisel for the 2015 Pirelli Calendar; this was her fourth appearance in Pirelli since 2003. She returned as the face of Calvin Klein's Euphoria fragrance shot by Inez and Vinoodh, ten years since her first exclusive contract with Calvin Klein. Vice President of Calvin Klein Fragrances, Coty Inc, Friedemann Schmid, commenting on her return to the brand, said "Natalia Vodianova truly embodies the sensual glamour and sophistication of the Euphoria woman". Vodianova returned for the Stella McCartney campaign for the spring/summer 2015 and fall/winter 2015 collections. She ranked No. 5 on the 2015 Forbes list of highest earning models, earning an estimated $7 million.In 2016, Vodianova was selected and photographed by Annie Leibovitz as part of the 43rd Pirelli Calendar, which celebrated some of the world's most inspiring women, such as Tavi Gevinson, Serena Williams, Amy Schumer, Yoko Ono, Fran Lebowitz, Patti Smith, Kathleen Kennedy, Yao Chen, Mellody Hobson, Ava DuVernay, Agnes Gund, and Shirin Neshat. This was her fifth appearance in Pirelli. She also fronted Riccardo Tisci's Givenchy 2016 Spring/Summer campaign along with Tisci's longtime muse Mariacarla Boscono. Other veteran models in the campaign included Lara Stone, Joan Smalls, Miranda Kerr, Gemma Ward, Iselin Steiro, Candice Swanepoel. This was Vodianova's fourth appearance in Tisci's Givenchy campaigns. Shot by Steven Meisel, she became the campaign model for Prada's spring/summer 2016 collection along with their comeback muse Sasha Pivovarova and Dutch newcomer Yasmin Wijnaldum. Vodianova appeared in her ninth ad campaign for Stella McCartney in their spring/summer 2016 collection together with Boscono. She also returned as the campaign model for Italian denim wear brand Miss Sixty. In October 2016, she appeared in her second campaign ad for David Yurman, which was shot by Bruce Weber for the fall/winter 2016 campaign.In 2017, Vodianova was again in the Campaign Ad for David Yurman along with model Taylor Marie Hill for the spring/summer collection. Vodianova was also hired by H&M for their Conscious Exclusive collection, a sustainable line using recycled and organic materials. Vodianova appeared in her 5th cover for W Magazine in their June/July 2017 edition, which was dedicated to Vodianova and photographed by Steven Meisel.Vodianova was ranked 14th in British television channel Five's 2005 television programme World's Greatest Supermodel. Forbes magazine estimates she earned US$4.5 million between August 2006 and July 2007, US$4.8 million between May 2007 and April 2008 and US$5.5 million between June 2008 and June 2009, making her the seventh highest earning model worldwide during all three time periods. In 2010, she was named in Tatler's top 10 best-dressed list.


=== Acting ===
She was cast as Medusa in the 2010 film Clash of the Titans. In addition, she played major roles in the films CQ (2001), Probka (2009), and a 2012 film adaptation of Belle du Seigneur. She also served as the narrator in a major broadcast of Swan Lake from the Mariinsky Theatre (2013).


=== Other work ===
In May 2009, Vodianova co-hosted the semi-finals of the Eurovision Song Contest in Moscow. On 12 December 2009, she was designated an ambassador of the 2014 Winter Olympics in Sochi. In 2010, she appeared at the 2010 Winter Olympics closing ceremony in Vancouver.


== Philanthropy ==
Vodianova is a founder of the Naked Heart Foundation, a philanthropic organisation that strives to provide a safe and inspiring environment in which to play for every child living in urban Russia and to help support families raising children with disabilities.She was inspired to found the charity after the Beslan tragedy. Vodianova's idea was to distract children who survived with play from their psychological trauma. The organisation built its first play park in 2006 in Nizhny Novgorod. It has since built nearly many more including one in Beslan.Vodianova also lends her support to a number of philanthropic causes, such as the (Bugaboo)RED campaign, an initiative to help eliminate AIDS in Africa. That same year, Vodianova became an ambassador for Hear the World, a global campaign that seeks to raise awareness of the topic of hearing and hearing loss and to promote good hearing all over the world.She is a spokesperson for the Tiger Trade Campaign, an alliance of 38 organisations "to bring back wild tigers by stopping trade in tiger parts and products from all sources." In an interview supporting the campaign, Vodianova said: "I'm proud that Russia, my country, is home to the most magnicifent of animals, the wild Siberian tiger. Today it is up to us to protect the tiger and its home, fewer than 350 Siberian tigers remain in the wild and no more than 3,400 tigers survive anywhere in the world. Unless we act now we will see the extinction of the wild tiger within our lifetime."Since 2015, an annual "Fabulous Fund Fair" is co-hosted by Vodianova and Karlie Kloss. Luxury brands run their own fair stalls to raise money for Naked Heart Foundation's work with special needs children.
In 2015, together with Timon Afinsky, Vodianova co-founded the digital app Elbi, which allows people to donate money with a "Love Button," create content, send messages, and vote for charitable purposes. The app was promoted by the Clinton Global Initiative Commitment in 2016 and supported by Joanna Shields, Bebo co-founder Michael Birch, Wikipedia founder Jimmy Wales, Huffington Post co-founder Arianna Huffington, Mind Candy founder Acton Smith, designer Diane von Furstenberg, co-founder of Lastminute.com and Made.com Brent Hoberman, as well as the founder and CEO of Illumination Entertainment, Chris Meledandri.


== Other projects ==


=== Special Olympics ===
Vodianova is a member of the international board of directors for the Special Olympics and supported the 2014 European Games and the 2015 World Games.


=== Running Heart ===
Vodianova organizes an annual charity called the ‘Running Hearts’ charity race in Russia, together with Polina Kitsenko. The event is promoted by the Naked Heart Foundation and Podium Market. In 2016, the marathon raised RUB 37 million. All the funds raised went towards the Naked Heart Foundation's project to develop early intervention services – a network of free support services for families raising children under 6 with conditions such as autism, Down's syndrome, cerebral palsy and learning disabilities.


=== Barbie Natalia ===
The Mattel company chose Vodianova for a first Russian Barbie doll. Vodianova agreed to the project stressing the "importance of play for the balanced development of any child" and added that with the new doll the foundation's program Play with Purpose would definitely "acquire a new meaning." Part of the profit on sales would go to the Naked Heart Foundation to create a system of free services for families raising children with special needs and building inclusive play parks for children.


=== FIFA 2018 World Cup ambassador ===
Vodianova was appointed the ambassador of her native city, Nizhny Novgorod, for the World Football Championship in Russia in 2018.


=== Fashion 4 Development ===
Fashion 4 Development's Annual First Ladies Luncheon is held during the United Nations General Assembly week in New York to celebrate cooperation between diplomacy and fashion for the greater good of women and children worldwide. In 2016, Vodianova was selected as one of the “Agents of Change” and received the Fashion 4 Development Award.
In May 2009, Vodianova co-hosted the semi-finals of the Eurovision Song Contest in Moscow. On 12 December 2009, she was designated an ambassador of the Winter Olympics in Sochi. In 2010, she appeared at the 2010 Winter Olympics closing ceremony in Vancouver.


=== Public speeches ===
Vodianova has spoken at the Clinton Global Initiative, Global Citizen, First Ladies’ Luncheon, Web Summit, Founders Forum, Millennial Summit, One Young World, Tech Crunch, Founders Forum, and LeWeb.


== Awards ==
In November 2010, Harper's Bazaar awarded Vodianova with the Inspiration of the Year Award, given in recognition of her philanthropic efforts.In April 2013, she was honoured with the Inspiration Award at the fourth annual DVF Awards. The award is given to women "who have demonstrated leadership, strength and courage in the face of adversity, and who use their influence to effect positive change." The same year, she was also the recipient of Harper's Bazaar's Philanthropist of the Year Award.On 10 November 2014, Vodianova was one of the honored 2014 Glamour Woman of the Year for her contribution and philanthropy as The Voice for Children. The ceremony was held at Carnegie Hall, with Arianna Huffington presenting the award to Vodianova.
She received Vogue India's Women of The Year award in 2017.


== Personal life ==
Vodianova met Justin Portman (b. 1969), half-brother of the 10th Viscount Portman and son of Edward Portman, 9th Viscount Portman, a British property heir, at a Paris dinner in 2001. They married in November 2001 when she was 8 months pregnant. In September 2002, over nine months after registering the marriage in the UK, they had a wedding ceremony in St. Petersburg, where Vodianova wore a dress designed by Tom Ford. They have three children: son Lucas Alexander (born 2001), daughter Neva (born 2006), and son Viktor (born 2007). Neva is named after the Russian river of the same name and currently attends école Jeannine Manuel in Paris. Vodianova and Portman announced their separation in June 2011.Following her separation from Portman, Vodianova began a relationship with Antoine Arnault, son of LVMH founder Bernard Arnault and the CEO of luxury brand Berluti. The couple has two sons: Maxim (born 2014) and Roman (born 2016). Vodianova announced their engagement on 31 December 2019. The COVID-19 pandemic forced the cancellation of their 27 June 2020 wedding at Saint-Pierre d'Hautvillers Abbey, and the couple officially married on 21 June 2020 at a Paris registry office.


== Notes ==


== References ==


== External links ==
Natalia Vodianova at Fashion Model Directory 
Natalia Vodianova on IMDb
Natalia Vodianova on Instagram
Profile at AskMen.com
Naked Heart Foundation