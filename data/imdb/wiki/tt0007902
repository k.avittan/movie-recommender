What You See Is What You Get is the second studio album by American country music singer Luke Combs, released through River House Artists and Columbia Nashville on November 8, 2019. It includes all five songs previously featured on the 2019 EP The Prequel, including the singles "Beer Never Broke My Heart" and "Even Though I'm Leaving" in addition to the track "1, 2 Many" (a collaboration with Brooks & Dunn), the single "Does to Me", and later the promotional single "Six Feet Apart". Combs toured North America throughout the remainder of 2019 and was to headline the C2C: Country to Country festival in Europe in 2020 in promotion of the album, however the festival was canceled due to the COVID-19 pandemic. The first four singles from the album, "Beer Never Broke My Heart", "Even Though I'm Leaving", "Does to Me" and "Lovin' on You" all reached number-one on the Billboard Country Airplay chart.


== Background ==
Combs said his intention with new track "1, 2 Many" was to "write a song that I felt like my '90s country music heroes would be proud of". It marks his second collaboration with Brooks & Dunn, with his first being a cover of their debut single "Brand New Man" on their album Reboot."Six Feet Apart", a standalone song released on May 1, 2020 in response to the COVID-19 pandemic, was later added to the end of the album's digital and streaming editions.On August 20, Combs announced that the album will be re-released as a deluxe album titled What You See Ain't Always What You Get on October 23, 2020, featuring "Six Feet Apart", as well as five new songs. The first of the new songs "Without You" was released as a single on September 18.


== Commercial performance ==
What You See Is What You Get debuted atop the US Billboard 200 on the chart dated November 23, 2019, earning 172,000 album-equivalent units, including 109,000 pure album sales. In addition to becoming Combs' first US number-one album, it opened with the "largest week for a country album" since 2018, and became the biggest week in terms of streams for a country album on record. The album has sold 320,000 copies in the United States as of March 2020, with 1,000,000 units consumed in total. After its reissue, on the chart dated November 7, 2020, the album returned to the top spot from number 21, selling 109,000 album-equivalent units. In the same week, the album broke the record for the most streams for a country record at 102.26 million streams. At 11 months and 15 days, What You See Is What You Got also became the first album since Bon Jovi's This House Is Not for Sale to return to number one after an extended wait.The album also topped the ARIA Albums Chart in Australia, becoming Combs's first number-one album there.


== Track listing ==


== Personnel ==
From What You See Is What You Get liner notes.Musicians

Luke Combs – lead and background vocals
Brooks & Dunn – vocals on "1, 2 Many"
Eric Church – vocals on "Does to Me"
Dave Cohen – piano, organ, synthesizer
Jon Conley – electric guitar, acoustic guitar, banjo
Doug Frasure – drums
Aubrey Haynie – fiddle
Wil Houchens – piano, organ, synthesizer
Ben Jordan – bass guitar
Buddy Leach – saxophone
Tim Marks – bass guitar
Carl Miner – acoustic guitar, banjo, mandolin
Scott Moffatt – background vocals, electric guitar, acoustic guitar, banjo, synthesizer, percussion, glockenspiel, clapping, programming
Gary Morse – pedal steel guitar, lap steel guitar
Sol Philcox-Littlefield – electric guitar, slide guitar
Jerry Roe – drums
Amanda Shires – fiddle on "Without You"
Jimmie Lee Sloas – bass guitar
Ilya Toshinsky – acoustic guitar, mandolinTechnical

Luke Armentrout – mastering assistant
Nick Autry – engineering of Brooks & Dunn's vocals on "1, 2 Many"
Taylor Chadwick – mastering assistant
Jim Cooley – mixing (1–5, 7, 8–10, 12, 13, 15, 16)
Andrew Darby – mastering assistant
Dan Davis – engineering assistant
Bobbi Giel – mastering assistant
Mike Gillies – digital editing
Alex Gilson – engineering
Rob Hendon – cover artist
Travis Humbert – session cleanup
Mike Kyle – engineering of Brooks & Dunn's vocals on "1, 2 Many"
Kam Luchterhand – engineering assistant
Andrew Mendelson – mastering
Scott Moffatt – producer; mixing (6, 7, 14, 16, 17)
Seth Morton – engineering assistant
Jason Mott – engineering assistant
Allen Parker – engineering of Eric Church's vocals on "Does to Me"
Megan Peterson – mastering assistant
Joey Stanca – engineering assistant
Preston White – engineering assistant


== Charts ==


== Certifications ==


== References ==