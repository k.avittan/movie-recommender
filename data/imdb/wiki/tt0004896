Anita Blake: Vampire Hunter is a series of urban fantasy novels,  short stories, and comic books by Laurell K. Hamilton. The books have sold more than six million copies; many have made The New York Times Best Seller list.The series is narrated in the first person by Anita Blake, who works in St. Louis, Missouri, as a professional zombie raiser, vampire executioner and supernatural consultant for the police. The early novels focus predominantly on crime-solving and action; the later ones on Anita's personal and sexual-defender relationships and power.


== Series synopsis ==
The series takes place in a parallel universe where supernatural creatures and powers are real and their presence is public knowledge. Supernatural beings are considered citizens with most of the rights of regular humans. The novels follow legal vampire executioner Anita Blake's ongoing conflicts with the supernatural as she attempts to solve a variety of mysteries, come to terms with her own abilities, and navigate an increasingly complex series of romantic and political relationships. As the series progresses, Anita's perspective on the supernatural changes; initially she sees preternatural beings simply as "monsters" to be fought, and later grows to see them as communities to be protected, as well as possible love interests.


== Books in the series ==
Guilty Pleasures (1993) ISBN 0-515-13449-X
The Laughing Corpse (Sep 1994) ISBN 0-425-19200-8
Circus of the Damned (1995) ISBN 0-515-13448-1
The Lunatic Cafe (1996) ISBN 0-425-20137-6
Bloody Bones (1996) ISBN 0-425-20567-3
The Killing Dance (1997) ISBN 0-425-20906-7
Burnt Offerings (1998) ISBN 0-515-13447-3
Blue Moon (1998) ISBN 0-515-13445-7
Obsidian Butterfly (2000) ISBN 0-515-13450-3
Narcissus in Chains (2001) ISBN 5-558-61270-3
Cerulean Sins (2003) ISBN 0-515-13681-6
Incubus Dreams (2004) ISBN 0-515-13975-0
Micah (2006) ISBN 0-515-14087-2
Danse Macabre (Jun-2006) ISBN 0-425-20797-8
The Harlequin (Jun-2007) ISBN 978-0-425-21724-5
Blood Noir (May-2008) ISBN 978-0-425-22219-5
Skin Trade (Jun-2009) ISBN 978-0-425-22772-5
Flirt (February 2010) ISBN 978-0-425-23567-6
Bullet (June 2010) ISBN 978-0-425-23433-4
Hit List (June 2011) ISBN 978-0-425-24113-4
Kiss the Dead (June 2012) ISBN 978-0-425-24754-9
Affliction (July 2013) ISBN 978-0-425-25570-4
Jason (December 2014) ISBN 978-0-515-15607-2
Dead Ice (June 2015) ISBN 978-0-425-25571-1 (The title was created by a fan during a contest Hamilton held)
Crimson Death (October 2016) ISBN 978-1-101-98773-5
Serpentine (August 7, 2018) ISBN 978-0-425-25568-1
Sucker Punch (August 4, 2020) ISBN 978-1-984-80443-3
Rafael (February 9, 2021) ISBN 978-0-593-33291-7 (not yet published)


=== Novellas and short-story collections ===
Out of This World (2001) ISBN 0-515-13109-1 (contains chapters from Narcissus in Chains, "Magic Like Heat Across My Skin", and short stories by other authors)
Strange Candy (2006) ISBN 0-425-21201-7 (contains original Anita Blake short story "Those Who Seek Forgiveness" (the very first story featuring Blake), "The Girl Who Was Infatuated with Death", and other short stories by Hamilton.)
Beauty: An Anita Blake, Vampire Hunter Outtake (May 2012) ISBN 978-1-101-57930-5 (#20.5)
Dancing: An Anita Blake Novella (September 2013)  ISBN 978-0-698-15643-2 (#22.5)
Shutdown (October 2013)(#22.6)
A Girl, a Goat, and a Zombie (November 2016), an eShort story available for download through Laurell K. Hamilton's website.
Wounded (December 2016) (#24.5)
Sweet Seduction (original short story/novelette; collected in Noir Fatale, May 2019)


=== Marvel Comics series ===
Marvel's license started under Dabel Brothers Publishing, and when they acquired that company's rights, they continued until their rights expired (which were contractually obligated through the first three novels).

Laurell K. Hamilton's Anita Blake, Vampire Hunter: The First Death 1–2 (7/2007 and 12/2007)
Anita Blake Vampire Hunter: Guilty Pleasures 1–12 (12/2006–8/2008)
Guilty Pleasures Handbook (2007)The Laughing Corpse Adaptation

Anita Blake: The Laughing Corpse—Animator 1–5 (10/2008–2/2009)
Anita Blake: The Laughing Corpse—Necromancer 1–5 (4/2009–9/2009)
Anita Blake: The Laughing Corpse—Executioner 1–5 (9/2009–3/2010)Circus of the Damned Adaptation

Anita Blake: Circus of the Damned—The Charmer 1–5 (5/2010–10/2010)
Anita Blake: Circus of the Damned—The Ingenue 1–5 (1/2011–5/2011)
Anita Blake: Circus of the Damned— The Scoundrel 1–5 (9/2011–5/2012)


== Adaptations ==


=== Comic books ===

In 2006 Marvel Comics and Dabel Brothers Productions began production on a graphic novel adaptation of the first three books in the series, beginning with Guilty Pleasures. Hamilton worked with Stacie M. Ritchie on the comic scripts, with Brett Booth illustrating the series until 2008.The first adaptation was well received, with the first volume of The Laughing Corpse being a New York Times bestseller in the graphic novel category. Interest in the series declined, but Marvel completed its contract for the first three novel adaptations in 2012.


=== Television ===
In March 2009 news sources began reporting that Hamilton had signed a deal with IFC to adapt the series for a full-length movie and potential television series. Shortly after, Hamilton announced that the reports were not rumors and that the series had officially been optioned. Despite initial planning, Hamilton later announced that the plans for the series had been dropped.


== Reception ==
Critical reception to the series has been mixed to positive, with Monsters and Critics praising many of the books in the series. The St. Louis Post-Dispatch praised Kiss the Dead while RT Book Reviews overall panned the novel.Fan reaction has been mixed, with some readers disliking the series' departure from crime noir thriller to focus more on the sexual dynamics in the series.


== See also ==

Anita Blake
List of Anita Blake: Vampire Hunter characters


== References ==


== External links ==
The Official Laurell K. Hamilton Website