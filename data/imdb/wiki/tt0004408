Peter Clemenza is a fictional character appearing in Mario Puzo's 1969 novel The Godfather and two of the three films based on it. He is played by Academy Award-nominee Richard Castellano in Francis Ford Coppola's 1972 film adaptation of the novel, and by Bruno Kirby (as a young man) in The Godfather Part II (1974).


== The Godfather ==
Born near Trapani, Sicily, Peter Clemenza is one of Don Vito Corleone's caporegimes and oldest friends, as well as the godfather of his eldest son, Sonny. He has a reputation as a superb judge of talent; his regime produced no fewer than five future capos—Sonny, Frank Pentangeli, Rocco Lampone, Al Neri, and Joey Zasa.
The novel explains that when Sonny sees his father kill blackhander Don Fanucci, he tells his father that he wants to join the “family business”; Vito then sends him to Clemenza for training.
He is a supporting character in the story, but several of his actions are key to the plot. For example, he is ordered by Don Corleone, via consigliere Tom Hagen, to oversee the punishment of two teenage boys who received suspended sentences for beating and sexually assaulting the daughter of undertaker Amerigo Bonasera. Vito's wife, Carmela, is the girl's godmother. Clemenza assigns the job to his "button man" Paulie Gatto, who recruits two former professional boxers turned Corleone Family loan enforcers to assist. 
When Gatto helps drug kingpin Virgil Sollozzo set up Vito to be assassinated, Sonny - now Vito’s heir apparent - orders Clemenza to execute him. Clemenza considers Gatto's actions a personal insult, having recruited and trained him. Clemenza chooses Rocco Lampone, then an associate, to kill Gatto so Lampone can "make his bones". Clemenza has Gatto drive him and Lampone around for several hours on the pretext of locating housing ("mattresses") for Corleone soldiers in the event war breaks out with the other crime families. After Lampone shoots Gatto, Clemenza—upon returning to the car after relieving himself—utters (to Lampone) his most famous line in the film: "Leave the gun. Take the cannoli."When it is decided that Vito's youngest son, Michael, will murder Sollozzo and Captain Mark McCluskey, a corrupt NYPD official on Sollozzo's payroll, Clemenza instructs Michael on using a gun, how to allay Sollozzo's suspicions, and what to do after the shooting. Michael retrieves a handgun that Clemenza had planted beforehand in the bathroom, and kills Sollozzo and McCluskey. Michael hides in Sicily, Clemenza prepares for an all-out war against the other four families. The war ultimately claims Sonny's life. Upon returning to New York, Vito dies a natural death, and Michael succeeds his father as Don.
Clemenza, on Michael's orders, personally murders both Don Victor Stracci and Carlo Rizzi, Michael's brother-in-law, who conspired with Barzini to murder Sonny. Clemenza is last seen greeting Michael as "Don Corleone" and kissing Michael's hand.


== The Godfather Part II ==
Clemenza does not appear in the present timeline of the film due to a disagreement between Castellano and Paramount Pictures over the character's dialogue and the amount of weight Castellano was expected to gain for the part. After Castellano bowed out of the film, Clemenza was written out of the script and replaced by Frank Pentangeli.  It is explained that, by the time of film, Clemenza has died under suspicious circumstances; when Vito’s middle son Fredo mentions that Clemenza died of a heart attack, Corleone assassin Willi Cicci scoffs, "That was no heart attack," implying that Clemenza may have been murdered.
Clemenza, however, appears in several flashbacks to Vito Corleone's early days, played by Bruno Kirby. He first meets Vito when asking him to hide some guns for him from the police. Vito does so, and Clemenza repays the favor by stealing an expensive carpet (assisted by a surprised Vito) and giving it to the Corleones for their apartment. Around the same time, Vito and Clemenza befriend a young Tessio. One of their lines of business is selling stolen dresses door-to-door; a deleted scene depicts Clemenza charming his way into the apartment of a young housewife and emerging a little later, having presumably had sex with her.
Later on, the trio's partnership is discovered by the local blackhander, Don Fanucci, who attempts to extort them. Clemenza initially suggests that they pay Fanucci to avoid any problems with him, but Vito talks him and Tessio into paying him less money. Shortly thereafter, Vito kills Fanucci and takes over the neighborhood - the beginnings of the future Corleone family. Clemenza is last seen at Vito's side as they open Genco Pura Olive Oil, the front for their criminal empire.


== The Sicilian ==
Clemenza appears briefly in Puzo's second Godfather installment, The Sicilian. He meets with Michael during his exile in Sicily, at his brother Domenico Clemenza's home in Trapani. They discuss what the fate of Turi Giuliano is to be, following the orders of a recovering Vito Corleone. Clemenza tells Michael that he should report to him after a week, with or without Giuliano, and that Michael could return to America afterward. Clemenza then leaves on a boat to Tunis, telling Michael that he would be back the following day to bring him back to date with the Don's orders.


== The Godfather Returns ==
Clemenza's gradual takeover of the Corleone empire in New York is briefly covered in The Godfather Returns, Mark Winegardner's sequel to Puzo's original novel. It also tells of Clemenza's involvement during Michael's return from exile and eventual, official initiation into the Corleone crime family. Most noteworthy, the novel depicts Clemenza's fatal heart attack (mentioned in The Godfather Part II), and the many conspiracy theories that he had actually been murdered by the Rosato Brothers, enemies of the Corleone Family.


== Video game version ==
The video game title The Godfather: The Game depicts Clemenza as his movie counterpart; Castellano's estate gave permission for his likeness to be used in the game. However, due to Castellano's death in 1988, all of Clemenza's dialogue had to be recorded by Jason Schombing. 
In the game, he becomes partners and good friends with the protagonist, Aldo Trapani. He gives him several missions, such as killing members of rival families, particularly the Cuneo Family.


== References ==


== External links ==