The manipulation of the devil stick (also devil-sticks, devilsticks, flower sticks, gravity sticks, or juggling sticks) is a form of gyroscopic juggling or equilibristics, consisting of manipulating one stick ("baton", 'center stick') between one or two other sticks held one in each hand. The baton is lifted, struck, or stroked by the two control sticks ('handsticks', 'sidesticks', or 'handles'), stabilizing the baton through gyroscopic motion.Manipulating devil sticks is one of the circus arts and is sometimes called devil-sticking, twirling, sticking, or stick juggling.


== History ==
Devil sticks are believed to have originated in China in the distant past, in the form of simple wooden juggling sticks. It was apparently brought to Britain sometime around 1813, when a publication mentioned that previous generations had not known of it.The first scientific analysis of the physics of the game, called "the Devil on Two Sticks," was published in 1855 by Benjamin Peirce.


== Variants ==
Devil sticks vary widely in size and construction materials, but the batons are generally based on one of two basic designs: tapered and straight. Tapered batons are tapered from the ends towards the middle such that the middle is thinner than the ends. Straight batons are uniform in width but have weights attached to the ends. Rarely, a hybrid design consisting of a tapered baton with weights attached to the ends is used.
Devil sticks are usually constructed of wood, plastic, aluminum, or composite materials such as fiberglass or carbon fiber. They are most often covered with an elastomer that both increases the friction coefficient (grip) between the baton and handles as well as providing some protection against repeated drops. Infrequently, other coverings such as cloth, suede, or leather are used. Even more rarely, a vinyl or mylar covering which reduces the stick's "grip" is used.   
Flower sticks have flower-shaped ends which  slow down the movement of the baton and make learning some moves and tricks easier. Heavier and floppier ends allow for greater control by expanding the sweet spot with more weight and increasing the duration of the control phase of a stroke or lift due to the momentum and continued motion of the 'flop' or 'tassel' on the ends of the central baton.
Fire devil sticks (also known as firesticks) typically have an aluminum core and have fuel-soaked wicks on the ends to allow them to be set on fire for visual effect. Both flower and non-flower versions of firesticks exist.
Illuminated devilsticks can create interesting visual effects in darkness with the use of battery-powered electric 'seed' bulbs; LEDs; or with phosphorescent or chemiluminescent materials.


== Gallery ==

		
		


== See also ==
Diabolo
Dexterity play


== References ==


== External links ==
DevilStick.org