Moonshine  was originally a slang term for high-proof distilled spirits that were and continue to be produced illicitly, without government authorization. Distilling such spirits outside a registered distillery remains illegal in the United States and most countries around the world. In recent years, however, commercial products labeled as moonshine have seen a resurgence of popularity.


== Terminology ==

Moonshine is known by many nicknames in English, including mountain dew, choop, hooch, homebrew, shiney, white lightning, white liquor, white whiskey, and mash liquor. Other languages and countries have their own terms for moonshine (see Moonshine by country).


== Moonshine stills ==
Moonshine stills are illegal to sell, import, and own, in most countries without permission. However, guides are often written by home brewing enthusiasts published on local brewery forums that explains where cheap equipment can be bought and how to assemble it into a still. To cut down most of the cost, stainless steel vessels are often replaced with plastic (e.g. polypropylene) vessels that can withstand heat, a concept of the plastic still.


=== Evaporation stills ===


==== Plastic still ====
A plastic still is a device for distillation specially adapted for separating ethanol and water. Plastic stills can  achieve a vapor alcohol content of 40%. Plastic stills are common for homebrewing of moonshine because they are cheap and easy to manufacture. The principle is that a smaller amount of liquid is placed in an open smaller vessel inside a larger one that is closed. The liquid is kept heated by an immersion heater at about 50 °C (122 °F), which causes it to slowly evaporate and condense on the inner walls of the outer vessel. The condensation that accumulates in the bottom of the vessel can then be diverted directly down through a filter containing activated carbon. The final product has approximately twice as much alcohol content as the starting liquid and can be distilled several times if stronger distillate is desired. The method is slow, and is not suitable for large-scale production.


=== Boiling stills ===


==== Fractional distillation ====


===== Column still =====

A column still, also called a continuous still, patent still or Coffey still, is a variety of still consisting of two columns.
Whereas a single pot still charged with wine might yield a vapor enriched to 40–50% alcohol, a column still can achieve a vapor alcohol content of 96%; an azeotropic mixture of alcohol and water. Further enrichment is only possible by absorbing the remaining water using other means, such as hydrophilic chemicals or azeotropic distillation, or a column of 3A molecular sieves, like 3A zeolite.


====== Spiral still ======
A spiral still is a type of column still with a simple slow air-cooled distillation apparatus, commonly used for bootlegging. Column and cooler consist of a 5-foot-long (1.5 m) copper tube wound in spiral form. The tube first goes up to act as a simple column, and then down to cool the product. Cookware usually consists of a 30-litre (6.6 imp gal; 7.9 US gal) wine bucket in pp plastic. The heat source is typically a 300W dip heater. The spiral burner is popular because despite its simple construction and low manufacturing cost it can provide 95% alcohol.


==== Pot still ====

A pot still is a type of distillation apparatus or still used to distill flavored liquors such as whisky or cognac, but not rectified spirit because they are bad at separating congeners. Pot stills operate on a batch distillation basis (as opposed to a Coffey or column stills which operate on a continuous basis). Traditionally constructed from copper, pot stills are made in a range of shapes and sizes depending on the quantity and style of spirit desired.
Spirits distilled in pots is commonly 40% ABV, and top out between 60-80% after multiple distillations.


== Safety ==
Poorly produced moonshine can be contaminated, mainly from materials used in the construction of the still. Stills employing automotive radiators as condensers are particularly dangerous; in some cases, glycol produced from antifreeze can be a problem. Radiators used as condensers could also contain lead at the connections to the plumbing. Using these methods often resulted in blindness or lead poisoning in those who consumed tainted liquor. This was an issue during Prohibition when many died from ingesting unhealthy substances. Consumption of lead-tainted moonshine is a serious risk factor for saturnine gout, a very painful but treatable medical condition that damages the kidneys and joints.Although methanol is not produced in toxic amounts by fermentation of sugars from grain starches, contamination is still possible by unscrupulous distillers using cheap methanol to increase the apparent strength of the product. Moonshine can be made both more palatable and perhaps less dangerous by discarding the "foreshot" – the first few ounces of alcohol that drip from the condenser. Because methanol vaporizes at a lower temperature than ethanol it is commonly believed that the foreshot contains most of the methanol, if any, from the mash. However, research shows this is not the case, and methanol is present until the very end of the distillation run. Despite this, distillers will usually collect the foreshots until the temperature of the still reaches 80 °C (176 °F).  Additionally, the head that comes immediately after the foreshot typically contains small amounts of other undesirable compounds, such as acetone and various aldehydes. Fusel alcohols are other undesirable byproducts of fermentation that are contained in the "aftershot", and are also typically discarded.
Alcohol concentrations at higher strengths (the GHS identifies concentrations above 24% ABV as dangerous) are flammable and therefore dangerous to handle. This is especially true during the distilling process when vaporized alcohol may accumulate in the air to dangerous concentrations if adequate ventilation is not provided.


=== Adulterated moonshine ===

The incidence of impure moonshine has been documented to significantly increase the risk of renal disease among those who regularly consume it, primarily from increased lead content.Outbreaks of methanol poisoning have occurred when methanol is used to adulterate moonshine (bootleg liquor).


=== Tests ===
A quick estimate of the alcoholic strength, or proof, of the distillate (the ratio of alcohol to water) is often achieved by shaking a clear container of the distillate. Large bubbles with a short duration indicate a higher alcohol content, while smaller bubbles that disappear more slowly indicate lower alcohol content.A more reliable method is to use an alcoholmeter or hydrometer. A hydrometer is used during and after the fermentation process to determine the potential alcohol percent of the moonshine, whereas an alcoholmeter is used after the product has been distilled to determine the volume percent or proof.


=== Myth ===

A common folk test for the quality of moonshine was to pour a small quantity of it into a spoon and set it on fire. The theory was that a safe distillate burns with a blue flame, but a tainted distillate burns with a yellow flame. Practitioners of this simple test also held that if a radiator coil had been used as a condenser, then there would be lead in the distillate, which would give a reddish flame. This led to the mnemonic, "Lead burns red and makes you dead." or "Red means dead."Other toxic components, such as methanol, cannot be detected with a simple burn test, as methanol flames are also blue and difficult to see in daylight.


== Legality ==


== History ==

Moonshine historically referred to "clear, unaged whiskey", once made with barley in Scotland and Ireland or corn mash in the United States, though sugar became just as common in illicit liquor during the last century. The word originated in the British Isles as a result of excise laws, but only became meaningful in the United States after a tax passed during the Civil War outlawing non-registered stills. Illegal distilling accelerated during the Prohibition era (1920-1933) which mandated a total ban on alcohol production under the Eighteenth Amendment of the Constitution. Since the amendment's repeal in 1933, laws focus on evasion of taxation on any type of spirits or intoxicating liquors. Applicable laws were historically enforced by the Bureau of Alcohol, Tobacco, Firearms and Explosives of the US Department of Justice, but are now usually handled by state agencies. Enforcement agents were once known colloquially as "revenuers".


=== Etymology ===
The earliest known instance of the term "moonshine" being used to refer to illicit alcohol dates to the 1785 copy of Grose's Dictionary of the Vulgar Tongue. Prior to that, "moonshine" referred to anything "illusory" or to literally the light of the moon. The U.S. Government considers the word a "fanciful term" and does not regulate its use on the labels of commercial products, as such, legal moonshines may be any type of spirit, which must be indicated elsewhere on the label.


=== Process ===
Moonshine distillation was done at night to avoid discovery. While moonshiners were present in urban and rural areas around the United States after the civil war, moonshine production concentrated in Appalachia because the limited road network made it easy to evade revenue officers and because it was difficult and expensive to transport corn crops. As a study of farmers in Cocke County, Tennessee, observes: "One could transport much more value in corn if it was first converted to whiskey. One horse could haul ten times more value on its back in whiskey than in corn." Moonshiners in Harlan County, Kentucky, like Maggie Bailey, sold moonshine in order to provide for their families. Others, like Amos Owens from Rutherford County, North Carolina and Marvin "Popcorn" Sutton from Maggie Valley, North Carolina, sold moonshine in nearby areas. Sutton's life was covered in a documentary on the Discovery Channel called "Moonshiners". The bootlegger once said that the malt (a combination of corn, barley, rye) is what makes the basic moonshine recipe work. In modern usage, the term "moonshine" still implies the liquor is produced illegally, and the term is sometimes used on the labels of legal products to market them as providing a forbidden drinking experience.
Once the liquor was distilled, drivers called "runners" or "bootleggers" smuggled moonshine and "bootleg" (illegally imported) liquor  across the region in cars specially modified for speed and load-carrying capacity. The cars were ordinary on the outside but modified with souped-up engines, extra interior room, and heavy-duty shock absorbers to support the weight of the illicit alcohol. After Prohibition ended, the out-of-work drivers kept their skills sharp through organized races, which led to the formation of the National Association for Stock Car Auto Racing (NASCAR). Several former "runners" became noted drivers in the sport.


== See also ==

Applejack (drink)
Bootleggers and Baptists
Bureau of Alcohol, Tobacco, Firearms and Explosives (ATF)
Congener (alcohol)
Dixie Mafia
Farmhouse ale
Free Beer
Homebrewing
Moonshine in popular culture
Nip joint
Rum-running
Sour mash


== References ==


== Sources ==
Davis, Elaine. Minnesota 13: "Wet" Wild Prohibition Days (2007) ISBN 9780979801709
Peine, Emelie K.; Schafft, Kai A. (Spring–Fall 2012). "Moonshine, Mountaineers, and Modernity: Distilling Cultural History in the Southern Appalachian Mountains". Journal of Appalachian Studies. Appalachian Studies Association. 18 (1/2): 93–112. JSTOR 23337709.CS1 maint: ref=harv (link)
Rowley, Matthew. Moonshine! History, songs, stories, and how-tos (2007) ISBN 9781579906481
Watman, Max. Chasing the White Dog: An Amateur Outlaw's Adventures in Moonshine (2010) ISBN 9781439170243
King, Jeff. The Home Distiller's Workbook: Your Guide to Making Moonshine, Whisky, Vodka, Rum and So Much More! (2012) ISBN 9781469989396


== External links ==
"Moonshine – Blue Ridge Style" An Exhibition Produced by the Blue Ridge Institute and the Museum of Ferrum College
Déantús an Phoitín (Poteen Making), by Mac Dara Ó Curraidhín (a one-hour 1998 Irish documentary film on the origins of the craft).
North Carolina Moonshine – Historical information, images, music, and film excerpts
Moonshine news page – Alcohol and Drugs History Society
Georgia Moonshine – History and folk traditions in Georgia, USA
"Moonshine 'tempts new generation'" – BBC on distilling illegal liquor in the 21st century.
Moonshine Franklin Co Virginia Moonshine Still from the past – Video