Beautiful Jim Key was a famous performing horse around the turn of the twentieth century. His promoters claimed that the horse could read and write, make change with money, do arithmetic for "numbers below thirty," and cite Bible passages "where the horse is mentioned." His trainer, "Dr." William Key, was a former slave, a self-trained veterinarian, and a patent medicine salesman. Key emphasized that he used only patience and kindness in teaching the horse, and never a whip.The horse became a celebrity thanks to the progressive promotion of A. R. Rogers. The horse performed at large venues from Atlantic City to Chicago.


== Tours ==
Beautiful Jim Key and his trainer periodically toured the United States in a special railroad car to promote the fledgling cause of the humane treatment of animals. They performed in venues in most of the larger American cities, including New York’s Madison Square Garden. The horse was among the most popular attractions at the 1904 St. Louis World's Fair. Beautiful Jim Key was supposedly intelligent enough that he could calculate mathematical problems, possibly even trigonometry.

President William McKinley saw Beautiful Jim Key perform at an exposition in Tennessee and declared, “This is the most astonishing and entertaining exhibition I have ever witnessed.” The President also commented that it was an example of what “kindness and patience” could accomplish.


== Cruelty to Animals ==
The horse was made an honorary member of George Thorndike Angell's American Humane Association. He also got 2 million kids to gather to pledge never be mean to animals.


== See also ==
Clever Hans
Lady Wonder
Betsy, a border collie known to understand over 340 words
Koko, a gorilla who learned sign language
Alex, a grey parrot known for intelligent use of speech
Animal cognition
Learned pig


== References ==

“A Tribute to Jim Key,” Atlanta Constitution, December 23, 1898


== External links ==
Digitized documents and images concerning Beautiful Jim Key in the Tennessee Virtual Archive (TeVA)
Article at Horseforum.com on "Doc Key and 'Beautiful Jim Key.'"
Article at the blog Horse & Man on "Beautiful Jim Key."
Article from Lee Gaskin's "At The Fair: The 1904 St. Louis World's Fair" website
Video with more pictures and information about Jim Key