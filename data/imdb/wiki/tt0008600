This list of most expensive watches sold at auction documents the watches sold at auction worldwide for at least 1.5 million US dollars. The final price listed includes the buyer's premium paid to the auction houses, and has been converted to US dollars, if needed, according to the currency exchange rate at the time of auction. Inflation-adjusted prices are also listed for reference. If a watch has been sold at auction for several times, only the highest final price is included. Finally, any auctioned watch without public online records from auctioneers (e.g. major auction houses) will not be included in the ranking.
As of December 2019, the most expensive watch (and wristwatch) ever sold at auction is the Patek Philippe Grandmaster Chime Ref. 6300A-010, fetching US$31.19 million (31,000,000 CHF) in Geneva on November 9, 2019. The most expensive pocket watch ever sold at auction is the Patek Philippe Henry Graves Supercomplication, fetching US$23.98 million (23,237,000 CHF) in Geneva on November 11, 2014.As of December 2019, at least 69 watches have been sold at auction for over two million US dollars, and at least 108 watches have been sold at auction for over 1.5 million US dollars. Among the top 10 of these watches, seven are Patek Philippe watches and three are Rolex watches.


== Summary (as of December 2019) ==


=== Auction records ===
Worldwide: The most expensive watch ever sold at auction worldwide is the Patek Philippe Grandmaster Chime Ref. 6300A-010, which fetched 31.19 million US dollars (31,000,000 CHF) in Geneva on November 9, 2019 (by Christie's).
Asia: The most expensive watch ever sold at auction in Asia is the Patek Philipe Gobbi Milan Double Signed "Heures Universelles" Ref. 2523, which fetched 8.967 million US dollars (70,175,000 HKD) in Hong Kong on November 23, 2019 (by Christie's).
Europe: The most expensive watch ever sold at auction in Europe is the Patek Philippe Grandmaster Chime Ref. 6300A-010, which fetched 31.19 million US dollars (31,000,000 CHF) in Geneva on November 9, 2019 (by Christie's).
North America: The most expensive watch ever sold at auction in North America is the Paul Newman's Rolex Daytona, which fetched 17.75 million US dollars in New York on October 26, 2017 (by Phillips).


=== Auction houses ===
The following table shows the breakdown of auction houses that have sold the most expensive watches at auctions (for at least 2 million US dollars). As of December 2019, there are at least five auction houses that have sold watches for no less than two million US dollars: Christie's, Phillips, Antiquorum, Sotheby's, and Poly Auction.

The following table has a threshold of 1.5 million US dollars.


=== Manufacturers ===
The following table shows the brand breakdown of the most expensive watches ever sold at auction (for at least 2 million US dollars). As of December 2019, there are seven manufacturers that have manufactured watches sold at auction for at least two million US dollars: Patek Philippe, Rolex, Breguet, George Daniels, Urwerk, Zenith, Vacheron Constantin and Jehan Cremsdorff (Paris). In particular, George Daniels and Jehan Cremsdorff were independent watchmakers. 
The following table has a threshold of 1.5 million US dollars. Four additional manufacturers join: Richard Mille, Omega, Chopard and F. P. Journe.


== Ranked list (as of December 2019) ==


=== Top 69 ===
The following table contains details of the 69 watches auctioned for at least two million US dollars, ranked according to their original auction prices. The inflation-adjusted price is given by consumer price index inflation-adjusted value of United States dollars in 2019. 


=== 70th–109th ===
The following table contains details of the watches auctioned for close to (but did not reach) two million US dollars, ranked according to their original auction prices. The threshold is 1.5 million US dollars. The inflation-adjusted price is given by consumer price index inflation-adjusted value of United States dollars in 2019.


== Temporary list (January - December 2020) ==
The following table contains details of the latest auction results from January to December 2020. The information is updated to the main list at the end of the year. 


== Unranked results ==
The following table contains some of the most expensive auctioned watches without public online records from auctioneers (e.g., major auction houses). This list is possibly incomplete. 


== See also ==
List of watch manufacturers
Patek Philippe Henry Graves Supercomplication
Patek Philippe Calibre 89
Rolex Daytona


== Notes ==


== References ==