Beowawe ( bay-ə-WAH-wee) is a small town, misnomered on the internet as a ghost town, in Eureka County, Nevada, United States.


== Description ==
The community is the site of a mining operation and a geothermal power plant, and has a public library. Beowawe is a Paiute Native American word meaning "gate", so named for the peculiar shape of the hills close to town which gives the effect of a gateway opening to the valley beyond. The town is located at an elevation of 4,695 feet (1,431 m), and is situated on State Route 306 5 miles (8 km) south of Interstate 80. The Humboldt River runs through northern Nevada near Beowawe. At approximately 300 miles (480 km) long, it is the second longest river in the arid Great Basin of North America. It has no outlet to the ocean, but instead empties into the Humboldt Sink.


== History ==
Beowawe was founded in 1868 with the arrival of the railroad. Gravelly Ford, a noted site on the California Trail, is a few miles east of Beowawe on Pioneer Pass Road. The famous "Maiden's Grave" marker overlooks the ford. A tall cross in the Beowawe cemetery commemorates the burial of Lucinda Duncan, a grandmother who died on the trail in 1863. Workers building the Central Pacific Railroad first noted the grave along the Humboldt River, and in 1906, it was moved to the hillside cemetery when the Union Pacific realigned its tracks. The town reached its peak around 1881 with a population of 60 people. The town consisted of an elementary school, church, post office, store, and library. In 1909, a power plant was built but, like many ghost towns, the boom had ended by 1916 and many of the residents had moved on. Currently, Beowawe is once again tied to energy production, the home to both a geothermal power plant and a large propane tank farm near the railroad.


== Geothermal system at Beowawe ==
The surface expression of the geothermal system consists of a "215-foot-high (66 m), 1-mile-long (1.6 km) opaline sinter terrace produced by hot spring and natural geyser (sic fumarole) activity along the base of the Malpais Rim. Since 1959, several companies have tested the potential of the area as a source of steam for electrical power generation. The spectacular hot water and steam plume that at present (1985/1986) vents continuously along the top of the sinter terrace is not a natural geyser, but is a free-flowing, uncapped geothermal well." (Struhsacker, 1986, p. 111).Photos of hot springs and fumaroles – photos '9-67' (1931) and '9-56' (date unknown) – show hydrothermal activity prior to power-generation. It is unknown if those smaller hydrothermal surface features are still active as of 2015, post-power plant development; field reconnaissance would be required to assess the activity of the hydrothermal features. Photos '9-69' (close-up, date unknown) and '9-108' (distant, 1971) show the uncapped well that is not a natural geyser, which was active prior to power-production. Geyser activity at the wellhead ceased circa 1985/1986 as the local dual-flash geothermal power plant began operations.The man-made geyser at Beowawe in central Nevada is similar to Fly Geyser in northwestern Nevada in that both were artificially produced by geothermal drilling, but the former area was developed for clean energy production and the latter area was not. Another striking difference between the localities is an aesthetic one: more travertine has precipitated from the hydrothermal fluids at Fly Geyser than at Beowawe, generating spectacular mounds at Fly Geyser's uncapped wellhead, thus suggesting dissimilar water chemistry between the two geothermal systems.


== Climate ==
According to the Köppen climate classification system, Beowawe has a cool semi-arid climate (Köppen type BSk).


== See also ==


== References ==


== External links ==
Eureka County website
Beowawe Branch Library
Elko County Rose Garden - Beowawe website
Elko County Rose Garden - Beowawe Geysers
Caithness Energy - Beowawe Geothermal Project site