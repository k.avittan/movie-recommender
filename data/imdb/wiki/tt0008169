The Promised Land (Hebrew: הארץ המובטחת‎, translit.: ha'aretz hamuvtakhat; Arabic: أرض الميعاد‎, translit.: ard al-mi'ad; also known as "The Land of Milk and Honey") is the land which, according to the Tanakh (the Hebrew Bible), God promised and subsequently gave to Abraham and to his descendants. In modern contexts the phrase "Promised Land" expresses an image and idea related both to the restored Homeland for the Jewish people and to salvation and liberation.
God first made the promise to Abraham (Genesis 15:18–21):

On that day the Lord made a covenant with Abram and said, "To your descendants I give this land, from the Wadi of Egypt to the great river, the Euphrates— the land of the Kenites, Kenizzites, Kadmonites, Hittites, Perizzites, Rephaites, Amorites, Canaanites, Girgashites and Jebusites."

He later confirmed the promise to Abraham's son Isaac (Genesis 26:3), and then to Isaac's son Jacob (Genesis 28:13). The Book of Exodus describes the Promised Land in terms of the territory from the  River of Egypt to the Euphrates river (Exodus 23:31). The Israelites conquered and occupied  a smaller area of former  Canaanite land and land east of the Jordan River after Moses led the Exodus out of Egypt (Numbers 34:1–12), and the Book of Deuteronomy presents this occupation as God's fulfilment of the promise (Deuteronomy 1:8). Moses anticipated that God might subsequently give the Israelites land reflecting the boundaries of God's original promise - if they were obedient to the covenant (Deuteronomy 19:8–9).
The concept of the Promised Land is the central tenet of Zionism, whose discourse suggests that modern Jews descend from the Israelites and Maccabees through whom they inherit the right to re-establish their  "national homeland".Palestinians also claim partial descent from the Israelites and Maccabees, as well as from other peoples who have lived in the region.African-American  spirituals invoke the imagery of the "Promised Land" as heaven or paradise
and as an escape from slavery, which can often only be reached by death. The imagery and term also appear elsewhere in popular culture (see Promised Land (disambiguation)), in sermons, and in speeches such as Martin Luther King Jr.'s 1968 "I've Been to the Mountaintop":

"I just want to do God's will. And He's allowed me to go up to the mountain. And I've looked over. And I've seen the Promised Land. I may not get there with you. But I want you to know tonight, that we, as a people, will get to the Promised Land. So I'm happy, tonight. I'm not worried about anything. I'm not fearing any man. Mine eyes have seen the glory of the coming of the Lord."


== Divine promise ==

The promise that is the basis of the term is contained in several verses of Genesis in the Torah. In Genesis 12:1 it is said:

The LORD had said to Abram, "Leave your country, your people and your father's household and go to the land I will show you."and in Genesis 12:7:

The LORD appeared to Abram and said, "To your offspring [or seed] I will give this land."Commentators have noted several problems with this promise and related ones: 

It is to Abram's descendants that the land will (in the future tense) be given, not to Abram directly nor there and then. However, in Genesis 15:7 it is said: He also said to him, "I am the LORD, who brought you out from Ur of the Chaldeans to give you this land to take possession of it." However, how this verse relates to the promises is a matter of controversy.
There is nothing in the promise to indicate God intended it be applied to Abraham’s physical descendants unconditionally, exclusively (to nobody but these descendants), exhaustively (to all of them) or in perpetuity.
Jewish commentators drawing on Rashi's comments to the first verse in the Bible, assert that no human collective ever has any a priori claim to any piece of land on the planet, and that only God decides which group inhabits which land in any point in time. This interpretation has no contradictions since the idea that the Jewish people have a claim to ownership rights on the physical land is based on the idea of God deciding to give the land to the Jewish people and commanding them to occupy it as referred to in Biblical texts previously mentioned.In Genesis 15:18–21 the boundary of the Promised Land is clarified in terms of the territory of various ancient peoples, as follows:

On that day the LORD made a covenant with Abram and said, "To your descendants I give this land, from the river of Egypt to the great river, the Euphrates - the land of the Kenites, Kenizzites, Kadmonites, Hittites, Perizzites, Rephaite, Amorites, Canaanites, Girgashites and Jebusites."The verse is said to describe what are known as "borders of the Land" (Gevulot Ha-aretz). In Jewish tradition, these borders define the maximum extent of the land promised to the descendants of Abraham through his son Isaac and grandson Jacob.The promise was confirmed to Jacob at Genesis 28:13, though the borders are still vague and is in terms of "the land on which you are lying".
Other geographical borders are given in Exodus 23:31 which describes borders as marked by the Red Sea, the "Sea of the Philistines" i.e. the Mediterranean, and the "River," (the Euphrates).
The promise is fulfilled in the biblical book of Joshua when the Israelites cross the Jordan river into the promised land for the first time. 

It took a long time before the Israelites could subdue the Canaanite inhabitants of the land. The furthest extent of the Land of Israel was achieved during the time of the united Kingdom of Israel under David. The actual land controlled by the Israelites has fluctuated considerably over time, and at times the land has been under the control of various empires. However, under Jewish tradition, even when it is not in Jewish occupation, the land has not lost its status as the Promised Land.


== Descendants of Abraham ==

Traditional Jewish interpretation, and that of most Christian commentators, define Abraham's descendants as Abraham's seed only through his son Isaac and his grandson Jacob, to the exclusion of Ishmael and Esau.  This may however reflect an eisegesis or reconstruction of primary verses based on the later biblical emphasis of Jacob's descendants.  The promises given to Abraham happened prior to the birth of Isaac and were given to all his offspring signified through the rite of circumcision. Johann Friedrich Karl Keil is less clear, as he states that the covenant is through Isaac, but notes that Ishmael's descendants have held much of that land through time.Mainstream Jewish tradition regards the promise made to Abraham, Isaac and Jacob as having been given to all Jews, including proselytes and in turn their descendants, with the traditional view being that a convert becomes a child of Abraham, as in the term "ben Avraham".


== Christian interpretation ==
In the New Testament, the descent and promise is reinterpreted along religious lines. In the Epistle to the Galatians, Paul the Apostle draws attention to the formulation of the promise, avoiding the term "seeds" in plural (meaning many people), choosing instead "seed," meaning one person, who, he understands to be Jesus (and those united with him). For example, in Galatians 3:16 he notes: 

"The promises were spoken to Abraham and to his seed. Scripture does not say “and to seeds,” meaning many people, but “and to your seed,” meaning one person, who is Christ."In Galatians 3:28–29 Paul goes further, noting that the expansion of the promise from singular to the plural is not based on genetic/physical association, but a spiritual/religious one: 

"There is neither Jew nor Gentile, neither slave nor free, neither male nor female, for you are all one in Christ Jesus. If you belong to Christ, then you are Abraham’s seed, and heirs according to the promise."In Romans 4:13 it is written: 

"It was not through the law that Abraham and his offspring received the promise that he would be heir of the world, but through the righteousness that comes by faith."


== Boundaries from the Book of Numbers ==
Boundaries of the 'Promised Land' given in the Book of Numbers (chapter 34)
The South border. —(v. 3) "Then your south quarter shall be from the wilderness of Zin along by the coast of Edom, and your south border shall be the outmost coast of the salt sea eastward : (v. 4) And your border shall turn from the south to the ascent of Akrabbim, and pass on to Zin : and the going forth thereof shall be from the south to Kadesh-barnea, and shall go on to Hazar-addar, and pass on to Azmon : (v. 5) And the border shall fetch a compass from Azmon unto the river of Egypt, and the goings out of it shall be at the sea."
The Western border. —(v. 6) "And as for the western border, ye shall even have the great sea for a border : this shall be your west border."
The North border. —(v. 7) "And this shall be your north border : from the great sea ye shall point out for you mount Hor : (v. 8) From mount Hor ye shall point out your border unto the entrance of Hamath ; and the goings forth of the border shall be to Zedad : (v 9) And the border shall go on to Ziphron, and the goings out of it shall be at Hazar-enan : this shall be your north border."
The East border. —(v. 10) "And ye shall point out your east border from Hazar-enan to Shepham : (v. 11) And the coast shall go down from Shepham to Riblah, on the east side of Ain ; and the border shall descend, and shall reach unto the side of the sea of Chinnereth eastward : (v. 12) And the border shall go down to Jordan, and the goings out of it shall be at the salt sea : this shall be your land with the coasts thereof round about."
Boundaries of the 'Promised Land' given by Jerome c.400
You may delineate the Promised Land of Moses from the Book of Numbers (ch. 34): as bounded on the south by the desert tract called Sina, between the Dead Sea and the city of Kadesh-barnea, [which is located with the Arabah to the east] and continues to the west, as far as the river of Egypt, that discharges into the open sea near the city of Rhinocolara; as bounded on the west by the sea along the coasts of Palestine, Phoenicia, Coele‑Syria, and Cilicia; as bounded on the north by the circle formed by the Taurus Mountains and Zephyrium and extending to Hamath, called Epiphany‑Syria; as bounded on the east by the city of Antioch Hippos and Lake Kinneret, now called Tiberias, and then the Jordan River which discharges into the salt sea, now called the Dead Sea.1845: Salomon Munk, Palestine, Description Géographique, Historique et Archéologique," in "L'Univers Pittoresque ;Under the name Palestine, we comprehend the small country formerly inhabited by the Israelites, and which is today part of Acre and Damascus pachalics. It stretched between 31 and 33° N. latitude and between 32 and 35° degrees E. longitude, an area of about 1300 French: lieues carrées. Some zealous writers, to give the land of the Hebrews some political importance, have exaggerated the extent of Palestine; but we have an authority for us that one can not reject. St. Jerome, who had long traveled in this country, said in his letter to Dardanus (ep. 129) that the northern boundary to that of the southern, was a distance of 160 Roman miles, which is about 55 French: lieues. He paid homage to the truth despite his fears, as he said himself, of availing the Promised Land to pagan mockery, "Pudet dicere latitudinem terrae repromissionis, ne ethnicis occasionem blasphemandi dedisse uideamur".


== See also ==
Battle of Refidim
Conquest of Canaan
Covenant of the pieces
Greater Israel
Land of Israel
Southern Levant
Who is a Jew?
Yom HaAliyah


== References ==