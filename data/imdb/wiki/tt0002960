Emma. is a 2020 British period comedy-drama film directed by Autumn de Wilde, from a screenplay by Eleanor Catton, based on Jane Austen's 1815 novel of the same name. It stars Anya Taylor-Joy as Emma Woodhouse, a young woman who interferes in the love lives of her friends. It also stars Johnny Flynn, Josh O'Connor, Callum Turner, Mia Goth, Miranda Hart, and Bill Nighy.
It was released in the United Kingdom on 14 February 2020, and in the United States on 21 February 2020. It received generally favourable reviews from critics, and grossed $25 million worldwide.


== Plot ==
In Regency-era England, wealthy Emma Woodhouse searches for a new companion after her governess, Miss Taylor, marries and becomes Mrs. Weston. Emma settles on Harriet Smith, a younger girl who Emma supposes is the unclaimed child of a gentleman; Harriet's parents are unknown but her education has been provided for. Emma learns that Mr. Robert Martin, a tenant farmer of her sister's husband's brother, Mr. Knightley, has proposed to Harriet. Though claiming she will not interfere, Emma manipulates Harriet into declining Mr. Martin's offer of marriage, much to Harriet's distress. Emma believes that Mr. Elton, the local vicar, is in love with Harriet and encourages Harriet to transfer her hopes to him.
At Christmas time, Emma's older sister and Mr. Knightley's younger brother come to visit. After everyone leaves dinner with the Westons early, Emma finds herself alone in a carriage with Mr. Elton, who declares his love for her. Emma promptly refuses him and Mr. Elton disappears for six weeks, eventually returning with a wife. Two much-talked-about members of Emma's social circle appear: Jane Fairfax, the governess niece of Miss Bates, and Frank Churchill, Mr. Weston's son from his first marriage. Emma grows jealous of Jane, but is entranced by Frank.
Frank's arrival prompts the Westons to hold a ball, where Mr. Elton embarrasses Harriet by pointedly refusing to dance with her. She is rescued by Mr. Knightley, who asks her to dance. Emma and Mr. Knightley also dance together, awakening romantic feelings between them. Though Emma leaves before Mr. Knightley can speak to her, he runs to her home only for their meeting to be interrupted by Frank, who has rescued Harriet after she was set upon by gypsies. Harriet intimates to Emma that she has fallen in love again, leading Emma to believe Harriet is in love with Frank. Emma again vows not to interfere, but manipulates circumstances so that Harriet and Frank may spend more time together.
Emma tries to spend more time with Mr. Knightley and is surprised when he repeatedly ignores her. On a picnic with their entire party of social acquaintances, Frank urges them to play a game to amuse Emma, who unthinkingly insults Miss Bates, leading the party to disband in discomfort. Mr. Knightley rebukes Emma for her behaviour, and a humiliated Emma apologizes to Miss Bates, who accepts her apology without question.
Frank Churchill's wealthy aunt dies, and he is no longer required to be at her beck and call. The Westons reveal that he has been secretly engaged to Jane Fairfax and was waiting for the death of his aunt, who was opposed to the match. The Westons had hoped he would marry Emma, but Emma is only distressed on account of Harriet. Emma breaks the news to Harriet, who reveals that she is actually in love with Mr. Knightley. Harriet realizes that Emma herself is in love with Mr. Knightley.
Mr. Knightley goes to Emma to comfort her about the news of Frank, and to reveal that he is in love with her and hopes to marry her. Initially pleased with his offer of marriage, Emma develops a nosebleed when she realizes how upset Harriet will be. Interfering one last time, she goes to Mr. Martin to make amends, offering him a portrait of Harriet she drew herself. Harriet tells Emma she has accepted Mr. Martin's offer of marriage, and that her father has revealed himself now that she is of age; he is not a gentleman, but a tradesman who makes galoshes. Emma congratulates Harriet and invites her and her father to her home.
Though Emma and Mr. Knightley are very much in love, Emma is distressed at the thought of leaving her father alone. To accommodate her wishes, Mr. Knightley suggests that rather than have Emma quit her father's home, he join them there. Emma happily agrees and the two are married.


== Cast ==


== Production ==
In October 2018, Anya Taylor-Joy was cast in the film adaptation of Emma, with Autumn de Wilde making her directorial debut with the film. In December 2018, Johnny Flynn joined the cast of the film.In March 2019, Bill Nighy, Mia Goth, Josh O'Connor, Callum Turner, Miranda Hart, Rupert Graves, Gemma Whelan, Amber Anderson and Tanya Reynolds joined the cast of the film. Alexandra Byrne was costume designer for the film. Principal photography began on 18 March 2019.Firle Place in Sussex was used for the exterior of Emma's home. Other filming locations included Lower Slaughter (exteriors) in the Cotswolds standing in for the village of Highbury, Wilton House near Salisbury in Wiltshire and Chavenage House at Beverston, Gloucestershire.


=== Music ===
In the film, Jane Fairfax outshines Emma by performing the third movement from Mozart's Piano Sonata No. 12 on the fortepiano. A trained pianist, Amber Anderson performed the piece for the soundtrack, and had to learn it twice over to adapt her technique for the shorter keys of the period instrument.The credit sequence features an original song by Johnny Flynn called "Queen Bee". Isobel Waller-Bridge asked Flynn to write a song for the film. Performed in a similar style to the film's period, Flynn wrote "Queen Bee" to convey Knightley's perspective on Emma.The soundtrack features many a cappella recordings of folk songs by artists like Maddy Prior and The Watersons. Autumn de Wilde had an immediate conception of the film's music as being rooted in folk music. She also conceived the orchestral score should emulate Sergei Prokofiev's Peter and the Wolf where each character is personified clearly.Anya Taylor-Joy, Anderson, and Flynn all sing onscreen during the course of the film. Taylor-Joy took pains to explain that her performance of "The Last Rose of Summer" uses an affected style that she imagines Emma Woodhouse would have used to charm her audience.


== Release ==
It was released in the United Kingdom on 14 February 2020, coinciding with Valentine's Day, and was released in the United States on 21 February 2020.Emma was released on 20 March digitally in the United States, Canada and the United Kingdom through Premium VOD in streaming platforms. This was because of movie theatre closures due to the COVID-19 pandemic restrictions. The film was released on DVD and Blu-ray on 19 May 2020.


== Reception ==


=== Box office ===
Emma grossed $10.1 million in the United States and Canada, and $15.1 million in other territories, for a worldwide total of $25.2 million.In North America, the film made $230,000 from five theaters in its opening weekend for a per-venue average of $46,000, the highest of 2020 at that point. The film went wide two weeks later, grossing $5 million from 1,565 theaters and finishing sixth at the box office.


=== Critical response ===
On Rotten Tomatoes, the film holds an approval rating of 87% based on 235 reviews, with a weighted average of 7.30/10. The website's critics consensus reads: "Other adaptations may do a better job of consistently capturing the spirit of the classic source material, but Jane Austen fans should still find a solid match in this Emma." On Metacritic, the film was assigned a weighted average score of 71 out of 100, based on 47 critics, indicating "generally favorable reviews." Audiences polled by CinemaScore gave the film an average grade of "B" on an A+ to F scale, and PostTrak reported it received an average 3 out of 5 stars, with 44% of people they surveyed saying they would definitely recommend it.


== Notes ==


== References ==


== External links ==
Official website 
Emma. on IMDb
Emma. at Rotten Tomatoes
Emma. at Metacritic