The Hazards of Helen is an American adventure film serial (or possibly a film series) of 119 twelve-minute episodes released over a span of slightly more than two years by the Kalem Company between November 7, 1914 and February 24, 1917.
At 23.8 hours, it is one of the longest non-feature-length motion picture series ever filmed, and is believed to be the longest of the film serial format. Based on a novel by John Russell Corvell and the play by Denman Thompson, the series was adapted to the silent screen by W. Scott Darling.
Episodes 1-48 were directed by J.P. McGowan and the remainder by J. Gunnis Davis, who was credited as James Davis.  Unlike the cliffhanger serials of the era, The Hazards of Helen is actually a film series made up of near autonomous single reel twelve-minute melodramas. Most episodes of this serial are presumed lost. 


== Production ==
Starring an independent, quick-thinking and inventive heroine, the series was filmed on location in the city of Glendale and in various parts of Tuolumne County in California. The film offered repeated dramatic situations for "Helen," a telegrapher, using such props as a moving train, a runaway boxcar, a heroine in distress tied to the railroad tracks, and other dangers. "Helen" did such things as leap off the roof of a building, roar around a sharp mountain curve behind the wheel of her speeding car, or jump onto a moving train from a car or a galloping horse while chasing the bad guy train robbers. Although the plot occasionally called for Helen to be rescued by a handsome male hero, in most episodes it was the dauntless Helen who found an ingenious way out of her dire predicament and single-handedly collared the bad guys, bringing them to justice.
The film series star, Helen Holmes, who began her career at Keystone Studios, did most of her own stunts. The series used several different stuntmen for the male parts, including Leo D. Maloney and the up-and-coming Harold Lloyd.
Along with Pearl White, who starred in Pathé's adventure serial The Perils of Pauline, Helen Holmes became a much talked about national celebrity and major box-office draw. 

Prior to filming the "Night Operator at Buxton" (episode 18), Helen Holmes fell ill and Anna Q. Nilsson replaced her on that one occasion. After 26 episodes, Holmes and director J.P. McGowan left to set up their own film production company. While working on the serial, the two had begun a relationship that led to marriage. Director J. Gunnis Davis took over and Elsie McLeod substituted in episodes 27-49 until a permanent "Helen" could be found.
The heroine for the remainder of the series was played by Rose Wenger Gibson, at the time married to Hoot Gibson. Rechristened "Helen" by the Kalem Company, she rose to a celebrity status equal to that of Helen Holmes.
"Hazard of Helen" was credited by the Las Vegas Age as the first great motion picture thrill to come to Las Vegas.


== Preservation ==
Episode 63, The Open Track, of The Hazards of Helen was preserved by the Academy Film Archive in 2010.


== Episodes ==


== See also ==
Hoot Gibson filmography
List of film serials
List of film serials by studio
Treasures from American Film Archives


== References ==


== External links ==
The Hazards of Helen on IMDb
The Hazards of Helen at silentera.com
Episode 26: "The Wild Engine" at Internet Archive
Episode 33: "In Danger's Path" at Internet Archive