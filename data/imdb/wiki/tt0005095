The Circular Staircase is a mystery novel by American writer Mary Roberts Rinehart. The story follows dowager Rachel Innes as she thwarts a series of strange crimes at a summer house she has rented with her niece and nephew. The novel was Rinehart's first bestseller and established her as one of the era's most popular writers. The story was serialized in All-Story for five issues starting with the November 1907 issue, then published in book form by Bobbs-Merrill in 1908.
Rinehart was inspired to write the novel after a visit to Melrose, a Gothic Revival castle in Northern Virginia.The Circular Staircase pioneered what became known as the "had I but known" school of mystery writing, which often feature female protagonists and narrators who foreshadow impending danger and plot developments by reflecting on what they might have done differently. Rinehart employed this formula in many of her later works, and it inspired dozens of subsequent stories. The novel was adapted for the screen twice: as a silent film in 1915, and for the television series Climax! in 1956. Its best known adaptation was as the play The Bat, which became a major Broadway hit and inspired a number of later works, including several adaptations of its own.


== Plot ==
Rachel Innes is a spinster who has had custody of her orphaned niece and nephew since they were children. Halsey and Gertrude are now 20 and 24, respectively, and they talk Rachel into renting a house in the country for the summer.
The first night Rachel is there, there is a mysterious trespasser and something falls down the stairs in the middle of the night. The second night, after Halsey and Gertrude have arrived, there is a murder, and Halsey and the friend he has brought to stay disappear.
Halsey returns a few days later, without his friend and without an explanation, but by then many other developments have occurred, to the chagrin of the residents.


== Adaptations ==

In 1912, Rinehart's friend Beatrice DeMille, mother of Cecil B. Demille, tried to work out a deal for her son's new film company to purchase the rights to The Circular Staircase and another Rinehart story. Though this failed to come together, Rinehart was soon able to sell her work to film companies, beginning with a group of comic stories that Essanay Studios adapted as short films in 1914. In 1915, Rinehart sold the film rights to The Circular Staircase to Selig Polyscope Company for an apparently small amount. The silent film, released in 1915, was the first feature-length adaptation of Rinehart's work. It was directed by Edward LeSaint and starred Guy Oliver as Halsey, Eugenie Besserer as Ray, and Stella Razeto as Gertrude. It received lukewarm reviews; one critic wrote that it followed the novel too closely to be effectively cinematic. The film is now lost.The Circular Staircase was also adapted for an episode of the television series Climax! that aired on June 21, 1956. The episode starred Judith Anderson as Rachel Innes.


=== The Bat ===

The Circular Staircase's most notable adaptation was as the 1920 stage play The Bat. Rinehart started working on the play in 1917 with Avery Hopwood; they made a number of alterations to the source to prepare it for the stage. They renamed characters, changed plot elements, and most significantly, added the titular villain, who disguises his identity under a frightening bat costume until the play's denouement. The Bat opened on August 23, 1920 at Broadway's Morosco Theatre. It was an immediate success with both audiences and critics. It ran for 878 performances in New York, and six additional companies took the show to other cities, in addition to a later London production and numerous individual performances. The play was extremely lucrative for Rinehart and her husband Stan, who had invested much of their wealth into it.Rinehart later denied that The Bat was connected to The Circular Staircase after the works' similarities led to legal troubles over film rights. In 1920, Rineheart bought back The Circular Staircase rights from Selig Polyscope, and hoped to license The Bat for a film. However, Selig re-released the 1915 Circular Staircase under the title The Bat. Rinehart tried to file suit to keep Selig from using the title. The Bat led to several adaptations of its own. In 1926 Rinehart licensed a novelization of The Bat, published under her name but ghostwritten by Stephen Vincent Benét, partly to distance The Bat from The Circular Staircase. It was filmed three times: as the 1926 silent film The Bat, as the 1930 talking film The Bat Whispers, and as the 1959 horror picture The Bat.


== References ==


=== Bibliography ===
Cohn, Jan (1980). Improbable Fiction: The Life of Mary Robert Rinehart. University of Pittsburgh Press. ISBN 0-8229-3401-9.CS1 maint: ref=harv (link)
Nickerson, Catherine Ross (1998). The Web of Iniquity: Early Detective Fiction by American Women. Durham, NC: Duke University Press. ISBN 0-8223-2271-4.CS1 maint: ref=harv (link)
Nicolella, Henry (2012). "The Circular Staircase".  In Soister, John T.; Nicolella, Henry; Joyce, Steve; Long, Harry H. (eds.). American Silent Horror, Science Fiction and Fantasy Feature Films, 1913-1929. Jefferson, NC: McFarland. pp. 89–92. ISBN 978-0-7864-8790-5. Retrieved March 16, 2016.CS1 maint: ref=harv (link)
Roseman, Mill (1977). Detectionary: A Biographical Dictionary of Leading Characters in Mystery Fiction. New York: The Overlook Press. ISBN 0-87951-041-2.CS1 maint: ref=harv (link)
Rzepka, Charles J. (2005). Detective Fiction. Malden, MA: Polity Press. ISBN 0-7456-2942-3.CS1 maint: ref=harv (link)


== External links ==
The Circular Staircase at RedeemingQualities
 The Circular Staircase public domain audiobook at LibriVox
Joyce McDonald at GirleBooks (October 2009)
Full novel at PinkMonkey