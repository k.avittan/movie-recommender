The Golden Age of Comedy (1957) is a compilation of silent comedy films from the Mack Sennett and Hal Roach studios, written and produced by Robert Youngson.
Youngson had previously produced several award-winning short documentaries beforehand, and this was the first compilation of its kind in feature-length form. Initially, the film was distributed by a small independent company, Distributors Corporation of America, before being taken up by major Hollywood studio Twentieth Century Fox.
The film's commercial success led Youngson to follow suit with other silent film compilations over the next decade. The film is often regarded as particularly bringing Laurel and Hardy back into the public's notice after years of obscurity (they are the film's most predominately featured performers), but Oliver Hardy died around the time of the film's release in August 1957. The film was released (along with several other Youngson compilations) on DVD for the first time in the United States in 2007. Its main musical theme is derived from Chopin's Etude Op. 10, No. 3, often known as "Tristesse".


== Plot Details ==
The film is divided into nine separate acts, each with their own title card.
Act One - The Laugh Factory. Featuring several Mack Sennett comedies. Films included are The Hollywood Kid, Wandering Willies, Musclebound Music, Wall Street Blues and Circus Today.
Act Two - Nobody Liked Them But The Public. Laurel and Hardy comedies, including Habeas Corpus, The Second Hundred Years, We Faw Down (identified by its British title We Slip Up) and the famous climactic pie fight from The Battle of the Century.
Act Three - The Cowboy Who Became a Legend. Featuring Will Rogers in Uncensored Movies.
Act Four - Two Unforgotten Girls. Featuring Carole Lombard in Run Girl Run and Jean Harlow in the Laurel and Hardy comedy Double Whoopee.
Act Five - The Great Actor. Highlighting Ben Turpin in several shorts he made for Mack Sennett.
Act Six - A Comedy Classic. Laurel and Hardy's Two Tars.
Act Seven - So Funny, So Sad. An extract from a Harry Langdon comedy, The Luck O' The Foolish.
Act Eight - Animal Comedy. Several examples of humour from animals such as dogs, cats and lions, with Billy Bevan, Charley Chase, Andy Clyde, Harry Gribbon and Kewpie Morgan. Titles featured include The Sting of Stings and Nip and Tuck.
Finale - Two Men On a Street. The shin-kicking, pants-ripping street battle sequence from Laurel and Hardy's You're Darn Tootin'.


== Cast ==
(Featured Performers)


== Crew ==
Narrated by Dwight Weist and Ward Wilson
Music By George Steiner
Written and Produced by Robert Youngson


== External links ==
The Golden Age of Comedy on IMDb