The Lucky Dog (1921) was the first film to include Stan Laurel and Oliver Hardy who would become the famous comedy duo  of Laurel and Hardy. 
Though they appear in scenes together they play independently of each other.
The film was shot as two reels, but some versions end abruptly after the first reel where Stan is robbed by Ollie.


== Plot ==

A hapless hero (Laurel), who after being thrown out onto the street for not paying his rent, is befriended by a stray dog. The dog and young man then (literally) bumps into a robber (Hardy) who is holding someone up. The bandit, who in the process has accidentally placed his victim’s money into the young man's back pocket, turns from his first victim, who runs off, to rob Stan. The robber then steals the money he had already stolen, from the bemused young man who had thought he was broke.
The young man and the dog escape and the dog makes friends with a poodle. The poodle’s lady owner (Florence Gilbert) persuades the young man to enter his dog into the local dog show. When his entry is refused, the young man sneaks in anyway, but is quickly thrown out, followed by all the dogs in the show. The young man spots the poodle’s owner outside looking for her dog and offers his dog in its place. She accepts and in turn offers him a lift to her home. This scene is witnessed by her jealous boyfriend, who happens to bump into the bandit and together the two plot their revenge on the young man.
At the lady's house, the young man is introduced to the boyfriend and the bandit, in disguise as the Count de Chease of Switzerland. The boyfriend proposes and is refused while the bandit attempts to shoot the young man only to have the gun jam. The boyfriend chases the lady around the house while the bandit tries to blow up the young man with a stick of dynamite. The dog comes to the rescue, chasing the bandit and the boyfriend into the garden with the dynamite and leaving them to be blown up.


== Production and release date ==
The precise date that the film was shot is not recorded. It had long been thought to have been made in 1917, partially due to comments Stan made about it during an interview in 1957. But it had also been dated to 1918 and 1919. However, on the basis of an examination of the dates that Stan was available for filming, and the appearance of a 1920 car license plate in one shot of the complete film, the most likely date filming took place is in the autumn and winter of 1920 and into early 1921. The film was released for distribution in late 1921 by Reelcraft.The film's production cost was thought to have been about $3,000.


== Cast ==
Stan Laurel – Young man
Oliver Hardy – Bandit
Florence Gilbert – Girl (Oftentimes incorrectly credited as Florence Gillet), her real name is Florence Ella Gleistein.
Jack Lloyd – Boyfriend


== References ==


=== Bibliography ===
Mitchell, Glenn. The Laurel & Hardy Encyclopedia. New York: Batsford, 2010, First edition 1995. ISBN 978-1-905287-71-0.


== External links ==
The Lucky Dog, Full Movie on YouTubeThe Lucky Dog on IMDb
"The Lucky Dog" on YouTube