A Price Above Rubies is a 1998 British-American drama film written and directed by Boaz Yakin and starring Renée Zellweger. The story centers on a young woman who finds it difficult to conform to the restrictions imposed on her by her community. Reviews of the movie were mixed, though generally positive to Zellweger's performance.
The title derives from a Jewish Sabbath tradition. The acrostic Sabbath chant The Woman of Valor (eishet chayil) which begins with the verse "... Who can find a woman of valor, her price is far above rubies ... ," which in turn is excerpted from The Book of Proverbs. This chant traditionally is a prelude to the weekly toast (kiddush) which begins the Sabbath meal.


== Plot summary ==
The film tells the story of Sonia (Renée Zellweger), a young Brooklyn woman who has just given birth to her first child. She is married, through an arranged marriage, to Mendel (Glenn Fitzgerald), a devout Hasidic Jew who is too repressed and immersed in his studies to give his wife the attention she craves. He even condemns her for making sounds during sex and considers nudity with sex "indecent".
Sonia is distressed and later, after a panic attack, she tries to kiss her sister-in-law Rachel. Rachel persuades her to talk to the Rebbe but Sonia cannot truly articulate what is upsetting her, instead resorting to a metaphor of a fire burning her up.
Sonia develops a relationship with Sender (Christopher Eccleston), who brings her into his jewellery business. Her husband forgets her birthday and Sonia says she longs for something beautiful in her life - even if it is a terrible beauty. Sender is the only release for Sonia's sexuality but she is repelled by his utter lack of morals. He is also abrupt and self-centred in his seductions, never waiting for Sonia to achieve orgasm.
Sonia sometimes sees and hears her brother. He appears as a child and judges her actions. On one occasion she buys a non-kosher egg roll whilst in Chinatown and her brother tells her off and an elderly street beggar-woman sees him and offers him candy. She comments on another woman's earrings and this leads Sonia to track down the maker of a ring she had discovered earlier that day.
The maker is Puerto Rican artist and jewellery designer Ramon (Allen Payne), who works as a salesperson in the jewelry quarter but keeps his artistry a secret from everyone in the business.
Later Sonia's husband tells her she cannot continue to work. She is furious. Her husband insists they see a marriage counsellor (their rabbi) but the man decides Sonia is not being a good enough Jew. She says she is tired of being afraid and if she is so offensive to God then 'let him do what he wants to me.' The counsellor says we bring suffering upon herself, but Sonia protests that her relatives who died in the Holocaust and her brother who died when he was ten did not deserve their suffering. The counsellor says that 'we' do not question the ways of God but Sonia corrects this to 'you' and asserts that she will question whatever she wants to.
Sonia stops wearing her wig and starts wearing a headscarf instead. She introduces Ramon and some samples to a jewellery buyer who expresses an interest in his potential as a designer. They argue at Ramon's flat as she becomes bossy over his career, and he tries to get her to model (clothed) with a naked male model so he can complete a sculpture. She runs away.
Sonia's marriage breaks down irrevocably. Sonia is locked out of her apartment, and finds that her son has been given to Rachel. She is told she may live in a tiny apartment owned by Sender and kept for 'business purposes'. When she arrives, Sender is eating at a table and it is clear he has set her up as his mistress when she asks what the price is to stay: he says above that of emeralds but below the price of rubies. This is 'freedom'. Sonia hands him back the keys and leaves.
None of her friends will take her phone calls and Sonia is homeless. She meets the beggar-woman on the street and is taken to an empty studio and given food. The woman refers to an old legend (one her brother spoke of at the start of the film), to encourage Sonia. Meantime, Mendel takes back his son - for nights only. Rachel protests but he says he would appreciate her caring for his son during the day when he is studying.
Sonia now goes to Ramon's place and he lets her stay. She says he was right to be wary of her when they met as she has destroyed every good thing she had. But Ramon disagrees, removes her jewellery, and points out that her necklace is 'a chain'. (It is unclear if the necklace is of religious significance or if he means the need to have financial security through jewellery is a chain or restriction). The two end up kissing.
Sonia dreams her brother returns from the lake to say he swam, and she - as her childhood self - says she swam too. When she wakes up in Ramon's bed there is a prominent crucifix on the wall. Sonia goes to speak to the widow of the Rebbe. The widow tells her that Sonia's words about being consumed by fire had awoken a fire in the Rebbe and for the first time in 20 years he had said 'I love you.' It is implied that they made love and the Rebbe had a heart attack. The widow is not unhappy with this outcome. She assists Sonia in reclaiming property from Sender's safe.
With Ramon's ring back in her keeping she returns it to Ramon. She doesn't want to stay as she does not feel she belongs. Ramon offers her time to think about what she wants.
Mendel arrives. Sonia asks after her son and then if Mendel misses her. He shakes his head. He asks the same of her and she shakes her head. They laugh. He apologises for forgetting her birthday but he knows that this was not all it was about. He gives her a ruby as token of his regret and invites her to visit their son.
Mendel leaves and Sonia says, 'God bless you'.


== Cast ==
Renée Zellweger as Sonia Horowitz
Christopher Eccleston as Sender Horowitz
Julianna Margulies as Rachel
Allen Payne as Ramon Garcia
Glenn Fitzgerald as Mendel Horowitz
Shelton Dane as Yossi
Kim Hunter as Rebbetzin
John Randolph as Rebbe Moshe
Kathleen Chalfant as Beggar Woman
Peter Jacobson as Schmuel
Edie Falco as Feiga
Allen Swift as Mr. Fishbein


== Reception ==
Roger Ebert of Chicago Sun-Times gave the movie three stars. While impressed by Zellweger's "ferociously strong performance", he found the film did not teach us "much about her society", and that the Hasidic community could have been treated in greater depth. Charles Taylor of Salon likewise appreciated Zellweger's performance, while also finding the cultural aspect treated too superficially. He described Sonia's choices as "clichés left over from the Liberated Woman movies of 20 years ago", and the movie generally as "that old middle-of-the-road groaner about the good and bad in every race". Maria Garcia of Film Journal International was more positively inclined to the movie, and called it a "beautifully wrought, skillfully rendered, and brilliantly acted film".


== References ==


== External links ==
A Price Above Rubies on IMDb
A Price Above Rubies at AllMovie
A Price Above Rubies at Rotten Tomatoes