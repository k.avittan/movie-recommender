The Pokémon anime, which debuted in Japan on April 1, 1997, has had over 1,000 episodes aired as of 2018. For various reasons, some have been taken out of rotation of reruns in certain countries, while others were altered or completely banned.


== Global removals ==


=== "Dennō Senshi Porygon" (Episode 38) ===

"Dennō Senshi Porygon" (でんのうせんしポリゴン, Dennō Senshi Porigon, translated as "Cyber Soldier Porygon" or "Electric Soldier Porygon") aired on TV Tokyo in Japan on December 16, 1997, at 6:30 pm Japan Standard Time. Eighteen and a half minutes into the episode, a scene in which Pikachu uses its Thunderbolt attack to stop vaccine missiles results in an explosion featuring rapid flashes of red and blue lights. Although red and blue flashes were shown earlier in the episode, a technique called "paka paka" made this scene especially intense; these flashes were extremely bright strobe lights, with blinks at a rate of about 12 Hz for about 5 seconds in almost fullscreen, and then for 2 seconds outright fullscreen.At this point, viewers complained of blurred vision, headaches, dizziness, and nausea. Seizures, blindness, convulsions, and lost consciousness were also reported. Japan's Fire Defense Agency reported a total of 685 viewers, 310 boys and 375 girls, were taken to hospitals by ambulances. Although many recovered during the ambulance trip, over 150 viewers were admitted to hospitals. Two people remained hospitalized for over two weeks. Others had seizures while watching news reports rebroadcasting clips of the scene. A fraction of the 685 children treated were diagnosed with photosensitive epilepsy.News of the incident spread quickly through Japan. On December 17, the day after the broadcast, TV Tokyo issued an apology to the Japanese people, suspended the program, and said it would investigate the cause of the seizures. Officers acting on orders from the National Police Agency questioned the program's producers about its contents and production process. The Ministry of Health, Labour and Welfare held an emergency meeting to discuss the case with experts and gather information from hospitals. The series exited the airwaves.Later studies showed that 5–10% of viewers had mild symptoms that did not need hospital treatment. Approximately 12,000 children reported mild symptoms of illness, but they more closely resembled symptoms of mass hysteria than a grand mal seizure. A study following 103 patients over three years found most viewers had no further seizures. Scientists believe the flashing lights triggered photosensitive seizures in which visual stimuli can cause altered consciousness. Although about 1 in 4,000 people are susceptible to these types of seizures, the number of people affected by this broadcast was unprecedented.After the airing of "Dennō Senshi Porygon", the Pokémon anime took a four-month hiatus. The TV Tokyo red circle logo and the Chu! (チュッ!) text were removed from the Pokémon opening and ending credits, the lightning flash was removed from the Dare da? (だれだ?, Who Is It?) segment, and the station only reran the first 37 episodes of Pokémon until it returned on April 16, 1998, when it aired "Pikachu's Goodbye", which was the only episode promoted during those months. After the hiatus, the time slot changed from Tuesday to Thursday. The opening theme was redone, black screens showing various Pokémon in spotlights were broken up into four images per screen, and opening animation omitted the TV Tokyo logo and following text. The Dare da? segment was redone, starting with a black screen without the lightning flash and continuing on the blue screen without the lightning flash. Before the incident, the opening showcased one Pokémon image per screen, ending with the TV Tokyo red circle logo and the Chu! text, and the Dare da segment's lightning flash. Before the resumption of broadcast, "Problem Inspection Report on Pocket Monster Animated Series" (アニメ ポケットモンスター問題検証報告, Anime Poketto Monsutā Mondai Kenshō Hōkoku) was shown. Broadcast in Japan on April 11, 1998, host Miyuki Yadama went over the circumstances of the program format and the on-screen advisories at the beginning of animated programs, as well as showing letters and fan drawings sent in by viewers, most of whom were concerned that the incident would lead to the anime's cancellation. After that episode aired, prior episodes with seizure-like effects were edited for rebroadcasting (especially the non-Japanese releases).
"Dennō Senshi Porygon" was the fifth episode to be banned in South Korea due to lightning flashes. This was the 38th episode of the original Japanese series.


=== Unaired episodes ===


==== "Battle of the Quaking Island! Dojoach vs. Namazun!!" (Episode 377) ====
In "Battle of the Quaking Island! Dojoach (Barboach) vs. Namazun (Whiscash)!!" Ash Ketchum has just finished the Mossdeep City Gym, and his next goal is the final Gym at Sootopolis City. Ash and his friends journey toward Jojo Island on the way and are caught in an earthquake caused by Whiscash. They then meet a Pokémon trainer named Chōta.
The episode was originally set to air in Japan on November 4, 2004, but was skipped due to the episode's similarities to the Chūetsu earthquake on October 23, 2004. The episode was later postponed, but was skipped in rotation order and eventually discontinued. While most of the other episodes were either not dubbed for English-language release or taken out of English-language syndication rotations, this episode of Pocket Monsters Advanced Generation was the second episode not to air outside of Japan, and the first episode not to air in Japan. Since then the move Earthquake alongside similar moves such as Fissure and Magnitude have not been used in the anime. This was the 377th episode of the Pokémon anime series.


==== "Team Rocket vs. Team Plasma!" (Episodes 682 and 683) ====
The two-part episode "Team Rocket vs. Team Plasma!" was originally scheduled to be broadcast on March 17 and March 24, 2011. The Pokémon Smash episode on the week that would've followed by the first part was preempted from broadcast due to the news coverage of the 2011 Tōhoku earthquake and tsunami, before being postponed due to the content of the episodes wherein Castelia City is destroyed. There were plans for the episodes to be broadcast at a later date, but the episode was never actually aired. Footage from this episode was later used in the episode "Meloetta and the Undersea Temple" which aired on September 27, 2012, as well as in "Strong Strategy Steals the Show", which aired on December 13, 2012. This was the 682nd episode and the 683rd episode of the Pokémon anime series.


=== Postponed episodes ===


==== "A Fishing Connoisseur in a Fishy Competition!" (Episode 696) ====
The episode "A Fishing Connoisseur in a Fishy Competition!" was postponed from its original airdate of April 7, 2011, to June 23, 2011, due to the 2011 earthquake and tsunami. The episode was originally titled "Hiun City Fishing Competition! Fishing Sommelier Dent Appears!!" (ヒウンシティのつり大会！釣りソムリエ・デント登場!! Hiun Shiti no Tsuri Taikai! Tsuri Somurie Dento Tōjō!!), but to fit in with its new airdate, references to the setting of Castelia (Hiun) City were removed. This was the 696th episode of the Pokémon anime series.


==== "An Undersea Place to Call Home!" (Episode 851) ====
The episode "An Undersea Place to Call Home!", featuring Ash, Clemont, Serena, and Bonnie helping a Skrelp (Kuzumo) return to its family in a sunken cruise ship, was originally set to broadcast on April 24, 2014. However, the sinking of MV Sewol led to the episode being pulled from its timeslot with plans for a later broadcast which Japan did eventually see officially on November 20, 2014.
It has had its world debut in South Korea on August 8, 2014 right after Episode 824 as originally planned. It also finally made its English debut on February 7, 2015, with the season 18 theme song "Be a Hero"


==== Episodes 1110–1116 ====
These seven episodes were postponed from their original air dates of April 26, May 3, May 10, May 17, May 24, May 31, and June 7, 2020, respectively due to the COVID-19 pandemic and replaced with reruns, but only six rerun episodes replacing six episodes 1110–1115 on April 26, May 3, May 10, May 17, May 24, and May 31, 2020. On June 7, 2020, the Pokemon anime resumed airing of new episodes, starting with episode 1110.


== Episodes removed outside of Asia ==
These episodes were never aired outside of Asia (with one notable exception of Episode 18 in the United States), and some episodes were never aired in South Korea.


=== Episodes removed by 4Kids Entertainment ===


==== "Beauty and the Beach" (Episode 18) ====

"Beauty and the Beach" is the 18th episode of the original Japanese series. It was the first episode to be originally skipped by 4Kids Entertainment upon the original American broadcast of the series until 2000. On June 24, 2000, an English-language version of the episode aired on Kids' WB as "Beauty and the Beach".
In this episode, the female characters all enter a beauty contest. Team Rocket also enters, with James donning a suit with inflatable breasts. One scene of the episode involved James showing off his artificial breasts for humorous effect, taunting Misty by saying: "Maybe, one day when you're older, you'll have a chest like this!" In one scene, he puffs up his breasts to over twice their original size. When it was dubbed and aired in 2000, all scenes of James in a bikini (about 40 seconds) were edited. There were a number of other sexualized scenes, such as one in which Ash and Brock are stunned at the sight of Misty in a bikini and another in which an older man appears to be attracted to her. In addition, all Japanese text on signs, etc. remains intact in this episode, as opposed to other first-season episodes in which it was digitally replaced with English text.
A continuity problem created with this episode's removal is due to a flashback in "Hypno's Naptime". This episode also marks the first time chronologically that Misty and Brock meet Gary and Delia. However, during the rerun of "Hypno's Naptime" on Kids Station, the flashback was changed to "Pokémon, I Choose You!".


==== "The Legend of Miniryu" (Episode 35) ====
"The Legend of Miniryu (Dratini)" (ミニリュウのでんせつ, Miniryū no Densetsu) was the second episode to be skipped by 4Kids Entertainment, and the first episode that was never aired in any dubbed format. This was the 35th episode of the original Japanese series.
The episode was skipped due to the frequent appearances of firearms throughout it. The absence leads to continuity problems as Ash captures 29 Tauros in this episode, with a 30th coming from Brock using one of his Safari Balls. The Tauros appear in later episodes and are used in tournaments by Ash, and only one episode alludes to where they came from.


==== "The Ice Cave!" (Episode 250) ====
"The Ice Cave!" (こおりのどうくつ!, Kōri no Dōkutsu!) was the third episode to be skipped over by 4Kids Entertainment; if aired, the episode would have been part of Pokémon: Master Quest (season 5). This episode was likely skipped because of the appearance of the controversial Pokémon Jynx. This was the 252nd episode of the original Japanese series. Some people believed Jynx was a racial stereotype of Africans similar to those from The Story of Little Black Sambo because of its big pink lips and black skin, or that it looked like a blackface actress. Jynx was later re-edited and given purple skin instead in later episodes.


=== Episodes removed by the Pokémon Company International ===


==== "Satoshi and Nagetukesaru! A Touchdown of Friendship!!" (Episode 1005) ====
"Satoshi (Ash Ketchum) and Nagetukesaru (Passimian)! A Touchdown of Friendship!!" is the 64th episode of the series Pocket Monsters Sun & Moon. It aired in Japan on March 1, 2018, but never aired in the United States. Although it was never explained why the episode was skipped over, some speculated that the face paint Ash used to disguise himself as a Passimian could resemble blackface. This was the 1,005th episode of the Pokémon anime series.Passimian had to wait until episode 1020, A Young Royal Flame Ignites! to be finally seen outside Japan and in the English dub for the first time, since that the banned episode was the first one in the original Japanese anime to show Passimian.


== Episodes temporarily withdrawn in the United States after September 11, 2001 ==

These episodes were temporarily removed after the September 11 attacks for destruction of buildings, the name, and/or weapons in the episode.


=== "Tentacool and Tentacruel" (Episode 19) ===
This episode was temporarily removed from rotation after the September 11 attacks of 2001, mainly because of the similarities between the attacks and Tentacruel attacking the city. The character Nastina also used military-style weapons during the fight scenes in the episode. However, Tentacruel striking a building was not removed from the dub's opening theme, and the episode is still available on the home video and DVD markets. The episode was aired a month after the September 11 attacks, and was not aired in the US again until the series began airing on Cartoon Network and Boomerang. It was also not shown for a short time in 2005 following Hurricane Katrina as it portrays a city flooded underwater.


=== "The Tower of Terror" (Episode 23) ===
This episode was temporarily withdrawn after the September 11 attacks due to its title. It has since been aired in the regular episode rotation. The episode features Ash and his friends travelling to Lavender Town to catch a Ghost Pokémon in the Pokémon Tower. Ash Ketchum and his Pokémon, Pikachu, ‘died’ during this episode and haunted his friends with the ghost type Pokémon Gastly, Haunter, and Gengar, before his spirit went back into his living body and came back to life. While the episode in question has no similarities to the September 11 attacks, the episode was likely pulled due to the title.


== See also ==
List of Pokémon episodes


== References ==


== External links ==
CNN archive news article about Seizure episode
CNN archive news article (2) about Seizure episode
Neuroscience for Kids (About "Dennō Senshi Porygon")
An article about Pokémon controversy, (includes the seizure scene)
Japanese article about the seizure episode
Psypoke :: The Pokémon Anime – Censorship A site that tells about certain things that were edited or banned outright
Pictures from the Preview of "Shaking Island Battle! Barboach vs. Whiscash!"
Japanese Web Newtype (using the 'web archive')
Information about the Shaking Island Battle! Barboach vs. Whiscash! episode
"The Legend of Dratini" episode at TV.com
List of banned episodes of Pokémon from Serebii.net