The Courier (also known as Ironbark) is a 2020 historical drama film directed by Dominic Cooke. The film stars Benedict Cumberbatch alongside  Rachel Brosnahan, Jessie Buckley, Merab Ninidze, Angus Wright, and Kirill Pirogov. 
The Courier had its world premiere at the Sundance Film Festival on 24 January 2020, and is scheduled to be released in the United States on 19 February 2021.


== Premise ==
The Courier tells the "true story of the British businessman who helped the CIA penetrate the Soviet nuclear programme during the Cold War. Wynne and his Russian source, Oleg Penkovsky (codenamed Ironbark), provided crucial intelligence that ended the Cuban Missile Crisis."


== Cast ==
Benedict Cumberbatch as Greville Wynne
Rachel Brosnahan as Emily Donovan
Jessie Buckley as Sheila Wynne
Merab Ninidze as Oleg Penkovsky
Angus Wright as Dickie Franks
Kirill Pirogov as Gribanov
Keir Hills as Andrew Wynne
Jonathan Harden as Leonard
Aleksandr Kotjakovs as Soviet Officer
Olga Koch as Irisa


== Production ==


=== Development ===
On 1 May 2018, it was announced that FilmNation Entertainment was producing a film about British spy Greville Wynne from a script by Tom O’Connor. Dominic Cooke is set to direct the film and produce alongside O’Connor, Ben Pugh, Rory Aitken, Adam Ackland, Josh Varney, and Leah Clarke. Production companies involved with the film include SunnyMarch.


=== Casting ===
Alongside the initial production announcement, it was confirmed that Benedict Cumberbatch had been cast as Greville Wynne. In October 2018, it was announced that Rachel Brosnahan, Jessie Buckley, Merab Ninidze, Angus Wright, and Kirill Pirogov had joined the cast of the film.  


=== Filming ===
Principal photography for the film commenced on 15 October 2018 and it lasted until 7 December 2018.


== Release ==
The film had its world premiere under the title Ironbark at the Sundance Film Festival on 24 January 2020. Shortly after, Roadside Attractions and Lionsgate acquired U.S. distribution rights to the film. The film, under the new name The Courier, was given an original 28 August 2020 theatrical release in the United States. However, it was delayed to 16 October 2020. It is scheduled to be released in the United Kingdom on 30 October 2020.


=== Critical response ===
On review aggregator Rotten Tomatoes, the film holds an approval rating of 82% based on 17 reviews, with an average rating of 6.56/10. On Metacritic, the film has a weighted average score of 62 out of 100, based on 10 critics, indicating "generally favorable reviews".


== References ==


== External links ==
The Courier on IMDb