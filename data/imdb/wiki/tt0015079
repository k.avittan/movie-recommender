Lone Wolf and Cub (Japanese: 子連れ狼, Hepburn: Kozure Ōkami, "Wolf taking along his child") is a manga created by writer Kazuo Koike and artist Goseki Kojima. First published in 1970, the story was adapted into six films starring Tomisaburo Wakayama, four plays, a television series starring Kinnosuke Yorozuya, and is widely recognized as an important and influential work.Lone Wolf and Cub chronicles the story of Ogami Ittō, the shōgun's executioner who uses a dōtanuki battle sword. Disgraced by false accusations from the Yagyū clan, he is forced to take the path of the assassin. Along with his three-year-old son, Daigorō, they seek revenge on the Yagyū clan and are known as "Lone Wolf and Cub".


== Plot ==
Ogami Ittō, formidable warrior and a master of the suiō-ryū swordsmanship, serves as the Kogi Kaishakunin (the Shōgun's executioner), a position of high power in the Tokugawa shogunate. Along with the oniwaban and the assassins, Ogami Ittō is responsible for enforcing the will of the shōgun over the daimyōs (lesser domain lords). For those samurai and lords ordered to commit seppuku, the Kogi Kaishakunin assists their deaths by decapitating them to relieve the agony of disembowelment; in this role, he is entitled and empowered to wear the crest of the shogunate, in effect acting in place of the shōgun.After Ogami Ittō's wife Azami gives birth to their son, Daigorō, Ogami Ittō returns to find her and all of their household brutally murdered, with only the newborn Daigorō surviving.  The supposed culprits are three former retainers of an abolished clan, avenging the execution of their lord by Ogami Ittō. However, the entire matter was planned by Ura-Yagyū (Shadow Yagyu) Yagyū Retsudō, leader of the Ura-Yagyū clan, in order to seize Ogami's post as part of a masterplan to control the three key positions of power: the spy system, the official assassins and the Shogunate Decapitator. During the initial incursion, an ihai (funeral tablet) with the shōgun's crest on it was placed inside the Ogami family shrine, signifying a supposed wish for the shogun's death. When the tablet is "discovered" during the murder investigation, its presence condemns Ittō as a traitor and thus he is forced to forfeit his post and is sentenced, along with Daigorō, to commit seppuku.
The one-year-old Daigorō is given a choice by his father: a ball or a sword. If Daigorō chose the ball, his father would kill him and send Daigorō to be with his mother; however, the child crawls toward the sword and reaches for its hilt; this assigns him the path of a rōnin. Refusing to kill themselves and fighting free from their house imprisonment, father and son begin wandering the country as "demons"—the assassin-for-hire team that becomes known as Lone Wolf and Cub, vowing to destroy the Yagyū clan to avenge Azami's death and Ittō's disgrace.
On meifumadō ("The Road to Hell"), the cursed journey for vengeance, Ogami Ittō and Daigorō experience numerous adventures. They encounter (and slay) all of Yagyū Retsudō's children (both legitimate and illegitimate) along with the entire Kurokuwa ninja clan, eventually facing Retsudō himself. When Retsudō and the Yagyū clan are unable to kill Ittō, the shogunate officially proclaims him and Daigorō outlaws with a price on their heads, authorizing all clans to try and arrest/kill them and permitting anyone to go after them for the bounty. The last duel between Ogami Ittō and Yagyū Retsudō runs 178 pages—one of the longest single fight-scenes ever published in comics.
Toward the end of their journey, Ogami Ittō's dōtanuki sword is surreptitiously tampered with and damaged by a supposed sword-polisher who is really an elite "Grass" ninja of the Yagyū clan. When Ittō is finally attacked by the last of the (kusa) Grass ninja, the sword breaks and Ittō receives wounds that are ultimately fatal. Deadlocked in mid-battle with Retsudō, Ittō's spirit leaves his body after years of fatigue and bloodshed, unable to destroy his longtime enemy and ending his path of meifumadō.
The story finishes with Daigorō taking up Retsudō's broken spear and charging in fury. Retsudō opens his arms, disregarding all defense, and allows Daigorō to drive the spear into his body. Embracing Daigorō with tears, Retsudō names him "grandson of my heart", closing the cycle of vengeance and hatred between the clans and concluding the epic.
Many of the stories are written in a non-chronological order, revealing different parts of the narrative at different times. For example, Ogami's betrayal is not revealed until the end of the first volume, after many stories have already passed.


== Creation and conception ==
Koike stated in an interview that he crafted the comic to be based upon the characters themselves and that the "essential tension between [ Ittō's ] imperative to meet these challenges while keeping his son with him on the journey" drove the story. According to Koike, "Having two characters as foils of each other is what sets things in motion" and that "If you have a strong character, the storyline will develop naturally, on its own."


== Characters ==
Ogami Ittō (拝 一刀)—The shogun's executioner, Ittō decides to avenge the death of his wife, Ogami Azami (拝 薊, "Asami" in the Dark Horse version) and to restore his clan.
Ogami Daigorō (拝 大五郎, romanized as "Daigoro" in the Dark Horse version)—The son of Ittō and Azami, Daigorō becomes a stronger warrior as the story progresses.
Yagyū Retsudō (柳生 烈堂)—The leader of the Shadow Yagyū clan, Retsudō tries everything in his power to ensure that Ittō dies.
Abe Tanomo (阿部 頼母, also known as Kaii (怪異))—The shogun's food taster and a master of poisons; originally ordered to assist Retsudō in disposing of Ittō, Tanomo dishonorably tries to kill Ittō, Daigorō and Retsudō in order to seize power for himself. In the original TV series, his character was introduced in Episode 13 of the third series, "Moon of Desire".


== Media ==


=== Manga ===


==== Japan ====
When Lone Wolf and Cub was first released in Japan in 1970, it became wildly popular (some 8 million copies were sold in Japan) for its powerful, epic samurai story and its stark and gruesome depiction of violence during Tokugawa era Japan.
Lone Wolf and Cub is one of the most highly regarded manga due to its epic scope, detailed historical accuracy, masterful artwork and nostalgic recollection of the bushido ethos. The story spans 28 volumes of manga, with over 300 pages each (totaling over 8,700 pages in all). Many of the panels of the series are depictions of nature, historical locations in Japan and traditional activities.


==== North America ====
Lone Wolf and Cub was initially released in North America in a translated English edition by First Comics in 1987. The monthly series of comic-book-sized issues featured covers by Frank Miller, Bill Sienkiewicz, Matt Wagner, Mike Ploog, and Ray Lago. Sales were initially strong, but fell sharply as the company went into a general decline. First Comics shut down in 1991 without completing the series, publishing less than a third of the total series over 45 issues.
Starting in September 2000, Dark Horse Comics began to release an English translation of the full series in 28 smaller-sized trade paperback volumes with longer page-counts (from 260 to over 300 pages), similar to the volumes published in Japan. Dark Horse completed presentation of the entire series, fully translated, with the publication of the 28th volume in December 2002. Dark Horse reused all of Miller's covers from the First Comics edition, as well as several done by Sienkiewicz, and commissioned Wagner, Guy Davis, and Vince Locke to produce new covers for several volumes of the collections. In October 2012, Dark Horse completed the release of all 28 volumes in digital format, as part of their "Dark Horse Digital" online service.


==== Volumes ====


==== Dark Horse Omnibus collected editions ====

Starting in May 2013, Dark Horse began publishing their translated editions of Lone Wolf and Cub in value-priced Omnibus editions.

Partial volumes collected in Omnibus form are marked with an asterisk (*).


==== Sequels and follow-up series ====
In 2002, a "reimagined" version of the story, Lone Wolf 2100, was created by writer Mike Kennedy and artist Francisco Ruiz Velasco with Koike's indirect involvement. The story was a post-apocalyptic take on the tale with several differences, such as a female cub and a worldwide setting: Daisy Ogami, daughter of a renowned scientist, and Itto, her father's cybernetic bodyguard and Daisy's subsequent protector, attempt to escape from the Cygnat Owari Corporation's schemes.
Dark Horse announced at the 2006 New York Comic Con that they had licensed Shin Lone Wolf & Cub, Kazuo Koike and Hideki Mori's follow-up to Lone Wolf and Cub, starring Ogami Itto's son Daigoro, the famous child in the baby cart. In this new series, which picks up immediately after the climactic battle of the original series, the bodies of Ogami Itto and Yagyu Retsudo are left lying on the beach with Daigoro left alone standing over his father's body (since no one, for political reasons, dares to bury either body or take charge of Daigoro). A bearded samurai, Tōgō Shigetada of the Satsuma clan and master of the Jigen-ryū style of swordsmanship (based on the actual historical personage Tōgō Shigetaka, creator of Jigen-ryū), wanders onto the battlefield and assists Daigoro with the cremation/funeral of Ogami Itto and Yagyu Retsudo.  Tōgō, who is on a training journey and also carries a dotanuki sword similar to Ogami's (and crafted by the same swordsmith), then assumes guardianship of Daigoro, including retrieving the baby cart and teaching/training Daigoro in Jigen-ryū.
The two soon become enmeshed in a plot by the Shogunate conceived by the ruthless Matsudaira Nobutsuna and spearheaded by his chief henchman Mamiya Rinzō (also based on an actual historical character) to topple the Satsuma clan and assume control of that fiefdom's great wealth, using Tōgō as an unwitting pawn. When Tōgō discovers that he has been tricked and used, he and Daigoro embark on the road of meifumado in a quest to kill the Shogun (which would force Matsudaira out into the open). However, Rinzō, who is not only a master of disguise but also Matsudaira's natural son, may have an even more devious plan of his own, including subverting the Shogun's own ninja and using opium to ensnare and enslave the Shogun himself. This series also introduces non-Japanese characters into the plotlines. Dark Horse began publishing the follow-up series, under the title New Lone Wolf and Cub, in June 2014; as of December 2016, all eleven volumes have been released.


=== Films ===
A total of six Lone Wolf and Cub films starring Tomisaburo Wakayama as Ogami Ittō and Tomikawa Akihiro as Daigoro have been produced based on the manga. They are also known as the Sword of Vengeance series, based on the English-language title of the first film, and later as the Baby Cart series, because young Daigoro travels in a baby carriage pushed by his father.
The first three films, directed by Kenji Misumi, were released in 1972 and produced by Shintaro Katsu, Tomisaburo Wakayama's brother and the star of the 26 part Zatoichi film series. The next three films were produced by Wakayama himself and directed by Buichi Saito, Kenji Misumi and Yoshiyuki Kuroda, released in 1972, 1973, and 1974 respectively.
Shogun Assassin (1980) was an English language compilation for the American audience, edited mainly from the second film, with 11 minutes of footage from the first. Also, the third film, Lone Wolf and Cub: Baby Cart to Hades was re-released on DVD in the US under the name Shogun Assassin 2: Lightning Swords of Death.

In 1992 the story was again adapted for film, Lone Wolf and Cub: Final Conflict also known as Handful of Sand or A Child's Hand Reaches Up (Kozure Ōkami: Sono Chiisaki te ni, literally In That Little Hand), directed by Akira Inoue and starring Masakazu Tamura as Ogami Itto.

In addition to the six original films (and Shogun Assassin in 1980), various television movies have aired in connection with the television series as pilots, compilations or originals. These include several starring Kinnosuke Yorozuya (see section Television series), in 1989 a TV movie called Lone Wolf With Child: An Assassin on the Road to Hell better known as Baby Cart In Purgatory where Hideki Takahashi plays Ogami Ittō and Tomisaburo Wakayama co-stars as Retsudo Yagyu.


==== Hollywood remake ====
In March 2012, Justin Lin was announced as the director on an American version of Lone Wolf and Cub. In June 2016, it was announced that producer Steven Paul had acquired the rights.


=== Television series ===

Two full-fledged television series based on the manga have been broadcast to date.
The first, Lone Wolf and Cub (Kozure Ōkami), was produced in a typical jidaigeki format and broadcast for three seasons from 1973 to 1976, each episode 45 minutes long. Season one originally aired 27 episodes, but the original 2nd episode "Gomune Oyuki (Oyuki of the Gomune)" was subsequently deleted from all rebroadcasts in Japan and VHS and DVD releases. Seasons two and three ran for 26 episodes each. Kinnosuke Yorozuya played Ogami Ittō, and later reprised the role in a 1984 TV movie; Daigoro was played by Katzutaka Nishikawa in the first two seasons and by Takumi Satô in the final season.
The series was co-produced by Union Motion Picture Co, Ltd. (ユニオン映画) and Studio Ship (スタジオシップ), a company formed by manga author Kazuo Koike, and originally aired on Nippon TV in Japan. It was subsequently broadcast in the United States as The Fugitive Samurai in the original Japanese with English subtitles, and released for the Toronto, Canada market by CFMT-TV (now OMNI 1) in the original Japanese with English subtitles as The Iron Samurai. It has also been aired in Germany dubbed in German, in Italy dubbed in Italian; around 1980, a Portuguese dub was aired in Brazil as O Samurai Fugitivo (The Fugitive Samurai) on TVS, actually SBT, and in Spanish, as El Samurai Fugitivo on the American Spanish TV station Univision.
The first season was released on DVD in Japan on December 20, 2006, apparently without subtitles. Twelve of the first 13 episodes were released on DVD in Germany as Kozure Okami, with audio in Japanese and German. In the US, Media Blasters released the first season on DVD on April 29, 2008, under its Tokyo Shock Label, containing the original Japanese with English subtitles. All of these releases excluded the deleted from distribution 2nd episode "Gomune Oyuki". It is unclear as to why this episode is no longer made available.
The latest television series, also titled Lone Wolf and Cub (Kozure Ōkami), aired from 2002 to 2004 in Japan with Kin'ya Kitaōji in the role of Ogami Ittō and Tsubasa Kobayashi as Daigoro. This series is not available on DVD.


=== Video game ===
In 1987, video game manufacturer Nichibutsu released a Japan-only beat 'em up based on the series named Kozure Ōkami. Players guide Ogami Itto through an army of assassins while carrying his infant son on his back. A baby cart powerup enables Ookami to mow down enemies with blasts of fire. The game is considered a rarity by the Video Arcade Preservation Society as there are no known instances of the game being owned, although it is available in ROM form for MAME.


=== Library requests ===
By 1990, many libraries understood the rise of graphic novels as a medium. Many were advised to purchase copies of various graphic novels to keep up public demand, listing many popular publications. One of the most prominent graphic novels listed was Lone Wolf and Cub, focusing on the Japanese elements in the storytelling. They would continue to add the volumes of the graphic novel well into 2003.


== Influence ==
Lone Wolf and Cub has influenced American comics, most notably Frank Miller in his Sin City and Ronin series.Novelist Max Allan Collins acknowledged the influence of Lone Wolf and Cub on his graphic novel Road to Perdition in an interview to the BBC, declaring that "Road to Perdition is 'an unabashed homage' to Lone Wolf and Cub".Darren Aronofsky had been trying to get an official Hollywood version off the ground, but never really had the rights in the first place.Episode 20 of the fifth season of the television series Bob's Burgers, "Hawk & Chick", is a parody inspired by Lone Wolf and Cub.The first season of the Disney+ Star Wars web series, The Mandalorian, has a plotline that is heavily influenced by Lone Wolf and Cub, with the titular character caring over a child of the same race as Yoda while being besieged by bounty hunters.


== References ==


== External links ==
Dark Horse Comics: Lone Wolf and Cub manga
Lone Wolf and Cub (manga) at Anime News Network's encyclopedia
Kodure Ookami at the Killer List of Videogames
Samurai and Son: The Lone Wolf and Cub Saga an essay by Patrick Macias at the Criterion Collection