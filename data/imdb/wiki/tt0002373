"Mother's Daughter" is a song by American singer Miley Cyrus. It was released on June 11, 2019, by RCA Records, as the lead single from Cyrus' second extended play She Is Coming (2019). It was written by Cyrus, Alma Miettinen, and its producer Andrew Wyatt. The Wuki remix received a nomination for Best Remixed Recording at the 62nd Annual Grammy Awards. Its music video won two MTV Video Music Awards in 2020.


== Composition ==
"Mother's Daughter" is 3 minutes and 39 seconds long. It was written by Cyrus, Alma Miettinen and its producer Andrew Wyatt. Lyrically, the song was described as a women's empowerment anthem. It focuses on the mother-daughter duo's strong relationship, and references how the singer's mother, Tish Cyrus, told her she'd "make it", singing: "I put my back into it, my heart in it/So I did it, yeah I did it.". Additionally, Cyrus describes herself as "nasty and evil", and explains that she has the freedom to do as she wishes: "Don't fuck with my freedom/I came up to get me some/I'm nasty, I'm evil/Must be something in the water or that I'm my mother's daughter", she sings in the chorus.


== Commercial performance ==
Following the release of She Is Coming, "Mother's Daughter" debuted at number 54 on the US Billboard Hot 100. It is Cyrus' 47th entry on the chart, and her highest debut since "Adore You" entered at number 42 following the release of Bangerz in 2013.


== Music video ==
The official music video was released on July 2, 2019. Alexandre Moors — who has helmed videos for Kendrick Lamar, Jennifer Lopez, ScHoolboy Q and Miguel, as well as the Jennifer Aniston-Alden Ehrenreich war drama The Yellow Birds — directed it. He and Cyrus finalized the concept as the anti-abortion “heartbeat bill” gained traction across the country.
The video is about the woman’s body — the right to own your own body and make it free from the male gaze, in any way shape and form, [...] It’s a broad message, and we’re not trying to be dogmatic. But we’re living in difficult times in America, and what I get from this video is that it injects a lot of energy and determination and the right fuel for the struggle.
It features Cyrus wearing a red latex catsuit against red lighting and aesthetics, and features shots of diverse women, including body positivity women, women of color, disabled women, transgender women, and a non-binary person using they/them pronouns, sitting and posing in different places. Cyrus' real-life mother Tish Cyrus makes a cameo appearance in the video. The video also shows nude scenes and has feminist signs flashing on-screen at random intervals. It is also sprinkled with images of breastfeeding, C-sections, menstruation pads — everything about the female body that's supposed to carry some taboo. The video is presented in 4:3 aspect ratio format.
Cyrus' red latex catsuit, calls back to the memorable outfits worn in Britney Spears' "Oops!...I Did It Again" and Lady Gaga's "Bad Romance" videos. Her version, however, has a bejeweled vagina dentata, as a notable embellishment. She also appears wearing a gold armor while atop a horse, holding a sword a la Joan of Arc. That latex look, otherwise, was not meant to be an explicit callback to the aforementioned musicians, but rather, "it was interesting to subvert some of the codes of the sexual attire, and actually transform beyond it into more of an armor or a fight suit" Moors said, "They look more like warriors than anything with sexual implications."In coordination with the video drop, Cyrus shared 18 posts on her Instagram account that featured statements from those who make cameos.
The Insider placed the music video second on their list of the best music videos of 2019, commenting that "aside from the genius styling and gorgeous direction, the real triumph of Cyrus' video is how it highlights a wide variety of people who might connect with the song's message of empowerment and freedom".


== Live performances ==
Cyrus first performed "Mother's Daughter", along with "Cattitude" and "D.R.E.A.M.", at BBC Radio 1's Big Weekend in Middlesbrough on May 25, 2019. It was also performed at Primavera Sound in Barcelona on May 31, Orange Warsaw Festival in Warsaw on June 1, Tinderbox in Odense on June 28, and at Glastonbury Festival in Pilton on June 30.


== Track listing ==
R3hab Remix – Single"Mother's Daughter" (R3hab Remix) – 2:32Wuki Remix – Single"Mother's Daughter" (Wuki Remix) – 3:08White Panda Remix – Single"Mother's Daughter" (White Panda Remix) – 2:55


== Credits and personnel ==
Credits adapted from Tidal.
Miley Cyrus – vocals, songwriter
Alma – songwriter
Andrew Wyatt – producer, songwriter
Jacob Munk – recording engineer
John Hanes – recording engineer
Tay Keith – programmer
Serban Ghenea – mixing engineer


== Awards and nominations ==


== Charts ==


== Certifications ==


== Release history ==


== References ==