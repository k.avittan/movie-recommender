Cats is a 1998 direct-to-video musical film based on the 1981 stage musical Cats by Andrew Lloyd Webber, itself based on Old Possum's Book of Practical Cats (1939) by T. S. Eliot. Lloyd Webber oversaw orchestration for the film and called on Gillian Lynne, the show's original choreographer, to train the cast for the film. David Mallet served as the director of this production.
The film is a recreation of the stage musical, but with a new staging and significant cuts made to reduce the run-time for television broadcast. It was filmed at the Adelphi Theatre in London in 1997, with the cast selected from various past and (then) present international productions of Cats. Initially released to VHS and subsequently DVD and Blu-ray, the film has since been shown on television channels including PBS, BBC, and Ovation TV.


== Synopsis ==

The musical tells the story of a tribe of cats called the Jellicles and the night they make what is known as the "Jellicle choice" and decide which cat will ascend to the Heaviside Layer and come back to a new life.


== Cast and characters ==

The cast for the film consists of former and contemporary members of various international stage productions of Cats, who were invited to reprise their stage roles. Among the cast were Elaine Paige and Susan Jane Tanner who originated the roles of Grizabella and Jellylorum in the West End respectively, and Ken Page who originated the role of Old Deuteronomy on Broadway. The exception was John Mills who was brought on to play Gus for the first time.The characters and their respective performers include:
Admetus: A tan, black, and beige with a soft, vaguely striped unitard, and a brown, white and black wig. He is portrayed by Frank Thompson who also plays the Rumpus Cat.
Alonzo: An elegant black and white tom often considered the "second-in command" after Munkustrap. He flirts a lot, but can still be serious, loyal and brave. Portrayed by Jason Gardiner.
Asparagus: A general chorus cat. (Not to be confused with Gus the Theatre Cat in the video production, as the two roles are portrayed separately due to the age of John Mills rendering him unable to perform the dancing and singing roles of Asparagus) Portrayed by Tony Timberlake.
Bombalurina – A saucy red female, confident and outgoing. Portrayed by Rosemarie Ford.
Bustopher Jones: A fat cat, Bustopher "dresses" in a smart suit and spats. As the upper class "St James' Street Cat", Bustopher spends his time at Gentlemen's clubs, socialising with London's high society. Portrayed by James Barron.
Cassandra: A sleek brown and cream female Abyssinian, with a braided tail and rolled wig, and no leg warmers. Portrayed by Rebecca Parker.
Coricopat: Male twin of Tantomile. Both of them are intuitive or even psychic, perfectly synchronised in their movements and nearly identical in appearance. Portrayed by  Tommi Sliiden.
Demeter: A shy and skittish queen. Portrayed by Aeva May.
Electra: A quiet and reserved kitten. She is a young orange and black tabby female who is close to Etcetera as well as being a fan of the Rum Tum Tugger. Portrayed by Leah Sue Morland.
Etcetera: A happy, energetic and excitable kitten who is a big fan of the Rum Tum Tugger. Portrayed by Jo Bingham.
Exotica: A dark, sleek brown and cream female, similar to Cassandra in appearance. She was a character created specially for Femi Taylor, who had been part of the original London cast as Tantomile.
Grizabella: The former Glamour Cat who has lost her sparkle and now only wants to be accepted by her former friends and family. Portrayed by original London cast member Elaine Paige.
Gus: A retired "Theatre Cat" who is now very old and ill and can no longer perform. Portrayed by John Mills.
Jellylorum: A mature female who watches out for the kittens and cares for Gus. Portrayed by original London cast member Susan Jane Tanner.
Jemima: A young, innocent and curious kitten. Portrayed by Veerle Casteleyn, whose voice was dubbed over by Helen Massey due to the producers fearing that she had a noticeable accent.
Jennyanydots: Known as the "old Gumbie cat", she sits all day and rules the mice and cockroaches by night. Portrayed by Susie McKenna.
Macavity: The show's only real villain, he goes out of his way to terrorise the Jellicles. Portrayed by Bryn Walters.
Mr. Mistoffelees/Quaxo: A young tom who has magical powers. His is a dance-only role; his signature dance move is the "Conjuring Turn", which is a string of fouettés. Portrayed by Jacob Brent.
Mungojerrie: Half of a pair of troublesome tabby cats, notorious "cat-burglars", Mungojerrie and Rumpleteazer. Portrayed by Drew Varley.
Munkustrap: A black and silver tom who is the leader, storyteller and protector of the Jellicle tribe. Portrayed by Michael Gruber.
Old Deuteronomy: The lovable patriarch of the Jellicle Tribe. He is very old and usually slow-moving. Portrayed by original Broadway cast member Ken Page.
Plato: A dancing role in the film, he performs a pas de deux with Victoria. Portrayed by Bryn Walters, who also plays Macavity.
Pouncival: A playful adolescent tom. He loves to annoy the older cats and play with the other kittens. Portrayed by Karl Morgan.
Rumpleteazer: Female half of a pair of notorious "cat-burglars", Mungojerrie and Rumpleteazer. Portrayed by Jo Gibb.
Rum Tum Tugger: The ladies' tom and Munkustrap's younger brother. His temperament ranges from clownish to serious. Portrayed by John Partridge.
Rumpus Cat: A legendary spiky haired cat with glowing red eyes; he is seen as a super hero figure amongst the Jellicles. Portrayed by Frank Thompson.
Skimbleshanks: The "railway cat". He is an active orange tabby cat who lives on a train, and acts as an unofficial chaperone of the night train to Glasgow. Portrayed by Geoffrey Garratt, but dubbed in his song by David Arneil.
Tantomile: Female twin of Coricopat. Both of them are perceived as intuitive or even psychic as they are the first among the tribe to sense the presence of strangers. Portrayed by Kaye Brown.
Tumblebrutus: A bouncy, troublesome young cat. Portrayed by Fergus Logan.
Victoria: A white kitten who is extremely gifted at dancing. Portrayed by Phyllida Crowley Smith.


== Production ==
The film was shot at the Adelphi Theatre in London over the course of 18 days in August 1997. The cast vocals were recorded first, followed by two run-throughs of the entire show that were captured from multiple angles by 16 cameras, ending with a few weeks of close-up and pick-up shots.The musical score was recorded with a 100-piece orchestra, with Simon Lee serving as the musical director and conductor. The film infuses the music from various international productions of the stage show.


=== Differences with the stage show ===
Directed for television broadcast, this production was restaged on a new set and was not filmed with an audience. Significant cuts were also made, with the film having a running time of slightly under 2 hours, compared to the 2 hours and 40 minutes running time of the stage show. The entirety of "Growltiger's Last Stand" was cut, along with parts of "The Jellicle Ball", "The Old Gumbie Cat", "Mr. Mistoffelees", "Macavity: The Mystery Cat", and "Mungojerrie and Rumpleteazer".


== Broadcast and home video ==
Cats was initially released on VHS and DVD by Polygram Video in 1998, reaching No. 6 on the Billboard Top Video Sales chart. The home video released by Universal Pictures in 2002 is certified 18x Platinum in the UK. A Blu-ray edition was released in 2013 that included additional behind-the-scenes footage.The film premiered on television on PBS' Great Performances on 2 November 1998. It has since been broadcast on other television channels including BBC and Ovation TV, and was shown on Great Performances again in November 2014.In May 2020, during the Coronavirus pandemic, Lloyd Webber announced that he would broadcast this film on YouTube with live commentary in honor of his beloved cat Mika, who was hit and killed by a car that morning.


== References ==


== External links ==
Cats on IMDb
Cats at Rotten Tomatoes
Cats: The Film playlist on YouTube