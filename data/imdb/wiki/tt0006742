Egon Schiele (German: [ˈʃiːlə] (listen); 12 June 1890 – 31 October 1918) was an Austrian painter. A protégé of Gustav Klimt, Schiele was a major figurative painter of the early 20th century. His work is noted for its intensity and its raw sexuality, and the many self-portraits the artist produced, including nude self-portraits. The twisted body shapes and the expressive line that characterize Schiele's paintings and drawings mark the artist as an early exponent of Expressionism.


== Biography ==


=== Early life ===

Schiele was born in 1890 in Tulln, Lower Austria. His father, Adolf Schiele, the station master of the Tulln station in the Austrian State Railways, was born in 1851 in Vienna to Karl Ludwig Schiele, a German from Ballenstedt and Aloisia Schimak; Egon Schiele's mother Marie, née Soukup, was born in 1861 in Český Krumlov (Krumau) to Johann Franz Soukup, a Czech father from Mirkovice, and Aloisia Poferl, a German Bohemian mother from Český Krumlov. As a child, Schiele was fascinated by trains, and would spend many hours drawing them, to the point where his father felt obliged to destroy his sketchbooks. When he was 11 years old, Schiele moved to the nearby city of Krems (and later to Klosterneuburg) to attend secondary school. To those around him, Schiele was regarded as a strange child. Shy and reserved, he did poorly at school except in athletics and drawing, and was usually in classes made up of younger pupils. He also displayed incestuous tendencies towards his younger sister Gertrude (who was known as Gerti), and his father, well aware of Egon's behaviour, was once forced to break down the door of a locked room that Egon and Gerti were in to see what they were doing (only to discover that they were developing a film). When he was sixteen he took the twelve-year-old Gerti by train to Trieste without permission and spent a night in a hotel room with her.


=== Academy of Fine Arts ===
When Schiele was 14 years old, his father died from syphilis, and he became a ward of his maternal uncle, Leopold Czihaczek, also a railway official. Although he wanted Schiele to follow in his footsteps, and was distressed at his lack of interest in academia, he recognised Schiele's talent for drawing and unenthusiastically allowed him a tutor, the artist Ludwig Karl Strauch. In 1906 Schiele applied at the Kunstgewerbeschule (School of Arts and Crafts) in Vienna, where Gustav Klimt had once studied. Within his first year there, Schiele was sent, at the insistence of several faculty members, to the more traditional Akademie der Bildenden Künste in Vienna in 1906. His main teacher at the academy was Christian Griepenkerl, a painter whose strict doctrine and ultra-conservative style frustrated and dissatisfied Schiele and his fellow students so much that he left three years later.


=== Klimt and first exhibitions ===

In 1907, Schiele sought out Gustav Klimt, who generously mentored younger artists. Klimt took a particular interest in the young Schiele, buying his drawings, offering to exchange them for some of his own, arranging models for him and introducing him to potential patrons. He also introduced Schiele to the Wiener Werkstätte, the arts and crafts workshop connected with the Secession. Schiele's earliest works between 1907 and 1909 contain strong similarities with those of Klimt, as well as influences from Art Nouveau. In 1908 Schiele had his first exhibition, in Klosterneuburg. Schiele left the Academy in 1909, after completing his third year, and founded the Neukunstgruppe ("New Art Group") with other dissatisfied students. In his early years, Schiele was strongly influenced by Klimt and Kokoschka. Although imitations of their styles, particularly with the former, are noticeably visible in Schiele's first works, he soon evolved his own distinctive style.

Klimt invited Schiele to exhibit some of his work at the 1909 Vienna Kunstschau, where he encountered the work of Edvard Munch, Jan Toorop, and Vincent van Gogh among others. Once free of the constraints of the Academy's conventions, Schiele began to explore not only the human form, but also human sexuality. Schiele's work was already daring, but it went a bold step further with the inclusion of Klimt's decorative eroticism and with what some may like to call figurative distortions, that included elongations, deformities, and sexual openness. Schiele's self-portraits helped re-establish the energy of both genres with their unique level of emotional and sexual honesty and use of figural distortion in place of conventional ideals of beauty. He also painted tributes to Van Gogh's Sunflowers as well as landscapes and still lifes.In 1910, Schiele began experimenting with nudes and within a year a definitive style featuring emaciated, sickly-coloured figures, often with strong sexual overtones. Schiele also began painting and drawing children. Schiele's self portrait, Kneeling Nude with Raised Hands (1910), is considered among the most significant nude art pieces made during the 20th century. Schiele's radical and developed approach towards the naked human form challenged both scholars and progressives alike. This unconventional piece and style went against strict academia and created a sexual uproar with its contorted lines and heavy display of figurative expression. At the time, many found the explicitness of his works disturbing.

From then on, Schiele participated in numerous group exhibitions, including those of the Neukunstgruppe in Prague in 1910 and Budapest in 1912; the Sonderbund, Cologne, in 1912; and several Secessionist shows in Munich, beginning in 1911. In 1911, Schiele met the seventeen-year-old Walburga (Wally) Neuzil, who lived with him in Vienna and served as a model for some of his most striking paintings. Very little is known of her, except that she had previously modelled for Gustav Klimt and might have been one of his mistresses. Schiele and Wally wanted to escape what they perceived as the claustrophobic Viennese milieu, and went to the small town of Český Krumlov (Krumau) in southern Bohemia. Krumau was the birthplace of Schiele's mother; today it is the site of a museum dedicated to Schiele. Despite Schiele's family connections in Krumau, he and his lover were driven out of the town by the residents, who strongly disapproved of their lifestyle, including his alleged employment of the town's teenage girls as models. Progressively, Schiele's work grew more complex and thematic, and he eventually would begin dealing with themes such as death and rebirth.


=== Neulengbach and imprisonment ===

Together the couple moved to Neulengbach, 35 km (22 mi) west of Vienna, seeking inspirational surroundings and an inexpensive studio in which to work. As it was in the capital, Schiele's studio became a gathering place for Neulengbach's delinquent children. Schiele's way of life aroused much animosity among the town's inhabitants, and in April 1912 he was arrested for seducing a young girl of 13, below the 14-year-old age of consent.When the police came to his studio to place Schiele under arrest, they seized more than a hundred drawings which they considered pornographic. Schiele was imprisoned while awaiting his trial. When his case was brought before a judge, the charges of seduction and abduction were dropped, but the artist was found guilty of exhibiting erotic drawings in a place accessible to children. In court, the judge burned one of the offending drawings over a candle flame. The twenty-one days he had already spent in custody were taken into account, and he was sentenced to a further three days' imprisonment. While in prison, Schiele created a series of 12 paintings depicting the difficulties and discomfort of being locked in a jail cell.

In 1913, the Galerie Hans Goltz, Munich, mounted Schiele's first solo show. A solo exhibition of his work took place in Paris in 1914.

In 1914, Schiele glimpsed the sisters Edith and Adéle Harms, who lived with their parents across the street from his studio in the Viennese district of Hietzing, 101 Hietzinger Hauptstraße. They were a middle-class family and Protestant by faith; their father was a master locksmith. In 1915, Schiele chose to marry the more socially acceptable Edith, but had apparently expected to maintain a relationship with Wally. However, when he explained the situation to Wally, she left him immediately and never saw him again. This abandonment led him to paint Death and the Maiden, where Wally's portrait is based on a previous pairing, but Schiele's is newly struck. (In February 1915, Schiele wrote a note to his friend Arthur Roessler stating: "I intend to get married, advantageously. Not to Wally.") Despite some opposition from the Harms family, Schiele and Edith were married on 17 June 1915, the anniversary of the wedding of Schiele's parents.


=== WWI to death ===

Although Schiele avoided conscription for almost a year, World War I now began to shape his life and work. Three days after his wedding, Schiele was ordered to report for active service in the army where he was initially stationed in Prague. Edith came with him and stayed in a hotel in the city, while Egon lived in an exhibition hall with his fellow conscripts. They were allowed by Schiele's commanding officer to see each other occasionally.
During the war, Schiele's paintings became larger and more detailed. His military service, however, gave him limited time, and much of his output consisted of linear drawings of scenery and military officers. Around this time, Schiele also began experimenting with the themes of motherhood and family. His wife Edith was the model for most of his female figures, but during the war (due to circumstance) many of his sitters were male. Since 1915, Schiele's female nudes became fuller in figure, but many were deliberately illustrated with a lifeless doll-like appearance.
Despite his military service, Schiele was still exhibiting in Berlin. He also had successful shows in Zürich, Prague, and Dresden. His first duties consisted of guarding and escorting Russian prisoners. Because of his weak heart and his excellent handwriting, Schiele was eventually given a job as a clerk in a POW camp near the town of Mühling. There, he was allowed to draw and paint imprisoned Russian officers; his commander, Karl Moser (who assumed that Schiele was a painter and decorator when he first met him), even gave him a disused store room to use as a studio. Since Schiele was in charge of the food stores in the camp, he and Edith could enjoy food beyond rations.By 1917, he was back in Vienna and able to focus on his artistic career. His output was prolific, and his work reflected the maturity of an artist in full command of his talents. He was invited to participate in the Secession's 49th exhibition, held in Vienna in 1918. Schiele had fifty works accepted for this exhibition, and they were displayed in the main hall. He also designed a poster for the exhibition; it was reminiscent of the Last Supper, with a portrait of himself in the place of Christ. The show was a triumphant success. As a result, prices for Schiele's drawings increased and he received many portrait commissions.
In the autumn of 1918, the Spanish flu pandemic that claimed more than 20,000,000 lives in Europe reached Vienna. Edith, who was six months pregnant, succumbed to the disease on 28 October. Schiele died only three days after his wife. He was 28 years old. During the three days between their deaths, Schiele drew a few sketches of Edith.


== Style ==
Some critics such as Jane Kallir have commented upon Schiele's work as being grotesque, erotic, pornographic, or disturbing, focusing on sex, death, and discovery. He focused on portraits of others as well as himself. In his later years, while he still worked often with nudes, they were done in a more realist fashion.


== Legacy ==

Schiele was the subject of the biographical film, Excess and Punishment (aka Egon Schiele – Exzess und Bestrafung), a 1980 film originating in Germany with a European cast that explores Schiele's artistic demons leading up to his early death. The film was directed by Herbert Vesely and stars Mathieu Carrière as Schiele, Jane Birkin as his early artistic muse Wally Neuzil, Christine Kaufman as his wife, Edith Harms, and Kristina Van Eyck as her sister, Adele Harms. Also in 1980, the Arts Council of Great Britain produced a documentary film, Schiele in Prison, which looked at the circumstances of Schiele's imprisonment and the veracity of his diary.
In 2016 another biographical film was released, Egon Schiele: Death and the Maiden (German: Egon Schiele: Tod und Mädchen).
Joanna Scott's 1990 novel Arrogance was based on Schiele's life and makes him the main figure. His life was also depicted in a theatrical dance production by Stephan Mazurek called Egon Schiele, presented in May 1995, for which Rachel's, an American post-rock group, composed a score titled Music for Egon Schiele. For The Featherstonehaughs contemporary dance company, Lea Anderson choreographed The Featherstonehaughs Draw On The Sketchbooks Of Egon Schiele in 1997.Schiele's life and work have also been the subject of essays, including a discussion of his works by fashion photographer Richard Avedon in an essay on portraiture entitled "Borrowed Dogs." Mario Vargas Llosa uses the work of Schiele as a conduit to seduce and morally exploit a main character in his 1997 novel The Notebooks of Don Rigoberto.Wes Anderson's film The Grand Budapest Hotel features a painting by Rich Pellegrino that is modeled after Schiele's style which, as part of a theft, replaces a so-called Flemish/Renaissance masterpiece, but is then destroyed by the angry owner when he discovers the deception.Julia Jordan based her 1999 play Tatjana in Color, which was produced off-Broadway at The Culture Project during the fall of 2003, on a fictionalization of the relationship between Shiele and the 12-year-old Tatjana von Mossig, the Neulengbach girl whose morals he was ultimately convicted of corrupting for allowing her to see his paintings. The opening chapters of Guy Mankowski's 2017 novel An Honest Deceit were cited to be heavily influenced by Schiele's paintings; in particular his portrayals of his sister, Gertrude.


=== Art collections ===
The Leopold Museum, Vienna houses perhaps Schiele's most important and complete collection of work, featuring over 200 exhibits. The museum sold one of these, Houses With Colorful Laundry (Suburb II), for $40.1 million at Sotheby's in 2011. Other notable collections of Schiele's art include the Egon Schiele-Museum, Tulln, the Österreichische Galerie Belvedere, and the Albertina Graphic Collection, both in Vienna. Viktor Fogarassy collected works by Schiele, including Dämmernde Stadt.


=== Auction history ===

Portrait of Wally, a 1912 portrait, was purchased by Rudolf Leopold in 1954 and became part of the collection of the Leopold Museum when it was established by the Austrian government, purchasing more than 5,000 pieces that Leopold had owned. After a 1997–1998 exhibit of Schiele's work at the Museum of Modern Art in New York City, the painting was seized by order of the New York County District Attorney and had been tied up in litigation by heirs of its former owner who claim that the painting was Nazi plunder and should be returned to them.The dispute was settled on 20 July 2010 and the picture subsequently purchased by the Leopold Museum for US$19 million. In 2013, the museum sold three drawings by Schiele for £14 million at Sotheby's London in order to settle the restitution claim over its 1914 Schiele painting Houses by the Sea. The most expensive, Liebespaar (Selbstdarstellung mit Wally) (1914/15), or Two lovers (Self Portrait With Wally), raised the world auction record for a work on paper by the artist to £7.88 million.On 21 June 2013 Auctionata in Berlin sold a watercolor from 1916, Reclining Woman, at an online auction for €1.827 million (US$2.418 million). This is a world record for the most expensive work of art ever sold at an online auction.


== Self-portraits ==

		
		
		
		
		
		
		


== Figurative works ==

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		


== Landscapes ==

		
		
		
		
		
		
		


== Notes ==


== References ==
Egon Schiele: The Complete Works Catalogue Raisonné of all paintings and drawings by Jane Kallir, 1990, Harry N. Abrams, New York, ISBN 0-8109-3802-2.
Egon Schiele: The Egoist (Egon Schiele : Narcisse écorché, collection « Découvertes Gallimard●Arts » [nº 475]) by Jean-Louis Gaillemin; translated from the French by Liz Nash, "Abrams Discoveries" series & 'New Horizons' series, 2006 (U.S. edition, Harry N. Abrams) / 2007 (UK edition, Thames & Hudson), ISBN 978-0-500-30121-0 & ISBN 0-500-30121-2.


== Further reading ==
David Williams (26 July 2019). "A man found a Egon Schiele drawing in a New York thrift store, and it could be worth a fortune". CNN.


== External links ==
Works by or about Egon Schiele at Internet Archive
"Egon Schiele’s birthplace, Tulln"
"Egon Schiele Museum, Tulln"
"Leopold Museum, Vienna", Leopold Museum, Vienna, houses the largest collection of Schiele's work.
"Oesterreichische Galerie, Belvedere" The Oesterreichische Galerie, Belvedere, in Vienna contains one of the greatest collections of Schiele's work.
"Live Flesh" A review of Schiele's work by Arthur Danto in The Nation.
Neue Galerie for German and Austrian Art (New York)
Self-portraits by Schiele
Tracey Emin & Egon Schiele - exhibition at the Leopold Museum Vienna Interview with the co-curator Diethard Leopold