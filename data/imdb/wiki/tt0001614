The Delhi Capitals are  a franchise cricket team that represents the city of Delhi in the Indian Premier League (IPL). Founded in 2008 as the Delhi Daredevils, the franchise is jointly owned by the GMR Group and the JSW Group. The team's home ground is Arun Jaitley Stadium, located in New Delhi.
The Delhi Capitals are the only current team to have never appeared in an IPL final and qualified for the IPL playoffs in 2019 for the first time in seven years. The leading run-scorer for the Capitals is Virender Sehwag, while the leading wicket-taker is Amit Mishra.


== Franchise history ==
The IPL is a cricket league organised by the Board of Control for Cricket in India (BCCI) and backed by the International Cricket Council (ICC). The inaugural tournament was held in April–June 2008, in which BCCI finalised a list of eight teams who participated in the tournament. The teams represented eight different cities in India, including Delhi. The teams were put up for auction in Mumbai on 20 February 2008, and the Delhi team was bought by the property development company GMR Group for US$84 million.In March 2018, GMR sold a 50% stake in the Delhi Daredevils to JSW Sports for ₹550 crore (US$77 million).In December 2018, the team changed its name from the Delhi Daredevils to the Delhi Capitals. Speaking to the rationale behind changing the team's name, co-owner and chairman Parth Jindal said, "Delhi is the power centre of the country, it is the capital, therefore the name Delhi Capitals." Co-owner Kiran Kumar Grandhi said, "The new name symbolizes Delhi’s identity and just like the city, we are aiming to be the centre of all action going forward."


== Team history ==


=== 2008 IPL season ===

During the inaugural edition of the IPL, the Delhi Daredevils won their initial two matches against the Rajasthan Royals and the Deccan Chargers. Then, after losing a match to the Kings XI Punjab, the team won their next two matches against the Royal Challengers Bangalore and the Chennai Super Kings.
However, the Delhi Daredevils lost in four straight matches, breaking the streak with a win against the Deccan Chargers. After another loss against the Kings XI Punjab, they recovered and won three matches to finish in the final four of the league.
The Delhi Daredevils lost to eventual champions, the Rajasthan Royals, in the semi-final.


=== 2009 IPL season ===

The Daredevils dominated for much of the 2009 IPL season under the captaincy of former Indian opener and Delhi-native, Gautam Gambhir, finishing at the top of the table. The likes of former Indian opener and triple-Centurion Sehwag and Gambhir had both developed since the prior season. They set up many large totals for the Daredevils, with South African power hitter and part-time Keeper AB de Villiers hitting one of only two centuries in the 2009 IPL (the other was made by the young Indian batsman Manish Pandey) at a match in South Africa. The bowling team, which was composed mainly of New Zealander spin pro Daniel Vettori, India senior spinner Amit Mishra, Pradeep Sangwan, and former India fast bowler Ashish Nehra, was bolstered by the off-season signing of Australian Dirk Nannes, which created a strong bowling line-up. The batting of the Daredevils featured Sehwag, Gambhir, power hitter Dilshan, AB de Villiers, and Manoj Tiwari. The Daredevils consisted of new and then uncapped players like Australia Captain Aaron Finch and Australia all-rounder Glenn Maxwell, who went on to become successful in T20 circuits.
However, despite finishing at the top of the points table during the regular season, the Daredevils crumbled in the semi-final after Deccan Chargers' captain Adam Gilchrist hit the then fastest fifty in IPL history (in just 17 balls). Nannes in particular, who was again picked over Aussie pacer Glenn McGrath, was punished by Gilchrist, and later by Aussie opening all-rounder Andrew Symonds. The semi-final loss meant Delhi did not contest the final of the 2009 IPL season, despite having the best record in the league stages. However, Delhi managed to qualify for the now-defunct 2009 Champions League Twenty20 as a result of their performance in the group stage.


=== 2012 IPL season ===

Due to the disbanding of the Kochi Tuskers Kerala, each team played the remaining eight teams twice, once at home and once away. Therefore, each team played an extended season of 16 matches.
In the 2012 IPL season a new side of the Delhi Daredevils was seen after a poor 2011 season where they finished last. They came back strongly, having purchased players such as Afro-English batting great Kevin Pietersen, Sri Lanka Batsman Mahela Jayawardene, and Kiwi Batsman Ross Taylor. They stood first in the Pool Table with the Kolkata Knight Riders at second place, the Mumbai Indians coming third, and the defending champions the Chennai Super Kings securing the fourth spot, consequently also qualifying for the Champions League Twenty20 in 2012 held in South Africa.
Australian pace bowler Glenn McGrath expressed disappointment over not being picked to play any match during the whole season, and stopped playing for Delhi.Former Sri Lankan Skipper Mahela Jayawardane was appointed as the new captain of the Delhi Daredevils after Indian opener Virender Sehwag stepped down from captaincy during the 2012 season of the Champions League Twenty20.


=== 2013 IPL season ===

Delhi Daredevils lost their first six matches of IPL 2013 and won their first match in their seventh game against the Mumbai Indians. Virender Sehwag and Mahela Jayawardene's partnership led to a win against the Mumbai Indians, in which they scored 161 runs. Sehwag was the "Man of the Match" for his innings of 95* off 57 balls. After this match, they faced the Kings XI Punjab and lost again. But in their ninth match of the season against the Pune Warriors India they won, putting them in eighth place in the points table. They defeated defending champions the Kolkata Knight Riders in their tenth Match by seven wickets but failed to advance to seventh place due to their net run-rate. In their next match against the Sunrisers Hyderabad, the Delhi Daredevils were bowled out for just 80 runs, giving the Sunrisers a win. In their match against the Rajasthan Royals, they scored 154 runs, thanks to recruit Ben Rohrer's half-century. However, that effort was in vain as the Rajasthan Royals chased the total down with the loss of only one wicket. This loss for the Daredevils officially eliminated them from the 2013 season.
In their next match against the Royal Challengers Bangalore, Delhi managed to contain the Royal Challengers who were 106 in 16.0 overs, but Bangalore scored 77 in their last four overs and reached a total of 183, thanks to a 99 by RCB Skipper Virat Kohli. The Daredevils lost the match by just four runs. The team next played the Chennai Super Kings who batted first and posted 168 runs. Delhi failed to gain momentum throughout the match and eventually lost by 33 runs. The following match was against the Kings XI Punjab who defeated them again, this time by seven runs, as they failed to chase down 172 runs. Their final game was against the Pune Warriors India. Delhi bowled first and the Pune Warriors posted a total of 171 runs. In the second innings, Delhi maintained the required run rate but began losing quick wickets after the tenth over. They could not chase the target and lost by 38 runs finishing last in the league table. Despite seeing a forgettable season, a few big names were added to the support staff as the season progressed. The legendary West Indies batsman Sir Vivian Richards was named as their new brand ambassador, former England spinner Jeremy Snape was added to the support staff, and renowned former Pakistan spinner Mushtaq Ahmed was named as their new spin bowling coach.


=== 2014 IPL season ===

Ahead of the IPL 2014 auction, on 10 January 2014, the Delhi Daredevils announced that they would not retain any players from their current squad for season seven. With no players retained, the team had the most "right-to-match" cards among all the franchises at the auction—three. They also had Rs 600 million (approx US$9.6 million) to spend at the auction.
The Delhi Daredevils experienced another poor season in 2014. They lost their first match, against the Royal Challengers Bangalore, however, they won their next match against the Kolkata Knight Riders. This was followed by a loss against the two-time champions the Chennai Super Kings and a close match which resulted in a loss against the Sunrisers Hyderabad. The Delhi Daredevils won their next match against the Mumbai Indians, having restricted them to 125. Later, when the tournament shifted from the makeshift venue in the United Arab Emirates (UAE) to India, Delhi lost their next nine matches. The Delhi Daredevils won only two out of their fourteen matches, both of which took place in the adopted venue in the UAE. Despite their poor performance, South Africa all-rounder JP Duminy, the team captain scored 410 runs from fourteen matches at an average of 51.25 and was the tournament's eighth highest run-scorer. The Delhi Daredevils once again finished last.


=== 2015 IPL season ===

The team finished seventh in the 2015 edition of the IPL. They received fierce criticism from the Delhi fans due to their string of poor performances in the previous two years.


=== 2016 IPL season ===

The Delhi Daredevils released many of their players, including the previous year's most expensive purchase, all-rounder Yuvraj Singh, whom they had bought for ₹16 crores. The Daredevils also released former Sri Lankan skipper and all-rounder Angelo Mathews, who was bought for ₹7.5 crore. New additions to the team included uncapped all-rounder Pawan Negi, who was bought for ₹8.5 crore, thereby becoming the most expensive Indian player in the IPL auction of 2016. Uncapped Indian batsman, Sanju Samson and Karun Nair, who previously played for the Rajasthan Royals, were also bought for hefty amounts. South African all-rounder Chris Morris was bought for ₹7 crore. English keeper Sam Billings and Australian Joel Paris also joined the Delhi squad. The team purchased three promising India U-19 players—local keeper Rishabh Pant, Rajasthan's pacer Khaleel Ahmed and Mahipal Lomror. After the Daredevils ended their association with South African batsman Gary Kirsten, they appointed Paddy Upton as their head coach. The Indian batsman and head coach of India U-19, Rahul Dravid was appointed the Daredevils' batting mentor. Former Indian pacer Zaheer Khan was appointed as the new captain of the Delhi Daredevils in the 2016 IPL season. Relative to their performance in the previous three seasons, the Delhi Daredevils improved the way they played. South African all-rounder Chris Morris got the fastest 50 (17 balls) of the tournament and was also effective in the bowling department. South African wicket-keeper Quinton de Kock was among the leading run-scorers in the season. After losing their first match against two-time champions the Kolkata Knight Riders, the Daredevils went on to dominate the Kings XI Punjab, the Royal Challengers Bangalore, defending champions the Mumbai Indians, the Kolkata Knight Riders in the second leg, and newcomers the Gujarat Lions. They won five of their first seven matches, gaining ten points. They lost their second match against newcomers the Gujarat Lions by just one run. The Daredevils were the favourites to qualify for the playoffs; however, they finished in sixth with fourteen points in fourteen games.


=== 2017 IPL season ===

Delhi lost South Africans de Kock and Duminy before the tournament, placing dependence on a young batting line-up and a bowling line-up of former Indian pacer Zaheer Khan, Indian pacer Mohammed Shami, South African all-rounder Chris Morris, Aussie bowling all-rounder Pat Cummins, South African young pacer Kagiso Rabada, Indian spinner Amit Mishra, young spinner Shahbaz Nadeem, Jayant Yadav and Ben Hilfenhaus. They lost to runners-up RCB in the first game and won by big margins against RPS and KXIP. After this, they lost five consecutive games. However, they bounced back by chasing 189 and 214 against defending champions SRH and GL respectively. Keralite wicket-keeper Sanju Samson got the first century of the season. Young wicket-keeper and local boy Rishabh Pant made 97 against GL. But the Delhi Daredevils lost to then two-time champions MI by a margin of 146 runs, which was the highest win by runs in IPL history. During the middle of the tournament, captain and former Indian pacer Zaheer Khan was down with an injury to his hamstring which caused him to miss three matches, and young Indian batsman Karun Nair was appointed as stand-in captain.
The Delhi Daredevils ended up at sixth position again with six wins (+12 points) and eight losses for the season.


=== 2018 IPL season ===
Going into the big auctions, each franchise were allowed to retain up to 3 players. Additionally, they could also use Right to Match cards to get back 2 players during the auctions. Delhi Daredevils retained Shreyas Iyer, Chris Morris and Rishabh Pant. Coach Rahul Dravid had to quit his job as the coach in order to protect his position as coach of India A and India U-19, following a conflict of interest debate. Ricky Ponting was appointed as the new coach.


=== 2019 IPL season ===

The Delhi Capitals retained fourteen players and traded their former player and India opener Shikhar Dhawan from Sunrisers Hyderabad. This was done by trading off the young all-rounder Vijay Shankar, and Abhishek Sharma and spinner Shahbaz Nadeem for the 12th season of the IPL. Retained players for the twelfth IPL season were: captain and Indian batsman Shreyas Iyer, Indian wicket-keeper and left-handed batsman Rishabh Pant, young Indian batsman Prithvi Shaw, Indian spinner Amit Mishra, young pacer Avesh Khan, uncapped bowler and injured Harshal Patel, uncapped bowling all-rounder Rahul Tewatia, Jayant Yadav, Manjot Kalra, New Zealand opener Colin Munro, New Zealand pacer Trent Boult, South African all-rounder Chris Morris, young South African pacer Kagiso Rabada and young Nepalese spinner Sandeep Lamichhane.
On the IPL auction day, 18 December 2018, DC filled up their 10 available player slots (seven Indian slots and three overseas slots) with: South African batsman Colin Ingram, Indian all-rounder Axar Patel, Indian all-rounder Hanuma Vihari, Sherfane Rutherford, Indian pacer Ishant Sharma, West Indian pacer Keemo Paul, and uncapped Indian cricketers Jalaj Saxena, Ankush Bains, Nathu Singh and Bandaru Ayyappa.
Delhi Capitals also brought the former Indian captain Sourav Ganguly as their official advisor and later traded Jayant Yadav to the Mumbai Indians after the IPL 2019 auction.
The Capitals began their campaign with a 37 run victory over three-time champions Mumbai. The Capitals entered the playoffs after seven years, and they won their first playoffs match against the Sunrisers Hyderabad by two wickets. They lost the second match against the Chennai Super Kings by six wickets and ended up as second runners up in the playoffs, their best-ever finish.


=== 2020 IPL season ===

Delhi Capitals released Hanuma Vihari, Jalaj Saxena, Manjot Kalra, Ankush Bains, Nathu Singh, Bandaru Ayappa, Chris Morris, Colin Ingram, and Colin Munro from their 2019 roster. They have added Jason Roy, Chris Woakes, Alex Carey, Shimron Hetmyer, Mohit Sharma, Tushar Deshpande, Marcus Stoinis, and Lalit Yadav for their 2020 roster during the IPL Auction. Chris Woakes, however pulled out of the tournament and was replaced by South African fast bowler Anrich Nortje. Jason Roy, became the second player (and second English Player) from Capitals to pull out of the tournament due to injury concerns and personal reasons, he was replaced by Australian bowling all-rounder Daniel Sams.


== Home ground ==
The Delhi Capitals play their home matches in the Arun Jaitley Stadium located in New Delhi. They also have the modern Shaheed Veer Narayan Singh International Stadium, Raipur as their second home ground.


== Team anthems ==
Bollywood playback singer Kailash Kher was the artist for the team's anthem—"Khelo Front Foot Pe" ("Play on the front foot") or "play aggressively".
The Delhi Daredevils launched their new anthem "Munday Dilli Ke" ("The Lads from Delhi") on 5 March 2012 on YouTube.Their anthem for the 2016 season of IPL, titled "Dhuandaar Dilli" was released on YouTube, sung by Sukhwinder Singh. In 2018, they released yet another theme song, titled "Dil Dilli hai, ab Dhadkega". The theme song for the 2019 season for the rechristened franchise, the Delhi Capitals was "Roar Machaa".


== Players ==

Former India Opener and Delhi local, Virender Sehwag was accorded the icon player status in the Delhi Daredevils team and was also the captain of the side during the first two seasons. However, he resigned and passed on the leadership to Gautam Gambhir for the 2010 season. But after Gambhir left the team for the Kolkata Knight Riders in the fourth edition, Sehwag was once again given the duty to captain the team. Since the start of the IPL in 2008, many international players such as Australian Glenn McGrath, South African batsman and part-time keeper AB de Villiers, Sri Lanka player Tillakaratne Dilshan, Australia opener and former vice-captain David Warner, Andrew McDonald, New Zealand spinner Daniel Vettori, Farveez Maharoof, Dirk Nannes and Aussie opener Aaron Finch have donned the cap for the Daredevils. The team included Indian players like Tamil Nadu Middle-order batsman and keeper Dinesh Karthik and Yo Mahesh.
In 2009, Mohammad Asif and Shoaib Malik left due to the ban on Pakistani players and Asif's positive drug test. Aussie opener David Warner, Andrew McDonald, English Opener Paul Collingwood and Owais Shah were the new signings. Fast bowler Ashish Nehra came in from Mumbai Indians as a trade-off for Indian Opener Shikhar Dhawan. For IPL 2010, Australian all-rounder Moises Henriques came in from the Kolkata Knight Riders in exchange for Manoj Tiwary and Owais Shah. Wayne Parnell was purchased at the auction for US$610,000.
In 2012, they bought Sri Lankan Mahela Jayawardene, Jamaican all-rounder Andre Russell, Doug Bracewell, Morne Morkel and English batsman Kevin Pietersen. The Daredevils also signed uncapped Indian all-rounder Pawan Negi, batsmen Manpreet Juneja and Kuldeep Rawal ahead of the 2012 season. The Daredevils appointed Mahela Jayawardene as vice-captain for 2012. On 29 February 2012, the Daredevils signed Kiwi Batsman Ross Taylor from the inaugural Champions Rajasthan Royals in a trade for an undisclosed amount.


== Seasons ==
The Daredevils played in the Champions League Twenty20 twice, in 2009 and 2012. While they were eliminated in the group stage in the former, they made the semifinals in the latter.


== Current squad ==
Players with international caps are listed in bold.
 *  denotes a player who is currently unavailable for selection.
 *  denotes a player who is unavailable for rest of the season.


== Administration and support staff ==


== Kit manufacturers and sponsors ==


== Statistics ==


=== Win–loss record ===
 Last updated: 25 July 2020


=== Head to head in IPL ===
 Last updated: 25 July 2020


=== Overall results in CLT20 ===


=== Head to head in CLT20 ===


== References ==


== External links ==
IPL team Delhi capitals web page on official IPL T20 website - IPLT20.com
The Official Delhi Capitals Site