Johanna Enlists is a 1918 silent film comedy-drama produced by and starring Mary Pickford with distributed by Paramount Pictures. The film was directed by William Desmond Taylor from a short story by Rupert Hughes, The Mobilization of Johanna. Frances Marion, a frequent Pickford collaborator, wrote the scenario. The film was made at a time during World War I when sentimental or patriotic films were immensely popular. It was an early starring vehicle for Monte Blue, the male lead opposite Pickford. The film survives in several prints, including one at the Library of Congress.


== Plot ==
As described in a film magazine, Johanna Renssaller (Pickford), an uncouth, freckled country lass, works from dawn until late at night. Her only love affairs were with the hired man and a "beautiful brakeman" on the railroad. The hired man proved to be married and the brakeman proved impossible. She prayed for a beau, and then a whole regiment of soldiers came along and camped on the farm. Everyone from Captain Archie van Renssaller (MacLean) down to Private Vibbard (Blue) fell in love with her, ate her pies, and sat in her hammock. She took milk baths and tried Isadora Duncan style calisthenics and finally fell in love with Captain van Renssaller. When the troops moved on, she rode at the head of the officer staff.


== Cast ==
Mary Pickford as Johanna Renssaller
Anne Schaefer as Ma Renssaller
Fred Huntley as Pa Renssaller
Monte Blue as Private Vibbard
Douglas MacLean as Captain Archie van Renssaller
Emory Johnson as Lt. Frank Le Roy


== Reception ==
Like many American films of the time, Johanna Enlists was subject to cuts by city and state film censorship boards. For example, the Chicago Board of Censors required a cut, in Reel 4, of views of a nude figure in a book.


== References ==


== External links ==
Johanna Enlists on IMDb
Johanna Enlists at AllMovie
Lantern slide
Johanna Enlists is available for free download at the Internet Archive