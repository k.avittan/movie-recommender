The New York Yankees are a professional baseball team based in New York City, New York in the borough of The Bronx. The New York Yankees are members of the American League (AL) East Division in Major League Baseball (MLB). The Yankees have won the World Series 27 times, more than any other MLB team. In baseball, the head coach of a team is called the manager, or more formally, the field manager. The duties of the team manager include team strategy and leadership on and off the field. Since starting to play as the Baltimore Orioles (no relationship to the current Baltimore Orioles team) in 1901, the team has employed 35 managers. The current manager is Aaron Boone, the current general manager is Brian Cashman and the current owner is Hal Steinbrenner, the son of George Steinbrenner, who first bought the Yankees in 1973.The franchise's first manager was Hall of Famer John McGraw, who managed the team for one year and part of a second before becoming manager of the New York Giants. In 1903, the team moved from Baltimore to New York, where it was initially known as the New York Highlanders. Its first manager in New York was Clark Griffith, who managed the team from 1903 to 1908. Miller Huggins was the next manager to manage the team for more than three seasons. Huggins took over the managerial duties in 1918 and led the Yankees to six American League championships and three World Series titles until his death in 1929. Huggins won 1,067 regular season games with the Yankees, which ranks fourth all-time among Yankee managers.Several other managers spent long tenures with the Yankees. Joe McCarthy managed the Yankees from 1931 until midway through the 1946 season. During his tenure, the Yankees won eight American League titles and won the World Series seven times. He won 1,460 regular season games with the Yankees and lost 867, both more than any other Yankee manager. Casey Stengel managed the team from 1949 until 1960, winning 10 American League championships, 7 World Series titles, and 1,149 games, which ranks third among Yankee managers. After Stengel was discharged, Ralph Houk managed the Yankees from 1961 through 1963, winning American League titles each season, and winning the World Series twice. He served a second term as Yankee manager from 1966 through 1973.From 1974 until 1995, no Yankee managerial term lasted as long as three complete seasons. Joe Torre managed the Yankees from 1996 through 2007 and the team made the playoffs each season. He also won six American League championships and four World Series titles. His 1,173 regular season wins are second all-time among Yankees managers. He also has the most playoff appearances, playoff wins and playoff losses of any Yankee manager. Torre was named American League Manager of the Year twice, in 1996 and 1998. His predecessor, Buck Showalter, also was named Manager of the Year in 1994. Torre left after the 2007 season and was replaced by Joe Girardi, who managed the Yankees from 2008 to 2017 winning one American League championship and one World Series title.Several managers have had multiple tenures with the Yankees. Billy Martin served five terms as Yankee manager. Before his death in 1989, Martin was rumored to be in line for a sixth term if the Yankees started the 1990 season poorly. Yogi Berra, Houk, Bob Lemon, Gene Michael, Lou Piniella and Dick Howser each served two terms as the Yankees' manager. Howser's first term lasted only a single game, as interim manager in 1978 between Martin's firing and Lemon's hiring. Howser also managed a full season in 1980, leading the team to the playoffs, but was fired after the Yankees failed to advance to the World Series. Howser has the highest career winning percentage among all Yankee managers at .632.


== Key ==


== Managers ==
Statistics current as of 2020
		
		
		
		
		
		
		


== Managers with multiple tenures ==


== See also ==


== Notes ==
a A running total of the number of managers of the Yankees. Thus, any manager who has two or more separate terms as a manager is only counted once.
b Three managers, Billy Martin, Dick Howser and Bob Lemon, managed the Yankees during their World Series Championship season of 1978. Lemon managed the Yankees in the playoffs, and thus is credited with the playoff appearance, league championship and World Series championship for that season.
c Two managers, Gene Michael and Bob Lemon, managed the Yankees during their league championship season of 1981. Lemon managed the Yankees in the playoffs, and thus is credited with the playoff appearance and league championship for that season.
d Buck Showalter won the Manager of the Year Award in 1994.
e Joe Torre won the Manager of the Year Award in 1996 and 1998.


== References ==
General"List of Hall of Famers". National Baseball Hall of Fame and Museum. Archived from the original on December 29, 2008. Retrieved 2008-12-31.Specific