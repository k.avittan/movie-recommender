Hadrian's Wall (Latin: Vallum Aelium), also known as the Roman Wall, Picts' Wall, or Vallum Hadriani in Latin, is a former defensive fortification of the Roman province of Britannia, begun in AD 122 in the reign of the emperor Hadrian. It ran from the banks of the River Tyne near the North Sea to the Solway Firth on the Irish Sea, and allowed the Roman Empire to project power some distance to the north, into the lands of the northern Ancient Britons, including the Picts.
It had a stone base and a stone wall. There were milecastles with two turrets in between. There was a fort about every five Roman miles. From north to south, the wall comprised a ditch, wall, military way and vallum, another ditch with adjoining mounds. It is thought the milecastles were staffed with static garrisons, whereas the forts had fighting garrisons of infantry and cavalry. In addition to the wall's defensive military role, its gates may have been customs posts.A significant portion of the wall still stands and can be followed on foot along the adjoining Hadrian's Wall Path. The largest Roman archaeological feature in Britain, it runs a total of 73 miles (117.5 kilometres)  in northern England. Regarded as a British cultural icon, Hadrian's Wall is one of Britain's major ancient tourist attractions. It was designated as a UNESCO World Heritage Site in 1987. In comparison, the Antonine Wall, thought by some to be based on Hadrian's wall (the Gillam hypothesis), was not declared a World Heritage site until 2008.Hadrian’s Wall marked the boundary between Roman Britannia and unconquered Caledonia to the north. The wall lies entirely within England and has never formed the Anglo-Scottish border. While it is less than 0.6 mi (1.0 km) south of the border with Scotland in the west at Bowness-on-Solway, in the east at Wallsend it is as much as 68 miles (109 km) away.


== Dimensions ==

Hadrian's Wall was 80 Roman miles or 117.5 km (73.0 mi) long; its width and height varied according to the construction materials available nearby. East of the River Irthing, the wall was made from squared stone and measured 3 metres (10 feet) wide and 5 to 6 metres (16 to 20 feet) high, while west of the river the wall was originally made from turf and measured 6 metres (20 feet) wide and 3.5 metres (11 feet) high; it was  later rebuilt in stone. These dimensions do not include the wall's ditches, berms and forts. The central section measured eight Roman feet wide (2.4 m, 7 ft 10 in) on a 3 m (10 ft) base. Some parts of this section of the wall survive to a height of 3 m (10 ft).
Immediately south of the wall, a large ditch was dug, with adjoining parallel mounds, one on either side. This is known today as the Vallum, even though the word vallum in Latin is the origin of the English word wall, and does not refer to a ditch. In many places – for example Limestone Corner – the Vallum is better preserved than the wall, which has been robbed of much of its stone.


== Route ==

Hadrian's Wall extended west from Segedunum at Wallsend on the River Tyne, via Carlisle and Kirkandrews-on-Eden, to the shore of the Solway Firth, ending a short but unknown distance west of the village of Bowness-on-Solway. The A69 and B6318 roads follow the course of the wall from Newcastle upon Tyne to Carlisle, then along the northern coast of Cumbria (south shore of the Solway Firth).
Although the curtain wall ends near Bowness-on-Solway, this does not mark the end of the line of defensive structures. The system of milecastles and turrets is known to have continued along the Cumbria coast as far as Risehow, south of Maryport. For classification purposes, the milecastles west of Bowness-on-Solway are referred to as Milefortlets.


== Purpose of construction ==

Hadrian's Wall was probably planned before Hadrian's visit to Britain in 122. According to restored sandstone fragments found in Jarrow which date from 118 or 119, it was Hadrian's wish to keep "intact the empire", which had been imposed on him via "divine instruction".Although Hadrian's biographer wrote "[Hadrian] was the first to build a wall 80 miles long to separate the Romans from the barbarians", reasons for the construction of the wall vary, and no recording of an exact explanation survives. Theories have been presented by historians, mostly of an expression of Roman power and Hadrian's policy of defence before expansion. On his accession to the throne in 117, there was unrest and rebellion in Roman Britain and from the peoples of various conquered lands across the Empire, including Egypt, Judea, Libya and Mauretania.These troubles may have influenced Hadrian's plan to construct the wall as well as his construction of frontier boundaries in other areas of the Empire, but to what extent is unknown. Scholars disagree over how much of a threat the inhabitants of northern Britain really presented and whether there was any economic advantage in defending and garrisoning a fixed line of defences like the Wall, rather than conquering and annexing what has become Northumberland and the Scottish Lowlands and defending the territory with a loose arrangement of forts.The frontiers of Rome were never expected to stop tribes from migrating or armies from invading, and while a frontier protected by a palisade or stone wall would help curb cattle-raiders and the incursions of other small groups, the economic viability of constructing and keeping guarded a wall 72 miles (116 km) long along a sparsely populated border to stop small-scale raiding is dubious.Another possible explanation for the wall is the degree of control it would have provided over immigration, smuggling and customs. Frontiers did not strictly mark the boundaries of the empire: Roman power and influence often extended beyond them. People within and beyond the frontier travelled through it each day when conducting business, and organised check-points like those offered by Hadrian's Wall provided good opportunities for taxation. With watch towers only a short distance from gateways, patrolling legionaries could have kept track of entering and exiting natives and Roman citizens alike, charging customs dues and checking for smuggling. Another theory is of a simpler variety—that Hadrian's Wall was partly constructed to reflect the power of Rome and was used as a political point by Hadrian. Once its construction was finished, it is thought to have been covered in plaster and then whitewashed: its shining surface reflected the sunlight and was visible for miles around.


== Construction ==

Construction started in 122 and was largely completed in six years. Construction started in the east, between milecastles four and seven, and proceeded westwards, with soldiers from all three of the occupying Roman legions participating in the work. The route chosen largely paralleled the nearby Stanegate road to the south; the Stanegate predated Hadrian's Wall by about thirty years, and would have been a crucial supply route for its construction, as it seems that the Military Way connecting-road alongside Hadrian's Wall was not built until decades later. The Stanegate ran from Luguvalium (Carlisle) to Coria (Corbridge), where there was a bridge over the River Tyne, and a junction with Dere Street which connected with the south. Along Stanegate were situated a series of forts, including Vindolanda, which could have offered protection and supplies during the building of the wall. To the north, the wall in its central and best-preserved section follows a hard, resistant igneous dolerite or diabase rock escarpment, known as the Whin Sill.
The initial plan called for a ditch and wall with 80 small gated milecastle fortlets, one placed every Roman mile, holding a few dozen troops each, and pairs of evenly-spaced intermediate turrets used for observation and signalling. However, very few milecastles are actually sited at exact Roman mile divisions: they can be up to 180 metres (200 yd) east or west because of landscape features or to improve signalling to the Stanegate forts to the south. Local limestone was used in the construction, except for the section to the west of the River Irthing where turf was originally used instead, for unknown reasons; it was later rebuilt in stone. Milecastles in this area were also built from timber and earth rather than stone, but turrets were always made from stone. The Broad Wall was initially built with a clay-bonded rubble core and mortared dressed-rubble facing stones, but this seems to have made it vulnerable to collapse, and repair with a mortared core was sometimes necessary.
The milecastles and turrets were of three different designs, depending on which Roman legion built them – inscriptions of the II Augusta, VI Victrix, and XX Valeria Legions show that all were involved in the construction. The turrets were about 493 metres (539 yards) apart and measured 14.02 m2 (150.9 sq ft) internally.
Construction was divided into lengths of about 8 kilometres (5 mi). One group of each legion would excavate the foundations and build the milecastles and turrets, and then other cohorts would follow with the wall construction. The wall was finished in 128.


=== "Broad Wall" and "Narrow Wall" ===

Early in its construction, just after reaching the North Tyne, the width of the wall was narrowed to 2.5 m (8.2 ft) or even less (sometimes as little as 1.8 m, 5.9 ft) (the "Narrow Wall"). However, Broad Wall foundations had already been laid as far as the River Irthing, where the Turf Wall began, demonstrating that construction worked from east to west. Many turrets and milecastles were optimistically provided with wider stub "wing walls" in preparation for joining to the Broad Wall, offering a handy reference for archaeologists trying to piece together the construction chronology.
Within a few years it was decided to add a total of 14 to 17 (sources disagree) full-sized forts along the length of the wall, including Vercovicium (Housesteads) and Banna (Birdoswald), each holding between 500 and 1,000 auxiliary troops (no legions were posted to the wall). The eastern end of the wall was extended further east from Pons Aelius (Newcastle) to Segedunum (Wallsend) on the Tyne estuary. Some of the larger forts along the wall, such as Cilurnum (Chesters) and Vercovicium (Housesteads), were built on top of the footings of mile castles or turrets, showing the change of plan. An inscription mentioning early governor Aulus Platorius Nepos indicates that the change of plans took place early on. Also, sometime during Hadrian's reign (before 138) the wall west of the Irthing was rebuilt in sandstone to about the same dimensions as the limestone section to the east.

After most of the forts had been added, the Vallum was built on the southern side. The wall was thus part of a defensive system which, from north to south, included:

A row of forts built 5 to 10 mi (8.0 to 16.1 km) north of the wall, used for scouting and intelligence (e.g. Bewcastle Roman Fort)
a glacis, an upward slope to deter assailants, backed by a deep ditch
a berm, a flat area in front of the wall, with rows of pits concealing stakes or thorns
The curtain wall itself
a later connecting road, known as the Military Way
The Vallum, two embankments with a ditch between them


=== Turf wall ===
From Milecastle 49 to the western terminus of the wall at Bowness-on-Solway, the curtain wall was originally constructed from turf, possibly due to the absence of limestone for the manufacture of mortar. Subsequently, the Turf Wall was demolished and replaced with a stone wall. This took place in two phases; the first (from the River Irthing to a point west of Milecastle 54), during the reign of Hadrian, and the second following the reoccupation of Hadrian's Wall subsequent to the abandonment of the Antonine Wall (though it has also been suggested that this second phase took place during the reign of Septimius Severus). The line of the new stone wall follows the line of the turf wall, apart from the stretch between Milecastle 49 and Milecastle 51, where the line of the stone wall is slightly further to the north.In the stretch around Milecastle 50TW, it was built on a flat base with three to four courses of turf blocks. A basal layer of cobbles was used westwards from Milecastle 72 (at Burgh-by-Sands) and possibly at Milecastle 53. Where the underlying ground was boggy, wooden piles were used.At its base, the now-demolished turf wall was 6 metres (20 feet) wide, and built in courses of turf blocks measuring 46 cm (18 inches) long by 30 cm (12 inches) deep by 15 cm (6 inches) high, to a height estimated at around 3.66 metres (12.0 feet). The north face is thought to have had a slope of 75%, whereas the south face is thought to have started vertical above the foundation, quickly becoming much shallower.


=== Standards ===
Above the stone curtain wall's foundations, one or more footing courses were laid. Offsets were introduced above these footing courses (on both the north and south faces), which reduced the wall's width. Where the width of the curtain wall is stated, it is in reference to the width above the offset. Two standards of offset have been identified: Standard A, where the offset occurs above the first footing course, and Standard B, where the offset occurs after the third (or sometimes fourth) footing course.


== Garrison ==
According to Sheppard Frere, the garrison reflected the political rather than military purpose of the wall.  The wall provided the soldiers with an elevated platform from which they could safely observe movement of the local population.  It had "heavy provision of cavalry" which could sally out from any of the milestone gates though as mentioned earlier, the garrison was neither expected nor trained to the level necessary to defend a city wall.  Overall the fortifications appear to have required additional strengthening after the initial design and were stronger than their equivalent in Germany, probably reflecting local resentment.
Frere believes that the milecastles, which would have needed 1,000 to 1,500 men, were held by a patrolling garrison of numeri, though he concedes that there are no inscriptions referring to numeri in Britain at the time. Command headquarters was at Uxelodunum (nowadays called Stanwix) near Carlisle, where the Ala Gallorum Petriana was based.  A signalling system allowed communication in minutes between Stanwix and York.Further information on the garrisoning of the wall has been provided by the discovery of the Vindolanda tablets, such as the record of an inspection on 18 May of a year 92 and 97, where only 456 of the full quota of 756 Belgae troops were present, the rest being sick or otherwise absent.


== After Hadrian ==

In the years after Hadrian's death in 138, the new emperor, Antoninus Pius, left the wall occupied in a support role, essentially abandoning it. He began building a new wall called the Antonine Wall about 160 kilometres (100 mi) north, across the isthmus running west-south-west to east-north-east. This turf wall ran 40 Roman miles, or about 60.8 km (37.8 mi), and had significantly more forts than Hadrian's Wall. This area later became known as the Scottish Lowlands, sometimes referred to as the Central Belt or Central Lowlands. 
Antoninus was unable to conquer the northern tribes, so when Marcus Aurelius became emperor, he abandoned the Antonine Wall and reoccupied Hadrian's Wall as the main defensive barrier in 164. In 208–211, the Emperor Septimius Severus again tried to conquer Caledonia and temporarily reoccupied the Antonine Wall. The campaign ended inconclusively and the Romans eventually withdrew to Hadrian's Wall. The early historian Bede (AD 672/3-735), following Gildas, wrote (circa AD 730):

[the departing Romans] thinking that it might be some help to the allies [Britons], whom they were forced to abandon, constructed a strong stone wall from sea to sea, in a straight line between the towns that had been there built for fear of the enemy, where Severus also had formerly built a rampart.

Bede obviously identified Gildas's stone wall as Hadrian's Wall (actually built in the 120s) and he would appear to have believed that the ditch-and-mound barrier known as the Vallum (just to the south of and contemporary with, Hadrian's Wall) was the rampart constructed by Severus. Many centuries would pass before just who built what became apparent.In the same passage, Bede describes Hadrian's Wall as follows: "It is eight feet in breadth, and twelve in height; and, as can be clearly seen to this day, ran straight from east to west." Bede by his own account lived his whole life at Jarrow, just across the River Tyne from the eastern end of the Wall at Wallsend, so as he indicates, he would have been very familiar with the Wall. What he does not say is whether there was a walkway along the top of the wall. It might be thought likely that there was, but if so it no longer exists.
In the late 4th century, barbarian invasions, economic decline and military coups loosened the Empire's hold on Britain. By 410, the estimated end of Roman rule in Britain, the Roman administration and its legions were gone and Britain was left to look to its own defences and government.  Archaeologists have revealed that some parts of the wall remained occupied well into the 5th century. It has been suggested that some forts continued to be garrisoned by local Britons under the control of a Coel Hen figure and former dux. Hadrian's Wall fell into ruin and over the centuries the stone was reused in other local buildings. Enough survived in the 7th century for spolia from Hadrian's Wall (illustrated at right) to find its way into the construction of St Paul's Church in Monkwearmouth-Jarrow Abbey, where Bede was a monk. It was presumably incorporated before the setting of the church's dedication stone, still to be seen in the church, precisely dated to 23 April 685.

The wall fascinated John Speed, who published a set of maps of England and Wales by county at the start of the 17th century. He described it as "the Picts Wall" (or "Pictes"; he uses both spellings). A map of Newecastle (sic), drawn in 1610 by William Matthew, described it as "Severus' Wall", mistakenly giving it the name ascribed by Bede to the Vallum. The maps for Cumberland and Northumberland not only show the wall as a major feature, but are ornamented with drawings of Roman finds, together with, in the case of the Cumberland map, a cartouche in which he sets out a description of the wall itself.


=== Preservation by John Clayton ===
Much of the wall has now disappeared. Long sections of it were used for roadbuilding in the 18th century, especially by General Wade to build a military road (most of which lies beneath the present day B6318 "Military Road") to move troops to crush the Jacobite insurrection. The preservation of much of what remains can be credited to John Clayton. He trained as a lawyer and became town clerk of Newcastle in the 1830s. He became enthusiastic about preserving the wall after a visit to Chesters. To prevent farmers taking stones from the wall, he began buying some of the land on which the wall stood. In 1834, he started purchasing property around Steel Rigg near Crag Lough. Eventually, he controlled land from Brunton to Cawfields. This stretch included the sites of Chesters, Carrawburgh, Housesteads, and Vindolanda. Clayton carried out excavation at the fort at Cilurnum and at Housesteads, and he excavated some milecastles.
Clayton managed the farms he had acquired and succeeded in improving both the land and the livestock. He used the profits from his farms for restoration work. Workmen were employed to restore sections of the wall, generally up to a height of seven courses. The best example of the Clayton Wall is at Housesteads. After Clayton's death, the estate passed to relatives and was soon lost at gambling. Eventually, the National Trust began acquiring the land on which the wall stands. At Wallington Hall, near Morpeth, there is a painting by William Bell Scott, which shows a centurion supervising the building of the wall. The centurion has been given the face of John Clayton.


=== World Heritage Site ===
Hadrian's Wall was declared a World Heritage Site in 1987, and in 2005 it became part of the transnational "Frontiers of the Roman Empire" World Heritage Site, which also includes sites in Germany.


=== Tourism ===

Although Hadrian's Wall was declared a World Heritage Site in 1987, it remains unguarded, enabling visitors to climb and stand on the wall, although this is not encouraged, as it could damage the historic structure. On 13 March 2010, a public event Illuminating Hadrian's Wall took place, which saw the route of the wall lit with 500 beacons. On 31 August and 2 September 2012, there was a second illumination of the wall as a digital art installation called "Connecting Light", which was part of the London 2012 Festival. In 2018, the organisations which manage the Great Wall of China and Hadrian's Wall signed an agreement to collaborate for the growth of tourism and for historical and cultural understanding of the monuments.


=== Hadrian's Wall Path ===

In 2003, a National Trail footpath was opened that follows the line of the wall from Wallsend to Bowness-on-Solway. Because of the fragile landscape, walkers are asked to follow the path only in summer.  


== Roman-period names ==
Hadrian's Wall was known in the Roman period as the vallum (wall) and the discovery of the Staffordshire Moorlands Pan in Staffordshire in 2003 has thrown further light on its name. This copper alloy pan (trulla), dating to the 2nd century, is inscribed with a series of names of Roman forts along the western sector of the wall: MAIS [Bowness-on-Solway] COGGABATA [Drumburgh] VXELODVNVM [Stanwix] CAMBOGLANNA [Castlesteads]. This is followed by RIGORE VALI AELI DRACONIS. Hadrian's family name was Aelius, and the most likely reading of the inscription is Valli Aelii (genitive), Hadrian's Wall, suggesting that the wall was called by the same name by contemporaries. However, another possibility is that it refers to the personal name Aelius Draco.


=== Forts ===
The Latin and Romano-Celtic names of all of the Hadrian's Wall forts are known, from the Notitia Dignitatum and other evidence such as inscriptions:

Segedunum (Wallsend)
Pons Aelius (Newcastle upon Tyne)
Condercum (Benwell Hill)
Vindobala (Rudchester)
Hunnum (Halton Chesters)
Cilurnum (Chesters aka Walwick Chesters)
Procolita (Carrowburgh)
Vercovicium (Housesteads)
Aesica (Great Chesters)
Magnis (Carvoran)
Banna (Birdoswald)
Camboglanna (Castlesteads)
Uxelodunum (Stanwix. Also known as Petriana)
Aballava (Burgh-by-Sands)
Coggabata (Drumburgh)
Mais (Bowness-on-Solway)Turrets on the wall include:

Leahill Turret
Denton Hall TurretOutpost forts beyond the wall include:

Habitancum (Risingham)
Bremenium (High Rochester)
Fanum Cocidi (Bewcastle) (north of Birdoswald)
Ad Fines (Chew Green)Supply forts behind the wall include:

Alauna (Maryport)
Arbeia (South Shields)
Coria (Corbridge)
Epiacum (Whitley Castle near Alston)
Vindolanda (Little Chesters or Chesterholm)
Vindomora (Ebchester)


== In popular culture ==
BooksNobel Prize-winning English author Rudyard Kipling contributed to the popular image of the "Great Pict Wall" in his short stories about Parnesius, a Roman legionary who defended the Wall against the Picts. These stories are a part of the Puck of Pook's Hill anthology, published in 1906.The Eagle of the Ninth is a celebrated children's novel by Rosemary Sutcliff, published in 1954. It tells the story of a young Roman officer venturing beyond Hadrian's Wall in search of the missing eagle standard of the lost Ninth Legion. It was inspired by the bronze Silchester eagle found in 1866.American author George R. R. Martin has acknowledged that Hadrian's Wall was the inspiration for The Wall in his best-selling series A Song of Ice and Fire, dramatised in the fantasy TV series Game of Thrones, in which the Wall is also in the north of its country and stretches from coast to coast.In M J Trow's fictional Britannia series, Hadrian's Wall is the central location, and Coel Hen and Padarn Beisrudd are portrayed as limitanei (frontier soldiers).
Hadrian's Wall by Adrian Goldsworthy is a short history of the wall (April 2018).  Google description and  preview. Kirkus review.FilmsThe 1991 American romantic action adventure film Robin Hood: Prince of Thieves uses Sycamore Gap as a location.
The wall has also been featured in recent films such as Centurion (2010) and The Eagle (2011), both inspired by the novel The Eagle of the Ninth. The wall was a major focal point of the 2004 film King Arthur in which one of the primary gates is opened for the first time since the wall's construction to allow Arthur and his knights passage into the north for their quest. The climactic Battle of Badon between the Britons led by Arthur and his knights, and the Saxons led by Cerdic and his son Cynric, takes place in the film just inside the wall.
The 2007 action adventure film The Last Legion also features Hadrian's Wall as the place where the final battle takes place.
The 2008 science fiction film Doomsday features Hadrian's Wall rebuilt to quarantine Scotland because of a deadly virus.
The 2015 film Dragonheart 3: The Sorcerer's Curse features Hadrian's Wall as a major dramatic focus between the north and south of the kingdom; the wall is later renamed the Dragon's Gate.
The 2014 action thriller The Prince, in a key scene, features John Cusack telling the story of Hadrian’s Wall as a cautionary tale in the development of the film’s main character, portrayed by Jason Patrick.MusicThe opening track from Maxim's first solo effort Hell's Kitchen is named "Hadrian's Wall".
The second track on the English-American hard rock band Black Country Communion's second album is called "The Battle for Hadrian's Wall". The lyrics reference outposts separated by a mile each, such as "We sit in waiting every mile on Hadrian's Wall".TelevisionThe final episode in the Blackadder series is the television film Blackadder: Back & Forth. In this episode, Blackadder and Baldrick attempt to collect significant historical souvenirs in order to win a bet. During their adventures, they take note at their own ancestors, one of whom is a Roman defender of Hadrian's Wall who makes slighting remarks about the wall's unimpressive (modern) height.PoetryThe English poet W. H. Auden wrote a script for a BBC radio documentary called Hadrian's Wall, which was broadcast on the BBC's north-eastern Regional Programme in 1937.  Auden later published a poem from the script, "Roman Wall Blues", in his book Another Time.  The poem is a brief monologue spoken in the voice of a lonely Roman soldier stationed at the wall.


== Gallery ==

		
		
		
		
		


== See also ==


== References ==


== Sources ==
Birley, A.R. (1963). Hadrians Wall Illustrated Guide. London: Her Majesty's Stationery Office (HMSO).
Burton, Anthony. Hadrian's Wall Path. 2004. Aurum Press Ltd. ISBN 1-85410-893-X.
Chaichian, Mohammad. 2014. "Hadrian's Wall: An Ill-Fated strategy for Tribal Management in Roman Britain," in Empires and Walls: Globalization, Migration, and Colonial Domination (Brill, pp. 23-52). https://www.amazon.com/Empires-Walls-Globalization-Migration-Domination/dp/1608464229.
Davies, Hunter. A Walk along the Wall, 1974. Weidenfeld & Nicolson: London ISBN 0 297 76710 0.
de la Bédoyère, Guy. Hadrian's Wall: A History and Guide. Stroud: Tempus, 1998. ISBN 0-7524-1407-0.
England's Roman Frontier: Discovering Carlisle and Hadrian's Wall Country. Hadrian's Wall Heritage Ltd and Carlisle Tourism Partnership. 2010.
Forde-Johnston, James L. Hadrian's Wall. London: Michael Joseph, 1978. ISBN 0-7181-1652-6.
Hadrian's Wall Path (map). Harvey, 12–22 Main Street, Doune, Perthshire FK16 6BJ. harveymaps.co.uk
Higgins, Charlotte (2014). "Chapter Seven: Hadrian's Wall". Under Another Sky: Journeys in Roman Britain. London: Vintage. ISBN 978-0-099552-09-3.
Hodgson, Nick (2017). Hadrian's Wall. Marlborough, UK: Robert Hale. ISBN 978 0 7198 1815 8.
Moffat, Alistair, The Wall. 2008. Birlinn Limited Press. ISBN 1-84158-675-7.
Shanks, Michael (2012). "'Let me tell you about Hadrian's Wall …' Heritage, Performance, Design". Text of the Reinwardt Memorial Lecture.
Speed, John – A set of Speed's maps were issued bound in a single volume in 1988 in association with the British Library and with an introduction by Nigel Nicolson as The Counties of Britain: A Tudor Atlas by John Speed.
Tomlin, R. S. O., "Inscriptions" in Britannia (2004), vol. xxxv, pp. 344–5 (the Staffordshire Moorlands cup naming the Wall).
Wilson, Roger J. A., A Guide to the Roman Remains in Britain. London: Constable & Company, 1980; ISBN 0-09-463260-X.


== External links ==
In Our Time Radio series with Greg Woolf, Professor of Ancient History at the University of St Andrews, David Breeze, Former Chief Inspector of Ancient Monuments for Scotland and Visiting Professor of Archaeology at the University of Durham and Lindsay Allason-Jones  OBE, FSA, FSA Scot, Former Reader in Roman Material Culture at the University of Newcastle 
Hadrian's Wall on the Official Northumberland Visitor website
Hadrian's Wall Discussion Forum
UNESCO Frontiers of the Roman Empire
News on the Wall path
English Lakes article
iRomans—website with interactive map of Cumbrian section of Hadrian Wall
Well illustrated account of sites along Hadrian's Wall