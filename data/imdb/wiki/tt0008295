Millstones or mill stones are stones used in gristmills, for grinding wheat or other grains.
Millstones come in pairs. The base or bedstone is stationary. Above the bedstone is the turning runner stone which actually does the grinding. The runner stone spins above the stationary bedstone creating the "scissoring" or grinding action of the stones. A runner stone is generally slightly concave, while the bedstone is slightly convex. This helps to channel the ground flour to the outer edges of the stones where it can be gathered up.
The runner stone is supported by a cross-shaped metal piece (rind or rynd) fixed to a "mace head" topping the main shaft or spindle leading to the driving mechanism of the mill (wind, water (including tide) or other means).


== History ==

Neolithic and Upper Paleolithic people used millstones to grind grains, nuts, rhizomes and other vegetable food products for consumption. These implements are often called grinding stones. They used either saddle stones  or rotary querns turned by hand. Such devices were also used to grind pigments and metal ores prior to smelting.
In India, grinding stones (Chakki) were used to grind grains and spices. These consist of a stationary stone cylinder upon which a smaller stone cylinder rotates. Smaller ones, for household use, were operated by two people. Larger ones, for community or commercial use, used livestock to rotate the upper cylinder.


== Material ==

The type of stone most suitable for making millstones is a siliceous rock called burrstone (or buhrstone), an open-textured, porous but tough, fine-grained sandstone, or a silicified, fossiliferous limestone. In some sandstones, the cement is calcareous.Millstones used in Britain were of several types:

Derbyshire Peak stones of grey Millstone Grit, cut from one piece, used for grinding barley; imitation Derbyshire Peak stones are used as decorative signposts at the boundaries of the Peak District National Park. Derbyshire Peak stones wear quickly and are typically used to grind animal feed since they leave stone powder in the flour, making it undesirable for human consumption.
French buhrstones, used for finer grinding. French Burr comes from the Marne Valley in northern France. The millstones are not cut from one piece, but built up from sections of quartz cemented together, backed with plaster and bound with shrink-fit iron bands. Slots in the bands provide attachments for lifting.  In southern England the material was imported as pieces of rock, only assembled into complete millstones in local workshops. It was necessary to balance the completed runner stone with lead weights applied to the lighter side.
Composite stones, built up from pieces of emery, were introduced during the nineteenth century; they were found to be more suitable for grinding at the higher speeds available when auxiliary engines were adopted.In Europe, a further type of millstone was used. These were uncommon in Britain, but not unknown:

Cullen stones, (stones from Cologne), a form of black lava quarried in the Rhine Valley at Mayen near Cologne, Germany.


== Patterning ==

The surface of a millstone is divided by deep grooves called furrows into separate flat areas called lands. Spreading away from the furrows are smaller grooves called feathering or cracking. The grooves provide a cutting edge and help to channel the ground flour out from the stones.
The furrows and lands are arranged in repeating patterns called harps. A typical millstone will have six, eight or ten harps. The pattern of harps is repeated on the face of each stone, when they are laid face to face the patterns mesh in a kind of "scissoring" motion creating the cutting or grinding function of the stones. When in regular use stones need to be dressed periodically, that is, re-cut to keep the cutting surfaces sharp.
Millstones need to be evenly balanced, and achieving the correct separation of the stones is crucial to producing good quality flour. The experienced miller will be able to adjust their separation very accurately.


== Grinding with millstones ==

Grain is fed by gravity from the hopper into the feed-shoe. The shoe is agitated by a shoe handle running against an agitator (damsel) on the stone spindle, the shaft powering the runner stone. This mechanism regulates the feed of grain to the millstones by making the feed dependent on the speed of the runner stone. From the feed shoe the grain falls through the eye, the central hole, of the runner stone and is taken between the runner and the bed stone to be ground. The flour exits from between the stones from the side. The stone casing prevents the flour from falling on the floor, instead it is taken to the meal spout from where it can be bagged or processed further. 

The runner stone is supported by the rind, a cross-
shaped metal piece, on the spindle. The spindle is carried by the tentering gear, a set of beams forming a lever system, or a screw jack, with which the runner stone can be lifted or lowered slightly and the gap between the stones adjusted. The weight of the runner stone is significant (up to 1,500 kilograms (3,300 lb)) and it is this weight combined with the cutting action from the porous stone and the patterning that causes the milling process.
Millstones for some water-powered mills (such as Peirce Mill) spin at about 125 rpm.Especially in the case of wind-powered mills the turning speed can be irregular. Higher speed means more grain is fed to the stones by the feed-shoe, and grain exits the stones more quickly because of their faster turning speed. The miller has to reduce the gap between the stones so more weight of the runner presses down on the grain and the grinding action is increased to prevent the grain being ground too coarsely. It has the added benefit of increasing the load on the mill and so slowing it down. In the reverse case the miller may have to raise the runner stone if the grain is milled too thoroughly making it unsuitable for baking. In any case the stones should never touch during milling as this would cause them to wear down rapidly. The process of lowering and raising the runner stone is called tentering and lightering. In many windmills it is automated by adding a centrifugal governor to the tentering gear.
Depending on the type of grain to be milled and the power available the miller may adjust the feed of grain to stones beforehand by changing the amount of agitation of the feed-shoe or adjusting the size of the hopper outlet.
Milling by millstones is a one-step process in contrast with roller mills in modern mass production where milling takes place in many steps. It produces wholemeal flour which can be turned into white flour by sifting to remove the bran.


== Heraldry ==

As an old item and symbol for industry, the millstone also has found its place as a charge in heraldry.


== See also ==
Bedrock mortar
Edge mill
Querns
Mühlsteinbrüche, historic millstone quarries in Saxony


== References ==


== External links ==
Millstone Dressing Tools
European millstone quarries: a database
Abandoned Millstones - Peak District
Video clip demonstrating millstone dressing
Millstones & Querns on Flickr