The Game of Life, also known simply as Life, is a board game originally created in 1860 by Milton Bradley, as The Checkered Game of Life. The Game of Life was America's first popular parlor game. The game simulates a person's travels through his or her life, from college to retirement, with jobs, marriage, and possible children along the way. Two to four or six players can participate in one game. Variations of the game accommodate up to ten players.
The modern version was originally published 100 years later, in 1960. It was created and co-designed by toy and game designer Reuben Klamer and was "heartily endorsed" by Art Linkletter.  It is now part of the permanent collection of the Smithsonian's National Museum of American History and an inductee into the National Toy Hall of Fame.


== History ==

The game was originally created in 1860 by Milton Bradley as The Checkered Game of Life, and was the first game created by Bradley, a successful lithographer. The game sold 45,000 copies by the end of its first year. Like many 19th-century games, such as The Mansion of Happiness by S. B. Ives in 1843, it had a strong moral message.The game board resembled a modified checkerboard. The object was to land on "good" spaces and collect 100 points. A player could gain 50 points by reaching "Happy Old Age" in the upper-right corner, opposite "Infancy" where one began.
Instead of dice – which were associated with gambling – players used a six-sided top called a teetotum.


== Modern game ==
In 1960 the modern version, The Game of Life, was introduced. A collaboration between Reuben Klamer and Bill Markham, it consists of a track which passes along, over, and through small mountains, buildings, and other features. A player travels along the track in a small plastic automobile, according to the spins of a small wheel on the board with spaces numbered 1 through 10. Each car has six holes into which pegs are added as the player "gets married" and "acquires children". Some "early modern" editions have eight cars. The modern game pegs or "people" are pink and blue to distinguish the genders. Each player starts the game with one peg that matches his/her gender.
There is also a bank which includes money in $5,000, $10,000, $20,000, $50,000, and $100,000 bills; automobile, life, fire, and/or homeowners' insurance policies (depending on the version); $20,000 promissory notes and stock certificates. Other tangibles vary between versions of the game. $500 bills were dropped in the 1980s as were $1,000 bills in 1992.


== Versions ==


=== 1960s version ===
The Game of Life, copyrighted by the Milton Bradley Company in 1963, had some differences from later versions. For example, once a player reached the "Day of Reckoning" space, he/she had to choose one of two options. The first was to continue along the road to "Millionaire Acres," if the player believed he/she had enough money to out-score all opponents. The second option was to try to become a "Millionaire Tycoon" by betting everything on one number and spinning the wheel. The player immediately won the game if the chosen number came up, or went to the "Poor Farm" and was eliminated if it did not. If no player became a Millionaire Tycoon, the one with the highest final total won the game.
This version had Art Linkletter as the spokesman, included his likeness on the $100,000 bills (with his name displayed on the bills as "Arthur Linkletter Esq.") and a rousing endorsement from Linkletter on the cover of the box. It was advertised as a "Milton Bradley 100th Anniversary Game" and as "A Full 3-D Action Game."
Winning Moves currently markets a classic 1960s edition.


=== 1970s/1980s versions ===
About halfway through the production of this version, many dollar values doubled. This description focuses on the later version with the larger dollar amounts. The late 1980s version also replaced the convertibles from earlier versions with minivans. Early 1960s-era convertibles were still used in the 1978 edition.  The "Poor Farm" was renamed "Bankrupt!" in which losing players would "Retire to the country and become a philosopher", and "Millionaire Acres" was shortened to "Millionaire!" in which the winner can "Retire in style".  
The gold "Revenge" squares added a byline, "Sue for damages", in the 1978 edition.


=== 1991 version ===
The Game of Life was updated in 1991 to reward players for good behavior, such as recycling trash and helping the homeless, by awarding players "Life Tiles."
The 1998 PC and Sony PlayStation video game adaptations of The Game of Life are based on this version.  Players could play either the "classic" version, using the Life Tiles, or the "enhanced" version, where landing on a space with a Life Tile allow players to play one of several mini-games.


=== 2005 version ===
An updated version of the game was released in 2005 with a few gameplay changes. The new Game of Life reduced the element of chance, although it is still primarily based on chance and still rewards players for taking risks.


=== 2013 version ===
The 2013 version removed the lawsuit square which was replaced by a lawsuit card. A new "keep this card for 100k" feature was added as well.


=== 2017 version ===
The 2017 version includes pegs and squares for acquiring pets.


=== Other versions ===


==== Board games ====
Hello Kitty Edition (1999, Japan Only)
The Game of Life in Monstropolis (Monsters, Inc. version) (2001)
The Game of Life Card Game (2002)
Fame Edition (or Game of Life Junior/travel version) (2002)
Star Wars: A Jedi's Path (2002)
Pirates of the Caribbean (2004)
The Simpsons Edition (2004)
Bikini Bottom SpongeBob SquarePants Edition (2005)
Pokémon Edition (2006, Japan only)
Pirates of the Caribbean: Dead Man's Chest (2006)
Twists and Turns Edition (2007)
The Game of Life Express (2007)[1]
Indiana Jones Edition (2008, Target exclusive)
Family Guy Collectors Edition (2008)
The Wizard of Oz Edition (2009)
The Game of Life - Haunted Mansion Theme Park Edition (2009)
The Game of Life High School Edition (aka "Pink Edition")
LIFE: Rock Star Edition
The Game of LIFE: It's a Dog's Life Edition (2011)
The Game of LIFE: Despicable Me (2014)
 LIFE: My Little Pony Edition 
Inside Out (2015)
 LIFE: Yo-Kai Watch Edition (2016)
 The Game of Life: Quarter Life Crisis (2019)


==== Video games ====
RPG Jinsei Game Nintendo Entertainment System (NES) video game (1993)
Super Jinsei Game series
Super Jinsei Game Super Famicom video game (1994)
Super Jinsei Game 2 Super Famicom video game (1995)
Super Jinsei Game 3 Super Famicom video game (1996)
The Game of Life for PC and PlayStation (1998)
The Game of Life/Yahtzee/Payday Game Boy Advance game
The Game of Life Wii game (2008)
The Game of Life WiiWare game (2009) (Japan Only)
The Game of Life Classic Edition iPhone game (2009)
Hasbro Family Game Night 3 for Xbox 360, Wii, and PlayStation 3 video game platforms, and was also later released as part of the Hasbro Family Game Night Fun Pack, which consisted as a compilation of both Hasbro Family Game Night 2 and Hasbro Family Game Night 3.
 The Game of Life: 2016 Edition for iOS, Android & Steam (service) by Marmalade Game Studio (2016) 


==== Television show ====
Game Show Edition on The Hub (2011)


== See also ==
Life as a BlackMan (board game)


== References ==


== External links ==
The Game of Life   at BoardGameGeek
The Game of Life rules from 1977 at Hasbro.com
The Game of Life rules from 1991 at Hasbro.com
The Game of Life rules from 2000 at Hasbro.com