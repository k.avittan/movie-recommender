The Loves of Pharaoh (German: Das Weib des Pharao, aka The Wife of the Pharaoh) is a 1922 German historical epic film directed by Ernst Lubitsch. It starred Emil Jannings.
A complete version of the film had been considered lost for years.  A digitally restored and  reconstructed version premièred on 17 September 2011.  The restored film includes the original music by composer Eduard Künneke that had been commissioned for the film by Lubitsch.Lubitsch is thought to have made The Loves of Pharaoh to show Hollywood that he could make an epic. The Loves of Pharaoh was his last German feature before he migrated to Hollywood in 1923.


== Restoration ==
The restoration was done by Thomas Bakels of Munich-based Alpha Omega GmbH, the company that did the digital 2001 and 2010 restorations on Fritz Lang's 1927 Metropolis. Bakels spent five years on the digital restoration; the Munich Film Museum did the reconstruction. The complete version of The Loves of Pharaoh had been lost, so restoration had to be done from parts of the film that had been found in various countries. The largest part came from a 35mm tinted nitrate print the German Federal Archives had acquired from a film archive in Russia in the 1970s. The Russian footage lasted only 55 minutes and was missing all the scenes dealing with love and emotion. This version revolved around the massive combat sequences. The restoration extended the Russian version with footage from an Italian nitrate print of Pharaoh that the George Eastman House in Rochester, New York, had acquired in 1998. The Italian print had been fragmented but contained the missing love scenes. To this was added other footage, and the title cards, which had turned up during the Munich Film Museum's restoration work on another movie. According to the introduction of the restoration, about 600 metres (2,000 ft) of the original 2,976 metres (9,764 ft) of film is still missing.


== Plot ==
Pharaoh Amenes (Emil Jannings) receives glad tidings: King Samlak of Ethiopia proposes an alliance, to be cemented by the marriage of Amenes to Samlak's daughter, Makeda. Sothis, Amenes's master builder, reports there has been an accident at the construction site of the treasury and begs for more time for his workers' sake, but Amenes is unmoved.
As Samlak and Makeda trek to Amenes, Ramphis, the son of Sothis, spots Makeda's despised Greek slave, Theonis. Entranced, he takes her home with him. When Ramphis tries to kiss Theonis, she playfully runs away toward the treasury, unaware that the penalty to approach the place is death. Ramphis chases after her, but they are caught and brought before Amenes.
Pharaoh sentences them both to be executed at dawn. Theonis throws herself at his feet and begs him to spare Ramphis, as it was all her fault. Amenes immediately falls under her spell. He offers to let Ramphis live in return for her. Theonis rejects him, but seeing Ramphis about to be crushed underneath a gigantic stone slab, she gives in. Amenes commutes Ramphis's sentence to a life working in the quarries; the prisoner is told that Theonis has been executed.
Amenes decides to make Theonis his queen, mortally offending King Samlak. Samlak raises his army and invades the country. Meanwhile, Ramphis triggers a rebellion at the quarry when he goes to the aid of a stricken fellow prisoner. He escapes in the confusion when the news of the invasion breaks.
Back in Amenes's city, Pharaoh prepares to lead out his army. Before he leaves, however, he demands that Theonis swear an oath not to take another man if he is killed in battle. When she refuses, he orders that she be sealed within the treasury. He has Sothis show him the secret entrance to the treasury, then has the builder blinded.
Samlak launches a surprise attack on Amenes's camp, routing the defenders. He personally shoots an arrow into the back of Amenes, causing him to fall from his fleeing chariot. Before he succumbs, Amenes asks Samlak not to harm Theonis.
Ramphis makes his way home. When he learns what has happened to his father and Theonis, he enters the treasury, intent on killing Theonis, blaming her for his father's blinding. However, when he sees her, he cannot go through with it.
Samlak marches on the city. He gives the terrified inhabitants a choice: give up their queen or he will sack the city. Theonis, apprised of the situation, decides to give herself up, but instead, Ramphis rallies the soldiers and prepares an ambush, having the army hide in and around the treasury, and lets Samlak break down the gates and enter the city. Then the Egyptians attack the unsuspecting and celebrating Ethiopian army. The Egyptians are victorious.
Queen Theonis chooses Ramphis as her king, to the delight of the soldiers. Then Amenes shows up, haggard but still alive. The chief priest tells him he has lost his throne, but that Theonis is still his wife. No one dares challenge the law of the gods. In desperation, Ramphis offers him back the throne in return for Theonis. When the couple leave the palace, the mob turns on them, stoning them to death, despite Amenes's attempt to stop it. Distraught, Amenes returns to his throne, then falls down dead.


== Cast ==
Emil Jannings as Pharaoh Amenes
Paul Biensfeldt as Menon, Amenes's governor
Friedrich Kühne as Oberpriester
Albert Bassermann as Sothis
Harry Liedtke as Ramphis
Paul Wegener as Samlak
Lyda Salmonova as Makeda
Dagny Servaes as Theonis


== See also ==
List of historical drama films
List of rediscovered films


== References ==


== External links ==
The Loves of Pharaoh on IMDb