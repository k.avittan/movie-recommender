The Thirty-Six Dramatic Situations is a descriptive list which was created by Georges Polti to categorize every dramatic situation that might occur in a story or performance. To do this Polti analyzed classical Greek texts, plus classical and contemporaneous French works. He also analyzed a handful of non-French authors. In his introduction, Polti claims to be continuing the work of Carlo Gozzi, who also identified 36 situations.


== Publication history ==

This list was published in a book of the same name, which contains extended explanations and examples. The original French-language book was written in 1895. An English translation was published in 1916 and continues to be reprinted to this day.
The list is popularized as an aid for writers, but it is also used by dramatists, storytellers and many others. Other similar lists have since been made.
It influenced Christina Stead and George Pierce Baker, the author of Dramatic Technique. The situations have been critiqued as being "concatenations of events rather than minimal or isolable motifs".


== The 36 situations ==
Each situation is stated, then followed by the necessary elements for each situation and a brief description.

Supplication
a persecutor; a suppliant; a power in authority, whose decision is doubtful.
The suppliant appeals to the power in authority for deliverance from the persecutor. The power in authority may be a distinct person or be merely an attribute of the persecutor, e.g. a weapon suspended in their hand. The suppliant may also be two persons, the Persecuted and the Intercessor, an example of which is Esther interceding to the king on behalf of the Jews for deliverance from the king's chief advisor.
Deliverance
an unfortunate; a threatener; a rescuer
The unfortunate has caused a conflict, and the threatener is to carry out justice, but the rescuer saves the unfortunate. Examples: Ifigenia in Tauride, Deliverance
Crime pursued by vengeance
a criminal; an avenger
The criminal commits a crime that will not see justice, so the avenger seeks justice by punishing the criminal. Example: The Count of Monte Cristo
Vengeance taken for kin upon kin
Guilty Kinsman; an Avenging Kinsman; remembrance of the Victim, a relative of both.
Two entities, the Guilty and the Avenging Kinsmen, are put into conflict over wrongdoing to the Victim, who is allied to both. Example: Hamlet
Pursuit
punishment; a fugitive
the fugitive flees punishment for a misunderstood conflict.  Example: Les Misérables, The Fugitive
Disaster
a vanquished power; a victorious enemy or a messenger
The vanquished power falls from their place after being defeated by the victorious enemy or being informed of such a defeat by the messenger. Example: Agamemnon (play)
Falling prey to cruelty/misfortune
an unfortunate; a master or a misfortune
The unfortunate suffers from misfortune and/or at the hands of the master. Example: Job (biblical figure)
Revolt
a tyrant; a conspirator
The tyrant, a cruel power, is plotted against by the conspirator. Example: Julius Caesar (play)
Daring enterprise
a bold leader; an object; an adversary
The bold leader takes the object from the adversary by overpowering the adversary. Example: Queste del Saint Graal, The Lord of the Rings
Abduction
an abductor; the abducted; a guardian
The abductor takes the abducted from the guardian. Example: Helen of Troy
The enigma
a problem; an interrogator; a seeker
The interrogator poses a problem to the seeker and gives a seeker better ability to reach the seeker's goals. Example: Oedipus and the Sphinx
Obtaining
(a Solicitor & an adversary who is refusing) or (an arbitrator & opposing parties)
The solicitor is at odds with the adversary who refuses to give the solicitor an object in the possession of the adversary, or an arbitrator decides who gets the object desired by opposing parties (the solicitor and the adversary). Example: Apple of Discord
Enmity of kin
a Malevolent Kinsman; a Hated or a reciprocally-hating Kinsman
The Malevolent Kinsman and the Hated or a second Malevolent Kinsman conspire together. Example: As You Like It
Rivalry of kin
the Preferred Kinsman; the Rejected Kinsman; the Object of Rivalry
The Object of Rivalry chooses the Preferred Kinsman over the Rejected Kinsman. Example: Wuthering Heights
Murderous adultery
two Adulterers; a Betrayed Spouse
Two Adulterers conspire to kill the Betrayed Spouse. Example: Clytemnestra, Aegisthus, Double Indemnity
Madness
a Madman; a Victim
The Madman goes insane and wrongs the Victim. Example: The Shining (novel)
Fatal imprudence
the Imprudent; a Victim or an Object Lost
The Imprudent, by neglect or ignorance, loses the Object Lost or wrongs the Victim.
Involuntary crimes of love
a Lover; a Beloved; a Revealer
The Lover and the Beloved have unknowingly broken a taboo through their romantic relationship, and the Revealer reveals this to them Example: Oedipus, Jocasta and the messenger from Corinth.
Slaying of kin unrecognized
the Slayer; an Unrecognized Victim
The Slayer kills the Unrecognized Victim. Example: Oedipus and Laius
Self-sacrifice for an ideal
a Hero; an Ideal; a Creditor or a Person/Thing sacrificed
The Hero sacrifices the Person or Thing for their Ideal, which is then taken by the Creditor. Example: The gospel
Self-sacrifice for kin
a Hero; a Kinsman; a Creditor or a Person/Thing sacrificed
The Hero sacrifices a Person or Thing for their Kinsman, which is then taken by the Creditor. Example: The gospel
All sacrificed for passion
a Lover; an Object of fatal Passion; the Person/Thing sacrificed
A Lover sacrifices a Person or Thing for the Object of their Passion, which is then lost forever.
Necessity of sacrificing loved ones
a Hero; a Beloved Victim; the Necessity for the Sacrifice
The Hero wrongs the Beloved Victim because of the Necessity for their Sacrifice.
Rivalry of superior vs. inferior
a Superior Rival; an Inferior Rival; the Object of Rivalry
A Superior Rival bests an Inferior Rival and wins the Object of Rivalry.
Adultery
two Adulterers; a Deceived Spouse
Two Adulterers conspire against the Deceived Spouse.
Crimes of love
a Lover; the Beloved
A Lover and the Beloved break a taboo by initiating a romantic relationship Example: Sigmund and his sister in The Valkyrie
Discovery of the dishonour of a loved one
a Discoverer; the Guilty One
The Discoverer discovers the wrongdoing committed by the Guilty One.
Obstacles to love
two Lovers; an Obstacle
Two Lovers face an Obstacle together. Example: Romeo and Juliet
An enemy loved
a Lover; the Beloved Enemy; the Hater
The allied Lover and Hater have diametrically opposed attitudes towards the Beloved Enemy.
Ambition
an Ambitious Person; a Thing Coveted; an Adversary
The Ambitious Person seeks the Thing Coveted and is opposed by the Adversary. Example: Macbeth
Conflict with a god
a Mortal; an Immortal
The Mortal and the Immortal enter a conflict.
Mistaken jealousy
a Jealous One; an Object of whose Possession He is Jealous; a Supposed Accomplice; a Cause or an Author of the Mistake
The Jealous One falls victim to the Cause or the Author of the Mistake and becomes jealous of the Object and becomes conflicted with the Supposed Accomplice.
Erroneous judgment
a Mistaken One; a Victim of the Mistake; a Cause or Author of the Mistake; the Guilty One
The Mistaken One falls victim to the Cause or the Author of the Mistake and passes judgment against the Victim of the Mistake when it should be passed against the Guilty One instead.
Remorse
a Culprit; a Victim or the Sin; an Interrogator
The Culprit wrongs the Victim or commits the Sin, and is at odds with the Interrogator who seeks to understand the situation. Example: The Bourne Supremacy
Recovery of a lost one
a Seeker; the One Found
The Seeker finds the One Found. Example: A Very Long Engagement
Loss of loved ones
a Kinsman Slain; a Kinsman Spectator; an Executioner
The killing of the Kinsman Slain by the Executioner is witnessed by the Kinsman. Example: Braveheart


== See also ==
Aarne–Thompson classification systems
Morphology (folkloristics)
The Golden Bough
The Seven Basic Plots
Vladimir Propp


== References ==


== External links ==
Full text available at Internet Archive
Full text available at Wikisource