Marriage and Morals is a 1929 book by philosopher Bertrand Russell, in which the author questions the Victorian notions of morality regarding sex and marriage.
Russell argues that the laws and ideas about sex of his time were a potpourri from various sources and were no longer valid. The subjects range from criticisms of social norms, theories about their origins and tendencies, evolutionary psychology, and instinctual attachment to children (or lack thereof), among others. Notably, the book found marital rape to be common, stating: "Marriage is for woman the commonest mode of livelihood, and the total amount of undesired sex endured by women is probably greater in marriage than in prostitution."


== Cultural response ==
Marriage and Morals prompted vigorous protests against and denunciations of Russell during his visit to the United States shortly after the book's publication. A decade later, the book, along with his protest of US involvement in WWI and his generally controversial position in public discourse, cost him his professorial appointment at the City College of New York due to a court judgment from a Catholic judge that his opinions made him "morally unfit" to teach. A public outcry, initiated by the mother of a student who was ineligible for his course in mathematical logic, preceded the ruling. John Dewey and several other intellectuals protested his treatment. Albert Einstein's often-quoted aphorism that "Great spirits have always encountered violent opposition from mediocre minds" is from his open letter in support of Russell.


== Eugenics ==
In chapter eighteen, Russell discusses eugenics, expressing humanitarian reservations about many aspects and cautious approval for others. On the topic of "negative eugenics" (selecting against undesirable qualities), he concludes that most such qualities are too subjectively defined, but that sterilization on the basis of "mental deficiency" is justifiable. He explores the idea of "positive eugenics" (selecting for desirable qualities), concluding that such a program would be fraught with moral issues but could produce a very militarily capable nation. On the subject of race, he accepts that black and aboriginal races are somehow inferior, but says that most racial eugenics is "an excuse for chauvinism."


== Nobel Prize ==
According to Russell, he was awarded the Nobel Prize in Literature for Marriage and Morals.
When I was called to Stockholm, at the end of 1950, to receive the Nobel Prize – somewhat to my surprise, for literature, for my book Marriage and Morals – I was apprehensive, since I remembered that, exactly 300 years earlier, Descartes had been called to Scandinavia by Queen Christina in the winter time and had died of the cold.
The Nobel Foundation, on the other hand, wrote that the prize recognized "his varied and significant writings in which he champions humanitarian ideals and freedom of thought"; not any particular work.


== References ==


== External links ==
 Quotations related to Marriage and Morals at Wikiquote
Full text at archive.org