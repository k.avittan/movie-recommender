A furnace, referred to as a heater or boiler in British English, is a heating unit used to heat up an entire building. Furnaces are mostly used as a major component of a central heating system. The name derives from Latin word fornax, which means oven. Furnaces are permanently installed to provide heat to an interior space through intermediary fluid movement, which may be air, steam, or hot water. Heating appliances that use steam or hot water as the fluid are normally referred to as a residential steam boiler or residential hot water boiler.  The most common fuel source for modern furnaces in North America and much of Europe is natural gas; other common fuel sources include LPG (liquefied petroleum gas), fuel oil and in rare cases coal or wood. In some areas electrical resistance heating is used, especially where the cost of electricity is low or the primary purpose is for air conditioning. Modern high-efficiency furnaces can be up to 98% efficient and operate without a chimney, with a typical gas furnace being about 80% efficient. Waste gas and heat are mechanically ventilated through PVC pipes that can be vented through the side or roof of the house. Fuel efficiency in a gas furnace is measured in AFUE (Annual Fuel Utilization Efficiency). Furnaces primarily run on natural gas or electricity. Furnaces that are used to boil water are called boilers.
The term furnace also refers to the various types of metallurgical furnaces, used for smelting and other metalworks, as well as industrial furnaces, which are used in various industrial applications such as chemical plants and providing heat to chemical reactions.


== Categories ==
Furnaces can be classified into four general categories, based on efficiency and design, natural draft, forced-air, forced draft, and condensing.


=== Natural draft ===
The first category of furnaces are natural draft, atmospheric burner furnaces.  These furnaces consisted of cast-iron or riveted-steel heat exchangers built within an outer shell of brick, masonry, or steel. The heat exchangers were vented through brick or masonry chimneys. Air circulation depended on large, upwardly pitched pipes constructed of wood or metal. The pipes would channel the warm air into floor or wall vents inside the home. This method of heating worked because warm air rises.
The system was simple, had few controls, a single automatic gas valve, and no blower. These furnaces could be made to work with any fuel simply by adapting the burner area. They have been operated with wood, coke, coal, trash, paper, natural gas, fuel oil as well as whale oil for a brief period at the turn of the century. Furnaces that used solid fuels required daily maintenance to remove ash and "clinkers" that accumulated in the bottom of the burner area. In later years, these furnaces were adapted with electric blowers to aid air distribution and speed moving heat into the home. Gas and oil-fired systems were usually controlled by a thermostat inside the home, while most wood and coal-fired furnaces had no electrical connection and were controlled by the amount of fuel in the burner and position of the fresh-air damper on the burner access door.


=== Forced-air ===
The second category of furnace is the forced-air, atmospheric burner style with a cast-iron or sectional steel heat exchanger. Through the 1950s and 1960s, this style of furnace was used to replace the big, natural draft systems, and was sometimes installed on the existing gravity duct work. The heated air was moved by blowers which were belt driven and designed for a wide range of speeds. These furnaces were still big and bulky compared to modern furnaces, and had heavy-steel exteriors with bolt-on removable panels. Energy efficiency would range anywhere from just over 50% to upward of 65% AFUE. This style furnace still used large, masonry or brick chimneys for flues and was eventually designed to accommodate air-conditioning systems.


=== Forced draft ===
The third category of furnace is the forced draft, mid-efficiency furnace with a steel heat exchanger and multi-speed blower.  These furnaces were physically much more compact than the previous styles.  They were equipped with combustion air blowers that would pull air through the heat exchanger which greatly increased fuel efficiency while allowing the heat exchangers to become smaller.  These furnaces may have multi-speed blowers and were designed to work with central air-conditioning systems.


=== Condensing ===

The fourth category of furnace is the high-efficiency, or condensing furnace.  High-efficiency furnaces can achieve from 89% to 98% fuel efficiency. This style of furnace includes a sealed combustion area, combustion draft inducer and a secondary heat exchanger.  Because the heat exchanger removes most of the heat from the exhaust gas, it actually condenses water vapor and other chemicals (which form a mild acid) as it operates.  The vent pipes are normally installed with PVC pipe versus metal vent pipe to prevent corrosion. The draft inducer allows for the exhaust piping to be routed vertically or horizontally as it exits the structure.  The most efficient arrangement for high-efficiency furnaces includes PVC piping that brings fresh combustion air from the outside of the home directly to the furnace.  Normally the combustion air (fresh air) PVC is routed alongside the exhaust PVC during installation and the pipes exit through a sidewall of the home in the same location. High efficiency furnaces typically deliver a 25% to 35% fuel savings over a 60% AFUE furnace.


== Types of furnace ==


=== Single-stage ===
A single-stage furnace has only one stage of operation, it is either on or off. This means that it is relatively noisy, always running at the highest speed, and always pumping out the hottest air at the highest velocity.
One of the benefits to a single-stage furnace is typically the cost for installation. Single-stage furnaces are relatively inexpensive since the technology is rather simple. However, the simplicity of single-stage gas furnaces come at the cost of blower motor noise and mechanical inefficiency. The blower motors on these single-stage furnaces consume more energy overall because regardless of the heating requirements of the space, the fan and blower motors operate at a fixed-speed.
Due to its One-Speed operation, a single-stage furnace is also called a single-speed furnace.


=== Two-stage ===
A two-stage furnace has to do two stage full speed and half (or reduced) speed. Depending on the demanded heat, they can run at a lower speed most of the time. They can be quieter, move the air at less velocity, and will better keep the desired temperature in the house.


=== Modulating ===
A modulating furnace can modulate the heat output and air velocity nearly continuously, depending on the demanded heat and outside temperature. This means that it only works as much as necessary and therefore saves energy.


== Heat distribution ==
The furnace transfers heat to the living space of the building through an intermediary distribution system. If the distribution is through hot water (or other fluid) or through steam, then the furnace is more commonly called a boiler. One advantage of a boiler is that the furnace can provide hot water for bathing and washing dishes, rather than requiring a separate water heater. One disadvantage to this type of application is when the boiler breaks down, neither heating nor domestic hot water are available.
Air convection heating systems have been in use for over a century.  Older systems rely on a passive air circulation system where the greater density of cooler air causes it to sink into the furnace area below, through air return registers in the floor, and the lesser density of warmed air causes it to rise in the ductwork; the two forces acting together to drive air circulation in a system termed 'gravity-fed'. The layout of these 'octopus’ furnaces and their duct systems is optimized with various diameters of large dampered ducts.

By comparison, most modern "warm air" furnaces typically use a fan to circulate air to the rooms of house and pull cooler air back to the furnace for reheating; this is called forced-air heat. Because the fan easily overcomes the resistance of the ductwork, the arrangement of ducts can be far more flexible than the octopus of old. In American practice, separate ducts collect cool air to be returned to the furnace. At the furnace, cool air passes into the furnace, usually through an air filter, through the blower, then through the heat exchanger of the furnace, whence it is blown throughout the building. One major advantage of this type of system is that it also enables easy installation of central air conditioning, simply by adding a cooling coil at the outlet of the furnace.
Air is circulated through ductwork, which may be made of sheet metal or plastic "flex" duct, and is insulated or uninsulated. Unless the ducts and plenum have been sealed using mastic or foil duct tape, the ductwork is likely to have a high leakage of conditioned air, possibly into unconditioned spaces. Another cause of wasted energy is the installation of ductwork in unheated areas, such as attics and crawl spaces; or ductwork of air conditioning systems in attics in warm climates.


== Furnace room ==
A furnace room is a mechanical room in a building for locating a furnace and auxiliary equipment. Such a room minimizes the visual impact of the furnace, pipes and other gear. A modern compact furnace for a single family home can readily fit into a small closet. However, care must be exercised to provide adequate ventilation as the exterior of the furnace unit emits a significant amount of heat, and a natural gas or any other fueled furnace will need an adequate amount of combustion air. 
A garage should never be used as a furnace room for a number of reasons. Air leakage around the connecting ductwork and other needed passages could act to transport potentially dangerous contaminants (including carbon monoxide) from the garage into the main body of the house, ductwork and other passages between the garage and the living areas of the house could be breaches of the required fire resistant barrier between these two areas, a furnace or other such appliance would need to be protected from potential vehicle impact by bollards or some other means, and any ignition source in a garage is required to be at least 18 inches above the floor level due to the potential of explosive gasoline vapors in any garage. In the picture, the airflow is from bottom to top, with an electronic air filter on the bottom, followed by a natural gas high efficiency or condensing furnace (98% efficient) in the middle, and air conditioning coils on top.


== See also ==
Forced-air gas
Jetstream furnace
Outdoor wood-fired boiler
Masonry heater


== Notes ==


== Reference ==