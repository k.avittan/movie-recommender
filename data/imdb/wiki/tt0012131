Return to Eden is an Australian television drama series starring Rebecca Gilling, James Reyne (of Australian Crawl fame), Wendy Hughes and James Smillie. It began as a three-part, six-hour mini-series shown on Network Ten on 27–29 September 1983. Gilling and Smillie, now joined by Peta Toppano would reprise their roles for a 22-part weekly series that began airing on 10 February 1986.
Return to Eden was created by Michael Laurence, and was also an international success, particularly in France where it has been shown 13 times.


== Mini-series (1983) ==


=== Synopsis ===
My name is Stephanie Harper, and this is Eden – here my father was king. On the day he died 17 years ago, I was 23, lonely and afraid. If I'd known then of the nightmare that lay ahead, I think I'd have chosen to die with him.
Stephanie Harper (Gilling) is a dull, frumpy 40-year-old heiress. Rich but insecure, and with two failed marriages behind her, she marries a handsome but unscrupulous younger man, tennis pro Greg Marsden (Reyne). Stephanie believes that she has found true love, but after the wedding, Greg promptly begins an affair with Stephanie's best friend Jilly Stewart (Hughes). Greg then plots to get rid of Stephanie and lay claim to her fortune. Whilst on their honeymoon, Greg pushes Stephanie into a crocodile-infested swamp, and he and Jilly watch as she is apparently mauled to death.
However, Stephanie miraculously survives and is later found washed up on a river bank by hermit Dave Welles (Kerr). Dave nurses Stephanie back to health, but her face and body are horrifically scarred from the encounter with the crocodile. Dave gives her precious gems that he mined locally, so that she can use them to try to move on with her life. Stephanie sells the gems and goes to an island clinic where she meets Dr. Dan Marshall (Smillie), a brilliant plastic surgeon who uses his talents to repair her face and body. After months of operations and physical exercise, Stephanie has been transformed into a very beautiful woman. Using the new alias of Tara Welles, given to her by Dave, Stephanie returns to Sydney and becomes a glamorous supermodel who later appears on the cover of Vogue. Using her new identity and fortune, she plots her revenge on both Greg and Jilly and aims to take back what is rightfully hers, particularly her beloved family home Eden, a vast mansion estate in the Northern Territory.


=== Production ===
The mini-series was shot in Sydney, the Darling Downs, the Barrier Reef and Arnhem Land and had a budget of AUS$2.5 million. The Eden property itself was shot on location at Jimbour Homestead. American director Karen Arthur directed the mini-series.


== Weekly series (1986) ==
Such was the enormous success of the mini-series that Return to Eden returned as a regular weekly series. Produced in 1985 and screened in 1986, the weekly series had a budget of AUS$8 million.The events of the series take place seven years after the end of the mini-series, with Stephanie now married to Dan Marshall and living at New Eden, a mansion in Sydney. Stephanie's children, Sarah and Dennis, both teenagers in the mini-series, are now adults, played by Nicki Paull and Peter Cousens. The opening episode featured Jilly's release from prison and the discovery that she is Stephanie's half-sister. Ongoing storylines dealt with her bitter jealousy and rivalry with Stephanie as she strives to take what she believes should rightfully be hers. The most prominent storyline was Stephanie's continued attempts to keep control of the family company, Harper Mining, which ruthless businessman Jake Sanders (Abineri) was trying to take away from her. It was revealed that Jake was in fact the late Greg Marsden's brother, out to avenge his death, and he teamed up with Jilly to eventuate his goal.  Rebecca Gilling and James Smillie reprised their roles from the original mini-series, while Peta Toppano replaced Wendy Hughes as Jilly, playing her as a glamorous, scheming "superbitch", much in the style of Dynasty's Alexis Colby.


=== Ending ===
The final episode screened in Australia on 8 July 1986 concluded with a cliffhanger ending in anticipation of a second season. Closing scenes featured Dennis being knocked unconscious and kidnapped by an unseen assailant, meanwhile, upstairs at Stephanie's mansion while a party went on downstairs, Jilly pulled a gun on Stephanie in another attempt to kill her. Jake attempted to wrest the gun from Jilly but the gun went off in the struggle, fatally wounding him and splattering Stephanie with his blood. Stephanie then grabbed the gun away from Jilly as Jake careened into the hallway, where Stephanie briefly held him. He careened down the hallway before falling down the stairs where he dies in full view of the shocked party guests. A horrified Stephanie, with blood on her face and clothes and still holding the gun in her hand, appears at the stairway. Jilly then appears and descending the stairs shouting after Jake she tells everyone that she just saw Stephanie shoot him.
The show's ratings had not been high enough in Australia to justify a renewal so this cliffhanger was never initially resolved for Australian audiences. The actors playing Stephanie, Jilly and Dennis were later brought back to film a hasty five-minute conclusion to the various unresolved story threads that was added to the end of the final episode. Dennis' abductor, revealed as his half-brother and Stephanie's long-lost son Chris (Harvey), quickly abandoned his plan to hold Dennis for ransom when Dennis told him that as Ahmal's son he would stand to inherit millions. Chris knew Stephanie had been arrested for Jake's murder. He had been following Stephanie so had witnessed the shooting incident from the bedroom balcony, and had taken photographs that proved what had really happened. He quickly cleared Stephanie who was released while Jilly was arrested for Jake's murder.
This new ending was never seen in the original Australian run and only included in the version of the last episode that was sold internationally. Network Ten repeated the series twice in the 1990s and at least one screening contained the revised ending.


=== Analysis ===
The 1986 series of Return to Eden was clearly intended as an Australian answer to the glamorous American "supersoaps" Dallas, Dynasty and Falcon Crest, with the requisite ostentatious fashions, boardroom struggles, bedroom romps and outrageous storylines. It is fair to argue that Return to Eden successfully delivered all the stock soap pleasures with the Australian setting giving the show a certain ironic distance.


== International distribution ==


=== United Kingdom ===
Return to Eden was screened on ITV and was a huge ratings hit during the summer of 1986 (the three-part mini-series had been screened in September 1984). The 1986 UK transmission also ended with the original cliffhanger, as did the 1989 repeat screening. However, the newer ending was screened many years later when the series was repeated on UK satellite television, primarily the now defunct Granada Plus, which showed it twice. It was shown again in the UK in 2010 by the satellite channel CBS Drama, which showed both the original 1983 mini-series and immediately followed this with the 1986 sequel series which included the revised ending.


=== India ===
Return to Eden was remade in India as the 1988 film Khoon Bhari Maang. It starred Rekha was directed and produced by Rakesh Roshan. The film was a critical as well as box office success.


== Cast ==
Mini-series (1983)

Weekly series (1986)


== Home media ==
Both the original mini-series and the follow-up weekly series have been made available on DVD in Australia.  Kaleidoscope Film has released the original 1983 mini-series as two "Region 0" discs. Unfortunately, even though the packaging states "Digitally Enhanced", the picture quality of the discs actually leaves a lot to be desired and is reproduced from many generations of copies down from the master. Bonus extras on the discs include interviews with Rebecca Gilling and James Reyne in 2001.
The 1985 series was released as a six-disc set in Australia by MRA Entertainment. Although the picture quality is generally better than the mini-series, there is some tape distortion and interference on some of the episodes during the series. The episodes end on the newer 'zoom-in' Worldvision logo (present after the company was sold to Spelling Entertainment in 1988) except for the final episode which uses the original 'scroll' logo and is obviously the originally aired episode. The DVD boxset does not include the revised ending, leaving the last episode with the unresolved original cliffhanger.
The series has also been released on DVD in the Netherlands in a nine-disc set, which contains the revised ending.
In 2018 MediumRare released the original mini-series in a two disc boxset. The picture quality in this release is a vast improvement over the previous Kaleidoscope release.
Tentatively scheduled for November 2020 in Australia is a new eleven disc release of the original mini-series & the 1985 sequel series.  Produced by Via Vision this latest re-release of the show can be pre-ordered direct from the Sanity website in Australia.


== Remake attempt ==
In 2012, the Nine Network announced it was planning to remake Return to Eden as a six-hour mini-series, however in March 2013 it was announced that the planned remake had been shelved for the time being due to financial reasons.


== Soundtrack ==
A soundtrack album for the series, featuring music composed by Brian May (not to be confused with the Queen guitarist) was released on vinyl during the show's original airing. It was reissued on  compact disc on the Buy Soundtrax label in 2010.

Notes about the CD:  Track 1 is actually the feature-length opening theme of Return to Eden the series, used in the first episode. Although Track 19 is labelled "Closing Titles", it is actually the series' regular opening theme music.


== References ==


== External links ==
Return to Eden (1983 miniseries) on IMDb
Return to Eden (1986 series) on IMDb
Return to Eden at the National Film and Sound Archive