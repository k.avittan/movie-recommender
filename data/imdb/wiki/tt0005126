The superior officer (SO) or sometimes, if the incumbent is a general officer, commanding general (CG), is the officer in command of a military unit. The commanding officer has ultimate authority over the unit, and is usually given wide latitude to run the unit as they see fit, within the bounds of military law. In this respect, commanding officers have significant responsibilities (for example, the use of force, finances, equipment, the Geneva Conventions), duties (to higher authority, mission effectiveness, duty of care to personnel), and powers (for example, discipline and punishment of personnel within certain limits of military law).
In some countries, commanding officers may be of any commissioned rank. Usually, there are more officers than command positions available, and time spent in command is generally a key aspect of promotion, so the role of commanding officer is highly valued. The commanding officer is often assisted by an executive officer (XO) or second-in-command (2i/c), who handles personnel and day-to-day matters, and a senior enlisted advisor. Larger units may also have staff officers responsible for various responsibilities.


== Commonwealth ==


=== Army ===
In the British Army, Royal Marines, and many other Commonwealth military and paramilitary organisations, the commanding officer of a unit is appointed. Thus the office of CO is an appointment.
The appointment of commanding officer is exclusive to commanders of major units (regiments, battalions and similar sized units). It is customary for a commanding officer to hold the rank of lieutenant colonel, and they are usually referred to within the unit simply as "the colonel" or the CO. "The colonel" may also refer to the holder of an honorary appointment of a senior officer who oversees the non-operational affairs of a regiment. However, the rank of the appointment holder and the holder's appointment are separate. That is, not all lieutenant colonels are COs, and although most COs are lieutenant colonels, that is not a requirement of the appointment.
Sub-units and minor units (companies, squadrons and batteries) and formations (brigades, divisions, corps and armies) do not have a commanding officer. The officer in command of a minor unit holds the appointment of "officer commanding" (OC). Higher formations have a commander (usual for a brigade) or a general officer commanding (GOC). Area commands have a commander-in-chief (e.g. C-in-C Land Army, C-in-C British Army of the Rhine). The OC of a sub-unit or minor unit is today customarily a major (although formerly usually a captain in infantry companies and often also in cavalry squadrons), although again the rank of the appointment holder and the holder's appointment are separate and independent of each other.
In some cases, independent units smaller than a sub-unit (e.g. a military police platoon that reports directly to a formation such as a brigade) will also have an OC appointed. In these cases, the officer commanding can be a captain or even a lieutenant.
Appointments such as CO and OC may have specific powers associated with them. For example, they may have statutory powers to promote soldiers or to deal with certain disciplinary offences and award certain punishments. The CO of a unit may have the power to sentence an offender to 28 days' detention, whereas the OC of a sub-unit may have the power to sentence an offender to 3 days' restriction of privileges.
Commanders of units smaller than sub-units (e.g. platoons, troops and sections) are not specific appointments and officers or NCOs who fill those positions are simply referred to as the commander or leader (e.g. platoon commander, troop leader, section commander/leader, etc).


=== Royal Air Force ===
In the Royal Air Force, the title of commanding officer is reserved for station commanders or commanders of independent units, including flying squadrons. As with the British Army, the post of a commander of a lesser unit such as an administrative wing, squadron or flight is referred to as the officer commanding (OC).


=== Royal Navy ===
In the Royal Navy and many others, commanding officer is the official title of the commander of any ship, unit or installation. However, they are referred to as "the captain" no matter what their actual rank, or informally as "skipper" or even "boss".


== United States ==
In the United States, the status of commanding officer is duly applied to all commissioned officers who hold lawful command over a military unit, ship, or installation.


=== Army ===
The commanding officer of a company, usually a captain, is referred to as the company commander (or the battery/troop commander for artillery/cavalry). The commanding officer of a battalion (or squadron of cavalry) is usually a lieutenant colonel. The commanding officer of a brigade, a colonel, is the brigade commander. At the division level and higher, however, the commanding officer is referred to as the commanding general, as these officers hold general officer rank.
Although holding a leadership position in the same sense as commanders, the individual in charge of a platoon, the smallest unit of soldiers led by a commissioned officer, typically a second lieutenant, is referred to as the platoon leader, not the platoon commander. This officer does have command of the soldiers under him but does not have many of the command responsibilities inherent to higher echelons. For example, a platoon leader cannot issue non-judicial punishment.
Non-commissioned officers may be said to have charge of certain smaller military units. They cannot, however, hold command as they lack the requisite authority granted by the head of state to do so. Those wielding "command" of individual vehicles (and their crews) are called vehicle commanders. This distinction in title also applies to officers who are aircraft commanders ("pilot in command"), as well as officers and enlisted soldiers who are tank and armored vehicle commanders. While these officers and NCOs have tactical and operational command (including full authority, responsibility, and accountability – especially in the case of aircraft commanders) of the soldiers and equipment in their charge, they are not accorded the legal authority of a "commanding officer" under the UCMJ or military regulations.
Warrant officers in the United States Armed Forces are single career-track officers that can, and occasionally do, hold command positions within certain specialty units, i.e. Special Forces and Army Aviation. However, warrant officers usually do not command if a commissioned officer is present; normally they serve as executive officer (2IC).


=== Marine Corps ===
The commanding officer of a company, usually a captain, is referred to as the company commander or the battery commander (for field artillery and low altitude air defense units). The commanding officer of a battalion or a squadron (Marine aviation), is usually a lieutenant colonel. The commanding officer of a regiment, aviation group, or Marine Expeditionary Unit (MEU), a colonel, is the regimental/group/MEU commander. At the Marine Expeditionary Brigade (MEB), Marine Logistics Group (MLG), Marine Division (MARDIV), Marine Aircraft Wing (MAW), Marine Expeditionary Force (MEF), and Fleet Marine Force (FMF) levels; however, the commanding officer is referred to as the commanding general, as these officers hold general officer rank.
The officer in charge of a platoon, the smallest tactical unit of Marines usually led by a commissioned officer, typically a first or second lieutenant, is referred to as the platoon commander. This distinction in title also applies to officers who are aircraft commanders, as well as officers, staff non-commissioned officers (staff sergeant – master sergeant), and non-commissioned officers (corporal and sergeant) who are tank and armored vehicle commanders. While these officers, SNCOs, and NCOs have tactical and operational command (including full authority, responsibility, and accountability—especially in the case of aircraft commanders) of the Marines and equipment in their charge, they are not accorded the legal authority of a "commanding officer" under the UCMJ or military regulations.


=== Navy ===
In the United States Navy, commanding officer is the official title of the commander of a ship, but they are usually referred to as "the Captain" regardless of their actual rank: "Any naval officer who commands a ship, submarine or other vessel is addressed by naval custom as "captain" while aboard in command, regardless of their actual rank." They may be informally referred to as "Skipper", though allowing or forbidding the use of this form of address is the commanding officer's prerogative.


=== Air Force ===
In the United States Air Force, the commanding officer of a unit is similarly referred to as the unit commander, such as squadron commander, group commander, wing commander, and so forth.


== See also ==
Command and control
Commander of the Royal Canadian Air Force
Staff (military)


== References ==