"The Little Match Girl" (Danish: Den Lille Pige med Svovlstikkerne, meaning "The little girl with the matchsticks") is a literary fairy tale by Danish poet and author Hans Christian Andersen. The story, about a dying child's dreams and hope, was first published in 1845. It has been adapted to various media, including animated and live-action films, television musicals, and video games.


== Summary ==

On a freezing New Year's Eve a poor young girl, shivering and barefoot, tries to sell matches in the street. Afraid to go home because her father will beat her for failing to sell any matches, she huddles in the angle between two houses and lights matches to warm herself.
In the flame of the matches she sees a series of comforting visions: a warm stove, a holiday feast, a happy family, and a Christmas tree. In the sky she sees a shooting star, which her late grandmother had told her means someone is on their way to Heaven. In the flame of the next match she sees her grandmother, the only person to have treated her with love and kindness. To keep the vision of her grandmother alive as long as possible, the girl lights the entire bundle of matches.
When the matches are gone the girl dies, and her grandmother carries her soul to Heaven. The next morning, passers-by find the girl frozen, and express pity. They do not know about the wonderful visions she had seen, or how happy she is with her grandmother in heaven.


== Publication ==
"The Little Match Girl" was first published December 1845, in Dansk Folkekalender for 1846. The work was re-published as a part of New Fairy Tales (4 March 1848), Second Volume, Second Collection (Nye Eventyr (1848), Andet Bind, Anden Samling), and again 18 December 1849 as a part of Fairy Tales (1850; Eventyr). The work was also published 30 March 1863 as a part of Fairy Tales and Stories (1863), Second Volume (Eventyr og Historier (1863), Andet Bind).


== Adaptations ==


=== Amusement park attractions ===

The Fairy Tale Forest (Sprookjesbos in Dutch) of the amusement park Efteling in the Netherlands has a three-dimensional attraction showing the story of the Little Match Girl, called Het Meisje met de Zwavelstokjes. In this attraction, use is made of the Pepper's ghost technique.


=== Anime and manga ===
In the episode 307 of Crayon Shin-chan, "Nene-chan is the Tragedy Heroine" (1999), the story inspires Nene-chan to play the Cinderella game with her friends.
In Is the Order a Rabbit?, Sharo starts daydreaming while handing out flyers, humorously seeing it as a death flag when she connects her actions to the match girl.
Chapter 18 of the manga series Binbou Shimai Monogatari (2004) replays the tale of "The Little Match Girl", featuring the protagonists Asu and Kyou with a happy ending twist.
In Chapter 24 (Volume 3) of Love Hina, Su makes Shinobu dress up as a Little Red Riding Hood type and sell matches to raise some travelling money to Okinawa. When that plot initially fails and Shinobu starts to cry, a good number of passers-by are moved to tears and prepare to buy all her matches until the two girls are chased off by resident Yakuza.
In the Japanese anime Gakuen Alice, the main character, Mikan Sakura puts on a play about The Little Match Girl to earn money.
Episode 201 of Gin Tama, "Everybody's a Santa", parodies The Little Match Girl, where Yagyu Kyubei narrates a humorous retelling of the story, featuring Kagura as the eponymous title character, replacing match sticks with Shinpachi, a human punching bag.
"Girl Who Doesn't Sell Matches But is Misfortunate Anyway" is the final episode of the 2010 anime series Ōkami-san, which draws inspiration from various fairy tales. The episode features a character called Machiko Himura, who is based on the little match girl.
"The Little Key Frames Girl", episode 11 of the anime Shirobako (2014), humorously replays the whole match girl story from a more modern and lower stakes point of view.
In "Christmas Osomatsu-san", episode 11 of the anime Osomatsu-san (2015), Iyami humorously acts as The Little Match Girl, dying in the end.
Match Shoujo, a manga by Sanami Suzuki (2014–15), is being made into a live-action film starring Sumire Sato, as the title character.
In "Let's Get Wiggy With It", episode 2 of the anime Bobobo-bo Bo-bobo (2003–05), Don Patch humorously recites a story of him selling churros at Christmas time with no one buying, showing a Churro buried and covered in snow in the end, resembling death.
In "Troupe Dragon, On Stage! (They Had A Troupe Name, Huh)", episode 10 of the anime Miss Kobayashi's Dragon Maid (2017), the main characters decide to stage a performance of "The Little Match Girl" for a nursing home on Christmas. Throughout the episode, the characters add their own ideas to the story (such as magical girls and the Forty-seven rōnin), to the point that the performance bears virtually no resemblance to the original.
In "Yuru Yuri" Season 3 episode 10, Akari and Kyoko light matches to keep themselves warm when the Kotatsu does not work. They see visions of shaved ice and a turkey dinner. They both survive however.
The cover art for chapter 43 (vol. 3) of Komi-san wa, Komyushou Desu. features Komi-san dressed up as the little match girl in a snowy street holding a lit match.
In Isekai Quartet 2 episode 11, Yunyun (a recurring character originally from KONOSUBA whose running joke is that she is always alone) acts out a play version of the story solo.
In Flint The Time Detective the main characters visit Hans Christian Andersen who is trapped in his own dreams and having his own creations acting oddly. Among them, the Little Match Girls is selling watches instead of matches.


=== Audio recordings ===
The record "Charles Dickens: A Christmas Carol" published by Peter Pan Records features a reading on the B side.
The Myths and Legends Podcast featured the Little Match Girl story in a 2020 episode centering on Hans Christian Andersen's fairy tales.


=== Comics ===
In issue #112 of Bill Willingham's Fables (a comic book series about living embodiments of storybook characters), The Little Match Girl is introduced to Rose Red as one of the paladins of the embodiment of Hope, ostensibly on the night that the girl is doomed to die (Christmas Eve, in this telling). The child identifies herself as "the caretaker of hope deferred", braving the deadly cold and saving the meager pennies she earns towards the promise of a better life in the future, and stubbornly denying that her death is close at hand.


=== Films ===


==== 16mm short subject films ====
In 1954, Castle Films released a 16 mm English language version of a 1952 black and white French short live-action film. Instead of her grandmother, the Virgin Mary, whom the match girl believes is her own long-lost mother, takes the girl to Heaven. No mention is made of the father beating the child.


==== Animated films ====
Color Rhapsodies (1937), a Charles Mintz studio color cartoon adaptation set in 1930s New York City, directed by Arthur Davis and Sid Marcus, and considered among the studio's best films. It was nominated for the 1937 Academy Award for Best Short Subject (Cartoons), though it lost to Disney's short The Old Mill. This version of the story is slightly different from Andersen's story, specifically near the end.
La Tartelette (1967), a Jacques Colombat short film inspired by Andersen's story, with a black humour twist ending.
Hans Christian Andersen no Sekai (1971, The World of Hans Christian Andersen), Toei Animation's animated film based on Andersen's works.
In the 1978 animated adaptation of "The Stingiest Man In Town", the character of Ebenezer Scrooge is seen passing by a caricature of the little match girl. Her first appearance during the musical number "The Stingiest Man in Town" shows Scrooge "buying" two packs of matches from her, only to pay her with a button from his coat and go on his way laughing at her expense, leaving her bewildered. Her second, and final, appearance in the special is in the musical number "Mankind Should Be My Business", it shows her shivering from the cold and a now-reformed Scrooge giving her his coat and a handful of gold coins.
The Little Match Girl (2003), an animated short film by Junho Chung for Fine Cut: KCET's Festival of Student Film.
"The Little Match Girl" (2005), ADV Films' adaptation released in Hello Kitty Animation Theater, Vol. 3.
The Little Match Girl (2006), the last of four Walt Disney Animation Studios shorts originally intended to be part of a Fantasia (2006)  compilation film, which project was canceled. This short was then developed as a stand-alone film and was nominated for the 2006 Academy Award for Best Animated Short, losing to The Danish Poet. This short was subsequently released as a special feature on the 2006 Platinum Edition DVD of The Little Mermaid (1989). In 2015, the short was released on the Walt Disney Animation Studios Short Films Collection Blu-ray Disc.
"Allumette" (2016) by Penrose Studios Released as a free VR animated film for PS4 and PC.


==== Live-action films ====
The Little Match Seller (1902), a short silent film directed by James Williamson
The Little Match Girl (1914), a 9 minute long silent film directed by Percy Nash.
The Little Match Girl (1928), La Petite Marchande d'Allumettes), a 40-minute silent film by Jean Renoir.
La Jeune Fille aux Allumettes (1952), French director Jean Benoît-Lévy's film version, includes a brief dance sequence with ballet star Janine Charrat.
Den lille pige med svovlstikkerne (1953), Danish film director Johan Jacobsen adapted the Andersen story to a short film, starring Karin Nellemose, Françoise Rosay, and Agnes Thorberg Wieth. The film competed for the short film prize of the 1954 Cannes Film Festival.
La vendedora de rosas (1998; Little Rose Selling Girl), directed by Víctor Gaviria, is a film by Colombian movie about homeless children victims of solvent abuse, loosely based on "The Little Match Girl"; it competed for the Palme d'Or at the 1998 Cannes Film Festival.
Resurrection of the Little Match Girl (2003) is a Korean movie.
Matchstick Girl (2015), a modern adaptation short film set in the UK, was produced and directed by Joann Randles.
Match Shojo (2016), a Japanese adaptation of Sanami Suzuki's manga starring Sumire Sato.


=== Games ===
Suikoden III, (2002), a video game for the PlayStation 2, contains a highly abridged play version of "The Little Match Girl". In the game, the player can cast characters in different roles and have them perform a shortened version of the story.
Yakuza 5, (2012), a video game for the PlayStation 3 has a substory named "The Little Match Girl" during Taiga Saejima's segment of the game that involves a little girl selling matches for 100 yen. A similar substory also appears in Yakuza (2005) and its remake, Yakuza Kiwami (2016).
The Little Match Girl, (2015), a visual novel for web browsers and Android that tells the story.
In The Witcher 3: Wild Hunt – Blood and Wine, the Little Match Girl, tired of seeing that no one buys her matches, starts selling tobacco, various alcoholic beverages and even drugs, stating that "it is much more profitable and demand is high".
THE iDOLM@STER 2, (2011), a video game for Xbox 360 and PlayStation 3, has a song titled "Little Match Girl" available in the first DLC pack for the Xbox 360 version and available by default in the PlayStation 3 version. The song features a romanticized version of the story. The song has made further appearances in THE iDOLM@STER SHINY FESTA, THE iDOLM@STER ONE FOR ALL, and in the 2011 anime adaptation of the series.
"QURARE Magic Library", (2014), a Korean mobile game that's also been imported to the PlayStation 4 to the west. There is a Kodex card in game called "Little Match Girl" which is a Super Rare + card and features the passive Resurrect V skill for Tank decks. The image is drawn by STUDIO NCG, and its description is very different from the original story, with a much darker ending.
"Dark Parables: The Match Girl`s Lost Paradise", (2018), a fifteenth installment of the Dark Parables franchise. The story involved unexplained fires known as The Flame of Illusion has occurred in Stars Hollow. The recent one involved a rich landowner who was caught in his own home, leaving no traces of bodies or fires anywhere. However, a young girl selling matches was the only solid clue. You, the Fairytale Detective, must stop these fires and the Match Girl.
"Shōjo Kageki Revue Starlight: Re LIVE", (2018), a 4-star card named "The Little Match Girl" is played by Yachiyo Tsuruhime of Siegfeld Institute of Music (voiced by Haruka Kudō). 


=== Literature ===
Anne Bishop published the short story "Match Girl" in Ruby Slippers, Golden Tears (1995).
On page 319 of Clarissa Pinkola Estés' book Women Who Run with the Wolves (1992), "The Little Match Girl", the author tells the story to her aunt, followed by a lucid analysis.
In Neil Gaiman's novella A Study in Emerald (2004), the main characters view a set of three plays, one of which is a stage adaptation of the "Little Match Girl".
Novelist Gregory Maguire read a short story based on "The Little Match Girl" over the air on NPR. He later expanded the short story into a novel, published as Matchless: A Christmas Story (2009).
William McGonagall retold "The Little Match Girl" in a poem.
Jerry Pinkney wrote an adaption of the story setting it in the early twentieth century.
Terry Pratchett's Discworld novel, Hogfather (1996), gave the story a less morbid ending, thanks to the intervention of Death himself: Death's manservant, Albert, notes that the tale of the little match girl is meant to remind people that they could be worse off even when completely penniless, but with Death currently acting as the Hogfather (the Discworld equivalent of Santa Claus) to compensate for the original's absence, he is able to use his current dual status to give the little match girl a gift of a future.
Hans Tseng adapted "The Little Match Girl" into a short story manga, featured in the first volume of Tokyopop's Rising Stars of Manga (2003).
In Anne Ursu's novel Breadcrumbs (2011), the main character Hazel meets in the woods a character based on the Little Match Girl.
"The Little Match Girl" was one of the key inspirations for the main character Rosemary in Michael Sajdak's novelette Voices of the Dead (2017).


=== Music ===
In 1989, Stephen DeCesare composed a song about the famous story and in 2019 re-released it for choral and/or vocal duets.
In 1994, Frederik Magle released the album The Song Is a Fairytale, of songs based on Hans Christian Andersen's fairytales, with Thomas Eje and Niels-Henning Ørsted Pedersen amongst others. "The Little Match Girl" is one of the songs.
In 1988–96, the German avant-garde composer Helmut Lachenmann wrote an opera based on the story, called Das Mädchen mit den Schwefelhölzern, also including a text by Red Army Faction founder Gudrun Ensslin.
In 1995, German singer Meret Becker included the song "Das Mädchen mit den Schwefelhölzern" in her album Noctambule.
In 2001, guitarist Loren Mazzacane Connors released the album The Little Match Girl based on the story.
In 2001, the Hungarian band Tormentor wrote the song "The Little Match Girl," with lyrics based on the story.
In 2002, GrooveLily released the album Striking 12, a musical based on "The Little Match Girl", which subsequently opened off-Broadway in 2006.
In 2005, Erasure's music video of their song "Breathe" was a modern adaptation of the story.
In 2006, the English band The Tiger Lillies and a string trio released the album The Little Match Girl based on the story.
Bulgarian composer Peter Kerkelov wrote a piece for classical guitar titled "The Little Match Girl"
American composer David Lang completed his own rendition of the original story in 2007. The Little Match Girl Passion is scored for four solo voices, soprano, alto, tenor and bass, with percussion, and was written for Paul Hillier and his ensemble Theater of Voices. The work was awarded the Pulitzer Prize in music in 2008. It presents Hans Christian Andersen's tale in Lang's characteristic post-minimalist style with thematic influence from Johann Sebastian Bach's St. John and St. Matthew Passions.
In 2010, Vocaloid producer Akuno-P produced the song "The Flames of the Yellow Phosphorus", an alternate take on the story. In the song, the girl must sell all the matches she has, or else her father will not feed her. After lighting a match, she decides to burn her father's house down and steal his money for food, as to not be cold nor starving. She gets burned at the stake as punishment.
In 2012, The Crüxshadows recorded the song "Matchstick Girl" on their album As the Dark Against My Halo, which according to the band's frontman, Rogue, refers to Hans Christian Andersen's fairy tale.
Tori Amos's 2015 musical The Light Princess includes the song "My Fairy-Story", where the main character reads this story and compares it to her own situation.
In 2015, Japanese techno-rap unit Wednesday Campanella produced the song "Match Uri no Shōjo" (「マッチ売りの少女」, 'The Little Match Girl').
In 2018, Loona (band) Chuu music video of their song "Heart Attack (Loona song)" was an adaptation of the story.


=== Television ===
In 1974, a contemporarized version set in Cincinnati on Christmas Eve was aired on WLWT. This Christmas special was placed in syndication and last aired on the Family Channel in December 1982. It is notable for featuring a then-9 year old Sarah Jessica Parker.
In 1986, HTV released The Little Match Girl as a musical based on the original story. The cast included Twiggy and Roger Daltrey. It included the song "Mistletoe and Wine", which became a Christmas hit in the UK two years later for Cliff Richard.
In 1987, a modernized version, The Little Match Girl, was shown on American television. The cast included Keshia Knight Pulliam, Rue McClanahan, and William Daniels.
In 2009, a modernized version set to original music and narrated by F. Murray Abraham was presented by HBO Storybook Musicals, in which the girl is the daughter of a homeless New York couple forced to live underground in an abandoned subway station due to the economic collapse of the 1990s.
A short parody version of The Little Match Girl was featured in the Robot Chicken episode "Garbage Sushi" with the Little Match Girl voiced by Minae Noji, her father voiced by Rob Paulsen, and her grandmother voiced by Seth Green. In this sketch version, she was selling matches stating that she must sell them or her father will beat her. Upon lighting a match, she sees a family. Upon lighting the second match, she sees a vision of her grandmother who tells the Little Match Girl that Vishnu is in Heaven and that Jesus is a fairy tale. Upon being told of her grandmother's plan, the girl learns the power of fire with the matches and kills her father for his abuse upon spilling alcohol on him and then using her matches to burn him to a skeleton. Then she finds her grandmother's jewelry and cracks an egg on her father's burning skeleton. By the final scene, the Little Match Girl is in a warmer location where she gives the bartender a big stack of money for a cup of Mai Tai. When asked what her name is, the girl states "My name's the Little F****** Match Girl" and then throws the cup of Mai Tai at the screen.
In 2017, a version of The Little Match Girl was featured on Christmas with the Mormon Tabernacle Choir featuring  Rolando Villazon televised on PBS.
The tale of the Young Matchgirl and the Toothless Goon was recreated through song by Rhod Gilbert on the original Dave TV show Taskmaster.


== See also ==

List of works by Hans Christian Andersen
Near death experience
Vilhelm Pedersen, the first illustrator of Andersen's fairy tales
"To Build a Fire", two versions of a short story by Jack London about an adult who suffers from hypothermia in the Yukon Territory
Child labour


== References ==


== External links ==
"The Little Match Girl" Jean Hersholt's English translation
"The Little Match Girl" in the New Normal Reader
Den Lille Pige med Svovlstikkerne Original Danish text
Surlalune: The Annotated Little Match Girl
"The Little Matchgirl" Creative Commons audiobook
"The Little Match Girl 1902 Adaptation (downloadable)" at the British Film Institute
David Lang's passion
English translation (full text) from "Andersen's Fairy Tales"
 The Little Match Girl public domain audiobook at LibriVox