Where Am I Going? is the third studio album by singer Dusty Springfield, released on Philips Records in the UK in 1967. By now, firmly established as one of the most popular singers in Britain, with several hits in America as well, Springfield ventured into more varying styles than before and recorded a wide variety of material for this album. Rather than the straightforward pop of A Girl Called Dusty or the mix of pop and soul of Ev'rything's Coming Up Dusty, Springfield recorded a variety of styles from jazz to soul, to pop and even show tunes (the standout title track, from the musical Sweet Charity). While not the success that her previous two albums were, Where Am I Going? was praised by fans and critics alike for showing a mature and sophisticated sensibility, despite the many different styles of music.
In the United States, the album was released in a quite altered form as The Look of Love, titled after a successful Springfield recording originally only released as a single B-side in Britain. Also included were two other singles — "Give Me Time", a U.K. and United States hit, and "What's It Gonna Be", a United States hit — not included on Where Am I Going?. The Look of Love also contained the UK single B-side "Small Town Girl", not on Where Am I Going?.
Where Am I Going? was released to CD for the first time in the early 1990s. A digitally remastered edition was released in 1998, and included the bonus tracks "I've Got a Good Thing", "Don't Forget About Me" (a later version appeared on Dusty in Memphis), and "Time After Time".
"Where Am I Going?" was sampled in the song "Zenophile", from the 2004 album Destroy Rock & Roll, by Scottish electronic music artist Mylo. "I Can't Wait Until I See My Baby's Face" was sampled in the song "Nothing Can Stop Us" from the 1991 album Foxbase Alpha by Saint Etienne.


== Track listing ==
Side A

"Bring Him Back" (Mort Shuman, Doc Pomus) - 2:18
"Don't Let Me Lose This Dream" (Aretha Franklin, Ted White) - 2:28
"I Can't Wait Until I See My Baby's Face" (Jerry Ragovoy, Chip Taylor) - 2:43
"Take Me for a Little While" (Trade Martin) - 2:25
"Chained to a Memory" (Kay Rogers, Richard Ahlert) - 2:39
"Sunny" (Bobby Hebb) - 1:58Side B

"(They Long to Be) Close to You" (Burt Bacharach, Hal David) - 2:33
"Welcome Home" (Chip Taylor) - 2:42
"Come Back to Me" (Alan Jay Lerner, Burton Lane) - 2:16
"If You Go Away (Ne Me Quitte Pas)" (Jacques Brel, Rod McKuen) - 4:05
"Broken Blossoms" (Traditional, Tom Springfield, Robert Gray) - 2:45
"Where Am I Going?" (Dorothy Fields, Cy Coleman) - 3:42Bonus tracks 1998 CD re-issue

"I've Got a Good Thing" (Jerry Ragovoy, Mort Shuman) - 2:52
Outtake from the Where Am I Going? sessions. First release: UK compilation Something Special, 1996.
"Don't Forget About Me" (London recording) (Gerry Goffin, Carole King) - 2:55
Outtake from the Where Am I Going? sessions. First release: UK compilation Something Special, 1996. Springfield would record a substantially re-arranged version of this song for her 1969 album Dusty in Memphis.
"Time After Time" (Jule Styne, Sammy Cahn) - 2:34
Outtake from the Where Am I Going? sessions. First release: UK 4-CD box set The Legend of Dusty Springfield, 1994.Track 13: Originally recorded in New York 1965, producer: Jerry Ragovoy. Additional overdubs / re-recording London 1967, producer: Johnny Franz.
Track 14: Re-recorded in 1968 for album Dusty in Memphis.
Tracks 14-15: Producer: Johnny Franz.


== Personnel and production ==
Dusty Springfield - lead vocals, backing vocals
Lesley Duncan - backing vocals
Madeline Bell - backing vocals
The Echoes - accompaniment
Alan Tew - accompaniment & orchestra director
Arthur Greenslade - accompaniment & orchestra director
Ivor Raymonde - accompaniment direction
Wally Stott - accompaniment & orchestra director
Peter Knight - accompaniment & orchestra director
Reg Guest - accompaniment & orchestra director
Pat Williams - musical arranger
Jim Tyler - musical arranger
Johnnie Spence - musical arranger
Johnny Franz - record producer
Roger Wake - digital remastering/remix (1998 re-issue)
Mike Gill - digital remastering/remix (1998 re-issue)


== References ==

Howes, Paul (2001). The Complete Dusty Springfield. London: Reynolds & Hearn Ltd. ISBN 1-903111-24-2.