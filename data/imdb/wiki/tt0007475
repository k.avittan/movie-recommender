Disaster Movie is a 2008 American parody film written and directed by Jason Friedberg and Aaron Seltzer. It stars Matt Lanter, Vanessa Minnillo, Gary "G Thang" Johnson, Crista Flanagan, Nicole Parker, Ike Barinholtz, Carmen Electra, Tony Cox, and Kim Kardashian in her feature film debut. Released on August 29, 2008 by Lionsgate, the film is a parody of the disaster film genre and pop culture.
The film was panned by critics and audiences for its forced humor, excessive references and poor directing, with many considering it to be worse than the previous movies created by Seltzer and Friedberg, as well as one of the worst films of all time. The movie received six nominations for the 29th Golden Raspberry Awards. It grossed nearly $35 million against a budget of $20 million.


== Plot ==
In the year 10,001 B.C., a caveman runs away from a predator through a plain and immediately gets into a fight with Wolf (Ike Barinholtz). After defeating him, the caveman then encounters the predator, a saber-toothed, gasoline-drinking Amy Winehouse (Nicole Parker), who informs him that the world will end on August 29, 2008 and that their fate lies in a Crystal Skull.
The sequence is then revealed to be a dream of everyman Will (Matt Lanter) in the present day. He then finds out that his girlfriend Amy (Vanessa Minnillo) is having an affair with Flavor Flav (Abe Spigner), and she breaks up with Will because he is not admitting his true feelings for her.
Later that day, Will has a "Super Duper Sweet Sixteen" party at his house, despite being 25. The guests include Juney (Crista Flanagan), Dr. Phil (John Di Domenico), Will's best friend Calvin (Gary "G Thang" Johnson), and Anton Chigurh (Barinholtz), among others. During the party, Amy arrives with her new boyfriend, a Calvin Klein underwear model. The party then comes to a halt when the room shakes and the lights go out. A bulletin on the radio claims there is a meteor shower and it is the end of the world. Soon after, the city starts to freeze over, and Will, Juney, Calvin, and Calvin's girlfriend Lisa (Kim Kardashian) retreat to a garage for shelter. When Juney mentions that the calamities are caused by global warming, Will realizes his dream as a caveman could be related. Later, Will is chided by the others for not committing himself to his relationship with Amy.
The gang leaves the garage and Will gets a call from Amy, where he admits his feelings for her before the call is dropped. He decides to go to rescue Amy. Lisa is later killed by a meteor. While the others comfort a distraught Calvin, Giselle (Parker), a prostitute, climbs out of a manhole and gets hit by a taxi. Calvin catches her, and they immediately fall in love with each other. Giselle's pimp, Prince Edwin (Tad Hilgenbrink), challenges Calvin to a dance fight for her love, but a tornado appears and Prince Edwin flees. Iron Man (Gerrard Fachinni), Hellboy (Barinholtz), and the Hulk (Roland Kickinger) attempt to fight it, but all are defeated by cows thrown by the tornado. After taking shelter, Will, Juney, Calvin, and Giselle encounter rabid knockoffs of Alvin and the Chipmunks, who attack the gang and kill Juney. The "Chipmunks" then go after Will and Calvin, but they trap them in a trash can, fatally suffocating them as they attempt to escape.
On their way to the museum where Amy is trapped, the group runs into Batman, who informs them that they must go to evacuation buses and that there will be no chance of survival if they go to save Amy. With time against them, Princess Giselle kills Speed Racer (Jared S. Eddo), and the group hijacks his Mach Five to drive to the museum. At the museum, they save Amy, who reveals that the Crystal Skull is the only thing that can stop the end of the world. Calvin and the Princess then find that the museum doors are closed and all of the artifacts have come alive, including Po from Kung Fu Panda (Yoshio Iizuka), who fights Calvin but is defeated. When Calvin makes out with the Princess, Calvin accidentally pulls her wig and discovers that she is actually a transvestite. While this happens, "Po" takes out a katana and simultaneously kills Calvin and the Princess.
Meanwhile, Will and Amy run into a nude Beowulf (Barinholtz), who fights with Will. After Amy stabs "Beowulf" in the back, Will and Amy encounter Indiana Jones (Tony Cox), who is revealed to be Will's father. "Indy" tries to put the Crystal Skull on the altar, but he flys through a stained glass window in the room. Will does it instead, and he averts further destruction. Will and Amy have a wedding ceremony performed by "The Guru Shitka" (Domenico), which ends with a musical number about all of the characters in the film dating each other (fucking in the unrated version) parodying "I'm Fucking Matt Damon" by Sarah Silverman.


== Cast ==


== Parodies ==


=== Films and TV shows ===
The Simpsons Movie (2007) - film poster
Cloverfield (2008) (Main plot)
Twister (1996)
Indiana Jones and the Kingdom of the Crystal Skull (2008)
10,000 BC (2008)
American Gladiators (2008)
My Super Sweet 16 (2005–2008)
Juno (2007)
Enchanted (2007)
High School Musical (2006)
Alvin and the Chipmunks (2007)
Kung Fu Panda (2008)
The Dark Knight (2008)
The Incredible Hulk (2008)
Step Up 2: The Streets
Iron Man (2008)
Hellboy II: The Golden Army (2008)
Sex and the City (2008)
Hancock (2008)
Superbad (2007)
Night at the Museum (2006)
The Love Guru (2008)
Hannah Montana (2006–2008)
Beowulf (2007)
Speed Racer (2008)
Brokeback Mountain (2005)
I Am Legend (2007)
The Day After Tomorrow (2004)
Jumper (2008)
No Country for Old Men (2007)
Wanted (2008)
The Chronicles of Narnia: Prince Caspian (2008)
Get Smart (2008)


=== Real-life people ===
Miley Cyrus
Amy Winehouse
Michael Jackson
Flavor Flav
Dr. Phil
Jonas Brothers
Jessica Simpson
Sarah Silverman
Matt Damon
Justin Timberlake


== Production ==
On April 2, 2008, less than 3 months after the release of Friedberg and Seltzer's previous spoof film Meet the Spartans, it was announced that the duo were in pre-production on a spoof of Superbad, with the title being Goodie Two Shoes. Unlike the previous Friedberg and Seltzer-directed films, which were financed by Regency Enterprises and distributed by 20th Century Fox, this film was financed by Grosvenor Park and distributed by Lionsgate in the United States.


=== Writing ===
Jason Friedberg and Aaron Seltzer wrote the film as a parody of Superbad, but similar to the duo's previous films, it parodies some of the big blockbusters and popular celebrities from 2007 and 2008. Since many of the films being spoofed were not released at the time that the script was being written, Friedberg and Seltzer used trailer footage to get the basic idea of the films.


=== Casting ===
On April 28, Matt Lanter, Vanessa Minnillo, Gary “G Thang” Johnson and Carmen Electra signed up to be the main cast members.


=== Filming ===
Filming took place in the state of Louisiana from April 28 to June 6. Oscar-nominated production designer William Elliott (who has done production design on the duo films since Date Movie), did the production design for the sets. The Chamber of commerce building in Shreveport was used for filming the natural history museum scenes in the film.


=== Post-production ===
After the filming had ended, Friedberg and Seltzer renamed the film as Disaster Movie.


== Release ==


=== Critical response ===
On Rotten Tomatoes, Disaster Movie has an approval rating of 1% based on 73 reviews with an average rating of 1.76/10. The site's critical consensus reads, "Returning to their seemingly bottomless well of flatulence humor, racial stereotypes, and stale pop culture gags, Jason Friedberg and Aaron Seltzer have produced what is arguably their worst Movie yet." On Metacritic, the film has a score of 15 out of 100 based on reviews from 12 critics, indicating "overwhelming dislike". Audiences polled by CinemaScore gave the film a grade of "F" on an A+ to F scale; as of April 2020, it is one of only 21 films to receive such a rating.It was featured in Empire's 50 Worst Movies Ever poll, Total Film's 66 Worst Movies Ever list and the MRQE's 50 Worst Movies list (where it holds a score of 17, the lowest score on the site). Disaster Movie became the lowest ranked film on IMDb's Bottom 100 list days after its premiere.Jason Solomons of The Guardian wrote that "Nothing can convey the grimness of Disaster Movie, which would be the Worst Movie Ever Made were it actually a movie at all." Adam Tobias of the Watertown Daily Times opined, "I just don't see how anyone could not find Disaster Movie one of the worst films of all time," further adding that the title of the film was appropriate, because the movie is "a disaster."  The Times newspaper named the film the worst of 2008.The only positive review posted on Rotten Tomatoes was by Jim Schembri from Australian newspaper The Age. Schembri called it "dumb but also undeniably funny in more spots than a right-thinking mature person feels comfortable admitting", the film was given 3½ stars out of five.The most positive major critic review listed on either Metacritic or Rotten Tomatoes was by Owen Gleiberman of Entertainment Weekly, who gave the film a C+ and remarked: "The movie is merciless sending up Juno's self-satisfied hipster gobbledygook, and it's quite funny to see Hannah Montana still promoting her tie-in products as she lies crushed and dying under a meteor." Gleiberman previously contributed the only positive review listed on either site (out of 17 at Metacritic and 57 at Rotten Tomatoes) of Friedberg and Seltzer's earlier effort Epic Movie.In a 2017 episode of the ABC show Big Fan, Kim Kardashian revealed that she is mortified by her character's death scene in the film and she "can't watch" [it].


=== Box office performance ===

On its domestic theatrical debut, Disaster Movie grossed $2,023,130 on its opening day, $5,836,973 over the three-day weekend, and $6,945,535 over the four-day weekend (including Labor Day). It ranked #7 for both the three- and four-day weekends. The film's takings for the weekend fell far short of the $17 million predicted by the Dallas Morning News. The film was not as commercially successful as previous Friedberg/Seltzer releases. On a $20 million budget, it grossed $14,190,901 domestically and $17,492,474 internationally for a worldwide total of $31,683,375, less than half the gross of Meet the Spartans.


== Home media ==
The DVD and Blu-ray were released on January 6, 2009. They both included an Unrated "Cataclysmic" Edition and the theatrical version, both with the same extras. About 410,934 DVD units were sold, bringing in $8,447,690 in revenue as of October 2009.


== Accolades ==
On January 21, 2009, the film received six nominations for the 29th Golden Raspberry Awards. The nominations were for Worst Picture, Worst Supporting Actress (Electra), Worst Supporting Actress (Kardashian), Worst Director, Worst Screenplay, and Worst Prequel, Remake, Rip-Off, or Sequel. Kardashian acknowledged her nomination on her blog, where she commented, "It's an honor just being nominated!"


== References ==


== External links ==
Official website
Disaster Movie on IMDb
Disaster Movie at Box Office Mojo