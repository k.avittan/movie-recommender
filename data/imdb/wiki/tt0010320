"Just Dropped In (To See What Condition My Condition Was In)" is a counterculture era song written by Mickey Newbury and, in 1968, a chart hit for The First Edition, which recorded the song in October 1967. Said to reflect the LSD experience, the song was intended to be a warning about the dangers of using the drug. It was first recorded by Jerry Lee Lewis, backed by members of "The Memphis Boys", the chart-topping rhythm section at Chips Moman's American Sounds Studio in Memphis, on May 9, 1967. The song appeared on Lewis' album Soul My Way, released November 1, 1967. Before Lewis' record was issued, on October 10, 1967, it was recorded by Teddy Hill & the Southern Soul as a single on Rice Records (Rice 5028 b/w "Stagger Lee") and produced by Norro Wilson.


== The First Edition version ==
"Just Dropped In ..." was recorded by The First Edition (with Kenny Rogers on lead vocals) in October 1967, and peaked at number five on the   Billboard charts. It was Rogers' first top ten hit. The song captures the psychedelic era of the late 1960s in stark contrast to the country folk harmonies that characterized most of The First Edition's catalog, and it garnered the group's first national TV audience on The Smothers Brothers Comedy Hour.
It was the group's second single from their eponymous album, The First Edition. Producer Mike Post reversed a few riffs to create the intro; the solo played by Glen Campbell was heavily compressed and a tremolo effect was used to achieve its sound. Another studio guitarist, Mike Deasy, provided the acoustic lead guitar parts. The track was recorded by Jimmy Valentine at "Valentine Recording Studios" located in Valley Village California. 
When Rogers signed with United Artists Records, in the mid-1970s after the group split, he re-recorded the track for his Ten Years of Gold album.
The First Edition version appears in:

The dream sequence from the Coen brothers' 1998 film The Big Lebowski
The 2010 action movie Faster
The 2015 HBO documentary Going Clear: Scientology and the Prison of Belief
Season 3, episode 1 of the TV series Chuck: "Chuck Versus the Pink Slip"
Season 1, episode 2 of the TV series Rowan & Martin's Laugh-In
Season 3, episode 1 of the TV series True Detective
Season 3, episode 1 of the TV series Goliath
Season 3, episode 4 of the TV series Young Sheldon
The end credits of the 2000 video game Driver 2
The title screen and end credits of the 2013 video game Stick It To The Man
The 5 March 2020 edition of the comic strip Zippy the Pinhead


== Other recordings ==
Bettye LaVette, Karen Records (KA 1544), 1968, flip side to "Get Away". On that release it is called "What Condition My Condition Is In".
Styvar Manor, British group, 1970, issued as single on Polydor.
Jim Turner of comedy troupe Duck's Breath Mystery Theatre performed (as his character Randee of the Redwoods) a surprisingly faithful version on the Ducks' 1986 album Born To Be Tiled.
Die Haut, 1988, with Nick Cave as guest singer.
Supergrass as a B-Side to their 1995 single Alright/Time
Tinsley Ellis, 1997, on his 1997 album Fire It Up (Tinsley Ellis album)
The Raymen, Death-Country/Goth-Americana-Billy band, 1998
Willie Nelson, on his 2001 album Rainbow Connection and his 2002 album The Great Divide
Mojo Nixon, 2001
Children of Bodom, melodic death metal band, on the Japanese release of their Blooddrunk album as a bonus track and on their 2009 cover album "Skeletons in the Closet"
Tom Jones, on the Deluxe Edition of "Spirit in the Room" in 2012
Murder by Death, Americana band, 2012
the Launderettes, Norwegian rock group, 2013,
The Fantastics, Donegal-based band, 2014.
Sinister Dexter, San Francisco soul/funk group, 2014
White Denim exclusively for the black comedy–crime drama television series Fargo, which played over the credits of the "Did you do this? No, you did it!" episode of the second season in 2015.
Abe Diddy and The Krautboys on the album Follow in 2016
Sharon Jones & The Dap-Kings, on the original motion picture soundtrack for the film Soul Men
Reef for their compilation album Together.
Jeff Walker Und Die Fluffers (former lead singer of Carcass), on album Welcome to Carcass Cuntry
Sam Feldt, in his 2017 album Sunrise, with Girls Love DJs and Joe Cleere.
Jacob Needham & The Blue Trees, on their debut album Procrastinated Memoirs, April 2018.
Eagles of Death Metal, on their 2019 release “EODM presents: Boots Electric Performing the Best Songs We Never Wrote”
Gretchen Peters, on her 2020 tribute album The Night You Wrote That Song: The Songs of Mickey Newbury


== References ==


== External links ==
Lyrics of this song at MetroLyrics