The Siberian Husky (Russian: Сибирский хаски, tr. Sibirskiy khaski) is a medium-sized working dog breed. The breed belongs to the Spitz genetic family. It is recognizable by its thickly furred double coat, erect triangular ears, and distinctive markings, and is smaller than a very similar-looking dog, the Alaskan Malamute.
Siberian Huskies originated in Northeast Asia where they are bred by the Chukchi people for sled-pulling, guarding, and companionship. It is an active, energetic, resilient breed, whose ancestors lived in the extremely cold and harsh environment of the Siberian Arctic. William Goosak, a Russian fur trader, introduced them to Nome, Alaska during the Nome Gold Rush, initially as sled dogs.


== Lineage ==

The first of the dogs arrived in the Americas 12,000 years ago around the commencement of the Holocene period, however people and their dogs did not settle in the Arctic until the Paleo-Eskimo people 4,500 years ago and then the Thule people 1,000 years ago, with both emigrating from Siberia. The Siberian Husky was originally developed by the Chukchi people of the Chukchi Peninsula in eastern Siberia. They were brought to Nome, Alaska, in 1908 for sled-dog racing.In 1989, a study was made of ancient canis remains dated to the Late Pleistocene and early Holocene that had been uncovered by miners decades earlier around Fairbanks, Alaska. These were identified as Canis lupus and described as "short-faced wolves". The collection was separated into those specimens that looked more wolf-like (i.e. the Beringian wolf), and those that looked more dog-like and in comparison to the skulls of Eskimo dogs from both Greenland and Siberia thought to be their forerunners.In 2015, a study using a number of genetic markers indicated that the Siberian Husky, the Alaskan Malamute and the Alaskan husky share a close genetic relationship between each other and were related to Chukotka sled dogs from Siberia. They were separate to the two Inuit dogs, the Canadian Eskimo Dog and the Greenland Dog. In North America, the Siberian Husky and the Malamute both had maintained their Siberian lineage and had contributed significantly to the Alaskan husky, which showed evidence of crossing with European breeds that were consistent with this breed being created in post-colonial North America.Nearly all dog breeds' genetic closeness to the gray wolf is due to admixture. However, several Arctic dog breeds show a genetic closeness with the now-extinct Taymyr wolf of North Asia due to admixture. These breeds are associated with high latitudes - the Siberian Husky and Greenland dog that are also associated with arctic human populations and to a lesser extent, the Shar-Pei and Finnish Spitz. An admixture graph of the Greenland dog indicates a best-fit of 3.5% shared material, however an ancestry proportion ranging between 1.4% and 27.3% is consistent with the data. This indicates admixture between the Taymyr wolf population and the ancestral dog population of these four high-latitude breeds. This introgression could have provided early dogs living in high latitudes with phenotypic variation beneficial for adaption to a new and challenging environment. It also indicates the ancestry of present-day dog breeds descends from more than one region.


== Description ==


=== Coat ===

A Siberian Husky has a double coat that is thicker than that of most other dog breeds. It has two layers: a dense undercoat and a longer topcoat of short, straight guard hairs. It protects the dogs effectively against harsh Arctic winters, and also reflects heat in the summer. It is able to withstand temperatures as low as −50 to −60 °C (−58 to −76 °F). The undercoat is often absent during shedding. Their thick coats require weekly grooming.Siberian Huskies come in a variety of colors and patterns, usually with white paws and legs, facial markings, and tail tip. The most common coats are black and white, then less common copper-red and white, grey and white, pure white, and the rare "agouti" coat, though many individuals have blondish or piebald spotting. Some other individuals also have the "saddle back" pattern, in which black-tipped guard hairs are restricted to the saddle area while the head, haunches and shoulders are either light red or white. Striking masks, spectacles, and other facial markings occur in wide variety. All coat colors from black to pure white are allowed. Merle coat patterns are not permitted by the American Kennel Club (AKC) and The Kennel Club (KC). This pattern is often associated with health issues and impure breeding.


=== Eyes ===

The American Kennel Club describes the Siberian Husky's eyes as "an almond shape, moderately spaced and set slightly obliquely." The AKC breed standard is that eyes may be brown, blue or black; one of each or Particoloured are acceptable (complete is heterochromia). These eye-color combinations are considered acceptable by the American Kennel Club. The parti-color does not affect the vision of the dog.


=== Nose ===
Show-quality dogs are preferred to have neither pointed nor square noses. The nose is black in gray dogs, tan in black dogs, liver in copper-colored dogs, and may be light tan in white dogs. In some instances, Siberian Huskies can exhibit what is called "snow nose" or "winter nose." This condition is called hypopigmentation in animals. "Snow nose" is acceptable in the show ring.


=== Tail ===

Siberian Husky tails are heavily furred; these dogs will often curl up with their tails over their faces and noses in order to provide additional warmth. As pictured, when curled up to sleep the Siberian Husky will cover its nose for warmth, often referred to as the "Siberian Swirl". The tail should be expressive, held low when the dog is relaxed, and curved upward in a "sickle" shape when excited or interested in something.


=== Size ===
The breed standard indicates that the males of the breed are ideally between 20 and 24 inches (51 and 61 cm) tall at the withers and weighing between 45 and 60 pounds (20 and 27 kg). Females are smaller, growing to between 19 to 23 inches (48 to 58 cm) tall at the withers and weighing between 35 to 50 pounds (16 to 23 kg). The people of Nome referred to Siberian Huskies as "Siberian Rats" due to their size of 40–50 lb (18–23 kg), versus the Alaskan Malamute's size of 75–85 lb (34–39 kg).


=== Behavior ===
The Husky usually howls instead of barking. They have been described as escape artists, which can include digging under, chewing through, or even jumping over fences.Because the Siberian Husky had been raised in a family setting by the Chukchi and not left to fend for themselves they could be trusted with children. The ASPCA classifies the breed as good with children. It also states they exhibit high energy indoors, have special exercise needs, and may be destructive "without proper care".Siberian Huskies have a high prey drive due to the Chukchi allowing them to roam free in the summer. The dogs hunted in packs and preyed on wild cats, birds, and squirrels, but with training can be trusted with other small animals. They would only return to the Chukchi villages when the snow returned and food became scarce. Their hunting instincts can still be found in the breed today.A 6 ft (1.83 m) fence is recommended for this breed as a pet, although some have been known to overcome fences as high as 8 ft (2.44 m). Electric pet fencing may not be effective. They need the frequent companionship of people and other dogs, and their need to feel as part of a pack is very strong.


== Health ==
A 1999 ASPCA publication shows the average life span of the Siberian Husky is 12 to 14 years. Health issues in the breed are mainly genetic, such as seizures and defects of the eye (juvenile cataracts, corneal dystrophy, canine glaucoma and progressive retinal atrophy) and congenital laryngeal paralysis. Hip dysplasia is not often found in this breed; however, as with many medium or larger-sized canines, it can occur. The Orthopedic Foundation for Animals currently has the Siberian Husky ranked 155th out of a possible 160 breeds at risk for hip dysplasia, with only two percent of tested Siberian Huskies showing dysplasia.Siberian Huskies used for sled racing may also be prone to other ailments, such as gastric disease, bronchitis or bronchopulmonary ailments ("ski asthma"), and gastric erosions or ulcerations.Modern Siberian Huskies registered in the US are largely the descendants of the 1930 Siberia imports and of Leonhard Seppala’s dogs, particularly Togo. The limited number of registered foundational dogs has led to some discussion about their vulnerability to the founder effect.


== History ==
The original sled dogs bred and kept by the Chukchi were thought to have gone extinct, but Benedict Allen, writing for Geographical magazine in 2006 after visiting the region, reported their survival. His description of the breeding practiced by the Chukchi mentions selection for obedience, endurance, amiable disposition, and sizing that enabled families to support them without undue difficulty.With the help of Siberian Huskies, entire tribes of people were able not only to survive, but to push forth into terra incognita.The Siberian Husky, Samoyed, and Alaskan Malamute are all breeds directly descended from the original sled dog. It is thought that the term "husky" is a corruption of the nickname "Esky" once applied to the Eskimo and subsequently to their dogs.Dogs from the Anadyr River and surrounding regions were imported into Alaska from 1908 (and for the next two decades) during the gold rush for use as sled dogs, especially in the "All-Alaska Sweepstakes," a 408-mile (657-km) distance dog sled race from Nome, to Candle, and back. Smaller, faster and more enduring than the 100- to 120-pound (45- to 54-kg) freighting dogs then in general use, they immediately dominated the Nome Sweepstakes. Leonhard Seppala, the foremost breeder of Siberian Huskies of the time, participated in competitions from 1909 to the mid-1920s.

On February 3, 1925, Gunnar Kaasen was first in the 1925 serum run to Nome to deliver diphtheria serum from Nenana, over 600 miles to Nome. This was a group effort by several sled-dog teams and mushers, with the longest (264 miles or 422 km) and most dangerous segment of the run covered by Leonhard Seppala and his sled team lead dog Togo. The event is despicted in the 2019 film Togo and is also loosely depicted in the 1995 animated film Balto, as the name of Gunnar Kaasen's lead dog in his sled team was Balto, although unlike the real dog, Balto the character was portrayed as half wolf in the film. In honor of this lead dog, a bronze statue was erected at Central Park in New York City. The plaque upon it is inscribed, Dedicated to the indomitable spirit of the sled dogs that relayed antitoxin six hundred miles over rough ice, across treacherous waters, through Arctic blizzards from Nenana to the relief of stricken Nome in the winter of 1925. Endurance · Fidelity · Intelligence
In 1930, exportation of the dogs from Siberia was halted. The same year saw recognition of the Siberian Husky by the American Kennel Club. Nine years later, the breed was first registered in Canada. The United Kennel Club recognized the breed in 1938 as the "Arctic Husky," changing the name to Siberian Husky in 1991. Seppala owned a kennel in Nenana before moving to New England, where he became partners with Elizabeth Ricker. The two co-owned the Poland Springs kennel and began to race and exhibit their dogs all over the Northeast.As the breed was beginning to come to prominence, in 1933 Navy Rear Admiral Richard E. Byrd brought about 50 Siberian Huskies with him on an expedition in which he hoped to journey around the 16,000-mile coast of Antarctica. Many of the dogs were trained at Chinook Kennels in New Hampshire. Called Operation Highjump, the historic trek proved the worth of the Siberian Husky due to its compact size and greater speeds. Siberian Huskies also served in the United States Army's Arctic Search and Rescue Unit of the Air Transport Command during World War II. Their popularity was sustained into the 21st century. They were ranked 16th among American Kennel Club registrants in 2012, rising to 14th place in 2013.Siberian huskies gained in popularity with the story of the "Great Race of Mercy," the 1925 serum run to Nome, featuring Balto and Togo. Although Balto is considered the more famous, being the dog that delivered the serum to Nome after running the final 53-mile leg, it was Togo who made the longest run of the relay, guiding his musher Leonhard Seppala on a 91-mile journey that included crossing the deadly Norton Sound to Golovin.In 1960, the US Army undertook a project to construct an under the ice facility for defense and space research, Camp Century, part of Project Iceworm involved a 150+ crew who also brought with them an unofficial mascot, a Siberian Husky named Mukluk.Huskies were extensively used as sled dogs by the British Antarctic Survey in Antarctica between 1945 and 1994. A bronze monument to all of BAS's dog teams sits outside its Cambridge headquarters.


== In popular culture ==

A bronze statue of Balto that has been displayed in New York City’s Central Park since 1925 is one of its enduringly popular features.
The television series Game of Thrones caused a huge uptick in demand for Siberian Huskies as pets, followed by a steep increase of their numbers at public shelters. Even though the animal actors were not Siberian Huskies, people were acquiring Siberian Huskies because they looked similar to the fictional direwolf characters depicted in the show. Two of the show's stars pleaded with the public to stop acquiring the dogs without first researching the breed.
Characters in film and television: The film Eight Below features six Siberian Huskies whose names are Max, Maya, Truman, Old Jack, Dewey and Shorty. In the horror television series Z Nation, a character adopts a Siberian Husky after its owner freezes to death outside his base, and the other dog turned into a zombie. The T.V. show Parks and Recreation uses a Siberian Husky as "spirit dog" for April Ludgate.
In the 2008 Disney film Snow Buddies, a black and white blue-eyed male Siberian Husky puppy named Shasta (voiced by Dylan Sprouse) is the protagonist.
Animated characters: The animated series Road Rovers features Exile who is a Siberian Husky. The show Krypto the Superdog features Tusky Husky.
Everest in the animated series PAW Patrol is a Siberian Husky.
Several purebred Siberian Huskies portrayed Diefenbaker, the "half-wolf" companion to RCMP Constable Benton Fraser, in the CBS/Alliance Atlantis TV series Due South.
Siberian Huskies are the mascots of the athletic teams of several schools and colleges, including: St. Cloud State University (St. Cloud State Huskies, Blizzard), Northern Illinois University (Northern Illinois Huskies, Victor), the University of Connecticut (Connecticut Huskies, Jonathan), Northeastern University (Northeastern Huskies, Paws), the Michigan Technological University (Michigan Tech Huskies, Blizzard), University of Washington (Washington Huskies, Harry), Houston Baptist University (Kiza the Husky), and Saint Mary's University (Saint Mary's Huskies)


== See also ==

Alaskan husky
Alaskan Klee Kai
Alaskan Malamute
Canadian Eskimo Dog
Greenland dog
Laika (dog breed)
Seppala Siberian Sleddog
Togo – Sled dog who ran in the 1925 serum run to Nome, Alaska


== References ==


== External links ==
Siberian Husky at Curlie