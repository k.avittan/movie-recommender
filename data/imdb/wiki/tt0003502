The Venetian Las Vegas is a luxury hotel and casino resort located on the Las Vegas Strip in Paradise, Nevada, United States, on the site of the old Sands Hotel. Designed by KlingStubbins, the hotel tower contains 36 stories and rises 475 feet (145 m). The Venetian is owned and operated by Las Vegas Sands. The Venetian also serves as the seat of the corporate headquarters for its parent company.
The Venetian resort complex includes the adjacent Palazzo resort and Sands Expo center, as well as the upcoming MSG Sphere at The Venetian. The Venetian-Palazzo complex includes the world's second-largest hotel, with 4,049 rooms, 3,068 suites and a 120,000-square-foot (11,000 m2) casino.; its sister casino, The Venetian Macao, is the largest in the world.


== History ==
In April 1996, Sheldon Adelson announced plans to create the largest resort on the Strip. This project would be situated on the former Sands property. On November 26, 1996, eight years after it was purchased by the owners of The Interface Group—Adelson, Richard Katzeff, Ted Cutler, Irwin Chafetz and Jordan Shapiro, the Sands Hotel was imploded to make way for The Venetian Resort Hotel Casino. Groundbreaking for the hotel began on April 14, 1997.
The resort opened on May 3, 1999, with flutter of white doves, sounding trumpets and singing gondoliers. Actress Sophia Loren joined Adelson, the chairman and owner, in dedicating the first motorized gondola. Built at a cost of $1.5 billion, it was one of the most expensive resorts of its kind when it opened.
On June 27, 2003, the 1,013-room Venezia Tower opened. It was built on top of the garage parking lot.
In 2004, the Venetian agreed to pay a $1 million penalty to settle a 12-count Gaming Control Board complaint. One of the 12 complaints alleged the hotel had held a drawing for a Mercedes-Benz that was rigged to be won by a high roller who had lost a large amount in the casino. The executives involved were fired.In 2010, it was announced that it will be affiliated with InterContinental Hotels Group.In October 2011, the Cantor Race & Sportsbook opened, which was the only Las Vegas sportsbook that was open 24 hours a day. On June 11, 2012, the Venetian opened Carnevale, a summer-long festival that is anchored by a nightly 3-D projection show on the clock tower.
In 2013, the Venetian agreed to pay the U.S. Department of Justice $47.4 million to settle charges over "alleged money laundering activities".Like other casinos in the state, the Venetian closed indefinitely in March 2020 in response to the COVID-19 pandemic and its effects on the state. In April 2020, the Venetian announced plans to incorporate emergency medical personnel and automatic camera-based body temperature scans into its eventual reopening, which occurred on June 4, 2020.


== Design ==
The hotel uses Venice, Italy, as its design inspiration and features architectural replicas of various Venetian landmarks, including the Palazzo Ducale, Piazza San Marco, Piazzetta di San Marco, the Lion of Venice Column and the Column of Saint Theodore, St Mark's Campanile, and the Rialto Bridge. The design architects for this project were The Stubbins Associates and WAT&G. Interior design was provided by Wilson Associates and Dougall Associates for the casino.


== Attractions ==
The Venetian includes an indoor shopping mall known as the Grand Canal Shoppes. The Sands Expo serves as the convention center for the Venetian and Palazzo. In October 2001, the Guggenheim Hermitage Museum opened within the resort, featuring its first collection.
In 2015, the resort's TAO Nightclub generated over $50 million in revenue, according to Nightclub & Bar Top 100. With an Asian-inspired theme, TAO features a 20 foot tall Buddha statue, an infinity edge pool stocked with koi, eight private "sky boxes" with mini-bars, a 40-foot-long terrace with views of the Strip, and two dance rooms. TAO Beach, located on top of TAO Nightclub, is the Venetian's day club and pool party. It offers seven cabanas, each with television, DVD player, Xbox 360, a stocked mini-fridge and a safe for valuables.The adjacent MSG Sphere at The Venetian, which is being built in partnership with The Madison Square Garden Company, will open in 2023.


== Entertainers ==
The Venetian is home to four theaters: The Opaline Theatre, The Palazzo Theatre, The Sands Showroom, and The Venetian Theatre. In October 2005, Blue Man Group opened at the Blue Man Theatre and continued performing there until September 2012, when the show relocated to the Monte Carlo resort. On June 24, 2006, the show, Phantom: The Las Vegas Spectacular, opened at a new Paris Opera House styled theatre at The Venetian. The show concluded on September 2, 2012. Tim McGraw and Faith Hill headlined their Soul2Soul concert series which began in December 2012 and ended in April 2014.


== Gallery ==
See also  Media related to The Venetian hotel (Las Vegas) at Wikimedia Commons
		
		
		
		
		
		
		


== In popular culture ==
FilmsThe Venetian's casino, lobby, and exterior were among the filming locations for the 2001 film Rat Race. The hotel rooms were also portrayed in the film, although these scenes were shot on a sound stage in Canada.
The Venetian was a prominent filming location for the 2005 film Miss Congeniality 2: Armed and Fabulous.
The exterior of the Venetian is portrayed in the 2007 film Resident Evil: Extinction. There is a dramatic zombie attack scene which takes place on the Venetian's replica of the Rialto Bridge.TelevisionA 2005 episode of Megastructures, titled "Ultimate Casino", focuses on the resort's design and construction.
The U.S. TV series What Not to Wear shot its series finale at The Venetian and Palazzo in 2013, inviting more than 100 past contributors from the show's 10-year run to participate.


== See also ==
Yardbird Southern Table & Bar, located at the Venetian


== References ==


== External links ==
Official website