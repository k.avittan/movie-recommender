Loggerheads Country Park is a country park in the village of Loggerheads, Denbighshire, Wales. The park has a wooded river valley that follows the course of the River Alyn and high cliffs from within the Clwydian Range of mountains, with views of the range's tallest mountain Moel Famau. The park has a visitor centre, woodland walks, and two landmarks—a historic corn mill called Pentre Mill, and a gorge called Devil's Gorge.


== History ==
The site was the location of Pentre Mill, a corn mill dating to the early 19th century. The mill was powered by a water wheel near the River Alyn; the water wheel was closed in the 1940s and restored in the 1990s. The park was also the location of lead mining and quarrying.In 1926, the Crosville Motor Bus Company acquired land at Loggerheads and constructed tea rooms and gardens for visitors who often arrived at the park by bus. Other amenities included a bandstand, boating lake, and refreshment kiosks. Loggerheads was popular in the 1920s and 1930s but declined after the Second World War. In 1974, Clwyd County Council bought the land and the gardens for use as a country park. In 1984 a wooden tea room burned down.


== Facilities ==

The park includes a visitor centre, corn mill, cafe, picnic area, nature trails, and walking tours. One of the mill buildings has been turned into an interactive audiovisual display explaining the landscape of the Clwydian Range and the Dee Valley Area of Outstanding Natural Beauty.


== Landmarks ==


=== Pentre Mill ===

Pentre Mill is a Grade II* listed corn mill at the park. A 1796 painting of Loggerheads by John Ingleby depicts a mill and water wheel at Loggerheads. The name of this mill is unconfirmed and Cadw, the Welsh Heritage Body, has confirmed Pentre as early 19th century. The mill was used to grind corn and cereal into flour and also as a circular saw to cut wood. The mill was powered by a water wheel fed from the nearby River Alyn.  It was in continuous use until 1942, when it was closed.After closure, the mill fell into disrepair. It was restored to working order in 1995. However, due to degradation of the sluice gate, weir, and water course, there was no water reaching the wheel. In 2012 the water course was repaired and the water wheel turns once more.


=== Devil's Gorge ===

Devil's Gorge is a gorge located in the park with two solid limestone walls that are 30 metres (98 ft) high. Walkers cross a bridge over the gorge as part of a route through the park; it is also a destination for rock climbing and abseiling. One climbing route is called the Devil's Haircut.


== References ==