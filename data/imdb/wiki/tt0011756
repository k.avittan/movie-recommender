The Testing Block is a 1920 American silent Western film directed by Lambert Hillyer and starring William S. Hart, Eva Novak, J. Gordon Russell, Florence Carpenter, Richard Headrick, and Ira McFadden. It was written by Lambert Hillyer and William S. Hart. It was released on December 26, 1920, by Paramount Pictures.


== Plot ==
As described in a film magazine, Nelly Gray (Novak), a member of a traveling show, is stranded when the manager abandons the troupe, and Sierra Bill (Hart), a member of a gang, helps with the gift of some gold. Although the gang leader has designs on the young woman, he agrees that the gang will fight it out among themselves and the last man standing will get her. Sierra Bill wins, and in a drunken fury forces Nelly to marry him. After the wedding he becomes the model husband and soon Sonny (Headrick) is born who is the apple of his father's eye. The gang leader returns with Rosita, a young Mexican woman, and discloses Sierra Bill's past to Nelly and suggests that he is even having an affair with Rosita. The gang leader directs Rosita through her cards to tell Sierra Bill that his wife is longing to leave him. Nelly agrees to leave with the trouble-maker, but with the promise that Sonny will soon join them. Sonny, however, is taken ill, and the husband and wife have a reunion at his bedside that brings a happy ending.


== Cast ==
William S. Hart as 'Sierra' Bill
Eva Novak as Nelly Gray
J. Gordon Russell as Ringe
Florence Carpenter as Rosita
Richard Headrick as Sonny
Ira McFadden as Slim


== Survival and Availability ==
The Testing Block has survived in complete form and a print is held by the Library of Congress. Much of the film was shot in and around Capitola, California, and to celebrate the film's 100th anniversary a scan of the LOC print was screened at the Capitola Community Center on January 26, 2020. Afterward the Capitola History Museum uploaded the film online, preceded by an introduction. Having been released in 1920, The Testing Block is in the public domain.


== References ==


== External links ==
The Testing Block on YouTube (Library of Congress film scan uploaded by the Capitola Historical Museum)The Testing Block on IMDb
Synopsis at AllMovie