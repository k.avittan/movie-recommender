A patrol boat (also referred to as a patrol craft, patrol ship or patrol vessel) is a relatively small naval vessel generally designed for coastal defence, border protection, immigration law-enforcement, search and rescue duties. There have been many designs for patrol boats. They may be operated by a nation's navy, coast guard, police force or customs and may be intended for marine ("blue water") or estuarine ("green water") or river ("brown water") environments. They are commonly found engaged in various border protection roles, including anti-smuggling, anti-piracy, fisheries patrols, and immigration law enforcement. They are also often called upon to participate in rescue operations. Vessels of this type include the original yacht (from Dutch/Low German jacht meaning hunting or hunt), a light, fast-sailing vessel used by the Dutch navy to pursue pirates and other transgressors around and into shallow waters.


== Classification ==
They may be broadly classified as inshore patrol vessels (IPVs) and offshore patrol vessels (OPVs). They are warships typically smaller in size than a corvette and can include fast attack craft, torpedo boats and missile boats, although some are as large as a frigate. The offshore patrol vessels are usually the smallest ship in a navy's fleet that is large and seaworthy enough to patrol off-shore in the open ocean. In larger militaries, such as in the United States military, offshore patrol vessels usually serve in the coast guard, but many smaller nations navies operate these type of ships.


== History ==

During both World Wars in order to rapidly build up numbers, all sides created auxiliary patrol boats by arming motorboats and seagoing fishing trawlers with machine guns and obsolescent naval weapons. Some modern patrol vessels are still based on fishing and leisure boats. Seagoing patrol boats are typically around 30 m (100 ft) in length and usually carry a single medium caliber artillery gun as main armament, and a variety of lighter secondary armament such as machine guns or a close-in weapon system. Depending on role, vessels in this class may also have more sophisticated sensors and fire control systems that would enable them to carry torpedoes, anti-ship and surface-to-air missiles.
Most modern designs are powered by gas turbine arrangements such as CODAG, and speeds are generally in the 25–30 knots (46–56 km/h; 29–35 mph) range. They are primarily used for patrol in a country's exclusive economic zone (EEZ). Common tasks are fisheries inspection, anti-smuggling (usually anti-narcotics) duties, illegal immigration patrols, anti-piracy patrols and search and rescue (law enforcement-type of work). The largest OPVs might also have a flight deck and helicopter embarked. In times of crisis or war, these vessels are expected to support the larger vessels in the navy.
Their small size and relatively low cost make them one of the most common type of warship in the world. Almost all navies operate at least a few offshore patrol vessels, especially those with only "green water" capabilities. They are useful in smaller seas such as the North Sea as well as in open oceans. Similar vessels for exclusively military duties include torpedo boats and missile boats. The United States Navy operated the Pegasus class of armed hydrofoils for years, in a patrol boat role. The River Patrol Boat (PBR, sometimes called "Riverine" and "Pibber") is a U.S. design of small patrol boat type designed to patrol waters of large rivers.


== Specific nations ==


=== Albania ===


==== Albanian Naval Force ====
Iliria


=== Argentina ===


==== Argentine Naval Prefecture ====
Mantilla-class patrol vessel
Z-28-class patrol vessel


=== Australia ===


==== Royal Australian Navy ====
Attack-class patrol boat (1967–1985)
Fremantle-class patrol boat (1979–2007)
Armidale-class patrol boat (2005–present)
Arafura-class offshore patrol vessel – expected to enter service in 2021


==== Australian Border Force Marine Unit ====
Bay-class patrol boat (1999–present)
Australian Customs Vessel Triton (2000–2016)
Cape-class patrol boat – Bay-class replacement from 2013 (2013–present)


==== Others ====
Pacific-class patrol boat – Australian-built, gifted by the Australian Government to 12 Pacific Island countries (1987–present)


=== Bahamas ===


==== Royal Bahamas Defence Force ====
Bahamas-class patrol vessel


==== Others ====
Protector-class patrol boat


=== Bangladesh ===


==== Bangladesh Navy ====
Padma-class offshore patrol vessel
Meghna-class patrol boat


==== Others ====
Durjoy-class patrol craft
Island-class patrol vessel
Sea Dragon-class offshore patrol vessel
Hainan-class submarine chaser
Kraljevica-class patrol boat
Barkat (Haizhui)-class patrol boat
Ajay-class patrol boat
Shaheed (Shanghai II)-class patrol boat


=== Belgium ===
P901 Castor (in Dutch) (2014–present)
P902 Pollux (in Dutch) (2015–present)


=== Brazil ===
Grajaú-class offshore patrol vessel
Bracuí-class patrol vessel – ex-River-class minesweeper
Imperial Marinheiro-class offshore patrol vessel
Piratini-class patrol vessel
J-class patrol vessel
Roraima-class river patrol vessel
Pedro Teixeira-class river patrol vessel
Macaé-class offshore patrol vessel
Amazonas-class corvette


=== Brunei ===
Darussalam-class offshore patrol vessel
Ijtihad-class patrol vessel
KH 27-class patrol boat
FDB 512-class patrol boat
Bendaharu-class patrol vessel
Perwira-class patrol vessel
Saleha-class patrol vessel
Pahlawan-class patrol vessel


=== Bulgaria ===
Obzor


=== Canada ===


==== Royal Canadian Navy ====
Orca-class patrol vessel


==== Canadian Coast Guard ====
Hero-class patrol vessel


=== Chile ===
Patrulleros de Zona Marítima FASSMER OPV-80 class – four of six units planned, built by ASMAR under the license of FASSMER Gmbh:
OPV-81 Piloto Pardo
OPV-82 Comandante Toro
OPV-83 Marinero Fuentealba – this unit has reinforced hull for Antarctic operations
OPV-84 Cabo Odger – with characteristics similar to the previous ship
6 Micalvi-class patrol vessels – built in ASMAR
18 Protector-class patrol crafts – built by ASMAR under the license of Fairey Brooke Marine
4 Dabur-class patrol crafts – built in Israel


=== China ===


==== People's Liberation Army Navy ====
Harbour security boat (PBI) – four newly built 80-ton class harbour security / patrol boats, and more are planned in order to take over the port security / patrol duties currently performed by the obsolete Shantou, Beihai, Huangpu, and Yulin-class gunboats, which are increasingly being converted to inshore surveying boats and range support boats
Shanghai III (Type 062I)-class gunboat – 2
Shanghai II-class gunboat
Shanghai I (Type 062)-class gunboat – 150+ active and at least 100 in reserve
Huludao (Type 206)-class gunboat – 8+
Shantou-class gunboat – less than 25 (in reserve, subordinated to naval militia)
Beihai-class gunboat – less than 30 (in reserve, subordinated to naval militia)
Huangpu-class gunboat – less than 15 (in reserve, subordinated to naval militia)
Yulin-class gunboat – less than 40 (being transferred to logistic duties)


==== China Coast Guard ====
Haixun-class cutter (Type 718)


=== Colombia ===


==== Colombian National Navy ====
Diligente-class patrol boat
Nodriza-class patrol boat
PAF-I-class patrol boat
PAF-II-class patrol boat
PAF-III-class patrol boat
PAF-IV-class patrol boat
Patrullera Fluvial Ligera-class patrol boat
Riohacha-class gunboat
Fassmer-80 class – built in Colombia by COTECMAR


=== Croatia ===
Šolta (OB-02)
Omiš (OB-31)


=== Denmark ===


==== Royal Danish Navy ====
Knud Rasmussen-class OPV – 2 vessels
Diana-class IPV – 6 vessels (in Danish)
Flyvefisken-class patrol vessel
Thetis class OPV – 4 ships (classed as ocean patrol frigates)
Agdlek-class IPV – 3 vessels
Barsø class IPV – 9 vessels (in Danish)
Hvidbjørnen class OPV (link in Danish) – 4 ships (classed as ocean patrol frigates)


==== Others ====
Beskytteren ocean patrol frigate OPV – later sold to Estonia and renamed EML Admiral Pitka (A230) (classed as ocean patrol frigates)


=== Eritrea ===


==== Eritrean Navy ====
Eritrea-class 60m patrol vessel


==== Others ====
Protector-class patrol boat


=== Finland ===


==== Finnish Navy ====
Kiisla class – formerly Finnish Border Guard, now Finnish Navy
VMV class


==== Others ====
VL Turva – an offshore patrol vessel built at STX Finland Rauma shipyard in 2014


=== France ===


==== French Navy ====
P400 class
Classe La Confiance (link in French)
Floréal class
Flamant class
Espadon 50 class (1991–2010)


==== Maritime Gendarmerie ====
Trident-class patrol boat
Géranium-class patrol boat
Jonquille-class patrol boat
Vedette-class patrol boat
Pétulante-class patrol craft
Pavois-class patrol craft


=== Germany ===

Potsdam class (2019–present)
Bad Bramstedt class (2002–present)
Helgoland class (2009–present)
PA class (1943–1945)
R boats (1929–1945)
Type 139 patrol trawler (1956 to mid-1970s)


=== Greece ===


==== Hellenic Navy ====
Osprey 55-class gunboats and derivatives HSY-55 and HSY-56A
Asheville-class gunboats
Nasty-class Coastal Patrol Vessels – formerly torpedo boats
Esterel-class Coastal Patrol Vessels


==== Hellenic Coast Guard ====
Saar 4 – acting as Offshore Patrol Vessels (OPV)
Stan Patrol 5509 OPV
Vosper Europatrol 250 Mk1 OPV
Abeking & Rasmussen Patrol Vessels – class Dilos
POB-24G Patrol Vessels – class Faiakas
CB90-HCG
Lambro 57 and derivatives – all being boats for coastal patrols


=== Hong Kong ===


==== Hong Kong Police Force ====
Sea Panther-class large command boat


=== Iceland ===


==== Icelandic Coast Guard ====
ICGV Týr
ICGV Óðinn
ICGV Þór
ICGV Ægir


=== India ===

Car Nicobar-class patrol vessel, Indian Navy
Saryu-class patrol vessel, Indian Navy
Bangaram-class patrol vessel, Indian Navy
Sukanya-class patrol vessel, Indian Navy
Samar-class offshore patrol vessel, Indian Coast Guard
Vishwast-class offshore patrol vessel, Indian Coast Guard
Sarojini Naidu-class patrol vessel, Indian Coast Guard
Tara Bai-class patrol vessel, Indian Coast Guard
Priyadarshini-class patrol vessel, Indian Coast Guard
Jija Bai-class patrol vessel, Indian Coast Guard
Vikram-class offshore patrol vessel, Indian Coast Guard
Aadesh-class patrol vessel, Indian Coast Guard


=== Indonesia ===

FPB 28, Indonesian Police and Indonesian Customs, 28 meter long patrol boat made by local shipyard PT PAL.
FPB 38, Indonesian Customs, 38 meter long aluminium patrol boat made by local shipyard PT PAL.
FPB 57, Indonesian Navy, 57 meter long patrol boat designed by Lurssen and made by PT PAL, ASM and heli deck equipped for some version.
PC-40, Indonesian Navy, 40 meter long FRP/Aluminum patrol boat, locally made by in house Navy's workshop.
PC-60 trimaran, Indonesian Navy, 63-meter-long composite material, is armed with 120 km range of anti-ship missile, made by PT Lundin industry
OPV 80 - 80 meter long marine steel, is armed with 57 mm main gun, designed by Terafulk and made by PT Citra Shipyard
OPV 110 - 110 meter long marine steel grade A, is armed with 57 mm main gun, made by PT Palindo Marine Shipyard


=== Ireland ===


==== Irish Naval Service ====
Emer-class Offshore Patrol Vessels
LÉ Deirdre (P20) (1972–2001)
LÉ Emer (P21) (1978–2013)
LÉ Aoife (P22) (1979–2015)
LÉ Aisling (P23) (1980–2016)
LÉ Eithne-class Helicopter Patrol Vessel
LÉ Eithne (P31) (1984-present)
Peacock-class Coastal Patrol Vessels
LÉ Orla (P41) (1984-present)
LÉ Ciara (P42) (1985-present)
Róisín-class Offshore Patrol Vessels
LÉ Róisín (P51)  (1999-present)
LÉ Niamh (P52)  (2001-present)
Samuel Beckett-class Offshore Patrol Vessels
LÉ Samuel Beckett (P61)  (2014-present)
LÉ James Joyce (P62)  (2015-present)
LÉ William Butler Yeats (P63)  (2016-present)
LÉ George Bernard Shaw (P64)  (2018-present)


=== Israel ===

Dabur class patrol boats – Israeli Navy
Dvora class fast patrol boat – Israeli Navy
Super Dvora Mk II – Israeli Navy
Super Dvora Mk III – Israeli Navy
Shaldag class fast patrol boats
Shaldag Mk II – Israeli Navy
Nachshol class patrol boats (Stingray Interceptor-2000) – Israeli Navy


=== Italy ===

Zara class, (Italian Guardia di Finanza)
Saettia class, (Italian Coast Guard)
Diciotti – CP 902 class, (Italian Coast Guard)
Dattilo-class, (Italian Coast Guard)
Cassiopea class, (Italian Marina Militare)
Cassiopea II class, (Italian Marina Militare)
Esploratore class, (Italian Marina Militare)
Comandanti-class, (Italian Marina Militare)


=== Japan ===

Shikishima (Japan Coast Guard), the largest patrol boat.
Mizuho-class (Japan Coast Guard),Large patrol Vessel with helicopter deck and hangar.
Tsugaru-class (Japan Coast Guard),Large patrol Vessel with helicopter deck and hangar.
Hida class (Japan Coast Guard), high-speed Large patrol Vessel with helicopter deck.
Kunigami class (Kunisaki class)
Iwami class
Hateruma class
Aso class (Japan Coast Guard), high-speed Large patrol Vessel.
Amami class (Japan Coast Guard), medium-sized patrol Vessel
Hayabusa-class (JMSDF,Japanese Navy), Corvette class patrol vessel by JMSDF(Navy) Fleet.
"Sōya"，(Japan Coast Guard), icebreaker


=== Latvia ===
Skrunda class, world's first SWATH patrol boat (Latvian Naval Forces)


=== Malaysia ===
Kedah class offshore patrol vessel, (Royal Malaysian Navy)
Keris-class littoral mission ship,(Royal Malaysian Navy)
Gagah Class Ship，Malaysian Maritime Enforcement Agency
Ramunia Class Ship，Malaysian Maritime Enforcement Agency
Nusa Class Ship，Malaysian Maritime Enforcement Agency
Sipadan Class Ship，Malaysian Maritime Enforcement Agency
Rhu Class Ship，Malaysian Maritime Enforcement Agency
Pengawal Class Ship，Malaysian Maritime Enforcement Agency
Peninjau Class Ship，Malaysian Maritime Enforcement Agency
Pelindung Class Ship，Malaysian Maritime Enforcement Agency
Semilang Class Ship，Malaysian Maritime Enforcement Agency
Penggalang Class Ship，Malaysian Maritime Enforcement Agency
Penyelamat Class Ship，Malaysian Maritime Enforcement Agency
Pengaman Class Ship，Malaysian Maritime Enforcement Agency
Kilat Class Ship，Malaysian Maritime Enforcement Agency
Malawali Class Ship，Malaysian Maritime Enforcement Agency
Langkawi Class Patrol Ship，Malaysian Maritime Enforcement Agency


=== Malta ===

Protector class offshore patrol vessel (Maritime Squadron of the Armed Forces of Malta) – 2002–present
Diciotti class offshore patrol vessel (Maritime Squadron of the Armed Forces of Malta) – 2005–present
P21 class inshore patrol vessel (Maritime Squadron of the Armed Forces of Malta) – 2010–present
Emer class offshore patrol vessel (Maritime Squadron of the Armed Forces of Malta) – 2015–present


=== Mexico ===


==== Mexican Navy ====
Coastal patrol boat
Azteca-class
Tenochtitlan-class
Offshore patrol vessel
Durango-class
Holzinger-class
Oaxaca-class
Uribe-class


=== Montenegro ===

Kotor class frigate


=== Morocco ===

OPV-70 class, offshore patrol vessel (Royal Moroccan Navy)
OPV-64 class, offshore patrol vessel (Royal Moroccan Navy)


=== Netherlands ===
Holland class offshore patrol vessels (Koninklijke Marine)


=== New Zealand ===
Protector OPV (Royal New Zealand Navy) (2008)
Lake-class inshore patrol vessel (Royal New Zealand Navy) (2008)
Moa-class patrol boat (Royal New Zealand Navy)(1983–2008)


=== Norway ===


==== Royal Norwegian Navy ====
Rapp-class
Tjeld-class
Storm-class
Snøgg-class
Hauk-class
Skjold-class


==== Norwegian Coast Guard ====

Barentshav class OPV
Harstad class OPV
Nordkapp class OPV
Nornen class
Svalbard class icebreaker


=== Peru ===
Río Zarumilla class, Peruvian Coast Guard
Rio Cañete class, Peruvian Coast Guard


=== Philippines ===


==== Philippine Navy ====

Alberto Navarette class
Jose Andrada class
Rizal class patrol vessel
Malvar class patrol vessel
Jacinto class patrol vessel
Gregorio del Pilar-class offshore patrol vessel


==== Philippine Coast Guard ====

Gabriela Silang-class Offshore Patrol Vessel
San Juan-class patrol vessel
Parola-class patrol vessel
Ilocos Norte-class patrol boat


=== Portugal ===


==== Portuguese Navy ====
Viana do Castelo class
Tejo-class
Centauro class
Argos class
NRP Rio Minho


==== National Republican Guard (GNR) ====
Ribamar class


=== Qatar ===


==== Qatari Emiri Navy (QEN) ====
Musherib class


=== Romania ===
SNR-17 class patrol boats, Romanian Border Police
Stefan Cel Mare patrol vessel, Romanian Border Police


=== Russia ===

Stenka class patrol boat (Project 02059), Russian Navy and Russian Coast Guard
Bogomol Class patrol boat (Project 02065), Russian Navy
Mirage class patrol vessel (Project 14310), Russian Coast Guard
Svetlyak class patrol boat (Project 10410)， Russian Coast Guard
Ogonek class patrol boat (Project 12130), Russian Coast Guard
Mangust class patrol boat (Project 12150, Russian Coast Guard
Sobol class patrol boat (Project 12200）, Russian Coast Guard
Terrier class patrol boat (Project 14170), Russian Navy and Russian Coast Guard
Rubin class patrol boat (Project 22460), Russian Coast Guard
Okean class patrol vessel (Project 22100), Russian Coast Guard
Vosh class river patrol craft (Project 12481), Russian Coast Guard
Piyavka class river patrol craft (Project 1249), Russian Coast Guard
Ogonek class river patrol craft (Project 12130), Russian Coast Guard


=== Senegal ===

Fouladou (OPV 190), Senegalese Navy
Kedougou (OPV 45), Senegalese Navy
Ferlo (RPB 33), Senegalese Navy
Conejera (Class Conejera P 31), Senegalese Navy
Fouta (Osprey 55), Senegalese Navy
Njambuur (PR 72), Senegalese Navy


=== Singapore ===

Fearless-class patrol vessel, Republic of Singapore Navy
PK class Interceptor Craft, Police Coast Guard
1st Generation PT class patrol Craft, Police Coast Guard (decommissioned)
2nd Generation PT class patrol Craft, Police Coast Guard (decommissioned)
3rd Generation PT class patrol Craft, Police Coast Guard
4th Generation PT class patrol  Craft, Police Coast Guard
PC class patrol Craft, Police Coast Guard
Swift-class coastal patrol craft
Independence-class littoral mission vessel,  Republic of Singapore Navy


=== Slovenia ===
Slovenian patrol boat Triglav


=== South Africa ===

Warrior class (modified Saar 4 Open Sea Patrol Vessels)
Namacurra class


=== South Korea ===
Chamsuri-class (Republic of Korea Navy)
Yoon Youngha-class patrol vessel


=== Spain ===

Meteoro class
Descubierta class
Serviola class
Anaga class
Barceló class
Toralla class
Conejera class
Chilreu class
P111 class patrol boat
Cabo Fradera class


=== Sri Lanka ===
Jayasagara  class (Sri Lanka Navy)
Colombo class (Sri Lanka Navy)


=== Suriname ===
Ocea Type FPB 98 class fast patrol boat
Ocea Type FPB 72 class fast patrol boat


=== Sweden ===

Hugin-class (based on the Norwegian Storm-class, decommissioned) – 16 ships
Kaparen-class (Hugin-class modified with better subhunting capacity, decommissioned) – 8 ships
Stockholm-class (commissioned as corvettes, later converted to patrol boats) – 2 ships
HMS Carlskrona (commissioned as minelayer, later converted to ocean patrol vessel)Additionally, the Royal Swedish Navy also operates smaller types of patrol boats (Swedish: bevakningsbåt = "guard boat"):

Typ 60-class (decommissioned) – 17 ships
Tapper class – 12 shipsThe Swedish Coast Guard operate an additional 22 patrol vessels for maritime surveillance.


=== Thailand ===
Pattani class (Royal Thai Navy)
River class (Royal Thai Navy)
T.991 class (Royal Thai Navy)
krabi class (Royal Thai Navy)


=== Turkey ===

Kılıç II class, Turkish Navy
Kılıç I class，Turkish Navy
Yıldız class，Turkish Navy
Rüzgar class，Turkish Navy
Doğan class，Turkish Navy
Kartal class，Turkish Navy
Türk class，Turkish Navy
Tuzla class，Turkish Navy
KAAN 15 class，Turkish Coast Guard Command
KAAN 19 class，Turkish Coast Guard Command
KAAN 29 class，Turkish Coast Guard Command
KAAN 33 class，Turkish Coast Guard Command
SAR 33 class，Turkish Coast Guard Command
SAR 35 class，Turkish Coast Guard Command
80 class，Turkish Coast Guard Command


=== United Kingdom ===

Kingfisher-class patrol vessel of 1935
Motor Launch of World War II
Harbour Defence Motor Launch of World War II
River class patrol vessel
Castle class patrol vessel
Archer class patrol vessel
Island class patrol vessel
Scimitar class patrol vessel


=== United States ===


==== United States Navy ====

179-foot Cyclone class patrol ship – US Navy (1993– )
85-Foot Mark VI patrol boat (2016- )


==== United States Coast Guard ====
87-foot Marine Protector class coastal patrol boat – USCG
110-foot Island class patrol boat – USCG
154-foot Sentinel class cutter – USCG


=== Venezuela ===

Venezuelan patrol boat Naiguatá (GC-23)


=== Vietnam ===
Type TT-120 patrol boat, Vietnam Coast Guard
Type TT-200 patrol boat, Vietnam Coast Guard
Type TT-400 patrol boat, Vietnam Coast Guard
DN 2000(Damen 9014 class) offshore patrol vessels, Vietnam Coast Guard


== References ==