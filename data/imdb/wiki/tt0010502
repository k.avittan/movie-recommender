Figures of the Night (German:Nachtgestalten) is a 1920 German silent horror film written, directed and produced by Richard Oswald and starring Paul Wegener, Conrad Veidt, Reinhold Schünzel and Erna Morena. It is based on the novel Eleagabal Kuperus by Karl Hans Strobl. Strobl was the editor of a German horror fiction magazine called Der Orchideengarten which was said to have been influenced by the works of Edgar Allan Poe. Strobl was an anti-Semitic and later willingly joined the Nazi Party, which may explain why he has become an obscure literary figure today.The film's art direction was by Hans Dreier, and Carl Hoffmann handled the cinematography. Hoffmann went on to collaborate with such noted German directors as Fritz Lang and F.W. Murnau. It was filmed at the Babelsberg Studio in Berlin.


== Plot ==
Multi-billionaire Thomas Bezug is the richest man in the world and the loneliest. He is a cripple who can only laboriously move on his crutches, domineering, cruel, only full of fanatical love for his son, who lives like a monkey in a cage, climbs on trees and eats nuts. His minions are muscle-bound giants and misshapen dwarfs. His cunning secretary only strives to bring wealth into his possession. A dancer he loves leaves him. Then an unemployed inventor demands the hand of his nymphomaniac daughter for an invention with which it is supposed to be able to extract oxygen from the air. With it he wants to blackmail the whole world and become ruler over all countries.The film is of note since it co-starred both Wegener and Veidt in the same movie, both of whom later went on to star in two different film versions of The Student of Prague (1913 and 1926 respectively). Director Richard Oswald later cast Wegener in his 1932 film Uncanny Stories.


== Cast ==
Paul Wegener as Thomas Bezug
Reinhold Schünzel as Sekretär
Erna Morena as Elisabeth, Bezugs Tochter
Erik Charell as Arnold, Bezugs Sohn, Affe, gorilla
Conrad Veidt as Clown
Anita Berber as Tänzerin
Paul Bildt as Erfinder
Theodor Loos
Willi Allen


== References ==


== Bibliography ==
Hutchings, Peter. The A to Z of Horror Cinema. Scarecrow Press, 2009.


== External links ==
Figures of the Night on IMDb