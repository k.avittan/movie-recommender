Charley's Aunt is a farce in three acts written by Brandon Thomas. The story centres on Lord Fancourt Babberley, an undergraduate whose friends Jack and Charley persuade him to impersonate the latter's aunt. The complications of the plot include the arrival of the real aunt and the attempts of an elderly fortune hunter to woo the bogus aunt. The play concludes with three pairs of young lovers united, along with an older pair – Charley's real aunt and Jack's widowed father.
The play was first performed at the Theatre Royal, Bury St Edmunds in February 1892. It then opened in London at the Royalty Theatre on 21 December 1892 and quickly transferred to the larger Globe Theatre on 30 January 1893. The production broke the historic record for longest-running play worldwide, running for 1,466 performances. It was produced by the actor W. S. Penley, a friend of Thomas, who appeared as Babberley.
The play was also a success on Broadway in 1893, and in Paris, where it had further long runs.  It toured internationally and has been revived continually and adapted for films and musicals.


== Synopsis ==


=== Act I ===
Jack Chesney and Charley Wykeham are undergraduates at Oxford University in love, respectively, with Kitty Verdun and Amy Spettigue.  Charley receives word that his aunt, Donna Lucia d'Alvadorez, a rich widow from Brazil whom he has never met, is coming to visit him.  The boys invite Amy and Kitty to lunch to meet her, also intending to declare their love to the girls, who are being sent away to Scotland with Amy's uncle, Stephen Spettigue, who is also Kitty's guardian.  They seek out another Oxford undergraduate, Lord Fancourt Babberley (known as "Babbs"), to distract Donna Lucia while they romance their girls.  While they are out, Babbs breaks into Jack's room to steal all his champagne, but Jack and Charley intercept him and persuade him to stay for lunch.  Babbs tells the boys about his own love, the daughter of an English officer called Delahay, whom he met in Monte Carlo, although he does not remember her name.  Babbs also uses Jack's room to try on his costume for an amateur play in which he is taking part.
Amy and Kitty arrive to meet Jack and Charley, but Donna Lucia has not arrived yet, and so the girls leave to go shopping until she shows up.  Annoyed, Jack orders Charley to go to the railway station to wait for Donna Lucia.  Jack soon receives an unexpected visit from his father, Sir Francis Chesney, a retired colonel who served in India.  Sir Francis reveals that he has inherited debts that have wiped out the family's fortunes; instead of going into politics as he had intended, Jack will have to accept a position in Bengal.  Horrified, Jack suggests that Sir Francis should marry Donna Lucia, a widow and a millionaire, in order to clear the family debts.  Sir Francis is hesitant but agrees to meet Donna Lucia before he makes a decision.

Charley receives a telegram saying that Donna Lucia will not be arriving for a few days.  The boys panic: the girls are coming, and they won't stay without a chaperone.  Fortunately Babbs's costume happens to be that of an old lady.  Jack and Charley introduce Babbs as Charley's aunt.  His strange appearance and unchanged voice (he had never acted before) do not raise any suspicions.  Babbs annoys the boys by accepting kisses from Amy and Kitty; the boys respond to his flirtations with violence.
Sir Francis soon enters to meet Donna Lucia. He takes one look at Babbs and tries to leave, but Jack retrieves him.  Spettigue arrives, angered that Kitty and Amy are lunching with the boys without his permission.  However the penniless Spettigue soon learns that Charley's aunt is Donna Lucia D'Alvadorez, the celebrated millionaire. He decides to stay for lunch to attempt to woo "Donna Lucia".


=== Act II ===
Outside Jack's rooms, in the grounds of St Olde's College, the boys are trying to get their girls alone so that they can confess their love. However, Babbs is in the way, charming the girls as Donna Lucia.  Jack's father, Sir Francis, has decided to propose marriage to Donna Lucia, purely for money.  Jack urgently corners Babbs and orders him to let his father down gently.  Babbs does so, which Sir Francis finds to be a relief.  Spettigue still wants to marry "Donna Lucia" for her money.
Meanwhile, the real Donna Lucia, who turns out to be an attractive woman of middle age, arrives with her adopted niece, Miss Ela Delahay, an orphan.  The money left to Ela by her father is enough to make her independent for life.  Ela reveals that her father had won a lot of money at cards from Fancourt Babberley, for whom Ela still holds a great deal of affection.  Donna Lucia recounts the story of a colonel named Frank who she once met more than twenty years ago, of whom she was similarly fond.  However, he was too shy to propose, and he left for India before he could tell her how he felt.  Sir Francis enters, Donna Lucia recognizes him, and the two rekindle their affection.  However, before she can introduce herself, she discovers that someone is impersonating her.  To investigate, she introduces herself as "Mrs Beverly-Smythe", a penniless widow.
Jack and Charley finally make their declarations of love to their girls. However, they discover that they need Spettigue's consent to marry. The girls enlist Babbs to get the consent from the greedy Spettigue.  Spettigue invites the entire party, including the real Donna Lucia and Ela, to his house, so that he can talk to "Donna Lucia" in private.  Babbs, recognizing Ela as the girl he fell in love with in Monte Carlo, tries to escape, but he is caught by Spettigue.


=== Act III ===
Babbs is upset by being in the same room as the girl he loves without being able to talk to her.  Jack and Charley try to calm him down. Babbs spends time with the real Donna Lucia, Ela, Amy and Kitty, during which the real Donna Lucia embarrasses Babbs by showing how little he really knows about Donna Lucia.  Ela takes a liking to the fake Donna Lucia, who sounds like the man she loves, and pours her heart out to Babbs, telling him of the anguish of losing her father and of the man who cared for him in his dying days, Lord Fancourt Babberley. She admits that she loves him and longs to see him again.

Babbs tricks Spettigue into giving the letter of consent for the marriages of Charley to Amy and Jack to Kitty by accepting marriage to Spettigue. (Kitty's father's will specified that if she marries without Spettigue's consent, Spettigue would inherit all of the money.)  Charley can no longer keep up the lie and admits that "Donna Lucia" is not really his aunt.  Babbs, now dressed in a suit, confirms that he had been playing the part of Charley's aunt.  As he is about to return to Spettigue the letter of consent, the real Donna Lucia reveals her identity and takes the letter, stating that it "is addressed to and has been delivered to Donna Lucia d’Alvadorez".
Spettigue storms off, threatening to dispute the letter.  Amy is upset at everyone for making a fool of her uncle.  Donna Lucia reassures her and gives the girls the letter.  Sir Francis and Donna Lucia are engaged (he made the proposal before he realized her identity); the young couples can marry; and Babbs confesses his feelings to Ela.


== Productions ==


=== Original production ===
The play was originally given at Bury St. Edmunds on 29 February 1892, commissioned by the local hunt, which sponsored a new play every year for its annual social festivities, known as the "Hunt Bespeak."  Penley produced the piece and played Lord Fancourt Babberly, Both the local paper and the leading national theatrical paper, The Era, record Charley's surname as Wyckenham. It was soon simplified to Wykeham.  A provincial tour followed, including Colchester, Cambridge and Cheltenham, with the original cast.  Penley recast some of the roles, presenting the play at Derby in May. The original and Derby casts were as follows:

Stephen Spettigue, uncle of Amy, guardian of Kitty, and the story's villain – Henry Crisp
Colonel Sir Francis Chesney, father of Jack Chesney – Arthur Styan (Gerald Godfrey in Derby)
Jack Chesney, Oxford undergraduate, in love with Kitty – Wilton Heriot (H. J. Carvill in Derby)
Charley Wykeham, Oxford undergraduate, in love with Amy – Ernest Lawford (Brandon Hurst in Derby)
Lord Fancourt Babberley, undergraduate pulled unwillingly into Jack and Charley's scheme – W. S. Penley
Brassett, Jack's valet – Harry Nelson (Percy Brough in Derby) – Charles King
Donna Lucia d'Alvadorez, Charley's aunt from Brazil – Ada Branson
Amy Spettigue, Stephen Spettigue's young niece – Lena Burleigh (Rose Nesbitt in Derby)
Kitty Verdun, Stephen Spettigue's young ward – Dora de Winton
Ela Delahay, orphaned young woman accompanying Donna Lucia (loved by Lord Fancourt) – Emily Cudmore

After a further provincial tour, Penley secured the Royalty Theatre in London, which had suddenly fallen vacant, and opened the play there on 21 December 1892.  He again recast the piece, except for a few roles, as follows:

Stephen Spettigue – Ernest Hendrix
Colonel Chesney – Brandon Thomas
Jack Chesney – Percy Lyndal
Charley Wykeham – H. Farmer
Lord Fancourt Babberley – W. S. Penley
Brasset – Cecil Thornbury
Footman – G. Graves
Donna Lucia – Ada Branson
Amy Spettigue – Kate Gordon
Kitty Verdun – Nina Boucicault
Ela Delahay – Emily CudmoreThe play was an immediate success, opening to enthusiastic audiences and excellent notices from the press.  It soon transferred to the larger Globe Theatre on 30 January 1893. It ran for a record-breaking 1,466 performances across four years, closing on 19 December 1896.


=== Revivals ===
During the original London run, seven companies toured the United Kingdom with the play.  The piece was successfully staged throughout the English-speaking world and, in translation, in many other countries.  It had a major success on Broadway, opening on 2 October 1893 at the Standard Theatre, starring Etienne Girardot, where it ran for another historic long run of four years.  It was revived on Broadway several times until 1970. Charley's Aunt was given in a German translation as Charleys Tante at Weimar in August 1894. The first French production (La Marraine de Charley) was the following month at the Théâtre de Cluny in Paris, where it ran for nearly 300 performances. The play was produced in Berlin every Christmas for many years.  In 1895, The Theatre recorded that Charley's Aunt had been taken up in country after country.  "From Germany it made its way to Russia, Holland, Denmark and Norway, and was heartily welcomed everywhere."Thomas and Penley quarrelled and went to law over the licensing of an 1898 American production.  Penley contended that the original idea for the play had been his, and that Thomas had merely turned it into a playscript.  Penley had, on this pretext, secretly negotiated a deal with the American producer, Charles Frohman, which gave Thomas only one third of the royalties.  Penley told a journalist, in 1894: "The play was my idea and Brandon Thomas wrote it.  Later on, we went down into the country and worked at it.  Then we worked it out on the stage."  Despite this rift, Penley continued to play Fancourt Babberley in frequent West End productions until he retired from acting in 1901.

Thomas revived the play at the Comedy Theatre in London in 1904, once again playing Sir Francis Chesney.  He revived it again in 1905, 1908 and in 1911, when his daughter, Amy Brandon Thomas, played Kitty.  In her later years, Amy played the role of Donna Lucia in revivals. Thomas's son, Jevan Brandon-Thomas played Jack in three London revivals of the play and directed the annual London revivals from 1947 to 1950.  Amy Brandon-Thomas insisted on setting the play in the present at each revival, despite protests from critics that it would be better played in the period in which it was written.  Eventually, for a West End revival in 1949, Victorian dresses and settings were introduced, designed by Cecil Beaton. Nearly continuous revivals have played "somewhere in London" and elsewhere in Britain since the original production. Foreign language productions have included a 2007 Czech production.Actors who have played Lord Fancourt Babberley in the West End include Richard Goolden, Leslie Phillips, John Mills, Frankie Howerd, Tom Courtenay, Griff Rhys Jones and John Wood. Performers who played the juvenile roles early in their careers include Noël Coward, John Gielgud, Rex Harrison, Betty Marsden, Ralph Michael and Gerald Harper.  In the US, Babbs has been played in various revivals by such actors as José Ferrer, Roddy McDowall and Raúl Juliá.


== Adaptations ==

Silent film versions of the play were released in 1915 and 1925, the latter featuring Sydney Chaplin (brother of Charlie Chaplin) and Ethel Shannon. A well-received sound film version starring Charles Ruggles was released in 1930. In 1934 a German version Charley's Aunt was released; directed by Robert A. Stemmle it starred Fritz Rasp, Paul Kemp and Max Gülstorff. Arthur Askey took the leading role in a 1940 British film Charley's (Big-Hearted) Aunt that developed themes from the original play. Perhaps the best known film version was released in 1941, directed by Archie Mayo and starring Jack Benny in the title role. This version slightly alters the plotline from the original version (for instance, Babbs is framed for accidentally setting off a fire alarm at Oxford University and faces expulsion). The play's story was popular in Germany and Austria, with at least four different film versions being released: in 1934; 1956 (starring Heinz Rühmann); 1963 (starring Peter Alexander), and a television version in 1976. A 1959 Danish film version starred Dirch Passer in the principal role and featured Ove Sprogøe, Ghita Nørby and Susse Wold.  In the film, Passer sings the song "Det er svært at være en kvinde nu til dags" (English: "It is hard to be a woman nowadays"). Passer had first played the role in Charley's Tante in 1958 at the ABC Theatre where it was a hit and played for 1½ years. An Austrian film version was made in 1963. In Spain, there is a 1981 film version starring Paco Martínez Soria, titled La Tía de Carlos.  Two film adaptations have appeared in Egypt: a silent version in 1920 titled al-Khala al-Amrikiyya starring Ali al-Kassar, and a sound film in 1960 titled Sukkar Hanim, starring Samia Gamal.The earliest operatic adaptation of the play is the 1897 zarzuela La viejecita by Manuel Fernández Caballero, which is still performed in Spain. The role of Carlos (Charlie) was written as a breeches role for Lucrecia Arana. Jevan Brandon-Thomas wrote a pantomime version, Babbs in the Wood, for the amusement of the 1930 London cast and their friends. The Observer commented, "It is quite clear that Mr Brandon-Thomas could earn a handsome living at any time in low – very low – comedy." A Broadway musical version, Where's Charley?, with a book by George Abbott and music and lyrics by Frank Loesser, starred Ray Bolger as Charley.  It ran between 1948 and 1950 at the St. James Theatre and featured the song "Once in Love with Amy" for Bolger. The musical was made into a 1952 film (with Bolger repeating his stage role) and had a successful run in London beginning in 1958 at the Palace Theatre.In 1957, CBS television in the US aired a live production as part of the Playhouse 90 series, starring Art Carney as Babbs, and Orson Bean as Charley and Jeanette MacDonald as the real Donna Lucia. In the 1960s BBC Television broadcast three productions of the play. The first, in 1961 featured Bernard Cribbins as Babbs, Donald Wolfit as Spettigue and Rosalie Crutchley as Donna Lucia. Richard Briers played Babbs in a 1965 version, and a 1969 production starred  Danny La Rue, Coral Browne and Ronnie Barker.A Soviet version was made for television in 1975, entitled Hello, I'm Your Aunt!. A Chinese version, Li Cha's Aunt, first adapted as a musical play in 2015; it was performed over 500 times and revived. Li Cha's Aunt was adapted as a Chinese film of the same name in 2018 (the film poster also shows the English title "Hello, Mrs. Money"). Indian versions include Moruchi Mavshi, a Marathi adaptation by Pralhad Keshav Atre first performed in 1947 and still revived, and Khalid Ki Khala, an Urdu adaptation mounted by the Hindustani Theatre in 1958.


== Notes ==


== References ==
Gaye, Freda (ed.) (1967). Who's Who in the Theatre (fourteenth ed.). London: Sir Isaac Pitman and Sons. OCLC 5997224.CS1 maint: extra text: authors list (link)


== External links ==
IBDB entry
Charley's Aunt on IMDb (1925 film version)
Charley's Aunt on IMDb (1930 film version)
Charley's Aunt on IMDb (1934 German film version)
Charley's Aunt on IMDb (1941 film version)
Charley's Aunt on IMDb (1956 film version)
Charley's Aunt on IMDb (1983 TV film version)
Zdravstvuyte, ya vasha tyotya! (Hello, I'm Your Aunt!) on IMDb (1975 film version, Soviet Union)
List of longest theatre runs, 1875–1920
Numerous photos from Charley's Aunt
Charleys Aunt performance details in the University of Bristol Theatre Archive
Critique in ČSFD.cz
Theatre Company of Jan Hrušínský, Prague