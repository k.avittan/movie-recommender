"Dream Weaver" is a song by the American singer Gary Wright, released as the first single from his third studio album The Dream Weaver in December 1975.
The track features Wright on vocals and keyboards and Jim Keltner on drums. According to Gary Wright, the song was inspired by Autobiography of a Yogi, which was given to him by George Harrison. Paramhansa Yogananda's poem "God! God! God!" made reference to "the idea of the mind weaving dreams". The expression "Dream Weaver" was popularized by John Lennon in 1970 in his song "God", taken from his solo album John Lennon/Plastic Ono Band. This song depicts Lennon's declaration that he was the dream weaver of the 1960s, breaking away from the influences and dogmas that influenced his life.
All instrumentation was created using keyboards except for Keltner's percussion. Gary Wright re-recorded "Dream Weaver" twice, first in 1986 (spelled "Dreamweaver" this time) for the Fire and Ice movie soundtrack, then in 1992 a longer version for the Wayne's World movie soundtrack.


== Chart performance ==
In 1976, the song became a hit in the US; it peaked at #2 on the Billboard chart.  It was kept from #1 by both "December, 1963 (Oh, What a Night)" by The Four Seasons and "Disco Lady" by Johnnie Taylor. "Dream Weaver" did get to #1 on Cashbox.


== In popular culture ==


=== In films ===
The 1982 film Endangered Species, when a minor character was listening to it on a portable radio.
The 1984 film This Is Spinal Tap.
According to Wes Craven, the song (and its keyboard intro/outro) inspired the concept behind the 1984 film A Nightmare on Elm Street.
The 1986 film Sid and Nancy.
The song was re-recorded for the soundtrack of the 1986 film Fire and Ice.
The 1987 film Spaceballs.
The 1989 film Bill & Ted's Excellent Adventure.
The song was once again re-recorded for the soundtrack of the 1992 film Wayne's World. A re-recorded version by Wright appears in the 2016 movie Ghost Team.
The 1994 film Airheads.
The 1996 film The People vs. Larry Flynt.
The 1997 film The Full Monty.
The 2003 film Daddy Day Care.
The 2010 Pixar film Toy Story 3.
The song appeared twice in the 2016 film Ice Age: Collision Course.
The 2020 Nickelodeon Movies films The SpongeBob Movie: Sponge on the Run
The 2021 Illumination film Minions: The Rise of Gru
The 2022 Paramount Animation film Spellbound


=== In television ===
Gary Wright performed the song on an episode of The Midnight Special.
Portions of "Dream Weaver" were first sampled in a short film appearing in the 1990 Wayne's World episode of Saturday Night Live, in which guest star Wayne Gretzky appears, and Wayne Campbell is picturing that song playing as he imagines himself defeating Gretzky in street hockey and then winning his wife, Janet Jones.
"Dream Weaver" was heard over the closing credits of the sixth episode of the fourth season of the classic HBO sketch comedy series  Mr. Show with Bob and David.
Fox comedy That '70s Show episode, "The First Time" featured "Dream Weaver".
"Dream Weaver" appeared in the Fox black comedy Scream Queens in the episode, "Lovin' the D".
A Japanese version of "Dream Weaver" is heard in the Fox animated comedy Family Guy in the episode, "Carter and Tricia".
"Dream Weaver" was the name of season 1, episode 13 of The Neighbors, where the plot centers around Marty and Debbie Weaver meeting at a high school dance while "Dream Weaver" played.
"Dream Weaver" was featured in an episode of Nickelodeon's Nicky, Ricky, Dicky & Dawn titled "Quaddyshack". It was also featured in a Nicky, Ricky, Dicky & Dawn episode titled "The Wonderful Wizard of Quads".
"Dream Weaver" was featured in an episode of Netflix's Fuller House titled "Hale's Kitchen".
"Dream Weaver" was played on The Tonight Show Starring Jimmy Fallon.


== In other media ==


=== Use by other musical artists ===
"Dream Weaver" and "Love Is Alive" are sampled in "Wordz of Wisdom", a single from 3rd Bass's release, The Cactus Album (1989).
Dance artist Erin Hamilton covered it for her album One World (1999).
It was featured prominently in the gay love story film Trick (1999).
Crowbar recorded a doom metal version of "Dream Weaver" for their album Equilibrium (2000).
Electronic rock artist JES covered it as a single release (2015).
Canadian singer-songwriter Kevin Quain released a version of it in October 2017.
Multiple short clips of the song can be heard in the background of the Stadia launch trailer.


== Chart performance ==


=== Certifications ===


== See also ==
1975 in music


== References ==


== External links ==
CASH BOX TOP SINGLES – 1976
Lyrics of this song at MetroLyrics
Stadia – Official Launch Trailer