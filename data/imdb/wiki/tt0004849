William Surrey Hart (December 6, 1864 – June 23, 1946) was an American silent film actor, screenwriter, director and producer. He is remembered as a foremost Western star of the silent era who "imbued all of his characters with honor and integrity." During the late 1910s and early 1920s, he was one of the most consistently popular movie stars, frequently ranking high among male actors in popularity contests held by movie fan magazines.


== Biography ==
Hart was born in Newburgh, New York, to Nicholas Hart (c. 1834–1895) and Rosanna Hart (c. 1839–1909). William had two brothers, who died very young, and four sisters. His father was born in England, and his mother was born in Ireland. He was a distant cousin of the western star Neal Hart.
He began his acting career on stage in his 20s, and in film when he was 49, which coincided with the beginning of film's transition from curiosity to commercial art form. Hart's stage debut came in 1888 as a member of a company headed by Daniel E. Bandmann. The following year he joined Lawrence Barrett's company in New York and  later spent several seasons with Mlle. Hortense Rhéa's traveling company. He toured and traveled extensively while trying to make a name for himself as an actor, and for a time directed shows at the Asheville Opera House in North Carolina, around the year 1900. He had some success as a Shakespearean actor on Broadway, working with Margaret Mather and other stars; he appeared in the original 1899 stage production of Ben-Hur. His family had moved to Asheville but, after his youngest sister Lotta died of typhoid fever in 1901, they all left together for Brooklyn until William went back on tour.Hart went on to become one of the first great stars of the motion picture Western. Fascinated by the Old West, he acquired Billy the Kid's "six shooters" and was a friend of legendary lawmen Wyatt Earp and Bat Masterson. He entered films in 1914 where, after playing supporting roles in two short films, he achieved stardom as the lead in the feature The Bargain. Hart was particularly interested in making realistic Western films. His films are noted for their authentic costumes and props, as well as Hart's acting ability, honed on Shakespearean theater stages in the United States and England.
Beginning in 1915, Hart starred in his own series of two-reel Western short subjects for producer Thomas Ince, which were so popular that they were supplanted by a series of feature films. Many of Hart's early films continued to play in theaters, under new titles, for another decade. In 1915 and 1916 exhibitors voted him the biggest money making star in the United States. In 1917 Hart accepted a lucrative offer from Adolph Zukor to join Famous Players-Lasky, which merged into Paramount Pictures. In the films Hart began to ride a brown and white pinto he called Fritz. Fritz was the forerunner of later famous movie horses known by their own name, e.g., horses like Tom Mix's Tony, Roy Rogers's Trigger and Clayton Moore's Silver. In 1917, to signify "his patriotism and loyalty to Uncle Sam" it was announced to "change the name of his favorite horse from Fritz to one more truly American." He also volunteered from 1917 to 1918 with the Four Minute Men program to give short pro-war speechs across the country. Hart was now making feature films exclusively, and films like Square Deal Sanderson and The Toll Gate were popular with fans. Hart married young Hollywood actress Winifred Westover. Although their marriage was short-lived, they had one child, William S. Hart, Jr. (1922–2004).
In 1921, Hollywood comic actor Roscoe Arbuckle was charged with rape and manslaughter in the death of aspiring actress Virginia Rappe. Amid the controversy, many of Arbuckle's fellow actors declined public comment on the case. However, Hart, who had never worked with Arbuckle or even met him, made a number of damaging public statements in which he presumed the actor's guilt. Arbuckle, who was eventually acquitted but saw his career ruined, later wrote a premise for a film parodying Hart as a thief, bully and wife beater, and it was bought by Buster Keaton. The following year, Keaton co-wrote, directed and starred in the 1922 comedy film The Frozen North. As a result, Hart refused to speak to Keaton for many years.By the early 1920s, Hart's brand of gritty, rugged Westerns with drab costumes and moralistic themes gradually fell out of fashion. The public became attracted by a new kind of movie cowboy, epitomized by Tom Mix, who wore flashier costumes and was involved in more action scenes. Paramount dropped Hart, who then made one last bid for his kind of Western. He produced Tumbleweeds (1925) with his own money, arranging to release it independently through United Artists. The film turned out well, with an epic land-rush sequence, but did only fair business at the box office. Hart was angered by United Artists' failure to promote his film properly and sued the studio. The legal proceedings dragged on for years, and the courts finally ruled in Hart's favor, in 1940.
After Tumbleweeds, Hart retired to his Newhall, California, ranch home, "La Loma de los Vientos", which was designed by architect Arthur R. Kelly. In 1939 he appeared in his only sound film, a spoken prologue for a reissue of Tumbleweeds. In this segment, filmed at his ranch, the 74-year-old Hart reflected on the Old West and fondly recalled his silent movie heyday. The speech turned out to be his farewell to the screen. Most prints and video versions of Tumbleweeds circulating today include the speech. 
Hart died on June 23, 1946, in Newhall at age 81. He was buried in Green-Wood Cemetery in Brooklyn, New York.


== Dedications ==
For his contribution to the motion picture industry, William S. Hart has a star on the Hollywood Walk of Fame at 6363 Hollywood Blvd. In 1975, he was inducted into the Western Performers Hall of Fame at the National Cowboy & Western Heritage Museum in Oklahoma City, Oklahoma.
As part of the Natural History Museum of Los Angeles County, California, Hart's former home and 260-acre (1.1 km²) ranch in Newhall is now William S. Hart Park. The William S. Hart High School District as well as William S. Hart Senior High School, both located in the Santa Clarita Valley in the northern part of Los Angeles County, were named in his honor. A Santa Clarita baseball field complex is named in his honor.
The "Range Rider of the Yellowstone," a statue commissioned by Hart and modeled from life, stands on the Rimrocks in front of the airport at Billings, Montana. Hart donated it to the city in 1927, where it remains a memorial to his memory. [1].
On November 10, 1962, Hart was honored posthumously in an episode of The Roy Rogers and Dale Evans Show, a short-lived western variety program on ABC.


== Published books ==

After Hart retired from film making he began writing short stories and book-length manuscripts. His published books are:

Pinto Ben and Other Stories (written with Mary Hart), 1919, Britton Publishing Company
The Golden West Boys, Injun and Whitey, 1920, Grossett & Dunlap
Injun and Whitey Strike Out for Themselves, 1921, Grossett & Dunlap
Injun and Whitey to the Rescue, 1922, Grossett & Dunlap
"Told Under a White Oak Tree" (credited as by "Bill Hart's Pinto Pony"), 1922, Houghton Mifflin Co.
A Lighter of Flames, 1923, Thomas Y. Crowell
The Order of Chanta Sutas, 1925, unknown publisher
My Life East and West, 1929, Houghton Mifflin Co.
Hoofbeats, 1933, Dial Press
Law on Horseback and Other Stories, 1935, self-published
And All Points West (written with Mary Hart), 1940, Lacotah Press


== Selected filmography ==

		
		
		
		
		


== William S. Hart Ranch and Museum ==
When Hart died, he bequeathed his home to Los Angeles County so that it could be converted into a park and museum. His former home in Newhall, Santa Clarita, California has become a satellite of the Natural History Museum of Los Angeles County and remains free and open to the public to this day. The home is a Spanish Colonial Revival style mansion and contains many of the movie star's possessions including Native American artifacts and works by artists Charles Marion Russell, James Montgomery Flagg, and Joe de Yong. The Museum is an important part of Hart's legacy as he said before he died: "When I was making pictures, the people gave me their nickels, dimes, and quarters. When I am gone, I want them to have my home." The surrounding 265-acre William S. Hart Park includes the mansion, trails, an animal area with farm animals, bison, and a picnic area. Since 2015, the park has been home to the Santa Clarita Cowboy Festival and Annual Hart of the West Powwow. The former being previously held at Melody Ranch.


== References ==


== Further reading ==
William Surrey Hart, My Life East and West, New York: Houghton Mifflin Company, 1929.
Jeanine Basinger, Silent Stars, 1999 (ISBN 0-8195-6451-6). (chapter on William S. Hart and Tom Mix)
Ronald L. Davis, William S. Hart: Projecting the American West, University of Oklahoma Press, 2003.


== External links ==

William S. Hart on IMDb
William S. Hart at the Internet Broadway Database 
In Loving Memory of William S. Hart
William S. Hart Ranch and Museum
"William S. Hart History". Santa Clarita Valley Historical Society. Retrieved July 4, 2004. (Photos & text)
William S. Hart Photos and History
Ron Schuler's Parlour Tricks: The Good Badman
The Haunted Hart Ghost Site
William S. Hart Union High School District, Santa Clarita Valley, California
William S. Hart High School, Newhall, California
Photographs of William S. Hart
William S. Hart at Find a Grave
Works by William S. Hart at Project Gutenberg
Works by William Surrey Hart at Faded Page (Canada)
Works by or about William S. Hart at Internet Archive
Works by William S. Hart at LibriVox (public domain audiobooks)