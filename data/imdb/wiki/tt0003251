"Ta-ra-ra Boom-de-ay" is a vaudeville and music hall song. The song's first known public performance was in Henry J. Sayers' 1891 revue Tuxedo, which was performed in Boston, Massachusetts. The song became widely known in the version sung by Lottie Collins in London music halls in 1892. The melody was later used in various contexts, including as the theme song to the television show Howdy Doody.


== Background ==
The song's authorship was disputed for some years. It was originally credited to Henry J. Sayers, who was the manager of Rich and Harris, a producer of the George Thatcher Minstrels. Sayers used the song in the troupe's 1891 production Tuxedo, a minstrel farce variety show in which "Ta-ra-ra Boom-de-ay" was sung by Mamie Gilroy. However, Sayers later said that he had not written the song, but had heard it performed in the 1880s by a black singer, Mama Lou, in a well-known St. Louis brothel run by "Babe" Connors. Another American singer, Flora Moore, said that she had sung the song in the early 1880s.Stephen Cooney, Lottie Collins' husband, heard the song in Tuxedo and purchased from Sayers rights for Collins to perform the song in England. Collins worked up a dance routine around it, and, with new words by Richard Morton and a new arrangement by Angelo A. Asher, she first sang the song at the Tivoli Music Hall on The Strand in London in December 1891 to an enthusiastic reception; it became her signature tune. Within weeks, she included it in a pantomime production of Dick Whittington and performed it to great acclaim in the 1892 adaptation of Edmond Audran's opérette, Miss Helyett. According to reviews at the time, Collins delivered the suggestive verses with deceptive demureness, before launching into the lusty refrain and her celebrated "kick dance", a kind of cancan in which, according to one reviewer, "she turns, twists, contorts, revolutionizes, and disports her lithe and muscular figure into a hundred different poses, all bizarre".The song was performed in France under the title 'Tha-ma-ra-boum-di-hé', first by Mlle. Duclerc at Aux Ambassadeurs in 1891, but the following year as a major hit for Polaire at the Folies Bergère. In 1892 The New York Times reported that a French version of the song had appeared under the title 'Boom-allez'.Later editions of the music credited its authorship to various persons, including Alfred Moor-King, Paul Stanley, and Angelo A. Asher. Some claimed that the song was originally used at American revival meetings, while Richard Morton, who wrote the version of the lyric used in Lottie Collins' performances, said that its origin was "Eastern". Around 1914 Joe Hill wrote a version which tells the tale of how poor working conditions can lead workers into "accidentally" causing their machinery to have mishaps. A 1930s lawsuit determined that the tune and the refrain were in the public domain.

The character Tarara, in the 1893 Gilbert & Sullivan comic opera Utopia, Limited, is the "public exploder". A 1945 British film of the same name describes the history of music hall theatre. From 1974 to 1988 the Disneyland park in Anaheim, California USA presented a portion of the song as part of a musical revue show entitled America Sings. Containing four acts in a revolving carousel theater, the song was part of the finale in Act 3: The Gay 90s.


== Other lyrics ==
The widely recognizable melody has been re-used for numerous other songs, children's camp songs, parodies, and military ballads from the early 20th century. It was used for the theme song to the show Howdy Doody (as "It's Howdy Doody Time"). A popular folk parody among elementary school children was "We had no school today" (with several variants).


== Lyrics ==


=== As sung by Lottie Collins ===


=== As laundered and published by Henry J. Sayers as sheet music ===


=== Joe Hill version ===


== Notes ==


== References ==
Sheet music (1940) from the library of Indiana University
Banana Splits, Dilly Sisters and Hocus Pocus Park
The Dilly Sisters singing Ta-ra-ra Boom-de-ay (begins at 1:59) Accessed 2009/02/09