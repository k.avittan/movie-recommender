The Forgiven is a 2017 British drama film directed by Roland Joffé starring Forest Whitaker, Eric Bana and Jeff Gum. Joffé co-wrote the script with Michael Ashton on the basis of the play The Archbishop and the Antichrist by Michael Ashton, which tells a story involving Archbishop Desmond Tutu's search for answers during the Truth and Reconciliation Commission and his meetings with the fictional character Piet Blomfeld.


== Plot ==
After the end of apartheid, Archbishop Desmond Tutu is running the Truth and Reconciliation Commission and visits Cape Town's Pollsmoor Maximum Security Prison to meet with Piet Blomfeld, an ex-security police officer and Afrikaner Weerstandsbeweging member, to assess his candidacy for amnesty. Blomfeld is a potential witness to murders committed during the time of Apartheid, particularly the murder of the teenage daughter of Mrs. Morobe, who begs the Archbishop to find answers about her missing daughter. Blomfeld initially shows no regret and no willingness to speak, instead using his time during the meetings to berate the Archbishop with insults. However, when the Archbishop shows him a photograph of a young black girl in his file and tells him that he is in two prisons, (the other being the prison within himself) he begins to reconsider.
Francois Schmidt, one of Blomfeld's fellow officers and a participant in the murders, initially tries to pay off a black gang to assassinate Blomfeld, but Blomfeld (having been affected by the Archbishop's words) spares the life of the prisoner sent to kill him and manages to earn the respect of the gang to the point where they tell Schmidt to fight his own battles. He also forms somewhat of a bond with the prisoner, Benjamin. As this is going on, Howard Varney (one of the members of the Commission) speaks with Mrs. Van der Berghe, who had known Blomfeld when he was a child. He learns that Piet Blomfeld had been friends with a young black girl (the girl in the photograph); his father Rian did not approve and murdered the girl and her entire family before beating Piet so badly he broke his arm. Afterwards the Blomfelds moved to Zimbabwe; while Piet was at boarding school the rest of his family was killed (presumably as retaliation for Rian's crimes.)
Blomfeld ultimately decides to confess, and calls the archbishop. Schmidt arranges for the prison to be locked down and then attacks Blomfeld accompanied by riot troopers; Blomfeld is ultimately beaten to death by the troopers. However, he has sent an audio recording, confessing to the murder of Mrs. Morobe's daughter and her boyfriend, which is played to the Truth and Reconciliation Commission (he also leaves Benjamin what little money is in his bank account). Mrs. Morobe confronts Hansi Coetzee, one of the participants in her daughter's murder and ultimately forgives him. Afterwards the Archbishop is met by Linda Coetzee, who thanks him for his work and admits that she hadn't wanted to face what her husband had done.
The movie ends with the Archbishop and his wife visiting the beach he had gone to as a child, now successfully integrated.


== Cast ==
Forest Whitaker as Archbishop Desmond Tutu
Eric Bana as Piet Blomfeld
Jeff Gum as Francois Schmidt
Morné Visser as Hansi Coetzee
Thandi Makhubele as Mrs. Morobe
Terry Norton as Lavinia
Osbert Solomons as Mogomat
Rob Gough as Howard Varney
Debbie Sherman as Linda Coetzee
Warrick Grier as Kruger
Nandiphile Mbeshu as Benjamin
David Butler as Governor
Dominika Jablonska as Forensic Clerk
Shane John Kruger as DTF Officer
Michael MacKenzie as Young Warder
Vuyolwethu Adams as Idukew
Joe Nabe as Judge Draad
Alexander Wallace as Young Piet Blomfeld
Robert Hobbs as Burly


== Production ==
On 2 November 2015, it was announced that Forest Whitaker and Vince Vaughn would star as Archbishop Desmond Tutu and the fictional character Piet Blomfield, respectively, in the film adaptation of Michael Ashton's play The Archbishop and the Antichrist, to be directed by Roland Joffé. Joffé also co-wrote the script with Ashton, which would be produced by Craig Baumgarten. On 27 October 2016, Eric Bana joined the film to play the brutal murderer Blomfeld, replacing Vaughn. The film was retitled The Forgiven and would be produced by Joffé with Baumgarten through Link Entertainment and Zaheer Goodman-Bhyat through Light and Dark Films.


== Release ==
The film premiered at the London Film Festival on 13 October 2017. It was also shown as the closing film at the Pan African Film Festival on 19 February 2018 and was shown at the Belgrade Film Festival on 28 February 2018 before being released in the United States on 9 March 2018.


== Reception ==


=== Box office ===
The Forgiven grossed $39,177 in territories outside North America, sales of its DVD/Blu-ray releases have cashed $54,156.


=== Critical response ===
On review aggregator website Rotten Tomatoes, the film has an approval rating of 55% based on 22 reviews and an average rating of 6.2/10. On Metacritic, the film has a score of 41 out of 100 based on 9 critics, indicating "mixed or average reviews". Gary Goldstein of the Los Angeles Times wrote that the 'sluggishly paced film's disparate parts never come together as a compelling whole.' Glenn Kenny of The New York Times wrote that the film by Roland Joffé, who is 'not known for a light touch', 'is heavy-handed from its early texts explaining apartheid through its end credits' but praised the performances of Forest Whitaker and Eric Bana.


== References ==


== External links ==
The Forgiven on IMDb 
The Forgiven at AllMovie