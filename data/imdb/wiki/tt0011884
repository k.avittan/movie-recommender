Can You Ever Forgive Me? is a 2018 American biographical film directed by Marielle Heller and with a screenplay by Nicole Holofcener and Jeff Whitty, based on the 2008 confessional memoir of the same name by Lee Israel. Melissa McCarthy stars as Israel, and the story follows her attempts to revitalize her failing writing career by forging letters from deceased authors and playwrights. The film also features Richard E. Grant, Dolly Wells, Jane Curtin, Anna Deavere Smith, Stephen Spinella, and Ben Falcone in supporting roles. Israel took the title from an apologetic line in a letter in which she posed  as Dorothy Parker.The film had its world premiere at the Telluride Film Festival on September 1, 2018, and was released in the United States on October 19, 2018, by Fox Searchlight Pictures. The film grossed $11.7 million on a $10 million budget and was praised for McCarthy and Grant's performances.  It was named by the National Board of Review as one of their top ten films of 2018. For their performances, McCarthy and Grant earned nominations for Best Actress and Best Supporting Actor, respectively, at the 91st Academy Awards, the 76th Golden Globe Awards, and the 72nd British Academy Film Awards, among other ceremonies. Holofcener and Whitty were nominated for the Academy Award for Best Adapted Screenplay and won the Writers Guild of America Award for Best Adapted Screenplay.


== Plot ==
In 1991, following the critical and commercial failure of her biography of Estée Lauder, author Lee Israel struggles with financial troubles, writer's block, and alcoholism. She hopes to write a biography of comedienne Fanny Brice. Lee meets with her literary agent, Marjorie, who sharply rejects her plan for the Brice book and explains that Lee, with her difficult personality, is responsible for her own career slump. 
With Marjorie unable to secure her an advance for a new book, regardless of subject matter, Lee sells her possessions to cover living expenses.  She sells a personal letter she received long ago from Katharine Hepburn to a used bookstore merchant and autograph dealer named Anna. Meanwhile, Lee forms a friendship with an old acquaintance, Jack Hock.
Visiting the special collections department of a Manhattan library to research Fanny Brice, Lee discovers two letters that were typewritten by Brice, dead since 1951. She removes one of them from the building and shows it to Anna. Anna makes Lee an offer that is lower than what she was expecting due to the bland content of the letter. Lee returns home and uses a typewriter to add a postscript to the letter. Lee returns to Anna’s store where Anna, amused by what “Fanny Brice” wrote “several decades ago,” offers Lee $350. 
Lee then starts forging and selling letters "by" deceased celebrities, incorporating intimate details to command high prices. Anna, a fan of Lee’s writing, tries to initiate a romantic relationship, but the socially phobic Lee rebuffs her.
When one of Lee Israel's letters raises suspicion for its unguarded discussion of Noël Coward's sexuality, her buyers start blacklisting her. Unable to sell the forgeries, she has Jack Hock sell the letters on her behalf. She also starts stealing authentic letters from libraries and archives for Jack to sell, replacing them with forged duplicates. While Lee is out of town committing one such theft, her cat dies under Jack's care. She ends their friendship but continues their partnership out of necessity.
The FBI arrests Jack while he is attempting a sale. He cooperates with them, resulting in Lee being served with a court summons. She retains a lawyer, who advises her to show contrition by getting a job, doing community service, and joining Alcoholics Anonymous. In court, Lee admits she enjoyed creating the forgeries and does not regret her actions, but realizes that her crimes were not worth it because they did not show her true self as a writer. The judge sentences Lee Israel to five years' probation and six months' house arrest.
Sometime later, Lee has a chance encounter in public with Jack and reconciles with him. Jack, dying of AIDS, grants her permission to write a memoir about their escapades. While passing a bookstore, she sees a Dorothy Parker letter she forged on sale for $1,900. Disgusted, she writes the store owner a sarcastic note in Parker's voice. Upon receiving Israel's note and realizing that the letter in the storefront window is a fake, the owner removes it from the window, but changes his mind and decides to keep it on display.


== Cast ==


== Production ==
In 2011, when the project was first conceived, Sam Rockwell was set to play the character of Jack Hock. In April 2015, it was announced that Julianne Moore would play Israel, with Nicole Holofcener set to direct. On May 14, 2015, Chris O'Dowd joined the cast. In July 2015, Moore and Holofcener dropped out of the project due to "creative conflicts." In May 2016, Melissa McCarthy—whose husband, Ben Falcone, had been cast in a supporting role in Holofcener's film—was confirmed to have been cast as Israel, with Marielle Heller directing from Holofcener's script. In January 2017, Richard E. Grant, Jane Curtin, Dolly Wells, Anna Deavere Smith, and Jennifer Westfeldt joined the cast. Westfeldt does not appear in the finished film.
Filming, which took place in New York City, began in January 2017 and concluded on March 2, 2017.


== Release ==
Can You Ever Forgive Me? had its world premiere at the Telluride Film Festival on September 1, 2018. It also screened at the Toronto International Film Festival that same month. The film was released in the United States on October 19, 2018.
The film was dedicated to the author and movie subject Leonore Carol Israel December 3, 1939 - December 24, 2014.


== Reception ==


=== Box office ===
Can You Ever Forgive Me? grossed $8.8 million in the United States and Canada, and $3.7 million in other territories, for a total worldwide gross of $12.5 million, against a production budget of $10 million.Can You Ever Forgive Me? grossed $150,000 from five theaters in its opening weekend. During its second weekend, it earned $380,000 from 25 theaters. It expanded to 180 theaters in its third week, making $1.08 million. The film grossed $1.5 million from 391 theaters in its fourth weekend. During its fifth weekend, it earned $880,000 from 555 theaters, bringing the total box office gross to over $5 million. During its 11th weekend in release the film crossed $7.5 million stateside.


=== Critical response ===
On the online review aggregator Rotten Tomatoes, the film holds an approval rating of 98% based on 304 reviews, with an average rating of 8.19/10. The website's critical consensus reads, "Deftly directed and laced with dark wit, Can You Ever Forgive Me? proves a compelling showcase for deeply affecting work from Richard E. Grant and Melissa McCarthy." On Metacritic, the film has a weighted average score of 87 out of 100, based on 53 critics, indicating "universal acclaim".

Peter Debruge wrote in Variety that "it takes an actress as delightful as [Melissa McCarthy] to make such a woman not just forgivable but downright lovable"; however, in how the film was promoted he concluded that "one gets the impression that Fox Searchlight is trying to hide (or at least downplay) the homosexual side of this story: Lee was a lesbian, while the openly gay Jack [Hock] can hardly pass a fire hydrant without asking for its phone number." Film Journal International said McCarthy's performance was "stunning" and her previous film roles "could not anticipate how fearlessly and credibly she inhabits Lee Israel." Eric Kohn of IndieWire said the film was a "charming melancholic comedy" where "Heller channels the dark urban milieu of vintage Woody Allen", and in which McCarthy's performance "elevates the material at every opportunity."


== Accolades ==


== References ==


== External links ==
Official website
Can You Ever Forgive Me? on IMDb
Can You Ever Forgive Me? at Metacritic
Can You Ever Forgive Me? at Rotten Tomatoes
Can You Ever Forgive Me? at Box Office Mojo