"Home, Sweet Home" is a song adapted from American actor and dramatist John Howard Payne's 1823 opera Clari, or the Maid of Milan, the song's melody was composed by Englishman Sir Henry Bishop with lyrics by Payne. Bishop had earlier published a more elaborate version of this melody, naming it "A Sicilian Air", but he later confessed to having written it himself.
The song's lyrics are:

When the song was published separately, it quickly sold 100,000 copies. The publishers made a considerable profit from it, net £2,100 in the first year, and the producer of the opera did well. Only Payne did not really profit by its success. "While his money lasted, he was a prince of bohemians", but had little business sense. In 1852 Henry Bishop "relaunched" the song as a parlour ballad, and it became very popular in the United States throughout the American Civil War and after. The song's American premiere took place at the Winter Tivoli Theatre in Philadelphia on October 29, 1823, and was sung by "Mrs. Williams."
As early as 1827 this song was quoted by Swedish composer Franz Berwald in his Konzertstück for Bassoon and Orchestra (middle section, marked Andante). Gaetano Donizetti used the theme in his opera Anna Bolena (1830) Act 2, Scene 3 as part of Anna's Mad Scene to underscore her longing for her childhood home. It is also used with Sir Henry Wood's Fantasia on British Sea Songs and in Alexandre Guilmant's Fantasy for organ Op. 43, the Fantaisie sur deux mélodies anglaises, both of which also use "Rule, Britannia!". In 1857 composer/pianist Sigismond Thalberg wrote a series of variations for piano (op. 72) on the theme of "Home! Sweet Home!".
In 1909, it was featured in the silent film The House of Cards, an Edison Studios film.  In the particular scene, a frontier bar was hurriedly closed due to a fracas. A card reading "Play Home Sweet Home" was displayed, upon which an on-screen fiddler promptly supplied a pantomime of the song.  This may imply a popular association of this song with the closing hour of drinking establishments.The song was reputedly banned from being played in Union Army camps during the American Civil War for being too redolent of hearth and home and so likely to incite desertion.
In 1926 a "Music Stair" railing (see image) at Chatham Manor in Fredericksburg, Virginia was designed by Washington, D.C. based architect Oliver H. Clarke for the homeowners, the Devores. The ornamental iron railing features the first few bars of the score to “Home, Sweet Home.”  
The Village of East Hampton acquired his grandfathers seventeenth-century house, known as "Home Sweet Home," and the windmill behind it, converting the homestead into a living museum in the landmarked East Hampton Village District.
The song is known in Japan as "Hanyū no Yado" ("埴生の宿") ("My Humble Cottage").  It has been used in such movies as The Burmese Harp and Grave of the Fireflies.  It is also used at Senri-Chūō Station on the Kita-Osaka Kyūkō Railway.
Payne's tune, though, is perhaps most commonly recognized in the score from MGM's The Wizard of Oz. The melody is played in a counterpart to "Over the Rainbow" in the final scene as Dorothy (played by Judy Garland) tells her family, "there's no place like home".In the 1939 film First Love, the song is performed by Deanna Durbin.
In the 1946 20th Century Fox film Anna and the King of Siam, as well as in Rodgers and Hammerstein's 1951 musical, The King and I (and its 1956 film adaptation), Anna Leonowens teaches her students to sing "Home! Sweet Home" as part of her psychological campaign to induce the King to build her a house of her own.


== Notable recordings ==
Popular recordings were made by John Yorke AtLee (1891), Harry Macdonough (1902), Richard Jose (1906), Alma Gluck (1912), Alice Nielsen (1915) and Elsie Baker (1915).Later recordings were made by Deanna Durbin (recorded July 7, 1939 for Decca Records, catalog No. 2758B), Vera Lynn (1941) and by Bing Crosby (recorded July 30, 1945 with Victor Young and His Orchestra). The Crosby version was included in his album Auld Lang Syne (1948).
Katherine Jenkins included the song in her album Home Sweet Home (2014).


== References ==


== External links ==
Sheet music and MIDI for Home, Sweet Home, from Project Gutenberg
Text of the poetry from ompersonal.com.ar
Derek B Scott sings “Home, Sweet Home” (1823) from victorianweb.org
'Home Sweet Home' from seiyaku.com
The short film A NATION SINGS (1963) is available for free download at the Internet Archive
Far Cry New Dawn version of "Home, Sweet Home" on YouTube.