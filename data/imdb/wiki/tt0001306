The Bookseller/Diagram Prize for Oddest Title of the Year, originally known as the Diagram Group Prize for the Oddest Title and commonly known as the Diagram Prize, is a humorous literary award that is given annually to a book with an unusual title. The prize is named after the Diagram Group, an information and graphics company based in London, and The Bookseller, a British trade magazine for the publishing industry. Originally organised to provide entertainment during the 1978 Frankfurt Book Fair, the prize has since been awarded every year by The Bookseller and is now organised by the magazine's diarist Horace Bent. The winner was initially decided by a panel of judges, but since 2000 the winner has been decided by a public vote on The Bookseller's website.Controversy has arisen since the creation of the awards; there have been two occasions when no award was given because no titles were judged to be odd enough, Bent has complained about some of the winners chosen by the public, and the 2008 winner, The 2009–2014 World Outlook for 60-milligram Containers of Fromage Frais, proved controversial because rather than being written by its listed author, Philip M. Parker, it was instead written by a machine of Parker's invention. The most recent winner, in November 2019, was The Dirt Hole and its Variations by Charles L Dobbins.


== History ==
Although the award was created by The Bookseller, the idea of an award celebrating books with odd titles was proposed by Bruce Robinson and Trevor Bounford of the Diagram Group in order to provide entertainment during the Frankfurt Book Fair in 1978. Originally known as the Diagram Group Prize for the Oddest Title at the Frankfurt Book Fair, any book that was at the fair could be nominated, but other books outside of the fair were also included. In 1982, Horace Bent, diarist for The Bookseller, took over administrative duties. Following two occasions in 1987 and 1991 when no prize was given due to a lack of odd titles, The Bookseller opened suggestions to the readers of the magazine. In 2000, the winner was voted for by the public instead of being decided by Bent. In 2009, online submissions sent on Twitter were accepted. This resulted in the highest number of submissions for the prize in its history, with 90 books being submitted (50 from Twitter), almost three times the number from the previous year (32). However, Bent also expressed his annoyance at people who gave submissions that broke the rules, with some of the books mentioned being published as far back as 1880. The 2014 prize allowed nominations from self-published works, the first book being Strangers Have the Best Candy by Margaret Meps Schulte, which won the prize.The Diagram Prize receives considerable press coverage every year. In 2008, more people voted for the Diagram Prize (8,500 votes) than The Best of Booker Prize (7,800). The prize is either a magnum of champagne or a bottle of claret for the person who nominates the winning title, and increased publicity for both the book and its author. In 2014, the nominator was Brian Payne, who works as the deputy chief sub-editor of The Bookseller. Due to his position he decided to reject the bottle of claret that he won saying it, "would remain in the cellar." In 2018, all the nominations came from staff at The Bookseller, so the claret was awarded to a random voter who voted for the eventual winner.


=== Format ===
Nominees were originally limited to just books at the Frankfurt Book Fair, but this was extended to submissions sent in by The Bookseller magazine's traditional readership of librarians, publishers, and booksellers in order to decrease the risk of no award being given. In 2009, submissions could be sent to either Bent's or The Bookseller's Twitter accounts. People cannot nominate their own works, nor can they select books they publish themselves. Titles that are deliberately created to be funny are normally rejected. Also, nominators, judges and voters are actively discouraged from reading any of the nominations, "for fear that becoming too close to the work may cloud their judgement in declaring the text's title 'odd'. Especially considering the prize champions 'odd titles' and not 'odd books' (see the Man Booker for the latter)". The winner was originally voted for by a panel of judges, but since 2000 the winner has been voted for by members of the public via the Internet. Bent resisted this move and threatened to resign, but he later reconsidered and now creates the short list of finalists. Also, the title of the book must be in English, although the language in the book can be any language.


=== Books about the prize ===
In September 2008, a book about the Diagram Prize was published by Aurum Press entitled How to Avoid Huge Ships and Other Implausibly Titled Books. With an introduction written by Joel Rickett, the book was released to celebrate the 30th anniversary of the prize. It featured a collection of book covers from winners and runners-up from previous years. A follow-up book was released in October 2009, entitled Baboon Metaphysics And More Implausibly Titled Books, including an introduction by Bent.


=== Controversy ===
So far, there have been two occasions in which no award has been presented. Bent did not offer a prize in 1987 and 1991, as he felt there was no title that was odd enough to deserve the prize. The prize has become noteworthy enough that, in 2004, The Bookseller castigated publishers for choosing titles with a view to winning it, saying, "There were too many self-consciously titled entries – presumably in a bid to emulate the 2003 champion, Big Book of Lesbian Horse Stories". Bent has also expressed his dislike of people voting for ruder titles, stating that he himself would not have voted for the 2007 winner If You Want Closure in Your Relationship, Start with Your Legs.In 2009, the choice of The 2009–2014 World Outlook for 60-milligram Containers of Fromage Frais as winner of the 2008 award was controversial, as Parker did not write the book himself, but used an automated authoring machine which produces thousands of titles on the basis of Internet and database searches. Philip Stone, charts editor and awards administrator at The Bookseller, commented by saying: "I think it's slightly controversial as it was written by a computer, but given the number of celebrity memoirs out there that are ghostwritten, I don't think it's too strange."In 2018, one of the nominations, Joy of Waterboiling, was controversial because the book was written mostly in German, but the rules of the prize state that only the title needs to be in English in order to qualify for nomination.


=== Diagram of Diagrams ===
Two special anniversary awards known as the "Diagram of Diagrams" (the name reflects the "Booker of Bookers") have been presented to honour both the 15th and the 30th anniversaries of the Diagram Prize. The nominations of the prizes were all of the previous winners up to that point in time. In 1993, the winner of the 15th anniversary award was Proceedings of the Second International Workshop on Nude Mice, the winner of the first Diagram Prize. The second "Diagram of Diagrams", announced on 5 September 2008, was Greek Rural Postmen and Their Cancellation Numbers, the 1996 winner.


=== Winners ===


== Current nominations ==
The 2020 nominations were announced on 23 October, 2020. The winner will be revealed on 27 November, 2020. The nominations are:
A Dog Pissing at the Edge of a Path by Gregory Forth (McGill-Queen's University Press)
Introducing the Medieval Ass by Kathryn L Smithies (University of Wales Press)
Classical Antiquity in Heavy Metal Music by K F B Fletcher and Osman Umurhan (Bloomsbury Academic)
How to Make Love to a Despot by Stephen D. Krasner (W W Norton)
Lawnmowers: An Illustrated History by Brian Radam (Amberley Publishing)
The Slaughter of Farmed Animals: Practical Ways to Enhance Animal Welfare by Temple Grandin and Michael Cockram (CABI)


== See also ==
Bulwer-Lytton Fiction Contest, for the worst opening line of a (fictitious) book.
Lyttle Lytton Contest, a derivative favouring extremely short first sentences.


== References ==


== Bibliography ==
Bent, Horace (2009). Baboon Metaphysics and More Implausibly Titled Books. London: Aurum Press. ISBN 978-1-84513-498-3
Rickett, Joel (2008). How to Avoid Huge Ships and Other Implausibly Titled Books. London: Aurum Press. ISBN 978-1-84513-321-4


== External links ==
Horace Bent on The Bookseller.