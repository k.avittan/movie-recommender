The passion fruit is the fruit of a number of plants in the genus Passiflora.


== Etymology ==
The passion fruit is so called because it is one of the many species of passion flower, the English translation of the Latin genus name, Passiflora, and may be spelled "passion fruit", "passionfruit", or "passion-fruit". Around 1700, the name was given by missionaries in Brazil as an educational aid while trying to convert the indigenous inhabitants to Christianity; its name was flor das cinco chagas or "flower of the five wounds" to illustrate the crucifixion of Christ, with other plant components also named after an emblem in the Passion of Jesus.


== Appearance and structure ==
Passion fruits are round or oval. They can be yellow, red, purple, and green. The fruits have a juicy edible center composed of a large number of seeds.


== Varieties ==
Edible passion fruits can be divided into three main types:

purple passion fruit (fruits of Passiflora edulis Sims),
yellow passion fruit (Passiflora edulis f. flavicarpa Deg.),
giant granadilla (Passiflora quadrangularis L.).


== Uses ==
The part of the fruit that is used (eaten) is the pulpy juicy seeds. Passion fruits can also be squeezed to make juice.


== Nutrition ==
Raw passion fruit is 73% water, 22% carbohydrates, 2% protein and 0.7% fat (table). In a 100 gram amount, fresh passion fruit supplies 97 calories, and contains 36% of the Daily Value (DV) of vitamin C, 42% dietary fiber, B vitamins riboflavin (11% DV) and niacin (10% DV), 12% iron and 10% phosphorus (table). No other micronutrients are in significant content.


== Phytochemicals ==
Several varieties of passionfruit are rich in polyphenol content, and yellow  varieties of the fruit were found to contain prunasin and other cyanogenic glycosides in the peel and juice.


== Gallery ==

		
		
		
		
		


== See also ==
Fassionola
POG juice


== Notes ==


== References ==