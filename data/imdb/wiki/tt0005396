Gladiolus (from Latin, the diminutive of gladius, a sword) is a genus of perennial cormous flowering plants in the iris family (Iridaceae).It is sometimes called the 'sword lily', but is usually called by its generic name (plural gladioli).The genus occurs in Asia, Mediterranean Europe, South Africa, and tropical Africa. The center of diversity is in the Cape Floristic Region. The genera Acidanthera, Anomalesia, Homoglossum, and Oenostachys, formerly considered distinct, are now included in Gladiolus.


== Description ==
Gladioli grow from rounded, symmetrical corms, (similar to crocuses) that are enveloped in several layers of brownish, fibrous tunics.Their stems are generally unbranched, producing 1 to 9 narrow, sword-shaped, longitudinal grooved leaves, enclosed in a sheath. The lowest leaf is shortened to a cataphyll. The leaf blades can be plane or cruciform in cross section.
The flowers of unmodified wild species vary from very small to perhaps 40 mm across, and inflorescences bearing anything from one to several flowers. The spectacular giant flower spikes in commerce are the products of centuries of hybridisation and selection.
The flower spikes are large and one-sided, with secund, bisexual flowers, each subtended by 2 leathery, green bracts. The sepals and the petals are almost identical in appearance, and are termed tepals. They are united at their base into a tube-shaped structure. The dorsal tepal is the largest, arching over the three stamens. The outer three tepals are narrower. The perianth is funnel-shaped, with the stamens attached to its base. The style has three filiform, spoon-shaped branches, each expanding towards the apex.The ovary is 3-locular with oblong or globose capsules, containing many, winged brown, longitudinally dehiscent seeds.
These flowers are variously coloured, ranging from pink to reddish or light purple with white, contrasting markings, or white to cream or orange to red.


== Ecology ==
The South African species were originally pollinated by long-tongued anthophorini bees, but some changes in the pollination system have occurred, allowing pollination by sunbirds, noctuid and Hawk-moths, long-tongued flies and several others. In the temperate zones of Europe many of the hybrid large flowering sorts of gladiolus can be pollinated by small well-known wasps. Actually, they are not very good pollinators because of the large flowers of the plants and the small size of the wasps. Another insect in this zone which can try some of the nectar of the gladioli is the best-known European Hawk-moth Macroglossum stellatarum which usually pollinates many popular garden flowers like Petunia, Zinnia, Dianthus and others.Gladioli are used as food plants by the larvae of some Lepidoptera species including the Large Yellow Underwing, and gladiolus thrips.


== Horticulture ==
Gladioli have been extensively hybridized and a wide range of ornamental flower colours are available from the many varieties. The main hybrid groups have been obtained by crossing between four or five species, followed by selection: 'Grandiflorus', 'Primulines' and 'Nanus'. They can make very good cut flowers for display.The majority of the species in this genus are diploid with 30 chromosomes (2n=30) but the Grandiflora hybrids are tetraploid and possess 60 chromosomes (2n=4x=60). This is because the main parental species of these hybrids is Gladiolus dalenii which is also tetraploid and includes a wide range of varieties (like the Grandiflora hybrids).


== Species ==
The genus Gladiolus contains about 300 species, the World Checklist of Selected Plant Families had over 276 species in 1988, As of February 2017, it accepted 300 species.There are 260 species of Gladiolus endemic to southern Africa, and 76 in tropical Africa. 
About 10 species are native to Eurasia.The genus Gladiolus has been divided into many sections. Most species, however, are only tentatively placed. 

Known hybrids include:

Gladiolus × colvillii (G. cardinalis × G. tristis): Colville's gladiolus
Gladiolus × gandavensis (G. dalenii × G. oppositiflorus) (sect. Ophiolyza)
Gladiolus × hortulanus

		
		
		
		
		
		
		


== Cultivation ==
In temperate zones, the corms of most species and hybrids should be lifted in autumn and stored over winter in a frost-free place, then replanted in spring. Some species from Europe and high altitudes in Africa, as well as the small 'Nanus' hybrids, are much hardier (to at least −15 °F/-26 °C) and can be left in the ground in regions with sufficiently dry winters. 'Nanus' is hardy to Zones 5–8. The large-flowered types require moisture during the growing season, and must be individually staked as soon as the sword-shaped flower heads appear. The leaves must be allowed to die down naturally before lifting and storing the corms. Plants are propagated either from small cormlets produced as offsets by the parent corms, or from seed. In either case, they take several years to get to flowering size.  Clumps should be dug up and divided every few years to keep them vigorous.They are affected by thrips, (thrip simplex), and wasps Dasyproctus bipunctatus, which burrow into the flowers causing them to collapse and die.Numerous garden cultivars have been developed, of which ‘Robinetta’ (a G. recurvus hybrid), with pink flowers, has gained the Royal Horticultural Society’s Award of Garden Merit.


== In culture ==
Gladiolus is the birth flower of August.
Gladioli are the flowers associated with a fortieth wedding anniversary.
American Ragtime composer Scott Joplin composed a rag called “Gladiolus Rag”
"Gladiolus" was the word Frank Neuhauser correctly spelled to win the 1st National Spelling Bee in 1925.
The Australian comedian and personality Dame Edna Everage's signature flowers are gladioli, which she refers to as "gladdies".
The Mancunian singer Morrissey is known to dance with gladioli hanging from his back pocket or in his hands, especially during the era of The Smiths. This trait of his was made known in the music video for "This Charming Man", where he swung a bunch of yellow gladioli while singing.Gladiolus in art
		
		


== References ==


== Bibliography ==
G. R. Delpierre and N. M. du Plessis (1974). The winter-growing Gladioli of Southern Africa. Tafelberg-Uitgewers Beperk 120 colour photographs and descriptions.
Peter Goldblatt (1996).  A monograph of the genus Gladiolus in tropical Africa (83 species). Timber Press
Peter Goldblatt, J. C. Manning (1998). Gladiolus in southern Africa: Systematics, Biology, and Evolution, including 144 watercolor paintings. Cape Town: Fernwood Press.


== External links ==
Taxonomy of Gladiolus in GBIF Biodiversity Data PortalDressler, S.; Schmidt, M. & Zizka, G. (2014). "Gladiolus". African plants – a Photo Guide. Frankfurt/Main: Forschungsinstitut Senckenberg.