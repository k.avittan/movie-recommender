Beau Geste is an adventure novel by P. C. Wren, which details the adventures of three English brothers who enlist separately in the French Foreign Legion following the theft of a valuable jewel from the country house of a relative. Published in 1924, the novel is set in the period before World War I. It has been adapted for the screen several times.


== Plot summary ==
Michael "Beau" Geste is the protagonist (and an archetype). The main narrator is his younger brother John. The three Geste brothers are portrayed as behaving according to the English upper class values of a time gone by, and "the decent thing to do" is, in fact, the leitmotif of the novel. The Geste brothers are orphans and have been brought up by their aunt Lady Patricia at Brandon Abbas. The rest of Beau's band are mainly Isobel and Claudia (possibly the illegitimate daughter of Lady Patricia) and Lady Patricia's relative Augustus (the caddish nephew of the absent Sir Hector Brandon). While not mentioned in Beau Geste, the American Otis Vanbrugh appears as a friend of the Geste brothers in a sequel novel.  John and Isobel are devoted to each other and it is in part to spare her any suspicion of being a thief that he takes the extreme step of joining the French Foreign Legion (following the steps of his elder brothers).
The detonator of the main plot is the disappearance of a precious jewel known as the "Blue Water". Suspicion falls on the band of young people, and Beau leaves England to join the French Foreign Legion in Algeria, followed by his brothers, Digby (his twin) and John. After recruit training in Sidi Bel Abbes and some active service skirmishing with tribesmen in the south, Beau and John are posted to the small garrison of the fictional desert outpost of Fort Zinderneuf, while Digby and his American friends Hank and Buddy are sent to Tanout-Azzal to train with a mule mounted company. The commander at Fort Zinderneuf (after the death of two more senior officers) is the sadistic Sergeant Major Lejaune, who drives his abused subordinates to the verge of mutiny. An attack by Tuaregs prevents mass desertion (only the Geste brothers and a few loyalists are against the scheme). Throughout the book, Beau's behaviour is true to France and the Legion, and he dies at his post. Digby, Hank and Buddy arrive with a relief column from Tokotu that reaches Fort Zinderneuf too late. Together with John (the last man standing at Zinderneuf) they desert and experience a long trek during which Digby is killed in a skirmish with Arabs.
After becoming separated from his friends Hank and Buddy in the desert, John returns to Brandon Abbas. The last survivor of the three brothers, he is welcomed by their aunt and his fiancée Isobel. The reason for the jewel theft is revealed to have been a matter of honour, and to have been the only "decent thing" possible. In Beau Ideal and other sequels P. C. Wren ties loose strings together, including recording that Michael Geste's original reasons for joining the Foreign Legion were honour but also his doomed and impossible love for Claudia.


== Title ==
The phrase "beau geste" (pronounced [bo ʒɛst]) is from the French, meaning "a gracious (or fine) gesture".In French, the phrase includes the suggestion of a fine gesture with unwelcome or futile consequences, and an allusion to the chanson de geste, a literary poem celebrating the legendary deeds of a hero.


== Sequels ==
P. C. Wren wrote the sequels Beau Sabreur (in which the narrator is a French officer of Spahis who plays a secondary role in Beau Geste)  and Beau Ideal. In this third volume Wren details what happened the night of the theft of the Blue Water. He also wrote Good Gestes, a collection of short tales (about half of them about the Geste brothers and their American friends Hank and Buddy, who also feature prominently in Beau Sabreur and Beau Ideal) and Spanish Maine (UK)  (The Desert Heritage (USA)), where loose ends are tied up and the successive tales of John Geste's adventures come to an end. Life in the Foreign Legion is also represented in some, but not all, of Wren's subsequent novels: Port O'Missing Men, Soldiers of Misfortune, Valiant Dust, Dead Men's Boots, Flawed Blades, The Wages of Virtue, Stepsons of France, and The Uniform of Glory.


== Analysis ==
The original novel, on which the various films are more or less loosely based, provides a detailed and fairly authentic description of life in the pre-1914 Foreign Legion, which has led to (unproven) suggestions that P. C. Wren himself served with the Legion. Before he became a successful writer Wren's recorded career was that of a school teacher in India.


== Adaptations for film, radio and theatre ==
Beau Geste (1926), starring Ronald Colman, William Powell, Noah Beery Sr.
Beau Geste (1939), with Gary Cooper, Ray Milland, Robert Preston
Beau Geste (1966), with Guy Stockwell, Doug McClure, Telly Savalas
Beau Geste (1982 BBC serial), starring Benedict Taylor, Anthony Calf, Jonathon Morris
"Beau Geste". The Campbell Playhouse (Mar 1, 1939), starring Orson Welles, Laurence OlivierBeau Geste was also adapted for the stage in 1929 by British theatrical producer Basil Dean. The production featured Laurence Olivier in the lead role and fellow actors included Madeleine Carroll and Jack Hawkins. The play ran for just five weeks.


== Parodies ==
Beau Hunks 1931, a 1931 movie starring Laurel and Hardy.
The Goon Show episode "Under Two Floorboards (A Story of the Legion)" (broadcast January 25, 1955)
"The Tiddlywink Warriors" in Earthman's Burden (1957) by Poul Anderson and Gordon R. Dickson published by Gnome Press (with multiple reprints over the years
Follow that Camel (1967) A Carry on film featuring a character called B. O. West.
The Generation Game (around 1975) did a parody of Beau Geste, may have been the first to use the name Beau Peep.
The Last Remake of Beau Geste (1977), starring Marty Feldman, Ann-Margret and Michael York
Beau Peep (started 1978) a strip cartoon in the Daily Star newspaper.
Soul Music (1994), by Terry Pratchett. The Death of the Discworld uses the name Beau Nidle and has him join the Klatchian Foreign Legion, a parody of the French Foreign Legion.
The comic strip Crock claims to be "the greatest and longest-running parody" of Beau Geste, although it bears little similarity to the original novel.
Snoopy from the Peanuts comic strip frequently refers to Fort Zinderneuf when roleplaying as a Foreign Legionnaire.


== References ==


== Bibliography ==
Thomas, R. S. (1990–12). "P C Wren's Beau Geste". Children's Literature in Education, vol. 21, no. 4, December 1990.
Coleman, Terry (2005). Olivier. Macmillan. ISBN 0-8050-7536-4.
Tibbetts, John C., and James M. Welsh, eds. The Encyclopedia of Novels Into Film (2nd ed. 2005) pp 24–26.


== External links ==
Beau Geste at Faded Page (Canada)