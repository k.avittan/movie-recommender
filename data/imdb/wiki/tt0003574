The Wordless Book is a Christian evangelistic book. Evidence points to it being invented by the famous London Baptist preacher Charles Haddon Spurgeon, in a message given on January 11, 1866 to several hundred orphans regarding Psalm 51:7 "Wash me, and I shall be whiter than snow."  It is called a "book", as it is usually represented with pages, although it can be shown on a single page or banner.
The book consists of several blocks of pure color that, in sequence, represent a nonverbal catechism about basic Christian teachings for the instruction of children, the illiterate, or people of different cultures.  The presentation of the book is meant to be a verbal experience, however, providing the "reader" a visual cue to expound Christian doctrine  extemporaneously or in impromptu situations.


== Variations on a theme ==
Spurgeon’s concept contained only three colors: "first was black, the second was red, and the third was pure white".

black: representing the sinful state of humanity by nature. Usually referred to as the dark page.
red: representing the blood of Jesus.
white: representing the perfect righteousness that God has given to believers through the atoning sacrifice of Jesus Christ his Son, usually referred to as the clean page.By 1880 at least, the book was being widely used in evangelism among orphanages, Sunday schools, and in cross-cultural missions.
Different versions came about when Dwight Lyman Moody added another color: gold (after white) – representing Heaven – in 1875.  Hudson Taylor and missionaries of the China Inland Mission used the four-color version in open-air preaching and individual evangelism.  It has been used by missionaries and teachers such as Jennie Faulding Taylor, Amy Carmichael, Fanny Crosby (who was blind), and the modern-day Child Evangelism Fellowship, which added a fifth color: green (after white, before gold) – representing one's need to grow in Christ after salvation.  Some modern Baptists add a sixth color: blue (after white, before green) – representing  baptism etc. Blue can also represent faith, placed between red and white.


== Color cosmology ==
The success of the Wordless Book in communicating with East Asian peoples is disputable due to the influence of "color cosmology" (Wu Xing) or color psychology in Chinese culture. For example, in Chinese symbolism, red is the color of good luck and success, and is used for decoration and wedding attire (during the traditional half of the wedding ceremony, while the bridal attire in the modern half is usually white). Money in Chinese societies is traditionally given in red packets; while white is the funeral color instead of black.


== Different forms ==
Wordless books have been put into many forms for easy use and insertion into everyday situations. Among these are salvation bracelets and various sports gear, including soccer balls, basketballs, volleyballs and martial arts belts.


== See also ==
19th-century Protestant missions in China
Christianity in China
The Four Spiritual Laws
Historical Bibliography of the China Inland Mission


== References ==


=== Citations ===


=== Bibliography ===
Austin, Alvyn (2007), China’s Millions: The China Inland Mission and Late Qing Society, Grand Rapids,  MI: Eerdmans, ISBN 978-0-8028-2975-7
Spurgeon, Charles (1911), The Metropolitan Tabernacle Pulpit, 57, London: Passmore and Alabaster, ISBN 1-56186-057-3
Taylor, James Hudson (January 1892), China's Millions, London: China Inland Mission


== External links ==

The Wordless Book: History, Availability, Exposition, and Story
The Wordless Book: Exposition by Charles Spurgeon