The Lamb, the Woman, the Wolf is a 1914 silent Western drama film directed by Allan Dwan and featuring Murdock MacQuarrie, Pauline Bush, and Lon Chaney. The film is now considered lost.


== Plot ==
The Lamb is a hard-working local newspaper editor who cares for his invalid mother. He falls in love with The Woman, but does not propose, because he feels it would be unfair to burden a young girl with his problems. The Wolf (Chaney), a husky mountain man, returns to the town after being away for five years and tries to rekindle his relationship with the Woman. He succeeds in marrying her and they go off together to live in the mountains where the Wolf works as the paymaster for a mining company. 
The Wolf turns out to be a violent brute and the Woman soon realizes their marriage was a mistake. Meanwhile, the Lamb's mother passes away and he goes off into the mountains to live a life of solitude. There he falls in with a band of outlaws and becomes a hardened criminal. 
The Wolf, having tired of the Woman, plans to steal the mining company's payroll that is in her safekeeping and then abandon her. The Lamb plans to steal the same payroll, not realizing it is in the safekeeping of the Woman he once loved. The Lamb arrives first, and the Woman does not recognize him. Seeing that his presence has badly shaken her, the Lamb puts his revolver down on her desk temporarily to calm her down. When a second masked man suddenly breaks in, the Woman picks up the gun and shoots him, then realizes that she has killed her own husband. She suddenly recognizes the Lamb, who repents for attempting to rob her, and the two go off to a new life together. 


== Cast ==
Murdock MacQuarrie as The Lamb
Pauline Bush as The Woman
Lon Chaney as The Wolf


== Reception ==
Moving Picture World opined "The subtitles indicated more pathos in the beginning than the situations warranted. Mr. MacQuarrie (as the Lamb) was much more interesting after he had abandoned his meek attitude and began defending himself. The situations...are quite stirring and the interest is maintained from this point...The production as a whole is one of about average merit."


== References ==


== External links ==
The Lamb, the Woman, the Wolf on IMDb