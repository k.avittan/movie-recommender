Right of way is "the legal right, established by usage or grant, to pass along a specific route through grounds or property belonging to another", or "a path or thoroughfare subject to such a right". A similar right of access also exists on land held by a government, lands that are typically called public land, state land, or Crown land. When one person owns a piece of land that is bordered on all sides by lands owned by others, an easement may exist, or be created so as to initiate a right of way through the bordering land.
This article focuses on access by foot, by bicycle, horseback, or along a waterway, while Right-of-way (transportation) focuses on land usage rights for highways, railways, pipelines.
A footpath is a right of way that legally may only be used by pedestrians. A bridleway is a right of way that legally may be used only by pedestrians, cyclists and equestrians, but not by motorised vehicles. In some countries, especially in Northern Europe, where the freedom to roam has historically taken the form of general public rights, a right of way may not be restricted to specific paths or trails.


== Alternative definitions ==

A further definition of right of way, chiefly in American transport, is as a type of easement granted or reserved over the land for as to  transportation purposes, this can be for a highway, public footpath, railway, canal, as well as electrical transmission lines, oil and gas pipelines.The term may also describe priority of traffic flow, "the legal right of a pedestrian, vehicle, or ship to proceed with precedence over others in a particular situation or place". In hiking etiquette, where when two groups of hikers meet on a steep trail, a custom has developed in some areas whereby the group moving uphill has the right of way.


== Worldwide ==


=== New Zealand ===
There is extensive public access in New Zealand, including waterways and the coast, but it is "often fragmented and difficult to locate".


=== Republic of Ireland ===

In the Republic of Ireland, pedestrian rights of way to churches, known as mass paths, have existed for centuries. In other cases, the modern law is unclear; on the one hand, Victorian era laws on easements protect a property owner's rights, amplified by the 1937 constitution, which stipulate that a right of way has to be specifically dedicated to public use. Opposing these, those claiming general rights of way hark back to an anti-landed gentry position that has endured since the Land War of the 1880s. Rights of way can be asserted by Adverse possession, but proving continuous use can be difficult. A case heard in 2010 concerning claims over the Lissadell House estate was based on the historical laws, since amended by the Land and Conveyancing Law Reform Act, 2009.The 2009 Act abolished the doctrine of lost modern grant, and allows a user to claim a right of way after 12 year of use across private land owned by another, 30 years on state land and 60 years on the foreshore. The claim must be confirmed by a court order and duly registered, an expensive process. The user must prove "enjoyment without force, without secrecy and without the oral or written consent of the […] owner", a restatement of the centuries-old principle of Nec vi, nec clam, nec precario.


=== United Kingdom ===


==== England and Wales ====

In England and Wales, other than in the 12 Inner London Boroughs and the City of London, public rights of way are paths on which the public have a legally protected right to pass and re-pass. The law in England and Wales differs from that in Scotland in that rights of way only exist where they are so designated (or are able to be designated if not already) whereas in Scotland any route that meets certain conditions is defined as a right of way, and in addition there is a general presumption of access to the countryside. Private rights of way or easements also exist.
Footpaths, bridleways and other rights of way in most of England and Wales are shown on definitive maps. A definitive map is a record of public rights of way in England and Wales. In law it is the definitive record of where a right of way is located. The highway authority (normally the county council, or unitary authority in areas with a one-tier system) has a statutory duty to maintain a definitive map, though in national parks the national park authority usually maintains the map.


===== London =====
Definitive maps of public rights of way have been compiled for all of England and Wales as a result of the Countryside and Rights of Way Act 2000, except the twelve Inner London boroughs which, along with the City of London, were not covered by the Act.
To protect the existing rights of way in London, the Ramblers launched their "Putting London on the Map" in 2010 with the aim of getting "the same legal protection for paths in the capital as already exists for footpaths elsewhere in England and Wales. Currently, legislation allows the Inner London boroughs to choose to produce definitive maps if they wish, but none do so.The launch event of "Putting London on the Map" took place at the British Library, and since then "the Inner London Area of the Ramblers has been working with Ramblers Central Office staff to try to persuade each of the Inner London boroughs on the desirability of producing definitive maps of rights of way".In 2011 Lambeth Council passed a resolution to work towards creating a definitive map for their borough, but this does not yet exist. The City of London has produced a Public Access Map. Definitive maps exist for the Outer London boroughs.


===== Permissive paths =====
Some landowners allow access over their land without dedicating a right of way. These are often physically indistinguishable from public rights of way, but they are may be subject to restrictions. Such paths are often closed at least once a year, so that a permanent right of way cannot be established in law.


==== Scotland ====
In Scotland, a right of way is a route over which the public has been able to pass unhindered for at least 20 years. The route must link two "public places", such as villages, churches or roads. Unlike in England and Wales there is no obligation on Scottish local authorities to signpost rights of way. However the charity Scotways, formed in 1845 to protect rights of way, records and signs the routes.
The Land Reform (Scotland) Act 2003 codified in law traditional, non-motorised, access practices on land and water. Under the 2003 Act a plain language explanation of rights is published by Scottish Natural Heritage: the Scottish Outdoor Access Code. Certain categories of land are excluded from this presumption of open access, such as railway land, airfields and private gardens.Section 4 of the Access Code explains how land managers are permitted to request the public to avoid certain areas for a limited period in order to undertake management tasks, however longer term restrictions must be approved by the local authority. The ability to temporarily restrict public access is commonly exercised without notice by shooting, forestry or wind farm operators, but does not extend to public Rights of Way. In Scotland the public have a higher degree of freedom on Rights of Way than on open land. Blocking a Right of Way in Scotland is a criminal obstruction under the Highways Act, just as in England and Wales, but the lack of publicly accessible Rights of Way maps in Scotland makes it very difficult to enforce.While in England and Wales, highway authorities have a duty to maintain legally recognised maps of rights of way, in Scotland different legislation applies and there is no legally recognised record of rights of way. However, there is a National Catalogue of Rights of Way (CROW), compiled by the Scottish Rights of Way and Access Society (Scotways), in partnership with Scottish Natural Heritage, and the help of local authorities. There are three categories of rights of way in CROW:

vindicated – routes declared to be rights of way by some legal process;
asserted – routes which have been accepted as rights of way by the landowner, or where local authorities are prepared to take legal action to protect them;
claimed – other right of way routes, which have not been vindicated or asserted, but which appear to meet the common law conditions and have not yet been legally disputed.


==== Northern Ireland ====
Northern Ireland has very few public rights of way and access to land in Northern Ireland is more restricted than other parts of the UK, so that in many areas walkers can only enjoy the countryside because of the goodwill and tolerance of landowners. Permission has been obtained from all landowners across whose land the Waymarked Ways and Ulster Way traverse. Much of Northern Ireland's public land is accessible, e.g. Water Service and Forest Service land, as is land owned and managed by organisations such as the National Trust and the Woodland Trust.Northern Ireland shares the same legal system as England, including concepts about the ownership of land and public rights of way, but it has its own court structure, system of precedents and specific access legislation.


=== United States ===
In the United States, a right-of-way is normally created as a form of easement. The easement may be an easement appurtenant, that benefits a neighboring property, or an easement in gross, that benefits another individual or entity as opposed to another parcel of land. See also "Alternative definitions" above, with regard to
types of easement granted or reserved over land for transportation purposes,


== Right to roam ==

The freedom to roam, or everyman's right is the general public's right to access certain public or privately owned land for recreation and exercise. Access is permitted across any open land, in addition to existing paths and tracks.
In England and Wales public access rights apply to certain categories of mainly uncultivated land—specifically "mountain, moor, heath, down and registered common land". Developed land, gardens and certain other areas are specifically excluded from the right of access. Agricultural land is accessible if it falls within one of the categories described above (See  Countryside and Rights of Way Act 2000). Most publicly owned forests have a similar right of access by virtue of a voluntary dedication made by the Forestry Commission. People exercising the right of access have certain duties to respect other people's rights to manage the land, and to protect nature.
In Scotland and the Nordic countries of Finland, Iceland, Norway and Sweden as well as the Baltic countries of Estonia, Latvia and Lithuania the freedom to roam may take the form of general public rights which are sometimes codified in law. The access is ancient in parts of Northern Europe and has been regarded as sufficiently basic that it was not formalised in law until modern times.
This right also usually includes access to lakes and rivers, and therefore activities like swimming, canoeing, rowing and sailing. The Land Reform (Scotland) Act 2003 gives everyone statutory access rights to most inland water in Scotland (excluding motorized vehicles), providing that they respect the rights of others.The Rivers Access Campaign is being undertaken by the British Canoe Union (BCU) to open up the inland water-ways in England and Wales on behalf of members of the public. Under current UK law, public access to rivers is restricted, and only 2% of all rivers in England and Wales have public access rights. The BCU is using the campaign not just to raise awareness of the access issues, but to try to bring about changes in the law.
Many tropical countries such as Madagascar have historic policies of open access to forest or wilderness areas.


== Public land ==
Some land long considered public or crown land may in fact be the territory of indigenous people, in countries that were colonised.


=== Crown land in Canada ===
Much of Canada is Crown land owned by the provinces. Some is leased for commercial activity, such as forestry or mining, but on much of it there is free access for recreational activities like hiking, cycling, canoeing, cross-country skiing, horse back riding, and licensed hunting and fishing, etc. At the same time access can be restricted or limited for various reasons (e.g., to protect public safety or resources, including the protection of wild plants and animals).
In the Canadian Territories Crown land is administered by the Canadian Federal Government. Canadian National Parks have been created from Crown land and are also administered by the Federal Government. There are also provincial parks and nature reserves that have been similarly created. The aboriginal peoples in Canada may have specific rights on Crown land established under treaties signed when Canada was a British colony, and have claimed ownership of some Crown land.


=== Crown land in Australia ===
Much of Australia's land area is Crown land, which is administered by the Australian states. Much consists of pastoral leases, land owned and run by Aboriginal people (e.g. APY lands), and “unallocated” Crown land. Access to the latter is normally permitted for recreational purposes, though motorized vehicles are required to follow roads.


=== Public land in the US ===
Most state and federally managed public lands are open for recreational use. Recreation opportunities depend on the managing agency, and run the gamut from the free-for-all, undeveloped wide open spaces of the Bureau of Land Management lands to the highly developed and controlled US national parks and state parks. Wildlife refuges and state wildlife management areas, managed primarily to improve habitat, are generally open to wildlife watching, hiking, and hunting, except for closures to protect mating and nesting, or to reduce stress on wintering animals. National forests generally have a mix of maintained trails and roads, wilderness and undeveloped portions, and developed picnic and camping areas.


== Water ==


=== Foreshore ===

Public rights of way frequently exist on the foreshore of beaches. In legal discussions the foreshore is often referred to as the wet-sand area.
For privately owned beaches in the United States, some states such as Massachusetts use the low water mark as the dividing line between the property of the State and that of the beach owner. Other states such as California use the high-water mark.
In the UK, the foreshore is generally deemed to be owned by the Crown although there are notable exceptions, especially what are termed several fisheries which can be historic deeds to title, dating back to King John's time or earlier, and the Udal Law, which applies generally in Orkney and Shetland. While in the rest of Britain ownership of land extends only to the High water mark, and The Crown is deemed to own what lies below it, in Orkney and Shetland it extends to the lowest Spring ebb.
Where the foreshore is owned by the Crown the public has access below the line marking high tide.
In Greece, according to the L. 2971/01, the foreshore zone is defined as the area of the coast which might be reached by the maximum climbing of the waves on the coast (maximum wave run-up on the coast) in their maximum capacity (maximum referring to the “usually maximum winter waves” and of course not to exceptional cases, such as tsunamis etc.). The foreshore zone, apart from the exceptions in the law, is public, and permanent constructions are not allowed on it.
As with the dry sand part of a beach, legal and political disputes can arise over the ownership and public use of the foreshore. One recent example is the New Zealand foreshore and seabed controversy involving the land claims of the Māori people. However, the Marine and Coastal Area (Takutai Moana) Act 2011 guarantees free public access.


=== Rivers ===
The Rivers Access Campaign is being undertaken by the British Canoe Union (BCU) to open up the inland water-ways in England and Wales on behalf of members of the public. Under current England and Wales law, public access to rivers is restricted, and only 2% of all rivers in England and Wales have public access rights.


== See also ==


== References ==


== External links ==
Heritage Paths (Scottish rights of way)
Keep Ireland Open
Public rights of way (Naturenet)
Public rights of way for shoreline access in Rhode Island, USA
The Ramblers: Basic rights of way law
Scotsway: The Scottish Rights of Way & Access Society