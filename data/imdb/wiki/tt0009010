A Devil's wheel, Teufelsrad, Human Roulette Wheel or Joy Wheel is an amusement ride at public festivals and fairs. Devil's wheels have been used at Oktoberfest celebrations since at least 1910.


== Overview ==
A Devil's wheel is a rotating circular platform upon which riders sit or lie. It begins by spinning slowly and increases its speed to make riders slide off the platform. Spectators stand around the platform, the announcers often offer some satirical commentary.
Riders who slide off the platform usually hit a cushioned wall. Injuries are possible if riders collide with spectators or other objects. An operator controls the speed of the platform, sometimes slowing it down to give participants the opportunity to re-position themselves toward the center.


== Function ==

The Platform is a disc. It is not an inertial system because of its rotation. The riders' sense of balance is compromised by the rotating disk. This complicates movement towards or away from the center of the wheel. If one is moving radially outward, in addition to the expected centrifugal force (directed outward), the Coriolis force applies directed tangentially (i.e. laterally).


== Versions and use ==
A smaller version of the Devil's Wheel was displayed at EXPERIMINTA Science Center FrankfurtRheinMain in December 2011. Devil's wheels is still a part of the Munich Oktoberfest and in the Taunus Wunderland.Elements may be added to the Devils Wheel to make it even more difficult for the riders to hold on. A ball attached to a chain may swing at them or the ride may shake.


== In popular culture ==
In Evelyn Waugh's first published novel, Decline and Fall, the character Otto Silenus describes a Devil's Wheel (referred to as the "big wheel at Luna Park") as a metaphor for life.


== References ==


== External links ==
Abbildungen und Beschreibung eines Teufelsrades auf dem Oktoberfest
Spiele auf dem Teufelsrad, München
Video of Human Roulette Wheel at Steeplechase Park, Coney Island, early 1900s