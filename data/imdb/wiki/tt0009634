Souls for Sale is a 1923 American silent comedy-drama film written, directed, and produced by Rupert Hughes, based on the novel of the same name by Hughes. The film stars Eleanor Boardman in her first leading role, having won a contract with Goldwyn Pictures through their highly publicized "New Faces of 1921" contest just two years earlier.
The film is notable for its insights into the early film industry. Among the significant cameos in the film are appearances by directors King Vidor, Fred Niblo, Marshall Neilan, Charlie Chaplin, and Erich von Stroheim, as well as a number of actors, producers, and other filmmakers. Souls for Sale includes rare behind-the-scenes footage of Chaplin and von Stroheim directing the films A Woman of Paris and Greed, respectively.
Souls for Sale was previously thought to have been lost until incomplete prints of the film were discovered. The film was later restored and aired on Turner Classic Movies and was released on DVD in June 2009.Souls for Sale is one of many works from 1923 that entered the public domain in the United States in 2019.


== Plot ==
Remember "Mem" Steddon (Eleanor Boardman) marries Owen Scudder (Lew Cody) after a whirlwind courtship. However, on their wedding night, she has a change of heart. When the train taking them to Los Angeles stops for water, she impulsively and secretly gets off in the middle of the desert. Strangely, when Scudder realizes she is gone, he does not have the train stopped.
Mem sets off in search of civilization. Severely dehydrated, she sees an unusual sight: an Arab on a camel. It turns out to be actor Tom Holby (Frank Mayo); she has stumbled upon a film being shot on location. When she recuperates, she is given a role as an extra. Both Holby and director Frank Claymore (Richard Dix) are attracted to her. However, when filming ends, she does not follow the troupe back to Hollywood, but rather gets a job at a desert inn.
Meanwhile, Scudder is recognized and arrested at the train station. He turns out to be a cold-blooded murderer who marries women, insures them, and then kills them for the payoff. He escapes and persuades a gullible Abigail Tweedy (Dale Fuller) to file off his handcuffs. She becomes his next victim, though fortunately for her, he only robs her of her savings. He leaves the country and targets Englishwoman Lady Jane (Aileen Pringle). To his profound embarrassment, she turns out to be the same sort of crook as he; she and her father "Lord Fryingham" (William Orlamond) rob him, but let him live.
When the inn closes for the season, Mem travels to Hollywood in search of work. Her actress friend from the desert shoot, Leva Lemaire (Barbara La Marr), persuades Claymore to give her a screen test for the only uncast role in his next production: a comic part. Though she fails miserably, Claymore decides to train her anyway. She proves to be talented and steadily gets better and better parts.
Just as Mem is rising to fame, Scudder returns and sneaks into her bedroom. Holby and Claymore have become rivals for Mem's affections. When Scudder sees their warmly autographed photographs, he flies into a jealous rage. Mem, aware of her husband's past and fearful of a career-ending scandal, offers him money to leave her alone, but he wants her. Scudder leaves only when she threatens to kill herself. Claymore shows up, but when Scudder overhears the director propose marriage to his protegée, Scudder tries to shoot him. Claymore wrestles away his gun, but lets him go at Mem's urging.
When star Robina Teele (Mae Busch) is seriously injured by a falling light, Claymore decides to have Mem take her place. Filming continues on an outdoor circus set, complete with a full-scale Big Top tent. In the climax, a lightning storm sets the huge tent on fire in the middle of filming. (Claymore orders his cameramen to keep shooting.) Scudder, who has sneaked into the audience of extras, takes advantage of the panic and confusion to try to kill an unsuspecting Claymore by driving a wind machine (with a deadly propeller) at him. Holby spots Scudder and struggles with him. When Mem stumbles into the machine's path, Scudder rushes to save her and loses his own life. He apologizes before dying, explaining that all his life there was something wrong with him, but he did at least one thing right: they were never legally married after all. Afterward, Mem chooses Claymore over Holby.


== Cast ==

Cast notes:

This was the first film in which William Haines was credited as part of the cast.
Numerous stars of the silent era have cameos in this film. Erich von Stroheim is shown filming a scene from Greed. Charlie Chaplin guides Boardman in a fake scene from A Woman of Paris, which he did actually direct. The making of two other films, The Eternal Three and The Famous Mrs. Fair, is also presented. Other cameos include Hobart Bosworth, Barbara Bedford, Chester Conklin, William H. Crane, Kenneth C. Beaton (K.C.B.), Elliott Dexter, Raymond Griffith, Bessie Love, June Mathis, Marshall Neilan, ZaSu Pitts, John St. Polis, Kathlyn Williams, and Claire Windsor.


== Production ==
Writer/director Rupert Hughes was the brother of Howard Hughes Sr., and was responsible for introducing his nephew, Howard Hughes Jr., to the world of Hollywood movies.


== Reception ==
Carl Sandburg wrote in a contemporaneous review that it "[Sets] forth an eloquent advocacy of the viewpoint of Hollywood and the heart of moviedom by anyone who believes in it." Roger Ebert called it "a prime example of the mid-range entertainment Hollywood was producing so skillfully at the time." He also noted that Hughes "[A]dapted it from his own novel ... and judging by his title cards, he was well aware of how absurd his plot was."


== Rediscovery ==
Souls for Sale was thought to be lost, until copies began surfacing in various film vaults and private collections in the 1980s and 1990s. In 2005, a partnership between MGM and Turner Classic Movies resulted in a restored print of the film. Marcus Sjöwall, winner of TCM's Young Film Composers Competition, composed a new score for the film. The restored version with the new score premiered on TCM on January 24, 2006.


== Home media ==
In June 2009, the Warner Archive Collection released Souls for Sale on manufactured-on-demand DVD.


== See also ==
List of United States comedy films
List of rediscovered films


== References ==


== External links ==
Souls for Sale on IMDb
Souls for Sale at AllMovie
Souls for Sale at the American Film Institute Catalog
Souls for Sale at Rotten Tomatoes
Souls for Sale on YouTube
Souls for Sale is available for free download at the Internet Archive