"Ali Baba and the Forty Thieves" (Arabic: علي بابا والأربعون لصا‎) is a folk tale added to the One Thousand and One Nights in the 18th century by its French translator Antoine Galland, who heard it from the Maronite storyteller Hanna Diyab. As one of the most familiar of the "Arabian Nights" tales, it has been widely retold and performed in many media, especially for children, for whom the more violent aspects of the story are often suppressed.
In the original version, Ali Baba (Arabic: علي بابا‎ ʿAlī Bābā) is a poor woodcutter who discovers the secret of a thieves' den, and enters with the magic phrase "Open Sesame". The thieves try to kill Ali Baba, but Ali Baba's faithful slave-girl foils their plots. Ali Baba's son marries her and Ali Baba keeps the secret of the treasure.


== Textual history ==
The tale was added to the story collection One Thousand and One Nights by one of its European translators, Antoine Galland, who called his volumes Les Mille et Une Nuits (1704–1717). Galland was an 18th-century French Orientalist who heard it in oral form from a Maronite story-teller, called Hanna Diyab, who came from Aleppo in modern-day Syria and told the story in Paris. In any case, the earliest known text of the story is Galland's French version. Richard F. Burton included it in the supplemental volumes (rather than the main collection of stories) of his translation (published as The Book of the Thousand Nights and a Night) and thought its origins were Greek Cypriot.The American Orientalist Duncan Black MacDonald discovered an Arabic-language manuscript of the story at the Bodleian Library; however, this was later found to be a counterfeit.


== Story of Afternoon ==
Ali Baba and his older brother, Kasim, are the sons of a merchant. After their father's death, the greedy Kasim marries a wealthy woman and becomes well-to-do, building on their father's business. Ali Baba marries a poor woman and settles into the trade of a woodcutter.
One day, Ali Baba is at work collecting and cutting firewood in the forest, when he happens to overhear a group of 40 thieves visiting their stored treasure. Their treasure is in a cave, the mouth of which is sealed by a huge rock. It opens on the magic words "open sesame" and seals itself on the words "close sesame". When the thieves are gone, Ali Baba enters the cave himself and takes a single bag of gold coins home.
Ali Baba and his wife borrow his sister-in-law's scales to weigh their new wealth. Unbeknownst to them, Kasim's wife puts a blob of wax in the scales to find out what Ali Baba is using them for, as she is curious to know what kind of grain her impoverished brother-in-law needs to measure. To her shock, she finds a gold coin sticking to the scales and tells her husband. Under pressure from his brother, Ali Baba is forced to reveal the secret of the cave. Kasim goes to the cave, taking a donkey with him to take as much treasure as possible. He enters the cave with the magic words. However, in his greed and excitement over the treasure, he forgets the words to get out again and ends up trapped. The thieves find him there and kill him. When his brother does not come back, Ali Baba goes to the cave to look for him, and finds the body quartered and with each piece displayed just inside the cave's entrance, as a warning to anyone else who might try to enter.
Ali Baba brings the body home where he entrusts Morgiana, a clever slave-girl from Kasim's household, with the task of making others believe that Kasim has died a natural death. First, Morgiana purchases medicines from an apothecary, telling him that Kasim is gravely ill. Then, she finds an old tailor known as Baba Mustafa whom she pays, blindfolds, and leads to Kasim's house. There, overnight, the tailor stitches the pieces of Kasim's body back together. Ali Baba and his family are able to give Kasim a proper burial without anyone suspecting anything.
The thieves, finding the body gone, realize that another person must know their secret, and they set out to track him down. One of the thieves goes down to the town and comes across Baba Mustafa, who mentions that he has just sewn a dead man's body back together. Realizing the dead man must have been the thieves' victim, the thief asks Baba Mustafa to lead the way to the house where the deed was performed. The tailor is blindfolded again, and in this state he is able to retrace his steps and find the house. The thief marks the door with a symbol so the other thieves can come back that night and kill everyone in the house. However, the thief has been seen by Morgiana who, loyal to her master, foils the thief's plan by marking all the houses in the neighborhood similarly. When the 40 thieves return that night, they cannot identify the correct house, and their leader kills the unsuccessful thief in a furious rage. The next day, another thief revisits Baba Mustafa and tries again. Only this time, a chunk is chipped out of the stone step at Ali Baba's front door. Again, Morgiana foils the plan by making similar chips in all the other doorsteps, and the second thief is killed for his failure as well. At last, the leader of the thieves goes and looks himself. This time, he memorizes every detail he can of the exterior of Ali Baba's house.
The leader of the thieves pretends to be an oil merchant in need of Ali Baba's hospitality, bringing with him mules loaded with 38 oil jars, one filled with oil, the other 37 hiding the other remaining thieves. Once Ali Baba is asleep, the thieves plan to kill him. Again, Morgiana discovers and foils the plan, killing the 37 thieves in their oil jars by pouring boiling oil on them. When their leader comes to rouse his men, he discovers they are all dead and escapes. The next morning, Morgiana tells Ali Baba about the thieves in the jars. They bury them, and Ali Baba shows his gratitude by giving Morgiana her freedom.
To exact revenge, the leader of the thieves establishes himself as a merchant, befriends Ali Baba's son (who is now in charge of the late Cassim's business), and is invited to dinner at Ali Baba's house. However, the thief is recognized by Morgiana, who performs a sword dance with a dagger for the diners and plunges it into the thief's heart, when he is off his guard. Ali Baba is at first angry with Morgiana, but when he finds out the thief wanted to kill him, he is extremely grateful and rewards Morgiana by marrying her to his son. Ali Baba is then left as the only one knowing the secret of the treasure in the cave and how to access it.


== Classification ==
The story has been classified in the Aarne–Thompson classification system as AT 676.


== In popular culture ==


=== Audio recordings & Music ===
Audio readings/dramatizations include:

Dick Bentley played Ali Baba in a musical dramatization on Riverside Records (RLP 1451)/Golden Wonderland (GW 231).
The story was dramatized for Tale Spinners for Children on United Artists Records (UAC 11018).
Anthony Quayle narrated the story on Caedmon Records (TC 1251)/Fontana Records (SFL 14108).
Martyn Green narrated the story on Arabian Nights' Entertainment (Riverside Records RLP 1405).
Bing Crosby narrated and sang a version of the story for Simon & Schuster Records (A298:20)/Gala Records (GLP 351).
Bing Crosby recorded the story on 25 April 1957, linking the narrative with songs. This was issued as an album Ali Baba and the Forty Thieves in 1957.
The second track on "super group" Dark Lotus' album Tales from the Lotus Pod (2001) is titled "Ali Baba".
John Holt sings of the dream he had of Ali Baba in his song titled "Ali Baba".
On the first track on Licensed to Ill, "Rhymin' & Stealin", the Beastie Boys chant "Ali Baba and the forty thieves".


=== Books and comics ===
Tom Holt's mythopoeic novel Open Sesame is based on characters from the story of Ali Baba and the Forty Thieves.
In an Alvin comic book (Dell Comics No. 10, Jan.-Mar. 1965), The Chipmunks (Alvin, Theodore, and Simon) join eccentric scientist Dr. Dilby in his time machine. Their first stop is ancient Persia, where they meet Ali Baba and help him fight the 40 Thieves.
Although not a direct adaptation, the characters of Ali Baba, Cassim, and Morgiana as well as part of the concept of the Forty Thieves are featured in the Japanese manga series Magi. In 2012, this manga was adapted to anime.
Ali Baba was referenced in the classic Charles Dickens' novel A Christmas Carol, whom Ebenezer Scrooge recalls from his childhood.


=== Theatre - Stage ===

The story has been used as a popular pantomime plot for many years. An example of the "pantomime Ali Baba" was the pantomime/musical Chu Chin Chow (1916).
40 Thieves (1886) was a pantomime at the Royal Lyceum Theatre, Edinburgh.
Ali-Baba (1887) is an opéra comique, with music by Charles Lecocq.
Badi-Bandar Rupkatha (বাঁদী-বান্দার রূপকথা) is a 2014 Bangladeshi theatrical dance adaption of Ali Baba and Forty Thieves organised by Srishti cultural centre and Nrityanchal. Many leading Bangladeshi dancers performed in the adaption such as Shamim Ara Nipa, Shibli Sadiq, etc.


=== Theatrical Films ===


==== Live-action Foreign-language films ====
Ali Baba et les quarante voleurs is a 1902 short silent film directed by Ferdinand Zecca, and possibly the first film adaptation.*
Alibaba a film made in Bengali in 1937, was an adaptation of Kshirodprasad Bidyabinod play based on the original story. Sadhana Bose and Madhu Bose starred in the film as Mariana and Abdalla respectively.
Ali Baba is a 1940 Urdu/Hindi fantasy film directed by Mehboob Khan for Sagar Movietone. The music was directed by Anil Biswas, with lyrics written by Safdar Aah. The film was a bilingual, made in Punjabi language as Alibaba at the same time. It starred Surendra, Sardar Akhtar, Ghulam Mohammed and Wahidan Bai.
Ali Baba We El Arbeen Haramy (1942, in aka Ali Baba and the Forty Thieves) is an Egyptian film adaptation, starring Ali AlKassar as Ali Baba and the comedian actor Ismail Yasin as his assistant.
Ali Baba et les quarante voleurs (1954) is a French film starring Fernandel and Samia Gamal.
Alibaba Aur 40 Chor is a 1954 Hindi/Urdu fantasy action film directed by Homi Wadia
Alibabavum 40 Thirudargalum (1956, in English: Alibaba and Forty Thieves) is a Tamil film starring M. G. Ramachandran.
Ali Baba 40 Dongalu (1970) is a Telugu film based on Ali Baba, starring N T Rama Rao and Jayalalitha.
Ali Baba Bujang Lapok (1960) is a Malaysian comedy film which quite faithfully adhered to the tale's plot details but introduced a number of anachronisms for humour, for example the usage of a truck instead of donkey by Kassim Baba to steal the robbers' loot.
Adventures of Ali-Baba and the Forty Thieves (Hindi: Alibaba Aur 40 Chor, Russian: Приключения Али-Бабы и сорока разбойников, romanized: Priklucheniya Ali-Baby i soroka razboynikov) is a 1980 Indian-Soviet film based on the Arabian Nights story of Ali Baba and the Forty Thieves, directed by Uzbek director Latif Faiziyev with Indian director Umesh Mehra. The film stars Indian actors Dharmendra, Hema Malini and Zeenat Aman alongside Russian, Caucasian and Central Asian actors. The storyline is slightly altered to extend as a long movie. The writers were Shanti Prakash Bakshi and Boris Saakov, the music was scored by musician R.D. Burman, and the Choreographer was P. L. Raj. It was the most successful Indian-Soviet co-production, becoming a success in both India and the Soviet Union.
Alibaba Aur 40 Chor (Alibaba And 40 Thieves) is 1966 Hindi adventure fantasy film produced and directed by Homi Wadia and starring Sanjeev Kumar in the lead role.
Ali Baba 40 Dongalu' (transl. Ali Baba and the 40 Thieves) is a 1970 Telugu-language fantasy swashbuckler film, produced by N. Ramabrahmam under the Sri Gowtham Pictures banner and directed by B. Vittalacharya. It stars N. T. Rama Rao, Jayalalithaa in the lead roles
Marjina Abdulla (1973) is a Bengali musical film adaptation.
Ali Baba ve Kırk Haramiler (1971, in English: Ali Baba and the Forty Thieves) is a Turkish film, starring Sadri Alışık as Ali Baba.
Alibabayum 41 Kallanmaarum is a 1975 Indian Malayalam film, directed by J. Sasikumar and produced by M. J. Kurien. Starring Prem Nazir as Ali Baba
Ali Baba Aur 40 Chor (1980), a Soviet-Indian joint film starring Dharmendra, Hema Malini, Rolan Bykov and Zeenat Aman, was largely based on the adventure tale.


==== Live-action English-language films ====
Ali Baba and the Forty Thieves (1944), remade as The Sword of Ali Baba (1965), reimagines the thieves as freedom fighters against Mongol oppression, and Ali Baba as their leader. Frank Puglia portrayed the character named Kassim in both versions.
The story of Ali Baba was featured in Inkheart (2008). One of the 40 Thieves, named Farid (played by Rafi Gavron), is brought out of the story by Mortimer "Mo" Folchart and ends up becoming his ally.


==== Animation - USA ====
A Popeye cartoon, Popeye the Sailor Meets Ali Baba's Forty Thieves (1937), features Popeye meeting and defeating the titular group and their leader Abu Hassan (portrayed by Popeye's nemesis Bluto).
A Merrie Melodies Bugs Bunny/Daffy Duck cartoon, Ali Baba Bunny (1957), has a similar premise to the concept of the treasure-filled magical cave.
The Disneytoon Studios film DuckTales the Movie: Treasure of the Lost Lamp uses the reference of the folk tale but alters the name of Ali Baba to Collie Baba, the story origins reveals that the DuckTales version of the greatest thief that he stolen the magic lamp from the evil sorcerer name Merlock for good.
In Aladdin and the King of Thieves (1996), the 40 thieves play an integral part in the story. However, the story is very different from the original Ali Baba story, particularly Cassim's new role as Aladdin's father and the King of Thieves.
In the animated movie Ali Baba and the 40 Thievess-The Lost Scimitar of Arabia (2005), Ali Baba, the son of the Sultan of Arabia, is worried about his father's safety when he discovers that the Sultan's evil brother, Kasim, has taken over the throne and is plotting to kill him. With his friends, Ali returns to Arabia and successfully avoids his uncle's henchmen. Out in the desert, Ali becomes the leader of a group of forty men who are ready to fight against Kasim.


==== Animation - Europe & Asia ====
The story was adapted in the 1971 anime Ali Baba and the Forty Thieves (アリババと４０匹の盗賊, Aribaba to Yonjuppiki no Tozoku), storyboarded by Hayao Miyazaki.
Ali Baba is a 1973 Bengali short animated musical drama film directed by Rohit Mohra.
In the anime Magi: The Labyrinth of Magic (serialized since June 2009), Ali Baba appears as one of the main characters and one of Aladdin's friends. At some point in the show, he is shown as the leader of a gang of thieves called Fog Troupe. Morgiana is his loyal friend, whom Ali Baba freed from slavery, and Cassim is his friend from the slums, who is constantly jealous of Ali Baba and tries to bring him ill fate, when he can.


=== Television ===


==== Live-action ====
Indian TV serial based on Arabian Nights named Alif Laila 1993 by Ramanand Sagar had a 12 episode retelling of Alibaba and 40 thieves.
Ali Baba (2007) is a French telefilm starring Gérard Jugnot and Catherine Zeta-Jones.
In the American/British television mini-series Arabian Nights (2000), the story is told faithfully with two major changes. The first is: when Morgiana discovers the thieves in the oil jars, she alerts Ali Baba and, together with a friend, they release the jars on a street with a steep incline that allows the jars to roll down and break open. Furthermore, the city guard is alerted and arrest the disoriented thieves as they emerge from their containers. Later, when Morgiana defeats the thief leader, Ali Baba, who is young and has no children, marries the heroine himself.
2019 BBC/FX adaptation, in which Ali Baba was portrayed by Kayvan Novak, Ali Baba's role was expanded form the reference in the original novel in this TV adaption of the Dickens's novel


==== Animation ====
Happily Ever After: Fairy Tales for Every Child televised a "gender bender" version of Ali Baba in 1999, featuring the voices of Jurnee Smollett as a female Ali Baba, Tommy Davidson as Cassim, Marla Gibbs as the Grandmother, Will Ferrell as Mamet the Moocher, George Wallace as Baba Mustafa, and Bruno Kirby as the Great One.
In The Simpsons episode "Moe Goes from Rags to Riches" (29 January 2012), Lisa is Scheherazade, who tells the story to Nelson Muntz as King Shahryar.
Elements of Ali Baba were featured in the second Dinosaur King series, from episodes 18 through 21. One of the most common elements of the story featured the 39 Thieves (one of its 40 members was out sick), and it featured the "Open Sesame" phrase.


=== Video games ===
Ali Baba (1981) is a computer video game by Quality SoftwareAli Baba and 40 Thieves (1982), is an arcade video game by Sega.
Blade of Ali Baba is a unique item of the video game Diablo II (2000)
A version of Ali Baba appears in Sonic and the Secret Rings (2007), where he is portrayed by the character Tails.
A gang of thieves known as the "Forty Thieves" appears in Sly Cooper: Thieves in Time (2013).
"Alibaba" appears as the hacker alias of Futaba Sakura in Persona 5.
The story Alibaba and the 40 thieves appears on the website Poptropica as a playable island.


=== Military ===
At the United States Air Force Academy, Cadet Squadron 40 was originally nicknamed "Ali Baba and the Forty Thieves" before eventually changing its name to the "P-40 Warhawks".The name "Ali Baba" was often used as derogatory slang by American and Iraqi soldiers and their allies in the Iraq War, to describe individuals suspected of a variety of offenses related to theft and looting. Additionally, British soldiers routinely used the term to refer to Iraqi civilians. In the subsequent occupation, it is used as a general term for the insurgents.The Iraqis adopted the term "Ali Baba" to describe foreign troops suspected of looting.


=== Miscellaneous ===
Alibaba Group of China used the name because of its universal appeal.
Zero-knowledge proofs are often introduced to students of computer science with a pedagogical parable involving "Ali Baba's Cave."


== Gallery ==

		
		
		
		
		
		


== Notes ==


== External links ==
Ali Baba and the Forty Thieves (e-text, in English, at Bartleby.com)
Ali Baba and the Forty Thieves at the Internet Movie Database
Arabian Nights at the Internet Movie Database
The Sword of Ali Baba at the Internet Movie Database