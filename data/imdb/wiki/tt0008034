"The Green Door" (or "Green Door") is a 1956 popular song with music composed by Bob "Hutch" Davie and lyrics written by Marvin Moore


== Jim Lowe version ==
The song was first recorded by Jim Lowe, whose version reached number one on the US pop chart.  The lyrics describe the allure of a mysterious private club with a green door, behind which "a happy crowd" play piano, smoke and "laugh a lot", and inside which the singer is not allowed.  "Green Door" was backed by the orchestra of songwriter Davie, with Davie also playing piano, and by the vocal group the High Fives.  The track was arranged by Davie, who added thumbtacks to the hammers of his piano and sped up the tape to give a honky-tonk sound.  Released by Dot Records, the single reached #1 on the Billboard charts for one week on November 17, 1956, replacing "Love Me Tender" by Elvis Presley.  Outside the US, Lowe's version reached #8 on the charts, in the United Kingdom.


=== Charts ===


== Lyrics ==
The singer cannot get any sleep each evening, due to the sound of the music coming from the club. He tries to go there by knocking once on the green door, trying to tell the person behind the door that he had been there before, only to have the door slammed immediately ("hospitality's thin there"). Then, through the keyhole, he tries to say the possible secret password "Joe sent me" (the password for Hernando's Hideaway), which only results in laughter as he is again rejected admittance into the private club.


=== Possible inspirations ===
This song was written by Marvin Moore describing a place in Dallas, Texas, where he and Charlie Boland, a sports announcer, climbed a stairway to a green door where a piano played in the background and a man with a huge cigar kept people out unless they had a card of membership in one of the performing arts or unions. In a copy of the original lyrics he actually began using Dallas in the lyric. A copy of said original lyrics, along with an original letter in 1966 from Marvin Moore to Charlie Boland's daughter explains this to her after she sang the song to Marvin Moore one night over the phone at the age of 9.
After the Great Chicago Fire, a tavern opened in Chicago, the Green Door Tavern.  During prohibition, this was a popular place to get secret libations.  As the door of the tavern is green, the color became a symbol of a speakeasy. During the Prohibition Era many restaurants would paint their doors green to indicate the presence of a speakeasy.One suggestion of the song's origins is that it was inspired by an afterhours club in Dallas, Texas, to which lyricist Moore had been refused entry because he did not know the correct password.At the time of the song's initial popularity in the 1950s, many believed it was inspired by a green-doored restaurant and bar called "The Shack" in Columbia, Missouri, where singer Jim Lowe had attended the University of Missouri. However long-time Shack owner Joe Franke doubts this theory.An oft-repeated urban legend has developed saying the song refers to London's first lesbian club, Gateways (1930–1985), which was in Bramerton Street in Chelsea. It had a green door and was featured in the film The Killing of Sister George. But aside from that there is no substantive connection between the 1950s American song and the British club.In "The Green Door", a short story by O. Henry from his 1906 book The Four Million, a man named Rudolf Steiner is handed a mysterious card reading, "The Green Door." On entering the door he meets a starving young woman. He quickly rushes out and returns laden with food, and they become friends over supper; finally Steiner promises to visit her again the next day and there is romance in the offing. Eventually it turns out that the card was an advertisement for an entirely different "Green Door", a theatre play. O. Henry uses the eponymous green door as a symbol for everyday adventures which he encourages us to seek out.
It is also possible that the song is a reference to an H. G. Wells short story, "The Door in the Wall."Behind the Green Door (1940) is a Penny Parker mystery novel by Mildred Wirt Benson. In the novel, the secret door hides some illegal activity at a ski-resort hotel; no music or vice is involved in this book aimed at adolescent girls. It was reprinted in 1951, a few years before the song appeared.
Fitz-James O'Brien's short story, "The Lost Room", details a man being locked out of his own room by a group of demons and bears some similarity to the themes of the song.


== Shakin' Stevens version ==
Welsh singer Shakin' Stevens covered the song in 1981 for his album Shaky. It became his second UK number 1, topping the charts for four weeks in August 1981.


=== Charts ===


== Other recordings ==
In the UK, a version by Frankie Vaughan was even more popular than the original, reaching No. 2.
Another UK recording, by Glen Mason, reached No. 24 on the UK chart.
Gene McDaniels released a version of it as a single in 1960, but it failed to chart.
In 1964, Bill Haley & His Comets recorded a version for a single release on Decca Records during an unsuccessful attempt to make a comeback with the label that had made them famous with "Rock Around the Clock" (this version was produced by Milt Gabler); Haley and the Comets also recorded an instrumental version in 1962 for the Mexican Orfeon Records label. *Country humorist Mayf Nutter re-charted the song in 1973.
Crystal Gayle recorded the song in 1977, and it has since become a fan favorite at her concerts.
Psychobilly band the Cramps covered the song on their 1981 album, Psychedelic Jungle.
The Spanish pop-punk group Los Nikis made a Spanish version in 1986.
Other versions have been recorded by Roland Alphonso, Wynder K. Frog, Houston and Dorsey, Ray Hamilton, The Promenade Orchestra and Chorus featuring vocals by Joe Seneca, Danny Colfax Mallon, Country Dick Montana, Esquerita, Jumpin' Gene Simmons, Skip & Flip (1961), The Jerms, The Go-Katz and Skitzo.


== Cultural impact ==
It is also the name of a letter written by David Berg, the former leader of the cult once called the Children of God and later renamed "The Family"—he used it as a metaphorical door to hell.Behind the Green Door is a 1972 pornographic film starring Marilyn Chambers. In 1986 Behind the Green Door: the Sequel was released and in 2012 New Behind the Green Door.
There are bars, taverns and saloons named "The Green Door" in many American locations, including Cheyenne, Wyoming, New York City, Park Hall, Maryland, Chicago, Illinois and Lansing, Michigan.
Within the American intelligence community, "green door" is a slang verb and adjective, relating to the restriction of an individual's or organization's access to information and/or locations: "We green doored them," or "The situation has been highlighted by the 'Green Door' compartmentation and exclusion". This meaning was alluded to in episode 4 of Ashes to Ashes, set in 1981, when Shakin' Stevens' cover played whilst the protagonist police detectives sneaked out of a top-secret MOD research centre.
It was also featured in Quentin Tarantino's 2019 film, Once Upon a Time in Hollywood, sung by Leonardo DiCaprio (playing the character of fictional actor Rick Dalton) during a segment on Hullaballoo.


== References ==