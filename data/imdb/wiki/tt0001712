Elizabeth "Jane" Shore (née Lambert) (c. 1445 – c. 1527) was one of the many mistresses of King Edward IV of England. She became the best-known to history through being later accused of conspiracy by the future King Richard III, and compelled to do public penance. She was also a sometime mistress of other noblemen, including Edward's stepson, Thomas Grey, 1st Marquess of Dorset, and William Hastings, 1st Baron Hastings but ended her life in bourgeois respectability.


== Early life and first marriage ==
Born in London in about 1445, Elizabeth Lambert was the daughter of a prosperous merchant, John Lambert, and his wife Amy, who was the daughter of a well-off grocer named Robert Marshall. The name "Jane", which has sometimes been attached to her, was the invention of a 17th-century playwright (Heywood), because during the course of the sixteenth century, her real first name was omitted, then forgotten by authors. The tradition must go further back, however, as on 28 August 1599 was licensed the ‘History of the Life and Death of Master Shore and Jane Shore his Wife'.Spending time in her father's shop at a young age may have brought the young Elizabeth into contact with ladies of high rank. C. J. S. Thompson's highly romanticised biography, The Witchery of Jane Shore, the Rose of London: The Romance of a Royal Mistress (1933) claimed that she was able to observe their behaviour and gain an understanding of the manners of those higher ranking than herself. She was thought to have been highly intelligent, and as a result received an education that was not usually associated with a person of her class. Thompson also claimed that her beauty earned her the title of "The Rose of London" – although this is not mentioned in contemporary sources. According to Thomas More, writing when Shore was elderly, she had been fair of body though not tall; she was attractive to men more through her personality than her physical beauty, being intelligent, literate, merry and playful.She attracted many suitors, among them William Hastings, 1st Baron Hastings, friend and confidant of Edward IV. It is likely Hastings fell in love with Elizabeth Lambert before her marriage; his affection for her is apparent later in life by his continual protection of her.Such extreme attention made John Lambert desirous of finding his daughter a suitable husband. Such an opportunity presented itself with William Shore (d. 1494), a goldsmith and banker and common visitor to the Lambert home. He was approximately 14 or 15 years older than Jane. Though handsome and well-to-do, he never really won her affections. Their marriage was annulled in March 1476 after she petitioned for the annulment of her marriage on the grounds that her husband was impotent, which prevented her from fulfilling her desire to have children. Pope Sixtus IV commissioned three bishops to decide the case, and they granted the annulment.


== Royal mistress ==
According to the Patent Rolls for 4 December 1476, it was during this same year that Shore began her liaison with Edward IV, after his return from France. Edward did not discard her as he did many of his mistresses, and was completely devoted to her. She had a large amount of influence over the king, but would not use it for her own personal gain. This was exemplified by her practice of bringing those out of favour before the king to help them gain pardon. Shore, according to the official records, was not showered with gifts, unlike many of Edward's previous mistresses. Their relationship lasted until Edward's death in 1483.


== Prison, second marriage and later life ==

Shore's two other lovers were Edward IV's eldest stepson, Thomas Grey, 1st Marquess of Dorset, and William Hastings, 1st Baron Hastings. Grey's wife was the wealthy heiress Cecily Bonville, 7th Baroness Harington, who also happened to be Hastings' stepdaughter. Shore was instrumental in bringing about the alliance between Hastings and the Woodvilles, which was formed while Richard, Duke of Gloucester, was Protector, before he took the throne as King Richard III. She was accused of carrying messages between Hastings and Edward IV's widow, Elizabeth Woodville. It was because of her role in this alliance that Shore was charged with conspiracy, along with Hastings and the Woodvilles, against the Protector's government.Shore's punishment included open penance at Paul's Cross for her promiscuous behaviour by Richard, though this may have been motivated by the suspicion that she had harboured Grey when he was a fugitive or as a result of Richard's antagonism towards any person who represented his older brother's court. A clash of personalities between the lighthearted Shore and stern Richard also generated a mutual dislike between the two. Shore accordingly went in her kirtle through the streets one Sunday with a taper in her hand, attracting a lot of male attention along the way.After her public penitence, Shore resided in Ludgate prison. While there, she captivated the King's Solicitor General, Thomas Lynom. After he expressed an interest in Shore to Richard, the King tried to dissuade him for his own good. This is evinced by a letter to John Russell from Richard, where the King asked the chancellor to try to prevent the marriage, but if Lynom were determined on the marriage, to release Shore from prison and put her in the charge of her father until Richard's next arrival in London when the marriage could take place. They were married and had one daughter. It is believed that Shore lived out the remainder of her life in bourgeois respectability. Lynom lost his position as King's Solicitor when Henry VII defeated Richard III at the Battle of Bosworth in August 1485, but he was able to stay on as a mid-level bureaucrat in the new reign, becoming a gentleman who sat on the commissions in the Welsh Marches and clerk controller to Arthur, Prince of Wales, at Ludlow Castle. Thomas More attested that even in old age an attentive observer might discern in her shriveled countenance traces of her former beauty.


== Fiction ==
For a bibliography see James L. Harner, "Jane Shore in Literature: A Checklist" in Notes and Queries, v. 226, December 1981, p. 496.


=== Drama ===

She is a significant character in The True Tragedy of Richard III, an anonymous play written shortly before Shakespeare's Richard III. In the play she is reduced to destitution on the streets, ignored by both former lovers and people she had helped after Richard frightens citizens with severe punishments if she is supported in any way.
"Mistress Shore" is frequently mentioned in William Shakespeare's play, Richard III. (She actually appears in Laurence Olivier's 1955 film version, played by Pamela Brown – she has only one line: "Good morrow, my Lord", which is interpolated into the film. The film shows her as attending to Edward IV, but afterwards having a passionate affair with Lord Hastings.) Edward IV, Thomas Grey, and Lord Hastings are all characters in the play.
The story of Jane Shore's wooing by Edward IV, her influence in court, and her tragic death in the arms of Matthew Shore is the main plot in a play by Thomas Heywood, Edward IV (printed 1600). The play shows her struggling with the morality of accepting the king's offers, using her influence to grant pardons to those wrongfully punished, and expressing regret for her relationship with Edward. In this version, her first marriage is never annulled, but the two are reconciled right before dying and being buried together in "Shores Ditch, as in the memory of them". This is supposed to be the origin of the name Shoreditch.
The Tragedy of Jane Shore is a 1714 play by Nicholas Rowe. Rowe portrays her as a kind woman who encourages her lover Hastings to oppose Richard's usurpation of power. In revenge Richard forces her to do penance and to become an outcast. As in Heywood's version, her husband seeks her out and they are reconciled before she dies.
A performance of Jane Shore was given on Saturday 30 July 1796 at a theatre in Sydney. The pamphlet for the play was printed by a convict in the settlement, George Hughes, who was the operator of Australia's first printing press.  The pamphlet for the play is the earliest surviving document printed in Australia.  It was presented as a gift to Australia by the Canadian Government and is held at the National Library of Australia in the National Treasures collection in Canberra.


=== Poetry ===
Thomas Churchyard published a poem about her in Mirror for Magistrates.
Anthony Chute's 1593 poem "Beauty Dishonoured, written under the title of Shore's wife" is supposed to be the lament of Jane Shore, whose ghost tells her life story and makes moral reflections.
Michael Drayton wrote a poem about her in his Heroical Epistles.
Andrew Marvell refers to her in "The King's Vows", a satire on Charles II, in which the king says, "But what ever it cost I will have a fine Whore, /As bold as Alce Pierce and as faire as Jane Shore."


=== Novels ===
The Goldsmith's Wife (1950) by Jean Plaidy
She appears in Anne, The Rose of Hever (1969) by Maureen Peters
She appears in Elizabeth, the Beloved (1972) by Maureen Peters
Figures in Silk (2008) by Vanora Bennett is told from her (fictional) sister Isabel's perspective as well as Jane's
She is the main character in Isolde Martyn's Mistress to the Crown (2013)
She is the main character in Royal Mistress (2013) by Anne Easter Smith.
She is mentioned several times and modern translation of the Thomas Lynom letter concerning her is published in Josephine Tey's novel "The Daughter of Time" (1956).
She appears as a minor character in The Sunne in Splendour (1982) by Sharon Kay Penman
Shore appears in Philippa Gregory's The White Queen (2009), a novel about Elizabeth Woodville, Queen Consort to Edward IV, under her real name, Elizabeth.  In the television adaptation, she is referred to by her more familiar name of "Jane Shore".
A character in George R. R. Martin's A Song of Ice and Fire series is forced to perform a penance walk modeled very loosely after Shore's.


=== Film ===
The IMDB database lists three films entitled Jane Shore: 

Jane Shore (1911)
Jane Shore (1915) (in which she was played by Blanche Forsyth)
Jane Shore (1922) (in which she was played by Sybil Thorndike)


=== Television ===
Shore is portrayed by Emily Berrington in The White Queen, the 2013 TV adaptation of Gregory's novel.


== See also ==
English royal mistress


== References ==


== Sources ==
Clive, Mary (1973). This Sun of York: A Biography of Edward IV. London: Macmillan.
Kendall, Paul M. (1956). Richard the Third. New York: W.W. Norton and Company Inc.
Scofield, Cora L. (1967). The Life and Reign of Edward the Fourth: King of England and of France and Lord of Ireland 2. London: Frank Cass and Co. Ltd.
Thompson, C.J.S. (1933). The Witchery of Jane Shore, the Rose of London. the romance of a royal mistress. London.
Thompson, C.J.S. (2003). The Witchery of Jane Shore. Whitefish: Kessinger Publishing.


== External links ==
Kingsford, Charles Lethbridge (1911). "Shore, Jane" . Encyclopædia Britannica. 24 (11th ed.). p. 1003.