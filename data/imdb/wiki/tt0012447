The March Hare (called Haigha in Through the Looking-Glass) is a character most famous for appearing in the tea party scene in Lewis Carroll's 1865 book Alice's Adventures in Wonderland.
The main character, Alice, hypothesizes,

"The March Hare will be much the most interesting, and perhaps as this is May it won't be raving mad – at least not so mad as it was in March.""Mad as a March hare" is a common British English phrase, both now and in Carroll's time, and appears in John Heywood's collection of proverbs published in 1546. It is reported in The Annotated Alice by Martin Gardner that this proverb is based on popular belief about hares' behaviour at the beginning of the long breeding season, which lasts from February to September in Britain. Early in the season, unreceptive females often use their forelegs to repel overenthusiastic males. It used to be incorrectly believed that these bouts were between males fighting for breeding supremacy.Like the character's friend, the Hatter, the March Hare feels compelled to always behave as though it is tea-time because the Hatter supposedly "murdered the time" whilst singing for the Queen of Hearts. Sir John Tenniel's illustration also shows him with straw on his head, a common way to depict madness in Victorian times. The March Hare later appears at the trial for the Knave of Hearts, and for a final time as "Haigha" (which is pronounced to rhyme with "mayor", according to Carroll, and a homophone of "hare" in a non-rhotic accent), the personal messenger to the White King in Through the Looking-Glass (Alice either does not recognize him as the March Hare of her earlier dream, or chooses not to comment about this).


== Interpretations ==


=== Alice in Verse ===

The major departure from Carroll's original here is that instead of appearing a jittery witness, the March Hare is cast as the Prosecutor. After the charge is read, the Hare addresses the court with an opening statement that more or less vindicates the accused, before turning his accusing eye upon the court itself for failing to serve tea with the evidence (the tarts).


=== Alice in the Country of Hearts ===
In this Japanese manga, Alice in the Country of Hearts, the March Hare is Elliot March. Elliot is Blood Dupre (the Hatter)'s right-hand man. He is basically human with the exception of two brown rabbit ears. When called a rabbit, he often becomes insulted and rants about how his ears are 'just bigger than average'. He isn't specifically crazy or mad, but he is a bit violent in the beginning. He almost kills Alice with his long-barrelled gun before Blood stopped him. But, as the story progresses, it is shown that Elliot is a lovable, amusing character who is really very sweet.


=== Pandora Hearts ===
In this Japanese manga, Pandora Hearts, the March Hare is a "Chain" whose "Contractor" is Reim Lunettes. It has the ability to fake death which helps Reim escape his attackers and proved to be so realistic that his comrades believed he really was dead. The March Hare was said to be a "gentle Chain" which was not suited for battle, but very useful in its own ways. In a way, it contradicts all the varieties of the March Hare, as the Hare is shown to be mad or even insane. The character Reim himself is also similar to March Hare as his friend Break has the chain Mad hatter mirrors the friendship of the Hatter and the Hare's.


== In popular culture ==
The March Hare was played by Charlie Ruggles in Alice in Wonderland.
In SyFy's TV Miniseries Alice, the March Hare is represented by the character Mad March.
The March Hare is featured as the primary antagonist in the Once Upon a Time story "Tea Party in March" in the graphic novel Once Upon a Time: Out of the Past.
In the song entitled "We Have Heaven" by the British rock group Yes, a lyric mantra is sung from beginning to end saying "Tell the Moon Dog, tell the March Hare...".


=== Disney animated film ===

Disney's Alice in Wonderland, an animated film, depicted the March Hare at the tea party as being deliriously confused. He repeatedly offers Alice a cup of tea, but distractedly pulls the cup out of her reach or takes it from her hands just as she is about to drink. He was voiced by Jerry Colonna, after whom his appearance and personality were modelled. He was animated by Ward Kimball. Kimball also led the Dixieland band Firehouse Five Plus Two, in which he played trombone.
This version of the character was also a semi-regular on Bonkers and one of the guests in House of Mouse, often seen seated with the Mad Hatter. During these appearances, the March Hare was voiced by Jesse Corti and Maurice LaMarche.
The March Hare also appears in the "Mad T Party" in Disney's California Adventure park. He is based on the 2010 film's Thackery Earwicket interpretation, and plays bass guitar. He is often found hopping around with Mallymkun the Dormouse on stage.


=== Tim Burton's Alice in Wonderland ===

The March Hare appears in the 2010 Disney film Alice in Wonderland, voiced by Paul Whitehouse. His full name is Thackery Earwicket; this, however, is not mentioned in the film. In the movie, the March Hare behaves as if constantly nerve-wracked and completely delirious. He is a cook in the film, and the way he eccentrically throws dishes and pots suggests he is an amalgam of both the March Hare and the cook from Lewis Carroll's original book. The March Hare has a strong Scottish accent in this movie, while his friend the Mad Hatter (played by Johnny Depp) switches into a Scottish accent as well whenever his emotions are strained. He is first seen in the "Tea Party" scene, which takes place at his "Hare House" windmill. Thackery hosts a tea party, which he shares with Tarrant Hightopp the Mad Hatter, Mallymkun the Dormouse, and Chess the Cheshire Cat. He appears a second time in the White Queen's kitchen, frantically cooking and throwing dishes. His third appearance is at the Frabjous Day scene, in which he stands with the other characters wielding a ladle as his weapon, nervous and somewhat ready to go to battle. Burton stated that because Whitehouse is a great comedic actor, a lot of his lines came from improvisation.


=== Games ===
In the game American McGee's Alice, the March Hare is portrayed as a victim of the Mad Hatter's insane experimentation. Both the Hare and the Dormouse have become clockwork cyborgs.
The March Hare appears in Alice: Madness Returns (a sequel to American McGee's Alice).
In the video game adaptation of Tim Burton's Alice in Wonderland, Thackery Earwicket is a playable character. His special ability is telekinesis and his main form of attack is to throw dishes. He also uses his big ears and large feet as weapons. He uses his telekinesis to defeat the Bandersnatch.


== References ==