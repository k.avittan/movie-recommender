Billy Porter (born September 21, 1969) is an American actor and singer. He attended the Musical Theater program at Pittsburgh Creative and Performing Arts School's School of Drama, graduated from Carnegie Mellon University School of Drama, and achieved fame performing on Broadway before starting a solo career as a singer and actor.Porter won the 2013 Best Actor in a Musical for his role as Lola in Kinky Boots at the 67th Tony Awards. He credits the part for "cracking open" his feminine side to confront toxic masculinity. For the role, Porter also won the Drama Desk Award for Outstanding Actor in a Musical and Outer Critics Circle Award for Outstanding Actor in a Musical. In 2014 Porter won the Grammy Award for Best Musical Theater Album for Kinky Boots. He currently stars in the television series Pose for which he was nominated for a Golden Globe Award and won the 2019 Primetime Emmy Award for Outstanding Lead Actor in a Drama Series, becoming the first openly gay black man to be nominated and win in any lead acting category at the Primetime Emmys.
Porter is included in Time magazine 's 100 Most Influential People of 2020.


== Early life ==
Porter was born in Pittsburgh, Pennsylvania, to William E. Porter and Cloerinda Jean Johnson Porter Ford. His sister is Mary Martha Ford-Dieng. After attending Reizenstein Middle School, Porter went to Taylor Allderdice High School and then the Pittsburgh Creative and Performing Arts School, where he graduated in 1987. He graduated from the College of Fine Arts at Carnegie Mellon University with a B.F.A. in Drama and earned a certification from the graduate-level Professional Program in Screenwriting at UCLA.
During the summers of 1985–1987, Porter was a member of entertainment groups, "Spirit" and "Flash," which performed daily at Kennywood Park in West Mifflin, Pennsylvania.


== Career ==
Porter played Teen Angel in the 1994 Broadway revival of Grease. Other shows he has been in include Topdog/Underdog at City Theatre (2004), Jesus Christ Superstar and Dreamgirls at Pittsburgh Civic Light Opera (2004), and the song cycles Myths and Hymns and Songs for a New World (Off-Broadway, 1995).Porter wrote and performed in his one-person autobiographical show, Ghetto Superstar (The Man That I Am) at Joe's Pub in New York City in February and March 2005. He was nominated for "Outstanding New York Theater: Broadway & Off Broadway Award" at the 17th GLAAD Media Awards.In September 2010, Porter appeared as Belize in Signature Theatre Company's 20th Anniversary production of Tony Kushner's Angels in America.Porter originated the role of "Lola" in Kinky Boots on Broadway in 2013, with songs by Cyndi Lauper, book by Harvey Fierstein and directed/choreographed by Jerry Mitchell. Porter won both the 2013 Drama Desk Award for Outstanding Actor in a Musical and Tony Award for Best Actor in a Musical for this role.Porter has also appeared in many films. He played a major role as Shiniqua, a drag queen who befriends Angel (David Norona) and Lee (Keivyn McNeill Graves) in Seth Michael Donsky's Twisted (1997), an adaptation of Oliver Twist. He has also appeared on The RuPaul Show.
He has had a musical career with three solo albums released, Billy Porter on DV8/A&M Records in 1997, At the Corner of Broadway + Soul in 2005 on Sh-K-Boom Records and Billy's Back on Broadway (Concord Music Group) in 2014. He featured in a number of songs in the tribute album; It's Only Life: The Songs of John Bucchino in 2006 released on PS Classics. He sings on Adam Guettel's 1999 album Myths and Hymns studio cast album on Nonesuch Records. He also covered "Only One Road" that was included on the Human Rights Campaign compilation album Love Rocks.
Porter wrote the play While I Yet Live, which premiered Off-Broadway at Primary Stages in September 2014 in previews, officially on October 12. In addition to Porter, the cast included Lillias White and S. Epatha Merkerson.Porter released Billy Porter Presents the Soul of Richard Rodgers in April 2017. The album, which features new, soulful takes on classic Richard Rodgers songs, includes solos and duets from the following artists (in addition to Porter himself): Tony and Grammy Award winners Cynthia Erivo (The Color Purple), Renée Elise Goldsberry (Hamilton) and Leslie Odom Jr. (Hamilton), Tony Award-winner Patina Miller (Pippin), Grammy Award winners Pentatonix and India Arie, Tony Award nominees Brandon Victor Dixon (Shuffle Along), Joshua Henry (Violet), and Christopher Jackson (Hamilton), alongside YouTube and Kinky Boots star Todrick Hall and multiple Grammy Award nominees Deborah Cox and Ledisi.Porter reprised the role of Lola in Kinky Boots in September 2017 on Broadway, where he did a 15-week run.In 2018, Porter starred in the FX show Pose in the role of Pray Tell. In 2019, Pose earned its renewal for a third season after airing just one episode from the second season. In August 2018, Porter confirmed via Instagram that he was joining the cast of American Horror Story for its eighth season, subtitled Apocalypse. Porter duetted with Pose co-star Dyllón Burnside and sang from his album in a benefit concert emceed by Burnside on July 23, 2018, to celebrate the season 1 finale and to raise money for GLSEN. In June 2019, to mark the 50th anniversary of the Stonewall riots, sparking the start of the modern LGBTQ rights movement, Queerty named him one of the Pride50 "trailblazing individuals who actively ensure society remains moving towards equality, acceptance, and dignity for all queer people." Also in June 2019 he presented the Excellence in Theatre Education Award at the 73rd Tonys at Radio City Music Hall. However, he earned media coverage for his haute couture red and pink gown, upcycled from Kinky Boots' stage curtains, in a uterine shape, and his impromptu performance of "Everything's Coming up Roses" from Gypsy, for host James Corden's "Broadway karaoke." In September 2019, he was nominated for a Golden Globe Award and won the Primetime Emmy Award for Outstanding Lead Actor in a Drama Series for Pose, becoming the first openly gay black man to be nominated and win in any lead acting category at the Primetime Emmys.
Also, in 2019, Porter had a cameo appearance in Taylor Swift's "You Need to Calm Down" music video that featured twenty LGBTQ icons.Porter performed "For What It's Worth" with Stephen Stills during the 2020 Democratic National Convention.


== Fashion ==
At the 2019 Golden Globes, Porter gained attention for wearing an embroidered suit and pink cape designed by Randi Rahm. He continued to make fashion waves that year when he wore a fitted tuxedo jacket and a velvet gown by Christian Siriano with 6" Rick Owens boots to the 91st Academy Awards. In February 2019, he was an Official Council of Fashion Designers of America (CFDA) Ambassador for New York Fashion Week: Mens. Porter attended the 2019 Met Gala and embraced the Camp: Notes on Fashion theme by being carried on a litter by six shirtless men while sporting a "Sun God" ensemble. The Blonds designed Porter's outfit, and it included a bejeweled catsuit outfitted with 10-foot wings, a 24-karat gold headpiece, as well as custom gold-leaf Giuseppe Zanotti shoes and fine jewels by Andreoli, John Hardy, and Oscar Heyman.


== Personal life ==
Porter is gay; he married his now-husband, Adam Smith on January 14, 2017.Porter shared his racial views in a 2020 interview with Vanity Fair, "The reason why our country is in the mess we're in is simply because of whiteness. White supremacy. White people choke-holding power and sucking the life out of humanity".


== Awards and nominations ==
Broadway.com Audience Choice Awards, 2013 for Favorite Leading Actor in a Broadway Musical in Kinky Boots, wonFavorite Funny Performance, nom
Best Onstage Pair with Stark Sands, nom
Critics' Choice Awards, 2019, Best Television Drama Series Actor for Pose, nom2020, nom
Dorian Awards, 2019, TV Performance of the Year – Actor, wonTV Musical Performance of the Year, "Home" (with Mj Rodriguez and Our Lady J), won
2020, TV Performance of the Year – Actor for Pose, won
Wilde Wit of the Year, Billy Porter, nom
Wilde Artist of the Decade, nom
Drama League Award, 2013, Distinguished Performance Award for Kinky Boots, nomDrama Desk Award, Outstanding Leading Actor in a Musical, won
Emmy Awards (Primetime), 2019, Outstanding Lead Actor in a Drama Series for Pose, won
Fred and Adele Astaire Awards, 2013, Outstanding Male Dancer in a Broadway Musical Show for Kinky Boots, nom
GLAAD Media Awards, 2017, Vito Russo Award to Billy Porter, won
Gold Derby Awards, 2019, Television Drama Series Actor of the Year for Pose, wonPerformer of the Year, nom
Actor of the Decade, nom
Golden Globe Awards, 2019, Best Actor in a Television Series – Drama, nom77th Golden Globe Awards, 2020, nom
Grammy Awards, 2014, Best Musical Theater Album for Kinky Boots, won
Outer Critics Circle Award, 2013, Outstanding Lead Actor in a Musical for Kinky Boots, won
Television Critics Association, 2019, Individual Achievement in Drama for Pose, nom
Tony Awards, 2013, Best Leading Actor in a Musical for Kinky Boots, won
Queerties, 2020, Badass of the Year for Billy Porter, won


== Discography ==


=== Albums ===
1997: Billy Porter (DV8/A&M Records)
2005: At the Corner of Broadway + Soul (Sh-K-Boom Records)
2014: Billy's Back on Broadway (Concord Music Group)
2017: Billy Porter Presents the Soul of Richard Rodgers (Masterworks Broadway)


=== Singles ===
1997: "Show Me"/"What Iz Time"
2005: "Awaiting You"/"Time" (Live) (Sh-K-Boom Records)
2017: "Edelweiss"
2019: "Love Yourself"
2020: The Shapeshifters feat. Billy Porter "Finally Ready" 


=== Other songs ===
"Only One Road" on Love Rocks compilation album
"Love Is on the Way" on The First Wives Club album
"Destiny" with Jordan Hill on Jim Brickman's Greatest Hits album
"Where Is Love?" with Liz Callaway


=== Appears in ===
Featured on a number of songs on tribute album It's Only Life: The Songs of John Bucchino
Adam Guettel's album Myths and Hymns in 1999
He is featured with Alan Cumming, David Raleigh and Ari Gold in a cover of "That's What Friends Are For", of 'The Friends Project' in support of the Ali Forney Center, a NYC shelter for homeless LGBT youth. The song was arranged and produced by Nathan Leigh Jones and directed by Michael Akers.


== Concerts ==
Porter has performed at various venues in New York City, including Lincoln Center, which was broadcast on PBS in 2015
 and Joe's Pub in New York City.


== Filmography ==


=== Film ===


=== Television ===


=== Theatre ===
Sources:Playbill Vault; Off-Broadway Database
Miss Saigon, Ensemble/John (u/s), Broadway (1991)
Grease, Teen Angel, Broadway (1994)
The Merchant of Venice, Solanio, Off-Broadway (1995)
Songs for a New World, performer, Off-Broadway (1995)
Smokey Joe's Cafe, performer, Broadway (1995–97)
Miss Saigon, John (replacement), Broadway (1998–99)
Jesus Christ Superstar, Jesus of Nazareth, Helen Hayes Performing Arts Center, Nyack, NY (1998)
Dreamgirls, James Thunder Early, New York Actors Fund concert (September 2001)
Radiant Baby, Various, Off-Broadway (2003)
Topdog/Underdog, City Theatre, Pittsburgh, PA (2004)
Little Shop of Horrors, Audrey ll (replacement), Broadway (2004)
Chef's Theater: A Musical Feast, Performer, Off-Broadway (2004)
Ghetto Superstar, Performer, Off-Broadway (2005) – also playwright
Birdie Blue, Bam/Little Pimp/Sook/Minerva, Off-Broadway (2005)
Putting It Together, performer, New York (2009)
Angels in America, Belize, Off-Broadway (2010)
Kinky Boots, Lola, Broadway (2013–2015)
Kinky Boots, Lola (replacement), Tour (2014)
HAM: A Musical Memoir, Off-Broadway (2015) – director
 Shuffle Along, or the Making of the Musical Sensation of 1921 and All That Followed, Aubrey Lyles, Broadway (2016)
White Rabbit Red Rabbit, Off-Broadway (2016)
Kinky Boots, Lola (replacement), Broadway (2017)


== See also ==
LGBT culture in New York City
List of self-identified LGBTQ New Yorkers


== References ==


== External links ==
Billy Porter at AllMovie
Billy Porter at AllMusic 
Billy Porter on IMDb
Billy Porter on LastFM