This is a comprehensive listing of Wallace Reid's (1892-1923) silent film output. Reid often played a clean-cut, well-groomed American go-getter on screen, which is how he is best remembered, but he could alternate with character roles, especially in his early short films, most of which are now lost. Some films have him as a director, some have him as an actor and some have him as both in particular his numerous short films. His first feature film is the famous appearance as a young blacksmith in The Birth of a Nation in 1915.


== 1911 ==
The Phoenix (1910) *short ...as Young Reporter
The Leading Lady (1911) *short
The Reporter (1911) *short ...as Cohn, Jones' Assistant
The Mother of the Ranch (1911) *short ...as The Mother's Friend back East
War (1911) *short  ...as Midas


== 1912 ==

A Red Cross Martyr, or, On the Firing Lines of Tripoli (1912) *short
The Path of True Love (1912) *short ...as The Country Boy
Chumps (1912) *short ...as George, the Denouement
Jean Intervenes (1912) *short ...as Billy Hallock
Indian Romeo and Juliet (1912) *short ...as Oniatore/Romeo
Playmates (1912) *short ...as Party Guest at Piano(uncredited)
The Telephone Girl (1912) *short ...as Jack Watson
The Seventh Son (1912) *short ...as One of the Beecham Brothers
The Illumination (1912) *short ...as Giuseppe's Father
At Scrogginses' Corner (1912) *short
Brothers (1912) *short ...as Minor role
The Victoria Cross (1912) *short ...as Lt. Cholmodeley
The Hieroglyphic (1912) *short ...
Fortunes of a Composer (1912) *short ...as Opera Attendee (uncredited)
Diamond Cut Diamond (1912) *short ...as The Office Clerk
Curfew Shall Not Ring Tonight (1912) *short ...as Basil Underwood
His Mother's Son (1912) *short ...
Kaintuck (1912) *short ...as The Artist
Virginius (1912) *short ...as Icilius
The Gamblers (1912) *short ...as Arthur Ingraham
Before the White Man Came (1912) *short ...as Wathuma-the Leopard
A Man's Duty (1912) *short ...as Dick Wilson-Union Soldier
At Cripple Creek (1912) *short ...as Joe Mayfield
Making Good (1912) *short ...as Billy Burns
The Secret Service Man (1912) *short ...as The Secret Service Man
The Indian Raiders (1912) *short ...as Tom
His Only Son (1912) *short ...as Bob Madden
Every Inch a Man (1912) *short ...as Robert Chapman-the Son
Early Days in the West (1912) *short ...as Dan, a Young Pioneer
Hunted Down (1912) *short ... as John Dayton
A Daughter of the Redskins (1912) *short ...as Captain Stark, U.S.A.
The Cowboy Guardians (1912) *short ...as
The Tribal Law (1912) *short ...as Tall Pine aka Jose Seville-Apache Brave (*directed)
An Indian Outcast (1912) *short ...as Wally, a Cowboy
The Hidden Treasure (1912) *short ...as Bill Binks (*directed)
The Sepoy Rebellion (1912) *short ...as Extra (uncredited)


== 1913 ==
Love and the Law (1913)*short ...as Sheriff John Allen (*directed)
Their Masterpiece (1913)*short ...as Jack Sanders
Pirate Gold (1913)*short...
The Rose of Old Mexico (1913)*short ...as Paul Hapgood (*directed)
The Picture of Dorian Gray (1913)*short ...as Dorian Gray
Near to Earth (1913)*short ...as
The Eye of a God (1913)*short ...as Frank Hammond
The Ways of Fate (1913)*short ... as Jim Conway (*directed)
When Jim Returned (1913)*short ...as Jim (*directed)
The Tattooed Arm (1913)*short ...as Ben Hart (*directed)
The Brothers (1913)*short ...as Robert Gregory (*directed)
The Deerslayer (1913)*short ...as Chingachgook
Youth and Jealousy (1913)*short ...as Ben (*directed)
The Kiss (1913)*short ...as Ralph Walters (*directed)
Her Innocent Marriage (1913)*short ...as Will Wayne (*directed)
A Modern Snare (1913)*short ...as Ralph-the New Sheriff (*directed)
On the Border (1913)*short ...as Bill Reeves-the Cowboy (*directed)
When Luck Changes (1913)*short ...as Cal Jim (*directed)
Via Cabaret (1913)*short ...as Harry Reeder (*directed)
The Spirit of the Flag (1913)*short ...as Dr. Reid
Hearts and Horses (1913)*short ...as Bill Walters (*directed)
In Love and War (1913)*short ...as David-the Journalist
Women and War (1913)*short ...as The Boy
The Guerilla Menace (1913)*short ...as Captain Bruce Douglas
Calamity Anne Takes a Trip (1913)*short ...Policeman
Song Bird of the North (1913)*short ...as Fowle-a Mission Worker
Pride of Lonesome (1913)*short ...as Edward Daton (*directed)
The Powder Flash of Death (1913)*short ...as Captain Bruce Douglas
A Foreign Spy (1913)*short ... (*directed)
The Picket Guard (1913)*short ...as Sentry
Mental Suicide (1913)*short ...as Reid-a Contractor
Man's Duty (1913)*short ...Bill, the Selfish One
An Even Exchange (1913)*short ...as Joe
The Animal (1913)*short ...as The Animal
The Harvest of Flame (1913)*short ...as The Inspector (*directed)
The Spark of Manhood (1913)*short ... (*directed)
The Mystery of Yellow Aster Mine (1913)*short ...as Reid-Rosson's Brother
The Gratitude of Wanda (1913)*short ...as Wally (*directed)
The Wall of Money (1913)*short ...as Wallace-McQuarrie's Son
The Heart of a Cracksman (1913)*short ...as Gentleman Crook (*directed)
The Crackman's Reformation (1913)*short ...as Gentleman Crook
The Fires of Fate (1913)*short ...as Wally-the Doctor (*directed)
Cross Purposes (1913)*short ...as Wally (*directed)
Retribution (1913)*short ...as Reid (*directed)
A Cracksman Santa Claus (1913)*short ...as Gentleman Crook
The Lightning Bolt (1913)*short ...as Reid (*directed)
A Hopi Legend (1913)*short ... (*directed)


== 1914 ==
Whoso Diggeth a Pit (1914)*short ...as Wally
The Intruder (1914)*short ...as The Woodsman (*directed)
The Countess Betty's Mine (1914)*short ...as Wallace (*directed)
The Wheel of Life (1914)*short ...as The Prospector (*directed)
Fires of Conscience (1914)*short ...as Ray-The Prospector (*directed)
The Greater Devotion (1914)*short ...as 'Devotion' (*directed)
A Flash in the Dark (1914)*short ...as A Miner (*directed)
Breed o' the Mountains (1914)*short ...as Joe Mayfield (*directed)
Regeneration (1914)*short ...The Artist (*directed)
The Voice of the Viola (1914)*short ...as Wallace (*directed)
The Heart of the Hills (1914)*short ...The Woodsman (*directed)
The Way of a Woman (1914)*short ...as Pierre (*directed)
The Mountaineer (1914)*short ...as Jim-the Mountaineer (*directed)
The Spider and Her Web (1914)*short ...
Cupid Incognito (1914)*short ...as Jack Falkner (*directed)
A Gypsy Romance (1914)*short ...as Jose-King of the Gypsies (*directed)
The Test (1914)*short ...as The Poor Man (*directed,actor)
The Skeleton (1914)*short ...as Jack-the Young Husband (*directed)
The Fruit of Evil (1914)*short ... (*directed)
The Daughter of a Crook (1914)*short ...as Neal
Women and Roses (1914)*short ...as Wallace (*directed)
The Quack (1914)*short ...as Wallace Rosslyn (*directed)
The Siren (1914)*short ...as Dane Northrop (*directed)
The Man Within (1914)*short ...as Wallace Rosslyn (*directed)
Passing of the Beast (1914)*short ...as Jacques-the Woodsman (*directed)
Love's Western Flight (1914)*short ...as Wally-the Ranch Owner (*directed)
A Wife on a Wager (1914)*short ...as Wally Bristow (*directed)
 'Cross the Mexican Line  (1914)*short ... as Lt. Wallace (*directed)
The Den of Thieves (1914)*short ...as Wallace (*directed)
Arms and the Gringo (1914)*short ...as Sullivan
The City Beautiful (1914)*short ...as The Country Boy
Down by the Sounding Sea (1914)*short ...as John Ward-the Man from the Sea
The Avenging Conscience (1914)*short ...as The Doctor(uncredited)
Moonshine Molly (1914)*short ...as Lawson Keene
The Second Mrs. Roebuck (1914)*short ...as Samuel Roebuck
Sierra Jim's Reformation (1914)*short ...as Tim-the Pony Express Rider
The High Grader (1914)*short ...as Dick Raleigh
Down the Hill to Creditville (1914)*short ...as Marcus Down
Her Awakening (1914)*short ...as Bob Turner
Her Doggy (1914)*short ...as The Doctor
For Her Father's Sins (1914)*short ...as Frank Bell
A Mother's Influence (1914)*short ... as Wallace Burton-the Son
Sheriff for an Hour (1914)*short ...as Jim Jones
The Niggard (1914)*short ...as Elmer Kent
The Odalisque (1914)*short ...as Curtiss
The Little Country Mouse (1914)*short ...as Lieutenant Hawkhurst
Another Chance (1914)*short ...Detective Flynn
Over the Ledge (1914)*short ...as Bob
At Dawn (1914)*short ...as The Lieutenant
The Joke on Yellentown (1914)*short ...as
The Exposure (1914)*short ...as The Reporter
Baby's Ride (1914)*short ...as Father


== 1915 ==

The Three Brothers (1915)*short ...as Jean Gaudet/Will
The Craven (1915)*short ...as Bud Walton
The Birth of a Nation ...as Jeff-A Blacksmith
The Lost House (1915)*short ...as Ford
Enoch Arden (1915)*short ...as Phillip Ray
Station Content (1915)*short ...as Jim Manning
A Yankee from the West (1915) ...as Billy Milford aka Hell-in-the Mud
The Chorus Lady (1915) ...as Danny Mallory
Carmen (1915) ...as Don José
Old Heidelberg (1915) ...as Prince Karl Heinrich
The Golden Chance (1915) ...as Roger Manning


== 1916 ==
To Have and to Hold (1916) ...as Captain Ralph Percy
The Love Mask (1916) ...as Dan Derring
Maria Rosa (1916) ...as Andreas
The Selfish Woman (1916) ...as Tom Morley
The House with the Golden Windows (1916) ...as Tom Wells
Intolerance (1916) ...as young man killed in battle(uncredited)
The Yellow Pawn (1916) ...as James Weldon
The Wall of Flame (1916)*short ...Wallace-the Fire Inspector
The Wrong Heart (1916)*short ... (*directed)


== 1917 ==

Joan the Woman (1917) ...as Eric Trent 1431/Eric Trent 1917
The Golden Fetter (1917) ...as James Roger Ralston
The Man Who Saved the Day (1917)*short ...John King (*directed)
Buried Alive (1917)*short (*directed)
The Tell-Tale Arm (1917)*short
The Prison Without Walls (1917) ...as Huntington Babbs
A Warrior's Bride (1917)*short (*directed)
The Penalty of Silence (1917)*short (*directed)
The World Apart (1917) ...as Bob Fulton
Big Timber (1917) ...as Jack Fife
The Squaw Man's Son (1917) ...as Lord Effington, aka Hal
The Hostage (1917) ...as Lieutenant Kemper
The Woman God Forgot (1917) ...as Alvarado
Nan of Music Mountain (1917) ...as Henry de Spain
The Devil-Stone (1917) ...as Guy Sterling


== 1918 ==

Rimrock Jones (1918) ...as Rimrock Jones
The Thing We Love (1918) ...as Rodney Sheridan
The House of Silence (1918) ...as Marcel Levington
Believe Me, Xantippe (1918) ...as George MacFarland
The Firefly of France (1918) ...as Devereux Bayne
Less Than Kin (1918) ...as Hobart Lee/Lewis Vickers
The Source (1918) ...as Van Twiller Yard
His Extra Bit (1918)*short ...
The Man from Funeral Range (1918) ...as Harry Webb
Too Many Millions (1918) ... as Walsingham Van Doren


== 1919 ==

The Dub (1919) ...as John Craig (The 'Dub')
Alias Mike Moran (1919) ...as Larry Young
The Roaring Road (1919) ...as Walter Thomas 'Toodles' Walden
You're Fired (1919) ...as Billy Deering
The Love Burglar (1919) ...as David Strong
The Valley of the Giants (1919) ...as Bryce Cardigan
The Lottery Man (1919) ...as Jack Wright
Hawthorne of the U.S.A. (1919) ...as Anthony Hamilton Hawthorne


== 1920 ==

Double Speed (1920) ...as 'Speed' Carr
Excuse My Dust (1920) ... as 'Toodles' Walden
The Dancin' Fool (1920) ...as Sylvester Tibble
Sick Abed (1920) ...as Reginald Jay
What's Your Hurry? (1920) ...as Dusty Rhoades
Always Audacious (1920) ...as Perry Dayton/ 'Slim' Attucks


== 1921 ==

The Charm School (1921) ... as Austin Bevans
The Love Special (1921) ...Jim Glover
Too Much Speed (1921) ...as 'Dusty' Rhoades
The Hell Diggers (1921) ...as Teddy Darman
The Affairs of Anatol (1921) ...as Anatol Spencer
Forever (1921) ...as Peter Ibbetson
Don't Tell Everything (1921) ...as Cullen Dale


== 1922 ==
Rent Free (1922) ...as Buell Arnister Jr
The World's Champion (1922) ...as William Burroughs
Across the Continent (1922) ...as Jimmy Dent
The Dictator (1922) ...as Brooke Travers
A Trip to Paramountown (1922)*short ...cameo;as himself
Nice People (1922) ...as Captain Billy Wade
The Ghost Breaker (1922) ...as Walter Jarvis, a Ghost Breaker
Clarence (1922) ...as Clarence Smith
Thirty Days (1922) ...as John Floyd
Night Life in Hollywood (1922) ...cameo;as himself
Hollywood (1923) ...cameo;as himself (posthumous release)


== Director ==
Films for which Wallace Reid directs but does not appear in:

Where Destiny Guides (1913)*short
The Latent Spark (1913)*short
The Fugitive (1913)*short
When the Light Fades (1913)*short
Brother Love (1913)*short
The Orphan's Mine (1913)*short
The Renegade's Heart (1913)*short
The Mute Witness (1913)*short
The Homestead Race (1913)*short
Suspended Sentence (1913)*short
Dead Man's Shoes (1913)*short


== References ==


== External links ==
Wallace Reid at IMDb.com