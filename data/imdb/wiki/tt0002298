Law of the West is a computer game for the Commodore 64, the Apple II family and the NES. It was one of Accolade's launch products and is the only game in Accolade's history to be designed by Accolade co-founder Alan Miller.  Graphics were by Mimi Doggett and the music was by Ed Bogas. The game is a kind of graphical adventure game taking place in the American Old West. In Scandinavia the game was published by American Action.


== Gameplay ==
The entirety of the game happens in a small frontier town, and the player takes the role of the town's Sheriff. The scenes are located along the town's main street, with a view from behind the Sheriff's back (the view is so close that only the Sheriff's waist, right arm and gun are visible). Various other characters appear on the main street in front of the Sheriff.
The actual gameplay mostly concerns the Sheriff discussing with the various characters via a selection menu similar to those in contemporary graphical adventures. For each line the other character says, the game offers a selection of four different responses, and the discussion progresses depending on the chosen response. Law of the West marks the first use of this now-common interaction style.
Depending on the outcome of a discussion, the Sheriff may get involved in a gunfight. In this case, the gameplay becomes a pure test of shooting skills, as the Sheriff has to shoot his opponent before getting shot himself. Gunplay is accomplished by pushing up on the joystick to draw the weapon, which causes a joystick-controlled crosshairs to appear on the screen. The various characters would react if the Sheriff drew his gun before they had a chance to say their opening line, usually refusing to speak until the gun was put away.
If the Sheriff gets shot, he blacks out, and the doctor is called to help. Depending on the Sheriff's previous interaction with the doctor and the other characters in the game (and if the doctor's in town), he may either be healed, or left for dead (either by the doctor refusing to help, or the doctor being drunk at the time). The only goal of the game is for the player to make it alive to the end of the day; if the player wishes he can gun down every character he meets without talking to them albeit this will be reflected negatively in the final score.


== Characters ==
Characters interacted with in the game include:
Belle – A female cattle rustler.
Little Willy – A kid with a secret to tell.
The Deputy – Never there to help you, challenges the Sheriff's authority.
The Doctor – Grumpy and cynical, will not heal Sheriff if he is too trigger happy.
The Mexicali Kid – A fugitive from across the border.
Miss April – School teacher. Enjoys spreading gossip.
Miss Rose – The Saloon hostess. Tries to seduce the Sheriff.


== Locations ==
Game takes place in the town of Gold Gulch. There are 4 different locations in this game:

The Saloon
The Wells Fargo stagecoach office
The train station
The bank


== Scoring ==
The game ends when the Sheriff is shot and the Doctor does not heal him, or when the sun sets after 11 scenarios. The player is then presented with the score, which is based on various factors such as how the player dealt with the characters, how many crimes they prevented, and even their success with the ladies.
If the player chose to shoot everybody, they will receive the lowest possible score.


== Reception ==
Zzap!64 found the game to be initially impressive, with striking graphics and a strong atmosphere.  However, it was thought to be too short and limited to have lasting appeal.  It was given an overall rating of 78%.


== References ==


== External links ==
Law of the West at MobyGames
Law of the West at Lemon 64