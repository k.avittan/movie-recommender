Soul Train is an American music-dance television program which aired in syndication from October 2, 1971, to March 27, 2006. In its 35-year history, the show primarily featured performances by R&B, soul, dance/pop, and hip hop artists, although funk, jazz, disco, and gospel artists also appeared.  The series was created by Don Cornelius, who also served as its first host and executive producer.
Production was suspended following the 2005–2006 season, with a rerun package (known as The Best of Soul Train) airing for two years subsequently. As a nod to Soul Train's longevity, the show's opening sequence during later seasons contained a claim that it was the "longest-running first-run, nationally syndicated program in American television history," with over 1,100 episodes produced from the show's debut through the 2005–2006 season. Despite the production hiatus, Soul Train held that superlative until 2016, when Entertainment Tonight surpassed it completing its 35th season. Among non-news programs, Wheel of Fortune surpassed that mark in 2018.


== History ==


=== Chicago origins ===
The origins of Soul Train can be traced to 1965 when WCIU-TV, an upstart UHF station in Chicago, began airing two youth-oriented dance programs: Kiddie-a-Go-Go and Red Hot and Blues. These programs—specifically the latter, which featured a predominantly African Americans group of in-studio dancers—would set the stage for what was to come to the station several years later. Don Cornelius, a news reader and backup disc jockey at Chicago radio station WVON, was hired by WCIU in 1967 as a news and sports reporter. Cornelius also was promoting and emceeing a touring series of concerts featuring local talent (sometimes called "record hops") at Chicago-area high schools, calling his traveling caravan of shows "The Soul Train".  WCIU-TV took notice of Cornelius's outside work and in 1970, allowed him the opportunity to bring his road show to television.
After securing a sponsorship deal with the Chicago-based retailer Sears, Soul Train premiered on WCIU-TV on August 17, 1970, as a live show airing weekday afternoons. Beginning as a low-budget affair, in black and white, the first episode of the program featured Jerry Butler, The Chi-Lites, and the The Emotions as guests. Cornelius was assisted by Clinton Ghent, a local professional dancer who appeared on early episodes before moving behind the scenes as a producer and secondary host.


=== Move to syndication ===

The program's immediate success attracted the attention of another locally based firm—the Johnson Products Company (manufacturers of the Afro Sheen line of hair-care products)—and they later agreed to co-sponsor the program's expansion into broadcast syndication. Cornelius and Soul Train's syndicator targeted 25 markets outside of Chicago to carry the show, but stations in only seven other cities—WAGA-TV, WBRC, WJW (TV), WJBK, KIAH, KTTV, and WKBS-TV—purchased the program, which began airing on a weekly basis on October 2, 1971. By the end of the first season, Soul Train was on in the other eighteen markets. At the time, there were no other commercial television programs being produced by black people for a black audience; the only nationally available show by blacks for blacks at the time was the public television series Soul! When the program moved into syndication, its home base was also shifted to Los Angeles, where it remained for the duration of its run. Soul Train was part of a national trend toward syndicated music-oriented programs targeted at niche audiences; two other network series (Hee Haw for country music, and The Lawrence Welk Show for traditional music) also entered syndication in 1971 and would go on to have long runs.
Though Don Cornelius moved his operations west, a local version of Soul Train continued in Chicago; Cornelius hosted both the local Chicago and Los Angeles–based national programs simultaneously but soon focused his attention solely on the national edition. He continued to oversee production in Chicago, where Clinton Ghent hosted episodes on WCIU-TV until 1976, followed by three years of once-weekly reruns. The syndicated version was picked up in the Chicago market by CBS-owned-and-operated station WBBM-TV at its launch; the program moved to WGN-TV in 1977 and remained there for the rest of its Chicago run.
Don Cornelius hosted every national episode of Soul Train during this era except for one: comedian Richard Pryor guest hosted the final episode of the 1974-75 season.In 1985, Chicago-based Tribune Entertainment (WGN's syndication wing) took over Soul Train's syndication contract; the series would continue distribution through Tribune for the rest of its original run.


=== Later years ===
Don Cornelius ended his run as host at the end of the show's 22nd season in 1993, though he remained the show's main creative force from behind the scenes. The following fall, Soul Train began using guest hosts weekly until comedian Mystro Clark began a two-year stint as permanent host in 1997. Clark was replaced by actor Shemar Moore in 2000. In 2003, Moore was succeeded by actor Dorian Gregory, who hosted through 2006.
Soul Train pulled into its last stop when production of first-run episodes was suspended at the conclusion of the 2005–06 season, the show's 35th. Instead, for two seasons starting in 2006–07, the program aired archived episodes (all from between 1973 and 1988) under the title The Best of Soul Train. 
This was because in later years, Nielsen ratings dropped to below 1.0; most of the stations that aired Soul Train by that point were either list of Fox television affiliates or independent station that would later become list of former WB affiliates or list of former UPN affiliates, and, in the process, some of the stations which had been airing Soul Train on Saturday afternoons started rescheduling the program to overnight time slots. The future of Soul Train was uncertain with the announced closing of Tribune Entertainment in December 2007, which left Don Cornelius Productions to seek a new distributor for the program. Cornelius soon secured a deal with Trifecta Entertainment & Media.


=== Attempted revivals and new ownership ===
When Don Cornelius Productions still owned the program, clips of the show's performances and interviews were kept away from online video sites such as YouTube owing to copyright infringement claims. Cornelius also frowned upon the unauthorized distribution of Soul Train episodes through the sale of third-party VHS or DVD compilations.
In May 2008, Cornelius sold the rights to the Soul Train library to MadVision Entertainment, whose principal partners came from the entertainment and publishing fields. The price and terms of the deal were not disclosed. However, by the start of the 2008–09 television season, the Tribune Broadcasting-owned stations (including national carrier WGN America) that had been the linchpin of the show's syndication efforts dropped the program, and many others followed suit. Soul Train's website acknowledged that the program had ceased distribution on September 22, 2008.
Following the purchase by MadVision, the Soul Train archives were exposed to new forms of distribution. In April 2009, MadVision launched a Soul Train channel on YouTube. Three months later, the company entered into a licensing agreement with Time Life to distribute Soul Train DVD sets. MadVision then came to terms with ViacomCBS-owned BET to relaunch the Soul Train Music Awards for BET's spin-off channel, BET Her, in November 2009. Centric would broadcast archived episodes of the program. Archived episodes can also be seen on Bounce TV.
MadVision sold the rights to Soul Train to a consortium led by basketball player Magic Johnson and backed by private equity firm InterMedia Partners in 2011. The Johnson-InterMedia consortium planned on a potential film project Cornelius had briefly mentioned prior to selling the franchise, as well as producing potential stage adaptations and a cruise. As part of the sale, Johnson's Aspire TV channel also began airing reruns of the series.
Cornelius continued to appear for Soul Train documentaries and ceremonies up until his death by suicide in February 2012. In 2013, the cruise-based revival, called the Soul Train Cruise, began taking place. The cruise is presented by Centric.All rights and trademarks to the Soul Train brand including the show's extensive library, the annual cruise event, and the award shows are under the ownership of ViacomCBS after its ViacomCBS Domestic Media Networks division acquired the franchise in 2016.


=== Influence ===
Some commentators have called Soul Train a "black American Bandstand," another long-running program with which Soul Train shares some similarities. Cornelius acknowledged Bandstand as a model for his program; as the years advanced and Soul Train evolved into a tradition in its own right, he tended to bristle at the Bandstand comparisons.In 1973, Dick Clark, host and producer of Bandstand, launched Soul Unlimited — controversial for its pronounced racial overtures — to compete directly with Soul Train. Cornelius, with help from Jesse Jackson, openly accused Clark of trying to undermine TV's only Black-owned show. Agreeing, ABC canceled it after a few episodes. Clark later agreed to work with Cornelius on a series of network specials featuring R&B and soul artists.Cornelius was relatively conservative in his musical tastes and was admittedly not a fan of the emerging hip hop genre, believing that the genre did not reflect positively on African-American culture (one of his stated goals for the series). Even though Cornelius would feature rap artists on Soul Train frequently during the 1980s, he publicly would admit (to the artists' faces such as Kurtis Blow) that the genre was one that he did not understand; as rap continued to move further toward hardcore hip hop, Cornelius would admit to be frightened by the antics of groups such as Public Enemy. Rosie Perez testified in the 2010 VH1 documentary Soul Train: The Hippest Trip in America that Cornelius also disliked seeing the show's dancers perform sexually suggestive "East Coast" dance moves. Cornelius admittedly had rap artists on the show only because the genre was becoming popular among his African-American audience, though the decision alienated middle-aged, more affluent African Americans like himself. This disconnect (which was openly mocked in an In Living Color sketch where Cornelius and the show were lampooned as extremely old and out of touch) eventually led to Cornelius's stepping down as host in the early 1990s and the show's losing its influence.Questlove, drummer for hip-hop band The Roots and a fan of the program, authored Soul Train: The Music, Dance, and Style of a Generation. ISBN 978-0-0622-8838-7., which was published in 2013.


== Program elements ==
Within the structure of the program, there were two enduring elements. The first was the "Soul Train Scramble Board", where two dancers are given 60 seconds to unscramble a set of letters that form the name of that show's performer or a notable person in African American history. In describing the person's renown, the host concluded their description with the phrase "...whose name you should know". Cornelius openly admitted after the series ended its run that the game was usually set up so everybody won in an effort not to cause embarrassment for the show or African Americans in general.


=== Soul Train line ===
There was also the popular "Soul Train Line" (a variant of the 1950s fad then known as The Stroll), in which all the dancers form two lines with a space in the middle for dancers to strut down and dance in consecutive order. Originally, this consisted of a couple—with men on one side and women on the other. In later years, men and women had their own individual lineups. Sometimes, new dance styles or moves were featured or introduced by particular dancers. In addition, there was an in-studio group of dancers who danced along to the music as it was being performed. Rosie Perez, Damita Jo Freeman, Darnell Williams, Cheryl Song, Louie "Ski" Carr, Alfie Lewis, Pat Davis ("Madam Butterfly"), Alise Mekhail, Andrea N. Miles, Carmen Electra, Nick Cannon, MC Hammer, Jermaine Stewart, Heather Hunter, Fred Berry, Laurieann Gibson, Pebbles, and NFL legend Walter Payton were among those who got noticed dancing on the program over the years. Two former dancers, Jody Watley and Jeffrey Daniel, enjoyed years of success as members of the R&B group Shalamar after they were chosen by Soul Train talent booker/record promoter Dick Griffey and Cornelius to replace the group's original session singers in 1978.


=== Guest stars ===
Each musical guest usually performed twice on each program; after their first number, they were joined by the program host onstage for a brief interview.  From time to time, stand-up comedians, such as Tom Dreesen (whom Don Cornelius knew from his time in Chicago) and Franklyn Ajaye (known in the 1970s for being a star of the hit movie Car Wash), would be featured on the program to perform a brief comedy routine.
Soul Train was also known for two popular catchphrases, referring to itself as the "Hippest trip in America" at the beginning of the show and closing the program with "...and you can bet your last money, it's all gonna be a stone gas, honey. I'm Don Cornelius, and as always in parting, we wish you love, peace...and SOUL!"


== UK version ==

In 1985, Cornelius gave permission for a version of the show in the United Kingdom.  The UK version, hosted by former Soul Train dancer and member of Shalamar Jeffrey Daniel, was titled 620 Soul Train and ran for one series.


== Spinoffs ==

In 1987, Soul Train launched the Soul Train Music Awards, which honors the top performances in R&B, hip hop, and gospel music (and, in its earlier years, jazz music) from the previous year.
Soul Train then produced the short-lived Soul Train Comedy Awards in 1993, which discontinued that same year.Soul Train later created two additional annual specials: The Soul Train Lady of Soul Awards, first airing in 1995, celebrated top achievements by female performers; and the Soul Train Christmas Starfest, which premiered in 1998, featured holiday music performed by a variety of R&B and gospel artists. Award categories for the Soul Train Lady of Soul Awards presented to female recipients included:
R&B/Soul Album of the Year, Solo
Best R&B/Soul Album of the Year, Group or Duo
R&B/Soul Song of the Year
Best R&B/Soul Single, Solo
Best R&B/Soul Single, Group or Duo
Best R&B/Soul or Rap New Artist
Best Jazz Album
Best Gospel Album
Best R&B/Soul or Rap Music VideoSpecial awards given were The Aretha Franklin Award for Entertainer of the Year, and The Lena Horne Award for Outstanding Career Achievements.The Lady of Soul Awards and Christmas Starfest programs last aired in 2005. In April 2008, Don Cornelius announced that year's Soul Train Music Awards ceremony had been canceled. Cornelius cited 2007–08 Writers Guild of America strike by the Writers Guild of America as one of the reasons, though a main factor may have been the uncertainty surrounding Soul Train's future. Cornelius also announced that a motion picture based on the program was in development. Subsequent owners of the franchise have followed their own agenda for the program, which included a revival of the Soul Train Music Awards in 2009.


== Theme music ==
Soul Train used various original and current music for theme songs during its run, including

1971–1973: "Soul Train (Hot Potato)", by King Curtis (Curtis Ousley) and later redone by The Rimshots as "Soul Train, Parts 1 & 2". [The original 1962 version, which was used on the show, was recorded nine years before the show was named "Hot Potatoes (Piping Hot)"]
1973–1975: "TSOP (The Sound of Philadelphia)", composed by Gamble and Huff and recorded by MFSB with vocals by The Three Degrees. Released as a single, this song became a pop and R&B radio hit in 1974 and the show's best-known theme.
1975–1976: "Soul Train '75", by The Soul Train Gang, which was later released as a single for the newly formed SOLAR Records
1976–1978: "Soul Train '76 (Get On Board)", by The Soul Train Gang
1978–1980: "Soul Train Theme '79", produced by the Hollywood Disco Jazz Band with vocals by the Waters
1980–1983: "Up On Soul Train", first by the Waters and later by The Whispers, whose version appears in their 1980 album Imagination.
1983–1987: "Soul Train's a Comin'", by R&B artist O'Bryan
1987–1993: "TSOP '87", a remake of the original "TSOP (The Sound of Philadelphia)," composed and produced by George Duke
1989–1993: "TSOP '89", a remixed version of "TSOP '87", by George Duke
1993–1999: "Soul Train '93" (Know You Like to Dance)", by Naughty by Nature with a saxophone solo by Everette Harp
2000–2006: "TSOP 2000", with rap vocals by Samson and music by Dr. Freeze, and again featuring an Everette Harp saxophone solo. However, a portion of "Know You Like to Dance" was still used in the show's second-half opening segment during this period, though in earlier episodes, a portion of "TSOP 2000" was played.


== See also ==
List of Soul Train episodes
List of people who appeared on Soul Train
SOLAR Records
Soul Train Music Awards
American Soul, a 2019 TV series on BET based on Soul Train
American Bandstand
Showtime at the Apollo
The Midnight Special
Don Kirshner's Rock Concert
Electric Circus
The Party Machine with Nia Peeples
Sherman's Showcase, a parody of the series and Cornelius


== References ==


== External links ==
Official website
Soul Train DVD from Time-Life
Soul Train on IMDb
Soul Train at TV.com