"______, thy name is ______" is a snowclone used to indicate the completeness with which something or somebody (indicated by the second part) embodies a particular quality (indicated by the first part), usually a negative one.


== History ==
In most instances, the usage is an allusion to the Shakespearean play Hamlet (I, ii, 146).  In this work, the title character is chastised by his uncle (and new stepfather), Claudius, for grieving his father so much, calling it unmanly.  In his resultant soliloquy, Hamlet denounces his mother's swift remarriage with the statement, "Frailty, thy name is woman."  He thus describes all of womankind as frail and weak in character.  The phrase is recognized as one of the "memorable expressions"  from the play to become "proverbial".In the book Idiom Structure in English by Adam Makkai, the author asserts that the phrase is included among English idioms that are expressed in a "standard format" and whose usage "signals to the hearer that he is using an authority in underscoring his own opinion."  Researchers Andrew Littlejohn and Sandhya Rao Mehta acknowledged that the famous quote rendered not only a discursive use, but a constructional one as well, noting that "the structure itself can be used a salient, but neutral equation formula...'noun thy name is noun.'"


== Examples ==


=== Law ===
Supreme Court Justice Antonin Scalia, dissenting from the Court's decision in King v. Burwell, upholding the Patient Protection and Affordable Care Act, repeatedly used the construction to criticize the Court's majority opinion, stating: "Understatement, thy name is an opinion on the Affordable Care Act!"; "Impossible possibility, thy name is an opinion on the Affordable Care Act!"; and "Contrivance, thy name is an opinion on the Affordable Care Act!" (25 June 2015)


=== Quoted ===
Amos Bronson Alcott famously said of William Ellery Channing in 1871, "Whim, thy name is Channing."  He was referring to Channing's Transcendentalist poetry style.


=== Literature ===
The poet Anne Sexton titled a poem "Divorce, Thy Name Is Woman".
Borrowing directly from Hamlet, Edmond Dantès (disguised as Abbé Busoni) utters the phrase "Frailty, thy name is woman!" in The Count of Monte Cristo after learning that his fiancée, Mercédès, has married his rival Fernand.
In the James Joyce novel Ulysses, Leopold Bloom utters the phrase, "Fraility, thy name is marriage," in response to a quip.
In the Clive Cussler novel "Plague Ship", Juan Cabrillo quips, "Ego, thy name is Gomez," to Corporation helicopter pilot Gomez Adams after he brags about his perfect takeoff.
In Stephen King short story Herman Wouk Is Still Alive, Ollie responds to his friend, "Ingratitude, thy name is woman".


=== Music ===
The Half Man Half Biscuit song "Whiteness, Thy Name Is Meltonian", from the 1993 album This Leaden Pall, refers to a brand of None-More-White shoe polish.
The lyrics "Frailty, thy name is weakness. Vengeance, thy name is pain" appear in the Dark Tranquillity song "...Of Melancholy Burning".


=== Television ===
Samantha on Bewitched says to Darrin, "Vanity, thy name is human" (1.8; 1964).
In Taxi (1982), Jim, on learning that Zena is to marry another man (whilst believing that she is still with Louis): "Oh, perfidy! Thy name is woman!"
Bart Simpson says, "Comedy, thy name is Krusty" in The Simpsons episode "Krusty Gets Busted" (1990).
Danny Tanner on Full House says "Loneliness, thy name is Danny" (in the episode "A Date with Fate", 1994).
Professor Farnsworth on Futurama says in his recorded message, "Oh vanity, thy name is Professor Farnsworth" (2.15; 1999).
Referring to his wife, Ray Barone on Everybody Loves Raymond (in episode 19, "The Canister", of the show's fifth season, 2001), says, "Devil, thy name is woman!"
Gil Grissom on CSI:Crime Scene Investigation says, "Vanity, thy name is Hodges" (6.13; 2005), referring to another CSI, David Hodges, after walking in on him trying to color some of his greying hair with a sharpie while looking intensely at his reflection in a piece of lab equipment.
Dr. Perry Cox on Scrubs says to Dr. Elliot Reid, "Hypocrisy, thy name is you" (in the episode "My Inconvenient Truth", 2007).
Winn Schott on Supergirl says to James Olsen, "Jealousy, thy name is Olsen" (1.18; 2016), when Olsen denied he had a look after seeing how Kara talks to Barry Allen.


== References ==


=== Footnotes ===


=== Works cited ===
Buell, Lawrence (1973). "Ellery Channing: The Major Phase of a Minor Poet". Literary Transcendentalism: Style and Vision in the American Renaissance. Ithaca; London: Cornell University Press. pp. 239–262. ISBN 978-1-5017-0766-7. JSTOR 10.7591/j.ctt1g69x7r.CS1 maint: ref=harv (link)
Makkai, Adam (1 January 1972).  Walter de Gruyter (ed.). Idiom Structure in English. The Hague, The Netherlands: Mouton & Co. N.V.
Lederer, Richard (11 May 2010). The Miracle of Language. New York, New York: Simon and Schuster. ISBN 978-0671028114.
Littlejohn, Andrew; Rao Mehta, Sandhya (4 December 2012). Language Studies: Stretching the Boundaries. Newcastle upon Tyne, United Kingdom: Cambridge Scholars Publishing. ISBN 978-1443839723.


== External links ==
"Frailty, thy name is woman", from The New Dictionary of Cultural Literacy