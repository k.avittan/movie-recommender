This Perfect Day  is a science fiction novel by American writer Ira Levin, about a technocratic dystopia. Levin won a Prometheus Award in 1992 for this novel.  This Perfect Day is one of two Levin novels yet to be adapted to film (the other being Son of Rosemary, the sequel to Rosemary's Baby).


== Backstory ==
The world is managed by a central computer called UniComp (referred as just "Uni" in speech), which has been programmed to keep every single human on the surface of the earth in check. People are continually drugged by means of monthly treatments (delivered via transdermal spray or jet injector) so that they will remain satisfied and co-operative "Family members." They are told where to live, when to eat, whom to marry, when to reproduce, and the job for which they will be trained. Everyone is assigned a counselor, who acts somewhat like a mentor, confessor, and parole agent; violations against "brothers" and "sisters" by themselves and others are expected to be reported at a weekly confession.
Everyone wears a permanent identifying bracelet that interfaces with access points, which act as scanners. Uni uses them to tell "Family members" where they are allowed to go and what they are allowed to do. Around the age of 62, every person is humanely euthanized by Uni with an overdose of the treatment liquids; almost anything in them is poisonous if an excess dose is given. Most people think that old men die of natural causes. Since some die at 61 or 63, no one is too suspicious of the regularity. The few who happen to be resistant to the drugs or purposely change their behavior to avoid strong doses of some of the drugs in the monthly treatment are dealt with by the programmers of UniComp. The long-lived men and women, in their underground hideaway, are the real but invisible world government. They live in absolute luxury and choose their own members through a form of meritocracy. In part, people who choose by evasion and modifying their own behavior to leave the main Family are subtly redirected to "nature preserves" of imperfect life on islands. They, however, have been put in place by the programmers as a place to isolate trouble-making Family members. The top minds in the outcasts are further manipulated into trying to overthrow the "Programmers" and the ones who nearly succeeded are forced to join the programmers to "help them maintain the equilibrium" in the "perfect" world of UniComp and "The Family."
Despite the name, the "programmers" are not allowed to program Uni since Wei reserves the privilege solely to himself. Instead, they do administrative work.
Even the basic facts of nature are subject to the programmers' will: men do not grow facial hair, and it rains only at night. Dampers even control the movement of tectonic plates. Reference is made in the story to permanent settlements on Mars, Venus, and the Moon; outposts on Saturn's moon Titan and on Mercury; and even to interstellar space exploration. The outposts have their own equivalents of UniComp.
The full rhyme is sung by children bouncing a ball (similar to a clapping game):

Christ, Marx, Wood and Wei,
Led us to this perfect day.
Marx, Wood, Wei and Christ,
All but Wei were sacrificed.
Wood, Wei, Christ and Marx,
Gave us lovely schools and parks.
Wei, Christ, Marx and Wood,
Made us humble, made us good.Wei Li Chun is the person who started the Unification. Only the programmers and their attendants know that he remains alive as the head of the programmers by extending his lifespan by having his head transplanted onto successive youthful bodies.
Bob Wood is mentioned throughout the novel but never discussed in detail.  A painting is mentioned depicting Wood presenting the Unification Treaty.  In one conversation in which the protagonist discusses his discovery that people once had varying lifespans, one character comments that controlling people's lifespans is the ultimate realization of Wei and Wood's thinking. The poem also mentions Jesus Christ, a central figure of Christianity, and presents the historical Karl Marx, an ideologue of communism. Both pillars of the official state ideology are depicted on its coat of arms, with a cross and sickle. Sacrificed is a poetic synonym for dead.
Uniformity is the defining feature. There is only one language, and all ethnic groups have been eugenically merged into one race, "The Family." It is so genetically uniform that no transplants are rejected. There are only four personal names for men (Bob, Jesus, Karl, and Li) and four for women (Anna, Mary, Peace, and Yin). Instead of surnames, individuals are distinguished by a nine-character alphanumeric code, their "nameber" (a neologism from "name" and "number"), such as WL35S7497. Everyone eats "totalcakes," drinks "cokes," wears exactly the same thing, and is satisfied every day.
It is assumed that Uni and the restriction on names were created not very long ago since Papa Jan was one of the employees who constructed it, and he claims to have remembered when there were many names. However, to make the members genetically uniform, many generations are needed.


== Plot summary ==
Li RM35M4419, nicknamed "Chip" (as in "chip off the old block") by his nonconformist grandfather Jan, is a typical child Member, but through a mistake in genetic programming, he has one green eye. Through his grandfather's encouragement, he learns how to play a game of "wanting things," including imagining what career he might pick if he had the choice. Chip is told by his adviser that "deciding" and "picking" are manifestations of selfishness, and he tries to forget his dreams.
As Chip grows up and begins his career, he is mostly a good citizen but commits minor subversive acts, such as procuring art materials for another "nonconformist" member who was denied them.  His occasional oddities attract the attention of a secret group of Members of nonconformists, like Chip.  There, he meets King, a Medicenter chief who obtains members' records for potential future recruitment to the group; King's beautiful girlfriend, Lilac, a strong-willed and inquisitive woman with unusually-dark skin; and Snowflake, a rare albino member. They teach Chip how to get his treatments reduced so that he can feel more and stronger emotions. Chip begins an affair with Snowflake but is really attracted to Lilac.
Chip and Lilac begin to search through old museum maps and soon discover islands around the world that have disappeared from their modern map. They begin to wonder if perhaps other "incurable" members have escaped to the islands. King tells them that the idea is nonsense, but Chip soon learns that King has already interacted with some "incurables" and that they are indeed real. Before he can tell Lilac, Chip's ruse is discovered by his adviser.  He and all the other members of the group are captured and treated back into docility (except King, who takes his own life before he can be captured).
Some years later, Chip's regular treatment is delayed by an earthquake. In the meanwhile, he begins to "wake up" again and remembers Lilac and the islands. He is able to shield his arm from the treatment nozzle and becomes fully awake for the first time. He locates Lilac again and kidnaps her. At first, she fights him, but as she too becomes more "awake," she remembers the islands and comes willingly. In the process, she is raped by Chip. Finding a convenient abandoned boat on the beach, they head for the nearest island of incurables, Majorca. There, they learn that UniComp, as a last resort, has planted failsafes that eventually lead all incurables to the islands, where they will be trapped forever from the treated population. After living "free" on Majorca, Chip and Lilac eventually marry and have a child together.
Chip conceives of a plan to destroy the computer, UniComp, by blowing up its refrigeration system. He recruits other incurables to join him, and they make their way to the mainland. Just as they reach UniComp, one of the incurables, an agent of the programmers, betrays his partners and leads the rest of the group at gunpoint to a secret luxurious underground city beneath UniComp, where they are met by Wei, one of the original planners of the Unification. Wei and the other "programmers" live in UniComp have arranged the test so that the most daring and resourceful incurables will make their way to UniComp. There, they too will live in luxury as programmers.
After joining the programmers and a few tentative attempts to confide in someone, Chip shares his misgivings with no one, lies low, and dedicates himself to the task of gaining Wei's confidence.  For example, to deceive Wei, Chip consents to the replacement of his green eye with a brown one even though it involves giving up a cherished part of his identity.
However, nine months later, a new group of incurables arrives, and Chip leaves the welcome party with the intention of using the newcomers' explosives to blow up the master computer. A physical struggle with Wei, who has the body of a young athlete, results in him bring shot. Just before he is killed, Wei reveals his true motive in creating the dystopia: "there's joy in having it, in controlling, in being the only one." Chip knew all along that it was power hunger, not altruism, that drove Wei to chicanery and murder. On his way up from the underground city towards sunlight, Chip tells an angry programmer:  "'There’s joy in having it': those were [Wei's] last words. Everything else was rationalization. And self-deception."
The book ends with Chip riding a helicopter toward Majorca, where his wife, son, financial sponsors, and friends are hopefully waiting for him. For the first time in his life, he sees raindrops in daytime, nature's affirmation that the era of slavery and total control is finally over.


== Reception ==
Cherry Wilder, discussing This Perfect Day, stated, "The ideology of the "utopia" is weak, but the quality of the writing and the level of invention are high." David Pringle gave This Perfect Day two stars out of four and described the book as "a slickly-written update of Huxley's Brave New World, lacking in originality but very professionally put together."


== See also ==
Alphaville (film)
Brave New World
Equilibrium (film)
Gattaca
Never Let Me Go (novel)
Nineteen Eighty-Four
The Glass Fortress (2016 film)
The Matrix
THX 1138
V for Vendetta (film)
We (novel)
Wir (film)
World government


== Editions ==
Hardcover: Random House (June 1970) ISBN 0-394-44858-8
Paperback: Fawcett Books (November 1976) ISBN 0-449-22638-7
Paperback: Fawcett Books (March 1971) ISBN 978-0-449-22638-4
Paperback: Pegasus Books (November 2010) ISBN 978-1-60598-129-1
Audio Cassette: Book of the Road Audio (December 1985) ISBN 0-931969-32-8
(in French): Un bonheur insoutenable, J'ai lu, N°434, 1972 ISBN 2-290-33285-2
(in German): Die sanften Ungeheuer, Hoffmann und Campe 1972, ISBN 3-455-04403-4
(in Italian): Questo giorno perfetto, Garzanti 1970
(in Dutch): De dag der dagen, Bruna 1979, ISBN 90 229 1835 1
(in Swedish): En vacker dag
(in Danish): Fagre nye elektronverden, 1989
(in Romanian): O zi perfecta, Vremea 1995


== References ==


== External links ==
This Perfect Day title listing at the Internet Speculative Fiction Database
Riggenbach, Jeff (December 2, 2010). "Ira Levin and This Perfect Day". Mises Daily. Ludwig von Mises Institute.
By-category breakdown: [1]