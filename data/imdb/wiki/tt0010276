Human Desire is a 1954 American film noir drama film directed by Fritz Lang and starring Glenn Ford, Gloria Grahame and Broderick Crawford. It is loosely based on Émile Zola's 1890 novel La Bête humaine. The story had been filmed twice before: La Bête humaine (1938), directed by Jean Renoir, and Die Bestie im Menschen, starring Ilka Grüning (1920).


== Plot ==
Returning Korean War vet Jeff Warren is a train engineer, driving streamliners hauling passenger trains for the fictional Central National railroad. Warren worked alongside Alec Simmons and was a boarder in his home before going off to fight for three years. Jeff has moved back in and is resuming his duties as an engineer. Alec's daughter Ellen is a very attractive young woman who is smitten with Jeff.
Carl Buckley is a gruff, hard-drinking assistant yard supervisor married to the younger, and more vibrant, Vicki. After Carl is fired for talking back to his boss, Carl begs Vicki to visit John Owens, a man she has known since her childhood and an important customer of the railroad. Carl hopes Owens' influence could help him reclaim his job, but when Vicki is gone for hours, Carl surmises that she has been unfaithful. After a violent argument during which he elicits the truth from her, Carl forces Vicki to write a short letter to Owens, setting up a meeting with him later that night in his sleeping car drawing room. Owens is taking the train to Chicago and Carl and Vicki are returning home. Carl and Vicki barge into Owens' room and Carl kills Owen with a knife that he had whittled. Carl takes Owens' wallet and pocket watch to make the murder appear to be the result of a robbery, and he also takes the letter that Vicki had written. Carl tells Vicki that he is keeping the letter to prevent her from going to the police. After the murder, Carl sees Jeff on the train and persuades Vicki to distract him seductively so that he can pass unnoticed.
At the murder inquest, Jeff and other train passengers are called as witnesses. Jeff denies having seen anyone suspicious. Vicki and Jeff soon resume their relationship. She lies about the night of the murder, saying that she had visited Owens' compartment for a liaison but found him dead. Jeff questions why she did not seem distressed when they met on the train. Vicki explains that she is frightened of Carl's temper and shows Jeff marks on her body where Carl had hit her.
Ellen hopes that Jeff will invite her to an upcoming dance, but she knows that he is involved with Vicki. Jeff tells Vicki that he wants to marry her and that she should leave Carl. She tells Jeff the truth about Owens' murder and the letter, but Jeff remains determined to keep Vicki.
Carl has become a drunk and has again lost his job. Vicki tells Jeff that Carl is selling the house and forcing her to leave town with him. She cannot find the letter and suspects that Carl must keep it with him. She suggests that she and Jeff will have to part forever but hints that things would be easier with Carl out of the way.
Jeff, clutching a large monkey wrench, follows a drunk Carl in a railyard before a passing train blocks the view of the characters. Later, Jeff tells Vicki that he could not murder Carl and accuses her of setting him up so that he would kill Carl. She tells Jeff that she loves him and that if he loved her he would have killed for her. Jeff hands her the letter, which he has taken from Carl's pocket.
Vicki is now free to leave Carl and gets on the next train. Carl confronts Vicki, imploring her not to leave him. He offers her the letter but she tells him that he does not have it. Carl accuses her of running away with Jeff, which she denies, but she admits that she is in love with Jeff, though he had rejected her. Carl strangles Vicki to death.
Meanwhile, Jeff has regained some happiness and, as he operates the train, is thinking about the dance that he will attend with Ellen.


== Cast ==
Glenn Ford as Jeff Warren
Gloria Grahame as Vicki Buckley
Broderick Crawford as Carl Buckley
Edgar Buchanan as Alec Simmons
Kathleen Case as Ellen Simmons
Peggy Maley as Jean
Diane DeLaire as Vera Simmons
Grandon Rhodes as John Owens


== Production ==
This film was largely shot in the vicinity of El Reno, Oklahoma. It used the facilities of what was at the time the Rock Island Railroad (now Union Pacific), though some of the moving background shots show East Coast scenes such as the Pulaski Skyway and the famous "Trenton Makes — The World Takes" bridge over the Delaware River.


== Reception ==
Critic Dave Kehr wrote of the film, "Gloria Grahame, at her brassiest, pleads with Glenn Ford to do away with her slob of a husband, Broderick Crawford. ... A gripping melodrama, marred only by Ford's inability to register an appropriate sense of doom." Variety wrote that Lang "... goes overboard in his effort to create mood." Bosley Crowther of The New York Times wrote, "[T]here isn't a single character in it for whom it builds up the slightest sympathy—and there isn't a great deal else in it for which you're likely to have the least regard."


== Preservation ==
The Academy Film Archive preserved Human Desire in 1997.


== References ==


== External links ==
Human Desire on IMDb
Human Desire at AllMovie
Human Desire at the TCM Movie Database
Human Desire at the American Film Institute Catalog
Human Desire film trailer on YouTube