Lumber Jack-Rabbit is a 1953 3-D Warner Bros. Looney Tunes cartoon short directed by Chuck Jones and written by Michael Maltese The cartoon was released on September 26, 1953, and stars Bugs Bunny.It was notable as the first Warner Bros. cartoon short produced in 3-D. It premiered with the Warner Bros. 3-D feature The Moonlighter and the 3-D Lippert short, Bandit Island.


== Plot ==
A narrator recalls the character Paul Bunyan and his exploits, and states that many people still question the giant's existence. The narrator then advises the viewer to ask "a certain rabbit" whether the giant is real.
Just then Bugs Bunny comes walking by, toting a bindle and singing "Jimmy Crack Corn". Bugs comments on the "funny-looking trees" he passes, oblivious to the fact that they are abnormally large asparagus, and to the fact that he has entered a rather large vegetable garden. He then finds an oversized carrot and lies next to it for a nap, seemingly taking the carrot to be a boulder.
Bugs quickly sits up and states that he smells carrots. Turning his attention to the supposed boulder against which he's propped, he scratches some of it onto his finger and tastes it. He ecstatically comes to the conclusion that he has discovered a "carrot mine"; and he starts to dig frantically through the carrot.
On the other end of the garden, Paul Bunyan leaves his log cabin, accompanied by his dog, Smidgen (a gag in itself, as the word is a measurement for a small amount). Paul checks the time by pulling a grandfather clock from his shirt picket. He then instructs the dog to watch over the vegetables until he returns that night, and leaves by stepping over the mountains.
Back in the garden, Bugs has managed to tunnel through several carrots and to lay down tracks for the mining cart he is using to dump the excess carrot chunks over a cliff. Smidgen, drawn to the sound of Bugs' singing, pulls up the carrot that Bugs is in with his teeth. Bugs comes out, stops at the sight of what he assumes is a large billboard (Smidgen's dog license), and wonders where it came from. Suddenly, he realizes that he is suspended high in the air, and frantically climbs up the carrot onto the edge of Smidgen's nose. Bugs then sees the large bloodshot eyes staring at him, and realizes what he's up against. However, he becomes enraged, stating to the audience: "I'll be scared later. Right now, I'm too mad."

Bugs climbs up, approaches Smidgen's eye, and balls his fists to fight. Smidgen takes his fingers to flick Bugs off his bridge, but Bugs jumps up, causing Smidgen to flick his own eye. Bugs then walks through the dog's head, out the left ear to the ground below, and runs down the garden with Smidgen not far behind. Bugs then happens upon a wormhole; he dives in, forcing its previous occupant, a worm, out. Smidgen sticks his nose over the hole and starts to sniff. Bugs has a feather, which he uses to tickle Smidgen's nose, causing Smidgen to give a hearty sneeze that rockets Bugs into Paul's cabin and inside a moose call horn.
Smidgen runs into the cabin, grabs the horn and gives several blows that alert a nearby moose. The normal-sized moose happily runs after what it believes is another moose, only to find in waiting an enormous dog. The moose instantly flees, yelping like a dog. Smidgen gives the horn another blow, sending Bugs flying into the barrel of a revolver. Smidgen fires it, sending the bullet Bugs is riding on into a nearby apple in a fruit basket. Smidgen grabs the apple and takes a large bite, leaving Bugs' lower half exposed. Smidgen then eats the entire apple, grabs a toothpick to pick his teeth, and walks away, seemingly feeling assured that Bugs has been eaten and taken care of. As Smidgen picks his teeth, Bugs comes out unharmed on the top of the toothpick. Bugs then hops up, grabs Smidgen's ear, and wraps the ear around Smidgen's head covering his eyes.
Bugs dives into the hair on the back of Smidgen's neck. From there, Bugs proceeds to scratch the surface of Smidgen's skin, causing the dog to writhe in ecstasy. This gives Bugs the opportunity to run down Smidgen's leg and leave the cabin. Smidgen runs out after him.
Bugs, feeling as though he's safe, stops to catch his breath, not knowing Smidgen is right behind him. Before he knows it, Bugs is licked by the dog's enormous tongue, which lifts him off the ground each time. Bugs tries to run away, but stops at the sight of something. He calls Smidgen's attention to a giant redwood tree; and the dog runs off towards it.


== Availability ==
"Lumber-Jack Rabbit" is available on the Looney Tunes Superstars DVD. However, it was cropped to widescreen.


== 3-Dimensional Gimmick ==
The only obvious concession that Lumber Jack-Rabbit made to the 3-D format was at the very beginning of the cartoon, where the zooming "WB" shield overshoots its mark and nearly crashes into the screen, before pulling back to its correct position. This is complemented by a slight variation on "The Merry-Go-Round Broke Down" theme, wherein the opening twanging sound is more exaggerated and reverberant. While this effect can be attributed only to this one short, the shield overshooting its mark has become a common motif in tributes and reboots to the Looney Tunes franchise, including in the opening sequence to the 2011 sitcom The Looney Tunes Show.
The closing sequence is modified as well, with the "That's all Folks!" script fading in rather than being written out.


== References ==


== External links ==
Lumber Jack-Rabbit on IMDb