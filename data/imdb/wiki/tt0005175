Daughter of the Nile (Ni luo he nyu er) (I kori tou Neilou) is a 1987 film by Taiwanese filmmaker Hou Hsiao-hsien.


== Background ==
The film's title is a reference to a character in a manga called Crest of the Royal Family who is hailed as Daughter of the Nile.  The film is a study of the life of young people in contemporary Taipei urban life, focusing on the marginalised figure of a woman and centred on a fast-food server's hapless crush on a gigolo.  The introductory sequence of the film suggests a parallel between the difficulties faced by people in the film (Taiwan's urban youth, transitioning from a classical civilization into a changing world) and the mythic struggles of characters in the Egyptian Book of the Dead.
It features Taiwan pop singer Lin Yang, Jack Kao (Kao Jai) as her brother, and Tianlu Li in the role of the grandfather.  Li became a central part of Hou's major films, and Kao starred in several of them.


== Synopsis ==
Lin Hsiao-yang (Lin Yang), tries to keep her family together while working as a waitress at Kentucky Fried Chicken and going to night school. Her mother and older brother are dead. Her father (Fu Sheng Tsui) works out of town. It is up to Lin Hsiao-yang to take care of her pre-teen sister, who has already begun to steal, and a brother (Jack Kao) who is a burglar and gang member.


== Cast ==
Lin Yang as Lin Hsiao-yang
Jack Kao as Lin Hsiao-fang, the brother
Fan Yang as Ah-sang
Tianlu Li as Grandfather
Fu Sheng Tsui as Father
Xin Shufen as Xiao-fen, Shao Fang's fiance
Yu An-shun as Hsiao-yang's Classmate
Wu Nien-jen as TutorAdditional castHuang Chiung-yao
Chen Chien-wen
Yang Tzu-tei
Lin Chu
Chen Shu-fang
Tsai Tsan-te
Tian Weiwei
You Jingru
Lei Guowei
Zue Zhizheng
Zhu Youcheng


== Critical response ==
In his 2008 in-depth analysis of Daughter of the Nile, Michael Joshua Rowin of Reverse Shot wrote that Daughter is one of Hou's most accessible films, and that although the film never found theatrical distribution in the United States and never received a home video release, its foreshadowing of the themes Hou would later use in Millennium Mambo,  Hou's first film to be distributed in the United States, make Daughter ripe for rediscovery, summarizing "Daughter's themes and immediate imagery would be the future of Hou."


== Screenings and reception ==
The film was originally released in October 1987 at the Turin International Film Festival of Young Cinema in Italy, where it won a Special Jury Prize in the International Feature Film Competition for Hou Hsiao-hsien. 
When it screened in January 1988 at the AFI Fest, The Washington Post wrote, "Hou Hsiao-hsien has the slickness that gives Daughter of the Nile the most East-West crossover appeal.  In September 1988 it screened at both the Toronto Festival of Festivals and the New York Film Festival.  After the NYFF screening, Vincent Canby of The New York Times wrote the film "... is not about alienation as much as it is an example of it. It is an artifact from a revolution taking place elsewhere".  When it aired at the Chicago International Film Festival in October, 1988, Lloyd Sachs of the Chicago Sun-Times wrote "slow and grudgingly revealing, Taiwanese director Hou Hsiao-hsien's "Daughter of the Nile" does not lend itself to easy description".In October 1999 the film was screened as part of a Hou Hsiao-hsien retrospective by New York's Anthology Film Archives.  In October 2000 it was screened in a Taiwanese film retrospective at both the National Gallery of Art and the Freer Gallery.In April 2002 in screened at the Buenos Aires International Festival of Independent Cinema in Argentina, and in 2005 it screened at the Thessaloniki International Film Festival in Greece.
In December 2006 it screened as part of a Hou Hsiao-hsien retrospective at the Canadian National Film Repository.Daughter of the Nile will have a dual format (Blu-ray and DVD) home video release in May 2017.


== Awards ==
Daughter of the Nile won the special jury prize at the 1987 Turin International Festival of Young Cinema, and entered into the Directors' Fortnight at Cannes Film Festival.


== Further reading ==
Literary culture in Taiwan: martial law to market law by Sung-sheng Chang ISBN 0-231-13234-4
Senses of Cinema, "Hou Hsiou-hsien's Urban Female Youth Trilogy", by Daniel Kasman
New Chinese cinemas: forms, identities, politics, by Nick Browne ISBN 0-521-44877-8
Envisioning Taiwan: fiction, cinema, and the nation in the cultural imaginary, by June Chun Yip ISBN 0-8223-3367-8


== References ==


== External links ==
Daughter of the Nile on IMDb