Under Great White Northern Lights is a 2009 documentary film about The White Stripes' summer 2007 tour across Canada directed by Emmett Malloy.  It contains live concert and off-stage footage. The film's accompanying album is a collection of various recordings from throughout the tour. The documentary was released on DVD and Blu-ray, and the album was released on CD as well as 180-gram vinyl LP. A special edition box set was also available. The CD, LP, DVD, BD, and box set were all released on March 16, 2010 in Canada, with other dates worldwide.


== Film ==


=== Development and production ===

Under Great White Northern Lights documents The White Stripes' summer 2007 tour across Canada in support of the album Icky Thump, in which they performed in every province. Jack White conceived the idea of touring Canada after learning that Scottish relatives on his father's side had lived for a few generations in Nova Scotia before relocating to Detroit to work in the car factories.  The notion was a personal ambition, and he did not expect the tour to earn any money.  Jack says that The White Stripes initially had not planned to film the tour, but decided to do so shortly before leaving at the suggestion of their manager.The film was directed by a friend of the duo, Emmett Malloy.  It contains both live concert and off-stage footage. The bulk of the filming took place in the Yukon, Northwest Territories, Nunavut and Nova Scotia.  Interspersed between concert footage are clips of them performing surprise gigs in public, including in a bowling alley, on a boat, and on a bus. In an outdoor stage in Newfoundland and Labrador, they played a single note, fulfilling their mission to play every province.  They also met, played music, and ate raw caribou with a group of Alaskan native elders.  Their 10th anniversary occurred during the tour on the day of their show in Glace Bay, Nova Scotia, which they celebrated with a personal party.  The film ends with footage of them after the concert later that evening; Jack plays "White Moon" for Meg on piano, which leaves her crying silently in his arms. When asked about the ending, the director recounted, "There was nothing sad about that moment, but it was an intense moment. These two have been through a lot — every deep relationship, whether it be brother and sister or husband and wife."


=== Promotion and release ===
The White Stripes' Under Great White Northern Lights made appearances at film festivals internationally. It premiered at the Toronto International Film Festival on September 18, 2009. Jack and Meg appeared at the premiere and made a short speech before the movie started about their love of Canada and why they chose to debut their movie in Toronto; they watched it together for the first time that night. The film had its US premiere at the South by Southwest Festival in March 2010.The band provided advanced-screening kits that allowed fans to hold local premiere in their own spaces. The actual DVD was released on March 16, 2010.


== Album ==
The live album was recorded during the band's tour across Canada. It is available as both a CD and 180-gram vinyl LP. A limited edition box set containing the film was released on March 16, 2010. The box set includes the Under Great White Northern Lights documentary, the 16-track live album CD, and the same 16-track live album on 180-gram vinyl. Exclusive components to the box set includes a live 7" single featuring "Icky Thump" on one side and "The Wheels on the Bus" (records live in Winnipeg on July 2, 2007) on the other, a 208-page hardcover book of photographs from the tour, one of six possible silkscreen prints, and a DVD of the band's 10th anniversary show in Glace Bay, Nova Scotia, entitled Under Nova Scotian Lights.


=== Track listing ===
All songs by Jack White, except where noted.

"Let's Shake Hands" – 3:13
"Black Math" – 3:06
"Little Ghost" – 2:23
"Blue Orchid" – 2:55
"The Union Forever" (White, Pepe Guízar) – 4:26
"Ball and Biscuit" (incorporating portions of "I Believe I'll Dust My Broom" and "Phonograph Blues" both by Robert Johnson) – 3:08
"Icky Thump" – 4:13
"I'm Slowly Turning Into You" – 5:33
"Jolene" (Dolly Parton) – 3:55
"300 M.P.H. Torrential Outpour Blues" – 4:52
"We Are Going to Be Friends" – 2:24
"I Just Don't Know What to Do with Myself" (Burt Bacharach, Hal David) – 3:11
"Prickly Thorn, But Sweetly Worn" – 3:23
"Fell in Love with a Girl" – 2:26
"When I Hear My Name" – 2:40
"Seven Nation Army" – 7:32


=== Personnel ===
Jack White – vocals, guitars, keyboards, synthesizer
Meg White – drums, percussion
Hector MacIsaac – bagpipes


=== Chart performance ===


=== Certifications ===


== Critical reception ==
Under Great White Northern Lights received largely positive reviews from critics.  Metacritic gave the live release a 78/100 ("Generally favorable"), based on 16 critical reviews.  Rolling Stone gave the DVD release 4 out of 5 stars.  Entertainment Weekly gave the live release an A, saying that "this concert CD/DVD does a great job of highlighting both sides of the Stripes' carefully controlled public persona," and that "if UGWNL is their last hurrah, it's one hell of a goodbye."  Slant Magazine gave the album 3.5 out of five stars and The A.V. Club gave the DVD release a B+. Robert Christgau gave it a one-star honorable mention (), saying, "The present-day guitar god refuses to die." The box set also won a Grammy for Best Boxed or Special Limited Edition Package.
Several outlets commented on how the film's behind-the-scenes footage highlighted the difference between Jack's extroverted, "boisterous" personality and Meg's quiet nature—with her statements even being put in subtitles.  Rolling Stone noted that Meg's shyness "gives the film a subtext", considering that, shortly after the conclusion of the Canadian leg, the band cancelled the rest of their tour dates due to her "acute anxiety," and never played another concert.Under Great White Northern Lights enjoyed commercial success.  It reached number one on Billboard's Top music video sales.


== References ==


== Further reading ==
de Wilde, Autumn (2010), The White Stripes: Under Great White Northern Lights. Chronicle Books. ISBN 0811872238, 9780811872232


== External links ==
The White Stripes' official site
Under Great White Northern Lights on IMDb