Late Phases is a 2014 American-Mexican co-production horror drama film by director Adrián García Bogliano and his first feature film in the English language. The film had its world premiere on March 9, 2014, at South by Southwest and stars Nick Damici as a blind war veteran that becomes the victim of a werewolf attack.


== Synopsis ==
Will moves his father, Ambrose McKinley, a fiercely independent, yet blind Vietnam War veteran and his seeing eye dog German Shepherd into a retirement community at the edge of a forest. Willful and adamant that he can live on his own, Ambrose and Will are clearly not on the best of terms. He meets three neighbor women, Gloria, Anna, and Victoria, who while at first admire Ambrose, are quickly put off by his attitude. He meets his neighbor Delores, who shares the duplex with him. That night, during a full moon, something breaks into Delores' kitchen and brutally slashes her to death. Ambrose hears the commotion and is also attacked by a massive werewolf. His dog comes to his defense as Ambrose struggles to find his gun. He deters the creature, but his dog is mortally wounded. The next day, the police find him cradling his dog, and despite the destruction and his claims of the attack, it is shrugged off as an animal attack.
Suspecting a werewolf is responsible for the murders and mysterious disappearances of other park residents who wander into the woods, Ambrose mounts a defense. He buys an immaculate tombstone for his dog and begins to slowly dig a grave for him. Meeting with the local congregation, Ambrose, who could hear a rasping in the werewolf's breathing, monitors the people around him for similar issues. He encounters Gloria's husband, who is encased in an iron lung, but Gloria suspects Ambrose of trying to kill her husband when he accidentally disconnects the machine; she throws him out. His suspicions fall on the church's pastor, Father Roger Smith, who has developed a wheeze after years of chain smoking. He suggests Ambrose visit the church, prompting him to take a local shuttle from his house, where he meets another suspect, James Griffin, who is a friend of Smith's and has a similar wheeze from asthma. Uncomfortable with Ambrose's habits and lifestyle, the three women convince James to privately transport Ambrose. Ambrose goes to a gun shop and convinces the owner to create silver bullets. Ambrose learns that James had also previously purchased silver ammunition. The tombstone arrives, and Ambrose puts it in his back yard, still digging the grave. Complaints of this and the smell of his dog's decomposition gets Ambrose observed by the police, who become concerned for his well being. Despite this, Ambrose convinces them not to interfere.
On the day of the next full moon, Ambrose finally finishes the grave and lays his dog to rest. Meanwhile, James meticulously begins to visit with the congregation, violently biting Gloria, Anne, Victoria, and Victoria's husband Bennet, cursing them to become werewolves. Ambrose visits Gloria, but finds her missing and her husband viciously decapitated; he runs home to arm himself. James confesses to Father Smith that he is a werewolf, but Smith does not believe him until he transforms. When he flees, the wolf viciously mauls him. Back at his home, Ambrose calls and leaves his son a voicemail, revealing that he had lost his deceased wife's ring, and he had not sold it, as Will suspected. He apologizes and says his goodbyes, as he believes he will not survive the night. Using his keen hearing, Ambrose dispatches the werewolves descending on his property with his weapons and a series of booby traps. He kills the four new werewolves but is bitten by James. In the struggle, they battle into Ambrose's back yard, where Ambrose finally kills James and calmly sits down on his porch. Will, having heard his father's final words, rushes to his house. He sees the bodies of werewolves, half-turned back into their human forms, before discovering Ambrose dead, having purposefully overdosed on his medication before the battle had even begun to ensure that he will not live as a werewolf.
Later, Will and his wife stand at his father's military funeral at dusk, where Will accepts the tri-folded flag and gives it to his wife.  He then takes out his father's rifle, takes aim at the rising moon, and fires.


== Cast ==
Nick Damici as Ambrose McKinley
Ethan Embry as Will McKinley
Lance Guest as James Griffin
Tina Louise as Clarissa
Rutanya Alda as Gloria B.
Caitlin O'Heaney as Emma
Erin Cummings as Anne
Tom Noonan as Father Roger
Larry Fessenden as O'Brien
Al Sapienza as Bennet
Bernardo Cubria as Officer Lang
Karen Lynn Gorney as Delores
Karron Graves as Victoria Kaye
Haythem Noor as Nelson
Kareem Savinon as Edward


== Production ==
Bogliano first announced his intention to film Late Phases in 2012, as he had been impressed with the script that Stolze had written. Nick Damici was confirmed to be leading the film in February 2013, and filming began in Hudson Valley, New York in June of the same year. Damici had some difficulty portraying a blind man, as he stated that you "can't just blindfold yourself to learn because it doesn’t work that way. Even when you’re blindfolded you’re seeing more than a blind person sees. I just had to do it looking peripherally and trusting Adrian that it came off okay." The film's biggest challenge came from the werewolf transformation scene, as Bogliano noted that "[not even] the producers knew if it was going to work until we finally finished."  Fessenden, who produced and had a cameo, brought on special effects artist Robert Kurtzman.  Fessenden said Kurtzman agreed to do the film because he has a soft spot for werewolves.


== Reception ==
The film received mixed reviews. Shock Till You Drop gave Late Phases a lukewarm review, commenting that while it was "occasionally thrilling" it was "obviously going for the Bubba Ho-Tep vibe" but didn't have the "ominpresent quirkiness and energy Bubba Ho-Tep carried." Twitch Film panned the film overall, as they felt that it was "a concept that looks like it absolutely should have worked on paper, but ends up becoming a cautionary tale about what happens when you put too much stock in your wolf rather than your sheep."  Patrick Cooper of Bloody Disgusting gave the film five skulls out of five, stating that the film is "a tale of hardcore werewolf violence, a tangible father/son relationship, redemption, and a whole lotta heart. It’s funny, brash, and exciting, but knows when to pull back and let the emotion sink in. Simply put, it’s a masterpiece of the werewolf genre because of what it accomplishes on top of the scares, which is deliver a truly emotional, heartfelt story of a father and son."


== Release ==
The film was selected to screen in April 2014 at the Stanley Film Festival. The home release was March 10, 2015, on video on demand, digital download and on Blu-ray and DVD by Dark Sky Films. The film is currently available to be streamed on Netflix as of January 2018.


== References ==


== External links ==
Late Phases on IMDb
Late Phases at Rotten Tomatoes