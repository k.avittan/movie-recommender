An Officer and a Gentleman is a 1982 American romantic drama film starring Richard Gere, Debra Winger, and Louis Gossett Jr., who won the Academy Award for Best Supporting Actor for the film, making him the first African American male to do so. It tells the story of Zack Mayo (Gere), a United States Navy Aviation Officer Candidate who is beginning his training at Aviation Officer Candidate School. While Zack meets his first true girlfriend during his training, a young "townie" named Paula (Winger), he also comes into conflict with the hard-driving Marine Corps Gunnery Sergeant Emil Foley (Gossett Jr.) training his class.
The film was written by Douglas Day Stewart and directed by Taylor Hackford. Its title is an old expression from the Royal Navy and later from the U.S. Uniform Code of Military Justice's charge of "conduct unbecoming an Officer and a Gentleman" (from 1860). The film was commercially released in the U.S. on August 13, 1982. It was well received by critics, with a number calling it the best film of 1982. It also was a financial success, grossing $130 million against a $6 million budget.


== Plot ==
The film begins with recent college graduate Zack Mayo preparing to report for Aviation Officer Candidate School (AOCS). He moved to the Philippines as a child to live with his father Byron, a Navy Senior Chief Boatswain's Mate, after his mother's suicide. Initially, his father is reluctant to take him in; Byron was at sea most of the time, and wasn't good at being a father even while in port. Zack had to beg not to be sent back, before Byron let him stay. 
Having grown up as a Navy brat at Subic Bay, Zack is now determined, despite his father's disapproval, to become a Navy pilot; he jokes about how one day Byron will have to salute him.
Upon arrival at AOCS, Zack and his fellow OCs are shocked by the draconian treatment they receive from their head drill instructor, Marine Gunnery Sergeant Emil Foley. Foley makes it clear that AOCS is set up to wash out as many cadets as possible, so that only the most talented will be commissioned as ensigns in the U.S. Navy's $1 million flight training program. Foley also warns the male candidates about the "Puget Sound Debs" (local girls who angle for marriage to a Naval Aviator in order to escape their dull lives and who will not stop at feigning pregnancy, or at actually getting pregnant, to "trap" a potential husband). Zack and fellow cadet Sid Worley meet two local factory workers, Paula Pokrifki and Lynette Pomeroy, at a Navy Ball. Zack begins a relationship with Paula, and Sid with Lynette.
Foley runs the program mercilessly. Cadet Topper Daniels DORs (Drops On Request) after nearly drowning in the Dilbert Dunker. Foley accuses Zack of lacking proper motivation (in other words, signing up for the wrong reasons), and of not caring about anyone other than himself. When Zack's side business of renting out pre-shined boots and belt buckles is discovered, Foley hazes him for an entire weekend, in order to get Zack to DOR. When Zack doesn't, Foley threatens to simply eject him. Zack breaks down, screaming that he has no other options and nothing to fall back on. Satisfied that Zack has come to a needed self-realization, Foley lets him prove himself as a team-player. Zack starts by dividing his entire stash of pre-shined buckles and boots among the other cadets, particularly his and Sid's other roommate, Lionel Perryman. 
The following weekend, Zack has an awkward meal with Paula and her family. He learns that her actual father was an OC, like himself, who refused to marry her mother, after which her mother entered a loveless marriage with the man who raised Paula. Back at AOCS, Zack passes up a chance to break the all-time obstacle course-record; instead, he coaches fellow cadet Casey Seeger to a successful climb over a 12-foot-high wall (3.7 m).
Zack attends a dinner with Sid and his parents. He learns that Sid has a long-time girlfriend back home, who he is planning to marry once he receives his commission. Meanwhile, Lynette drops hints to Sid that she might be pregnant with his child. After having a severe anxiety attack during a high-altitude simulation in a pressure chamber, Sid DORs without saying goodbye. He goes to Lynette's house to propose marriage, which delights her, until she discovers that he is no longer in the running for Naval Aviation. Her ruse having worked too well, Lynette reveals her true colors, confirming that she was never pregnant, that she wants an officer for a husband. Rejected, Sid checks into a motel and hangs himself. Finding his friend dead, Zack resolves to DOR himself. However, with less than a week before graduation, Foley is not about to let him go that easily. He engages Zack in an unofficial "combato" match; Zack seems to have the upper hand, until Foley, also the base's martial arts instructor, drops him hard with a groin shot. An injured Foley tells Zack to do whatever he sees fit, but adds that he'll look for Zack at graduation.
Zack stays on and is commissioned into the Navy with the remainder of his class. Following naval tradition, he receives his first salute from Foley in exchange for a US silver dollar. While tradition calls for the drill instructor to place the coin in his left belt-pocket, Foley places the coin in his right pocket, acknowledging Zack as a special candidate. Zack thanks the Gunnery Sergeant for not giving up on him, like his real father did. Foley is touched by this, in spite of himself. While leaving the base, Zack, now Ensign Mayo, briefly looks on as Foley initiates another newly-arrived OC class, just as he did with Zack's class thirteen weeks earlier. Zack then seeks out Paula at her factory. She beams upon seeing that he has completed the program and come back for her. Zack sweeps Paula up into his arms and carries her out of the factory. Her co-workers, including Lynette, applaud and congratulate them.


== Cast ==
(in credits order)


== Production ==


=== Locations ===
The film was shot in late 1981 on the Olympic Peninsula of Washington, at Port Townsend and Fort Worden.  The U.S. Navy did not permit filming at NAS Pensacola in the  Florida panhandle, the site of the actual Aviation Officer Candidate School in 1981.  Deactivated U.S. Army base Fort Worden stood in for the location of the school, an actual Naval Air Station in the Puget Sound area, NAS Whidbey Island.  However, that installation, which is still an operating air station today, was and is a "fleet" base for operational combat aircraft and squadrons under the cognizance of Naval Air Force Pacific, not a Naval Air Training Command installation.
A motel room in Port Townsend, The Tides Inn on Water Street (48.1105°N 122.765°W﻿ / 48.1105; -122.765), was used for the film. Today, there is a plaque outside the room commemorating this (although the room has been extensively refurbished in the interim). Some early scenes of the movie were filmed in Bremerton, with ships of the Puget Sound Naval Shipyard in the background.
The "Dilbert Dunker" scenes were filmed in the swimming pool at what is now Mountain View Elementary School (Port Townsend Jr High School during filming).  According to the director's commentary on the DVD, the dunking machine was constructed specifically for the film and was an exact duplicate of the actual one used by the Navy.  As of 2010, Mountain View Elementary is closed and is now home to the Mountain View Commons, which holds the police station, food bank and the YMCA, the latter of which holds the pool.
The filming location of Paula Pokrifiki's house was 1003 Tremont in Port Townsend.  As of 2009, the house was shrouded by a large hedge, and the front porch had been remodeled.  The neighboring homes and landscape look identical to their appearance in the film, including the 'crooked oak tree' across the street from the Pokrifiki home.  This oak tree is visible in the scene near the end of the film in which Richard Gere returns to the home to request Paula's help in finding his friend Sid.  In the film, the plot has Paula leaving on a ferry ride away from the naval base.  In reality, Paula's home is located approximately 8 blocks from Fort Worden.
Lynette Pomeroy's house was located on Mill Road, just west of the main entrance of the Port Townsend Paper Corp. mill. The house no longer exists, but the concrete driveway pad is still visible.
The interior of the USO building at Fort Worden State Park was used for the reception scene near the beginning of the film.

The concrete structure used during the famous Richard Gere line "I got nowhere else to go!" is the Battery Kinzie located at Fort Worden State Park.  The scene was filmed on the southwest corner of the upper level of the battery.  The 'obstacle course' was constructed specifically for the film and was located in the grassy areas just south and southeast of Battery Kinzie.
The decompression chamber was one of the only sets constructed for the film and as of 2013, it is still intact in the basement of building number 225 of the Fort Worden State Park. It can be seen through the windows of the building's basement.
Building 204 of Fort Worden State Park was used as the dormitory and its porch was used for the film's closing 'silver dollar' scene.
The blimp hangar used for the famous fight scene between Louis Gossett Jr. and Richard Gere is located at Fort Worden State Park and as of 2013 is still intact, but has been converted into a 1200-seat performing arts center called the McCurdy Pavilion.
The filming location for the exterior of 'TJ's Restaurant' is located at the Point Hudson marina in Port Townsend. The space is now occupied by a company that makes sails. The fictional "TJ's" is an homage to the Trader Jon's bar in Pensacola, Florida, as a naval aviator hangout until it closed later in November 2003. For years, it was traditional for graduating Aviation Officer Candidate School classes to celebrate their commissioning at "Trader's."


=== Casting ===
Originally, folk music singer and occasional actor John Denver was signed to play Zack Mayo. But a casting process eventually involved Jeff Bridges, Harry Hamlin, Christopher Reeve, John Travolta, and Richard Gere. Gere eventually beat all the other actors for the part. John Travolta had turned down the role, as he did with American Gigolo (another Richard Gere hit).The role of Paula was originally given to Sigourney Weaver, then to Anjelica Huston and later to Jennifer Jason Leigh, who dropped out to do the film Fast Times at Ridgemont High instead.  Eventually, Debra Winger replaced Leigh for the role of Paula. Rebecca De Mornay, Meg Ryan, and Geena Davis auditioned for the role of Paula.
In spite of the strong on-screen chemistry between Gere and Winger, the actors didn't get along during filming.  Publicly, she called him a "brick wall" while he admitted there was "tension" between them. Thirty years later, Gere was complimentary towards Winger when he said that she was much more open to the camera than he was, and he appreciated the fact that she presented him with an award at the Rome Film Festival.R. Lee Ermey was originally the main cast for Gunnery Sgt. Emil Foley due to his time of being an actual drill instructor for the United States Marine Corps at Marine Corps Recruit Depot San Diego in the 1960s. However, Taylor Hackford instead cast Louis Gossett Jr. and had Ermey coach him for his role as the film's technical advisor. It was there where the "steers and queers" comment from Gossett's character in the 1982 movie came from, which was later used for Ermey's role in the 1987 film Full Metal Jacket.Hackford kept Gossett Jr. in separate living quarters from other actors during production so Gossett could intimidate them more during his scenes as drill instructor. In addition to R. Lee Ermey, Gossett was advised by Gunnery Sergeant Buck Welscher.


=== Props ===
Richard Gere rides a 750cc T140E Triumph Bonneville. In the United Kingdom, Paramount linked with Triumph Motorcycles (Meriden) Ltd on a mutual promotion campaign. Triumph's then-chairman, John Rosamond, in his book Save The Triumph Bonneville! (Veloce 2009), states it was agreed cinemas showing the film would be promoted at their local Triumph dealer, and T140E Triumph Bonnevilles supplied by the dealer would be displayed in the cinema's foyers.


=== Ending ===
Richard Gere balked at shooting the ending of the film, in which Zack arrives at Paula's factory wearing his naval dress whites and carries her off the factory floor. Gere thought the ending would not work because it was too sentimental. Director Taylor Hackford agreed with Gere until, during a rehearsal, the extras playing the workers began to cheer and cry. When Gere saw the scene later, with a portion of the score (that was used to write "Up Where We Belong") played at the right tempo, he said it gave him chills. Gere is now convinced Hackford made the right decision. Screenwriter Michael Hauge, in his book Writing Screenplays That Sell, echoed this opinion: "I don't believe that those who criticized this Cinderella-style ending were paying very close attention to who exactly is rescuing whom."


== Release ==
Two versions of the film exist. The original, an uncensored R-rated cut and an edited-for-broadcast television cut (which first aired on NBC in 1986) are nearly identical. The main difference is that the nudity and a majority of the foul language are edited out when the film airs on regular television.  However, the group marching song near the beginning of the film and Mayo's solo marching song are not voiceover edits; they are reshoots of those scenes for television.  Also, the sex scene between Mayo and Paula is cut in half, and the scene where Mayo finds Sid's naked body hanging in the shower is also edited.


=== Home media ===
The film has been available on various formats, first on VHS and also DVD. It was first released on DVD in 2000 with two extra features, audio commentary and film trailer. It was released as a collectors edition in 2007 with new bonus material. The film debuted on Blu-ray in the U.S. by Warner Bros. and UK by Paramount Pictures in 2013, however the same bonus features ported from the 2007 DVD are only on the U.S. release. It was re-released in 2017 by Paramount Pictures.


== Reception ==


=== Box office ===
An Officer and a Gentleman was an enormous box office success and went on to become the third-highest-grossing film of 1982, after E.T.: The Extra Terrestrial and Tootsie. It grossed $3,304,679 in its opening weekend and $129,795,554 overall at the domestic box office. It sold an estimated 44 million tickets in the US.


=== Critical response ===
An Officer and a Gentleman was well received by critics and is widely considered one of the best films of 1982. The film holds a 79% "Fresh" rating on the review aggregate website Rotten Tomatoes, based on 28 reviews, with the consensus: "Old-fashioned without sacrificing its characters to simplicity, An Officer and a Gentleman successfully walks the fine line between sweeping romance and melodrama". It received rave reviews from critics, most notably from Roger Ebert, who gave it four stars. Ebert described An Officer and a Gentleman as "a wonderful movie precisely because it's so willing to deal with matters of the heart...it takes chances, takes the time to know and develop its characters, and by the time this movie's wonderful last scene comes along, we know exactly what's happening, and why, and it makes us very happy."Rex Reed gave a glowing review where he commented: "This movie will make you feel ten feet tall!" The British film critic Mark Kermode, an admirer of Taylor Hackford observed, "It's a much tougher film than people remember it being; it's not a romantic movie, it's actually a movie about blue-collar, down-trodden people."
The film is recognized by American Film Institute in these lists:

2002: AFI's 100 Years...100 Passions—#29
2004: AFI's 100 Years...100 Songs:
"Up Where We Belong"—#75
2006: AFI's 100 Years...100 Cheers—#68


=== Awards and nominations ===
Louis Gossett Jr. became the first African-American actor to win the Academy Award for Best Supporting Actor and the fourth African-American Oscar winner overall (after Hattie McDaniel, Sidney Poitier and Isaac Hayes).
Producer Don Simpson complained about the song "Up Where We Belong", "The song is no good. It isn't a hit," and unsuccessfully demanded it be cut from the film. It later became the number one song on the Billboard charts.


== Soundtrack ==


=== Track Listing (Original Record) ===


=== Charts ===


== Adaptations ==
The Takarazuka Revue adapted the movie as a musical in 2010 in Japan (Takarazuka Grand Theater; Tokyo Takarazuka Theater). The production was performed by Star Troupe and the cast included Reon Yuzuki as Zack Mayo, Nene Yumesaki as Paula Pokrifki and Kaname Ouki as Gunnery Sergeant Emil Foley.
A stage musical, with book by Douglas Day Stewart and Sharleen Cooper Cohen and songs by Ken Hirsch and Robin Lerner, directed by Simon Phillips, opened on May 18, 2012 at the Lyric Theatre in Sydney, Australia. The production received mixed reviews and closed after six weeks.


== See also ==
Conduct unbecoming an officer and a gentleman


== References ==


== External links ==
Louis Gossett Jr. Wins Supporting Actor in An Officer and a Gentleman: 1983 Oscars on YouTube
An Officer and a Gentleman on IMDb
An Officer and a Gentleman at the TCM Movie Database
An Officer and a Gentleman at AllMovie
An Officer and a Gentleman at Rotten Tomatoes