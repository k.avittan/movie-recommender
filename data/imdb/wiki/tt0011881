The Wonderful Chance (also The Thug and His Wonderful Chance) is a 1920 American silent crime drama film produced by Lewis Selznick and released by Select Pictures. This picture stars Eugene O'Brien in a dual role and was directed by George Archainbaud. While this film survives today in several archives, it is best known for featuring Rudolph Valentino in a villain role rather
than the hero. In the 1960s scenes from the film were used in the documentary The Legend of Rudolph Valentino (1961) narrated by Graeme Ferguson.


== Plot ==
As described in a film magazine, recently released convict 'Swagger' Barlow (O'Brien) is mistaken for Lord Birmingham (O'Brien) and is feted and dined, while the true nobleman is held by a scheming band of crooks. He falls in love with Peggy (Mansfield), the daughter of his host Parker Winton (Cook). Through the actions of Barlow, Lord Birmingham is released. Peggy, after explanations, agrees to wait for Barlow to "come back."


== Cast ==
Eugene O'Brien as Lord Birmingham / 'Swagger' Barlow
Martha Mansfield as Peggy Winton
Tom Blake as 'Red' Dugan
Rudolph Valentino as Joe Klinsby
Joseph Flanagan as Haggerty (credited as Joe Flanagan)
Warren Cook as Parker Winton


== Production ==
Henry Cronjager's use of the "double exposure" method to film an actor on screen in two different roles at the same time, was one of the first uses of this method. This occurs when Eugene O'Brien, in the guise of "Swagger" Barlow, interrogates himself in the persona of Lord Birmingham. Unlike the more common, and easier, method of using a split screen, the use of double exposure allows the actor to appear on the same side of the screen in both roles, in this case allowing Barlow to circle Birmingham.


== Preservation ==
Copies of the film are in the George Eastman House Motion Picture Collection and Museum of Modern Art film archive, and it has been released on DVD.


== References ==


== External links ==
The Wonderful Chance on IMDb
The Wonderful Chance synopsis at AllMovie
The Wonderful Chance is available for free download at Internet Archive