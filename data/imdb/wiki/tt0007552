What Will People Say (Norwegian: Hva vil folk si) is a 2017 internationally co-produced drama film directed and written by Iram Haq. The film's scenes set in Pakistan were all shot in India, mostly in Rajasthan.The film had its world premiere at the 42nd Toronto International Film Festival on 9 September 2017. It was selected as the Norwegian entry for the Best Foreign Language Film at the 91st Academy Awards, but it was not nominated.


== Plot ==
The film is inspired by director Iram Haq's own life. 16-year-old Nisha lives with her parents in Norway. She is living a double life. She loves playing basketball with her Norwegian friends, parties, drinks, and has a Norwegian boyfriend, Daniel. However, she belongs to a traditional Pakistani family and behaves like a docile ideal daughter at home, as her Western nature clashes with their more reserved one.
One night, Nisha's father Mirza catches Daniel in Nisha's room and angrily begins beating him up. Their neighbors call the police and Nisha is taken to a child welfare agency. The counselor tells her that she has done nothing wrong but Mirza insists that she should marry Daniel, assuming that they had sex, which is not true. The Pakistani community in Norway tell Mirza to do something about Nisha or she will become unruly and rebellious. Mirza gets scared about their reputation becoming ruined because of her actions and decides to send her away.
They fly to Pakistan, where Nisha is taken to Mirza's sister's home. Mirza tries to make amends with Nisha but fails. She is left at her aunt's house, where she is taught cooking, praying, and doing household chores. She tries to contact her friend in Norway for help by sneaking out to a cyber cafe but is caught by her aunt and beaten badly. They also burn her passport, leaving her with no other choice but to adjust to life there. Her days are spent going to the market with her aunt, praying, and cooking. She also starts paying attention in school and tries to fly a kite, symbolizing her desire to be free. 
Nisha and her cousin Amir, the son of her aunt, start to like each other. They grow closer and start kissing one night. They are caught by policemen, who make Nisha strip and take pictures of her naked whilst insulting her. The next morning, they take the two home and blackmail her aunt with the incriminating pictures. Nisha's aunt tells her father that Nisha seduced Amir to have sex with her. Nisha tries to get them to hear the truth but to no avail. 
Mirza arrives in Pakistan and takes Nisha back home to Norway. On the way, he tries to force her to jump off a cliff, yelling that she has ruined his life, but she can't do it, begging her father for forgiveness, and both break down. Back home, Nisha's parents watch her closely. When the child welfare agency learn that she is back, her mother insists she lie about her situation. Her parents arrange her marriage to a Pakistani man who lives in Canada so that people will stop talking. Mirza is upset when her future in-laws say that Nisha won't continue her studies after marriage and will not work. Shattered, Nisha climbs out her window to run away. As she looks back, she makes eye contact with her father, who watches her from the window with teary eyes. He does nothing to stop her, watching her as she flees.


== Cast ==
Maria Mozhdah - Nisha
Adil Hussain - Mirza
Ekavali Khanna - Mother Najma
Rohit Suresh Saraf - Amir
Ali Arfan - Asif
Sheeba Chaddha - Aunt
Jannat Zubair Rahmani - Salima
Lalit Parimoo - Uncle
Farukh Jaffer - Granny


== Reception ==
On review aggregator website Rotten Tomatoes, the film holds an approval rating of 81%, based on 21 reviews, and an average rating of 7/10. Metacritic gives the film a weighted average rating of 69 out of 100, based on 10 critics, indicating "generally favorable reviews".


== See also ==
List of submissions to the 91st Academy Awards for Best Foreign Language Film
List of Norwegian submissions for the Academy Award for Best Foreign Language Film


== References ==


== External links ==
What Will People Say on IMDb