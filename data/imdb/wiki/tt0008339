Kipo and the Age of Wonderbeasts is an American animated television series created by Radford Sechrist, adapted from his 2015 webcomic Kipo. The series is produced by DreamWorks Animation Television with animation outsourced to South Korean studio Mir.
The young adult animated series follows a girl called Kipo Oak, who is searching for her father after being forced to flee from her burrow, and must explore the post-apocalyptic surface world ruled by mutated animals to find him. Along the way she befriends human survivors Wolf and Benson, and the mutant animals Dave and Mandu.
The series has been critically acclaimed for its design, characterization, music, worldbuilding, voice acting, and diversity. The series is particularly notable for its representation of LGBT and characters of color.The show's three seasons, each ten episodes long, were released in 2020. Season 1 was released on January 14, season 2 on June 12, and season 3 on October 12.


== Synopsis ==
Kipo Oak, a thirteen-year-old girl, is searching for her father after being forced to flee from her underground city. To do so, she travels through an overgrown post-apocalyptic urban wasteland called "Las Vistas", ruled by sapient mutant animals ("mutes"), together with her new friends Wolf, Mandu, Benson and Dave. Sechrist compared the series to The Wizard of Oz, "but instead of ruby slippers [Kipo] has Converse on".In the first season, Kipo is separated from her father Lio after their burrow is attacked by a "mega monkey", a colossal, mutated spider monkey. Traversing through Las Vistas, she meets and befriends Wolf, a cold and hardened girl who has been surviving on her own after her adoptive wolf family turned on her, the upbeat Benson and his insect companion Dave, and the pig Mandu. Together, they set out to find the burrow Lio and the rest of Kipo's community fled to after their first one was destroyed. As the five journey along they make many mute allies and enemies, including the tyrant Scarlemagne, who intends to create a human army with his mind-controlling pheromones. Kipo also starts to experience strange physical mutations that give her mute-like abilities.
In the second season, Kipo seeks to rescue her people after they are captured by Scarlemagne, and also discover the origin of her mute powers. Kipo learns she was experimented with mute DNA by her parents before her birth, turning her into a half-human, half-jaguar hybrid. She also learns about Scarlemagne's tragic past and his ties to her family, and that her mother Song, previously thought to be deceased, is actually the mega monkey who destroyed her burrow, mutated as a side effect from her pregnancy with Kipo and mind-controlled by the manipulative Dr. Emilia. Kipo starts to lose control of her abilities, putting her at risk of transforming into a mega jaguar permanently, but she is able to regain control with the help of her friends and family. She defeats Scarlemagne and frees Song from her mind control, but meanwhile Dr. Emilia has turned most of Kipo's people over to her cause of reverting mutes back into normal animals so human civilization can rise again.
In the third season, Kipo creates the "Human Mute Ultimate Friendship Alliance" to thwart Dr. Emilia, but struggles to make humans and mutes work together. Emilia manages to create an anti-mute cure with Kipo's DNA, and it is with this cure that Kipo turns her mother human again. Ultimately, Kipo succeeds in making peace between mutes and humans. With her plans foiled and unwilling to accept change, Emilia turns herself into a mega mute to kill Kipo and her friends, but she is defeated with Scarlemagne's help, who sacrifices himself to save Kipo. Five years later, Kipo happily lives on the surface where humans and mutes finally co-exist in harmony.


== Development ==
The series was first announced at the Annecy International Animated Film Festival in June 2019. It is based on Sechrist's 2015 webcomic Kipo.Kipo and the Age of Wonderbeasts was created by Radford Sechrist, previously a storyboard artist for Dan Vs. and later director on the Voltron: Legendary Defender. The series has five writers in addition to showrunner Sechrist and executive producer Bill Wolkoff. They worked in two teams, each comprising a director and three board artists. The animation is made by Studio Mir in South Korea using traditional animation methods. About sixty people worked on the series at DreamWorks, and about fifty-five at Studio Mir.


== Characters ==


=== Main ===
Kipo Oak (voiced by Karen Fukuhara) – An enthusiastic and curious young girl who is searching for her people. She later learns that her parents injected her with mute DNA, giving her the powers of a huge pink jaguar.
Wolf (voiced by Sydney Mikayla) – Kipo's best friend. A hardened and young survivor on the surface world who was raised by wolves. She wields a spear made from the stinger of a deathstalker scorpion that she dubs "Stalkie".
Benson Mekler (voiced by Coy Stewart) – A happy-go-lucky surface survivor, Dave's best friend, and last of the "fanatics", a group previously at war with Dave's species for two centuries.
Dave (voiced by Deon Cole) – A mutant insect and Benson's best friend who repeatedly molts from baby to child to teen to adult to elderly. He is the last of his kind, a species of identical-looking insects who all shared the name Dave and were at war for over two centuries with the "fanatics" over a cooling fan.
Mandu (voiced by Dee Bradley Baker) – A mutant pig with four eyes and six legs adopted by Kipo.
Lio Oak (voiced by Sterling K. Brown) – Kipo's father, a scientist, and teacher of their underground community. He and Song were originally working for Dr. Emilia to turn mutes back into normal animals so humanity could reclaim the surface.
Hugo "Scarlemagne" Oak (voiced by Dan Stevens) – A deranged, flamboyant, power-hungry mandrill and Kipo's adoptive older brother. He seeks to create a mute-only utopia and rule Las Vistas with an army of enslaved humans. Due to human experimentation, the pheromones in his sweat can control primate minds.
Song Oak (voiced by Jee Young Han) – Kipo's mother and Lio's wife who originally worked with him to revert mutes back into normal animals before they changed their minds; thought to be deceased, she is later revealed to be alive.
Dr. Emilia (voiced by Amy Landecker) – A cold-hearted, manipulative, remorseless human scientist who wants to end the mutes' existence.


=== Supporting ===
Molly Yarnchopper (voiced by Lea Delaria) - A Timbercat and Yumyan Hammerpaw's second-in-command.
Yumyan Hammerpaw (voiced by Steve Blum) - Axe Lord of the Timbercats. His name is a parody of the tall tale character Paul Bunyan.
Cotton (voiced by Grey Griffin) - Rocker leader of the Umlaut Snakes from Cactus Town.
Good Billions and Bad Billions (voiced by John Hodgman and GZA) - Astronomer co-leaders of the Newton Wolves.
Jamack (voiced by Jake Green) - A Mod Frog who sought out to capture Kipo after she caused him to be banished from his pack, but has since become a reluctant ally.
Tad Mulholland (voiced by Michael-Leon Wooley) - A sentient colony of tardigrades who traps others in dream worlds.
Hoag (voiced by Jeff Bennett) - A member of Kipo and Lio's underground community and Doag's father.
Troy (voiced by Giullian Yao Gioiello) - A boy from Kipo and Lio's underground community and Benson's boyfriend.
Asher and Dahlia (voiced by Rhea Butcher and Fryda Wolff) - Twin siblings from Kipo and Lio's underground community. Asher is non-binary while Dahlia is female.
Amy and Brad (voiced by Avrielle Corti and Ace Gibson) - A pair of rats who manage an amusement park.
Zane and Greta (voiced by Carlos Alazraqui and Anna Vocino) - Dr. Emilia's assistants who later side with Kipo.
Roberto (voiced by Antonio Alvarez) - Troy's father.
Fun Gus (voiced by Jack Stanton) - A sentient mold with a child-like personality.
Label (voiced by Betsy Sodaro) - Leader of the Fitness Raccoons.
Mrs. Sartori (voiced by Grey Griffin) - Leader of the Mod Frogs.
Easy (voiced by Matt Lowe) - Leader of the Humming Bombers.
Doag (voiced by Rebecca Husain) - Hoag's daughter.
Gerard (voiced by Dee Bradley Baker) - An orangutan Noble and Scarlemagne's second-in-command.
Lemieux (voiced by Grey Griffin) - A tarsier Noble.
Loretta and Wheels (voiced by Grey Griffin and Alanna Ubach) - Co-leaders of the Scooter Skunks.
Harris and Kwat (voiced by Ian Harding and Grey Griffin) - A pair of Mod Frogs and Jamack's former goons.
Camille (voiced by Joan Jett) - An Umlaut Snake.
Puck (voiced by John Lavelle) - A theater musician otter.
Ida, Florabel, and Bev (voiced by Kay Bess, Chris Anthony Lansdowne, and Mindy Sterling) - The "Chevre Sisters", a trio of blind, soothsaying goats.
Ruffles (voiced by Matt Lowe) - A Timbercat.
Tongue Depressor (voiced by David Neher) - A Fitness Raccoon.
Margot (voiced by Faith Graham and Victoria Grace) - Wolf's adoptive wolf sister.


== Music ==
The soundtrack to the series, including several original songs, was composed by Daniel Rojas. It draws on an eclectic mix of musical styles, from folk to classical music and hip-hop, with Wu-Tang Clan founding member and Bad Billions' voice actor GZA being given co-writing credit for the 'Newton Wolves Rap'. The first soundtrack album, Kipo and the Age of Wonderbeasts: Season 1 Mixtape, was released in January 2020 by Back Lot Music. The second soundtrack album, titled Kipo and the Age of Wonderbeasts: Season 2 Mixtape, was released in late May 2020 by Back Lot Music.


== Episodes ==


=== Season 1 (2020) ===


=== Season 2 (2020) ===


=== Season 3 (2020) ===


== Reception ==
The first two seasons of Kipo and the Age of Wonderbeasts have a 100% score on Rotten Tomatoes. At io9, Beth Elderkin described Kipo as a "must-watch", writing that it joined the likes of She-Ra and the Princesses of Power, Gravity Falls and Steven Universe as a series with a broad appeal to many age groups, and highlighting its music and art design. At Collider, Dave Trumbore noted Kipo's similarity to other recent female-led animated portal fantasy series such as Amphibia and The Owl House, and described it as a "classic in the making" that drew on cultural touchstones such as Fallout, The Warriors, The Island of Doctor Moreau, Planet of the Apes and Alice in Wonderland.Writing for Polygon, Petrana Radulovic appreciated that beneath a standard fantasy exploration quest, the series is a "vibrant mosaic, with a unique world, multidimensional character relationships, and a deeper underlying plot" about the tensions between mutes and humans. She also noted that Benson was the first character to have an explicit coming out as gay in an all-ages animation series, and that the understated manner of the scene, in episode 6, made it all the more noteworthy. Charles Pulliam-Moore at io9 likewise wrote that the series's "casual queerness is fantastic" because Benson's orientation is not treated as a plot point to complicate Kipo's feelings for him, but, "with a distinct matter-of-factness", as just one aspect of his character.


== References ==


== External links ==
Kipo and the Age of Wonderbeasts on Netflix 
Kipo and the Age of Wonderbeasts on IMDb