James John "Jim" Corbett (September 1, 1866 – February 18, 1933) was an American professional boxer and a World Heavyweight Champion, best known as the only man who ever defeated the great John L. Sullivan (hence the "man who beat the man" concept of the championship boxing lineage.) Despite a career spanning only 20 bouts, Corbett faced the best competition his era had to offer; squaring off with a total of 9 fighters who would later be enshrined alongside him in the International Boxing Hall of Fame. Corbett introduced a truly scientific approach to boxing, in which technique triumphed over brute force, he pioneered the daily boxing training routine and regimen, which, being adopted by other boxers elsewhere, almost intact survived to modern days. A "big-money fighter," Corbett was one of the first athletes, whose showmanship in and out of the ring was just as good as his boxing abilities, also being arguably the first sports sex symbol of the modern era after the worldwide airing of his championship prizefight versus Robert Fitzsimmons popularized boxing immensely among the female audience, and did so in an era while the prizefighting was illegal in 21 states and still considered among the most infamous crimes against morality.


== Biography ==
In 1854, Jim's father, Patrick Corbett, emigrated from Ballycusheen, Ballinrobe, County Mayo, Ireland, to the US. James Corbett returned to Ballinrobe in 1894, and among the highlights of his visit were the boxing demonstrations he gave in Ballinrobe Town Hall. The proceeds from the event's entrance fees were donated for the upkeep of Partry Church, where his uncle, the Reverend James Corbett, was then parish priest. He also donated a stained glass window to the church. He coached boxing at the Olympic Club in San Francisco. He stood at 6 ft 1 in (1.85 m), with a reach of 73 inches (185 cm).


== Boxing career ==
Dubbed by the media Gentleman Jim Corbett, he graduated from Sacred Heart High School in San Francisco, California and was rumored to have a college education. He also pursued a career in acting, performing at a variety of theatres. He has been called the "Father of Modern Boxing" for his scientific approach and innovations in technique.

On May 21, 1891, Corbett fought Peter "Black Prince" Jackson, a much-heralded bout between crosstown rivals, since Corbett and Jackson were boxing instructors at San Francisco's two most prestigious athletic clubs. They fought to a no-contest after 61 rounds.The fight vaulted Corbett to even greater national prominence and the public clamored for a contest between Sullivan and him. The champion reluctantly agreed, and the fight was finally set. Corbett went into rigorous training and was even more confident of his chances after he sparred with Sullivan in a short exhibition match on a San Francisco stage. On September 7, 1892, at the Olympic Club in New Orleans, Louisiana, Corbett won the World Heavyweight Championship by knocking out John L. Sullivan in the 21st round. Corbett's new scientific boxing technique enabled him to dodge Sullivan's rushing attacks and wear him down with jabs.Corbett did not prove to be a "Fighting Champion" in today's terms, meaning he defended the title very rarely. What must be remembered is this was an era before boxing commissions and the regulation of the sport was minimal at best. Boxing was outlawed in most states, so arranging a time and place for a bout was largely a hit-and-miss proposition. Corbett treasured his title and viewed it as the ultimate promotional tool for his two main sources of income, theatrical performances and boxing exhibitions.

In his only successful title defense on January 25, 1894, Corbett knocked out Charley Mitchell of Great Britain in three rounds. On September 7, 1894, he took part in the production of one of the first recorded boxing events, a fight with Peter Courtney. This was filmed at the Black Maria studio at West Orange, New Jersey, and was produced by William K.L. Dickson. It was only the second boxing match to be recorded.

Jim Corbett lost his Heavyweight Championship to the Cornish British boxer Bob "Ruby Robert" Fitzsimmons in Carson City, Nevada. Corbett was dominant for most of the fight and knocked Fitzsimmons to the canvas in the sixth round. Fitzsimmons recovered and, though badly cut, rallied from that point on. When Mrs. Fitzsimmons called out, "Hit him in the slats, Bob!", where "slats" meant the abdominal area, her husband followed her advice. The body blows took their toll, and though Corbett continued to outbox his opponent masterfully, ringsiders could see the champion slowing down. Fitzsimmons put Corbett down in the 14th round with a withering body blow to the solar plexus, and Corbett, despite his best efforts to do so, could not regain his feet by the end of the ten-count. This fight, lasting over an hour and a half, was released to cinemas later that year as The Corbett-Fitzsimmons Fight, the longest film ever released at the time.
Devastated by the loss of his title, Corbett did everything he could to lure Fitzsimmons back into the ring. He was sure Fitzsimmons's victory had been a fluke, mostly attributed to his overtraining, which left him short on stamina in the later rounds, and was confident he would win the rematch. Perhaps Fitzsimmons felt the same way, for not even a $30,000 guaranteed purse posted by Corbett's manager, William A. Brady, could get Ruby Robert back into the ring with Gentleman Jim. It may also have been Fitzsimmon's intense personal dislike of Corbett, who had often publicly insulted him, which ruled out any chance of another fight.

This set the stage for what most boxing experts and ring historians consider to be Corbett's finest fight. Refusing to face Corbett, Ruby Robert chose the hulking James J. Jeffries, a former sparring partner of Corbett's and a big heavyweight even by modern standards, for his title defense.

Jeffries had learned much of his trade training with Corbett and was now handled by Corbett's old manager, William Brady. Corbett, who had been put on the back burner during Fitzsimmons reign, wasted no time in suggesting a title fight between his old sparring partner and himself after Jeffries beat Fitzsimmons. Brady, liking Corbett and reflecting after a recent poor showing against Tom Sharkey that his old fighter had little left in the tank at 34, agreed to the match. The fight was set for the Seaside Arena in Coney Island, New York.
While Jeffries went through the motions in training, Corbett prepared like a Spartan for battle. He knew, with his speed, he could box rings around his larger and stronger opponent, but he was giving up size, strength, almost 30 lb. in weight and a seven-year age difference. The key was stamina and the ability to last the 25-round fight limit.
The early rounds saw Corbett moving quickly while "Big Jeff" attempted body shots. However, Jeffries could not lay a glove on the "Dancing Master". Round after round, Corbett had his way, darting in to land punches, then dancing away to avoid retaliation. By the 20th round, Jeffries' corner was in a panic. Manager Brady dismissed trainer Tommy Ryan from the corner and took charge himself with the simple but direct order, "Knock him out or lose your title!" Corbett only had to stay upright for the last five rounds to be heavyweight champion once again. Jeffries stalked Corbett around the ring, looking for an opening. Corbett danced away from any threat through the 22nd round.Midway through the 23rd round, Corbett leaned back to avoid a blow from Jeffries, bounced off the ropes and was put on the canvas by a short right hand. He was counted out. Corbett found himself embraced by the public after this gallant effort. The adoration was short-lived, as his next fight, a five-round knockout over Kid McCoy, was widely believed to be a fix. Corbett managed to contest for the heavyweight title one last time when he met Jeffries for a second match in San Francisco in 1903. Now 37, reflexes slowing, Corbett survived a withering body blow in the second round and used every trick he knew to hang on until being knocked out in the tenth.


== Life after boxing ==

Following his retirement from boxing, Corbett returned to acting, appearing in low-budget films and giving talks about pugilism. He also performed a minstrel show in skits with Cornelius J. O'Brien.Corbett was married to Mary Olive Morris Higgins from 1886 to 1895. After their divorce, he married the actress Jessie Taylor, also known by her stage name, Vera. She survived Corbett by more than a quarter century. From 1903 until his death, Corbett lived at 221-04 Corbett Road in a three-story home in the Bayside neighborhood of the borough of Queens in New York City.In 1924, he had a friendly sparring match with the future champion Gene Tunney, an admirer of Corbett's scientific style. Tunney was amazed at the ability of Corbett to spar, even at the age of about 60, even claiming Corbett had better defense than Benny Leonard.Corbett's brother, Joe Corbett, was a Major League Baseball pitcher. Corbett's great-great-great-nephew, Dan Corbett, was a professional heavyweight boxer from San Antonio, Texas, who won the United States Boxing Federation and International Boxing Organization Intercontinental Heavyweight titles before retiring.


== Death ==
Corbett died of liver cancer on February 18, 1933, aged 66. His body was interred in the Cypress Hills Cemetery in Brooklyn, New York. On its creation, he was elected posthumously to the International Boxing Hall of Fame.


== Professional boxing record ==


== Films ==

Thomas Edison wrote "I remember Jim Corbett very well, for he was a very important part of the first motion picture that we made for public exhibition. Yes, he can justly claim the distinction of being the oldest living film star, and I extend him my hearty congratulations," from a letter in 1930.
Corbett and Courtney Before the Kinetograph (1894, Short)
Actor's Fund Field Day (1910, Short) - Himself
How Championships Are Won—And Lost (1910, Documentary short) - Himself
The Man from the Golden West (1913) - Gentleman Jim
The Burglar and the Lady (1914, based on the play of the same name that he also appeared in) - Danvers / Raffles
The Other Girl (1916) - Frank Sheldon, 'Kid Garvey'
The Midnight Man (1919) - Bob Gilmore aka Jim Stevens aka The Midnight Man
The Prince of Avenue A (1920) - Barry O'Connor
The Beauty Shop (1922) - Panatella
Broadway After Dark (1924) - Himself, Cameo Appearance
James J. Corbett and Neil O'Brien (1929, Short) - Himself - Prizefighter
Happy Days (1929) - Interlocutor - Minstrel Show #1 (final film role)
At the Round Table (1930, Short) - Himself


== Cultural references ==
Corbett authored his autobiography under the title The Roar of the Crowd; the story was serialized by The Saturday Evening Post in six weekly installments during October/November 1894. The following year, G.P. Putnam's Sons published it in book form, marketing it as the "True Tale of the Rise and Fall of a Champion." In 1942, the story was made into a Hollywood motion picture titled Gentleman Jim, starring Errol Flynn as Corbett.
In 1966, the actor James Davidson played Corbett in the episode "The Fight San Francisco Never Forgot" of the syndicated western television series Death Valley Days, hosted by Ronald W. Reagan. In the story line, Walter Watson (John McLiam) becomes boxing instructor at the Olympic Club in San Francisco. In this capacity, he fights off a local bully and begins training Corbett to face him in a match vital to the future of Watson and the club.Corbett is mentioned in a Season 2 episode of The Simpsons, "Homer vs. Lisa and the 8th Commandment", when Mr. Burns remembers paying a nickel to attend a fight between Corbett and "an Eskimo fellow."


== See also ==
List of heavyweight boxing champions


== References ==


== External links ==
James J. Corbett - CBZ Profile
Boxing record for James J. Corbett from BoxRec
Boxing Hall of Fame
James J. Corbett on IMDb
James J. Corbett at Find a Grave
Corbett's Home