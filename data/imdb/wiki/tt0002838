To Face Her Past is a 1996 television film directed by Steven Schachter. Based on a true story, the film stars Patty Duke, Tracey Gold, David Ogden Stiers, Gabrielle Carteris and James Brolin.


== Plot ==
Beth Bradfield (Patty Duke) is a housewife with what appears to be a stable life in an American village. One day, her 24-year-old daughter Lori (Tracey Gold), who is married to Jesse Molina (Maurice Benard) and recently gave birth to his daughter Molly (Laura and Megan Jaime), unexpectedly collapses and is hospitalized. After several tests, she is diagnosed with leukemia. Her doctor (Erick Avari) reveals to Beth that Lori is in urgent need of a donor, though her rare blood type makes finding one a difficult task.
After a period of time without finding a donor, during which Lori's health worsens, Beth realizes that only confronting a dark secret from her past can save her daughter: when Beth was eighteen years old, she was set to marry a young man named Greg Hollander (James Brolin), and shortly after found out that she was pregnant. She gave birth to a daughter, but gave her up under the immense pressure of Greg's parents Vic (Francis X. McCarthy) and Kate (Susan Brown), who felt that Greg and Beth were too young to start a family. Megan was raised by her grandparents, under the pretense that her mother abandoned her.
Back in the present, Beth meets Megan (Gabrielle Carteris) for the first time since her birth. Megan, now a successful vice-president for a company, has not been able to forgive her mother over the years, and refuses to help her. She later decides, though, that she wants to do all that she can to help save a woman's life, and agrees to be Lori's donor, though she makes clear that wants nothing to do with Beth. After Lori finds out that her donor is her sister, she bonds with Megan. Over the time, Megan's hostile behavior towards Beth disappears.
Trouble starts again when the doctor informs Beth that tests with Lori's father are essential. What Beth has been fearing for years is now reality: After a test, it turns out that her husband Ken Bradfield (David Ogden Stiers) is not Lori's father. Nine months before Lori was born, Beth had a short-lived affair with Greg. Realizing that Greg is Lori's father, Beth contacts him to help her, but Greg remains distant and refuses to be a part of their lives. Nevertheless, Lori eventually recovers after a successful surgery. Meanwhile, Ken has found out about Lori's true father. Even though he does not forgive Beth, he comes to terms with the truth and he realizes that even though he is not Lori's biological father, he will serve as her father no matter what.


== Cast ==
Patty Duke as Beth Bradfield
Tracey Gold as Lori Molina
David Ogden Stiers as Ken Bradfield
Gabrielle Carteris as Megan Hollander
James Brolin as Greg Hollander
Maurice Benard as Jesse Molina
Susan Brown as Kate Hollander
Francis X. McCarthy as Vic Hollander
Laura and Megan Jaime as Molly Molina
Erick Avari as Dr. Webster
Michele Lamar Richards as Janet
Christopher Tichy as Young Greg
Shonda Whipple as Young Beth


== References ==


== External links ==
To Face Her Past on IMDb