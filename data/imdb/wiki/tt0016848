This is a complete list of animated films released theatrically starring Felix the Cat (169 silent cartoons and 15 sound cartoons, for a total of 184 cartoons).  "View" links are linked to a public domain file of the cartoon at archive.org or the official British Pathé archives at britishpathe.com.


== Short films ==


=== Silent films ===


==== Paramount Pictures (1919–1921) ====
The first 25 Felix cartoons were distributed to theaters by Paramount Pictures. The character was named "Master Tom" until The Adventures of Felix.


==== Margaret J. Winkler (1922–1925) ====
64 cartoons


==== General Electric (ca. 1925 commercial for Mazda Lamps) ====


==== Educational Pictures (1925–1928) ====
78 cartoons.


=== Sound films ===


==== Copley Pictures (1929–1930) ====
Copley Pictures was the first distributor to issue Felix cartoons with sound. There were 12 originally with sound, and 15 reissues, for a total of 27 cartoons.


===== Sound reissues =====
A number of silent Felix cartoons were also re-issued by Copley at this time, with their intertitles removed and sound added. Newer simple titles were also inserted in most reissues, which removed signs of Educational Pictures copyrights. However, in some cases, original titles and intertitles were retained. Jacques Kopfstein was hired by Pat Sullivan to add sound to the film. This was done via the "goat gland" system of adding sound. All Felix shorts that were re-issued in sound have Post-synchronized soundtracks. (The soundtrack was made to match the already-existing film.) As a result, the synchronization is not perfect, and there is occasionally an audible delay between the action and the sound effect.


===== New releases =====
Copley also distributed 12 cartoons originally with sound.


==== Van Beuren Studios (1936) ====

This short revival of Felix (as a more childlike character, similar to his later 1959 incarnation) was produced by Van Beuren Studios and distributed to theaters by RKO Radio Pictures. All of these cartoons were the first to be produced in three-strip Technicolor.


== Television ==


== Feature length films ==
More than five decades following the last theatrical shorts, a Felix feature film was produced. Though intended to be released theatrically, however it didn't happen and was released as a direct-to-video feature instead.


== References ==


== Sources ==
Gerstein, David A. (2007). "The Classic Felix Filmography". The Classic Felix the Cat Page. Archived from the original on 2012-01-23. Retrieved 2009-04-10. 
Canemaker, John (1996). Felix: The Twisted Tale of the World's Most Famous Cat. Da Capo Press. ISBN 0-306-80731-9.