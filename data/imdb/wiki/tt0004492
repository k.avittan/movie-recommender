"The Necklace" or "The Diamond Necklace" (French: La Parure) is an 1884 short story by French writer Guy de Maupassant. It is known for its twist ending (Ironic ending), which was a hallmark of de Maupassant's style. The story was first published on 17 February 1884 in the French newspaper Le Gaulois.


== Plot ==
Madame Mathilde Loisel has always imagined herself an aristocrat, despite being born into a family of clerks (which she describes as an "accident of fate"). Her husband is a low-paid clerk who tries his best to make her happy but has little to give. Still, the young couple can afford the services of a young Breton girl as a servant. After much effort, he secures for them an invitation to a ball sponsored by the Ministry of Education. 
Madame Loisel refuses to go, for she has nothing to wear and wishes not to be embarrassed. Upset at her displeasure, Loisel gives her 400 francs - all the money he had been saving to go hunting with his friends - so she can buy a dress. Even after Madame Loisel does so, she is still unhappy because she has no jewels to wear with it. She spurns Loisel's idea of wearing fresh flowers instead, but takes his suggestion to borrow some jewelry from her friend, Madame Jeanne Forestier. She borrows a diamond necklace as her only ornamentation.
Madame Loisel enjoys herself at the ball, dancing with influential men and reveling in their admiration. Once she and Loisel return home, though, she discovers that she has lost Jeanne's necklace. Unable to find it or anyone who knows where it might have gone, they resign themselves to buying a replacement. At the Palais-Royal shops, they find a similar necklace priced at 40,000 francs and bargain for it, eventually settling at 36,000. Loisel uses an inheritance from his father to cover half the cost and borrows the rest at high interest. Madame Loisel gives the necklace to Jeanne without mentioning the loss of the original, and Jeanne does not notice the difference.
Mr. Loisel and Madame Loisel live in poverty for ten years, with him taking on night work as a copier to earn extra money and her sacrificing her beauty to do household chores on her own, while constantly bargaining with the clerks and vegetable sellers. They gave up their servant, and have moved to a poor apartment up many stairs. After all the loans are paid off, Madame Loisel  encounters Jeanne on the Champs-Élysées, but Jeanne barely recognizes her due to her shabby clothing and unkempt appearance. Jeanne is also accompanied by her young daughter, of which little mention is made except by inference as the Loisels have no children. Madam Loisel tells Jeanne about the loss and replacement of the necklace and of the hard times she has endured on Jeanne's account. She blames her former friend for the past miserable 10 years. Jeanne reveals that the necklace she lent to Madame Loisel had contained fake diamonds and was worth no more than 500 francs.


== Themes ==
One of the themes within "The Necklace" is the dichotomy of reality versus appearance. Madame Loisel is beautiful on the outside, but inside she is discontented with her less-than-wealthy lifestyle. Mathilde is gripped by a greed that contrasts with her husband's kind generosity. She believes that material wealth will bring her joy, and her pride prevents her from admitting to Madame Forestier that she is not rich, and that she has lost the necklace she borrowed.
Because of her pride and obsession with wealth, Mathilde loses ten years of her life and spends all of her savings on replacing the necklace, only to find out that the original necklace was a fake to begin with—a falsely wealthy appearance, just like Madame Loisel herself.The story demonstrates the value of honesty; if Mathilde had told the truth to Madame Forestier, she would likely have been able to easily replace the necklace and enjoy the prosperity she wanted but never had.


== Adaptations and other influence ==
The following are direct adaptations of "The Necklace":

The Diamond Necklace (1921), a British silent film directed by Denison Clift and starring Milton Rosmer, Jessie Winter, and Warwik Ward
A String of Pearls (1926 film) (《一串珍珠》) (1926), a Chinese film directed by Li Zeyuan
"The Necklace" (1949), the first episode of the NBC-TV series Your Show Time (producer Stanley Rubin won the first-ever Emmy Award for this episode)
Mathilde (2008), a stage musical by the Irish composer Conor Mitchell
"දියමන්ති මාලය" (Diyamanthi Maalaya), a translation by K. G. KarunathilakeThe following works were inspired in part by "The Necklace":

"Paste" (1899), a short story by Henry James in which the twist ending is reversed
"Mr. Know-All" (1925) and "A String of Beads" (1943), short stories by Somerset Maugham that both revolve around the price of a necklace
"The Diamond Pendant" in Impact #1, E.C. Comics, March/April 1955; adaptation by Carl Wessler, illustrated by Graham Ingels
Vennila Veedu (2014), a Tamil family drama uses a similar story as its main theme
The subplot of the season 4 episode 13 of "Mom" ("A Bouncy Castle and an Aneurysm" OAD: 9 Feb 2017) is a comedic version of the story with Anna Faris' character losing the necklace belonging to her wealthy friend.In Vladimir Nabokov's novel Ada or Ardor (1969), one of the characters, a writer, claims she has written a short story entitled "La Rivière du diamants", which mimics Maupassant's "The Necklace". The moment in which this occurs is set in the book to be around 1884, the year in which Maupassant actually published his short story.


== References ==


== External links ==
 French Wikisource has original text related to this article: La Parure
 The full text of The Necklace at Wikisource
 Media related to La Parure at Wikimedia Commons
 The Necklace public domain audiobook at LibriVox
The Necklace - Annotated text aligned to Common Core Standards