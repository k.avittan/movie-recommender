Arabian Knights is an animated segment of The Banana Splits Adventure Hour, created by Hanna-Barbera Productions. The series is based on the Arabian Nights, a classic work of Middle Eastern literature. The cast includes Henry Corden, Paul Frees, Frank Gerstle, Shari Lewis, Jay North and John Stephenson.


== Plot: Secret origins ==
Once a peaceful country, Persia was overrun by the powerful forces of the evil conqueror Bakaar the Black Sultan. When all the states of Persia were overthrown, Bakaar invaded the capital city of Baghdad. Bakaar and his forces breached the royal palace and forcibly claimed the throne from Prince Turhan and ordered the prince to be killed. Prince Turhan fled the palace, pursued by the guards of Bakaar. In his escape, the prince is helped by a magician named Fariik who with his magic slipped away from the guards. On their way to the state of El-Rabaul to seek aid from his uncle the Caliph, Turhan and Fariik meet Raseem while being chased by the guards in the Caves of Doom. Raseem tells them that Bakaar's forces have already usurped the throne of El-Rabaul and imprisoned the Caliph and his daughter Princess Nida. The trio then heads to El-Rabaul on Raseem's donkey Zazuum.
In El-Rabaul, they rescue Princess Nida who is about to be sold into slavery. There, Princess Nida demonstrates her mastery of disguise and voice mimicry by tricking the guards, but they soon find themselves cornered by Bakaar's men. There Bez jumps into the scene, shapeshifts into an elephant, charges through the guards, and carries everyone to safety. In the Caves of Doom, they decide to form a heroic band and swear to protect their land from the tyranny of Bakaar, thus forming an alliance called the Arabian Knights.


== Series direction ==
Arabian Knights was one of several animated miniseries used as filler segments for Hanna-Barbera's Banana Splits Adventure Hour show. Each episode ran between 9 and 10 minutes in length. For the most part, it competed with the other series, namely the animated The Three Musketeers, the part live action, part animated Micro Ventures, and the live action Danger Island segments.
Often, the general thrust of the Arabian Knights adventures deals with the Knights trying to effect some action against Bakaar’s soldiers, finding a means to further complete their goal of turning the tables on the evil dictator, or dealing with another villain set within the Knights' universe.
For the most part, these adventures were done with a semi-serious slant, with a goodly portion of action and comedy mixed in. Hardly anyone was injured or harmed—save for Bakaar's minions (with a usually comic effect), and the episodes usually had an upbeat, happy ending for the Knights.


== Characters ==


=== The Arabian Knights ===
Prince Turhan (voiced by Jay North) - The prince of Baghdad and rightful heir to the throne of the Sultan, Turhan is a young and handsome teenage boy. Apart from being brave and bold, he is an agile acrobat, an athlete, and a master swordsman. Turhan gathers a small group of friends to lead a revolt against the tyranny of Bakaar and reclaim his throne to restore the peace and prosperity in his state. Turhan is the de facto leader of the Knights and the driving force behind fighting Bakaar and ending the usurper’s reign. He owns a magic scimitar that can be thrown and returns like a boomerang, in unique ways.
Princess Nida (voiced by Shari Lewis) -  Daughter of the Caliph of El-Rabaul, Princess Nida is a lovely teenage girl who is the cousin of Prince Turhan. She is a master of disguise and an expert of voice mimicry. Nida is also skilled in martial arts and can hold her own in the Knights' battles.
Fariik the Magician (voiced by John Stephenson) - A short, tubby, jolly little man, Fariik is a skilled practitioner and spell-master of the mystical arts who lends a magical hand when needed. To cast his spell, Fariik would quote "Rosan Kobar." He is the first to have offered aid to Turhan after Prince Turhan is deposed by Bakaar. Sometimes, the magic gets very difficult and can take a lot of energy out of him. Fariik can animate carpets and pillows for the occasional airborne transport of the team. Fariik can render himself and others invisible for stealth or camouflage purposes. At times, he can dissolve himself and others into smoke to make an escape from narrow places. He can levitate objects, alter the size of things and persons, create things out of thin air, and alter the form of one thing into another.
Raseem the Strong (voiced by Frank Gerstle) - A tall muscular man with brief costume and fez, Raseem is the powerhouse of the team with incredible physical strength, endurance, and stamina. He first met Turhan and Fariik when they hid in the Caves of Doom that he occupies. Raseem mistakes them for Bakaar's men and attacks them. However, upon learning they are against Bakaar, he befriends them, and he goes with them to find the Caliph (to get his aid against Bakaar). He often boasts of his strength, saying he is as strong as 30 men. At one time, Raseem said he was as strong as seven elephants. Strong enough to shatter stone walls, lift and catch boulders, catch and throw back cannonballs, and fight off scores of Bakaar’s soldiers at a time, Raseem's power makes for a good equalizer in the Knights' battles.
Zazuum (voiced by Don Messick) - Raseem’s faithful little donkey with long ears, Zazuum was first used by Turhan, Fariik, and Raheem to get to El-Rabaul. Zazuum is quite an intelligent donkey as he aids his fellow team members. Zazuum can spin himself so fast he becomes like a destructive miniature tornado that can defeat any enemy or destroy anything coming his way. He does not like his tail to be pulled, as it enrages him to the point of being a berserk loose cannon that can shatter rocks into pebbles. In addition, Zazuum can smell gold very precisely.
Bez the Beast (voiced by Henry Corden) - A dark-skinned man with a turban and green clothing, Bez can shapeshift into an animal, bird, or aquatic creature from a mighty elephant to a tiny mouse. Often, he triggers this ability by speaking the phrase “Size of a —!” and naming the animal or bird he wishes to turn into, followed by a loud boom that proclaims his transformation. Sometimes the magical phrase is followed by clapping his hands together. If the creature is small enough, Bez can turn into many versions of it such as turning into a million bees. Bez is known to turn into some larger animals which often serve as alternate modes of transportation for the Arabian Knights like an elephant or a camel for land transportation. He can also transform into mythical creatures like a winged stallion or a roc for the occasional airborne transportation. His shapeshifting makes him a handy spy for the Knights in order to find out what Bakaar is planning.


=== Villains ===
Bakaar the Black Sultan (voiced by John Stephenson) - The corrupt conqueror and nemesis of the Arabian Knights, Bakaar usurps the throne of Turhan’s father and takes control of Baghdad. Clearly wishing to command all of the kingdoms that bow to Baghdad, he sets all of his forces to the goal of capturing Turhan (and later all of the Knights) to secure his place on the throne. While cruel, Bakaar does not usually involve himself directly in the fight against the Knights. He prefers to let his soldiers and minions to do the job. Bakaar's distinctive manner of dress—a single-spiked, fur-lined helmet in place of a crown—hints at ancestry from the Mongols.
Vangore (voiced by Paul Frees) - Bakaar's enforcer and captain of his guards. Often hatches plots to capture the Knights. Apparently, Vangore's father was a camel driver. He has red hair shaved to a topknot (which would historically be an indication of Galatian ancestry and of being enslaved).
Sundar - Bakaar's court magician.


== Episodes ==


== References ==


== External links ==
Arabian Knights on IMDb
"Arabian Knights" at International Hero
"Arabian Knights" at Don Markstein's Toonopedia. Archived from the original on April 4, 2012.