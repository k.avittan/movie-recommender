Bobby Bumps was the titular character of a series of American silent animated short subjects produced by Bray Productions from 1915–25. Inspired by R. F. Outcault's Buster Brown, Bobby Bumps was a little boy who, accompanied by his dog Fido, regularly found himself in and out of mischief. Each cartoon begins with a cartoonist's hand drawing Bobby, Fido and the backgrounds.The first two cartoons were released in 1915 by Universal; the next few years' titles were released by Paramount Pictures as part of their Paramount Bray Pictograph and, later, Paramount Magazine short-subjects packages. Mid-1920s episodes were released by Educational Pictures.
The series was created by Earl Hurd, who directed and/or animated most of the films. The Bobby Bumps cartoons were the first to be produced using the cel animation process. Previously, animated cartoons were produced using paper animation: a new drawing was made for each frame of film. With cel animation, Bray drew his characters on clear sheets of celluloid, which he placed over still backgrounds during the photography process. Cel animation revolutionized the animation industry, and Hurd and his employer J.R. Bray held a patent for the process (and received licensing payments from all studios using the process) until 1932.
A 1918 short, Bobby Bumps Becomes an Ace, reflects the country's concerns about World War I. In this short, Bobby dreams that he shoots down German fighters and tries to sink a U-boat.In 2019, a comprehensive Blu-ray/DVD collection from animation researcher/restorer Tommy José Stathes, Cartoon Roots: Bobby Bumps and Fido, was released, containing fifteen shorts and much background material.


== Filmography ==
1915

Bobby Bumps Gets Pa's Goat
Bobby Bumps Adventures1916

Bobby Bumps and His Pointer Pup  - First Booby Bumps cartoo produced at Bray Productions. 
Bobby Bumps Gets a Substitute
Bobby Bumps and His Goatmobile
Bobby Bumps Goes Fishing  
Bobby Bumps' Fly Swatter
Bobby Bumps and the Detective Story
Bobby Bumps Loses His Pup
Bobby Bumps and the Stork 
Bobby Bumps Starts a Lodge
Bobby Bumps Helps Out a Book Agent 
Bobby Bumps Queers a Choir
Bobby Bumps at the Circus1917

Bobby Bumps in the Great Divide
Bobby Bumps Adopts a Turtle
Bobby Bumps, Office Boy 
Bobby Bumps Outwits the Dogcatcher
Bobby Bumps Volunteers
Bobby Bumps, Daylight Camper
Bobby Bumps, Submarine Chaser 
Bobby Bumps' Fourth
Bobby Bumps' Amusement Park 
Bobby Bumps, Surf Rider
Bobby Bumps Starts to School
Bobby Bumps' World "Serious"
Bobby Bumps, Chef
Fido's Birthday Party
Bobby Bumps, Early Shopper 
Bobby Bumps' Tank1918

Bobby Bumps' Disappearing Gun
Bobby Bumps at the Dentist
Bobby Bumps' Fight
Bobby Bumps On the Road
Bobby Bumps Caught in the Jamb
Bobby Bumps Out West
Bobby Bumps Films a Fire
Bobby Bumps Becomes an Ace
Bobby Bumps on the Doughnut Trail
Bobby Bumps and the Speckled Death 
Bobby Bumps, Incubator
Bobby Bumps in Before and After
Bobby Bumps Puts a Beanery on the Bum1919

Bobby Bumps' Last Smoke
Bobby Bumps' Lucky Day
Bobby Bumps' Night Out with Some Night Owls
Bobby Bumps' Pup Gets the Flea-enza
Bobby Bumps: Eel-ectric Launch
Bobby Bumps and the Sand Lizard
Bobby Bumps and the Hypnotic Eye
Bobby Bumps Throwing the Bull - Final Booby Bumps cartoon produced at Bray Productions. 
Their Master's Voice1920

Bobby Bumps' Non-Stop Flight
The Doughnut Lifter
Captain Kiddlets
Oh, What a Knight
Bobby Bumps, Cave Man
A Trip to the Moon
Pot Luck
Bobby Bumps' Orchestra (sometimes listed as Bobby Bumps, Conductor)1921

Pen and Inklings
Mixed Drinks
Checkmated
Bobby Bumps Joins the Band
Bobby Bumps Working on an Idea
Shadow Boxing
Hunting and Fishing1922

One Ol' Cat
Railroading
Bobby Bumps at School
Fresh Fish (includes a live-action Bobby)1923

The Movie Daredevil
Their Love Grew Cold
Chicken Dressing1925

Bobby Bumps and Company


== References ==


=== Citations ===


=== Sources ===
Lund, Karen (June 1999) "Innovative Animators" The Library of Congress Information Bulletin. Retrieved September 6, 2007.