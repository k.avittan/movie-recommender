"The Soul Mate" is the 136th episode of the American television sitcom Seinfeld. This was the second episode for the eighth season. It was originally broadcast on the NBC network on September 26, 1996. In this episode, George tries to figure out what caused the damage to a briefcase he left behind at a meeting of the Susan Ross foundation, Elaine is attracted to a man based on his professed disinterest in having children, and Jerry and Kramer find themselves on opposing sides of a love triangle.


== Plot ==
The foundation chair, Wyck, clears his throat just before describing Susan's death as an "accident", making George suspect Wyck thinks he murdered Susan. Jerry suggests he put a running tape recorder in his briefcase and leave it behind at the next board meeting so he can hear what the board members say about him in his absence. Following Jerry's advice, George returns to find the briefcase damaged and the tape with a recording of the horrified exclamations "What are you doing?!" and "Dear God!" followed by a loud thud. After struggling to come up with a theory which could explain this, George confronts the board with the recording. Wyck claims Quint was moving a chair and dropped it on the briefcase. He offers no explanation for the horrified cries, but George is satisfied with his story and leaves with his briefcase this time. He thus misses the board members casually commenting that they all believe he murdered Susan.
Elaine insists she has no desire to have a baby. When a man, Kevin, overhears her declaration and seconds it, she starts dating him. However, when Kevin tells her he got a vasectomy, she gets second thoughts about whether she wants to have children.
Kramer falls in love with Jerry's girlfriend Pam, but is restrained by loyalty to Jerry. When Elaine tells Kramer that Jerry is not in love with Pam, he gets the confidence to confront Jerry about his feelings. However, his praises for Pam convince Jerry that she is too good a woman to let go of. Kramer enlists Newman's help; the smooth-talking Newman feeds Kramer romantic lines which enable him to win Pam's heart. When Newman taunts Jerry over this, Jerry convinces him to stop helping Kramer in exchange for advice on wooing his obsession, Elaine. His advice is that Elaine doesn't want children.
Pam tells Jerry and Kramer that she has feelings for them both and can't choose between them. She mentions she doesn't want to have children, so they line up with Newman for their own vasectomies. Elaine and Kevin go to get his vasectomy reversed. Realizing Elaine must have changed her mind about children, Jerry and Newman leave before their scheduled vasectomies, but after Kramer gets his.


== Production ==
The episode is dedicated to Victor Wayne Harris, an assistant prop master for the show who died in August 1996 of complications of a stroke. Harris appears in the episode "The Pilot", in the scene where George is on the phone calling for his test results; right at the end of the scene Harris appears as a man with a white t-shirt and a cap approaching the phone.
In Elaine and Kevin's conversation about his vasectomy, Elaine references the hairstyle she wore in seasons three and four.The scene at the bookstore is an homage to Edmond Rostand's play Cyrano de Bergerac, while the Jerry/Newman chase sequence is an homage to the chase sequences in Bugs Bunny cartoons.


== External links ==
"The Soul Mate" on IMDb
"The Soul Mate" at TV.com


== References ==