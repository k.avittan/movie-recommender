Story Teller (sold as Story Time in Australia and New Zealand) was a magazine partwork published by Marshall Cavendish between 1982 and 1985.


== Publishing history ==


=== The original collection ===
The original Story Teller was released from December 1982 and throughout 1983 as a fortnightly partwork. Each magazine contained a selection of children's stories, some traditional folk tales like "Anansi the Spiderman", some children's tales such as Gobbolino, the Witch's Cat, and some contemporary works written especially for the series, like "Timbertwig". Most issues contained a poem or two, as well. The stories were accompanied by lavish colour artwork, and inside each issue was an offer to purchase custom made binders for the magazine as well as cases to hold the tapes.
Each issue of Story Teller came with a cover-mounted cassette tape containing a reading of the stories, complete with music and sound effects. What set Story Teller apart from other partworks was the stories were read by professional actors and celebrities of the time, including Richard Briers, Sheila Hancock, Derek Jacobi, and Nigel Lambert.
Two distinguishing features of the audio cassettes were the "Story Teller" jingle that introduced and ended each tape and the characteristic "ping" that sounded when the time came to turn the pages to encourage children to read along.  The "Story Teller" jingle is an existing track called "Children's Carnival" by Ted Atking and Alain Feanch.Longer stories were split over multiple issues to encourage parents to buy the next issue. These were referred to as Story Teller Serials. As one serial came to end, another would start. Many of these would be simple two-part stories, but a selection of stories (usually well-known ones such as Peter Pan and the Wizard of Oz) were spread over several issues. Pinocchio was the longest serial, with seven installments.
The original collection was 26 issues long with each tape lasting up to 45 minutes. An exception was issue 26, which was 90 minutes long because it also contained the special preview issue for Story Teller 2, which immediately followed the original series.
(The New Zealand and Australian Story Time only ran for 1 series, so the final Issue 26 was the standard 45 minutes long and did not feature the special preview for the next series. This was the sole difference between its UK counterpart; the cassettes and artwork were otherwise identical. Similarly, the cassette carry case was available in Australia. However, in New Zealand, a smaller box was provided, made out of cardboard wrapped in a red plastic with small domes at the corners joining it all together and a piece of Velcro for the flap on the top).


=== Storyteller 2 ===
Story Teller 2, which was previewed in issue 26 of the original Story Teller series in the UK, continued the tradition of the original by combining traditional and contemporary children's stories. (In New Zealand and Australia, Story Time only ran for 1 series.)


=== Little Story Teller ===
When Story Teller 2 ended, Marshall Cavendish followed it up with another 26-part series, Little Story Teller, which, as its title suggests, was aimed at a younger audience than the original series. Many of the stories in Little Story Teller featured the adventures of the inhabitants of the Magic Mountain, which included Leroy the Lion, Dotty the Dragon, and Morris and Doris the hamsters.


=== Christmas specials ===
Three Christmas specials were also published. Released annually along with each series, the Christmas Story Tellers featured festive stories and even songs. The third Christmas Story Teller included stories suited to both the original series and Little Story Teller. Of the Christmas specials only Christmas Story 2 was made available in New Zealand or Australia, under the title of Christmas Story Time.


=== Story Teller Song Book ===
Christmas Story 3 was widely assumed to be the last title from the Story Teller series but in 1986, Marshall Cavendish released the Story Teller Song Book. The 52-page publication contained 20 all-time sing-along favourites rather than stories but it still retained the Story Teller tradition of featuring colouring and activity pages as well as an accompanying cassette tape.


=== My Big Book of Fairy Tales ===
In 1987, Marshall Cavendish revisited the world of Story Teller by publishing a big hardback book called My Big Book of Fairy Tales. Although the publication lacked the Story Teller branding, it was essentially a compilation of the best stories from Story Teller; it contained 73 stories from the two series and three Christmas issues. The original text and illustrations were used, except for the story "The Frog Prince", which featured new artwork (for no apparent reason). The book was rereleased in 1989 with a different cover and again in 1994. Unlike the partwork, My Big Book of Fairy Tales was not accompanied by a cassette.


=== Availability ===
The partwork is now regarded as highly collectible, and issues can still be found today in secondhand and charity shops, but finding a complete set can be very difficult. Digital copies can also be found on auction sites such as eBay, but these are of dubious legality.


== Stories and readers ==


=== Story Teller 1 ===
Part 1

Gobbolino, the Witch's Cat: Sheila Hancock
The Hare and the Tortoise: Bernard Cribbins
The Shoe Tree: Sheila Hancock
The Emperor's New Clothes: Bernard Cribbins
The Red Nightcaps: Marise Hepworth
Aldo in Arcadia (1): Nigel Lambert & John Brewer
The Forest Troll: Nigel LambertPart 2

The Elves & the Shoemaker: Brian Blessed
Master Tiger: Nigel Lambert
Aldo in Arcadia (2): Nigel Lambert, John Brewer & John Green
The Last Slice of Rainbow: Sheila Hancock
Gobbolino the Ship's Cat: Sheila Hancock
The Greedy Fox: Brian Blessed
Sinbad & the Valley of Diamonds: Brian Blessed
Bring on the Clowns: Nigel LambertPart 3

The Great Big Hairy Boggart: Dermot Crowley
The Owl and the Pussycat: Susannah York
Gobbolino the Knight's Cat: Sheila Hancock
The Lion & the Mouse: Dermot Crowley
Simon's Canal: Susannah York
Hansel and Gretel: Susannah York
Aldo in Arcadia (3): Robert Powell, Susannah York, Nigel Lambert & John Brewer
Child of the Sun: Dermot CrowleyPart 4

Narana's Strange Journey: Roy Hudd
Rhubarb Ted: Nigel Lambert
Gobbolino the Kitchen Cat: Sheila Hancock
Noisy Neighbours: Nigel Lambert
Jester Minute (Part 1): Nigel Lambert
The Princess & the Pea: Tina Jones
The Ant and the Grasshopper: Marian Hepworth
Santa's Early Christmas: Roy HuddPart 5

Timbertwig: George Layton
The Fox and the Crow: Hayley Mills
Drummerboy & the Gypsy: George Layton
Rapunzel: Hayley Mills
Virgil's Big Mistake: Nigel Lambert
Jester Minute (Part 2): Nigel Lambert
'O Here It Is: Hayley MillsPart 6

Beauty and the Beast: Hywel Bennett
Dodo & the Pot of Gold: Patricia Brake
Timbertwig & the Caravan of Surprises: George Layton
The Flying Piggy-Bank: Patricia Brake
The Land of the Bumbley Boo: Patricia Brake
The Moon and the Millpond: Dick Vosburgh
The Friendly Bear: Hywel BennettPart 7

The Billy Goats Gruff: Nigel Pegram
The Snow Queen: Liza Goddard
A Pocketful of Trouble: Nigel Pegram
Little Spook of Spook Hall: Liza Goddard
The Silly Tortoise: Nigel Pegram
Timbertwig Gets a New Hat: George Layton
Faster than Fairies: Liza GoddardPart 8

Dot & the Kangaroo (Part 1: Dot Loses Her Way): Carole Boyd
Oliphaunt: Joss Ackland
The Goose that Laid the Golden Egg: Carole Boyd
The Selfish Giant: Joss Ackland
Jester Minute & the Vanishing castle (1): Nigel Lambert
The Creation of Man: Joss Ackland
Boffy & The Teacher Eater: Nigel LambertPart 9

Abdulla and the Genie: Nigel Lambert
Dot & the Kangaroo (Part 2): Carole Boyd
Jester Minute and the Vanishing Castle (2): Nigel Lambert
The Boy Who Cried Wolf: Robert Powell
Neville Toogood: Carole Boyd
The Pied Piper of Hamelin: Robert PowellPart 10

Gulliver's Travels (Part 1): Joanna Lumley
Dot & the Kangaroo (Part 3): Carole Boyd
Mike's Bike (Part 1): Mick Ford
The Three Wishes: Carole Boyd
David and Goliath: Mick Ford
The Enchanted Horse: Joanna Lumley
Mr. Tom Narrow: Carole BoydPart 11

Gulliver's Travels (Part 2): Joanna Lumley
Pinocchio (Part 1): Ian Lavender
The Dog and the Bone: David Graham
Sleeping Beauty: Joanna Lumley
Walter Spaggot: Nigel Lambert
Growling at Tigers: David Graham
Mike's Bike (Part 2): Mick FordPart 12

The Mighty Prince: Una Stubbs
Ford's Toy Cars (Part 1): George Layton
Drummerboy Races for his Life: George Layton
First Flight: Ian Lavender
The Town Mouse & the Country Mouse: Una Stubbs
Pinocchio (Part 2): Ian Lavender
The Gingerbread Man: Una StubbsPart 13

The Tinder Box: Siân Phillips
Ford's Toy Cars (Part 2): George Layton
Warrior Girl: Floella Benjamin
Pinocchio (Part 3): Ian Lavender
Three Bald Spots: Carole Boyd
The Ugly Duckling: Siân PhillipsPart 14

The Monster in the Labyrinth: Dermot Crowley
Who's Stronger?: Diana Rigg
Pinocchio (Part 4): Ian Lavender
The Old Man of Torbay: Diana Rigg
Scarlet Braces: Dermot Crowley
Grogre the Ogre (Part 1): Nigel Lambert
Cinderella: Diana RiggPart 15

Pandora's Box: Morag Hood
A Fishy Tale: Morag Hood
Pinocchio (Part 5): Ian Lavender
Grogre the Ogre (Part 2): Nigel Lambert
The Parasol: Kay Parks
The Flying Jacket: Lionel Jeffries
The Three Little Pigs: Lionel JeffriesPart 16

Sam's Big Break: Tommy Eylie
The Mango-Seller: Judy Geeson
Hen-Hustler Kluk: Tommy Eylie
Puss in Boots: Judy Geeson
Grogre the Ogre (Part 3): Nigel Lambert
Pinocchio (Part 6): Ian LavenderPart 17

William Tell: Tom Baker
I Saw a Ship a-Sailing: Carole Boyd
Pinocchio (Part 7): Ian Lavender
Anansi and the Fancy Dress Party: Tom Baker
Jojo's Jigsaw Puzzle: Carole Boyd
Can You Keep a Secret?: Carole Boyd
The Lion and the Peacock: Tom BakerPart 18

Heidi (Part 1): Denise Bryer
Father William: Steven Pacey
George and the Dragon: Steven Pacey
The Frog Prince: Gemma Craven
Bubble and Squeek: Steven Pacey
No Mules: Gemma CravenPart 19

Jack & the Beanstalk: Brian Blessed
Why the Giraffe Can't Speak: Carole Boyd
Sinbad and the Amazing Islands: Brian Blessed
The Book of Beasts (Part 1): John Baddeley
Car Attack: Carole Boyd
Heidi (Part 2): Denise Bryer
Hedge's Problem Tree: Carole BoydPart 20

Rumpelstiltskin: Hayley Mills
Heidi (Part 3): Denise Bryer
The Green Maiden of the Lake: Hayley Mills
The Book of Beasts (Part 2): John Baddeley
It Makes a Change: Denise Bryer
Lutra the Otter: Michael Tudor BarnesPart 21

The Bold Little Tailor: Michael Hordern
The Wolf in Sheep's Clothing: Nigel Lambert
Wiser than the Czar: Michael Hordern
Bobbie and the Magic Go-Cart: Nigel Lambert
Heidi (Part 4): Denise Bryer
The Mighty Rabbit: Nigel LambertPart 22

Waldorf's Fantastic Trip (Part 1): Gay Soper
The Midas Touch: Joanna Lumley
A Meal with a Magician: George Layton
Eleven Wild Swans: Joanna Lumley
Timbertwig Catches a Marrow: George Layton
The Human Fly from Bendigo: Joanna LumleyPart 23

Timbertwig's Birthday: George Layton
Waldorf's Fantastic Trip (Part 2): Gay Soper
Goldilocks: Annette Crosbie
Dad and the Cat and the Tree: David Ashford
The Faery Flag: Annette Crosbie
The Runaway Piano: David Ashford
The Little Red Hen: Annette CrosbiePart 24

I Wish, I Wish: Carole Boyd
Counting Chickens: Carole Boyd
A Hedgehog Learns to Fly: John Brewer, John Green and Steven Pacey
The Little Tin Soldier: Ian Holm
Kingdom of the Seals: Ian Holm
Aldo in Arcadia (4): Steven Pacey, Nigel Lambert, John Green & John BrewerPart 25

Little Red Riding Hood: Denise Bryer
The Happy Prince: Tim Curry
Aldo in Arcadia (5): John Brewer, Tina Jones, Nigel Lambert & Steven Pacey
Mr Miacca: Denise Bryer
The Great Pie Contest: Steven Pacey
Stolen Thunder: Tim CurryPart 26

The Goblin Rat: Liza Goddard
Thumbelina: Liza Goddard
Where Can an Elephant Hide?: Steven Pacey
A Lion at School: Liza Goddard
Captain Bones: Dermot Crowley
Aldo in Arcadia (6): John Brewer, Tina Jones, Nigel Lambert & Steven PaceyPart 26 Story Teller 2 Special Preview Issue

The Wind in the Willows: Michael Jayston
Jack-in-the-Box: Steven Pacey
Campbell the Travelling Cat: Una Stubbs
Arthur the Lazy Ant: Steven Pacey
Dragon Child: Una Stubbs
The Magic Porridge Pot: Steven Pacey
The Lobster Quadrille: Una Stubbs


=== Story Teller 2 ===
Part 1

The Wizard of Oz : Miriam Margolyes
The Creatures with Beautiful Eyes : Martin Shaw
The Circus Animal's Strike : David Tate
Yushkin the Watchmaker : Martin Shaw
Rumbles in the Jungles (1) : David Tate
The Dancing Fairies : Miriam Margolyes
There Once Was a Puffin : Martin ShawPart 2

The Magic of Funky Monkey : Gemma Craven
The Snake and the Rose : Gemma Craven
Rumbles in the Jungles (2) : David Tate
The Wizard of Oz: In The Forest : Miriam Margolyes
The Wind in the Willows: The Wild Wood : Michael Jayston
The Troll : Gemma CravenPart 3

The Musicians Of Bremen : Nigel Hawthorne
Little Joe and the Sea Dragon : Dermot Crowley
The Lord of the Rushie River (1) : Denise Bryer
The Wizard of Oz: The Ernerald City : Miriam Margolyes
Party in the Sky : Nigel Hawthorne
Kebeg : Denise Bryer
The Song of the Engine : Nigel HawthornePart 4

Shorty the Statellite and the Lost Rocket : Nigel Lambert
The Wizard of Oz: Quest for the Wicked Witch : Miriam Margolyes
Petrushka : Janet Suzman
The Garden : Nigel Lambert
Master of the Lake : Janet Suzman
The Lord of the Rushie River (2) : Denise Bryer
Riloby-rill : Janet SuzmanPart 5

The Snow Bear : Derek Jacobi
The Inn of Donkeys : Derek Jacobi
Shorty the Statellite and the Brigadier : Nigel Lambert
The Nightingale : Derek Jacobi
Hugo and the Man Who Stole Colours : Nigel Lambert
The Wizard of Oz: A Great And Terrible Humbug : Miriam Margolyes
Recipe : Nigel LambertPart 6

Gobbolino and the Little Wooden Horse (1) : Sheila Hancock
The Wizard of Oz: The Final Journey : Miriam Margolyes
The Farmer, the Tomt and the Troll : Carole Boyd
Shorty The Statellite And The Shooting Star : Nigel Lambert
The Fishing Stone : Carole Boyd
Silly Old Baboon : Nigel LambertPart 7

Traveller Ned : Morag Hood
Gobbolino and the Little Wooden Horse (2) : Sheila Hancock
Little Bear And The Beaver : Ian Lavender
The Ju-Ju Man : Floella Benjamin
A Song for Slug : Ian Lavender
Larkspur Gets Her Wings : Morag Hood
Windy Nights : Ian LavenderPart 8

The Most Beautiful House : Robin Nedwell
Gobbolino and the Little Wooden Horse (3) : Sheila Hancock
The Orchestra That Lost Its Voice : Steven Pacey
Stone Soup : Debbie Arnold
The Man Who Knew Better : Robin Nedwell
How the Polar Bear Became : Debbie Arnold
The Marrog : Steven PaceyPart 9

Diggersaurs (1) : Steven Pacey
Molly Whuppie : Eve Karpf
Young Kate : Eve Karpf
Upside-Down Willie (1) : Steven Pacey
Gobbolino and the Little Wooden Horse (4) : Sheila Hancock
Meeting : Eve KarpfPart 10

Toad of Toad Hall (1) : Richard Briers
Simeom the Sorcerer's Son (1) : George Layton
Anansi and the Python : Ysanne Churchman
Stone Drum : Ysanne Churchman
Upside-Down Willie (2) : Steven Pacey
Gobbolino and the Little Wooden Horse (5) : Sheila Hancock
Hannibal : George LaytonPart 11

Grogre the Golden Ogre (1) : Nigel Lambert
Anya's Garden : Carole Boyd
Miss Priscilla's Secret : Carole Boyd
Simeom the Sorcerer's Son (2) : George Layton
The Tortoises' Picnic : David Adams
Toad of Toad Hall (2) : Richard Briers
Big Gumbo : Carole BoydPart 12

Box of Robbers : Patricia Hodge
The Challenging Bull : Nigel Lambert
Barney's Winter Present : Antonia Swinson
Toad of Toad Hall (3) : Richard Briers
Minnie the Floating Witch : Patricia Hodge
Grogre the Golden Ogre (2) : Nigel Lambert
An Eskimo Baby : Patricia HodgePart 13

Brer Rabbit and the Tar-Baby : Dick Vosburgh
Geordie's Mermaid : Susan Jameson
Grogre the Golden Ogre (3) : Nigel Lambert
Gatecrashers : Susan Jameson
Toad of Toad Hall (4) : Richard Briers
The Princess Who Met the North Wind : Susan JamesonPart 14

Peter Pan (1) : Derek Jacobi
The Scrubs and the Dubs (1) : Windsor Davies
Horace's Vanishing Trick : Gay Soper
The Tumbledown Boy : Gay Soper
The Horn Flute : John Shrapnel
King Ferdinand's Fancy Socks : Gay Soper
The Flower Seller : John ShrapnelPart 15

Peter Pan (2) : Derek Jacobi
Cath's Cradle : Una Stubbs
The Scrubs and the Dubs (2) : Windsor Davies
Willow Pattern : Anthony Jackson
Gary the Greatest : Anthony Jackson
Campbell Finds a Castle : Una Stubbs
A Child's Thought : Una StubbsPart 16

The Thin King and the Fat Cook : Patricia Brake
Peter Pan (3) : Derek Jacobi
Bored Brenda : Patricia Brake
The Swords of King Arthur : Mick Ford
Touching Silver : Patricia Brake
Noggin and the Birds : Oliver Postgate
Goblin Market : Mick FordPart 17

Longtooth's Tale (1) : Steven Pacey
Shubiki's Hat : Eva Haddon
Big Red Head (1) : Ruth Madoc
The Tree that Sang : Steven Pacey
Too Many Buns for Rosie : Eva Haddon
Peter Pan (4) : Derek Jacobi
The Moon : Eva HaddonPart 18

Longtooth's Tale (2) : Steven Pacey
Galldora and the Woods-Beyond : Eve Karpf
Big Red Head (2) : Ruth Madoc
At the Forge : James Bryce
Mouse in the Snow : Eve Karpf
Peter Pan (5) : Derek Jacobi
I Had a Little Nut-Tree : James BrycePart 19

Alice's Adventures in Wonderland (1) : Patricia Hodge
Wonder Wellies : Nigel Lambert
Peter and the Mountainy Men : Carole Boyd
The Treachery of Morgan : Mick Ford
Pat's Piano : Carole Boyd
Danger in the Reeds : Nigel Lambert
My Uncle Paul of Pimlico : Nigel LambertPart 20

Arthur Gives Back His Sword : Mick Ford
Butterflies on the Moon : Geoffrey Matthews
Ginger's Secret Weapons : Cass Allen
Alice's Adventures in Wonderland (2) : Patricia Hodge
A Great Escape : Cass Allen
The Miller and His Donkey : Geoffrey Matthews
Sheep-Dog : Geoffrey MatthewsPart 21

Never Tangle With aA Tengu : Christopher Timothy
Diggersaurs (2) : Steven Pacey
Nothing Like a Bath : Denise Bryer
Alice's Adventures in Wonderland (3) : Patricia Hodge
The Neat and Tidy Kitchen : Denise Bryer
Tommy's Shadow : Steven Pacey
My Mother Said : Denise BryerPart 22

Quest of the Brave : Martin Jarvis
The Captain's Horse : Martin Jarvis
Alice's Adventures in Wonderland (4) : Patricia Hodge
The City of Lost Submarines (1) : David Tate
Nogbad Comes Back : Oliver Postgate
Ostriches Can't Fly : Joanna Wake
The Cottage : Joanna WakePart 23

Cyril Snorkel - The Performing Beast : Denise Bryer
Dorrie and the Witch's Visit : Denise Bryer
What the Smoke Said : George Layton
Alice's Adventures in Wonderland (5) : Patricia Hodge
The City of Lost Submarines (2) : David Tate
Simon Rhymon : George Layton
The Sunlight Falls Upon the Grass : Denise BryerPart 24

Harlequin and Columbine (1) : Leonard Rossiter
The City of Lost Submarines (3) : David Tate
The Kind Scarecrow : Maureen O'Brien
Seadna and The Devil : Anthony Jackson
The Birthday Candle : Maureen O'Brien
Superbabe : Anthony Jackson
Upon My Golden Backbone : Maureen O'BrienPart 25

It Takes Time to Teach a King : Dermot Crowley
Harlequin And Columbine (2) : Leonard Rossiter
Cabbage and the Foxes : Carole Boyd
The Donkey Who Fetched the Sea : Dermot Crowley
Give It to Zico! (1) : Ian Lavender
The Electric Imps : Carole Boyd
If You Should Meet a Crocodile : Carole BoydPart 26

The Mermaid Who Couldn't Swim : Maureen O'Brien
Give It to Zico! (2) : Ian Lavender
Mandy and the Space Race : Maureen O'Brien
Somewhere Safe : Maureen O'Brien
Noggin and the Money : Oliver Postgate
Harlequin and Columbine (3) : Leonard Rossiter


=== Christmas Story Teller ===
Part 1

Bertie's Escapade : Bernard Cribbins
The Chocolate Soldier : Carole Boyd
Timbertwig's Christmas Tree : George Layton
King John's Christmas : Nigel Lambert
Snow White and the seven Dwarfs : Liza Goddard
Boo Ho Ho! : Bernard Cribbins
What Wanda Wanted : Carole Boyd
Aladdin and his Magic Lamp : George Layton
The Great Sleigh Robbery : Nigel Lambert
The First Christmas : Liza GoddardPart 2

Gobbolino's Christmas Adventure : Sheila Hancock
Shorty and the Starship : Nigel Lambert
Mole's Winter Welcome : Richard Briers
Santa's Sunny Christmas : Miriam Margolyes
Good King Wenceslas : Sheila Hancock
Dick Whittington and His Cat : Richard Briers
The Tale of the Little Pine Tree : Miriam Margolyes
The Fairies' Cake : Miriam Margolyes
Grogre and the Giant Nasher : Nigel Lambert
A Christmas Carol : Joss AcklandPart 3
Readers and singers: Derek Griffiths, Carole Boyd, Denise Bryer, Nigel Lambert, Steven Pacey, Claire Hamill, Tom Newman.

Jingle Bells
A Carol for Gobbolino
Leroy Learns to Skate
Snow
Snow Song
Mother Goose
Christmas Fun
Rudolph to the Rescue
Away in a Manger
I Saw Three Ships Come Sailing By
Dotty and the Teddy Bears
Clara and the Nutcracker Doll
O Little Town of Bethlehem
The Surprise Christmas
The Forgotten Toys
Minnie's Dinner Spell
Morris's Christmas Stocking
Hurray for Christmas!


== In other languages ==
Dutch "Luister Sprookjes en Vertellingen"
German "Erzähl mir was"
French "Raconte-moi des histoires"
Italian "I Racconta Storie" and "C'era una volta" (re-edited with CDs instead of cassette tapes)
Greek "Άμπρα Κατάμπρα" (Abracadabra) (re-edited 2008 with CDs instead of cassette tapes)
Spanish "Cuenta Cuentos"
Afrikaans "Storieman"


== Similar partworks ==
Story Teller became such a huge success in the 80s that other publishers released similar partworks, including Fabbri's Once Upon a Time collection and Disney's Storytime series. In addition to "clones" of the Story Teller series, several paperback books containing selections from the actual Story Teller series were released (with accompanying cassettes) in the US, under the title "Look, Listen and Read". These compilations contained stories or themes that related to each other, either by author or content. Examples include The Best of Aesop, The Legend of King Arthur, Jack and the Beanstalk, and Rapunzel.


=== Disney's Storytime ===
The main differentiator between Story Teller and Disney's Storytime was the fact that the latter featured only Disney characters. Storytime hit newsagents' shelves soon after Story Teller proved to be a bestseller. It was published in 24 parts and customised binders and cassette boxes were produced to house the collection (just like Story Teller). Disney movies, such as Snow White and the Seven Dwarfs (1937) and Sleeping Beauty (1959), were serialised. (Note: the Australian and New Zealand versions of Story Teller were published as Story Time - not to be confused with Disney's Storytime series.)


== References ==


== External links ==
There are three dedicated websites:

The Magical World of Story Teller
Story Teller Yahoo! Group