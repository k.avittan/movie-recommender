A romance scam is a confidence trick involving feigning romantic intentions towards a victim, gaining their affection, and then using that goodwill to commit fraud.  Fraudulent acts may involve access to the victim's money, bank accounts, credit cards, passports, e-mail accounts, or national identification numbers; or forcing the victims to commit financial fraud on their behalf. In many instances, a mail-order bride scam will also bait the victim into committing felonies to establish citizenship for the perpetrator.According to FBI IC3 statistics, the crime has been at an alarming rise. Monetary loss in the United States rose from $211 million to $475 million from 2017 to 2019. Number of cases rose from 15372 to 19473 in only two years.
According to Australia government, the crime has also been in the rise in Australia.  Monetary loss in Australia rose from $20.5 million to $28.6 million from 2017 to 2019.


== Stolen images ==

Romance scammers create personal profiles using stolen photographs of attractive people for the purpose of asking others to contact them. This is often known as catfishing. Communications are exchanged between the scammer and victim over a period of time until the scammer feels they have connected with the victim enough to ask for money.
These requests may be for gas money, bus or airplane tickets to visit the victim, medical or education expenses. There is usually the promise the scammer will one day join the victim in the victim's home. The scam usually ends when the victim realizes they are being scammed or stops sending money.


== Internet ==
Criminal networks defraud lonely people around the world with false promises of love and romance. Scammers post profiles on dating websites, social media accounts, classified sites and even online forums to search for new victims. The scammer usually attempts to obtain a more private method of communication such as an email or phone number to build trust with the victim. Scammers prey on the victim's false sense of a relationship to lure them into sending money.
In 2016, the US Federal Bureau of Investigation received reports of more than US $220 million being lost by victims of relationship scams. This was approximately seven times what was stolen through phishing scams and almost 100 times the reported losses from ransomware attacks.


== Known variations ==
Narratives used to extract money from the victims of romance scams include the following:

The scammer says their boss has paid them in postal money orders, and asks the victim to cash the forged money orders and then wire money to the scammer. The bank eventually reverts the money order cash but not the wire transfer.
The scammer says they need the victim to send money to pay for a passport.
The scammer says they require money for flights to the victim's country because they are being held back in their home country by a family member or spouse. In all cases the scammer never comes, or instead says that they are being held against their will by immigration authorities who are demanding bribes.
The scammer says they have had gold bars or other valuables seized by customs and need to pay taxes to before they can recover their items and join the victim in their country.
The scammer meets the victim on an online dating site, lives in a foreign country, falls in love, but needs money to join the victim in his/her country.
The scammer says they are being held against their will for failure to pay a bill or require money for hospital bills.
The scammer says they need the money to pay their phone bills in order to continue communicating with the victim.
The scammer says they need the money for their own or their parents' urgent medical treatment.
The scammer says they need the money to complete their education before they can visit the victim.
The scammer offers a job, often to people in a poor country, on payment of a registration fee. These are particularly common at African dating sites.
The scammer actually is employed directly or indirectly by a website, with a share of the victim's member or usage fees passed on to the scammer.


=== Blackmail ===
Some romance scammers seek out a victim with an obscure fetish and will make the victim think that if they pay for the scammer's plane ticket, they will get to live out their sexual fantasy with the scammer. Other scammers like to entice victims to perform sexual acts on webcam. They then record their victims, play back the recorded images or videos to them, and then extort money to prevent them from sending the recordings to friends, family, or employers, often discovered via social media sites such as Facebook or Twitter.


=== Pro-daters ===
The pro-dater differs from other scams in method of operation; a face-to-face meeting actually does take place in the scammer's country but for the sole purpose of manipulating the victim into spending as much money as possible in relatively little time, with little or nothing in return. The scheme usually involves accomplices, such as an interpreter or a taxi driver, each of whom must be paid by the victim at an inflated price. Everything is pre-arranged so that the wealthy foreigner pays for expensive accommodation, is taken not to an ordinary public café but to a costly restaurant (usually some out-of-the-way place priced far above what locals would ever be willing to pay), and is manipulated into making various expensive purchases, including gifts for the scammer such as electronics and fur coats.The vendors are also typically part of the scheme. After the victim has left, the merchandise is returned to the vendors and the pro-dater and their various accomplices take their respective cut of the take. As the pro-dater is eager to date again, the next date is immediately set up with the next wealthy foreigner.The supposed relationship goes no further, except to inundate the victim with requests for more money after they return home. Unlike a gold digger, who marries for money, a pro-dater is not necessarily single or available in real life.


=== 419 scams ===
Another variation of the romance scam is when the scammer insists they need to marry in order to inherit millions of dollars of gold left by a father, uncle, or grandfather. A young woman will contact a victim and tell him of her plight: not being able to remove the gold from her country as she is unable to pay the duty or marriage taxes. The woman will be unable to inherit the fortune until she gets married, the marriage being a prerequisite of the father, uncle or grandfather's will.
The scammer convinces their victim they are sincere until they are able to build up enough of a rapport to ask for thousands of dollars to help bring the gold into the victim's country. The scammer will offer to fly to the victim's country to prove they are a real person so the victim will send money for the flight. However, the scammer never arrives. The victim will contact the scammer to ask what happened, and the scammer will provide an excuse such as not being able to get an exit visa, or an illness, theirs or a family member.
Scammers are very adept at knowing how to "play" their victims - sending love poems, sex games in emails, building up a "loving relationship" with many promises of "one day we will be married". Often photos of unknown African actresses will be used to lure the victim into believing they are talking to that person. Victims may be invited to travel to the scammer's country; in some cases the victims arrive with asked-for gift money for family members or bribes for corrupt officials, only to be beaten and robbed or murdered.


=== Impersonation of military personnel ===
A rapidly growing technique scammers use is to impersonate American military personnel. Scammers prefer to use the images, names and profiles of soldiers as this usually inspires confidence, trust and admiration in their victims. Military public relations often post information on soldiers without mentioning their families or personal lives, so images are stolen from these websites by organized Internet crime gangs often operating out of Nigeria or Ghana.
These scammers tell their victims they are lonely, or supporting an orphanage with their own money, or needing financial assistance because they cannot access their own money in a combat zone. The money is always sent to a third party to be collected for the scammer. Sometimes the third party is real, sometimes fictitious. Funds sent by Western Union and MoneyGram do not have to be claimed by showing identification if the sender sends money using a secret pass phrase and response. The money and can be picked up anywhere in the world. Some scammers may request Bitcoin as an alternative payment method.


=== Who is most likely to become a victim ===
Sensitive people are more vulnerable to online dating scams, based on a study conducted by the British Psychological Society. Per their results, sensitive and less emotionally intelligent people are more likely to be vulnerable to online dating scams.SCAMwatch, a website run by the Australian Competition and Consumer Commission (ACCC), provides information about how to recognise, avoid and report scams.In 2005, the ACCC and other agencies formed the Australasian Consumer Fraud Taskforce (ACFT). The site provides information about current scams, warning signs and staying safe online.


== Cultural references ==
The Swedish film Raskenstam (1983; alternate title, Casanova of Sweden) is a fictionalized romantic comedy based on the true story of Swedish undertaker Gustaf Raskenstam, who seduced over 100 women and convinced many to support his various projects financially. He usually used newspaper contact ads, often with the headline "Sun and spring", which has become an idiomatic expression in Sweden. The film was directed by Gunnar Hellstrom, written by Hellstrom and Birgitta Stemberg, and executive produced by Hellstrom and Brian Wikstrom.
Several films and television episodes depict the story of Raymond Fernandez and Martha Beck, the American serial killer couple known as "The Lonely Hearts Killers", who are believed to have killed as many as 20 women during their murderous spree between 1947 and 1949. The pair met their unsuspecting victims through lonely hearts ads.The Honeymoon Killers (1969 film)
Deep Crimson (1996 film)
Lonely Hearts (2006 film)
Cold Case: "Lonely Hearts" (season 4, episode 9), airdate 19 November 2006
Alleluia (2014 film)
The 2008 web series SPAMasterpiece Theater featured an episode "Love Song of Kseniya" that centered on a romance scam email spam read by website Boing Boing's Xeni Jardin.


== See also ==

419 scams
Advance-fee scam
Badger game
Catfishing


== References ==


== External links ==
Phishing at Curlie