A gringo (, Spanish: [ˈɡɾiŋɡo], Portuguese: [ˈɡɾĩɡu]) (male) or gringa (female) is someone considered a foreigner from the perspective of Spanish and Portuguese-speaking countries in Latin America. Gringo usually refers to a foreigner, especially from the United States or (to a lesser extent) Canada. Although it is considered an offensive term in some Spanish-speaking countries, in most Spanish-speaking countries and in Brazilian Portuguese, the term simply means "foreign". In English it often carries a derogatory connotation, and some times does so in Spanish and Portuguese. Possible other connotations may include monolingualism, a lack of understanding of Hispanic culture, and blond hair with white skin.According to the Oxford English Dictionary, the first recorded use in English comes from John Woodhouse Audubon's Western Journal of 1849–1850, in which Audubon reports that his party was hooted and shouted at and called "Gringoes" while passing through the town of Cerro Gordo, Veracruz.


== Etymology ==
The word gringo originally referred to any kind of foreigner. It was first recorded in 1787 in the Spanish Diccionario castellano con las voces de Ciencias y Artes:
GRINGOS, llaman en Málaga a los extranjeros, que tienen cierta especie de acento, que los priva de una locución fácil, y natural Castellana; y en Madrid dan el mismo, y por la misma causa con particularidad a los Irlandeses.
Gringos is what, in Malaga, they call foreigners who have a certain type of accent that prevents them from speaking Castilian easily and naturally; and in Madrid they give the same name, and for the same reason, in particular to the Irish.
The most likely theory is that it originates from griego ('Greek'), used in the same way as the English phrase "it's Greek to me". Spanish is known to have used Greek as a stand-in for incomprehensibility, though now less common, such as in the phrase hablar en griego (lit. 'to speak Greek'). The 1817 Nuevo diccionario francés-español, for example, gives gringo and griego as synonyms in this context:
This derivation requires two steps: griego > grigo, and grigo > gringo. Corominas notes that while the first change is common in Spanish (e.g. priesa to prisa), there is no perfect analogy for the second, save in Old French (Gregoire to Grigoire to Gringoire). However, there are other Spanish words whose colloquial form contains an epenthetic n, such as gordiflón and gordinflón ('chubby'), and Cochinchina and Conchinchina ('South Vietnam'). It is also possible that the final form was influenced by the word jeringonza, a game like Pig Latin also used to mean "gibberish".Alternatively, it has been suggested that gringo could derive from the Caló language, the language of the Romani people of Spain,  as a variant of the hypothetical *peregringo, 'peregrine', 'wayfarer', 'stranger'.


=== Folk etymologies ===
There are several folk etymologies that purport to derive the origin of gringo from word coincidences.
Many theories date the word to the Mexican–American War (1846–1848), as a result of American troops singing a song which began with "Green grows..." such as "Green Grow the Rushes, O", "Green Grow the Lilacs", and various others. Other theories involve locals yelling "Green, go!" at invading American soldiers (sometimes in conflicts other than the Mexican–American War), in reference to their supposedly green uniforms. Another derives it from Irish "Erin go bragh" ("Ireland forever"), which served as the motto for Saint Patrick's Battalion who fought alongside the Mexican army.


== Modern usage ==


=== Spain ===
Gringo is only heard among Latin American immigrants or native Spaniards imitating their speech for fun or solidarity.  Guiri is a common colloquial slur for foreigners in Spain. These terms can be merely descriptive, derogatory, or friendly depending on the context and situation.


=== Argentina ===
The word gringo is mostly used in rural areas following the original Spanish meaning. Gringo in Argentina was used to refer to non-Spanish European immigrants who first established agricultural colonies in the country. The word was used for Swiss, German, Polish, Italian and other immigrants, but since the Italian immigrants were the larger group, the word used to be mostly linked to Italians in the lunfardo argot. It has also found use in the intermittent exercise Gringo-Gaucho between Argentine Naval Aviation and USN aircraft carriers.


=== Brazil ===
In Brazil, the word gringo means simply foreigner and has no connection to any physical characteristics or specific countries. Unlike most Hispanic American countries, in which gringo is never used to refer to other Latin Americans, in Brazil, there is no such distinction in the use of the term. Most foreign footballers in the Brazilian Championship that came from other Latin American countries are nevertheless referred as "gringos" by the sport media and by sport fans. Tourists are called gringos, and there is no differentiation in the use of the term for Latin Americans or people from other regions, like Europe.As the word has no connection to physical appearance in Brazil, black African or African American foreigners are also called gringos, unlike some other countries in which the term implies fair skin. Popularly used terms for fair-skinned and blond people are generally based in specific nationalities, like "alemão" (i.e., German), "russo" (Russian) or, in some regions, "galego" (Galician) which are used for both Brazilians and foreigners with such characteristics, regardless of their real ethnic origins.


=== Colombia ===
This term is generally used to refer to people who come from the United States, without any additional connotation; that is, it is not considered derogatory or friendly. The connotation will depend on the context and the person using the word 


=== Activism ===
In 1969, José Ángel Gutiérrez, one of the leaders of the Mexican American Youth Organization, said his and MAYO's use of the term, rather than referring to non-Latinos, referred to people or institutions with policies or attitudes that reflect racism and violence.


== Other uses ==
In Mexican cuisine, a gringa is a flour tortilla with al pastor pork meat with cheese, heated on a comal and optionally served with a salsa de chile (chilli sauce). The name is either a reference to the white flour used, or its creation, when two women from the United States asked a Mexico City taquería for a Mexican dish but disliked corn tortillas.


== See also ==


== Notes ==


== References ==