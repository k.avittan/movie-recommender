Tarnish is a thin layer of corrosion that forms over copper, brass, aluminum, magnesium, neodymium and other similar metals as their outermost layer undergoes a chemical reaction. Tarnish does not always result from the sole effects of oxygen in the air. For example, silver needs hydrogen sulfide to tarnish, although it may tarnish with oxygen over time. It often appears as a dull, gray or black film or coating over metal. Tarnish is a surface phenomenon that is self-limiting, unlike rust. Only the top few layers of the metal react, and the layer of tarnish seals and protects the underlying layers from reacting.
Tarnish actually preserves the underlying metal in outdoor use, and in this form is called patina. The formation of patina is necessary in applications such as copper roofing, and outdoor copper, bronze, and brass statues and fittings. Patina is the name given to tarnish on copper based metals, while Toning is a term for the type of tarnish which forms on coins.


== Chemistry ==

Tarnish is a product of a chemical reaction between a metal and a nonmetal compound, especially oxygen and sulfur dioxide. It is usually a metal oxide, the product of oxidation. Sometimes it is a metal sulfide. The metal oxide sometimes reacts with water to make the hydroxide; and carbon dioxide to make the carbonate. It is a chemical change. There are various methods to prevent metals from tarnishing.


== Prevention and removal ==
Using a thin coat of polish can prevent tarnish from forming over these metals. Tarnish can be removed by using steel wool, sandpaper, emery paper, baking soda or a file to rub or polish the metal's dull surface.  Fine objects (such as silverware) may have the tarnish electrochemically reversed (non-destructively) by resting the objects on a piece of aluminium foil in a pot of boiling water with a small amount of salt or baking soda, or it may be removed with a special polishing compound and a soft cloth. Gentler abrasives, like calcium carbonate, are often used by museums to clean tarnished silver as they cannot damage or scratch the silver and will not leave unwanted residues.


== References ==


== External links ==
Herman Silver Restoration, Conservation & Preservation – the most thoroughly researched and practiced silver care information on the Web.
Common causes of gold tarnishing & prevention.
How to Clean Silver Tarnish at Home.https://www.valerietylercollection.com/blogs/valerie-tyler-collection-blog/tagged/how-to-clean-brass