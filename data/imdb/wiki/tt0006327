Young Romance is a romantic comic book series created by Joe Simon and Jack Kirby for the Crestwood Publications imprint Prize Comics in 1947. Generally considered the first romance comic, the series ran for 124 consecutive issues under Prize imprint, and a further 84 (issues #125-208) published by DC Comics after Crestwood stopped producing comics.


== Background ==

In his introduction to Eclipse Comics' 1988 collection of some of the earliest Simon & Kirby romance comics, Richard Howell writes that, "Romance has always been a major component in entertainment, be it novels, plays, or movies, but for over ten years after the first appearance of comic books, romance only had a token presence in their four-color pages". This changed in 1947 with the return from war of one of comics' earliest and best-known creative partnerships, that of Joe Simon and Jack Kirby, who had already created Captain America, the Boy Commandos and the Newsboy Legion.Working for Hillman Periodicals, the two created a "teen-humor comic book called My Date", cover-dated March 1947, which contained within its pages "ground-breaking" stories concerned with "comparatively faithful depictions of teen-age life, centering especially on romantic experiences and aspirations." Arguably itself the first "romance comic," positive reaction to My Date allowed Simon to negotiate a deal with Crestwood publishers Teddy Epstein and Paul Blyer (or "Bleier") "before the four-issue run of My Date had run more than half its course", and to receive an unheard of 50% share of profits in return for producing their follow-up for that company.


== History ==
Launched with a September 1947 cover date, the Prize Comics title Young Romance signaled its distinction from traditional superhero and genre comics with a cover banner stating the series was "designed for the more adult readers of comics". Told from a first person perspective, underlining its claim to be recounting "true" stories, the title was an instant success, "bec[oming] Jack and Joe's biggest hit in years" and selling "millions of copies" and a staggering 92% of its print run. Crestwood increased the print run by the third issue to triple the initial numbers, and well as upgrade the title from bimonthly to monthly through issues #13-72 (Sept. 1949 - Aug. 1954).

Within a year-and-a-half, Simon & Kirby were launching companion titles for Crestwood to capitalize on the success of this new genre. The first issue of Young Love (Feb. 1949) also sold well with "indistinguishable" content from its parent-title. Further spin-off titles Young Brides (married couples' stories) and In Love ("book-length" stories) also followed from Crestwood/Prize, and were produced by the Simon & Kirby stable of artists and writers. Other companies, including Quality Comics, Fawcett Publications, Fox Features Syndicate, and Timely Comics, capitalized on the romance boom. Despite the glut of titles, the Simon and Kirby Romance titles "continued to sell five million" a month, allowing the pair "to earn more than enough to buy their own homes".The slew of imitators caused Crestwood to adopt the "Prize Group" seal on the covers of the Simon & Kirby produced titles as "the easiest means for readers to tell the S&K-produced love comics from the legions of imitators."


=== Creators ===
"For the first five years", Simon and Kirby produced "at least one story (usually a lengthy lead feature) per issue," but the increased output of the Crestwood/Prize romance titles meant that in many cases they merely oversaw production. They remained "involved with every story", despite not writing or drawing them all, and "maintained a high standard of quality" by employing artists including "Jerry Robinson, Mort Meskin, Bruno Premiani, Bill Draut, Ann Brewster, John Prentice, and Leonard Starr" to work on the title(s). Many of the other artists' output, according to Howell "show the distinctive S&K layout style, and it was not uncommon for a newer artist's work to show signs of S&K retouching".Lettering duties were initially handled almost entirely by Howard Fergeson, while Bill Draut occasionally lettered his own work. After the death of Fergeson, Ben Oda took on "the same herculean task".


=== Covers ===

As with most contemporary romance comics, and the pulps before them, the covers of Young Romance (and all the Simon & Kirby romance output) varied between photographic covers (see above) and regular artwork (typically produced by Simon & Kirby themselves). The photographic covers often depicted film starlets; Young Love Vol. 1, issue #4 for example, featured a cover picture of "then MGM starlet Joy Lansing," which was then reused as the cover for Eclipse Comics' 1988 "Real Love" collection, which reprinted in black and white a number of the Simon & Kirby romance stories, including early work by Leonard Starr, who went on to create the newspaper strip feature Mary Perkins, On Stage.


== Publication history ==
Launched in September 1947, Young Romance ran for 124 issues, until June 1963. Initially bimonthly, strong sales and demand inspired an increased production schedule, and from issue #13 (Sept. 1949) the title became monthly. Continuing to be released monthly for the next five years, the title reverted to bimonthly with status issue #73 (Oct. 1954), and continuing on this schedule for 17 years, missing only one month (August 1963) – when the title switched publishers from Crestwood/Prize to DC Comics, alongside sister publication Young Love. With issue #172 (Aug. 1971), the title returned to monthly release for 20 issues, and between issue #192 (March  1973) and the final issue, #208 (Dec. 1975), the title was again bimonthly.


=== DC Comics ===

Following Crestwood/Prize's Young Romance #124 (June 1963), the Arleigh Publishing division of National Periodical Publications, commonly known as DC Comics, obtained the Crestwood/Prize romance titles Young Love and Young Romance in 1963, upon Crestwood Publications "leav[ing] the comic book business" Larry Nadle succeeded Phyllis Reed as editor. Premiering with Young Romance #125 (Aug. 1963), the pair of titles became "part of a reasonably popular romance line aimed at young girls" for a further 12 years. By DC's 15th issue of Young Romance, the published circulation statement listed sales of 204,613; this gradually dwindled throughout the early 1970s to a published circulation figure of 119,583 by issue #196 (Nov. 1973). Creators who worked on the DC incarnation included writer Steve Englehart. Issues #197 (Jan.-Feb. 1974) to #204 (March–April 1975) of the series were in the 100 Page Super Spectacular format. The series ran through 1975's issue #208 (Nov.-Dec. 1975).
In 2013, DC also published a Valentine's Day special Young Romance: The New 52 Valentine's Day Special #1.


=== Reprints ===
Some Simon & Kirby romance-comics stories, predominantly from Young Romance were reprinted in 1988 by Eclipse Books under the title Real Love (edited, and with an introduction by Richard Howell).
Kirby biographer Greg Theakston has also reprinted some Simon & Kirby romance comics and pages in a number of books on Jack Kirby, while John Morrow's TwoMorrows Publishing has also featured occasional artwork from romance titles in issues of The Jack Kirby Collector.
In 2000, as part of its Millennium Edition reprints of key DC comics, DC Comics reprinted the first issue of Young Romance, even though it (as well as the first issue of MAD magazine) were not originally published by DC.Fantagraphics Books released Young Romance: The Best of Simon & Kirby's Romance Comics in 2012, and Young Romance 2: The Early Simon & Kirby Romance Comics in 2014.


== Notes ==


== References ==


== External links ==
Young Romance at Cover Browser
Young Romance at Don Markstein's Toonopedia
"Jack Kirby: A by-the-month Chronology" compiled by Ray Owens and TwoMorrows