The Wounded Knee Massacre, also known as the Battle of Wounded Knee, was a domestic massacre of nearly three hundred Lakota people, by soldiers of the United States Army. It occurred on December 29, 1890, near Wounded Knee Creek (Lakota: Čhaŋkpé Ópi Wakpála) on the Lakota Pine Ridge Indian Reservation in the U.S. state of South Dakota, following a botched attempt to disarm the Lakota camp. The previous day, a detachment of the U.S. 7th Cavalry Regiment commanded by Major Samuel M. Whitside intercepted Spotted Elk's band of Miniconjou Lakota and 38 Hunkpapa Lakota near Porcupine Butte and escorted them 5 miles (8.0 km) westward to Wounded Knee Creek, where they made camp. The remainder of the 7th Cavalry Regiment, led by Colonel James W. Forsyth, arrived and surrounded the encampment. The regiment was supported by a battery of four Hotchkiss mountain guns.On the morning of December 29, the U.S. Cavalry troops went into the camp to disarm the Lakota. One version of events claims that during the process of disarming the Lakota, a deaf tribesman named Black Coyote was reluctant to give up his rifle, claiming he had paid a lot for it. Simultaneously, an old man was performing a ritual called the Ghost Dance. Black Coyote's rifle went off at that point, and the U.S. Army began shooting at the Native Americans. The Lakota warriors fought back, but many had already been stripped of their guns and disarmed.By the time the massacre was over, more than 250 men, women, and children of the Lakota had been killed and 51 were wounded (4 men and 47 women and children, some of whom died later); some estimates placed the number of dead as high as 300. Twenty-five soldiers also died, and thirty-nine were wounded (six of the wounded later died). Twenty soldiers were awarded the Medal of Honor. In 2001, the National Congress of American Indians passed two resolutions condemning the military awards and called on the U.S. government to rescind them. The Wounded Knee Battlefield, site of the massacre, has been designated a National Historic Landmark by the U.S. Department of the Interior. In 1990, both houses of the U.S. Congress passed a resolution on the historical centennial formally expressing "deep regret" for the massacre.


== Prelude ==

In the years leading up to the conflict, the U.S. government had continued to seize Lakota lands. The once-large bison herds, a staple of the Great Plains indigenous peoples, had been hunted to near-extinction. Treaty promises to protect reservation lands from encroachment by settlers and gold miners were not implemented as agreed. As a result, there was unrest on the reservations.
During this time, news spread among the reservations of a Paiute prophet named Wovoka, founder of the Ghost Dance religion. He had a vision that the Christian Messiah, Jesus Christ, had returned to Earth in the form of a Native American.According to Wovoka, the white invaders would disappear from Native lands, the ancestors would lead them to good hunting grounds, the buffalo herds and all the other animals would return in abundance, and the ghosts of their ancestors would return to Earth—hence the "Ghost Dance". They would then return to earth to live in peace. All this would be brought about by the performance of the slow and solemn Ghost Dance, performed as a shuffle in silence to a slow, single drumbeat. Lakota ambassadors to Wovoka, Kicking Bear and Short Bull taught the Lakota that while performing the Ghost Dance, they would wear special Ghost Dance shirts as seen by Black Elk in a vision. Kicking Bear mistakenly said the shirts had the power to repel bullets.U.S. settlers were alarmed by the sight of the many Great Basin and Plains tribes performing the Ghost Dance, worried that it might be a prelude to armed attack. Among them was the U.S. Indian agent at the Standing Rock Agency where Chief Sitting Bull lived. U.S. officials decided to take some of the chiefs into custody in order to quell what they called the "Messiah craze". The military first hoped to have Buffalo Bill—a friend of Sitting Bull—aid in the plan, to reduce the chance of violence. Standing Rock agent James McLaughlin overrode the military and sent the Indian police to arrest Sitting Bull.
On December 15, 1890, 40 Native American policemen arrived at Sitting Bull's house to arrest him. When Sitting Bull refused to comply, the police used force on him. The Sioux in the village were enraged. Catch-the-Bear, a Lakota, shouldered his rifle and shot Lt. Bullhead, who reacted by firing his revolver into the chest of Sitting Bull. Another police officer, Red Tomahawk, shot Sitting Bull in the head, and he dropped to the ground. He died between 12 and 1 p.m. After Sitting Bull's death, 200 members of his Hunkpapa band, fearful of reprisals, fled Standing Rock to join Chief Spotted Elk (later known as "Big Foot") and his Miniconjou band at the Cheyenne River Indian Reservation.
Spotted Elk and his band, along with 38 Hunkpapa, left the Cheyenne River Reservation on December 23 to journey to the Pine Ridge Indian Reservation to seek shelter with Red Cloud.Former Pine Ridge Indian agent Valentine T. McGillycuddy was asked his opinion of the "hostilities" surrounding the Ghost Dance movement, by General Leonard Wright Colby, commander of the Nebraska National Guard (portion of letter dated January 15, 1891):

"As for the 'Ghost Dance' too much attention has been paid to it. It was only the symptom or surface indication of a deep-rooted, long-existing difficulty; as well treat the eruption of smallpox as the disease and ignore the constitutional disease."
"As regards disarming the Sioux, however desirable it may appear, I consider it neither advisable, nor practicable. I fear it will result as the theoretical enforcement of prohibition in Kansas, Iowa and Dakota; you will succeed in disarming and keeping disarmed the friendly Indians because you can, and you will not succeed with the mob element because you cannot."
"If I were again to be an Indian agent, and had my choice, I would take charge of 10,000 armed Sioux in preference to a like number of disarmed ones; and furthermore agree to handle that number, or the whole Sioux nation, without a white soldier. Respectfully, etc., V.T. McGillycuddy."
"P.S. I neglected to state that up to date there has been neither a Sioux outbreak or war. No citizen in Nebraska or Dakota has been killed, molested or can show the scratch of a pin, and no property has been destroyed off the reservation."


== Fight and ensuing massacre ==

After being called to the Pine Ridge Agency, Spotted Elk of the Miniconjou Lakota nation and 350 of his followers were making the slow trip to the agency on December 28, 1890, when they were met by a 7th Cavalry detachment under Major Samuel M. Whitside southwest of Porcupine Butte. John Shangreau, a scout and interpreter who was half Sioux, advised the troopers not to disarm the Indians immediately, as it would lead to violence. The troopers escorted the Native Americans about five miles westward (8 km) to Wounded Knee Creek where they told them to make camp. Later that evening, Colonel James W. Forsyth and the remainder of the 7th Cavalry arrived, bringing the number of troopers at Wounded Knee to 500. In contrast, there were 350 Lakota: 230 men and 120 women and children. The troopers surrounded Spotted Elk's encampment and set up four rapid-fire Hotchkiss-designed M1875 mountain guns.At daybreak on December 29, 1890, Forsyth ordered the surrender of weapons and the immediate removal of the Lakota from the "zone of military operations" to awaiting trains. A search of the camp confiscated 38 rifles, and more rifles were taken as the soldiers searched the Indians. None of the old men were found to be armed. A medicine man named Yellow Bird allegedly harangued the young men who were becoming agitated by the search, and the tension spread to the soldiers.Specific details of what triggered the massacre are debated. According to some accounts, Yellow Bird began to perform the Ghost Dance, telling the Lakota the falsehood that their "ghost shirts" were "bulletproof". As tensions mounted, Black Coyote refused to give up his rifle; he spoke no English and was deaf, and had not understood the order. Another Indian said: "Black Coyote is deaf," and when the soldier persisted, he said, "Stop. He cannot hear your orders." At that moment, two soldiers seized Black Coyote from behind, and (allegedly) in the struggle, his rifle discharged. At the same moment, Yellow Bird threw some dust into the air, and approximately five young Lakota men with concealed weapons threw aside their blankets and fired their rifles at Troop K of the 7th. After this initial exchange, the firing became indiscriminate.

According to commanding General Nelson A. Miles, a "scuffle occurred between one deaf warrior who had [a] rifle in his hand and two soldiers. The rifle was discharged and a battle occurred, not only the warriors but the sick Chief Spotted Elk, and a large number of women and children who tried to escape by running and scattering over the prairie were hunted down and killed."At first all firing was at close range; half the Indian men were killed or wounded before they had a chance to get off any shots. Some of the Indians grabbed rifles from the piles of confiscated weapons and opened fire on the soldiers. With no cover, and with many of the Indians unarmed, this lasted a few minutes at most. While the Indian warriors and soldiers were shooting at close range, other soldiers used the Hotchkiss guns against the tipi camp full of women and children. It is believed that many of the soldiers were victims of friendly fire from their own Hotchkiss guns. The Indian women and children fled the camp, seeking shelter in a nearby ravine from the crossfire. The officers had lost all control of their men. Some of the soldiers fanned out and finished off the wounded. Others leaped onto their horses and pursued the Natives (men, women, and children), in some cases for miles across the prairies. In less than an hour, at least 150 Lakota had been killed and 50 wounded. Historian Dee Brown, in Bury My Heart at Wounded Knee, mentions an estimate of 300 of the original 350 having been killed or wounded and that the soldiers loaded 51 survivors (4 men and 47 women and children) onto wagons and took them to the Pine Ridge Reservation. Army casualties numbered 25 dead and 39 wounded.


== Eyewitness accounts ==

Thomas Tibbles (1840–1928), journalist:"Suddenly, I heard a single shot from the direction of the troops. Then three or four. A few more. And immediately, a volley. At once came a general rattle of rifle firing then the Hotchkiss guns."
Dewey Beard (Iron Hail, 1862–1955), Minneconjou Lakota survivor: as told to Eli S. Ricker :"... then many Indians broke into the ravine; some ran up the ravine and to favorable positions for defense."
Black Elk (1863–1950); medicine man, Oglala Lakota:"I did not know then how much was ended. When I look back now from this high hill of my old age, I can still see the butchered women and children lying heaped and scattered all along the crooked gulch as plain as when I saw them with eyes still young. And I can see that something else died there in the bloody mud, and was buried in the blizzard. A people's dream died there. It was a beautiful dream.
And I, to whom so great a vision was given in my youth, — you see me now a pitiful old man who has done nothing, for the nation's hoop is broken and scattered. There is no center any longer, and the sacred tree is dead."
American Horse (1840–1908); chief, Oglala Lakota:"There was a woman with an infant in her arms who was killed as she almost touched the flag of truce ... A mother was shot down with her infant; the child not knowing that its mother was dead was still nursing ... The women as they were fleeing with their babies were killed together, shot right through ... and after most all of them had been killed a cry was made that all those who were not killed or wounded should come forth and they would be safe. Little boys ... came out of their places of refuge, and as soon as they came in sight a number of soldiers surrounded them and butchered them there."
Edward S. Godfrey; captain; commanded Co. D of the 7th Cavalry:"I know the men did not aim deliberately and they were greatly excited. I don't believe they saw their sights. They fired rapidly but it seemed to me only a few seconds till there was not a living thing before us; warriors, squaws, children, ponies, and dogs ... went down before that unaimed fire."
(Godfrey was a lieutenant in Captain Benteen's force during the Battle of the Little Bighorn.)
Hugh McGinnis; First Battalion, Co. K, 7th Cavalry:"General Nelson A. Miles who visited the scene of carnage, following a three-day blizzard, estimated that around 300 snow shrouded forms were strewn over the countryside. He also discovered to his horror that helpless children and women with babies in their arms had been chased as far as two miles from the original scene of encounter and cut down without mercy by the troopers. ... Judging by the slaughter on the battlefield it was suggested that the soldiers simply went berserk. For who could explain such a merciless disregard for life? ... As I see it the battle was more or less a matter of spontaneous combustion, sparked by mutual distrust ..."
The Reverend Father Francis M.J. Craft; Catholic Missionary:"…The whole trouble originated through interested whites, who had gone about most industriously and misrepresented the army and its movements upon all the agencies. The Indians, were in consequence alarmed and suspicious. They had been led to believe that the true aim of the military was their extermination. The troops acted with the greatest kindness and prudence. In the Wounded Knee fight the Indians fired first. The troops fired only when compelled to. I was between both, saw all, and know from an absolute knowledge of the whole affair whereof I say." 


== Aftermath ==

Following a three-day blizzard, the military hired civilians to bury the dead Lakota. The burial party found the deceased frozen; they were gathered up and placed in a mass grave on a hill overlooking the encampment from which some of the fire from the Hotchkiss guns originated. It was reported that four infants were found alive, wrapped in their deceased mothers' shawls. In all, 84 men, 44 women, and 18 children reportedly died on the field, while at least seven Lakota were mortally wounded.
Miles denounced Forsyth and relieved him of command. An exhaustive Army Court of Inquiry convened by Miles criticized Forsyth for his tactical dispositions but otherwise exonerated him of responsibility. The Court of Inquiry, however, was not conducted as a formal court-martial.
The Secretary of War concurred with the decision and reinstated Forsyth to command of the 7th Cavalry. Testimony had indicated that for the most part, troops attempted to avoid non-combatant casualties. Miles continued to criticize Forsyth, whom he believed had deliberately disobeyed his commands in order to destroy the Indians. Miles promoted the conclusion that Wounded Knee was a deliberate massacre rather than a tragedy caused by poor decisions, in an effort to destroy the career of Forsyth. This was later whitewashed and Forsyth was promoted to major general.The American public's reaction to the massacre at the time was generally favorable. Many non-Lakota living near the reservations interpreted the battle as the defeat of a murderous cult; others confused Ghost Dancers with Native Americans in general. In an editorial response to the event, the young newspaper editor L. Frank Baum, later the author of The Wonderful Wizard of Oz, wrote in the Aberdeen Saturday Pioneer on January 3, 1891:

The Pioneer has before declared that our only safety depends upon the total extermination of the Indians. Having wronged them for centuries, we had better, in order to protect our civilization, follow it up by one more wrong and wipe these untamed and untamable creatures from the face of the earth. In this lies future safety for our settlers and the soldiers who are under incompetent commands. Otherwise, we may expect future years to be as full of trouble with the redskins as those have been in the past.

Soon after the event, Dewey Beard, his brother Joseph Horn Cloud, and others formed the Wounded Knee Survivors Association, which came to include descendants. They sought compensation from the U.S. government for the many fatalities and injured. Today the association is independent and works to preserve and protect the historic site from exploitation, and to administer any memorial erected there. Papers of the association (1890–1973) and related materials are held by the University of South Dakota and are available for research. It was not until the 1990s that a memorial to the Lakota was included in the National Historic Landmark.
More than 80 years after the massacre, beginning on February 27, 1973, Wounded Knee was the site of the Wounded Knee incident, a 71-day standoff between militants of the American Indian Movement—who had chosen the site for its symbolic value—and federal law enforcement officials.


=== Stranded 9th Cavalry ===
The battalion of 9th Cavalry was scouting near the White River (Missouri River tributary) about 50 miles north of Indian agency at Pine Ridge when the Wounded Knee Massacre occurred, and rode south all night to reach the reservation. In the early morning of December 30, 1890, F, I, and K Troops reached the Pine Ridge agency, however, their supply wagon guarded by D Troop located behind them was attacked by 50 Sioux warriors near Cheyenne Creek (about 2 miles from the Indian agency). One soldier was immediately killed. The wagon train protected itself by circling the wagons. Corporal William Wilson volunteered to take a message to the agency at Pine Ridge to get help after the Indian scouts refused to go. Wilson took off through the wagon circle with Sioux in pursuit and his troops covering him. Wilson reached the agency and spread the alarm. The 9th Cavalry within the agency came to rescue the stranded troopers and the Sioux dispersed. For his actions, Corporal Wilson received the Medal of Honor.


=== Drexel Mission Fight ===

Historically, Wounded Knee is generally considered to be the end of the collective multi-century series of conflicts between colonial and U.S. forces and American Indians, known collectively as the Indian Wars. It was not however the last armed conflict between Native Americans and the United States.
The Drexel Mission Fight was an armed confrontation between Lakota warriors and the United States Army that took place on the Pine Ridge Indian Reservation on December 30, 1890, the day following Wounded Knee. The fight occurred on White Clay Creek approximately 15 miles north of Pine Ridge, where Lakota fleeing from the continued hostile situation surrounding the massacre at Wounded Knee had set up camp.
Company K of the 7th Cavalry—the unit involved at Wounded Knee—was sent to force the Lakotas to return to the areas they were assigned on their respective reservations. Some of the "hostiles" were Brulé Lakota from the Rosebud Indian Reservation. Company K was pinned down in a valley by the combined Lakota forces and had to be rescued by the 9th Cavalry, an African American regiment nicknamed the "Buffalo Soldiers".Among the Lakota warriors was a young Brulé from Rosebud named Plenty Horses, who had recently returned from five years at the Carlisle Indian School in Pennsylvania. A week after this fight, Plenty Horses shot and killed army lieutenant Edward W. Casey, commandant of the Cheyenne Scouts (Troop L, 8th Cavalry). The testimony introduced at the trial of Plenty Horses and his subsequent acquittal also helped abrogate the legal culpability of the U.S. Army for the deaths at Wounded Knee.


=== Winter guards ===
The 9th Cavalry were stationed on the Pine Ridge reservation through the rest of the winter of 1890–1891 until March 1891, lodging in their tents. By then, the 9th Cavalry was the only regiment on the reservation after being the first to arrive in November 1890.


== Medals of Honor ==
For this 1890 campaign, the US Army awarded 20 Medals of Honor, its highest commendation.In the Nebraska State Historical Society's summer 1994 quarterly journal, Jerry Green construes that pre-1916 Medals of Honor were awarded more liberally; however, "the number of medals does seem disproportionate when compared to those awarded for other battles."  Quantifying, he compares the three awarded for the Battle of Bear Paw Mountain's five-day siege, to the twenty awarded for this short and one-sided action.Historian Will G. Robinson notes that, in contrast, only three Medals of Honor were awarded among the 64,000 South Dakotans who fought for four years of World War II.Native American activists have urged the medals be withdrawn, calling them "medals of dishonor". According to Lakota tribesman William Thunder Hawk, "The Medal of Honor is meant to reward soldiers who act heroically. But at Wounded Knee, they didn't show heroism; they showed cruelty." In 2001, the National Congress of American Indians passed two resolutions condemning the Medals of Honor awards and called on the U.S. government to rescind them.A small number of the citations on the medals awarded to the troopers at Wounded Knee state that they went in pursuit of Lakota who were trying to escape or hide.  Another citation was for "conspicuous bravery in rounding up and bringing to the skirmish line a stampeded pack mule."


== Remembrance ==


=== Commemorations of Native American deaths ===

In 1891 The Ghost Shirt, thought to have been worn by one who died in the massacre, was brought to Glasgow, Scotland, by George C Crager, a Lakota Sioux interpreter with Buffalo Bill's Wild West Show. He sold it to the Kelvingrove Museum, which displayed the shirt until it was returned to Wounded Knee Survivors Association in 1998.St. John's Episcopal Mission Church was built on the hill behind the mass grave in which the victims had been buried, some survivors having been nursed in the then-new Holy Cross Mission Church. In 1903, descendants of those who died in the battle erected a monument at the gravesite. The memorial lists many of those who died at Wounded Knee along with an inscription that reads:

"This monument is erected by surviving relatives and other Ogalala and Cheyenne River Sioux Indians in memory of the Chief Big Foot massacre December 29, 1890. Col. Forsyth in command of US troops. Big Foot was a great chief of the Sioux Indians. He often said, 'I will stand in peace till my last day comes.' He did many good and brave deeds for the white man and the red man. Many innocent women and children who knew no wrong died here."

The Wounded Knee Battlefield was declared a U.S. National Historic Landmark in 1965 and was listed on the U.S. National Register of Historic Places in 1966.
Beginning in 1986, the group named "Big Foot Memorial Riders" was formed where they will go to continue to honor the dead. The ceremony has attracted more participants each year and riders and their horses live with the cold weather, as well as the lack of food and water, as they retrace the path that their family members took to Wounded Knee. They carry with them a white flag to symbolize their hope for world peace, and to honor and remember the victims so that they will not be forgotten.


=== Seventh Cavalry Regiment ===
When the 7th Cavalry Regiment returned to duty at Fort Riley from Pine Ridge, South Dakota, the soldiers of the regiment raised money for a monument for members of the regiment killed at Wounded Knee.  About $1,950 was collected, and on July 25, 1893, the monument was dedicated with 5,500 people in attendance. Today, the stone edifice still stands near Waters Hall.


== Popular culture ==


=== Massacre or battle ===

The incident was initially referred to as the "Battle of Wounded Knee".  Some American Indian groups have objected to this description and refer to it as the "Wounded Knee Massacre".  The location of the conflict is officially known as the "Wounded Knee Battlefield". The U.S. Army currently refers to it as "Wounded Knee".


=== Bury my heart at Wounded Knee ===
In his 1931 poem "American Names", Stephen Vincent Benet coined the phrase "Bury my heart at Wounded Knee".  The poem is about his love of American place names, not making reference to the "battle." However, when the line was used as the title of historian Dee Brown's 1970 best-selling book, awareness was raised and Benet's phrase became popularly associated with the incident.
Since the publication of the book, the phrase "Bury my heart at Wounded Knee" has been used many times in reference to the battle, especially in music.
In 1972, Robbie Basho released the song "Wounded Knee Soliloquy" on the album, The Voice of the Eagle.
In 1973, Stuttgart, Germany's Gila released a krautrock/psychedelic folk album by the same name.
In 1992, Beverly (Buffy) Sainte-Marie released her song entitled "Bury My Heart at Wounded Knee", on her Coincidence and Likely Stories album.


=== In other music ===
Artists who have written or recorded songs referring to the battle at Wounded Knee include: Walela "Wounded Knee" from their 1997 self-titled album. Nightwish ("Creek Mary's Blood" from their 2004 album "Once" featuring John Two-Hawks); Manowar ("Spirit Horse Of The Cherokee" from the 1992 album The Triumph Of Steel ); Grant Lee Buffalo ("Were You There?" from the album Storm Hymnal 2001); Johnny Cash (1972's "Big Foot," which is strongly sympathetic); Gordon Lightfoot ("Protocol" from his 1976 album Summertime Dream); Indigo Girls (a 1995 cover of Sainte-Marie's song); Charlie Parr ("1890" on his 2010 album When the Devil Goes Blind); Nik Kershaw ("Wounded Knee" on his 1989 album The Works); Southern Death Cult ("Moya"); The Waterboys ("Bury My Heart"); Uriah Heep; Primus; Nahko and Medicine for the People; Patti Smith; Robbie Robertson; Five Iron Frenzy wrote the 2001 song "The Day We Killed" with mentions of Black Kettle, and quotes Black Elk's account from Black Elk Speaks on their album Five Iron Frenzy 2: Electric Boogaloo; Toad the Wet Sprocket; Marty Stuart; Bright Eyes; and "Pocahontas" by Neil Young. On Sam Roberts' 2006 Chemical City album, the song "The Bootleg Saint" contains line critical of Knee Massacre. There is also a Welsh song called "Gwaed Ar Yr Eira Gwyn" by Tecwyn Ifan on this incident. The song "American Ghost Dance" by the Red Hot Chili Peppers makes extensive reference to the massacre as well.
In 2010, composer Roland Barrett published a song entitled "Ghost Dances" for concert band. It was acclaimed as one of his best works including traditional music and rhythms from the 'Ghost Dance'.
In 1973, the American rock band Redbone, formed by Native Americans Patrick and Lolly Vasquez, released the song, "We Were All Wounded at Wounded Knee". The song ends with the subtly altered sentence, "We were all wounded by Wounded Knee." The song reached the number-one chart position across Europe. In the U.S., the song was initially withheld from release and then banned by several radio stations. Richard Stepp's 2008 Native American Music Awards Native Heart nominated album, The Sacred Journey, has "Wounded Knee" as its final track.


=== In film ===
The massacre has been referred to in films, including Thunderheart (1992), Legends of the Fall (1994), Hidalgo (2004), and Hostiles (2017). The 2005 TNT mini-series Into the West included scenes of the massacre. In 2007, HBO Films released a film adaptation of the Dee Brown bestseller Bury My Heart at Wounded Knee. The 2016 film Neither Wolf Nor Dog has its climax at the massacre site, and was filmed on location there.


=== Other ===
In the 1992 video game Teenage Mutant Ninja Turtles: Turtles in Time, one level is called "Bury My Shell At Wounded Knee." It takes place in 1885 AD, on a train in the Old American West.
In the 1996 DC comic book Saint of Killers, written by Garth Ennis, the main character becomes a surrogate Angel of Death, reaping souls whenever men kill other men violently. The story is set in the 1880s and, near the end of chapter 4, it is said that "four years later" he was called upon at Wounded Knee.
The white supremacist religion known as Creativity openly celebrates the massacre, declaring its anniversary a religious holiday known as “West Victory Day”.
In the 2013 video game BioShock Infinite, several main characters are veterans of Wounded Knee. The protagonist, Booker DeWitt, is haunted by his deeds during the battle and at one point confronts one of his (fictional) superiors from the event.


== Order of battle ==
   
7th U.S. Cavalry
Col James W. Forsyth

Adjutant: 1st Lt. Lloyd S. McCormick
Quartermaster: 1st Lt. Ezra B. Fuller
Assistant Surgeon & Medical Director: Cpt. John Van Rennselaer Hoff
Assistant Surgeon: 1st Lt. James Denver GlennanFirst Squadron
Maj Samuel Whitside

Adjutant: 1st Lt. William Jones Nicholson
Troop A: Cpt. Myles Moylan, 1st Lt. Ernest A. Garlington
Troop B: Cpt. Charles A. Varnum, 1st Lt. John C. Gresham
Troop I: Cpt. Henry J. Nowlan, 2nd Lt. John C. Waterman
Troop K: Cpt. George D. Wallace (k), 1st Lt. James D. MannSecond Squadron
Cpt. Charles S. Isley

Adjutant: 1st Lt. W.W. Robinson II
Troop C: Cpt. Henry Jackson, 2nd Lt. T.Q. Donaldson
Troop D: Cpt. Edward S. Godfrey, 2nd Lt. S.R.J. Tompkins
Troop E: Cpt. Charles S. Isley, 1st Lt. Horatio G. Sickel, 2nd Lt. Sedgwick Rice
Troop G: Cpt. Winfield S. Edgerly, 1st Lt. Edwin P. BrewerBattery E, 1st U.S. ArtilleryCaptain Allyn Capron

2nd Lt. Harry L. Hawthorne (2nd U.S. Artillery)
4 Hotchkiss Breech-Loading Mountain RiflesTroop A, Indian Scouts

1st Lt. George W. Taylor (9th U.S. Cavalry)
2nd Lt. Guy H. Preston (9th U.S. Cavalry)Lakota120 men, 230 women and children


== Gallery ==

		
		
		
		
		
		
		
		
		
		
		


== See also ==
Wounded Knee Incident (1973)
List of events named massacres
Indian massacres in the United States
Genocide of indigenous peoples
History of South Dakota
Plains Indians Wars
List of battles fought in South Dakota
Manifest Destiny
Wounded Knee of Alaska


== Notes ==


== References ==

L. Frank Baum editorial that appeared in the Aberdeen (SD) Saturday Review on January 3, 1891, just five days after the massacre. The author wrote about those terrible "Redskins," his favorite word for Indians. He wrote, "The Pioneer has before declared that our only safety depends upon the total extermination of the Indians. Having wronged them for centuries we had better, in order to protect our civilization, follow it up by one or more wrong and wipe these untamed and untamable creatures from the face of the earth."


== Further reading ==
Andersson, Rani-Henrik. The Lakota Ghost Dance of 1890. Lincoln, NE: University of Nebraska Press, 2009.ISBN 978-0-8032-1073-8.
Brown, Dee. Bury My Heart at Wounded Knee: An Indian History of the American West, Owl Books (1970). ISBN 0-8050-6669-1.
Craft, Francis M. At Standing Rock and Wounded Knee: The Journals and Papers of Father Francis M. Craft, 1888-1890, edited and annotated by Thomas W. Foley, Norman, Oklahoma: The Arthur H. Clark Company (2009). ISBN 978-0-87062-372-1.
Champlin, Tim. A Trail To Wounded Knee : A Western Story. Five Star (2001).
Coleman, William S.E. Voices of Wounded Knee, University of Nebraska Press (2000). ISBN 0-8032-1506-1.
Cozens, Peter. The Earth is Weeping: The Epic Story on the Indian wars for the American West, Atlantic Books (20160) ISBN 978-1-78649-151-0.
Foley, Thomas W. Father Francis M. Craft, Missionary to the Sioux, Lincoln, NE: University of Nebraska Press (2002). ISBN 0-8032-2015-4.
Greene, Jerome A. American Carnage: Wounded Knee, 1890. Norman, OK: University of Oklahoma Press, 2014.
Hämäläinen, Peka. Lakota America: A New History of Indigenous Power, New Haven, CT: Yale University Press (2019). ISBN 978-0-300-21595-3.
Smith, Rex Alan. Moon of Popping Trees. Lincoln, NE: University of Nebraska Press (1981). ISBN 0-8032-9120-5.
Treuer, David. The Heartbeat of Wounded Knee : Native America from 1890 to the Present. New York: Riverhead Books (2019). ISBN 978-1594633157
Utley, Robert M. Last Days of the Sioux Nation. 2nd Edition New Haven, CT: Yale University Press (2004). ISBN 978-0300103168.
Utley, Robert M. The Indian Frontier 1846–1890. Albuquerque, NM: University of New Mexico Press (2003). ISBN 0-8263-2998-5.
Utley, Robert M. Frontier Regulars The United States Army and the Indian 1866–1891. New York: Macmillan Publishing (1973). ISBN 0-8032-9551-0.
Yenne, Bill. Indian Wars: The Campaign for the American West, Westholme (2005). ISBN 1-59416-016-3.


== External links ==
The Wounded Knee Museum in Wall, South Dakota
"Walter Mason Camp Collection," includes photographs from the Battle of Wounded Knee Creek, Brigham Young University
"A Dark Day" – Education Resource, Dakota Pathways
"THE GHOST DANCE.; HOW THE INDIANS WORK THEMSELVES UP TO FIGHTING PITCH", eyewitness account by reporter, New York Times, November 22, 1890
Army at Wounded Knee
Remember the Massacre at Wounded Knee. Jacobin. December 29, 2016.