Puppy love, also known as a crush, is an informal term for feelings of romantic or platonic love, often felt during childhood and adolescence. It is named for its resemblance to the adoring, worshipful affection that may be felt by a puppy.
The term can be used in a derogatory fashion, presuming the affair to be shallow and transient in comparison to other forms of love. Sigmund Freud, however, was far from underestimating the power of early love, recognizing the validity of "the proverbial durability of first loves".


== Characteristics ==
Puppy love is a common experience in the process of maturing. The object of attachment may be a peer, but the term can also describe the fondness of a child for an adult. Most often, the object of the child's infatuation is someone years older, like a teacher, friend of the family, actor, or musician, about whom the child will spend their time daydreaming or fantasizing.A crush is described as a coming-of-age experience where the child is given a sense of individualism because they feel intimate emotions for a person not part of their own family.


== Popular culture ==
Canadian singer Paul Anka wrote and released the single "Puppy Love" in 1960, reaching number 2 in the Billboard Hot 100 and number 33 in the UK singles charts. The remake by Donny Osmond peaked at US number 3 in 1972. Country singer Dolly Parton's first single, released in the 1950s when she was a child, was also called "Puppy Love". American singer Barbara Lewis in January 1964 released her song entitled "Puppy Love". Australian rock band Front End Loader feature the song "Puppy Love" on their 1992 eponymous album. Bow Wow released a song called "Puppy Love" in January 2001. American hip hop artist Brother Ali has also composed a song about puppy love titled "You Say (Puppy Love)".
F. Scott Fitzgerald wrote short stories "valuing the intuitiveness of puppy love over mature, reasoned affection...[its] 'unreal, undesirous medley of ecstasy and peace'".


== See also ==
Attraction
Childhood friend
Childhood sweetheart
Infatuation
Limerence
New relationship energy
Puppy love in China
Teen idol


== References ==