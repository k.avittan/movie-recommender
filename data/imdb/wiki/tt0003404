Star India Private Limited is an Indian media conglomerate and a wholly owned subsidiary of The Walt Disney Company India. It is headquartered in Mumbai, Maharashtra. It has a network of 60 channels in eight languages, reaching out to 9 out of 10 cable and satellite TV homes in India. The network reaches approximately 790 million viewers a month across India and more than 100 countries. Star India generates more than 30,000 hours of content every year.


== History ==


=== Founding ===
Star TV (Satellite Television Asian Region) was founded in 1991 as a joint venture between Hutchison Whampoa and Li Ka-Shing. It was launched as a Pan-Asian beam-to-air Hollywood English-language entertainment channel for Asian audiences.


=== 1990–2000 ===
In 1990, Star TV included Star Plus (then an English-language entertainment channel), Prime Sports, Channel V, Star Movies.
In 1992, Rupert Murdoch’s  News Corporation purchased 63.6% of  STAR for $525 million, followed by the purchase of the remaining 36.4% on 1 January 1993. Star broadcasting operations were run from Rupert Murdoch's Fox Broadcasting premises. Murdoch declared that: (telecommunications) have proved an unambiguous threat to totalitarian regimes everywhere ... satellite broadcasting makes it possible for information-hungry residents of many closed societies to bypass state-controlled television channels
Between 1994 and 1998, Star India was launched, later launched Star Movies, Channel V, and Star News with a limited Hindi offering.In 1998, STAR News was launched as a dedicated news channel with content from NDTV.


=== 2001–2010 ===
In 2001, Star India acquired South India based Vijay TV. In 2003, Star India's deal with NDTV ended and Star News was made into a 24-hour news channel. And it entered into a joint venture (JV) with Anand Bazar Patrika Group to comply with the regulations set for uplinking of news and current affairs channels by the Government of India. It subsequently exited from this joint venture in 2012. After the split the channel was renamed to ABP News and operated by Anand Bazar Patrika Group.In 2004, Star One was launched as a Hindi content channel. In 2008, Star Jalsha, a Bengali language entertainment channel and Star Pravah, a Marathi language entertainment channel were launched.In 2009, Star India acquired Trivandrum, India based media conglomerate Asianet Communications Limited which served Malayalam language content. In August 2009, the Star Group restructured its Asian broadcast businesses into three units – Star India, Star, and Fox International Channels Asia.In the same year, Star Affiliate and CJ Group of South Korea launched CJ Alive (later known as ShopCJ), a 24-hour Indian television shopping channel which used STAR Utsav for hosting the television marketing programs in six-hour slots in its initial stage of launch. Star Affiliate exited the joint venture in May 2014. This venture was called Star CJ.
21st Century Fox launched a film production and distribution business in India through Fox Star Studios India, an affiliate of Star India in the same year.


=== 2011–present ===
In 2012, Star India acquired the BCCI Rights for India cricket for the period 2012 through 2018. ESPN was renamed Star Sports 4, STAR Cricket was renamed Star Sports 3, Star Sports was renamed to Star Sports 1 and Star Sports 2 kept its name. STAR Cricket HD and ESPN HD were renamed to Star Sports HD1 and Star Sports HD2 respectively.In 2015, Star India launched its video-on-demand service, Hotstar (now Disney+ Hotstar) and forayed into online streaming.In 2015, Star India has acquired broadcast businesses of Maa Television Pvt. Ltd to boost its presence in Telugu speaking markets where Star doesn't have any presence before. By this acquisition Star has regional presence in the entire South Indian markets.In February 2017, Star India and global media conglomerate, TED, announced a new TV series, TED Talks India – Nayi Soch. Its programmes starred Bollywood actor Shahrukh Khan and featured newer TED talks made in Hindi language. The programme followed the signature TED format of prominent speakers voicing their opinions in an 18-minute or less monologue in front of a live audience.On 28 August 2017, Star India replaced its Hindi Entertainment channel Life OK with a free-to-air channel Star Bharat.On 5 September 2017, Star India won the global media rights to broadcast Indian Premier League (IPL) for 5 years beginning in 2018. The company bid INR 16,347.50 crore to secure the rights from the previous broadcaster, Sony Pictures Network.On 14 December 2017, The Walt Disney Company announced the acquisition of 21st Century Fox, which included Star India.On 13 December 2018, Disney announced Uday Shankar who serves as chairman of Star India will lead Disney's Asian operations and will become the new chairman of Disney India, which became a wholly owned subsidiary of The Walt Disney Company, being re-organized under The Walt Disney Company (India) Private Limited. On 27 August 2018, the channel Star Life was launched in Africa in English language offering the English dubbed Indian Hindi series from the Indian star channels.On 4 January 2019, Star TV shut down its television operations in USA for promotion of its digital counterpart, Hotstar.On 20 March 2019, Star India became a subsidiary of Disney India as the deal was closed. Now, Star India owns UTV and Disney India TV Channels.


== Business results ==
In August 2012, Business Line of The Hindu group reported that Star CJ Alive is claiming average sale of products worth Rs. 9.5 million on week days and Rs.12 million on weekends. Approximately 65% of the sales come from electronics segment while household segment stands second in the race. In December 2012, some sections of the media reported that Rupert Murdoch run News Corp may exit Star CJ by selling its entire stake to Providence Equity Partners. This share transfer reportedly ran into trouble when the Department of Revenue discovered that funds from other jurisdictions were being channelised to exploit Indo Mauritius double taxation avoidance agreement.STAR India Entertainment provides 51 channels while STAR India Sports provides 15 channels.


== Divisions ==
Star Media Networks
Star Sports
Star Media Networks South
Asianet Star Communications
Star Maa Network
Fox Star Studios
Novi Digital Entertainment
Disney+ Hotstar
Mashal Sports (74%)
Pro Kabaddi league
Indian Super League (40%)
UTV Software Communications
UTV Indiagames
UTV Motion Pictures


== Owned channels ==


=== On air channels ===


=== Defunct channels ===


== Digital ==
In January 2015, Star India launched Hotstar, a mobile and online entertainment OTT platform owned by its affiliate, Novi Digital Entertainment. The platform features content in 9 Indian languages and broadcasts sporting events.In April 2020, the service merged with Disney+ in India forming Disney+ Hotstar. Hotstar operates independently and coexists with Disney+ in Canada, UK and US.


== National Sports Network in India ==
The STAR sports group has planned to build a national sports network in India. The plan includes broadcasting in Indian languages. Starting from regional language Hindi to expansion for South Indian languages Tamil, Kannada, Telugu, Marathi and Malayalam. ESPN was renamed Star Sports 4, Star Cricket was renamed Star Sports 3, Star Sports was renamed to Star Sports 1 and Star Sports 2 kept its name. Star Sports Telugu, Star Cricket HD and ESPN HD renamed to Star Sports HD1 and Star Sports HD2 respectively. Star Sports 3 will be predominantly a Hindi-only channel. STAR plans to start with cricket, field hockey and football (soccer) initially. Domestic cricket of India like Deodhar Trophy and Ranji Trophy are considered. Besides this, two non-professional leagues like the University Cricket and University Field Hockey are also planned to be launched by STAR. In football, Barclays Premier League is also considered for regional broadcasting by STAR.


== Broadcasting rights ==
In September 2014, STAR India acquired a 35% minority stake in Football Sports Development Limited, a joint venture with IMG-Reliance to launch the Indian Super League, a professional
soccer league. In March 2015, STAR India, in association with its affiliate, acquired a 74% majority stake in Mashal Sports Private Limited in March 2015, for the operation of the Pro Kabaddi League.
In January 2015, STAR India launched Hotstar, a new mobile and online entertainment destination for television shows, movies and sports content.
On 4 September 2017, STAR India acquired digital as well as media rights for
Indian Premier League for a term of five years 2018–2023 with a whooping bid of ₹163475 million (US$2.55 billion).
STAR India has acquired media rights for BCCI Domestic and International Cricket Series matches in India through 2023, International Cricket Council matches through 2023, Bangladesh National Cricket Team through 2020, New Zealand Cricket Board through 2020, Premier League Football through 2019, Federation International De Hockey through 2022, Wimbledon through 2022, and Bundesliga through 2020.


== Reception ==
Kyunki Saas Bhi Kabhi Bahu Thi, Kahaani Ghar Ghar Ki, Kasautii Zindagii Kay, Kaun Banega Crorepati aired on Star Plus during 2000s, made the channel the market leader in India becoming the flagship channel of Star India.Disney+ Hotstar, a subsidiary of Star India, censored the Last Week Tonight with John Oliver's episode on Narendra Modi where John Oliver discusses Prime Minister Modi and his Hindu Nationalism as a growing threat to democracy in India.


== See also ==
Star TV (Asian TV networks)
Star China Media
Fox Networks Group


== References ==


== Further reading ==
"Star India, Netflix Part of Global Coalition Fighting Online Piracy". India West. 4 July 2017. Retrieved 24 July 2017.


== External links ==
Official website