James Edward Bond Jr. (January 27, 1933 – April 26, 2012), known as Jimmy Bond, was an American double bass player, arranger and composer who performed and recorded with many leading jazz, blues, folk and rock musicians between the 1950s and 1980s.


== Biography ==
Bond was born in Philadelphia, and learned the double bass and tuba as well as studying orchestration and composition. He attended the Juilliard School between 1950 and 1955.  He also played bass in clubs in Philadelphia, with musicians including Charlie Parker,  Thelonious Monk and Gene Ammons.  After his studies ended, he played regularly with some of the leading jazz musicians of the day, including Chet Baker, Ella Fitzgerald, and Sonny Rollins, and in 1958 started touring with George Shearing.He moved to Los Angeles in 1959.  He became resident bass player at the Renaissance nightclub on Sunset Boulevard, where he played with Ben Webster. Art Pepper, Jim Hall and Jimmy Giuffre, and also recorded with Paul Horn.  From 1962, he became a session musician in Los Angeles.  From then until the early 1970s – and on a more occasional basis until the 1980s – he played on hundreds of recordings covering not only jazz but also rock, pop, folk, and gospel.  He became one of the members of the Wrecking Crew, a group of session musicians often associated with work for Phil Spector.  Other musicians with whom he recorded included Randy Newman, Frank Zappa, Tim Buckley, The Jazz Crusaders, Nina Simone, Lightnin' Hopkins, Jimmy Witherspoon, Linda Ronstadt, Henry Mancini, Lou Rawls, Tony Bennett, and B.B. King.   Increasingly he also worked as an arranger, with producers Nik Venet, David Axelrod and others, as well as composing and arranging advertising jingles.He died in 2012, aged 79, as a result of complications from cardiopulmonary disease.


== Discography ==
With Curtis Amy

Groovin' Blue (Pacific Jazz, 1961) with Frank ButlerWith Earl Anderza

Outa Sight (Pacific Jazz, 1962)With Chet Baker

Chet Baker Sings (Pacific Jazz, 1956)With Louis Bellson

Big Band Jazz from the Summit (Roulette, 1962)With Tim Buckley

Goodbye and Hello (Elektra, 1967)With Terry GibbsThat Swing Thing! (Verve, 1961)With Joe Gordon

Lookin' Good! (Contemporary, 1961)With Lightnin' Hopkins

Lightnin' Strikes (Verve Folkways, 1966)
Something Blue (Verve Forecast, 1967)With Paul Horn

Something Blue (HiFi Jazz, 1960)
The Sound of Paul Horn (Columbia, 1961)With The Jazz Crusaders

Freedom Sound (Pacific Jazz, 1961)
Lookin' Ahead (Pacific Jazz, 1962)
The Festival Album (Pacific Jazz, 1966)With Irene Kral

Wonderful Life (Mainstream, 1965)With Julie London

Feeling Good (Liberty, 1965)With Brownie McGhee and Sonny Terry

Down South Summit Meetin' (World Pacific, 1960) with Lightnin' Hopkins and Big Joe Williams
A Long Way from Home (BluesWay, 1969)
I Couldn't Believe My Eyes (Bluesway, 1969 [1973])With Frank Morgan

Frank Morgan (Gene Norman Presents, 1955)With Gerry Mulligan

If You Can't Beat 'Em, Join 'Em! (Limelight, 1965)
Feelin' Good (Limelight, 1966)With Nina Simone

Little Girl Blue (Bethlehem, 1959)
Nina Simone and Her Friends (Bethleham, 1959)With Art Pepper

Smack Up (Contemporary, 1960)
Intensity (Contemporary, 1960)With Jim Sullivan

U.F.O. (Monnie, 1969)With Gerald Wilson 

You Better Believe It! (Pacific Jazz, 1961)
Moment of Truth (Pacific Jazz, 1962)With Jimmy Woods

Awakening!! (Contemporary, 1962)


== References ==