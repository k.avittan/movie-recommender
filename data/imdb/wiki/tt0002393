A non-commissioned officer (NCO) is a military officer who has not earned a commission. Non-commissioned officers usually obtain their position of authority by promotion through the enlisted ranks. (Non-officers, which includes most or all enlisted personnel, are of lower rank than any officer.) In contrast, commissioned officers usually enter directly from a military academy, Officer Candidate School (OCS), or Officer Training School (OTS) after receiving a post-secondary degree.
The NCO corps usually includes many grades of enlisted, corporal and sergeant; in some countries, warrant officers also carry out the duties of NCOs. The naval equivalent includes some or all grades of petty officer. There are different classes of non-commissioned officers, including junior non-commissioned officers (JNCO) and senior (or staff) non-commissioned officers (SNCO).


== Function ==
The non-commissioned officer corps has been referred to as "the backbone" of the armed services, as they are the primary and most visible leaders for most military personnel. Additionally, they are the leaders primarily responsible for executing a military organization's mission and for training military personnel so they are prepared to execute their missions. NCO training and education typically includes leadership and management as well as service-specific and combat training.
Senior NCOs are considered the primary link between enlisted personnel and the commissioned officers in a military organization. Their advice and guidance are considered particularly important for junior officers and in many cases to officers of all senior ranks.


== National usage ==


=== Australia ===
In the Australian Army, lance corporals and corporals are classified as junior NCOs (JNCOs), sergeants and staff sergeants (currently being phased out) are classified as senior NCOs (SNCOs) and warrant officer class two and warrant officer class one are classified as warrant officers (WOs).
In the New South Wales Police Force, NCOs perform supervisory and coordination roles.  The ranks of probationary constable through to leading senior constable are referred to as "constables".  All NCOs within the NSW Police are given a warrant of appointment under the Commissioner's hand and seal.
All officers within the Australian Defence Force Cadets are non-commissioned. ADFC officers are appointed by the Director-General of their respective branch.


=== Brazil ===
In Brazil, a non-commissioned officer is called "Graduado" and includes the ranks from corporal to sub-lieutenant (or sub-officer in Brazilian Air Force), the latter being equivalent to warrant officers.


=== Canada ===
In the Canadian Forces, the Queen's Regulations and Orders formally defined a non-commissioned officer as "A Canadian Forces member holding the rank of Sergeant or Corporal." In the 1990s, the term "non-commissioned member" (NCM) was introduced to indicate all ranks in the Canadian Forces from recruit to chief warrant officer.By definition, with the unification of the Canadian Forces into one service, the rank of sergeant included the naval rank of petty officer 2nd class, and corporal includes the naval rank of leading seaman; corporal also includes the appointment of master corporal (naval master seaman).
NCOs are officially divided into two categories: junior non-commissioned officers, consisting of corporals/leading seamen and master corporals/master seamen; and senior non-commissioned officers, consisting of sergeants and petty officers 2nd class. In the Royal Canadian Navy, however, the accepted definition of "NCO" reflects the international use of the term (i.e. all grades of petty officer).
Junior non-commissioned officers mess and billet with privates and seamen; their mess is usually referred to as the junior ranks mess. Conversely, senior non-commissioned officers mess and billet with warrant officers; their mess is normally referred to as the warrant officers and sergeants mess (army and air force establishments) or the chiefs and petty officers mess (naval establishments).
As a group, NCOs rank above privates and below warrant officers. The term "non-commissioned members" includes these ranks.


=== Finland ===
In the Finnish Defence Force, NCO's (aliupseeristo) includes all ranks from corporal (alikersantti, lit. sub-sergeant) to sergeant major (sotilasmestari, lit. soldier master). Ranks of lance corporal (korpraali) and leading seaman (ylimatruusi) are considered not to be NCO ranks. This ruling applies to all branches of service and also to the troops of the Border Guard.


=== France ===
In France, Belgium and most former French colonies, the term sous-officier
(meaning: "lower officer" or "sub-officer") is a class of ranks between the rank-and-file (hommes du rang) and commissioned officers (officiers). Corporals (caporal and caporal-chef) belong to the rank-and-file. Sous-officiers include two subclasses: "subalternes" (sergents and sergents-chefs) and "supérieurs" (adjudants, adjudants-chefs and majors). "Sous-officiers supérieurs" can perform various functions within a regiment or battalion, including commanding a platoon or section.


=== Germany ===
In Germany and German-speaking countries like Austria, the term Unteroffizier (with the quite literal translation into English as "under"(unter)  "officer"(offizier), thus "under officer", with both words being the progenitors of their English equivalents, and, in use, having the meaning of "lower officer" or sub-officer) describes a class of ranks between normal enlisted personnel (Mannschaften or in Austria Chargen) and officers (Offiziere). In this group of ranks there are, in Germany, two other classes: Unteroffiziere mit Portepee (with sword-knot) and Unteroffiziere ohne Portepee (without swordknot), both containing several ranks, which in Austria would be Unteroffiziere (NCOs) and Höhere Unteroffiziere (senior NCOs or literally translated as "higher under officers").


=== Ireland ===
In Ireland, the Irish Defence Forces have a professional body of non-commissioned officers from the ranks of Corporal (Cpl) (OR-4) to Regimental Sergeant Major (RSM) (OF-9) in the Irish Army and Air Corps. 
In the Irish Naval Service the NCO ranks rise from Leading Hand or Leading Rate (OR-4) to Warrant Officer (OR-9). In Irish Naval Service parlance the rate or rating is the service members specialisation. Therefore L/S refers to Leading Seaman, L/RRT refers to Leading Radio Radar Technician and so on.
Further to the distinctions within the Irish Defence Forces you have Junior and Senior NCOs. Junior NCOs are Corporals and Sergeants, (OR-4 and OR-5), and Senior NCOs are Company Sergeant and Company Quartermaster Sergeants to Regimental Sergeant Majors and Regimental Quartermaster Sergeants (OR-7 to OR-9). In the Irish Naval Service this further complicated by having Junior and Senior Ratings. Junior ratings from Ordinary Rate (OR-1) to Leading Rate (OR-4), and Senior Ratings are from Petty Officer (OR-5) to Warrant Officer (OR-9). Therefore, it can occur that a person incorrectly describes themselves as a Senior NCO when in fact they mean a Senior Rating.


=== New Zealand ===
In the New Zealand Defence Force, a non-commissioned officer is defined as:

"(a) In relation to the Navy, a rating of warrant officer, chief petty officer, petty officer, or leading rank; and includes—
(i) A non-commissioned officer of the Army or the Air Force attached to the Navy; and
(ii) A person duly attached or lent as a non-commissioned officer to or seconded for service or appointed for duty as a non-commissioned officer with the Navy:
(b) In relation to the Army, a soldier above the rank of private but below the rank of officer cadet; and includes a warrant officer; and also includes—
(i) A non-commissioned officer of the Navy or the Air Force attached to the Army; and
(ii) A person duly attached or lent as a non-commissioned officer to or seconded for service or appointed for duty as a non-commissioned officer with the Army:
(c) In relation to the Air Force, an airman above the rank of leading aircraftman but below the rank of officer cadet; and includes a warrant officer; and also includes—
(i) A non-commissioned officer of the Navy or the Army attached to the Air Force; and
(ii) A person duly attached or lent as a non-commissioned officer to or seconded for service or appointed for duty as a non-commissioned officer with the Air Force:" – Defence Act 1990, Sect 2 (Interpretation)


=== Norway ===
On 1 January 2016, the Norwegian Armed Forces reintroduced non-commissioned officers in all service branches, having had a single rank tier since 1930, except for certain technical and maintenance units from 1945 to 1975. The NCOs are called specialists, and rank from sergeant to sergeant major (NATO ranks OR5–OR9). The Specialist Corps lance corporal and corporal ranks (OR2–OR4) are reserved for enlisted personnel, while the rank of private (OR1) is for conscripts only.
The NCOs are in charge of military training, discipline, practical leadership, role modelling, unit standards and mentoring officers, especially juniors. Officers commanding platoons and above are assigned a chief or master sergeant, which is the unit's highest ranking specialist, although chief and master sergeants are functions and not ranks in themselves.
Norway took a top-down approach to establishing the Specialist Corps. Since August 2015, volunteer commissioned officers have converted into sergeant majors, command sergeants and first sergeants.


=== Singapore ===
In the Singapore Armed Forces, the term "non-commissioned officer" is no longer officially used, being replaced with Specialist for all ranks from 3rd Sergeant to Master Sergeant (Staff and Master Sergeants are known as Senior Specialists). The term used to address Warrant Officers and Specialists combined is "WOSpec". The term "NCO" however is still frequently used unofficially in the army.


=== Sweden ===
In 1983 the NCO corps, since 1972 called the Platoon Officer Corps, was disbanded and its members were given commissions as officers in ranks of second or first lieutenant in Sweden's new one-tier military leadership system. In 2009 a similar system as the NCO corps was re-established, called "specialist officers". Direct recruitment from civilian life is followed by basic and preparatory leadership training, and advanced leadership training during 1.5 year as a specialist cadet at the military academy in Halmstad, a warrant as an OR-6, followed by specialist technical training. Swedish specialist officers have relative ranks that match those of the commissioned officers; an OR-7 takes precedence over a second lieutenant, for instance.


=== United Kingdom ===

In the British Armed Forces, NCOs are divided into two categories.  Lance corporals (including lance bombardiers) and corporals (including lance sergeants, bombardiers, and lance corporals of horse) are junior NCOs.  Sergeants (including corporals of horse), staff sergeants (including colour sergeants and staff corporals), and RAF chief technicians and flight sergeants are senior NCOs.
Warrant officers are often included in the senior NCO category, but actually form a separate class of their own, similar in many ways to NCOs but with a royal warrant. Senior NCOs and WOs have their own messes, which are similar to officers' messes (and are usually known as sergeants' messes), whereas junior NCOs live and eat with the unranked personnel, although they may have a separate corporals' club to give them some separate socialising space.
The Royal Navy does not refer to its petty officers and chief petty officers as NCOs, but calls them senior ratings (or senior rates). Leading ratings and below are junior ratings.


=== United States ===

In the Army, Air Force, and Marine Corps, all ranks of sergeant are termed NCOs, as are corporals in the Army and Marine Corps. A Marine Corps lance corporal (E-3) is not  an NCO, but rather junior enlisted. The rank of corporal (E-4) in the Army and Marine Corps is a junior NCO, and is to be shown the same respect as any other NCO. In the Air Force, E-5 (staff sergeant) and E-6 (technical sergeant) are classified under the NCO tier, while E-7 (master sergeant), E-8 (senior master sergeant), and E-9 (chief master sergeant) are considered senior non-commissioned officers (SNCOs).  In the Navy and Coast Guard, all ranks of petty officer are so designated. Junior NCOs (E-4 through E-6 grade), or simply "NCOs" (E-4 and E-5 only) in Marine Corps usage, and function as first-tier supervisors and technical leaders.
NCOs serving in the top three enlisted grades (E-7, E-8, and E-9) are termed senior non-commissioned officers (chief petty officers in the Navy and Coast Guard). Senior NCOs are expected to exercise leadership at a more general level. They lead larger groups of service members, mentor junior officers, and advise senior officers on matters pertaining to their areas of responsibility.
Within the Marine Corps, senior NCOs are referred to as staff noncommissioned officers (SNCOs) and also include the rank of staff sergeant (E-6). SNCOs are those career Marines serving in grades E-6 through E-9 and serve as unit leaders and supervisors, primary assistants and technical advisors to officers, and senior enlisted advisors to commanding officers, commanding generals, and other higher-level commanders. The ranks include staff sergeant, gunnery sergeant (E-7), master sergeant / first sergeant (E-8), and master gunnery sergeant / sergeant major (E-9).
The title of superintendent is used by the Air Force as the title of the non-commissioned officer in charge (NCOIC) of a section, flight, squadron, group, staff agency, directorate, or similar organization. These positions are assigned to senior non-commissioned officers (SNCOs), as opposed to the titles "NCOIC" and "chief" (which are held by junior NCOs). The titles of commander and director are used for commissioned officers assigned as commanding officer of a unit or the head of a staff agency, directorate, or similar organization, respectively.
A select few senior NCOs in paygrade E-9 serve as "senior enlisted advisors" to senior commanders in each service (e.g., major command, fleet, force, etc.) and in DoD (unified commands, e.g., United States Strategic Command, United States European Command, United States Pacific Command, etc., and DoD agencies, e.g. the Defense Information Systems Agency, Defense Intelligence Agency and the National Security Agency. One senior E-9, selected by the service chief of staff, is the ranking NCO/PO in that service, holds the highest enlisted rank for that service, and is responsible for advising their service secretary and chief of staff.
One E-9 holds a similar position as the SEA to the Chairman of the Joint Chiefs of Staff. Senior enlisted advisors, service enlisted advisors and the SEA to the Chairman advise senior officer and civilian leaders on all issues affecting operational missions and the readiness, utilization, morale, technical and professional development, and quality of life of the enlisted force.
Warrant officers in the United States Armed Forces are considered specialty officers and fall in between enlisted and commissioned officers. US warrant officers also have their own tier and paygrade. However, when US warrant officers achieve the rank of chief warrant officer (CWO2) or higher, they are commissioned and are considered commissioned US officers just like any other commissioned officer, but are still held in a different paygrade tier.


==== U.S. Army NCO Candidate Course ====
Beginning in 1967 at Fort Benning, Georgia, the US Army Noncommissioned officer candidate course (NCOC) was a Vietnam-war era program developed to alleviate shortages of enlisted leaders at squad and platoon level assignments, training enlisted personnel to assume jobs as squad leaders in combat.Based loosely on the Officer Candidate School (OCS), NCOC was a new concept (at the time) where high performing trainees attending basic infantry combat training were nominated to attend a two-phased course of focused instruction on jungle warfare, and included a hands-on portion of intense training, promotion to sergeant, and then a 12-week assignment leading trainees going through advanced training.Regular Army soldiers who had received their promotion through traditional methods (and others) used derisive terms for these draftees (typically) who were promoted quicker, such as "Instant NCOs", "Shake 'n' Bake", and "Whip n' Chills".The program proved to be so successful that as the war began to wind down they elected to institutionalize training noncommissioned officers and created the NCO Education System (NCOES), which was based around the NCO candidate course. The NCO candidate course generally ended 1971–1972.


==== U.S. Navy Accelerated Advancement ====
Within the U.S. Navy there are different ways that a sailor can earn accelerated advancement to the rank of petty officer third class. If a person tests high enough on their entrance exam they are able to select certain jobs that require a significant amount of training, far greater than the amount required for a basic job (12 months vs. 2 weeks). Because these jobs are more technically advanced, the schools have higher attrition rates, demand more responsibility, and require longer initial enlistments, these sailors are able to advance to petty officer third class. Another way for a sailor to earn accelerated advancement is by graduating in the top 10% of their class within their "A" school. For certain ratings, such as Corpsman, this has been discontinued.


== See also ==
Comparative military ranks
Military ranks
Noncommissioned officer's creed


== References ==


== External links ==
The Center for Advanced Studies of the U.S. Army Noncommissioned Officer (United States)
A Short History of the NCO (United States)
History of noncommissioned officers (United States)
Educating Noncommissioned Officers: The history (United States)
The United States Army Non-Commissioned officer's guide Field Manual FM7-22.7 (United States)