Mary Ann Jackson (January 14, 1923 – December 17, 2003) was an American  child actress who appeared in the Our Gang short subjects series from 1928 to 1931. She was a native of Los Angeles, California.


== Career ==
Mary Ann Jackson's film career began under the shadow of her actor relatives, mother Charlotte Jackson (1891–1992) and older sister, "Peaches" Jackson (1913–2002). Peaches had a fairly prolific stint as a child actress, working with such stars as Rudolph Valentino and DW Griffith in full-length features.
Jackson made her film debut in a 1925 Ruth Taylor short, Dangerous Curves Behind. Her first big break came with the role of Baby Smith in the comedy short series The Smiths.


=== Our Gang ===
Jackson joined the Our Gang cast in 1928, at the tail end of the silent era. Often used as the second female lead or the spunky older sister of "Wheezer" (Bobby Hutchins), Mary Ann's snappy delivery came in handy during the series' somewhat rocky transition to sound. With her bob hairstyle and freckles, tomboyish Mary Ann was a vast departure from the winsome miniature heroines who would populate the series before and after her tenure.

Jackson left the Our Gang series in 1931, at age 8, and appeared in a few two-reel Mickey McGuire comedies; some attempts were made to ease her into features, but, as she said in 1990: "I wasn’t a girlie girl type. I didn’t fit the mold so I was discarded like a piece of rubbish."
Jackson felt that children distorting themselves for a part in a short or feature was "sick". She told her mother: "This is not for me. I don't want the responsibility or the rejection. I'm not an actress, I'm not talented, leave me alone, let me get on with my life!"In 1933, longtime Our Gang director Robert McGowan wrote in an Los Angeles Times feature that they normally preferred to cast children who had no previous acting experience, but Jackson was a rare exception: "Mary Ann proved a real find and was my ideal little gang leading lady. She wasn't pretty, but she was intelligent and willing and had just a touch of pathos in her makeup. She proved a rare find for me."


== Later life ==
Jackson eventually took a job at the May Company in downtown Los Angeles, and made only a few brief forays into acting. In 1941, she doubled for Edith Fellows.
Mary Ann Jackson married at age 20, but was widowed. She married again, and spent many years with her second husband and her two children in the San Fernando Valley. Later in life, Mary Ann (dressed in '50s' attire) and her sister Peaches would stop by Santa Monica bars and drink vodka martinis.
Upon recounting her Our Gang years, she laughed at the many women who laid false claim to being Mary Ann Jackson. They often cited their Louise Brooks-style bangs, which she loathed and had chopped off as soon as she left her acting career.
In 1990, she was delighted and amused to learn that during their stint with Our Gang, Jackie Cooper had been "desperately in love with her". She did have fond regard for her days at the Hal Roach Studios and was impressed with her large fan following. Asked to recall her thoughts on the gang, she responded, "Everything I have to say about [Hal] Roach and the gang is 'nice'. There's not one bad thing I can think of. It was just fun and fun and fun."


== Death ==
Mary Ann Jackson died of a heart attack at her home in Los Angeles on December 17, 2003.


== References ==


== Further reading ==
Maude Robinson Toombs (June 1927). "She Walked Through Custard to Fame; Being the complete life-history of Mary Ann Jackson, the pug-nosed little four-year-old star of the 'Jimmy Smiths' comedy series". Cinema Art. VI: 25, 48.


== External links ==
Mary Ann Jackson on IMDb
Mary Ann Jackson at AllMovie
Mary Ann Jackson at Find a Grave