Ruby Gloom is a Canadian flash-animated television series based on the Mighty Fine apparel line of the same name created by illustrator Martin Hsu. The series was produced by Nelvana and began airing on October 13, 2006 in Canada on the network YTV. It features the voices of Sarah Gadon, Stacey DePass, Emily Hampshire, Peter Keleghan, Scott McCord, David Berni, Jeremy Harris, and Adrian Truss.


== History ==


=== Franchise ===
In 2001, Ruby Gloom began as a drawing on a piece of paper by illustrator Martin Hsu and was then spawned into a franchise by the American company Mighty Fine three years later. Ruby Gloom began as a stationery line, and was featured on pencil cases, backpacks, clothing, key chains, and plush toys which were sold through Doeworld, a subsidiary of Mighty Fine. Books were also made for the series, the first being Ruby Gloom's Keys to Happiness in 2004, as well as two calendars for 2004 and 2005. Ruby Gloom was depicted as a small girl with bright red hair, who wears a black dress and red and yellow striped socks. She has a pet cat named Doom Kitty, and was described as "The Happiest Girl in the World." Ruby was originally marketed to the goth subculture, but was later adapted for kids. The product line had an interactive website where users could chat in a forum, and click around for free things such as desktop icons and screensavers. The words: "Ruby Gloom is happy in her own way. Even though she is frowning, to her, a frown is only a smile upside down. In fact, Ruby is so happy, she could just die" greeted viewers as they entered the website. Towards the end of 2004 the website changed to forum only dubbed "The Mansion" By the end of 2004 the website had been redesigned and featured an interactive mansion to explore and user submitted artwork. Ruby is shown on the roof with her cat Doom Kitty and was given a smile for the first time. By 2006 "The Mansion" forum was removed, and the website had taken on a format aimed at kids.


=== Television series ===
In early 2005, Martin Hsu sold the rights to Ruby Gloom to Canadian animation studio Nelvana. In May 2005, the series was announced on the 2005 issue of Animation Magazine.


== Main characters ==


=== Ruby Gloom ===
Ruby is a young girl who can find happiness in anything. "Look on the bright side" is her mantra. There isn't a single thing in the world that gets her down or a single negative that she can't turn into a positive. Her positive attitude allows her to act as the voice of reason in the mansion when her friends lose focus on real issues. Ruby has a crush on one of her friends, Skull Boy, and nearly reveals her feelings to him in the episode "Sunny Daze", when the two of them are trapped in a cave. Her positive outlook also makes Ruby very loyal to her friends. She likes to write in her diary and knit. Voiced by Sarah Gadon.


=== Doom Kitty ===
As Ruby's pet cat, Doom Kitty has a complete free run of the mansion and is therefore usually the only one with total knowledge of what is going on in any given area of the house. This keeps her one step ahead of Ruby and the others most of the time, because whenever something strange happens in the mansion, Doom will usually have all the facts already while everyone else is still trying to figure things out. Unfortunately, since Doom can't talk, her only hope of communicating with the others is through mime with violin noises. Ruby usually has an easy time understanding her, but it doesn't always work. This shows that Doom hates it when she is trying to communicate, and her friends think that she is playing charades. She sometimes tries to warn Scaredy Bat about Boo Boo's pranks.


=== Iris ===
Like Ruby, Iris goes through life without a care in the world, but in a much different way. Where Ruby finds happiness in anything, Iris finds adventure. From safaris to fighting wild animals to even shooting herself out of a cannon, there isn't a single thing this cyclops girl won't try at least once, as long as she thinks it's fun. Unfortunately, Iris's adventuresome nature also makes her quite impulsive and she has a bad habit of jumping into a situation without considering potential consequences. This usually results in trouble she doesn't expect. However, as soon as the trouble is gone, she's off again on another adventure. Voiced by Stacey DePass.


=== Misery ===
Misery doesn't mean to be a walking disaster area. Her mishaps are usually a combination of clumsiness and extraordinary bad luck. Evidently, these traits run in her family, as Misery has had relatives who have been present at every single major disaster in recorded history. Fortunately, her friends are all kindhearted enough to take this in their stride. Misery spends most of the time in a state of detached apathy and with ever-present trails of tears running from her eyes. She does have tiny moments of happiness throughout the series, but alas they're few and far between, her bad luck rears its ugly head and strikes her down with a vengeance, usually in the form of lightning bolts. Misery has good luck on Friday the 13th. This results with her bad luck being transferred to her friends. She also keeps "Dust Bunnies" as pets, despite being allergic to them and sneezing them away. However, they always return to her. She is considered a fan favorite due to her comic relief and cute appearance. Voiced by Emily Hampshire; singing voice provided by Jeen O'Brien.


=== Skull Boy ===
A man of many talents, Skull Boy is the mansion's official Jack of All Trades. From film directing to private investigating, Skull Boy can pretty much do it all. His only problem is that he can't seem to find one thing and stick to it for any serious length of time. But if Skull Boy doesn't know where he's going, it's mostly because he doesn't know where he comes from. This is referred to in many episodes when each time he finds a new skill he surmises his must come from a long line of others with that same skill, indicating he knows little to nothing about his origins. There have been several moments that confirm that Ruby has a crush on him, such as in the episode "Quadro-Gloomia" when Frank and Len get inside Ruby's head and experience her feelings for Skull Boy first hand. He does share the same feelings towards Ruby, but he is clueless about her feelings.  Even though Skull Boy doesn't know where he is from, he has a family – his friends are his family. Voiced by Scott McCord.
In Skull Boy's endeavor to discover his roots, he has undergone many personae and occupations. These include:


=== Frank and Len ===
Rockers by nature, these conjoined brothers live in the mansion's garage. They have their own garage band, known as R.I.P (pronounced "RIP"). Frank and Len's strength lies in performing music. Unfortunately, this seems to be all that is their strength, since they both tend to lose focus on anything that isn't their music. They can hardly focus on something if it doesn't have a beat. Frank is actually the more intelligent of the two, often getting annoyed at Len's lack of understanding. Len is the more sentimental one, who seems to have a thing for small animals and things that don't exactly matter to anyone else. Even though they seem upbeat for their music, the two of them are almost never on the same page, even though they're conjoined. Despite this fact, Frank and Len are close. For example, in the episode "Quadrogloomia", when the brothers needed a way to become unconscious, they were unable to hit each other over the head with planks. Another example of this is in the episode "Iris Springs Eternal" where Ruby, Frank, and Len were sliding down a mud slide when Frank states, "Len, if we don't get out of this I just want you to know that you've been the best brother a brother could have for a brother." Len replies with "same here." Also, as of the episode "Quadrogloomia", Frank and Len are the only housemates (other than Doom Kitty) who know about Ruby's crush on Skull Boy. Voiced by David Berni (as Frank) and Jeremy Harris (as Len).


=== Edgar, Allan, and Poe ===
Poe the crow, along with his brothers Edgar and Allan, live in an attached addition to the mansion. However, out of the three, Poe is the one seen most often. Edgar and Allan are rarely seen and when they are, they never speak but act as Poe's bodyguards and servants since they are taller than him. Poe meanwhile, as a man of culture and sophistication, likes to think of himself as a role model to the rest of the mansion's residents. Unfortunately, he tends to be a little too attached to his image and likes to cover up the parts of him that make him look like less than what he makes himself out to be. For example, he'd rather tell people he's performing in an actual opera than admit that he's staying home to listen to opera records. Poe's pampered nature also tends to make him overreact to things that everyone else would more or less take in stride. There is at least some evidence that Poe's cultured accent is an affectation. In episode "Unsung Hero", Poe reverts to an East End accent while speaking to Skull Boy. Skull Boy remarks that Poe's Oxford accent has vanished much to Poe's discomfort. Poe is fond of claiming to be a descendant of Edgar Allan Poe's pet budgie Paco, which is proven in the episode "Time Flies". Poe is voiced by Adrian Truss.


=== Scaredy Bat ===
Scaredy Bat is a bat with an Indian accent who is afraid of almost everything. Where Ruby finds happiness and Iris finds adventure, Scaredy finds only fear. Despite being a bat, he's afraid of flying and is also afraid of the dark. He's also afraid of his own reflection. However, he has overcome those fears after several episodes. This constant state of fear is perfect for Boo Boo, the mansion's ghost, since Scaredy is the only one who is afraid of him. The only thing he's not afraid of is a crumpet. Even if his fear sometimes gets the better of him, Scaredy is the loyal and talented drummer of Frank and Len's band, R.I.P. When he is behind the kit, Scaredy is a beat machine and forgets his fear. He also enjoys eating mosquitoes. Voiced by Peter Keleghan.


=== Boo Boo ===
The mansion's ghost, Boo Boo sees it as his job to haunt. However, it's a little difficult for him to do that when everyone thinks he's cute, and he hates being thought of as cute. In the episode "Doom with a View", (Boo Boo's first appearance in the series), he takes exception to the other characters referring to his cuteness when they first see him.  When he first arrived at the mansion, Boo Boo wasn't an official ghost in that he couldn't pass through walls and only Doom Kitty, Ruby's cat, could see him. According to Mr. White and Mr. White, Boo Boo couldn't become a full ghost until he had scared at least one person and Ruby's mansion was the house they'd selected for him. But since Boo Boo couldn't go through walls and Doom was the only one in the house who could see him, his job was very difficult. However, when Doom splashed milk on him to reveal his presence, Boo Boo got a slight scare out of Misery when he almost splashed milk onto a tablecloth once owned by her "great-great-really-great-grandmother." However, it was enough of a scare for Boo Boo to become a full-fledged ghost, since he can now pass through walls and everyone can see him. After that though, his only real motivation for staying in the mansion has been to scare Scaredy Bat, as he hasn't been able to scare anyone else since, besides people from outside of Gloomsville. However, Boo Boo enjoys pranking more than he enjoys scaring, much to Mr. White's disappointment. Despite this, he and Scaredy seem to be good friends (perhaps best friends). Doom Kitty hates Boo Boo, especially since she was framed in "Doom with a View", and his presence always gets her into trouble. Despite being treated like a main character Boo Boo only appeared in 18 episodes and had speaking roles in 11 of them. Voiced by Barbara Mamabolo.


=== Mr. Buns ===
Mr. Buns is a stuffed rabbit that Ruby created from one of her leftover socks. The floppy, yet enigmatic Mr. Buns loves tea cozies and pretty much finds himself in the middle of almost all the action in the mansion. He also has a strange tendency to show up anywhere and do random tasks. When Misery first sees him, she calls him a stuffed sock. Later, she is convinced that he is a "special one of a kind friend".


== Minor characters ==


=== Mr. White and Mr. White ===
These two large ghosts are the mentors and teachers of young Boo Boo in his learning to be a ghost. Distinguished by their glasses and high rank within "The Family", they typically act in a mafia orientated manner (even having their own pasta making skit and Italian music playing in the background), often threatening to take Boo Boo away if he does not complete enough scares or if he is pulling pranks, such as the line "Less Taunt. More Haunt." Noticeably, the Mr. White with the hat is a higher rank than the other Mr. White, who is usually driving their mode of transport. The characters are voiced by Harvey Atkin and Ron Rubin.


=== Squig ===
A large flying worm that Iris made friends with in her travels. Although he is not often seen, he accompanies Iris on all her adventures as both her companion and sometimes her mode of transport. He is about five times as big as Iris, and is black and red with patterns down his back, and has a small set of wings at the end of his tail. According to Iris, he can fly because nobody ever told him he can't.


=== Venus ===
Another of Iris's many pets, Venus is a venus flytrap. When Venus first appeared, she was very voracious and typically acted as a pet. However, after she ate the heart that Skull Boy made in the episode "Science Fair or Foul", she became much more caring and knowledgeable, eventually becoming a successful writer and being able to speak very eloquently, as seen in the skit "Suspense Writing with...?". Venus speaks with a thick French accent, because the record Iris used to teach her was dictated by a French man. However, Skull Boy's French accent is stronger, leading her to confusion.


=== Mr. Mummbles ===
Ruby's next door neighbour, Mr. Mummbles lives in a glass house with his pet, a frog named Socrates. Mr. Mummbles received his name from Poe in the episode "Poe-Ranoia", where Poe thought that he was a murderer, and since then the name has stuck. After a revelation that Mr. Mummbles designs and creates amusement park rides, he can be seen in later episodes testing these new rides. His rides are designed to make people happy, sometimes having features that tickle the passengers. Mr. Mummbles also follows the rule "Safety First", however his rides seem to bend in the opposite direction to being safe, which includes massive spirals, gaps and fire on the tracks. Mr. Mummbles is voiced by Derek McGrath.


=== The Skele-Tunes ===
A group of Jazz/scat/Cajun musicians that befriend Skull Boy (referring to him as "SB"). The band contains four skeletons. The vocalist is the jazzy Skele-T, instantly recognized by his outfit, earrings and gold tooth. The other skeletons each have a different instrument, one with an accordion, one with a tuba and one using his rib cage as a xylophone. They are shown to be "Jokers", disappearing and joking around with Skull Boy. Skele-T does most of the talking, using jazz slang and phrases, such as "You're straight from the fridge daddio" and throwing in skeleton puns like "You fracture me" instead of "You crack me up". When Skull Boy leaves to try to find his roots, Skele-T even invites him to join their band, but he refuses due to a case of homesickness. Skele-T is voiced by Michael Dunston.


=== Misery's cousins and ancestors ===
In the episode "I'll Be Home For Misery" introduces Misery's other relatives throughout the ages for their "once in a millennium" family reunion.
Malady and Malaise
These two sisters are Misery's cousins, who, when both are together, cause a localized ice age. Malady is the one character that is more disease-prone than her cousin, Misery. She apparently suffers from every known disease, ailment, illness, symptom and allergy almost simultaneously, and when asked about them, she runs through all the events and procedures that led to one injury, which usually lasts hours until the conclusion. Malaise suffers from narcolepsy and can often be seen lying on a couch or bed or even asleep on the floor randomly. She is accompanied by many creatures including a snake, spiders, an octopus, snails and butterflies, which usually attach themselves to the full-body veil she wears. Despite their faults, Misery loves her cousins.
Mayhem – A cavegirl that resembles Misery. It is shown that she possesses great physical strength and little intelligence. It is implied that she chased away the dinosaurs, and is very sad that they're gone. She chases Scaredy Bat throughout the episode "I'll Be Home For Misery", shouting "Here Birdy, Birdy, Birdy". She is also seen in the episode, "Time Flies".
Mildew – A Japanese geisha that resembles Misery. She speaks in respectful tones but clashes the most with Mayhem and Migraine. Unlike her relatives, Mildew has normal colored eyes and skin. She also seems to constantly clarify whenever she is joking or not, as she once said: "Misery, I've heard so much about you. All bad. Joke."
Migraine – Arriving on the Titanic, Migraine usually has her arms back yelling "I'm the queen of the world". She also talks like a valley girl and has a Hawaiian flower in her hair. She is also seen in the episodes "Time Flies" and "Disaster Becomes You".
Morose – Truly goth, Morose has dyed green/black hair and green and black striped stockings. Very thick streaks of eyeshadow run down her face because it mixes with her tears. She is also a singer who unlike Misery, can sing while awake and performs a song with Frank & Len while playing keyboards. The boys love it until their house falls in around her. One of Morose's only happy moments in Gloomsville is when she said "cool... another verse?". Misery calls her "Aunt Morose" in the episode "Sunny Daze".
Mopey – The optimistic member of Misery's family, she has hair just like Ruby's, which Ruby compliments and Mopey says "Right back atcha!".
Motley – Misery's great great really great grandmother. Burnt in the Great Fire of London, Motley has almost no hair left and seems generally singed all over with the bit of remaining hair on fire. Unlike Misery, she prefers light in the darkness. A piece of her table cloth was the only thing salvaged from the fire, and became Misery's prized possession.
Myopic, Malice and Misbegotten – The triplets are little kids that seem pretty happy for members of the misery family. They dance and laugh around Morose leading Morose to wail "Children, Cheerful, Playful Children... Why?! Why?!"
In addition to these named family members, four others are shown. A relative that resembles Misery in a French Peasant Maid's outfit, who plays drums while Morose sings and plays her keyboard, another relative in a Medieval Princess gown and pointy hat (who appeared in the episode "Disaster Becomes You"), one dressed as a 1970s soul sister with a white streak of hair like Frankenstein's bride, and one with white hair in braided pigtails curved up like bullhorns with a spider web in the middle.
The end of the episode suggests that the entire reunion was a dream, so the question remains how many of them are actually Misery cousins (such as Malady, Malaise, Mayhem, etc.) and how many are from Misery's imagination as they just look like Misery in different getups from other episodes (Motley, Mopey, the 1970s Soul Sister, etc.).


=== Bunny ===
A pink bunny with red eyes, he pretends to be a cute and loving bunny with a cold, but is actually evil. He wanted to steal everything from Ruby's mansion. Doom Kitty was the only one who knew he was evil. Later everyone else finds out this bunny is evil when Doom Kitty traps him in a cage and everyone saw their stuff in his carriage, so they kicked him out.


=== Gunther and Uta ===
Two twins with a German accent who came to live in the Gloom household, after their run-in with a witch and her gingerbread house. They thought Ruby and her friends were about to get rid of them despite knowing that what Ruby and her friends do delights everyone else.


== Episodes ==
These are the episodes that have aired on television. Most of the episodes contain a short skit at the beginning and end of the episodes, "Gloomer Rumor", the first episode of the series, is the only episode to have no opening skit, but an ending skit.


=== Season 1 (2006–07) ===


=== Season 2 (2007) ===


=== Season 3 (2007–08) ===


== Broadcast ==
Ruby Gloom premiered on YTV in Canada on October 13, 2006. In the United Kingdom, the series aired on Pop and Pop Girl in 2008. Channel RTÉ Two in Ireland broadcast the series in 2010.  Australian channels ABC1 and ABC3 carried the series in 2008. More recently, as of 2020, the series has become available on demand via YouTube. In 2011 the channel Hub Network aired the series in the US, however the reruns lasted for a short period of time.


== Awards and honors ==
Ruby Gloom was nominated for a 2014 Gemini Award in the category of "Best Animated Program or Series."The script for episode "Yam Ween", written by Carolyn Bennett, was a finalist in the 2007 Canadian Screenwriting Awards
.


== Website ==
The product line had an interactive website where users could chat in a forum, and click around for free things such as desktop icons and screensavers. The words: "Ruby Gloom is happy in her own way.
Even though she is frowning, to her, a frown is only a smile upside down. In fact, Ruby is so happy, she could just die" greeted viewers as they entered the website. Towards the end of 2004 the website changed to forum only dubbed "The Mansion" By the end of 2004 the website had been redesigned and featured an interactive mansion to explore and user submitted artwork. Ruby is shown on the roof with her cat Doom and was given a smile for the first time. By 2006 "The Mansion" forum was removed, and the website had taken on its show format geared more towards kids.


== Reception ==
Ruby Gloom received generally positive reviews from both critics and audiences, praising it for its characters, writing, soundtrack, and plot.
Common Sense Media gave the series a rating of four stars out of five, saying: "A nice mix of sweet-and-sour, Ruby Gloom's dark gothic setting underscores all the cooperation and kindness. Adults will enjoy jokes that kids may miss, such as when it's revealed that Ruby eats Glum Flakes cereal for breakfast. And all but the most sensitive kids will be too enraptured by fantastic elements like talking pictures and a school for ghosts to be unnerved by dark elements like Misery's constant talk of disasters and death".
As stated above, the series was nominated for a 2014 Gemini Award in the category of "Best Animated Program or Series.


== DVD releases ==


=== Region 1 ===
Canada – There are two DVDs available from Nelvana (in association with the TV station YTV). The DVDs present the episodes in NTSC 1.85:1 (16x9) anamorphic widescreen, with English Dolby Digital 5.1 sound and French Dolby Digital 2.0 sound. There are no subtitles or closed captions. The opening title sequence on the DVDs is the full version, and not the trimmed version that airs on YTV.
The DVDs in release order are:

Ruby Gloom: Grounded in Gloomsville – Contains the first four episodes of the series (as listed above), along with a behind-the-scenes special feature showing the voice recording of the episode "Hair(Less): The Musical" (parts 1 and 2).Ruby Gloom: Misery Loves Company – Contains the episodes "Iris Springs Eternal", "Poe-ranoia", "Skull Boys Don't Cry", and "Misery Loves Company", with no special features.Following this, a third DVD entitled Ruby Gloom: Pet Poepulation was scheduled for release on September 9, 2009, but never became available.
United States – In 2013, kaboom! Entertainment and Phase 4 Films released six Ruby Gloom DVDs in the U.S. Each disc contains four episodes, arranged as a continuous show, with the repeated opening songs and individual mini-episodes trimmed out.  All of the mini-episodes are included separately as a bonus feature, instead of being incorporated in their main episode as originally broadcast.
The discs are as follows:

Ruby Gloom: Happiest Girl in the World – Contains the episodes "Gloomer Rumor", "Doom With a View", "Missing Buns", and "Iris Springs Eternal".Ruby Gloom: I Heart Rock & Roll – Contains the episodes "Unsung Hero", "Quadra-gloomia", "Skull Boy's Don't Cry", and "Bad Hare Day".Ruby Gloom: Gloomates – Contains the episodes "Gloomates", "Seeing Eye to Eyes", "Name That Toon", and "Broken Records".Ruby Gloom: Grounded in Gloomsville – Contains the episodes "Grounded in Gloomsville", "Ruby Cubed", "Once in a Blue Luna", and "Time Flies".Ruby Gloom: Tooth or Dare – Contains the episodes "Tooth or Dare", "Skull in the Family", "Shaken. Not Scared", and "Misery Loves Company".Ruby Gloom: Welcome to Gloomsville – Contains the episodes "Venus de Gloomsville", "Science Fair or Foul", "Poe-Ranoia", and "Happy Yam Ween".As with the Canadian release, the DVDs present the episodes in NTSC 1.85:1 (16x9) anamorphic widescreen, with English Dolby Digital 5.1 sound, but they have Spanish Dolby Digital 2.0 sound instead of French.
Brazil – Seasons one and two were released on 3-disc DVD box sets; however, the box sets do not include all of the episodes from each season (despite the DVD covers indicating this). Both box sets are presented in NTSC 1.33:1 (4x3) full screen (the sides of the widescreen image are cut to create the full screen ratio, also known as pan and scan) with Portuguese and English Dolby Digital 5.1 sound. The opening title sequence is the trimmed version that airs on most TV channels (such as Canada's YTV). Neither box set includes any special features.
The box sets are:
Ruby Gloom: Full season 1 – Contains the first thirteen episodes from season one.
Ruby Gloom: Full season 2 – Contains thirteen episodes (the remaining eight episodes from season one plus the first five episodes from season two).


=== Region 2 ===
France – Ruby Gloom: 1 is available from France Télévisions Distribution (in association with the TV station France 3) and contains the first six episodes of the series (as listed above). The episodes are presented in PAL 1.33:1 (4x3) full screen (the sides of the widescreen image are cut to create the full screen ratio, also known as pan and scan) with French Dolby Digital 2.0 sound.
Germany – Two DVDs were made available from SPV GmbH (in association with the TV station Super RTL) containing the first eight episodes of the series (as listed above), with each DVD consisting of four episodes. The episodes are presented in PAL 1.33:1 (4x3) full screen (the sides of the widescreen image are cut to create the full screen ratio, also known as pan and scan) with German Dolby Digital 2.0 sound.
On October 15, 2010, Edel Germany GmbH released Ruby Gloom – Willkommen in Gloomsville (Ruby Gloom – Welcome to Gloomsville), which contains the first seven episodes of the series (as listed above).
Japan – A DVD box set entitled Ruby Gloom's Bible is available from Sony Music Entertainment and contains twenty of the series' first 24 episodes (as listed above) in random order on five DVDs. The episodes are presented in NTSC 1.85:1 widescreen with Japanese Dolby Digital 2.0 sound.
United Kingdom – In the UK, Platform Entertainment Ltd. released an on TBA,


=== Region 4 ===
Australia – There are four DVD volumes available from Magna Pacific containing the first sixteen episodes of the series (as listed above), with each volume consisting of four episodes. The episodes are presented in PAL 1.33:1 (4x3) full screen (the sides of the widescreen image are cut to create the full screen ratio, also known as pan and scan) with English Dolby Digital 2.0 sound.


== References ==


== External links ==
Official Ruby Gloom site
Ruby Gloom on IMDb
Ruby Gloom on Facebook
Save Ruby Gloom
Ruby Gloom – Martin Hsu