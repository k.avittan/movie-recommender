Soul Man is a 1986 American comedy film about a white man who takes tanning pills in order to pretend to be black and qualify for a black-only scholarship at Harvard Law School. The film was directed by Steve Miner and stars C. Thomas Howell, Rae Dawn Chong, Arye Gross, James Earl Jones, Leslie Nielsen, James B. Sikking, and Julia Louis-Dreyfus.The title refers to the Sam and Dave song "Soul Man". The original soundtrack includes a version performed by Sam Moore and Lou Reed.


== Plot ==
Mark Watson (Howell) is the pampered son of a rich family who is about to attend Harvard Law School along with his best friend Gordon (Gross). Unfortunately, his father's neurotic psychiatrist talks his patient into having more fun for himself instead of spending money on his son. Faced with the prospect of having to pay for law school by himself, Mark decides to apply for a scholarship, but the only suitable one is for African-Americans only. He decides to cheat by using tanning pills in a larger dose than prescribed to appear as an African-American. Watson then sets out for Harvard, naïvely believing that black people have no problems at all in American society.
However, once immersed in a Black student's life, Mark finds out prejudice and racism truly exists. He meets a young African-American student named Sarah Walker (Chong), whom he first only flirts with; gradually, however, he genuinely falls in love with her. In passing, she mentions that he received the scholarship she was in the running for at the last minute. Due to this, she not only has to handle her classes but work as a waitress to support herself and her young son George.
Slowly, Mark begins to regret his deed since he has landed in jail under suspicion of stealing his own car, been the subject of stereotypes of black men and pursued by his landlord's daughter and classmate Whitney (Melora Hardin) simply because he's not white.
After a chaotic day in which Sarah, his parents (who are not aware of his double life) and Whitney all make surprise visits at the same time, he drops the charade and publicly reveals himself to be white. Most people he has come into contact with realize this makes sense, but Sarah is furious.
Once the charade is over, Mark speaks to his professor (Jones). He has learned more than he bargained for since he admits that he didn't know what it was like to truly be black because he could have changed back to being white at any time.
Because Mark must forfeit his scholarship, his father agrees to loan him the money for school, but with exorbitant interest. He goes to Sarah and begs for another chance, to which she agrees after Mark stands up for her and George when two male students tell a racist joke in front of them.


== Cast ==
C. Thomas Howell as Mark Watson
Rae Dawn Chong as Sarah Walker
Arye Gross as Gordon Bloomfield
James Earl Jones as Professor Banks
Leslie Nielsen as Mr. Dunbar
James B. Sikking as Bill Watson
Melora Hardin as Whitney Dunbar
Julia Louis-Dreyfus as Lisa Stimson
Maree Cheatham as Dorothy Watson
Wallace Langham as Barky Brewer


== Production ==
Howell later said, "when I made the movie, I didn't go into it with the idea that I had a responsibility to sort of teach America a lesson. I went into it because it was a great script. It was so well-written, so funny, and—sadly—very true. A lot of the experiences this guy goes through, maybe he wouldn't have gone through them if he was a white person, but when he's black, it's a very different experience."Ron Reagan, son of then-president Ronald Reagan and first lady Nancy Reagan, had a small role in the film.


== Reception ==


=== Controversy ===
The film was widely criticized for featuring a Caucasian actor wearing blackface. Members of the NAACP spoke out against the film and an African-American student group at UCLA organized a picket of a cinema screening Soul Man.NAACP Chapter President Willis Edwards said in a statement at the time, "We certainly believe it is possible to use humor to reveal the ridiculousness of racism. However the unhumorous and quite seriously made plot point of Soul Man is that no black student could be found in all of Los Angeles who was academically qualified for a scholarship geared to blacks."In defending the film, producer Steve Tisch said it was like Tootsie (1982) which featured a man masquerading as a woman for career advancement. "It used comedy as a device to expose sexual stereotyping. I think Soul Man uses it to explode racial stereotyping."Actress Rae Dawn Chong said of the controversy:

It was only controversial because Spike Lee made a thing of it. He'd never seen the movie and he just jumped all over it… He was just starting and pulling everything down in his wake. If you watch the movie, it's really making white people look stupid… [The film] is adorable and it didn't deserve it... I always tried to be an actor who was doing a part that was a character versus what I call 'blackting,' or playing my race, because I knew that I would fail because I was mixed. I was the black actor for sure, but I didn't lead with my epidermis, and that offended people like Spike Lee, I think. You're either militant or you're not and he decided to just attack. I've never forgiven him for that because it really hurt me. I didn't realize [at the time] that not pushing the afro-centric agenda was going to bite me. When you start to do well people start to say you're a Tom [as in Uncle Tom] because you're acceptable.
Spike Lee responded by saying, "In my film career, any comment or criticism has never been based on jealousy.""A white man donning blackface is taboo," said C Thomas Howell. "Conversation over — you can't win. But our intentions were pure: We wanted to make a funny movie that had a message about racism."Howell later expanded:

I’m shocked at how truly harmless that movie is, and how the anti-racial message involved in it is so prevalent... This isn’t a movie about blackface. This isn’t a movie that should be considered irresponsible on any level... It’s very funny... It made me much more aware of the issues we face on a day-to-day basis, and it made me much more sensitive to racism... It’s an innocent movie, it’s got innocent messages, and it’s got some very, very deep messages. And I think the people that haven’t seen it that judge it are horribly wrong. I think that’s more offensive than anything. Judging something you haven’t seen is the worst thing you can really do. In fact, Soul Man sort of represents that all the way through. I think it’s a really innocent movie with a very powerful message, and it’s an important part of my life. I’m proud of the performance, and I’m proud of the people that were in it.  A lot of people ask me today, “Could that movie be made today?"... Robert Downey Jr. just did it in Tropic Thunder!... The difference is that he was just playing a character in Tropic Thunder, and there was no magnifying glass on racism, which is so prevalent in our country. I guess that’s what makes people more uncomfortable about Soul Man. But I think it’s an important movie.
The film was seen by President Ronald and Nancy Reagan at Camp David.:"The Reagans enjoyed the film and especially enjoyed seeing their son Ron," a White House spokesman said at the time.


=== Critical reception ===
Controversy aside, the film was panned by critics. It has a score of only 13% on Rotten Tomatoes from 23 reviews. Roger Ebert gave Soul Man one out of four stars, writing that the main premise "is a genuinely interesting idea, filled with dramatic possibilities, but the movie approaches it on the level of a dim-witted sitcom."


=== Box office ===
Despite the controversy the film was a box office success. On its opening weekend, it debuted at #3 behind Crocodile Dundee and The Color of Money with $4.4 million. In total, Soul Man went on to gross $27.8 million domestically.


== Soundtrack ==


=== Charts ===


== Music video ==
A music video for the film's soundtrack was released for the Sam & Dave song "Soul Man" performed by Sam Moore and Lou Reed.  The video starred actors Bruce Willis, Cybill Shepherd, Rae Dawn Chong, C. Thomas Howell, Ron Reagan Jr., George Segal, Jamie Farr, boxer Ray Mancini and the children's character Gumby, all lip synching to the song. Soul Man executive producer Steve Tisch got the actors to do the cameos.


== Influence ==
Defunct mathcore band Botch has a track named "C. Thomas Howell as the 'Soul Man'" on their release We Are the Romans.


== Home media ==
Soul Man was released on DVD on March 19, 2002, by Anchor Bay Entertainment. Special Features included a theatrical and teaser trailer, along with an audio commentary by director Steve Miner and C.Thomas Howell.It was released again by Anchor Bay Entertainment as a double feature along with Fraternity Vacation on November 20, 2007.
On October 20, 2011, it was released again as a double feature by Image Entertainment along with 18 Again!


== References ==


== External links ==
Soul Man on IMDb
Soul Man at AllMovie
Soul Man at Box Office Mojo
Review of film at Vice Magazine
Review of film by Roger Ebert
Review of film at New York Times