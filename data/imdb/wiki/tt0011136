Dry and Thirsty is a 1920 American silent comedy film, directed by Craig Hutchinson. It is a satire of the Prohibition era in the United States. The film spoofs former Secretary of State William Jennings Bryan, who campaigned for Prohibition; the character is dubbed "William Allways Tryan."
The stars, Billy Bletcher and Vera Reynolds, were a popular comedy duo in "Gayety Comedies". At the time of Dry and Thirsty's release, Gayety also released Twin Bedlam, another Bletcher/Reynolds comedy.This was the first film for vaudeville actor Tom Dempsey, appearing under the stage name "John Dempsey". He later appeared in The Bush Leaguer (1927), Nifty Numbers (1928) and other Educational Pictures comedies, and more than thirty Mack Sennett comedies in the 1930s.


== Plot ==
Horace Radish is trying to find some alcohol to drink, but the task turns out to be a difficult one as the practice of drinking is illegal. His search takes him to the Bootlegger's Haven Hotel, however William Allways Tryan, the "father of Prohibition" is also staying at the hotel with his wife.


== Cast list ==
Billy Bletcher as Horace Radish
Vera Reynolds as Mrs. Tryan
John Dempsey as William Allways Tryan


== Reception ==
Motion Picture News reviewed the film on February 28, 1920: "Dry and Thirsty employs the dream situation around the prohibition idea. The hero being fond of liquor goes to a "bootlegger's" to obtain it. He is also fond of flirting and his eyes cannot behave when he sees the wife of the prohibitionist. Complications ensue and then the grand awakening. Just a fair comedy."The Film Daily gave a poor review on June 6, 1920: "Lacking a meritorious theme and commendable incidents, this single reel comedy in which the short Billy Fletcher [sic] plays the leading role, fails to register very heavily. Most of it consists of a pursuit of some liquor by the featured comedian, and the finish sees him reclining on a chair in a hotel while a bell hop serves him with the desired refreshment. The plot has no particular climax and the incidents in most cases are not sufficiently unusual to merit praise."


== Further reading ==
Clown Princes and Court Jesters by Kalton C. Lahue and Samuel Gill, A.S. Barnes (1970), pages 54–55
Griffithiana (film journal), page 176


== References ==


== External links ==
Dry and Thirsty on IMDb