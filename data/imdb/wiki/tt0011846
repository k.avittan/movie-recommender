West Is West is a 2010 British comedy-drama film directed by Andy DeEmmony and starring Om Puri, Linda Bassett, Aqib Khan, Ila Arun and Jimi Mistry. A sequel to the 1999 comedy film East Is East, it was written by Ayub Khan-Din and produced by Leslee Udwin for Assassin Films and BBC Films.
The film was first shown at the 2010 Toronto International Film Festival on 12 September. It premiered at the BFI London Film Festival on 19 October 2010, followed by UK and Irish release on 25 February 2011.  The first US showing was on 2 November 2010 at the South Asian International Film Festival, followed by several other US festivals. While it was released in Canada on 25 March 2011, it never received a US release.


== Plot ==
In 1976, five years after the original film East Is East, little is known about most of the Khan children, except that they seldom communicate with their parents. Tariq is now a hippie (looking like George Harrison) who runs a new age store with older brother Nazir, and his English girlfriend is unaware of his true ethnicity. Maneer lives with his extended family in Pakistan where he is searching for a suitable wife, but without success. Sajid, the youngest Khan child, no longer wears a parka, and is a truant constantly bullied due to his Pakistani background, but the headmaster—a former British soldier who served in the Punjab—is sympathetic, encouraging him to embrace his heritage. After Sajid is caught shoplifting, his father George, who has retained his bullying nature, attacks him at home. When Sajid retaliates, calling him a "dirty Paki bastard", a devastated George states all his other children in England have become British and he cannot lose Sajid as well. He takes him to Pakistan to meet their extended family and show him life in their native land is better, though Ella openly disapproves.
On arriving in Pakistan, George and Sajid are greeted by relatives including Tanvir, a lazy man who often tries to swindle George—and is his son-in-law through his marriage to one of George's daughters with his first wife Basheera. On the family farm, George is reunited with Basheera and their daughters whom he had abandoned thirty years before, and hands out gifts to all, telling them he is staying for a month to find Maneer a wife, but soon discovers no family will give their daughter away as they fear Maneer will leave his wife for an English woman as his father George did when he left Basheera for Ella. Furious, George blames his family in England, but Maneer reminds him he himself is at fault. Basheera is also angry with George for abandoning her when she needed him.
When Tanvir explains to Sajid he will not tolerate any trouble, he is abruptly told to "fuck off". Sajid is taken to the local school for enrolment where he meets spiritual teacher Pir Naseem and local boy Zaid who Sajid loathes at first, and refuses to enrol. Zaid, who can speak basic English, advises him, and the two become firm friends. Zaid teaches him Pakistani culture, and Pir Naseem promises George he will discipline his son when he misbehaves. Sajid gradually appreciates his culture and new surroundings which pleases George, except he is slightly jealous of the bond between his son and Pir Naseem. Eventually Sajid discovers Rochdale-born Pakistani woman Neelam who bears a striking resemblance to Maneer's favourite singer Nana Mouskouri; like Maneer she is also looking for a spouse in Pakistan, and with her approval Sajid plans a meeting between the pair.
Meanwhile, upon discovering George has withdrawn the family savings, Ella travels to Pakistan with her best friend Annie in tow. She is furious to discover her husband is building a house for his family there, and plans to take Sajid back to England with her, but he refuses to leave. During her stay, Ella fights with Basheera and her daughters, and refuses to give them access to the new house, but upon realising how alike they are the two women put their differences aside. Maneer and Neelam soon marry, and George for the first time in years begins to appreciate Ella as a wife who stood by him during hard times. The film ends with George and his England-based family returning home and Sajid finally proud of his Asian background, whilst George's chippy now serves Pakistani-style kebabs.


== Cast ==
Om Puri as George
Linda Bassett as Ella
Aqib Khan as Sajid
Lesley Nicol as Annie
Jimi Mistry as Tariq
Ila Arun as Basheera
Emil Marwa as Maneer
Vijay Raaz as Tanvir
Vanessa Hehir as Esther
Robert Pugh as Headmaster Jordan
Zita Sattar as Neelam
Nadim Sawalha as Pir Naseem
Raj Bhansali as Zaid
Yograj Singh custom officerOm Puri, Linda Bassett, Lesley Nicol, Jimi Mistry, and Emil Marwa are the only actors from the original movie who reprise their roles in West is West.


== Soundtrack ==
Shankar–Ehsaan–Loy composed the score.  Rob Lane arranged it. The songs were recorded in Purple Rain studio, in Mumbai. Rob Lane joined Shankar–Ehsaan–Loy to compose music for the film. They used several Indian classical instruments like Jaltarang, Santoor, Flute  and Sarangi. The renowned Jaltarang player Milind Tulnkar was roped in to play the instrument for ten to twelve sequences.


== Critical reception ==
The film received mixed reviews. As of June 2020, the film holds a 64% approval rating on Rotten Tomatoes, based on 25 reviews with an average rating of 5.37 out of 10. Several reviewers noted that while the film still had the sense of humour of East Is East, it did not reach the level attained in the original. In his review for The Globe and Mail, Rick Groen noted the "sudden leaps into unabashed melodrama", saying that "When they fail, the hurlyburly gets annoying. But when they succeed, the result can be genuinely touching."Sukhdev Sandhu of The Daily Telegraph in his review described the soundtrack as "delightful". Rediff reviewer Shaikh Ayaz, in his review, stated "The music is beautifully cherry-picked, mixing Sufi tunes with Punjabi folk music." Nikhat Kazmi of Times of India, in her review, said that the "soulful music" of the movie was memorable. Pankaj Sabnani of Glamsham also gave the music a thumbs up, saying "Shankar-Ehsaan-Loy's music is excellent and gels well with the film." Daily Bhaskar review described the music as "soothing" and "situational".


== References ==


== External links ==
West Is West on IMDb
Interview with West Is West producer Leslie Udwin