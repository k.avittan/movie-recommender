A Murphy bed (in North America), also called a wall bed, pull down bed, or fold-down bed, is a bed that is hinged at one end to store vertically against the wall, or inside a closet or cabinet.


== History ==
The bed is named after  William Lawrence Murphy (1876–1957), an Irish immigrant in New York who wanted to find a creative method of making space in his small apartment. He applied for his first patents around 1900. According to legend, he was wooing an opera singer, but living in a one-room apartment in San Francisco, and the moral code of the time frowned upon a woman entering a man's bedroom. Murphy's invention converted his bedroom into a parlor, enabling him to entertain. Earlier foldup beds had existed, and were even available through the Sears, Roebuck & Co. catalog, but Murphy introduced pivot and counterbalanced designs for which he received a series of patents, including one for a "Disappearing Bed" on June 18, 1912 and another for a "Design for a Bed" on June 27, 1916.Murphy beds are used for space-saving purposes, much like trundle beds, and are popular where floor space is limited, such as small houses, apartments, hotels, mobile homes and college dormitories. In recent years, Murphy bed units have included options such as lighting, storage cabinets, and office components. They have seen a resurgence in popularity in the early 2010s due to the weak economy, with children moving back in with their parents and families choosing to renovate homes rather than purchasing larger ones.In 1989, the United States Court of Appeals for the Second Circuit ruled that the term "Murphy Bed" had entered common usage so thoroughly that it was no longer eligible for trademark protection.


== Designs and models ==
Most Murphy beds do not have box springs. Instead, the mattress usually lies on a wood platform or wire mesh and is held in place so as not to sag when in a closed position. The mattress is attached to the bed frame, often with elastic straps to hold the mattress in position when the unit is folded upright. Piston-lifts or torsion springs make modern Murphy beds easy to lower and raise.

Since the first model several other variations and designs have been created, including: sideways-mounted Murphy beds, Murphy bunk beds, and solutions that include other functions. Murphy beds with tables or desks that fold down when the bed is folded up are popular, and there are also models with sofas and shelving solutions.


== Risks ==
If not secured or used properly, a Murphy bed could collapse on the operator. A 1945 court case in Illinois found that a tenant assumed the risk of injury from a wall bed installed in a rented inn room. In 1982, a drunk man suffocated inside a closed Murphy bed, and two women were entrapped and suffocated by an improperly installed wall bed in 2005. A 2014 lawsuit alleged that a defective Murphy bed led to the death of a Staten Island man.


== In popular culture ==
Murphy beds were a common setup for comic scenes in early cinema, including in silent films. The earliest known film to feature a Murphy bed is the lost 1900 Biograph Company film A Bulletproof Bed, which was remade in 1903 by Edison Pictures as the extant film Subub Surprises the Burglar. It was a recurrent slapstick element in many Keystone Studios productions of the 1910s, including Cursed by His Beauty (1914), Fatty's Reckless Fling (1915), He Wouldn't Stay Down (1915), and Bath Tub Perils (1916). Charlie Chaplin's 1916 One AM also features an exaggerated encounter with a Murphy bed.
Later films which use Murphy beds as comic props (often to cause injury or frustration, or to hide a clandestine guest) include Laurel and Hardy's Be Big (1930), Jimmy Stewart and Ginger Rogers's Vivacious Lady (1938), Buster Keaton's Spite Marriage (1929) and Nothing But Pleasure (1940), Abbott and Costello's Hit the Ice (1943), several Three Stooges shorts (including 1952's Corny Casanovas), It's a Mad, Mad, Mad, Mad World, the Popeye cartoon Shuteye Popeye, Bob Hope's Boy, Did I Get a Wrong Number (1966), the James Bond film You Only Live Twice, The Night They Raided Minsky's, Mel Brooks's Silent Movie, The Pink Panther Strikes Again, The Great Muppet Caper, Police Academy 2, Who Framed Roger Rabbit, Spy Hard, and Freddy vs. Jason. Murphy beds have also been used in television series; for example, Caroline Channing used a Murphy bed in 2 Broke Girls, and an episode of Laverne and Shirley re-creates scenes from Chaplin's One A.M. Murphy bed gag. Murphy beds were a routine enough feature of comic film to invite commentary from retailers; one store based in Vancouver, British Columbia remarked in an advertisement, "Gone are the days of Laurel and Hardy where the beds were portrayed as a fold away trap for your worst enemies."In comics, the Murphy bed is depicted in the Tintin book Red Rackham's Treasure as being an invention of Professor Calculus.


== See also ==
Slide Away Bed
Sofa bed
Sleeping car


== References ==