The Batman Adventures: Mad Love is a one-shot comic book written by Paul Dini (writer on Batman: The Animated Series and Batman Beyond) and Bruce Timm  (executive producer on The New Batman/Superman Adventures and the co-creator of Batman: The Animated Series). Set in the continuity of Batman: The Animated Series, it won an Eisner Award for "Best Single Story" in 1994. It was later adapted (with minor alterations for pacing) as an episode of the animated series The New Batman Adventures.


== Plot summary ==
After having his latest plan to kill Commissioner Gordon foiled by Batman, The Joker retreats to one of his hideouts to plan his next move, but is being annoyed by his lovestruck sidekick Harley Quinn and kicks her out of their hideout. As Harley sits outside, she remembers of how she first met Joker, back when she was the psychologist Dr. Harleen Quinzel and spending her internship at Arkham Asylum. Convinced by Joker himself into doing it, Harleen interviewed Joker and learned that he was abused as a child by his alcoholic father. After more interviews, Harleen determined that Batman was the primary source of Joker's anger, but also that she was falling in love with him. Harleen became Joker's partner in escaping from the asylum in hopes that she could win his love.
Harley decides that the only way she can make the Joker love her back is to kill Batman, which she attempts to do by, after capturing him, preparing to feed him to a school of piranhas. Batman distracts her by telling her the Joker had been using her from the start, with Joker's stories to her of having an unhappy childhood being lies he has told to others, some with different details each time. When she tearfully insists Joker really will love her, Batman convinces her to call Joker so he will know she will accomplish her goal (as the piranhas would leave no convincing evidence, other than bones and a tattered costume which anyone can fabricate, and without a body, his removed utility belt won't be convincing enough ether). Joker arrives, however, infuriated by how Harley would try to kill Batman herself, telling her of how he's the only one who can kill Batman. When she tries explaining of how he would receive credit, he tells her the very fact of her explaining everything doesn't change anything, and knocks her out a window, where she is found by nearby police officers. Joker then decides nonetheless to use the opportunity to finally kill Batman, which escalates into a chase ending atop a moving subway train. Batman taunts Joker by saying that Harley, with her plan, had come closer to killing him than he ever did. Joker attacks him in rage, but Batman sends him plunging into a burning smokestack.
Back in Arkham Asylum, a severely injured Harley Quinn renounces The Joker forever, wanting nothing more than to heal and then leave Arkham for good. Lying on her bed a moment later, however, Harley finds flowers sent by her clownish beau with a "get well soon" card and falls in love with him again.


== Reprints ==
Mad Love was reprinted as a graphic novella in 1998 (ISBN 1563892448), and in 2009 the story was collected – alongside a number of others by Bruce Timm and Paul Dini – in a hardcover collection titled Batman: Mad Love and Other Stories (ISBN 9781401222451).


== Critical reaction ==
IGN Comics said that "Mad Love is everything a comic book should be" and called it "one Batman book everyone should read". The website ranked Mad Love #12 on a list of the 25 greatest Batman graphic novels.


== Awards ==
1994:
Won "Best Single Issue" Eisner Award
Bruce Timm was nominated for "Best Penciller/Inker or Penciller-Inker Team" Eisner Award, for his work on Mad Love


== In other media ==
An animated adaptation of the issue, nearly identical in script and design to the original comic, originally aired on the WB Network on January 16, 1999, as a part of The New Batman Adventures. The script was written by Paul Dini, and the episode was directed by Butch Lukic. Perhaps the only contrasts to the comic over the episode were the revamped character designs and the removal of minor scenes for pacing and time concerns.
In 2008, Warner Premiere Digital adapted Mad Love as part of DC Comics' motion comics line, available for download through digital outlets such as iTunes and Xbox Live.  Subscribers can download each chapter separately from Xbox Live, but iTunes groups the seven chapters into three downloads (Chapters 1 & 2, Chapters 3, 4, & 5, and Chapters 6 & 7).
Batman: Arkham Asylum, also penned by Dini, lifts much of its dialogue from Harley Quinn's patient interviews from Mad Love. Batman: Arkham Origins, the prequel to Asylum, also uses much of Mad Love's plot in retelling Harley Quinn's first encounter with the Joker.
Paul Dini and Pat Cadigan wrote a novel adaptation of the former's original story under the title, Harley Quinn: Mad Love, released on November 13, 2018 by Titan Books.


== Notes ==


== References ==