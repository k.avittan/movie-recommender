A Florida Enchantment (1914) is a silent film directed by Sidney Drew and released by Vitagraph Studios. It is notable for its depiction of lesbian, gay, and transgender themes. The film is based on the 1891 novel and 1896 play (now lost) of the same name written by Fergus Redmond and Archibald Clavering Gunter.


== Plot ==
In the film, Lillian Travers, a wealthy Northern woman about to be married, visits her aunt in Florida. While there, she stops in a curiosity shop and buys a small casket which contains a note and a vial of seeds. At her aunt's house she reads the note which explains that the seeds change men into women and vice versa. Angry with her fiancé, Fred, Lillian decides to test the effects of the seeds. The next morning, Lillian discovers that she has transformed into a man. Lillian's transformation into Lawrence Talbot has also sometimes been read as a transformation into a butch lesbian. This reading is bolstered by the later transformation of Lillian's fiancé into what could be an effeminate gay man. However, as Lillian and her fiancé are shown attracted both to each other and to the same sex (albeit at different times), the film has also been considered to have the first documented appearance of bisexual characters in an American motion picture.


== Cast ==
Edith Storey - Lillian Travers/Lawrence Talbot
Sidney Drew - Dr. Frederick Cassadene
Ethel Lloyd - Jane
Grace Stevens - Constancia Oglethorpe
Charles Kent - Major Horton
Jane Morrow - Bessie Horton
Ada Gifford - Stella Lovejoy
Lillian Burns - Malvina
Allan Campbell - Stockton Remington
Cortland van Deusen - Charley Wilkes
Frank O'Neil - Gustavus Duncan


== Production background ==
The film is also known for its use of blackface antics; an aspect carefully dissected in Siobhan Somerville's "Queering the Color Line."  Since its inclusion in Vito Russo's The Celluloid Closet, the film has increasingly been seen as one of the earliest film representations of homosexuality and cross-dressing in American culture.


== References ==


== External links ==
A Florida Enchantment on IMDb