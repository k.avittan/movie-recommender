The White Shark Café is a remote mid-Pacific Ocean area noted as a winter and spring habitat of otherwise coastal great white sharks.
The area, halfway between Baja California and Hawaii, received its unofficial name in 2002 from researchers at Stanford University's Hopkins Marine Station who were studying the great white shark species using satellite tracking tags. They identified a zone with a radius of approximately 250 kilometres (160 mi) centered at approximately 23.37°N 132.71°W﻿ / 23.37; -132.71. The findings, which were initially published in the January 3, 2002 issue of the journal Nature, showed three of four tagged sharks traveled to the Café during a six-month period after they were tagged off the central coast of California.Although the area had not previously been suspected as a shark habitat, when mapping the satellite tracking data, researchers discovered that members of the species frequently travel to and loiter in the area. It was once believed the area had very little food for the animals (researchers described it as the shark equivalent of a desert), but research in early 2018 by the vessel Falkor showed that there is a rich and diverse food chain too deep to be detected by satellites that provides a potentially abundant food supply for the sharks.  Male, female, and juvenile great whites have been tracked there.The sharks tracked to the area came from diverse rookeries along the North American coast. They typically took up to 100 days to arrive, traveling around 1 m/s (3.3 ft/s), during which they make periodic dives as deep as 3,000 feet (910 m). While at the Café, they dive to depths of 1,500 feet (460 m) as often as once every ten minutes.By 2006, researchers had observed consistent migration and other behavior. Tracking data indicates that white sharks will leave feeding grounds near the coast in winter, travel to the Café, and some may even summer near Hawaii. But many linger in the Café, often for months, before returning to the coast in the fall, coinciding with the elephant seal breeding season (a favored prey).


== Notes ==


== Sources ==
Jorgensen, SJ; Reeb, CA; Chapple, TK; Anderson, S; Perle, C (2010). "Philopatry and Migration of Pacific White Sharks". Proceedings of the Royal Society B. 277: 679–688. doi:10.1098/rspb.2009.1155. PMC 2842735. PMID 19889703. direct PDF link


== External links ==
Voyage to the White Shark Café, website for the 2018 expedition aboard Falkor
Tagging of Pacific Predators: White Sharks, part of the Census of Marine Life
White Shark Cafe - The Film, a documentary about the White Shark Café