Richard Harding Davis (April 18, 1864 – April 11, 1916) was an American journalist and writer of fiction and drama, known foremost as the first American war correspondent to cover the Spanish–American War, the Second Boer War, and the First World War. His writing greatly assisted the political career of Theodore Roosevelt. He also played a major role in the evolution of the American magazine. His influence extended to the world of fashion, and he is credited with making the clean-shaven look popular among men at the turn of the 20th century.


== Biography ==
Davis was born on April 18, 1864 in Philadelphia, Pennsylvania. His mother Rebecca Harding Davis was a prominent writer in her day. His father, Lemuel Clarke Davis, was himself a journalist and edited the Philadelphia Public Ledger. As a young man, Davis attended the Episcopal Academy. In 1882, after an unhappy year at Swarthmore College, Davis transferred to Lehigh University, where his uncle, H. Wilson Harding, was a professor. While at Lehigh, Davis published his first book, The Adventures of My Freshman (1884), a collection of short stories. Many of the stories had originally appeared in the student magazine the Lehigh Burr. In 1885, Davis transferred to Johns Hopkins University.After college, his father helped him gain his first position as a journalist at the Philadelphia Record, but he was soon dismissed. After another brief position at the Philadelphia Press, Davis accepted a better-paying position at the New York Evening Sun where he gained attention for his flamboyant style and his writing on controversial subjects such as abortion, suicide and execution. He first attracted attention in May to June 1889, by reporting on the devastation of Johnstown, Pennsylvania, following the destructive flood. He added to his reputation by reporting on other noteworthy events such as the first electrocution of a criminal (the execution of William Kemmler in 1890).
Davis became a managing editor of Harper's Weekly, and was one of the world's leading war correspondents at the time of the Second Boer War in South Africa. As an American, he had the opportunity to see the war first-hand from both the British and Boer perspectives. Davis also worked as a reporter for the New York Herald, The Times, and Scribner's Magazine.
He was popular among a number of leading writers of his time, and is considered the model for illustrator Charles Dana Gibson's dashing "Gibson man", the male equivalent of his famous Gibson Girl. He is mentioned early in Sinclair Lewis' book Dodsworth as the example of an exciting, adventure-seeking legitimate hero.
Davis had success with his 1897 novel Soldiers of Fortune, which he turned into a play written by Augustus Thomas. His novel was filmed twice, in 1914 and in 1919 by Allan Dwan. The 1914 version starring Dustin Farnum was shot on the Cuban locations that Davis used in his novel, and Davis was present during the filming.During the Spanish–American War, Davis was on a United States Navy warship when he witnessed the shelling of Matanzas, Cuba, a part of the Battle of Santiago de Cuba. His story made headlines, but as a result, the Navy prohibited reporters from being aboard any American naval vessel for the rest of the war.

Davis was a good friend of Theodore Roosevelt, and he helped create the legend surrounding the Rough Riders, of which he was made an honorary member. Some have even gone so far to accuse Davis of involvement in William Randolph Hearst's alleged plot to have started the war between Spain and the United States in order to boost newspaper sales; however, Davis refused to work for Hearst after a dispute over fictionalizing one of his articles.

Despite his alleged association with yellow journalism, his writings of life and travel in Central America, the Caribbean, Rhodesia and South Africa during the Second Boer War were widely published. He was one of many war correspondents who covered the Russo-Japanese War from the perspective of the Japanese forces.Davis later reported on the Salonika Front of the First World War, where he was arrested by the Germans as a spy, but released.


== Personal life ==
Davis was married twice, first to Cecil Clark, an artist, in 1899, and, following their 1912 divorce, to Bessie McCoy, an actress and vaudeville performer, who is remembered for her signature "Yama Yama Man" routine. Davis and Bessie had a daughter, Hope.Davis died of a heart attack on April 11, 1916, while on the telephone. It was seven days before his 52nd birthday. His friend and fellow author John Fox, Jr. was surprised by his sudden death, writing, "He was so intensely alive that I cannot think of him as dead—and I do not. He is just away on another of those trips and it really seems queer that I shall not hear him tell about it." His wife Bessie would also die young, at age 42 in 1931 from intestinal problems.


== Legacy ==
A plaque denoting his boyhood home can be seen at 21st and Chancellor Streets in Philadelphia.
Davis's Gallegher and Other Stories became the series Gallegher, starring Roger Mobley, Edmond O'Brien, and Harvey Korman on Walt Disney's Wonderful World of Color on NBC.


== Partial list of works ==

Stories for Boys (1891)
Cinderella and Other Stories (1891)
Gallegher, and Other Stories (1891)
The West from a Car Window (1892)
Van Bibber and Others (1892)
The Rulers of the Mediterranean (1893)
The Exiles, and Other Stories (1894)
Our English Cousins (1894)
About Paris (1895)
The Princess Aline (1895)
Three Gringos in Central America and Venezuela (1896)
Soldiers of Fortune (1897)
Cuba in War Time (1897)
Dr. Jameson's Raiders vs. the Johannesburg Reformers (1897)
A Year From a Reporter's Note-Book (1898)
The King's Jackal (1898)
The Cuban & Porto Rican Campaigns (1899)
The Lion and the Unicorn (1899)
With Both Armies (1900), on the Second Boer War
Ranson's Folly (1902)
Captain Macklin: His Memoirs (1902)
The Bar Sinister (1903)
Real Soldiers of Fortune (1906) – an early biography of Winston Churchill (1874–1965), Major Frederick Russell Burnham, D.S.O., (1861–1947), Chief of Scouts, General Henry Douglas McIver (1841–1907), James Harden-Hickey (1854–1898), Captain Philo McGiffen (1860–1897), William Walker (1824–1860)
The Congo and coasts of Africa (1907)
The Scarlet Car (1906)
Vera, the Medium (1908)
The White Mice (1909)
Once Upon A Time  (1910)
Notes of a War Correspondent (1910)
The Nature Faker (1910)
The Red Cross Girl (1912)
The Lost Road and Other Stories (1913)
Peace Manoeuvres; a Play in One Act (1914)
The Boy Scout (1914)
With the Allies (1914)
With the French in France and Salonika (1916)
The Man Who Could Not Lose (1916)
The Deserter (1917)


== Filmography ==
Ranson's Folly, directed by Edwin S. Porter (1910, short film, based on the novel Ranson's Folly)
Her First Appearance, directed by Ashley Miller (1910, short film, based on the short story Her First Appearance)
Gallegher, directed by Ashley Miller (1910, short film, based on the novel Gallegher)
The Winning of Miss Langdon, directed by Edwin S. Porter (1910, short film, based on the short story A Peace Manoeuvres)
The Romance of Hefty Burke (1910, short film, based on the short story The Romance in the Life of Hefty Burke)
An Eventful Evening (1911, short film, based on the short story Miss Civilization)
The Disreputable Mr. Raegen (1911, short film, based on the short story My Disreputable Friend Mr. Raegen)
How the Hungry Man Was Fed (1911, short film, based on the short story The Hungry Man Was Fed)
Van Bibber's Experiment, directed by Ashley Miller (1911, short film, based on the short story Van Bibber's Burglar)
The Crucial Test (1911, short film, based on the short story A Derelict)
How Sir Andrew Lost His Vote, directed by Ashley Miller (1911, short film, based on the novel In the Fog)
Eleanor Cuyler (1912, short film, based on the novel Eleanor Cuyler)
Soldiers of Fortune, directed by William F. Haddock (1914, based on the novel Soldiers of Fortune)
The Man Who Could Not Lose, directed by Carlyle Blackwell (1914, based on the novel The Man Who Could Not Lose)
The Last Chapter, directed by William Desmond Taylor (1914, based on the short story An Unfinished Story)
The Lost House, directed by Christy Cabanne (1915, short film, based on the short story The Lost House)
Captain Macklin, directed by John B. O'Brien (1915, based on the novel Captain Macklin: His Memoirs)
The Dictator, directed by Oscar Eagle (1915, based on the play The Dictator)
The Galloper, directed by Donald MacKenzie (1915, based on the play The Galloper)
Ranson's Folly, directed by Richard Ridgely (1915, based on the novel Ranson's Folly)
Playing Dead, directed by Sidney Drew (1915, based on the novel Playing Dead)
The Buried Treasure of Cobre, directed by Frank Beal (1916, short film, based on the novel The Buried Treasure of Cobre)
Somewhere in France, directed by Charles Giblyn (1916, based on the novel Somewhere in France)
Vera, the Medium, directed by Broncho Billy Anderson (1917, based on the novel Vera, the Medium)
The Boy Who Cried Wolf, directed by Edward H. Griffith (1917, based on the short story The Boy Who Cried Wolf)
Billy and the Big Stick, directed by Edward H. Griffith (1917, based on the short story Billy and the Big Stick)
Gallegher, directed by Ben Turbett (1917, short film, based on the novel Gallegher)
The Scarlet Car, directed by Joseph De Grasse (1917, based on the novel The Scarlet Car)
The Trap, directed by Frank Reicher (1919, based on the play The Trap)
Soldiers of Fortune, directed by Allan Dwan (1919, based on the novel Soldiers of Fortune)
The Men of Zanzibar, directed by Rowland V. Lee (1922, based on the short story The Men of Zanzibar)
Restless Souls, directed by Robert Ensminger (1922, based on the novel Playing Dead)
The Dictator, directed by James Cruze (1922, based on the play The Dictator)
The Scarlet Car, directed by Stuart Paton (1923, based on the novel The Scarlet Car)
The Exiles, directed by Edmund Mortimer (1923, based on the novel The Exiles)
Stephen Steps Out, directed by Joseph Henabery (1923, based on the novel The Grand Cross of the Crescent)
Cupid's Fireman, directed by William A. Wellman (1923, based on the short story Andy M'Gee's Chorus Girl)
24 short films starring Earle Foxe (1924–1927, based on the "Van Bibber" short stories)
Honor Among Men, directed by Denison Clift (1924, based on the novel The King's Jackal)
White Mice, directed by Edward H. Griffith (1926, based on the novel The White Mice)
Ranson's Folly, directed by Sidney Olcott (1926, based on the novel Ranson's Folly)
Almost Human, directed by Frank Urson (1927, based on the novel The Bar Sinister)
Let 'Er Go Gallegher, directed by Elmer Clifton (1928, based on the novel Gallegher)
Driftwood, directed by Christy Cabanne (1928, based on the short story Driftwood)
Fugitives, directed by William Beaudine (1929, based on the novel The Exiles)
It's a Dog's Life, directed by Herman Hoffman (1955, based on the novel The Bar Sinister)
Gallegher (1965–1967, Disney TV series, 10 episodes, based on the novel Gallegher)


== References ==


== Further reading ==
Bleiler, Everett (1948). The Checklist of Fantastic Literature. Chicago: Shasta Publishers. p. 32.
Lubow, Arthur. The Reporter Who Would Be King: A Biography of Richard Harding Davis (Biography. New York: Charles Scribner's Sons, 1992). ISBN 0-684-19404-X;
Osborn, Scott Compton. (1960) Richard Harding Davis: The Development of a Journalist (Dissertation thesis, University of Kentucky. OCLC 44083545. [reprinted by Twayne Publishers, Boston, 1978. ISBN 978-0-8057-7192-3; OCLC 3965741
Downey, Fairfax Davis. Richard Harding Davis: His Day. New York: C. Scribner's Sons, 1933.
Miner, Lewis S. Front Lines and Headlines: The Story of Richard Harding Davis. New York: J. Messner, 1959.
Quinby, Henry Cole. Richard Harding Davis: A Bibliography. New York: E.P. Dutton & Co., 1924.


== External links ==
Works by Richard Harding Davis at Project Gutenberg
Works by or about Richard Harding Davis at Internet Archive
Works by Richard Harding Davis at LibriVox (public domain audiobooks) 
Works by Richard Harding Davis at Online Books Page
Finding Aid to Richard Harding Davis Miscellaneous Correspondence and Other Documents, 1887-1916, Special Collections, Linderman Library, Lehigh University
"Rheims during the Bombardment" from Scribner's Magazine, January 1915 at World War One Gallery
Collected Journalism of Richard Harding Davis at The Archive of American Journalism
Finding Aid for the Papers of Richard Harding Davis, 1863-1916, Special Collections, University of Virginia Library