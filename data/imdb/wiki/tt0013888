Brass Commandments is a 1923 American silent western film directed by Lynn Reynolds and starring William Farnum, Wanda Hawley, and Tom Santschi. The novel of the same name by Charles Alden Seltzer that the film is based upon was later filmed as Chain Lightning (1927).


== Plot ==
As described in a film magazine, Flash Lanning (Farnum) returns West as the request of the sheriff to put an end to the cattle rustling. In the small hotel he finds a big bully attempting to kiss Gloria Hallowell (Hawley), the young woman at the desk, and he immediately goes to her assistance. She does not recognize him but tells him of her love for Lanning, although she has never met him. Flash does not tell her that he is that man, and when she finds out later, she changes her feelings in the matter. Flash is attacked by Campan (Santschi), the leader of the local gang, but he overpowers him and tells the man to leave town. Ellen Bosworth (Adams), returning to her ranch, overhears that supposed friend Clearwater (Gordon) is one of the rustlers. She tells Flash of this, and he forces Clearwater to warn him of when the next raid will be conducted. However, Campan captures the two young women and takes them into the desert. Flash learns of this and sets out in pursuit. A sandstorm overtakes Campan and his party and they are about to perish when Flash arrives. He rescues the two young women and leaves Campan to his fate. He tells Gloria of his love for her and she of her love for him.


== Cast ==
William Farnum as Stephen 'Flash' Lanning
Wanda Hawley as Gloria Hallowell
Tom Santschi as Campan
Claire Adams as Ellen Bosworth
Charles Le Moyne as Dave De Vake
Joe Rickson as Tularosa
Lon Poff as Slim Lally
Al Fremont as Bill Perrin
Joseph Gordon as Clearwater
C.E. Anderson as Bannock


== Preservation ==
Brass Commandments is not listed as a holding in any film archives making it a lost film.


== References ==


== Bibliography ==
Solomon, Aubrey. The Fox Film Corporation, 1915-1935: A History and Filmography. McFarland, 2011.


== External links ==
Brass Commandments on IMDb
Synopsis at AllMovie