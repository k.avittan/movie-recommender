Chester Cooper Conklin (January 11, 1888 – October 11, 1971) was an early American film  comedian who started at Keystone Studios as one of Mack Sennett’s Keystone Cops, often paired with Mack Swain. He appeared in a series of films with Mabel Normand and worked closely with Charlie Chaplin, both in silent and sound films.


== Early life ==
Conklin was born in Oskaloosa, Iowa. One of three children, he grew up in a violent household. When he was eight, his mother was found burned to death in the family garden. Although first judged a suicide, his father, a devoutly religious man who hoped his son would be a minister, was eventually charged with murder, but found not guilty at trial.
Conklin won first prize when he gave a recitation at a community festival. A few years later, he ran away from home after vowing to a friend he would never return, a promise he kept. Heading to Des Moines he found employment as a hotel bellhop, but then moved to Omaha, Nebraska, where his interest in theatre led to a career in comedic acting. In St. Louis, Missouri, he saw a performance by the vaudeville team of Joe Weber and Lew Fields, which prompted Conklin to develop a character based on his boss at the time, a man with a thick accent and a bushy walrus moustache. With this character, Conklin broke into vaudeville, and spent several years touring with various stock companies, doing vaudeville shows and minstrel shows. He also performed as a clown with the Al G. Barnes Wild Animal Show.


== Career ==

After seeing several Mack Sennett comedies while in Venice, California during the 1913 winter break, the 27-year-old Conklin went to Keystone Studios, applied for a job and was hired as a Keystone Kop with a salary of $3 a day. Sennett directed him in his first film, a comedy short titled Hubby's Job.In 1914, Conklin co-starred with Mabel Normand in a series of films: Mabel's Strange Predicament, Mabel's New Job, Mabel's Busy Day and Mabel at the Wheel.  In that same year he appeared in Making a Living, in which Charlie Chaplin made his film debut. He would go on to make more than a dozen films with Chaplin while at Keystone and the two became lifelong friends. Years later, Conklin would perform with Chaplin in two more feature-length films, first in 1936 in Modern Times and in 1940's The Great Dictator. During this time, Chaplin kept Conklin on year-round salary.While at Keystone, Conklin became most famous when he was teamed up with the robust comic Mack Swain to make a series of comedies. With Swain as "Ambrose" and Conklin as the grand mustachioed "Walrus", they performed these roles in several films including The Battle of Ambrose and Walrus and Love, Speed and Thrills, both made in 1915. Beyond these "Ambrose & Walrus" comedies, the two appeared together in twenty-six different films.
In 1920, when Sennett refused to discuss a contract renewal with Conklin and insisted on referring him to an underling, Conklin quit and went to Fox Film Corporation, which had earlier approached him about doing a series of comedy shorts. He also worked at the Famous Players-Lasky Corporation studio. In between, he had a significant role as ZaSu Pitts' father in director Erich von Stroheim's acclaimed 1924 MGM production, Greed, although the part was cut from the film and the footage is now lost, and in 1928 in the Christie Film Company version of Tillie's Punctured Romance with W.C. Fields,  which had nothing to do with the 1914 Chaplin version (in which Conklin had also appeared) aside from the title. Paramount Pictures teamed up Conklin and Fields for a series of comic films between 1927 and 1931.Conklin made the transition to talkies and, although he would continue to act for another thirty years, age and the shift in moviegoing tastes to more sophisticated comedy saw his roles limited to secondary or smaller parts in shorts, including the Three Stooges shorts Flat Foot Stooges (as a fire chief), Dutiful But Dumb (as a bartender), Three Little Twirps (as a Circus butcher), Phony Express (as a bartender), and Micro-Phonies (as a drunken pianist who answers a song request with "Know it? I wrote it!"). Conklin also appeared in films that appealed to nostalgia for the silent era, such as Hollywood Cavalcade (1939) and The Perils of Pauline (1947). In Soundies musicals, he appeared with other silent-comedy alumni as The Keystone Kops, as well as on the televised This Is Your Life tribute to Mack Sennett. Conklin was part of Preston Sturges' unofficial "stock company" of character actors in the 1940s, appearing in cameo parts in six films written by Sturges.  In 1957, he was a guest challenger on the TV panel show To Tell The Truth, dressed in his Keystone Kops uniform.


== Personal life ==
On April 12, 1933, Conklin was divorced from Minnie V. Conklin after a marriage of 18 years and nine months. He married Margherita Rouse on May 5, 1934, in Hollywood. She died on May 14, 1937. On June 17, 1965, Conklin married former actress June Gunther in Las Vegas.


== Decline and death ==
Conklin's career hit bottom in the 1950s, and he took work as a department-store Santa Claus to make ends meet.  In the 1960s, Conklin was living at the Motion Picture Country Home and Hospital when he fell in love with another patient there, June Gunther. The two got married in Las Vegas in 1965, his fourth marriage and her fourth, and set up housekeeping in Van Nuys, California; the groom was seventy-nine and the bride sixty-five.  Conklin made one last film after that, a Western comedy, A Big Hand for the Little Lady, released in 1966.
Chester Conklin died in autumn 1971 in California at the age of 85. He was cremated and his ashes were given to his family. Following his death, his great nephew, Robert Stoltz, along with his sisters rested his ashes at sea in the Pacific Ocean.
For his contribution to the motion picture industry, Conklin has a star on the Hollywood Walk of Fame at 1560 Vine Street.


== Selected filmography ==


== Miscellania ==
Contrary to some sources Conklin was not born Jules Cowles who was another silent actor.


== References ==


== External links ==
Chester Conklin on IMDb
Chester Conklin at AllMovie
Chester Conklin at the TCM Movie Database 
Chester Conklin at Find a Grave
Chester Conklin at Virtual History