Western Kentucky (rivalry)The Marshall Thundering Herd football team is an intercollegiate varsity sports program of Marshall University. The team represents the university as a member of the Conference USA Eastern division of the National Collegiate Athletic Association, playing at the NCAA Division I Football Bowl Subdivision level.
Marshall plays at Joan C. Edwards Stadium, which seats 38,227 and is expandable to 55,000.  As of the end of the 2015 football season, Marshall has an impressive 148–26 overall record at Joan C. Edwards Stadium for a winning percentage of .851.  The University of Alabama ranks second with an .825 winning percentage at Bryant–Denny Stadium. The stadium opened in 1991 as Marshall University Stadium with a crowd of 33,116 for a 24–23 win over New Hampshire. On September 10, 2010, the Thundering Herd played the in-state rival West Virginia Mountaineers in Huntington in front of a record crowd of 41,382.  Joan C. Edwards Stadium is one of two Division I stadium named solely for a woman with South Carolina's Williams-Brice Stadium being the other. The playing field itself is named James F. Edwards Field after Mrs. Edwards' husband, businessman and philanthropist James F. Edwards.


== History ==


=== Early history (1895–1934) ===

Boyd Chambers was Marshall's head football coach from 1909–1916. He is best known for calling the "Tower Play", where one receiver lifted another up on his shoulders to complete a pass, during the 1915 season.


=== Tolley era (1968–1970) ===

 
Rick Tolley was Marshall's head football coach for two seasons, coming to Marshall from his post as defensive line coach for Wake Forest and posting records of 3–7 and 3–6 before being killed on November 14, 1970 in the infamous plane crash in which all 75 passengers, including 37 players, five coaches, administrators, family and friends (along with the Southern Airways five-person crew) were killed traveling home from a game against East Carolina.


=== Jim Donnan era (1990–1995) ===
Led by head coach Jim Donnan, who came to Marshall from his post as offensive coordinator at Oklahoma, Marshall won the Division I-AA national championship in 1992 over Youngstown State (31–28) and was national runner-up in 1991, 1993 and 1995. Marshall set an I-AA record with five straight seasons making at least the semi-finals of the I-AA Playoffs from 1991–95 (and added one more in 1996). Donnan was named NCAA Division I-AA Coach of the Year twice during his tenure at Marshall and resigned after the 1995 season to accept the head football coach position at Georgia.


=== Bob Pruett era (1996–2004) ===

Bob Pruett left his post as defensive coordinator at Florida under Steve Spurrier to become head football coach at Marshall, where he served for nine seasons from 1996 to 2004. During his tenure at Marshall, the Thundering Herd compiled a record of 94–23 (.803 winning percentage), featured two undefeated seasons, won six conference championships, won 5 of 7 bowl games, and captured the I-AA National Championship in 1996. Marshall moved to Division I-A and the Mid-American Conference in all sports in 1997. The 1996 team, with Chad Pennington, Randy Moss, John Wade, Chris Hanson, Eric Kresser, Doug Chapman and many other players who played professional football, was 15–0, had no game closer than a two touchdown win and was ranked No. 1 all-season. Marshall won the MAC title five of its eight seasons (1997-98-99-2000–2002) and were runners up in 2001 in the conference before moving to Conference USA in 2005. Since moving back to Division I-A, Marshall has finished in the Top 25 four times: 1999 (10th AP/10th coaches' poll), 2001 (21st coaches' poll), 2002 (24th AP/19th coaches' poll), 2014 (23rd AP/22nd coaches' poll). Marshall fell to Ole Miss in the 1997 Motor City Bowl, 34–31, but won the next three games in Michigan's Pontiac Silverdome, beating Louisville 48–29 in 1998, beating No. 25 BYU 21–3 in 1999 to finish 13–0 and beating Cincinnati in 2000, 25–14. Marshall and East Carolina matched-up in one of college football's greatest bowl games in 2001 at the GMAC Bowl in Mobile, Alabama, a 64–61 double overtime win by the Herd over the Pirates of Conference USA. It is one of the highest scoring bowl games of all-time, and the Herd rallied from a 38–8 halftime hole behind Byron Leftwich's five touchdown passes. Marshall would fall to the Bearcats in the 2004 Plains Capital Fort Worth Bowl at TCU's Amon G. Carter Stadium, 32–14, in Bob Pruett's final game as head coach before his retirement.


=== Mark Snyder era (2005–2009) ===

Mark Snyder came to his alma mater to become head football coach from his defensive coordinator position at Ohio State. Snyder coached the likes of Ahmad Bradshaw, Marcus Fitzgerald and Cody Slate during his time as head coach at Marshall. Snyder's best season was a 6–6 2009 season, which turned out to be his last. He resigned after five seasons, that included only one bowl berth, the 2009 Little Caesar's Pizza Bowl.


=== Doc Holliday era (2010–present) ===
On December 17, 2009, Marshall officially named Doc Holliday, an assistant coach at WVU under Bill Stewart, as the next head coach for the Thundering Herd football team. Marshall athletic director Mike Hamrick said Holliday had signed a five-year contract and would be paid $600,000 per season. Holliday, a WVU alum, almost defeated Stewart's Mountaineers in 2010, but an untimely fumble by freshman Tron Martinez led to the Herd blowing a 15-point lead in the game's final minutes, breaking the hearts of Herd fans. Holliday then led Marshall to a 10–4 season in 2013, capped with a victory in the Military Bowl. In the 2014 season he led the team to a 13–1 season, winning the school's first C-USA Championship and the inaugural Boca Raton Bowl against Northern Illinois 52–23.


== Conference affiliations ==
Independent (1895–1925, 1969–1975)
West Virginia Athletic Conference (1925–1933)
Buckeye Conference (1933–1939)
WVIAC (non-competing member, membership in regards to school being accredited College)  (1939–1948)
Ohio Valley Conference (1948–1952)
Mid-American Conference (1953–1969, 1997–2005)
Southern Conference (1977–1997)
Conference USA (2005–present)


== Championships ==


=== National championships ===
Marshall has won two NCAA Division I-AA national championships.


=== Conference championships ===
Marshall has won 13 conference championships, 12 outright and one shared.
† Co-champions


=== Division championships ===
Marshall has eight division championships.
† Co-champions


== Bowl games ==

Marshall has been invited to play in 16 bowl games in its history, compiling a record of 12–4.


== Head coaches ==


== Division I-AA playoff results ==
Marshall has appeared in the I-AA playoffs eight times, compiling a record 23–6 in those games. They are two-time I-AA National Champions and four-time national runners-up.


== Rivalries ==


=== Ohio ===

Marshall competes against Ohio in the Battle for the Bell, with a traveling bell trophy as the prize for the victor. With Marshall's move to Conference USA in 2005 this rivalry game has been on hiatus. The regularly scheduled series resumed between the two schools in 2010. The rivalry was renewed in 2009 when the Herd and Bobcats faced off in the 2009 Little Caesars Pizza Bowl, which the Herd won 21–17. Ohio leads the all-time series over Marshall, however the Thundering Herd have won 10 of 15 meetings since rejoining the FBS in 1997. The six-year series contract between the two schools ran out following the 2015 season. The rivalry series will return for 2019 and 2020, when Marshall and Ohio are scheduled to play a home-and-home against one another; first at Marshall, then at Ohio. Ohio leads the series 33–20–6 through the 2018 season.


=== West Virginia ===

Marshall played West Virginia in the annual Friends of Coal Bowl until 2012. Marshall and WVU first played in 1911, but it wasn't until 2006 before the two schools from the "Mountain State" faced off annually for the Governor's Cup. Some believe the rivalry began due to political pressure from the state government. The two last played in 2012, and there are no immediate plans to renew the rivalry. West Virginia holds a 12–0 lead in the series as of 2019.


=== East Carolina ===

Marshall and East Carolina have a "friendly" rivalry with one another. They are emotionally bonded by the tragic plane crash on November 14, 1970. The Thundering Herd were coming back from Greenville, North Carolina after a 17–14 loss to the Pirates when their plane crashed near Ceredo, West Virginia. The teams have been bonded ever since.
One of Marshall and ECU's most memorable games was the 2001 GMAC Bowl as they combined for a bowl record, 125 points, as Marshall overcame a 30-point deficit to beat East Carolina 64–61 in double overtime. After Marshall defeated East Carolina in 2013, it marked ECU's last conference match-up as a member of Conference USA. On April 3, 2014, both schools announced that the two teams will meet again for a home and home seridatees in 2020 and 2021. East Carolina will host Marshall at Dowdy–Ficklen Stadium in Greenville, NC on September 5, 2020. Marshall will host the second and final game of the series at Joan C. Edwards Stadium in Huntington, West Virginia on September 11, 2021.ECU was 6–3 against the Herd from 2005 to 2013 when both schools were in Conference USA. East Carolina leads the series 10–5 as of 2019.


== Home venues ==
Fairfield Stadium (1927–1990)
Joan C. Edwards Stadium (1991–present)


== Herd football traditions ==
Marshall football is rich in traditions. Some Marshall football traditions include:

Marco – The school mascot is an American Bison, the species named the National Mammal in the summer of 2016, and Marco always sports a Marshall jersey. Sometimes called a buffalo, Marco had a female companion in the 1970s, Marsha, and a green-furred "son" named Buffy, who appeared in 1979–80. MARshall COllege is where the name came from, kept when the College became a University in 1961.
Marching Thunder – The Marshall University Marching Band known as the "Marching Thunder!"
"Sons of Marshall" – Marshall's fight song: "We are the sons of Marshall, Sons of the great John Marshall. Year after year we go to Marshall U., Cheering for our team and gaining knowledge, too. Proudly we wear our colors, Love and loyalty we share – Sure from far and near, You'll always hear 'The Wearing of the Green' For it's the Green and White of Marshall U!"
"We Are…Marshall" Chant – Marshall's cheer, and title of movie in 2006 about plane crash and rebirth of program.
Thunder Clap – Marshall fans clap their hands over their heads in unison following some Marshall scores. One clap per point scored in the game for the Herd.
Marshall Cheerleaders – One cheerleading tradition occurs after every Marshall touchdown. A male cheerleader presses a female cheerleader over his head once for each point scored in the game by Marshall (as the fans do the Thunder Clap).
Marshall Maniacs – The student cheering section at most Marshall football games.
Thunder Walk – Marshall players and coaches make their way to the locker room through a gathering of Thundering Herd fans on the West Lot, and led in by the Herd cheerleaders and "Marching Thunder" Marshall marching band, prior to every home game.


== Top 25 Finishes ==


=== I-AA Polls ===
Sources:


=== 1-A/FBS Polls ===
Sources:


== Individual award winners ==


== All-Americans ==
Sources:
Mike Barber (1987, 1988)
Mike Bartrum (1992)
Rogers Beckett (1999)
Troy Brown (1991, 1992)
B. J. Cohen (1995, 1996)
Melvin Cunningham (1995, 1996)
Josh Davis (2001)
Sean Doctor (1987, 1988)
Johnathan Goddard (2004)
Chris Hanson (1996)
Eric Kresser (1996)
Byron Leftwich (2001, 2002)
Billy Lyon (1994, 1995, 1996)
Albert McClellan (2005, 2006)
Randy Moss (1996, 1997)
Michael Payton (1991, 1992)
Chad Pennington (1998, 1999)
Steve Sciullo (2002)
Cody Slate (2006)
Mark Snyder (1987)
Darius Watts (2001, 2002)


== Hall of Fame ==


=== College football ===
Marshall has five players and one coach in the College Football Hall of Fame, starting with Mike Barber (1985–88) who was a record-setting receiver for Marshall who helped lead the Herd to its first I-AA title game in 1987 and its first Southern Conference title in 1988. He still holds the receiving yardage record at MU with over 4,200 yards and was a two-time All-American before he was drafted by the San Francisco 49ers in the fourth round in 1989. Barber also played for the Arizona Cardinals and Cincinnati Bengals.
Harry "Cy" Young, who starred in football and baseball at Marshall College (University status in 1961) from 1910–1912. Young then left Marshall, and was a two-sport All-American at Washington & Lee. He is a member of the W&L HOF, MU HOF, WV Sportswriters HOF and Virginia Sports HOF besides the College FB HOF.
Jackie Hunt (1939–41) set a national scoring record in 1940 with 27 touchdowns in a ten-game season. He rushed for nearly 4,000 yards for Thundering Herd, a hometown star for the Huntington High Pony Express before joining Marshall. He was drafted by the Chicago Bears and was a two-time All-American, playing in the Blue-Gray Game following his career.
Troy Brown (1991–92) considered the single-most dangerous scoring threat in all of Division I-AA during his two seasons in Huntington, few can match the heralded career of Marshall's record-breaking wide receiver. A dual threat on the playing field, Brown's elusive nature as a receiver and kick returner led the Thundering Herd to back-to-back trips to the Division I- AA (now FCS) National Championship game, garnering the NCAA title in 1992. He caught 139 receptions for 2,746 yards and 24 touchdowns in his career en route to earning First Team All-America honors his senior year. Brown went on to play 14 years in the NFL with the New England Patriots, where he became the franchise's all-time leading receiver and won three Super Bowls with the team.
Jim Donnan (1990–1995) the only coach representing Marshall in the College Football Hall of Fame. Donnan spent six seasons with Marshall and posted a 64–21 record. He led the Thundering Herd to four Division I-AA National Championship games, winning the 1992 national title. In 1994, the Thundering Herd won the Southern Conference Championship. His 15–4 playoff record ranks second best in NCAA FCS history. He was named Division I- AA Coach of the Year in 1992 and 1995.


=== Pro football ===
Frank Gatski, C, 1985. Gatski is the only Marshall player to have his jersey number retired and was Marshall's first player in the Professional Football Hall of Fame. The university retired Gatski's No. 72 during a halftime ceremony at Joan C. Edwards Stadium on October 15, 2005. Gatski died a month later, at age 86. During his career with the Cleveland Browns (1946–56) and the Detroit Lions (1957) he won eight championships in 11 title game appearances. Cleveland won the All-American Football Conference four straight years, going 14–0 in 1948, before joining the NFL. The Browns won NFL titles in 1950, 1954 and 1955 and were runners-up in 1951, 1952 and 1953. Gatski's Lions beat the Browns for his final title in 1957. The 31st Street Bridge, connecting Huntington to Proctorville, Ohio, is also named in Gatski's honor, joining U.S. Senator Robert Byrd (formerly the Sixth St. Bridge) and Congressman Nick Rahall (the former 17th St. Bridge) among three structures stretching across the Ohio River from West Virginia to Ohio.
Randy Moss, WR, 2018. Moss is the second player in the Professional Football Hall of Fame to have been a member of the Thundering Herd. In a career that spanned 14 seasons with the Minnesota Vikings, Oakland Raiders, New England Patriots, Tennessee Titans, and the San Francisco 49ers, Moss en-massed the fourth-most receiving yards (15,292) and second-most receiving touchdowns (156) in NFL history. Moss appeared in two Super Bowls (losing both); Super Bowl XLII with the Patriots and Super Bowl XLVII with the 49ers. As of the end of the 2017 NFL season, Moss still currently holds the NFL record for 17 receiving touchdowns as a rookie (1998), when he also won the AP Offensive Rookie of the Year award, and most receiving touchdowns in a season (23), set back in 2007. Moss over his career also reached the 1,000-yard receiving mark eight times, was elected to six Pro Bowls (winning the MVP in 1999), made the First-team All-Pro four times, and selected as a member of the NFL 2000s All-Decade Team. In addition to his receiving abilities, Moss additionally accumulated two touchdown passes, one touchdown on a punt return, and an interception in his career.


=== Marshall University Hall of Fame ===
Established in 1984, members from the football team are listed below.
1970 Crash Victims 1990 Honored
Bob Adkins, '39 1984
Mike Barber, '88 1994
Mike Bartrum, '92 2007
Ahmad Bradshaw, '06 2017
Troy Brown, '92 2002
Doug Chapman, '99 2010
George Chaump, 2013
B. J. Cohen, '97 2005
Larry Coyer, '64 1987
Chris Crocker, '02 2013
Melvin Cunningham, '96 2016
Josh Davis, '04 2018
Sean Doctor, '88 2000
Jim Donnan, 2008
Carl Fodor, '85 1991
Frank Gatski, '42 1985
John Grace, '99 2010
Len Hellyer, '56 1988
Cam Henderson, '33–55 1984
Eric Ihnat, '90 2017
Dewey Klein, '91 2018
Carl Lee, '82 1995
Byron Leftwich, '02 2007
Billy Lyon, '96 2007
Albert McClellan, '09 2020
Giradie Mercer, '99 2019
Randy Moss, '97 2010
Reggie Oliver, '73 1984
Tim Openlander, '96 2015
Chris Parker '95 2000
Chad Pennington, '99 2010
Tony Petersen, '88 1994
Bob Pruett, '65 1999
Steve Sciullo, 02 2020
Charlie Slack, '56 1985
Ed Ulinski, '41 1986
John Wade, '97 2010
Darius Watts, '03 2014
Norm Willey, '49 2003
Jamie Wilson, '96 2019
William "Bill" Richard Winter, '64 1990
Max Yates, '01 2019


== Future non-conference opponents ==
Announced schedules as of October 20, 2020.


== References ==


== External links ==
Official website