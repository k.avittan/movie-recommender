The Phantom Ship (1839) is a Gothic novel by Frederick Marryat which explores the legend of the Flying Dutchman.


== Plot introduction ==
The plot concerns the quest of Philip Vanderdecken of Terneuzen in the Netherlands to save his father - who has been doomed to sail for eternity as the Captain of the Bewitched Phantom Ship, after he made a rash oath to heaven and slew one of the crew whilst attempting to sail round the Cape of Good Hope. Vanderdecken learns upon his mother's death that there exists a way by which his father's disturbed spirit may be laid to rest, and vows to live at sea until he has spoken with his father face to face and accomplished this purpose.
Vanderdecken sails around the world in a number of ships, in the employ of the Dutch East India Company, so that he can redeem his father by presenting him with the relic of the Holy Cross he wears round his neck. His quest, however, brings him into conflict with earthly and unearthly powers as the sight of the Flying Dutchman brings doom to all who encounter her.


== Themes ==
The legend of the Flying Dutchman forms the background to the story and makes regular appearances throughout the novel, while Marryat adds many other supernatural details. He introduces as the heroine, Amine, the daughter of one Mynheer Poots, a miser. Having Arab blood in her veins, she possesses some of the secrets of Arabian magic, but her incautious use of her magic arts brings her into the dungeons of the Inquisition at Goa. Likewise, there is Schrifter, the demon pilot; and Krantz, with a tale of horrors in the Harz mountains; atrocious monks; and ghosts that will not be drowned.


== Publication ==
The novel was originally serialised in The New Monthly Magazine beginning in March 1837 and ending in August 1839.
One chapter concerning a werewolf has often been excerpted in anthologies of supernatural fiction as The White Wolf of the Hartz Mountains.


== Reviews ==
The reviews for the novel were generally poor. The Athenaeum thought that the work "falls sadly short of the racy marine stories by which the author won his first fame". In particular, it noted that Marryat "dashes off scenes of portent and terror with the same familiar and slip-shod style ... and the result is a feebleness of effect, not to be found in his other novels." Likewise in referring to the book, The Dublin Review thought that the "falling off in his last novel ... is very considerable", and stated that "a string of extravagant adventures, carelessly put together, and heavily told, deaden curiosity,—the Flying Dutchman makes his appearance as regularly as a packetboat, and becomes at last almost as tiresome."In June 1839 Burton's Gentleman's Magazine published an anonymous review of the novel which it is believed was written by Edgar Allan Poe. The review was largely an attack on Marryat's abilities as a writer. Turning to The Phantom Ship Poe wrote:
The old legend of the Flying Dutchman (a legend, by the bye, possessing all the rich materiel which a vigorous imagination could desire) is worked up with so many of the pitiable niaiseries upon which we have commented, that few persons of disciplined intellect will derive from the medley any other impressions than those of the ridiculous and outré. The story, however, is by no means the worst from the pen of Captain Marryatt, and thus far we most unequivocally recommend it.
In more recent times S.T. Joshi has called the novel "an aesthetic disaster – appallingly prolix, and written in a stiff, cumbersome style that reads like a bad translation from a foreign language." 


== References ==


== External links ==

 The Phantom Ship at Project Gutenberg
The Phantom Ship at the Internet Archive