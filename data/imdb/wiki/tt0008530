A girth, sometimes called a cinch (Western riding), is a piece of equipment used to keep the saddle in place on a horse or other animal. It passes under the barrel of the equine, usually attached to the saddle on both sides by two or three leather straps called billets. Girths are used on Australian and English saddles, while western saddles and many pack saddles have a cinch, which is fastened to the saddle by a single wide leather strap on each side, called a latigo.Although a girth is often enough to keep a well-fitting saddle in place, other pieces of equipment are also used in jumping or speed sports  such as polo, eventing, show jumping, and fox hunting; or on rough terrain such as trail riding. These include breastplates, overgirths, cruppers, and, on pack saddles, breeching.
Studies have shown that, although girths may restrict the movement of the ribcage in the horse, they have no effect on the horse's ability to take in air.


== Types of Girths ==

Several types of girth are shaped to allow ample room for the elbows. The Balding style is a flat piece of leather cut into three strips which are crossed and folded in the center, and the Atherstone style is a shaped piece of baghide with a roughly 1.5” wide strip of stronger leather running along the center. A variation on this is the overlay girth, in which the piece of leather in the center is the same curved shape as the girth. This overlay is often stitched in a decorative design.
Unshaped girths are commonly made of flat, heavy cotton, or padded cotton with nylon webbing reinforcement, or out of leather as in the tri-fold or threefold girth, popular among sidesaddle riders and traditional foxhunters.
Fleece girth covers are often used on sensitive horses to protect the barrel of the horse, and some styles of girth come with attached or removable sheepskin liners that perform the same function.
A dressage girth, or Lonsdale girth, is shorter than the usual girths used on other saddles. This is because the dressage saddle has longer billets, to keep the buckles out from under the rider's leg, and so a shorter girth may be used. Dressage girths can be made of all the materials, and in all the styles, mentioned before, and also can be made entirely of very strong elastic.

An overgirth or surcingle is often used in addition to a regular leather girth. Made of leather or nylon with an elastic insert (for racing), the overgirth completely encircles the horse around belly and the saddle's seat. It is used by stockmen, eventers, polo players, in flat racing, and by steeplechase jockeys to provide more security in holding the saddle in place.
Some girths (those used on jumpers and eventers) have a belly guard (or stud guard), to protect the belly from being stabbed by horseshoe studs as the animal tucks his legs up underneath him over a tall obstacle.


== Western cinches ==
The traditional western cinch was made of multiple strands of heavy cords, usually made of mohair, or, in cheaper designs, cotton. Modern designs are also made of synthetic fiber or a synthetic-mohair blend.  The number of cords used varies with width and design, but the standard range is from 17 to 30 strands, creating an end product that is 4 to 7 inches wide at the widest point in the center of the cinch. This design is sometimes known as a "string", "strand," "cord" or "rope" cinch. Each cord is knotted around a large ring, called a cinch ring, placed at either end.  In the center, additional cording or very heavy thread is used to gather all the cords into a set width and make the cinch lie flat.  Wider cinches are narrowed to fit the cinch ring by allowing two layers of cord to form at the ring, sometimes aided by decorative weaving that stabilizes the cords.  
Cinches are also made of more solid materials.  One of the first non-traditional designs incorporated 1/2" thick felt backed by nylon webbing on the side away from the horse.  Other materials, such as neoprene, also supported internally or on one side by heavy web or nylon or a similar synthetic material, are also used.   Cinches are sometimes covered with a sleeve or covering made of fleece, usually synthetic. Fleece is also sometimes used to line the inside of a cinch.
The cinch attaches to the saddle by means of a latigo on either side.  The latigo is a wide, flexible strap, usually of leather, though nylon webbing is also seen.  The latigo is attached to the off (right) side of the saddle at the saddle's cinch ring or "dee ring", doubled in thickness and knotted or buckled to the cinch, usually kept attached to both cinch and saddle at all times, except to make fitting adjustments.  The latigo on the near (left) side is attached to the saddle at all times, but the loose end is used to secure the saddle for riding by running it through the left cinch ring one or more times, back through the saddle's dee ring, and then finally buckled or knotted when tight.  It is loosened and removed from the cinch to take off the saddle.


== Fitting the Girth ==
A girth should first and foremost spread pressure evenly over the entire area. If it is too narrow, or if it has a narrow reinforcing strip down its center, it may cause discomfort. It is also best if it has some "give" to it, which makes it more comfortable for the horse. Many riders also choose a girth that allows for extra elbow room, so the horse is not restricted as his leg moves backward.
To measure for a girth, the saddle with a pad should be placed on the horse. A measuring tape is then used to measure from the middle hole of the billet on one side, under the horse's belly, to the middle billet on the other side.
If a girth is slightly too small, a girth extender may be used. A girth extender attaches to the billets of the saddle and lengthens them, so that a shorter girth may be used.


== Use of the billets ==
Most jumping saddles have three billets. This not only allows the rider a spare should one break, but can also provide an adjustment option. For horses on which the saddle sits nicely, neither slipping forward or back, the first and third billets should be used. On horses where the saddle slips back, the first and second billets should be used.The second and third should never be used together, as they are attached to a single piece of webbing to the saddle's tree. Since the first billet is attached to a separate piece of webbing, riders can safely combine its use with either of the other two billets.There are other girthing systems available such as the Adjustable Y system or a similar girthing system. These also provide an adjustment option and have a front girth strap which is connected to the saddle tree point, and a rear girth strap giving it a Y shape and stability.


== See also ==
Saddle
Saddle sores


== References ==