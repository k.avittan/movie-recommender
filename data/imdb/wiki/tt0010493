Mutt and Jeff was a long-running and widely popular American newspaper comic strip created by cartoonist Bud Fisher in 1907 about "two mismatched tinhorns". It is commonly regarded as the first daily comic strip. The concept of a newspaper strip featuring recurring characters in multiple panels on a six-day-a-week schedule had previously been pioneered through the short-lived A. Piker Clerk by Clare Briggs, but it was Mutt and Jeff as the first successful daily comic strip that staked out the direction of the future trend.
Mutt and Jeff remained in syndication until 1983, employing the talents of several cartoonists, chiefly Al Smith who drew the strip for nearly fifty years. The series eventually became a comic book, initially published by All-American Publications and later published by DC Comics, Dell Comics and Harvey Comics. Later it was also published as cartoons, films, pop culture merchandise and reprints.


== Syndicated success ==
Harry Conway "Bud" Fisher was a sports cartoonist for the San Francisco Chronicle in the early 1900s, a time when a newspaper cartoon was single panel. His innovation was to tell a cartoon gag in a sequence, or strip, of panels, creating the first American comic strip to successfully pioneer that since-common format. The concept of a newspaper strip featuring recurring characters in multiple panels on a six-day-a-week schedule actually had been created by Clare Briggs with A. Piker Clerk four years earlier, but that short-lived effort did not inspire further comics in a comic-strip format. As comics historian Don Markstein explained,

Fisher's comic strip was very similar to A. Piker Clerk, which cartoonist Clare Briggs ... had done in the very same daily format for The Chicago American in 1903. But tho Fisher was born in Chicago, it's unknown whether or not he ever saw the Briggs strip, so let's give him the benefit of the doubt and say he had an idea. Despite the Briggs primacy, A. Mutt is considered the first daily strip because it's the one that sparked a trend in that direction, which continues to this day.
A. Mutt, the comic strip that became better known by its later title, Mutt and Jeff, debuted on November 15, 1907 on the sports pages of the San Francisco Chronicle. The featured character had previously appeared in sports cartoons by Fisher but was unnamed. Fisher had approached his editor, John P. Young, about doing a regular strip as early as 1905, but was turned down. According to Fisher, Young told him, "It would take up too much room, and readers are used to reading down the page, and not horizontally."This strip focused on a single main character until the other half of the duo appeared on March 27, 1908. It appeared only in the Chronicle, so Fisher did not have the extended lead time that syndicated strips require. Episodes were drawn the day before publication, and frequently referred to local events that were currently making headlines or to specific horse races being run that day. A 1908 sequence about Mutt's trial featured a parade of thinly-disguised caricatures of specific San Francisco political figures, many of whom were being prosecuted for graft.
On June 7, 1908, the strip moved off the sports pages and into Hearst's San Francisco Examiner where it was syndicated by King Features and became a national hit, subsequently making Fisher the first celebrity of the comics industry. Fisher had taken the precaution of copyrighting the strip in his own name, facilitating the move to King Features and making it impossible for the Chronicle to continue the strip using another artist.
A dispute between Fisher and King Features arose in 1913, and Fisher moved his strip on September 15, 1915, to the Wheeler Syndicate (later the Bell Syndicate), who gave Fisher 60% of the gross revenue, an enormous income in those times. Hearst responded by launching a lawsuit which ultimately failed. By 1916, Fisher was earning in excess of $150,000 a year. By the 1920s, merchandising and growing circulation had increased his income to an estimated $250,000.In 1918, Mutt and Jeff added a Sunday strip and, as success continued, Fisher became increasingly dependent on assistants to produce the work. Fisher hired Billy Liverpool and Ed Mack, artists Hearst had at one point groomed to take over the strip, who would do most of the artwork. Other assistants on the strip included Ken Kling, George Herriman, and Maurice Sendak (while still in high school).Fisher appeared to lose all interest in the strip during the 1930s, and after Mack died in 1932, the job of creating the strip fell to Al Smith. In c. 1944, the new Chicago-based Field Syndicate took over the strip. Mutt and Jeff retained Fisher's signature until his death, however, so it wasn't until December 7, 1954, that the strip started being signed by Smith.Al Smith received the National Cartoonists Society Humor Comic Strip Award in 1968 for his work on the strip. Smith continued to draw Mutt and Jeff until 1980, two years before it ceased publication.
In the introduction to Forever Nuts: The Early Years of Mutt & Jeff, Allan Holtz gave the following reason for the strip's longevity and demise:

The strip's waning circulation got a shot in the arm in the 1950s when President Eisenhower sang its praises, and then again in the 1970s when a nostalgia craze swept the nation. It took the 1980s, a decade focused on the here and now, and a final creative change on the strip when even Al Smith had had enough, to finally allow the strip the rest it had deserved for decades.
During this final period it was drawn by George Breisacher. Currently, Andrews McMeel Universal continues to syndicate Mutt and Jeff under the imprint Classic Mutt and Jeff (in both English and Spanish language versions) under the copyright of Pierre S. de Beaumont (1915–2010), founder of the Brookstone catalog and retail chain. De Beaumont inherited ownership of the strip from his mother, Aedita de Beaumont, who married Fisher in 1925 (the couple parted after four weeks, but never divorced).


== Characters and story ==
Augustus Mutt is a tall, dimwitted racetrack character—a fanatic horse-race gambler who is motivated by greed. Mutt has a wife, known only as Mrs. Mutt (Mutt always referred to her as "M'love"; Al Smith admitted in a Boston Globe newspaper column that her first name was Ima -- and conceded that he didn't use it often because it wasn't a complimentary name). The Mutts' son was named Cicero. Mutt first encountered the half-pint Jeff, an inmate of an insane asylum who shares his passion for horseracing, in 1908. They appeared in more and more strips together until the strip abandoned the horse-race theme, and concentrated on Mutt's other outlandish, get-rich-quick schemes. Jeff usually served as a (sometimes unwilling) partner. Jeff was short, bald as a billiard ball, and wore mutton chop sideburns. He has no last name, stating his name is "just Jeff—first and last and always it's Jeff". However, at one point late in the strip's life, he is identified in the address of a cablegram as "Othello Jeff". He has a twin brother named Julius. They look so much alike that Jeff, who cannot afford to have a portrait painted, sits for Julius, who is too busy to pose. Rarely does Jeff change from his habitual outfit of top hat and suit with wing collar. Friends of Mutt and Jeff have included Gus Geevem, Joe Spivis, and the English Sir Sidney. Characteristic lines and catchphrases that appeared often during the run of the strip included "Nix, Mutt, nix!", "For the love of Mike!" and "Oowah!"
The original inspiration for the character of "Jeff" was Jacques "Jakie" Fehr, a tiny (4'8") irascible Swiss-born shopkeeper in the village of Occidental, California. One summer day in 1908, Fisher, a member of San Francisco's Bohemian Club, was riding the North Pacific Coast narrow-gauge railway passenger train northbound to the Bohemian Grove, the club's summer campsite. During a stop in Occidental, Fisher got off the train to stretch his legs and observed the diminutive walrus-moustached Fehr in heated altercation with the tall and lanky "candy butcher", who sold refreshments on the train and also distributed newspapers to shops in towns along the train route. The comic potential in this scene prompted Fisher to add the character of Jeff to his A. Mutt comic strip, with great success.


== Cicero's Cat ==

Starting October 27, 1926, the Sunday page included a topper strip about Cicero, Mutt's son. On December 3, 1933, the topper began to focus on Cicero's pet, Desdemona. Under the title Cicero's Cat, this pantomime strip ran until 1972.


== Comic books and reprints ==
The Cupples & Leon Company produced at least 18 reprint collections of Mutt and Jeff daily strips, in 10" x 10" softcover books from 1919 to 1933. They also published two larger hardcover editions, Mutt and Jeff BIG Book (1926) and Mutt and Jeff BIG Book No. 2 (1929).
Mutt and Jeff also appeared in comic books. They were featured on the front cover of Famous Funnies #1, the first modern format comic book, and reprints appeared in DC Comics' All American Comics. It has been suggested that some of the Mutt and Jeff material published by DC Comics were new stories drawn by Sheldon Mayer
In 1939, DC gave them their own comic book, published until 1958 for 103 issues, that consisted entirely of newspaper reprints. Dell Comics took over the feature after DC dropped it, but their tenure only lasted for one year and 12 issues. Many of the Dell issues featured new, conventional-length stories drawn by Smith.
Harvey Comics, which had several other comic strip reprint comics running at the time, picked up Mutt and Jeff from Dell, and this version of the comic ran to 1965 for a total of 33 issues, plus two short-lived spinoff titles: Mutt & Jeff Jokes and Mutt & Jeff New Jokes. These later versions also included Smith's Cicero's Cat.
In 2007, comics publisher NBM published a reprint volume, Forever Nuts: The Early Years of Mutt & Jeff. ISBN 1-56163-502-2


== Stage shows and sheet music ==

Mutt and Jeff: A Musical Comedy Song Book (1912) Songs include: "The Barn-Yard Rag"; "Sail on Silv'ry Moon"; "Mr. Ragtime Whippoorwill"; "Oh You Girl!"; "A Mother Old and Gray"; "Let Me Call You Sweetheart"; "Years Years Ago"; "If I Forget"; "Bohemia Rag"; "Undertaker Man"; "Tell Me That You Love Me"
The Face in the Flag I Love (from Mutt and Jeff in Panama, 1913)
At the Funny Page Ball (1918)
Mutt and Jeff on Their Honeymoon (aka Mutt and Jeff Divorced, 1920) Songs include: "My Dearie"; "My Dixie Rose"; "The Wild Irish Rose That God Gave Me"; "Why Can't My Dreams Come True"; "Just One Little Smile"; "Songs My Mother Sang to Me"; "When Someone Dreams of Someone"; "When I Am Dreaming of You"
Mutt and Jeff: And They Called It the Funny Sheet Blues (1923)
Mutt and Jeff Songster (Date unknown)


=== Program from Mutt and Jeff Divorced (1920) ===

		
		


== Motion pictures ==


=== Live-action ===

In early July 1911, during the silent era of motion pictures, at David Horsley's Nestor Comedies in Bayonne, New Jersey, Al Christie began turning out a weekly one-reel live-action Mutt and Jeff comedy short, which was based on the comic strip.
The Mutt and Jeff serial was extremely popular and after the Nestor Company established a studio in Hollywood, in late October 1911, Christie continued to oversee a weekly production of a one-reel episode.
In the fall of 1911, Nestor began using an alternate method of displaying the intertitles in the Mutt and Jeff comedies. Instead of a cut to the dialogue titles, the dialogue was displayed at the bottom of the image on a black background so the audience could read them as a subtitle, which was similar to the way they appeared in the cartoon strips. Horsley was very proud of the device and claimed to have entered a patent on it. He advertised the Mutt and Jeff movies as "talking pictures".The first actors to portray Mutt and Jeff in the comedy shorts were Sam D. Drane, a tall man noted for his resemblance to President Lincoln, who actually played Lincoln in his last movie, The Crisis (1916), as A. Mutt, and Gus Alexander, whose nickname was "Shorty", as Jeff. When Alexander was leaving the serial, Christie hired the small actor Bud Duncan. Duncan played Jeff in two installments before the serial ended in 1912.


=== Animation ===

In 1916, Fisher licensed the production of Mutt and Jeff for animation with pioneers Charles Bowers and Raoul Barré of the Barré Studio. The animated series lasted 11 years and more than 300 animated Mutt and Jeff shorts were released by the Fox Film Corporation, making it the longest continuing theatrical animated movie serial and second longest to Krazy Kat.

In 1973, a feature film was released consisting of eleven redrawn colorized Mutt and Jeff silent films, with the short Slick Sleuths used as the frame, titled Mutt And Jeff Meet Bugoff, which added new dialogue and soundtrack songs. Radio & Television Film Packagers were the producers of the film, which received a very limited theatrical release, primarily being shown on the 16MM circuit.
In 2005, Inkwell Images released a DVD documentary entitled Mutt and Jeff: the Original Animated Odd Couple; included on the disc are several Mutt and Jeff animated cartoons. Also, individual Mutt and Jeff cartoons have been mixed with other titles on low-cost video collections, such as the Cartoon Craze DVDs from Digiview Productions.


== In popular culture ==
Any pair of individuals of different sizes has come to be known as a "Mutt and Jeff".
Mutt and Jeff, Texas, was a small community located at the intersection of State Highway 37 and Farm Road 14, near Big Sandy Creek, six miles from Winnsboro. The town was so named in the 1920s because of two area merchants, who reminded locals of the comic strip characters. The population decreased during the 1930s, and Mutt and Jeff, Texas, was abandoned by the early 1960s.
The "good cop/bad cop" police interrogation tactic is also called "Mutt and Jeff".
In rhyming slang, "mutton" is used as a shortening of "Mutt'n'Jeff", meaning "deaf".
In Frank Henenlotter's Frankenhooker, when Jeffrey Franken is going through various body parts, he refers to two mismatched breasts as 'Mutt and Jeff'.
In the TV series Twin Peaks' pilot, Bookhouse Boy biker Joey Paulsen says to the fellow biker sitting next to him at The Roadhouse "Scotty, Mutt and Jeff just crawled in" when Bobby Briggs and Mike Nelson enter the bar.
In Disney's Aladdin stage musical, Mutt and Jeff are referred to by name as examples of great friends.
In a Gasoline Alley sequence begun on October 19, 2015, Mutt telephones Walt Wallet with news regarding Jeff.
Paul Brickhill (in The Dam Busters) referred to two test pilots as Mutt and Jeff.
In the 2002 Simpsons episode "Helter Shelter", in which the Simpsons participate in a reality television game show in which they live in Victorian house and have access to items available only in 1895, Bart laments having access only to Mutt and Jeff comic books and is quoted as saying, "This has been the worst week of my life. I miss my toys and my video games. Mutt and Jeff comics are NOT funny! They're gay, I get it!". Mutt and Jeff was not created until 1907.
Mutt and Jeff, better known in Spanish as Eneas and Benitín, are mentioned in the song "Día de Enero" (English: "January Day") by Colombian singer Shakira in her sixth studio album Fijación Oral 1.
In the 1973 film "The Sting", Robert Redford's character Johnny Hooker refers to the mark's bodyguards as "Mutt and Jeff".
In the musical "Annie", Mutt & Jeff are referred to in the song "We Got Annie".


== References ==


== External links ==
A.Word.A.Day
The Classic Mutt and Jeff strips GoComics
A suicide-themed Mutt and Jeff strip from November 1911
The short film Slick Sleuths is available for free download at the Internet Archive (An animated short film)
Mutt and Jeff at Don Markstein's Toonopedia. Archived from the original on April 4, 2012.