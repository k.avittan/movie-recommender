Crataegus monogyna, known as common hawthorn, oneseed hawthorn, or single-seeded hawthorn, is a species of flowering plant in the rose family Rosaceae. It is native to Europe, northwest Africa and West Asia but has been introduced in many other parts of the world.


== Names ==
Other common names include may, mayblossom, maythorn, quickthorn, whitethorn, motherdie, and haw. 
This species is one of several that have been referred to as Crataegus oxyacantha, a name that has been rejected by the botanical community as too ambiguous. In 1793, Medikus published the name C. apiifolia for a European hawthorn now included in C. monogyna, but that name is illegitimate under the rules of botanical nomenclature.


== Description ==
The common hawthorn is a shrub or small tree 5–14 metres (15 to 45 feet) tall, with a dense crown. The bark is dull brown with vertical orange cracks. The younger stems bear sharp thorns, approximately 12.5 mm (half an inch) long. The leaves are 20 to 40 mm (1 to 1½ inches) long, obovate and deeply lobed, sometimes almost to the midrib, with the lobes spreading at a wide angle. The upper surface is dark green above and paler underneath.
The hermaphrodite flowers are produced in late spring (May to early June in its native area) in corymbs of 5–25 together; each flower is about 10 mm diameter, and has five white petals, numerous red stamens, and a single style; they are moderately fragrant. The flowers are pollinated by midges, bees and other insects and later in the year bear numerous haws. The haw is a small, oval dark red fruit about 10 mm long, berry-like, but structurally a pome containing a single seed. Haws are important for wildlife in winter, particularly thrushes and waxwings; these birds eat the haws and disperse the seeds in their droppings.
The common hawthorn is distinguished from the related but less widespread Midland hawthorn (C. laevigata) by its more upright growth, the leaves being deeply lobed, with spreading lobes, and in the flowers having just one style, not two or three. However they are inter-fertile and hybrids occur frequently; they are only entirely distinct in their more typical forms.

		
		
		
		
		
		
		
		
		


== Uses ==


=== Medicinal use ===

Crataegus monogyna is one of the most common species used as the "hawthorn" of traditional herbalism. The plant parts used are usually sprigs with both leaves and flowers, or alternatively the fruit ("berries"). Hawthorn has been investigated by evidence-based medicine for treating cardiac insufficiency.Crataegus monogyna is a source of antioxidant phytochemicals, especially extracts of hawthorn leaves with flowers.


=== In gardening and agriculture ===
Common hawthorn is extensively planted as a hedge plant, especially for agricultural use. Its spines and close branching habit render it effectively stock- and human-proof, with some basic maintenance. The traditional practice of hedge laying is most commonly practised with this species. It is a good fire wood which burns with a good heat and little smoke.Numerous hybrids exist, some of which are used as garden shrubs. The most widely used hybrid is C. × media (C. monogyna × C. laevigata), of which several cultivars are known, including the very popular 'Paul's Scarlet' with dark pink double flowers. Other garden shrubs that have sometimes been suggested as possible hybrids involving the common hawthorn, include the various-leaved hawthorn of the Caucasus, which is only very occasionally found in parks and gardens.


=== Edible "berries", petals, and leaves ===
The fruit of hawthorn, called haws, are edible raw but are commonly made into jellies, jams, and syrups, used to make wine, or to add flavour to brandy.  Botanically they are pomes, but they look similar to berries. A haw is small and oblong, similar in size and shape to a small olive or grape, and red when ripe. Haws develop in groups of two or three along smaller branches. They are pulpy and delicate in taste. In this species (C. monogyna) they have only one seed, but in other species of hawthorn there may be up to five seeds.
Petals are also edible, as are the leaves, which if picked in spring when still young are tender enough to be used in salads. Hawthorn petals are used in the medieval English recipe for spinee, an almond-milk based pottage recorded in 'The Forme of Cury' by the Chief Master-Cook of King Richard II, c. 1390.


== Notable trees ==
An ancient specimen, and reputedly the oldest tree of any species in France, is to be found alongside the church at Saint Mars sur la Futaie, Mayenne. The tree has a height of 9 m (30 feet), and a girth of 265 cm (8'8") (2009). The inscription on the plaque beneath reads: "This hawthorn is probably the oldest tree in France. Its origin goes back to St Julien (3rd century)", but such claims are impossible to verify.
A famous specimen in England was the Glastonbury or Holy Thorn which, according to legend, sprouted from the staff of Joseph of Arimathea after he thrust it into the ground while visiting Glastonbury in the 1st century AD. The tree was noteworthy because it flowered twice in a year, once in the late spring which is normal, but also once after the harshness of midwinter had passed. The original tree at Glastonbury Abbey, felled in the 1640s during the English Civil War, has been propagated as the cultivar 'Biflora'. A replacement was planted by the local council in 1951, but was cut down by vandals in 2010.The oldest known living specimen in East Anglia, and possibly in the United Kingdom, is known as The Hethel Old Thorn, and is located in the churchyard in the small village of Hethel, south of Norwich, in Norfolk. It is reputed to be more than 700 years old, having been planted in the 13th century.


== In culture ==
The hawthorn is associated with Faerie in Ireland, and as such is not disturbed by those who believe in the danger fairies traditionally represent.


== See also ==
The hawthorn button-top gall on Hawthorn, is caused by the dipteron gall-midge Dasineura crataegi.
Haweater
List of Lepidoptera that feed on hawthorns
Folklore about hawthorns, primarily the European species C. laevigata and/or C. monogyna and hybrids between these two species.


== Notes ==


== References ==
Philips, R. (1979). Trees of North America and Europe, Random House, Inc., New York. ISBN 0-394-50259-0.
Kheloufi, A., Mansouri, L. M., & Vanbellinghen, C. (2019).  Seed germination of Crataegus monogyna–a species with a stony endocarp. Reforesta, (7), 73-80.


== Further reading ==
Bahorun, Theeshan, et al. (2003). "Phenolic constituents and antioxidant capacities of Crataegus monogyna (Hawthorn) callus extracts". Food/Nahrung 47.3 (2003): 191–198.


== External links ==
Crataegus monogyna in Topwalks
Hawthorn Gallery (photographs of a number of such trees, including Hethel Old Thorn)
"Hawthorn" . Encyclopedia Americana. 1920.
Crataegus monogyna at Flora Iberica