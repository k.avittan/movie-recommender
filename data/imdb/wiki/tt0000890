Outlaw King, stylized as Outlaw/King, is a 2018 historical action drama film about Robert the Bruce, the 14th-century Scottish King who launched a guerilla war against the larger English army. The film largely takes place during the 3-year historical period from 1304, when Bruce decides to rebel against the rule of Edward I over Scotland, thus becoming an "outlaw", up to the 1307 Battle of Loudoun Hill. Outlaw King was co-written, produced, and directed by David Mackenzie. It stars Chris Pine, Aaron Taylor-Johnson, Florence Pugh, Billy Howle, Sam Spruell, Tony Curran, Callan Mulvey, James Cosmo, and Stephen Dillane.
It premiered at the Toronto International Film Festival on September 6, 2018, and was released on November 9, 2018, by Netflix.


== Plot ==
In the year 1304, outside the besieged Stirling Castle, John Comyn, Robert Bruce, and other Scottish nobility surrender to Edward I of England. King Edward promises to return lands to the nobility in exchange for their homage. After the formalities have ended, Bruce spars with Edward's heir, the Prince of Wales, and the King weds Bruce to his goddaughter, Elizabeth de Burgh. Lord James Douglas arrives to ask for the restoration of his ancestral lands but is dismissed by Edward, on the basis of the previous Lord Douglas' treason. The King and the Prince depart from Scotland, leaving its management to Comyn and Bruce, under the supervision of the Earl of Pembroke, Aymer de Valence. Elizabeth arrives to marry Bruce. On the wedding night, he respects his reluctant wife's wishes and delays the consummation. Bruce witnesses Englishmen conscripting his servants to the King's army. Not long after, his father, the Lord of Annandale, passes away, lamenting the loss of his friendship with the King of England, while admitting it may have been a mistake to trust Edward.
Soon after, while delivering taxes to the English in Berwick, Bruce notes how unpopular the English are. There is rioting after the public display of the quartered body of William Wallace, which spurs Bruce to plan another revolt. He discusses the issue with his family, who agree with him. Bruce tries to persuade John Comyn to join him, but the latter refuses and threatens to inform Edward about the plan. In a panic, Bruce stabs Comyn to death. The incident compels the clergy of Scotland to offer a pardon to Bruce, bargaining that he supports the Catholic Church in Scotland and he accepts the Crown of Scotland. Bruce accepts the deal, but King Edward soon hears of it. He declares Bruce an outlaw and, at his son's request, sends the Prince of Wales to crush the uprising, under the dragon banner, symbolizing the abandonment of chivalry and representing King Edward's order that no quarter to be shown to any supporter of Bruce.
Bruce calls a council of the nobles, where most refuse to break their oaths to Edward. Despite the lack of expected support, Bruce heads to Scone, where he is to be crowned. On the road, he encounters Douglas, who pledges his allegiance if Robert will help Douglas reclaim his birthright. In Scone, Bruce is crowned king of Scots. The ambitious de Valence decides to move against Bruce before the Prince arrives. Bruce wishes to avoid bloodshed and challenges de Valence to single combat. De Valence accepts but insists on delaying the duel a day, as it is Sunday. During the night, at Methven, Bruce finally consummates his marriage, but the English launch a surprise attack. Bruce sends his wife and Marjorie Bruce, to safety with his brother Nigel, and stays to fight a losing battle, during which most of the Scottish army is killed. Bruce escapes with fifty men. The group takes Angus' advice and flees to Islay. While traveling there, John MacDougall parleys with them. He is bitter about the murder of his cousin Comyn but allows the army to pass but later attacks Bruce's entourage as they're trying to cross Loch Ryan. Some of them get away in boats, but they cannot prevent the death of Robert the Bruce's brother Alexander.
Prince Edward arrives in Scotland and searches for Bruce at Kildrummy Castle, only to find Bruce's wife, daughter, and brother. The sadistic prince has his brother hanged and drawn, and has Bruce's daughter and wife taken to England. Bruce's company meets up with Lord Mackinnon, who refuses to lend them any men. The band presses on with their voyage to Islay anyway; there, they learn of the fall of Kildrummy Castle. Bruce decides to take back the castle through stealth. The successful operation inspires Bruce to begin guerilla warfare. Shortly thereafter, Bruce is reunited with his other brother, Thomas. In England, Marjorie is separated from her stepmother Elizabeth to be given religious instruction by nuns. After Edward hears that Douglas Castle has been re-taken, he goes after Bruce himself. He offers Elizabeth a pardon if she will annul her marriage to Robert, but she refuses and is placed in a hanging cage.
The following year, King Edward dies shortly after arriving in Scotland, and the Prince of Wales takes control of his father's forces. In defiance of his father's dying wishes, he orders his late father's burial in Scotland. Bruce decides to fight the new king in a pitched battle at Loudoun Hill, despite being outnumbered six to one. Clan Mackinnon arrives to aid Bruce. Edward's army is composed almost entirely of cavalry, so Robert overcomes this army's size disadvantage by ensuring that during the battle,  the English cavalry charge into a spear wall hidden by a ditch, and thus incur heavy losses. Many horsemen attempt to attack the flanks but become bogged down in the mud, just as the Scots anticipated. As the English knights fall from their horses, many are slain, and the battle becomes an open brawl, wherein the ferocious Scots prevail over the disoriented English soldiers. Realising the battle is hopeless, de Valence orders a retreat. However, determined to kill his nemesis, Edward does not join the retreat. Instead, he engages in a duel with Bruce as the Scots look on. Although Bruce prevails, he allows Edward to leave unharmed. The epilogue reveals that Elizabeth was released as a part of a hostage exchange. The Prince of Wales was crowned King Edward II, only to be killed by his own lords. Three hundred years later, Robert's descendant unified the crowns of England and Scotland, and Sir James Douglas’ descendant, Marion Hamilton, would marry Kentigern Hunter, who died at the battle of Pinkie Cleugh, the last pitched battle between England and Scotland before the union of the crowns, in 1547.


== Cast ==
Chris Pine as Robert the Bruce
Aaron Taylor-Johnson as James Douglas, Lord of Douglas
Florence Pugh as Elizabeth de Burgh
Billy Howle as Edward, Prince of Wales
Tony Curran as Angus MacDonald
Lorne MacFadyen as Neil Bruce
Alastair Mackenzie as Lord Atholl
James Cosmo as Robert de Brus, 6th Lord of Annandale
Callan Mulvey as John Comyn III of Badenoch
Stephen McMillan as Drew Forfar, Squire
Paul Blair as Bishop Lamberton
Stephen Dillane as King Edward I of England
Steven Cree as Christopher Seton
Kim Allan as Isabella MacDuff, Countess of Buchan
Sam Spruell as Aymer de Valence, 2nd Earl of Pembroke
Rebecca Robin as Margaret of France, Queen of England
Jack Greenlees as Alexander Bruce
Jamie Maclachlan as Roger De Mowbray
Benny Young as Sir Simon Fraser
Clive Russell as Lord MacKinnon of Skye
Josie O'Brien as Marjorie Bruce
Matt Stokoe as John Segrave, 2nd Baron Segrave


== Production ==
Principal photography began on 28 August 2017 on location in both Scotland and England. Filming took place in various locations including Linlithgow Palace & Loch, and St Michael's Parish Church, Borthwick Castle, Doune Castle, Craigmillar Castle, Dunfermline Abbey, Glasgow Cathedral, Muiravonside Country Park, Mugdock Country Park, Aviemore, Isle of Skye (Talisker Bay, Coral Beaches and Loch Dunvegan), Glen Coe, Loch Lomond, Gargunnock, University of Glasgow, Blackness Castle, Seacliff Beach and Berwick-upon-Tweed and Tweedmouth (the latter two both in Northumberland - Berwick-upon-Tweed's bridge doubling for London Bridge). Principal production concluded in November 2017.


== Release ==
The film had its world premiere at the Toronto International Film Festival on September 6, 2018. The premiere's runtime of 137 minutes and its pacing were criticised in early reviews, and Mackenzie subsequently cut nearly 20 minutes from the film. Cut material includes a battle scene, a major confrontation backdropped by a waterfall, an eight-minute chase sequence, and a scene in which Pine’s character meets William Wallace in the woods. The film had its European premiere at the London Film Festival in October 2018 and was commercially released on November 9, 2018.


== Reception ==
On review aggregator Rotten Tomatoes, the film holds an approval of 61% based on 148 reviews, and an average rating of 6.16/10. The website's critical consensus reads, "Muddy and bloody to a fault, Outlaw King doesn't skimp on the medieval battle scenes, but tends to lose track of the fact-based legend at the heart of its story." On Metacritic, the film has a weighted average score of 59 out of 100, based on 38 critics, indicating "mixed or average reviews".


=== Accolades ===


== Historical authenticity ==

The film implies that Robert I ("Robert the Bruce") began his rebellion almost immediately after the execution of William Wallace, implying that he intended to avenge Wallace. In truth, he began his rebellion a full year after  Wallace's death. During the intermediate period, Edward I became suspicious of Robert I and ordered him to stay at Kildrummy Castle.The film shows Robert I marrying Elizabeth de Burgh after surrendering to Edward I. In reality, Bruce's second marriage occurred years before in 1302.
The film's depiction of Edward II's role in the Battle of Loudoun Hill is heavily flawed. It is unlikely that he was present at the battle in any capacity. Moreover, it is certain that he would not have challenged Bruce to single combat. Even if he had been present and challenged Bruce to personal combat, a hostage as valuable as Edward II would not have been allowed to flee.The title character in Outlaw King is that of an enigmatic and well-behaved man of the people who desires to restore Scotland to its inhabitants. However, historian Fiona Watson notes the real Robert I was most likely cold, canny, and driven by his personal ambition.The color yellow is mostly absent from the clothing of the fighting men. In contrast, yellow dye was not only the most common dye in Scotland during the period, it was highly favoured by the fighters with the means to afford it. Historian Fergus Cannan notes that while many historical writers comment on its prevalence, it remains absent from appearances in popular culture related to Scottish history.The film depicts the character of Edward II as a cruel and oppressive person who is eager to succeed his father despite any historical evidence of Edward II having displayed such traits. On the contrary, Edward II was reluctant to assume the mantle of kingship and was known to be generous with his servants.The film portrays Edward I dying before the battle of Loudoun Hill when, in actuality, he died several months later. Furthermore, the film implies that Edward I was buried where he died when, in fact, he was interred at Westminster Abbey in London.
In the film, when Robert I tells his brothers about his plan to start a rebellion, artichokes can be seen on the table. However, artichokes were not introduced to the British Isles until the 16th century.


== See also ==
Robert the Bruce (2019) stars Angus Macfadyen as Robert the Bruce, but depicts different events.


== References ==


== External links ==
Outlaw King on Netflix
Outlaw King on IMDb
Outlaw King at AllMovie
Outlaw King at Rotten Tomatoes
Outlaw King at Metacritic