Spare a Copper is a 1940 British black-and-white musical comedy war film directed by John Paddy Carstairs and starring George Formby, Dorothy Hyson and Bernard Lee. It was produced by Associated Talking Pictures. It is also known as Call a Cop. The film features the songs, "I'm The Ukulele Man", "On The Beat", "I Wish I Was Back On The Farm" and "I'm Shy". Beryl Reid makes her film debut in an uncredited role, while Ronald Shiner appears similarly uncredited, in the role of the Piano Mover and Tuner.Working on the film as associate producer and writer, this production was an early assignment for director Basil Dearden: "it was relatively easy to fit the Formby films into the new demands thrown up by the war: whereas George had typically had to overcome rogues and villains in his 1930s films, these were now simply replaced by spies and saboteurs".The film title is a pun, using the colloquial term "copper" meaning a policeman, with the longer phrase "spare a copper" used by beggars - meaning can you spare a penny (which I might have).


== Synopsis ==
Formby plays a bumbling War Reservist police officer called George Carter who aspires to become a member of the flying squad. The film is set in Merseyside where the battleship HMS Hercules is being built.  A group of saboteurs are planning to blow it up. George manages to foil them. One of the saboteurs, called "Jake", is played by Bernard Lee.
The saboteurs include fellow police officers who plan to shoot Formby in a remote area but he escapes in a motorised toy car. A crazy chase ensues ending in Formby going round and round a wall of death before foiling the plot.


== Cast ==
George Formby as George
Dorothy Hyson as Jane
Bernard Lee as Jake
John Warwick as Shaw
Warburton Gamble as Sir Robert Dyer
John Turnbull as Inspector Richards
George Merritt as Brewster
Eliot Makeham as Fuller
Ellen Pollock as Lady Hardstaff
Edward Lexy as Night watchman
Jack Melford as Dame
Hal Gordon as Sergeant
Jimmy Godden as Manager
Grace Arnold as Music shop customer
Charles Carson as Admiral


== Critical reception ==
The Times critic wrote in 1940: "the structure of Mr. George Formby's films do not alter very much, and the same blue-print that has done serviceable work in the past was taken out of its drawer for Spare a Copper".
In a 1940 issue, Monthly Film Bulletin called it "a good Formby film...With a better story than most".
TV Guide dismissed the film as a "mediocre WW II comedy".
Halliwell's Film Guide comments, "one of the last good Formby comedies, with everything percolating as it should".
George Perry wrote in "Forever Ealing", "the notion of unsuspected German spies in respectable positions was to recur in more serious Ealing films such as The Foreman Went to France and Went the Day Well? These comedy films were judged as very good for public morale at the time while delivering an important message."


== References ==


== External links ==
Spare a Copper at AllMovie
Spare a Copper at the British Film Institute's Film and TV Database
Spare a Copper on IMDb