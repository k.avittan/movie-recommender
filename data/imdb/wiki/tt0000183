The Corsican Brothers (French: Les Frères corses) is a novella by Alexandre Dumas, père, first published in 1844. It is the story of two conjoined brothers who, though separated at birth, can still feel each other's physical distress. It has been adapted many times on the stage and in film.


== Plot ==
The story starts in March 1841, when the narrator travels to Corsica and stays at the home of the widow Savilia de Franchi who lives near Olmeto and Suddacarò. She is the mother of former conjoined twins Louis and Lucien. Louis is a lawyer in Paris, while Lucien clings to his Corsican roots and stays at his mother's home.
The brothers were separated at birth by a doctor with his scalpel. Despite being separated, Louis and Lucien can still feel each other's emotions, even at a distance. Lucien explains he has a mission to undertake, with reluctance. He has to mediate in a vendetta between the Orlandi and Colona families and invites the narrator to accompany him and meet the head of the Orlandi family.


== Adaptations ==


=== Theatre ===
The play The Corsican Brothers, by Dion Boucicault, based on the story, was first seen in 1852.


=== Film ===
The Corsican Brothers (1898), directed by film pioneer and inventor George Albert Smith
The Corsican Brothers (1902), directed by Dicky Winslow
The Corsican Brothers (1912), featuring George Lessey
The Corsican Brothers (1915), starring King Baggot in the dual title roles, directed by George Lessey
The Corsican Brothers Up To Date (1915), directed by Charles Hutchison
The Corsican Brothers (1917), directed by André Antoine
The Corsican Brothers (1920), directed by Louis J. Gasnier
The Corsican Brothers (1941), starring Douglas Fairbanks, Jr.
Apoorva Sagodharargal (1949), a Tamil film starring M. K. Radha and P. Bhanumathi, directed by Acharya
The Bandits of Corsica (1953), starring Richard Greene in the dual title roles, directed by Ray Nazarro
The Corsican Brothers, (1955), an Argentine film, directed by Leo Fleider
The Corsican Brothers (1961 film), a French-Italian production starring Geoffrey Horne
Aggi Pidugu (1964), a Telugu film directed by B. Vittalacharya, starring N. T. Rama Rao, Krishna Kumari and Rajasri
Start the Revolution Without Me (1970), a parody starring Gene Wilder and Donald Sutherland as two sets of identical twins
Neerum Neruppum (1971), a Tamil film starring M. G. Ramachandran and Jayalalithaa
Gora Aur Kala (1972), a Hindi film starring Rajendra Kumar, Hema Malini, Rekha
Cheech and Chong's The Corsican Brothers (1984), a parody
The Corsican Brothers (1985), a Hallmark Hall of Fame movie featuring Trevor Eve


== In popular culture ==
G.I. Joe: A Real American Hero characters Tomax and Xamot are twin brothers described as having "Corsican Syndrome" in that they share a psychic link and can feel each other's pain.
In the U.S. television science fiction series Warehouse 13, "The Corsican Brothers' Vest" is an artifact which causes who ever hurts the wearer to feel the pain (and get wounded) instead of him. In episode 24 of season 2 H.G.Wells' character wears the vest and, when being shot, the shooter gets the bullet instead.
In Dear Bill, the parody of British political life during the Thatcher era, Denis Thatcher routinely refers to the government's PR consultants Saatchi and Saatchi as "The Corsican Brothers".
In 1970 US television, a comedic parody aired on the television sitcom Bewitched. In the episode titled "The Corsican Cousins", Samantha's mother, Endora, places a spell on Samantha so she will feel and act out everything her cousin, Serena does.
In the 1981 novel The Xanadu Talisman by Peter O'Donnell, Georges and Bernard Martel are compared to the Corsican Brothers because "one was good and one was bad".
Volume 14 Chapter 01 of the manga series Black Jack involves the title character treating twin brothers who are able to feel each other's pain, which their guardian compares to the Corsican Brothers.


== External links ==
The Corsican Brothers by Alexandre Dumas (English) Henry Frith's translation on Project Gutenberg
 The Corsican Brothers public domain audiobook at LibriVox
The Corsican Brothers (1898) at the Internet Movie Database
The Corsican Brothers (1902) at the Internet Movie Database
The Corsican Brothers (1912) at the Internet Movie Database
The Corsican Brothers (1915) at the Internet Movie Database
The Corsican Brothers Up To Date (1915) at the Internet Movie Database
The Corsican Brothers (1917) at the Internet Movie Database
The Corsican Brothers (1920) at the Internet Movie Database
The Bandits of Corsica (1953) at the Internet Movie Database
Zoot Radio, free old time radio show downloads of The Corsican Brothers


== References ==