Rex, also known as Rex the Wonder Horse and King of the Wild Horses (born 1916 or 1917) was a 16 hands (64 inches, 163 cm) Morgan stallion who starred in films and film serials in the 1920s and 1930s.
His trainer was Jack "Swede" Lindell, who found him in a boys' school in Golden, Colorado.  He found that Rex had the unusual behaviour of trying to bite a whip when it was cracked.  Lindell encouraged this and would often stand behind the camera to get a dramatic shot on film. Lindell never left Rex alone on set unless the horse was locked in his own trailer.In one scene from the 1927 silent film No Man's Law, Rex protects the modesty of a young woman (Barbara Kent) swimming in the nude from a pair of rowdy villains. Chasing one around in circles, rearing up and bucking like a wild mustang, until he finally runs him off of a cliff, he sneaks up behind the other and nudges him with his nose over the ledge and into the watering hole. He then prods the now-clothed young woman back to her father.
Sound films were obviously not a problem for Rex, who continued to star in features and serials. By now Rex was a full-fledged "movie horse" accustomed to cameras and crew members, but he could still revert to his wild ways on occasion. 
During filming of The Law of the Wild Rex made a commotion on set.  When he charged the camera (with Lindell behind it) as intended he did not stop when Lindell gave the signal to do so (by holding his whip in both hands).  He reared, knocking over several reflectors and causing the cast and crew to scatter for cover. Rex chased one actor, Ernie Adams, who attempted to hide under a car. Rex dropped to his knees and attempted to bite Adams with his head thrust sideways underneath the car.  Lindell managed to call Rex off by simply cracking the whip, after which the horse calmly walked over to him.  When William Witney, who was working as an assistant director on the serial, made his 1956 film Stranger at my Door, he described the event to trainer Glenn Randall and the scene was recreated for that film.The 1936 serial Robinson Crusoe of Clipper Island features Rex, but he appears in only some of the chapters; two look-alike horses double for him. The real Rex has a small white mark on his forehead, just under his mane.


== Filmography ==

The King of the Wild Horses (1924)
Black Cyclone (1925)
The Devil Horse (1926)
Wild Beauty (1927)
No Man's Law (1927)
Wild Beauty (1928)
Guardians of the Wild (1928)
Two Outlaws (1928)
Wild Blood (1929)
Border Romance (1929)
Plunging Hoofs (1929)
The Harvest of Hate (1929)
Hoofbeats of Vengeance 1929
The Vanishing Legion (1931)
Wild Horse Stampede (1933)
King of the Wild Horses (1933)
The Law of the Wild (1934)
The Adventures of Rex and Rinty (1935)
Stormy (1935)
Robinson Crusoe of Clipper Island (1936) Rex is billed, but he is one of three look-alike horses used in the serial
King of the Sierras (1938) Rex's last film


== See also ==
Wonder Horses


== References ==


== External links ==
Rex on IMDb
Rex, King of the Wild Horses at B-Westerns
Rex the Wonder Horse at Allmovie
Rex the Wonder Horse at The New York Times
"Morgans & Movie Stars" (PDF). (134 KiB) by Susan Graf
Excerpt from No Man's Law featuring Rex and Barbara Kent swimming, YouTube.com