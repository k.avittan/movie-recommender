What Happened to Mary (sometimes erroneously referred to as What Happened to Mary?) is the first serial film made in the United States. Produced by Edison Studios, with screenplays by Horace G. Plympton, and directed by Charles Brabin, the action films starred Mary Fuller. 
Twelve one-reel episodes were released monthly beginning July 1912, coinciding with the literary serial of the same name published in McClure's The Ladies' World magazine. In addition to the motion pictures and regular magazine installments, What Happened to Mary was also adapted as a stage play, followed by a novelization, making it an early precursor of the multimedia franchise.


== Cast ==
Mary Fuller as Mary
Marc McDermott as Lieutenant Strakey
Charles Ogle as Richard Craig, Mary's uncle
Herbert Yost as Henry, Craig's son
Miriam Nesbitt as Madame Jolatsy, a spy
Bliss Milford as Daisy
Bigelow Cooper as Mr. Foster, the Lawyer
William Wadsworth as Billy Peart
Harold M. Shaw as Rev. Cooper
Harry Eytinge as Secretary, Occidental Trust Company
Walter Edwin as Manager of the Society Queen
Yale Benner as John Chase
James Smith as Stage Manager
Carey Lee as Leading Lady
Arthur Housman as Principal Comedian


== Production ==
This serial came to be after the editor of The Ladies' World, Charles Dwyer, met Horace G. Plympton, manager of Thomas Edison's New York motion picture studio on Decatur Avenue and Oliver Place in the Bronx. He was interested in the concept of the story and the plan for an installment published in each issue. A few days after the meeting he suggested making a film version of each installment: the parallel release of magazine and movie episodes supporting each other.The first chapter of the story was printed with a competition, with The Ladies' World cover advertising "One Hundred Dollars For You IF You Can Tell What Happened to Mary". The closest correct guess at the events of the next twenty minutes of the story, in 300 words or less, would win $100.  This prize was awarded to Lucy Proctor of Armstrong, California with the answer that Mary is rescued by a young man in his car.  Proctor's solution was printed in the September 1912 issue.Although they would later become synonymous with the medium, and though the heroine did participate in perilous action sequences, no chapter-ending cliffhangers were employed in this production. The sequel Who Will Marry Mary? (which did pose a question in its title) was released in 1913.


== Episodes ==
The serial consisted of twelve one-reel episodes released from July 26, 1912 to June 27, 1913:

The Escape from Bondage
Alone in New York
Mary in Stageland
The Affair at Raynor's
A Letter to the Princess
A Clue to Her Parentage
False to Their Trust
A Will and a Way
A Way to the Underworld
The High Tide of Misfortune
A Race to New York
Fortune SmilesA stage version written by Owen Davis and featuring Olive Wyndham as Mary premiered at the Lyric Theatre in Allentown, Pennsylvania on March 4, 1913, before opening at the Fulton Theatre in New York on March 24, 1913. The 1913 novelization by Bob Brown incorporated material from the play, the films, and the Ladies' World stories.


== References ==


== External links ==
What Happened to Mary on IMDb
What Happened to Mary at AllMovie
What Happened to Mary (novel) at the Internet Archive