Making the sign of the cross (Latin: signum crucis), or blessing oneself or crossing oneself, is a ritual blessing made by members of some branches of Christianity. This blessing is made by the tracing of an upright cross or + across the body with the right hand, often accompanied by spoken or mental recitation of the trinitarian formula: "In the name of the Father, and of the Son, and of the Holy Spirit. Amen."The use of the sign of the cross traces back to early Christianity, with the second century Apostolic Tradition directing that it be used during the minor exorcism of baptism, during ablutions before praying at fixed prayer times, and in times of temptation.The movement is the tracing of the shape of a cross in the air or on one's own body, echoing the traditional shape of the cross of the Christian crucifixion narrative. Where this is done with fingers joined, there are two principal forms: one—three fingers, right to left—is exclusively used by the Eastern Orthodox Church, Church of the East and the Eastern Rite (Catholic) churches in the Byzantine, Assyrian and Chaldean traditions; the other—left to right to middle, other than three fingers—sometimes used in the Latin Rite (Catholic) churches, Lutheranism, Anglicanism and in  Oriental Orthodoxy. The sign of the cross is used in some branches of Methodism. The ritual is rare within the Reformed tradition and in other branches of Protestantism.
Many individuals use the expression "cross my heart and hope to die" as an oath, making the sign of the cross, in order to show "truthfulness and sincerity", sworn before God, in both personal and legal situations.


== Origins ==
The sign of the cross was originally made in some parts of the Christian world with the right-hand thumb across the forehead only. In other parts of the early Christian world it was done with the whole hand or with two fingers. Around the year 200 in Carthage (modern Tunisia, Africa), Tertullian wrote: "We Christians wear out our foreheads with the sign of the cross." Vestiges of this early variant of the practice remain: in the Roman Rite of the Mass in the Catholic Church, the celebrant makes this gesture on the Gospel book, on his lips, and on his heart at the proclamation of the Gospel; on Ash Wednesday a cross is traced in ashes on the forehead; chrism is applied, among places on the body, on the forehead for the Holy Mystery of Chrismation in the Eastern Orthodox Church.


== Gesture ==

The open right hand is used in Western Christianity. The five open fingers are often said to represent the Five Wounds of Christ. This symbolism was adopted after the more ancient gesture of two or three fingers was simplified. Though this is the most common method of crossing by Western Christians, other forms are sometimes used. The West employs the "Small Sign of the Cross" (+), using only the thumb: The priest or deacon, while announcing the Gospel text, "makes the Sign of the Cross on the book and on his forehead, lips, and breast. The people acclaim: 'Glory to you, O Lord'." Some in the congregation have taken to imitating the celebrant. The Small Sign is also used during the majority of the Sacraments.
In the Eastern Orthodox and Byzantine Catholic churches, the tips of the first three fingers (the thumb, index, and middle ones) are brought together, and the last two (the "ring" and little fingers) are pressed against the palm. The first three fingers express one's faith in the Trinity, while the remaining two fingers represent the two natures of Jesus, divine and human.The Oriental Orthodox (Armenians, Copts, Ethiopians, etc.), like the Western Christians, touch the left shoulder before the right.


=== Motion ===
The sign of the cross is made by touching the hand sequentially to the forehead, lower chest or stomach, and both shoulders, accompanied by the Trinitarian formula: at the forehead In the name of the Father (or In nomine Patris in Latin); at the stomach or heart and of the Son (et Filii); across the shoulders and of the Holy Spirit/Ghost (et Spiritus Sancti); and finally: Amen.There are several interpretations, according to Church Fathers: the forehead symbolizes Heaven; the solar plexus (or top of stomach), the earth; the shoulders, the place and sign of power. It also recalls both the Trinity and the Incarnation. Pope Innocent III (1198–1216) explained: "The sign of the cross is made with three fingers, because the signing is done together with the invocation of the Trinity. ... This is how it is done: from above to below, and from the right to the left, because Christ descended from the heavens to the earth..."There are some variations: for example a person may first place the right hand  in holy water. After moving the hand from one shoulder to the other, it may be returned to the top of the stomach. It may also be accompanied by the recitation of a prayer (e.g., the Jesus Prayer, or simply "Lord have mercy"). In some Catholic regions, like Spain, Italy and Latin America, it is customary to form a cross with the index finger and thumb and then to kiss one's thumb at the conclusion of the gesture, while in the Philippines, this extra step evolved into the thumb quickly touching the chin or lower lip. Ending the sign of the cross some place the right open hand upon the other (the left open hand), with all the five fingers, like in a prayer, till the height of the face.


=== Sequence ===
Athanasius of Alexandria (269–373 A.D.)

By the signing of the holy and life-giving cross, devils and various scourges are driven away. For it is without price and without cost and praises him who can say it. The holy fathers have, by their words, transmitted to us, and even to the unbelieving heretics, how the two raised fingers and the single hand reveal Christ our God in His dual nature but single substance. The right hand proclaims His immeasurable strength, His sitting on the right hand of the Father, and His coming down unto us from Heaven. Again, by the movement of the hands to our right the enemies of God will be driven out, as the Lord triumphs over the Devil with His inconquerable power, rendering him dismal and weak.

Theodoret (393–457) gave the following instruction:

This is how to bless someone with your hand and make the sign of the cross over them. Hold three fingers, as equals, together, to represent the Trinity: God the Father, God the Son, and God the Holy Ghost. These are not three gods, but one God in Trinity. The names are separate, but the divinity one. The Father was never incarnate; the Son incarnate, but not created; the Holy Ghost neither incarnate nor created, but issued from the Godhead: three in a single divinity. Divinity is one force and has one honor. They receive on obeisance from all creation, both angels and people. Thus the decree for these three fingers.
You should hold the other two fingers slightly bent, not completely straight. This is because these represent the dual nature of Christ, divine and human. God in His divinity, and human in His incarnation, yet perfect in both. The upper finger represents divinity, and the lower humanity; this way salvation goes from the higher finger to the lower. So is the bending of the fingers interpreted, for the worship of Heaven comes down for our salvation. This is how you must cross yourselves and give a blessing, as the holy fathers have commanded.
Peter of Damascus (12th century) gave the following instruction:

Then we should also marvel how demons and various diseases are dispelled by the sign of the precious and life-giving Cross, which all can make without cost or effort. Who can number the panegyrics composed in its honor? The holy fathers have handed down to us the inner significance of this sign, so that we can refute heretics and unbelievers. The two fingers and single hand with which it is made represent the Lord Jesus Christ crucified, and He is thereby acknowledged to exist in two natures and one hypostasis or person.
The use of the right hand betokens His infinite power and the fact that He sits at the right hand of the Father. That the sign begins with a downward movement from above signifies His descent to us from heaven. Again, the movement of the hand from the right side to the left drives away our enemies and declares that by His invincible power the Lord overcame the devil, who is on the left side, dark and lacking strength.
Historian Herbert Thurston interprets this as indicating that at one time both Eastern and Western Christians moved the hand from the right shoulder to the left, although the point is not entirely clear. German theologian Valentin Thalhofer thought writings quoted in support of this, such as that of Innocent III, refer to the small cross made upon the forehead or external objects, in which the hand moves naturally from right to left, and not the big cross made from shoulder to shoulder. Andreas Andreopoulos, author of The Sign of the Cross, gives a more detailed description of the development and the symbolism of the placement of the fingers and the direction of the movement.Today, Western Christians and the Oriental Orthodox touch the left shoulder before the right. Eastern Catholics and Orthodox Christians use the right-to-left movement.
The proper sequence of tracing the sign of the cross is taught to converts from Christian denominations that are either nontrinitarian or not using the gesture and from other non-Christian religions


== Use ==
The sign of the cross may be made by individuals upon themselves as a form of prayer and by clergy upon others or objects as an act of blessing. The gesture of blessing is certainly related to the sign of the cross, but the two gestures developed independently after some point. In Eastern Christianity, the two gestures differ significantly. Priests and deacons are allowed to bless using the right hand, while bishops may bless simultaneously with both, the left mirroring the right. Individuals may make it at any time, clergy must make it at specific times (as in liturgies), and it is customary to make it on other occasions.
Although the sign of the cross dates to ante-Nicene Christianity, it was rejected by some of the Reformers and is absent from some forms of Protestantism. It was commended and retained by Martin Luther and remains in use by Lutheran clergy, but its use is not universal by the laity. In Anglicanism, its use was revived by the Oxford Movement and is fairly common. It is required by the Book of Common Prayer  for the priest to use it when administering Baptism and this was codified by the canon law of the Church of England in 1604. In the Reformed tradition, such as Presbyterianism, especially the mainline Presbyterian,  its use would be during baptism, communion, confirmation, benedictions and sometimes with the creeds. Ministers and some laity in Methodism very rare occasions will use it. Other Protestants and Restorationist Christians do not use it all.
Some, particularly Roman Catholics and Eastern Christians, might make the sign of the cross in response to perceived blasphemy. Others sign themselves to seek God's blessing before or during an event with uncertain outcome. In Hispanic countries, people often sign themselves in public, such as athletes who cross themselves before entering the field or while concentrating for competition.


=== Catholicism ===
The sign of the cross is a prayer, a blessing, and a sacramental. As a sacramental, it prepares an individual to receive grace and disposes one to cooperate with it. The Christian begins the day, prayers, and activities with the Sign of the Cross: "In the name of the Father and of the Son and of the Holy Spirit. Amen." In this way, a person dedicates the day to God and calls on God for strength in temptations and difficulties. John Vianney said a genuinely made Sign of the Cross "makes all hell tremble."In the Roman or Latin Rite Church it is customary to make the full Sign of the Cross using holy water when entering a church. The first three fingers of the right hand are dipped into the font containing the holy water and the Sign of the Cross is made on oneself. This gesture has a two-fold purpose:  to remind one of one's baptism and the rights and responsibilities that go with it  and to also remind one that one is entering a sacred place that is set apart from the world outside.Also, a longer version is commonly said while making the sign of the cross on the forehead, the mouth and the chest: "By the sign of the cross deliver us from our enemies, you who are our God. In the name of the Father, and of the Son, and of the Holy Spirit. Amen." After the "you who are our God" part is recited, the ritual then proceeds with the normal Catholic sign of the cross as explained above.


==== Liturgical ====
Roman Catholicism draws a distinction between liturgical and non-liturgical use of the sign of the cross. The sign of the cross is expected at two points of the Mass: the laity sign themselves during the introductory greeting of the service and at the final blessing; optionally, other times during the Mass when the laity often cross themselves are during a blessing with holy water, when concluding the penitential rite, in imitation of the priest before the Gospel reading (small signs on forehead, lips, and heart), and perhaps at other times out of private devotion. In the ordinary form of the Roman Rite the priest signs the bread and wine at the epiclesis before the consecration. In the Tridentine Mass the priest signs the bread and wine 25 times during the Canon of the Mass, ten times before and fifteen times after they have been consecrated. The priest also uses the sign of the cross when blessing a deacon before the deacon reads the Gospel, when sending an Extraordinary Minister of Holy Communion to take the Eucharist to the sick (after Communion, but before the end of the Mass), and when blessing the congregation at the conclusion of the Mass.
Ordained bishops, priests and deacons have more empowerment to bless objects and other people. While lay people may preside at certain blessings, the more a blessing is concerned with ecclesial or sacramental matters, the more it is reserved to clergy. Extraordinary Ministers of Holy Communion do not ordinarily have a commission to bless in the name of the Church, as priests and deacons do. At this point in the liturgy, their specific function is to assist the clergy in the distribution of holy Communion. Extraordinary Ministers of Communion blessing those who do not wish to or cannot receive communion can speak or raise the hand but not make the sign of the cross over the person.


==== Non-liturgical ====
A priest or deacon blesses an object or person with a single sign of the cross, but a bishop blesses with a triple sign of the cross. In the Catholic organization the Legion of Mary, members doing door-to-door parish surveys bless the homes of those not home by tracing the sign of the cross on the door.


=== Eastern Orthodoxy ===

In the Eastern traditions, both celebrant and congregation make the sign of the cross quite frequently. It is customary in some Eastern traditions to cross oneself at each petition in a litany and to closely associate oneself with a particular intention being prayed for or with a saint being named. The sign of the cross is also made upon entering or leaving a church building, at the start and end of personal prayer, when passing the main altar (which represents Christ), whenever all three persons of the Trinity are addressed, and when approaching an icon.


==== Priests' Hand Blessing ====

When an Eastern Orthodox or Byzantine catholic bishop or priest blesses with the sign of the cross, he holds the fingers of his right hand in such a way that they form the Greek abbreviation for Jesus Christ "IC XC". The index finger is extended to make the "I"; the middle finger signify letter "C"; the thumb touches the lowered third finger to signify the "X" and the little finger also signifies the letter "C".When a priest blesses in the sign of the cross, he positions the fingers of his right hand in the manner described as he raises his right hand, then moves his hand downwards, then to his left, then to his right. A bishop blesses with both hands (unless he is holding some sacred object such as a cross, chalice, Gospel Book, icon, etc.), holding the fingers of both hands in the same configuration, but when he moves his right hand to the left, he simultaneously moves his left hand to the right, so that the two hands cross, the left in front of the right, and then the right in front of the left. The blessing of both priests and bishops consists of three movements, in honour of the Holy Trinity.


=== Old Believers ===

In Russia, until the reforms of Patriarch Nikon in the 17th century, it was customary to make the sign of the cross with two fingers (symbolising the dual nature of Christ).  The enforcement of the three-finger sign was one of the reasons for the schism with the Old Believers whose congregations continue to use the two-finger sign of the cross.


=== Lutheranism ===
Among Lutherans the practice was widely retained. For example, Luther's Small Catechism states that it is expected before the morning and evening prayers. Lutheranism never abandoned the practice of making the sign of the cross in principle and it was commonly retained in worship at least until the early 19th century. During the 19th and early 20th centuries it was largely in disuse until the liturgical renewal movement of the 1950s and 1960s. One exception is The Lutheran Hymnal (1941) of the Lutheran Church–Missouri Synod (LCMS), which states that "The sign of the cross may be made at the Trinitarian Invocation and at the words of the Nicene Creed 'and the life of the world to come.'" Since then, the sign of the cross has become fairly commonplace among Lutherans at worship. The sign of the cross is now customary in the Divine Service. Rubrics in contemporary Lutheran worship manuals, including Evangelical Lutheran Worship of the Evangelical Lutheran Church in America and Lutheran Service Book used by LCMS and Lutheran Church–Canada, provide for making the sign of the cross at certain points in the liturgy. Places approximate the Roman Catholic practice: at the trinitarian formula, the benediction, at the consecration of the Eucharist, and following reciting the Nicene or Apostles' Creed.
Devotional use of the sign of the cross among Lutherans also includes after receiving the Host and Chalice in the Eucharist, following Holy Absolution; similarly, they may dip their hands in the baptismal font and make the sign of the cross upon entering the church.


=== Methodism ===
The sign of the cross can be found in the Methodist liturgy of both African-American Methodist Episcopal Churches and The United Methodist Church. It is made by some clergy during the Great Thanksgiving, Confession of Sin and Pardon, and benediction. John Wesley, the principal leader of the early Methodists, prepared a revision of The Book of Common Prayer for Methodist use called The Sunday Service of the Methodists in North America which instructs the presiding minister to make the sign of the cross on the forehead of children just after they have been baptized. Making the sign of the cross at baptism is retained in the current Book of Worship of The United Methodist Church, and is widely practiced (sometimes with oil). Furthermore, on Ash Wednesday the sign of the cross is almost always applied by the elder to the foreheads of the laity. The liturgy for healing and wholeness, which is becoming more commonly practiced, calls for the pastor to make the sign of the cross with oil upon the foreheads of those seeking healing.Whether or not a Methodist uses the sign for private prayer is a personal choice, but it is encouraged by the bishops of the United Methodist Church. Some United Methodists also perform the sign before and after receiving Holy Communion, and some ministers also perform the sign when blessing the congregation at the end of the sermon or service.


=== Reformed tradition and Presbyterians ===
In some Reformed churches, such as the PCUSA and the Cumberland Presbyterian Church, Presbyterian Church in America the sign of the cross is used on the foreheads during baptism or during an Ash Wednesday service when ashes are imposed on the forehead. The sign of the cross is on rare occasions used during Communion and during the Confession of Sin and the Creeds. In instances during a Benediction, when the minister concludes the service using the Trinitarian blessing, a hand is extended and a sign of the cross is made out toward the congregation, but this is also quite rare.


=== Anglicans / Episcopalians ===
Anglicans and Episcopalians make the sign of the cross from touching one's forehead to chest or upper stomach, then from left side to right side of the breast, and often ending in the center. It is used during worship services and in day to day life by most or all Anglicans / Episcopalians such as those in the Church Of England.


=== Baptists and Evangelicals ===
Baptists and Evangelicals feel that crossing themselves is not necessary and that it is the choice of the person whether or not if they will cross themselves during prayer. Therefore, most Baptists and Evangelicals do not make the sign of the cross, but a small proportion of them still do. It is often looked at as an ancient Christian practice and is not necessary in the current church.


=== Pentecostals ===
Most pentecostals do not make the sign of the cross at all. In fact, some Pentecostal churches do not include the latin cross in their church.


=== Armenian Apostolic ===
It is common practice in the Armenian Apostolic Church to make the sign of the cross when entering or passing a church, during the start of service and at many times during Divine Liturgy. The motion is performed by joining the first three fingers, to symbolize the Holy Trinity, and putting the two other fingers in the palm, then touching one's forehead, below the chest, left side, then right side and finishing with open hand on the chest again with bowing head.


=== Assyrian Church of the East ===
The Assyrian Church of the East uniquely holds the sign of the cross as a sacrament in its own right. Another sacrament unique to the church is the Holy Leaven.


== See also ==

Christian symbolism
Crossed fingers
Prayer in Christianity
Veneration


== References ==


== External links ==


=== Catholic ===
"Sacramentals", Catechism of the Catholic Church
Thurston, Herbert. "The Cross and Crucifix in Liturgy." The Catholic Encyclopedia. Vol. 4. New York: Robert Appleton Company, 1908.
Ghezzi, Bert. "Significance of the Sign of the Cross"


=== Orthodox ===
Sign of the Cross (orthodoxwiki.org)
"Why do Orthodox Christians 'cross themselves' different than Roman Catholics?"
The Church Council of the Hundred Chapters (1551) (Old Believers)


=== Protestant ===
Why Do Lutherans Make the Sign of the Cross? (ELCA website)
Episcopalian site