Life Savers is an American brand of ring-shaped hard and soft candy.  Its range of mints and fruit-flavored candies is known for its distinctive packaging, coming in paper-wrapped aluminum foil rolls. 
Candy manufacturer Clarence Crane of Garrettsville, Ohio (father of the poet Hart Crane) invented the brand in 1912 as a "summer candy" that could withstand heat better than chocolate. The candy's name is due to the fact that its shape resembles that of a traditional ring-style life preserver also known as a "life saver".
After registering the trademark, Crane sold the rights to his Pep-O-Mint peppermint candy to Edward John Noble for $2,900. Instead of using cardboard rolls, which were not very successful, Noble created tin-foil wrappers to keep the mints fresh. Noble founded the Life Savers and Candy Company in 1913 and significantly expanded the market for the product by installing Life Savers displays next to the cash registers of restaurants and grocery stores.  He also encouraged the owners of the establishments to always give customers a nickel in their change to encourage sales of the 5¢ Life Savers. The slogan "Still only 5 cents" helped Life Savers to become a favorite treat for children with a tight allowance. Since then, many different flavors of Life Savers have been produced. The five-flavor roll first appeared in 1935.
A series of mergers and acquisitions by larger companies began in 1956.  Life Savers is currently a property of Mars, Incorporated. In recent decades, the brand expanded to include Gummi Savers in 1992, Life Saver Minis in 1996, Creme Savers in 1998, and Life Saver Fusions in 2001. Discontinued varieties include Fruit Juicers, Holes, Life Saver Lollipops, and Squeezit.
In 1995, a Life Savers drink was introduced to compete with Snapple and Fruitopia, but it was quickly discontinued.


== History ==
Life Savers was first created in 1912 by Clarence Crane, a Garrettsville, Ohio, candy maker (and father of the famed poet Hart Crane). Clarence had switched from the maple sugar business to chocolates the year before, but found that they sold poorly in the summer, because air conditioning was rare and they melted. He saw a machine pharmacies used to make pills that were round and wafer-shaped, and thought he'd use those to make mints, which at the time were made square (because they were pressed in sheets and then sliced into squares). The machinery could also punch a hole in the center, and Crane named the candy after its resulting life preserver shape.
In 1913, Crane sold the formula for his Life Savers candy to Edward Noble of Gouverneur, New York for $2,900. Noble started his own candy company and began producing and selling the mints known as Pep-O-Mint Life Savers. He also began to package the mints into rolls wrapped in tin foil to keep them from going stale. This process was done by hand until 1919 when machinery was developed by Edward Noble's brother, Robert Peckham Noble, to streamline the process.

Robert was a Purdue-educated engineer. He took his younger brother's entrepreneurial vision and designed and built the manufacturing facilities needed to expand the company.  The primary manufacturing plant for Life Savers was located in Port Chester, New York, a local landmark replete with a Life Savers motif cast into the cornice.  Robert led the company as its Chief Executive Officer and primary shareholder for more than 40 years, until selling the company in the late 1950s.
By 1919, six other flavors (Wint-O-Green, Cl-O-Ve, Lic-O-Rice, Cinn-O-Mon, Vi-O-Let and Choc-O-Late) had been developed, and these remained the standard flavors until the late 1920s. In 1920, a new flavor called Malt-O-Milk was introduced. This flavor was received so poorly that it was discontinued after only a few years. In 1925, the tinfoil was replaced with aluminum foil.
Noble promoted the candy at the cash registers of saloons, cigar stores, drug stores, barber shops, and restaurants. He had the candy placed, with a five-cent price, near the cash register.
In 1921, the company began to produce solid fruit drops. In 1925, technology improved to allow a hole in the center of the fruit candies. These were introduced as the "fruit drop with the hole" and came in Orange, Lemon and Lime, each of which were packaged in their own separate rolls. In contrast to the opaque white mints previously produced by the company, these new candies were crystal-like in appearance. These new flavors quickly became popular with the public. Four new flavors were quickly introduced, namely, anise, butter rum, cola and root beer, which were made in the clear fruit drop style. These did not prove to be as popular as the three original fruit drop flavors.
In 1931, the Life Savers "Cough Drop" was introduced with menthol, but it was not successful. In 1931, rolls of pineapple and cherry fruit drops were also introduced. As the public response proved positive for these, a new variety of mint called Cryst-O-Mint, made in this same crystal-like style, was introduced in 1932. In 1935, the classic "Five Flavor" rolls were introduced, offering a selection of five different flavors (pineapple, lime, orange, cherry, and lemon) in each roll. This flavor lineup was unchanged for nearly 70 years, until 2003, when three of the flavors were replaced in the United States, making the rolls pineapple, cherry, raspberry, watermelon, and blackberry.  However, orange was subsequently reintroduced and blackberry was dropped. The original five flavor lineup is still sold in Canada. In the late 1930s and early 1940s, four new mint flavors were introduced: Molas-O-Mint, Spear-O-Mint, Choc-O-Mint and Stik-O-Pep.
During the Second World War, other candy manufacturers donated their sugar rations to keep Life Savers in production so that the little candies could be shared with Armed Forces as a tasty reminder of life at home. Soon after the war ended, the manufacturing license was withdrawn. In 1947, U.K.-based Rowntree's—which formerly had been licensed to make Life Savers—started to manufacture a similar product called the Polo mint.In 1981, Nabisco Brands Inc. acquired Life Savers from the E.R. Squibb Corporation. A number of early mint flavors, including Cl-O-Ve, Vi-O-Let, Lic-O-Rice and Cinn-O-Mon were discontinued due to poor sales. Nabisco introduced a new Cinnamon flavor ("Hot Cin-O-Mon") as a clear fruit drop-type candy. This replaced the white mint flavor Cinn-O-Mon, which had recently been discontinued. The other original mint flavors were retired. A number of other flavors were also quickly discontinued, after Nabisco took over, in order to make the business more profitable. In 2004, the U.S. Life Savers business was acquired by Wrigley's. Wrigley's introduced two new mint flavors (for the first time in over 60 years) in 2006: Orange Mint and Sweet Mint. They also revived some of the early mint flavors (such as Wint-O-Green).
Life Savers production was based in Holland, Michigan, until 2002 when it was moved to Montreal, Québec, Canada. Significantly lower sugar prices in that country were the reason behind the move. The company's headquarters in Port Chester, New York, where Life Savers were made from 1920 until 1984, was distinctive. Although it has been converted to apartments, it still retains some Lifesavers signage.  It was added to the National Register of Historic Places in 1985.


== Timeline ==

1912: Crane's Peppermint Life Savers created by Clarence Crane in Garrettsville, Ohio.
1913: Edward Noble bought the Life Saver formula, renamed Pep-O-Mint Life Savers, and started Mint Products Company in New York City.
1921: The first fruit flavors were produced as solid candies.
1925: Technology improved to allow a hole in the center of the fruit candies.
1927: Cherry flavor is invented and added to regular flavors.
1935: The original Five Flavor roll of Life Savers debuted.
1956: Life Savers Limited merged with Beech-Nut.
1968: Beech-Nut Life Savers merged with Squibb.
1981: Nabisco Brands Inc. acquired Life Savers from the E.R. Squibb Corporation.
1987: Canadian Life Savers business acquired by Hershey Canada.
1992: Life Savers Gummies launches in three varieties: Grape, Five Flavor, and Mixed Berry.
1996: Canadian Life Savers business acquired by Beta Brands Limited.
2000: Kraft acquires Nabisco.
2001: Kraft acquires Canadian Life Savers business from Beta Brands.
2004: Life Savers business acquired by Wrigley's.
2008: Mars acquires Wrigley


== See also ==
Fruit Tingles
Polo mints – A similar European confection by Nestlé, which had a trademark battle with Life Savers over the ring-shaped form.
Triboluminescence – An optical phenomenon in which light is generated when material is subject to mechanical breaking, especially noticeable when crushing Wint-O-Green Life Savers in the dark.


== References ==


== External links ==
Official website