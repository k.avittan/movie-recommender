Dance, Fools, Dance (1931) is a pre-Code Metro-Goldwyn-Mayer movie starring Joan Crawford, Clark Gable, and Lester Vail in a story about a reporter investigating the murder of a colleague. Story and dialogue were created by Aurania Rouverol, and the film was directed by Harry Beaumont. Dance, Fools, Dance was the first of eight movies featuring Crawford and Gable.


== Plot summary ==
Former socialite Bonnie Jordan (Joan Crawford) and her brother Rodney (William Bakewell) have their lives turned upsidown one day when their father loses his entire fortune in the stock market crash, and subsequently dies of a heart attack. 
Due to their inheritance being wiped out overnight, the siblings are forced to fire their wait staff, sell their belongings, and work to earn a living. 
Bonnie decides to get a mans job and winds up as a cub reporter for a newspaper, while Rodney decides to get involved with a beer-running gang, but things begin to escalate for him quickly. 
On one caper, Rodney drives the get away car after his gang guns down a rival group, leaving Rodney emotionally scarred. Things only get worse when Bonnie's journalist colleague Bert Scranton (Cliff Edwards)finds out too much, and Gang chief Jake Luva (Clark Gable) orders Rodney to murder him under threat of death, leaving him no choice but to go through with it. 
Bonnie is given the task of investigating the murder of her colleague, and she infiltrates Bert Scranton’s club as a dancer, eventually learning the horrifying truth that her brother is the murderer. 
Bert soon catches on to her act though, and he ambushes Bonnie, intending to kill her. However, Rodney arrives just in time and a shootout occurs, with Bonnie barely escaping with her life. As the authorities arrive, Bert and his henchmen are dead, but so is Rodney...and Bonnie cradles his head and cry’s. 
Pulling herself together, Bonnie phones the paper and through tears she reports on the details of the story, including the role that her brother played. 
Despite the paper wanting to keep her on, Bonnie decides that she wants to get away from it all, and as she leaves she meets an old friend who is still rich, and the movie ends as the two kiss, with the implication that they married and lived happily ever after.


== Cast ==
Joan Crawford as Bonnie Jordan
Lester Vail as Bob Townsend
Cliff Edwards as Bert Scranton
William Bakewell as Rodney Jordan
William Holden as Stanley Jordan
Clark Gable as Jake Luva
Earle Foxe as Wally Baxter (credited as Earl Foxe)
Purnell B. Pratt as Parker
Hale Hamilton as Selby
Natalie Moorhead as Della
Joan Marsh as Sylvia
Russell Hopton as Whitey
Sam McDaniel as Jake Luva's Butler


== Reception ==
Photoplay commented: "Again, Joan Crawford proves herself a great dramatic actress. The story ... is hokum, but it's good hokum, and Joan breathes life into her characterization." Andre Sennwald noted in The New York Times, Miss Crawford's acting is still self-conscious, but her admirers will find her performance well up to her standard."


=== Box office ===
According to MGM records, the film earned $848,000 in the U.S. and Canada, and $420,000 elsewhere, resulting in a profit of $524,000.


== Historical note ==
Several events in the screenplay are based loosely on real-life crimes that occurred in Chicago prior to the film's production, such as the St. Valentine's Day Massacre in 1929 and the murder of reporter Jake Lingle by underworld hoodlums in 1930.


== See also ==
Joan Crawford filmography
Clark Gable filmography


== References ==


== External links ==
Dance, Fools, Dance on IMDb
Dance, Fools, Dance at AllMovie
Dance, Fools, Dance at the TCM Movie Database
Dance, Fools, Dance at the American Film Institute Catalog
Poster for Swedish release of Dance, Fools, Dance
Webpage dedicated to Joan Crawford