A food pyramid is a representation of the optimal number of servings to be eaten each day from each of the basic food groups. The first pyramid was published in Sweden in 1974. The 1992 pyramid introduced by the United States Department of Agriculture (USDA) was called the "Food Guide Pyramid" or "Eating Right Pyramid". It was updated in 2005 to "MyPyramid", and then it was replaced by "MyPlate" in 2011.


== Swedish origin ==

Amid high food prices in 1972, Sweden's National Board of Health and Welfare developed the idea of "basic foods" that were both cheap and nutritious, and "supplemental foods" that added nutrition missing from the basic foods. Anna-Britt Agnsäter, chief of the "test kitchen" for Kooperativa Förbundet (a cooperative Swedish retail chain), held a lecture the next year on how to illustrate these food groups. Attendee Fjalar Clemes suggested a triangle displaying basic foods at the base. Agnsäter developed the idea into the first food pyramid, which was introduced to the public in 1974 in KF's Vi magazine. The pyramid was divided into basic foods at the base, including milk, cheese, margarine, bread, cereals and potato; a large section of supplemental vegetables and fruit; and an apex of supplemental meat, fish and egg. The pyramid competed with the National Board's "dietary circle," which KF saw as problematic for resembling a cake divided into seven slices, and for not indicating how much of each food should be eaten. While the Board distanced itself from the pyramid, KF continued to promote it.
Food pyramids were developed in other Scandinavian countries, as well as West Germany, Japan and Sri Lanka. The United States later developed its first food pyramid in 1992.


== Food pyramid published by the WHO and FAO ==
The World Health Organization, in conjunction with the Food and Agriculture Organization, published guidelines that can be effectively represented in a food pyramid relating to objectives in order to prevent obesity, improper nutrition, chronic diseases and dental caries based on meta-analysis  though they represent it as a table rather than as a "pyramid". The structure is similar in some respects to the USDA food pyramid, but there are clear distinctions between types of fats, and a more dramatic distinction where carbohydrates are categorized on the basis of free sugars versus sugars in their natural form. Some food substances are singled out due to the impact on the target issues that the "pyramid" is meant to address. In a later revision, however, some recommendations are omitted as they automatically follow other recommendations while other sub-categories are added. The reports quoted here explain that where there is no stated lower limit in the table below, there is no requirement for that nutrient in the diet.

All percentages are percentages of calories, not of weight or volume. To understand why, consider the determination of an amount of "10% free sugar" to include in a day's worth of calories. For the same amount of calories, free sugars take up less volume and weight, being refined and extracted from the competing carbohydrates in their natural form. In a similar manner, all the items are in competition for various categories of calories.
The representation as a pyramid is not precise, and involves variations due to the alternative percentages of different elements, but the main sections can be represented.


== USDA food pyramid ==


=== History ===

The USDA food pyramid was created in 1992 and divided into six horizontal sections containing depictions of foods from each section's food group. It was updated in 2005 with colorful vertical wedges replacing the horizontal sections and renamed MyPyramid. MyPyramid was often displayed with the food images absent, creating a more abstract design.
In an effort to restructure food nutrition guidelines, the USDA rolled out its new MyPlate program in June 2011. My Plate is divided into four slightly different sized quadrants, with fruits and vegetables taking up half the space, and grains and protein making up the other half. The vegetables and grains portions are the largest of the four.
A modified food pyramid was proposed in 1999 for adults aged over 70.


==== Vegetables ====
A vegetable is a part of a plant consumed by humans that is generally savory but is not sweet. A vegetable is not considered a grain, fruit, nut, spice, or herb. For example, the stem, root, flower, etc., may be eaten as vegetables. Vegetables contain many vitamins and minerals; however, different vegetables contain different spreads, so it is important to eat a wide variety of types. For example, green vegetables typically contain vitamin A, dark orange and dark green vegetables contain vitamin C, and vegetables like broccoli and related plants contain iron and calcium. Vegetables are very low in fats and calories, but ingredients added in preparation can often add these.


==== Grains ====
These foods provide complex carbohydrates, which are a good source of energy and provide much nutrition when unrefined. Examples include corn, wheat, pasta, and rice.


==== Fruits ====
In terms of food (rather than botany), fruits are the sweet-tasting seed-bearing parts of plants, or occasionally sweet parts of plants which do not bear seeds.
These include apples, oranges, grapes, bananas, etc. Fruits are low in calories and fat and are a source of natural sugars, fiber and vitamins. Processing fruit when canning or making into juices may add sugars and remove nutrients. The fruit food group is sometimes combined with the vegetable food group. Note that a massive number of different plant species produce seed pods which are considered fruits in botany, and there are a number of botanical fruits which are conventionally not considered fruits in cuisine because they lack the characteristic sweet taste, e.g., tomatoes or avocados.


==== Oils and sweets ====
A food pyramid's tip is the smallest part, so the fats and sweets in the top of the Food Pyramid should comprise the smallest percentage of the diet. The foods at the top of the food pyramid should be eaten sparingly because they provide calories, but not much in the way of nutrition. These foods include salad dressings, oils, cream, butter, margarine, sugars, soft drinks, candies, and sweet desserts.


==== Dairy ====
Dairy products are produced from the milk of mammals, usually but not exclusively cattle. They include milk, yogurt and cheese. Milk and its derivative products are a rich source of dietary calcium and also provide protein, phosphorus, vitamin A, and vitamin D. However, many dairy products are high in saturated fat and cholesterol compared to vegetables, fruits and whole grains, which is why skimmed products are available as an alternative.
Historically, adults were recommended to consume three cups of dairy products per day. More recently, evidence is mounting that dairy products have greater levels of negative effects on health than previously thought and confer fewer benefits. For example, recent research has shown that dairy products are not related to stronger bones or less fractures; on the flip side, another study showed that milk (and yogurt) consumption results in higher bone mineral density in the hip. Overall, the majority of research suggests that dairy has some beneficial effects on bone health, in part because of milk's other nutrients.


==== Meat and beans ====
Meat is the tissue – usually muscle – of an animal consumed by humans. Since most parts of many animals are edible, there is a vast variety of meats. Meat is a major source of protein, as well as iron, zinc, and vitamin B12. Meats, poultry, and fish include beef, chicken, pork, salmon, tuna, shrimp, and eggs.
The meat group is one of the major compacted food groups in the food guide pyramid. Since many of the same nutrients found in meat can also be found in foods like eggs, dry beans, and nuts, such foods are typically placed in the same category as meats, as meat alternatives. These include tofu, products that resemble meat or fish but are made with soy, eggs, and cheeses. For those who do not consume meat or animal products (see Vegetarianism, veganism and Taboo food and drink), meat analogs, tofu, beans, lentils, chick peas, nuts and other high-protein vegetables are also included in this group. The food guide pyramid suggests that adults eat 2–3 servings per day. One serving of meat is 4 oz (110 g), about the size of a deck of cards.


=== Controversy ===

Certain dietary choices that have been linked to heart disease, such as an 8 oz (230 g) serving of hamburger daily, were technically permitted under the pyramid. The pyramid also lacked differentiation within the protein-rich group ("Meat, Poultry, Fish, Dry Beans, Eggs, and Nuts").In April 1991, the U.S. Department of Agriculture (USDA) halted publication of its Eating Right Pyramid, due to objections raised by meat and dairy lobbying groups concerning the guide’s display of their products. Despite the USDA’s explanations that the guide required further research and testing, it was not until one year later—after its content was supported by additional research—that the Eating Right Pyramid was officially released. This time, even the guide’s graphic design was altered to appease industry concerns. This incident was only one of many in which the food industry attempted to alter federal dietary recommendations in their own economic self-interest.Some of the recommended quantities for the different types of food in the old pyramid have also come under criticism for lack of clarity. For instance, the pyramid recommends two to three servings from the protein-rich group, but this is intended to be a maximum. The pyramid recommends two to four fruit servings, but this is intended to be the minimum.The fats group as a whole have been put at the tip of the pyramid, under the direction to eat as little as possible, which is largely problematic. Under the guide, one would assume to avoid fats and fatty foods, which can lead to health problems. For one, fat is essential in a person's general sustainability. Research suggests that unsaturated fats aid in weight loss, reduce heart disease risk, lower blood sugar, and even lower cholesterol. Also, they are very long sustaining, and help keep blood sugar at a steady level. On top of that, these fats help brain function as well.Several books have claimed that food and agricultural associations exert undue political power on the USDA. Food industries, such as milk companies, have been accused of influencing the United States Department of Agriculture into making the colored spots on the newly created food pyramid larger for their particular product. The milk section has been claimed to be the easiest to see out of the six sections of the pyramid, making individuals believe that more milk should be consumed on a daily basis compared to the others. Furthermore, the inclusion of milk as a group unto itself implies that is an essential part of a healthy diet, despite the many people who are lactose intolerant or choose to abstain from dairy, and a number of cultures that have historically consumed little if any dairy products. Joel Fuhrman says in his book Eat to Live that U.S. taxpayers must contribute $20 billion on price supports to artificially reduce the price of cattle feed to benefit the dairy, beef and veal industries, and then pay the medical bills for an overweight population. He asks if the USDA is under the influence of the food industry, because a food pyramid based on science would have vegetables at its foundation.These controversies prompted the creation of pyramids for specific audiences, particularly the mediterranean pyramid in 1993 and some Vegetarian Diet Pyramids.


== Alternatives to the USDA pyramid ==

The Harvard School of Public Health proposes a healthy eating pyramid, which includes calcium and multi-vitamin supplements as well as moderate amounts of alcohol, as an alternative to the Food Guide Pyramid.
Many observers believe that the Harvard pyramid follows the results of nutrition studies published in peer-reviewed scientific journals more closely.
But in their book Fantastic Voyage: Live Long Enough to Live Forever, published in 2004, Ray Kurzweil and Terry Grossman M.D., point out that the guidelines provided in the Harvard Pyramid fail to distinguish between healthy and unhealthy oils. In addition, whole-grain foods are given more priority than vegetables, which should not be the case, as vegetables have a lower glycemic load. Other observations are that fish should be given a higher priority due to its high omega-3 content, and that high fat dairy products should be excluded. As an alternative, the authors postulate a new food pyramid, emphasising low glycemic-load vegetables, healthy fats, such as avocados, nuts and seeds, lean animal protein, fish, and extra virgin olive oil.
The University of Michigan Integrative Medicine’s Healing Foods Pyramid emphasizes plant-based choices, variety and balance. It includes sections for seasonings and water as well as healthy fats.


== MyPlate ==

MyPlate is the current nutrition guide published by the United States Department of Agriculture, depicting a place setting with a plate and glass divided into five food groups. It replaced the USDA's MyPyramid guide on June 2, 2011, concluding 19 years of USDA food pyramid diagrams.


== See also ==

Food and Nutrition Service
Fruits & Veggies – More Matters
Healthy diet
Healthy eating pyramid
History of USDA nutrition guides
Human nutrition
List of nutrition guides
Nutrition Education
Overall Nutritional Quality Index


== References ==


== External links ==
USDA Dietary Guidelines
Tufts Researchers Update Their Food Guide Pyramid for Older Adults