"Apache" is an instrumental written by English composer Jerry Lordan. The original version was by guitarist Bert Weedon, but Lordan did not like the version. The Shadows recorded "Apache" in June 1960; when it was released the next month, their version topped the UK Singles Chart for five weeks. Bert Weedon's original recording was released at the same time and reached number 24.In 1961, Danish jazz guitarist Jørgen Ingmann's cover of "Apache" (which he recorded in the fall of 1960) went to No. 2 in the US and No. 2 in Canada. A 1973 version by the Incredible Bongo Band has been called "hip-hop’s national anthem".  Although this version was not a hit on release, its long percussion break has been sampled countless times on hip hop and dance tracks since the 1980s.
In March 2005, Q magazine placed "Apache" by the Shadows at No. 96 on its list of the 100 Greatest Guitar Tracks.


== Composition and original recording ==
English songwriter and composer Jerry Lordan came up with the tune. The title "Apache" reflects the source of Lordan's inspiration: the 1954 American western film Apache.
The original recording was by British guitarist Bert Weedon in early 1960. It remained unreleased for several months. In mid-1960 the Shadows were on tour with Lordan as a supporting act. The band discovered "Apache" when Lordan played it on a ukulele. Lordan figured the tune would fit the Shadows; the band agreed.


== Shadows version ==
The recording was done at the EMI Abbey Road Studio in London.  Singer-guitarist Joe Brown had bought an Italian-built guitar echo chamber that he did not like and gave it to Hank Marvin, who developed a distinctive sound using it and the tremolo arm of his Fender Stratocaster. Bruce Welch borrowed an acoustic Gibson J200 guitar from Cliff Richard, the heavy melodic bass was by Jet Harris, percussion was by Tony Meehan and Cliff Richard, who played a Chinese drum at the beginning and end to provide an atmosphere of stereotypically Native American music.
Record producer Norrie Paramor preferred the flip side, an instrumental of the army song "The Quartermaster's Stores", now called "The Quatermasster's Stores" after the TV series Quatermass. Paramor changed his mind after his daughter preferred "Apache". It has been cited by a generation of guitarists as inspirational and is considered one of the most influential British rock 45s of the pre-Beatles era. In a 1963 NME article, The Shadows said, "What's the most distinctive sound of our group?  We often wondered what it is ourselves. Really, it is the sound we had when we recorded 'Apache' – that kind of Hawaiian sounding lead guitar ... plus the beat."


== Charts (Shadows version) ==


=== UK chart history ===
The Shadows' "Apache" entered the UK top 40 on 21 July 1960 at no. 35, climbing into the top 20 the following week. A fortnight later, the song rose twelve places to no. 3 and, on 25 August, deposed "Please Don't Tease" – on which The Shadows backed Cliff Richard – to begin a five-week run at no. 1.
On 29 September, "Apache" dropped to no.2, replaced by "Tell Laura I Love Her" by Ricky Valance. The Shadows version proved to be an enduring hit, enjoying a 19-week run in the top 40 which concluded on 24 November, reappearing for one more week on 8 December. During this run, the group's follow-up single "Man of Mystery/The Stranger" peaked at no.5, alongside the no.3 success of "Nine Times Out of Ten" (backing Cliff Richard).


== Jørgen Ingmann version ==
After the Shadows version began its rise up the UK charts, Weedon's original climbed to no.24 in the UK. However, neither the Shadows nor Weedon had any impact on North America. Then, in late 1960, Jørgen Ingmann produced his own 'twangy' multi-tracked cover version that was released in the United States in November 1960. In 1961, this cover version, credited to "Jørgen Ingmann and His Guitar", made No. 2 on the Billboard Hot 100, behind Blue Moon by The Marcels. On other US charts, "Apache" reached No. 9 on the US R&B chart. The track reached no.1 on Canada's CHUM Chart.


== Edgar Broughton Band's "Apache Drop Out" ==
In 1970, English progressive rock group The Edgar Broughton Band released a single "Apache Drop Out", which combined "Apache" with a version of Captain Beefheart's "Drop Out Boogie". The highly unorthodox single reached No. 33 on the UK Singles Chart.


== Incredible Bongo Band version ==
A 1973 version by Michael Viner and a funk group called The Incredible Bongo Band added a bongo drum introduction and included more percussion. The drum break was played by Jim Gordon. Although this version was not a hit on its initial release, it became heavily sampled in early hip hop music, including by Afrika Bambaataa, who cited its influence. It has been sampled by hip hop performers such as The Sugarhill Gang, L.L. Cool J, The Roots and Nas, techno performers The Future Sound of London and Moby, and drum and bass acts J Majik and Goldie.The 2013 documentary Sample This, directed by Dan Forrer and narrated by Gene Simmons, recounts the story of The Incredible Bongo Band and its recording of "Apache".


=== Notable samples ===


=== The Sugarhill Gang version ===

In 1981, the rap group known as the Sugarhill Gang covered the Incredible Bongo Band's version of the song on its second album, 8th Wonder. In 1982, this version peaked at No. 53 on the Billboard Hot 100, No. 51 on the US Dance chart, and No. 13 on the US R&B chart. In 1995, this version gained additional popularity after being featured in "Viva Lost Wages", a sixth-season episode of the NBC sitcom The Fresh Prince of Bel-Air, as well as in "Whoops, There It Is", a subsequent clip show from the series. Using the distinctive beat and bongo drums as well as Native American war cries, the Sugarhill Gang added rap lyrics with references to include the following:

The Lone Ranger (a.k.a. "kemosabe") is mentioned extensively, as well as his sidekick ("Tonto, jump on it! Jump on it! Jump on it!") and his horse ("'Hi-yo, Silver!' is what I say").
The lyric "Now what you hear is not a test" recalls the Sugarhill Gang's earlier hit, "Rapper's Delight".
The instrumental "Popcorn" by Hot Butter (who had released a version of "Apache" as a follow-up to "Popcorn") is referenced via the lyric "(What's that?) Hot buttered popcorn!"
The recording engineer for Sugar Hill Records, Steve Jerome, was also a member and engineer for "Popcorn" by Hot Butter.
The popcorn and its butter are referenced in lyrics right beforehand, recalling a 1976 Mazola margarine commercial "We Call It Maize" featuring a Native American woman.
The "Monster Mash" is mentioned in this song, as well as the Jerk.A reworked version of this song for children titled simply "Jump on It!" is featured as the title track on the Sugarhill Gang's album Jump on It!. This song differs from the original version with the signature "Jump on it" line being replaced by "Jump up", lyrics encouraging children to learn science, mathematics, and English, and a stronger funk influence.


==== Sir Mix-A-Lot: "Jump on It" (1996) ====
In 1996, Sir Mix-A-Lot played off the lyrics to the Sugarhill Gang's version of "Apache" in his hit "Jump on It", released on the album Return of the Bumpasaurus. The lyrics contain the names of the following cities of the United States:


==== Fatboy Slim: "Apache" (1998) ====
The soundtrack of the movie Snatch has an extract of a DJ mix CD (On the Floor at the Boutique) by Fatboy Slim, which contained the Incredible Bongo Band version of "Apache".


==== Switch: "A Bit Patchy" (2005/2006) ====
In 2005, Switch extensively sampled the covered version by Michael Viner's Incredible Bongo Band for his track "A Bit Patchy" (wordplay on "Apache"). The track has since been used to advertise William Hill Online on TV and has been remixed by artists such as Eric Prydz and Sinden.


=== Other songs that sample the Incredible Bongo Band's "Apache" ===
DJ Grandmaster Flash interpolated parts of the Incredible Bongo Band song "Apache" in his song "The Adventures of Grandmaster Flash on the Wheels of Steel".
West Street Mob, a group on the Sugar Hill Records label, made a song which interpolated parts of the "Apache" song by the Incredible Bongo Band; this song was called "Break Dance (Electric Boogie)".
Boogie Down Productions sampled the break in their 1992 track "Who Are The Pimps?"
Young MC sampled the break in his 1988 rap "Know How", mixed with Isaac Hayes' "Theme From Shaft".
MC Zappa sampled various portions of the Incredible Bongo Band version of "Apache" in his song "Supperhero" from his 2020 EP Hindsight.
MC Hammer sampled the bongo loop in his track "Turn This Mutha Out", off the 1988 album Let's Get It Started.
Vanilla Ice sampled "Apache" in his hit song "Ninja Rap". It was also sampled in the Ultimix version of "Ice Ice Baby".
C + C Music Factory sample the start of "Apache" in the song "Things That Make You Go Hmmm..."
It was used as the beat and background music on the song "We Run This" on Missy Elliott's album The Cookbook.
The Beastie Boys used a sample from the Incredible Bongo Band version of "Apache" in their live version of "Root Down" – most notably the version that appears on the Tibetan Freedom Concert live album.
It was also used in the original version of "Can I Get Witcha" by The Notorious B.I.G.
The British blues singer Amy Winehouse used a sample from "Apache" in her song "In My Bed" produced by Salaam Remi for her 2003 album Frank. Salaam Remi also utilized "Apache" for sampling when he produced NY Rapper Nas' hit single "Made You Look" from his 2002 release God's Son.
UK rapper M.I.A. made "Apache" the center of her 2005 Radio One B-side "Apache Riddim".
Nas sampled IBB's "Apache" in his 2003 single, "Made You Look", and on his 2006 single "Hip Hop Is Dead".
Also in 2006, The Federation sampled a piece of the song for their single "I Only Wear My White Tees Once".
Sampled on the song "Funky" by DJ Shadow and Cut Chemist on their 2007 collaboration, "The Hard Sell".
It is used in the song "Against All Odds" by Chase & Status featuring Kano.
The Roots sample the bongo break at the beginning for their song "Thought @ Work" from their album Phrenology, which is an homage to "Men at Work" by Kool G Rap and DJ Polo, which also samples the break.
Double Dee and Steinski sampled the bongo break for their classic mash-up/collage "Lesson 1 – The Payoff Mix".
Madonna incorporated the bongo samples to "Into the Groove" during her 2008–2009 Sticky & Sweet Tour.
TLC sampled the bongo loop in the video version of their song, "Hat 2 Da Back".
Petter Askergren used bongo samples in his cover of Thomas Di Leva's hit "Dansa din djävul".
Rage Against the Machine interpolated parts of the percussion break in their cover of "Renegades of Funk".
Hip hop artists Jay-Z and Kanye West sampled the bongo drums on their track "That's My Bitch" from their 2011 collaborative album, Watch the Throne.


== Other cover versions ==
From this point, the song became a staple of instrumental combos on both sides of the Atlantic. Among many recordings, Spanish rock band Los Pekenikes covered "Apache" in 1961, The Ventures in 1962 (on the album The Ventures Play Telstar and the Lonely Bull) and Davie Allan and The Arrows (as "Apache '65") in 1965. Sonny James recorded a vocal music version in 1961. It was produced in Nashville by Chet Atkins and was review-rated as a Spotlight Winner. George Harrison said The Beatles used to play "Apache" as well as other Shadows' hits ("FBI", "The Frightened City") during their shows in Hamburg.
In 1960 Jerry Lordan recorded his own version together with The Johnnie Spence Orchestra.
In the 1961, Si Zentner released a version on his Big Band Plays the Big Hits album, along with "Up a Lazy River".
In 1969, Eyes of Blue released a version titled "Apache '69" under the name the Imposters. The B-side of this Mercury label release (MF1080) was the song "QIII".
In 1972, the Moog-based band of session musicians called Hot Butter released a cover version of "Apache" as follow-up to their hit "Popcorn".
In 1976, the electro-rock French band Rockets, in their first eponymous album, released a version featuring synthesizers, disco-rock drumming, and heavily treated guitars.
In the 1970s the Tennessee Farm Band did a version.
In 1977, The Tommy Seebach Band recorded a disco-styled version and filmed an accompanying music video of "Apache". Set on a rocky hillside, it featured scantily clad dancers around a grinning Tommy Seebach while he plays keyboards. This version was successful in Europe.
In 1981 The Sugarhill Gang released "Apache (Jump On It)”. It is one of the earliest and most popular songs in the history of mainstream rap. It was the fifth rap song to appear on the Hot 100, entering on February 13, 1982 and peaking at #53 two months later. The sound of bongo drums are emulated from the 1973 version of “Apache” by the Incredible Bongo Band.
Black Sabbath with Tony Martin on vocals played a live cover of the song at their 1989 concert in Moscow.
Ska-Dows recorded a ska version of "Apache", including some lyrics, mostly the word "Apache!" shouted repeatedly.
The 1992 compilation album Ruby Trax features a version of "Apache" by Senseless Things.
In 1992, Norwegian a cappella group Bjelleklang recorded their version of "Apache" on the album Holiholihoo.
In 1993, General Base, one of the projects of Thomas Kukula, released "Apache" as single and it also appears on the First album.
The California Guitar Trio covered "Apache" for their 1995 album, Invitation.
The song was covered by Ritchie Blackmore in the 1996 collection Twang!: A Tribute to Hank Marvin & the Shadows.
In 2002, Portishead's Geoff Barrow and Adrian Utley recorded a cover version which was released on limited white, green, pink and black vinyl 7-inch single under the name The Jimi Entley Sound.
in 2003, Jimmy Thackery covered "Apache" for his album Guitar.
In 2005, the German band Scooter covered this song as an instrumental for the album Who's Got The Last Laugh Now? in a techno version. Later that year, a single was released which combined elements of "Apache" and "Rock Bottom" from the same album, known as "Apache Rocks The Bottom". This later appeared on the second Disc of the UK edition of its 2008 album Jumping All Over The World.
On the 2006 album Hier is Normaal, the Dutch band Normaal made a compilation of instrumental songs of their own and other artists. "Apache" is also in it. The song, "Varkens Pesten", means literally "bullying pigs".
French guitarist Jean-Pierre Danel recorded a version of "Apache" on his No. 1 hit album Guitar Connection (Sony Music) in 2006. The album went platinum and included a DVD on which Danel shows how to play the songs, including "Apache".
In 2010, Cisco Herzhaft performs a solo version in finger picking on his album "The Cisco's System"
"Apache" was covered by the folk band 17 Hippies on their 2007 album Heimlich.


=== Notable live covers ===
Junior Brown regularly performs "Apache" in his live shows.
In 2010, Jeff Beck performed a version of "Apache" during his tribute concert for Les Paul in New York City; it was released in February 2011 on the CD Jeff Beck's Rock n' Roll Party Honoring Les Paul, and also performed on the DVD/Blu-ray release of the same concert, also released in February 2011.


== Interpolations ==
Wyclef Jean's "Masquerade" includes the melodic hook played on violin as the song closes.
"Symphony of the Nymph" (2012) by Ariel Pink's Haunted Graffiti features a melody from "Apache".
David Bowie borrowed part of the melody of Apache for the chorus of the song "How Does the Grass Grow?" from his 2013 album The Next Day.


== Soundtrack appearances ==
A 30-second edit of The Shadows version was used in an April 1988 UK TV advertisement for Tango fizzy drink.
An 80-second edit of The Shadows version was used in the 1989 feature film Scandal about the Profumo affair.
A 60-second portion of a re-mastered version by The Shadows was used in the 2012/13 UK TV advert for Mattessons 'Fridge Raiders' snack. It is known as the 'You must be Hank Marvin' advert, reflecting the rhyming slang term for 'starving'.
A version of "Apache" was used as the theme to the long-running television show Wild Chicago, which aired in Chicago on PBS.
Another 30 second sampled version by The Incredible Bongo Band is used in a TIAA commercial in 2018.
Various versions are used in commercials for Jardiance.
The song was going to be used in Joker (2019 film) but was cut.


== Minnesota Lynx ==
The Minnesota Lynx of the WNBA adopted "Apache" as the unofficial team anthem in 2007. Following victories, the team would dance to the song at center court. For the first home game of the team's first WNBA Finals appearance, the team brought in the Sugarhill Gang to perform the song at halftime.


== References ==


== External links ==
soul-sides.com: All Roads Lead to Apache (history of various versions of "Apache", including audio samples)