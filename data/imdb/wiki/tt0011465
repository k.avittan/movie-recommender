Spy Kids is an American spy adventure comedy family film series created by Robert Rodriguez. The main series follows the adventures of Carmen and Juni Cortez, two children who become involved in their parents' espionage. The rest of their family are spies as well, including their maternal grandparents, and estranged uncle Machete. The films tend to have a strong Latino theme, as Rodriguez is of Mexican descent.


== Background ==


=== Influences ===
Spy Kids was influenced by James Bond films. Director Robert Rodriguez says the first film was "Willy Wonka-meets-James Bond" and the second was the "Mysterious Island and James Bond mix". Technology in the films is portrayed as looking friendly, and a bit cartoonish.
The spy organization featured in the films is called the OSS. The initials seem to have been derived from the Office of Strategic Services, a former U.S. intelligence organization during World War II which later evolved into the CIA. Note there is a character named Donnagon Giggles, after William Joseph Donovan, the director of the real OSS. What the initials stand for in the Spy Kids universe is not specified on screen, but, according to one of the books, they stand for the Organization of Super Spies.


=== Themes ===
One of the chief themes of Spy Kids is the unity of family. The films also play with the idea of children having adult responsibilities, and how keeping secrets from family members can have a negative effect on relationships. The first film also deals extensively with sibling rivalry and the responsibility of older children. It also has a strong sense of Latino heritage.


=== Technical innovations ===
The other films were shot with High Definition digital video, parts of the third film using an anaglyphic process to create the 3-D effects. Audiences were given red/blue glasses with their ticket purchase in movie theatres. Four sets of these glasses were also included in the DVD release. The third film was also used as a test for a special Texas Instruments digital projector which is supposed to be able to project polarized 3D, a process that does not require the red-blue lenses, which was later reused for The Adventures of Sharkboy and Lavagirl in 3-D (2005).


== Films ==


=== Spy Kids (2001) ===

After retiring from espionage for ten years, Gregorio and Ingrid (Antonio Banderas and Carla Gugino) are pulled back into duty for their important assignment despite the fact they were out of practice, and were captured. Their two children, Carmen and Juni (Alexa Vega and Daryl Sabara), stay with their uncle Felix Gumm (Cheech Marin) and discover the truth of their parents' past, which they had neglected to tell them because they were afraid that if they knew, they would picture danger at every corner; and decide to rescue them. On their first mission, Carmen and Juni manage to bring around their estranged uncle, Isador "Machete" Cortez (Danny Trejo), a genius gadget inventor and Juni helps to redeem a tv show host named Fegan Floop (Alan Cumming). Together, Carmen and Juni thwart the plan of Floop's notorious second in-command Alexander Minion (Tony Shalhoub) to develop an army of androids resembling young children (including Carmen and Juni themselves) for a mastermind named Mr. Lisp (Robert Patrick) and his partner Ms. Gradenko (Teri Hatcher). The robots based on Carmen and Juni became part of Floop's show.


=== Spy Kids 2: The Island of Lost Dreams (2002) ===

As agents of the OSS, Carmen and Juni try to save the daughter (Taylor Momsen) of The President Of The United States (Christopher McDonald) while facing a particularly hard competition with Gary and Gerti Giggles (Matt O'Leary and Emily Osment), the two children of a double-dealing agent Donnagon Giggles (Mike Judge), whom Carmen and Juni helped to rescue them from the first film. Juni gets fired from the OSS after fighting with Gary over a smaller version of the transmooker, a device that can shut off all electronic devices even though it was Gary who started the fight. Juni loses his spot for the best spy kid of the year award, while Donnagon plans to steal the transmooker to take over the world. On their second mission, Carmen and Juni follow the trail to the mysterious island of Leeke Leeke which is home to Romero (Steve Buscemi), an eccentric scientist who attempted to create genetically-miniaturised animals, but instead ended up with his island inhabited by mutant monsters. Eventually, Donnagon is fired and Gary is suspended, and the transmooker is destroyed. Juni is offered his job back, but in order to take a break from the OSS, he retires to start his own private eye agency.


=== Spy Kids 3-D: Game Over (2003) ===

After retiring from the OSS, Juni is thrust back into service when an evil mastermind named Sebastian "The Toymaker" (Sylvester Stallone) creates a fictional video game called Game Over, which hypnotizes its users. Carmen was sent on a mission to disable the game, but disappeared on Level 4. With the help of his maternal grandfather, Valentin Avellan (Ricardo Montalban), who uses a wheelchair, Juni is sent after Carmen and helps her to disable the game in order to save the world. It is revealed that Sebastian was the one who disabled Valentin in the first place. Instead of avenging his former partner, Valentin forgives Sebastian who is redeemed.


=== Spy Kids: All the Time in the World (2011) ===

The OSS has become the world's top spy agency, while the Spy Kids department has become defunct. A retired spy Marissa (Jessica Alba) is thrown back into the action along with her two stepchildren, Rebecca and Cecil (Rowan Blanchard and Mason Cook), when a maniacal Timekeeper (Jeremy Piven) attempts to take over the world. In order to save the world, Rebecca and Cecil must team up with Marissa.


== Television ==


=== Spy Kids: Mission Critical (2018) ===

On June 16, 2016, Netflix announced an animated Spy Kids television show entitled Spy Kids: Mission Critical with The Weinstein Company. The first and second seasons will both consist of 10 episodes and is produced by Mainframe Studios.The series was announced by Mike Fleming Jr and created by Michael Hefferon and Sean Jara on May 6, 2015, with the villains being described “as colorful” as the protagonists, and the series is said to contain “as much comedy as wish fulfillment.” FM DeMarco is the head writer of the series. WOW! Unlimited Media Inc.'s Vancouver-based Mainframe Studios, the TV division of Rainmaker Entertainment, partnered with the Weinstein Company to produce the series, with the series being described as a "multi-season commitment". Robert Rodriguez serves as executive producers due to Bob Weinstein dropping out and Harvey Weinstein getting fired from TWC. It premiered on Netflix worldwide on April 20, 2018.


== Cast and crew ==


=== Principal cast ===

List indicator(s)
A dark grey cell indicates the character did not appear.


=== Crew ===


== Reception ==


=== Box office performance ===
The first film was a surprise hit, opening with $26.5 million and grossing a total of $112.7 million USD in North America and $35.2 million over-seas. The second film had a disappointing but still strong opening weekend of $16.7 million and a total of $25 million since its Wednesday launch. Overall, it grossed $85.8 million in North America and $33.8 million overseas. The third film opened with a surprising $33.4 million. In the end, it grossed $111 million in North America and $86 million internationally. The fourth film grossed $85 million at the box office. Altogether, the Spy Kids series grossed over $535 million worldwide.


=== Critical and public response ===
The first film received positive reviews from critics and audiences alike, becoming the most critically acclaimed film of the series. The second film, again achieved positive reviews from critics and audiences. The third film in series received more mixed reviews from critics and audiences. It was criticized for the poor 3D visual effects. The fourth and final film, received mostly negative reviews from critics and audiences. The film was panned upon release, being criticized for the plot, acting, visual effects and the 4D Aroma-Scope effect in the film.


== Related film series ==
Isador "Machete" Cortez (played by Danny Trejo), who appeared in all four Spy Kids films as a supporting character, additionally had a series of two stand-alone films: Machete and Machete Kills, also written and directed by Robert Rodriguez. However, the Machete films share little in common with the Spy Kids films thematically and are not considered direct spin-offs, the first film instead being an adult-oriented action exploitation film, with the second film introducing science fiction elements; both films additionally share several cast members and characters with the Spy Kids films. The idea for a Machete film came from a fake trailer promoting the Grindhouse double-feature by Rodriguez and Quentin Tarantino. Trejo and Rodriguez have made two conflicting statements regarding its canonicity to the Spy Kids films. Trejo stated that the films depict "what Uncle Machete does when he’s not taking care of the kids", while Rodriguez stated in a Reddit AMA that they are alternate universes. Regardless, Rodriguez has stated he was prompted by an incident on the set of the first Machete film to start envisioning a fourth film in the main Spy Kids series, casting Jessica Alba as Machete's sister Marissa, a different character to the one she portrayed in Machete, with Trejo additionally reprising his role alongside her.


== Home media ==
September 18, 2001 (Spy Kids) on DVD by Buena Vista Home Entertainment
February 18, 2003 (Spy Kids 2: The Island of Lost Dreams) on DVD by Buena Vista Home Entertainment
February 24, 2004 (Spy Kids 3D: Game Over) on DVD by Buena Vista Home Entertainment
August 2, 2011 (Spy Kids, Spy Kids 2: The Island of Lost Dreams, and Spy Kids 3-D: Game Over) on DVD and Blu-ray Disc by Lionsgate (However, all 3 DVDs are still the original Buena Vista Home Entertainment copies.)
November 22, 2011 (Spy Kids: All the Time in the World) on DVD and Blu-ray by Anchor Bay Entertainment
December 4, 2012 (Spy Kids 3-D: Game Over, The Adventures of Sharkboy and Lavagirl in 3-D 3D Double Feature) on Blu-ray 3D Disc by Lionsgate


== Notes ==


== References ==