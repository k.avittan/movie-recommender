Alexander Amini (born April 29, 1995) is an American scientist from Dublin, Ireland, currently studying at the Massachusetts Institute of Technology (MIT) in America. He is the first prize winner of the 23rd European Union Contest for Young Scientists and the 47th BT Young Scientist and Technology Exhibition in 2011 at the age of fifteen for his project entitled: “Tennis Sensor Data Analysis: An Automated System for Macro Motion Refinement”, in which he combined his passions for computer science, mathematics, and tennis.


== Early life ==
Amini was born in Fort Worth, Texas on 29 April 1995 and grew up in Yorktown Heights, New York. He began to play tennis at the age of five, taught by his father. While living in New York, Amini attended Yorktown High School, where he took his first computer programming classes, learning to program in Java, and Microsoft Visual Basic. In his spare time, Amini also taught himself many other languages including Python, BASH, AWK, and sed.


== BT Young Scientist and Technologist ==
After Amini completed the 9th grade in America, his family immigrated to Dublin, Ireland. On August 28, 2010, Amini was enrolled at Castleknock College and began Fourth Year.Amini entered the 47th BT Young Scientist and Technology Exhibition with his project entitled "Tennis Sensor Data Analysis." On January 14, 2011 Amini was announced the BT Young Scientist and Technologist of the Year 2011 by Tánaiste and Minister for Education and Skills, Ms. Mary Coughlan, T.D and Graham Sutherland, CEO, BT Ireland . He was presented with a cheque for €5,000, a Waterford Crystal trophy and the opportunity to represent Ireland at the 23rd European Union Contest for Young Scientists taking place in Helsinki, Finland in September 2011.A quote from one of the interviewers of Alexander Amini during the competition:
"We interviewed Alexander Amini from Castleknock College on Wednesday, and I was struck by his clear intelligence, but also the comfort with which he handled the issues he was treating. The ingenuity of his project is undoubted, but his ability to manipulate such advanced technologies is outstanding. He was a deserved winner and will indubitably go on to bigger things."


== European Union Young Scientist ==
After winning the top prize at the national competition, Amini was offered the opportunity to represent Ireland in the European Union Contest for Young Scientists. Amini competed against 87 projects and a total of 38 different countries from all around the world during the competition hosted in Helsinki, Finland during September 2011. Again, Amini won the top prize with his project entitled "Tennis Sensor Data Analysis: An Automated System for Macro Motion Refinement".Amini received €7000, and honorary expense paid trips to the Nobel Prize ceremonies in Stockholm, Sweden and the London International Youth Science Forum.


== Tennis Sensor Data Analysis ==
Amini's project, entitled: Tennis Sensor Data Analysis, was focused on sensors capable of capturing audio, video, and inertial data (such as accelerometers, gyroscopes, and magnetometers). In his study, he applied such inertial sensors onto the bodies of tennis athletes, collected and analyzed gigabytes of tennis sensor data. He discovered a technique for automatically distinguishing between 15 different tennis stroke types using only inertial data, with accuracy averaging over 98%. Amini tested his algorithms after collecting data at the elite Sánchez-Casal Tennis Academy, and created a real-time computer software system that employs the algorithms he discovered. His algorithms analyze sports performance data as it is generated and provide unbiased and precise motion refinement feedback to the athlete or coach. Amini's findings are relevant to a wide variety of motion assessment scenarios in sports, physical therapy, and emergency response.


== MIT Deep Learning ==
Amini is lead organizer and instructor for the MIT's popular artificial intelligence and deep learning course, MIT 6.S191: Introduction to Deep Learning.


== References ==


== External links ==
OFFICIAL WEBSITE