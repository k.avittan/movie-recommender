Mutiny is a revolt among a group of people (typically of a military or of a crew) to oppose, change, or overthrow an organization to which they were previously loyal. The term is commonly used for a rebellion among members of the military against an internal force, but it can also sometimes mean any type of rebellion against any force. Mutiny does not necessarily need to refer to a Military Force and can describe a political, economic, or Power structure in which there's a change of power.
During the Age of Discovery, mutiny particularly meant open rebellion against a ship's captain. This occurred, for example, during Ferdinand Magellan's journeys around the world, resulting in the killing of one mutineer, the execution of another, and the marooning of others; on Henry Hudson's Discovery resulting in Hudson and others being set adrift in a boat; and the notorious mutiny on the Bounty.


== Penalty ==
Those convicted of mutiny often faced capital punishment.


=== United Kingdom ===
Until 1689, mutiny was regulated in England by Articles of War instituted by the monarch and effective only in a period of war. In 1689, the first Mutiny Act was passed which passed the responsibility to enforce discipline within the military to Parliament. The Mutiny Act, altered in 1803, and the Articles of War defined the nature and punishment of mutiny until the latter were replaced by the Army Discipline and Regulation Act in 1879. This, in turn, was replaced by the Army Act in 1881.
Today the Army Act 1955 defines mutiny as follows:
Mutiny means a combination between two or more persons subject to service law, or between persons two at least of whom are subject to service law—
(a) to overthrow or resist lawful authority in Her Majesty's forces or any forces co-operating therewith or in any part of any of the said forces,
(b) to disobey such authority in such circumstances as to make the disobedience subversive of discipline, or with the object of avoiding any duty or service against, or in connection with operations against, the enemy, or
(c) to impede the performance of any duty or service in Her Majesty's forces or in any forces co-operating therewith or in any part of any of the said forces.
The same definition applies in the Royal Navy and Royal Air Force.
The military law of England in early times existed, like the forces to which it applied, in a period of war only. Troops were raised for a particular service and were disbanded upon the cessation of hostilities. The crown, by prerogative, made laws known as Articles of War for the government and discipline of the troops while thus embodied and serving. Except for the punishment of desertion, which was made a felony by statute in the reign of Henry VI, these ordinances or Articles of War remained almost the sole authority for the enforcement of discipline until 1689 when the first Mutiny Act was passed and the military forces of the crown were brought under the direct control of parliament. Even the Parliamentary forces in the time of Charles I and Oliver Cromwell were governed, not by an act of the legislature, but by articles of war similar to those issued by the king and authorized by an ordinance of the Lords and Commons exercising in that respect the sovereign prerogative. This power of law-making by prerogative was however held to be applicable during a state of actual war only, and attempts to exercise it in time of peace were ineffectual. Subject to this limitation, it existed for considerably more than a century after the passing of the first Mutiny Act.
From 1689 to 1803, although in peacetime the Mutiny Act was occasionally suffered to expire, a statutory power was given to the crown to make Articles of War to operate in the colonies and elsewhere beyond the seas in the same manner as those made by prerogative operated in time of war.
In 1715, in consequence of the rebellion, this power was created in respect of the forces in the kingdom but apart from and in no respect affected the principle acknowledged all this time that the crown of its mere prerogative could make laws for the government of the army in foreign countries in time of war.
The Mutiny Act of 1803 effected a great constitutional change in this respect: the power of the crown to make any Articles of War became altogether statutory, and the prerogative merged in the act of parliament. The Mutiny Act 1873 was passed in this manner.
Such matters remained until 1879 when the last Mutiny Act was passed and the last Articles of War were promulgated. The Mutiny Act legislated for offences in respect of which death or penal servitude could be awarded, and the Articles of War, while repeating those provisions of the act, constituted the direct authority for dealing with offences for which imprisonment was the maximum punishment as well as with many matters relating to trial and procedure.
The act and the articles were found not to harmonize in all respects. Their general arrangement was faulty, and their language sometimes obscure. In 1869, a royal commission recommended that both should be recast in a simple and intelligible shape. In 1878, a committee of the House of Commons endorsed this view and made recommendations as to how the task should be performed. In 1879, passed into law a measure consolidating in one act both the Mutiny Act and the Articles of War, and amending their provisions in certain important respects. This measure was called the Army Discipline and Regulation Act 1879.
After one or two years experience finding room for improvement, it was superseded by the Army Act 1881, which hence formed the foundation and the main portion of the military law of England, containing a proviso saving the right of the crown to make Articles of War, but in such a manner as to render the power in effect a nullity by enacting that no crime made punishable by the act shall be otherwise punishable by such articles. As the punishment of every conceivable offence was provided, any articles made under the act could be no more than an empty formality having no practical effect.
Thus the history of English military law up to 1879 may be divided into three periods, each having a distinct constitutional aspect: (I) prior to 1689, the army, being regarded as so many personal retainers of the sovereign rather than servants of the state, was mainly governed by the will of the sovereign; (2) between 1689 and 1803, the army, being recognised as a permanent force, was governed within the realm by statute and without it by the prerogative of the crown and (3) from 1803 to 1879, it was governed either directly by statute or by the sovereign under an authority derived from and defined and limited by statute. Although in 1879 the power of making Articles of War became in effect inoperative, the sovereign was empowered to make rules of procedure, having the force of law, to regulate the administration of the act in many matters formerly dealt with by the Articles of War. These rules, however, must not be inconsistent with the provisions of the Army Act itself, and must be laid before parliament immediately after they are made. Thus in 1879 the government and discipline of the army became for the first time completely subject either to the direct action or the close supervision of parliament.
A further notable change took place at the same time. The Mutiny Act had been brought into force on each occasion for one year only, in compliance with the constitutional theory:

that the maintenance of a standing army in time of peace, unless with the consent of parliament, is against law. Each session therefore the text of the act had to be passed through both Houses clause by clause and line by line. The Army Act, on the other hand, is a fixed permanent code. But constitutional traditions are fully respected by the insertion in it of a section providing that it shall come into force only by virtue of an annual act of parliament. This annual act recites the illegality of a standing army in time of peace unless with the consent of parliament, and the necessity nevertheless of maintaining a certain number of land forces (exclusive of those serving in India) and a body of royal marine forces on shore, and of keeping them in exact discipline, and it brings into force the Army Act for one year.


==== Sentence ====
Until 1998 mutiny and another offence of failing to suppress or report a mutiny were each punishable with death. Section 21(5) of the Human Rights Act 1998 completely abolished the death penalty in the United Kingdom. (Prior to this, the death penalty had already been abolished for murder, but it had remained in force for certain military offences and treason, although no executions had been carried out for several decades.) This provision was not required by the European Convention on Human Rights, since Protocol 6 of the Convention permitted the death penalty in time of war, and Protocol 13, which prohibits the death penalty for all circumstances, did not then exist. The UK government introduced section 21(5) as a late amendment in response to parliamentary pressure.


=== United States ===
The United States' Uniform Code of Military Justice defines mutiny thus:

Art. 94. (§ 894.) 2004 Mutiny or Sedition.(a) Any person subject to this code (chapter) who—
(1) with intent to usurp or override lawful military authority, refuses, in concert with any other person, to obey orders or otherwise do his duty or creates any violence or disturbance is guilty of mutiny;
(2) with intent to cause the overthrow or destruction of lawful civil authority, creates, in concert with any other person, revolt, violence, or other disturbance against that authority is guilty of sedition;
(3) fails to do his utmost to prevent and suppress a mutiny or sedition being committed in his presence, or fails to take all reasonable means to inform his superior commissioned officer or commanding officer of a mutiny or sedition which he knows or has reason to believe is taking place, is guilty of a failure to suppress or report a mutiny or sedition.
(b) A person who is found guilty of attempted mutiny, mutiny, sedition, or failure to suppress or report a mutiny or sedition shall be punished by death or such other punishment as a court-martial may direct.
U.S. military law requires obedience only to lawful orders. Disobedience to unlawful orders (see Superior orders) is the obligation of every member of the U.S. military, a principle established by the Nuremberg and Tokyo Trials following World War II and reaffirmed in the aftermath of the My Lai Massacre during the Vietnam War. However, a U.S. soldier who disobeys an order after deeming it unlawful will almost certainly be court-martialed to determine whether the disobedience was proper. In addition, simple refusal to obey is not mutiny, which requires collaboration or conspiracy to disobedience.


== Famous mutinies ==


=== 16th century ===
Sack of Antwerp, one of the many mutinies in the Spanish Army of Flanders during the Eighty Years' War; this mutiny caused the provinces of the Habsburg Netherlands to temporarily unite in rebellion against Philip II of Spain and sign the Pacification of Ghent.
Sack of Rome (1527), military event carried out by the mutinous troops of Charles V, Holy Roman Emperor.


=== 17th century ===
Discovery mutiny in 1611 during the 4th voyage of Henry Hudson, after having been trapped in pack ice over the winter, his desire to continue incited the crew to casting him and 8 others adrift.
Batavia was a ship of the Dutch East India Company (VOC), built in 1628 in Amsterdam, which suffered both mutiny and shipwreck during her maiden voyage.
Second English Civil War
Corkbush Field mutiny (1647)
Banbury mutiny (1649)
Bishopsgate mutiny (1649)


=== 18th century ===

A failed 1787 mutiny aboard the Middlesex occurred two weeks before HMS Bounty's final England departure, which included the lead mutineer of HMS Bounty Fletcher Christian's older brother Charles.
Mutiny aboard HMS Bounty, a mutiny aboard a British Royal Navy ship in 1789 that has been made famous by several books and films.
Quibéron mutinies were major mutinies in the French fleet in 1793.
HMS Hermione was a 32-gun fifth-rate frigate of the British Royal Navy. While operating in the Caribbean in 1797 a portion of the crew mutinied, killing the captain, eight other officers, two midshipmen and a clerk before surrendering the ship to the Spanish authorities. The mutiny was the bloodiest recorded in the history of the Royal Navy.
Spithead and Nore mutinies were two major mutinies by sailors of the British Royal Navy in 1797.
Vlieter Incident was a mutiny of a squadron of the fleet of the Batavian Republic which caused it to be surrendered to the British without a fight in 1799 at the start of the Anglo-Russian invasion of Holland.


=== 19th century ===
Vellore Mutiny, outbreak against the British East India Company on 10 July 1806, by sepoys forming part of the garrison of a fortress and palace complex at Vellore (now in Tamil Nadu state, southern India).
The Froberg mutiny by the Froberg Regiment in Fort Ricasoli, Malta in 1807. The mutiny was suppressed and 30 men were executed.
The US whaler Globe mutiny of 1824.
Barrackpore Mutiny, (2 November 1824), incident during the First Anglo-Burmese War (1824–26), generally regarded as a dress rehearsal for the Indian Mutiny of 1857 because of its similar combination of Indian grievances against the British.
La Amistad, in 1839. A group of captured African slaves being transported in Cuba mutinied against the crew, killing the captain.
The brig USS Somers had a mutiny plotted onboard on her first voyage in 1842. Three men were accused of conspiring to commit mutiny, and were hanged.
The Indian rebellion of 1857 was a period of armed uprising in India against British colonial power, and was popularly remembered in Britain as the Indian Mutiny or Sepoy Mutiny. It is remembered in India as the First War of Independence.
The Sharon, a New England whaler, was subject to multiple mass desertions, mutinies and the murder and dismemberment of a cruel (and from the record, sociopathic) captain by four Polynesians who had been pressed into service on the Sharon.
In 1857 on the whaleship Junior, Cyrus Plummer and several accomplices engineered a mutiny that resulted in the murder of Captain Archibald Mellen and Third Mate John Smith. The mutineers were captured and found guilty in the fall of 1858. Plummer was sentenced to be hanged and his accomplices received life sentences. The story made national and international news and Plummer was able to garner a stay of execution from President James Buchanan and was ultimately pardoned by Ulysses S. Grant.
The Cavite Mutiny of 1872 in the Philippines.
The Brazilian Naval Revolt was the occasion of two mutinies in 1893 and 1894.


=== 20th century ===
Mutiny aboard the Russian battleship Potemkin, a rebellion of the crew against their officers in June 1905 during the Russian Revolution of 1905. It was made famous by the film The Battleship Potemkin.
The Revolta da Chibata ("Revolt of the Lash") was a Brazilian naval mutiny of 1910, where Afro-Brazilian crewmen rose up against oppressive white officers who frequently beat them. Their goal was to have their living conditions improved and the chibata (whips or lashes) banned from the navy.
Guaymas Mutiny On 22 February 1914, Mexican Navy sailors under Lieutenant Hilario Rodríguez Malpica seized control of gunboat Tampico off Guaymas, Mexico. This event led to a naval campaign off Topolobampo during the Mexican Revolution.
Curragh Incident, also known as the Curragh Mutiny of 20 July 1914 occurred in the Curragh, Ireland, where British officers threatened to resign rather than enforce the Home Rule Act 1914.
Etaples Mutiny by British troops, 1917
French Army mutinies in 1917. The failure of the Nivelle Offensive in April and May 1917 resulted in widespread mutiny in many units of the French Army.
Wilhelmshaven mutiny broke out in the German High Seas Fleet on 29 October 1918. The mutiny was one of the factors leading to the end of the First World War, to the collapse of the monarchy and to the establishment of the Weimar Republic.
Kiel mutiny Major revolt by sailors on 3 November 1918 in response to further arrests of suspected Wilhelmshaven ringleaders, eventually sparked the German Revolution of 1918-1919 and the end of the First World War.
Black Sea mutiny (1919) by crews aboard the French dreadnoughts Jean Bart and France, sent to assist the White Russians in the Russian Civil War. The ringleaders (including André Marty and Charles Tillon) received long prison sentences.
The 1920 mutiny of the mainly Irish unit of Connaught Rangers in the British Army against martial law being imposed and brutally enforced by the Black and Tans in Ireland during the Irish War of Independence. The leader, Private James Daly, became the last member of the British Armed Forces to be executed for mutiny when he was shot by firing squad on 2 November 1920.
Kronstadt rebellion, an unsuccessful uprising of Russian sailors, led by Stepan Petrichenko, against the government of the early Russian SFSR in the first weeks of March in 1921. It proved to be the last large rebellion against Bolshevik rule.
Irish Army Mutiny, a crisis in March 1924 provoked by a proposed reduction in army numbers in the immediate post-Civil War period.
Invergordon Mutiny, an industrial action by around a thousand sailors in the British Atlantic Fleet, that took place on 15–16 September 1931. For two days, ships of the Royal Navy at Invergordon were in open mutiny, in one of the few military strikes in British history.
Mutiny aboard the Dutch warship the De Zeven Provinciën as a result of salary cuts in early February 1933.
1936 Naval Revolt in Portugal, also known as the Mutiny on the Tagus ships. Sailors aboard two Portuguese ships imprisoned their officers and attempted to sail out into the open sea. Coastal artillery disabled both ships and the Estado Novo shortly thereafter founded the Legião Portuguesa.
Cocos Islands Mutiny, a failed mutiny by Sri Lankan servicemen on the then-British Cocos (Keeling) Islands during the Second World War.
Battle of Bamber Bridge on 24–25 June 1943, a racially motivated mutiny by black soldiers in a segregated truck unit stationed in Bamber Bridge, Lancashire, United Kingdom.
Port Chicago mutiny on August 9, 1944, three weeks after the Port Chicago disaster. 258 out of the 320 African-American sailors in the ordnance battalion refused to load any ammunition. See also African-American mutinies in the United States armed forces.
Terrace mutiny, a mutiny by French-Canadian soldiers in Terrace, British Columbia, in November 1944.


==== After World War II ====
Post–World War II demobilization strikes occurred within Allied military forces stationed across the Middle East, India and South-East Asia in the months and years following the Second World War.
The Royal Indian Navy Mutiny encompasses a total strike and subsequent mutiny by the Indian sailors of the Royal Indian Navy on board ship and shore establishments at Bombay Harbour on 18 February 1946.
SS Columbia Eagle incident occurred on 14 March 1970 during the Vietnam War when sailors aboard an American merchant ship mutinied and hijacked the ship to Cambodia.
East Bengal Regiment occurred when East Bengal Regiment high rank officer refused order from Pakistan army command in east Pakistan present day Bangladesh and join the Bangladesh freedom movement.
Unit 684 Mutiny occurred when members of South Korean black ops Unit 684 mutinied for unclear reasons.
The Storozhevoy Mutiny occurred on 9 November 1975 in Riga, Latvian SSR, Soviet Union. The political officer locked up the Soviet Navy captain and sailed the ship toward Leningrad.
The Velos mutiny On 23 May 1973, the captain of Velos, refused to return to Greece after a NATO exercise.
Following Operation Blue Star against Sikh militants holed in the Golden Temple in the Sikh holy city of Amritsar, many soldiers and officers of Indian Army's Sikh Regiment mutinied or resigned.


=== 21st century ===
2003 Oakwood mutiny – A group of 321 officers and personnel of the Philippines Armed Forces took over the Oakwood Premier Ayala Center serviced apartment tower in Makati City to show the Filipino people the alleged corruption of Pres. Gloria Macapagal-Arroyo.
2003 Fort Bonifacio Crisis – Members of the Philippine Marines staged a protest over the removal of their Commandant Maj. Gen. Renato Miranda.
2009 Bangladesh Rifles revolt – A group of Bangladesh border guards revolted, demanding equal rights to the regular army and killed several of their officers.
2011 Mutiny on Lurongyu 2682, a Chinese fishing trawler in the South Pacific. After a month-long killings, 11 of the 33 crew returned to China.
2013 1st Battalion Yorkshire Regiment, British Army Sixteen soldiers were jailed after a court martial for staging a 'sit-in' protest against their Captain and Colour Sergeant
2014 Nigerian Army: A total of 54 soldiers were sentenced to death by firing squad by a court martial in two separate trials, after they had refused to fight to recapture a town that had been captured by Boko Haram insurgents. The sentences are subject to the approval of senior officers.
2020 Malian mutiny


== See also ==
Coup d'état
List of revolutions and rebellions


== References ==


== Further reading ==
 Chisholm, Hugh, ed. (1911). "Mutiny". Encyclopædia Britannica (11th ed.). Cambridge University Press.
Guttridge, Leonard F (1992). Mutiny: A History of Naval Insurrection. Annapolis, Maryland: United States Naval Institute Press. ISBN 0-87021-281-8.
Bell, Christopher M; Elleman, Bruce A, eds. (2003). Naval Mutinies of the Twentieth Century: An International Perspective. Portland, Oregon: Frank Cass Publishers. ISBN 0-7146-8468-6. OCLC 464313205.
Pfaff, Steven and Michael Hechter. 2020. The Genesis of Rebellion: Governance, Grievance, and Mutiny in the Age of Sail. Cambridge University Press.