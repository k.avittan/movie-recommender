Clay is a type of fine-grained natural soil material that contains hydrous aluminium phyllosilicates (clay minerals) that develops plasticity when wet. Geologic clay deposits are mostly composed of phyllosilicate minerals containing variable amounts of water trapped in the mineral structure. Clays are plastic due to particle size and geometry as well as water content, and become hard, brittle and non–plastic upon drying or firing. Depending on the soil's content in which it is found, clay can appear in various colours from white to dull grey or brown to deep orange-red.

Although many naturally occurring deposits include both silts and clay, clays are distinguished from other fine-grained soils by differences in size and mineralogy. Silts, which are fine-grained soils that do not include clay minerals, tend to have larger particle sizes than clays. There is, however, some overlap in particle size and other physical properties.  The distinction between silt and clay varies by discipline. Geologists and soil scientists usually consider the separation to occur at a particle size of 2 μm (clays being finer than silts), sedimentologists often use 4–5 μm, and colloid chemists use 1 μm. Geotechnical engineers distinguish between silts and clays based on the plasticity properties of the soil, as measured by the soils' Atterberg limits. ISO 14688 grades clay particles as being smaller than 2 μm and silt particles as being larger. Mixtures of sand, silt and less than 40% clay are called loam. 


== Formation ==

Clay minerals typically form over long periods as a result of the gradual chemical weathering of rocks, usually silicate-bearing, by low concentrations of carbonic acid and other diluted solvents. These solvents, usually acidic, migrate through the weathering rock after leaching through upper weathered layers. In addition to the weathering process, some clay minerals are formed through hydrothermal activity. There are two types of clay deposits: primary and secondary. Primary clays form as residual deposits in soil and remain at the site of formation. Secondary clays are clays that have been transported from their original location by water erosion and deposited in a new sedimentary deposit. Clay deposits are typically associated with very low energy depositional environments such as large lakes and marine basins.


== Grouping ==
Depending on the academic source, there are three or four main groups of clays: kaolinite, montmorillonite-smectite, illite, and chlorite. Chlorites are not always considered to be a clay, sometimes being classified as a separate group within the phyllosilicates. There are approximately 30 different types of "pure" clays in these categories, but most "natural" clay deposits are mixtures of these different types, along with other weathered minerals.
Varve (or varved clay) is clay with visible annual layers, which are formed by seasonal deposition of those layers and are marked by differences in erosion and organic content. This type of deposit is common in former glacial lakes. When fine sediments are delivered into the calm waters of these glacial lake basins away from the shoreline, they settle to the lake bed. The resulting seasonal layering is preserved in an even distribution of clay sediment banding.Quick clay is a unique type of marine clay indigenous to the glaciated terrains of Norway, Canada, Northern Ireland, and Sweden. It is a highly sensitive clay, prone to liquefaction, which has been involved in several deadly landslides.


== Identification ==


=== X-ray diffraction ===

Powder X-ray diffraction can be used to identify clays.


=== Chemical ===
The physical and reactive chemical properties can be used to help elucidate the composition of clays.


== Historical and modern uses ==

Clays exhibit plasticity when mixed with water in certain proportions. However, when dry, clay becomes firm, and when fired in a kiln, permanent physical and chemical changes occur. These changes convert the clay into a ceramic material. Because of these properties, clay is used for making pottery, both utilitarian and decorative, and construction products, such as bricks, walls, and floor tiles. Different types of clay, when used with different minerals and firing conditions, are used to produce earthenware, stoneware, and porcelain. Prehistoric humans discovered the useful properties of clay. Some of the earliest pottery shards recovered are from central Honshu, Japan. They are associated with the Jōmon culture, and recovered deposits have been dated to around 14,000 BC. Cooking pots, art objects, dishware, smoking pipes, and even musical instruments such as the ocarina can all be shaped from clay before being fired. 
Clay tablets were the first known writing medium. Scribes wrote by inscribing them with cuneiform script using a blunt reed called a stylus. Purpose-made clay balls were used as sling ammunition. Clay is used in many industrial processes, such as paper making, cement production, and chemical filtering. Until the late 20th century, bentonite clay was widely used as a mold binder in the manufacture of sand castings.
Clay, being relatively impermeable to water, is also used where natural seals are needed, such as in the cores of dams, or as a barrier in landfills against toxic seepage (lining the landfill, preferably in combination with geotextiles). Studies in the early 21st century have investigated clay's absorption capacities in various applications, such as the removal of heavy metals from waste water and air purification.


=== Medical use ===
Traditional uses of clay as medicine goes back to prehistoric times. An example is Armenian bole, which is used to soothe an upset stomach. Some animals such as parrots and pigs ingest clay for similar reasons.   Kaolin clay and attapulgite have been used as anti-diarrheal medicines.


=== As a building material ===

Clay as the defining ingredient of loam is one of the oldest building materials on Earth, among other ancient, naturally-occurring geologic materials such as stone and organic materials like wood. Between one-half and two-thirds of the world's population, in both traditional societies as well as developed countries, still live or work in buildings made with clay, often baked into brick, as an essential part of its load-bearing structure. Also a primary ingredient in many natural building techniques, clay is used to create adobe, cob, cordwood, and rammed earth structures and building elements such as wattle and daub, clay plaster, clay render case, clay floors and clay paints and ceramic building material. Clay was used as a mortar in brick chimneys and stone walls where protected from water.


== See also ==


== Notes ==


== References ==
Guggenheim, Stephen; Martin, R. T. (1995), "Definition of clay and clay mineral: Journal report of the AIPEA nomenclature and CMS nomenclature committees", Clays and Clay Minerals, 43 (2): 255–256, Bibcode:1995CCM....43..255G, doi:10.1346/CCMN.1995.0430213
Clay mineral nomenclature American Mineralogist.
Ehlers, Ernest G. and Blatt, Harvey (1982). 'Petrology, Igneous, Sedimentary, and Metamorphic' San Francisco: W.H. Freeman and Company.  ISBN 0-7167-1279-2.
Hillier S. (2003) "Clay Mineralogy." pp 139–142 In Middleton G.V., Church M.J., Coniglio M., Hardie L.A. and Longstaffe F.J. (Editors) Encyclopedia of Sediments and Sedimentary Rocks. Kluwer Academic Publishers, Dordrecht. clay is used to make multiple things.


== External links ==
The Clay Minerals Group of the Mineralogical Society
Information about clays used in the UK pottery industry
The Clay Minerals Society
Organic Matter in Clays