The Way of the World is a play written by the English playwright William Congreve. It premiered in early March 1700 in the theatre in Lincoln's Inn Fields in London. It is widely regarded as one of the best Restoration comedies and is still occasionally performed.  Initially, however, the play struck many audience members as continuing the immorality of the previous decades, and was not well received.


== Characters ==
The play is centred on the two lovers Mirabell and Millamant (originally played by John Verbruggen and Anne Bracegirdle). In order for them to marry and receive Millamant's full dowry, Mirabell must receive the blessing of Millamant's aunt, Lady Wishfort. Unfortunately, Lady Wishfort is a very bitter lady who despises Mirabell and wants her own nephew, Sir Wilfull, to wed Millamant.  Meanwhile, Lady Wishfort, a widow, wants to marry again and has her eyes on an uncle of Mirabell's, the wealthy Sir Rowland.
Another character, Fainall, is having a secret affair with Mrs. Marwood, a friend of Fainall's wife.  Mrs. Fainall, who is Lady Wishfort's daughter, herself once had an affair with Mirabell and remains his friend. In the meantime, Mirabell's servant Waitwell is married to Foible, Lady Wishfort's servant. Waitwell pretends to be Sir Rowland and, on Mirabell's command, tries to trick Lady Wishfort into a false engagement.


== Plot ==
Act 1 is set in a chocolate house where Mirabell and Fainall have just finished playing cards. A footman comes and tells Mirabell that Waitwell (Mirabell's male servant) and Foible (Lady Wishfort's female servant) were married that morning. Mirabell tells Fainall about his love of Millamant and is encouraged to marry her. Witwoud and Petulant appear and Mirabell is informed that should Lady Wishfort marry, he will lose £6000 of Millamant's inheritance. He will only get this money if he can get Lady Wishfort's consent to his and Millamant's marriage.
Act 2 is set in St. James’ Park. Mrs. Fainall and Mrs. Marwood are discussing their hatred of men. Fainall appears and accuses Mrs. Marwood (with whom he is having an affair) of loving Mirabell (which she does). Meanwhile, Mrs. Fainall (Mirabell's former lover) tells Mirabell that she hates her husband, and they begin to plot to deceive Lady Wishfort into giving her consent to the marriage. Millamant appears in the park and, angry about the previous night (when Mirabell was confronted by Lady Wishfort), she tells Mirabell of her displeasure in his plan, which she only has a vague idea about. After she leaves, the newly wed servants appear and Mirabell reminds them of their roles in the plan.
Acts 3, 4 and 5 are all set in the home of Lady Wishfort. We are introduced to Lady Wishfort who is encouraged by Foible to marry the supposed Sir Rowland – Mirabell's supposed uncle – so that Mirabell will lose his inheritance. Sir Rowland is, however, Waitwell in disguise, and the plan is to entangle Lady Wishfort in a marriage which cannot go ahead, because it would be bigamy, not to mention a social disgrace (Waitwell is only a serving man, Lady Wishfort an aristocrat). Mirabell will offer to help her out of the embarrassing situation if she consents to his marriage. Later, Mrs. Fainall discusses this plan with Foible, but this is overheard by Mrs. Marwood. She later tells the plan to Fainall, who decides that he will take his wife's money and go away with Mrs. Marwood.
Mirabell and Millamant, equally strong-willed, discuss in detail the conditions under which they would accept each other in marriage (otherwise known as the "proviso scene"), showing the depth of their feeling for each other. Mirabell finally proposes to Millamant and, with Mrs. Fainall's encouragement (almost consent, as Millamant knows of their previous relations), Millamant accepts. Mirabell leaves as Lady Wishfort arrives, and she lets it be known that she wants Millamant to marry her nephew, Sir Wilfull Witwoud, who has just arrived from the countryside. Lady Wishfort later gets a letter telling her about the Sir Rowland plot. Sir Rowland takes the letter and accuses Mirabell of trying to sabotage their wedding. Lady Wishfort agrees to let Sir Rowland bring a marriage contract that night.
By Act 5, Lady Wishfort has found out the plot, and Fainall has had Waitwell arrested. Mrs. Fainall tells Foible that her previous affair with Mirabell is now public knowledge. Lady Wishfort appears with Mrs. Marwood, whom she thanks for unveiling the plot. Fainall then appears and uses the information of Mrs. Fainall's previous affair with Mirabell and Millamant's contract to marry him to blackmail Lady Wishfort, telling her that she should never marry and that she is to transfer her fortune to him. Lady Wishfort offers Mirabell her consent to the marriage if he can save her fortune and honour. Mirabell calls on Waitwell who brings a contract from the time before the marriage of the Fainalls in which Mrs. Fainall gives all her property to Mirabell. This neutralises the blackmail attempts, after which Mirabell restores Mrs. Fainall's property to her possession and then is free to marry Millamant with the full £12000 inheritance.


== Epigraph of the 1700 edition ==
The epigraph found on the title page of the 1700 edition of The Way of the World contains two Latin quotations from Horace's Satires. In their wider contexts they read in English:

"It is worthwhile, for those of you who wish adulterers no success, to hear how much misfortune they suffer, and how often their pleasure is marred by pain and, though rarely achieved, even then fraught with danger."
"I have no fear in her company that a husband may rush back from the country, the door burst open, the dog bark, the house shake with the din, the woman, deathly pale, leap from her bed, her complicit maid shriek, she fearing for her limbs, her guilty mistress for her dowry and I for myself."The quotations offer a forewarning of the chaos to ensue from both infidelity and deception.


== Historical context ==
In 1700, the world of London theatre-going had changed significantly from the days of, for example, The Country Wife. Charles II was no longer on the throne, and the jubilant court that revelled in its licentiousness and opulence had been replaced by the far more dour and utilitarian Dutch-inspired court of William of Orange. His wife, Mary II, was, long before her death, a retiring person who did not appear much in public. William himself was a military king who was reported to be hostile to drama. The political instabilities that had been beneath the surface of many Restoration comedies were still present, but with a different side seeming victorious.
One of the features of a Restoration comedy is the opposition of the witty and courtly (and Cavalier) rake and the dull-witted man of business or the country bumpkin, who is understood to be not only unsophisticated but often (as, for instance, in the very popular plays of Aphra Behn in the 1670s) either Puritan or another form of dissenter. In 1685, the courtly and Cavalier side was in power, and Restoration comedies belittled the bland and foolish losers of the Restoration. However, by 1700, the other side was ascendant. Therefore, The Way of the World's recreation of the older Restoration comedy's patterns is only one of the things that made the play unusual.
The 1688 revolution concerning the overthrow of James II created a new set of social codes primarily amongst the bourgeoisie. The new capitalist system meant an increasing emphasis on property and property law. Thus, the play is packed with legal jargon and financial and marital contracts. These new legal aspects allow characters like Mrs. Fainall to secure her freedom through an equitable trust and for Mirabell and Millamant's marriage to be equal through a prenuptial agreement.
This shift in social perspectives is perhaps best shown in the characters of Fainall and Mirabell, who represent respectively the old form and new form of marital relations: sexual power at first and then developing into material power.


== Further points of consideration ==
Several aspects of the play give rise to critical discussion:

The love expressed in the play tends to be centred on material gain rather than the love of the partner.
Women's subjugation to their husbands under both law and custom at the time, and an attempt to improve the position of wife, underlie a scene where Millamant states her terms for a pre-nuptial agreement with Mirabell.
None of the characters in the play can really be seen as 'good', and as such it is difficult to find a hero or heroine, or indeed anybody whom one would find deserving of sympathy.


== References ==

Congreve, William (2000). The Way of the World. London, England: A & C Black Limited.
Klekar, Cynthia. “Obligation, Coercion, and Economy: The Gift of Deed in Congreve’s The Way of the World.” In  The Culture of the Gift in Eighteenth-Century England, ed. Linda Zionkowski and Cynthia Klekar. New York: Palgrave MacMillan, 2009


== External links ==
 The Way of the World at Project Gutenberg.
The Way of the World at the Internet Broadway Database
Oxford Playhouse Review of The Way of the World on the BBC website.
Daily Info review of the play.
 The Way of the World public domain audiobook at LibriVox
Some analysis of the play as part of a college writing assignment.