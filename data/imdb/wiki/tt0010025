The Country Cousin is a Walt Disney animated short film released on October 31, 1936 by United Artists. The winner of an Oscar at the 9th Academy Awards for Best Animated Short Film, the film was produced by Walt Disney, directed by Wilfred Jackson, and animated by Art Babbitt and Les Clark. As is true for most cartoons in the Silly Symphonies series, The Country Cousin was built around a musical score, which was written by Leigh Harline. The film's story was based on one of Aesop's Fables, The Town Mouse and the Country Mouse. It was accompanied by a storybook for young children, which was common for many of the Silly Symphonies.


== Plot ==
The Country Cousin tells the story of a mouse from "Podunk" (an American English name denoting a place "in the middle of nowhere") coming to visit his relative in the city. The opening shot/title card is of the mouse in question, Abner, staring up at the city skyscrapers, with the sign directing him to Podunk facing the opposite way. He receives a telegram (titled a "Mouse-O-Gram") from his cousin, Monty, telling him to "Stop being a hick" and come live with him in the city. Without specifying the location of Abner or Monty, the film sets out to contrast the lifestyles of the archetypal country and city inhabitants.
Abner is shown walking along a railroad towards the city, and when he arrives, the differences between the two become clear. Abner is wearing overalls and is portrayed as bumbling and oblivious to the dangers of the city, whilst Monty sports a top-hat and is acutely aware of the problems they face (as exemplified whenever he tells Abner to shush, which is also the only sign of dialogue here). Monty leads Abner to a meal that the human residents of the household have set out. The two mice begin by dining on cheese, but Abner lacks any sense of etiquette, and soon begins gorging himself on celery, cream, and  mustard, the latter of which generates a reaction to the excessive heat. Cooling himself with Champagne, Abner finds himself partial to the taste, and becomes drunk, provoking the disdain of Monty. Abner begins seeing in double vision, and creates more noise by bringing down a pile of bread (causing the double visions of Monty to hide behind the real one).
Abner then assesses his reflection (which, in his intoxicated state, he thinks is someone else) in a block of jelly . As he walks away, he slips on a piece of butter lying on a saucer. The saucer, spinning and flying across the table, catches Monty on the way, and breaks several pieces of dishware too. Monty is infuriated, and as the pair are now on the floor, he attempts to find a secluded spot, safe from the sleeping pet cat. The inebriated Abner, with a great sense of bravado, kicks the cat's behind, whilst his cousin rushes back to his home (and is never seen again from that point onwards). The cat bares its teeth at Abner, which only leads him to inspect the inside of its mouth, thus bringing Abner back to his senses. He is then chased by the cat, first receiving an electric shock by jumping inside a socket (the cat receives a shock as well), and then ending up on the city street when he jumps out of the window.
Abner is faced with escaping from the footsteps of the people, a bike, an electric tram, and other vehicles. He finds himself at the city limits again, and flees back to his home town deciding that the city life just isn't for him.


== LP version ==
In the early 1960s, Disneyland Records released an LP version of the story, narrated by Sterling Holloway, with music by Camarata. Because the running time of the LP was about thirty minutes, more than twice the length of the original cartoon's running time, much additional material was added to the plotline of the story. On the LP cover, the story was humorously billed as being after Aesop - way after. Also, the mice on the cover are patterned after the mice from the 1950 Disney film Cinderella, with Lucifer as the cat.


== Home media ==
The short was released on the 2001 Walt Disney Treasures DVD box set Silly Symphonies.


== References ==


== External links ==
The Country Cousin on IMDb 
The Country Cousin in The Encyclopedia of Disney Animated Shorts