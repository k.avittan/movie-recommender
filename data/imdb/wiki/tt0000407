Saint Ursula (Latin for 'little female bear') is a legendary Romano-British Christian saint, died on 21 October 383. Her feast day in the pre-1970 General Roman Calendar is 21 October. There is little definite information about her and the anonymous group of holy virgins who accompanied her and on some uncertain date were killed at Cologne. They remain in the Roman Martyrology, although their commemoration does not appear in the simplified Calendarium Romanum Generale (General Roman Calendar) of the 1970 Missale Romanum.The earliest evidence of a cult of martyred virgins at Cologne is an inscription from c. 400 in the Church of St. Ursula, located on Ursulaplatz in Cologne which states that the ancient basilica had been restored on the site where some holy virgins were killed. The earliest source to name one of these virgins Ursula is from the 10th century.Her legendary status comes from a medieval story that she was a princess who, at the request of her father King Dionotus of Dumnonia in south-west Britain, set sail along with 11,000 virginal handmaidens to join her future husband, the pagan governor Conan Meriadoc of Armorica. After a miraculous storm brought them over the sea in a single day to a Gaulish port, Ursula declared that before her marriage she would undertake a pan-European pilgrimage. She headed for Rome with her followers and persuaded the Pope, Cyriacus (unknown in the pontifical records, though from late 384 AD there was a Pope Siricius), and Sulpicius, bishop of Ravenna, to join them. After setting out for Cologne, which was being besieged by Huns, all the virgins were beheaded in a massacre. The Huns' leader fatally shot Ursula with an arrow in about 383 AD (the date varies).
There is only one church dedicated to Saint Ursula in the United Kingdom. It is located in Wales at Llangwyryfon, Ceredigion.


== Legend of the Eleven Thousand Companions ==


=== Lack of historical credibility ===
The Catholic Encyclopedia (1912) article on Saint Ursula states that "this legend, with its countless variants and increasingly fabulous developments, would fill more than a hundred pages. Various characteristics of it were already regarded with suspicion by certain medieval writers, and since [Caesar] Baronius have been universally rejected". Neither Jerome nor Gregory of Tours refers to Ursula in his writings. Gregory of Tours mentions the legend of the Theban Legion, to whom a church that once stood in Cologne was dedicated. The most important hagiographers (Bede, Ado, Usuard, Notker the Stammerer, Hrabanus Maurus) of the early Middle Ages also do not enter Ursula under 21 October, her feast day.


=== Tenth-century legend ===
A legend resembling Ursula's appeared in the first half of the tenth century; it does not mention the name Ursula, but rather gives the leader of the martyred group as Pinnosa or Vinnosa. Pinnosa's relics were transferred about 947 from Cologne to Essen, and from this point forward Ursula's role was emphasised. In 970, for example, the first Passio Ursulae was written, naming Ursula rather than Pinnosa as the group's leader (although Pinnosa is mentioned as one of the group's members). This change might also be due in part to the discovery at this time of an epitaph speaking of Ursula, the "innocent virgin".


=== Misreading of Latin ===

According to Geoffrey of Monmouth, a 12th-century British cleric and writer, Ursula was the daughter of Dionotus, ruler of Cornwall. However, this may have been based on his misreading of the words Deo notus in the second Passio Ursulae, written about 1105. The plot may have been influenced by a story told by the 6th-century writer Procopius about a British queen sailing with 100,000 soldiers to the mouth of the Rhine in order to compel her unwilling groom Radigis, king of the Varni, to marry her.
While there was a tradition of virgin martyrs in Cologne by the fifth century, their number may have been limited to between two and eleven, according to different sources. Yet the cleric Wandelbert of the Abbey of Prüm stated in his martyrology in 848 that the number of martyrs counted "thousands of saints" who were slaughtered on the boards of the River Rhine. The 11,000 were first mentioned in the late 9th century; suggestions as to where this number came from have included the reading of the name Undecimillia or Ximillia as a number, or reading the abbreviation XI. M. V. as 'eleven thousand [in Roman numerals] virgins' rather than 'eleven martyred virgins'. One scholar has suggested that in the eighth or ninth century, when the relics of virgin martyrs were found, they included those of a girl named Ursula, who was eleven years old—in Latin, undecimilia. This was subsequently misread or misinterpreted as undicimila ('eleven thousand'), thus producing the legend of the 11,000 virgins. In fact, the stone bearing the virgin Ursula's name states that she lived eight years and two months. Another theory is that there was only one virgin martyr, named Undecimilla, "which by some blundering monk was changed into eleven thousand". It has also been suggested that cum [...] militibus, "with [...] soldiers", was misread as cum [...] millibus, "with [...] thousands". Most contemporary sources, however, cling to the number 11,000. The Passio from the 970s tries to bridge conflicting traditions by stating that the eleven maidens each commanded a ship containing one thousand virgins. Implicitly, the legend also refers to the twelve heavenly legions, mentioned in Matthew 26:53.


=== Skeletal remains ===
The Basilica of St. Ursula in Cologne holds the alleged relics of Ursula and her 11,000 companions. It contains what has been described as a "veritable tsunami of ribs, shoulder blades, and femurs ... arranged in zigzags and swirls and even in the shapes of Latin words". The Goldene Kammer (Golden Chamber), a 17th-century chapel attached to the Basilica of St. Ursula, contains sculptures of their heads and torsos, "some of the heads encased in silver, others covered with stuff of gold and caps of cloth of gold and velvet; loose bones thickly texture the upper walls". The peculiarities of the relics themselves have thrown doubt upon the historicity of Ursula and her 11,000 maidens. When skeletons of little children ranging in age from two months to seven years were found buried with one of the sacred virgins in 1183, Hermann Joseph, a Praemonstratensian canon at Steinfeld, explained that they were distant relatives of the eleven thousand. A surgeon of eminence was once banished from Cologne for suggesting that, among the collection of bones which are said to pertain to the heads, there were several belonging to full-grown mastiffs. The relics may have come from a forgotten burial ground.


=== Catholic official stance ===
Nothing reliable is known about the girls said to have been martyred at the spot. A commemoration of Saint Ursula and her companions in the Mass of Saint Hilarion, formerly in the General Roman Calendar on 21 October, was removed in 1969, because "their Passio is entirely fabulous: nothing, not even their names, is known about the virgin saints who were killed at Cologne at some uncertain time". However, they are still mentioned in the Roman Martyrology, the official but professedly incomplete list of saints recognised by the Catholic Church, which speaks of them as follows: "At Cologne in Germany, commemoration of virgin saints who ended their life in martyrdom for Christ in the place where afterwards the city's basilica was built, dedicated in honour of the innocent young girl Ursula who is looked on as their leader".


== Veneration ==


=== Catholic order ===
The Order of Ursulines, founded in 1535 by Angela Merici, and devoted to the education of young girls, has also helped to spread Ursula's name throughout the world. St. Ursula was named the patron saint of school girls.


=== Celebrations ===
The town of Binangonan in the province of Rizal in the Philippines also has a church dedicated to St. Ursula, where her feast is celebrated on 21 October. A fluvial procession at Laguna Lake is carried out to commemorate Ursula's journey. It is made up of a group of men and women in colourful, traditional Filipino costumes dancing in the streets with the image of Ursula and chanting joyfully. Prior to her feast day, a nine-day novena is held at the 224-year-old Santa Ursula Church. During this novena, a woman is assigned as cantor to sing a chant in honour of Ursula.


=== Church music and art ===
Hildegard of Bingen composed many chants in honour of Ursula.
Michael Haydn wrote the Missa in honorem Sanctae Ursulae to commemorate the day Ursula Oswald joined a Benedictine Abbey.
Hans Memling fashioned during the 1480s a wooden shrine that contained the relics of Ursula, which is now at the Hans Memling Museum in Bruges. It tells the story of Ursula in six bow-arched panels, with the two front panels showing Ursula accompanied by 10 virgins, each representing 1,000 virgins.


=== Places named after her companions ===
Christopher Columbus named the Virgin Islands in the Caribbean in her honour when sailing past them in 1493.
Ferdinand Magellan rounded Cape Virgenes on 21 October, 1520, and entered the Straits of Magellan, naming the cape after Ursula's virgins.
João Álvares Fagundes, a Portuguese explorer, gave in 1521 the name Eleven Thousand Virgins to what is now known as Saint-Pierre and Miquelon.
Basel, a Swiss city about 400 km south of Cologne, has tradition of Ursula and her companions passing through Basel intending to go to Rome. The legend gave name to the Eleven Thousand Virgins Alley (Elftausendjungfern-Gässlein), which climbs one side of the Münsterberg, a hill in the center of the city.


=== UK and Anglican Church ===
The small village of Llangwyryfon, near Aberystwyth in west Wales, has a church dedicated to Ursula. The village name translates as 'Church of the Virgins'. She is believed to have come from this area.
There are Anglican churches dedicated to St. Ursula in the United Kingdom, Switzerland, the United States and the Caribbean.
The street in London called St Mary Axe is named after the Church of St Mary Axe, originally dedicated to St. Mary the Virgin, St. Ursula and the 11,000 virgins. It was demolished in the late 16th century; the site is located close to where the skyscraper informally known as The Gherkin now stands. A manuscript dated 1514 claims that the church contained a holy relic: an axe used by the Huns to execute the virgins.
Whitelands College, the oldest educational institution of the Church of England, has been under the patronage of St. Ursula since its formation. She is the patron saint of the college's chapel.


=== Visions ===
It was recorded that Elizabeth of Schönau experienced a vision that revealed to her the martyrdom of Ursula and her companions.
		
		


== Cordula, Ursula's companion ==
Cordula was, according to a legend in an edition of the Roman Martyrology presented in an English translation on a traditionalist Catholic website, one of Ursula's companions: "Being terrified by the punishments and slaughter of the others, Cordula hid herself, but repenting her deed, on the next day she declared herself to the Huns of her own accord, and thus was the last of them all to receive the crown of martyrdom". In his Albert the Great, Joachim Sighart recounts that, on 14 February, 1277, while work was being done at the church of St John the Baptist (Johanniterkirche) in Cologne, Cordula's body was discovered; it was fragrant and on her forehead was written: Cordula, Queen and Virgin. When Albert the Great heard of the finding, he sang mass and transferred the relics. Later, Cordula's supposed remains were moved to Königswinter and Rimini. Cordula's head was claimed by the Cathedral of Palencia. She is listed in the Roman Martyrology on 22 October.


== Similarities with Sunniva ==
There are striking parallels between the 11th-century legend of Ursula and the story of Sunniva of Selje. Their names were sometimes confused by contemporaries. Both saints were considered to be Christian princesses who fled their homeland by ship in order to postpone or avert an undesired marriage with a pagan king. Both were accompanied by a large group of associates, both became victims of hostile foes. The development of their legends may have been interdependent. The martyrdom of Sunniva, however, took place after the first draft of the Passio Ursulae.


== References ==


== External links ==
(In Italian) Sant' Orsola e compagne
Colonnade Statue St Peter's Square
Saint Ursula and the 11,000 British Virgins