Upstairs Downstairs is a British drama series, broadcast on BBC One from 2010 to 2012, and co-produced by BBC Wales and Masterpiece. Created and written by Heidi Thomas, it is a continuation of the London Weekend Television series Upstairs, Downstairs, which ran from 1971 to 1975 on ITV.
The series resumes the story of 165 Eaton Place, the fictional setting of both iterations of the programme, in 1936, six years after the original series concluded. Jean Marsh reprises her role as Rose Buck, who becomes housekeeper of the re-established household, with Ed Stoppard and Keeley Hawes playing its new owners Sir Hallam and Lady Agnes Holland. The first series, consisting of three episodes, was broadcast across consecutive nights during Christmas 2010. The second and last series consists of six episodes, and first aired between 19 February 2012 and 25 March 2012. The series is set from 1936 to the outbreak of the Second World War in 1939.


== Production history ==


=== Series 1 ===
In October 2009, it was announced that the BBC was to revive the series as two 90-minute episodes to be broadcast on BBC One in the autumn of 2010, written by Heidi Thomas and set in 1936, six years after the original series finished.
At the time the original show had ended its run, the year was 1930, the Great Depression had already begun, and the Bellamy family had lost all its money in the crash of 1929. James Bellamy, the only son of Richard, the Viscount Bellamy, had been responsible for persuading not only his family but also the faithful family servant, Rose Buck, to invest all their money in the stock market. Consequently, James committed suicide and the Bellamy family, as well as all the servants, left Eaton Place to start new lives. Mr. Hudson, the head butler, and Mrs. Bridges, the cook, married and moved to the seaside, taking the kitchen maid, Ruby Finch, to live with them; Edward, the chauffeur, and his wife Daisy, the maid, were given new positions by the Bellamys' cousin, Georgina, and her husband, Lord Stockbridge, in their new country house; and Rose was offered the job as maid to Lord and Lady Bellamy at their small villa.
The new series sees Jean Marsh, as the only original cast member from the LWT series, reprising the role of Rose Buck. Buck now runs a domestic servant agency, after a time away nursing a relative in the country. She returns to 165 Eaton Place as housekeeper to the new owners, the Holland family, with Dame Eileen Atkins playing one of the characters upstairs.
There are no explanations as to what has happened to Lady Bellamy, Georgina, Lord Stockbridge, as well as Mr. Hudson, Mrs. Bridges, Edward, Daisy, and Ruby during that six-year period. In fact, in the first instalment, the Bellamy family and the original staff are only fleetingly referred to when Mrs. Thackeray, the new cook, refers to 165 Eaton Place as "the Bellamy house". However, Rose does mention Lord Bellamy as "the late", meaning he has died by 1936. Also, there are scenes with Rose serving tea to Lady Agnes and Mrs. Thackeray at her domestic agency. Rose mentions that the teapot was given to her as a gift by Lord and Lady Bellamy in appreciation for all her "impeccable" years of service to the family. Also, in another scene when Lady Agnes gives Rose the keys to the wine cellar, she mentions that they had belonged to Mr. Hudson. Rose takes the keys and looks at them, smiling, in a very nostalgic way.
In August 2010, the BBC announced that the planned two 90-minute shows would instead consist of three hour-long episodes, with Keeley Hawes, Ed Stoppard, Anne Reid, Claire Foy, Adrian Scarborough, Art Malik, Ellie Kendrick, Blake Ritson, and Nico Mirallegro joining Jean Marsh and Eileen Atkins in the cast. Filming began in Cardiff in the middle of August 2010, with parts of the city transformed into 1930s Belgravia for exterior scenes, and the interiors shot in BBC's Llandaff studios in Cardiff. Further filming of exterior scenes took place in Leamington Spa, Warwickshire in September 2010, with a terrace in Clarendon Square (by the same architect, Peter Frederick Robinson, as the London square used in the original series) doubling up as Eaton Place.
The new Upstairs Downstairs was made in-house by BBC Wales as a co-production with Masterpiece on PBS, and was broadcast on BBC1 with the debut episode shown on 26 December 2010. A soundtrack album of music from the new series by composer Daniel Pemberton was released on iTunes. The three episodes were picked up by overseas broadcasters, including the ABC in Australia, NRK in Norway, Sky in New Zealand, DR in Denmark, YLE in Finland, IBA in Israel and TV4 in Sweden.


=== Series 2 ===
BBC1 recommissioned six 60-minute episodes of the drama to be broadcast in 2012, with Stoppard, Hawes, Reid, Foy, Malik, Ritson, Mirallegro, Scarborough and Jackson all returning to the series. Sarah Gordy and Alexia James, introduced during series 1 as Hallam's sister Pamela, who has Down's Syndrome, and the household's Jewish ward Lotte respectively, also continue to appear on a recurring basis. Eileen Atkins does not appear in series 2, having stated in the press that she was unhappy with the development of her character; so Maud therefore dies before the series resumes. Ellie Kendrick also left the cast, with the character of Ivy replaced in the household with Eunice McCabe (Ami Metcalf). (The relationship between Eunice and Mrs. Thackeray, the cook, is quite similar to the interaction between Ruby, the kitchen maid (played by Jenny Tomasin) and Mrs. Bridges, the cook (played by Angela Baddeley) in the previous series Upstairs, Downstairs.)
Jean Marsh, after suffering from a stroke and heart attack, was unable to attend the majority of filming for the series; scripts were altered shortly before filming to accommodate her absence. On-screen, Rose is confined to a sanatorium after contracting tuberculosis, and appears in two scenes over the series.
Other new additions to the cast include Alex Kingston as the younger half-sister of Maud, Blanche Mottershead and Laura Haddock as maid Beryl Ballard. Special guest stars in the series include Kenneth Cranham as Sergeant Ashworth,  Michael Landes as American multimillionaire Caspar Landry, Emilia Fox as Blanche's lover Lady Portia Alresford and Sarah Lancashire as Violet Whisset, a love interest for Mr Pritchard. Derbyshire manufacturer The Dolls House Emporium supplied a period 'Montgomery Hall' dolls' house as a prop for the 2012 series.


== Characters ==


=== Upstairs ===


=== Downstairs ===


== Plot ==
Sir Hallam Holland, a young diplomat, moves into the townhouse along with his wife, Lady Agnes, in January 1936 shortly before the death of King George V. They engage former parlourmaid Rose Buck, now running her own agency for domestic servants, to find them staff as they renovate the house to its former glory after years of being mothballed.
As they settle into London life, they are soon confronted with Lady Agnes' fiery young sister Lady Persephone, Sir Hallam's overbearing widowed mother, who moves herself into the house along with secretary and pet monkey, and a young, barely-trained house staff serving under a reluctant head housemaid.  Added to these stressors are the still-painful memory of Lady Agnes's past miscarriage, a mystery surrounding Sir Hallam's sister, who died as a child, and a surprise foster-child they feel obliged to maintain.
Downstairs, life goes on much as it always has.  Rose, convinced to remain, hires a staff of strong young hopefuls, to assist her trusted friend the cook, and the well-credentialled new butler.  Besides training the young servants and navigating between her old memories and the new demands of the house, Rose must also help train Lady Agnes in the ways and manner of running a townhouse.  The new downstairs family slowly begins to pull together as a unit, overcoming obstacles of age, class and race as they come to know one another's stories.


== Episodes ==


=== Series 1 (2010) ===
The first series began airing in the UK in December 2010. These episodes were featured in the U.S. on PBS's Masterpiece Classic in April 2011. VisionTV in Canada premiered the episodes in October 2011. ABC1 in Australia began airing the series on 4 December 2011.
(1936)


=== Series 2 (2012) ===
(1938–39)


== Reception ==
Series 1 was aired in the UK over three consecutive nights commencing 26 December 2010. It was a substantial ratings success—garnering viewing figures of 8.85, 8.13 and 8.18 million respectively,  and winning its slot against all seasonal competition.  The programme was nominated for six Primetime Emmy awards in 2011.The second series of six episodes began on 19 February 2012, with an audience of 7.78 million, but by the time it ended on 25 March, the viewing figures had dropped to 5.22 million. It was confirmed on 21 April 2012 that Upstairs Downstairs would not be returning for a third series.


== Soundtrack ==
Composer Carl Davis was brought in for the second series, composing new incidental music, occasionally using elements from Alexander Faris's original theme for the original 1971 series — which was popularised as a single release by Mantovani. Davis's music was released as a soundtrack album   and was nominated for the Ivor Novello Awards in 2013, but did not win.


== References ==


== External links ==
Upstairs Downstairs at BBC Programmes 
Upstairs Downstairs on IMDb