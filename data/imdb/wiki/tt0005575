Renunciation (or renouncing) is the act of rejecting something, especially if it is something that the renunciant has previously enjoyed or endorsed.
In religion, renunciation often indicates an abandonment of pursuit of material comforts, in the interests of achieving spiritual enlightenment. It is highly practiced in Jainism. In Hinduism, the renounced order of life is sannyāsa; in Buddhism, the Pali word for "renunciation" is nekkhamma, conveying more specifically "giving up the world and leading a holy life" or "freedom from lust, craving and desires".  See Sangha, Bhikkhu, Bhikkhuni, Śramaṇa. In Christianity, some denominations have a tradition of renunciation of the Devil.
Renunciation of citizenship is the formal process by which a person voluntarily relinquishes the status of citizen of a specific country. A person can also renounce property, as when a person submits a disclaimer of interest in property that has been left to them in a will.


== Mahatma Gandhi ==
It is widely believed in India that voluntary renunciation is how one gains power, for whatever purpose. In South Africa (1893-1915) Gandhi tried many spiritual practices and experiments, almost all of them including a component of renunciation, based in the practice of Sannyasi. After reading Unto This Last by John Ruskin in 1904 he redoubled his commitment to gain greater control over self, increasing his capacity to work for the common welfare and find a greater sense of oneness with others. The ultimate renunciation is of self, one's separateness from others and the world.


== Christianity ==
In some Christian denominations, renunciation of the Devil is a common liturgical rubric. This is most often seen in connection with the sacrament of baptism. In the Roman Catholic church a baptism usually contains the "Prayer of Exorcism". Later in the ceremony, the parents and godparents are asked to publicly renounce the devil.The Church of England dismissed this rubric in a 2014 renewal of liturgy. According to The Independent, this was done in an attempt to "widen the appeal" of the rite. A prior report for the Church's Liturgical Commission stated that "[f]or the majority of those attending, the existing provision can seem complex and inaccessible."In the Church of Norway, the public renunciation of the Devil is an obligatory element in the Main Service. It is stated by the congregation before the profession of faith (usually the Apostles' Creed, as the Nicene Creed is largely reserved for special observances). When performed in a service which includes a baptism, it is also considered an extension of the testimony given by the sponsors, as they are required to confess to a denomination which does not rejects the Apostles' or the Nicene Creed, nor rejects infant baptism.


== See also ==
Asceticism
Nekkhamma
Sannyasa


== References ==


== External links ==