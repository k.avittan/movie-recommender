Little House on the Prairie (later known as Little House: A New Beginning in its final season) is an American Western historical drama series, starring Michael Landon, Melissa Gilbert, Karen Grassle, and Melissa Sue Anderson, about a family living on a farm in Plum Creek near Walnut Grove, Minnesota, in the 1870s, '80s, and '90s. The show is an adaptation of Laura Ingalls Wilder's best-selling series of Little House books. In 1972, with the encouragement of his wife and daughter, television producer and former NBC executive Ed Friendly  acquired the film and television rights to Wilder’s novels from Roger Lea MacBride and engaged Blanche Hanalis to write the teleplay for a two-hour motion picture pilot.  Friendly then asked Michael Landon to direct the pilot; Landon agreed on the condition that he could also play Charles Ingalls.
The regular series was preceded by a two-hour pilot movie, which first aired on March 30, 1974. The pilot was based on Laura Ingalls Wilder's third Little House book in the series, Little House on the Prairie. The series premiered on the NBC network on September 11, 1974, and last aired on May 10, 1982. During the 1982–83 television season, with the departure of Landon and Grassle, the series was broadcast with the new title Little House: A New Beginning.


== Premise ==

Based on the autobiographical Little House series, episodes of Little House on the Prairie usually concern members of the Ingalls family, who live on a small farm near the village of Walnut Grove, Minnesota. Many episodes concern the maturation of the family's second daughter, Laura. Episodes also focus on other family and community members, though, providing a depiction of life in a small agrarian community in late 19th-century America. The show's central characters are Charles Ingalls (farmer and mill worker), his wife Caroline, and their three daughters, Mary, Laura, and Carrie, though the family expands with the birth of daughter Grace and adoption of son Albert in season five, as well as the adoption of birth siblings Cassandra and James at the end of season seven (a son, Charles "Freddy" Jr., was also born, but died as an infant).
Other essential characters include the Oleson family: Nels, proprietor of the town's general store, Oleson's Mercantile, as well as Nellie's Restaurant and Hotel; his malicious, gossiping wife, Harriet, who runs the Mercantile and Restaurant with him and serves as the show's principal antagonist; and their three children, biologically Nellie and Willie, and adopted Nancy; Isaiah Edwards, Grace Snider Edwards and their three adopted children; the Garvey family, Jonathan, Alice, and Andy; Rev. Robert Alden; Lars Hanson, the town's founder and proprietor of the town's mill; and Dr. Hiram Baker, the town's physician and veterinarian. Teacher-turned-lawyer Adam Kendall is introduced at the end of season four and later weds Mary Ingalls, and Almanzo Wilder is introduced in season six and later weds Laura Ingalls.


== Cast and characters ==

Melissa Gilbert has the most appearances of the series, a total of 190 of the 204 episodes. Michael Landon appeared in all but four episodes of seasons one through eight, but departed from being a regular part of the cast when the show was retooled as Little House: A New Beginning (season nine).


=== Main cast ===
Michael Landon as Charles Ingalls  (seasons 1–8, guest in 9, two post-series movies)
Karen Grassle as Caroline Quiner Ingalls (seasons 1–8, one post-series movie)
Melissa Gilbert as Laura Ingalls Wilder (seasons 1–9, three post-series movies)
Melissa Sue Anderson as Mary Ingalls Kendall (seasons 1–7, guest in 8)
Lindsay and Sidney Greenbush as Carrie Ingalls (seasons 1–8)
Matthew Labyorteaux as Albert (Quinn) Ingalls (seasons 5–8, guest in 9, one post-series movie)
Richard Bull as Nels Oleson (seasons 1–9, three post-series movies)
Katherine MacGregor as Harriet Oleson (seasons 1-9)
Alison Arngrim as Nellie Oleson Dalton (seasons 1–7, guest in 9)
Jonathan Gilbert as Willie Oleson (seasons 1–9, two post-series movies)
Victor French as Mr. Edwards (seasons 1–3, guest in 6, 8–9, three post-series movies)
Bonnie Bartlett as Grace Snider Edwards (seasons 1–3, guest in 6)
Kevin Hagen as Dr. Hiram Baker (seasons 1–9, three post-series movies)
Dabbs Greer as Rev. Robert Alden (seasons 1–9, two post-series movies)
Charlotte Stewart as Eva Beadle Simms (seasons 1–4)
Karl Swenson as Lars Hanson (seasons 1–5)
Radames Pera as John (Sanderson, Jr.) Edwards (seasons 2-4)
Brian Part as Carl (Sanderson) Edwards (seasons 2-3)
Kyle Richards as Alicia (Sanderson) Edwards (seasons 2–3, guest in 6, 8)
Merlin Olsen as Jonathan Garvey (seasons 4–7)
Hersha Parady as Alice Garvey (seasons 4–6)
Patrick Labyorteaux as Andrew "Andy" Garvey (seasons 4–7)
Linwood Boomer as Adam Kendall (seasons 4–7, guest in 8)
Ketty Lester as Hester-Sue Terhune (seasons 5–9)
Wendi and Brenda Turnbaugh as Grace Ingalls (seasons 5–8)
Queenie Smith as Mrs. Amanda 'May' Whipple (seasons 1-4)
Dean Butler as Almanzo Wilder (seasons 6–9, three post series movies)
Lucy Lee Flippin as Eliza Jane Wilder (season 6, guest in 7 and 8 )
Steve Tracy as Percival Dalton (season 6 and 7)
Jason Bateman as James (Cooper) Ingalls (seasons 7 and 8)
Melissa Francis as Cassandra (Cooper) Ingalls (seasons 7 and 8)
Allison Balson as Nancy Oleson (seasons 8 and 9, three post-series movies)
Shannen Doherty as Jenny Wilder (season 9, three post-series movies)
Stan Ivar as John Carter (season 9)
David Friedman as Jason Carter (season 9)
Lindsay Kennedy as Jeb Carter (season 9)
Pamela Roylance as Sarah Reed Carter (season 9)


=== Notable guest stars ===
Many actors, who were either well-known or went on to become famous, guest-starred on the show.


== Production notes ==
Of the 204 episodes, Michael Landon directed the largest number at 87; producer William F. Claxton handled the majority of the remaining shows at 68, while co-star Victor French helmed 18. Maury Dexter (who was often an assistant director) and Leo Penn directed the remaining episodes at 21 and three episodes, respectively.
Interior shots were filmed at Paramount Studios in Los Angeles, while exteriors were largely filmed at the nearby Big Sky Ranch in Simi Valley, where the town of Walnut Grove had been constructed. Many other filming locations were also used during the course of the series, including Old Tucson Studios and various locations in Sonora, California. Many of the exterior shots of Walnut Grove and the other Minnesota towns shown in the series include noticeable mountainous terrain in the background scenery. In reality, however, the southern Minnesota landscape where the show is supposed to take place includes no tall mountains.
The series theme song was titled "The Little House" and was written and conducted by David Rose. The ending theme music, also written by Rose, originally appeared as a piece of incidental music in a later-season episode of Michael Landon's previous long-running series, Bonanza.


== Themes ==
Little House explored many different themes including frequently portrayed ones of adoption, alcoholism, faith, poverty, blindness, and prejudice of all types, including racism. Some plots also include subjects such as drug addiction (e.g. Albert's addiction to morphine), leukemia, child abuse, and even rape. Although predominantly a drama, the program has many lighthearted and comedic moments, as well.
Some of the episodes written by Michael Landon were recycled storylines from ones that he had written for Bonanza. Season two's "A Matter of Faith" was based on the Bonanza episode "A Matter of Circumstance"; season five's "Someone Please Love Me" was based on the Bonanza episode "A Dream To Dream"; season seven's "The Silent Cry" was based on the Bonanza episode "The Sound of Sadness"; season eight's "He Was Only Twelve" was based on the Bonanza episode "He Was Only Seven"; and season nine's "Little Lou" was based on the Bonanza episode "It's A Small World".
In 1997, TV Guide ranked the two-part episode "I'll Be Waving as You Drive Away" at 97 on its 100 Greatest Episodes of All Time list; the episode was about Mary going blind.


== Spin-offs and sequels ==


=== Little House: A New Beginning ===
When Michael Landon decided to stop acting on the show, and stay on only as an executive producer and occasional writer and director, season nine was renamed. The focus was put on the characters of Laura and Almanzo, and more characters were added to the cast. A new family, the Carters (Stan Ivar as John, Pamela Roylance as Sarah, Lindsay Kennedy as older son Jeb, and David Friedman as younger son Jason), move into the Ingalls house. Meanwhile, Almanzo and Laura take in their niece, Jenny Wilder, when Almanzo's brother dies and raise her alongside their daughter, Rose. The Wilders appear prominently in some episodes, while in others they appear only in early scenes used to introduce the story or its characters. The explanation given for the original characters' absence was that they moved to Burr Oak, Iowa, to pursue a promising life. The show lost viewers, and this version of the series was cancelled after one season, but the show lived on for another 1.5 years in movie format.


=== Backdoor pilot ===
The series finale movie, Hello and Goodbye, in which Laura and Almanzo finish renovating the late Mrs. Flannery's home into a boardinghouse and start to take in residents, was meant as a backdoor pilot for an entirely new spinoff alongside what was supposed to have been another few seasons of the original show.
In that episode, Mr. Edwards moved in after his mute son Matthew left with his father and he realized that not only was his cabin falling down, it was situated a considerable distance from all his friends.
Willie and Rachel, wanting their own space and to be out from under Harriet's thumb in the rooming house upstairs of the hotel and restaurant  elected to move in with Laura and Almanzo, as well, while Willie cooked and ran the restaurant with Rachel.
Writer Sherwood Montague rounded out the ensemble and the show was supposed to have covered his attempts to bring sophistication to Walnut Grove, but low viewership led to cancellation of both the main show and the intended spinoff.
The three movie specials listed below were produced to tie up loose ends to storylines on both the main series and those opened up in Hello and Goodbye.


=== Movie specials ===
Three made-for-television post-series movies followed during the 1983–84 television season: Little House: Look Back to Yesterday (1983), Little House: The Last Farewell (1984), and Little House: Bless All the Dear Children (1984).
In The Last Farewell, Charles and Caroline decide to visit Walnut Grove. They learn that a railroad tycoon actually holds the deed to the township, and he wants to take it over for his own financial gain. Despite their best efforts, the townspeople are unable to drive the businessman away. At a town meeting, John Carter offers a supply of explosives that he has. Each man takes a turn blowing up his own building in an emotional farewell to the town.When asked why the set was blown up, the show's producer, Kent McCray, said that when the series started, he made an agreement with the property owners that at the end of the series he would put the acreage back to its original state. When the production crew were estimating the cost of dismantling all the buildings, Michael Landon thought for a while and said, "What if we blow up the town? That would get the buildings all in pieces and you can bring in your equipment to pick up the debris and cart it away." He then said that he would write it where they blow up all the buildings, except for the little house and the church. Both McCray and Landon wept as the town blew up.Bless All the Dear Children was filmed prior to The Last Farewell, but ended up being the last of the three movies to air. Given its Christmas-related content, NBC opted to air it during the following Christmas season.
Two other Little House movies were made in conjunction with the Landon series: the 1974 pilot for the program and The Little House Years (1979), a Thanksgiving special/clip show that aired in the middle of season six.


== Broadcast history ==
For the first two seasons, the show was aired on Wednesday nights at 8pm ET/7pm CT, to moderate ratings. In 1976, the series became a Monday night staple on NBC; after the move, it remained in the top 30 for the rest of its run.


== Reception ==


=== Seasons ===
Season 1 (1974–75): No. 13, 23.5 ratingSeason 2 (1975–76): No. 33Season 3 (1976–77): No. 16, 22.3 rating
Season 4 (1977–78): No. 7, 24.1 rating
Season 5 (1978–79): No. 14, 23.1 rating
Season 6 (1979–80): No. 16, 21.8 rating
Season 7 (1980–81): No. 10, 22.1 rating
Season 8 (1981–82): No. 25, 19.1 rating (tied with: The Facts of Life)
Season 9 (1982–83): No. 29, 17.4 rating


=== Accolades ===
1978: Emmy Award for Outstanding Cinematography in Entertainment Programming for a Series, Ted Voigtlander, episode "The Fighter"
1979: Emmy Award for Outstanding Cinematography for a Series, Ted Voigtlander, episode "The Craftsman"
1979: Emmy Award for Outstanding Music Composition for a Series, David Rose, episode "The Craftsman"
1980: TP de Oro, Spain, Mejor Actriz Extranjera (Best Foreign Actress), Melissa Sue Anderson
1981: Western Writers of America Spur Award for Best TV Script, Michael Landon, episode "May We Make Them Proud"
1982: Emmy Award for Outstanding Achievement in Music Composition for a Series (Dramatic Underscore), David Rose, episode "He Was Only Twelve" (Part 2)
1983: Young Artist Award for Best Young Actress in a Drama Series, Melissa Gilbert
1984: Young Artist Award for Best Young Actress in a Drama Series, Melissa Gilbert


=== Popularity in Spain ===
In the late 1970s and early 1980s, La Casa de la Pradera (The House on the Prairie) was one of Spanish Television's most popular series. In 1976 Karen Grassle (Caroline Quiner Ingalls) won Spanish television's prestigious TP de Oro award for best foreign actress, and the series itself won for best foreign series; Melissa Sue Anderson (Mary Ingalls) won the TP de Oro in 1980 thanks in part to the enhanced profile she received as a result of her visit to Spain and her appearance on Spanish Television's 625 Lineas program in early 1979. The continued popularity of the show led to the appearance of Katherine MacGregor (Harriet Oleson) on 625 Lineas and Ding Dong in 1980.


== Other media ==


=== Syndication ===
The show remains popular in syndicated reruns and has been on the air in the U.S. continuously since its original run. In addition to airing on local stations, it has been airing multiple times each day on INSP and Hallmark Channel. In the past, it has aired on TV Land and TBS. Currently, the show is airing on Cozi TV & UpTV weekdays.
In the U.S., television syndication rights are currently owned by NBCUniversal Television Distribution. Originally, NBC licensed these rights to Worldvision Enterprises, since networks could not own syndication arms at the time. As a result of corporate changes, Paramount Domestic Television (now CBS Television Distribution) would inherit the rights via Spelling Entertainment, and NBCUniversal re-acquired the rights in the mid-2000s because the fin-syn rules were repealed in 1993. In Canada, reruns of the series began airing weeknights on CTS, a Christian-based network, as of September 1, 2008.
Because of its historical context and its connection to the book series, it is deemed acceptable for use by the FCC to meet federal E/I programming guidelines. The show is typically stripped (run five days a week) in syndication, which is enough to completely cover a TV station's E/I requirements and more.
NBC owns ancillary rights and thus is the worldwide licensor for DVD rights as well. Sister company NBCUniversal Television Distribution also distributes the series internationally with MGM Television handling international distribution.


=== Home media ===
The entire series has been released on standard-definition DVD, high-definition Blu-ray, and on both standard and high-definition Digital Copy.  In addition, some individual episodes have been released on DVD and VHS.  Starting with Season 7, the Blu-ray's are only available exclusively through Amazon.com.
There are multiple DVD sets which are noticeably different from one another.  The original DVD sets sold in the U.S. and Canada were released under license from NBCUniversal by Imavision Distribution, a company based in Quebec. A majority of the episodes in the original North American DVD versions had scenes cut from the episodes—these were derived from the syndicated television versions by Worldvision Enterprises, the series' former distributor. Other episodes were time-compressed and are NTSC-converted video prints from UK PAL masters, while others were derived from 16MM syndication prints, also from Worldvision. Only a handful of episodes in the original sets were in their original uncut versions. The episodes in these original sets are also known to have relatively poor video quality, such as tracking lines, as well as audio problems, though the quality issues are not as pronounced in the first few seasons as they are in the later seasons.  The first three seasons of the old sets notably are also missing closed-captioning.
These original North American DVD sets included interviews with former cast members Alison Arngrim, Dabbs Greer and Dean Butler. For the original movies & complete series sets, Imavision provided numerous additional special features including additional interviews with many of the cast members such as Melissa Gilbert and Melissa Sue Anderson and specials highlighting Michael Landon, the casting of the show, and more.  Imavision also released a French-language version of the series. Both versions are in NTSC color and coded for all regions. Later copies of these original sets were distributed by Lionsgate Home Entertainment following their acquisition of Imavision, but these should not be confused with the Lionsgate re-releases described below. The DVD sets sold in the United Kingdom were released by Universal Playback (a Universal Studios Home Entertainment label); this version is in PAL color and coded for region 2.  Unlike the original North American DVD sets, the UK version contains mostly uncut episodes.
In 2014, Lionsgate Home Entertainment began re-releasing the series in North America on DVD, and also for the first time, in high definition on Blu-ray, as well as Digital Copy through providers such as Vudu and Amazon Video. These new releases, which are stated to come direct from the original broadcast masters, contain mostly uncut episodes and are remastered to have superior picture and sound. The Blu-rays, with their high bitrate, high definition 1080p picture (as opposed to standard definition picture on the DVDs) currently provide the best viewing experience of the show that is commercially available.  The first six seasons on Blu-ray notably also contain lossless audio as opposed to the compressed audio on the DVDs.  Starting with Season 7, Lionsgate chose to only release the remaining Blu-ray's exclusively through Amazon.com.  In the process, they made several other changes to the Blu-ray's including compressing the audio (though with a relatively high bitrate), simplifying the on-screen disc menus, and eliminating the slipcovers and included Digital Copy codes that had been present for the previous seasons.
The newer Lionsgate remastered sets all contain English, French, and Spanish audio as well as English subtitles.  They do not include the special features present on the earlier non-remastered releases, but rather seasons 1 through 6 each contain a roughly 15 minute segment of a special called "The Little House Phenomenon".  Season 1 also contains the original Pilot movie.  Season 7 contains no special features.  Seasons 8 & 9 contain the three post-series movie specials as extras, with "Look Back to Yesterday" and "The Last Farewell" appearing on Season 8, and "Bless All The Dear Children" appearing on Season 9.  Some fans of the show have been perplexed as to why Lionsgate did this, both because all of the movies take place after the Season 9 timeline, and also because they included "The Last Farewell" on Season 8 when that is considered by fans to be the end to the show given its significant and memorable ending.  Lionsgate's decision as to which movies to include on which season appears to have been based on broadcast order rather than production order, since "Bless All The Dear Children" was the last episode broadcast even though "The Last Farewell" was the last one produced.  None of the available releases of the series contain "The Little House Years", which was a three-hour Thanksgiving special aired during Season 6 that largely consisted of flashback clips.
While the re-releases are substantially better than what was previously available, there are a handful of episodes that still were released in edited form or contain other problems.  The most significant of these, affecting all formats of the remastered releases, include over 3 minutes missing from the Season 7 episode, "Divorce, Walnut Grove Style," almost 4 minutes missing from Season 9's "Home Again," and extremely low volume of the townspeople's singing on the English audio of the last scene of the final movie, "The Last Farewell."


=== List of releases ===


== Film adaptation ==
In October 2012, Sony Pictures announced that a film adaptation of the Little House on the Prairie novel was under development. In early 2016, it was widely reported that Paramount Pictures had picked up the project in turnaround, but an agreement was never reached.


== References ==


== External links ==

Little House on the Prairie on IMDb
Little House: Look Back to Yesterday on IMDb
Little House: The Last Farewell on IMDb
Little House: Bless All the Dear Children on IMDb
Little House On The Prairie Episode Guide
Little House On The Prairie
Little House Books