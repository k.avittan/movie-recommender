Fantômas (French: [fɑ̃tomas]) is a fictional character created by French writers Marcel Allain (1885–1969) and Pierre Souvestre (1874–1914).
One of the most popular characters in the history of French crime fiction, Fantômas was created in 1911 and appeared in a total of 32 volumes written by the two collaborators, then a subsequent 11 volumes written by Allain alone after Souvestre's death. The character was also the basis of various film, television, and comic book adaptations. In the history of crime fiction, he represents a transition from Gothic novel villains of the 19th century to modern-day serial killers.
The books and movies that came out in quick succession anticipate current production methods of Hollywood, in two respects: First, the authors distributed the writing among themselves; their "working method was to draw up the general plot between them and then go off and write alternate chapters independently of each other, meeting up to tie the two halves of the story together in the final chapter." This approach allowed the authors to produce almost one novel per month. Second, the movie rights to the books were immediately snapped up. Such a system ensured that the film studio could produce sequels reliably.
The popular depiction of Fantômas as wearing a blue mask, black gloves, and using technological devices did not originate in the novels, but is a result of the popularity of the trilogy of Fantômas movies directed by André Hunebelle in the 1960s. The trilogy, which started in 1964 with Fantômas, departed considerably from the novels by giving the story a more comedic tone as preceded by the first two Pink Panther films, and by making Fantômas (played by Jean Marais) more of a James Bond enemy by likewise borrowing from the first two Bond films. Despite these discrepancies, the blue-masked Fantômas is arguably the one that is most easily remembered.


== Overview ==

Fantômas is a criminal genius, ruthless and particularly elusive. As described by Schütt (2003) in her analysis of French crime fiction literature, Fantômas is the cause of almost any unsolved crime, a merciless criminal who can get away with his evil deeds by impersonating pretty much anyone. He is obsessively chased by Inspector Juve, who is generally the only one able to see Fantômas's tracks.Fantômas was introduced a few years after Arsène Lupin, another well-known thief. But whereas Lupin draws the line at murder, Fantômas has no such qualms and is shown as a sociopath who enjoys killing in a sadistic fashion.
He is totally ruthless, shows no mercy, and is loyal to none, not even his own children. He is a master of disguise, always appearing under an assumed identity, often that of a person whom he has murdered. Fantômas makes use of bizarre and improbable techniques in his crimes, such as plague-infested rats, giant snakes, and rooms that fill with sand.
Fantômas's background remains vague. He might be of British and/or French ancestry. He appears to have been born in 1867.In the books, it is established that c. 1892, the man who later became Fantômas called himself Archduke Juan North and operated in the German principality of Hesse-Weimar. There he fathered a child, Vladimir, with an unidentified noblewoman. In circumstances unrevealed, he was arrested and sent to prison.
C. 1895, Fantômas was in India. There, an unidentified European woman gave birth to a baby girl, Hélène, whose father might be Fantômas or an Indian prince who was Fantômas' acolyte. The girl was raised in South Africa.
In 1897, Fantômas was in the United States of America and Mexico. There, he ruined his then business partner, Etienne Rambert.
In 1899, he fought in the Second Boer War in South Africa under the name of Gurn. He fought in the Transvaal as an artillery sergeant under the command of Lord Roberts. He became aide-de-camp to Lord Edward Beltham of Scottwell Hill and fell in love with his younger wife, Lady Maud Beltham.
Upon their return to Europe, soon before the first novel begins (c. 1900), Gurn and Lady Beltham were surprised in their Paris love nest, Rue Levert, by her husband. Lord Beltham was about to shoot Maud when Gurn hit him with a hammer then strangled him.
Fantômas then impersonated Etienne Rambert and framed his son, Charles, for a murder he had committed. As Etienne, he persuaded Charles to go into hiding, but the young man was soon found out by French police detective Juve, who was truly obsessed with the capture of Fantômas. Juve knew that Charles was innocent and gave him a new identity: journalist Jerôme Fandor who is employed at the newspaper La Capitale. Juve later arrested Gurn and, at his trial, brought forward a convincing argument that Gurn and Fantômas were one and the same, though the evidence was too circumstantial to make a real case. On the eve of his execution, Gurn/Fantômas escaped from custody by being replaced by an actor who had modelled the appearance of his latest character after him and was guillotined in his place.
Lady Beltham remained constantly torn between her passion for the villain and her horror at his criminal schemes. She eventually committed suicide in 1910.
Fandor fell in love with Hélène and, despite Fantômas's repeated attempts to break them up, married her.
Fantômas's evil son, Vladimir, reappeared in 1911. Vladimir's girlfriend was murdered by Fantômas and Vladimir himself was eventually shot by Juve.


== Characters ==
Fantômas: A criminal genius, known by many nicknames, such as "the "master of everything and everyone," the "torturer" or the "elusive", and whose face and true identity remain unknown. A ruthless criminal, he won't hesitate to torture and kill to achieve his goals.
Juve: An inspector in the Sûreté of Paris, he is the sworn archenemy of Fantômas, whom he pursues obsessively. The intelligent and stubborn Juve is completely devoted to capturing or killing Fantômas.
Jérôme Fandor: a former victim of Fantômas, he was originally named Charles Rambert. Fantômas killed his parents and then framed him as the killer. Juve, the only one to understand his wrong accusation, provides him with the identity of journalist Fandor so as to save him from going to jail and to use his help in capturing the murderer. He falls in love with Hélène.
Hélène: She is the beautiful stepdaughter of Fantômas, who might even be her biological father. The criminal protects her jealously. She falls in love with J. Fandor, and is engaged to marry him, eventually helping her fiancé to fight the criminal.
Lady Maud Beltham: The wife of Lord Beltham, she becomes the lover of Fantômas, towards whom she feels both passionate love and repulsion for his many crimes.
Bouzille: a streetwise tramp who often helps Juve and Fandor, and even Fantômas on occasion, and provides comic relief in the series.
The Beadle: a thug and companion of Fantômas who earns his nickname after his custom of taking the money of passers-by by smashing their heads against the pavement, he is the most notorious of Fantômas' gang of Apaches.
Mother Toulouche: An old woman who is a sort of leader among Fantômas' Apaches. She is portrayed as an equally ruthless killer and thug.


== Books ==


=== By Allain and Souvestre ===
1. Fantômas (1911; transl. 1915; retransl. 1986)
2. Juve contre Fantômas (1911; transl. 1916 as The Exploits of Juve; retransl. 1987 as The Silent Executioner)
3. Le Mort qui Tue (1911; transl. 1917 as Messengers of Evil; retransl. 2008 as The Corpse who Kills)
4. L'Agent Secret (1911; transl. 1917 as A Nest of Spies)
5. Un Roi Prisonnier de Fantômas (1911; transl. 1918 as A Royal Prisoner)
6. Le Policier Apache (1911; transl. 1924 by Alfred Allinson as The Long Arm of Fantômas [UK title: The Limb of Satan])
7. Le Pendu de Londres (1911; transl. 1920 as  Slippery as Sin)
8. La Fille de Fantômas (1911; transl. 2006 by Mark P. Steele as The Daughter of Fantomas) (ISBN 1932983562)
9. Le Fiacre de Nuit (1911)
10. La Main Coupée
11. L'Arrestation de Fantômas (1912)
12. Le Magistrat Cambrioleur (1912)
13. La Livrée du Crime (1912)
14. La Mort de Juve (1912)
15. L'Evadée de Saint-Lazare (1912)
16. La Disparition de Fandor (1912)
17. Le Mariage de Fantômas (1912)
18. L'Assassin de Lady Beltham (1912)
19. La Guêpe Rouge (1912)
20. Les Souliers du Mort (1912)
21. Le Train Perdu (1912)
22. Les Amours d'un Prince (1912)
23. Le Bouquet Tragique (1912)
24. Le Jockey Masqué (1913)
25. Le Cercueil Vide (1913)
26. Le Faiseur de Reines (1913)
27. Le Cadavre Géant (1913)
28. Le Voleur d'Or (1913)
29. La Série Rouge (1913)
30. L'Hôtel du Crime (1913)
31. La Cravate de Chanvre (1913, transl. 2017 by Sheryl Curtis as The Death of Fantomas)
32. La Fin de Fantômas (1913, transl. 2017 by Sheryl Curtis as The Death of Fantomas)


=== By Allain ===
33. Fantômas est-il ressuscité? (1925; transl. 1925 by Alfred Allinson as The Lord of Terror)
34. Fantômas, Roi des Recéleurs (1926; transl. 1926 by Alfred Allinson as Juve in the Dock)
35. Fantômas en Danger (1926; transl. 1926 by Alfred Allinson as Fantômas Captured)
36. Fantômas prend sa Revanche (1926; transl. 1927 by Alfred Allinson as The Revenge of Fantômas)
37. Fantômas Attaque Fandor (1926; transl. 1928 by Alfred Allinson as Bulldog and Rats)
38. Si c'était Fantômas? (1933)
39. Oui, c'est Fantômas! (1934)
40. Fantômas Joue et Gagne (1935)
41. Fantômas Rencontre l'Amour (1946)
42. Fantômas Vole des Blondes (1948)
43. Fantômas Mène le Bal (1963)


=== Notes ===
The original covers by Gino Starace are often considered works of lurid genius in themselves and may be seen at the "Fantômas Lives" site. The first Fantômas book cover, showing a contemplative masked man dressed in evening dress and holding a dagger, boldly stepping over Paris, is so well known that it has become a visual cliché.
The novel The Yellow Document, or Fantômas of Berlin by Marcel Allain (1919), despite its title, is not a Fantômas novel.
The last novel written by Allain was published as a newspaper serial, but never appeared in book form.
During the 1980s, the first two novels of the series were published in revised English translations: Fantômas appeared in 1986 with an introduction by American poet John Ashbery; and Juve contre Fantômas appeared in 1987 under the title The Silent Executioner, with an introduction by American artist Edward Gorey.


== Films ==


=== Silent serials ===
1. Fantômas (1913)
2. Juve Contre Fantômas (1913)
3. Le Mort Qui Tue (1913)
4. Fantômas Contre Fantômas (1914)
5. Le Faux Magistrat (1914)The silent film pioneer Louis Feuillade directed five Fantômas serials starring René Navarre as Fantômas, Bréon as Juve, Georges Melchior as Fandor, and Renée Carl as Lady Beltham. They are regarded as masterpieces of silent film. His later serial Les Vampires, which concerns the eponymous crime syndicate (and not actual vampires) is also reminiscent of the Fantômas series.
There was a 1920 20-episode American Fantômas serial directed by Edward Sedgwick starring Edward Roseman as Fantômas, which bore little resemblance to the French series. In it, Fantômas's nemesis is detective Fred Dixon, played by John Willard. It was partially released in France (12 episodes only) under the title Les Exploits de Diabolos (The Exploits of Diabolos). A novelization of this serial was written by David Lee White for Black Coat Press under the title Fantômas in America in 2007.


=== Other films ===

6. Fantômas (1932), directed by Paul Féjos, with Jean Galland as Fantômas.
7. Monsieur Fantômas (1937), directed by Ernst Moerman is a surrealist silent comedy short subject.
8. Fantômas (1946), directed by Jean Sacha, with Marcel Herrand as Fantomas.
9. Fantomas Against Fantomas (1949), directed by Robert Vernay, with Maurice Teynac as Fantomas.
10. Fantômas (1964), the first of three films directed by André Hunebelle; with Jean Marais as both Fantômas and Fandor, Louis de Funès as Juve, and Mylène Demongeot as Fandor's bride, the photographer Hélène. Their tone was generally much more light-hearted than the novels' and the characters were updated in a James Bond-like style including Bond-type gadgets like the flying Citroën DS of Fantômas with retractable wings that converts to an airplane.
11. Fantômas se déchaîne (1965)
12. Fantômas contre Scotland Yard (1966)Macario Against Zagomar was a 1944 Italian film that used "Zagomar" when they were unable to obtain the rights of Fantomas.


== Television ==
A Fantômas series of four 90-minute episodes was produced in 1980 starring Helmut Berger as Fantômas, Jacques Dufilho as Juve, and Gayle Hunnicutt as Lady Beltham. Episodes 1 and 4 were directed by Claude Chabrol; episodes 2 and 3 by Luis Buñuel's son, Juan Luis Buñuel.
The French movie version of Fantômas appears in the Czechoslovakian 1979-1981 children fantasy series Arabela as well as its sequel series Arabela se vrací, performed respectively by actors František Peterka and Pavel Nový. In this version, he does not perform a villain's role, but becomes an ally and friend of the protagonists. Fantômas also has a cameo appearance in the Czech children's series Lucie, postrach ulice as a TV character, where he resembles his original 1911 book serial covers' depiction.


== Comic books ==


=== French ===
"Fantômas contre les Nains". A weekly color page written by Marcel Allain and drawn by Santini was published in Gavroche #24-30 (1941). This series was interrupted because of censorship; a sequel, Fantômas et l'Enfer Sous-Marin was written but not published.
A daily "Fantômas" strip drawn by Pierre Tabary was syndicated by Opera Mundi from November 1957 to March 1958 (192 strips in total), adapting the first two novels.
Seventeen Fantômas fumetti magazines adapting books 1, 2, 3, and 5 were published by Del Duca in 1962 and 1963.
A new weekly "Fantômas" color page, written by Agnès Guilloteau and drawn by Jacques Taillefer, was again syndicated by Opera Mundi in 1969 and published in Jours de France.
Finally, a series of Fantômas graphic novels written by L. Dellisse and drawn by Claude Laverdure were published by Belgian publisher Claude Lefrancq: L'Affaire Beltham (1990), Juve contre Fantômas (1991), and Le Mort qui Tue (1995).


=== Mexican ===
During the 1960s the Mexican comics publisher Editorial Novaro produced a Fantomas, La Amenaza Elegante (Fantomas, the Elegant Menace) comic book series that became popular throughout Latin America. This was apparently meant to be the same character, although rewritten as a hero, and with no acknowledgement to the original French books or films. Perhaps as a way to make the original French character more attuned to Latin American audiences who crave justice avengers in fiction and national politics. It is not known if this was done with or without legal permission.
This Fantômas was a thief who committed spectacular robberies just for the thrill of it, and wore a white skintight mask all the time or a variety of disguises so his true face was never shown to his nemeses. The character was also pursued by the authorities, in his case mainly by a French police inspector named Gerard. His mask in the Latin American version – which was clearly inspired by the black mask worn by the Italian comic book criminal Diabolik — and his use of it, seems to have been influenced by the popular images generated by Mexican wrestling.
Apparently the series was also influenced by the James Bond movies, as Fantômas, equipped with advanced technology created by a scientist called Professor Semo, had all kind of adventures around the world, and even fought other, more cruel criminals. The Latin American Fantômas, created in Mexico, encompassed the aesthetic of both the British James Bond and American Hugh Hefner, who created Playboy magazine, with a necessary dose of traditional Latin American machismo. A mixture that immediately gained traction with vast Latino audiences, mostly prepubescent and adolescents males.
Latino Fantômas also was a millionaire, owning several corporations under assumed identities, had secret headquarters outside Paris, and was assisted by several secret agents, including the 12 "Zodiac Girls", beautiful women who assisted him personally and dressed provocatively, known only by their codenames – the signs of the zodiac. The attractive female element was another concession to Latin American audiences familiar with beautiful women as part of male-dominated environments playing key roles without hiding their femininity and preceding Charlie's Angels by decades.
Although cancelled years ago (Novaro folded in 1985, and a character revival by rival Grupo Editorial Vid in Mexico in the 1990s did not last long), it is from this Mexican comic that the character is best known in both Central America and South America. Fantômas continues to be one of Latin America's favorite comic characters. For more information on this version of the character check the link to the Fantomas Lives website below.


=== American ===
A Fantômas short story by Paul Kupperberg and Roy Mann appeared in Captain Action Comics No. 1, published in 2009 by Moonstone Books.


== New Fiction ==
"Yes Virginia, There is a Fantomas" by William Patrick Maynard was published in Tales of the Shadowmen Volume 6, published in 2009 by Black Coat Press.  Fantomas has appeared in several stories in the series.
The first third of the novella, "Reign of Terror" by R. Allen Leider was published in Awesome Tales No. 3, published in 2016 by Bold Venture Press. The second third of the novella was published in Awesome Tales No. 9, with the final third coming.


== Cultural influence ==
The Fantômas novels and the subsequent films were highly regarded by the French avant-garde of the day, particularly by the surrealists. Blaise Cendrars called the series "the modern Aeneid"; Guillaume Apollinaire said that "from the imaginative standpoint Fantômas is one of the richest works that exist." The painter René Magritte, the surrealist poet and novelist Robert Desnos and the cubist painter Juan Gris produced works alluding to Fantômas.
The films were also popular in the Soviet Union. After their success Fantômas had a short appearance in two popular Soviet comedies: Seven Old Men and One Girl (1968) and Grandads-Robbers (1972).  In 2007, Russian author Andrey Shary published the book Sign F: Fantomas in Books and on the Screen, dealing in particular with this phenomenon.


== Pastiches, homages, and related characters ==
Fantômas may well have been influenced by its less well remembered predecessor, Zigomar, the creation of Léon Sazie, which first appeared as a serial in Le Matin in 1909, then as a pulp magazine (28 issues) in 1913, and again in Zigomar contre Zigomar for eight more issues in 1924.
In France alone, Fantômas spawned numerous imitators. Among those are Arnould Galopin's Tenebras, Gaston René's Masque Rouge, Arthur Bernède's Belphégor, R. Collard's Demonax and Marcel Allain's own Tigris, Fatala, Miss Teria and Ferocias.
The 1915 adventure film Filibus was probably inspired largely by the popularity of Fantômas, although the film's title character, a female air pirate, strongly resembles Arsène Lupin.
After the success of the first Fantômas film serial directed by Louis Feuillade, Gaumont produced another serial directed by Feuillade in the same style, Les Vampires, about a gang of criminals. As Feuillade had been criticized for glorifying outlaws, his next serial for Gaumont, Judex, starred this time a positive hero, a mysterious avenger conceived as an honest version of Fantômas. Judex was himself featured in various adaptations, sequels and remakes. The original Judex serial was released in the United States and appears to have been an inspiration for the American pulp character The Shadow, who was himself an inspiration for Batman.
Fantômas has appeared in an unauthorized fashion in two French stage plays: Nick Carter vs. Fantômas (1910) by Alexandre Bisson and Guillaume Livet (translated, ISBN 978-1-934543-05-4) and Sherlock Holmes vs. Fantômas (La Mort d' Herlock Sholmes, ou Bandits en Habits Noirs, 1914) by Pierre de Wattyne and Yorril Walter (translated, ISBN 978-1-934543-67-2).
A number of Italian dark villains of the 1960s were clearly influenced by Fantômas. Among the most famous are Diabolik, Kriminal, Killing, and Satanik.
The British film critic and writer Kim Newman has argued that Fantômas inspired the Pink Panther film series starring Peter Sellers. In the initial 1963 Pink Panther film, Fantômas was transformed into Sir Charles Lytton (the Phantom), and Inspector Juve became Inspector Clouseau.
Fantômas also has many plot points in common with the 1969 Hong Kong movie Temptress of a Thousand Faces.
Paperinik, (Duck Avenger) alter-ego of Donald Duck created by Guido Martina and Giovan Battista Carpi in 1969, is partly based on Fantômas. His predecessor, Fantomius, (Fantomallard) was obviously also named after Fantômas. In France, the character is known as Fantomiald; in Germany as Phantomias; in Greek as Phantom Duck and in Spain as Patomas.
Fantômas inspired Julio Cortázar's 1975 novella Fantomas contra los vampiros multinacionales.
Fantômas' grandson, Fantoma Mark III, serves as an antagonist for Arsène Lupin III (grandson of Arsène Lupin) in two episodes of the Red Jacket installment of the Lupin III franchise, wherein his appearance and portrayal are comparable to Jean Marais' version of the character.
A character in the Doctor Who novel The Man in the Velvet Mask (1996) is named Fantômas.
Fantomas was the inspiration for the character Mr. Ixnay, who appears in The Chuckling Whatsit, a 1997 graphic novel by writer/artist Richard Sala, creator of Invisible Hands, an animated homage to old-fashioned mystery thrillers which aired on MTV.
In 1999, former Faith No More singer Mike Patton named his metal group Fantômas after the fictional character.
There is a Marvel Comics character named Fantomex, first appearing in August 2002. He was created by Grant Morrison and Igor Kordey for the title New X-Men.
In one of the back-story sections of the graphic novel The League of Extraordinary Gentlemen, Volume II, Fantômas is described as being a member of "Les Hommes Mystérieux", the French counterpart of the League, alongside Arsène Lupin, the sky-pirate Robur, and the Nyctalope. In the follow-up, The League of Extraordinary Gentlemen: Black Dossier, the League's encounter with Les Hommes is halted once Fantômas detonates a bomb which destroys the Opera Garnier, after saying in unaccented English, "I win".
The character of Spectrobert in Gahan Wilson's book Everybody's Favorite Duck is a direct parody of Fantômas.
Fantômas is a partial inspiration for the character of Phantom Limb on the Adult Swim cartoon The Venture Bros. His last name, in fact, is Fantômas. A picture similar in appearance to the masked villain can be seen hanging on the wall of his office at State University in "The Invisible Hand of Fate". It is revealed that Phantom Limb is actually the grandson of the famous criminal. Fantômas himself briefly appears in the series during a flashback in the season 3 episode "ORB" as one of the original members of the Guild of Calamitous Intent.
Fantômas has a German cousin in the person of the (supposedly British) Lord Lister.
In the story "Fantômville" by Nick Campbell, in the book Wildthyme in Purple, Fantomas meets the time traveller Iris Wildthyme.
In the film adaptation of Alan Moore and Kevin O'Neill's The League of Extraordinary Gentlemen, the Fantom (a character resembling the title character from The Phantom of the Opera) owes more than a little to Fantômas, although he is ultimately revealed to be M, who in turn is revealed to be Professor Moriarty.
In the Egyptian movie, “Romantic Chase”, produced in 1968, Fantômas is played by Egyptian actor Hasan Mustafa. The role of Fantômas is a butler that wants to steal all the money of his employer.  The film itself is an adaptation of What's New Pussycat? which was produced in 1965.
In the Czech Fantasy TV series Arabela  and the sequel Arabela Returns (Arabela se vrací), the French movie version of Fantomas is one of the residents in the World of Fairy Tales.


== References ==


== Further reading ==
Priestman, Martin (2003). The Cambridge Companion to Crime Fiction. Cambridge: Cambridge University Press. pp. 71–72. ISBN 0-521-00871-9.
Green, Martin (1991). Seven Types of Adventure Tale: An Etiology of a Major Genre. Penn State Press. pp. 197–198. ISBN 0-271-02729-0.


== External links ==
Fantômas works at Project Gutenberg
French Wold Newton Universe – Fantômas
Picture of the Fantômas Citroën DS with retractable wings that converts to an airplane via the Internet Archive
Centenary of the birth of the character of Fantômas: February 10, 2011 (in French)
 Fantômas public domain audiobook at LibriVox