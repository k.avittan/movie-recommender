Aryan () is, originally, a term used as a self-designation by Indo-Iranian peoples in ancient times, in contrast to "non-Indo-Aryan" or "non-Iranian" peoples. In Ancient India, the term ā́rya- was used by the Indo-Aryan speakers of the Vedic period as a religious label for themselves, as well as the geographic region known as Āryāvarta, where the Indo-Aryan culture emerged. Similarly, ancient Iranian peoples used the term airya- as an ethnic label for themselves and in reference to their mythical homeland, Airyanem Vaejah, in the Avesta scriptures. The root also forms the etymological source of the place names Iran and Alania.

Although the root *h₂er(y)ós ("a member of one’s own group", in contrast to an outsider) is most likely of Proto-Indo-European (PIE) origin, the use of Arya as an ethno-cultural self-designation is only attested among Indo-Iranian peoples, and it is not known if PIE speakers had a term to designate themselves as a group. In any case, scholars point out that, even in ancient times, the idea of being an "Aryan" was religious, cultural and linguistic, not racial.


== Etymology ==
The English word "Aryan" (originally spelt "Arian") was borrowed from the Sanskrit word ā́rya (आर्य) during the 18th century. It is thought to be the self-designation used by all Indo-Iranian people in ancient times.


=== Indo-Iranian ===

Archeologist J.P. Mallory argues that "As an ethnic designation, the word [Aryan] is most properly limited to the Indo-Iranians".In early Vedic literature, the term Āryāvarta (Sanskrit: आर्यावर्त, abode of the Aryans) was the name given to northern India, where the Indo-Aryan culture was based. The Manusmṛti (2.22) gives the name Āryāvarta to "the tract between the Himalaya and the Vindhya ranges, from the Eastern (Bay of Bengal) to the Western Sea (Arabian Sea)". Initially the term was used as a national name to designate those who worshipped the Vedic deities (especially Indra) and followed Vedic culture (e.g. performance of sacrifice, Yajna).The Avestan term airya ('venerable') and the Old Persian ariya are also derivates of *aryo-, and were likewise used as self-designations. In Iranian languages, the original self-identifier lives on in ethnic names like "Alans" and "Iron". Similarly, the name of Iran is the Persian word for the land or place of the Aryans.


=== Proto-Indo-Iranian ===
The Sanskrit term comes from proto-Indo-Iranian *arya- or *aryo-, the name used by the Indo-Iranians to designate themselves.
Indo-Iranian: *arya-,
Avestan airya- meaning Aryan, Iranian in the larger sense,
Old Indo-Aryan ari- meaning attached to, faithful, devoted person and kinsman; aryá- meaning kind, favourable, attached to and devoted; árya- meaning Aryan, faithful to the Vedic religion.


=== Proto-Indo-European ===
It has been argued that the word is derived from the theorized Proto-Indo-European language, deriving from the reconstructed root word *h₂er(y)ós ("members of one's own group, peer, freeman"). Cognates may include:

the Hittite prefix arā- meaning member of one's own group, peer, companion and friend;
Celtic *aryo-, "freeman",Gaulish: arios, "freeman, lord",
Old Irish: aire, "freeman, noble, chief",
Old Norse: arjosteR, "noble, most distinguished" (perhaps from Germanic *arjaz),The word *h₂er(y)ós itself is believed to have come from the root *h₂er- meaning "put together". The original meaning in Proto-Indo-European had a clear emphasis on the "in-group status" as distinguished from that of outsiders, particularly those captured and incorporated into the group as slaves. While in Anatolia, the base word has come to emphasize personal relationship, in Indo-Iranian the word has taken a more ethnic meaning.A review of numerous other ideas, and the various problems with each is given by Oswald Szemerényi. According to Szemerényi it is probably a Near-Eastern loanword from the Ugaritic ary, kinsmen.


== Modern usage ==


=== Demographics ===
Aryan continues to be used for demographics as part of ethnolinguistic groups referring to Indo-Aryan and Iranian populations.

Indo-Aryan peoples: Refers to the population speaking an Indo-Aryan language or identifying as Indo-Aryan peoples. Indo-Aryans form the predominant ethno-linguistic group in Northern India. Indo-Aryan, or locally used as Aryan, is used as an ethnic demographic alongside other South Asian ethnic groups such as Dravidian peoples, Tibeto-Burman and Tai peoples, Scheduled Castes and Scheduled Tribes, etc. These ethno-linguistic groups are often used to attribute caste and ethnicity, and are still implemented in the Reservation in India and the Quota system in Pakistan. There are estimated to be ~1.3 billion people that identify as Indo-Aryan today.
Iranian peoples: Iranian or Iranic derives from Aryan, which continues to be used as an ethnic demographic in Greater Iran. Iranian peoples such as Persians, Pashtuns, Baloch people, etc. are the main ethnolinguistic group in Iran with sizable populations in bordering regions. There is an estimated 200 million people identifying as Iranian peoples.


=== Scholarship ===
Proto-Indo-Europeans: during the 19th century, it was proposed that "Aryan" was also the self-designation of the Proto-Indo-Europeans, a hypothesis that has been abandoned.
"Aryan language family": the Indo-Aryan languages (including the Dardic), Iranian languages and Nuristani languages,


=== Racial Theory: White supremacy and Nazism ===
Drawing on misinterpreted references in the Rigveda by Western scholars in the 19th century, the term "Aryan" was adopted as a racial category by French writer Arthur de Gobineau, whose ideology of race was based on an idea of blond northern European "Aryans" who had migrated across the world and founded all major civilizations, before being diluted through racial mixing with local populations. Through the works of Houston Stewart Chamberlain, Gobineau's ideas later influenced the Nazi racial ideology which saw the "Aryan race" as innately superior to other putative racial groups. The atrocities committed in the name of this racial ideology have led academics to avoid the term "Aryan", which has been replaced in most cases by "Indo-Iranian", with only the South Asian branch still being called "Indo-Aryan".
During the 19th century it was proposed that "Aryan" was also the self-designation of the Proto-Indo-Europeans. Based on speculations that the Proto-Indo-European homeland was located in northern Europe, a 19th-century hypothesis which is now abandoned, the word developed a racialist meaning.The Nazis used the word "Arier" meaning "Aryan" to describe people in a racial sense. The Nazi official Alfred Rosenberg believed that the Nordic race was descended from Proto-Aryans, who he believed had prehistorically dwelt on the North German Plain and who had ultimately originated from the lost continent of Atlantis. According to Nazi racial theory, the term "Aryan" described the Germanic peoples. However, a satisfactory definition of "Aryan" remained problematic during Nazi Germany.The Nazis considered the purest Aryans to be those that belonged to the "Nordic race" physical ideal, known as the "master race" during Nazi Germany. Although the physical ideal of the Nazi racial theorists was typically the tall, fair-haired and light-eyed Nordic individual, such theorists accepted the fact that a considerable variety of hair and eye colour existed within the racial categories they recognised. For example, Adolf Hitler and many Nazi officials had dark hair and were still considered members of the Aryan race under Nazi racial doctrine, because the determination of an individual's racial type depended on a preponderance of many characteristics in an individual rather than on just one defining feature.In September 1935, the Nazis passed the Nuremberg Laws. All Aryan Reich citizens were required to prove their Aryan ancestry, one way was to obtain an Ahnenpass by providing proof through baptismal certificates that all four grandparents were of Aryan descent.In December 1935, the Nazis founded Lebensborn to counteract the falling Aryan birth rates in Germany, and to promote Nazi eugenics.


=== Name ===
Aryan (name), including derivatives such as Arya, Ariyan, Aria, and Aaryan, continues to be used as given names and surnames in South Asia and Iran. There has also been a rise in names associated with Aryan in the West, which has been popularized due to pop culture.  According to the U.S. Social Security Administration in 2012, "Arya" was the fastest-rising girl's name in popularity in the U.S., jumping from 711th to 413th position. The name entered the top 200 most commonly used names for baby girls born in England and Wales in 2017. Although this could also be attributed to an increase of South Asian and Iranian diasporas immigrating into the West.


== Usage and adaptation in other languages ==


=== In Sanskrit literature ===
In Sanskrit and related Indo-Aryan languages, ārya means "one who does noble deeds; a noble one". Āryāvarta ("abode of the āryas") is a common name for the northern Indian subcontinent in Sanskrit literature. Manusmṛti (2.22) gives the name to "the tract between the Himalaya and the Vindhya ranges, from the Eastern Sea to the Western Sea". The title ārya was used with various modifications throughout the Indian Subcontinent. Kharavela, the Emperor of Kalinga in second century BCE, is referred to as an ārya in the Hathigumpha inscriptions of the Udayagiri and Khandagiri Caves in Bhubaneswar, Odisha. The Gurjara-Pratihara rulers in the 10th century were titled "Maharajadhiraja of Āryāvarta". Various Indian religions, chiefly Hinduism, Jainism and Buddhism, use the term ārya as an epithet of honour; a similar usage is found in the name of Arya Samaj.
The usage of self-designation Arya was not limited to north India. The south indian emperor of Chola kingdom Rajendra Chola I gives himself the title Aryaputra (son of arya).
In Ramayana and Mahabharata, ārya is used as an honorific for many characters including Hanuman.


=== In Avesta and Persian literature ===
Unlike the several meanings connected with ārya- in Old Indo-Aryan, the Old Persian term only has an ethnic meaning. That is in contrast to Indo-Aryan usage, in which several secondary meanings evolved, the meaning of ar- as a self-identifier is preserved in Iranian usage, hence the word "Iran". The airya meant "Iranian", and Iranian anairya  meant and means "non-Iranian". Arya may also be found as an ethnonym in Iranian languages, e.g., Alan and Persian Iran and Ossetian Ir/Iron The name is itself equivalent to Aryan, where Iran means "land of the Aryans," and has been in use since Sassanid times.The Avesta clearly uses airya/airyan as an ethnic name (Vd. 1; Yt. 13.143-44, etc.), where it appears in expressions such as airyāfi; daiŋˊhāvō "Iranian lands, peoples", airyō.šayanəm "land inhabited by Iranians", and airyanəm vaējō vaŋhuyāfi; dāityayāfi; "Iranian stretch of the good Dāityā", the river Oxus, the modern Āmū Daryā. Old Persian sources also use this term for Iranians. Old Persian which is a testament to the antiquity of the Persian language and which is related to most of the languages/dialects spoken in Iran including modern Persian, the Kurdish languages, Balochi, and Gilaki makes it clear that Iranians referred to themselves as Arya.
The term "Airya/Airyan" appears in the royal Old Persian inscriptions in three different contexts:

As the name of the language of the Old Persian version of the inscription of Darius I in Behistun
As the ethnic background of Darius I in inscriptions at Naqsh-e-Rostam and Susa (Dna, Dse) and Xerxes I in the inscription from Persepolis (Xph)
As the definition of the God of the Aryans, Ahura Mazdā, in the Elamite language version of the Behistun inscription.For example in the Dna and Dse Darius and Xerxes describe themselves as "An Achaemenian, A Persian son of a Persian and an Aryan, of Aryan stock". Although Darius the Great called his language the Aryan language, modern scholars refer to it as Old Persian because it is the ancestor of modern Persian language.The Old Persian and Avestan evidence is confirmed by the Greek sources. Herodotus in his Histories remarks about the Iranian Medes that: "These Medes were called anciently by all people Arians; " (7.62). In Armenian sources, the Parthians, Medes and Persians are collectively referred to as Aryans. Eudemus of Rhodes apud Damascius (Dubitationes et solutiones in Platonis Parmenidem 125 bis) refers to "the Magi and all those of Iranian (áreion) lineage"; Diodorus Siculus (1.94.2) considers Zoroaster (Zathraustēs) as one of the Arianoi.
Strabo, in his Geography, mentions the unity of Medes, Persians, Bactrians and Sogdians:The name of Ariana is further extended to a part of Persia and of Media, as also to the Bactrians and Sogdians on the north; for these speak approximately the same language, with but slight variations.
The trilingual inscription erected by Shapur's command gives us a more clear description. The languages used are Parthian, Middle Persian and Greek. In Greek the inscription says: "ego ... tou Arianon ethnous despotes eimi" which translates to "I am the king of the Aryans". In the Middle Persian Shapour says: "I am the Lord of the EranShahr" and in Parthian he says: "I am the Lord of AryanShahr".The Bactrian language (a Middle Iranian language) inscription of Kanishka the Great, the founder of the Kushan Empire at Rabatak, which was discovered in 1993 in an unexcavated site in the Afghanistan province of Baghlan, clearly refers to this Eastern Iranian language as Arya.
In the post-Islamic era one can still see a clear usage of the term Aryan (Iran) in the work of the 10th-century historian Hamzah al-Isfahani. In his famous book "The History of Prophets and Kings", al-Isfahani writes, "Aryan which is also called Pars is in the middle of these countries and these six countries surround it because the South East is in the hands China, the North of the Turks, the middle South is India, the middle North is Rome, and the South West and the North West is the Sudan and Berber lands". All this evidence shows that the name arya "Iranian" was a collective definition, denoting peoples (Geiger, pp. 167 f.; Schmitt, 1978, p. 31) who were aware of belonging to the one ethnic stock, speaking a common language, and having a religious tradition that centered on the cult of Ahura Mazdā.In Iranian languages, the original self-identifier lives on in ethnic names like "Alans", "Iron". Similarly, The word Iran is the Persian word for land/place of the Aryan.


=== In Latin literature ===
The word Arianus was used to designate Ariana, the area comprising Afghanistan, Iran, North-western India and Pakistan. In 1601, Philemon Holland used 'Arianes' in his translation of the Latin Arianus to designate the inhabitants of Ariana. This was the first use of the form Arian verbatim in the English language. In 1844 James Cowles Prichard first designated both the Indians and the Iranians "Arians" under the false assumption that the Iranians as well as the Indians self-designated themselves Aria. The Iranians did use the form Airya as a designation for the "Aryans," but Prichard had mistaken Aria (deriving from OPer. Haravia) as a designation of the "Aryans" and associated the Aria with the place-name Ariana (Av. Airyana), the homeland of the Aryans. The form Aria as a designation of the "Aryans" was, however, only preserved in the language of the Indo-Aryans.


=== In European languages ===
The term "Aryan" came to be used as the term for the newly discovered Indo-European languages, and, by extension, the original speakers of those languages. In the 19th century, "language" was considered a property of "ethnicity", and thus the speakers of the Indo-Iranian or Indo-European languages came to be called the "Aryan race", as contradistinguished from what came to be called the "Semitic race". By the late 19th century, among some people, the notions of an "Aryan race" became closely linked to Nordicism, which posited Northern European racial superiority over all other peoples. This "master race" ideal engendered both the "Aryanization" programs of Nazi Germany, in which the classification of people as "Aryan" and "non-Aryan" was most emphatically directed towards the exclusion of Jews. By the end of World War II, the word 'Aryan' had become associated by many with the racial ideologies and atrocities committed by the Nazis.
Western notions of an "Aryan race" rose to prominence in late-19th- and early-20th-century racialism, an idea most notably embraced by Nazism. The Nazis believed that the "Nordic peoples" (who were also referred to as the "Germanic peoples") represent an ideal and "pure race" that was the purest representation of the original racial stock of those who were then called the Proto-Aryans. The Nazi Party declared that the "Nordics" were the true Aryans because they claimed that they were more "pure" (less racially mixed) than other people of what were then called the "Aryan people".


== History ==


=== Before the 19th century ===
While the original meaning of Indo-Iranian *arya as a self-designator is uncontested, the origin of the word (and thus also its original meaning) remains uncertain. Indo-Iranian ar- is a syllable ambiguous in origin, from Indo-European ar-, er-, or or-. No evidence for a Proto-Indo-European (as opposed to Indo-Iranian) ethnic name like "Aryan" has been found. The word was used by Herodotus in reference to the Iranian Medes whom he describes as the people who "were once universally known as Aryans".The meaning of 'Aryan' that was adopted into the English language in the late 18th century was the one associated with the technical term used in comparative philology, which in turn had the same meaning as that evident in the very oldest Old Indo-Aryan usage, i.e. as a (self-) identifier of "(speakers of) Indo-Aryan languages". This usage was simultaneously influenced by a word that appeared in classical sources (Latin and Greek Ἀριάνης Arianes, e.g. in Pliny 1.133 and Strabo 15.2.1–8), and recognized to be the same as that which appeared in living Iranian languages, where it was a (self-)identifier of the "(speakers of) Iranian languages". Accordingly, 'Aryan' came to refer to the languages of the Indo-Iranian language group, and by extension, native speakers of those languages.


=== Avestan ===
The term Arya is used in ancient Persian language texts, for example in the Behistun inscription from the 5th century BCE, in which the Persian kings Darius the Great and Xerxes are described as "Aryans of Aryan stock" (arya arya chiça). The inscription also refers to the deity Ahura Mazda as "the god of the Aryans", and to the ancient Persian language as "Aryan". In this sense the word seems to have referred to the elite culture of the ancient Iranians, including both linguistic, cultural and religious aspects.  The word also has a central place in the Zoroastrian religion in which the "Aryan expanse" (Airyana Vaejah)  is described as the mythical homeland of the Iranian people's and as the center of the world.


=== Vedic Sanskrit ===
The term Arya is used 36 times in 34 hymns in the Rigveda.
According to Talageri (2000, The Rig Veda. A Historical Analysis)
"the particular Vedic Aryans of the Rigveda were one section among these Purus, who called themselves Bharatas." Thus it is possible, according to Talageri, that at one point Arya did refer to a specific tribe.
While the word may ultimately derive from a tribal name, already in the Rigveda it appears as a religious distinction, separating those who sacrifice "properly" from those who do not belong to the historical Vedic religion, presaging the usage in later Hinduism where the term comes to denote religious righteousness or piety. In RV 9.63.5, ârya "noble, pious, righteous" is used as contrasting with árāvan "not liberal, envious, hostile":

índraṃ várdhanto aptúraḥ kṛṇvánto víśvam âryam apaghnánto árāvṇaḥ
"[the Soma-drops], performing every noble work, active, augmenting Indra's strength, driving away the godless ones." (trans. Griffith)


=== Sanskrit epics ===
Arya and Anarya are primarily used in the moral sense in the Hindu Epics. People are usually called Arya or Anarya based on their behaviour. Arya is typically one who follows the Dharma. This is historically applicable for any person living anywhere in Bharata Varsha or vast India.
According to the Mahabharata, a person's behaviour (not wealth or learning) determines if he can be called an Arya.


=== Religious use ===
The word ārya is often found in Hindu, Buddhist, and Jain texts. In the Indian spiritual context, it can be applied to Rishis or to someone who has mastered the four noble truths and entered upon the spiritual path. According to Indian leader Jawaharlal Nehru, the religions of India may be called collectively ārya dharma, a term that includes the religions that originated in the Indian subcontinent (e.g. Hinduism, Buddhism, Jainism and possibly Sikhism).


==== Hinduism ====
"O my Lord, a person who is chanting Your holy name, although born of a low family like that of a Chandala, is situated on the highest platform of self-realization. Such a person must have performed all kinds of penances and sacrifices according to Vedic literatures many, many times after taking bath in all the holy places of pilgrimage. Such a person is considered to be the best of the Arya family" (Bhagavata Purana 3.33.7).
"My dear Lord, one's occupational duty is instructed in Śrīmad-Bhāgavatam and Bhagavad-gītā according to Your point of view, which never deviates from the highest goal of life. Those who follow their occupational duties under Your supervision, being equal to all living entities, moving and nonmoving, and not considering high and low, are called Āryans. Such Āryans worship You, the Supreme Personality of Godhead." (Bhagavata Purana 6.16.43).
According to Swami Vivekananda, "A child materially born is not an Arya; the child born in spirituality is an Arya." He further elaborated, referring to the Manu Smriti: "Says our great law-giver, Manu, giving the definition of an Arya, 'He is the Arya, who is born through prayer.' Every child not born through prayer is illegitimate, according to the great law-giver: The child must be prayed for. Those children that come with curses, that slip into the world, just in a moment of inadvertence, because that could not be prevented – what can we expect of such progeny?..."(Swami Vivekananda, Complete Works vol.8)
Swami Dayananda founded a Dharmic organisation Arya Samaj in 1875. Sri Aurobindo published a journal combining nationalism and spiritualism under the title Arya from 1914 to 1921.


==== Buddhism ====

The word ārya (Pāli: ariya), in the sense of "noble" or "exalted", is very frequently used in Buddhist texts to designate a spiritual warrior or hero, which use this term much more often than Hindu or Jain texts. Buddha's Dharma and Vinaya are the ariyassa dhammavinayo. The Four Noble Truths are called the catvāry āryasatyāni (Sanskrit) or cattāri ariyasaccāni (Pali). The Noble Eightfold Path is called the āryamārga (Sanskrit, also āryāṣṭāṅgikamārga) or ariyamagga (Pāli).
In Buddhist texts, the ārya pudgala (Pali: ariyapuggala, "noble person") are those who have the Buddhist śīla (Pāli sīla, meaning "virtue") and who have reached a certain level of spiritual advancement on the Buddhist path, mainly one of the four levels of awakening or in Mahayana Buddhism, a bodhisattva level (bhumi). Those who despise Buddhism are often called "anāryas".


==== Jainism ====
The word ārya is also often used in Jainism, in Jain texts such as the Pannavanasutta.


=== 19th century ===
In the 19th century, linguists still supposed that the age of a language determined its "superiority" (because it was assumed to have genealogical purity). Then, based on the assumption that Sanskrit was the oldest Indo-European language, and the (now known to be untenable) position that Irish Éire was etymologically related to "Aryan", in 1837 Adolphe Pictet popularized the idea that the term "Aryan" could also be applied to the entire Indo-European language family as well. The groundwork for this thought had been laid by Abraham Hyacinthe Anquetil-Duperron. In particular, German scholar Karl Wilhelm Friedrich Schlegel published in 1819 the first theory linking the Indo-Iranian and the German languages under the Aryan group. In 1830 Karl Otfried Müller used "Arier" in his publications.


=== Theories of Aryan invasion ===
Translating the sacred Indian texts of the Rig Veda in the 1840s, German linguist Friedrich Max Muller found what he believed to be evidence of an ancient invasion of India by Hindu Brahmins a group he described as "the Arya". Muller was careful to note in his later work that he thought Aryan was a linguistic category rather than a racial one. Nevertheless, scholars used Muller's invasion theory to propose their own visions of racial conquest through South Asia and the Indian Ocean. In 1885, the New Zealand polymath Edward Tregear argued that an "Aryan tidal-wave" had washed over India and continued to push south, through the islands of the East Indian archipelago, reaching the distant shores of New Zealand. Scholars such as John Batchelor, Armand de Quatrefages, and Daniel Brinton extended this invasion theory to the Philippines, Hawaii, and Japan, identifying indigenous peoples who they believed were the descendants of early Aryan conquerors.In the 1850s Arthur de Gobineau supposed that "Aryan" corresponded to the suggested prehistoric Indo-European culture (1853–1855, Essay on the Inequality of the Human Races). Further, de Gobineau believed that there were three basic races – white, yellow and black – and that everything else was caused by race miscegenation, which de Gobineau argued was the cause of chaos. The "master race", according to de Gobineau, were the Northern European "Aryans", who had remained "racially pure". Southern Europeans (to include Spaniards and Southern Frenchmen), Eastern Europeans, North Africans, Middle Easterners, Iranians, Central Asians, Indians, he all considered racially mixed, degenerated through the miscegenation, and thus less than ideal. 
By the 1880s a number of linguists and anthropologists argued that the "Aryans" themselves had originated somewhere in northern Europe. A specific region began to crystallize when the linguist Karl Penka (Die Herkunft der Arier. Neue Beiträge zur historischen Anthropologie der europäischen Völker, 1886) popularized the idea that the "Aryans" had emerged in Scandinavia and could be identified by the distinctive Nordic characteristics of blond hair and blue eyes. The distinguished biologist Thomas Henry Huxley agreed with him, coining the term "Xanthochroi" to refer to fair-skinned Europeans (as opposed to darker Mediterranean peoples, who Huxley called "Melanochroi").

This "Nordic race" theory gained traction following the publication of Charles Morris's The Aryan Race (1888), which touches racist ideology. A similar rationale was followed by Georges Vacher de Lapouge in his book L'Aryen et son rôle social (1899, "The Aryan and his Social Role"). To this idea of "races", Vacher de Lapouge espoused what he termed selectionism, and which had two aims: first, achieving the annihilation of trade unionists, considered "degenerate"; second, the prevention of labour dissatisfaction through the creation of "types" of man, each "designed" for one specific task (See the novel Brave New World for a fictional treatment of this idea).
Meanwhile, in India, the British colonial government had followed de Gobineau's arguments along another line, and had fostered the idea of a superior "Aryan race" that co-opted the Indian caste system in favor of imperial interests. In its fully developed form, the British-mediated interpretation foresaw a segregation of Aryan and non-Aryan along the lines of caste, with the upper castes being "Aryan" and the lower ones being "non-Aryan". The European developments not only allowed the British to identify themselves as high-caste, but also allowed the Brahmins to view themselves as on-par with the British. Further, it provoked the reinterpretation of Indian history in racialist and, in opposition, Indian Nationalist terms.In The Secret Doctrine (1888), Helena Petrovna Blavatsky described the "Aryan root race" as the fifth of seven "Root races", dating their souls as having begun to incarnate about a million years ago in Atlantis. The Semites were a subdivision of the Aryan root race. "The occult doctrine admits of no such divisions as the Aryan and the Semite, ... The Semites, especially the Arabs, are later Aryans — degenerate in spirituality and perfected in materiality. To these belong all the Jews and the Arabs." The Jews, according to Blavatsky, were a "tribe descended from the Tchandalas of India," as they were born of Abraham, which she believed to be a corruption of a word meaning "No Brahmin". Other sources suggest the origin Avram or Aavram.
The name for the Sassanian Empire in Middle Persian is Eran Shahr which means Aryan Empire. In the aftermath of the Islamic conquest in Iran, racialist rhetoric became a literary idiom during the 7th century, i.e., when the Arabs became the primary "Other" – the anaryas – and the antithesis of everything Iranian (i.e. Aryan) and Zoroastrian. But "the antecedents of [present-day] Iranian ultra-nationalism can be traced back to the writings of late nineteenth-century figures such as Mirza Fatali Akhundov and Mirza Aqa Khan Kermani. Demonstrating affinity with Orientalist views of the supremacy of the Aryan peoples and the mediocrity of the Semitic peoples, Iranian nationalist discourse idealized pre-Islamic [Achaemenid and Sassanid] empires, whilst negating the 'Islamization' of Persia by Muslim forces." In the 20th century, different aspects of this idealization of a distant past would be instrumentalized by both the Pahlavi monarchy (In 1967, Iran's Pahlavi dynasty [overthrown in the 1979 Iranian Revolution] added the title Āryāmehr Light of the Aryans to the other styles of the Iranian monarch, the Shah of Iran being already known at that time as the Shahanshah (King of Kings)), and by the Islamic republic that followed it; the Pahlavis used it as a foundation for anticlerical monarchism, and the clerics used it to exalt Iranian values vis-á-vis westernization.


=== 20th century ===

In the United States, the best-selling 1907 book Race Life of the Aryan Peoples by Joseph Pomeroy Widney consolidated in the popular mind the idea that the word "Aryan" is the proper identification for "all Indo-Europeans", and that "Aryan Americans" of the "Aryan race" are destined to fulfill America's manifest destiny to form an American Empire.Gordon Childe would later regret it, but the depiction of Aryans as possessors of a "superior language" became a matter of national pride in learned circles of Germany (portrayed against the background that World War I was lost because Germany had been betrayed from within by miscegenation and the "corruption" of socialist trade unionists and other "degenerates").
Alfred Rosenberg—one of the principal architects of Nazi ideological creed—argued for a new "religion of the blood", based on the supposed innate promptings of the Nordic soul to defend its "noble" character against racial and cultural degeneration. Under Rosenberg, the theories of Arthur de Gobineau, Georges Vacher de Lapouge, Blavatsky, Houston Stewart Chamberlain, Madison Grant, and those of Hitler, all culminated in Nazi Germany's race policies and the "Aryanization" decrees of the 1920s, 1930s, and early 1940s. In its "appalling medical model", the annihilation of the "racially inferior" Untermenschen was sanctified as the excision of a diseased organ in an otherwise healthy body, which led to the Holocaust.

By the end of World War II, the word "Aryan" among a number of people had lost its Romantic or idealist connotations and was associated by many with Nazi racism instead.
By then, the term "Indo-Iranian" and "Indo-European" had made most uses of the term "Aryan" superfluous in the eyes of a number of scholars, and "Aryan" now survives in most scholarly usage only in the term "Indo-Aryan" to indicate (speakers of) North Indian languages. It has been asserted by one scholar that Indo-Aryan and Aryan may not be equated and that such an equation is not supported by the historical evidence, though this extreme viewpoint is not widespread.
The use of the term to designate speakers of all Indo-European languages in scholarly usage is now regarded by some scholars as an "aberration to be avoided." However, some authors writing for popular consumption have continued using the word "Aryan" for "all Indo-Europeans" in the tradition of H. G. Wells, such as the science fiction author Poul Anderson, and scientists writing for the popular media, such as Colin Renfrew.Echoes of "the 19th century prejudice about 'northern' Aryans who were confronted on Indian soil with black barbarians [...] can still be heard in some modern studies." In a socio-political context, the claim of a white, European Aryan race that includes only people of the Western and not the Eastern branch of the Indo-European peoples is entertained by certain circles, usually representing white nationalists who call for the halting of non-white immigration into Europe and limiting immigration into the United States. They argue that a large intrusion of immigrants can lead to ethnic conflicts such as the 2005 Cronulla riots in Australia and the 2005 civil unrest in France.
In recent decades, the idea of an Aryan invasion/migration into India has been disputed mainly by Indian scholars, who claim various alternate Indigenous Aryans scenarios contrary to established Kurgan model. However, these alternate scenarios are rooted in traditional and religious views on Indian history and identity and are universally rejected in mainstream scholarship. According to Michael Witzel, the "indigenous Aryans" position is not scholarship in the usual sense, but an "apologetic, ultimately religious undertaking":
The "revisionist project" certainly is not guided by the principles of critical theory but takes, time and again, recourse to pre-enlightenment beliefs in the authority of traditional religious texts such as the Purånas. In the end, it belongs, as has been pointed out earlier, to a different 'discourse' than that of historical and critical scholarship. In other words, it continues the writing of religious literature, under a contemporary, outwardly 'scientific' guise ... The revisionist and autochthonous project, then, should not be regarded as scholarly in the usual post-enlightenment sense of the word, but as an apologetic, ultimately religious undertaking aiming at proving the "truth" of traditional texts and beliefs. Worse, it is, in many cases, not even scholastic scholarship at all but a political undertaking aiming at "rewriting" history out of national pride or for the purpose of "nation building".


== See also ==
Arya (name)
Airyanem Vaejah
Aryavarta
Ariana
Arya samaj
Graeco-Aryan
Mleccha


== Notes ==


== References ==


=== Bibliography ===


== Further reading ==
"A word for Aryan originality". A. Kammpier.
Arvidsson, Stefan; Wichmann, Sonia (2006), Aryan Idols: Indo-European Mythology as Ideology and Science, University of Chicago Press
Fussmann, G.; Francfort, H.P.; Kellens, J.; Tremblay, X. (2005), Aryas, Aryens et Iraniens en Asie Centrale, Institut Civilisation Indienne, ISBN 2-86803-072-6
Ivanov, Vyacheslav V.; Gamkrelidze, Thomas (1990), "The Early History of Indo-European Languages", Scientific American, 262 (3): 110–116, doi:10.1038/scientificamerican0390-110
Lincoln, Bruce (1999), Theorizing Myth: Narrative, Ideology, and Scholarship, University of Chicago Press
Morey, Peter; Tickell, Alex (2005). Alternative Indias: Writing, Nation and Communalism. Rodopi. ISBN 90-420-1927-1.
Poliakov, Leon (1996), The Aryan Myth: A History of Racist and Nationalistic Ideas in Europe, New York: Barnes & Noble Books, ISBN 0-7607-0034-6
Sugirtharajah, Sharada (2003). Imagining Hinduism: A Postcolonial Perspective. Taylor & Francis. ISBN 978-0-203-63411-0.
Tickell, A (2005), "The Discovery of Aryavarta: Hindu Nationalism and Early Indian Fiction in English",  in Peter Morey; Alex Tickell (eds.), Alternative Indias: Writing, Nation and Communalism, pp. 25–53