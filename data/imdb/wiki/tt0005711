A Man's Prerogative is a lost American 1915 silent drama film directed by George Nichols. The film stars Robert Edeson and Mary Alden as a newlywed couple who must contend with real and perceived infidelity. It was filmed under the working titles Above Reproach and As Ceasar's Wife.


== Synopsis ==
Married couple Oliver, an attorney, and Elizabeth, a writer, each believe that the other is unfaithful. Oliver believes that Elizabeth is cheating with Charles, an artist who illustrates her stories, and Elizabeth believes that Oliver has taken up with Catherine, one of Charles's models. Elizabeth is also frustrated that Oliver believes that she should tolerate his flirtations with Catherine, as men have the right to do as they please. The two almost separate, but Elizabeth refuses as she feels that they should either divorce outright or remain together. The two remain together and maintain the status quo until Elizabeth becomes pregnant. Oliver refuses to believe that it is his child and that Charles is the father.
Meanwhile Catherine has become increasingly jealous of Elizabeth and after Charles toasts the writer, Catherine shoots and kills him. Before he dies, Charles confirms that Elizabeth was never unfaithful. Elizabeth ends up losing the baby but she and Oliver make up and swear not to suspect the other of infidelity again.


== Cast ==
Robert Edeson as Oliver
Mary Alden as Elizabeth
Charles Clary as Charles
Billie West as Catherine


== Reception ==
In a 1915 review for A Man's Prerogative in Motography, Charles R. Congdon praised the movie for comprehensively covering the topic of double standards for men and women. The Houston Daily Post also praised this element in their review for the film.


== References ==


== External links ==
A Man's Prerogative on IMDb
A Man's Prerogative at the American Film Institute Catalog