"There was an Old Woman Who Lived in a Shoe" is a popular English language nursery rhyme, with a Roud Folk Song Index number of 19132. Debates over its meaning and origin have largely centered on attempts to match the old woman with historical female figures who have had large families, although King George II (1683–1760) has also been proposed as the rhyme's subject. 


== Lyrics ==
The most common version of the rhyme is:

The earliest printed version in Joseph Ritson's Gammer Gurton's Garland in 1794 has the coarser last line:

Many other variations were printed in the 18th and 19th centuries. Marjorie Ainsworth Decker published a Christian version of the rhyme in her The Christian Mother Goose Book published in 1978:


== Origins and meaning ==

Iona and Peter Opie pointed to the version published in Infant Institutes in 1797, which finished with the lines:

The term "a-loffeing", they believe, was Shakespearean, suggesting that the rhyme is considerably older than the first printed versions. They then speculated that if this were true, it might have a folklore meaning and pointed to the connection between shoes and fertility, perhaps exemplified by casting a shoe after a bride as she leaves for her honeymoon, or tying shoes to the departing couple's car. Archaeologist Ralph Merifield has pointed out that in Lancashire it was the custom for females who wished to conceive to try on the shoes of a woman who had just given birth.Debates over the meaning of the rhyme have largely revolved around matching the old woman with historical figures, as Peter Opie observed "for little reason other than the size of their families". Candidates include Queen Caroline, the wife of King George II (1683–1760), who had eight children, and Elizabeth Vergoose of Boston, who had six children of her own and ten stepchildren. Some evidence suggests the rhyme refers to the wife of Feodor Vassilyev of Shuya, Russia, who reportedly birthed 69 children during her lifetime (1707–c.1782).Albert Jack has proposed a political origin for the rhyme. George II was nicknamed the "old woman", because it was widely believed that Queen Caroline was the real power behind the throne. According to this explanation, the children are the Members of Parliament (MPs) that George was unable to control, the whip refers to the political office of that name – the MP whose role is to ensure that members of his party vote according to the party line – and the bed is the House of Commons, which MPs were required to attend daily. The phrase "gave them some broth without any bread" may refer to George's parsimony in the wake of the South Sea Bubble of 1721, and his attempts to restore his own and the country's finances.


== Notes ==


== References ==


=== Sources ===
Jack, Albert (2008), Pop Goes the Weasel: The Secret Meanings of Nursery Rhymes (ebook), Penguin, ISBN 978-0-14-190930-1
Merridew, Ralph (1987), The Archaeology of Ritual and Magic, Batsford, ISBN 978-0-7134-4870-2
Opie, I.; Opie, P. (1997), The Oxford Dictionary of Nursery Rhymes (2nd ed.), Oxford University Press, ISBN 978-0-19-860088-6