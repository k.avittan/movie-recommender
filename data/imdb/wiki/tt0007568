The Tampa Bay Rays are an American professional baseball team based in St. Petersburg, Florida. The Rays compete in Major League Baseball (MLB) as a member club of the American League (AL) East division. Since its inception, the team's home venue has been Tropicana Field.
Following nearly three decades of unsuccessfully trying to gain an expansion franchise or enticing existing teams to relocate to the Tampa Bay Area, an ownership group led by Vince Naimoli was approved on March 9, 1995.  The Tampa Bay Devil Rays began play in the 1998 Major League Baseball season.
Their first decade of play, however, was marked by futility; they finished in last place in the AL East in all but the 2004 season, when they finished second to last. Following the 2007 season, Stuart Sternberg, who had purchased controlling interest in the team from Vince Naimoli two years earlier, changed the team's name from "Devil Rays" to "Rays", now meant to primarily refer to a burst of sunshine rather than a manta ray, though a manta ray logo remains on the uniform sleeves. The 2008 season saw the Tampa Bay Rays post their first winning season, their first AL East championship, and their first American League Pennant (defeating the rival Boston Red Sox in the ALCS), though they lost to the Philadelphia Phillies in that year's World Series. Since then, the Rays have played in the postseason five more times, winning the American League pennant again in 2020.
The Tampa Bay Rays' chief rivals are the Boston Red Sox and the New York Yankees. Regarding the former, there have been several notable on-field incidents. The Rays also have an intrastate interleague rivalry with the National League (NL)'s Miami Marlins (originally the Florida Marlins), whom they play in the Citrus Series.


== History ==


=== Professional baseball in Tampa Bay ===

Former civic leader and St. Petersburg Times (now Tampa Bay Times) publisher, Jack Lake, first suggested St. Petersburg pursue a Major League baseball team in the 1960s. The notable influences Lake held in the sport are what led to the serious discussions that changed St. Petersburg from a spring training location to a major league city. He spoke to anyone who would listen about his desire to see the city of St. Petersburg have a Major league baseball team. His colorful direction dominated the mindset in both sports and business circles dating back to 1966. He was said to have the foresight and prominence to make it happen.
Local leaders made many unsuccessful attempts to acquire a major league baseball team in the 1980s and 1990s. The Minnesota Twins, Chicago White Sox, Texas Rangers, and Seattle Mariners all considered moving to either Tampa or St. Petersburg before deciding to remain in their current locations. The Florida Suncoast Dome (now named Tropicana Field) was built in St. Petersburg in 1990 with the purpose of luring a major league team. That same year two separate groups, one in Tampa and another in Sarasota, were seeking to get an expansion team. The Tampa group even registered the name "Florida Panthers", after a local feline - a trademark that ended up being purchased by entrepreneur Wayne Huizenga one year later, and used by him to name an NHL ice hockey team.When Major League Baseball announced that it would add two expansion teams for the 1993 season, it was widely assumed that one of the teams would be placed in the Dome. However, in addition to the application from St. Petersburg, a competing group applied to field a team in Tampa, prompting much conflict over the bid.  The two National League teams were awarded to Denver (Colorado Rockies) and Miami (Florida Marlins) instead.
In 1992, San Francisco Giants owner Bob Lurie agreed in principle to sell his team to a Tampa Bay-based group of investors led by Vince Naimoli, who would then move the team to St. Petersburg. However, at the 11th hour, MLB owners nixed the move under pressure from San Francisco officials and the Giants were sold to a group that kept them in San Francisco.Finally, on March 9, 1995, new expansion franchises were awarded to Naimoli's Tampa Bay group and a group from Phoenix (the Arizona Diamondbacks). The new franchises were scheduled to begin play in 1998.
Next for the Tampa Bay franchise was selecting a nickname. Naimoli wanted a name that would include "Rays" somewhere in it. The city and team agreed that the team should be called the Tampa Bay Sting Rays. But there was a problem as the Maui Sting Rays already held the rights to the name and wanted $35,000 to buy the name. Instead, the team opted for a local variety of the ray, the devil ray.
The Tampa Bay area finally had a team, but the stadium in St. Petersburg was already in need of an upgrade. In 1993, the stadium was renamed the Thunderdome and became the home of the Tampa Bay Lightning hockey team and the Tampa Bay Storm Arena Football League team. After the birth of the team, the naming rights were sold to Tropicana Products and $70 million was spent on renovations.


== Season results ==

The records of the Rays' last five seasons in Major League Baseball.

These statistics are current through the 2020 Major League Baseball regular season.


== Rivals ==


=== AL East ===
Tampa Bay's primary rivals are the Boston Red Sox and the New York Yankees. The Red Sox/Rays rivalry dates back to the 2000 season, when Devil Ray Gerald Williams took exception to being hit by a pitch thrown by Boston pitcher Pedro Martínez and charged the mound, resulting in a game full of retaliations and ejections on both sides.  There have been several other incidents between the teams during the ensuing years, including one in 2005 that resulted in two bench-clearing fights during the game and a war of words between then-Devil Rays manager Lou Piniella and then-Boston pitcher Curt Schilling through the media in the following days. The rivalry reached its highest level to date during the 2008 season, including a brawl during a June meeting in Fenway Park and a seven-game American League Championship Series between the teams that ended in the Rays' first ever pennant win.
As a fellow member of the AL East division, the Yankees and Rays play many times each season. There has always been some feeling of a rivalry between the teams because the Yankees make Tampa their spring training, as well as having a minor league team in the Tampa Tarpons; home and fan loyalty in the Tampa Bay area has historically been divided, especially among transplants from the northeastern U.S. The rivalry became more heated in spring training of 2008, when a home plate collision between Rays outfielder Elliot Johnson and Yankees catcher Francisco Cervelli was followed the next day by spikes-high slide by Yankees outfielder Shelley Duncan into Rays' second baseman Akinori Iwamura, prompting Rays outfielder Jonny Gomes to charge in from his position in right field and knock Duncan to the ground. The Rays and Yankees met in postseason for the first time in the 2020 American League Division Series in which Tampa Bay won in five games.


=== Citrus Series ===

The Rays also have a geographical rivalry with the Miami Marlins. Tampa Bay currently leads the series, 59–56.


== Ballparks ==


=== Tropicana Field ===

The Rays have played at Tropicana Field since their inception in 1998. The facility, which was originally called the "Florida Suncoast Dome",  was built in the late 1980s to attract an MLB team through either relocation or expansion. After St. Petersburg was awarded an expansion franchise in 1995, the dome underwent extensive renovations and naming rights were sold to Tropicana Products, which was based in nearby Bradenton.
Tropicana Field underwent further renovations in 2006 and 2007 after Stu Sternberg gained controlling ownership of the team. Most of the changes sought to improve fans' game-day experience. For the players, the biggest change was the installation of a new Field Turf surface in 2007, which was replaced in turn with a new version of AstroTurf for the 2011 season.


=== New ballpark ===

The Rays' current ownership has long hinted that Tropicana Field does not generate enough revenue and that it is too far from the Tampa Bay Area's population center. In 2007, the team announced a plan to build a covered ballpark at the current site of Al Lang Field on the St. Petersburg waterfront, and a local referendum was scheduled to decide on public financing. However, in the face of vocal opposition, the Rays withdrew the proposal in 2009 and stated they had abandoned all plans for a ballpark in downtown St. Petersburg waterfront, preferring a location nearer the center of Pinellas County or across the bay in Tampa.Since 2009, local officials, media, and business leaders have explored possibilities for a new stadium for the Rays somewhere in the Tampa Bay area. However, St. Petersburg mayor Bill Foster has repeatedly insisted that the Rays honor their lease agreement with the city, which runs through 2027 and prohibits the team from entering into talks with other communities, resulting in a protracted stalemate.In October 2014, Sternberg, frustrated with efforts to build a new stadium in the Tampa Bay area, had discussions with Wall Street associates about moving the Rays to Montreal, which has been without a Major League Baseball franchise since the Montreal Expos moved to Washington, D.C. in 2005 to become the Washington Nationals. On December 9, 2014, reports surfaced that owner Stuart Sternberg will sell the team if a new stadium is not built.On February 9, 2018, the team said that Ybor City is their  preferred site for a new stadium. However, at the December 2018 Winter Meetings in Las Vegas, Sternberg announced that plans for the proposed stadium in Ybor fell through, meaning the Rays were still on track to play at Tropicana Field until 2027. Later in December 2018, the team sent a letter to St. Petersburg's mayor, Rick Kriseman, foregoing an extension to search for a new stadium outside of the city. On June 20, 2019, Major League Baseball's executive council gave the team permission to explore playing early-season home games in the Tampa Bay area and later-season home games in Montreal.


== Uniforms ==

The current Rays primary uniform has been used with little change since the team officially shortened its name from "Devil Rays" to "Rays" for the 2008 season. The home jersey is a traditional white with the name "Rays" in dark blue across the chest and a yellow "sunburst" on the letter "R". The Rays' road uniform is gray, also with a sunburst and the team name across the chest. Both feature dark blue piping and caps featuring a white "TB" logo.The Rays' first alternate jersey also features the name "Rays" and a yellow sunburst on chest, but is a dark blue material with Columbia blue piping, white characters for the player name, and player numbers that are simply a white outline. This alternate jersey is worn both at home and on the road with either white or gray pants. The Rays' second alternate jersey is similar, but is a light Columbia blue. This second alternate is usually worn only for Sunday home games with white pants, and as of 2018, is paired with the alternate dark blue "devil ray" cap.
Initially, the dark blue "devil ray" alternate logo was only featured on the right sleeve of the home and away uniforms, but in 2019, the patch was added on both the dark blue and Columbia blue alternates.


=== Past uniforms ===

During their first three seasons, the Devil Rays wore traditional white home and gray road uniforms with the text "Devil Rays" (home) and "Tampa Bay" (road) in an unconventional multicolor "rainbow" across the chest. The inaugural caps were also unusual: black with a purple brim at home and all black on the road, with both versions featuring a devil ray graphic and no letters at all. The caps changed in 1999 to feature a smaller ray and the letters "TB" and were all black for both home and road games. During the 1999 and 2000 seasons, the Devil Rays wore an alternate black jersey featuring the same rainbow text as the white and gray uniforms.
In 2001, the Devil Rays dropped the multicolor text and de-emphasized purple in favor of more green. They also changed the font on their jersey tops and shortened the name on the home whites to read simply "Rays" while keeping "Tampa Bay" on the road grays.In 2005, the home uniforms were again tweaked to include still more green. The primary home whites became a sleeveless jersey worn with green sleeved undershirts, and the primary home caps were changed from black to green. In addition, a small ray with a long tail was added under the name "Rays" on the chest of the home jerseys.


=== "Turn Back the Clock" Nights ===
The Rays staged a "Turn Back the Clock" promotion with a retro theme and throwback uniforms several times early in their existence, and it has become an annual tradition since 2006. Because the franchise does not yet have a long history from which to choose uniforms, they have usually worn the uniforms of other historical local teams.
On these special occasions, Rays have worn the uniforms of the Tampa Tarpons of the Florida State League (in 1999, 2006, and 2010), the St. Petersburg Pelicans of the Senior Professional Baseball Association (in 2008), the St. Petersburg Saints (in 2000 and 2007) and Tampa Smokers (in 2011) of the Florida International League, and the University of Tampa Spartans (in 2000). The Rays have worn their own uniforms for Turn Back the Clock night only once: in 2009, when they wore Devil Rays "rainbow" uniforms from their 1998 inaugural season.For Turn Back the Clock night during the 2012, 2013, and 2014 seasons, the Rays took the field wearing specially designed 1980 Tampa Bay Rays "faux-back" uniforms that represented what the team might have worn had the franchise existed during the late 1970s and early 80s. These uniforms are patterned after the San Diego Padres' uniforms from the late 1970s, only in Rays' team colors.The Rays' opponent on Turn Back the Clock night also wears throwbacks from the same era as the Rays' retro uniforms. For example, the Houston Astros wore their 1980s "Rainbow Guts" uniforms, the New York Mets wore the road uniforms of their 1969 championship team, the Chicago White Sox wore their red and white home uniforms from the 1970s, and the Baltimore Orioles wore their rare all-orange uniforms from the early 1970s. Perhaps the most memorable such game was on June 23, 2007, when the Devil Rays wore St. Pete Saints uniforms from the early 1950s, and the Los Angeles Dodgers wore the gray road uniforms of the World Series-winning 1955 Brooklyn Dodgers to honor Don Zimmer, who played on that Dodger team and was a senior adviser for the Rays prior to his death. Rays management also gave away a bobblehead at the game featuring a young Zimmer in a Dodgers uniform and an older Zimmer in a Devil Rays uniform.


== Team media ==


=== Radio ===
WDAE (620 AM) has been the flagship station of the Rays radio network since 2009. The play-by-play announcers are Dave Wills and Andy Freed with Neil Solondz serving as the pregame and postgame host. Rich Herrera served as the host during pre- and post-game shows for the Tampa Rays Baseball Radio Network from 2005 to 2011.  The (Devil) Rays original radio team consisted of Paul Olden and Charlie Slowes, who broadcast games from 1998 to 2005. Slowes went to the Washington Nationals, where he is now lead announcer, while Olden pursued a photography career before replacing Bob Sheppard as the public address announcer at Yankee Stadium in 2008. Rays games have been aired on WFLA 970 AM (1998–2004) and WHNZ 1250 AM (2005–2008) in the past.
In 2013, the Rays became the second team to enter into a contract to have games broadcast nationally by Compass Media Networks in a Game of the Week format. The broadcast team utilized during the 2013 season was TJ Rives calling play-by-play and a rotating circuit of analysts in Rob Dibble, Jeff Nelson, and Steve Phillips. 22 Rays games were produced nationally by Compass Media for the 2013 season.


=== Television ===
Fox Sports Sun broadcasts the Rays' games on television. Through the 2008 season, many games also aired on Ion Television affiliate broadcast stations throughout the state of Florida, with WXPX-TV in Tampa as the flagship. However, after the 2008 season, Fox Sports signed an agreement to become the exclusive local broadcaster of the Rays, and will air 155 games per year through 2016. Fox Sports Florida began broadcasting a portion of the schedule in HD beginning in 2007 after Tropicana Field's broadcast equipment was upgraded for in-house HD production. Most Rays home games are now broadcast in HD.
Dewayne Staats (play-by-play) and former MLB pitcher Brian Anderson (color commentary) are the TV voices of the Rays. For the first 11 seasons of the franchise, Staats teamed with former MLB pitcher Joe Magrane on the Rays' TV broadcasts. Magrane departed after conclusion of the 2008 season to take a position at the MLB Network. Former minors catcher and MLB manager Kevin Kennedy then served as the primary color commentator in 2009 and 2010, with Brian Anderson filling in on some road trips, after which Anderson took over as the everyday commentator from 2011.
Early on, as Staats' first wife was battling cancer, Paul Olden would occasionally fill in for Staats. As a result, Paul Olden ended up calling Wade Boggs' 3,000th hit.


=== Awards ===
Staats, Magrane, Wills, Olden and Slowes have all been nominated for the Ford C. Frick Award, the broadcasters' path to the Baseball Hall of Fame.


=== Promotions ===
Bobbleheads 2013  This year the Rays released several bobbleheads.  The first was released on April 22 and featured David Price's dog "Astro". On April 24 the Rays released the famous "Joe Gnome" bobblehead. May 24–26 saw the release of the Fernando Rodney bobblehead in his famous shooting-the-arrow position. On July 6 the team released the Evan Longoria retro bobblehead.Bobbleheads 2012  The first bobblehead released was Kyle Farnsworth on May 24.  June 3 saw the release of a Desmond Jennings Bobblehead.  Coach Joe Maddon saw his bobblehead released on June 15.  On June 29 the Rays released the whimsical Zim Bear. Although this was not a bobblehead, the likeness of long-time coach Don Zimmer proved to be a fan favorite.  July saw the release of two bobbleheads with James Shields on July 1 and Matt Moore on July 22.  The last bobblehead was Matt Joyce on September 23.


=== The Rookie ===
The Tampa Bay Devil Rays were featured in the movie, The Rookie, a 2002 drama  directed by John Lee Hancock. It is based on the true story of pitcher Jim Morris, who had a brief but famous Major League Baseball career with the team.
Morris was a 35-year-old high school baseball coach who could repeatedly throw a baseball 98 mph (158 km/h), an ability that only a few major leaguers could equal at the time. He was persuaded to try out for professional ballclubs and signed with the Tampa Bay Devil Rays organization. Morris was initially assigned to the minor league Class AA Orlando Rays (now the Montgomery Biscuits), but quickly moved up to the AAA Durham Bulls and was called up to the "Bigs" during the September 1999 roster expansions.
Jim Morris spent parts of two seasons with the Tampa Bay Devil Rays as a reliever, pitching 15 innings in 21 games, with an earned run average of 4.80 and no decisions.


== Rays fandom ==

Although widespread support has been slow to build with the lack of success in its first ten seasons, it has taken some cues from the powerhouses of the American League East. Whereas Red Sox fans are referred to as Red Sox Nation, the Orioles fan base is referred to Birdland, and Yankee fans are referred to as Yankees Universe (and the team itself being called the "Evil Empire"), the Rays have adopted the term Rays Republic for their fan base. The team has also had its fair share of notable fans and outrageous fan traditions over the years.


=== More Cowbell ===
The Rays' Cowbell was originally a promotional idea thought up by principal owner Stuart Sternberg, who got the idea from the Saturday Night Live sketch. Since then, it has become a standard feature of home games, something akin to the Sacramento Kings of the NBA and the bells their fans ring during games. Road teams have often considered the cowbell a nuisance. Once a year the Rays hold an annual "cowbell night" and give away free cowbells. Cowbells are available for purchase throughout the year as well. The cowbells are rung most prominently when the opposing batter has two strikes, when the opposing fans try to chant, and when the Rays make a good play.


=== DJ Kitty ===
Whenever the game situation calls for a rally, the 'DJ Kitty' appears on the large video screen in Tropicana Field. A large anthropomorphic cat, wearing a Rays jersey, appears on the screen wielding a turntable similar to those used by rap DJs. Loud music is played over the PA system while the arrival of the DJ Kitty is proclaimed on display boards throughout the ballpark. The character was created in 2010 by Rays entertainment director Lou Costanza in an attempt to rally the Rays players and the fans at Tropicana Field.


=== Professional wrestlers ===
Rays games are frequently visited by professional wrestlers, as there are a large number of wrestlers living in the Tampa Bay Area. The Nasty Boys (Brian Knobs and Jerry Sags), Brutus Beefcake, and Hulk Hogan all appear on a semi-regular basis at Rays games. John Cena appears on occasion.
The Rays held a "Legends of Wrestling Night" on May 18, 2007, featuring several wrestling matches after the game, an 8–4 loss to the Florida Marlins. Outfielder and wrestling fan Jonny Gomes ran interference for the Nasty Boys during the main event.A second "Wrestling Night" was held on April 19, 2008, after a 5–0 win over the Chicago White Sox. Gomes participated again, this time making a post-match save for the Nasty Boys.


=== Mohawks ===
During the playoffs, Rays players and coaches sport mohawk haircuts, nicknamed "rayhawks". The trend started during their 2008 World Series run.


== Team slogans ==


=== 2008 ===

The mantra 9=8 (spoken as "nine equals eight") was used by the Rays during the 2008 season. The phrase was originally created by manager Joe Maddon while riding his bike after the 2007 season. The meaning of the phrase was that if nine players play nine innings of hard baseball every day, that team would become one of the eight teams who qualify for the postseason. Prior to 2008 season, the Rays had never had a winning season in franchise history, much less a postseason appearance.
After a slow start to the 2008 season, where they lost nine of their first 15 games, the Rays began to pick up speed and found themselves among the best teams in the league that year. Maddon had blue T-shirts made with the phrase on the back in yellow, representing the team's new colors, and gave them to the players during the season. His idea to put the slogan on the back of the shirt, rather than the front, was that a person who was walking behind someone wearing the shirt would see it.Rays right fielder Gabe Gross, who was acquired by the team through a trade early into the 2008 campaign, said that as much as it was 9=8, it was more along the lines that 13=8, because the Rays had many players contributing to the team's success that season.The Rays played well enough throughout the year that they surpassed their previous team record for wins in a single season by more than 30 wins, and ultimately clinched the AL East division title for their first postseason appearance in franchise history. As the phrase 9=8 had come to fruition, Maddon stated that the phrase also meant that theory and reality had come together.With each level the Rays reached, the equation was changed. After they clinched their postseason spot, it became 9=4, to represent the teams advancing to the LCS. When they won the ALDS, it became 9=2, for the teams advancing to the World Series. When they won the ALCS, it became 9=1, representing the possible World Series Championship. In the end, they did not win the World Series, losing to the Philadelphia Phillies four games to one.


=== 2009 ===
A week before Spring training for the 2009 season, Maddon introduced a new slogan, '09 > '08. The meaning of his new idea was that he doesn't like to use the words "great" or "greater", but would rather the phrase be spoken as "better than." His only problem was that there is no symbol for "better than." Originally thinking about creating a new symbol to mean "better than", he admitted that he didn't want to get "too nuts", so the symbol for greater than would have to do. Re-emphasizing that 9 would always equal 8 in the Rays' math, the upcoming season would be greater than the previous. He wanted the players to understand that "in order to build this new road we have to be better than we were last year." Unfortunately for the Rays, 2009 was not better than 2008. Though they finished the season in 3rd place with an 84–78 record, making it the second best season in franchise history, they failed to reach the postseason.


=== 2010 ===

For the 2010 season, another slogan was created. Unlike the previous two seasons, this slogan did not involve any sort of math. The slogan was WIN, an acronym that stood for What's Important Now?, with the message being "stay in the moment." In his explanation of the slogan, Maddon credited Ken Ravizza, the performance consultant of the Rays and a sports psychologist, as the creator. Maddon stated, "It's always about staying in the present tense and having a higher state of awareness." GTMI became another notable slogan during the year, standing for Get The Man In (though it is reported a player has used a "more colorful" term to take place of the word "man"), referring to an in-game situation in which the Rays had runners in scoring position. Historically, the team had a habit of stranding runners on third base with less than two outs. In practices during the 2010 season, the Rays would run the "get-the-man-in drill" to improve situational hitting. Derek Shelton, who came into the season as the team's new hitting coach, taught that batters should not look for a pitch they could hit for a home run, but one that they could hit well enough to score runners.


== Roster ==


== Minor league affiliations ==

The Tampa Bay Rays farm system consists of eight minor league affiliates.


== Awards, league leaders, and individual records ==


=== Baseball Hall of Famers ===


=== Florida Sports Hall of Fame ===


=== Retired numbers ===

The Tampa Bay Rays have retired three numbers. These numbers are displayed to the left of the center field scoreboard and "K Counter" on a small wall.
Jackie Robinson's number 42 was retired by all of Major League Baseball.


=== Selected individual franchise single-season records ===
Statistics below are through the end of the 2018 season.

Highest batting average: .325, Jeff Keppinger (2012)
Most games: 162, Aubrey Huff (2003), Evan Longoria (2014), and Delmon Young (2007)
Most hits: 198, Aubrey Huff (2003)
Highest slugging %: .627, Carlos Peña (2007)
Most doubles: 47, Aubrey Huff (2003)
Most triples: 19, Carl Crawford (2004)
Most home runs: 46, Carlos Peña (2007)
Most RBIs: 121, Carlos Peña (2007)
Most stolen bases: 60, Carl Crawford (2009)
Most wins: 21, Blake Snell (2018)
Lowest ERA: 1.89, Blake Snell (2018)
Strikeouts: 252, Chris Archer (2015)
Complete games: 11, James Shields (2011)
Shutouts: 4, James Shields (2011)
Saves: 48, Fernando Rodney (2012)


== Team salaries ==
Opening Day payrolls for 25-man roster (since 1998):


== Footnotes ==
a The Finish column lists regular season results and excludes postseason play.
b The Wins and Losses columns list regular season results and exclude any postseason play. Regular and postseason records are combined only at the bottom of the list.
c The GB column lists "Games Back" from the team that finished in first place that season. It is determined by finding the difference in wins plus the difference in losses divided by two.
d ALDS stands for American League Division Series.
e ALCS stands for American League Championship Series.
f CPOY stands for Comeback Player of the Year
g CYA stands for Cy Young Award.
h MOY stands for Manager of the Year.
j ROY stands for American League Rookie of the Year.


== See also ==
Baseball awards
List of MLB awards
Tampa Bay Rays all-time roster
Ted Williams Museum and Hitters Hall of Fame (including Tampa Bay Rays exhibit)


== References ==


== External links ==
Tampa Bay Rays official website
Tampa Bay Times coverage of the Tampa Bay Rays