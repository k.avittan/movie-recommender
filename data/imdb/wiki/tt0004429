On the Border is the third studio album by American rock group the Eagles, released in 1974. Apart from two songs produced by Glyn Johns, it was produced by Bill Szymczyk because the group wanted a more rock‑oriented sound instead of the country-rock feel of the first two albums. It is the first Eagles album to feature guitarist Don Felder. On the Border reached number 17 on the Billboard album chart and has sold two million copies.
Three singles were released from the album: "Already Gone", "James Dean" and "Best of My Love". The singles peaked at numbers 32, 77 and 1 respectively. "Best of My Love" became the band's first of five chart toppers. The album also includes "My Man", Bernie Leadon's tribute to his deceased friend Gram Parsons. Leadon and Parsons had played together in the pioneer country rock band Flying Burrito Brothers, before Leadon joined the Eagles.
This is the first album by the Eagles to be released in Quadraphonic surround sound. It was released on Quadraphonic 8-track tape and CD-4 LP. A hidden message carved into the run out groove of some vinyl LPs reads: "He who hesitates is lunch".


== Background ==
The album was initially produced by Glyn Johns and recorded at Olympic Studios in London, but during the making of the album, disagreement arose between the Eagles and their producer. As the band tried to lean towards a more hard rock sound, they felt that producer Glyn Johns was overemphasizing their country-influenced rock sound. Johns however felt that the Eagles were not capable of what the band wanted and told the band: "You are not a rock-and-roll band, The Who is a rock-and-roll band, and you're not that."  The band—Glenn Frey in particular, but not Don Henley—were also unhappy with the no-drug policy of Johns during the recording; furthermore they did not feel at home recording in London. The band was concerned about the lack of success of the previous album Desperado, and were more assertive in wanting more input into the album, which Johns was unwilling to allow. The Eagles spent six weeks recording in London, with both the band and the producer becoming frustrated with each other and frequent arguments between Johns and Frey. The band then took a break, decided to find a new producer and discarded all the recordings except for two usable tracks, "Best of My Love" and "You Never Cry Like a Lover".The band relocated back to California and hired Bill Szymczyk, who had produced The Smoker You Drink, the Player You Get by Joe Walsh—who was also managed by their manager Irving Azoff and who would go on to join the Eagles in late 1975—that interested the band. The band recorded the rest of the album at the Record Plant Studios in Los Angeles. They were allowed more input in how the album was made and enjoyed more freedom with Szymczyk in the making of the album. Szymczyk suggested they bring in a harder-edged guitarist to add slide guitar to the song "Good Day in Hell". Bernie Leadon suggested his old friend Don Felder, whom they had met and jammed with on a few occasions. The band was so impressed that they invited Felder to become the fifth Eagle. The only other track on this album on which he appeared was "Already Gone". They credited him as a late arrival on the album's liner notes.
On the difference in sound between Johns' and Szymczyk's productions, Henley said: "There’s a lot less echo with Bill, for one thing. There’s more of a raw and funky presence. Glyn had a stamp he put on his records which is a deep echo that is really smooth like ice cream". He thought that the production on the two songs that Johns produced was good and necessary. Frey, however, found that L.A. country-rock records were "all too smooth and glassy", and wanted a "tougher sound". Their friend and collaborator J. D. Souther ascribed the change of producer to "Eagles’ desire to get more of a live, thin sound on the albums".The first two singles released were more rock-oriented; Frey was reluctant to release the Johns-produced "Best of My Love" as a single, and held off its release for some months. However, when it was finally released, the label had truncated the song–without the band's knowledge or approval–so that it would be more radio-friendly. "Best of My Love" would become their biggest hit thus far, and their first No. 1 on the charts.


== Songs ==
"Already Gone", "James Dean", and "Best of My Love" were released as singles from the album, and information on these songs can be found in their respective articles.  The following are other noteworthy tracks from the album:


=== "My Man" ===
Bernie Leadon's "My Man" is a tribute to Gram Parsons, who had died of a drug overdose in September 1973. Leadon and Parsons had been members of the pioneering country-rock band The Flying Burrito Brothers. In the lyrics, Leadon makes reference to Parsons' song "Hickory Wind" ("like a flower he bloomed till that old hickory wind called him home") which appeared on the Byrds' groundbreaking country-rock album Sweetheart of the Rodeo in 1968, the only Byrds album Parsons appeared on. 


=== "On the Border" ===
A hard rock song, the track was inspired by the Watergate scandal and fears of the government overstepping its bounds and infringing on people's privacy. Barely audible at the end of the song, Glenn Frey can be heard whispering "Say Goodnight, Dick," a line made famous by Dan Rowan of Rowan and Martin but in this case referring to Richard Nixon's resignation. Nixon would indeed resign five months after the release of the album.  Henley however judged the song they wrote a "clumsy, incoherent attempt" as they were still learning how to write. According to Henley, the song was supposed to be "an R&B-type song" but missed the mark.In the liner notes, T.N.T.S. was credited for the vocals on the track – it refers to Tanqueray 'n' tonic, a drink favored by the producer Szymczyk. According to Henley, the drink "“helped out“ on the hand-clap overdub and the Temptations-like background vocals on the title track" by adding an element of spontaneity to the song.


=== "Ol' 55" ===
David Geffen played to Glenn Frey a demo of three songs by Tom Waits; Frey loved this song and took it to Henley suggesting that they split the vocals on the song. Frey said: "It’s such a car thing. Your first car is like your first apartment. You had a mobile studio apartment! “Ol’ 55” was so Southern California, and yet there was some Detroit in it as well. It was that car thing, and I loved the idea of driving home at sunrise, thinking about what had happened the night before."


=== "Good Day in Hell" ===
According to Henley, the song was written by Frey as a tribute to Danny Whitten and Gram Parsons. He also described the song as another of their "running commentaries on the perils of the music business and the lifestyle that often comes with it".


== Critical reception ==
In an early review, Janet Maslin of Rolling Stone found the album "competent and commercial", but was disappointed that it did not live up to the potential for bigger things shown in Desperado. She also thought that with three guitarists in the band, there were "just too many intrusive guitar parts here, too many solos that smack of gratuitous heaviness. Many of the arrangements seem to lose touch with the material somewhere in mid-song." Overall, she judged the album "a tight and likable collection, with nine potential singles working in its favor and only one dud ("Midnight Flyer") to weigh it down," and that it's "good enough to make up in high spirits what it lacks in purposefulness."
William Ruhlmann of AllMusic noted in his retrospective review the R&B direction in its title track that would be pursued more fully in later albums, and considered the album "which looked back to their earlier work and anticipated their later work" to be "a transitional effort that combined even more styles than most of their records did."


== Commercial performance ==
The album became the band's most successful album of the three released thus far. It debuted at number 50 on the US Billboard 200 chart in its first week of its release, peaking at number 17 in its sixth week on the chart. The album was certified Gold by the Recording Industry Association of America (RIAA) two and a half months after its release June 5, 1974, and it was then certified double platinum for shipments of 2 million copies on March 20, 2001, the album.


== Track listing ==


== Personnel ==


=== Eagles ===
Glenn Frey – vocals; acoustic rhythm guitar; electric rhythm guitar; lead guitar; slide guitar; piano
Don Henley – vocals, drums
Bernie Leadon – vocals; lead, acoustic and rhythm guitars; banjo; pedal steel
Randy Meisner – vocals, bass guitar
Don Felder – lead guitar on “Already Gone” and slide guitar on “Good Day in Hell” (credited as "late arrival")Additional musician

Al Perkins – pedal steel guitar (on "Ol' 55")


=== Technical ===
Bill Szymczyk – producer, engineer
Glyn Johns – producer and engineer on "You Never Cry Like a Lover" and "Best of My Love"
Rod Thaer, Gary Ladinsky, Allan Blazek – engineers
Lee Hulko – mastering
Beatian Yazz – painting
Henry Diltz – photography
Rick Griffin – lettering
Gary Burden – art direction, design


== Charts ==


== Certifications ==


== References ==