A sea captain, ship's captain, captain, master, or shipmaster, is a high-grade licensed mariner who holds ultimate command and responsibility of a merchant vessel. The captain is responsible for the safe and efficient operation of the ship‍—‌including its seaworthiness, safety and security, cargo operations, navigation, crew management, and legal compliance‍—‌and for the persons and cargo on board.


== Duties and functions ==
The captain ensures that the ship complies with local and international laws and complies also with company and flag state policies. The captain is ultimately responsible, under the law, for aspects of operation such as the safe navigation of the ship, its cleanliness and seaworthiness, safe handling of all cargo, management of all personnel, inventory of ship's cash and stores, and maintaining the ship's certificates and documentation.One of a shipmaster's particularly important duties is to ensure compliance with the vessel's security plan, as required by the International Maritime Organization's ISPS Code. The plan, customized to meet the needs of each individual ship, spells out duties including conducting searches and inspections, maintaining restricted spaces, and responding to threats from terrorists, hijackers, pirates, and stowaways. The security plan also covers topics such as refugees and asylum seekers, smuggling, and saboteurs.On ships without a purser, the captain is in charge of the ship's accounting. This includes ensuring an adequate amount of cash on board, coordinating the ship's payroll (including draws and advances), and managing the ship's slop chest.On international voyages, the captain is responsible for satisfying requirements of the local immigration and customs officials. Immigration issues can include situations such as embarking and disembarking passengers, handling crew members who desert the ship, making crew changes in port, and making accommodations for foreign crew members. Customs requirements can include the master providing a cargo declaration, a ship's stores declaration, a declaration of crew members' personal effects, crew lists and passenger lists.The captain has special responsibilities when the ship or its cargo are damaged, when the ship causes damage to other vessels or facilities. The master acts as a liaison to local investigators and is responsible for providing complete and accurate logbooks, reports, statements and evidence to document an incident. Specific examples of the ship causing external damage include collisions with other ships or with fixed objects, grounding the vessel, and dragging anchor. Some common causes of cargo damage include heavy weather, water damage, pilferage, and damage caused during loading/unloading by the stevedores.All persons on board including public authorities, crew, and passengers are under the captain's authority and are his or her ultimate responsibility, particularly during navigation. In the case of injury or death of a crew member or passenger, the master is responsible to address any medical issues affecting the passengers and crew by providing medical care as possible, cooperating with shore-side medical personnel, and, if necessary, evacuating those who need more assistance than can be provided on board the ship.


=== Performing marriages ===
There is a common belief that ship captains have historically been, and currently are, able to perform marriages. This depends on the country of registry, however most do not permit performance of a marriage by the master of a ship at sea.
In the United States Navy, a captain's powers are defined by its 1913 Code of Regulations, specifically stating: "The commanding officer shall not perform a marriage ceremony on board his ship or aircraft. He shall not permit a marriage ceremony to be performed on board when the ship or aircraft is outside the territory of the United States." However, there may be exceptions "in accordance with local laws and the laws of the state, territory, or district in which the parties are domiciled" and "in the presence of a diplomatic or consular official of the United States, who has consented to issue the certificates and make the returns required by the consular regulations."
Furthermore, in the United States, there have been a few contradictory legal precedents: courts did not recognize a shipboard marriage in California's 1898 Norman v. Norman but did in New York's 1929 Fisher v. Fisher (notwithstanding the absence of municipal laws so carried) and in 1933's Johnson v. Baker, an Oregon court ordered the payment of death benefits to a widow because she had established that her marriage at sea was lawful. However, in Fisher v. Fisher the involvement of the ship's captain was irrelevant to the outcome. New Jersey's 1919 Bolmer v. Edsall said a shipboard marriage ceremony is governed by the laws of the nation where ownership of the vessel lies.
In the United Kingdom, the captain of a merchant ship has never been permitted to perform marriages, although from 1854 any which took place had to be reported in the ship's log.Spanish and Filipino law, as narrow exceptions, recognise a marriage in articulo mortis (on the point of death) solemnized by the captain of a ship or chief of an aeroplane during a voyage, or by the commanding officer of a military unit.
Japan allows ship captains to perform a marriage ceremony at sea, but only for Japanese citizens. Malta, Bermuda and the Bahamas permit captains of ships registered in their jurisdictions to perform marriages at sea. Princess Cruises, whose ships are registered in Bermuda, has used this as a selling point for their cruises, while Cunard moved the registration of its ships Queen Mary 2, Queen Victoria and Queen Elizabeth from Southampton to Bermuda in 2011 to allow marriages to be conducted on their ships.Some captains obtain other credentials (such as ordination as ministers of religion or accreditation as notaries public), which allow them to perform marriages in some jurisdictions where they would otherwise not be permitted to do so. Another possibility is a wedding on a ship in port, under the authority of an official from that port.
In works of fiction, ship captains have performed marriages in various media, including the 1951 film The African Queen, and episodes of The Love Boat, How I Met Your Mother, The Office (U.S. TV series) and various Star Trek series.


== Licensing ==


=== United States ===

To become a master of vessels of any gross tons upon oceans in the United States, one must first accumulate at least 360 days of service (Recency – 90 days in the past three years on vessels of appropriate tonnage) while holding a chief mate's license.  The chief mate's license, in turn, requires at least 360 days of service (Recency – 90 days in the past three years on vessels of appropriate tonnage) while holding a second mate's license, passing a battery of examinations, and approximately 13 weeks of classes. Similarly, one must have worked as a third mate for 360 days (Recency – 90 days in the past three years on vessels of appropriate tonnage) to have become a second mate.
There are two methods to attain an unlimited third mate's license in the United States: to attend a specialized training institution, or to accumulate "sea time" and take a series of training classes and examinations.Training institutions that can lead to a third mate's license include the U.S. Merchant Marine Academy (deck curriculum), and the six state maritime academies in Maine, Massachusetts, New York, Texas, or California or the Great Lakes Maritime Academy, or a three-year apprentice mate training program approved by the Commandant of the U.S. Coast Guard. Furthermore, third mate's licenses can be obtained through the U.S. Coast Guard Academy and the U.S. Naval Academy with approved courses and requisite sea time as an Officer in Charge of a Navigational Watch.
A seaman may start the process of obtaining a license after three years of service in the deck department on ocean steam or motor vessels, at least six months of which as able seaman, boatswain, or quartermaster. Then the seaman takes required training courses, and completes on-board assessments. Finally, the mariner can apply to the United States Coast Guard for a third mate's license.
An alternate method of obtaining a license as a master of vessels of any gross tons upon oceans, without sailing as a third, second, or chief mate, is to obtain one year of sea service as a 1st class pilot of any gross tons or mate of vessels of any gross tons upon Great Lakes and inland waters. Then pass an examination for the license of master of vessels of any gross tons upon Great Lakes and inland waters. A master of vessels of any gross tons upon Great Lakes and inland waters may, without any additional sea service, take the examination for master of vessels of any gross tons upon near coastal waters. If the candidate does not already have sufficient deep sea experience he may with six months of additional sea service, in any licensed capacity, take a partial examination consisting primarily of celestial navigation and have the near coastal restriction removed.
46CFR 11.403
A master of 1,600 ton vessels can, under certain circumstances, begin the application process for an unlimited third mate's license.Some employers offer financial assistance to pay for the training for their employees. Otherwise, the mariner is responsible for the cost of the required training. A Chief Mate to Master formal training generally takes about 12 weeks and provides the knowledge, skills and other soft skills training to take on the duties and responsibilities.Various US states require and issue shipmaster or captain licenses in order to be employed in operating a vessel for hire, while navigating within "non-federal" waters. (Such as a lake or river charter boat "skipper"). Most states honor a USCG master's certificate as an alternative to their state licensing. These state licenses certify that the captain has given satisfactory evidence that they can safely be entrusted with the duties and responsibilities of operating or navigating passenger carrying vessels of the tonnage and upon the waters specified. The state licensed captains command vessels that range from small uninspected vessels to large excursion vessels that carry over 100 passengers, so the licenses are not issued haphazardly. For example, see Washington State's Certification of Charter Boats and Operators licenses.


== Employment ==


=== United Kingdom ===
As of 2008, the U.K. Learning and Skills Council lists annual salaries for senior deck officers as ranging from £22,000 to over £50,000 per year. The Council characterizes job opportunities for senior deck officers as "generally good" and expects a "considerable increase" in the job market over the next few years.


=== United States ===
As of 2013, captains of U.S.-flagged deep sea vessels make up to US$1,500 per day, or US$80,000 to US$300,000 per year. Captains of smaller vessels in the inland and coastal trade earn between US$350 and US$700 per day, or US$65,000 to $180,000 per year. Captains of large ferries average US$56,794 annually.In 2005, 3,393 mariners held active unlimited master's licenses. 87 held near-coastal licenses with unlimited tonnage, 291 held unlimited tonnage master's licenses on inland and Great Lakes waters, while 1,044 held unlimited licenses upon inland waters only. Some 47,163 active masters licenses that year had tonnage restrictions, well over half of those being for near-coastal vessels of up to 100 tons gross tonnage.As of 2006, some 34,000 people were employed as captains, mates, and pilots of water vessels in the United States. The U.S. Bureau of Labor Statistics projects 18% growth in this occupation, expecting demand for 40,000 shipmasters in 2016.


== Uniform ==

Uniforms are worn aboard many ships, or aboard any vessels of traditional and organized navigation companies, and are required by company regulation on passenger and cruise vessels.
In the passenger-carrying trade a unified corporate image is often desired and it is useful for those unfamiliar with the vessel to be able to identify members of the crew and their function. Some companies and some countries use an executive curl similar to that of the Royal Navy.
In the United States, and in numerous other maritime countries, captains and officers of shipping companies may wear a merchant navy or merchant marine regular uniform in conjunction with their employment.


== Related terms ==


=== Master mariner ===


=== Captain's seniority ===
In a few countries, such as UK, USA and Italy, some captains with particular experience in navigation and command at sea, may be named commodore or senior captain or shipmaster senior grade.


=== Master ===
The term master is descended from the Latin magister navis, used during the imperial Roman age to designate the nobleman (patrician) who was in ultimate authority on board a vessel. The magister navis had the right to wear the laurus or corona laurèa and the corona navalis. Carrying on this tradition, the modern-day shipmaster of some nations wears golden laurel leaves or golden oak leaves on the visor of his cap.


=== Skipper ===
A skipper is a person who has command of a boat or watercraft or tug, more or less equivalent to "captain in charge aboard ship." At sea, or upon lakes and rivers, the skipper as shipmaster or captain has command over the whole crew. The skipper may or may not be the owner of the boat.
The word is derived from the Dutch word schipper; schip is Dutch for "ship". In Dutch sch- is pronounced [sx] and English-speakers rendered this as [sk].
The word "skipper" is used more than "captain" for some types of craft, for example fishing boats.
It is also more frequently used than captain with privately owned noncommercial or semi-commercial vessels, such as small yachts and other recreational boats, mostly in cases where the person in command of the boat may not be a licensed or professional captain, suggesting the term is less formal. In the U.S., a "skipper" who is in command of a charter vessel that carries paying passengers must be licensed by a state or the USCG. If the vessel carries over six paying passengers, it must be an "inspected vessel" and a higher class license must be obtained by the skipper/master depending on the vessel's gross tons.
In the Royal Navy, Royal Marines, U.S. Navy, U.S. Marine Corps, U.S. Coast Guard, and merchant naval slang, it is a term used in reference to the commanding officer of any ship, base, or other command regardless of rank. It is generally only applied to someone who has earned the speaker's respect, and only used with the permission of the commander/commanding officer in question.
Skipper RNR was an actual rank used in the British Royal Naval Reserve for skippers of fishing boats who were members of the service. It was equivalent to Warrant Officer. Skippers could also be promoted to Chief Skipper RNR (equivalent to Commissioned Warrant Officer) and Skipper Lieutenant RNR.


== See also ==

The captain goes down with the ship – Maritime tradition
Bottomry
Maritime pilot – Mariner who manoeuvres ships through dangerous or congested waters that are subject to statutory pilotage by virtue of a legal requirement of that territory.
List of sea captains
Pilot in command – Crew position responsible for command of an aircraft and ultimately responsible for all operations of the aircraft
Category:Fictional captains


== Notes ==


== References ==
Aragon, James R.; Messer, Tuuli Anna (2001). Master's handbook on ship's business. Cambridge, Md: Cornell Maritime Press. ISBN 0-87033-531-6.
Bureau of Labor Statistics (U.S.A.) (2014). "Water Transportation Occupations". Occupational Outlook Handbook, 2014-15 Edition. Government Printing Office. Retrieved 2014-08-19.
Commonwealth of Australia (2008). "Ship's Master". Job Guide. Archived from the original on 2009-01-08. Retrieved 2009-03-01.
Hayler, William B. (2003). American Merchant Seaman's Manual. Cornell Maritime Press. ISBN 0-87033-549-9.
International Maritime Organization (1995) [1978]. "II: Standards Regarding the Master and Deck Department". International Convention on Standards of Training, Certification and Watchkeeping for Seafarers, 1978. Section A–II/1.
Learning and Skills Council (2005). "Merchant Navy Deck Officer Job Profile". Careers Advice Website. London: United Kingdom. Archived from the original on 2008-09-17. Retrieved 2008-10-21.
Pelletier, James Laurence (2007). Mariner's Employment Guide. Augusta, Maine: Marine Techniques. ISBN 978-0-9644915-0-2.
Turpin, Edward A.; McEwen, William A. (1980). Merchant Marine Officers' Handbook (4th ed.). Centreville, MD: Cornell Maritime Press. ISBN 0-87033-056-X.


== External links ==
 Media related to Ship captains at Wikimedia Commons
 The dictionary definition of sea captain at Wiktionary