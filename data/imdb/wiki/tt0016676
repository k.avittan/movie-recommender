Born to the West (reissue title Hell Town) is a 1937 American Western film starring John Wayne, Marsha Hunt, and John Mack Brown. Filmed in black and white and based upon a Zane Grey novel, the movie incorporates footage from an earlier and higher budgeted silent version, a common practice of the era. The picture features fast chases, gun-fights, unusual poker gambling, and peppy light dialogue for the love interest.


== Plot ==

Dare Rudd and Dinkey Hooley, roaming cowhands, drift into Montana, where they meet Dare's cousin, Tom Fillmore, cattleman and banker. Tom offers them jobs but they pass, until Dare sees Tom's sweetheart, Judy Worstall and decides to take the job. He is put in charge of a cattle drive, replacing ranch-foreman Lynn Hardy, who is in cahoots with Bart Hammond, rustler. Dare delivers the cattle to the railhead and is about to return when he is persuaded into a poker game by Buck Brady, a crooked gambler. Dare is almost cleaned out when Tom appears and takes a hand and discovers the dealer is switching decks.
On the vast mountainous Montana vista, to the soft strains of a "ride 'em" chorus, horned cattle are quietly herded until raiders divert them. Hearing shots, Dare (Wayne) "This is no time to think" in a tall white hat, and hungry dark-mustached wiry side-kick lightning-rod salesman Dink Hooley (an uncredited Syd Saylor) mis-call "the winning side", add their wild mustangs to confusion and dusty stampede at jerky triple time of original silent film. Rough shrubby terrain provides a dangerous battleground. 
Seemingly safe across border in next state, Wayne's cousin Tom Fillmore (John Mack Brown), local "big man," Bank President, and "shining" good sheep of the family surprises the pair, and offers them a job. "People around here spend too much time thinking"; John just fist fights and proposes while Tom's girl Judy (Marsha Hunt) bandages his eye, "I guess I'll just marry you" he says. She declines to answer, but says, "You've been hurt enough for one day."  When a rattler scares her horse, Tom's somersaults and Dare wins the chase.  
"I wound up the cat and kicked the clock out" is Dare trying to turn a new leaf and be responsible. Judy asks Tom to take the cook's apron off Wayne, so the boss does promote his cousin to foreman of the herding. First night out, rustlers attack – empty blankets "Hope it don't start raining". Dare makes the sale for over $10K, but gets convinced to pay out wages and stay the night to celebrate, proving who is "the best player west of the Mississippi". The bartender serves a deck under the bad guy's tray of drinks, and Dare  loses almost everything.
When Dare is late returning, Tom tells Judy the cost was worth every penny to show her Dare's true nature. She pleads for him to save Dare "You're smart about these things,  smarter than any man I know." He arrives in time to take over playing and catches their trick. Fillmore hands have already left, so the bad guys shoot Tom in the shoulder and pursue the trio. Dink diverts some and catches up to bring the hands back, while the cousins hole up, Dare admitting Tom is the best poker player. Back home after winning the gunfight, Tom tells Judy where Dare is riding out of town, and that he wants to offer him a partnership, so Judy brings the Montana-bound buddies back.


== Cast ==
Onscreen credits do not list roles, in order:

John Wayne as Dare Rudd
Marsha Hunt as Judy Rustoe
John Mack Brown as Tom Fillmore
John Patterson as Lynn Hardy
Monte Blue as Bart Hammond
Lucien Littlefield as Cattle Buyer
Syd Saylor as Dink Hooley (salesman) (uncredited)
Earl Dwire as Cowhand (uncredited)


== See also ==
List of American films of 1937
John Wayne filmography


== External links ==
Born to the West on IMDb
Born to the West is available for free download at the Internet Archive
Official website