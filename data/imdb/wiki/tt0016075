Up the River is a 1930 American pre-Code comedy film directed by John Ford and starring Spencer Tracy, Claire Luce, Warren Hymer, and Humphrey Bogart. The plot concerns escaped convicts. The picture is notable for the feature film debuts of both Tracy and Bogart. Despite the order of billing, Tracy's and Bogart's roles were equally large, and this is the only movie in which they both appeared. Fox remade the film in 1938.


== Plot ==
Two convicts, St. Louis (Spencer Tracy) and Dannemora Dan (Warren Hymer) befriend another convict named Steve (Humphrey Bogart), who is in love with woman's-prison inmate Judy (Claire Luce). Steve is paroled, promising Judy that he will wait for her release five months later. He returns to his hometown in New England and his mother's home.
However, he is followed there by Judy's former "employer", the scam artist Frosby (Gaylord Pendleton). Frosby threatens to expose Steve's prison record if the latter refuses to go along with a scheme to defraud his neighbors. Steve goes along with it until Frosby defrauds his mother. At this moment St. Louis and Dannemora Dan break out of prison and come to Steve's aid, taking away a gun he planned to use on the fraudster, instead stealing back bonds stolen by Frosby. They return to prison in time for its annual baseball game against a rival penitentiary. The film closes with St. Louis on the pitcher's mound with his catcher, Dannemora Dan, presumably ready to lead their team to victory.


== Cast ==
Spencer Tracy as Saint Louis
Claire Luce as Judy Fields
Warren Hymer as Dannemora Dan
Humphrey Bogart as Steve Jordan
Gaylord Pendleton as Frosby
William Collier, Sr. as Pop
Joan Lawes as Jean


== Production ==
Tracy had previously starred in two Warner Bros. shorts earlier the same year and Bogart had been an unbilled extra in a silent film, as well as starring in two shorts. Up the River is the first credited feature film for both actors, and is the only film that Tracy and Bogart ever appeared in together. Both had been cast in  The Desperate Hours in 1955, but neither would consent to second billing, so the role intended for Tracy went to Fredric March instead. Bogart is listed fourth after top-billed Tracy in Up the River but his role is equally large and his likeness is featured prominently on posters that did not include Tracy's image. This is the only film Bogart made with director John Ford.  Nearly three decades later, Ford directed Tracy again in The Last Hurrah (1958).


== References ==


== Bibliography ==
New England Vintage Film Society, Inc. (2008). Spencer Tracy: The Pre-Code Legacy of a Hollywood Legend. Newton, MA: New England Vintage Film Society. ISBN 978-1-4363-4138-7.


== External links ==
Up the River on IMDb
Up the River at AllMovie
Up the River at the TCM Movie Database