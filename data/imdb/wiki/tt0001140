Bullying is the use of force, coercion, or threat, to abuse, aggressively dominate or intimidate. The behavior is often repeated and habitual. One essential prerequisite is the perception (by the bully or by others) of an imbalance of physical or social power. This imbalance distinguishes bullying from conflict. Bullying is a subcategory of aggressive behavior characterized by the following three minimum criteria: (1) hostile intent, (2) imbalance of power, and (3) repetition over a period of time. Bullying is the activity of repeated, aggressive behavior intended to hurt another individual, physically, mentally, or emotionally.
Bullying ranges from one-on-one, individual bullying through to group bullying, called mobbing, in which the bully may have one or more "lieutenants" who are willing to assist the primary bully in their bullying activities. Bullying in school and the workplace is also referred to as "peer abuse". Robert W. Fuller has analyzed bullying in the context of rankism. The Swedish-Norwegian researcher Dan Olweus says bullying occurs when a person is "exposed, repeatedly and over time, to negative actions on the part of one or more other persons", and that negative actions occur "when a person intentionally inflicts injury or discomfort upon another person, through physical contact, through words or in other ways". Individual bullying is usually characterized by a person behaving in a certain way to gain power over another person.A bullying culture can develop in any context in which humans interact with each other. This may include school, family, the workplace, the home, and neighborhoods. The main platform for bullying in contemporary culture is on social media websites. In a 2012 study of male adolescent American football players, "the strongest predictor [of bullying] was the perception of whether the most influential male in a player's life would approve of the bullying behavior."Bullying may be defined in many different ways. In the United Kingdom, there is no legal definition of bullying, while some states in the United States have laws against it. Bullying is divided into four basic types of abuse – psychological (sometimes called emotional or relational), verbal, physical, and cyber.Behaviors used to assert such domination may include physical assault or coercion, verbal harassment, or threat, and such acts may be directed repeatedly toward particular targets. Rationalizations of such behavior sometimes include differences of social class, race, religion, gender, sexual orientation, appearance, behavior, body language, personality, reputation, lineage, strength, size, or ability. If bullying is done by a group, it is called mobbing.


== Etymology ==
The word "bully" was first used in the 1530s meaning "sweetheart", applied to either sex, from the Dutch boel "lover, brother", probably diminutive of Middle High German buole "brother", of uncertain origin (compare with the German buhle "lover"). The meaning deteriorated through the 17th century through "fine fellow", "blusterer", to "harasser of the weak". This may have been as a connecting sense between "lover" and "ruffian" as in "protector of a prostitute", which was one sense of "bully" (though not specifically attested until 1706). The verb "to bully" is first attested in 1710.In the past, in American culture, the term has been used differently, as an exclamation/exhortation, in particular famously associated with Theodore Roosevelt and continuing to the present in the bully pulpit, Roosevelt's coining and also as faint/deprecating praise ("bully for him").


== Types ==
Bullying has been classified by the body of literature into different types. These can be in the form of nonverbal, verbal, or physical behavior. Another classification is based on perpetrators or the participants involved, so that the types include individual and collective bullying. Other interpretation also cite emotional and relational bullying in addition to physical harm inflicted towards another person or even property. There is also the case of the more recent phenomenon called cyberbullying.
Physical, verbal, and relational bullying are most prevalent in primary school and could also begin much earlier while continuing into later stages in individuals lives.


=== Individual ===
Individual bullying tactics are perpetrated by a single person against a target or targets. Individual bullying can be classified into four types outlined below:


==== Physical ====
Physical bullying is any bullying that hurts someone's body or damages their possessions. Stealing, shoving, hitting, fighting, and intentionally destroying someone's property are types of physical bullying. Physical bullying is rarely the first form of bullying that a target will experience. Often bullying will begin in a different form and later progress to physical violence. In physical bullying the main weapon the bully uses is his/her body, or some part thereof, when attacking his/her target. Sometimes groups of young adults will target and alienate a peer because of some adolescent prejudice. This can quickly lead to a situation where they are being taunted, tortured, and "beaten up" by their classmates. Physical bullying will often escalate over time, and can lead to a detrimental ending, and therefore many try to stop it quickly to prevent any further escalation.


==== Verbal ====
Verbal bullying is one of the most common types of bullying. This is any bullying that is conducted by speaking or other use of the voice and does not involve any physical contact. Verbal bullying includes any of the following:

Derogatory name-calling and nicknaming
Spreading rumors or lying about someone
Threatening someone
Yelling at or talking to someone in a rude or unkind tone of voice, especially without justifiable cause
Mocking someone's voice or style of speaking
Laughing at someone
Making insults or otherwise making fun of someoneIn verbal bullying, the main weapon the bully uses is voice. In many cases, verbal bullying is common in both genders, but girls are more likely to perform it. Girls, in general, are more subtle with insults than boys. Girls use verbal bullying, as well as social exclusion techniques, to dominate and control other individuals and show their superiority and power. However, there are also many boys with subtlety enough to use verbal techniques for domination, and who are practiced in using words when they want to avoid the trouble that can come with physically bullying someone else.


==== Relational ====
Relational bullying or sometimes referred to as social aggression is the type of bullying that uses relationships to hurt others. The term also denotes any bullying that is done with the intent to hurt somebody's reputation or social standing which can also link in with the techniques included in physical and verbal bullying. Relational bullying is a form of bullying common among youth, but particularly upon girls. Social exclusion (slighting or making someone feel "left out") is one of the most common types of relational bullying. Relational bullying can be used as a tool by bullies to both improve their social standing and control others. Unlike physical bullying which is obvious, relational bullying is not overt and can continue for a long time without being noticed.


==== Cyberbullying ====
Cyberbullying is the use of technology to harass, threaten, embarrass, or target another person. When an adult is involved, it may meet the definition of cyber-harassment or cyberstalking, a crime that can have legal consequences and involve jail time. This includes bullying by use of email, instant messaging, social media websites (such as Facebook), text messages, and cell phones. It is stated that Cyberbullying is more common in secondary school than in primary school.


=== Collective ===
Collective bullying tactics are employed by more than one individual against a target or targets. Collective bullying is known as mobbing, and can include any of the individual types of bullying. Trolling behavior on social media, although generally assumed to be individual in nature by the casual reader, is sometime organized efforts by sponsored astroturfers.


==== Mobbing ====

Mobbing refers to the bullying of an individual by a group, in any context, such as a family, peer group, school, workplace, neighborhood, community, or online. When it occurs as emotional abuse in the workplace, such as "ganging up" by co-workers, subordinates or superiors, to force someone out of the workplace through rumor, innuendo, intimidation, humiliation, discrediting, and isolation, it is also referred to as malicious, nonsexual, nonracial/racial, general harassment.


== Characteristics ==


=== Of bullies and accomplices ===
Studies have shown that envy and resentment may be motives for bullying. Research on the self-esteem of bullies has produced equivocal results. While some bullies are arrogant and narcissistic, they can also use bullying as a tool to conceal shame or anxiety or to boost self-esteem: by demeaning others, the abuser feels empowered. Bullies may bully out of jealousy or because they themselves are bullied. Psychologist Roy Baumeister asserts that people who are prone to abusive behavior tend to have inflated but fragile egos. Because they think too highly of themselves, they are frequently offended by the criticisms and lack of deference of other people, and react to this disrespect with violence and insults.Researchers have identified other risk factors such as depression and personality disorders, as well as quickness to anger and use of force, addiction to aggressive behaviors, mistaking others' actions as hostile, concern with preserving self-image, and engaging in obsessive or rigid actions. A combination of these factors may also be causes of this behavior. In one study of youth, a combination of antisocial traits and depression was found to be the best predictor of youth violence, whereas video game violence and television violence exposure were not predictive of these behaviors.Bullying may also result from a genetic predisposition or a brain abnormality in the bully. While parents can help a toddler develop emotional regulation and control to restrict aggressive behavior, some children fail to develop these skills due to insecure attachment with their families, ineffective discipline, and environmental factors such as a stressful home life and hostile siblings. Moreover, according to some researchers, bullies may be inclined toward negativity and perform poorly academically. Dr. Cook says, "A typical bully has trouble resolving problems with others and also has trouble academically. He or she usually has negative attitudes and beliefs about others, feels negatively toward himself/herself, comes from a family environment characterized by conflict and poor parenting, perceives school as negative and is negatively influenced by peers."Contrarily, some researchers have suggested that some bullies are psychologically strongest and have high social standing among their peers, while their targets are emotionally distressed and socially marginalized. Peer groups often promote the bully's actions, and members of these peer groups also engage in behaviors, such as mocking, excluding, punching, and insulting one another as a source of entertainment. Other researchers also argued that a minority of the bullies, those who are not in-turn bullied, enjoy going to school, and are least likely to take days off sick.Research indicates that adults who bully have authoritarian personalities, combined with a strong need to control or dominate. It has also been suggested that a prejudicial view of subordinates can be a particularly strong risk factor.Brain studies have shown that the section of the brain associated with reward becomes active when bullies are shown a video of someone inflecting pain on another.


=== Of typical bystanders ===
Often, bullying takes place in the presence of a large group of relatively uninvolved bystanders. In many cases, it is the bully's ability to create the illusion they have the support of the majority present that instills the fear of "speaking out" in protestation of the bullying activities being observed by the group. Unless the "bully mentality" is effectively challenged in any given group in its early stages, it often becomes an accepted, or supported, norm within the group.Unless action is taken, a "culture of bullying" is often perpetuated within a group for months, years, or longer.Bystanders who have been able to establish their own "friendship group" or "support group" have been found to be far more likely to opt to speak out against bullying behavior than those who have not.In addition to communication of clear expectations that bystanders should intervene and increasing individual self-efficacy, there is growing research to suggest interventions should build on the foundation that bullying is morally wrong.Among adults, being a bystander to workplace bullying was linked to depression.


=== Of victims ===
Dr. Cook says, "A typical victim is likely to be aggressive, lack social skills, think negative thoughts, experience difficulties in solving social problems, come from a negative family, school and community environments and be noticeably rejected and isolated by peers." Victims often have characteristics such as being physically and mentally weak, as well as being easily distraught emotionally. They may also have physical characteristics that make them easier targets for bullies such as being overweight or having some type of physical deformity. Boys are more likely to be victims of physical bullying while girls are more likely to be bullied indirectly.The results of a meta-analysis conducted by Cook and published by the American Psychological Association in 2010 concluded the main risk factors for children and adolescents being bullied, and also for becoming bullies, are the lack of social problem-solving skills.Children who are bullied often show physical or emotional signs, such as: being afraid to attend school, complaining of headaches or a loss of appetite, a lack of interest in school activities and spending time with friends or family, and having an overall sense of sadness.


== Effects ==
Mona O'Moore of the Anti-Bullying Centre at Trinity College in Dublin, has written, "There is a growing body of research which indicates that individuals, whether child or adult, who are persistently subjected to abusive behavior are at risk of stress related illness which can sometimes lead to suicide."
Those who have been the targets of bullying can suffer from long term emotional and behavioral problems. Bullying can cause loneliness, depression, anxiety, lead to low self-esteem and increased susceptibility to illness. Bullying has also been shown to cause maladjustment in young children, and targets of bullying who were also bullies themselves exhibit even greater social difficulties. A mental health report also found that bullying was linked to eating disorders, anxiety, body dysmorphia and other negative psychological effects.


=== Suicide ===

Even though there is evidence that bullying increases the risk of suicide, bullying alone does not cause suicide. Depression is one of the main reasons why kids who are bullied die by suicide. It is estimated that between 15 and 25 children die by suicide every year in the UK alone because they are being bullied. Certain groups seem to incur a higher risk for suicide, such as Native Americans, Alaskan Natives, Asian Americans, and LGBT people. When someone feels unsupported by family or friends, it can make the situation much worse for the victim.In a self-report study completed in New York by 9th through 12th graders, victims of bullying reported more depressive symptoms and psychological distress than those who did not experience bullying. All types of involvement in bullying among both boys and girls is associated with depression even a couple years later. Another study that followed up with Finnish teens two years after the initial survey showed that depression and suicidal ideation is higher with teens who are bullied than those who did not report experiencing bullying. A Dutch longitudinal study on elementary students reported that boys who are bully-victims, who play both roles of a victim and a bully, were more likely to experience depression or serious suicidal ideation than the other roles, victims or bullies only, while girls who have any involvement in bullying have a higher level of risk for depression. In a study of high school students completed in Boston, students who self reported being victims of bullying were more likely to consider suicide when compared to youth who did not report being bullied. The same study also showed a higher risk of suicidal consideration in youth who report being a perpetrator, victim, or victim-perpetrator. Victims and victim-bullies are associated with a higher risk of suicide attempts. The place where youth live also appears to differentiate their bullying experiences such that those living in more urban areas who reported both being bullied and bullying others appear to show higher risk of suicidal ideation and suicide attempts. A national survey given to American 6th through 10th grade students found that cyberbullying victims experience a higher level of depression than victims experiencing other forms of bullying. This can be related to the anonymity behind social media. If a teen is being bullied and is displaying symptoms of depression it should be questioned and interventions should be implemented. The Danish study showed that kids who are bullied talked to their parents and teachers about it and some reported a decrease in bullying or a stop in the bullying after a teacher or parent intervened. The study emphasizes the importance of implementing program-collaborations in schools to have programs and anti-bullying interventions in place to prevent and properly intervene when it occurs. The study also shows the importance of having parents and teachers talk to the bullies about their bullying behavior in order to provide the necessary support for those experiencing bullying.While some people find it very easy to ignore a bully, others may find it very difficult and reach a breaking point. There have been cases of apparent bullying suicides that have been reported closely by the media. These include the deaths of Ryan Halligan, Phoebe Prince, Dawn-Marie Wesley, Nicola Ann Raphael, Megan Meier, Audrie Pott, Tyler Clementi, Jamey Rodemeyer, Kenneth Weishuhn, Jadin Bell, Kelly Yeomans, Rehtaeh Parsons, Amanda Todd, Brodie Panlock, Jessica Haffer, Hamed Nastoh, Sladjana Vidovic, April Himes, Cherice Moralez and Rebecca Ann Sedwick. According to the suicide awareness voices for education, suicide is one of the leading causes of death for youth from 15 to 24 years old. Over 16 percent of students seriously consider suicide, 13 percent create a plan, and 8 percent have made a serious attempt.


=== Positive development ===
Some have argued that bullying can teach life lessons and instill strength. Helene Guldberg, a child development academic, sparked controversy when she argued that being a target of bullying can teach a child "how to manage disputes and boost their ability to interact with others", and that teachers should not intervene but leave children to respond to the bullying themselves.The teaching of such anti-bullying coping skills to "would-be-targets" and to others has been found to be an effective long term means of reducing bullying incidence rates and a valuable skill-set for individuals.


=== Hormonal ===
Statistically controlling for age and pubertal status, results indicated that on average verbally bullied girls produced less testosterone, and verbally bullied boys produced more testosterone than their nonbullied counterparts.


== Dark triad ==

Research on the dark triad (narcissism, Machiavellianism, and psychopathy) indicate a correlation with bullying as part of evidence of the aversive nature of those traits.


== Projection ==

A bully may project his/her own feelings of vulnerability onto the target(s) of the bullying activity. Despite the fact that a bully's typically denigrating activities are aimed at the bully's targets, the true source of such negativity is ultimately almost always found in the bully's own sense of personal insecurity and/or vulnerability. Such aggressive projections of displaced negative emotions can occur anywhere from the micro-level of interpersonal relationships, all the way up through to the macro-level of international politics, or even international armed conflict.


== Emotional intelligence ==

Bullying is abusive social interaction between peers which can include aggression, harassment, and violence. Bullying is typically repetitive and enacted by those who are in a position of power over the victim. A growing body of research illustrates a significant relationship between bullying and emotional intelligence (EI). Mayer et al., (2008) defines the dimensions of overall EI as "accurately perceiving emotion, using emotions to facilitate thought, understanding emotion, and managing emotion". The concept combines emotional and intellectual processes. Lower emotional intelligence appears to be related to involvement in bullying, as the bully and/or the victim of bullying. EI seems to play an important role in both bullying behavior and victimization in bullying; given that EI is illustrated to be malleable, EI education could greatly improve bullying prevention and intervention initiatives.


== Context ==


=== Cyberbullying ===

Cyberbullying is any bullying done through the use of technology. This form of bullying can easily go undetected because of lack of parental/authoritative supervision. Because bullies can pose as someone else, it is the most anonymous form of bullying. Cyberbullying includes, but is not limited to, abuse using email, instant messaging, text messaging, websites, social networking sites, etc. With the creation of social networks like Facebook, Myspace, Instagram, and Twitter, cyberbullying has increased. Particular watchdog organizations have been designed to contain the spread of cyberbullying.


=== Disability bullying ===

It has been noted that disabled people are disproportionately affected by bullying and abuse, and such activity has been cited as a hate crime. The bullying is not limited to those who are visibly disabled, such as wheelchair-users or physically deformed such as those with a cleft lip, but also those with learning disabilities, such as autism and developmental coordination disorder.There is an additional problem that those with learning disabilities are often not as able to explain things to other people, so are more likely to be disbelieved or ignored if they do complain.


=== Gay bullying ===

Gay bullying and gay bashing designate direct or indirect verbal or physical actions by a person or group against someone who is gay or lesbian, or perceived to be so due to rumors or because they are considered to fit gay stereotypes. Gay and lesbian youth are more likely than straight youth to report bullying, as well as be bullied.


=== Legal bullying ===

Legal bullying is the bringing of a vexatious legal action to control and punish a person. Legal bullying can often take the form of frivolous, repetitive, or burdensome lawsuits brought to intimidate the defendant into submitting to the litigant's request, not because of the legal merit of the litigant's position, but principally due to the defendant's inability to maintain the legal battle. This can also take the form of Strategic Lawsuit Against Public Participation (SLAPP). It was partially concern about the potential for this kind of abuse that helped to fuel the protests against SOPA and PIPA in the United States in 2011 and 2012.


=== Military bullying ===

In 2000, the UK Ministry of Defence (MOD) defined bullying as "the use of physical strength or the abuse of authority to intimidate or victimize others, or to give unlawful punishments".Some argue that this behaviour should be allowed, due to ways in which "soldiering" is different from other occupations. Soldiers expected to risk their lives should, according to them, develop strength of body and spirit to accept bullying.


=== Parental bullying of children ===

Parents who may displace their anger, insecurity, or a persistent need to dominate and control upon their children in excessive ways have been proven to increase the likelihood that their own children will in turn become overly aggressive or controlling towards their peers.
The American Psychological Association advises on its website that parents who may suspect their own children may be engaging in bullying activities among their peers should carefully consider the examples which they themselves may be setting for their own children regarding how they typically interact with their own peers, colleagues, and children.


=== Prison bullying ===

The prison environment is known for bullying. An additional complication is the staff and their relationships with the inmates. Thus, the following possible bullying scenarios are possible:

Inmate bullies inmate (echoing school bullying)
Staff bullies inmate
Staff bullies staff (a manifestation of workplace bullying)
Inmate bullies staff


=== School bullying (bullying of students in schools) ===

Bullying can occur in nearly any part in or around the school building, although it may occur more frequently during physical education classes and activities such as recess. Bullying also takes place in school hallways, bathrooms, on school buses and while waiting for buses, and in classes that require group work and/or after school activities. Bullying in school sometimes consists of a group of students taking advantage of or isolating one student in particular and gaining the loyalty of bystanders who want to avoid becoming the next target. In the 2011 documentary Bully, we see first hand the torture that kids go through both in school and while on the school bus. As the movie follows around a few kids we see how bullying affects them both at school as well as in their homes. While bullying has no age limit, these bullies may taunt and tease their target before finally physically bullying them. Bystanders typically choose to either participate or watch, sometimes out of fear of becoming the next target.
Bullying can also be perpetrated by teachers and the school system itself; there is an inherent power differential in the system that can easily predispose to subtle or covert abuse (relational aggression or passive aggression), humiliation, or exclusion—even while maintaining overt commitments to anti-bullying policies.In 2016, in Canada, a North American legal precedent was set by a mother and her son, after the son was bullied in his public school. The mother and son won a court case against the Ottawa-Carleton District School Board, making this the first case in North America where a school board has been found negligent in a bullying case for failing to meet the standard of care (the "duty of care" that the school board owes to its students). Thus, it sets a precedent of a school board being found liable in negligence for harm caused to a child, because they failed to protect a child from the bullying actions of other students. There has been only one other similar bullying case and it was won in Australia in 2013 (Oyston v. St. Patricks College, 2013).


=== Sexual bullying ===

Sexual bullying is "any bullying behaviour, whether physical or non-physical, that is based on a person's sexuality or gender. It is when sexuality or gender is used as a weapon by boys or girls towards other boys or girls – although it is more commonly directed at girls. It can be carried out to a person's face, behind their back or through the use of technology."


=== Trans bullying ===

Trans bashing is the act of victimizing a person physically, sexually, or verbally because they are transgender or transsexual. Unlike gay bashing, it is committed because of the target's actual or perceived gender identity, not sexual orientation.


=== Workplace bullying ===

Workplace bullying occurs when an employee experiences a persistent pattern of mistreatment from others in the workplace that causes harm. Workplace bullying can include such tactics as verbal, nonverbal, psychological, physical abuse and humiliation. This type of workplace aggression is particularly difficult because, unlike the typical forms of school bullying, workplace bullies often operate within the established rules and policies of their organization and their society. Bullying in the workplace is in the majority of cases reported as having been perpetrated by someone in authority over the target. However, bullies can also be peers, and occasionally can be subordinates.The first known documented use of "workplace bullying" is in 1992 in a book by Andrea Adams called Bullying at Work: How to Confront and Overcome It.Research has also investigated the impact of the larger organizational context on bullying as well as the group-level processes that impact on the incidence, and maintenance of bullying behavior. Bullying can be covert or overt. It may be missed by superiors or known by many throughout the organization. Negative effects are not limited to the targeted individuals, and may lead to a decline in employee morale and a change in organizational culture. A Cochrane Collaboration systematic review has found very low quality evidence to suggest that organizational and individual interventions may prevent bullying behaviors in the workplace.


==== In academia ====

Bullying in academia is workplace bullying of scholars and staff in academia, especially places of higher education such as colleges and universities. It is believed to be common, although has not received as much attention from researchers as bullying in some other contexts.


==== In blue collar jobs ====
Bullying has been identified as prominent in blue collar jobs, including on oil rigs and in mechanic shops and machine shops. It is thought that intimidation and fear of retribution cause decreased incident reports. In industry sectors dominated by males, typically of little education, where disclosure of incidents are seen as effeminate, reporting in the socioeconomic and cultural milieu of such industries would likely lead to a vicious circle. This is often used in combination with manipulation and coercion of facts to gain favour among higher-ranking administrators.


==== In information technology ====

A culture of bullying is common in information technology (IT), leading to high sickness rates, low morale, poor productivity, and high staff-turnover. Deadline-driven project work and stressed-out managers take their toll on IT workers.


==== In the legal profession ====

Bullying in the legal profession is believed to be more common than in some other professions. It is believed that its adversarial, hierarchical tradition contributes towards this. Women, trainees and solicitors who have been qualified for five years or less are more impacted, as are ethnic minority lawyers and lesbian, gay and bisexual lawyers.


==== In medicine ====

Bullying in the medical profession is common, particularly of student or trainee doctors and of nurses. It is thought that this is at least in part an outcome of conservative traditional hierarchical structures and teaching methods in the medical profession, which may result in a bullying cycle.


==== In nursing ====

Even though The American Nurses Association believes that all nursing personnel have the right to work in safe, non-abusive environments, bullying has been identified as being particularly prevalent in the nursing profession although the reasons are not clear. It is thought that relational aggression (psychological aspects of bullying such as gossiping and intimidation) are relevant. Relational aggression has been studied among girls but not so much among adult women.


==== In teaching ====

School teachers are commonly the subject of bullying but they are also sometimes the originators of bullying within a school environment.


=== In other areas ===
As the verb to bully is defined as simply "forcing one's way aggressively or by intimidation", the term may generally apply to any life experience where one is motivated primarily by intimidation instead of by more positive goals, such as mutually shared interests and benefits. As such, any figure of authority or power who may use intimidation as a primary means of motivating others, such as a neighborhood "protection racket don", a national dictator, a childhood ring-leader, a terrorist, a terrorist organization, or even a ruthless business CEO, could rightfully be referred to as a bully. According to psychologist Pauline Rennie-Peyton, we each face the possibility of being bullied in any phase of our lives.


=== Machines ===
Children have been observed bullying anthropomorphic robots designed to assist the elderly. Their attacks start with blocking the robots' paths of movement and then escalate to verbal abuse, hitting and destroying the object. Seventy-five percent of the kids interviewed perceived the robot as "human-like" yet decided to abuse it anyway, while 35% of the kids who beat up the robot did so "for enjoyment".


== Prevention ==
Bullying prevention is the collective effort to prevent, reduce and stop bullying. Many campaigns and events are designated to bullying prevention throughout the world. Bullying prevention campaigns and events include: Anti-Bullying Day, Anti-Bullying Week, International Day of Pink, International STAND UP to Bullying Day and National Bullying Prevention Month. Anti-Bullying laws in the U.S. have also been enacted in 23 of its 50 states, making bullying in schools illegal.


== Responding to bullying ==
Bullying is typically ongoing and not isolated behaviour. Common ways that people try to respond, are to try to ignore it, to confront the bullies or to turn to an authority figure to try to address it.
Ignoring it often does nothing to stop the bullying continuing, and it can become worse over time.
It can be important to address bullying behaviour early on, as it can be easier to control the earlier it is detected.
Bystanders play an important role in responding to bullying, as doing nothing can encourage it to continue, while small steps that oppose the behaviour can reduce it.Authority figures can play an important role, such as parents in child or adolescent situations, or supervisors, human-resources staff or parent-bodies in workplace and volunteer settings. Authority figures can be influential in recognising and stopping bullying behaviour, and creating an environment where it doesn't continue.
In many situations however people acting as authority figures are untrained and unqualified, do not know how to respond, and can make the situation worse.
In some cases the authority figures even support the people doing the bullying, facilitating it continuing and increasing the isolation and marginalising of the target.
Some of the most effective ways to respond, are to recognise that harmful behaviour is taking place, and creating an environment where it won't continue.
People who are being targeted have little control over which authority figures they can turn to and how such matters would be addressed, however one means of support is to find a counsellor or psychologist who is trained in handling bullying.


== See also ==


== References ==


== Further reading ==
Kohut MR The Complete Guide to Understanding, Controlling, and Stopping Bullies & Bullying: A Complete Guide for Teachers & Parents (2007)
Bullies and Victims in Schools: a guide to understanding and management by Valerie E. Besag (1989)
The Fight That Never Ends by Tim Brown
Odd Girl Out: The Hidden Culture of Aggression in Girls" by Rachel Simmons ISBN 0-15-602734-8
Bullycide, Death at Playtime by Neil Marr and Tim Field ISBN 0-9529121-2-0
Bullycide in America: Moms Speak Out about the Bullying/Suicide Connection – by Brenda High, Bullycide.org
A Journey Out of Bullying: From Despair to Hope by Patricia L. Scott
"Peer Abuse Know More! Bullying From A Psychological Perspective" By Elizabeth Bennett
New Perspectives on Bullying by Ken Rigby
Garbarino, J. & de Lara, E. (2003). And Words Can Hurt Forever: How to Protect Adolescents from Bullying, Harassment, and Emotional Violence. The Free Press: New York NY.
Joanne Scaglione, Arrica Rose Scaglione Bully-proofing children: a practical, hands-on guide to stop bullying 2006
Why Is Everybody Always Picking on Me: A Guide to Handling Bullies for Young People. by Terrence Webster-Doyle. Book and Teaching curriculum.
"Why Nerds are Unpopular", by Paul Graham. This essay is an example of how even medium differences, in a hierarchical, zero-sum, or negative environments, can lead to ostracism or persecution.
Lord of the Flies by William Golding (1954). A famous work describing how a group of schoolboys trapped on an island descends into savagery.


== External links ==

Bullying at Curlie
Citizens Advice in the UK
Bullying. No Way! (Australian Education Authorities)
Bullying in schools (UK – schools)
PBSKids.org "Great Books About Bullies"
Be Brave Against Bullying, a UFT project
U.S. Department of Education's Education Resources Information Center (ERIC)
Bully Online