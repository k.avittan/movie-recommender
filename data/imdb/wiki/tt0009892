Ravished Armenia, also known as Auction of Souls, is a 1919 American silent film based on the autobiographical book Ravished Armenia by Arshaluys (Aurora) Mardiganian, who also played the lead role in the film. The film, which depicts the 1915 Armenian Genocide by the Ottoman Empire from the point of view of Armenian survivor Mardiganian, who plays herself in the film, survives in an incomplete form.


== Plot ==
According to a contemporary New York Times article, the first half of the film shows "Armenia as it was before Turkish and German devastation, and led up to the deportation of priests and thousands of families into the desert. One of the concluding scenes showed young Armenian women flogged for their refusal to enter Turkish harems and depicted the Turkish slave markets." The story was adapted for the screen by Henry Leyford Gates, who also wrote the book.


== Production ==
The Selig production, which used several thousand Armenian residents of southern California as extras, was filmed in 1918-1919 near Newhall, California. During a scene in which Mardiganian was escaping from a harem by jumping from one roof to another she fell and broke her ankle. The production, however, continued with Mardiganian being carried to each set for her scenes.
The film shows young Armenian girls being "crucified" by being nailed to crosses. However, almost 70 years later Mardiganian revealed to film historian Anthony Slide that the scene was inaccurate and went on to describe what was actually an impalement. She stated that "The Turks didn't make their crosses like that. The Turks made little pointed crosses. They took the clothes off the girls. They made them bend down, and after raping them, they made them sit on the pointed wood, through the vagina. That's the way they killed – the Turks. Americans have made it a more civilized way. They can't show such terrible things."H.L. Gates later ghostwrote a 20-part newspaper series for "Queen of the Artists' Studios" Audrey Munson in which he described the filming of the crucifixion scene in the California desert and said that one of the 12 artist's models employed for the scene died several days later from influenza as a result of the exposure during filming. He named the dead woman as Corinne Gray.


== Distribution ==
The initial New York performance of the silent film took place on February 16, 1919, in the ballroom of the Plaza Hotel, with society leaders, Mrs. Oliver Harriman and Mrs. George W. Vanderbilt, serving as co-hostesses on behalf of the American Committee for Armenian and Syrian Relief. To raise funds for relief, the film was shown in several American cities at an admission price of $10 per person at a time when the typical American theater charged an admission of $0.25 to $0.35. Later, when the film went into general distribution, ads described it as the "$10 per seat picture."The film was first screened in London under the title Auction of Souls and in 1920 was shown twice daily for three weeks at the Royal Albert Hall to obtain support for the protection of national minorities. This showing of the film, which contains depictions of the flogging of women and their nude crucifixion, was authorized with cuts to five scenes that had been agreed to by the film producers without the film's formal submission to the British Board of Film Censors, which never certified the film for general viewing in the United Kingdom.The film was released in Paris on December 11, 1919, Salle Gaveau. It was sponsored by the dukess of Rohan.


== Reception ==
Because of the film's subject matter, distributors often scheduled limited showings of the film to community leaders prior to releasing it to local theaters. Still, in some states there were attempts to ban or censor the film. For example, after the Pennsylvania State Board of Censors banned the film, the distributors sued and overturned the state agency decision in court. In reversing the Board's ban, the decision of the judge stated:

The court finds it a fact and a question of law that there is nothing in the scenes which make them sacrilegious, obscene, indecent or immoral, or such nature as to tend to debase or corrupt morals. Viewing the picture as a whole, the court finds as a fact that it is educational in nature. It is not only a vivid portrayal of the story Ravished Armenia, but it is also a picture of conditions as they existed in Armenia a few months ago.Mardiganian felt she had been cheated out of her $7000 fee for the film and received only $195. She sued her legal guardian Eleanor Brown Gates, the novelist wife of the screenwriter Henry Leyford Gates. She won $5,000.


== Film restoration (2009) ==
A restored and edited 24-minute segment of the historic motion picture (originally composed of 9 reels, i.e. approximately 90 minutes) was released in 2009 by the Armenian Genocide Resource Center of Northern California. It is based on a rare surviving reel of film edited in Soviet Armenia.  It includes a music score, an introduction, 125 subtitles, and a slideshow of several black-white production stills. The DVD is distributed by Heritage Publishing, Richmond, California, and is copyrighted by Richard Kloian.


== See also ==
Henry Morgenthau, Sr.
The Forty Days of Musa Dagh


== References ==


== External links ==
Ravished Armenia on IMDb
Ravished Armenia (1919) at A Cinema History
Credo, a 2005 presentation of the surviving segment of Ravished Armenia: Part 1 on YouTube, Part 2 on YouTube, Part 3 on YouTube