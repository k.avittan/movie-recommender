Possession is nine-tenths of the law is an expression meaning that ownership is easier to maintain if one has possession of something, or difficult to enforce if one does not. The expression is also stated as "possession is nine points of the law", which is credited as derived from the Scottish expression "possession is eleven points in the law, and they say there are but twelve."Although the principle is an oversimplification, it can be restated as: "In a property dispute (whether real or personal), in the absence of clear and compelling testimony or documentation to the contrary, the person in actual, custodial possession of the property is presumed to be the rightful owner. The rightful owner shall have their possession returned to them; if taken or used. The shirt or blouse you are currently wearing is presumed to be yours, unless someone can prove that it is not."


== Analysis ==
The adage is not literally true, that by law the person in possession is presumed to have a nine times stronger claim than anyone else, but that "it places in a strong light the legal truth that every claimant must succeed by the strength of his own title, and not by the weakness of his antagonist's." The principle bears some similarity to uti possidetis ("as you possess, so may you continue to possess"), which currently refers to the doctrine that colonial administrative boundaries become international boundaries when a political subdivision or colony achieves independence. Under Roman law, it was an interdictum ordering the parties to maintain possession of property until it was determined who owned the property.


=== Cases ===
In the Hatfield-McCoy feud, with testimony evenly divided, the doctrine that possession is nine-tenths of the law caused Floyd Hatfield to retain possession of the pig that the McCoys claimed was their property. It has been argued that in some situations, possession is ten-tenths of the law. While the concept is older, the phrase "Possession is nine-tenths of the law" is often claimed to date from the 16th century. In some countries, possession is not nine-tenths of the law, but rather the onus is on the possessor to substantiate his ownership.This concept has been applied to both tangible and intangible products. In particular, "knowledge management" presents problems with regard to this principle. Google's possession of a large amount of content has been the cause of some wariness due to this principle. It has been said that there was a time in which the attitude towards rights over genetic resources was that possession is nine tenths of the law, and for the other tenth reliance could be made on
the principle that biological resources were the heritage of mankind.Aboriginal people frequently encounter this principle. There is some question as to whether the principle applies to Native American land claims. It has been said that "squatter's rights" and "possession is 9/10ths  of the law" were largely responsible
for how the American West was really won.


== See also ==
Adverse possession
Conversion (law)
Finders keepers
Homesteading
Might makes right


== References ==