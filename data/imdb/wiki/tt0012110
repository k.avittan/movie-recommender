Season of the Witch (originally released as Hungry Wives) is a 1973 American drama film written and directed by George A. Romero, and starring Jan White, Raymond Laine, and Anne Muffly. The film follows a housewife in suburban Pittsburgh who becomes involved in witchcraft after meeting a local witch.
Described by Romero himself as a "feminist film," Season of the Witch was originally made under the title Jack's Wife, with a small crew, giving Romero the duties of an editor, cinematographer and screenwriter. The film's distributor cut major parts of the film and changed its title to Hungry Wives, marketing it as a softcore pornography film. The film failed to find an audience on its initial release and was re-released years later under the title Season of the Witch.


== Plot ==
Joan Mitchell (Jan White) is the 39-year-old wife of a businessman, Jack Mitchell (Bill Thunhurst). They live in suburban Pittsburgh with their 19-year-old daughter Nikki (Joedda McClain), a college student. Joan is attractive for her age, but unhappy and discontented with her housewife role. Jack is successful, but busy, domineering, and occasionally violent, embarking on long business trips every week. Joan has been seeing a psychotherapist because of her recurring dreams about her husband controlling her. He makes repeated references to needing to "kick some ass"—a colleague's, his own child's, his wife's. Eventually, he strikes Joan in the face.
Joan and her friends learn about a new woman in the neighborhood named Marion Hamilton (Virginia Greenwald) who is rumored to practice witchcraft. Prompted by curiosity, Joan and one of her friends, Shirley (Anne Muffley), drive over to Marion's house one night for a Tarot reading. It turns out that Marion is the leader of a local secret witches' coven.
Joan and Shirley drive home to Joan's house, where they meet Gregg (Raymond Laine), a student teacher at Nikki's college (with whom Nikki has a very casual sexual relationship). The four drink and talk. Gregg shows an interest in Joan, who rebuffs him. Joan throws Gregg out of her house after he cruelly tricks Shirley into believing that she has smoked pot. After taking Shirley home, Joan returns home to hear Nikki and Gregg having sex.  Turned on, she quietly goes to her bedroom and begins touching herself until Nikki walks in on her.
The next day, a furious Nikki leaves without telling anybody where she is going, and soon afterward Jack leaves for yet another one-week business trip, with Joan feeling more lonely than ever. Joan buys a book about witchcraft. She conjures a spell to make Gregg attracted to her, and soon they are engaged in an affair. She also has increasingly terrifying nightmares, in which she is attacked by an intruder wearing a Satanic mask. As she explores witchcraft further, practicing rituals and researching spells, Joan's world continues to change. The police tell Joan they have found Nikki in Buffalo, New York and that she will be coming home in three or four days. After one last sexual encounter with Gregg, Joan tells him she does not want to see him again.
After another terrifying nightmare involving the masked intruder, Joan shoots and kills her husband, who has unexpectedly returned home early from his trip.  Whether this event is accidental or intentional is not revealed. Joan is initiated into Marion's coven in an elaborate and campy ritual. The language used by the women makes reference to treasuring each coven member as part of the sisterhood. Cleared of her husband's death, which was ruled an accident, Joan attends a party with her friends.  Prompted by a compliment on her beautiful and youthful appearance, she quietly reveals that she is a witch. She smiles wryly when people around her refer to as "Mrs. Mitchell", or simply "Jack's wife".


== Cast ==


== Production ==
The film originally began production under the title of Jack's Wife. Romero got the idea for the film after reading about witchcraft for a different project. While working with public television in Pittsburgh, Romero became aware of the Feminist movement, which also influenced his script.The film was shot with a small crew in 1972 on 16mm film in the North Hills suburbs and Pittsburgh. It suffered from production problems when the original budget of $250,000 was lowered to $100,000. Romero was pressured by the distributor to make two sex scenes between the characters of Joan and Gregg pornographic, but refused.


== Style ==
In 1973, Romero described the film as "not really" being a horror film, but as a film that deals with the occult peripherally.


== Release ==
Romero had trouble finding distributors for the film. In 1973, Romero described that distributors were finding the film "too wordy". The film was distributed by Jack H. Harris and re-titled Hungry Wives! on its initial release in 1973, with screenings beginning on February 14. Several cuts were made to the film, reducing its original runtime of 130 minutes to 89. The film was promoted as a softcore pornography film and failed to find an audience on its initial release. After the success of Romero's Dawn of the Dead in 1978, the film was re-released under the title Season of the Witch.


=== Home media ===
Both the original film negative and Romero's original cut of the film are lost. Parts of Romero's original version have been found and re-edited into a home video version of the film which currently runs 104 minutes.Anchor Bay released the film under its Season of the Witch title in 2005 on DVD at its partly-restored 104 minute running time. The DVD contains featurettes on the film, other works of Romero and promotional material and Romero's earlier film There's Always Vanilla.In November 2017, the film was released in a restored version by Arrow Films on Blu-ray in a multi-disc set entitled Between Night and Dawn, which also contains the Romero films There's Always Vanilla and The Crazies.


== Reception ==
In 1980, Vincent Canby of The New York Times wrote a negative review for Hungry Wives!, stating that the film "has the seedy look of a porn film but without any pornographic action. Everything in it, from the actors to the props, looks borrowed and badly used." Kim Newman (the Monthly Film Bulletin) compared to the film to Romero's Martin, stating that the latter "managed to exploit the hybrid of tatty realism and vampire lore quite fruitfully, but Jack's Wife is fatally unsure of its approach." Newman noted that the acting varies from the "nicely underplayed (Jan White, Virginia Greenwald) through the desperately hammy (Anne Muffly) to the dangerously laid-back (Ray Laine)." The review concluded that "there is enough evidence here of Romero's quirky talent to maintain interest throughout"In a review of the Season Of The Witch home video, The A.V. Club compared the film to Romero's Night of the Living Dead stating that the film "looks significantly less impressive [...] Where Night Of The Living Dead sandwiched some undistinguished, talky bits featuring actors of widely varying skill between the zombie horror, Season Of The Witch is nearly all undistinguished talky bits featuring actors of widely varying skill. Frankly, it’s kind of a slog." Slant Magazine gave the film a four out of five star rating, calling the film Romero's Belle du Jour. Online film database Allmovie gave a mixed review for the film noting poor production quality, "wildly uneven acting", and that the film is "burdened both by its directness and its talkiness". The review went on to note that "this is as effective a film as the director has made, in many respects years ahead of its time, assuming a position more extreme than The Stepford Wives or even, for the most part, Thelma and Louise." TV Guide gave the film one star out of four, referring to the film as "One of the most problematic entries in George Romero's filmography", noting cheap production and poor acting.Scholar Bernice Murphy referred to the film as "cheaply made and jarringly edited," and noted it as an effort for Romero to distance himself from the supernatural horror themes he had become known for.


== See also ==
List of American films of 1973


== Notes ==


== References ==


== Sources ==
Lippe, Richard; Williams, Tony; Wood, Robin (2011). "The George Romero Interview".  In Williams, Tony (ed.). George A. Romero: Interviews. University Press of Mississippi. pp. 59–68. ISBN 1617030279.CS1 maint: ref=harv (link)
Mayo, Mike (2013). The Horror Show Guide: The Ultimate Frightfest of Movies. Visible Ink Press. ISBN 978-1-578-59459-7.CS1 maint: ref=harv (link)
Muir, John Kenneth (2002). Horror Films of the 1970s. McFarland. ISBN 0786491566.CS1 maint: ref=harv (link)
Murphy, Bernice (2009). The Suburban Gothic in American Popular Culture. Springer. ISBN 978-0-230-24475-7.CS1 maint: ref=harv (link)
Nicotero, Sam (2011). "Romero: An Interview with the director of Night of the Living Dead".  In Williams, Tony (ed.). George A. Romero: Interviews. University Press of Mississippi. pp. 18–35. ISBN 1617030279.CS1 maint: ref=harv (link)
Quarles, Mike (2001). Down and Dirty: Hollywood’s Exploitation Filmmakers and Their Movies. McFarland. ISBN 978-0-786-46257-5.CS1 maint: ref=harv (link)
Redfern, Nick; Steiger, Brad (2014). The Zombie Book: The Encyclopedia of the Living Dead. Visible Ink Press. ISBN 978-1-578-59530-3.CS1 maint: ref=harv (link)
Williams, Tony (2015) [2003]. The Cinema of George A. Romero: Knight of the Living Dead. Columbia University Press. ISBN 978-0-231-85075-9.CS1 maint: ref=harv (link)


== External links ==
Season of the Witch on IMDb