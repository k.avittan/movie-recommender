The Perils of Penelope Pitstop is an American animated television series produced by Hanna-Barbera Productions that premiered on CBS on September 13, 1969. The show ran for one season with a total of 17 half-hour episodes, the last first-run episode airing on January 17, 1970. Repeats aired on CBS until September 4, 1971; and in syndication as Fun World of Hanna-Barbera from 1976 to 1982. It is a spin-off of Wacky Races, reprising the characters of Penelope Pitstop and the Ant Hill Mob.Rebroadcasts of the show air on the Cartoon Network-owned channel Boomerang and the series can be streamed in its entirety on the Boomerang streaming service.


== Production ==
The series was patterned on the silent movie era melodrama cliffhanger movie serial The Perils of Pauline. It originally was to star also the characters of Dick Dastardly and Muttley. though Dastardly and Muttley were later dropped in pre-production. Those characters would be later reused in their own series, Dastardly and Muttley in Their Flying Machines.
Joe Ruby and Ken Spears were the head writers for the series, and Ruby, Spears, and Warner Bros. Cartoons veteran Michael Maltese wrote the stories for the individual episodes. Deciding to feature the characters in a different setting, studio heads decided to set the characters into an active adventure format strongly reminiscent of the 1910s.


== Plot ==
The series features the more popular characters from Wacky Races, namely Penelope Pitstop and the Ant Hill Mob, the latter of whom take on the role of heroes in contrast to their previously nefarious personalities.In each episode, Penelope's guardian, Sylvester Sneekly, attempts to claim Penelope's inheritance for himself by attacking her in the disguise of his sinister alter-ego the Hooded Claw. Aided by his twin henchmen the Bully Brothers, who always speak in unison, the Claw creates over-elaborate Rube Goldberg-style plots to do away with Penelope. Even though the Ant Hill Mob often came to Penelope's rescue, she herself often needed to save the Mob from the unintended effects of their attempts to rescue her. But just as quickly as Penelope was delivered from one quandary, she almost immediately found herself ensnared in another one of the Claw's traps. Despite their overall bumbling, however, the Ant Hill Mob always managed to rescue Penelope and foil the Hooded Claw's plans in the end.
While Penelope was curiously helpless whenever the Hooded Claw grabbed her, once he left her tied up for his fiendish plans to take effect, she usually became resourceful and ingenious, sometimes coming up with spontaneous or creative methodologies to escape her own predicaments.


== Characters ==
Penelope Pitstop is the title character and the main protagonist of the spin-off. She is the heiress who inherits the Pitstops' vast fortune, but is unaware that her guardian, Sylvester Sneekly, who disguises himself as the Hooded Claw, has plans to kill her for her fortune. She is often rescued by the Ant Hill Mob from her perils and she also rescues them when they fail to rescue her. Despite constantly being portrayed as the typical damsel-in-distress, Penelope is shown to be clever and resourceful when in danger.
The Ant Hill Mob is a group of seven dwarf mobsters. Sharing much in common with the Seven Dwarfs of the fairy tale Snow White and the Seven Dwarfs, they are usually the ones called upon to rescue Penelope when she is caught in one of the Hooded Claw's schemes.
Clyde: The Moe Howard-like leader of the Mob.
Dum Dum (Not to be confused with the Touché Turtle character): A clueless dunce.
Pockets: Capable of pulling whatever item may be needed (or not needed) from his improbably large pockets.
Snoozy: A constantly sleeping character.
Softie: Cries at any opportunity, often at ironic times (often heard sobbing "Penelope is saved!")
Yak Yak: In many ways Softie's opposite, Yak Yak instead chuckles at any opportunity, including inappropriate ones ("Hee-hee=hee-hee-hee-hoo! We're gonna crash! Hee-hee-hee-hee-hee-hoo!").
Zippy: A character with impressive speed.
Chugga-Boom (a.k.a. Chuggie): The Ant Hill Mob's sentient touring car.
Sylvester Sneekly/the Hooded Claw is the guardian of Penelope Pitstop and the main antagonist of the spin-off who has motives to kill Penelope because of her vast fortune. Described by the show's narrator as "that villain of villains" in his disguise as the Hooded Claw, he manages to capture or kidnap Penelope, who is unaware of her guardian's secret identity. In most episodes, he usually makes deadly traps for Penelope to get her killed and thereby get her inheritance. However, he always fails to kill Penelope when the Ant Hill Mob rescues her or Penelope outsmarts him when she manages to free herself from his traps. His catchphrase: when his plans are foiled, he angrily yells "Blast!"
The Bully Brothers: The Hooded Claw's henchmen. The two large goons speak almost entirely in unison in a guttural voice: "Right, Claw."


== Voice actors ==
Janet Waldo – Penelope Pitstop
Mel Blanc – Yak Yak, Chug-a-Boom, the Bully Brothers
Don Messick - Dum Dum, Snoozy, Pockets, Zippy
Paul Winchell – Clyde, Softie
Gary Owens – Narrator
Paul Lynde – Sylvester Sneekly/the Hooded Claw (uncredited)


== Episodes ==


== Home video ==
On May 10, 2005 (2005-05-10), Warner Home Video (via Hanna-Barbera Productions and Warner Bros. Family Entertainment) released the complete series on DVD in Region 1 territories. The UK had the full series released on DVD on July 31, 2006 in Region 2 PAL format. The series is also available at the iTunes Store.


== Cultural references ==
The 1984 song "The Power of Love" by British dance-pop band Frankie Goes to Hollywood makes reference to the show's main villain, The Hooded Claw.
The 2008 series Headcases featured segments parodying The Perils of Penelope Pitstop.
The Hooded Claw appears in the Wacky Races (2017) episode "The Trial of Dick Dastardly" voiced by Tom Kenny impersonating Paul Lynde. He appears as the prosecutor at Dick Dastardly's trial. There is also a reference to the original voice actor Paul Lynde's appearances on the original 1966-1981 version of Hollywood Squares as the Hooded Claw is shown in a middle square.


== References ==


== External links ==
The Perils of Penelope Pitstop  site
The Perils of Penelope Pitstop at Don Markstein's Toonopedia. Archived from the original on September 4, 2015.
The Perils of Penelope Pitstop on IMDb
The Perils of Penelope Pitstop  at the Big Cartoon DataBase
The Perils of Penelope Pitstop at TV.com