His Hour is a 1924 American silent drama film directed by King Vidor. This film was the follow-up to Samuel Goldwyn's Three Weeks, written by Elinor Glyn, and starring Aileen Pringle, one of the biggest moneymakers at the time of the Metro-Goldwyn-Meyer amalgamation.


== Plot ==
Gritzko (John Gilbert) is a Russian nobleman and Tamara (Aileen Pringle) is the object of his desire.


== Cast ==


== Production ==
His Hour marked the first of five pictures that John Gilbert and King Vidor would make together for M-G-M.
Adapted from a novel by Elinor Glyn, an author of torrid romances chic in the 19th Century, His Hour was Vidor’s attempt to tap into the popularity of  Jazz Age "flaming-youth" pictures dealing with marital infidelity.
The movie includes many titillating seduction scenes, one of which was deemed too salacious for release.
Produced under the strictures of the new Production Code, producer Louis B. Mayer censured Vidor for incorporating some of Glyn’s “hot-cheeked” depictions of sexual decadence.A former officer of the Russian Imperial Army, by now living in Los Angeles, served as a technical adviser on the film. His actual name has not been confirmed, however the studio press releases referred to him as Mike Mitchell. This film marked the first of four times that John Gilbert and King Vidor would work together. Despite showcasing his riding ability and appearance, Gilbert hated the script and felt it gave him nothing substantial to do as an actor.


== Reception ==
In this, Gilbert’s first film with King Vidor, audiences were impressed with the star as a romantic leading man


== Box office ==
According to MGM's records, the film made a profit of $159,000.MGM sent Elinor Glynn records which stated the film cost $211,930 and earned $317,442 resulting in a profit of only $105,511. This meant Glynn, who was entitled to 33.3% of net profits, earned $35,170.


== Preservation status ==
A print is preserved at the Museum of Modern Art.


== Footnotes ==


== References ==
Baxter, John. 1976. King Vidor. Simon & Schuster, Inc. Monarch Film Studies. LOC Card Number 75-23544.
Brownlow, Kevin and Kobal, John. 1979. Hollywood: The Pioneers. Alfred A. Knopf Inc. A Borzoi Book, New York. ISBN 0-394-50851-3
Durgnat, Raymond and Simmon, Scott. 1988. King Vidor, American. University of California Press, Berkeley. ISBN 0-520-05798-8
Landazuri, Roberto. 2009.  Bardelys the Magnificent. San Francisco Silent Film Festival (SFSFF) https://silentfilm.org/bardelys-the-magnificent/ Retrieved 11 June 2020.


== External links ==
His Hour on IMDb
Synopsis at AllMovie