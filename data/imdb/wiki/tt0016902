Forlorn River is a Western novel written by Zane Grey, first published in 1927.


== Plot ==
Ben Ide spends his time chasing wild horses in Northern California, accompanied by the wanderer, Nevada and his Indian companion, Modoc.  Rather than catching horses, he has earned the reputation of being a cattle rustler.  But Ina Blaine, his childhood sweetheart, knows this is impossible.  She defends Ben against the suspicions of her newly-rich father and his mysterious associate, Les Setter, who has a previous connection to Nevada.
Looking toward the future, Ben Ide and his companions buy out a couple of ranchers in a severe drought and proceed to catch a lot of wild horses.  He is after one in particular- California Red, whom Ina's father has promised as a present for her, if any man should catch him.  Setter and Blaine set out to steal Ben's new land while he's off, and trouble follows.


== Characters ==
Ben Ide is a young man whose dream is to catch wild horses. This has earned him the scorn of his father, a rancher, and the two no longer speak. Ben has moved up into the hills and befriended Nevada and Modoc. He was a well-regarded youth until his split with his father.
Ina Blaine is a young woman fresh out of college, and has come home to a very changed family.  Her family came into a large amount of money while she was away, and this puts her at odds with the other members of her family, as she wants the simply country life, while they want her to marry a well-to-do youth.  She adamantly defends Ben Ide's character.
Mr. Blaine is Ina's father who has become entangled in the shady business activities of Les Setter.
Les Setter is a man working to help Mr. Blaine increase his newfound wealth. He has a previous connection with Nevada, and is very suspicious of Ben. He takes a liking to Ina, though she doesn't trust him.
Nevada is a young man with a shady past, saved from death by Ben Ide. He loyally helps Ben catch horses, and falls in love with Ben's younger sister. He has a previous acquaintance with Les Setter.
Modoc is a companion of Nevada, Modoc is an Indian that doesn't speak much.  He is a good tracker familiar with Indian ways of catching wild horses and surviving in the harsh environment.


== Background ==
The story is set in a remote wilderness valley located in northern California. The author accurately describes the geography of the region throughout the novel, identifying Mount Shasta, Tule Lake, and the landscape in and around Lava Beds National Monument. The "Forlorn River" that flows through the area is the Lost River which flows out of Clear Lake Reservoir in California and into Oregon near Klamath Falls, eventually flowing back into California and emptying into Tule Lake. The ice caves where Ben captures the wild horses and where the rustlers are captured is at the lava tubes located in Lava Beds National Monument.


== Publication history ==
Forlorn River was originally serialized in Ladies Home Journal in 1926. It was published by Harper & Brothers in book form in 1927.


== Sequel ==
Forlorn River was followed by a sequel in 1928 titled Nevada.


== Adaptations ==
Two film adaptations of the novel were produced: Forlorn River (1926) starring Jack Holt, and Forlorn River (1937) starring Buster Crabbe. The novel was also published in comic book format by Dell Comics in Four Color #395 in May 1952.


== References ==
Citations
Bibliography


== External links ==
Zane Grey Inc.
Zane Grey's West Society
Forlorn River at The Literature Network
Forlorn River at Classic Reader
Forlorn River at The Online Books Page