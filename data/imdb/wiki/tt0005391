"Girl from the North Country" (occasionally known as "Girl of the North Country") is a song written by Bob Dylan. It was recorded at Columbia Recording Studios in New York City in April 1963, and released the following month as the second track on Dylan's second studio album, The Freewheelin' Bob Dylan. Dylan re-recorded the song as a duet with Johnny Cash in February 1969. That recording became the opening track on Nashville Skyline, Dylan's ninth studio album.
Live performances by Dylan appear on the albums Real Live (1984), The 30th Anniversary Concert Celebration (1993; recorded 1992), The Bootleg Series Vol. 13: Trouble No More 1979–1981 (Deluxe Edition) (2017; recorded 1981), Live 1962-1966: Rare Performances From The Copyright Collections (2018; recorded 1964), and The Bootleg Series Vol. 15: Travelin' Thru, 1967-1969 (Deluxe Edition) (2019; recorded 1969).  A February 1964 performance for Canadian television was included on the DVD release of Martin Scorsese's PBS television documentary on Dylan, No Direction Home in 2005.


== Origin of the song ==
The song was written following his first trip to England in December, 1962, upon what he thought to be the completion of his second album. It is debated as to whom this song is a tribute; some claim former girlfriend, Echo Helstrom, and some Bonnie Beecher, both of whom Dylan knew before leaving for New York. However, it is suspected that this song could have been inspired by his then girlfriend, Suze Rotolo. Dylan left England for Italy to search for Suze, whose continuation of studies there had caused a serious rift in their relationship. Unbeknownst to Dylan, Rotolo had already returned to the United States, leaving about the same time that Dylan arrived in Italy. It was here that he finished the song, ostensibly inspired by the apparent end of his relationship with Rotolo. Upon his return to New York in mid-January, he persuaded Rotolo to get back together, and to move back into his apartment on 4th Street. Suze Rotolo is the woman featured on the album cover, walking arm in arm with Dylan down Jones Street, not far from their apartment.
While in London, Dylan met several figures in the local folk scene, including English folksinger Martin Carthy. "I ran into some people in England who really knew those [traditional English] songs," Dylan recalled in 1984. "Martin Carthy, another guy named [Bob] Davenport. Martin Carthy's incredible. I learned a lot of stuff from Martin." Carthy exposed Dylan to a repertoire of traditional English ballads, including Carthy's own arrangement of "Scarborough Fair," which Dylan drew upon for aspects of the melody and lyrics of "Girl from the North Country," including the line from the refrain "Remember me to one who lives there, she once was a true love of mine". Musically, this song is nearly identical to his composition "Boots of Spanish Leather", composed and recorded one year later for the album The Times They Are a-Changin'.


== Notable versions ==
Altan performed this song on their album Another Sky
Johnny Cash and Joni Mitchell performed a duet of the song on The Johnny Cash Show.
Cash also recorded a duet with Dylan himself during their February 1969 recording session while Dylan was in Nashville for Nashville Skyline. This version appears in the 2012 film Silver Linings Playbook starring Bradley Cooper and Jennifer Lawrence.
Rosanne Cash included the song on her 2009 album The List, which is based on a list of 100 country songs Johnny Cash recommended to her as "essential."
Joe Cocker and Leon Russell performed the song on the 1970 live album Mad Dogs and Englishmen.
Counting Crows covered the song on their 2013 live album Echoes of the Outlaw Roadshow.
Eels perform a slowed down, live, acoustic version on Eels with Strings: Live at Town Hall as well as he Myspace Transmissions Session 2009.
Roy Harper recorded his version of the song on his 1974 album Valentine.
Tom Northcott's 1968 single charted in Canada.
Tony Rice covered the song on his 1993 album "Tony Rice Plays and Sings Bluegrass"
Leon Russell performed the song on his 1970 live album at the Fillmore East.
Rod Stewart also covered this song on his 1974 album Smiler.
Sting covered the song for Chimes of Freedom: Songs of Bob Dylan Honoring 50 Years of Amnesty International.
The Black Crowes often covered the song in concert throughout their career, usually featuring an extended dual guitar solo.
The Clancy Brothers released their version on their 1972 album Save The Land.
Pete Townshend covered the song on his 1982 album All the Best Cowboys Have Chinese Eyes
John Waite covered the song on his 2004 album The Hard Way
Link Wray & his Ray Men released a version in 1965.
Neil Young released a cover of this song on his 2014 album A Letter Home.
Leo Kottke covered the song in a recorded performance at No Exit Coffeehouse in 1969.
Conor Oberst, M. Ward and Jim James performed the song as part of Bright Eyes' concert at Austin City Limits in 2004


== See also ==
List of Bob Dylan songs based on earlier tunes


== References ==


== External links ==
Lyrics at Bob Dylan's official website