Our Lady of Sorrows (Latin: Beata Maria Virgo Perdolens), Our Lady of Dolours, the Sorrowful Mother or Mother of Sorrows (Latin: Mater Dolorosa), and Our Lady of Piety, Our Lady of the Seven Sorrows or Our Lady of the Seven Dolours are names by which the Virgin Mary is referred to in relation to sorrows in her life. As Mater Dolorosa, it is also a key subject for Marian art in the Catholic Church.
The Seven Sorrows of Mary are a popular Roman Catholic devotion. In common religious Catholic imagery, the Virgin Mary is portrayed in a sorrowful and lacrimating affect, with one or seven long knives or daggers piercing her heart, often bleeding. Devotional prayers that consist of meditation began to elaborate on her Seven Sorrows based on the prophecy of Simeon. Common examples of piety under this title are Servite rosary or the Chaplet of the Seven Sorrows of Our Lady, the Seven Joys of Mary, and, more recently, Sorrowful and Immaculate Heart of Mary.
The feast of Our Lady of Sorrows is liturgically celebrated every 15 September, while a feast of Friday of Sorrows is observed in some Catholic countries.


== Seven Sorrows of Mary ==
The Seven Sorrows (or Dolors) are events in the life of the Blessed Virgin Mary that are a popular devotion and are frequently depicted in art.These Seven Sorrows should not be confused with the five Sorrowful Mysteries of the Rosary.They're often depicted as seven daggers stabbed into her.

The prophecy of Simeon. (http://www.usccb.org/bible/luke/2)
The flight into Egypt. (http://www.usccb.org/bible/matthew/2)
The loss of the Child Jesus in the Temple of Jerusalem. (http://www.usccb.org/bible/luke/2)
Mary's meeting Jesus on the Via Dolorosa. (not in the New Testament)
The Crucifixion of Jesus on Mount Calvary. (http://www.usccb.org/bible/matthew/27), (http://www.usccb.org/bible/mark/15), (http://www.usccb.org/bible/luke/23), (http://www.usccb.org/bible/john/19)
The Piercing of the Side of Jesus with a spear, and his descent from the Cross. (http://www.usccb.org/bible/john/19)
The burial of Jesus by Joseph of Arimathea. (http://www.usccb.org/bible/matthew/27),   (http://www.usccb.org/bible/mark/15), (http://www.usccb.org/bible/luke/23), (http://www.usccb.org/bible/john/19)It is a tradition for Catholics to say daily one Our Father and seven Hail Marys for each mystery.


== Devotions to the Seven Sorrows ==


=== Western Christianity ===

Earlier, in 1232, seven youths in Tuscany founded the Servite Order (also known as the "Servite Friars", or the "Order of the Servants of Mary"). Five years later, they took up the sorrows of Mary, standing under the Cross, as the principal devotion of their order.Over the centuries several devotions, and even orders, arose around meditation on Mary's Sorrows in particular. The Servites developed the three most common devotions to Our Lady's Sorrows, namely the Rosary of the Seven Sorrows, the Black Scapular of the Seven Dolours of Mary and the Novena to Our Sorrowful Mother. The Black Scapular is a symbol of the Confraternity of Our Lady of Sorrows, which is associated with the Servite Order. Most devotional scapulars have requirements regarding ornamentation or design. The devotion of the Black Scapular requires only that it be made of black woollen cloth. From the National Shrine of Saint Peregrine spread the Sorrowful Mother Novena, the core of which is the Via Matris.


=== Eastern Christianity ===

On February 2, the same day as the Great Feast of the Meeting of the Lord, Orthodox Christians and Eastern Catholics commemorate a wonder-working icon of the Theotokos (Mother of God) known as "the Softening of Evil Hearts" or "Simeon's Prophecy".It depicts the Virgin Mary at the moment that Simeon the Righteous says, "Yea, a sword shall pierce through thy own soul also...." (Luke 2:35). She stands with her hands upraised in prayer, and seven swords pierce her heart, indicative of the seven sorrows. This is one of the few Orthodox icons of the Theotokos which do not depict the infant Jesus. The refrain "Rejoice, much-sorrowing Mother of God, turn our sorrows into joy and soften the hearts of evil men!" is also used.


== Liturgical feast ==


=== Our Lady of Compassion ===
The Feast of Our Lady of Sorrows grew in popularity in the 12th century, although under various titles. Some writings would place its roots in the eleventh century, especially among the Benedictine monks. The first altar to the Mater Dolorosa was set up in 1221 at the Cistercian monastery of Schönau. 

The formal feast of the Our Lady of Sorrows was originated by a provincial synod of Cologne in 1423. It was designated for the Friday after the third Sunday after Easter and had the title: Commemoratio angustiae et doloris B. Mariae V. Its object was the sorrow of Mary during the Crucifixion and Death of Christ. Before the sixteenth century this feast was limited to the dioceses of North Germany, Scandinavia, and Scotland.According to Fr. William Saunders, "... in 1482, the feast was officially placed in the Roman Missal under the title of Our Lady of Compassion, highlighting the great love our Blessed Mother displayed in suffering with her Son. The word compassion derives from the Latin roots cum and patior which means "to suffer with".

After 1600 it became popular in France and was set for the Friday before Palm Sunday. By a Decree of 22 April 1727, Pope Benedict XIII extended it to the entire Latin Church, under the title "Septem dolorum B.M.V.". In 1954, it still held the rank of major double (slightly lower than the rank of the September feast) in the General Roman Calendar. Pope John XXIII's 1960 Code of Rubrics reduced it to the level of a commemoration.


=== The Seven Sorrows of the Blessed Virgin Mary ===
In 1668, a separate feast of the Seven Sorrows of Mary, celebrated on the third Sunday in September, was granted to the Servites. Pope Pius VII introduced it into the General Roman Calendar in 1814. In 1913, Pope Pius X, in view of his reform giving precedence to Sundays over ordinary feasts, moved this feast to September 15, the day after the Feast of the Cross. It is still observed on that date.
Since there were thus two feasts with the same title, on each of which the Stabat Mater sequence was recited, the Passion Week celebration was removed from the General Roman Calendar in 1969 as a duplicate of the September feast. Each of the two celebrations had been called a feast of "The Seven Sorrows of the Blessed Virgin Mary" (Latin: Septem Dolorum Beatae Mariae Virginis). Recitation of the Stabat Mater was made optional.

The existence of traditional manifestations of public devotion, such as processions with statues of Our Lady of Sorrows on days leading to Good Friday, has led to the maintenance on the previous Friday of the liturgical celebration of the Sorrows of Mary in some local calendars, as in Malta.
For everywhere the latest edition of the Roman Missal provides on that Friday the optional collect:

Pope Benedict XVI's motu proprio Summorum Pontificum authorizes, under certain conditions, continued use of the 1962 Roman Missal, which contains the feast of the Friday of the fifth week of Lent.


== Patronage ==
Our Lady of Sorrows is the patron saint of:

people named Dolores, Dolorita, Lola and Pia.
Poland: the icon Our Lady of Sorrows, Queen and Patroness of Poland (see also: Patron saints of Poland § Primary) was canonically crowned by Pope Paul VI on 15 August 1967.
Malta: on Friday of Sorrows, almost all parishes in Malta hold devotional penitentiary processions with a life-size statue of Our Lady of Sorrows through the streets of the parish.
Slovakia: 15 September is also a national public holiday
Granada, Spain: September 15 is a public holiday in the city. 
The Congregation of Holy Cross
Order of the Servants of Mary
Mola di Bari and the Molise region of Italy
Nuestra Señora de la Soledad de Porta Vaga, Queen and Patroness of the City and Province of Cavite, Philippines
Mississippi, United States
Dolores, Abra, Philippines
Lanzarote, Canary Islands
Mater Dolorosa (Berlin-Lankwitz)
Pinabacdao, Samar, Philippines
Jia-an, Jiabong, Samar, Philippines
Ronda, Cebu, Philippines
Tanawan, Bustos, Bulacan, PhilippinesChurches:

Our Lady of Sorrows Basilica, Chicago, United States
Our Lady of Sorrows Basilica, Šaštín-Stráže, Slovakia
Église Notre-Dame-des-Sept-Douleurs, Montreal, Canada
Senhora das Dores Church, Póvoa de Varzim, Portugal
Nuestra Señora de los Dolores, Montevideo
St. Mary's Church (Fairfax Station, Virginia)
Our Lady of Sorrows Church (Santa Barbara, California)
Our Lady of Sorrows Church in Ká-Hó, Coloane, Macau.
Our Lady of Sorrows of Calolbon (Batong Paloway), Paloway, San Andres, Catanduanes, Philippines
National Shrine of Our Lady of Sorrows, Dolores, Quezon, Philippines
Diocesan Shrine of Nuestra Señora de los Dolores de Turumba, Pakil, Laguna, Philippines
Church of Our Lady of Seven Sorrows (Czech: Kostel Panny Marie Sedmibolestné, lit. 'Church of Virgin Mary of Seven Pains'), Rabštejn nad Střelou, Czech Republic
Our Lady of Piety, Piedade, Goa, India


== Gallery ==
Our Lady of Sorrows, depicted as "Mater Dolorosa" (Mother of Sorrows) has been the subject of some key works of Roman Catholic Marian art. Mater Dolorosa is one of the three common artistic representations of a sorrowful Virgin Mary, the other two being Stabat Mater and Pietà.In this iconography, Our Lady of Seven Sorrows is at times simply represented in a sad and anguished mode by herself, her expression being that of tears and sadness. In other representations the Virgin Mary is depicted with seven swords in her heart, a reference to the prophecy of Simeon at the Presentation of Jesus at the Temple.

		
		
		
		
		
		
		
		
		
		
		


== See also ==
Acts of Reparation to the Virgin Mary
Mission San Francisco de Asís in San Francisco, California, known also as Mission Dolores
Pietà
Marian art in the Catholic Church
Scapular of the Seven Sorrows of Mary
Seven Joys of Mary
Stabat Mater
The Glories of Mary


== References ==


== Further reading ==
The Seven Sorrows of Mary, by Joel Giallanza, C.S.C. 2008, published by Ave Maria Press, ISBN 1-59471-176-3


== External links ==
The Seven Sorrows Devotion