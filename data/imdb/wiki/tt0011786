The tree of the knowledge of good and evil (Biblical Hebrew: עֵ֕ץ הַדַּ֖עַת ט֥וֹב וָרָֽע‎ ʿêṣ had-daʿaṯ ṭōwḇ wā-rāʿ [ʕesˤ hadaʕaθ tˤov wɔrɔʕ]) is one of two specific trees in the story of the Garden of Eden in Genesis 2–3, along with the tree of life.


== In Genesis ==


=== Narrative ===
Genesis 2 narrates that Yahweh places the first man and woman in a garden with trees of whose fruits they may eat, but forbids them to eat from "the tree of the knowledge of good and evil." When, in Genesis 3, a serpent persuades the woman to eat from its forbidden fruit and she also lets the man taste it, God expels them from the garden.


=== Meaning of good and evil ===
The phrase in Hebrew: טוֹב וָרָע, tov wa-raʿ, literally translates as good and evil. This may be an example of the type of figure of speech known as merism, a literary device that pairs opposite terms together in order to create a general meaning, so that the phrase "good and evil" would simply imply "everything." This is seen in the Egyptian expression evil-good, which is normally employed to mean "everything." In Greek literature, Homer also uses the device when he lets Telemachus say, "I [wish to] know everything, the good and the evil."; though the words used – ἐσθλός for "good" and χερείων for "evil" – are better termed "superior" and "inferior".However, if tree of the knowledge of good and evil is to be understood to mean a tree whose fruit imparts knowledge of everything, this phrase does not necessarily denote a moral concept. This view is held by several scholars.Given the context of disobedience to God, other interpretations of the implications of this phrase also demand consideration. Robert Alter emphasizes the point that when God forbids the man to eat from that particular tree, he says that if he does so, he is "doomed to die." The Hebrew behind this is in a form regularly used in the Hebrew Bible for issuing death sentences.


== Religious views ==


=== Judaism ===
In Jewish tradition, the Tree of Knowledge and the eating of its fruit represents the beginning of the mixture of good and evil together. Before that time, the two were separate, and evil had only a nebulous existence in potential. While free choice did exist before eating the fruit, evil existed as an entity separate from the human psyche, and it was not in human nature to desire it. Eating and internalizing the forbidden fruit changed this and thus was born the yetzer hara, the evil inclination. In Rashi's notes on Genesis 3:3, the first sin came about because Eve added an additional clause to the Divine command: Neither shall you touch it. By saying this, Eve added to YHWH's command and thereby came to detract from it, as it is written: Do not add to His Words (Proverbs 30:6). However, In Legends of the Jews, it was Adam who had devoutly forbidden Eve to touch the tree even though God had only mentioned the eating of the fruit.
When Adam ate from the Tree of Knowledge, all the animals ate from it, too 
In Kabbalah, the sin of the Tree of Knowledge (called Cheit Eitz HaDa'at) brought about the great task of beirurim, sifting through the mixture of good and evil in the world to extract and liberate the sparks of holiness trapped therein. Since evil no longer has independent existence, it henceforth depends on holiness to draw down the Divine life-force, on whose "leftovers" it then feeds and derives existence. Once evil is separated from holiness through beirurim, its source of life is cut off, causing the evil to disappear. This is accomplished through observance of the 613 commandments in the Torah, which deal primarily with physical objects wherein good and evil are mixed together. Thus, the task of beirurim rectifies the sin of the Tree and draws the Shechinah back down to earth, where the sin of the Tree had caused Her to depart.


=== Christianity ===

In Christian tradition, consuming the fruit of the tree of knowledge of good and evil was the sin committed by Adam and Eve that led to the fall of man in Genesis 3.
In Catholicism, Augustine of Hippo taught that the tree should be understood both symbolically and as a real tree – similarly to Jerusalem being both a real city and a figure of Heavenly Jerusalem. Augustine underlined that the fruits of that tree were not evil by themselves, because everything that God created was good (Gen 1:12). It was disobedience of Adam and Eve, who had been told by God not to eat of the tree (Gen 2:17), that caused disorder in the creation, thus humanity inherited sin and guilt from Adam and Eve's sin.In Western Christian art, the fruit of the tree is commonly depicted as the apple, which originated in central Asia. This depiction may have originated as a Latin pun: by eating the mālum (apple), Eve contracted malum (evil).


=== Islam ===

The Quran never refers to the tree as the "Tree of the knowledge of good and evil" but rather typically refers to it as "the tree" or (in the words of Iblis) as the "tree of immortality." Muslims believe that when God created Adam and Eve, he told them that they could enjoy everything in the Garden except this tree ( a Normal tree ) and so, Satan appeared to them and told them that the only reason God forbade them to eat from that tree is that they would become Angels or Immortal
( demonic lie ).
When they ate from this tree their nakedness appeared to them and they began to sew together, for their covering, leaves from the Garden. became the reason for the slip. The Quran mentions the sin as being a 'slip', and after this 'slip' they were sent to the destination they were intended to be on: Earth. Consequently, they repented to God and asked for his forgiveness and were forgiven. It was decided that those who obey God and follow his path shall be rewarded with everlasting life in Jannah, and those who disobey God and stray away from his path shall be punished in Jahannam.
God in Quran (Al-A'raf 27) states:

"[O] Children of Adam! Let not Satan tempt you as he brought your parents out of the Garden, stripping them of their garments to show them their shameful parts. Surely he [Satan] sees you, he and his tribe, from where you see them not. We have made the Satans the friends of those who do not believe."


=== Gnosticism ===
Uniquely, the Gnostic religion held that the tree was entirely positive or even sacred. Per this saga, it was the archons who told Adam and Eve not to eat from its fruit then lied to them by claiming they would die after tasting it. But later in the story, an instructor is sent from the Pleroma by the aeons to save humanity and reveal gnosis. This savior does so by telling Adam and Eve that eating the fruit is the way into salvation. Examples of the narrative can be found within the Gnostic manuscripts On the Origin of the World and the Secret Book of John.Manichaeism, which has been considered a Gnostic sect, echoes these notions as well, presenting the primordial aspect of Jesus as the instructor.


== Other cultures ==
A cylinder seal, known as the Adam and Eve cylinder seal, from post-Akkadian periods in Mesopotamia (c. 23rd – 22nd century BCE), has been linked to the Adam and Eve story. Assyriologist George Smith (1840–1876) describes the seal as having two facing figures (male and female) seated on each side of a tree, holding out their hands to the fruit, while between their backs is a serpent, giving evidence that the fall of man account was known in early times of Babylonia.
The British Museum disputes this interpretation and holds that it is a common image from the period depicting a male deity being worshiped by a woman, with no reason to connect the scene with the Book of Genesis.


== See also ==
Adam and Eve (Latter Day Saint movement)
Dream of the Rood
Enlightenment (spiritual)
Original sin


== References ==


=== Bibliography ===
Alter, Robert. A translation with commentary (2004). The five books of Moses. New York: W.W. Norton. ISBN 0-393-33393-0.CS1 maint: ref=harv (link)
Knight, Douglas (1990).  Watson E. Mills (ed.). Mercer dictionary of the Bible (2d corr. print. ed.). Macon, GA: Mercer University Press. ISBN 0-86554-402-6.CS1 maint: ref=harv (link) Media related to Tree of the knowledge of good and evil at Wikimedia Commons