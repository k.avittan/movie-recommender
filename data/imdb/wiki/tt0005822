"Old Folks at Home" (also known as "Swanee River") is a minstrel song written by Stephen Foster in 1851.  Since 1935 it has been the official state song of Florida, although in 2008 the original lyrics were revised.


== Composition ==

"Old Folks at Home" was commissioned in 1851 by E. P. Christy for use by Christy's Minstrels, his minstrel troupe.  Christy also asked to be credited as the song's creator, and was so credited on early sheet music printings.  As a result, while the song was a success, Foster did not directly profit much from it.Foster had composed most of the lyrics but was struggling to name the river of the opening line, and asked his brother to suggest one. The first suggestion was "Yazoo" (in Mississippi), which despite fitting the melody perfectly, was rejected by Foster. The second suggestion was "Pee Dee" (in South Carolina), to which Foster said, "Oh pshaw! I won't have that." His brother then consulted an atlas and called out "Suwannee!" Foster said, "That's it, exactly!" Adding it to the lyrics, he purposely misspelled it as "Swanee" to fit the melody.  Foster himself never saw the Suwannee, or even visited Florida, but nevertheless Florida made "Old Folks At Home" its state song in 1935, replacing "Florida, My Florida".  Despite the song's popularity during the era, few people outside of Florida actually knew where the Suwannee River was, or that it was even a real place.Antonín Dvořák's Humoresque No. 7, written in the 1890s, is musically similar and is sometimes played along with "Old Folks at Home". The Library of Congress's National Jukebox presents a version with soprano Alma Gluck and violinist Efrem Zimbalist Sr.


== Lyrics revisions ==
Written in the first person from the perspective and in the dialect of an African slave (at a time when slavery was legal in 15 of the states of the US), the song's narrator states "longing for de old plantation", which has been criticized as romanticizing slavery.  The word, "darkies", used in Foster's lyrics, has been amended; for example, "brothers" was sung in place of "darkies" at the dedication of the new Florida state capitol building in 1978. In general, at public performances another word like "lordy", "mama", "darling", "brothers", "children", or "dear ones" is typically substituted.
In practice, the pronunciation, as written in dialect, has long been disregarded in favor of the corresponding standard American English usage, as demonstrated by the song's performances at the 1955 Florida Folk Festival.


=== State song of Florida ===
As the official state song of Florida, "Old Folks at Home" has traditionally been sung as part of a Florida governor's inauguration ceremony. However, over time, the lyrics were progressively altered to be less offensive; as Diane Roberts observed:

Florida got enlightened in 1978; we substituted "brothers" for "darkies".  There were subsequent revisions.  At Jeb Bush's second inauguration as governor in 2003, a young black woman gave a moving, nondialect rendition of "Old Folks at Home", except "still longing for the old plantation" came out "still longing for my old connection". Perhaps someone confused Stephen Foster's lyrics with a cell phone commercial.
In his 2007 inauguration ceremony, Charlie Crist decided not to include the state song, but rather to use in its place, "The Florida Song", a composition written by a black Floridian jazz musician, Charles Atkins. Crist then encouraged state Senator Tony Hill, who was the leader of the legislature's Black Caucus, to find a new song. Hill joined forces with state Representative Ed Homan and the Florida Music Educators Association to sponsor a contest for a new state song.On January 11, 2008, the song "Florida (Where the Sawgrass Meets the Sky)" was selected as the winner. The Florida Legislature considered the issue and ultimately adopted it as the state anthem while retaining "Old Folks at Home" as the state song, replacing its original lyrics with a revised version approved by scholars at the Stephen Foster Memorial, University of Pittsburgh. Governor Crist stated that he was not pleased by the "two songs" decision; but he signed the bill, creating a new state anthem and establishing the reworded version of the state song by statute, rather than by resolution like the 1935 decision.


== Lyrics ==


== Notable recordings ==
Joel Whitburn identifies early successful recordings by Len Spencer (1892), Vess Ossman (1900), Haydn Quartet (1904), Louise Homer (1905), 
Alma Gluck (1915), Taylor Trio (1916) and by Oscar Seagle and Columbia Stellar Quartet (1919).The song enjoyed a revival in the 1930s with versions by Jimmie Lunceford and by Bunny Berigan. Bing Crosby sang the song in the 1935 movie Mississippi and also recorded the song commercially the same year.Kenny Ball And His Jazzmen recorded a swing version of the song (using only the first verse and chorus twice over and substituting "Lordy" for "darkies") in 1962 for Pye Records. The recording appeared on the B side of their 1963 single "Sukiyaki".  Another swing version was recorded by Hugh Laurie (2011).


== Other film/TV appearances ==
1930 Mammy – sung by minstrel chorus
1935 Mississippi — sung by Bing Crosby
1936 Mr. Deeds Goes to Town – 1st verse sung by Jean Arthur
1939 Swanee River
1940 Remember the Night – performed by Fred MacMurray (piano and vocal)
1941 Babes on Broadway – Eddie Peabody on banjo, dubbing for Mickey Rooney
1941 Nice Girl? – sung by Deanna Durbin
1942 The Palm Beach Story – sung by the Ale and Quail Club members
1944 Ghost Catchers – danced by the Ghost
1947 Road to Rio – a few lines sung by Bing Crosby and Bob Hope
1956 The Honeymooners – The beginning of Swanee River is played by Ed Norton (on the harmonica) before Ralph's apology in "A Matter of Record" (#1.15).
1956 The Honeymooners – In the episode, "The $99,000 Answer" (#1.19), Ed Norton has a strange habit that before he can play any song he always plays a few notes of "Old Folks at Home"/"Swanee River" to warm up. The first question for Ralph in the game show is "Who is the composer of Swanee River?" The first few notes, the same ones that Ed had played earlier, are played so that Ralph knows the song. With only a few seconds left, Ralph, recognizing the song, but panicking, says "Ed Norton?"
1963 The Jack Benny Program – in Season 13 Episode 20, Jack Benny plays Stephen Foster as he tries to write some of his famous songs. The episode features Connie Francis as Foster's wife, who inadvertently helps Foster break his writer's block by commenting on unusual events around their home. She ends up singing "Old Folks at Home".


== References ==


== External links ==
Closeup of Foster's notebook page with first draft of "Old Folks at Home", including substitution of "Pedee" with "Swannee"
Recording of "Old Folks at Home" at the 1955 Florida Folk Festival; made available for public use by the State Archives of Florida