The Hand is a 1981 American psychological horror film written and directed by Oliver Stone, based on the novel The Lizard's Tail by Marc Brandell. The film stars Michael Caine and Andrea Marcovicci. Caine plays Jon Lansdale, a comic book artist who loses his hand, which in turn takes on a murderous life of its own. The original film score is by James Horner, in one of his earliest projects. Warner Bros. released the movie on DVD on September 25, 2007.


== Plot ==
Jon Lansdale (Michael Caine) is a comic book illustrator, whose relationship with his beautiful wife Anne (Andrea Marcovicci) and daughter Lizzie (Mara Hobel) is on the rocks. While driving, Anne and Lansdale end up behind a slow-moving truck and an impatient driver behind them. In the heat of an argument, Anne accidentally pulls back too fast while Lansdale is waving down the truck driver, causing his right hand to be completely severed. Anne attempts to find the severed hand but it is too late.
Lansdale then starts a painful adjustment to not having his drawing hand, while Anne deals with the guilt of having caused the accident and tries to get closer to her husband. Lansdale attempts unsuccessfully to recover the hand himself, though he does find his signet ring that Anne gave him. The couple move to New York and Lansdale is approached by his friend and agent Karen Wagner (Rosemary Murphy) to co-produce his comic with another artist, David Maddow (Charles Fleischer). Lansdale however, begins to show signs of a mental breakdown, and when he shows the test boards to Karen, they are all marked up. The deal is off and Karen fires Lansdale, who cannot recall marking up the boards while questioning his daughter about the incident. In a fit of frustration, Lansdale loses his signet ring. Anne is unable to cope with Lansdale's increasingly erratic behavior and general instability. Lansdale becomes jealous of Anne's yoga instructor and begins his slow descent into total darkness when an encounter with a homeless man (Oliver Stone) leaves the man dead at the "hand" of his former appendage. It's not entirely clear whether or not this was a real event or something in his mind. Lansdale begins having hallucinations about various inanimate objects coming to life as a hand, like a shower faucet.
After his final meeting with Wagner, Lansdale comes forth with his intention to take an offer to teach at a small community college in California. At the suggestion of his friend Brian (Bruce McGill), Lansdale rents out a cabin in the woods for the time being. While the majority of his students fail to show an interest in comic books, this is not the case with Stella (Annie McEnroe), who also takes a personal interest in Lansdale. Annie insists on staying with Lansdale during the Christmas season, and he begins to have violent hallucinations about the hand strangling her. That night, the missing ring reappears on Lansdale's pillow.
Lansdale and Stella begin a romantic relationship despite the latter showing an interest in Brian. Not long after, Lansdale meets Brian at a bar, but is confused as he should be on a two-week vacation with Stella. He soon learns that Brian hasn't seen her since she last went to Lansdale's cabin. Lansdale picks up Anne at the airport, but soon realizes she has no real intention of staying as she previously claimed. He hallucinates the hand strangling her, causing the car to crash in a fiery explosion. While his wife and daughter are at the cabin, Lansdale confesses to Brian that he slept with Stella. Soon after, Brian is killed in his car by the hand.
That night, Lansdale awakens to see the hand trying to kill Anne. Lizzie overhears the commotion and calls the police, and Lansdale chases the hand outside into the nearby barn. The hand tries to attack him, but Lansdale stabs it. The hand crawls to a nearby spare tire and explodes into a puff of smoke, before wrapping itself around Lansdale's throat and causing him to lose consciousness.
Lansdale awakens to find his own hand (the one attached) around his throat while the police skulk around.  The sheriff, with Lizzy, and his deputies attempt to ask Lansdale what happened. Discovering that Anne is still alive, Lansdale attempts to explain what happened when the officers notice a pungent smell permeating the area in the carport around the car, specifically from the trunk.  Lansdale tries to prove that nothing is wrong by opening the trunk, only to be horrified by the sight of Stella and Brian's dead bodies stuffed inside.
At a local insane asylum, a psychologist (Viveca Lindfors) is trying to communicate with Lansdale. She wants him to remember and admit that it was him, not his severed hand, who killed all the victims. He says that the hand wants to kill her because it hates her. Suddenly, the hand (which apparently existed all along) appears from behind the psychiatrist and strangles her. Lansdale, completely taken over by the essence of the hand, looks at her dead body and starts to laugh. He loosens the restraint on his other hand and gets up, presumably to escape from the asylum.


== Cast ==
Michael Caine - Jon Lansdale
Andrea Marcovicci - Anne Lansdale
Annie McEnroe - Stella Roche
Bruce McGill - Brian Ferguson
Viveca Lindfors - Psychologist
Rosemary Murphy - Karen Wagner
Mara Hobel - Lizzie Lansdale
Pat Corley - Sheriff
Charles Fleischer - David Maddow


== Production ==
This was Oliver Stone's second feature film as director following 1974's Seizure, and his first film made for a major studio. Like Seizure, The Hand is a horror film and is derived from a similar premise (an artist's slow descent into madness). The general plot, themes, and characters of The Hand resemble those of The Hands of Orlac and The Beast with Five Fingers, leading some to declare The Hand an unofficial remake or spiritual adaptation of the aforementioned works.
The film was shot partly in Big Bear Lake, California.The film's special effects were provided by Stan Winston and Carlo Rambaldi. Several lifelike animatronic hands were built, each designed for a specific action or motion (i.e. crawling, walking, grabbing). "Mandro," Prince Valiant/Conan The Barbarian-styled comic book character, was drawn by artist Barry Windsor-Smith, who at the time was a real-life illustrator for the Marvel Conan comics.


=== Casting ===
Oliver Stone's first choice for the lead was Academy Award-winner Jon Voight, who declined the role.  Stone also approached other Oscar-winners Dustin Hoffman and Christopher Walken. Michael Caine, however, after the success of his previous film Dressed to Kill, was interested in making another horror film to earn enough to put a down payment on a new garage he was having built, and he agreed to take the part after talks with the director. Caine's experience with Stone resulted in their being friends later in life, although Stone and Caine have not worked together since.


== Critical reception ==
The Hand has a Rotten Tomatoes approval rating of 11% based on 9 reviews, with a weighted average of 3.78/10.
On Metacritic, the film has a weighted average score of 59 out of 100, based on 6 critics, indicating "mixed or average reviews".Allmovie called the film "an overwrought misfire that is best left to horror film completists."The Terror Trap gave it 2/4 stars, stating that the film was "Strictly for Caine filmography completists or Stone enthusiasts."
Rob Gonsalves from eFilmCritic awarded the film 2/5 stars, calling it "inescapably cheesy".Brett Gallman from Oh, the Horror! wrote, "Clearly out the Orlac and Beast With Five Fingers mode, The Hand is just as silly as the fictional comics drawn up by its protagonist, but Stone mostly plays it as a straighter-than-straight schizoid thriller that examines psychological breakdowns more so than the visceral fallout."Vincent Canby from The New York Times gave the film a positive review, writing, "Mr. Stone's screenplay is tightly written, precise and consistent in its methods, and seemingly perfectly realized in the performances of the very good cast headed by Mr. Caine."


== See also ==
The Hands of Orlac
The Beast with Five Fingers
Evil Dead II


== References ==


== External links ==
The Hand on IMDb
The Hand at Rotten Tomatoes
The Hand at AllMovie
The Hand at the American Film Institute Catalog