The Lost Battalion is a true 2001 made-for-television war drama film about the Lost Battalion of World War I, which was cut off and surrounded by German forces in the Argonne Forest during the Meuse-Argonne Offensive of 1918. Later the Versailles treaty put an end to the Great War. The film was directed by Russell Mulcahy, written by James Carabatsos, and starred Rick Schroder as Major Charles Whittlesey. The film was shot in Luxembourg. It is an A&E Original Movie, premiering on the network in 2001. It is also played on A&E's sister networks such as The History Channel. It was released on home video in January 2002.


== Plot ==
The movie follows the events and hardships suffered by the nine companies, roughly 550 men, of the United States Army 77th Infantry Division ("Statue of Liberty") that had been completely cut off and surrounded by German forces a short distance away in the Argonne Forest. The force was led by Maj. Charles W. Whittlesey, who was disparagingly described as a "New York lawyer" by his commanders. The battalion was part of what was supposed to be a three-pronged attack through the German lines. The battalion believed another American force was on its right flank and a French force on its left, not knowing that they had both retreated. The battalion lost contact with headquarters. Whittlesey sent several runners to headquarters, but none of them returned. He ended up having to rely on carrier pigeons to communicate. During the siege, American artillery began firing on the German position in the forest as the Americans cheered. However, the artillery began falling on the American line, killing numerous men in friendly fire. Whittlesey sends a pigeon to headquarters with a message saying they were being hit by their own artillery, saying, "For heaven's sake, stop it". The headquarters receives the message and stops firing. Witnessing the incident, the Germans attack the disoriented American force, but they are repelled by the Americans in fierce fighting and retreat back to their trenches.
After several days and numerous repelled attacks, the Americans are still holding on, despite being desperately low on supplies, forcing them to reuse bandages and other medical supplies and take food off dead soldiers. The Germans capture two Americans, one of them  wounded, and begin using them to try to negotiate with Whittlesey. The uninjured prisoner, Lt. Leak, receives good treatment from a German officer who speaks fluent English, having lived in America for several years. He tries to convince the lieutenant that there is no hope, to which the American responds, "What you're up against, Major, is a bunch of Mick, Dago, Polack and Jew-boy gangsters from New York city: They'll never surrender." Later, the second American eventually agrees to take a message from the Germans to Whittlesey urging surrender after his captor says he wants to save lives. The Germans then send him escorted by a German soldier with a white flag back to the American line with the message. Whittlesey responds by throwing the flag back towards the Germans. The Americans continue to hold despite relentless attacks and low supplies.
Eventually, an American pilot is sent out to search for the force, and flies right over their position. Realizing it is an American plane, the Americans make noise to try to get the pilot's attention. The Germans prepare to shoot at the plane, but their commander tells them to hold their fire because if the pilot knew the Germans' location he would also know the Americans'. The pilot locates the Americans and signals to them, and the Germans begin firing at him. The pilot is critically wounded, but manages to circle the location on his map and navigate back to the airfield, dead by the time he lands. The men at the base see his map and take it back to headquarters. After six days, reinforcements finally arrive at the American lines. The Germans retreated after the fifth day.  Major General Robert Alexander arrives in a car, telling Whittlesey that there will be "commendations and promotions for everyone". Whittlesey is furious about the debacle, and is further angered by Alexander's insistence that the casualties that they suffered were "acceptable losses". Alexander reveals that the battalion's hold in the middle of the German line enabled the Americans to break through the entire line. Alexander offers to take Whittlesey back to headquarters in his car, to which Whittlesey responds, "That's not acceptable, sir. I'll stay with my men."


== Cast ==
Rick Schroder - Maj. Charles W. Whittlesey
Phil McKee - Capt. George G. McMurtry
Jamie Harris - Sgt. Gaedeke
Jay Rodan - Lt. James V. Leak
Adam James - Capt. Nelson M. Holderman
Daniel Caltagirone - Pvt. Philip Cepaglia
Michael Goldstrom - Pvt. Jacob Rosen
André Vippolis - Pvt. Frank Lipasti
Rhys Miles Thomas - Pvt. Bob Yoder
Arthur Kremer - Pvt. Abraham Krotoshinsky
Adam Kotz - Col. Johnson
Justin Scot - Pvt. Omer Richards
Anthony Azizi - Pvt. Nat Henchman
George Calil - Pvt. Lowell R. Hollingshead
Wolf Kahler - Major General (Generalmajor) Freidrich Wilhelm Von Sybel
Joachim Paul Assböck - Maj. Fritz Heinrich Prinz
Michael Brandon - Maj. Gen. Robert Alexander


== Reception ==
The film has received generally positive reviews, praised for its historical accuracy, cast, and intense action. Military.com wrote, "The script is compelling on several levels. Whittlesey, the bookish-looking New York lawyer turned soldier, is the Everyman Warrior that viewers enjoy identifying with. Most would like to think that placed in a similar situation they too would find the courage to act as Whittlesey did. Moreover, the movie offers another take on the classic theme that pits an intrepid underdog David against the prohibitive favorite Goliath. As with the Biblical David — but not with Col. Davy Crockett at the Alamo and Custer at Little Bighorn — the underdog prevails here".BeyondHollywood.com praised the visuals and cinematography for creating a war movie suitable for television without compromising on intensity and action. "The problem with many war films is that after the gore and bloodletting of Saving Private Ryan any war movie looks like an exercise in G-rated filmmaking. The Lost Battalion gets around this problematic obstacle in two ways — it is based entirely on a true story and Jonathan Freeman blesses it with excellent cinematography...The movie is frenetic, chaotic, and completely breathtaking to look at." The review continued, "The Lost Battalion is filled with brutal and personal action that makes you feel like you're there as the men struggle to survive. The movie has immediacy and a sense of claustrophobia as the enemy appears a few dozen yards in front of you, so close that you can see their eyes from your separate positions. The trench warfare aspect of World War I comes alive in bloody color and pale brown dirt."


== Awards ==
The film was nominated for three 2002 Emmy Awards: Outstanding Single Camera Picture Editing; Outstanding Single Camera Sound Mixing; and Outstanding Sound Editing. It won the Motion Picture Sound Editors award for Best Sound Editing in Television (Effects). It won the Christopher Award. It was nominated for the American Cinema Editors award for Best Edited Motion Picture for Commercial Television.


== References ==


== External links ==
The Lost Battalion on IMDb
The Lost Battalion at AllMovie
The Lost Battalion at the TCM Movie Database