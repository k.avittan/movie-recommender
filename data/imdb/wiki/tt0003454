Three Friends (1972) is the third album by British progressive rock band Gentle Giant. It was the band's first release to chart in America, peaking at #197 on the Billboard 200. It is the only album by Gentle Giant to feature drummer Malcolm Mortimore following the departure of Martin Smith.
A concept album, Three Friends deals with three childhood friends whose lives take them very different places. However, each of the three friends are unsatisfied with their new lives. The closing song ends the story on a cliffhanger; whether or not the three friends ever reunite is never revealed.


== Production ==
Three Friends was the band's first self-produced album. The two former albums were produced by David Bowie and T.Rex producer Tony Visconti.
Gary Green's guitar solo on "Peel the Paint" uses an echoplex belonging to Mike Ratledge that Green's brother Jeff, a roadie with Ratledge's band Soft Machine, had borrowed.
The song "Schooldays" starts off with the sounds of a schoolyard playground in England. Ray Shulman told Songfacts about the song: "We started off using the sound effects of the schoolyard so it would be very nostalgic, and that's a whole song about being at school together, and how these friends went their own different ways. One goes into manual labour, one goes into white-collar work, and one is an artist." 


== U.S. and Canadian LP release ==
For the U.S. and Canadian edition, on Columbia Records, the album cover was changed to a modified version of the design from the self-titled UK debut Gentle Giant. The album title and band name were added over the forehead of the giant image and the colour tinting of the cover was changed.


== U.S. CD release ==
The US CD edition on Columbia also has the marker for track 6 in the wrong place. On this version track number 5 ends early at 3:23 instead of 5:51 while incorrectly extending the length of track six from 3:04 to 5:32. This mistake has also carried over to online digital music distributors such as iTunes and Spotify. It has since been corrected.


== Track listing ==
All tracks are written by Kerry Minnear, Derek Shulman, Phil Shulman, and Ray Shulman.


== Personnel ==


=== Gentle Giant ===
Gary Green – electric guitar, mandolin (track 2), tambourine (tracks 2, 5)
Kerry Minnear – Hammond organ (tracks 1, 3–6), piano (tracks 1, 2, 5), electric piano (tracks 2, 3, 5), Mellotron (tracks 2, 6), Minimoog (tracks 1, 4, 6), Clavinet (track 2, 3), Baldwin spinet electric harpsichord (track 2), vibraphone (track 2), bongos (track 2), triangle (track 2), lead vocals (2, 6)
Malcolm Mortimore – drums, concert snare (track 2), hi-hat (track 2), bass drum (track 2)
Derek Shulman – lead vocals (3–6)
Phil Shulman – tenor saxophone (tracks 1, 3, 4), baritone saxophone (track 1, 3), lead vocals (1, 2, 4, 6)
Ray Shulman – bass, fuzz bass (track 1), violins (track 4), electric violin (track 5), twelve-string guitar (track 1), vocals (track 6)


=== Additional personnel ===
Boy's voice on "Schooldays" – Calvin Shulman
Engineer – Martin Rushent
Sleeve Design – Rick Breach
Publisher – Excellency Music
Phonographic Copyright (p) – Malibu Record Productions, Inc.


== Charts ==


== References ==