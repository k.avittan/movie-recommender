The Last of the Duanes is a 1924 silent film western directed by Lynn Reynolds and starring Tom Mix, with his horse Tony the Wonder Horse. It is based on a 1914 Zane Grey novel, Last of the Duanes. A print of the film exists, after it was discovered in a chicken farm in the Czech Republic.This was the second of four films based on the novel; a 1919 silent film adaptation starred William Farnum, a 1930 adaptation starred George O'Brien, and a 1941 adaptation featured George Montgomery.


== Plot ==
Buck Duane, the pacifist son of a famous gunman, shoots the man that killed his father, and then goes on the run. He rescues a beautiful young woman named Jenny from a gang of outlaws, and proves his innocence.


== Cast ==
Tom Mix - Buck Duane
Marian Nixon - Jenny
Brinsley Shaw - Cal Bain
Frank Nelson - Euchre
Lucy Beaumont - Mrs. Bland
Harry Lonsdale - Bland


== Reception ==
The Film Daily said, "Inasmuch as the current production is supposed to be one of Fox's specials it is somewhat disappointing. There is not the continuous fast action that Tom Mix's admirers will expect of a feature in which he appears. The trouble is that Lynn Reynolds has expended too much footage. He could readily have saved nearly two reels and made a much faster entertainment. Mix has a well suited role and enough opportunities to 'do his stuff' and he does it in his usual satisfying style. That there is too much uneventful business intervening between his action bits is no fault of his."The Muncie Evening Press said that "Tom Mix never has been seen to better advantage in a western character role than in his portrayal of Buck Duane." The Buffalo Courier wrote, "Whenever Tom Mix and his famous, clever horse Tony get together in a film play of western life it can be expected that thrilling things are going to happen. It never fails that Tom and Tony get into and out of many startling situations and in The Last of the Duanes, they are said to do even better than usual."A review in Variety read, "The combination of the star and Zane Grey, with the capable Lynn Reynolds directing and a hand-picked cast, is supremely happy for this type of film. Those who like westerns (and who can begin to count them) will rave, and the comparatively few who don't will feel a lot more kindly toward them". The "magnificent" appearance of the western scenery was noted.Mae Tinee of Chicago Daily Tribune wrote, "Full of opportunities for Mr. Mix and his famous horse, Tony, to do their w.k. stuff. both of them in The Last of the Duanes are as you like 'em".


== Censorship ==
In November 1924, The Film Daily complained that the Canadian censors cut the film: 

"A Tom Mix production, The Last of the Duanes, had all of its claws removed, literally speaking, by the Quebec Board of Censors... It was made into a harmless milk-and-water type of drama by the Quebec Board, all gun play having been deleted in the film, although there is plenty of it in the book. 'There seems little doubt that the censor has insisted on cutting the gunplay,' declared the Montreal Star, 'and gunplay in a Western film is as necessary as alcohol in a cocktail.'"


== References ==


== External links ==
The Last of the Duanes on IMDb
The Last of the Duanes at the American Film Institute Catalog
synopsis at AllMovie