The B.C. Rich Warlock is both an electric guitar and bass guitar made by B.C. Rich. It features a distinct jagged shape and two humbucker pickups. It was designed by company founder Bernie Rico in 1969: “This was the only guitar I ever designed at a drafting table, using straight-edges and French curves. It was lots of curves going into straight lines.  At first I thought it was the ugliest guitar I’d ever designed." The design wasn't built until local guitarist Spencer Sercombe of Shark Island, prompted Bernie to do so but once it was it soon found favor in the emerging heavy metal scene. "The introduction of the Warlock in 1981 marked the beginning of B.C. Rich’s rise to iconic status in heavy metal. [...] The confluence of B.C. Rich’s far-out designs and the emerging hair metal culture of the late '70s and early '80s helped cement the brand’s place in the market".Over the years since its introduction the Warlock has become a de facto image of a heavy metal guitar, so much so that in 2015 it was selected to represent Metal in the exhibition Medieval to Metal: The Art & Evolution of the Guitar at the Leigh Yawkey Woodson Art Museum.There are many variations of the Warlock. In ascending order of price, they are:
Electric Guitars:

B.C. Rich Bronze Series Electric Guitar
B.C. Rich Revenge Warlock
B.C. Rich Red Bevel Warlock Guitar and Amp Pack
B.C. Rich Metal Master Warlock
B.C. Rich Warlock IT Electric Guitar
B.C. Rich Kerry King Warlock with Kahler Tremolo
B.C. Rich NJ Deluxe Warlock Electric Guitar
B.C. Rich Deluxe Series Warlock (Rare, only 200 made exclusively for Guitar Center)Electric Bass Guitars:

B.C. Rich Revenge Warlock Electric Bass Guitar
B.C. Rich Bronze Warlock Bass Electric Guitar
B.C. Rich Paolo Gregoletto Signature Warlock Bass
B.C. Rich Warlock NJ Deluxe 4-String Bass
B.C. Rich NJ Deluxe Warlock 5-String Bass


== Notable users ==
Kerry King of Slayer
Chris Poland of Megadeth
Chris Kael of Five Finger Death Punch
Blackie Lawless of W.A.S.P.
Lita Ford
Max Cavalera of Soulfly, formerly of Sepultura
Mick Thomson of Slipknot
Robb Flynn of Machine Head
C.C. DeVille of Poison
Paolo Gregoletto of Trivium (ex-Metal Militia)
Paul Stanley of Kiss
Mick Mars and Nikki Sixx of Mötley Crüe (Shout at the Devil-era)
Craig Goldy of Dio and Giuffria
ICS Vortex
Shakey Graves
Michael Stützer of Artillery
Weird Paul Petroskey
Tripp Eisen, formerly of Static X, Dope and the Murderdolls
Joey Jordison (when playing guitar for the Murderdolls)
Jeordie White
The Seer of Magic Sword


== See also ==
B.C. Rich Mockingbird


== References ==


== External links ==
B.C. Rich Warlock product page at bcrich.com