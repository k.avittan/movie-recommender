The eastern moose (Alces alces americana) is a subspecies of moose that modernly ranges throughout Eastern Canada, New England and northern New York. It inhabits boreal forests and mixed deciduous forests. It is the third largest North American subspecies, after the western moose and the Alaskan moose. Males are aggressive during mating season and will attack anything that provokes them.


== Range and Distribution ==
The eastern moose's range spans a broad swath of northeastern North America, which includes New Brunswick, Newfoundland and Labrador (native to Labrador, introduced to Gander Bay, NF in 1878 and to Howley, NF in 1904), Nova Scotia, Quebec, Eastern Ontario, Maine, New Hampshire, Vermont, Massachusetts, Connecticut, and northern New York.
Eastern moose that roam in Cape Breton Highlands National Park are descended from the western moose that originated from Alberta's Elk Island National Park.The population and range of the eastern moose increased in the decades leading up to the early 2000s as reforestation increased habitat area, but in more recent years diseases and parasites, including winter tick and brainworm, have cut into the population.There are about 350,000 eastern moose, with about ​3⁄4 of them mating every autumn and winter.


== Habitat ==
Eastern moose live in thick boreal or mixed deciduous forests near large amounts of food. In the summer, they move to have access to wetlands and aquatic vegetation, and prefer temperatures under 15 °C (59 °F).


== Diet ==
The eastern moose's diet is similar to that of other moose species. It consumes up to 32 kg (71 lb) a day of terrestrial vegetation, including forbs and shoots from trees such as willow and birch. It also forages for aquatic plants such as lilies and pondweed during the spring and summer. Like other moose species, it lacks upper front teeth but has eight sharp incisors on its lower jaw.


== Size and weight ==
Eastern moose are the third largest subspecies of moose only behind the western moose and the Alaska moose. Males stand on average 1.7–2.0 m (5.6–6.6 ft) at the shoulder and weigh on average 634 kg (1,398 lb). Females stand on average 1.7 m (5.6 ft) at the shoulder and weigh on average 270–360 kg (600–790 lb). Eastern moose antlers have an average span of about 1.5 m (4.9 ft) across.


== Social structure and reproduction ==
Eastern moose are solitary most of the year, only come into contact to mate or compete for breeding rights. Females without calves are mostly peaceful towards humans. Males use mating calls to attract females or challenge other bulls. At 10–11 months young are chased off by their mothers to fend for themselves.


== Hunting ==
Eastern moose are hunted for food and sport during autumn and winter.


== References ==