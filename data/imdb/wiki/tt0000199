The Cheapside Street whisky bond fire in Glasgow on 28 March 1960 was Britain's worst peacetime fire services disaster.  The fire at a whisky bond killed 14 fire service and 5 salvage corps personnel. This fire was overshadowed only by a similar fire in James Watt Street (also in Glasgow) on 19 November 1968, when 22 people died.


== Fire ==
On the evening of 28 March 1960, a fire started in a bonded warehouse owned by Arbuckle, Smith and Company in Cheapside Street, Anderston, Glasgow.
The Glasgow Fire Service was initially alerted by a 999 call at 7:15 pm from the foreman of the Eldorado Ice Cream Company, which was near the whisky bond. He reported smoke coming from a second floor window of the warehouse. In response, two pumps from West Station with Sub Officer James Calder in charge were sent, along with a turntable ladder from Central Station. Also responding initially was the fire boat St Mungo and a salvage tender and crew from the Glasgow Salvage Corps.
The first fire crews arrived at 7:21 pm: after a quick reconnaissance, three more pumps were requested to attend. Crews were informed by civilians that smoke and flame had been seen on the Warroch Street side of the building and additional crews and equipment were sent to investigate. Assistant Firemaster Swanson had now arrived on the scene and having been fully apprised of the situation increased the number of pumps (fire engines) to eight. This message was sent at 7:49 pm. 
Seconds after it was transmitted, an explosion occurred. The warehouse contained over a million gallons of whisky held in 21,000 wooden casks, and 30,000 gallons of rum. As the temperature of the fire increased, some of these casks ruptured, causing a massive boiling liquid expanding vapour explosion (BLEVE) that burst the front and rear walls of the building outwards causing large quantities of masonry to collapse into the street. This collapse instantly killed three firemen in Cheapside Street as well as 11 firemen and five salvagemen who were battling the blaze from the rear of the building in Warroch Street.
By 8:12 pm, Firemaster Chadwick assumed command and upgraded the incident to twenty pumps. At its peak, thirty pumps, five turntable ladders and various special vehicles attended. In all, 450 firefighters from the Greater Glasgow area were involved in fighting the fire, which took a week to extinguish. Witnesses reported seeing bright blue flames leaping 40 feet (12 metres) into the sky, with the glow visible across the entire city. Neighbouring buildings, including a tobacco warehouse, an ice cream factory and the Harland and Wolff engine works, were engulfed.  The recovery of the bodies in Warroch Street was not completed until 10.20 am on 31 March.
The incident remains Britain's worst peacetime fire services disaster.


== Firefighters and salvagemen killed ==

The following is a list of the fire fighters/salvagemen who lost their lives in the line of duty on 28 March 1960 during this event:
Fireman John Allan – Glasgow Fire Service
Fireman Christopher Boyle – Glasgow Fire Service
Sub Officer James Calder – Glasgow Fire Service
Fireman Gordon Chapman – Glasgow Fire Service
Fireman William Crockett – Glasgow Fire Service
Fireman Archibald Darroch – Glasgow Fire Service
Fireman Daniel Davidson – Glasgow Fire Service
Fireman Alfred Dickinson – Glasgow Fire Service
Fireman Alexander Grassie – Glasgow Fire Service
Salvageman Gordon McMillan – Glasgow Salvage Corps
Fireman Ian McMillan – Glasgow Fire Service
Fireman George McIntyre – Glasgow Fire Service
Sub Officer John McPherson – Glasgow Fire Service
Leading Salvageman James McLellan – Glasgow Salvage Corps
Fireman Edward McMillan – Glasgow Fire Service
Salvageman James Mungall – Glasgow Salvage Corps
Superintendent Edward Murray – Glasgow Salvage Corps
Salvageman William Oliver – Glasgow Salvage Corps
Fireman William Watson – Glasgow Fire Service


== Awards for bravery ==
Fireman James Dunlop (24 January 1929 – 28 September 2014) was awarded the George Medal.
Station Officer Peter McGill who later retired as the Deputy Firemaster of Glasgow Fire Service in 1975 was awarded the George Medal
Fireman John Nicholson was awarded the British Empire Medal for Gallantry
Sub Officer Charles Neeson was awarded the British Empire Medal for Gallantry
Fireman George Alexander was awarded the British Empire Medal for Gallantry
Fireman William Watters was awarded the Queens Commendation for Brave Conduct


== Memorial services ==
The men who were killed were buried in the rubble, but were later laid to rest in the fire service tomb in Glasgow Necropolis. A memorial service is held on 28 March each year, with representatives of the fire service and Glasgow City Council present. Memorial services and other observations were held in 2010 to mark the 50th anniversary of the disaster.  Due to the ban on mass gatherings during the COVID-19 pandemic, the 60th anniversary commemoration was a more limited affair, signified by a wreath-laying by Chief Officer Martin Blunden alone.The reverse side of the monument remembers those firefighters lost in the Kilbirnie Street fire in 1972.


== References ==