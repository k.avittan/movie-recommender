Robbery Under Arms is a bushranger novel by Thomas Alexander Browne, published under his nom de plume Rolf Boldrewood. It was first published in serialised form by The Sydney Mail between July 1882 and August 1883, then in three volumes in London in 1888. It was abridged into a single volume in 1889 as part of Macmillan's one-volume Colonial Library series and has not been out of print since.
It is considered a classic of Australian colonial literature, alongside Marcus Clarke's convict novel For the Term of his Natural Life (1876) and Fergus Hume's mystery crime novel The Mystery of a Hansom Cab (1886), and has inspired numerous adaptations in film, television and theatre.


== Plot introduction ==
Writing in the first person, the narrator Dick Marston tells the story of his life and loves and his association with the notorious bushranger Captain Starlight, a renegade from a noble English family. Set in the bush and goldfields of Australia in the 1850s, Starlight's gang, with Dick and his brother Jim's help, sets out on a series of escapades that include cattle theft and robbery under arms.


== Plot summary ==

The book begins with Dick sitting in gaol, with just under one month before his scheduled execution for his crimes. He is given writing material, and begins documenting his life's story.
He starts with his childhood, with a father (Ben) who is prone to violence, particularly when he has been drinking; his mother, his sister (Aileen) and brother, Jim. He documents his first exposure to his father's crimes, the theft of a red calf, and the disapproval of this crime by his mother, who says she thought he had given up stealing since the theft which led to his transportation as a convict from England. Dick's first active involvement in crime, comes where the brothers choose to go cattle duffing (stealing), even though an offer of solid, honest work had been made with neighbour and friend, George Storefield. The divergent lives of the brothers to that of George is a recurring theme of the book from this point forward, as they continue to meet up at different points throughout the story's course. This first theft includes their introduction to Captain Starlight, his Aboriginal assistant, Warrigal, and their hideaway, Terrible Hollow.
Further thefts follow, leading up to the brazen theft of 1000 head, driven overland to Adelaide with Starlight. After the success of this adventure, the brothers "lie low" in Melbourne, where they meet the sisters, Kate and Jeanie Morrison. The brothers return to home for Christmas, leading to incarceration and trial of Dick and Starlight. (The magistrate chooses to refer to Starlight only by this nickname, at the Captain's request). Warrigal helps Dick and Starlight escape to Terrible Hollow. The gang later has its first stage holdup.
The brothers then move to the Turon goldfields. Their prosperity through honest, hard work gives them the chance for escape from the country to start a new life overseas. Jim is re-united with Jeanie Morrison and marries her. Dick meets Kate Morrison again, but her tumultuous nature leads her, in an angry mood, to alert the police to their presence, and they narrowly escape capture and return to the safety of Terrible Hollow.

Seeing no alternative to crime, the gang joins forces with a soon- to- be rival Dan Moran and his friends to stage a major hold up of the armed, escorted stagecoach leaving the goldfields. The robbery is a success, with the members splitting up after sharing the gold takings. Starlight's crew hears word of Moran's planned home invasion of a police informant named Mr Whitman, at a time when Mr Whitman was known to be absent. Marston and Starlight intervene, forcing Moran and his men to leave, thus preventing further harm to the women present and the home being burnt down at the end of the night.
At the height of their infamy, the gang attend the Turon horse race, where Starlight's horse, Rainbow, wins. The same weekend, they attend the wedding of a publican's daughter, Bella Barnes, where Starlight fulfils his earlier promise to dance, unrecognised, with her at her wedding, despite the presence of the entire town, including the goldfields commissioner and other dignitaries. Ben Marston is later ambushed and wounded by bounty hunters. Moran, nearby, releases him, and shoots all four bounty hunters in cold blood, again highlighting the different honour codes between the two gangs. Ben returns to Terrible Hollow, and is nursed by his daughter, Aileen. Aileen and Starlight begin a relationship, and arrange to marry.
Despite the animosity between the rivals, they team up again to rob the home of the Goldfield Commissioner, Mr Knightley, but are met with more resistance than they expected. One of Moran's associates is shot in the skirmish, and Moran is keen to kill Knightley when they later have him face to face. Starlight turns the tables by giving Mr Knightley one of his own pistols. He them proceeds to arrange for Knightley's wife to go to Bathurst and withdraw some cash, and meet Moran's men by "the Black Stump", outside of Bathurst town. Starlight passes the time gambling with Mr Knightley, sharing his food, drink and company. Starlight loses money in the gambling, and arranges to repay by direct payment into his account, as well as paying for the horse he is offered when leaving.

Throughout the book, there have been chance meetings with Dick's childhood friend and neighbour, George Storefield who, in contrast with the Marston boys, works hard, keeps within the law and thrives financially. Dick starts to hold up George, now a successful grazier, businessman, magistrate and landholder, before realising who it was. George offers the brothers safe haven and cattle mustering work, which would allow Dick, Starlight and Jim safer travel to Townsville in Queensland, from where they plan to leave to San Francisco. They accept the offer, but are caught, partially due to betrayal by Warrigal and Kate Morrison along the way, and Starlight and Jim are shot dead. Dick is wounded and brought to trial, bringing the story to where it began, with Dick expecting to be hanged shortly.
In a surprise ending, Dick's sentence is reduced to fifteen years imprisonment due to petitions from Storefield, Knightley and other prominent people. He serves twelve years, is visited occasionally by Gracey Storefield, whom he marries shortly after his release, before moving to a remote area of Queensland to manage a station for her brother, George Storefield.


== Characters in Robbery Under Arms ==
Dick Marston: The narrator, an Australian bushman.
Jim Marston: His brother.
Aileen Marston: Dick and Jim's sister.
Mrs Marston: Ben's wife and the mother of Dick, Jim and Aileen.
Ben Marston: Dick, Jim and Aileen's father, a Lincolnshire man transported for poaching.
Jeanie Morrison/"Jeanie Marston": One of two sisters that the Marston brothers meet in Melbourne while "lying low". Later marries Jim. She is Kate's sister.
Kate Morrison: One of two sisters that the Marston brothers meet in Melbourne while "lying low". She is Jeanie's sister and Jim's sister-in-law.
George Storefield: Neighbour and friend of the Marston family.
Gracy Storefield: George's sister and Dick's wife after serving his prison sentence.
Captain Starlight: An honourable bushranger.
Warrigal: Starlight's Aboriginal assistant.
Rainbow: Starlight's horse.
Mr Whitman: A police informant.
Mr Knightley: The goldfield commissioner.
Mrs Knightley: Mr Knightley's wife.
Miss Falkland: A woman whom Starlight and his crew rescues when Dan Moran leads his men in a home invasion of Mr Whitman.
Bella Barnes: A friend of the bushrangers with whom Starlight vows to dance at her wedding.
Dan Moran: A corrupt bushranger.Note: The spelling of the surname Marston varies in some editions/adaptations. Alternate spellings include "Marsden" and "Masterton".


== Allusions/references from other works ==
The early film adaptations of the book are referenced in the title of Kathryn Heyman's 2006 novel, Captain Starlight's Apprentice.


== Major themes ==
The book, written in first person narrative, contains repeated regrets for the narrator's crimes, highlighting how seemingly minor crimes led to an inescapable life of further crimes. These are contrasted with the success of Dick's hardworking childhood friend, who becomes a successful landowner, merchant and magistrate.
The themes of honour and loyalty are repeated throughout the story, which eventually leads to Dick's redemption from hanging and, having served his time in gaol, a presumed peaceful, safe and legal life.
As a ripping yarn, originally told in periodical installments, the story mostly centres around the lovable villains, who are adventurers and thieves but nevertheless with high moral standards and, in some ways, trapped by circumstances of their own making.


== Literary significance and criticism ==
English author Thomas Wood called the novel "a classic, which for life and dash and zip and colour — all of a period — has no match in all Australian letters."Robbery Under Arms is cited as an important influence on Owen Wister's 1902 novel The Virginian, widely regarded as the first western.


== Allusions/references to actual history, geography and current science ==
In his December 1888 Preface to the New Edition, Browne wrote:

"...though presented in the guise of fiction, this chronicle of the Marston family must not be set down by the reader as wholly fanciful or exaggerated. Much of the narrative is literally true, as can be verified by official records."

Although bushranger Frank Pearson claimed Captain Starlight was based on him, Boldrewood claims his character was a composite, based in part on Harry Redford but primarily on Thomas Smith, known as Captain Midnight. The cattle robbery follows the lifting of about a thousand head by Redford from Bowen Downs Station in 1870. Some of the exploits recounted are based on actual incidents involving bushrangers Daniel Morgan, Ben Hall, Frank Gardiner and John Gilbert, although not all factual events which contributed to the novel took place during the period in which the novel is set. Terrible Hollow, the gang's bush hideaway, is drawn from a sunken valley reported in the Gwydir district.


== Film, TV, or theatrical adaptations ==
1890: Popular stage melodrama by Garnet Walch and Alfred Dampier.
1907: Robbery Under Arms - black and white silent film, one of Australia's earliest features, produced by Charles MacMahon.
1907: Robbery Under Arms - silent film made by J and N Tait, who had made The Story of the Kelly Gang
1911: Captain Starlight, or Gentleman of the Road - another black and white silent film made by Dampier's daughter, Lily, and her husband, Alfred Rolfe.
1920: Robbery Under Arms - silent film written and directed by actor Kenneth Brampton, who also played the role of Captain Starlight.
1957: Robbery Under Arms - sound and technicolour film version for the British Rank Organisation, directed by Jack Lee, starring Peter Finch as Captain Starlight.
1985: Robbery Under Arms - film and mini-series starring Steven Vidler as Dick Marston and Sam Neill as Captain Starlight, released in separate versions for screen and television. Produced by Jock Blair and directed by Donald Crombie and Ken Hannam.Some attempts to adapt the novel in the 1920s and 30s were hindered by the bushranger ban. It has been called "the great lost film" of Ken G. Hall's career.Browne was paid twenty shillings per performance for Alfred Dampier's theatrical adaptation. The novel has also been adapted for radio.
'Rolfe Boldrewood' spent time camped between Warrnambool and Port Fairy in South West Victoria along the banks of the Merri River.  He was particularly interested in nearby 'Tower Hill', a place not unlike the 'Terrible Hollow', which some authorities speculate may have been his inspiration for 'The Hollow'.  At nearby Koroit, 'Henry Handel Richardson' aka Ethel Florence Lindesay, spent some of her childhood, and used the town to describe unfavourable places in her novels.


== Release details ==
1888, First edition, England, Remington and Co Publishers, Hardback (3 vols.)
2006, edited by Paul Eggert and Elizabeth Webby, Australia, University of Queensland Press, Academy Editions of Australian Literature, ISBN 978-0-7022-3574-0, Pub date 31 October 2006, Hardback and paperback. (Full text of original serialised version, including the 29,000 words missing from later publications.)


== Footnotes ==


== References ==
Boldrewood, Rolf. Robbery Under Arms (World Classics series ed.). London: Oxford University Press. ISBN 0192505106.


== External links ==

 Robbery Under Arms at Project Gutenberg, based on the 1889 single volume edition
"Robbery Under Arms" (PDF). (1.53 MiB), based on the 1889 single volume edition
Rolf Boldrewood on IMDb
Elizabeth Webby, Killing the Narrator: National difference in adaptations of Robbery Under Arms JASAL 1, 2002