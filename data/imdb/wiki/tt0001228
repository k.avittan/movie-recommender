The Gambler is a series of American Western television films starring Kenny Rogers as Brady Hawkes, a fictional old-west gambler. The character was inspired by Rogers' hit single "The Gambler".


== Overview ==
There are five movies in the series. The first four are directed by Dick Lowry while the last was directed by Jack Bender. The movies are:

Kenny Rogers as The Gambler (1980) (TV)
Kenny Rogers as The Gambler: The Adventure Continues (1983) (TV)
Kenny Rogers as The Gambler, Part III: The Legend Continues (1987) (TV)
The Gambler Returns: The Luck of the Draw (1991) (TV)
Gambler V: Playing for Keeps (1994) (TV)


== Cast ==


== Films ==


=== Kenny Rogers as The Gambler (1980) ===

Kenny Rogers as The Gambler debuted on CBS on April 8, 1980. It was a ratings and critical success that has spawned four sequels. The show won a Best Edited Television Special Eddie Award and garnered two Emmy Award nominations (for cinematography and editing of a limited series).Kenny Rogers stars as Brady Hawkes, the titular gambler, who embarks on a journey to meet Jeremiah (Ronnie Scribner), the young son he never knew after Jeremiah sends him a letter. Along the way, Brady meets Billy Montana (Bruce Boxleitner) and the two become friends. Billy (while trying to help Brady in his quest) fancies himself as a professional poker player on his own. Although Billy makes mistakes along the way (some of these include trying to find a way to cheat or do some smooth talking), Brady makes sure that he stays on good behavior during a train ride to Yuma. The duo help Jennie Reed (Lee Purcell), a prostitute who has trouble with a train baron. At the end, Brady's son's stepfather (Clu Gulager) is confronted.


==== Production ====
The movie was produced by the fledgling Kragen & Co. production company. The ranch headquarters at Valle Grande in Valles Caldera, New Mexico was used as a filming location.


==== Cast ====
Christine Belford as Eliza
Ronnie Scribner as Jeremiah
Lee Purcell as Jennie Reed
Clu Gulager as Rufe Bennett
Harold Gould as Arthur Stobridge


=== Kenny Rogers as The Gambler: The Adventure Continues (1983) ===
Kenny Rogers again stars as Brady Hawkes in the miniseries Kenny Rogers as The Gambler: The Adventure Continues which premiered on November 28 and 29, 1983. The show was an even bigger ratings success than the first and was nominated for two Emmy Awards (sound editing and sound mixing of a limited series).Billy Montana, Brady, and his son Jeremiah (now played by Charles Fields) are traveling to a gambling event in San Francisco when they encounter the vicious McCourt gang. The McCourt gang force the train to stop and they take Jeremiah hostage and demand a $1 million ransom. Brady and Billy are determined to get Jeremiah back as well as the $1 million ransom which belonged to the train boss. Brady and Billy find help in their mission and meet a female bounty hunter Kate Muldoon played by Linda Evans. Kate is the fastest female gun in the west. They form a posse together in a race to save Jeremiah.


==== Cast ====
Linda Evans as Kate Muldoon
Johnny Crawford as Masket
Charles Fields as Jeremiah
David Hedison as Carson
Bob Hoy as Juno
Brion James as Reece
Paul Koslo as Holt
Cameron Mitchell as Col. Greeley
Mitchell Ryan as Charlie McCourt
Gregory Sierra as Silvera


=== Kenny Rogers as The Gambler, Part III: The Legend Continues (1987) ===
Kenny Rogers as The Gambler, Part III: The Legend Continues was broadcast on November 22, 1987.In this installment, Brady Hawkes and Billy Montana help protect some Sioux Indians from the government and some cattle thieves.


==== Cast ====
George Kennedy as Gen. Nelson Miles
Linda Gray as Mary Collins
Marc Alaimo as Pvt. Bob Butler
Jeff Allin as Homesteader
George American Horse as Chief Sitting Bull
Michael Berryman as Cpl. Catlett
Sam Boxleitner as Boy with Hat
Jeffrey Alan Chandler as Plow Salesman
Melanie Chartoff as Deborah
Richard Chaves as Iron Dog
Matt Clark as Sgt. Grinder
Charles Durning as Sen. Henry Colton


=== The Gambler Returns: The Luck of the Draw (1991) ===
The Gambler Returns: The Luck of the Draw is a 1991 television film starring Kenny Rogers and Reba McEntire. Rogers reprises Hawkes in the fourth installment of the series. The film originally aired on NBC on November 3, 1991. It was nominated for a Costume Design Emmy.


==== Plot ====
It's 1906 and professional gambling will be outlawed in just three weeks. Therefore, Burgundy Jones (McEntire) has just that long to get Brady Hawkes safely to San Francisco for the last poker tournament, with a very special mystery player. This is made more difficult, as Hawkes is still smarting after a hard-fought loss to another professional poker player in England, who will also be at the tournament.


==== Production ====
The film features Rogers' character running across a galaxy of old TV western characters played by the original actors, including Gene Barry as Bat Masterson, Hugh O'Brian as Wyatt Earp, Jack Kelly as Bart Maverick, Clint Walker as Cheyenne Bodie, David Carradine as Kung Fu's Caine, Chuck Connors and Johnny Crawford from The Rifleman, Brian Keith as The Westerner, James Drury and Doug McClure from The Virginian, Paul Brinegar from Rawhide, and Reba McEntire as Burgundy Jones.
The characters are attending a poker game said to be in honor of "the late Mr. Paladin" from Have Gun — Will Travel (the actor who played him, Richard Boone, had died in 1981). The game was played at the hotel at which Paladin lived. The game's dealer is "Hey Girl", Paladin's friend. As each veteran character appears, a few bars from his original series' theme momentarily plays in the background, reminiscent of a doorbell.  Everyone in the film, including President Theodore Roosevelt (Claude Akins), seems openly thrilled to encounter Brady Hawkes.
The Gambler Returns was directed by Dick Lowry.


==== Cast ====


=== Gambler V: Playing for Keeps (1994) ===
Gambler V: Playing for Keeps is the fifth installment of The Gambler series and the first not directed by Dick Lowry, with Jack Bender taking the helm. The movie premiered on October 2, 1994.


==== Plot ====
Brady Hawkes' son, Jeremiah (with Kris Kamm as the third actor in the role) gets involved with outlaws Butch Cassidy (Scott Paulin) and the Sundance Kid (Brett Cullen). Brady tries to save him before he winds up in jail or dead.


==== Cast ====
Scott Paulin as Butch Cassidy
Brett Cullen as Sundance Kid
Mariska Hargitay as Etta Place
Kris Kamm as Jeremiah Hawkes
Stephen Bridgewater as Flatnose Bill Curry
Richard Riehle as Frank Dimaio
Ned Vaughn as Ford Hayes
Martin Kove as Black Jack


=== Future films ===
On March 15, 2011 Kenny Rogers told Jimmy Fallon on his television show, Late Night with Jimmy Fallon, that he was asked if he would want to be in another Gambler movie.  He began by saying that he had a bad knee and thought it would be hard for him, but continued and said that the first scene in the movie would be a shootout.  Supposedly he would get shot in the shoulder and knee to cover his physical disabilities. However, in the years that followed Rogers announced his retirement from show business and stated that his 2016 tour would be his last and after this he would be spending his time with family. Rogers later died on March 20, 2020.


== In other media ==


=== Books ===
Pirtle, Caleb, III; Dobbs, Frank Q. (1996). Jokers Are Wild. Kenny Rogers' The Gambler. 1. Penguin Group. ISBN 9781572970533.
Pirtle, Caleb, III; Dobbs, Frank Q. (1996). Dead Man's Hand. Kenny Rogers' The Gambler. 2. Boulevard Books. ISBN 9781572970939.
Pirtle, Caleb, III; Dobbs, Frank Q. (1998). Dying Man's Bluff. Kenny Rogers' The Gambler. Berkley Publishing Group. ISBN 9781572971813.


=== Slot machine ===
A video slot machine based on The Gambler can be found in most Las Vegas casinos. It was manufactured by now-defunct International Game Technology.


== References ==


== External links ==
IMDBKenny Rogers as The Gambler (1980) on IMDb
Kenny Rogers as The Gambler: The Adventure Continues (1983) on IMDb
Kenny Rogers as The Gambler, Part III: The Legend Continues (1987) on IMDb
The Gambler Returns: The Luck of the Draw (1991) on IMDb
Gambler V: Playing for Keeps (1994) on IMDbAllMovieKenny Rogers as The Gambler (1980) at AllMovie
Kenny Rogers as The Gambler: The Adventure Continues (1983) at AllMovie
Kenny Rogers as The Gambler, Part III: The Legend Continues (1987) at AllMovie
The Gambler Returns: The Luck of the Draw (1991) at AllMovie
Gambler V: Playing for Keeps (1994) at AllMovie