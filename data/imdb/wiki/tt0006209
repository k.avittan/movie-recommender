In mathematics, a function on a normed vector space is said to vanish at infinity if 

  
    
      
        f
        (
        x
        )
        →
        0
      
    
    {\displaystyle f(x)\to 0}
   as 
  
    
      
        ‖
        x
        ‖
        →
        ∞
        .
      
    
    {\displaystyle \|x\|\to \infty .}
  For example, the function 

  
    
      
        f
        (
        x
        )
        =
        
          
            1
            
              
                x
                
                  2
                
              
              +
              1
            
          
        
      
    
    {\displaystyle f(x)={\frac {1}{x^{2}+1}}}
  defined on the real line vanishes at infinity. The same applies to the function

  
    
      
        h
        (
        x
        ,
        y
        )
        =
        
          
            1
            
              x
              +
              y
            
          
        
      
    
    {\displaystyle h(x,y)={\frac {1}{x+y}}}
  where 
  
    
      
        x
      
    
    {\displaystyle x}
   and 
  
    
      
        y
      
    
    {\displaystyle y}
   are real and correspond to the point 
  
    
      
        (
        x
        ,
        y
        )
      
    
    {\displaystyle (x,y)}
   on 
  
    
      
        
          
            R
          
          
            2
          
        
      
    
    {\displaystyle \mathbb {R} ^{2}}
  .More generally, a function 
  
    
      
        f
      
    
    {\displaystyle f}
   on a locally compact space (which may or may not have a norm) vanishes at infinity, if given any positive number 
  
    
      
        ϵ
      
    
    {\displaystyle \epsilon }
  , there exists a compact subset 
  
    
      
        K
      
    
    {\displaystyle K}
   such that 

  
    
      
        ‖
        f
        (
        x
        )
        ‖
        <
        ϵ
      
    
    {\displaystyle \|f(x)\|<\epsilon }
  whenever the point 
  
    
      
        x
      
    
    {\displaystyle x}
   lies outside of 
  
    
      
        K
      
    
    {\displaystyle K}
  .In other words, for each positive number 
  
    
      
        ϵ
      
    
    {\displaystyle \epsilon }
   the set
  
    
      
        
          {
          
            x
            ∈
            X
            :
            ‖
            f
            (
            x
            )
            ‖
            ≥
            ϵ
          
          }
        
      
    
    {\displaystyle \left\{x\in X:\|f(x)\|\geq \epsilon \right\}}
  is compact.
For a given locally compact space 
  
    
      
        Ω
      
    
    {\displaystyle \Omega }
  , the set of such functions

  
    
      
        f
        :
        Ω
        →
        
          K
        
      
    
    {\displaystyle f:\Omega \rightarrow \mathbb {K} }
  (where 
  
    
      
        
          K
        
      
    
    {\displaystyle \mathbb {K} }
   is either 
  
    
      
        
          R
        
      
    
    {\displaystyle \mathbb {R} }
   or 
  
    
      
        
          C
        
      
    
    {\displaystyle \mathbb {C} }
  ) forms an 
  
    
      
        
          K
        
      
    
    {\displaystyle \mathbb {K} }
  -vector space with respect to pointwise scalar multiplication and addition, often denoted 
  
    
      
        
          C
          
            0
          
        
        (
        Ω
        )
      
    
    {\displaystyle C_{0}(\Omega )}
  .
Here, note that the two definitions could be inconsistent with each other: if 
  
    
      
        f
        (
        x
        )
        =
        ‖
        x
        
          ‖
          
            −
            1
          
        
      
    
    {\displaystyle f(x)=\|x\|^{-1}}
   in an infinite dimensional Banach space, then 
  
    
      
        f
      
    
    {\displaystyle f}
   vanishes at infinity by the 
  
    
      
        ‖
        f
        (
        x
        )
        ‖
        →
        0
      
    
    {\displaystyle \|f(x)\|\to 0}
   definition, but not by the compact set definition.
Aside from this difference, both of these notions correspond to the intuitive notion of adding a point at infinity, and requiring the values of the function to get arbitrarily close to zero as one approaches it. This definition can be formalized in many cases by adding an (actual) point at infinity.


== Rapidly decreasing ==

Refining the concept, one can look more closely to the rate of vanishing of functions at infinity. One of the basic intuitions of mathematical analysis is that the Fourier transform interchanges smoothness conditions with rate conditions on vanishing at infinity. The rapidly decreasing test functions of tempered distribution theory are smooth functions that are 

  
    
      
        O
        (
        
          |
        
        x
        
          
            |
          
          
            −
            N
          
        
        )
      
    
    {\displaystyle O(|x|^{-N})}
  for all 
  
    
      
        N
      
    
    {\displaystyle N}
  , as 
  
    
      
        
          |
        
        x
        
          |
        
        →
        ∞
      
    
    {\displaystyle |x|\to \infty }
  , and such that all their partial derivatives satisfy the same condition too. This condition is set up so as to be self-dual under Fourier transform, so that the corresponding distribution theory of tempered distributions will have the same good property.


== See also ==
Infinity
Projectively extended real line
Zero of a function


== References ==


== Bibliography ==
Hewitt, E and Stromberg, K (1963). Real and abstract analysis. Springer-Verlag.CS1 maint: multiple names: authors list (link)