The Seven Daughters of Eve is a 2001 book by Bryan Sykes that presents the science of human mitochondrial genetics to a general audience. Sykes explains the principles of genetics and human evolution, the particularities of mitochondrial DNA, and analyses of ancient DNA to genetically link modern humans to prehistoric ancestors.
Following the developments of mitochondrial genetics, Sykes traces back human migrations, discusses the "out of Africa theory" and casts serious doubt upon Thor Heyerdahl's theory of the Peruvian origin of the Polynesians, which opposed the theory of their origin in Indonesia.  He also describes the use of mitochondrial DNA in identifying the remains of Emperor Nicholas II of Russia, and in assessing the genetic makeup of modern Europe.
The title of the book comes from one of the principal achievements of mitochondrial genetics, which is the classification of all modern Europeans into seven groups, the mitochondrial haplogroups.  Each haplogroup is defined by a set of characteristic mutations on the mitochondrial genome, and can be traced along a person's maternal line to a specific prehistoric woman.  Sykes refers to these women as "clan mothers", though these women did not all live concurrently.  All these women in turn shared a common maternal ancestor, the Mitochondrial Eve.
The last third of the book is spent on a series of fictional narratives, written by Sykes, describing his creative guesses about the lives of each of these seven "clan mothers".  This latter half generally met with mixed reviews in comparison with the first part.


== Mitochondrial haplogroups in The Seven Daughters of Eve ==
The seven "clan mothers" mentioned by Sykes each correspond to one (or more) human mitochondrial haplogroups.

Ursula: corresponds to Haplogroup U (specifically U5, and excluding its subgroup K)
Xenia: corresponds to Haplogroup X
Helena: corresponds to Haplogroup H
Velda: corresponds to Haplogroup V
Tara: corresponds to Haplogroup T
Katrine: corresponds to Haplogroup K
Jasmine: corresponds to Haplogroup J


== Additional daughters ==
Sykes wrote in the book that there were seven major mitochondrial lineages for modern Europeans, though he subsequently wrote that with the additional data from Scandinavia and Eastern Europe, Ulrike (see below) could have been promoted to be the eighth clan mother for Europe.Others have put the number at 10, 12 or even 18.  These additional "daughters" generally include haplogroups I, M and W.  For example, a 2004 paper re-mapped European haplogroups as H, J, K, N1, T, U4, U5, V, X and W.. Richards, Macaulay, Torroni and Bandelt include I, W and N1b as well as Sykes' '7 daughters' within their 2002 pan-european survey (but - illustrating how complex the question can be - also separate out pre-V, HV1 and pre-HV1, and separate out U to include U1, U2, U3, U4 and U7 as well as U5). Likewise, Sykes has invented names for an additional 29 "clan mothers" worldwide (of which four were native American, nine Japanese and 12 were from Africa), each corresponding to a different haplogroup identified by geneticists: "Fufei, Ina, Aiyana/Ai, Yumi, Nene, Naomi, Una, Uta, Ulrike, Uma, Ulla, Ulaana, Lara, Lamia, Lalamika, Latasha, Malaxshmi, Emiko, Gaia, Chochmingwu/Chie, Djigonasee/Sachi, Makeda, Lingaire, Lubaya, Limber, Lila, Lungile, Latifa and Layla."


== Editions ==
Bryan Sykes The Seven Daughters of Eve: The Science That Reveals Our Genetic Ancestry, W.W. Norton, 17 May 2002, hardcover, 306 pages, ISBN 0-393-02018-5


== References ==