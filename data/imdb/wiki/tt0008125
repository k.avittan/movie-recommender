"Hungry Heart" is a song written and performed by Bruce Springsteen on his fifth album, The River. It was released as the album's lead single in 1980 and became Springsteen's first big hit on the Billboard Hot 100 chart peaking at number five.


== History ==
When Springsteen met Joey Ramone in Asbury Park, New Jersey, Ramone asked him to write a song for The Ramones. Springsteen composed "Hungry Heart" that night, but decided to keep it for himself on the advice of his producer and manager, Jon Landau. Previously, upbeat and catchy Springsteen songs such as "Blinded by the Light", "Because the Night", and "Fire" had been given away and become hits for others, and Landau preferred the trend not continue.
The title is drawn from a line in Alfred, Lord Tennyson's famous poem "Ulysses": "For always roaming with a hungry heart".
Springsteen's voice was slightly sped up on the recording, producing a higher-pitched vocal, a technique first used by Brian Wilson in the production of "Caroline, No" in 1966. (Dire Straits had done the same thing on 1978's "Setting Me Up".) Mark Volman and Howard Kaylan of The Turtles sang backup. The mix of songwriting and production techniques was successful, and "Hungry Heart" reached No. 5 on the Billboard Hot 100 in late 1980 and was his biggest hit until "Dancing in the Dark" hit No. 2 in 1984. In the subsequent Rolling Stone Readers' Poll, "Hungry Heart" was voted Best Single for the year.
"Hungry Heart" was used on several movie soundtracks over the years; including the obscure 1982 Israeli film Kvish L'Lo Motzah (a.k.a. Dead End Street, which was the first motion picture to feature Springsteen music), the 1983 Tom Cruise hit movie Risky Business, the 1992 dramedy Peter's Friends, and the 1998 Adam Sandler comedy The Wedding Singer. In 2000 the song was used in the film The Perfect Storm as well as in 2013 in Warm Bodies.  It was also featured in the eleventh episode of season 2 of The Handmaid's Tale.
The single was his first British hit in the United Kingdom, although only reaching No. 44 on the UK Singles Chart. It did better in 1995 when reissued in conjunction with his Greatest Hits album, and reached No. 28.
On the day of his murder in December 1980, John Lennon said he thought "Hungry Heart" was "a great record" and even compared it to his single "(Just Like) Starting Over", which was actually released three days after "Hungry Heart".
The "Everybody's Got a Hungry Heart" episode of Japanese anime series Battle B-Daman is named after the lyric in the song. Weezer mentions the song in their 2008 song "Heart Songs" on their album Weezer ("The Red Album").
Billboard Magazine described "Hungry Heart" as "a magnificently styled midtempo love song" with an "extremely memorable" hook. The aggregation of critics' lists at acclaimedmusic.net rated this song as the No. 38 song of 1980, as well as No. 341 of the 1980s and No. 1870 all time.  The song has also been listed as the No. 1 single of 1980 by  Dave Marsh and Kevin Stein and as one of the 7500 most important songs from 1944 through 2000 by Bruce Pollock.  It was also listed as No. 625 on Marsh's list of the 1001 Greatest Singles Ever Made.


== Music video ==
No music video was recorded for the initial release in 1980, however a video clip was filmed for the song's re-release in 1995. This was filmed on July 9, 1995 at the tiny Café Eckstein in Berlin, Prenzlauer Berg. The video featured German rock star Wolfgang Niedecken and his "Leopardefellband", although neither are heard on the actual audio track, as this so-called "Berlin 95" version (which was also released on CD singles) just features Springsteen's live vocals and audience noise laid over the song's original 1980 E Street Band studio recording.


== Track listing ==
"Hungry Heart" – 3:19
"Held Up Without a Gun" – 1:15"Held Up Without a Gun" is a track from The River sessions that began a Springsteen tradition of using songs that did not appear on his albums as B-sides.  A River Tour performance of it is included on The Essential Bruce Springsteen compilation album's optional disc.  This was the only live performance of the song in a regular Springsteen concert (apart from rehearsals) until the song reappeared twice during the Magic tour and once during the Wrecking Ball Tour. The studio version remained unavailable on CD until the release of 2015 box set The Ties That Bind: The River Collection.
The cover of the single sleeve shows The Empress Hotel, one of Asbury Park's fading landmarks of the time.


== Live performance history ==

At the beginning of The River Tour, Springsteen and the E Street Band played the song as an instrumental for the first verse and chorus.  During the November 20, 1980, show in Chicago's Rosemont Horizon, right after the single hit the Top 10, the audience spontaneously sang the lyrics back to the band during this intro. A tradition was thus born of Springsteen always letting the audience sing the first verse and chorus.
Such a performance from December 28, 1980 at Nassau Coliseum (with Volman and Kaylan guesting) is included on the Live/1975-85 box set, but the ritual became even stronger during the 1984–1985 Born in the U.S.A. Tour, when "Hungry Heart" was a featured selection early in the second set. Even in Japan, where not many fans knew the words, this was still one of the best-received songs of his performances.
"Hungry Heart" was a regular in Springsteen band concerts through the early 1990s, but beginning with the 1999–2000 E Street Band Reunion Tour, it has only been irregularly performed, exemplifying a later-era Springsteen practice of avoiding his most popular radio hits. When it is played, it is often with guest artists singing along, since it is one of Springsteen's easiest songs to perform.
"Hungry Heart" regained its prominent placement in setlists during the 2009 Working On A Dream Tour and 2012–2013 Wrecking Ball Tour, where Springsteen would crowd surf during the middle of the song.


== Charts ==


== Certifications ==


== Cover versions ==
The song has also been recorded by Rod Stewart, Jesse Malin, Minnie Driver, Mike Love, Aidan Moffat, Paul Young, Smokie, Lucy Wainwright Roche, Paul Baribeau and Ginger Alford, Sexton Blake, The School, The Chuck Norris Experiment, Juanes, and The Mavericks.


== References ==


== Bibliography ==
Marsh, Dave.  Glory Days: Bruce Springsteen in the 1980s.  Pantheon Books, 1987.  ISBN 0-394-54668-7.


== External links ==
Lyrics & Audio clips from Brucespringsteen.net
Killing Floor song performance database