The Woman in the Window is a thriller novel by Dan Mallory writing under the pen name "A. J. Finn".


== Plot Summary ==
Anna Fox suffers from agoraphobia due to a traumatic car accident and lives a reclusive life at her large home in New York City. She has recently separated from her husband, Ed, who has custody of their nine-year-old daughter, Olivia. However, they frequently talk on the phone.
To pass the time, Anna spends her days drinking too much alcohol, playing online chess, communicating with other recluses through the "Agora online forum," watching old movies, and meeting with her shrink and physical therapist. She also spends time spying on her neighbors, including the Russells, a family that moved in across the street. There is Ethan, the reserved and polite teenage son; Alistair, the controlling father; and Jane, a friendly woman with whom Anna shares many interests. One evening, while looking out the window, Anna witnesses Jane being stabbed and calls the police. The Russells deny that any sort of attack took place. The police, including lead investigators Detective Little and Norelli, also don't believe Anna's story as another woman who claims to be Jane is alive and uninjured. Anna insists the woman claiming to be Jane is not the same woman she met before.
Anna has a number of encounters with the Russells and becomes convinced that something is suspicious about them. After she receives an anonymous email with a picture of herself sleeping, she calls the police. A detective confronts her with the tragic truth: her husband and daughter died in the car accident that triggered her agoraphobia, and she has been imagining her conversations with them. Knowing her medications can cause hallucinations, they theorize that Anna could have taken the picture and emailed it to herself. Anna realizes that the murder may have been a hallucination as well.
Anna finds a picture she had taken of the "Jane" she met and shows it to Ethan. He breaks down and tells her the truth: Jane and Alistair are his adoptive parents, and Katie, the woman Anna assumed was Jane, is his biological mother. Katie tracked the family down in order to see her son again, but her frequent unwanted visits led to an altercation with Jane, which resulted in Katie being stabbed. Alistair and Jane hid the body and lied to the police.
Anna urges Ethan to talk to the police, but he convinces her that he will talk his parents into turning themselves in. Ethan later sends a text confirming he and his parents are going to the police. That night, Anna realizes that Ethan mentioned something that he couldn't have known. She is startled by Ethan in her room, where he confesses that he has psychopathic tendencies, that he has been sneaking into her home at night to watch her, and that he has stalked other women as well. He reveals that he was the one who killed Katie because of his resentment about the abuse and neglect he faced as a child under her care, and that his father knew, but kept it a secret to protect Jane.
Realizing that he intends to kill her too, Anna flees. He pursues her to the roof where she pushes him through an old skylight to his death. Alistair is arrested as an accessory to Katie's murder, and Anna slowly begins to rebuild her life.


== Characters ==
Anna Fox:  a reclusive, alcoholic 39-year-old woman who suffers from severe agoraphobia and anxiety
Ed Fox:  Anna's estranged husband who frequently calls her on the phone
Olivia Fox: Anna's 9-year-old daughter whom lives with Ed
Ethan Russell: the shy and polite teenage son who lives across the street and becomes friends with Anna
Jane Russell: a mysterious woman who is portrayed as two different people, one maybe imagined by Anna
Alistair Russell: an intimidating man who Anna believes stabbed and killed Jane
David Winters: Anna's tenant who lives in the basement of her large home and frequently does house work for the neighborhood
Dr. Julian Fielding: Anna's therapist who visits her once a week
Bina: Anna's physical therapist who works on helping her recover from the car accident


== Reception ==
The review aggregator website Book Marks reported that 38% of critics gave the book a "rave" review, whilst another 38% of the critics expressed "positive" impressions, based on a sample of 13 reviews.According to Alexandra Alter in The New York Times, the book is "strikingly similar" to the earlier published novel, Saving April, by Sarah A. Denzil, with "parallels [that] are numerous, and detailed."  The book's publisher responded, "the outline of The Woman in the Window, including characters and main plot points, was fully formed by Sept. 20, 2015, before Saving April was released."


== Film adaptation ==


== References ==