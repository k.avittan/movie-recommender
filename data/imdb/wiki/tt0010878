The phrase "God helps those who help themselves" is a motto that emphasizes the importance of self-initiative and agency. The expression is still famous around the globe and used to inspire people for self-help. The phrase originated in ancient Greece as "the Gods help those who help themselves" and may originally have been proverbial. It is illustrated by two of Aesop's Fables and a similar sentiment is found in ancient Greek drama. Although it has been commonly attributed to Benjamin Franklin, the modern English wording appears earlier in Algernon Sidney's work.
The phrase is often mistaken as a scriptural quote, though it is not stated verbatim in the Bible. Some Christians have criticized the expression as being contrary to the Bible's message of God's grace. A variant of the phrase can also be found in the Quran (13:11).


== Origin ==

The sentiment appears in several ancient Greek tragedies. Sophocles, in his Philoctetes (c. 409 BC), wrote, "No good e'er comes of leisure purposeless; And heaven ne'er helps the men who will not act."Euripides, in the Hippolytus (428 BC), mentions that, "Try first thyself, and after call in God; For to the worker God himself lends aid." In his Iphigeneia in Tauris, Orestes says, "I think that Fortune watcheth o'er our lives, surer than we. But well said: he who strives will find his gods strive for him equally."A similar version of this saying "God himself helps those who dare" better  translated as "divinity helps those who dare" "audentes deus ipse iuuat" comes from Ovid, Metamorphoses, 10.586. The phrase is spoken by Hippomenes when contemplating whether to enter a foot race against Atalanta for her hand in marriage. If Hippomenes were to lose, however, he would be killed. Hippomenes decides to challenge Atalanta to a race and, with the aid of Venus, Hippomenes was able to win the race.The same concept is found in the fable of Hercules and the Wagoner, first recorded by Babrius in the 1st century AD. In it, a wagon falls into a ravine, or in later versions becomes mired, but when its driver appeals to Hercules for help, he is told to get to work himself. Aesop is also credited with a similar fable about a man who calls on the goddess Athena for help when his ship is wrecked and is advised to try swimming first. It has been conjectured that both stories were created to illustrate an already existing proverb.The French author Jean de La Fontaine also adapted the first of these fables as Le chartier embourbé (Fables VI.18) and draws the moral Aide-toi, le ciel t'aidera (Help yourself and Heaven will help you too).  A little earlier, George Herbert had included "Help thyself, and God will help thee" in his proverb collection, Jacula Prudentum (1651). But it was the English political theorist Algernon Sidney who originated the now familiar wording, "God helps those who help themselves", apparently the first exact rendering of the phrase. Benjamin Franklin later used it in his Poor Richard's Almanack (1736) and has been widely quoted.


=== Islamic texts ===
A passage with similar sentiments can be found in the Quran:Indeed Allah will not change the conditions of a population until they change what is in themselves.  Qur'an 13:11
It has a different meaning in that it implies that helping oneself is a prerequisite for expecting the help of God.
Trust in God But Tie Your Camel is an Arab proverb with a similar meaning. It is also one of the reported sayings of the Islamic prophet Muhammad. According to Tirmidhi, one day Mohammed noticed a Bedouin leaving his camel without tying it. He asked the Bedouin, "Why don't you tie down your camel?" The Bedouin answered, "I placed my trust in Allah." At that, Mohammed said, "Tie your camel and place your trust in Allah."


=== Other historical uses ===
The French society Aide-toi, le ciel t'aidera (Help yourself and Heaven will help you too) played an important role in bringing about the July Revolution of 1830 in France.The Canadian society Aide-toi, le Ciel t’aidera is credited with introducing the celebration of Saint-Jean-Baptiste Day for French Canadians, and was founded by Louis-Victor Sicotte.
Aide-toi et Dieu t'aidera (Help yourself, and God will help you) was the motto on the ship's wheel of the famous UK-built Confederate sea raider CSS Alabama, captained by Raphael Semmes during the American Civil War.


== Prevalence and assessment ==
The phrase is often quoted to emphasize the importance of taking initiative. There is also a relationship to the Parable of the Faithful Servant, and the Parable of the Ten Virgins, which has a similar eschatological theme: be prepared for the day of reckoning. However, the argument has been made that this is a non-Biblical concept.


=== Christian Scripture ===
While the term does not appear verbatim in Christian scriptures, these passages are used to suggest an ethic of reliance on God.

Colossians 3:23 - Whatever you do, work at it with all your heart, as working for the Lord, not for men.
Deuteronomy 28:8 - The Lord will send a blessing on your barns and on everything you put your hand to.
Proverbs 6:10–12  - A little sleep, a little slumber, a little folding of the hands to rest—and poverty will come on you like a bandit and scarcity like an armed man.
Proverbs 12:11 - He who works his land will have abundant food, but he who chases fantasies lacks judgment.
Proverbs 12:24 - Diligent hands will rule, but laziness ends in slave labor.
Proverbs 13:4 - The sluggard craves and gets nothing, but the desires of the diligent are fully satisfied.
Proverbs 21:31 - The horse is made ready for the day of battle, but victory rests with the Lord.
Matthew 5:3–4 - God blesses those who realize their need for him; and who mourn will be comforted.
1 Timothy 5:8 - If anyone does not provide for his relatives, and especially for his immediate family, he has denied the faith and is worse than an unbeliever.Reliance upon God is not mentioned, but is strongly implied.
Conversely, instances where Jesus served or healed someone would be evidence of God helping those who cannot help themselves. (See Mark 6:34; Mark 1:30-31; and Mark 10:46-52.)


=== Prevailing views ===
The belief that this is a phrase that occurs in the Bible, or is even one of the Ten Commandments, is common in the United States. The beliefs of Americans regarding this phrase and the Bible has been studied by Christian demographer and pollster George Barna of The Barna Group. To the statement "The Bible teaches that God helps those who help themselves"; 53% of Americans agree strongly, 22% agree somewhat, 7% disagree somewhat, 14% disagree strongly, and 5% stated they don't know. Of "born-again" Christians 68% agreed, and 81% of non "born-again" Christians agreed with the statement. In a February 2000 poll, 53% strongly agreed and 22% agreed somewhat that the Bible teaches the phrase. Of the 14 questions asked, this was the least biblical response, according to Barna.  A poll in the late 1990s showed the majority (81%) believe the concept is taught by the Bible, another stating 82%.Despite being of non-Biblical origin the phrase topped a poll of the most widely known Bible verses. Seventy-five percent of American teenagers said they believed that it was the central message of the Bible.Barna critiques this as evidence of Americans' unfamiliarity with the Bible and believes that the statement actually conflicts with the doctrine of Grace in Christianity. It "suggests a spiritual self-reliance inconsistent with Christianity" according to David Kinnaman, vice president of the Barna Research Group. Christian minister Erwin Lutzer argues there is some support for this saying in the Bible (2 Thessalonians 3:10, James 4:8); however, much more often God helps those who cannot help themselves, which is what grace is about (the parable of the Pharisee and the Publican, Ephesians 2:4–5, Romans 4:4–5).  The statement is often criticised as espousing a Semi-Pelagian model of salvation, which most Christians denounce as heresy.


== See also ==
Trust in God and keep your powder dry.
Arbeit Macht Frei
Jedem das Seine


== References ==


== External links ==
 The dictionary definition of god helps those who help themselves at Wiktionary
 Works related to Hercules and the Wagoner at Wikisource
 The dictionary definition of devil take the hindmost at Wiktionary - a negative form