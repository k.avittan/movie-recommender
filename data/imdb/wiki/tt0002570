The Asterisk War: The Academy City on the Water (Japanese: 学戦都市アスタリスク, Hepburn: Gakusen Toshi Asutarisuku, lit. "Academy Battle City Asterisk") is a Japanese light novel series written by Yū Miyazaki, and illustrated by Okiura. Media Factory has published ten volumes since September 25, 2012, under their MF Bunko J imprint. The series was later serialized from January 2013 in Media Factory's seinen manga magazine Monthly Comic Alive where it was adapted into a manga. The artwork for the manga has been done by Ningen, three volumes have been collected into tankōbon. Eventually, an anime television series adaptation by A-1 Pictures was made which began airing on October 3, 2015. The anime aired for two seasons at twelve episodes each from October 3, 2015, to June 18, 2016. Other media adaptations include a video game called The Asterisk War: Houka Kenran which was released in Japan on January 28, 2016.
The anime has been licensed by Aniplex of America in North America, Madman Entertainment in Australasia and MVM Films in the United Kingdom. The light novels and manga were licensed and localized by Yen Press beginning summer 2016.


== Plot ==
In the 21st century, Earth is in a rapidly declining economic state as megacorporations known as the Integrated Enterprise Foundations (統合企業財体, Tōgō Kigyō Zaitai), rose to power after the Invertia (落星雨 (インヴェルティア), Inverutia) impact event destroyed most of the planet's cities and resulted in humans gaining superpowers to become the Genestella (星脈世代（ジェネステラ）, Jenesutera). The city of Rikka (六花), also called Asterisk, has six academies where the Genestella participate in tournaments called Festas (星武祭 (フェスタ), Fesuta). Battle performances at Seidoukan Academy (星導館学園, Seidōkan Gakuen), the city's fifth top-ranking academy, are falling significantly and the academy's incumbent student council president, Claudia Enfield, is determined to find a solution to the problem. Kirin Toudou is the academy's top fighter, but Claudia, as well as Lieseltanian princess, Julis-Alexia von Riessfeld, are close behind.
Meanwhile, Ayato Amagiri investigates the disappearance of his sister, Haruka, a former student at Seidoukan. During his first day at the academy, Ayato is forced to fight Julis after discovering her half-dressed when returning her handkerchief to her. Claudia voids their duel and enrolls Ayato in Seidoukan. After rescuing Julis from a plot to cripple the Phoenix Festa, he sets out to become her protector and close friend. Eventually, Ayato wins against Kirin and is named the new top student at the academy. After Ayato participates in the Phoenix Festa with Julis to solve Seidoukan's performance crisis, they continue to participate in various Festas and also fight threats outside of the academy.


== Characters ==


=== Seidoukan Academy ===
Ayato Amagiri (天霧 綾斗, Amagiri Ayato)
Voiced by: Atsushi Tamaru, Ayaka Suwa (child) (Japanese); Erik Scott Kimerer (English)
Ayato is the top-ranked fighter at Seidoukan Academy. Transferring to Seidoukan at the recommendation of Claudia Enfield, Ayato searches for his sister Haruka's whereabouts. Afterward, he comes across one of the Four-Colored Runeswords, «Ser-Veresta»: Demon Blade of the Black Furnace (黒炉の魔剣 (セルベレスタ), Seru Beresuta), which was once wielded by Haruka. After rescuing Julis when she was attacked by Silas, Ayato decides that his new goal is to protect Julis. Eventually, after dueling with Kirin Toudou, he becomes the academy’s top student and earns himself the alias of Gathering Clouds (叢雲, Murakumo). Partnering up with Julis, they take on the Phoenix Festa together.
Julis-Alexia von Riessfeld (ユリス＝アレクシア・フォン・リースフェルト, Yurisu-Arekushia Fon Rīsuferuto)
Voiced by: Ai Kakuma (Japanese); Kira Buckland (English)
Julis is the pink-haired fifth top-ranking fighter at Seidōkan Academy, known by the alias of «Glühen Rose»: Witch of Resplendent Flames (華焔の魔女 (グリューンローズ), Gryūen Rōzu) and is the First Princess of Lieseltania, a small country located in the western Czech Republic. Her full name is Julis-Alexia Marie Florentia Renate von Riessfeld, according to Eishirō. She wields a rapier-type lux called «Aspera Spina» which she uses during the Phoenix tournament and bears the image of a flower for her power - control over flames. Her wish is to achieve what is known as a Grand Slam, a feat that involves rising to take the trophy in each of the three Festa, the Phoenix, the Gryps, and the Lindvolus. Teaming up with Ayato allowed her to achieve victory in the Phoenix and she joins Claudia's Team Enfield to participate in the Gryps. She resents Allekant Académie, specifically, the woman known as Magnum Opus, on the case of Orphelia Landlufen.
Claudia Enfield (クローディア・エンフィールド, Kurōdia Enfīrudo)
Voiced by: Nao Tōyama (Japanese); Erika Harlacher (English)
Claudia is the blonde second top-ranking fighter of Seidoukan Academy and its student council president, known by the alias of «Parta Morta»: Commander of a Thousand Visions (千見の盟主 (パルタモルタ), Paruta Moruta). At the beginning of the series, Claudia recommends Ayato's transfer into the academy. She has held the position of Student Council President since junior high and wants to win the Gryps to make her wish come true because her powers are more geared towards team battles and as such, are not practical in the Phoenix and the Lindvolus. She uses the «Pan-Dora», an extremely powerful Orga Lux with the ability of precognition at the cost of reliving a different way of dying every night in her dreams.
Saya Sasamiya (沙々宮 紗夜, Sasamiya Saya)
Voiced by: Shiori Izawa (Japanese); Sarah Anne Williams (English)
The blue-haired daughter of Soichi and Kaya Sasamiya, Saya is Ayato's childhood friend and as such, was taught by Ayato the basics of the Amagiri Shinmei Style. She moved abroad six years ago due to her father's career as a Lux manufacturer and researcher. She seems to be an air-headed stoic who has trouble waking up in the morning and is directionally-challenged. Her main weapons are Luxes that her father made for her, including a Type 38 Lux Grenade Launcher: Helneklaum, a Type 39 Laser Cannon: Wolfdora, and a Type 41 Lux Twin Blaster: Waldenholt.
Kirin Toudou (刀藤 綺凛, Tōdō Kirin)
Voiced by: Ari Ozawa (Japanese); Brianna Knickerbocker (English)
Kirin is a silver-haired student in the junior high branch of Seidoukan Academy. Before meeting Ayato, she is the academy's top fighter at the beginning of the series, having defended her rank with only a normal nihontō, a testament to the absolute power of the Toudou Style (刀藤流, Tōdōryū) that she is the heir of. She is known as the Keen-Edged Tempest (疾風刃雷, Shippū Jinrai) and along with Saya Sasamiya, rose to finish as a semifinalist in the Phoenix Festa.
Eishirou Yabuki (夜吹 英士郎, Yabuki Eishirō)
Voiced by: Yūma Uchida (Japanese); Max Mittelman (English)
Eishirou is Ayato's roommate. Well known as a member of the academy's newspaper club, he's also a member of the Shadow Star, a secret police force that works for the Integrated Enterprise Foundation (which is essentially the world government).
Lester MacPhail (レスター・マクフェイル, Resutā Makufeiru)
Voiced by: Takanori Hoshino (Japanese); Patrick Seitz (English)
Lester is the ninth highest-ranking fighter at Seidoukan Academy, his alias being «Kornephoros»: Axe of the Roaring Distance (轟遠の烈斧 (コルネフォロス), Koruneforosu). Before the beginning of the series, he is ranked 5th but after losing to Julis, his rank drops. His Lux is an ax named «Bardiche-Leo».
Silas Norman (サイラス・ノーマン, Sairasu Nōman)
Voiced by: Yūichi Iguchi (Japanese); Kyle McCarley (English)
First introduced as a sidekick of Lester's, Silas is actually the mastermind behind the attacks Phoenix participants. After being defeated by Ayato, he was given a deal by Galaxy, the Integrated Enterprise Foundation, backing Seidoukan Academy, and is put to use as an operative of Shadowstar, as shown in Volume 9.
Randy Hooke (ランディ・フック, Randi Fukku)
Voiced by: Nobuyuki Kobushi (Japanese); Vic Mignogna (English)
Randy is another sidekick of Lester's and was his partner in the Phoenix before their team was defeated by Irene Urzaiz.
Kyouko Yatsuzaki (八津崎 匡子, Yatsuzaki Kyōko)
Voiced by: Yūko Kaida (Japanese); Johanna Luis (English)
Kyouko is the homeroom teacher of Ayato's class. Before becoming a teacher, she was the former second top-ranking fighter of Le Wolfe who led a team to victory in the Gryps Festa, being the only team from Le Wolfe to be victorious. As a student, her alias was «Machiaverus»: Witch of Nails (釘絶の魔女 (マキアヴェルス), Makiaverusu) but it is unknown if she kept the alias after graduating (although this seems to be the case as Claudia addresses her by her alias).


=== St. Gallardworth Academy ===
Ernest Fairclough (アーネスト・フェアクロフ, Ānesuto Feakurofu)
Voiced by: Takahiro Sakurai (Japanese); Vic Mignogna (English)
The student council president of St. Gallardworth Academy, Ernest Fairclough is the top-ranked student as well as the bearer of the alias, «Pendragon»: The Holy Knight (聖騎士 (ペンドラゴン), Pendoragon), a title granted to him because of his wielding of the Orga Lux, also, one of the Four-Colored Runeswords, «Lei-Glems»: Demon Blade of White Purification (白濾の魔剣 (レイグレム), Reiguremu). He is the leader of Team Lancelot.
Laetitia Blanchard (レティシア・ブランシャール, Retishia Buranshāru)
Voiced by: Saori Onishi
The Vice-President of St. Gallardworth Academy, Laetitia Blanchard is ranked #2 in her school, known as «Saint» (聖女, Seijō) and «Gloriara»: Witch of Shining Wings (光翼の魔女 (グロリアラ), Guroriāra). She is a member of Team Lancelot and seems to harbor feelings of deep affection towards Ernest.
Elliot Forster (エリオット・フォースター, Eriotto Fōsutā)
Voiced by: Rie Takahashi (Japanese); Julie Anne Taylor (English)
Elliot Forster, like Kirin Toudou, is a junior high student and is an exceptionally skilled swordsman, rising to the rank of #12 despite the controlled society of St. Gallardworth. His alias is «Claíomh Solais»: Shining Sword (輝剣 (クライオソレ), Kurau Sorasu).


=== Jie Long Seventh Institute ===
Xinglou Fan (范 星露, Fan Shinrū)
Voiced by: Omi Minami (Japanese); Cherami Leigh (English)
Despite only being nine years of age, Fan Xinglou is the inheritor of the title «Ban'yuu Tenra» (万有天羅, Banyū Tenra) and is the student council president of Jie Long Seventh Institute, considrered to be the most powerful Genestella as stated by Commander Helga Lindwall.
Shenyun Li (黎沈雲, Rī Shen'yun)
Voiced by: Satsumi Matsuda (Japanese); Erica Mendez (English)
«Phantom Builder» (幻映創起, Gen'ei Shinki) Shenyun Li is the ninth-ranked fighter in Jie Long and the brother of Shenhua. He is a very capable fighter, having forced Ayato to break the second layer of the seal placed on him by Haruka. He is a member of Team Huanglong (Team Yellow Dragon).
Shenhua Li (黎沈華, Rī Shenfa)
Voiced by: Risae Matsuda (Japanese); Lauren Landa (English)
«Phantom Vanisher» (幻映霧散, Gen'ei Musan) Shenhua Li is the tenth-ranked fighter in Jie Long and the sister of Shenyun. Shenhua's illusionary techniques are among the most powerful in all of Asterisk. She is a member of Team Huanglong (Team Yellow Dragon).
Hufeng Zhao (趙虎峰, Jao Fūfon)
Voiced by: Juri Kimura (Japanese); Kimberly Woods (English)
«Peerless Thorn» (天苛武葬, Tenka Musou) Hufeng Zhao is the fifth-ranked fighter in Jie Long and one of the main disciples of Xinglou. He is the leader of the Wood Faction and is a member of Team Huanglong (Team Yellow Dragon).
Song Ran (宋然, Son Ran)
Voiced by: Eiichiro Tokumoto (Japanese); Chris Cason (English)
Luo Kunzhan (羅坤展, Ruo Kunzan)
Voiced by: Tsuguo Mogami


=== Allekant Académie ===
Ernesta Kühne (エルネスタ・キューネ, Erunesuta Kyūne)
Voiced by: Chinatsu Akasaki (Japanese); Cassandra Lee Morris (English)
A student from Allekant Academy and leader of Pygmalion (Sculptor Faction). Her ultimate goal is to have her puppets gain equal rights to those of humans.
Camilla Pareto (カミラ・パレート, Kamira Parēto)
Voiced by: Mutsumi Tamura (Japanese); Cristina Vee (English)
A student from Allekant Academy and the leader of Ferrovious (Lions Faction). Her right arm and leg were destroyed in a war, leading Ernesta to give her prosthetics. Her ultimate goal is to create a Lux universally compatible with anyone. She develops a rivalry with Saya after claiming her father's Luxes were objectively inferior to other weapons.
Shuma Sakon (左近洲馬, Sakon Shūma)
Voiced by: Nobuo Tobita (Japanese); Lucien Dodge (English)
He is the student council president of Allekant. Not much else is known about him.
AR-D (アルディ, Arudi)
Voiced by: Wataru Hatano (Japanese); Keith Silverstein (English)
The puppet robot piloted by Ernesta, who displays incredible defenses.
RM-C (リムシィ, Rimushi)
Voiced by: Haruka Yoshimura (Japanese); Laura Post (English)
The puppet robot piloted by Camilla, who is superior to AR-D in every way.
Hilda Jane Rowlands (ヒルダ・ジェーン・ローランズ, Hiruda Jeen Rooranzu)
Voiced by: Yū Kobayashi (Japanese); Karen Strassman (English)Known as Magnus Opus, she is the mad scientist behind Ophelia The strongest Strega in Asterisk


=== Le Wolfe Black Institute ===
Dirk Eberwein (ディルク・エーベルヴァイン, Diruku Ēberuvain)
Voiced by: Tomokazu Sugita (Japanese); Kaiji Tang (English)
The student council president of Le Wolfe Black Institute, Dirk Eberwein is the first non-Genestella to rise to the position of student council president with wits alone. He is so infamous, he has earned the alias of «Tyrant»: The Crafty King (悪辣の王 (タイラント), Akuratsu no Ō (Tairanto)).
Korona Kashimaru (樫丸ころな, Kashimaru Korona)
Voiced by: Moe Toyota (Japanese); Xanthe Huynh (English)
The «Tyrant»'s secretary, Korona is a Strega with the ability to predict the future using tarots.
Irene Urzaiz (イレーネ・ウルサイス, Irēne Urusaisu)
Voiced by: Yumi Uchiyama (Japanese); Erica Lindbeck (English)
Le Wolfe's third-ranking student, Irene Urzaiz is a problem child who owes a huge debt to Dirk Eberwein. She is known as «Lamilexia»: Vampire Princess (吸血暴姫 (ラミレクシア), Kyūketsuki Abahime (Ramirekushia)) due to her wielding of the Orga Lux, «Gravisheath»: Blood Scythe of Supreme Collapse (覇潰の血鎌 (グラヴィシース), Hatsu no Chikama (Gurabishīsu)).
Priscilla Urzaiz (プリシラ・ウルサイス, Purishira Urusaisu)
Voiced by: Juri Nagatsuma (Japanese); Christine Marie Cabanos (English)
Irene's younger sister, Priscilla Urzaiz is a Strega with the much-coveted ability of self-regeneration and Irene uses her, much to her reluctance, as a power source for the Gravisheath, whose cost to wield it is to offer it blood.
Ophelia Landlufen (オーフェリア・ランドルーフェン, Ōferia Randorūfen)
Voiced by: Maaya Sakamoto (Japanese); Ryan Bartley (English)
A childhood friend of Julis, Orphelia Landlufen was collateral in the paying off of the orphanage's debt. She was sold to Frauenlob, the Integrated Enterprise Foundation backing Allekant Académie where she was used in an experiment by Hilda Jane Rowlands in order to artificially create Genestella. However, an anomaly in the experiment resulted in Hilda creating one of the world's strongest Strega. Through a series of covert dealings, Dirk Eberwein got his hands on Orphelia and she became a student at Le Wolfe. Her sheer power alone has allowed her to become the two-time champion of the solo tournament, the Lindvolus, defeating even the Sigrdrífa, Sylvia Lyyneheym. She is the top-ranked student in the official charts of Le Wolfe as well as the unofficial ranking websites. Her alias is «Erenshkigal»: Witch of Solitary Venom (孤毒の魔女 (エレシキガル), Kojidoku no Majō (Erenshukīgaru)).
Moritz Nessler (モーリッツ・ネスラー, Mōrittsu Nesurā)
Voiced by: Masayuki Kato
Gerd Schiele (ゲルト・シーレ, Geruto Shīre)
Voiced by: Kenichiro Matsuda


=== Queenvale Academy for Young Ladies ===
Sylvia Lyyneheym (シルヴィア・リューネハイム, Shiruvia Ryūnehaimu)
Voiced by: Haruka Chisuga (Japanese); Mela Lee (English)
Sylvia Lyyenehym is the student council president of Queenvale Academy for Young Ladies and is the top-ranked student. Her alias is «Sigrdrífa»: Witch of Fearsome Melody (戦律の魔女 (シグルドリーヴァ), Ikusaritsu no Majō (Shigurudorīva)). She is the world's top idol and is one of the strongest Strega in the history of Asterisk, the runner-up of the previous Lindvolus, losing only because Orphelia Landlufen was in the competition. Due to her popularity, she usually goes out in disguise. She is also in love with Ayato, the first of the five main heroines to realize that fact.
Yuzuhi Renjouji (蓮城寺 柚陽, Renjōji Yuzuhi)
Another childhood friend of Ayato's, Yuzuhi reunited with Ayato in the seventh light novel volume. She is also an adept practitioner in the Amagiri Shinmei Style, although her exceptional skill lies in the bow, to the point that Ayato admits that he cannot hope to match Yuzuhi using the bow.


== Media ==


=== Light novels ===
The series is written by Yū Miyazaki and illustrated by Okiura. It is published by Media Factory. The first volume was released in September 2012, and as of April 2018, fourteen volumes have been released. Yen Press licensed the novels for publication in North America. The first English volume was released on August 30, 2016. Volumes 1 through 5 were translated into English by Melissa Tanaka and the following volumes are translated by Haydn Trowell.


==== Wings of Queenvale ====
Yuu Miyazaki has also worked on a spinoff, focusing on Minato Wakamiya, the main protagonist, Chloe Flockhart, Minato's best friend, Sophia Fairclough, the younger sister of Ernest Fairclough, Nina Achenwall, a Strega whose powers are invoked through the mental image of a deck of cards, and Yuzuhi Renjouji, a childhood friend of Ayato Amagiri who trained in the Yatsuka Dojo of the Amagiri Shinmei Style, all students of Queenvale Academy for Young Ladies.


=== Manga ===
A manga adaptation by Ningen was published from 2013 to 2016 and has been compiled into five volumes. It was released in English by Yen Press. The English translation is by Melissa Tanaka. A manga adaptation of the spinoff Wings of Queenvale novels by Shou Akane premiered in Kodansha's Bessatsu Shōnen Magazine in August 2014, and ended on September 2016. The spinoff manga adaptation was collected in four volumes.


=== Anime ===
Directed by Manabu Ono and Kenji Seto, an anime adaptation of the series was broadcast from October 3 to December 19, 2015, with a second season following from April 2 through June 18, 2016.
Four pieces of theme music are used for the two seasons: two opening themes and two ending themes. The opening themes, "Brand-new world" and "The Asterisk War", are both performed by Shiena Nishizawa. The first ending theme, titled "Waiting for the rain", is performed by Maaya Sakamoto, while the second ending theme is "Ai no Uta (Words of Love)" (愛の詩) performed by Haruka Chisuga.Aniplex of America licensed the series for an English-language release in North and South America and simulcasted the series on Crunchyroll's streaming services. Aniplex released the series on DVD and Blu-ray in four compilations, each containing 6 episodes, from September 20, 2016 to April 25, 2017.


==== Season 1 ====


==== Season 2 ====


=== Video game ===
A PlayStation Vita simulation game by Bandai Namco Games titled The Asterisk War: Houka Kenran was released in Japan on January 28, 2016.


== Notes ==


== References ==


== External links ==
Official light novel website at MF Bunko J (in Japanese)
Official anime website
The Asterisk War (light novel) at Anime News Network's encyclopedia