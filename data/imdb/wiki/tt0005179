David Harum; A Story of American Life is a best-selling novel of 1898 whose principal legacy is the colloquial use of the term horse trading.


== Literary significance and criticism ==
Written by retired Syracuse, New York banker Edward Noyes Westcott, the work was rejected by six publishers before being accepted for publication by D. Appleton & Company. Published in the fall of 1898, some six months after the author's death, it sold an impressive 400,000 copies during the following year. Although the book contains the mandatory love story, the character and philosophy of the title character, small town banker and horse trader David Harum, expressed in the dialect of  19th-century rural central New York is the focus of the book.The main appeal of the work seems to have been to businessmen, attracted by its approval of a much more relaxed code of business ethics than was presented in most novels of the time. Harum was an inveterate horse-trader and considered engaging in the dubious practices long associated with this activity as morally justified by the expectation that similar practices would be employed by his adversary. In principle, he contended that this made horse-trading quite different from other lines of business, yet in practice most business dealings seemed to him to be a species of horse trading, justifying considerable deviation from conventional standards of probity. 
The fact that these sentiments were placed in the mouth of an elderly country banker—on the face of it, a clear spokesman for "traditional values"—was particularly appealing in that it made these business ethics appear a reflection of the practices of shrewd businessmen through the ages rather than an indicator of moral degeneration. Harum's version of the Golden Rule -- Do unto the other feller the way he'd like to do unto you, an' do it fust.—was widely quoted,  and the term horse trading came into use as an approbatory term for what others would deem ethically dubious business practices.The success of the book led to the identification of some of its characters with living persons; the late author's sister felt compelled on that account to write to Publishers Weekly, declaring that while the character of David Harum himself might be called a "composite", all the others were entirely fictitious. The concession concerning the main character was a necessary one: the resemblance of the fictitious David Harum, banker and horse trader from the (also fictitious) central New York village of Homeville, and the real David Hannum, banker and horse trader from the (real) central New York village of Homer was too great to deny. Hannum is perhaps better remembered for his role in the  Cardiff Giant hoax.


== Adaptations ==

The undramatic character of the book's action was something of an impediment to its adaptation to the stage, but its popularity insured that an attempt would be made. The result was a quite serviceable star vehicle for veteran comic actor William H. Crane—so much so that Crane became largely identified with the role. In 1915, Famous Players made a film adaptation of the play, again featuring Crane.In 1934, the story was again adapted to the screen, this time as a vehicle for Will Rogers. This film was successful at the box office.In 1936, David Harum became a radio serial, which was broadcast until 1951. These later adaptations owed little more than incidentals to the book; rather they used the story as a vehicle for presenting a more generic version of the cracker-barrel philosopher. In one respect the radio show however was true to Harum's business philosophy—the program had a reputation for being particularly aggressive in using the story line to push its sponsors' premium offers.


== Ice cream ==
A David Harum is also a kind of ice cream sundae named for the character in the book. It consists of vanilla ice cream, crushed strawberry, crushed pineapple, whipped cream, and a cherry.


== References ==


== External links ==
The Gutenberg Project text link.
Page Images at Google Book Search link
David Harum on IMDb (1934 film adaptation)
"In Pathetic Remembrance", a poem by Florence Earle Coates