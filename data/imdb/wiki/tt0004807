Who Goes There? is a science fiction horror novella by American writer John W. Campbell Jr., written under the pen name Don A. Stuart. It was first published in the August 1938 Astounding Science Fiction.
The novella has been adapted four times as a film: the first in 1951 as The Thing from Another World; the second in 1972 as Horror Express, the third in 1982 as The Thing, directed by John Carpenter; and most recently as a prequel to the Carpenter version, also titled The Thing, released in 2011. A fifth film, again with Carpenter involved, is in development from Blumhouse Productions.
In 2010, Peter Watts wrote the award-winning short story The Things, retelling the novella's and 1982 film's events from the alien's point of view with a creative commons licence.As of 2019, an official sequel is being written by John Gregory Betancourt, after the release of the first draft of the novella, an expanded novel written by Campbell Jr. titled Frozen Hell.


== Plot ==
A group of scientific researchers, isolated in Antarctica by the nearly-ended winter, discover an alien spaceship buried in the ice, where it crashed twenty million years before. They try to thaw the inside of the spacecraft with a thermite charge, but end up accidentally destroying it when the ship's magnesium hull is ignited by the charge. However, they do recover the alien pilot from the ancient ice, which the researchers believe was searching for heat when it was frozen. Thawing revives the alien, a being which can assume the shape, memories, and personality of any living thing it devours, while maintaining its original body mass for further reproduction. Unknown to them, the alien immediately kills and then imitates the crew's physicist, a man named Connant; with some 90 pounds of its matter left over it tries to become a sled dog. The crew discovers the dog-Thing and kills it in the process of transformation. Pathologist Blair, who had lobbied for thawing the Thing, goes insane with paranoia and guilt, vowing to kill everyone at the base in order to save mankind; he is isolated within a locked cabin at their outpost. Connant is also isolated as a precaution and a "rule-of-four" is initiated in which all personnel must remain under the close scrutiny of three others.
The crew realizes they must isolate their base and therefore disable their airplanes and vehicles, yet they pretend things are normal during radio transmissions to prevent any rescue attempts. The researchers try to figure out who may have been replaced by the alien (simply referred to as the Thing), in order to destroy the imitations before they can escape and take over the world. The task is almost impossibly difficult when they realize that the Thing is also telepathic, able to read minds and project thoughts. A sled dog is conditioned by human blood injections (from Copper and Garry) to provide a human-immunity serum test, as in rabbits. The initial test of Connant is inconclusive as they realize that the test animal received both human and alien blood, meaning that either Doctor Copper or expedition Commander Garry is actually an alien. Assistant commander McReady takes over and deduces that all the other animals at the station, save the test dog, have already become imitations; all are killed by electrocution and their corpses burned.
Everyone suspects each other by now but must stay together for safety, deciding who will take turns sleeping and standing watch. Tensions mount and some men begin to go mad thinking they are already the last human or wondering if they would even know if they weren't human any longer. Ultimately, Kinner, the cook, is murdered and accidentally revealed to be a Thing. McReady realizes that even small pieces of the creature will behave as independent organisms. He then uses this fact to test which men have been "converted" by taking blood samples from everyone and dipping a heated wire in the vial of blood. Each man's blood is tested, one at a time, and the donor is immediately killed if his blood recoils from the wire; fourteen men in all, including Connant and Garry, are revealed to be aliens. The remaining men go to test the isolated Blair and on the way see the first albatross of the Antarctic spring flying overhead; they shoot the bird to prevent a Thing from infecting it and flying to civilization.
When they reach Blair's cabin they discover he is a Thing. They realize that it has been left to its own devices for a week, coming and going as it pleased as it is able to squeeze under doors by transforming itself. With the creatures inside the base destroyed, McReady and two others enter the cabin to kill the Thing that was once Blair. McReady forces it out into the snow and destroys it with a blowtorch. Afterwards the trio discover that the Thing was dangerously close to finishing the construction of an atomic-powered anti-gravity device that would have allowed it to escape to the outside world.


== Characters ==


=== Secondary magnetic expedition ===
Although the expedition based at Big Magnet comprises 37 members, only 16 are named in the story, and all but three by last name alone. By the end of the story, 15 of them have been replaced by alien impostors.

Barclay: present at alien excavation.
Benning: aviation mechanic. He survives. He appears in the 1982 adaptation renamed George Bennings with the occupation of meteorologist, and is portrayed by Peter Maloney.
Blair: biologist, present at alien excavation. Blair goes insane after the Thing escapes, due to his desire to thaw the Thing. Blair is locked in the tool shed, where he is replaced by a Thing. Blair appears in the 1982 adaptation portrayed by A. Wilford Brimley.
(Bart) Caldwell: a member of the team. Caldwell appears in the 1982 adaptation, renamed "Palmer" and portrayed by David Clennon.
Clark: dog handler. Clark is later revealed to be a Thing. He appears in the 1982 adaptation, played by Richard Masur
Connant: physicist, cosmic ray specialist. He is the first member of the team to be assimilated. In Frozen Hell, he is given the first name Jerry.
Dr. Copper: physician, present at the alien excavation. Copper appears in the 1982 adaptation portrayed by Richard Dysart.
(Samuel) Dutton: revealed to be a Thing
Garry: expedition commander. Garry is eventually revealed as a Thing and killed. Garry appears in the 1982 adaptation portrayed by Donald Moffat.
Harvey
Kinner: scar-faced cook. Kinner is later revealed to be a Thing. He appears in the 1982 adaptation, renamed "Nauls” and portrayed by T. K. Carter.
McReady: expedition second-in-command, meteorologist, present at alien excavation. McReady appears in the 1982 adaptation portrayed by Kurt Russell, now with the name "R.J. MacReady" and the occupation of the helicopter pilot. MacReady reappears in the video game played by Russell and the comics based on the film.
(Vance) Norris: muscular physicist. Norris appears in the 1982 adaptation portrayed by Charles Hallahan, though he is given Kinner's fate. Norris has much of Kinner's characterization.
Pomroy: livestock handler.
Ralsen: sledge keeper.
Van Wall: chief pilot, present at alien excavation.


=== Non-human characters ===
"The Thing", the antagonist, is a malevolent, shapeshifting alien creature. It appears in all three film adaptions. In the first film (1951), it is depicted as a tall, menacing humanoid alien that is composed of vegetable matter. In the two later film adaptions, the Things retains their ability to shapeshift, although they do not have telepathic abilities.
Charnauk: lead Alaskan husky, first openly attacked by the Thing
Chinook and Jack: two other huskies


== Versions ==
Two slightly different versions of the original novella exist.  It appeared in Astounding in a 12-chapter version, which also appears in Adventures in Time and Space and The Antarktos Cycle (under the title The Thing from Another World).  An extended 14-chapter version appears in The Best of John W. Campbell and the Who Goes There? collection.  
In 2018, it was found that Who Goes There? was actually a shortened version of a larger novel previously written by Campbell. The expanded manuscript (including an entirely different opening), titled Frozen Hell, was found in a box of manuscripts sent by Campbell to Harvard University. The discovery was made by author and biographer Alec Nevala-Lee, during his research on a biography of Campbell and other authors from the Golden Age of Science Fiction.A Kickstarter campaign was launched to publish the full novel. When completed on December 1, the campaign had raised more than $155,000, compared to its original $1,000 goal. E-book versions of the novel, published by Wildside Press, began distributing digitally to campaign backers on January 16, 2019, with physical copies following in June the same year.In January 2020, a new film, to be produced by Jason Blum's Blumhouse Productions and released and distributed by Universal Pictures (as a part of the former's first-look deal) was going to be made based on the new, expanded version of the original book.


== Critical reception ==
In 1973, the story was voted by the Science Fiction Writers of America as one of the stories representing the "most influential, important, and memorable science fiction that has ever been written". It was published with the other top voted stories in The Science Fiction Hall of Fame, Volume Two.


== Adaptations ==


=== Film ===
The Thing from Another World (1951) is a loose adaptation of the original story. It features James Arness as the Thing, Kenneth Tobey as the Air Force officer, and Robert O. Cornthwaite as the lead scientist. In this adaptation, the alien is a humanoid invader (i.e., two arms, two legs, a head) from an unknown planet. A plant-based life form, the alien and its race need animal blood to survive. He, or rather it, is a one-alien army, capable of creating an entire army of invaders from seed pods contained in (his) body.
The John Carpenter 1982 adaptation The Thing, sticks more closely to Campbell's original story. Carpenter remade the film due to The Thing from Another World being one of his favorite films, and the 1951 adaptation is an Easter egg in Carpenter's original  Halloween. Carpenter directed the film from a Bill Lancaster screenplay.  Carpenter's idea was not to compete with the direction of the earlier film. In both the novella and this adaptation, the Thing can imitate any animal-based life form, absorbing the respective hosts' personalities and memories along with their bodies (although the telepathy aspect is omitted). When the story begins, the creature has already been discovered and released from the ice by another expedition. This version maintains the digestions and transformations alluded to in the original novella, via practical effects such as animatronics and stop motion animation.William F. Nolan, author of Logan's Run, also wrote a Who Goes There? screen treatment for Universal Studios in 1978; however, it was not published until 2009 in the Rocket Ride Books edition of Who Goes There?. Nolan's alternate take on Campbell's story reduced down the number of characters, replaced the single alien with a group of three, and downplayed monster elements in favor of an "impostor" theme, in a vein similar to The Body Snatchers by Jack Finney.In January 2020, Universal and Blumhouse Productions announced a new film based on the new, expanded version of the original book, Frozen Hell.


=== Comics ===
Héctor Germán Oesterheld and Alberto Breccia adapted the story as "Tres ojos" (Spanish: "Three Eyes") for their Sherlock Time series, published in the Argentine comic book Hora Cero (issue 89—104) from May to August 1959.In 1976, the story was also published in comic book form in issue 1 of Starstream (script by Arnold Drake and art by Jack Abel).In 1991 Dark Horse Comics published a two-issue miniseries The Thing From Another World. In July 1992, a 4 issue miniseries sequel was published, The Thing From Another World: Climate of Fear.


=== Radio drama ===
The story has been adapted as a radio drama multiple times, including by BBC Radio 4 in their Chillers series (24 January 2002), and the Suspense radio drama series (2013).The earliest adaptation was for the Exploring Tomorrow radio series in 1958 (under the title The Escape), hosted by John W. Campbell, Jr. himself; however, no recordings of this episode are known to exist.


=== Board and video games ===
The Thing (2010 card game) and The Thing: Infection at Outpost 31 (2017 board game) are both based on the John Carpenter movie.
Who Goes There?, a board game from Certifiable Studios, was released in May 2018, following a Kickstarter campaign, which raised over $612,000 compared to its $54,097 goal.  In March 2020, Certifiable Studios launched a Kickstarter for a second edition of the game with "updated mechanics" and "additional expansion characters."  It is available either as a complete game, or as an add-on for those with the first edition game already. Its initial goal was $34,097 and as of March 19, has raised over $300,000 in pledges.
In 2002, Universal Interactive and Konami co-published the video game The Thing, released for Windows, Xbox and PlayStation 2. A third-person shooter survival horror video game, set as a sequel to John Carpenter's 1982 film of the same name , director who even voices a character in an uncredited cameo.  . The game was a commercial success, selling over one million units worldwide across all platforms.


== Cultural impact ==


=== In literature ===
In December 1936, John W. Campbell published a story titled "Brain Stealers of Mars" in Thrilling Wonder Stories, which also features shape-shifting, mind-reading aliens. The earlier story has a humorous tone, but takes a philosophical note as members of another alien race describe living stoically alongside the shapeshifters.Writer A. E. van Vogt was inspired by Who Goes There? to write Vault of the Beast (1940), which he submitted to Astounding Science Fiction. "I read half of it standing there at the news-stand before I bought the issue and finished it," van Vogt later recalled. "That brought me back into the fold with a vengeance. I still regard that as the best story Campbell ever wrote, and the best horror tale in science fiction."The Thing is one of the aliens featured in Barlowe's Guide to Extraterrestrials (1979; second edition 1987). Barlowe's main illustration depicts the Thing halfway through its transformation into a sled dog.The story is referenced, and embedded within The Rack of Baal (1985), a 'choose-your-own-adventure' gamebook written by Mark Smith and Jamie Thomson, about a time-travelling special agent called "The Falcon". A section of the plot plays out on a frozen world occupied by a single mining station crewed by only a few people. One inhabitant is one called 'Sil McReady', who, in a cynical inversion of the original story, actually turns out to be infected with the alien organism.
In 2010, Canadian science fiction writer Peter Watts published and podcast a short story called "The Things" in which the alien entity from "Who Goes There?" is the first-person narrator. The characters and events in "The Things" are the same as those in the 1982 John Carpenter film. Confused, damaged, and unfamiliar with earth biology, the alien—a well-meaning ambassador—only slowly begins to understand why it has been received with such hostility, and that the humans are individuals who do not want to be assimilated. Upon figuring this out, it is utterly revulsed by the "thinking cancers." "The Things" was nominated for a number of awards, including the Hugo; it won the 2010 Shirley Jackson Award for best short story. In 2011, "The Things" was recorded and released by Escape Pod as an audio podcast.


=== In film and television ===
David Denby of New York magazine suggested that Invasion of the Body Snatchers (1956) and Alien (1979) may have been influenced by Campbell's story.The 1993 episode "Ice" of the science fiction TV series The X-Files borrows its premise from the storyline.
In the 1995 Star Trek: Deep Space Nine episode "The Adversary," a shapeshifting alien infiltrates the crew of a starship. The episode explores similar themes of paranoia and contains a "blood test" scene. The writers have cited The Thing from Another World as inspiration.
The 2013 episode "The Thingy!" of the family action-comedy series The Aquabats! Super Show! also borrows the story's premise, albeit in a much more comedic tone.
The fourth episode of the first season of the HBO crime drama True Detective, first broadcast in 2014, is named after the novella.


=== In other media ===
In 2006, Dark Horse Comics released a pre-painted snap together model kit of the alien as described in the original short story. It was sculpted and painted by Andrea Von Sholly. The model was unlicensed and was simply titled 'The Space Thing'.
The 2018 video game Among Us has been considered a spiritual adaptation of the story and its film adaptations. Inspired by the party game Mafia, players take the role of a group of isolated astronauts, one or more of whom is an impostor plotting the demise of the remainder of the crew. Though the game diverges from many of the specifics of the story, most notably in that its impostors merely kill rather than replace the crew, its gameplay creates a markedly similar atmosphere of paranoia and fear and encourages players to replicate elements of the story, such as the "rule of four" and litmus tests to prove one's identity.


== References ==


== Further reading ==
Campbell, John W; Nolan, William F (2009). Who Goes There? The Novella That Formed The Basis Of "The Thing". Rocket Ride Books. ISBN 978-0-9823322-0-7.
Hamilton, John (2007). The Golden Age and Beyond: The World of Science Fiction. ABDO Publishing Company. pp. 8–11. ISBN 978-1-59679-989-9.
Leane, Elizabeth. "Locating the Thing: The Antarctic as Space Alien in John W. Campbell's 'Who Goes There'" Science Fiction Studies. Volume 32, Issue 2, July 2005: 225–239 Literature Criticism Online. Web. 3 November 2011.
Nevala-Lee, Alec (2018-10-23). Astounding: John W. Campbell, Isaac Asimov, Robert A. Heinlein, L. Ron Hubbard, and the Golden Age of Science Fiction. HarperCollins. ISBN 9780062571960.
Witalec, Janet. "John Carpenter (1948–)." Contemporary Literary Criticism. Vol 161, 2003: 143–204 Literature Criticism Online. Web. 2 Nov. 2011.


== External links ==
Who Goes There? title listing at the Internet Speculative Fiction Database