Girl before a Mirror is a painting by Pablo Picasso that was created in March 1932. Considered to be one of his masterpieces, the painting has elicited varied interpretations of this portrait of Picasso's lover and her reflection.


== Background ==
Marie-Thérèse Walter, one of Picasso's muses and paramours, is the subject of the painting. During the 1930s, she became his favorite subject and in this painting he used colors and symbols to which suggest different ways to view her. The mirror and the reflected subject suggest one possibility for her own view of herself.


== Description ==
The painting depicts a woman looking into a mirror, with a familiar yet contrasting mirror-image looking back at her. While the woman is painted with brighter colors and exhibits a more beautiful face, the figure in the mirror is darker and more grotesque. On top of this duality of imagery, the faces of both the woman and the reflection are bifurcated into different colorations, which suggests a duality of nature within both the girl and her reflected image. 
The painting features a very colorful palette, with bright pinks, yellows, and greens contrasting with vibrant reds and dark blacks. The skin and face of the girl is delicately beautiful in contrast with the rest of the painting, and is unlike many of Picasso's other Cubist faces of the time period. In contrast, the reflected face is much more grotesque and darkly colored, and the features are much less traditionally beautiful.


== Interpretations ==
Pablo Picasso presents a divergent view of his subject, with a brighter figure standing before a mirror which reflects a darker mirror image. While the face of the woman herself is split into both yellow and more naturalistic colors, they contrast in their application of make-up or natural skin, suggesting a two-fold nature of beauty.The reflection in the mirror is distorted and discolored, possibly representing the woman's dislike for herself. The colors used here are dark and make her look very old. Instead of the happiness reflected in the real girl, the reflected girl seems distraught. The woman, happily gravid, is concerned that she might not be attractive any more.


== References ==