Georges Méliès (1861–1938), a French filmmaker and magician, made a variety of short actuality films between 1896 and 1900. Méliès was established as a magician with his own theater-of-illusions, the Théâtre Robert-Houdin in Paris, when he attended the celebrated first public demonstration of the Lumière Brothers' Kinetoscope in December 1895. Unable to purchase a camera from the Lumières, who insisted that the venture had no future, he bought a film projector and some films from the British film experimenter Robert W. Paul and began projecting them at the Théâtre Robert-Houdin. Meanwhile, Méliès studied the principles on which Paul's projector ran, and in 1896 was able to modify the machine so that it could be used as a makeshift camera. At first, Méliès followed the custom of the time, and the example memorably set by the pioneering Lumières, by producing actuality films—brief "slice of life" incidents made by preparing naturalistic scenes for the camera or by filming events of the day. These "cityscapes, scenic views, and domestic vignettes" closely followed the model already set by the Lumières and their salaried operators, who had already been sent to various points abroad to publicize the Lumière camera and bring home actualities filmed in foreign climes. All told, Méliès filmed 93 films, or 18% of his entire output, outdoors as actuality footage.However, Méliès was also interested in expanding his line of films to include less common genres. His second film, Conjuring, captured a theatrical magic act on film; his sixth, Watering the Flowers, moved into comedy, remaking the Lumière's influential L'Arroseur Arrosé. Following his discovery of the substitution splice in 1896, Méliès moved further into fiction and trick films, building his own studio on his property in Montreuil, Seine-Saint-Denis to allow for the filming of his theatrically inspired, storytelling-based scènes composées—"artificially arranged scenes." His last nonfiction work was the seventeen-part Paris Exposition, 1900 film series. Because of his move away from actualities into fiction, he is generally regarded as the first person to recognize the potential of narrative film. In an advertisement, Méliès proudly described the difference between his innovative theatrical films and the actualities still being made by his contemporaries: "these fantastic and artistic films reproduce stage scenes and create a new genre entirely different from the ordinary cinematographic views of real people and real streets."


== Films ==

The following guide to Méliès's actuality films lists the numbers assigned in the catalogues of Méliès's studio, the Star Film Company; the original French and English titles; the presumed filming date; and whether the film survives or is presumed lost. Unless otherwise referenced, this data comes from Jacques Malthête's 2008 filmography of the films of Georges Méliès. Wherever possible, brief summaries of the films are given; unless otherwise cited, these are extrapolated from the available French and English titles.


== Notes ==


=== References ===


=== Citations ===


== External links ==