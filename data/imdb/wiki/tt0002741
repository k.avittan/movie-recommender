Caught in the Act is a live double album by Styx, released in 1984. It contains one new song, "Music Time", which was released as a single, reaching #40 on the Billboard Hot 100 charts.
Caught in the Act is also the name of a VHS video recording that featured the band acting out the concept established in their Kilroy Was Here album. A DVD version was released on December 11, 2007.
Shortly after this album's release, Tommy Shaw announced his departure from the band, to pursue a solo career. The band then went into hiatus for the rest of the 1980s. Caught In The Act would ultimately prove to be the final album by the massively successful 1975-84 Styx lineup of Dennis DeYoung, Shaw, James Young, Chuck Panozzo, and John Panozzo; by the time Shaw returned to the band in 1995, John Panozzo's declining health prevented his participation, and he died in 1996.


== Track listing ==


=== Side 1 ===
"Music Time" [*] (DeYoung) – 4:45
Lead vocals: Dennis DeYoung
Lead guitar: Tommy Shaw
"Mr. Roboto" (DeYoung) – 4:59
Lead vocals: Dennis DeYoung
"Too Much Time on My Hands" (Shaw) – 5:01
Lead vocals and lead guitar: Tommy Shaw
"Babe" (DeYoung) – 4:52
Lead vocals: Dennis DeYoung
Lead guitar: Tommy Shaw


=== Side 2 ===
"Snowblind" (Young, DeYoung) – 6:00
Co-lead vocals and lead guitar: James "JY" Young, 
Co-lead vocals: Tommy Shaw
"State Street Sadie" (uncredited) / "The Best of Times" (DeYoung) – 6:30
Lead vocals: Dennis DeYoung
Lead guitar: Tommy Shaw
"Suite Madame Blue" (DeYoung) – 8:51
Lead vocals and synthesizer solo: Dennis DeYoung
Lead guitar: James "JY" Young


=== Side 3 ===
"Rockin' the Paradise" (DeYoung, Young, Shaw) – 4:40
Lead vocals: Dennis DeYoung
First guitar solo: Tommy Shaw
Second guitar solo: James "JY" Young
"Blue Collar Man (Long Nights)" (Shaw) – 4:47
Lead vocals and lead guitar: Tommy Shaw
Guitar fills and end solo: James "JY" Young
"Miss America" (Young) – 6:15
Lead vocals and lead guitar: James Young
"Don't Let It End" (DeYoung) – 5:25 (on US LP, cassette and CD)
Lead vocals: Dennis DeYoung
Lead guitar: Tommy Shaw"Boat on the River" (Shaw) – 3:10 (on LP AMLM 66704, A&M Records 1984)
Accordion: Dennis DeYoung
Lead vocals, lead guitar, mandolin: Tommy Shaw


=== Side 4 ===
"Fooling Yourself (The Angry Young Man)" (Shaw) – 6:05
Lead vocals: Tommy Shaw
Synthesizer solos: Dennis DeYoung
"Crystal Ball" (Shaw) – 6:24
Lead vocals and lead guitar: Tommy Shaw
Synthesizer solo: Dennis DeYoung
"Come Sail Away" (DeYoung) – 8:56
Lead vocals, synthesizer solo: Dennis DeYoung
Lead guitar: Tommy Shaw* New studio recording


== Video track listing ==
Kilroy Was Here mini-movie
"Mr. Roboto" (DeYoung)
"Rockin' the Paradise (with J.Y. guitar solo)" (DeYoung, Shaw, Young)
"Blue Collar Man" (Shaw)
"Snowblind" (DeYoung, Young)
"Too Much Time on My Hands" (Shaw)
"Don't Let It End" (DeYoung)
"Heavy Metal Poisoning" (Young)
"Cold War (with extended Tommy Shaw guitar solo and extra verses)" (Shaw)
"State Street Sadie" (uncredited) / "The Best of Times" (DeYoung)
"Come Sail Away" (DeYoung)
"Renegade (with John Panozzo drum solo and band getting arrested)" (Shaw)
"Haven't We Been Here Before" (Shaw)
"Don't Let It End (Reprise)" (DeYoung)


== DVD bonus tracks ==
"Come Sail Away" (1977 video)
"Borrowed Time" (1979 video)
"Babe" (1979 video)
"Boat On the River" (1979 video)
"A.D. 1928"/"Rockin' the Paradise" (1981 video)
"The Best of Times" (1981 video)
"Too Much Time on My Hands" (1981 video)
"Mr. Roboto" (1983 video)
"Don't Let It End" (1983 video)
"Heavy Metal Poisoning" (1983 video)
"Haven't We Been Here Before" (video)
"Music Time" (1984 video)


== Personnel ==
Dennis DeYoung – vocals, keyboards, accordion
Tommy Shaw – vocals, guitars, mandolin
James "JY" Young – vocals, guitars, keyboards
Chuck Panozzo – bass
John Panozzo – drums


== Production (album) ==
Producer: Styx
Engineers: Gary Loizzo, Will Rascati, Rob Kingsland, Biff Dawes
Assistant engineer: Jim Popko
Mastering engineer: Ted Jensen at Sterling Sound, NYC


== Production (video) ==
Concert producer and director: Jerry Kramer
Kilroy Was Here mini-movie director: Brian Gibson
Cameramen: Wayne Isham


== Charts ==
Album - Billboard (United States)

Singles - Billboard (United States)


== References ==


== External links ==
Styx - Caught in the Act (1984) album review by Bret Adams, credits & releases at AllMusic.com
Styx - Caught in the Act (1984) album releases & credits at Discogs.com
Styx - Caught in the Act (1984) album credits & user reviews at ProgArchives.com
Styx - Caught in the Act (1984) album to be listened as stream at Spotify.com