Muhammad's wives, or the wives of Muhammad, were the women married to the Islamic prophet Muhammad. Muslims often use the term "Mothers of the Believers" prominently before or after referring to them as a sign of respect, a term derived from the Quran.Muhammad was monogamous for 25 years when married to his first wife, Khadija bint Khuwaylid. After her death in 619 CE, he over time married a number of women. His life is traditionally delineated by two epochs: pre-hijra (emigration) in Mecca, a city in western Arabia, from the year 570 to 622 CE, and post-hijra in Medina, from 622 until his death in 632. All but two of his marriages were contracted after the Hegira (or Hijra - migration to Medina).
Of his 13 wives, only two bore him children: Khadija and Maria al-Qibtiyya.


== Background ==


=== Objectives ===
In Arabian culture, marriage was contracted in accordance with the larger needs of the tribe and was based on the need to form alliances within the tribe and with other tribes. Virginity at the time of a first marriage was emphasized as a tribal honor.Watt states that all of Muhammad's marriages had the political aspect of strengthening friendly relationships and were based on the Arabian custom. Esposito points out that some of Muhammad's marriages were aimed at providing a livelihood for widows. He noted that remarriage was difficult for widows in a society that emphasized virgin marriages. F.E. Peters says that it is hard to make generalizations about Muhammad's marriages: many of them were political, some compassionate, and some perhaps affairs of the heart.The objectives of Muhammad's marriages have been described as:
Helping out the widows of his companions.
Creating family bonds between him and his companions (Muhammad married the daughters of Abu Bakr and Umar, whereas Uthman and Ali married his daughters. He therefore had family bonds with all the first four Caliphs).
Spreading the message by uniting different clans through marriage.
Increasing credibility and sources for conveying his private family life. If he only had one wife, then it would have been a tremendous responsibility on her to convey Muhammad's private acts of worship and family life, and people would try to discredit her to destroy the credibility of these practices. However, with multiple wives, there were a lot more sources to the knowledge, making it more difficult to discredit it. Therefore, his marriages gave more women the opportunity to learn and teach the matters of his private life.Muhammad's first marriage was at the age of about 25 to Khadijah. He was monogamously married to her for 25 years until her death, after which he is believed to have had multiple wives for the reasons explained above. With the exception of Aisha, Muhammad only married widows and divorcées or captives.


=== Terminology ===
"Mother of the Believers" is a term by which each of Muhammad's wives came to be prefixed over time. It is derived from the Quran (33:6): "The Prophet is closer to the believers than their selves, and his wives are (as) their mothers" is applied to all of the wives.


== Family life ==
Muhammad and his family lived in small apartments adjacent to the mosque at Medina. Each of these was six to seven spans wide (5.5  feet) and ten spans long (7.5  feet). The height of the ceiling was that of an average man standing. The blankets were used as curtains to screen the doors. According to an account by Anas bin Malik, "The Prophet used to visit all his wives in a round, during the day and night and they were eleven in number." I asked Anas, "Had the Prophet the strength for it?" Anas replied, "We used to say that the Prophet was given the strength of thirty (men)." And Sa'id said on the authority of Qatada that Anas had told him about nine wives only (not eleven)."Although Muhammad's wives had a special status as Mothers of the Believers, he did not allow them to use his status as a prophet to obtain special treatment in public.


== Muhammad's marriages ==


=== Khadijah bint Khuwaylid ===
At the age of 25, Muhammad wed his wealthy employer, the 28- or 40-year-old daughter of a merchant, Khadija. This marriage, his first, would be both happy and monogamous; Muhammad would rely on Khadija in many ways, until her death 25 years later. They had two sons, Qasim and Abd-Allah (nicknamed al-Ṭāhir and al-Ṭayyib respectively), both died young, and four daughters—Zaynab, Ruqaiya, Umm Kulthum and Fatimah. Shia scholars dispute the paternity of Khadija's daughters, as they view the first three of them as the daughters from previous marriages and only Fatimah as the daughter of Muhammad and Khadija. During their marriage, Khadija purchased the slave Zayd ibn Harithah, then adopted the young man as her son at Muhammad's request. Abu Talib and Khadija died in the same year. He declared the year as Aam ul-Huzn (year of sorrow).


=== Hijrah (migration) to Medina ===


==== Sawda bint Zamʿa ====
Before he left for Medina, it was suggested by Khawlah bint Hakim that he marry Sawda bint Zamʿa, who had suffered many hardships after she became a Muslim. Prior to that, Sawda was married to a paternal cousin of hers named As-Sakran bin ‘Amr, and had five or six sons from her previous marriage. There are disagreements in Muslim tradition whether Muhammad first married Sawda or Aisha, but Sawda is usually regarded as his second wife and she was living with him before Aisha joined the household. In one account, he married Sawda in Shawwal, when Sawda was about 55 years old, in the tenth year of prophethood, after the death of Khadija. At about the same period, Aisha was betrothed to him.As Sawda got older, and some time after Muhammad's marriage to Umm Salama, some sources claim that Muhammad wished to divorce Sawda. Some traditions maintain that Muhammad did not intend to divorce her, but only Sawda feared or thought that he would. Ibn Kathir says that Muhammad was worried that Sawda might be upset about having to compete with so many younger wives, and offered to divorce her. Sawda offered to give her turn of Muhammad's conjugal visits to Aisha, of whom she was very fond, stating that she "was old, and cared not for men; her only desire was to rise on the Day of Judgment as one of his wives". While some Muslim historians cite this story as a reason of revelation, citing Quran 4:128, others like Rashid Rida dispute this whole account as "poorly supported", or mursal.


==== Aisha bint Abu Bakr ====
Aisha was the daughter of Muhammad's close friend Abu Bakr. She was initially betrothed to Jubayr ibn Muṭʽim, a Muslim whose father, though pagan, was friendly to the Muslims. When Khawlah bint Hakim suggested that Muhammad marry Aisha after the death of Muhammad's first wife (Khadija), the previous agreement regarding marriage of Aisha with ibn Mut'im was put aside by common consent.The majority of traditional sources state that Aisha was betrothed to Muhammad at the age of six or seven, but she stayed in her parents' home until the age of nine, or ten according to Ibn Hisham, when the marriage was consummated with Muhammad, then 53, in Medina. Aisha's age at marriage has been a source of controversy and debate, and some historians, Islamic scholars, and Muslim writers have challenged the previously-accepted timeline of her life. Both Aisha and Sawda, his two wives, were given apartments adjoined to the Al-Masjid al-Nabawi mosque.According to Sunni belief, Aisha was extremely scholarly and inquisitive (the Shia belief is somewhat different, considering her role in The Battle of Camel against Ali, along with other matters). Her contribution to the spread of Muhammad's message was extraordinary, and she served the Muslim community for 44 years after his death. She is also known for narrating 2210 hadith, not just on matters related to Muhammad's private life, but also on topics such as marriage, sex, inheritance, pilgrimage, eschatology, among other subjects. She was highly regarded for her intellect and knowledge in various fields, including poetry and medicine, which received plenty of praise by early luminaries, such as the historian Al-Zuhri and her student Urwa ibn al-Zubayr.


=== Widows of the war with Mecca ===


==== Hafsa bint Umar and Zaynab bint Khuzayma ====
During the Muslim war with Mecca, many men were killed leaving behind widows and orphans. Hafsa bint Umar, daughter of Umar (‘Umar bin Al-Khattab), was widowed at battle of Badr when her husband Khunais ibn Hudhaifa was killed in action. Muhammad married her in 3 A.H./625 C.E. Zaynab bint Khuzayma was also widowed at the battle of Uhad. She was the wife of 'Ubaydah b. al-Hārith, a faithful Muslim and from the tribe of al-Muttalib, for which Muhammad had special responsibility. When her husband died, Muhammad aiming to provide for her, married her in 4 A.H. She was nicknamed Umm Al-Masakeen (roughly translates as the mother of the poor), because of her kindness and charity.Close to Aisha's age, the two younger wives Hafsa and Zaynab were welcomed into the household. Sawda, who was much older, extended her motherly benevolence to the younger women. Aisha and Hafsa had a lasting relationship. As for Zaynab, however, she became ill and died about three months after her marriage.


==== Hind bint Suhayl (Umm Salama) ====
The death of Zaynab coincided with that of Abu Salamah, a devout Muslim and Muhammad's foster brother, as a result of his wounds from the Battle of Uhud. Abu Salamah's widow, Umm Salama, also a devoted Muslim, had none but her young children. Her man-less plight reportedly saddened the Muslims, and after her iddah some Muslims proposed marriage to her; but she declined. When Muhammad proposed her marriage, she was reluctant for three reasons: she claimed to suffer from jealousy and pointed out the prospect of an unsuccessful marriage, her old age, and her young family that needed support. But Muhammad replied that he would pray to God to free her from jealousy, that he too was of old age, and that her family was like his family. She married Muhammad around the end of 4 AH.


==== Rayhana bint Zayd ====
In 626, Rayhana bint Zayd, was a Jewish woman from the Banu Nadir tribe, enslaved along with others after the defeat of the Banu Qurayza tribe.


=== Internal dissension ===
After Muhammad's final battle against his Meccan enemies, he diverted his attention to stopping the Banu Mustaliq's raid on Medina. During this skirmish, Medinan dissidents, begrudging Muhammad's influence, attempted to attack him in the more sensitive areas of his life, including his marriage to Zaynab bint Jahsh, and an incident in which Aisha left her camp to search for her lost necklace, and returned with a Companion of Muhammad.


==== Zaynab bint Jahsh ====
Zaynab bint Jahsh was Muhammad's cousin, the daughter of one of his father's sisters. In Medina Muhammad arranged the widowed Zaynab's marriage to his adopted son Zayd ibn Harithah. Caesar E. Farah states that Muhammad was determined to establish the legitimacy and right to equal treatment of the adopted. Zaynab disapproved of the marriage, and her brothers rejected it, because according to Ibn Sa'd, she was of aristocratic lineage and Zayd was a former slave. Watt states that it is not clear why Zaynab was unwilling to marry Zayd as Muhammad esteemed him highly. He postulates that Zaynab, being an ambitious woman, was already hoping to marry Muhammad; or that she might have wanted to marry someone of whom Muhammad disapproved for political reasons. According to Maududi, after the Qur'anic verse 33:36 was revealed, Zaynab acquiesced and married Zayd.
Zaynab's marriage was unharmonious. According to Watt, it is almost certain that she was working for marriage with Muhammad before the end of 626. "Zaynab had dressed in haste when she was told 'the Messenger of God is at the door.' She jumped up in haste and excited the admiration of the Messenger of God, so that he turned away murmuring something that could scarcely be understood. However, he did say overtly: 'Glory be to God the Almighty! Glory be to God, who causes the hearts to turn!'" Zaynab told Zayd about this, and he offered to divorce her, but Muhammad told him to keep her. The story laid much stress on Zaynab's perceived beauty. Nomani considers this story to be a rumor. Watt doubts the accuracy of this portion of the narrative, since it does not occur in the earliest source. He thinks that even if there is a basis of fact underlying the narrative, it would have been subject to exaggeration in the course of transmission as the later Muslims liked to maintain that there was no celibacy and monkery in Islam. Rodinson disagrees with Watt arguing that the story is stressed in the traditional texts and that it would not have aroused any adverse comment or criticism. This story has been rejected by most Muslim scholars mainly because of its lack of having any chain of narration and its complete absence from any authentic hadith. Some commentators have found it absurd that Muhammad would suddenly become aware of Zaynab's beauty one day after having known her all her life; if her beauty had been the reason for Muhammad to marry her, he would have married her himself in the first place rather than arranging her marriage to Zayd.Muhammad, fearing public opinion, was initially reluctant to marry Zaynab. The marriage would seem incestuous to their contemporaries because she was the former wife of his adopted son, and adopted sons were considered the same as biological sons. According to Watt, this "conception of incest was bound up with old practices belonging to a lower, communalistic level of familial institutions where a child's paternity was not definitely known; and this lower level was in process being eliminated by Islam." The Qur'an,33:37 however, indicated that this marriage was a duty imposed upon him by God. It implied that treating adopted sons as real sons was objectionable and that there should now be a complete break with the past. Thus Muhammad, confident that he was strong enough to face public opinion, proceeded to reject these taboos. When Zaynab's waiting period was complete, Muhammad married her. An influential faction in Medina, called "Hypocrites" in the Islamic tradition, did indeed criticize the marriage as incestuous. Attempting to divide the Muslim community, they spread rumors as part of a strategy of attacking Muhammad through his wives. According to Ibn Kathir, the relevant Qur'anic verses were a "divine rejection" of the Hypocrites' objections. According to Rodinson, doubters argued the verses were in exact conflict with social taboos and favored Muhammad too much. The delivery of these verses, thus, did not end the dissent.


==== Necklace incident ====
Aisha had accompanied Muhammad on his skirmish with the Banu Mustaliq. On the way back, Aisha lost her necklace which she had borrowed from her sister Asma Bint Abu Bakr (a treasured possession), and Muhammad required the army to stop so that it could be found. The necklace was found, but during the same journey, Aisha lost it again. This time, she quietly slipped out in search for it, but by the time she recovered it, the caravan had moved on. She was eventually taken home by Safw'an bin Mu'attal.Rumors spread that A'isha and Safw'an committed adultery although there were no witnesses to this. Disputes arose, and the community was split into factions. Meanwhile, Aisha had been ill, and unaware of the stories. At first, Muhammad himself was unsure of what to believe, but eventually trusted Aisha's protestations of innocence. Eventually, verses of surah Nur were revealed to Muhammad, establishing her innocence, and condemning the slanders and the libel. Although the episode was uneasy for both Muhammad and Aisha, in the end, it reinforced their mutual love and trust.According to shia (Allameh Tabataba'), revealing of Nur's verses belongs to Maria al-Qibtiyya, another wife of Muhammad. Also the accuracy of incident free from which wife of Muhammad, isn't confirmed by shia scholar (Grand Ayatollah Naser Makarem Shirazi), because the Ismah of Muhammad is violated.


=== Reconciliation ===


==== Juwayriyya bint al-Harith ====
One of the captives from the skirmish with the Banu Mustaliq was Juwayriyya bint al-Harith, who was the daughter of the tribe's chieftain. Her husband, Mustafa bin Safwan, had been killed in the battle. She initially fell among the booty of Muhammad's companion Thabit b. Qays b. Al-Shammas. Upon being enslaved, Juwayriyya went to Muhammad requesting that she - as the daughter of the lord of the Mustaliq - be released, however he refused. Meanwhile, her father approached Muhammad with ransom to secure her release, but Muhammed still refused to release her. Muhammad then offered to marry her, and she accepted. When it became known that tribes persons of Mustaliq were kinsmen of the prophet of Islam through marriage, the Muslims began releasing their captives. Thus, Muhammad's marriage resulted in the freedom of nearly one hundred families whom he had recently enslaved.


==== Safiyya bint Huyayy Ibn Akhtab ====
Safiyya bint Huyayy was a noblewoman, the daughter of Huyayy ibn Akhtab, chief of the Jewish tribe Banu Nadir, who was executed after surrendering at the Battle of the Trench. She had been married first to the poet Sallam ibn Mishkam, who had divorced her, and second to Kenana ibn al-Rabi, a commander. In 628, at the Battle of Khaybar, Banu Nadir was defeated, her husband was executed and she was taken as a prisoner. Muhammad freed her from her captor Dihya and proposed marriage, which Safiyya accepted. According to Martin Lings, Muhammad had given Safiyyah the choice of returning to the defeated Banu Nadir, or becoming Muslim and marrying him, and Safiyyah opted for the latter choice.According to a hadith, Muhammad's contemporaries believed that due to Safiyya's high status, it was only befitting that she be manumitted and married to Muhammad. Modern scholars believe that Muhammad married Safiyya as part of reconciliation with the Jewish tribe and as a gesture of goodwill. John L. Esposito states that the marriage may have been political or to cement alliances. Haykal opines that Muhammad's manumission of and marriage to Safiyaa was partly in order to alleviate her tragedy and partly to preserve their dignity, and compares these actions to previous conquerors who married the daughters and wives of the kings whom they had defeated. According to some, by marrying Safiyyah, Muhammad aimed at ending the enmity and hostility between Jews and Islam.Muhammad convinced Safiyya to convert to Islam. According to Al-Bayhaqi, Safiyyah was initially angry at Muhammad as both her father and husband had been killed. Muhammad explained "Your father charged the Arabs against me and committed heinous acts." Eventually, Safiyyah got rid of her bitterness against Muhammad. According to Abu Ya'la al-Mawsili, Safiyya came to appreciate the love and honor Muhammad gave her, and said, "I have never seen a good-natured person as the Messenger of Allah". Safiyyah remained loyal to Muhammad until he died.According to Islamic tradition, Safiyya was beautiful, patient, intelligent, learned and gentle, and she respected Muhammad as "Allah's Messenger". Muslim scholars state she had many good moral qualities. She is described as a humble worshiper and a pious believer. Ibn Kathir said, "she was one of the best women in her worship, piousness, ascetism, devoutness, and charity". According to Ibn Sa'd, Safiyyah was very charitable and generous. She used to give out and spend whatever she had; she gave away a house that she had when she was still alive.Upon entering Muhammad's household, Safiyya became friends with Aisha and Hafsa. Also, she offered gifts to Fatima. She gave some of Muhammad's other wives gifts from her jewels that she brought with her from Khaybar. However, some of Muhammad's other wives spoke ill of Safiyya's Jewish descent. Muhammad intervened, pointing out to everyone that Safiyya's "husband is Muhammad, father is Aaron, and uncle is Moses", a reference to revered prophets.Muhammad once went to hajj with all his wives. On the way Safiyya's camel knelt down, as it was the weakest in the caravan, and she started to weep. Muhammad came to her and wiped her tears with his dress and hands, but the more he asked her not to cry, the more she went on weeping. When Muhammad was terminally ill, Safiyya was profoundly upset. She said to him "I wish it was I who was suffering instead of you."


==== Ramla bint Abi Sufyan (Umm Habiba) ====
In the same year, Muhammad signed a peace treaty with his Meccan enemies, the Quraysh effectively ending the state of war between the two parties. He soon married the daughter of the Quraysh leader, Abu Sufyan ibn Harb, aimed at further reconciling his opponents. He sent a proposal for marriage to Ramla bint Abi Sufyan, who, was in Abyssinia at the time when she learned her husband had died. She had previously converted to Islam (in Mecca) against her father's will. After her migration to Abyssinia her husband had converted to Christianity. Muhammad dispatched ‘Amr bin Omaiyah Ad-Damri with a letter to the Negus (king), asking him for Umm Habiba’s hand — that was in Muharram, in the seventh year of Al-Hijra.


==== Maria al-Qibtiyya ====
Maria al-Qibtiyya was an Egyptian Coptic Christian, sent as a gift to Muhammad from Muqawqis, a Byzantine official. and bore him a son named Ibrahim, who died in infancy.


==== Maymuna binti al-Harith ====
As part of the treaty of Hudaybiyah, Muhammad visited Mecca for the lesser pilgrimage. There Maymuna bint al-Harith proposed marriage to him. Muhammad accepted, and thus married Maymuna, the sister-in-law of Abbas, a longtime ally of his. By marrying her, Muhammad also established kinship ties with the banu Makhzum, his previous opponents. As the Meccans did not allow him to stay any longer, Muhammad left the city, taking Maymuna with him. Her original name was "Barra" but he called her "Maymuna", meaning the blessed, as his marriage to her had also marked the first time in seven years when he could enter his hometown Mecca.


=== Muhammad's widows ===

According to the Qur'an, God forbade anyone to marry the wives of Muhammad, because of their respect and honour, after he died.

Nor is it right for you that ye should annoy Allah's Messenger, or that ye should marry his wives after him at any time.[Quran 33:53]
The extent of Muhammad's property at the time of his death is unclear. Although Qur'an [2.180] clearly addresses issues of inheritance, Abu Bakr, the new leader of the Muslim ummah, refused to divide Muhammad's property among his widows and heirs, saying that he had heard Muhammad say:

We (Prophets) do not have any heirs; what we leave behind is (to be given in) charity.Muhammad's widow Hafsa played a role in the collection of the first Qur'anic manuscript. After Abu Bakr had collected the copy, he gave it to Hafsa, who preserved it until Uthman took it, copied it and distributed it in Muslim lands.Some of Muhammad's widows were active politically in the Islamic state after Muhammad's death. Safiyya, for example, aided the Caliph Uthman during his siege. During the first fitna, some wives also took sides. Umm Salama, for example, sided with Ali, and sent her son Umar for help. The last of Muhammad's wives, Umm Salama lived to hear about the tragedy of Karbala in 680, dying the same year. The grave of the wives of Muhammed is located at al-Baqīʿ Cemetery, Medina.


== Timeline of marriages ==

The vertical lines in the graph indicate, in chronological order, the start of prophethood, the Hijra, and the Battle of Badr.


== Family tree ==

* indicates that the marriage order is disputed
Note that direct lineage is marked in bold.


== See also ==
Ahl al-Bayt
Women in Islam
Children of Muhammad


== References ==


== Bibliography ==


=== Wives of Muhammad ===
Al-Shati, Bint (December 2006). The wives of the Prophet. Matti Moosa (trans.), D. Nicholas Ranson. Gorgias Press LLC. ISBN 978-1-59333-398-0.


=== Women in Islam ===
Freyer Stowasser, Barbara (1996). Women in the Qur'an, Traditions, and Interpretation. Oxford University Press. ISBN 978-0-19-511148-4.
Mernissi, Fatima (1991). The Veil and the Male Elite; A Feminist Interpretation of Women's Rights in Islam. Addison-Wesley (now Perseus Books). originally published 1987 in French, 1991 english translation, Paperback 1993
Khadduri, Majid (1978). "Marriage in Islamic Law: The Modernist Viewpoints". American Journal of Comparative Law. The American Society of Comparative Law. 26 (2): 213–218. doi:10.2307/839669. JSTOR 839669.


=== General ===
Ramadan, Tariq (2007). In the Footsteps of the Prophet: Lessons from the Life of Muhammad. Oxford University Press. ISBN 0-19-530880-8.
Peters, Francis Edward (2003). Islam: A Guide for Jews and Christians. Princeton University Press. ISBN 0-691-11553-2.
Peters, Francis Edward (2003b). The Monotheists: Jews, Christians, and Muslims in Conflict and Competition. Princeton University Press. ISBN 0-691-11461-7. ASIN: B0012385Z6.
Peterson, Daniel (2007). Muhammad, Prophet of God. Wm. B. Eerdmans Publishing Company. ISBN 0-8028-0754-2.
Esposito, John (1998). Islam: The Straight Path. Oxford University Press. ISBN 0-19-511233-4.
Guillaume, Alfred (1955). The Life of Muhammad: A Translation of Ibn Ishaq's Sirat Rasul Allah. Oxford University Press. ISBN 0-19-636033-1.
Wessels, Antonie (1972). A modern Arabic biography of Muḥammad: a critical study of Muḥammad Ḥusayn Haykal's Ḥayāt Muḥammad. Brill Archive. ISBN 978-90-04-03415-0.
Haykal, Muhammad Husayn (1976). The Life of Muhammad.
Lings, Martin (1983). Muhammad: his life based on the earliest sources. Inner traditions international.
al-Mubarakpuri, Safi ur Rahman (1979). Ar-Raheeq Al-Makhtum. Muslim World League.
Nomani, Shibli (1970). Sirat Al-Nabi. Pakistan Historical Society.
Reeves, Minou (2003). Muhammad in Europe: A Thousand Years of Western Myth-Making. NYU Press. ISBN 978-0-8147-7564-6.
Rodinson, Maxime (1971). Muhammad. Allen Lane the Penguin Press. ISBN 978-1-86064-827-4.
Watt, William Montgomery (1956). Muhammad at Medina. Clarendon Press. ISBN 0-19-577286-5.
Watt, William Montgomery (1974). Muhammad: Prophet and Statesman. Oxford University Press. ISBN 0-19-881078-4.