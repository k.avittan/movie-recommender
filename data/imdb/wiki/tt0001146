Carmilla is an 1872 Gothic novella by Irish author Joseph Sheridan Le Fanu and one of the early works of vampire fiction, predating Bram Stoker's Dracula (1897) by 26 years. First published as a serial in The Dark Blue (1871–72), the story is narrated by a young woman preyed upon by a female vampire named Carmilla, later revealed to be Mircalla, Countess Karnstein (Carmilla is an anagram of Mircalla). The character is a prototypical example of the lesbian vampire, expressing romantic desires toward the protagonist. The novella notably never acknowledges homosexuality as an antagonistic trait, leaving it subtle and relatively unmentioned. The story is often anthologized and has been adapted many times in film and other media.


== Publication ==
Carmilla, serialized in the literary magazine The Dark Blue in late 1871 and early 1872, was reprinted in Le Fanu's short story collection In a Glass Darkly (1872). Comparing the work of two illustrators to the story, David Henry Friston, with that of Michael Fitzgerald, whose work appears in the magazine article, but not in modern printings of the book, reveals inconsistencies in the characters' depictions. Consequently, confusion has arisen relating the pictures to the plot. Isabella Mazzanti illustrated the book's 2014 anniversary edition, published by Editions Soleil and translated by Gaid Girard.


== Plot summary ==
Le Fanu presents the story as part of the casebook of Dr. Hesselius, whose departures from medical orthodoxy rank him as the first occult detective in literature.Laura, the teenage protagonist, narrates, beginning with her childhood in a "picturesque and solitary" castle amid an extensive forest in Styria, where she lives with her father, a wealthy English widower retired from service to the Austrian Empire. When she was six, Laura had a vision of a very beautiful visitor in her bedchamber. She later claims to have been punctured in her breast, although no wound was found.
Twelve years later, Laura and her father are admiring the sunset in front of the castle when her father tells her of a letter from his friend, General Spielsdorf. The General was supposed to bring his niece, Bertha Rheinfeldt, to visit the two, but the niece suddenly died under mysterious circumstances. The General ambiguously concludes that he will discuss the circumstances in detail when they meet later.
Laura, saddened by the loss of a potential friend, longs for a companion. A carriage accident outside Laura's home unexpectedly brings a girl of Laura's age into the family's care. Her name is Carmilla. Both girls instantly recognize the other from the "dream" they both had when they were young.
Carmilla appears injured after her carriage accident, but her mysterious mother informs Laura's father that her journey is urgent and cannot be delayed. She arranges to leave her daughter with Laura and her father until she can return in three months. Before she leaves, she sternly notes that her daughter will not disclose any information whatsoever about her family, past, or herself, and that Carmilla is of sound mind. Laura comments that this information seems needless to say, and her father laughs it off.
Carmilla and Laura grow to be very close friends, but occasionally Carmilla's mood abruptly changes. She sometimes makes romantic advances towards Laura. Carmilla refuses to tell anything about herself, despite questioning by Laura. Her secrecy is not the only mysterious thing about Carmilla; she never joins the household in its prayers, she sleeps much of the day, and she seems to sleepwalk outside at night.
Meanwhile, young women and girls in the nearby towns have begun dying from an unknown malady. When the funeral procession of one such victim passes by the two girls, Laura joins in the funeral hymn. Carmilla bursts out in rage and scolds Laura, complaining that the hymn hurts her ears.
When a shipment of restored heirloom paintings arrives, Laura finds a portrait of her ancestor, Mircalla, Countess Karnstein, dated 1698. The portrait resembles Carmilla exactly, down to the mole on her neck. Carmilla suggests that she might be descended from the Karnsteins even though the family died out centuries before.
During Carmilla's stay, Laura has nightmares of a large cat-like beast entering her room. The beast springs onto the bed and Laura feels something like two needles, an inch or two apart, darting deep into her breast. The beast then takes the form of a female figure and disappears through the door without opening it. In another nightmare, Laura hears a voice say, "Your mother warns you to beware of the assassin," and a sudden light reveals Carmilla standing at the foot of her bed, her nightdress drenched in blood. Laura's health declines, and her father has a doctor examine her. He finds a small blue spot, an inch or two below her collar, where the creature in her dream bit her, and speaks privately with her father, only asking that Laura never be unattended.
Her father then sets out with Laura, in a carriage, for the ruined village of Karnstein, three miles distant. They leave a message behind asking Carmilla and one of the governesses to follow once the perpetually late-sleeping Carmilla awakes. En route to Karnstein, Laura and her father encounter General Spielsdorf. He tells them his own ghastly story:
At a costume ball, Spielsdorf and his niece Bertha had met a very beautiful young woman named Millarca and her enigmatic mother. Bertha was immediately taken with Millarca. The mother convinced the General that she was an old friend of his and asked that Millarca be allowed to stay with them for three weeks while she attended to a secret matter of great importance.

Bertha fell mysteriously ill, suffering the same symptoms as Laura. After consulting with a specially ordered priestly doctor, the General realized that Bertha was being visited by a vampire. He hid with a sword and waited until a large black creature of undefined shape crawled onto his niece's bed and spread itself onto her throat. He leapt from his hiding place and attacked the creature, which had then taken the form of Millarca. She fled through the locked door, unharmed. Bertha died before the morning dawned.
Upon arriving at Karnstein, the General asks a woodman where he can find the tomb of Mircalla Karnstein. The woodman says the tomb was relocated long ago by the hero, a Moravian nobleman, who vanquished the vampires that haunted the region.
While the General and Laura are alone in the ruined chapel, Carmilla appears. The General and Carmilla both fly into a rage upon seeing each other, and the General attacks her with an axe. Carmilla disarms the General and disappears. The General explains that Carmilla is also Millarca, both anagrams for the original name of the vampire Mircalla, Countess Karnstein.
The party is joined by Baron Vordenburg, the descendant of the hero who rid the area of vampires long ago. Vordenburg, an authority on vampires, has discovered that his ancestor was romantically involved with the Countess Karnstein before she died and became one of the undead. Using his forefather's notes, he locates Mircalla's hidden tomb. An Imperial Commission exhumes the body of  Mircalla/Millarca/Carmilla. Immersed in blood, it seems to be breathing faintly, its heart beating, its eyes open. A stake is driven through its heart, and it gives a corresponding shriek; then the head is struck off. The body and head are burned to ashes, which are thrown into a river.
Afterward, Laura's father takes his daughter on a year-long tour through Italy to regain her health and recover from the trauma, which she never fully does.


== Sources ==

As with Dracula, critics have looked for the sources used in the writing of Carmilla. One source used was from a dissertation on magic, vampires and the apparitions of spirits written by Dom Augustin Calmet entitled  Traité sur les apparitions des esprits et sur les vampires ou les revenants de Hongrie, de Moravie, &c. (1751). This is evidenced by a report analyzed by Calmet, from a priest who learned information of a town being tormented by a vampiric entity three years earlier. Having traveled to the town to investigate and collecting information of the various inhabitants there, the priest learned that a vampire had tormented many of the inhabitants at night by coming from the nearby cemetery and would haunt many of the residents on their beds. An unknown Hungarian traveler came to the town during this period and helped the town by setting a trap at the cemetery and decapitating the vampire that resided there, curing the town of their torment. This story was retold by Le Fanu and adapted into the thirteenth chapter of Carmilla According to Matthew Gibson, the Reverend Sabine Baring-Gould's The Book of Were-wolves (1863) and his account of Elizabeth Báthory, Coleridge's Christabel (Part 1, 1797 and Part 2, 1800), and Captain Basil Hall's Schloss Hainfeld; or a Winter in Lower Styria (London and Edinburgh, 1836) are other sources for Le Fanu's Carmilla. Hall's account provides much of the Styrian background and, in particular, a model for both Carmilla and Laura in the figure of Jane Anne Cranstoun, Countess Purgstall.


== Influence ==
Carmilla, the title character, is the original prototype for a legion of female and lesbian vampires. Though Le Fanu portrays his vampire's sexuality with the circumspection that one would expect for his time, it is evident that lesbian attraction is the main dynamic between Carmilla and the narrator of the story:
Sometimes after an hour of apathy, my strange and beautiful companion would take my hand and hold it with a fond pressure, renewed again and again; blushing softly, gazing in my face with languid and burning eyes, and breathing so fast that her dress rose and fell with the tumultuous respiration. It was like the ardour of a lover; it embarrassed me; it was hateful and yet overpowering; and with gloating eyes she drew me to her, and her hot lips travelled along my cheek in kisses; and she would whisper, almost in sobs, "You are mine, you shall be mine, and you and I are one for ever." (Carmilla, Chapter 4).
When compared to other literary vampires of the 19th century, Carmilla is a similar product of a culture with strict sexual mores and tangible religious fear. While Carmilla selected exclusively female victims, she only becomes emotionally involved with a few. Carmilla had nocturnal habits, but was not confined to the darkness. She had unearthly beauty, and was able to change her form and to pass through solid walls. Her animal alter ego was a monstrous black cat, not a large dog as in Dracula. She did, however, sleep in a coffin. Carmilla works as a Gothic horror story because her victims are portrayed as succumbing to a perverse and unholy temptation that has severe metaphysical consequences for them.Some critics, among them William Veeder, suggest that Carmilla, notably in its outlandish use of narrative frames, was an important influence on Henry James' The Turn of the Screw (1898).


=== Bram Stoker's Dracula ===
Although Carmilla is a lesser known and far shorter Gothic vampire story than the generally considered master work of that genre, Dracula, the latter is influenced by Le Fanu's works: 

Both stories are told in the first person. Dracula expands on the idea of a first person account by creating a series of journal entries and logs of different persons and creating a plausible background story for their having been compiled.
Both authors indulge the air of mystery, though Stoker takes it further than Le Fanu by allowing the characters to solve the enigma of the vampire along with the reader.
The descriptions of the title character in Carmilla and of Lucy in Dracula are similar. Additionally, both women sleepwalk.
Stoker's Dr. Abraham Van Helsing is similar to Le Fanu's vampire expert Baron Vordenburg: both characters used to investigate and catalyze actions in opposition to the vampire.
The symptoms described in Carmilla and Dracula are highly comparable.


== In popular culture ==


=== Books ===
(alphabetical by author's last name)

In the Japanese light novel series High School DxD, written by Ichiei Ishibumi and illustrated by Miyama-Zero, the vampires are depicted as having a monarchial society divided among two major factions each under the rule of their respective Vampire Royal Family: The Tepes and the Carmilla. The Carmilla faction under the rule of the Carmilla Vampire Royal Family favors a matriarchal society for the world of vampires while the Tepes under the rule of the Tepes Vampire Royal Family prefer a patriarchal government.
Carmilla: A Dark Fugue is a short book by David Brian. Although the story is primarily centered around the exploits of General Spielsdorf, it nonetheless relates directly to events which unfold within Carmilla: The Wolves of Styria.
Theodora Goss' 2018 novel European Travel for the Monstrous Gentlewoman (the second in The Extraordinary Adventures of the Athena Club series) features a heroic Carmilla & her partner Laura Hollis aiding The Athena Club in their fight against Abraham Van Helsing. Tor.com's review of the novel states, "It’s utterly delightful to see Goss’s version of Carmilla and Laura, a practically married couple living happily in the Austrian countryside, and venturing forth to kick ass and take names."The novel Carmilla: The Wolves of Styria is a re-imagining of the original story. It is a derivative re-working, listed as being authored by J.S. Le Fanu and David Brian.
Rachel Klein's 2002 novel The Moth Diaries features several excerpts from Carmilla, as the novel figures into the plot of Klein's story, and both deal with similar subject matter and themes.
Carmilla: The Return by Kyle Marffin is the sequel to Carmilla.
Erika McGann's book The Night-Time Cat and the Plump Grey Mouse: A Trinity College Tale depicts Carmilla and Dracula as summoned by the ghosts of their respective creators, Sheridan Le Fanu and Bram Stoker, to fight one another, witnessed by the book's titular cat, Pangur Bán, whom Carmilla attempts to befriend after both Dracula and her forfeit their fight.
Ro McNulty's novella, Ruin: The Rise of the House of Karnstein, is a sequel to Le  Fanu's novella and takes place over 100 years later. Carmilla continues to play games with mortals, inserting herself into their lives and breaking them to her will. She settles herself around a teacher and his family, feeding on his baby daughter.
A vampire named Baron Karnstein appears in Kim Newman's novel Anno Dracula (1992). Carmilla herself is mentioned several times as a former (until her death at the hands of vampire hunters) friend of the book's vampire heroine Geneviève. Some short stories set in the Anno Dracula series universe have also included Carmilla.
Author Anne Rice has cited Carmilla as an inspiration for The Vampire Chronicles, her ongoing series beginning in 1976 with Interview with the Vampire.
Robert Statzer's novel To Love a Vampire (ISBN 978-1721227310) depicts Carmilla's encounter with a young Dr. Abraham Van Helsing, the hero of Bram Stoker's Dracula, during his days as a medical student. Originally published as a serial in the pages of Scary Monsters Magazine from March 2011 to June 2013, a revised version of To Love a Vampire was reprinted in paperback and Kindle editions in June 2018.


=== Comics ===
(alphabetical by series title)

In 1991, Aircel Comics published a six-issue black and white miniseries of Carmilla by Steven Jones and John Ross. It was based on Le Fanu's story and billed as "The Erotic Horror Classic of Female Vampirism". The first issue was printed in February 1991. The first three issues adapted the original story, while the latter three were a sequel set in the 1930s.
In the first story arc of Dynamite Entertainment's revamp of Vampirella, a villainous vampire, named Le Fanu, inhabits the basement of a Seattle nightclub called Carmilla.


=== Film ===
(chronological)

Danish director Carl Dreyer loosely adapted Carmilla for his film Vampyr (1932) but deleted any references to lesbian sexuality. The credits of the original film say that the film is based on In a Glass Darkly. This collection contains five tales, one of which is Carmilla. Actually the film draws its central character, Allan Gray, from Le Fanu's Dr. Hesselius; and the scene in which Gray is buried alive is drawn from "The Room in the Dragon Volant".
Dracula's Daughter (1936), Universal Pictures sequel to 1931 Dracula film, was loosely based on Carmilla.
French director Roger Vadim's Et mourir de plaisir (literally And to die of pleasure, but actually shown in the UK and US as Blood and Roses, 1960) is based on Carmilla and is considered one of the greatest films of the vampire genre. The Vadim film thoroughly explores the lesbian implications behind Carmilla's selection of victims, and boasts cinematography by Claude Renoir. The film's lesbian eroticism was, however, significantly cut for its US release. Annette Stroyberg, Elsa Martinelli and Mel Ferrer star in the film.
A more-or-less faithful adaptation starring Christopher Lee was produced in Italy in 1963 under the title La cripta e l'incubo (Crypt of the Vampire in English). The character of Laura is played by Adriana Ambesi, who fears herself possessed by the spirit of a dead ancestor, played by Ursula Davis (also known as Pier Anna Quaglia).
The British Hammer Film Productions also produced a fairly faithful adaptation of Carmilla titled The Vampire Lovers (1970) with Ingrid Pitt in the lead role, Madeline Smith as her victim/lover, and Hammer's regular Peter Cushing. It is the first installment of the Karnstein Trilogy.
The Blood Spattered Bride (La novia ensangrentada) is a 1972 Spanish horror film written and directed by Vicente Aranda, is based on the text. The film has reached cult status for its mix of horror, vampirism and seduction with lesbian overtones. British actress Alexandra Bastedo plays Mircalla Karnstein, and Maribel Martín is her victim.
Carmilla (1980) is a black-and-white made-for-television adaptation from Poland, starring singer Izabela Trojanowska in the title role and Monika Stefanowicz as Laura.
The 2000 Japanese anime film Vampire Hunter D: Bloodlust features Carmilla "the Bloody Countess" as its primary antagonist. Having been slain by Dracula for her vain and gluttonous tyranny, Carmilla's ghost attempts to use the blood of a virgin to bring about her own resurrection. She was voiced by Julia Fletcher in English and Beverly Maeda in Japanese.
In the direct-to-video movie, The Batman vs. Dracula (2005), Carmilla Karnstein is mentioned as Count Dracula's bride, who had been incinerated by sunlight years ago. Dracula hoped to revive her by sacrificing Vicki Vale's soul, but the ritual was stopped by the Batman.
Carmilla is featured as the main antagonist in the movie Lesbian Vampire Killers (2009), a comedy starring Paul McGann and James Corden, with Silvia Colloca as Carmilla.
The book is directly referenced several times in the 2011 film, The Moth Diaries, the film version of Rachel Klein's novel. There are conspicuous similarities between the characters in "Carmilla" and those in the film, and the book figures into the film's plot.
The Unwanted (2014) from writer/director Brent Wood relocates the story to the contemporary southern United States, with Hannah Fierman as Laura, Christen Orr as Carmilla, and Kylie Brown as Millarca.
The Curse of Styria (2014), alternately titled Angels of Darkness is an adaptation of the novel set in late 1980s with Julia Pietrucha as Carmilla and Eleanor Tomlinson as Lara.
In 2017 The Carmilla Movie, based on the 2015 web series Carmilla, was released. Directed by Spencer Maybee and produced by Steph Ouaknine, the movie follows up the web series 5 years after the finale.
Carmilla (2019), written and directed by Emily Harris, was inspired by the novella. Fifteen-year-old Lara (Hannah Rae) develops feelings for Carmilla (Devrim Lingnau), but her strict governess believes their strange houseguest is a vampire. Harris says she "stripped back" the supernatural layers to consider the story as a "derailed love story" and "a story about our tendency as humans to demonize the other".


=== Music ===
Doctor Carmilla aka Maki Yamazaki is a Retrospective-Futurist Visual Kei multi-instrumentalist, musician and composer.
Autumn, album composed by A Letter for Carmilla in October 2017 (Romantic Dungeon synth)Autumn, by A letter for Carmilla
The debut track of K-pop girl group Red Velvet subunit Irene & Seulgi, "Monster" (2020) is believed to be thematically inspired by Carmilla, with the music video having Irene loosely portray Laura and Seulgi loosely portraying Carmilla.


==== Opera ====
A chamber opera version of Carmilla appeared in Carmilla: A Vampire Tale (1970), music by Ben Johnston, script by Wilford Leach. Seated on a sofa, Laura and Carmilla recount the story retrospectively in song.


==== Rock music ====
Jon English released a song named Carmilla, inspired by the short story, on his 1980 album Calm Before the Storm.
British extreme metal band Cradle of Filth's lead singer Dani Filth has often cited Sheridan Le Fanu as an inspiration to his lyrics.  For example, their EP, V Empire or Dark Faerytales in Phallustein (1994), includes a track titled "Queen of Winter, Throned", which contains the lyrics: "Iniquitous/I share Carmilla's mask/A gaunt mephitic voyeur/On the black side of the glass". Additionally, the album Dusk... and Her Embrace (1996) was largely inspired by Carmilla and Le Fanu's writings in general. The band has also recorded an instrumental track titled "Carmilla's Masque", and in the track "A Gothic Romance", the lyric "Portrait of the Dead Countess" may reference the portrait found in the novel of the Countess Mircalla.
The Discipline band's 1993 album Push & Profit included a ten-minute song entitled Carmilla, based on Le Fanu's character.
The lyrics for "Blood and Roses", LaHost's track on the EMI compilation album Fire in Harmony (1985), are loosely based on the Roger Vadim film version of Carmilla.
The title track of  the album Symphonies of the Night (2013), by the German/Norwegian band Leaves' Eyes, was inspired by Carmilla.
Alessandro Nunziati, better known as Lord Vampyr and the former vocalist of Theatres des Vampires, has a song named "Carmilla Whispers from the Grave" in his debut solo album, De Vampyrica Philosophia (2005).
Theatres des Vampires, an Italian extreme gothic metal band, has produced a video single called "Carmilla" for its album Moonlight Waltz. They also reference the novel in innumerous other songs.
Carmillas of Love, by the band of Montreal references the novel in its title and lyrics.


=== Periodicals ===
A Japanese lesbian magazine is named after Carmilla, as Carmilla "draws hetero women into the world of love between women".


=== Radio ===
(chronological)

The Columbia Workshop presented an adaptation (CBS, July 28, 1940, 30 min.). Lucille Fletcher's script, directed by Earle McGill, relocated the story to contemporary New York state and allows Carmilla (Jeanette Nolan) to claim her victim Helen (Joan Tetzel).
The character Dr. Hesselius is featured as an occult sleuth in The Hall of Fantasy episode, "The Shadow People" (September 5, 1952), broadcast on the Mutual Broadcasting Network.
In 1975, the CBS Radio Mystery Theater broadcast an adaptation by Ian Martin (CBS, July 31, 1975, rebroadcast December 10, 1975). Mercedes McCambridge played Laura Stanton, Marian Seldes played Carmilla.
Vincent Price hosted an adaption (reset to 1922 Vienna) by Brainard Duffield, produced and directed by Fletcher Markle, on the Sears Radio Theater (CBS, March 7, 1979), with Antoinette Bower and Anne Gibbon.
On November 20, 1981, the CBC Radio series Nightfall aired an adaptation of Carmilla written by Graham Pomeroy and John Douglas.
BBC Radio 4 broadcast Don McCamphill's Afternoon Play dramatization June 5, 2003, with Anne-Marie Duff as Laura, Brana Bajic as Carmilla and David Warner as Laura's father.


=== Stage ===
(chronological) 

Wilford Leach and John Braswell's ETC Company staged an adaptation of Carmilla in repertory at La MaMa Experimental Theatre Club throughout the 1970s.
In Elfriede Jelinek's play Illness or Modern Women (1984), a woman, Emily, transforms another woman, Carmilla, into a vampire, and both become lesbians and join together to drink the blood of children.
A German language adaptation of Carmilla by Friedhelm Schneidewind, from Studio-Theatre Saarbruecken, toured Germany and other European countries (including Romania) from April 1994 until 2000.
The Wildclaw Theater in Chicago performed a full-length adaptation of Carmilla by Aly Renee Amidei in January and February 2011.
Zombie Joe's Underground Theater Group in North Hollywood performed an hour-long adaptation of Carmilla, by David MacDowell Blue, in February and March 2014.
Carmilla was also showcased at Cayuga Community College in Auburn, New York by Harelquin Productions with Meg Owren playing Laura and Dominique Baker-Lanning playing Carmilla.
The David MacDowell Blue adaptation of Carmilla was performed by The Reedy Point Players of Delaware City in October 2016. This production was directed by Sean McGuire, produced by Gail Springer Wagner, assistant director Sarah Hammond, technical director Kevin Meinhaldt and technical execution by Aniela Meinhaldt. The performance  featured Mariza Esperanza, Shamma Casson and Jada Bennett with appearances by Wade Finner, David Fullerton, Fran Lazartic, Nicole Peters Peirce, Gina Olkowski and Kevin Swed.


=== Television ===
(alphabetical by series title)

In Season 2 of Castlevania, Carmilla is introduced as a secondary antagonist, acting as a sly and ambitious general on Dracula's War Council. Unlike her video-game counterpart, who is immensely faithful to her leader, Carmilla takes issue with Dracula's plan to kill off their only source of food and has designs to take Dracula's place and build her own army to subjugate humanity. Her plans are bolstered by Dracula's death at the hands of his son, Alucard, and her kidnapping of the Devil Forgemaster, Hector.
The Doctor Who serial arc State of Decay (1980) features a vampire named Camilla (not Carmilla) who, in a brief but explicit moment, finds much to "admire" in the Doctor's female travelling companion Romana, who finds she has to turn away from the vampire's intense gaze.
A television version for the British series Mystery and Imagination was transmitted on 12 November 1966. Jane Merrow played the title role, Natasha Pyne her victim.
In 1989, Gabrielle Beaumont directed Jonathan Furst's adaptation of Carmilla as an episode of the Showtime television series Nightmare Classics, featuring Meg Tilly as the vampire and Ione Skye as her victim Marie. Furst relocated the story to an American antebellum southern plantation.
An episode of the anime series Hellsing features a female vampire calling herself "Laura". She is later referred to as "Countess Karnstein". The character is heavily implied to be sexually attracted to Integra Hellsing, a female protagonist of the series.
In episode 36 of The Return of Ultraman, the monster of the week in the episode, Draculas, originates from a planet named Carmilla. He possesses the corpse of a woman as his human disguise.
In season 2, episodes 5 and 6 of the HBO TV series True Blood, Hotel Carmilla, in Dallas, Texas, has been built for vampires. It features heavy-shaded rooms and provides room service of human "snacks" for their vampire clientele, who can order specific blood types and genders.
In the first and second season of the Freeform series Shadowhunters, based on Cassandra Clare's book series The Mortal Instruments, a vampire named Camille is a minor recurring character.


=== Web series ===
Carmilla is a web series on YouTube starring Natasha Negovanlis as Carmilla and Elise Bauman as Laura. First released on August 19, 2014, it is a comedic, modern adaptation of the novella which takes place at a modern-day university, where both girls are students. They become roommates after Laura's first roommate mysteriously disappears and Carmilla moves in, taking her place. The final episode of the web series was released on October 13, 2016.  In 2017, a movie was made based on the series. The Carmilla Movie was initially released on October 26, 2017 to Canadian audiences through Cineplex theatres for one night only. A digital streaming version was also pre-released on October 26, 2017, for fans who had pre-ordered the film on VHX. The following day the movie enjoyed a wide release on streaming platform Fullscreen.


=== Video games ===
The vampiress Carmilla is an antagonist in the Castlevania series. She is a key figure in Castlevania: Circle of the Moon in which she tries to resurrect Lord Dracula. In the time distorted fighting game Castlevania Judgement, she is a playable character battling to protect her master, and, in Hideo Kojima's Lords Of Shadow reimagining, she is a recurring boss and former leader of the heroic Brotherhood Of Light. In every game she is portrayed as having great admiration for Dracula that borders on obsessive devotion.
In the Japanese action game series OneeChanbara Carmilla is the matriarch of the Vampiric clan. She appears in the 2011 title Oneechanbara Z ~ Kagura ~ as the manipulator & main antagonist of sister heroines Kagura and Saaya, first using them to attack her rivals before trying (and failing) to eliminate them as pawns.
The main antagonist in Ace Combat Infinity is a mysterious girl only known as the "Butterfly Master", who pilots the fictional QFA-44 "Carmilla" aircraft. The Butterfly Master herself is situated in a low-orbit satellite, controlling the Carmilla aircraft through a "COnnection For Flight INterface", or "COFFIN".
Carmilla is a character that can be summoned by the player in the role-playing mobile game Fate/Grand Order. However, she also plays a small role in the main story arc. She is depicted as the elder version of Elizabeth Báthory, who appears as a separate Servant with whom she interacts.
In the action-adventure role-playing video game, Boktai: The Sun Is in Your Hand, Carmilla is a vampire with the ability to transform into a medusa-like banshee, who initially appears as a young woman. Though an antagonist, she is portrayed sympathetically, and is seemingly in love with fellow antagonist Sabata.
Carmilla appears as a playable servant in the game Heir of Light, with her evolved form bearing the name "Carmilla the Red Lily."
Carmilla is a playable support role character of the "Mobile legends: Bang Bang" released in February alongside with Cecilion and purchasable only with 32k of battle point or 599 Diamonds.


== References ==


== External links ==

 Carmilla at Project Gutenberg
Carmilla on IMDb
Carmilla (Mobile ed.). Inkitt.com.
 Carmilla public domain audiobook at LibriVox