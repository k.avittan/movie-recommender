"The King and the Beggar-maid" is a 16th-century broadside ballad that tells the story of an African king, Cophetua, and his love for the beggar Penelophon (Shakespearean Zenelophon). The story has been widely referenced and King Cophetua has become a byword for "a man who falls in love with a woman instantly and proposes marriage immediately".


== Story ==

Cophetua was an African king known for his lack of any sexual attraction to women. One day while looking out a palace window he witnesses a young beggar, Penelophon, "clad all in grey". Struck by love at first sight, Cophetua decides that he will either have the beggar as his wife or commit suicide.
Walking out into the street, he scatters coins for the beggars to gather and when Penelophon comes forward, he tells her that she is to be his wife. She agrees and becomes queen, and soon loses all trace of her former poverty and low class. The couple lives a "a quiet life during their princely reign" but are much loved by their people. Eventually they die and are buried in the same tomb.


== History ==
William Shakespeare mentions the ballad by title in several plays. It is referenced or alluded in Love's Labour's Lost (I, ii, 115 and V. i. 65–85), A Midsummer Night's Dream (IV, i, 65), Romeo and Juliet (II, i, 14), Richard II (V, viii, 80), and Henry IV, part 2 (V, iii, 107), all written in the 1590s. William Warburton believed that John Falstaff's lines in Henry IV, part 2, referencing Cophetua were taken from a now lost play based on the ballad. In Love's Labour's Lost, Armado asks his page Moth, "Is there not a ballad, boy, of 'The King and the Beggar'?", to which Moth responds, "The world was very guilty of such a ballad some three ages since, but I think now 'tis not to be found; or, if it were, it would neither serve for the writing nor the tune." Ben Jonson also makes reference to the ballad in his play Every Man in His Humour (1598) and William Davenant in The Wits (1634).The oldest version of the tale surviving is that titled "A Song of a Beggar and a King" in Richard Johnson's anthology Crown Garland of Goulden Roses (1612). This was the source of the ballad in the first edition of Francis J. Child's The English and Scottish Popular Ballads (1855), although it was removed from the second edition (1858). The ballad was also published in Thomas Percy's Reliques of Ancient English Poetry (1765).The ballad was probably sung to the melody (air) of "I Often with My Jenny Strove", published first in the third volume of Henry Playford's The Banquet of Music (1689). In the first volume of the anonymous Collection of Old Ballads (1723), a ballad titled "Cupid's Revenge"—which is a mere paraphrase of "The King and the Beggar-maid"—appears set to the music of "I Often with My Jenny Strove". This may be the original air of the Cophetua ballad.


== In later art and literature ==


=== Major treatments ===
The Cophetua story was famously and influentially treated in literature by Alfred, Lord Tennyson (The Beggar Maid, written 1833, published 1842); in oil painting by Edmund Blair Leighton (The King and the Beggar-Maid) and Edward Burne-Jones (King Cophetua and the Beggar Maid, 1884); and in photography by Julia Margaret Cameron and by Lewis Carroll (his most famous photograph; Alice as "Beggar-Maid", 1858).
The painting by Burne-Jones is referred to in the prose poem König Cophetua by the Austrian poet Hugo von Hofmannsthal and in Hugh Selwyn Mauberley (1920), a long poem by Ezra Pound. The painting has a symbolic role in a short novel Le Roi Cophetua by the French writer Julien Gracq (1970). This in turn inspired the 1971 film Rendez-vous à Bray, directed by the Belgian cineaste André Delvaux.
The story was combined with and inflected the modern re-telling of the Pygmalion myth, especially in its treatment by George Bernard Shaw as the 1913 play Pygmalion.
It has also been used to name a sexual desire for lower-class women by upper-class men. Although often attributed first to Graham Greene in his 1951 novel The End of the Affair, the term was used as early as 1942 by Agatha Christie in her mystery “The Body in the Library” [NY: Collier, pp. 119-121] when Jane Marple reflects on the attraction of older wealthy men for young lower-class girls and Sir Henry Clithering dubs it a "Cophetua Complex."
The English poet and critic James Reeves included his poem "Cophetua", inspired by the legend, in his 1958 book The Talking Skull.
Hugh Macdiarmid wrote a brief two-verse poem Cophetua in Scots, which is a slightly parodic treatment of the story.Alice Munro titled one story in her 1980 collection, "The Beggar Maid". Before her marriage to Patrick, Rose is told by him: "You're like the Beggar Maid." "Who?" "King Cophetua and the Beggar Maid. You know. The painting." The American edition of Munro's collection is also titled The Beggar Maid, a change from the Canadian title, Who Do You Think You Are?


== References ==


== Sources ==
King Cophetua and the Beggar-Maid (ballad)