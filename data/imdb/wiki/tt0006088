A list of the earliest Western films, by decade, released before 1920.


== 1890s ==
1894:

Annie Oakley
Bucking Broncho
Buffalo Bill
Buffalo Dance
Sioux Ghost DanceThese films were all shorts produced by William K. L. Dickson at the famous Edison "Black Maria" studio. All the performers were from Buffalo Bill's Wild West Show with Annie Oakley and Buffalo Bill himself exhibiting their rifle shooting skills. The two dances featured three members of the Sioux tribe who were thus the first Native Americans to perform on film.
1899:

Cripple Creek Bar-room Scene
Kidnapping By Indians (1 minute film shot in Blackburn Lancashire U.K)


== 1900s ==
1903:
The Great Train Robbery
Kit Carson
The Pioneers1904:1906:

The Story of the Kelly Gang (Australian Western)
From Leadville to Aspen: A Hold-Up in the Rockies1907:
The Bandit King
Robbery Under Arms1908:
The Bank Robbery
The Fight for Freedom1909:
Bill Sharkey's Last Game
The Cowboy Millionaire
The House of Cards
The Red Man's View


== 1910s ==
1910:
Abernathy Kids to the Rescue
Across the Plains
Broncho Billy's Redemption
In Old California
The Two Brothers1911:
The Abernathy Kids to the Rescue
An Accidental Outlaw
Across the Plains
The Battle
The Cowboy and the Lady
The Indian Brothers
Fighting Blood
The Last Drop of Water
The Last of the Mohicans (colonial Western)
The Lonedale Operator
Swords and Hearts
The Telltale Knife
Was He a Coward?1912:
According to Law
The Ace of Spades
Algie the Miner
The Belle of Bar-Z Ranch
The Chief's Blanket
Geronimo's Last Raid
The Girl and Her Trust (remake of 'The Lonedale Operator')
The Goddess of Sagebrush Gulch
The Half-Breed's Way
The Heart of an Indian
His Only Son
The Invaders
The Massacre
My Hero
A Temporary Truce
Under Burning Skies
When the Heart Calls
With the Enemy's Help1913:
The Abandoned Well
The Accidental Bandit
The Accusation of Broncho Billy
The Battle at Elderbush Gulch
Broken Ways
Calamity Anne's Beauty
Calamity Anne's Inheritance
During the Round-Up
Hearts and Horses
In the Secret Service
An Indian's Loyalty
Past Redemption
The Ranchero's Revenge
The Sheriff's Baby
The Tenderfoot's Money
Three Friends
Two Men of the Desert1914:
The Bargain
The Girl Stage Driver
Her Grave Mistake (considered lost)
The Man from the East
A Miner's Romance (considered lost)
A Ranch Romance (considered lost)
Rose of the Rancho
The Spoilers
The Squaw Man
A Ticket to Red Horse Gulch
The Tragedy of Whispering Creek (considered lost)
The Virginian1915:

Broncho Billy and the Baby
Buckshot John
The Desert Breed (considered lost)
The Girl of the Golden West
The Heart of a Bandit
Keno Bates, Liar
The Passing of the Oklahoma Outlaws
The Ring of Destiny
The Slave Girl
The Stagecoach Driver and the Girl1916:

According to St. John
Accusing Evidence
The Apostle of Vengeance
The Committee on Credentials
For the Love of a Girl
Hell's Hinges
A Knight of the Range
Lass of the Lumberlands
Liberty (presumed to be lost)
Love's Lariat
The Night Riders
The Passing of Hell's Crown
The Return of Draw Egan
Stampede in the Night
The Three Godfathers
The Wire Pullers
A Woman's Eyes1917:

A 44-Calibre Mystery
The Almost Good Man
The Bad Man of Cheyenne
Blood Money
The Drifter
The Dynamite Special
The Empty Gun
The Fighting Gringo
The Fighting Trail
Goin' Straight
The Golden Bullet
Hair-Trigger Burke
Hands Up!
The Honor of an Outlaw
The Little Moccasins
A Marked Man (considered lost)
The Mysterious Outlaw
The Narrow Trail
The Outlaw and the Lady
A Romance of the Redwoods
Roped In
The Scrapper (considered lost)
The Silent Man
Single Shot Parker aka 'The Heart of Texas Ryan'
Six-Shooter Justice
The Soul Herder (considered lost)
Squaring It
Straight Shooting
The Texas Sphinx
The Tornado (considered lost)
Wild and Woolly
The Wrong Man1918:

The Branded Man
Bucking Broadway
The Grand Passion
Hell Bent
Out West
Play Straight or Fight
Revenge
Riddle Gawne (partially lost)
Riders of the Purple Sage
Ruggles of Red Gap (considered lost)
The Scarlet Drop (partially lost)
The Squaw Man (partially lost)
Three Mounted Men (considered lost)
A Woman's Fool (considered lost)1919:

Ace High
Ace of the Saddle (considered lost)
The Black Horse Bandit
Bare Fists (considered lost)
By Indian Post
The Crooked Coin
The Crow
The Double Hold-Up
Elmo the Mighty (considered lost)
The Face in the Watch
A Fight for Love (considered lost)
The Fighting Brothers
The Fighting Heart
The Fighting Line
The Four-Bit Man
A Gun Fightin' Gentleman
Gun Law
The Gun Runners
His Buddy
The Jack of Hearts
The Kid and the Cowboy
Kingdom Come
The Knickerbocker Buckaroo (considered lost)
The Last Outlaw
Lightning Bryce (horror Western)
The Lone Hand
Marked Men (considered lost)
The Masked Rider (partially lost)
The Outcasts of Poker Flat (considered lost)
Partners Three
Rider of the Law (considered lost)
Roped (considered lost)
Rustlers
The Rustlers
Scarlet Days
The Tell Tale Wire
Terror of the Range (considered lost)
The Trail of the Holdup Man
The Tune of Bullets
A Western Wooing
The Wilderness Trail


== See also ==
Western (genre) films
Western (genre) film actors
Western (genre)
Movie Ranches


== References ==