Tinsel is a type of decorative material that mimics the effect of ice, consisting of thin strips of sparkling material attached to a thread. When in long narrow strips not attached to thread, it is called "lametta", and emulates icicles. It was originally a metallic garland for Christmas decoration. The modern production of tinsel typically involves plastic, and is used particularly to decorate Christmas trees. It may be hung from ceilings or wrapped around statues, lampposts, and so on. Modern tinsel was invented in Nuremberg, Germany, in 1610, and was originally made of shredded silver.

According to the Concise Oxford Dictionary, the word is from the Old French word estincele, meaning "sparkle".


== History ==
Modern tinsel was invented in Nuremberg around 1610.  Tinsel was originally made from extruded strands of silver. Because silver tarnishes quickly, other shiny metals were substituted. Before the 19th century, tinsel was used for adorning sculptures rather than Christmas trees. It was added to Christmas trees to enhance the flickering of the candles on the tree. Tinsel was used to represent the starry sky over a Nativity scene.

By the early 20th century, manufacturing advances allowed cheap aluminium-based tinsel, and until World War I, France was the world leader in its manufacture. Production was curtailed during the First World War as a result of wartime demand for copper.During the 1950s, tinsel and tinsel garlands were so popular that they frequently were used more than Christmas lights, as tinsel was much less of a fire hazard than lights were for the then-popular  aluminum Christmas trees, which were made from flammable aluminized paper.Lead foil was a popular material for tinsel manufacture for several decades of the 20th century. Unlike silver, lead tinsel did not tarnish, so it retained its shine. However, use of lead tinsel was phased out after the 1960s due to concern that it exposed children to a risk of lead poisoning. In the United States, the Food and Drug Administration (FDA) concluded in August 1971 that lead tinsel caused an unnecessary risk to children, and convinced manufacturers and importers to voluntarily stop producing or importing lead tinsel after January 1, 1972. The FDA did not actually ban the product because the agency did not have the evidence needed to declare lead tinsel a "health hazard."Modern tinsel is typically made from polyvinyl chloride (PVC) film coated with a metallic finish. Coated mylar film also has been used. These plastic forms of tinsel do not hang as well as tinsel made from heavy metals such as silver and lead.


== Figurative use ==
Germans refer to a row of military Awards and decorations as Lametta (German for tinsel), similar to dressing in full regalia or with a high level of formality. The expression was coined earlier to describe the appearance of Hermann Göring, e.g. in a Chanson by Claire Waldoff, Rechts Lametta, links Lametta, Und der Bauch wird imma fetta (right tinsel, left tinsel, and the belly gets immense).Humorist Loriot's 1977 film Weihnachten bei Hoppenstedts about a family Christmas involved a Prussian-educated grandfather humming the Helenenmarsch with "ra-da-buff" and deploring the lack of tinsel („Früher war mehr Lametta!“ "There used to be more tinsel!"), thus lamenting the changes in life due to the course of time, has become proverbial).


== Other uses ==

Tinsel prints are two different types of print, where tinsel is added after printing, for decorative effect.  The older type is a rare style of German religious woodcut from the early 15th century. The later type is English and 19th-century, especially used for prints of actors in their roles.Tinsel has many traditional uses in India, including decorations on images, garlands for weddings and other ceremonies, and ornamental trappings for horses and elephants. Tinsels of various types are popular materials used in fly tying.


== See also ==
Festoon
Legend of the Christmas Spider
Chaff (countermeasure)


== References ==