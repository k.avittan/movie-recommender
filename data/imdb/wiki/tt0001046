In Christian and Islamic tradition, the Seven Sleepers (Arabic: اصحاب الکهف‎, romanized: aṣḥāb al kahf, lit. 'Companions of the Cave') is the story of a group of youths who hid inside a cave outside the city of Ephesus around 250 CE to escape a religious persecution and emerged some 300 years later.
The earliest version of this story comes from the Syrian bishop Jacob of Serugh (c. 450 – 521), which is itself derived from an earlier Greek source, now lost.  An outline of this tale appears in Gregory of Tours (538–594), and in Paul the Deacon's (720–799) History of the Lombards. The best-known Western version of the story appears in Jacobus da Varagine's Golden Legend.
Accounts found in at least nine medieval languages and preserved in over 200 manuscripts, mainly dating to between the 9th and 13th centuries. These include 104 Latin manuscripts, 40 Greek, 33 Arabic, 17 Syriac, 6 Ethiopic, 5 Coptic, 2 Armenian, 1 Middle Irish and 1 Old English. It was also translated into Sogdian. In the thirteenth century, the poet Chardri composed an Old French version. A version of the story also appears in the Qur'an (Surah al-Kahf 18:9–26). It was also translated into Persian, Kyrgyz and Tatar.The Roman Martyrology mentions the Seven Sleepers of Ephesus under the date of 27 July (June according to Vatican II calendar). The Byzantine calendar commemorates them with feasts on 4 August and 22 October. The ninth-century Irish calendar Félire Óengusso commemorates the Seven Sleepers on 7 August. Syriac Orthodox calendars gives various dates: 21 April, 2 August, 13 August, 23 October and 24 October.


== Number, duration and names ==
Early versions do not all agree on or even specify the number of sleepers. The Jews and the Christians of Najran believed in only three brothers, the East Syriac five. Most Syriac accounts have eight. In aṭ-Ṭabarī, there are seven, eight or nine plus a dog. In the canonical Muslim account there are seven plus a dog named ar-Raqīm or Qiṭmīr.The number of years the sleepers slept also varies between accounts. The highest number, given by Gregory of Tours, was 373 years. Some accounts have 372. Jacobus da Varagine calculated it at 196 (from the year 252 until 448). Although other calculations suggest 195. Islamic accounts, including the Qur'an, give a sleep of 309 years. These are presumably lunar years, which would make it 300 solar years. Qu'ran 18:25 says, 'And they remained in their cave for three hundred years and exceeded by nine.'
Bartłomiej Grysa lists at least seven different sets of names for the sleepers:
Maximilian, Malkhus, Martinian, Dionisius, John, Serapion, Constantine, Anthony
Malchus, Maximian, Martinian, Dionisius, John, Serapion, Constantine
Yamblikh (Iamblichus), Maximilian, Martinian, Dionisius, John, Constantine, Anthony
Achillides, Diomedes, Diogenus, Probatus, Stephanus, Sambatus, Quiriacus (according to Gregory of Tours)
Yamlīḫā (Yamnīḫ), Makṯimilīnā (Maksimilīnā, Maḥsimilīnā), Mišlīnā, Marnūš (Marṭūs), Saḏnūš, Dabranūš (Bīrōnos), Kafašṭaṭyūš (Ksōṭōnos), Samōnos, Buṭōnos, Qālos (according to aṭ-Ṭabarī and ad-Damīrī)
Ikilios, Dionisios, Istifanos, Fruqtis, Sebastos, Qiryaqos (according to Michael the Syrian)
Aršellītīs, Diōmetios, Sabbastios, Probatios, Avhenios, Stafanos, Kīriakos (according to the Coptic version)


== Origins ==
Whether the original account was written in Syriac or Greek was a matter of debate, but today a Greek original is generally accepted. The pilgrim account De situ terrae sanctae, written between 518 and 531, records the existence of a church dedicated to the sleepers in Ephesus.The story appeared in several Syriac sources before Gregory of Tours's lifetime. The earliest Syriac manuscript copy is in MS Saint-Petersburg no. 4, which dates to the 5th century. It was retold by Symeon the Metaphrast. The Seven Sleepers form the subject of a homily in verse by the Edessan poet-theologian Jacob of Serugh (died 521), which was published in the Acta Sanctorum. Another sixth-century version, in a Syrian manuscript in the British Museum (Cat. Syr. Mss, p. 1090), gives eight sleepers.


== Christian interpretation ==


=== Story ===

The story says that during the persecutions by the Roman emperor Decius, around 250 AD, seven young men were accused of following Christianity. They were given some time to recant their faith, but they refused to bow to Roman idols. Instead they chose to give their worldly goods to the poor and retire to a mountain cave to pray, where they fell asleep. The Emperor, seeing that their attitude towards paganism had not improved, ordered the mouth of the cave to be sealed.

Decius died in 251, and many years passed during which Christianity went from being persecuted to being the state religion of the Roman Empire. At some later time–usually given as during the reign of Theodosius II (408–450)–in 447 A.D. when heated discussions were taking place between various schools of Christianity about the resurrection of body in the day of judgement and life after death, a landowner decided to open up the sealed mouth of the cave, thinking to use it as a cattle pen. He opened it and found the sleepers inside. They awoke, imagining that they had slept but one day, and sent one of their number to Ephesus to buy food, with instructions to be careful.Upon arriving in the city, this person was astounded to find buildings with crosses attached; the townspeople for their part were astounded to find a man trying to spend old coins from the reign of Decius. The bishop was summoned to interview the sleepers; they told him their miracle story, and died praising God. The various lives of the Seven Sleepers in Greek are listed and in other non-Latin languages at BHO.


=== Dissemination ===
The story rapidly attained a wide diffusion throughout Christendom. It was popularized in the West by Gregory of Tours, in his late 6th-century collection of miracles, De gloria martyrum (Glory of the Martyrs). Gregory claimed to have gotten the story from "a certain Syrian interpreter" (Syro quidam interpretante), but this could refer to either a Syriac- or Greek-speaker from the Levant. During the period of the Crusades, bones from the sepulchres near Ephesus, identified as relics of the Seven Sleepers, were transported to Marseille, France in a large stone coffin, which remained a trophy of the Abbey of St Victor, Marseille.
The Seven Sleepers were included in the Golden Legend compilation, the most popular book of the later Middle Ages, which fixed a precise date for their resurrection, 478 AD, in the reign of Theodosius.


== Islam ==

The story of the Companions of the Cave (Ashab-al-Kahf, in Arabic) is referred to in the Quran at Surah 18 (verses 9-26). The precise number of the sleepers is not stated. The Quran furthermore points to the fact that people, shortly after the incident emerged, started to make "idle guesses" as to how many people were in the cave. To this the Quran asserts that: "My Sustainer knows best how many they were". Similarly, regarding the exact period of time the people stayed in the cave, the Quran, after asserting the guesswork of the people that "they remained in the cave for 300 years and nine added", resolves that "God knows best how long they remained [there]." According some, the nine years added is the exact difference of 11 days between lunar and solar calendar multiplied by 300. This calculation is correct until the first decimal. The Quran says the sleepers included a dog, who sat at the entrance of the cave (verse 18).


== Caves of the Seven Sleepers ==
Several sites are attributed as the "Cave of the Seven Sleepers", but none have been archaeologically proven to be the actual site. As the earliest versions of the legend spread from Ephesus, an early Christian catacomb came to be associated with it, attracting scores of pilgrims. On the slopes of Mount Pion (Mount Coelian) near Ephesus (near modern Selçuk in Turkey), the grotto of the Seven Sleepers with ruins of the religious site built over it was excavated in 1926–1928. The excavation brought to light several hundred graves dated to the 5th and 6th centuries. Inscriptions dedicated to the Seven Sleepers were found on the walls and in the graves. This grotto is still shown to tourists.
Other possible sites of the cave of the Seven Sleepers are in Afşin and Tarsus, Turkey. Afşin is near the antique Roman city of Arabissus, which the East Roman Emperor Justinian paid a visit. The Emperor brought marble niches as gifts from Western Turkey for the site, which are preserved inside the Eshab-ı Kehf Kulliye mosque to this day. The site was a Hittite temple, used as a Roman temple and later as a church in Roman and Byzantine times. The Seljuks continued to use the place of worship as a church and a mosque. It was turned into a mosque over time with the conversion of the local population to Islam.
There is a cave near Amman, Jordan, also known as the cave of seven sleepers, which have seven graves present inside and a ventilation duct coming out of the cave.
Caves regarded as the cave from the story of the Seven Sleepers
		
		
		
		
		
		


== Modern literature ==


=== Early modern ===

The account had become proverbial in 16th century Protestant culture. The poet John Donne could ask,

In John Heywood's Play called the Four PP (1530s), the Pardoner, a Renaissance update of the protagonist in Chaucer's "The Pardoner's Tale", offers his companions the opportunity to kiss "a slipper / Of one of the Seven Sleepers", but the relic is presented as absurdly as the Pardoner's other offerings, which include "the great-toe of the Trinity" and "a buttock-bone of Pentecost."Little is heard of the Seven Sleepers during the Enlightenment, but the account revived with the coming of Romanticism. The Golden Legend may have been the source for retellings of the Seven Sleepers in Thomas de Quincey's Confessions of an English Opium-Eater, in a poem by Goethe, Washington Irving's "Rip van Winkle", H. G. Wells's The Sleeper Awakes. It also might have an influence on the motif of the "King asleep in mountain". Mark Twain did a burlesque of the story of the Seven Sleepers in Chapter 13 of Volume 2 of The Innocents Abroad.


=== Contemporary ===
Serbian writer Danilo Kiš retells the story of the Seven Sleepers in a short story, "The Legend of the Sleepers", in his book The Encyclopedia of the Dead. Italian author Andrea Camilleri incorporates the story in his novel The Terracotta Dog.
The Seven Sleepers appear in two books of Susan Cooper's The Dark Is Rising series; Will Stanton awakens them in The Grey King, and in Silver on the Tree they ride in the last battle against the Dark.
The Seven Sleepers series by Gilbert Morris takes a modern approach to the story, in which seven teenagers must be awakened to fight evil in a post-nuclear-apocalypse world.
John Buchan refers to the Seven Sleepers in The Three Hostages, where Richard Hannay surmises that his wife Mary, who is a sound sleeper, is descended from one of the seven who has married one of the Foolish Virgins.
The Persian–Dutch writer Kader Abdolah gives his own interpretation of the Islamic version of the story in the 2000 book Spijkerschrift.Turkish author Orhan Pamuk alludes to the story, specifically from the perspective of the dog, in the chapter "I am a Dog" in his 2001 novel My Name Is Red.


== Linguistic derivatives ==
In Welsh, a late riser may be referred to as a saith cysgadur—seven sleeper—as in the 1885 novel Rhys Lewis by Daniel Owen, where the protagonist is referred to as such in chapter 37, p. 294 (Hughes a'i Fab, Caerdydd, 1948).
In Scandinavian Languages, a late riser may be referred to as a sjusovare/syvsover—seven sleeper.The same is true in Hungarian: hétalvó, literally a "seven-sleeper", or one who sleeps for an entire week, is a colloquial reference to a person who oversleeps or who is typically drowsy.In Irish, “Na seacht gcodlatáin” refers to hibernating animals.


== See also ==
King asleep in mountain
The Men of Angelos
Rip Van Winkle
Seven Sleepers' Day
The Three Sleepers: characters in the C. S. Lewis children's novel The Voyage of the Dawn Treader


== References ==


== External links ==
Quran–Authorized English Version The Cave- Sura 18 – Quran – Authorized English Version
"SS. Maximian, Malchus, Martinian, Dionysius, John, Serapion, and Constantine, Martyrs", Butler's Lives of the Saints
(in English) Text containing the Seven Sleepers' commemoration as part of the Office of Prime.
Sura al-Kahf at Wikisource
(in English) Photos of the excavated site of the Seven Sleepers cult.
(in English) The Grotto of the Seven Sleepers, Ephesus
(in English) Mardan-e-Anjelos is a historical reenactment of the story of Ashaab-e-Kahf (also known as "The Companions of the Cave")
Link to 3D stereoview image for cross-eyed free viewing technique of Seven Sleepers near Ephesus – Turkey 
Gregory of Tours, The Patient Impassioned Suffering of the Seven Sleepers of Ephesus translated by Michael Valerie
The Lives of the Seven Sleepers from The Golden Legend by Jacobus de Varagine, William Caxton Middle English translation.
The Seven Sleepers of Ephesus by Chardri, translated into English by Tony Devaney Morinelli: Medieval Sourcebook. fordham.edu