Fast Company is a monthly American business magazine published in print and online that focuses on technology, business, and design. It publishes eight print issues per year.


== History ==
Fast Company was launched in November 1995 by Alan Webber and Bill Taylor, two former Harvard Business Review editors, and publisher Mortimer Zuckerman.The publication's early competitors included Red Herring, Business 2.0 and The Industry Standard.In 1997, Fast Company created an online social network, the "Company of Friends" which spawned a number of groups that began meeting. At one point the Company of Friends had over 40,000 members in 120 cities, although by 2003 that number had declined to 8,000.In 2000, Zuckerman sold Fast Company to Gruner + Jahr, majority owned by media giant Bertelsmann, for $550 million. Just as the sale was completed, the collapse of the dot-com bubble burst, leading to significant losses and a decline in circulation. Webber and Taylor left the magazine two years later in 2002, and John A. Byrne, previously a senior writer and former management editor with BusinessWeek, was brought in as the new editor. Under Byrne, the magazine won its first Gerald Loeb Award, the most prestigious honor in business journalism. But the magazine could not reverse its financial decline in the wake of the dot-com bust. Although the magazine was not specifically about Internet commerce, advertising pages continued to drop until they were one-third the 2000 numbers.In 2005, Gruner + Jahr put the magazine, as well as Inc. magazine, up for sale. Through a contact, Byrne contacted entrepreneur Joe Mansueto and helped guide him through the sale. A bidding war ultimately ensued, pitting The Economist against Mansueto's company Mansueto Ventures. Mansueto, the only bidder who promised to keep Fast Company alive, ultimately won the contest, buying both magazine titles for $35 million.Under former editor-in-chief Robert Safian, Fast Company was named by the American Society of Magazine Editors as the magazine of the year in 2014.Stephanie Mehta was named editor-in-chief in February 2018, having previously worked at Vanity Fair, Bloomberg, Fortune, and The Wall Street Journal. Fast Company is owned by Mansueto Ventures and is headquartered in New York, New York.


== Website ==
Launched in 1995, FastCompany.com covers leadership and innovation in business, environmental and social issues, entertainment and marketing, and, through its Co.Design site, the intersection of business and design, from architecture to electronics, consumer products to fashion. Fast Company also previously operated sites called Co.Labs, Co.Exist, and Co.Create. Co.Exist and Co.Create were rebranded as Ideas and Entertainment sections in 2017. Co.Labs was shut down in early 2015.


== Current activity ==


=== Franchises ===
Fast Company currently operates a number of franchises such as "Most Innovative Companies", "World Changing Ideas", "Innovation By Design", and "Most Creative People". For their Most Innovative Companies feature, Fast Company assesses thousands of businesses to create a list of 50 companies it considers the most innovative. The Most Creative People in Business is a list of 100 people from different industries.


== References ==


== External links ==
Official website