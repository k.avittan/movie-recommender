His First Flame is a 1927 American silent comedy film starring Harry Langdon and directed by Harry Edwards. Additional cast members include  Natalie Kingston, Ruth Hiatt, Vernon Dent, and others.


== Plot ==
The story tells of Harry Howells (Langdon), a recent college graduate who's madly in love with his sweetheart Ethel (Kingston) and hopes to marry her.  His woman hating uncle, however, Fire chief Amos McCarthy (Dent), tells his nephew to avoid marriage because all women want is money.
Even though Harry is determined to marry Ethel, it seems his uncle was right: Ethel is a gold-digger.  Harry is crestfallen.  Her sister, Mary Morgan (Hiatt), however, is very interested in Harry. Still, unhappy, Harry spends the night in the firehouse.  That night the fire alarm goes off, and it gives hapless Harry a chance to prove his mettle.


== Cast ==
Harry Langdon as Harry Howells
Natalie Kingston as Ethel Morgan
Ruth Hiatt as Mary Morgan
Vernon Dent as Amos McCarthy
Bud Jamison as Hector Benedict
Dot Farley as Mrs. Benedict
William McCall as President of the College
Irving Bacon as Man Who Jumps Out of Window (uncredited)
Margaret Cloud as Minor Role (uncredited)
Evelyn Francisco as Minor Role (uncredited)
Christian J. Frank as Minor Role (uncredited)
Thelma Hill as Girl Who Jumps Into the Hoop Net (uncredited)
Thelma Parr as Girl Who Drops Handkerchief Outside Firehouse (uncredited)
Elsie Tarron as Minor Role (uncredited)


== Critical reception ==
When the film was released, the film critic for The New York Times, Mordaunt Hall, liked the film and wrote, "Mr. Sennett and Mr. Langdon do their parts in this nice mile of fun. Mr. Sennett, who failed as a blacksmith and amassed millions as a maker of film humor, deserves no little credit for the hilarious situations in this picture. And Mr. Langdon is to be congratulated on a generous supply of sad smiles and wide-eyed effects...Mr. Langdon is at his best in this humorous piece of work. The heroine is impersonated by Natalie Kingston, who, while she does not appreciate Harry's wisdom, is attractive."In a review of Langdon's collective work, critic Michael Barrett discussed the film and wrote, "His First Flame was Langdon’s first feature, made for Sennett but not released until 1927 to cash in on his First National hits. Its central feature is a romantic triangle between rich idiot Langdon, his gold-digging fiancee Kingston, and his woman-hating uncle Dent. In a segment of very dark humor on the joys of domestic violence, Harry witnesses two simultaneous donnybrooks in neighboring houses, one in the foreground and the other in deep focus in the background. This is one of the set’s clearest examples of Langdon’s tendency to exploit humiliation and unease, an area of comedy that crosses into an audience’s discomfort zone. Among these early works, this daring sequence is perhaps the clearest application of Agee's warning (or celebration) about Langdon’s strange territory, though Agee was probably thinking most of the homicidal feature Long Pants."


== References ==


== External links ==
His First Flame on IMDb
His First Flame is available for free download at the Internet Archive
His First Flame at AllMovie
His First Flame at the TCM Movie Database
His First Flame film clip on YouTube