Amabel Ethelreid Normand (November 10, 1892 – February 23, 1930) was an American silent-film actress, screenwriter, director, and producer. She was a popular star and collaborator of Mack Sennett in his Keystone Studios films, and at the height of her career in the late 1910s and early 1920s had her own movie studio and production company. Onscreen, she appeared in 12 successful films with Charlie Chaplin and 17 with Roscoe "Fatty" Arbuckle, sometimes writing and directing (or co-writing/directing) movies featuring Chaplin as her leading man.Throughout the 1920s, her name was linked with widely publicized scandals, including the 1922 murder of William Desmond Taylor and the 1924 shooting of Courtland S. Dines, who was shot by Normand's chauffeur using her pistol. She was a suspect in the first crime for a short period, but disregarded from the latter. Her film career declined, and she suffered a recurrence of tuberculosis in 1923, which led to a decline in her health, retirement from films, and her death in 1930 at age 37.


== Early life and career ==
Born Amabel Ethelreid Normand in New Brighton, Richmond County, New York (before it was incorporated into New York City), she grew up in a working-class family. Her mother, Mary "Minne" Drury, of Providence, Rhode Island, was of Irish heritage, while her father was French Canadian. Her father, Clodman "Claude" Normand, was employed as a cabinetmaker and stage carpenter at Sailors' Snug Harbor home for elderly seamen. She had 5 siblings.Before she entered films at age 16 in 1909, Normand worked as an artist's model, which included posing for postcards illustrated by Charles Dana Gibson, creator of the Gibson Girl image, as well as for Butterick's clothing pattern manufacturers in lower Manhattan.For a short time, she worked for Vitagraph Studios in New York City for $25 per week, but Vitagraph founder Albert E. Smith admitted she was one of several actresses about whom he made a mistake in estimating their "potential for future stardom."

Her intensely beguiling lead performance, directed by D. W. Griffith in the dramatic 1911 short film Her Awakening, drew attention and she met director Mack Sennett while at Griffith's Biograph Company. She embarked on a topsy-turvy relationship with him; he later brought her across to California when he founded Keystone Studios in 1912. Her earlier Keystone films portrayed her as a bathing beauty, but Normand quickly demonstrated a flair for comedy and became a major star of Sennett's short films.Normand appeared with Charles Chaplin and Roscoe "Fatty" Arbuckle in many short films. She is credited as being the first film star to receive a pie thrown in the face.

She played a key role in starting Chaplin's film career and acted as his leading lady and mentor in a string of films in 1914, sometimes co-writing and directing or co-directing films with him. Chaplin had considerable initial difficulty adjusting to the demands of film acting, and his performance suffered for it. After his first film appearance in Making a Living, Sennett felt he had made a costly mistake.

Most historians agree  Normand persuaded Sennett to give Chaplin another chance, and she and Chaplin appeared together in a dozen subsequent films, almost always as a couple in the lead roles. In 1914, she starred with Marie Dressler and Chaplin in Tillie's Punctured Romance, the first feature-length comedy. Earlier that same year, in January/February, Chaplin first played his Tramp character in Mabel's Strange Predicament, although it wound up being the second Tramp film released; Chaplin offered a detailed account of his experience on the film in his autobiography. Normand directed Chaplin and herself in the film.

She opened her own company in partnership with Mack Sennett 1916. It was based in Culver City and was a subsidiary of the Triangle Film Corporation. She lost the company in 1918 when Triangle experienced a massive shake up which also had Sennett lose Keystone and establish his own independent studio. In 1918, as her relationship with Sennett came to an end, Normand signed a $3,500-per-week contract with Samuel Goldwyn. Around that same time, Normand allegedly had a miscarriage with Goldwyn's child.


== Scandals ==


=== The Roscoe Arbuckle trials ===
Normand's co-star in many films, Roscoe Arbuckle, was the defendant in three widely publicized trials for manslaughter in the 1921 death of actress Virginia Rappe. Although Arbuckle was acquitted, the scandal destroyed his career, and his films were banned from exhibition for a short time. Since she had made some of her best works with him, much of Normand's output was withheld from the public as a result. Arbuckle later returned to the screen as a director and actor but didn't attain his previous popularity despite being innocent and exonerated in court.


=== Taylor's murder ===

Director William Desmond Taylor shared her interest in books, and the two formed a close relationship. Author Robert Giroux claims that Taylor was deeply in love with Normand, who had originally approached him for help in curing her alleged cocaine dependency. According to Normand's subsequent statements to investigators, her repeated relapses were devastating for Taylor.Giroux says that Taylor met with federal prosecutors shortly before his death and offered to assist them in filing charges against Normand's cocaine suppliers. Giroux expresses a belief that Normand's suppliers learned of this meeting and hired a contract killer to murder the director. According to Giroux, Normand suspected the reasons for Taylor's murder, but did not know the identity of the man who killed him.According to Kevin Brownlow and John Kobal in their book Hollywood: The Pioneers, the idea that Taylor was murdered by drug dealers was invented by the studio for publicity purposes.On the night of his murder, February 1, 1922, Normand left Taylor's bungalow at 7:45 pm in a happy mood, carrying a book he had lent her. They blew kisses to each other as her limousine drove away. Normand was the last person known to have seen Taylor alive. The Los Angeles Police Department subjected Normand to a grueling interrogation, but ruled her out as a suspect. Most subsequent writers have done the same. However, Normand's career had already slowed, and her reputation was tarnished. According to George Hopkins, who sat next to her at Taylor's funeral, Normand wept inconsolably.


=== The Dines shooting ===
In 1924, Normand's chauffeur Joe Kelly shot and wounded millionaire oil broker and amateur golfer Courtland S. Dines with her pistol. In response, several theaters pulled Normand's films, and her films were banned in Ohio by the state film censorship board.


== Later career and death ==

Normand continued making films and was signed by Hal Roach Studios in 1926 after discussions with director/producer F. Richard Jones, who had directed her at Keystone. At Roach, she made the films Raggedy Rose, The Nickel-Hopper, and One Hour Married (her last film), all co-written by Stan Laurel, and was directed by Leo McCarey in Should Men Walk Home? The films were released with extensive publicity support from the Hollywood community, including her friend Mary Pickford.

In 1926, she married actor Lew Cody, with whom she had appeared in Mickey in 1918. They lived separately in nearby houses in Beverly Hills. However, Normand's health was in decline due to tuberculosis. After an extended stay in Pottenger Sanitorium, she died from tuberculosis on February 23, 1930 in Monrovia, California at the age of 37. She was interred as Mabel Normand-Cody at Calvary Cemetery, Los Angeles.


== Legacy ==

Mabel Normand has a star on the Hollywood Walk of Fame for her contributions to motion pictures at 6821 Hollywood Boulevard.
Her film Mabel's Blunder (1914) was added to the National Film Registry in December 2009.In June 2010, the New Zealand Film Archive reported the discovery of a print of Normand's film Won in a Closet (exhibited in New Zealand under its alternate title Won in a Cupboard), a short comedy previously believed lost. This film is a significant discovery, as Normand directed the movie and starred in the lead role, displaying her talents on both sides of the camera.


== Cultural references ==

A nod to Normand's celebrity in early Hollywood came through the name of a leading character in the 1950 film Sunset Boulevard, "Norma Desmond", which has been cited as a combination of the names Norma Talmadge and William Desmond Taylor. The film also frequently mentions Normand by name.
The 1974 Broadway musical Mack & Mabel (Michael Stewart and Jerry Herman) fictionalized the romance between Normand and Mack Sennett. Normand was played by Bernadette Peters and Robert Preston portrayed Mack Sennett.
"Hello Mabel" is a song by the Bonzo Dog Doo-Dah Band released in England on their second album The Doughnut in Granny's Greenhouse (released as Urban Spaceman in the US.) in November 1968.
Normand is mentioned during series 2 episode 1 of Downton Abbey by ambitious housemaid Ethel Parks. Daisy Mason (née Robinson), the kitchen maid, inquires what she is reading and Ethel responds, "Photoplay about Mabel Normand. She was nothing when she started, you know. Her father was a carpenter and they'd no money, and now she's a shining film star."
Singer/songwriter Stevie Nicks wrote a song about the actress entitled "Mabel Normand", which appears on her 2014 album, 24 Karat Gold: Songs from the Vault.


=== Fictional portrayals ===
Normand is played by actress Marisa Tomei in the 1992 film Chaplin opposite Robert Downey, Jr. as Charles Chaplin; by Penelope Lagos in the first biopic about Normand's life, a 35-minute dramatic short film entitled Madcap Mabel (2010); and by Morganne Picard in the motion picture Return to Babylon (2013).
In 2014, Normand was played on television by Andrea Deck in series 2, episode 8 of Mr Selfridge and by Kristina Thompson in the short film Mabel's Dressing Room.The character played by Alice Faye in Hollywood Cavalcade (1938) was reputed to have been based partly on Normand.


== Filmography ==
Some of her early roles are credited as "Mabel Fortesque".


=== Short films ===


=== Feature films ===


== References ==
Notes


== Further reading ==
Basinger, Jeanine (2000). Silent Stars. Wesleyan University Press. ISBN 978-0-8195-6451-1. Retrieved July 29, 2010.CS1 maint: ref=harv (link)
Harper Fussell, Betty (1992). Mabel: Hollywood's First I-Don't-Care Girl (Illustrated ed.). Limelight Editions. ISBN 978-0-87910-158-9. Retrieved July 29, 2010.CS1 maint: ref=harv (link)
Sherman, William Thomas (2006). Mabel Normand: A Source Book to Her Life and Films
Normand, Stephen (1974). Films in Review September Issue: Mabel Normand - A Grand Nephew's Memoir
Lefler, Timothy Dean (2016). Mabel Normand: The Life and Career of a Hollywood Madcap. ISBN 978-0-7864-7867-5


== External links ==
Mabel Normand on IMDb
Mabel Normand at the TCM Movie Database 
Mabel Normand at Find a Grave
Mabel Normand at the Women Film Pioneers Project
Madcap Mabel: Mabel Normand Website
Mabel Normand Source Book (pdf file)
Stephen Normand's website
Bibliography
Looking for Mabel Normand
Mabel Normand Home Page
Films of Mabel Normand on YouTube (playlist)