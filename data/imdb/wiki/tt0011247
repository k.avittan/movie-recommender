The Great Shadow, also known as The Great Shadow and other Napoleonic Tales, is an action and adventure novel written by Sir Arthur Conan Doyle and published in 1892 in J.W. Arrowsmith’s Bristol Library. The novel takes place in the Napoleonic era on the English–Scottish border city called West Inch. The Great Shadow refers to the Napoleon’s influence and his reputation that forms a shadow over West Inch.


== Plot summary ==
Instead of Sherlock Holmes being the main character, the story follows Jack Colder, who claims his only notable childhood experience was when he accidentally prevented a burglary during an attempt to escape a boarding school. This event caught Jim Horscroft and the two become friends. Once Jim goes off to medical school, Jack reunites with his cousin Edie, who found herself in a lot of money due to her father’s death. Jack takes a liking to Edie, but is deterred when Edie shows less enthusiasm and shows great attraction to men in battle. Upon hearing this, Jack insists that he will become a soldier despite both of his parents’ disapproval. Jack ultimately asks Edie to marry him.
It is at this time that Jim returns to West Inch, and he quickly takes a liking to Edie, who seems much more attracted to Jim. When Jack reveals to Jim that the two are engaged he is quickly off put and sinks into depression combined with drunkenness. After a couple days, Jim recovers and is caught embracing Edie by Jack. The two argue and ultimately decide to have Edie choose. Edie chooses Jim and the two become engaged.
The arrival of Lapp, a mystery French man who arrives on a small ship interrupts the peace as he claimed to have been in a ship wreck and was traveling for three days lost at sea. Jack offers him food and a place to stay while Jim is a little more hesitant. The boys quickly realize Lapp is very rich and has many battle scores that are only outmatched by his endless war stories that charm everyone, including Edie. Lapp claims he is to stay there until he is needed. Lapp becomes a regular in the community, Jack suspects Lapp is a spy after he catches him sneaking around on multiple occasions. When Jim goes off to finish school and get his diploma, Edie reveals to Jack that she had married Lapp, and the next day Lapp leaves on a ship and reveals in a note that he actually is Bonaventure De Lissac who is Napolon’s aide. This angers Jim greatly as he learns Napoleon has escaped and is on the move. Jim offers his services to Major Elliot, and which Jack joins them quickly. 
Major Elliot trains the boys as they prepare for the Battle of Waterloo. The French are described as having significant arms and were trained soldiers as opposed to the regiment Jack and Jim were a part of. The rest of the novel describes in vivid detail a soldier’s account of the Battle of Waterloo, where Jim and Jack walk from  half-a-mile away as the battle begins, before joining the battle at the very end. The ending of the book describes the French being defeated by the Duke of Wellington and Gebhard von Blucher.


== Historical background ==
The Battle of Waterloo took place on 18 June 1815 near Waterloo, which is now in Belgium. After a long reign as the French emperor, in which he conquered many regions, Napoleon Bonaparte was banished, and ultimately returned in 1815. Those who opposed him formed the Seventh Coalition and ultimately defeated Napoleon.
Napoleon was a popular influence on many writers towards the end of the 19th century. Conan Doyle also developed a series of short stories entitled Brigadier Gerard. The series featured Etienne Gerard, a Hussar in the French Army during the Napoleonic Wars. Other writings by Conan Doyle involving the Napoleonic Era include Adventures of Gerard and Uncle Bernac.


== Sir Arthur Conan Doyle ==
Sir Arthur Conan Doyle was born on 22 May 1859 in Edinburgh, Scotland. Conan Doyle credit his mother, Mary Doyle, who had a passion for books and he describes her as a master storyteller. He says in his biography that,""In my early childhood, as far as I can remember anything at all, the vivid stories she would tell me stand out so clearly that they obscure the real facts of my life."The book begins with Jack Colder recounting his life in a Jesuit boarding school, where he ends up meeting his best friend Jim. The novel has a pretty grim outlook on the boarding school with most of the attendees featuring almost and endless supply of money as well as a sense of entitlement. Conan Doyle also attended a Jesuit boarding school in England for seven years and was very critical of it.  Conan Doyle pursued an education in the medical field,where he began writing short stories.
Conan Doyle, who is best known for his novels detailing the life of Victorian detective Sherlock Holmes, also has written numerous short stories, nonfiction works, historical novels, short stories, poetry and one operetta.
Conan Doyle published The Sign of Four in 1890, which established Sherlock Holmes as a prominent character and did wonders in putting Conan Doyle on the map literature map in the United Kingdom and United States. However, Conan Doyle is said to have balanced writing historical novels with novels featuring Sherlock Holmes in the early 1890s.In the year Conan Doyle published The Great Shadow, His wife, Louisa, gave birth to a son named Kingsley. Conan Doyle calls this "the chief event' of their life.  
The following year Conan Doyle infamously decided to kill off Sherlock Holmes.


== Character list ==


=== Jack Colder ===
Jack is the narrator and main protagonist in the novel. Jack explains his really only significant memory as a child was when he accidentally stopped robbers from stealing money from the boarding school in which he attended, and was trying to leave that night. This accolade brought him a great amount of publicity at a school in which previously he felt like an outcast in. One person who started paying attention to him was his eventual best friend, Jim Horscroft. Jack returns to his hometown and immediately reaffirms his feelings for his cousin Edie. Edie was a very good childhood friend, and she recently received large inheritance after her parents died, and was sent to live with Jack's family. Edie and Jack strike up a relationship, but Jack gives clues that Edie isn't particularly interested in him claiming she likes him some days and ignores him other days. Things between Jack and Jim get tense when Jim returns from medical school and immediately takes a liking to Edie, who is much more affectionate towards Jim. Despite claiming that he will marry Edie, Jack catches Jim and Edie together, and ultimately Edie chooses Jim. Despite these breaking of trust, Jim and Jack are able to become best friends again. Jack is a very trusting character, almost to a fault as he has no issue offering up his home to Lapp without his parent's permission. Jack looks up to Lapp and is envious of the number of stories Lapp possesses as well as his unmatched charm. Jack is again betrayed when Lapp leaves with Edie, and in this case is not willing to patch things up, but rather joins Jim immediately upon his announcement of enlisting. Jack always has a sense of trying to be a real man, which usually can be linked to trying to impress Edie, and shows a strong sense of loyalty to Jim.


=== Jim Horscroft ===
Jim befriends Jack after the burglary incident at the boarding school. Jim is bigger and stronger than Jack, and is a lot less trusting than Jack. Upon arriving back from medical school, Jim immediately exhibits feelings for Edie, which she reciprocates towards him. Jim is apologetic towards Jim when he and Edie are caught together, but along with Jack he is rational in asking Edie to choose between the two of them, and ultimately Edie chooses Jim. Jim's lack of trust is apparent in his lacking of warming up to Lapp upon his arrival. Jim is the character who is initially the most immune to Lapp's charm, and is the most upfront in him questioning how he arrived, and the validity of the multiple stories Lapp tells. However, he like everyone else in West Inch eventually warms up to Lapp, and feels very betrayed by both Edie and Lapp when they elope together, although he blames Lapp significantly more than Edie. Out of rage Jim decides to enlist in the war, and throughout the training and by the viewing of the actual war, Jim remains the most calm among the anxious soldiers.


=== Edie ===
Edie is Jack's cousin and ultimately ends up living with Jack's family after the death of her parents. Edie is very obvious in displaying her liking for what she calls "real men", which she never really explains what that is to her, but she does provide ship captains, sailors, and soldiers as examples for what she thinks as real men. She doesn't really care for Jack and finds herself bored with him frequently despite his obvious feelings for her. She immediately switches her allegiance to Jim upon his arrival, and displays the same intentions of switching her feelings from Jim to Lapp upon Lapp's arrival, although she doesn't reveal her intentions until she tells Jack that she has married Lapp.


=== Bonaventure de Lissac (Lapp) ===
Lapp is seen as a great unknown throughout the book as he arrives on the island in an abandoned ship. When asked where he is from, he simply says he is from everywhere, because he has been everywhere. Lapp is incredibly charming, and incredibly rich, which is why everyone in West Inch is so fascinated by him. Lapp later explains in a letter to Jack after eloping with Edie that his name is Bonaventure de Lissac, and he is actually one of Napoleon's right hand men, and he has been writing to Napoleon for a while in order to gain information.


=== Major Elliot ===
Major Elliot is a former member of the English army and is currently stationed in West Inch. Like everyone else in West Inch, Major Elliot is deceived by Lapp, and in once case when Jack catches Lapp walking around, Lapp uses Major Elliot to reassure Jack that he isn't up to no good. Upon Jim and Jack's decision to enlist as a soldier, Major Elliot is sent to train him as well as other recent enlisted soldiers. Major Elliot does his best to train the men and help them ease their nerves during the beginning of the war.


== Themes ==


=== Love triangle ===
The Jack makes numerous claims that Jim is his best, and frankly one of his only friends. However, it is clear from the beginning that Edie is going to infuse herself into the friendship, when she makes it glaringly obvious she is interested in Jim more than Jack. However this love triangle produces surprisingly small amount of conflict as the two boys eventually force Edie to choose, and upon her choosing of Jim, Jack respectfully steps down.


=== Male camaraderie ===
The bond between Jim and Jack is obvious from the beginning of the novel. Jack’s narration is always very complimentary towards Jim, and his admiration of Jim is apparent. The friendship stands its fair share of conflict when Edie picks Jim over Jack, when Jim initially distrusts Lapp, and finally when Lapp leaves with Edie. Ultimately their friendship is put to the test when Jim decides to enlist in the army, which Jack immediately joins him in doing. This act is common in adventure novels in a sense of male comrades signing up for an adventure, and the other one joining. While this does occur later in the novel than customary, the sense of male camaraderie is very prevalent.


=== Heroism ===
The idea of heroism is certainly muddled in this novel. Heroism has a more classic definition of a strong and powerful warrior. This is backed up by Edie’s initial attraction to the soldiers who operate ships that enter their harbor. This leads to Jack wanting to join the army. Jim is the ideal candidate when it comes to heroism because he has a strong body and displays a certain amount of power, and this is reflected by Edie’s attraction. This all changes with the arrival of Lapp, whose many war stories immediately catch everyone’s attention, and ultimately Edie’s attention when the two get married and leave together. Finally this idea of heroism is shone in a different light during the Battle of Waterloo when it appears the French are initially the heroes with their grand experience and armor. However they were the defeated by the bravery and craftiness of the Duke of Wellington


=== Power of wealth ===
Lapp is yet another example of a theme considering his wealth goes a long way in West Inch. His deep pockets get the attention of Jack’s father who lets him stay in one of the rooms in exchange for a hefty rent. Lapp’s wealth as well as his charm allows him to gain the trust of basically everyone in West Inch. Edie is another example with her wealth allowing her to basically do whatever she wants, which is best exemplified in her changing her mind from Jack to Jim, and then from Jim to Lapp in regards to her affections.


== Critical reception ==
The novel received mixed reviews as far as content is concerned. A review published by The Anthneum acknowledges The Great Shadow as a “new vein” for Conan Doyle, but the specifics regarding parts of the Battle of Waterloo are questioned by the review, namely the specifics of Clinton’s 71st division and the questioning of how Jack and his comrades would be able to see some of the action considering they were half a mile away.  
Gilson Willets also acknowledges the significance of The Great Shadow when he combines it with Micah Clarke, The White Company and The Refugees as four of the great novels of recent years.


== References ==


== External links ==
 The Great Shadow public domain audiobook at LibriVox