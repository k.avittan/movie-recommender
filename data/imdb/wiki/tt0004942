A battle cry is a yell or chant taken up in battle, usually by members of the same combatant group.
Battle cries are not necessarily articulate (e.g. "Eulaliaaaa!", "Alala"..), although they often aim to invoke patriotic or religious sentiment. Their purpose is a combination of arousing aggression and esprit de corps on one's own side and causing intimidation on the hostile side. Battle cries are a universal form of display behaviour (i.e., threat display) aiming at competitive advantage, ideally by overstating one's own aggressive potential to a point where the enemy prefers to avoid confrontation altogether and opts to flee. In order to overstate one's potential for aggression, battle cries need to be as loud as possible, and have historically often been amplified by acoustic devices such as horns, drums, conches, carnyxes, bagpipes, bugles, etc. (see also martial music).
Battle cries are closely related to other behavioral patterns of human aggression, such as war dances and taunting, performed during the "warming up" phase preceding the escalation of physical violence. From the Middle Ages, many cries appeared on standards and were adopted as mottoes, an example being the motto "Dieu et mon droit" ("God and my right") of the English kings. It is said that this was Edward III's rallying cry during the Battle of Crécy. The word "slogan" originally derives from sluagh-gairm or sluagh-ghairm (sluagh = "people", "army", and gairm = "call", "proclamation"), the Scottish Gaelic word for "gathering-cry" and in times of war for "battle-cry". The Gaelic word was borrowed into English as slughorn, sluggorne, "slogum", and slogan.


== History ==


=== Antiquity ===
The war cry is an aspect of epic battle in Homer: in the Iliad, Diomedes is conventionally called "Diomedes of the loud war cry." Hellenes and Akkadians alike uttered the onomatopoeic cry "alala" in battle.
The troops of ancient Athens, during the Medic Wars and the Peloponnesian War were noted for going into battle shouting "Alala or Alale!", which was supposed to emulate the cry of the owl, the bird of their patron goddess Athena.
The Western Huns attacked with terrifying battle cries.
One of the common Hindu war cries was "Har Har Mahadev" meaning, "Mahadev conquers away!"
A war cry used in Kerala throughout history is, "Adi Kollu," meaning, "Strike and Kill!" This war cry was later adopted by the Madras Regiment of the Indian Army.
A common war cry used in Tamil Nadu was "Vetrivel, Veeravel," meaning, "Victorious Vel, Courageous Vel." Vel is the holy spear of Murugan, the Hindu war deity.


=== Middle Ages ===
Each Turkic tribe and tribal union had its distinct tamga (seal), totemic ongon bird, and distinct uran (battle cry) (hence the Slavic urah "battle cry"). While tamgas and ongons could be distinct down to individuals, the hue of horses and uran battle cries belonged to each tribe, were passed down from generation to generation, and some modern battle cries were recorded in antiquity. On split of the tribe, their unique distinction passed to a new political entity, endowing different modern states with the same uran battle cries of the split tribes, for example Kipchak battle cry among Kazakhs, Kirgizes, Turkmens, and Uzbeks. Some larger tribes' uran battle cries:
Kipchak – "ay-bas" ("lunar head").
Kangly (Kangars) – "bai-terek" ("sacred tree").
Oguzes – "teke" ("mount")
Desperta ferro! (in catalan meaning "Awake iron!") was the most characteristic cry of the Almogavar warriors, during the Reconquista.
Deus vult! ("God wills it!" in Latin) was the battle cry of the Crusaders.
Montjoie Saint Denis!: battle cry of the Kings of France since the 12th century.
Santiago y cierra, España! was a war cry of Spanish troops during the Reconquista, and of the Spanish Empire.
On 14 August 1431, the whole Holy Roman Empire army (of the 4th anti-Hussite crusade) was defeated by the Hussites in the Battle of Domažlice. Attacking imperial units started to retreat after hearing Ktož jsú boží bojovníci ("Ye Who Are Warriors of God") choral and were annihilated shortly after.
Allāhu akbar (الله أكبر, "God is [the] greatest") and Allāhu allāh (الله الله,"God! God!") were used by Muslim armies throughout history. Al-naṣr aw al-shahāda (النصر أو الشهادة, "Victory or martyrdom") was also a common battle cry; the At-Tawbah 9:52 says that God has promised to the righteous Muslim warrior one of these two glorious ideals.
Óðinn á yðr alla (Odin owns you all) - A reference to Odin's self sacrifice at Yggdrasil. Attributed to Eric the Victorious.


=== Pre-modern ===
When putting out peasants' rebellions in Germany and Scandinavia around 1500, such as in the Battle of Hemmingstedt, the Dutch mercenaries of the Black Guard yelled Wahr di buer, die garde kumt ("Beware, peasants, the guards are coming").  When the peasants counterattacked, they responded with Wahr di, Garr, de Buer de kumt ("Beware, Guard, of the farmer, [who is] coming").
The Spanish cried Santiago ("Saint James") both when reconquering Spain from the Moors and during conquest in early colonial America.
King Henry IV of France (13 December 1553 – 14 May 1610), a pleasure-loving and cynical military leader, famed for wearing a striking white plume in his helmet and for his war cry:  Ralliez-vous à mon panache blanc! ("Follow my white plume!").
The Sikh battle cry or jaikara Bole So Nihal...Sat Sri Akaal ("Shout Aloud in Ecstasy... True is the Great Timeless One") popularized by Guru Gobind Singh.
The Gurkha (Gorkha) soldiers' battle cry was, and still is, Jai Mahakali, Ayo Gorkhali! ("Victory to Goddess Mahakali, the Gurkhas are coming!")
The rebel yell was a battle cry used by Confederate soldiers during the American Civil War.
Finnish troops in the Swedish army in the 17th and 18th centuries, would use the battle cry Hakkaa päälle! ("Cut them down!" in Finnish), lending them the name Hackapell.
Irish Regiments of various Armies used and continue to use Gaelic war cries, Faugh a Ballagh ("Clear the way!") or Erin go Bragh (Ireland Forever)
The Swedish army in the 18th and 19th century would be issued with the command to attack with "För Fäderneslandet, gå på, Hurra!" (For the Fatherland, onwards, Hurrah!)
Argentine general José de San Martín is known in South America for his war cry: Seamos libres, que lo demás no importa nada! (Let's be free, nothing else matters!).
In the Texas Revolution, following the Battle of Goliad and the Battle of the Alamo, Texan soldiers would use the battle cry "Remember Goliad! Remember the Alamo!"
In the Battle of Dybbøl in 1864, both Danish and German forces used "Hurrah" as a war cry.
During World War One in the Italian Front of 1915. Before battle, Italian Soldiers would yell "Savoia" or "Avanti Savoia", which is "Come on Savoy!" or "Onwards Savoy!" in Italian (compare "For the king!" among British soldiers of the same era).


=== Modern ===
During World War II, Tennōheika Banzai (天皇陛下万歳, May the Emperor live for ten thousand years) served as a battle cry of sorts for Japanese soldiers, particularly in a "banzai charge". The most popular battle cry is "Ei ei oh" (エイエイオー), which is usually used at the start of battle.
Hooah is the war cry of the United States Army and Oorah is a war cry used by United States Marine Corps. The Slavic version, "Ura!" has been used by the Imperial Russian Army, the Red Army and is still used by the Russian Ground Forces, alongside many more Eastern European armed forces. It was also used by Yugoslav Partisans as Juriš/Јуриш.
During the Korean War, the Korean People's Army used the phrase "김일성 수령님 만세" (transliterated gim-ilseong sulyeongnim manse, translated as "Long live the Great Leader Kim Il-sung")
"Merdeka atau mati!" (English: Freedom or death!) Used by Indonesian national army and freedom fighters (Indonesian: Pedjoeang/pejuang) during Indonesian war of independence.
"Avanti Savoia!" (English: Go Savoia!) was the patriotic battle cry of the Italian Royal Army during World War one. Infantrymen would scream this motto when launching an attack against Austro-Hungarians.
In Afghanistan, Norwegian troops of the Telemark Battalion would sometimes use the battle cry "Til Valhall!" (To Valhalla!) followed by Oorah.
Muslim Mujahideen and soldiers in the Middle East and North Africa shout 'Allahu Akbar' (God is the greatest).
During the Greco-Italian war (in WWII) the Greeks would shout "Αέρα!" (wind) as their battle cry.
During Bangladesh Liberation War the Mukti Bahini would shout Joy Bangla (Victory to Bengal / Long live Bengal) during their battles against the Pakistani military.
During the Indochina War in Vietnam, the Viet Minh soldiers usually used "Xung phong" (English: Charge) whenever attacking the enemy. The same battle cry would be used later by PAVN and NLF forces during the Vietnam War.


== See also ==


== References ==
Guilhem Pepin, ‘Les cris de guerre " Guyenne ! " et " Saint George ! ". L’expression d’une identité politique du duché d’Aquitaine anglo-gascon’, Le Moyen Age, cxii (2006) pp 263–81


== External links ==
Philip Rance, 'War Cry' in The Encyclopedia of the Roman Army – Roman battle cries
Ross Cowan, The Clashing of Weapons and Silent Advances in Roman Battles – battle cries and the drumming of weapons in Roman warfare