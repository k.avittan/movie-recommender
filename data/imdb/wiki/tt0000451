Mary Jane's Mishap; or, Don't Fool with the Paraffin is a 1903 British short  silent comedy film, directed by George Albert Smith, depicting disaster following when housemaid Mary Jane uses paraffin to light the kitchen stove. The trick film, according to Michael Brooke of BFI Screenonline, "is an example of Smith's interest in cinematic effects - including, here, the use of superimposition to suggest ghosts," which, "is notable for its then sophisticated mix of wide establishing shots and medium close-ups," and, "also contains two wipes to denote a change of scene."


== Plot ==
A housemaid (played by Smith's wife, Laura Bayley) starts a fire in the kitchen stove by putting paraffin on it. It causes an explosion that sends her up the chimney. She emerges from the chimney pot on top of the house and her scattered remains fall to the ground. Later, Mary Jane's ghost rises from her grave to find her paraffin can and once she finds it, she goes to her final resting place.


== Production ==
The plot of Mary Jane's Mishap was probably inspired by a 1901 Edison Manufacturing Company film, The Finish of Bridget McKeen, but is enlarged to include new material, including a vein of dark comedy typical of the Brighton School of filmmaking.Mary Jane's Mishap was filmed in Smith's studio at St. Ann's Well Gardens, Hove. Most film historians have described the film's production as taking place in 1903, although John Barnes instead considered a production date of August 1902 to be more plausible.


== Critical analysis ==
John Barnes has written that "this could be considered as the first modern film", describing it as "far ahead of its time as regards film technique".


== References ==
Citations
BibliographyBarnes, John (2004), ""Mary Jane's Mishap": An Early British Film Re-Examined", Film History, 16 (1): 54–59, JSTOR 3815559


== External links ==
Mary Jane's Mishap on IMDb