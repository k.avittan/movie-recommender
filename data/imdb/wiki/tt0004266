James Oliver "Jim" Curwood (June 12, 1878 – August 13, 1927) was an American action-adventure writer and conservationist. His books were often based on adventures set in the Hudson Bay area, the Yukon or Alaska and ranked among the top-ten best sellers in the United States in the early and mid 1920s, according to Publishers Weekly. At least one hundred and eighty motion pictures have been based on or directly inspired by his novels and short stories; one was produced in three versions from 1919 to 1953. At the time of his death, Curwood was the highest paid (per word) author in the world.

He built Curwood Castle as a place to greet guests and as a writing studio in his hometown of Owosso, Michigan. The castle was listed on the National Register of Historic Places and is now operated by the city as a museum. The city commemorates him with an annual Curwood Festival. 


== Biography and career ==

Curwood was born in Owosso, Michigan, the youngest of four children. Attending local schools, Curwood left high school before graduation. He passed the entrance exam to the University of Michigan and was allowed to enroll  in the English department, where he studied journalism. 
After two years, Curwood quit college to become a reporter, moving to Detroit for work. In 1898, he sold his first story while attending the University of Michigan. In 1907 he was hired by the Canadian government to travel to the northern reaches of Canada to write and publish accounts of his travels to encourage tourism, his trips in Canada inspired his wilderness adventure stories. For many years he traveled to the Hudson Bay area, the Yukon and Alaska for several months each year for more inspiration. He wrote and published twenty-eight adventure/nature novels, two collections of short stories, one non-fiction volume "The Great Lakes", a volume of introspection "God's Country - The Trail to Happiness, and an Autobiography "The Glory of Living".
By 1922, Curwood had become very wealthy from the success of his writing. After a tour of Europe with his family where he toured old European castles, he came home and built his own, Curwood Castle in Owosso, Michigan. Constructed in the style of an 18th-century French chateau, the castle is set on the Shiawassee River near downtown Owosso. In one of the castles two large turrets, Curwood set up his writing studio. He also owned a lodge on the Ausable River near Roscommon, Michigan that he used as a retreat for rest and relaxation from his rigorous writing career.

Curwood was an avid hunter in his youth; however, as he grew older, he became an advocate of conservation and environmentalism. He was appointed to the Michigan Conservation Commission in 1927. The change in his attitude toward wildlife is expressed in a quote from The Grizzly King: "The greatest thrill is not to kill but to let live."
Curwood's daughter, Carlotta Curwood Tate documented in an account in the Curwood Collector that in 1927, while on a fishing trip in Florida, Curwood was bitten or stung through hip waders by something, source unknown. Health problems related to the bite escalated over the next few months as an infection developed. He died in Owosso at the age of 49, and was interred in Oak Hill Cemetery there in a family plot.


== Literary and film legacy ==
Curwood's adventure writing followed in the tradition of Jack London. Curwood set many of his works in the wilds of the Great Northwest and often used animals as lead characters (Kazan; Baree, Son of Kazan, The Grizzly King, and Nomads of the North).  Many of Curwood's adventure novels also feature romance as primary or secondary plot consideration. This approach gave his work broad commercial appeal; his novels ranked on many best-seller lists in the early and mid 1920s. One of his most successful books was his 1919 novel, The River's End. The book sold more than 100,000 copies and was the fourth best-selling title of the year in the United States, according to Publishers Weekly.Curwood's short stories and other pieces were published in various literary and popular magazines throughout his career. His bibliography includes more than 200 such articles, short stories, and serializations. His work was also published in Canada and the United Kingdom. Some of his books were translated into French, Italian, Russian, Spanish, Dutch, Swedish, Finnish, Czech and Polish, and published in those respective countries.


=== Adaptations ===
Over one hundred and eighty movies have been based on or inspired by Curwood's novels and short stories. Curwood's story "Wapi the Walrus" was adapted for film three times. The first was as Back to God's Country (1919), starring Nell Shipman as a brave and adventurous woman in the wilds of the North. Another version by the same title was released in 1927, and again by this title in 1953.A young John Wayne and Noah Beery Jr. starred in the 1934 film The Trail Beyond, based on Curwood's novel The Wolf Hunters. Filmmakers produced a film series featuring Kirby Grant as Mountie Corporal Rod Webb, assisted by his dog Chinook; they made a total of ten films.
In the late 20th century, French director Jean-Jacques Annaud adapted Curwood's 1916 novel The Grizzly King as the film The Bear (1988). The film's success prompted a revival of interest in Curwood's books.


== Legacy and honors ==

His writing studio, Curwood Castle, which he commissioned in a French chateau style, is listed on the National Register of Historic Places. It is preserved and operated as a historic house museum.
The city of Owosso holds an annual Curwood Festival during the first full weekend in June, to commemorate him and celebrate the city's heritage.
A mountain in L'Anse Township, Michigan was named as Mount Curwood in his honor.
The L'Anse Township Park was renamed as Curwood Park.


== List of his works ==


== Filmography ==
Fighting Chance (1913), a Vitagraph film directed by Ralph Ince and starring Anita Stewart, Rosemary Thelby, Ned Finley, Courtenay Foote
Betty in the Lion's Den (1913), a Vitagraph film directed by Frederick A. Thomson and starring Clara Kimball Young, Darwin Karr, Josie Sadler and Etienne Giradot, from a scenario by Curwood.
Diamond Cut Diamond (1913), a Lubin Mfg. Co. film directed by L. B. Carlton starring Isabelle Lamon. Scenario by Curwood
Does Advertising Pay (1913), a Vitagraph film directed by Larry Trimble, starring Wally Van, from a scenario by Curwood
Duty and the Man (1913), a Reliance film directed by Oscar Apfel starring James Ashley, Gertrude Robinson, Charles Elliott, Irving Cummings and George Siegmann. Likely based Probably based on Philip Steele.
The Feudist (1913), a Vitagraph film directed by Wilfred North and starring John Bunny, Sidney Drew, Flora Finch, Lillian Walker, Wallie Van, Kenneth Casey, Josie Sadler and Paul Kelly. Scenario by Curwood
Fifth Man: The Wanderers Return (1914), a Selig film produced and directed by F. J. Grandon. Starring Bessie Eyton, Charles Clary, Lafayette McKee, Roy Watson and Charles Wheelock. Based upon a short story by Curwood.
Battle of Frenchman's Run (1914), a Vitagraph film directed by Theodore Marsten and starring Dorothy Kelly, George Cooper, Harry Carey, Albert Roccardi and Charles H. West, based on a screenplay by Theodore Marsten, from a script by Curwood.
Caryl of the Mountains (1914), a Hearst-Selig film directed by Thomas Santschi and starring Kathlyn Williams, Thomas Santschi, Harry Lonsdae and Roy Watson, from a scenario by Curwood
Children of Fate (1914), a Nestor film directed by Wallace Reid and starring Wallace Reid, Dorothy Davenport, Joe King, Phil Dunham, Frank Borzage and Billy Wolbert. Reissued as Love's Western Flight, from a script by Curwood.
The Dream Girl: an Ideal Realized (1914), a Selig film directed by Thomas Santschi starring Thomas Santschi, Bessie Eyton, Harry Lonsdale, Edith Johnson, Charles Wheelock and Lex Wilmouth. Scenario by Curwood.
The Eugenic Girl (1914), a Selig film directed and produced by Thomas Santschi. Starring Elsie Greeson, Franklyn Hall, Harry McCabe and Thomas Santschi. Scenario by Curwood
Fatal Note: Jealous of His Own Love Letter (1914), a Selig film directed and produced by E. A. Martin. Starring Adele Lane and Edwin Wallock. Scenario by Curwood. Also used as basis for Phantom Patrol in 1936
The Awakening (1915), Vitagraph. Directed by Ralph Ince, screenplay by Ralph Ince from a script by Curwood.
Beautiful Belinda (1915), Selig film directed by E. A. Martin and starring C. C. Holland, Lee Morris and Lillian Hayward, based on a scenario by Curwood.
Cats (1915) by Selig, directed by Norval MacGregor from a scenario by Curwood
The Coyote (1915) by Selig. Directed by Guy Oliver based on Curwood's short story of the same title.
The Destroyers (1916) by Vitagraph. Directed by Ralph Ince and starring Lucille Lee Stewart. Based on Curwood's short story “Peter God”
Fathers of Men (1916) starring Robert Edison. Scenario by Curwood
Fiddling Man (1917), starring Jane Grey. Based upon the short story “The Fiddling Man” in Back to God's Country
Baree, Son of Kazan (1918), Vitagraph film starring Nell Shipman and Albert Whitman.
Back to God's Country (1919), starring Nell Shipman. Based on the short story “Wapi, the Walrus".
The River's End (1920), directed by Marshall Neilan and Victor Heerman. Starring Lewis Stone, Marjorie Daw and Jane Novak
The Courage of Marge O'Doone (1920), a Vitagraph film directed by David Smith. Starring Pauline Starke, Niles Welch and Boris Karloff. Adapted from Curwood's book with the same title.
Nomads of the North (1920), directed by David Hartford. Starring Lon Chaney, Betty Blythe and Lewis Stone
Isobel or The Trail's End (1920), directed by Edwin Carewe. Starring House Peters and Jane Novak
God's Country and the Law (1921), directed by Sidney Olcott. Starring Gladys Leslie
The Golden Snare (1921), directed by David Hartford. Starring Lewis Stone, Ruth Renick and Wallace Beery
The Girl from Porcupine (1921), directed by Dell Henderson. Starring Faire Binney and William Collier Jr.
Kazan (1921), directed by Bertram Bracken. Starring Jane Novak
Flower of the North (1921), directed by David Smith. Starring Henry B. Walthall and Pauline Starke
Jan of the Big Snows (1922), directed by Charles M. Seay. Starring Warner Richmond and Louise Prussing
The Broken Silence (1922), a Pine Tree Pictures film, directed by Dell Henderson and starring Zena Keefe, Robert Elliott, J. Barney Sherry and Gypsy O'Brien.
The Man from Hell's River (1922), directed by Irving Cummings. Starring Irving Cummings, Wallace Beery and Rin Tin Tin
I Am the Law (1922), directed by Edwin Carewe. Starring Alice Lake, Kenneth Harlan and Wallace Beery. (Film disowned by James Oliver Curwood)
The Valley of Silent Men (1922), directed by Frank Borzage. Starring Alma Rubens
Jacqueline, or Blazing Barriers (1923), directed by Dell Henderson. Starring Marguerite Courtot
Gold Madness (1923), directed by Robert Thornby. Starring Guy Bates Post
The Alaskan (1924), directed by Herbert Brenon. Starring Thomas Meighan
The Hunted Woman (1925), directed by Jack Conway. Starring Seena Owen, Earl Schenck and Victor McLaglen
Baree, Son of Kazan (1925), directed by David Smith. Starring Anita Stewart
My Neighbor's Wife (1925), directed by Clarence Geldart. Starring E. K. Lincoln, Helen Ferguson and Herbert Rawlinson
Steele of the Royal Mounted (1925), directed by David Smith. Starring Bert Lytell
The Ancient Highway (1925), a Paramount film directed by Irvin Willat. Starring Jack Holt, Billie Dove and Montagu Love
When the Door Opened (1925), directed by Reginald Barker. Starring Jacqueline Logan and Walter McGrail
The Wolf Hunters (1926), directed by Stuart Paton. Starring Robert McKim, Virginia Brown Faire and Alan Roscoe.
The Country Beyond (1926), a Fox Film Co. production directed by Irving Cummings. Starred Olive Borden, Ralph Graves, Gertrude Astor, J. Farrell MacDonald, Evelyn Selbie, Fred Kohler, Lawford Davidson, Alfred Fisher and Lottie Williams. Based on the Curwood's book of the same title.
Tentacles of the North (1926), directed by Louis Chaudet. Starring Gaston Glass and Alice Calhoun
The Flaming Forest (1926), directed by Reginald Barker. Starring Antonio Moreno and Renée Adorée
Prisoners of the Storm (1926), directed by Lynn Reynolds. Starring House Peters and Peggy Montgomery
A Captain's Courage (1926), a Rayart Pictures film directed by Louis Chaudet, produced by Ben Wilson Productions, and starring Edward Earle, Dorothy Dwan and Lafe McKee. Likely based on Curwood's The Courage of Captain Plum
Back to God's Country (1927), directed by Irvin Willat and starring Renée Adorée.
The Slaver (1927), directed by Harry Revier. Starring Pat O'Malley and Carmelita Geraghty
Hearts of Men (1928), directed by James P. Hogan. Starring Mildred Harris
Thundergod (1928), directed by Charles J. Hunt. Starring Cornelius Keefe and Lila Lee
The Old Code (1928), directed by Ben F. Wilson. Starring Walter McGrail, Lillian Rich and Cliff Lyons
The Yellowback (1929), directed by Jerome Storm. Starring Tom Moore
River's End (1930), directed by Michael Curtiz. Starring Charles Bickford and Evalyn Knapp
The Trail Beyond (1934), directed by R. N. Bradbury. Starring John Wayne
Fighting Trooper (1934), directed by Ray Taylor. Starring Kermit Maynard
Northern Frontier (1935), directed by Sam Newfield. Starring Kermit Maynard
Wilderness Mail (1935), directed by Forrest Sheldon. Starring Kermit Maynard
The Red Blood of Courage (1935), directed by John English. Starring Kermit Maynard and Ann Sheridan
The Test (1935), directed by Bernard B. Ray. Starring Rin-Tin-Tin Jr., Grant Withers, Grace Ford and Monte Blue
The Hawk (1935), directed by Edward Dmytryk. Starring Yancey Lane, Betty Jordan and Dickie Jones
Code of the Mounted (1935), an Ambassador Pictures film directed and produced by Sam Newfield. Starred Kermit Maynard, Lillian Miles, Robert Warwick, Syd Saylor, Jim Thorpe, Wheeler Oakman and Eddie Phillips. Adapted from silent the film Heels of Fate
Trails of the Wild (1935), directed by Sam Newfield. Starring Kermit Maynard
Trails End (1935), directed by Albert Herman. Starring Conway Tearle, Claudia Dell, Fred Kohler and Stanley Blystone
His Fighting Blood (1935), directed by John English. Starring Kermit Maynard
Timber War (1935), directed by Sam Newfield. Starring Kermit Maynard
Skull and Crown (1935), directed by Elmer Clifton. Starring Rin-Tin-Tin Jr., Regis Toomey and Molly O'Day
Song of the Trail (1936), directed by Russell Hopton. Starring Kermit Maynard
Caryl of the Mountains (1936), a Reliable film directed by Bernard B. Ray and starring Rin-Tin-Tin Jr. and Francis X. Bushman Jr.
The Country Beyond (1936), a 20th Century Fox Film production directed by Eugene Forde with Paul Kelly and Rochelle Hudson. Photoplay derived from Curwood's book with the same title
Wildcat Trooper (1936), directed by Elmer Clifton. Starring Kermit Maynard
Phantom Patrol (1936), directed by Charles Hutchison. Starring Kermit Maynard
Vengeance of Rannah (1936), directed by Bernard B. Ray. Starring Rin-Tin-Tin Jr. and Bob Custer
Wild Horse Round-Up (1936), directed by Alan James. Starring Kermit Maynard
God's Country and the Woman (1937), directed by William Keighley. Starring George Brent and Beverly Roberts
Valley of Terror (1937), directed by Albert Herman. Starring Kermit Maynard
The Silver Trail (1937), directed by Bernard B. Ray. Starring Rex Lease
Whistling Bullets (1937), directed by John English. Starring Kermit Maynard
The Fighting Texan (1937), by Ambassador-Conn. Directed by Charles Abbott and starring Kermit Maynard, Elaine Shepard, Frank LaRue, Bud Buster, Ed Cassidy, Bruce Mitchell and Murdock McQuarrie, from a story by Curwood.
Galloping Dynamite (1937), directed by Harry L. Fraser. Starring Kermit Maynard
Rough Riding Rhythm (1937), directed by J. P. McGowan. Starring Kermit Maynard
Roaring Six Guns (1937), directed by J. P. McGowan. Starring Kermit Maynard
Call of the Yukon (1938), a Republic Pictures film, directed by B. Reeves Eason, and starring Richard Arlen and Beverly Roberts, from the story "Swift Lightning".
River's End (1940), directed by Ray Enright. Starring Dennis Morgan and George Tobias
Law of the Timber (1941), directed by Bernard B. Ray. Starring Marjorie Reynolds, Monte Blue and J. Farrell MacDonald
Dawn on the Great Divide (1942), Monogram Pictures film starring Buck Jones, Raymond Hatton, Rex Bell, Mona Barrie, Harry Woods and Robert Frazer. Directed by Howard Bretherton from the silent film Wheels of Fate and a screenplay by Jess Bowers.
Northwest Trail (1945), directed by Derwin Abrahams. Starring John Litel, Joan Woodbury, Bob Steele and Madge Bellamy
God's Country (1946), directed by Robert Emmett Tansey. Starring Robert Lowery, Helen Gilbert, William Farnum and Buster Keaton
'Neath Canadian Skies (1946), a Golden Gate Pictures, Inc. film, directed by B. Reeves Eason and produced by William B. David. Starred Russell Hayden, Inez Cooper, Douglas Fowley and Kermit Maynard, based on a story by Curwood.
Kazan (1949), directed by Will Jason. Starring Stephen Dunne and Lois Maxwell
Corporal Rod Webb and Chinook the Wonder Dog (film series with 10 films)
Trail of the Yukon (1949), directed by William Beaudine. Starring Kirby Grant
The Wolf Hunters (1949), directed by Budd Boetticher. Starring Kirby Grant
Snow Dog (1949), directed by Frank McDonald. Starring Kirby Grant
Call of the Klondike (1950), a Monogram Pictures film, starring Kirby Grant, Anne Gwynne, Lynne Roberts, Tom Neal, Russell Simpson, and Chinook the Wonder Dog. Directed by Frank McDonald from story by Curwood.
Northwest Territory (1951), directed by Frank McDonald. Starring Kirby Grant
Yukon Manhunt (1951), directed by Frank McDonald. Starring Kirby Grant
Yukon Gold (1952), directed by Frank McDonald. Starring Kirby Grant
Fangs of the Arctic (1953), an Allied Artist Production film released by Monogram Pictures. Starring Kirby Grant and Warren Douglas, directed by Rex Bailey from a story by Curwood
Northern Patrol (1953), directed by Rex Bailey. Starring Kirby Grant
Yukon Vengeance (1954), directed by William Beaudine. Starring Kirby Grant
Timber Fury (1950), directed by Bernard B. Ray. Starring David Bruce
Back to God's Country (1953), starring Rock Hudson and Hugh O'Brian
Nikki, Wild Dog of the North (1961), directed by Jack Couffer and Don Haldane. Starring Jean Coutu, Émile Genest, Uriel Luft, Robert Rivard and Jacques Fauteux.
The Bear (1988), by RCA-Columbia, directed by Jean-Jacques Annaud, based on the novel published as The Grizzly King (1916).
Aventures dans le Grand Nord (French-Canadian TV series, 5 films based on stories by James Oliver Curwood)
L'Honneur des grandes neiges (1994, TV film), directed by Gilles Carle. Starring Jürgen Prochnow
Bari (1994, TV film), directed by Arnaud Sélignac. Starring Neve Campbell and Jeff Fahey
Kazan (1995, TV film), directed by Arnaud Sélignac. Starring Jeff Fahey
Chasseurs de loups, chasseurs d'or (1995, TV film), directed by René Manzor. Starring Lukas Haas and Daniel Gélin
Le Sang du chasseur (1995, TV film), directed by Gilles Carle. Starring Michael Biehn and Vincent Cassel


== Notes ==


== References ==
Eldridge, Judith A. James Oliver Curwood: God's Country and the Man. Bowling Green, OH: Bowling Green State University Popular Press, 1993.
"James Oliver Curwood". Shiawassee County, Michigan History. Retrieved on February 7, 2012.


== External links ==
Works by James Oliver Curwood at Project Gutenberg
Works by James Oliver Curwood at Faded Page (Canada)
Works by or about James Oliver Curwood at Internet Archive
Works by James Oliver Curwood at LibriVox (public domain audiobooks) 
James Oliver Curwood on IMDb