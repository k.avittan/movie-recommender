The Barrier is a lava dam retaining the Garibaldi Lake system in southwestern British Columbia, Canada. It is over 300 m (980 ft) thick and about 2.4 km (1.5 mi) long where it impounds the lake.The area below and adjacent to The Barrier is considered hazardous due to the unstable lava formation.


== Formation ==
The Barrier was formed about 9,000 years ago, when large lava flows emanated from Clinker Peak on the west shoulder of Mount Price.  The large lava flowed towards the Cheakamus River valley.  At the time of eruption, the valley was filled by glacial ice.  The lava flow was stopped by the ice and ponded, eventually cooling to form an ice-marginal lava flow.  When the ice melted away, the ice-cooled lava-flow front formed a precipitous cliff; water ponded behind the lava dam, forming Garibaldi Lake.


== Rubble Creek boulder field ==
The unstable lava formation of The Barrier has in the past unleashed several debris flows in the area below Garibaldi Lake. The most recent major landslide in 1855-1856 formed a large boulder field which gives Rubble Creek its name. At least 30,000,000 m3 (1.1×109 cu ft) of rock was removed from The Barrier during the 1855-1856 event.


== Hazards ==
Concerns about The Barrier's instability due to volcanic, tectonic, or heavy rainfall activity prompted the provincial government to declare the area immediately below it unsafe for human habitation in 1981. This led to the evacuation of the small resort village of Garibaldi nearby, and the relocation of residents to new recreational subdivisions away from the hazard zone. Should The Barrier completely collapse, Garibaldi Lake would be entirely released and downstream damage in the Cheakamus and Squamish Rivers would be considerable, including major damage to the town of Squamish and possibly an impact-wave on the waters of Howe Sound that would reach Vancouver Island.


== See also ==
Garibaldi Lake volcanic field
Garibaldi Volcanic Belt
Cascade Volcanoes
Volcanism in Canada
Garibaldi Provincial Park


== References ==