Child sacrifice is the ritualistic killing of children in order to please or appease a deity, supernatural beings, or sacred social order, tribal, group or national loyalties in order to achieve a desired result. As such, it is a form of human sacrifice. 
Child sacrifice is thought to be an extreme extension of the idea that, the more important the object of sacrifice, the more devout the person giving it up is.


== Pre-Columbian cultures ==

Archaeologists have found the remains of more than 140 children who were sacrificed in Peru's northern coastal region.


=== Aztec culture ===

Archeologists have found remains of 42 children. It is alleged that these remains were sacrificed to Tlaloc (and a few to Ehécatl, Quetzalcoatl and Huitzilopochtli) in the offerings of the Great Pyramid of Tenochtitlan by the Aztecs of pre-Columbian Mexico. Human sacrifice was an everyday activity in Tenochtitlan and women and children were not exempt. According to Bernardino de Sahagún, the Aztecs believed that, if sacrifices were not given to Tlaloc, the rain would not come and their crops would not grow.  Archaeologists have found the remains of 42 children sacrificed to Tlaloc (and a few to Ehecátl Quetzalcóatl) in the offerings of the Great Pyramid of Tenochtitlan. In every case, the 42 children, mostly males aged around six, were suffering from serious cavities, abscesses or bone infections that would have been painful enough to make them cry continually. Tlaloc required the tears of the young so their tears would wet the earth.  As a result, if children did not cry, the priests would sometimes tear off the children's nails before the ritual sacrifice.


=== Inca culture ===
The Inca culture sacrificed children in a ritual called qhapaq hucha. Their frozen corpses have been discovered in the South American mountaintops. The first of these corpses, a female child who had died from a blow to the skull, was discovered in 1995 by Johan Reinhard. Other methods of sacrifice included strangulation and simply leaving the children, who had been given an intoxicating drink, to lose consciousness in the extreme cold and low-oxygen conditions of the mountaintop, and to die of hypothermia.


=== Maya culture ===
In Maya culture, people believed that supernatural beings had power over their lives and this is one reason that child sacrifice occurred. The sacrifices were essentially to satisfy the supernatural beings. This was done through k'ex, which is an exchange or substitution of something. Through k’ex infants would substitute more powerful humans. It was thought that supernatural beings would consume the souls of more powerful humans and infants were substituted in order to prevent that. Infants are believed to be good offerings because they have a close connection to the spirit world through liminality. It is also believed that parents in Maya culture would offer their children for sacrifice and depictions of this show that this was a very emotional time for the parents, but they would carry through because they thought the child would continue existing. It is also known that infant sacrifices occurred at certain times. Child sacrifice was preferred when there was a time of crisis and transitional times such as famine and drought.There is archaeological evidence of infant sacrifice in tombs where the infant has been buried in urns or ceramic vessels. There have also been depictions of child sacrifice in art. Some art includes pottery and steles as well as references to infant sacrifice in mythology and art depictions of the mythology.


=== Moche culture ===
The Moche of northern Peru practiced mass sacrifices of men and boys.


=== Timoto-Cuica culture ===
The Timoto-Cuicas offered human sacrifices. Until colonial times children sacrifice persisted secretly in Laguna de Urao (Mérida). It was described by the chronicler Juan de Castellanos, who cited that feasts and human sacrifices were done in honour of Icaque, an Andean prehispanic goddess.


== Ancient Near East ==


=== Tanakh (Hebrew Bible) ===
References in the Tanakh point to an awareness of human sacrifice in the history of ancient Near Eastern practice. The king of Moab gives his firstborn son and heir as a whole burnt offering (olah, as used of the Temple sacrifice). In the book of the prophet Micah, the question is asked, 'Shall I give my firstborn for my sin, the fruit of my body for the sin of my soul?', and responded to in the phrase, 'He has shown all you people what is good. And what does Yahweh require of you? To act justly and to love mercy and to walk humbly with your God.' The Tanakh also implies that the Ammonites offered child sacrifices to Moloch.


==== Ban in Leviticus ====
In Leviticus 18:21, 20:3 and Deuteronomy 12:30–31, 18:10, the Torah contains a number of imprecations against and laws forbidding child sacrifice and human sacrifice in general. The Tanakh denounces human sacrifice as barbaric customs of Baal worshippers (e.g. Psalms 106:37).
James Kugel argues that the Torah's specifically forbidding child sacrifice indicates that it happened in Israel as well. The biblical scholar Mark S. Smith argues that the mention of "Topeth" in Isaiah 30:27–33 indicates an acceptance of child sacrifice in the early Jerusalem practices, to which the law in Leviticus 20:2–5 forbidding child sacrifice is a response. Some scholars have stated that at least some Israelites and Judahites believed child sacrifice was a legitimate religious practice.


==== Binding of Isaac ====

Genesis relates the binding of Isaac, by Abraham to present his son, Isaac, as a sacrifice on Mount Moriah. It was a test of faith (Genesis 21:12). Abraham agrees to this command without arguing. The story ends with an angel stopping Abraham at the last minute and making Isaac's sacrifice unnecessary by providing a ram, caught in some nearby bushes, to be sacrificed instead. Francesca Stavrakopoulou has speculated that it is possible that the story "contains traces of a tradition in which Abraham does sacrifice Isaac". Rabbi A.I. Kook, first Chief Rabbi of Israel, stressed that the climax of the story, commanding Abraham not to sacrifice Isaac, is the whole point: to put an end to the ritual of child sacrifice, which contradicts the morality of a perfect and giving (not taking) monotheistic God. According to Irving Greenberg the story of the binding of Isaac, symbolizes the prohibition to worship God by human sacrifices, at a time when human sacrifices were the norm worldwide.


==== Gehenna and Tophet ====

The most extensive accounts of child sacrifice in the Hebrew Bible refer to those carried out in Gehenna by two kings of Judah, Ahaz and Manasseh of Judah.


==== Judges ====

In the Book of Judges, the figure of Jephthah makes a vow to God, saying, "If you give the Ammonites into my hands, whatever comes out of the door of my house to meet me when I return in triumph from the Ammonites will be the Lord’s, and I will sacrifice it as a burnt offering" (as worded in the New International Version). Jephthah succeeds in winning a victory, but when he returns to his home in Mizpah he sees his daughter, dancing to the sound of timbrels, outside.  After allowing her two months preparation, Judges 11:39 states that Jephthah kept his vow. According to the commentators of the rabbinic Jewish tradition, Jepthah's daughter was not sacrificed but was forbidden to marry and remained a spinster her entire life, fulfilling the vow that she would be devoted to the Lord. The 1st-century CE Jewish historian Flavius Josephus, however, understood this to mean that Jephthah burned his daughter on Yahweh's altar, whilst pseudo-Philo, late first century CE, wrote that Jephthah offered his daughter as a burnt offering because he could find no sage in Israel who would cancel his vow. In other words, this story of human sacrifice is not an order or requirement by God, but the punishment for those who vowed to sacrifice humans.


=== Phoenicia and Carthage ===

Neighbors criticized Carthage for their child sacrifice practices. Plutarch (ca. 46–120 AD), Tertullian, Orosius, and Diodorus Siculus mention this practice; however, Livy and Polybius do not. The ancestors of Carthage, Canaanites, were also mentioned performing child sacrifices in the Hebrew Bible and by some Israelites, at a place called the Tophet ("roasting place").Some of these sources suggest that children were roasted to death on a heated bronze statue. According to Diodorus Siculus, "There was in their city a bronze image of Cronus extending its hands, palms up and sloping toward the ground so that each of the children when placed thereon rolled down and fell into a sort of gaping pit filled with fire."(Bib. Hist. 20.14.6)
Sites within Carthage and other Phoenician centers revealed the remains of young children in large numbers; some historians interpret this as evidence for frequent and prominent child sacrifice to the god Baal-hamon.
The accuracy of such stories is disputed by some modern historians and archaeologists. At Carthage, a large cemetery exists that combines the bodies of both very young children and small animals, and those who assert child sacrifice have argued that if the animals were sacrificed, then so too were the children. Recent archaeology, however, has produced a detailed breakdown of the ages of the buried children and, based on this and especially on the presence of prenatal individuals – that is, still births – it is also argued that this site is consistent with burials of children who had died of natural causes in a society that had a high infant mortality rate, as Carthage is assumed to have had. That is, the data support the view that Tophets were cemeteries for those who died shortly before or after birth, regardless of the cause of death.Greek, Roman and Israelite writers refer to Phoenician child sacrifice. Skeptics suggest that the bodies of children found in Carthaginian and Phoenician cemeteries were merely the cremated remains of children that died naturally. Sergio Ribichini has argued that the Tophet was "a child necropolis designed to receive the remains of infants who had died prematurely of sickness or other natural causes, and who for this reason were "offered" to specific deities and buried in a place different from the one reserved for the ordinary dead".According to Stager and Wolff, in 1984, there was a consensus among scholars that Carthaginian children were sacrificed by their parents, who would make a vow to kill the next child if the gods would grant them a favor: for instance that their shipment of goods was to arrive safely in a foreign port. They placed their children alive in the arms of a bronze statue of:

the lady Tanit ... . The hands of the statue extended over a brazier into which the child fell once the flames had caused the limbs to contract and its mouth to open ... . The child was alive and conscious when burned ... Philo specified that the sacrificed child was best-loved.
Later commentators have compared the accounts of child sacrifice in the Old Testament with similar ones from Greek and Latin sources speaking of the offering of children by fire as sacrifices in the Punic city of Carthage, which was a Phoenician colony. Cleitarchus in his "Scholia" of Plato's Republic mentions the practice:

There stands in their midst a bronze statue of Kronos, its hands extended over a bronze brazier, the flames of which engulf the child. When the flames fall upon the body, the limbs contract and the open mouth seems almost to be laughing until the contracted body slips quietly into the brazier. Thus it is that the 'grin' is known as ‘sardonic laughter,’ since they die laughing.
This reference also seems to clarify that the statue itself was not made to move by the flames, but rather the burnt and shrivelled body of the victim was contorted by them.
Diodorus Siculus too references this practice:

Himilcar, on seeing how the throng was beset with superstitious fear, first of all, put a stop to the destruction of the monuments, and then he supplicated the gods after the custom of his people by sacrificing a young boy to Cronus and a multitude of cattle to Poseidon by drowning them in the sea [...] in former times they had been accustomed to sacrifice to this god the noblest of their sons, but more recently, secretly buying and nurturing children, they had sent these to the sacrifice.
Plutarch in De superstitione also mentions the practice in Carthage:

they themselves offered up their own children, and those who had no children would buy little ones from poor people and cut their throats as if they were so many lambs or young birds.
These all mention the burning of children as offerings to Cronus or Saturn, that is to Ba'al Hammon, the chief god of Carthage (see Interpretatio Graeca and Interpretatio Romana for clarification).
Claims concerning Moloch and child sacrifice may have been made for negative-propaganda effect. The Romans and Israelites describe child sacrifice as a practice of their "evil" enemies. Some scholars think that after the Romans finally defeated Carthage and totally destroyed the city, they engaged in postwar propaganda to make their archenemies seem cruel and less civilized. The question of whether Phoenician child sacrifice was real or a myth continues to be discussed in academic circles, including the work of M'hamed Hassine Fantar.


=== Pre-Islamic Arabia ===
The Quran documents pagan Arabians sacrificing their children to idols.[Quran 6:137]


== Pre-Modern Europe ==
The Minoan civilization, located in ancient Crete, is widely accepted as the first civilization in Europe. An expedition to Knossos by the British School of Athens, led by Peter Warren, excavated a mass grave of sacrifices, particularly children, and unearthed evidence of cannibalism.
clear evidence that their flesh was carefully cut away, much in the manner of sacrificed animals. In fact, the bones of slaughtered sheep were found with those of the children... Moreover, as far as the bones are concerned, the children appear to have been in good health. Startling as it may seem, the available evidence so far points to an argument that the children were slaughtered and their flesh cooked and possibly eaten in a sacrifice ritual made in the service of a nature deity to assure an annual renewal of fertility.
Additionally, Rodney Castleden uncovered a sanctuary near Knossos where the remains of a 17-year-old were found sacrificed.

His ankles had evidently been tied and his legs folded up to make him fit on the table... He had been ritually murdered with the long bronze dagger engraved with a boar's head that lay beside him.
At Woodhenge, a young child was found buried with its skull split by a weapon. This has been interpreted by the excavators as child sacrifice, as have other human remains.
The Ver Sacrum ("A Sacred Spring") was a custom by which a Greco-Roman city would devote and sacrifice everything born in the spring, whether animal or human, to a god, in order to relieve some calamity.


== Africa ==


=== South Africa ===
The murder of children for body parts with which to make muti, for purposes of witchcraft, still occurs in South Africa. Muti murders occur throughout South Africa, especially in rural areas. Traditional healers or witch doctors often grind up body parts and combine them with roots, herbs, seawater, animal parts, and other ingredients to prepare potions and spells for their clients.


=== Uganda ===

In the early 21st century Uganda has experienced a revival of child sacrifice. In spite of government attempts to downplay the issue, an investigation by the BBC into human sacrifice in Uganda found that ritual killings of children are more common than Ugandan authorities admit. There are many indicators that politicians and politically connected wealthy businessmen are involved in sacrificing children in practice of traditional religion, which has become a commercial enterprise.


== See also ==
Binding of Isaac
Child cannibalism
Child sacrifice in pre-Columbian cultures
Child sacrifice in Uganda
Early infanticidal childrearing
Human sacrifice
Infanticide
Moloch
Religion in Carthage
Religious abuse


== References ==


== External links ==
 Media related to Child sacrifice at Wikimedia Commons
11/04/2008 Child survives sacrifice in Uganda UGPulse.com
Minoan Child Sacrifice