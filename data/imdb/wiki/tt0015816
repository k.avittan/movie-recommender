In Flames is a Swedish heavy metal band, formed by guitarist Jesper Strömblad in Gothenburg in 1990. At the Gates, Dark Tranquillity, and In Flames are the only remaining bands responsible for developing the genres known as Swedish death metal and melodic death metal.
During the band's early years, In Flames had a varying group of musicians recording with them, including many session musicians. By the release of Colony (1999) the group had established a stable lineup. Their sixth studio album Reroute to Remain (2002) showed the band moving toward a newer style of music that moved further away from melodic death metal and closer to alternative metal. This decision was criticized by fans of the group's heavier metal sound; however, it increased the band's mainstream audience and bolstered their album sales. As of 2008, In Flames has sold over two million records worldwide.Since the band's inception, In Flames have released thirteen studio albums, three EPs, and two live DVDs, their latest release being their thirteenth studio album I, the Mask in 2019. In Flames has been nominated for three Grammis Awards. They won two of these nominations; one for Soundtrack to Your Escape (2004) in the "Hard Rock/Metal of the Year" category, and one nomination for their following album Come Clarity (2006) in the same category.


== History ==


=== Founding and Lunar Strain ===
In Flames was founded in 1990 by Jesper Strömblad as a side project from his then-current death metal band, Ceremonial Oath. His purpose was to write songs with a more melodic musical direction, something which he was not allowed to do in Ceremonial Oath. In 1993, Strömblad decided to quit Ceremonial Oath due to musical differences and began focusing more on In Flames. That same year, Strömblad recruited Glenn Ljungström on guitar and Johan Larsson on bass guitar to form the first official In Flames line-up.The trio recorded a demo in August 1993 and sent it to Wrong Again Records. In order to increase their chances of getting signed to the label, the group lied and said they had thirteen songs already recorded, when in fact they only had three. The owner of the label enjoyed the music, and immediately signed them to the label.After being signed, work on the band's debut album began. The album, titled Lunar Strain, was recorded in Studio Fredman and released in August 1994. Since the band did not have a vocalist yet, Strömblad asked Mikael Stanne of Dark Tranquillity to provide session vocals. Many other session musicians participated in the recording as well, including guitarists Anders Iwers, Carl Näslund, and Oscar Dronjak (the latter of which provided backing vocals), vocalist Jennica Johansson, and violinist Ylva Wåhlstedt.


=== Subterranean ===

During 1994, In Flames recorded and self-produced their first EP, Subterranean, in Studio Fredman. In Flames still did not have a vocalist yet, so session vocals were provided this time by Henke Forss. In 1994, the band covered the song "Eye of the Beholder" for the Metallica tribute album Metal Militia: A Tribute to Metallica. In 1995, Subterranean was released. Subterranean garnered much attention and led the band to acquire a record deal with Nuclear Blast.


=== The Jester Race ===
In 1995, In Flames became tired of using session musicians to record an album or to do live shows, so the trio asked Björn Gelotte to join the band as the full-time drummer, and 6 months later asked Anders Fridén to join the band as the full-time vocalist. That same year, the new line up recorded the band's second studio album, The Jester Race (Released in 1996). This album was recorded once again in Studio Fredman, but unlike previous albums, it was co-produced by the studio's owner, Fredrik Nordström. Gelotte also provided some lead and acoustic guitars for the album. Afterwards, In Flames toured with Samael, Grip Inc., and Kreator.


=== Whoracle ===

In 1997, In Flames recorded and released their third studio album, Whoracle. This album was recorded once again in Studio Fredman and co-produced by Nordström. Once again Gelotte provided lead and acoustic guitars for the album. Ljungström and Larsson unexpectedly announced that they were leaving In Flames after the album was recorded. Niklas Engelin and Peter Iwers were recruited to fill in the vacant spots on guitar and bass respectively, during a tour with Dimmu Borgir. After the tour both Engelin and Iwers were asked to join the band as permanent members, to which they agreed. With the new line-up, In Flames then proceeded with a European tour and played their first two shows in Japan. However, by the end of that tour in 1998, Engelin quit In Flames, so the band decided to switch Gelotte from his position as drummer to guitarist, and they recruited Daniel Svensson to take over as drummer.


=== Colony ===
In 1999, the new line-up recorded and released the band's fourth studio album, Colony. This album was recorded once again in Studio Fredman and co-produced by Nordström. The second solo on "Coerced Coexistence" was recorded by Kee Marcello. Afterwards, In Flames toured Europe, Japan, and played their first show in the United States during the Milwaukee Metal Fest.


=== Clayman and  The Tokyo Showdown ===
In 2000, In Flames recorded and released their fifth studio album, Clayman. It was recorded in the same studio as their previous releases and again co-produced by Nordström. Afterwards, In Flames toured with Dream Theater, Slipknot, and Testament. In August 2001, In Flames released The Tokyo Showdown, a live album recorded during the Japanese tour in November 2000.


=== Reroute to Remain ===
In 2002, In Flames recorded and released their sixth studio album, Reroute to Remain. Unlike all of In Flames' previous albums, Reroute to Remain was not recorded in Studio Fredman or produced by Nordström. The album was produced by Daniel Bergstrand and recorded in a house the band rented in Denmark, except for the drums which were recorded at Dug-Out studio. Reroute to Remain represented a major stylistic shift for In Flames music, in the addition of clean vocals, catchier choruses and less growling. It was also their first album to have official singles released from it. That year the band toured with Slayer, Soulfly, and Mudvayne.


=== Soundtrack to Your Escape ===
In 2003, In Flames recorded their seventh studio album, Soundtrack to Your Escape, and released it in 2004. Like with Reroute to Remain, the majority of the album was recorded in a house that the band rented in Denmark, with the drums recorded in Dug-Out Studio. This album was also produced by Bergstrand. Soundtrack to Your Escape increased the band's popularity considerably, selling 100,000 copies in the United States and yielding a No. 2 single on the Swedish charts with The Quiet Place. Four music videos were filmed: "The Quiet Place", "My Sweet Shadow", "F(r)iend" and "A Touch of Red", all of which were directed by Patric Ullaeus. The ensuing world tour saw the band make their first trip to Australia, where they played to mostly sold out crowds. Afterward, In Flames toured with Judas Priest, Mötley Crüe, and Motörhead. In Flames was also featured on the main stage at Ozzfest 2005.


=== Come Clarity ===
In 2005, In Flames recorded and self-produced their eighth studio album, Come Clarity, in Dug-Out studio. That same year, In Flames released Used and Abused: In Live We Trust, a box set consisting of material filmed and recorded throughout various live performances during 2004. The DVD was filmed, edited and directed by Ullaeus. Also in 2005, In Flames decided to sign with an additional record label, Ferret Music, so future releases could have better distribution in North America. In 2006, Come Clarity was released in North America through Ferret Music and elsewhere through Nuclear Blast. It became the band's first No. 1 album on the Swedish charts. A bonus DVD of In Flames playing all the songs in a rehearsal studio was also released. Ullaeus directed their music video for "Take This Life". That same year, In Flames toured with Sepultura, co-headlined a U.S. tour with Lacuna Coil, joined The Unholy Alliance Tour, was one of the headliners on the Sounds of the Underground tour, and played on the main stage at Download Festival. In 2007, the band played in Dubai for the annual Dubai Desert Rock Festival. In Flames also played at Bloodstock Open Air festival in August 2007.


=== A Sense of Purpose ===

In October 2007, In Flames finished recording their ninth studio album in IF Studios, their own studio located in Gothenburg, Sweden (formerly Studio Fredman). During the recording sessions, the band released studio diaries documenting the recording process. On 23 January 2008, the band confirmed that they had recorded a video for the single "The Mirror's Truth", and posted photos from the video on their official Myspace profile. In Flames were featured on the Gigantour 3 North American tour with Megadeth, Children of Bodom, Job for a Cowboy, and High on Fire.On 4 April 2008, In Flames released their ninth studio album, A Sense of Purpose. The first single from the new album was titled "The Mirror's Truth" and was released in Europe on 7 March 2008. In June 2008, In Flames performed at Metaltown Festival, Metalcamp, Graspop Metal Meeting, Nova Rock Festival, Rock am Ring and Rock im Park, Gigantour and Download Festival. In late August 2008, In Flames filmed a video for the second single, "Alias", once again directed by Ullaeus. From January 2008 to December 2009 In Flames toured on the A Sense of Purpose Tour.
On 3 February 2009, the band announced that Strömblad would not be participating in the Australia/South America/Japan tour in order to get treatment for his alcohol abuse problems. Engelin was announced as his replacement for the tour. On 18 February, the band announced their United Kingdom and Ireland tour would not go ahead as planned due to Strömblad's alcohol rehabilitation and the birth of Iwers's child. On 4 March, the band announced that the third single from A Sense of Purpose is the track "Delight and Angers". The video premiered on the band's Myspace on 25 March. It was once again directed and produced by Ullaeus. Alex Pardee did the artwork for the song like he did with all of A Sense of Purpose.
The band joined with Killswitch Engage, Dead by April, Every Time I Die and Maylene and the Sons of Disaster for the UK leg of the Taste of Chaos 2009 tour in November 2009.


=== Sounds of a Playground Fading ===
The departure of guitarist and founding member Jesper Strömblad was announced on the band's MySpace on 12 February 2010. The parting was on good terms with Strömblad, stating that "I'm determined to fight and defeat my demons once and for all...", with the remaining band members backing his decision, though in a more recent declaration Fridén stated that their relationship with Strömblad was not going well. On 28 February 2011, it was announced that Engelin had re-joined the band to take the vacant spot left by Strömblad's departure.
In early 2010, In Flames collaborated with Pendulum on their third studio album Immersion. The song "Self Versus Self" is one of three collaborations on the album.On 11 October 2010, In Flames entered IF Studios in Gothenburg to start recording their new album, tentatively titled Sounds of a Playground Fading. On 25 January 2011, the band announced that the recording of their new album was finished and that the album was being mixed. The album was recorded with producer Roberto Laghi.To support the album, In Flames performed at the Sonisphere Festival in Knebworth, and alongside Megadeth, Trivium, Machine Head, Godsmack, and Disturbed at the Mayhem Festival 2011. They also played at Hellfest Festival 2011, headlining the second main stage, and Nova Rock Festival in Austria.
In March 2011, In Flames signed with Century Media Records. On 6 April 2011, In Flames announced their newest single, "Deliver Us", was slated for a May release. For part of the European tour, Jonas Ekdahl of DeathDestruction filled in for Svensson on drums, after Svensson's decision to remain in Sweden for a time following the birth of his child.
Sounds of a Playground Fading was released in Sweden on 15 June 2011, on 17 June 2011 in Germany, Austria, Switzerland and Norway, on 20 June 2011 in the UK, Benelux, France, Greece, Denmark and Portugal, on 21 June 2011 in North America, Spain and Italy, on 22 June 2011 in Finland, Hungary and Japan, and on 24 June 2011 in Australia and New Zealand. A video was made for "Deliver Us". It was filmed in Gothenburg, and was again directed by Ullaeus, who also directed the second video from Sounds of a Playground Fading, "Where the Dead Ships Dwell".
In an interview at Canada's Heavy MTL festival on 12 August 2012, frontman Anders Fridén revealed that In Flames will "start thinking about" recording their next studio album in 2013.


=== Siren Charms and Sounds from the Heart of Gothenburg ===
In Flames began recording their eleventh studio album in August 2013. On 10 April 2014, the In Flames website appeared to have been hacked. The alleged hacker threatened to leak information the next day. The band posted on its Facebook, saying, "We have encountered some issues regarding our online platforms. We want to assure you that no Jesterhead member information has been compromised, and our team is working on a solution right now. We apologize for the inconvenience." However, the site was not really hacked. The following day, the band's website was updated with a link to the supposed leak. A short video announced that the new album will be titled Siren Charms. The album was released on 9 September 2014, via the band's new label, Epic Records. Siren Charms includes vocals recorded by fans.  Following release of the album they started a World tour, set to last throughout 2015.  On 12 October 2014 the band officially announced on their Facebook page that they are to release a brand new Live Blu-ray/DVD and CD set based on their show in Gothenburg, which took place in November 2014 and was officially released on 23 September 2016 titled 'Sounds from the Heart of Gothenburg'. Longtime drummer Daniel Svensson announced his departure from the band to focus on his family life on 7 November 2015.


=== Battles ===

In early 2016, In Flames began recording a new album in Los Angeles, and finished the recording in April.On 25 August 2016, In Flames posted a YouTube link to a new song entitled "The End" along with a new profile picture with the In Flames logo accompanied by the word "Battles".  It was later revealed the same day that Battles will be the title of their twelfth studio album, and was released worldwide on 11 November 2016 through Nuclear Blast.
On 17 September 2016 the band announced on their Facebook page that Joe Rickard, formerly of American rock band RED, who performed session drums on Battles is now the new permanent drummer, replacing Daniel Svensson.On 29 November 2016, Peter Iwers announced the tour supporting Battles would be his last with In Flames.  This leaves just Anders and Björn as the only members left in the band who have constantly remained in the lineup since the 1990s. Bassist Håkan Skoger, who previously worked with Friden on his side project 'Passenger', played live with the band on their first UK tour date in Scotland. The band then recruited bassist Bryce Paul to play bass on their American tour.In December 2017, In Flames released 'Down, Wicked, and No Good' EP, which included three covers and a live performance.On 5 July 2018, it was announced the band recruited Tanner Wayne as the new drummer, replacing Joe Rickard.


=== I, the Mask ===
On 14 December 2018, In Flames announced that their 13th album titled I, the Mask would be released on 1 March 2019. As part of this announcement, they released two singles from the upcoming album, "I Am Above" and "(This Is Our) House". The music video for "I Am Above" features Swedish actor Martin Wallström and does not show any members of the band. The lyric video released for "(This Is Our) House" features live concert footage from the Borgholm Brinner music festival in Sweden. 
The album was produced by Howard Benson, mixed by Chris Lord-Alge, and mastered by Ted Jensen. On 10 January 2019, the lyric video for "I, the Mask", the title track of the album, was released via the Nuclear Blast Records' YouTube channel. In Flames was set to open up for Megadeth and Lamb of God with Trivium in the summer of 2020 in North America. The event has since been rescheduled to 2021 due to the COVID-19 pandemic.


=== Clayman – 20th Anniversary Edition ===
On 28 August 2020, In Flames released a 20th anniversary edition of Clayman. Apart from remastered versions of every song, it features re-recorded versions of "Only for the Weak", "Bullet Ride", "Pinball Map", and "Clayman".


== Musical style and lyrical content ==
In Flames, along with Dark Tranquillity and At the Gates, pioneered what is now known as melodic death metal. The founding members of all three bands all lived in Gothenburg, Sweden and they were all friends who shared the same musical interests. Eventually, the group of friends branched off into three bands with the same musical direction: In Flames, Dark Tranquillity, and At the Gates.
Jesper Strömblad formed In Flames to write music that combined the melodic guitar style of Iron Maiden with the brutality of death metal, something which he stated he had never heard any band do. In writing songs, Strömblad also decided to make use of keyboards, something which was (at the time) uncommon in death metal. Ever since In Flames' debut studio album, Lunar Strain, the band has made use of keyboards. However, despite their sometimes heavy use of keyboards in their music, they still refuse to recruit a full-time keyboardist.In Flames' musical style is characterized by the constant use of harmonized lead guitar melodies and screaming-style singing along with death growl. In early albums such as The Jester Race, In Flames would often employ two harmonized lead guitars playing over a rhythm guitar. However, since the band only has two guitar players, they found it hard to reproduce those songs during live performances and ever since Reroute to Remain they have focused on writing songs with the intention of playing them live. On the album Soundtrack to Your Escape, the band focused on less guitar melodies, giving place for more synths. However, this has not been continued on the later albums.In Flames' vocal style is characterized by the use of growled vocals or screamed vocals complemented at times by clean vocals. In more recent albums such as Come Clarity, the band makes more prominent use of clean vocals, especially during choruses. In Flames' lyrics have also varied during their career. In early albums such as The Jester Race and Whoracle, In Flames' lyrics focused on astrology, mankind, and other global themes. In later albums such as Soundtrack to Your Escape and Come Clarity, In Flames' lyrics focus more on personal issues, thoughts, and other introspective themes.Since Reroute to Remain was released in 2002, In Flames' fan-base has been split because of the gradual change in style towards a more alternative metal sound. The stylistic changes include screamed and clean vocals, as opposed to the death growl on the early records, more prominent rhythm section and less guitar melodies and solos, more obvious use of synthesizers and electronics, and a clear influence from modern American metal, especially in the uplifting chorus melodies. This later style has been mostly described as alternative metal. Since Siren Charms and more recently their latest studio album Battles, they have more extensively used clean vocals as opposed to screams and growls and have played a softer overall sound, putting more emphasis on a melodic alternative metal direction. Very minimal elements of their earlier melodic death metal sound are utilized at this point. However, some very mild elements of this style can still sometimes be heard in their music. Fridén's clean singing style has altered drastically as it is now more prominent and he has taken on a higher-pitched vocal range. In Flames has also been described as metalcore, with AllMusic calling the band one of the pioneers of metalcore along with calling In Flames one of the pioneers of melodic death metal.


=== Legacy ===
In Flames has influenced many bands. The band has been especially influential to metalcore, with bands such as Darkest Hour, As I Lay Dying, and Still Remains naming them as a source of inspiration. They have also influenced a number of subsequent melodic death metal bands, including Insomnium, Omnium Gatherum, and Blood Stain Child.


== Miscellaneous ==


=== Awards ===
In Flames has been awarded four Grammis awards (the Swedish equivalent of the Grammy Awards) to date. In 2005, In Flames won their first Grammis award in the category of Best Hard Rock/Metal Album for Soundtrack to Your Escape. In 2006, In Flames won the Swedish Export Award, their second Grammis award. In Flames was the first metal band to ever win that award and the Swedish economy minister at the time, Thomas Östros, was quoted as saying "Thanks to In Flames, Sweden now have a metal band in the absolute world elite." In 2007, In Flames once again won the category of Best Hard Rock/Metal Album for Come Clarity. They have also won the "Best international Band" award from the Metal Hammer Golden Gods Awards in 2008.


=== Jester Head mascot ===
When In Flames were recording their second studio album, The Jester Race, Anders Fridén and Niklas Sundin came up with an idea to create a symbol/mascot for In Flames. The result was the Jester Head. This symbol made its first appearance in the cover artwork of The Jester Race and ever since, the Jester has been featured in various forms somewhere on every studio album that In Flames has released, either as a song title or on the album artwork. The Jester Head is also featured in much In Flames' merchandise and has been featured in banners as part of In Flames' live set.
Swedish ice hockey goaltender, Robin Lehner, who is also from Gothenburg had the Jester Head painted on several of his protective face masks.


== Members ==
Initially formed as a side project by Ceremonial Oath bassist Jesper Strömblad in 1990, the group's first full lineup in 1993 also featured guitarist Glenn Ljungström and bassist Johan Larsson. As the band were without an official vocalist, their 1994 debut Lunar Strain featured contributions from Dark Tranquillity frontman Mikael Stanne, as well as several guest musicians. The 1995 EP Subterranean featured vocals by Dawn's Henke Forss, as well as drums by Daniel Erlandsson of Eucharist and Anders Jivarp of Dark Tranquillity. Around the time of the EP's release, Björn Gelotte joined In Flames as the band's first official drummer, and a short time later Anders Fridén was brought in from Dark Tranquillity as the group's first official vocalist.The lineup of Fridén, Strömblad, Ljungström, Larsson and Gelotte released The Jester Race in 1996 and Whoracle in 1997. By the end of the recording for Whoracle, however, both Ljungström and Larsson had announced that they were leaving the band. They were replaced for a short tour by Niclas Engelin and Peter Iwers, both of whom were ultimately retained as official members. Engelin only remained until the following year, however, and his role was taken over by Gelotte. The vacated position of drummer was soon filled by Daniel Svensson. The band's lineup remained stable for over a decade, releasing several albums.In February 2010, founding member Strömblad announced that he was leaving In Flames, with the band admitting that "in order to keep a very dear friend this is probably for the best". He was replaced for tour dates later in the year by a returning Niclas Engelin, who was later announced as a full member of the group in March 2011. In late 2015, long-term drummer Svensson also left In Flames, explaining that he wanted to "spend more time with ... [his] wife and three daughters". Joe Rickard was brought in as Svensson's replacement the following year. Iwers also departed a year after Svensson, explaining that he felt it was "the time to move on with other musical and non-musical adventures". Bryce Paul took over his place for future tour dates. In July 2018, Tanner Wayne replaced Rickard.Currently, rhythm guitarist Engelin is on hiatus with former Megadeth member Chris Broderick filling for live shows.


=== Official members ===


==== Current ====


==== Former ====


=== Other contributors ===


==== Session ====


==== Touring ====


=== Timeline ===

In Flames line-up at Rockharz Open Air 2018
		
		
		
		
		
		


== Discography ==

Studio albums

Lunar Strain (1994)
The Jester Race (1996)
Whoracle (1997)
Colony (1999)
Clayman (2000)
Reroute to Remain (2002)
Soundtrack to Your Escape (2004)
Come Clarity (2006)
A Sense of Purpose (2008)
Sounds of a Playground Fading (2011)
Siren Charms (2014)
Battles (2016)
I, the Mask (2019)


== References ==


== External links ==
Official website