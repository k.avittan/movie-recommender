A magic carpet, also called a flying carpet, is a legendary carpet and common trope in fantasy fiction. It is typically used as a form of transportation and can quickly or instantaneously carry its users to their destination.


== In literature ==
One of the stories in the One Thousand and One Nights relates how Prince Husain, the eldest son of Sultan of the Indies, travels to Bisnagar (Vijayanagara) in India and buys a magic carpet. This carpet is described as follows: "Whoever sitteth on this carpet and willeth in thought to be taken up and set down upon other site will, in the twinkling of an eye, be borne thither, be that place nearhand or distant many a day's journey and difficult to reach." The literary traditions of several other cultures also feature magical carpets, in most cases literally flying rather than instantly transporting their passengers from place to place.
Solomon's carpet was reportedly made of green silk with a golden weft, sixty miles (97 km) long and sixty miles (97 km) wide: "when Solomon sat upon the carpet he was caught up by the wind, and sailed through the air so quickly that he breakfasted at Damascus and supped in Media." The wind followed Solomon's commands, and ensured the carpet would go to the proper destination; when Solomon was proud, for his greatness and many accomplishments, the carpet gave a shake and 40,000 fell to their deaths. The carpet was shielded from the sun by a canopy of birds. In Shaikh Muhammad ibn Yahya al-Tadifi al-Hanbali's book of wonders, Qala'id-al-Jawahir ("Necklaces of Gems"), Shaikh Abdul-Qadir Gilani walks on the water of the River Tigris, then an enormous prayer rug (sajjada) appears in the sky above, "as if it were the flying carpet of Solomon [bisat Sulaiman]".In Russian folk tales, Baba Yaga can supply Ivan the Fool with a flying carpet or some other magical gifts (e.g. a ball that rolls in front of the hero showing him the way, or a towel that can turn into a bridge). Such gifts help the hero to find his way "beyond thrice-nine lands, in the thrice-ten kingdom". Russian painter Viktor Vasnetsov illustrated the tales featuring a flying carpet on two occasions.
In Mark Twain's "Captain Stormfield's Visit to Heaven", magic wishing carpets are used to instantaneously travel throughout Heaven.
Poul Anderson's Operation Chaos features a world making extensive use of magic in daily life, and among other things having flying carpets as a common, non-polluting means of transportation - in fierce competition with the also available flying brooms. Travelers need not sit on the bare carpet itself, as the carpet serves as the platform for a comfortable cabin. 
Magic carpets have also been featured in modern literature, movies, and video games, and not always in a classic context.


== See also ==
The Phoenix and the Carpet – 1904 children's novel by E. Nesbit
Old Khottabych – 1938 Soviet children's book and later 1956 film with the depiction of a flying carpet
"Magic Carpet Ride" – 1968 song by Steppenwolf
Asterix and the Magic Carpet – 1987 illustrated comic story book on the adventures of Asterix, Obelix and Cacofonix in India
King Solomon's Carpet – 1991 novel by Barbara Vine about the London Underground


== Notes ==


== External links ==
The secret history of the Flying Carpet