The Story of Joseph and His Brethren (Italian: Giuseppe venduto dai fratelli) is a 1961 Yugoslavian/Italian film directed by Irving Rapper and Luciano Ricci.
The film is also known as Joseph Sold by His Brothers, Joseph and His Brethren (American DVD box title) and Sold into Egypt in the United Kingdom.
It was the last film of Belinda Lee.


== Plot summary ==
Joseph, son of Israel (Jacob) and Rachel, lived in the land of Canaan with eleven brothers and one sister. He was Rachel's firstborn and Israel's eleventh son. Of all the sons, Joseph was loved by his father the most. Israel even arrayed Joseph with a "long coat of many colors". Israel's favoritism toward Joseph caused his half brothers to hate him, and when Joseph was seventeen years old he had two dreams that made his brothers plot his demise. In the first dream, Joseph and his brothers gathered bundles of grain. Then, all of the grain bundles that had been prepared by the brothers gathered around Joseph's bundle and bowed down to it. In the second dream, the sun (father), the moon (mother) and eleven stars (brothers) bowed down to Joseph himself. When he told these two dreams to his brothers, they despised him for the implications that the family would be bowing down to Joseph. They became jealous that their father would even ponder over Joseph's words concerning these dreams. (Genesis 37:1–11)
They saw their chance when they were feeding the flocks, the brothers saw Joseph from afar and plotted to kill him. They turned on him and stripped him of the coat his father made for him, and threw him into a pit. As they pondered what to do with Joseph, the brothers saw a camel caravan of Ishmaelites coming out of Gilead, carrying spices and perfumes to Egypt, for trade. Judah, the strongest, thought twice about killing Joseph and proposed that he be sold. The traders paid twenty pieces of silver for Joseph, and the brothers took Joseph's coat back to Jacob, who was lied to and told that Joseph had been killed by wild animals.


=== Potiphar's house ===
The Biblical story is clear Joseph was sold into slavery by his brothers led by Judah, "Come let us sell him to the Ishmaelites" there" (Genesis 37:27). In Egypt, Potiphar, the captain of Pharaoh's guard "bought Joseph from the Ishmaelites who had brought him down there" (Genesis 39:1). While serving in Potiphar's household, Yahweh was with Joseph so that he prospered in everything he did. Joseph found favor in the sight of Potiphar and so he became his personal servant. Then Joseph was promoted to oversee Potiphar's entire household as a superintendent. After some time, Potiphar's wife began to desire Joseph and sought to have an affair with him. Despite her persistence, he refused to have sexual intercourse with her for fear of sinning against God. After some days of begging for him, she grabbed him by his cloak, but he escaped from her leaving his garment behind. Angered by his running away from her, she took his garment and made a false claim against him by charging that he tried have sexual intercourse with her. This resulted in Joseph being thrown into prison (Genesis 39:1–20).


=== Joseph in prison ===
The warden put Joseph in charge of the other prisoners, and soon afterward Pharaoh's chief cup bearer and chief baker, who had offended the Pharaoh, were thrown into the prison. They both had dreams, and they asked Joseph to help interpret them. The chief cup bearer had held a vine in his hand, with three branches that brought forth grapes; he took them to Pharaoh and put them in his cup. The chief baker had three baskets of bread on his head, intended for Pharaoh, but some birds came along and ate the bread. Joseph told them that within three days the chief cup bearer would be reinstated but the chief baker would be hanged. Joseph requested the cup bearer to mention him to Pharaoh and secure his release from prison, but the cup bearer, reinstalled in office, forgot Joseph. After Joseph was in prison for two more years, Pharaoh had two dreams which disturbed him. He dreamt of seven lean cows which rose out of the river and devoured seven fat cows; and, of seven withered ears of grain which devoured seven fat ears. Pharaoh's wise men were unable to interpret these dreams, but the chief cup bearer remembered Joseph and spoke of his skill to Pharaoh. Joseph was called for, and interpreted the dreams as foretelling that seven years of abundance would be followed by seven years of famine, and advised Pharaoh to store surplus grain during the years of abundance.
When the famine came, it was so severe that people from surrounding nations "from all over the earth" came to Egypt to buy bread as this nation was the only Kingdom prepared for the seven-year drought.


=== Brothers sent to Egypt ===
In the second year of famine, Joseph's half brothers were sent to Egypt, by their father Israel, to buy goods. When they came to Egypt, they stood before the Vizier but did not recognize him to be their brother Joseph. However, Joseph did recognize them and did not receive them kindly, rather he disguised himself and spoke to them in the Egyptian language using an interpreter. He did not speak at all to them in his native tongue, Hebrew. After questioning them as to where they came from, he accused them of being spies.  They pleaded with him that their only purpose was to buy grain for their family in the land of Canaan. After they mentioned that they had left a younger brother at home, the Vizier (Joseph) demanded that he be brought to Egypt as a demonstration of their veracity. This brother was Joseph's blood brother, Benjamin. He placed his brothers in prison for three days. On the third day, he brought them out of prison to reiterate that he wanted their youngest brother brought to Egypt to demonstrate their veracity. The brothers conferred amongst themselves speaking in Hebrew, reflecting on the wrong they had done to Joseph. Joseph understood what they were saying and removed himself from their presence because he was caught in emotion. Joseph sent the brothers back with food but kept one brother, and the remaining brothers returned to their father in Canaan, and told him all that had transpired in Egypt. They also discovered that all of their money sacks still had money in them, and they were dismayed. Then they informed their father that the Vizier demanded that Benjamin be brought before him to demonstrate that they were honest men. After they had consumed all of the grain that they brought back from Egypt, Israel told his sons to go back to Egypt for more grain. With Reuben and Judah's persistence, they persuaded their father to let Benjamin join them for fear of Egyptian retribution. Upon their return to Egypt, the brothers were afraid because of the returned money in their money sacks. Then when they get there Joseph reveals to them that he is in fact their brother, Joseph. Then he has their father Jacob brought so they are all reunited in Egypt.


== Cast ==


== Production ==
It was one of two movies Rapper directed in Italy, the other being Pontius Pilate. Filming took place in Rome in October 1960. Robert Morley set fire to Belinda Lee's wig in one scene.


== Reception ==
The New York Times called it "the clumsiest, the silliest, the worst of the quasi Bible stories to come along since wide screen was born... if you go see it, be prepared to howl."The Monthly Film Bulletin called it "sedate and extremely prosaic."The Daily Mail reviewing Joseph in 1964 said Robert Morely put a stern face on a monstrous piece of miscasting" and "we come away sadly reflecting that properly handled, which she so rarely was, Belinda Lee might have been groomed into some kind of English Loren."


== References ==


== External links ==
Giuseppe venduto dai fratelli on IMDb
The Story of Joseph and His Brethren at TCMDB
Film page at BFI
The Story of Joseph and His Brethren is available for free download at the Internet Archive