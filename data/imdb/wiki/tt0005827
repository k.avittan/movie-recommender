A Dangerous Path is a fantasy novel, the fifth book in the Warriors series, written by Cherith Baldry and Kate Cary under the pseudonym of Erin Hunter. This individual book was written by Cherith Baldry. The series is about four Clans of wild cats, and their adventures in the forest in which they reside. The story follows a plan set up by Tigerstar, leader of ShadowClan, who wishes to destroy ThunderClan by using dogs to hunt them down. At the end of the book, Bluestar, leader of ThunderClan, dies after risking her life to kill the dogs, making Fireheart, now Firestar, the new leader of ThunderClan. Near the end of the  book, Fireheart's friend Graystripe also returns to ThunderClan from RiverClan, knowing that no one will respect him in RiverClan.
The book was first published in the US as a hardcover on 1 June 2004. It was later published as a paperback and as an e-book, and in the UK and Canada as a paperback. A Dangerous Path has also been published in foreign languages, including French and Chinese. The book received positive reviews from reviewers such as Booklist and Horn Book Review, who praised the tensions and fast pace.


== Synopsis ==


=== Setting ===
The story again takes place in a fictional forest populated by four groups of feral cats, ThunderClan, RiverClan, WindClan and ShadowClan. Each Clan has their own territory in which they hunt for prey and protect from enemies such as rival Clans and wild animals. The story follows Fireheart and his adventures in ThunderClan.
The forest is based on New Forest, which is located in Southern England. Besides New Forest, Loch Lomond, the Scottish Highlands, and the Forest of Dean also inspired the fictional locales in the novel.


== Plot ==
When a pack of dogs begins to run wild in the forest, Fireheart, deputy of ThunderClan, suspects that Tigerstar, ShadowClan's new leader, is behind it. Bluestar names Cloudpaw a warrior even though all of the apprentices had worked hard, and they were all old enough. Two of ThunderClan's apprentices, Swiftpaw the small black-and-white tom and Brightpaw the white she-cat with ginger patches, try to fight the dog pack themselves in an attempt to prove themselves. However, Swiftpaw is killed and Brightpaw badly disfigured by the pack of dogs, losing part of her face and one eye. While Brightpaw is recovering from the mauling, she repeats words she heard the dogs say: "pack, pack" and "kill, kill". Bluestar then "promotes" Brightpaw to the full status of a true warrior, but she shames Brightpaw by giving her the name Lostface.
Normal Clan life resumes, but Fireheart begins to have a strong sense of curiosity of how this happens. To find out, he asks Lostface if she remembers what happens, but she can only remember the words "Pack, pack, kill, kill". Whitestorm the big muscular snowy-white tom reports to Fireheart that they smell dog near Snakerocks, a pile of rocks where adders live, so they head there.
When they arrive, they find a trail of dead rabbits is found leading to the camp, ending with Brindleface in front of the camp, who had been killed by Tigerstar. Fireheart orders a patrol to get rid of the rabbits. Brindleface's death shocks everyone, and after her burial, the camp is evacuated to Sunningrocks to protect ThunderClan. With Bluestar still mentally unstable, it is up to Fireheart to destroy the pack and protect ThunderClan. He and the senior warriors decide that a patrol of cats will lead the dogs to and over the nearby gorge, drowning some of the dogs. But, before he leaves, he apologizes to Sandstorm for not obeying Bluestar and they fall in love, but all they did was realize the existing love they had for each other. The plan works: each cat successfully leads the pack closer to the gorge, breaking away from the group once they lead the dogs to a certain spot, where a fresh runner lies in wait so they can lead the dogs to the next point. Fernpaw the short-furred pale gray she-cat and her brother Ashpaw the pale gray tom with darker flecks (both of them are Brindleface's kits) took part in leading the dogs; they did it as revenge against Tigerstar for killing their mother. Fireheart, the last in line, keeps a good distance between him and the pack leader, until Tigerstar appears and pins Fireheart down. As the huge male dog leader with a massive black-and-tan head gets dangerously close, Tigerstar leaps away, leaving Fireheart to be killed by the pack. As Fireheart struggles against the lead dog , Bluestar, her old self returned once more, slams into the dog, releasing its grip on Fireheart and throwing the lead dog — and herself — over the side of the gorge.
Fireheart leaps down the cliff to the river and dives in to save Bluestar. Fireheart manages to get a hold on her, but is barely able to keep their heads above the water. Soon, however, Bluestar's RiverClan children, Stonefur the long-legged pale blue-gray tom and Mistyfoot the lithe blue-gray she-cat with dense fur, discover Fireheart struggling in the river trying to save their mother and help drag Bluestar to the bank. There, Bluestar pleads with her kits to forgive her for lying to them about who their parents were. They forgive her, as they had not known about their true heritage until then. Bluestar then tells Fireheart that she realised that they were not all traitors to the Clan, like Tigerstar was. She also tells him that he is now the leader of ThunderClan, and that "you are the fire that saved my Clan", fulfilling two prophecies: Goosefeather the plump speckled gray tom's prophecy that water would kill Bluestar, and Spottedleaf's prophecy stating that 'fire will save the Clan'.
With that, she loses her final life.
Fireheart is filled with grief, and Mistyfoot asks if she and Stonefur can help carry her back to camp and sit vigil for her.


== Main characters ==
Fireheart, deputy, and later leader of ThunderClan
Tigerstar, the leader of ShadowClan; formerly of Thunderclan
Bluestar, leader of ThunderClan, but then loses her nine lives
Stonefur, a warrior, then later deputy of RiverClan, Mistyfoot's brother and Bluestar's kit
Mistyfoot, a warrior of RiverClan, Stonefur's sister and Bluestar's kit
Cloudpaw, Fireheart's apprentice, Fireheart's nephew
Swiftpaw, an apprentice, but then loses his life in the war with the dogs
Brightpaw, is also an apprentice, but then is badly hurt by one of the dogs and name changed to Lostface
Sandstorm, a warrior which Fireheart loves
Whitestorm, a warrior from ThunderClan
Brindleface, also a warrior from ThunderClan, but killed by Tigerstar
Cinderpelt, the permanently wounded medicine cat  who can no longer be a warrior
Graystripe, a cat who leaves RiverClan, and is Fireheart's best friend


== Publication history ==
The book was first published in the US as a hardcover on 1 June 2004 by HarperCollins. It was then published as a paperback on 24 May 2005 and an e-book on 13 October 2009. A Dangerous Path was also published in the UK as a paperback on 2 April 2007 and in Canada as a hardcover on 20 May 2004. The book has also been published in foreign languages such as German, Japanese, French, Russian and Korean. The Chinese version was published on 31 December 2008 and is packaged along with a 3D trading card of Tigerstar.


== Critical reception ==
A Dangerous Path received a positive reception. A BookLoons review praised the rising tension in the book. Booklist called the book exciting and praised the fast pace writing "Hunter maintains the established characterisations of her sentient cats, who still retain their feline natures. With compelling intrigue and fast-paced actions, this is one of the most exciting books in the series". In a review for both Rising Storm and A Dangerous Path, Horn Book review praised the complex characters.


== Footnotes ==