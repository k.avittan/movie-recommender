The Whole Duty of Man is an English high church 'Protestant' devotional work, first published anonymously, with an introduction by Henry Hammond, in 1658. It was both popular and influential for two centuries, in the Anglican tradition it helped to define. The title is taken from Ecclesiastes 12:13, in the King James Version of the Bible: Let us hear the conclusion of the whole matter: Fear God, and keep his commandments: for this is the whole duty of man.The consensus view of modern scholars is that the likely author was Richard Allestree, but at the time of publication (towards the end of the Interregnum) the High Church tradition it represents was a politically dangerous position. The authorship was well concealed, and it has been noted that the work has been attributed to at least 27 people, beginning with Hammond himself.


== Other proposed authorships ==
Half a dozen other works appeared as by "the author of The Whole Duty of Man". A folio collection was published in 1684, edited by John Fell. Fell asserted that all the attributed works were from a single author.
One of the proposed candidates as author is Dorothy, Lady Pakington, under whose roof Hammond lived. In discussing her, Mary Hays noted as other such candidates Accepted Frewen, William Fulman, Richard Sterne, and Abraham Woodhead. Others mentioned in the Dictionary of National Biography article on Pakington by William Dunn Macray are Fell, Humphrey Henchman, William Chappell, and Obadiah Walker.A work with a similar title from 1673, The Whole Duty of Man According to the Law of Nature,  is an English translation of a Latin work on natural law by Samuel Pufendorf, and is unrelated.


== Influence ==
John Page's A Deed of Gift to my Dear Son of 1687 quotes heavily from The Whole Duty of Man.After the rupture in their close relationship at the end of 1710, Sarah Churchill, Duchess of Marlborough wrote to Queen Anne reminding her of "the directions of the author of C with relation to friendship".Samuel Johnson told Boswell that his mother made him read The Whole Duty of Man "from a great part of which I could derive no instruction." 


== Literary allusions ==
The Whole Duty of Man is mentioned in novels as a work typically to be found in small personal libraries. Examples are "a Family Bible, a "Josephus," and a "Whole Duty of Man", in The Mayor of Casterbridge by Thomas Hardy; "a shelf on which Mrs Julaper had her Bible, her Whole Duty of Man, and her Pilgrim's Progress", in The Haunted Baronet by Sheridan Le Fanu; and "the bookcases, where Fox's "Lives of the Martyrs" nestled happily beside "The Whole Duty of Man" in The Souls of Black Folk by W. E. B. Du Bois. Richard Brinsley Sheridan guyed it in The Rivals.Thomas Babington Macaulay wrote of William Sherlock's Treatise on Death that it "during many years, stood next to the Whole Duty of Man in the bookcases of serious Arminians". David Hume, a critic, wrote in his An Enquiry Concerning the Principles of Morals, that "I suppose, if Cicero were now alive, it would be found difficult to fetter his moral sentiments by narrow systems; or persuade him, that no qualities were to be admitted as virtues, or acknowledged to be a part of PERSONAL MERIT, but what were recommended by The Whole Duty of Man".Benjamin Franklin wrote, in a letter to his wife, regarding his daughter Sally, "I hope she continues to love going to church, and would have her read over and over again the Whole Duty of Man, and the Lady’s Library."


== References ==
Paul Elmen, Richard Allestree and The Whole Duty of Man, The Library 1951 s5-VI(1):19-27.


== Notes ==


== Further reading ==
R. C. Tennant, Christopher Smart and The Whole Duty of Man, Eighteenth-Century Studies, Vol. 13, No. 1 (Autumn, 1979), pp. 63–78.


== External links ==
The Whole Duty of Man at Google Books