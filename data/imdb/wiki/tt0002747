The Cheese Special is a 1913 American silent short comedy film featuring Max Asher and marking the film debut of Louise Fazenda. The scenario was written by Allen Curtis, but the identity of the director is unknown. It was the first production released by the newly formed Joker productions, as part of the Universal Film Manufacturing Company. The film is presumed to be lost and there is no published  synopsis of the film. Known production details state that it was shot on a beach resort and used a miniature train. The film had a wide release and was reviewed by The Moving Picture World as a low comedy suited for the burlesque theaters.


== Plot ==
No known script or plot of the film has surfaced, but the highlight of the film was summarized in the Corsicana Daily Sun as "a roaring Joker comedy in which Dauntless Dan stops the train with one hand and rescues the heroine with the other while the villain hisses between his teeth."


== Cast ==
Max Asher
Louise Fazenda as heroine/Schmaltz's daughter
Henry Mann as Schmaltz
Lee Morris
Bobby Vernon as Sylvion De Jardins


== Production ==
Few details are known about the production of the film, but it was shot on a beach resort and used a miniature train. Due to the lack of credits, both Louise Fazenda and Henry Mann's roles were provided by Photoplay, in response to an reader inquiry. The film was the first release by the new Joker productions, which was dedicated to producing only short comedy films. Kalton C. Lahue and Samuel Gill credit this production as being Louise Fazenda's film debut. Film historian Richard E. Braff notes that Allen Curtis as the writer, but provides no directorial credits. The single reel production was released on October 25, 1913.


== Reception ==
The new Joker line was billed as one of the "best comedies yet" by advertisers wanting to draw crowds to the theaters, but there may have been truth in the claims because it was cited as one of Max Asher's best roles in a 1914 edition of Moving Picture World. The film had a wide release with showings in Indiana, Kansas, Ohio, North Carolina, Maryland, Pennsylvania, Texas, and in Chicago, Illinois. The Moving Picture World review of the film noted that it was a low comedy suitable for the burlesque houses with "less particular audiences" than finer establishments because one of the minor characters spat frequently on camera, interrupting the humor and thus making it unfit for the best theaters. Another newspaper review claimed that it was one of the best comedies released in months, but ended up referring to the film as "The Chinese Special". The date of disappearance is unknown, but Universal destroyed its remaining copies of silent era films in 1948. The film is presumed to be lost.


== References ==


== External links ==
The Cheese Special on IMDb