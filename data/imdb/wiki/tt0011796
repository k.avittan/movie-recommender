Standard bed sizes are based on standard mattress sizes, which vary from country to country. Bed sizes also vary according to the size and degree of ornamentation of the bed frame. Dimensions and names vary considerably around the world, with most countries having their own standards and terminology. In addition, two mattresses with the same nominal size may still have slightly different dimensions, due to manufacturing tolerances, amount of padding, and support type. Beds are generally rectangular. More specialized shapes may be obtained by special order (for example circular beds). Mattress sizes may differ from bedding sizes.


== Factors and different uses ==
The choice of bed size may depend on:

The amount of available space inside the room.
Whether the bed is intended to be used by a single person or shared between two (or more).
Body size and shape, preferred sleeping position and personal preference (neither too large nor too small).Some recommend that the bed size should be at least 15 cm (6 in) longer than the height of the person sleeping in it, i.e. 200 cm (79 in) long for a 185 cm (73 in) tall person. Some recommend that a double bed should allow two persons enough room to both lie on their back with their hand behind their head without their elbows touching.


== Nomenclature ==
Naming standards on different bed sizes and their corresponding actual measurements can vary across national standards. Examples of such nomenclature are names like "Single", "Double", "Queen" or "King" size. Sometimes the naming standards are further divided by adding adjectives such as "Narrow", "Wide", "Extra Wide", "Long", "Extra Long" and so on, which also can vary across national standards.
For example, a King size bed may measure (in width by length):

150 cm × 200 cm (59 in × 79 in) in the UK.
165 cm × 203 cm (65 in × 80 in) in New Zealand.
180 cm × 190 cm (71 in × 75 in) in Portugal, but is also available in 195 and 200 cm (77 and 79 in) lengths.
180 cm × 200 cm (71 in × 79 in) in Indonesia.
183 cm × 191 cm (72 in × 75 in) in Singapore and Malaysia.
183 cm × 203 cm (72 in × 80 in) in Australia.
183 cm × 216 cm (72 in × 85 in) in India.
193 cm × 202 cm (76 in × 80 in) in the US.Because of the growth of international trade and the above potential cause of confusion, bed sizes are becoming more standardized. In most countries, using the International System of Units, bed dimensions most often appear in centimeters, and often only by width and length. In the United States, Canada, and some regions influenced by the former British Empire, dimensions are usually in inches.


== Europe ==

Europe may traditionally have had more variations in national bed size standards than any other part of the world, but in the recent years a few sizes have become more common than others. Bed sizes are defined in centimeters in all European countries, although supplementary Imperial equivalents are sometimes shown in the United Kingdom.
Today, the most common widths sold by pan European retailers are:

90 and 120 cm (35 and 47 in) for single beds.
160, 180 and 200 cm (63, 71 and 79 in) for double beds, with 180 cm (71 in) arguably being the most common.Other sizes are also available in some European countries (e.g., 80 or 140 cm [31 or 55 in]), but the widths listed above are the most common when looking at the European market as a whole. 135 cm (53 in) and 150 cm (59 in)  [nominal] are more typical in the UK and Ireland.
Today, the 200 cm (79 in) length is the most common bed length sold by pan-European retailers. Lengths of 210 or 220 cm (83 or 87 in) are often also available by special order. The longer beds are intended for taller people, and as a rule of thumb 210 cm (83 in) long beds may be recommended to people 190 cm (75 in) or taller, while 220 cm (87 in) beds are recommended for people over 200 cm (79 in) tall.


=== France ===
In France, single size beds are usually 90 cm × 190 cm (35 in × 75 in).
The most common sizes for double beds are:

140 cm × 190 cm (55 in × 75 in)
160 cm × 200 cm (63 in × 79 in)
180 cm × 200 cm (71 in × 79 in)


=== Italy ===
In Italy, beds are classified by name and use the term piazza as in "place" Standard sizes are:
Una piazza ("one place") or singolo ("single")
80 cm × 190 cm (31 in × 75 in)
90 cm × 190 cm (35 in × 75 in)Una piazza e mezza ("11/2 places")
120 cm × 190 cm (47 in × 75 in)Piazza e mezza francese ("French 11/2 places")
140 cm × 190 cm (55 in × 75 in)Due piazze ("two places") or letto matrimoniale ("matrimonial bed")
160 cm × 190 cm (63 in × 75 in)
180 cm × 190 cm (71 in × 75 in)Due to the popularity of imported beds (especially from IKEA), the 200 cm (79 in) length is becoming more common across all bed sizes.


=== Northern Europe ===
These sizes are for Germany, Poland, Belgium, Netherlands, Luxembourg, Norway, Sweden, Denmark, Iceland, Finland, Estonia, Latvia, and Lithuania. There are some variations between different countries, but these are the most common sizes:

Single
90 cm × 200 cm (35 in × 79 in). Extended variants are typically 210 cm-long (83 in).Three-quarter
140 cm × 200 cm (55 in × 79 in)Double
160 cm × 200 cm (63 in × 79 in)
180 cm × 200 cm (71 in × 79 in), most common double bed
200 cm × 200 cm (79 in × 79 in), a common extra-wide bed


=== Portugal ===
In Portugal, the most common widths for beds are:

Solteiro (single)
80 cm (31 in) or 90 cm (35 in)
Casal (double)
140 cm (55 in)
Queen size
160 cm (63 in)
King size
180 cm (71 in)
Super king size
200 cm (79 in)Beds are typically 190 cm (75 in) long, but 200 cm (79 in) are increasingly common.


=== Spain ===
In Spain, standardized lengths are 180, 190 and 200 cm (71, 75 and 79 in), with 190 cm (75 in) being the most common. Standardized widths are 80, 90, 105, 120, 135 or 150 cm (31, 35, 41, 47, 53 or 59 in). The most common bed sizes are:

Individual (single)
90 cm (35 in)
Matrimonio (marital)
135 or 150 cm (53 or 59 in)


=== Switzerland ===
Single
70 cm × 200 cm (28 in × 79 in), only used for double beds with two separate mattresses.
80 cm × 200 cm (31 in × 79 in), only used for double beds with two separate mattresses.
90 cm × 200 cm (35 in × 79 in), common single bed.
120 cm × 200 cm (47 in × 79 in), uncommon, mainly available as European-style futon beds that are sold with mattress, slats and frame.
140 cm × 200 cm (55 in × 79 in), common, especially among young people.Double
160 cm × 200 cm (63 in × 79 in), common.
180 cm × 200 cm (71 in × 79 in), common.
200 cm × 200 cm (79 in × 79 in).


=== UK and Ireland ===

In the United Kingdom and Ireland, beds are measured according to the size of mattress they hold, not the dimensions of the bed frame itself; bed frame sizes are not standardised and may differ between manufacturers. Below are the typical bed sizes from the National Bed Federation, which is the trade association for the majority of British and Irish bed manufacturers and their suppliers. Most manufacturers use a standard defined in feet and inches with the metric indicators not being exact equivalents.  There can legally be a tolerance of up to ±2 cm (0.8 in) in quoted measurements and the size of the mattress itself.
Some UK retailers may have a "queen" size that refers to one of the above standard sizes, but there is inconsistency around which size specifically, with both the small double and super king being prevalent.
Less common sizes include:
As well as UK standard sizes, common European sizes are also sometimes found in the UK from imports and IKEA. The typical length of an IKEA and other European mattresses is 200 cm (79 in), whereas UK lengths vary depending on the width of the mattress, being usually either 191 or 198 cm (75 or 78 in). Due to the popularity of imported beds (especially from IKEA), the 200 cm (79 in) length is becoming more common. In 2015, IKEA also started offering UK sizes in Britain alongside their standard European sizes.While many companies in the UK offer custom-made bed making services, most mass-produced bed linens, bed frames and mattresses fit only the standard bed sizes above.


== North America ==

The sizes of mattresses use non-numeric labels such as a "king" or "full", but are defined in inches. Historically most beds were "twins" or "doubles" but in the mid–1940s larger mattresses were introduced by manufacturers. These were later standardized as "queen" and "king", and first made a significant impact on the market in the 1950s and 60s. Standard mattress depth ranges from the "standard" size of 9 in (23 cm) to "high contour" of up to 13 in (33 cm). Below are the standard ISPA widths and heights in the United States and Canada.Note: While dimensions are specified to the half-inch below, the actual dimension used by manufactures or various websites may be half an inch larger or smaller.
Less common sizes include:

There are also a number of specialty sizes for specific use cases:
Bedding in the United States must state the type of bed it is intended for, in addition to the dimensions in both inches and centimeters.


== Oceania ==


=== Australia ===
The following bed sizes are available in Australia.


=== New Zealand ===
The following bed sizes are available in New Zealand.


== Africa ==


=== South Africa ===
In South Africa, bed sizes have standard lengths of either 188 or 200 cm (74 or 79 in), with the latter being called XL variants. XL mattresses have the same widths as their non-XL counterparts. The 200 cm (79 in) XL length is recommended for persons over 183 cm (6 ft) tall.
Single
92 cm (36 in) wide
Three-quarter
107 cm (42 in) wide
Double
137 cm (54 in) wide
Queen
152 cm (60 in) wide
King
183 cm (72 in) wide. Equal to the width of two single mattresses.
Super King
200 cm (79 in) wide. Available in XL length only.


== Asia ==


=== India ===
In India, the following sizes are standard:
Single
91 cm × 198 cm (36 in × 78 in)
Double
122 cm × 198 cm (48 in × 78 in)
Queen
152 cm × 198 cm (60 in × 78 in)
King
183 cm × 198 cm (72 in × 78 in)


=== China ===
In China, the following sizes are standard:
Lengths: 190, 195, 200, 210 cm (75, 77, 79, 83 in)
Single bed widths: 80, 90, 100, 110, 120 cm (31, 35, 39, 43, 47 in)
Double bed widths: 135, 140, 150, 180 cm (53, 55, 59, 71 in)In practice, bed sizes are usually categorized by the width. The length is typically 200 cm (79 in), but this may vary. The most common sizes are:
120 cm × 200 cm (47 in × 79 in)
150 cm × 200 cm (59 in × 79 in)
180 cm × 200 cm (71 in × 79 in)There are also other bed sizes available, but they are less common.


=== Indonesia ===
In Indonesia, the standard length is always 200 cm (79 in). The following market-standard sizes are commonly available in Indonesia:
Single
90 cm × 200 cm (35 in × 79 in)
Double or twin
120 cm × 200 cm (47 in × 79 in)
Queen
160 cm × 200 cm (63 in × 79 in)
King
180 cm × 200 cm (71 in × 79 in)
Super King
200 cm × 200 cm (79 in × 79 in)


=== Japan ===
Standard Japanese bedding sizes are described in the standard JIS S 1102:2017 Beds for domestic use by the Japanese Standards Association.


=== Taiwan ===
Traditionally, five sizes are common in Taiwan, locally known as 3 footer, 3.5 footer, 5 footer, 6 footer and 7 footer. They are also known as Taiwanese small single, twin, full, queen and king respectively.
However, American beds are also popular in Taiwan. Bedroom furniture and bedding of U.S. standard sizes can be purchased in major cities there.


== See also ==
Infant bed
Bed sheet


== References ==


== External links ==