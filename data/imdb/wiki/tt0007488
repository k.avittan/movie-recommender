Two Men of Sandy Bar is a 1916 American silent Western Melodrama directed by Lloyd B. Carleton and starring Hobart Bosworth, Gretchen Lederer along with Emory Johnson.
The film relies on a Bret Harte play penned in 1876. The film's main character is John Oakhurst, a character well known to the readers of Bret Harte's books. Oakhurst is an honest gambler whose compassion for others both wins him friends and causes hardships. A recurring theme in this film is using the bond of friendship to overcome life's obstacles. The film was released on April 3, 1916 by Universal.


== Plot ==
The plot unfolds during the time of the Gold Rush. Hobart Bosworth plays John Oakhurst, a gentleman gambler. While playing poker one night, Oakhurst meets a fellow gambler Sandy Morton, played by Emory Johnson. While the chivalrous Oakhurst is the consummate pro, Sandy Morton is the complete antithesis. Morton has a propensity towards excessive drink and is of low moral character. Despite these differences, they become fast friends.
Besides Sandy's daily struggles, he also has a troubled past. Sandy's father, Alexander Morton played by Frank MacQuarrie, owns a banking business. Old man Morton's fervent desire was to integrate Sandy into the family business. His father could not condone Sandy's lifestyle and disowned him. The conflict drove Sandy to leave home at an early age and drift west.
One day, John Oakhurst pulls up stakes. He boards a train for California. While traveling on the train, Oakhurst befriends an alcoholic gambler named John Pritchard Jack Curtis. Pritchard is traveling with his wife the Duchess played by Gretchen Lederer. Oakhurst discovers Pritchard is running from the authorities. Soon, a sheriff pursuing Pritchard catches up with him on the train. Oakhurst comes to the aid of his new acquaintance. A gunfight erupts, and Oakhurst wounds the sheriff. Pritchard flees. Before Pritchard departs, he tells Oakhurst to take care of the Duchess. Oakhurst and the Duchess finish their trip.
Both arrive in Sandy Bar. After settling in, Oakhurst finds out the Duchess is the Queen of the gambling halls. As time passes, the Duchess comes to admire Oakhurst's quick mental agility. She appreciates his physical skill with a pack of playing cards. The Duchess becomes captivated with Oakhurst.
One night as the Duchess glances around the gambling hall, she spots a new gambler who had drifted into town. She sashays to his table and finds out the young man name is Sandy Morton. While playing poker, Oakhurst looks up and also recognizes his friend. Oakhurst reunites with his old friend.

The latest news arrives in Sandy Bar. John Pritchard has died. Believing Pritchard has died, the Duchess feels the time is right for her and Oakhurst to get married. She asks Oakhurst to marry her, but Oakhurst refuses. The Duchess feels offended, and her spiteful alter ego emerges. One night, she exacts her revenge for Oakhurst's refusal. While Oakhurst is playing cards, she stacks his deck. The other players around the table soon believe they have caught Oakhurst cheating. The other gamblers call him out. They brand him a cheat. Oakhurst's reputation is in tatters; He drifts South to take refuge from the Sandy Bar gaming halls.
An outraged Morton can't forgive Oakhurst refusal to marry the Duchess. To atone for Oakhurst's rejection, Morton decides he will marry the Duchess. They become man and wife. After some time, the Duchess reveals her deceit with Oakhurst. The Duchess tells Morton about stacking the deck that destroyed Oakhurst's reputation. A furious Morton decides to parts ways with the duplicitous Duchess. He heads South hoping to locate his old friend Oakhurst.
Time passes, and we discover a contented Sandy Morton has settled down. Sandy has become a servant for a wealthy Southern California mine owner, Don Jose De Castro, played by William V. Mong.
John Oakhurst reenters the picture. While drifting south, he has settled in the same town as Sandy Morton. While playing poker one night, Oakhurst meets a lovely Spanish girl. They fall in love. The new love interest is Jovita played by Yona Landowska. Oakhurst wants to marry the beautiful Jovita and start a new life. A significant problem arises when Oakhurst discovers she is the daughter of Don Jose De Castro. Jovita father will not consent to the marriage of his daughter to a gambler. The couple continues to see each other in secret. They determine their only hope for marriage is to elope.
Time marches on, and we find out the Alexander Morton Sr. Sandy's father has reconsidered his sentiments about his son. He believes "time heals all wounds." Old Morton believes his son has had sufficient time to mend his unruly behaviors. He wants to welcome his son into the banking business. He has combed the country for years searching for his son. His search landed him in the same Spanish town as Oakhurst. Sandy Morton finds out his father is in town. He can't face him after all these years. He goes into hiding. Oakhurst finds out old man Morton is in town looking for his long-lost son. Oakhurst still wanting to protect his old friend hatches a plan.
Oakhurst believes Sandy Morton died years ago. If Oakhurst can convince the elder Morton, he is his son; the old man will take him into the banking business. If Oakhurst can become a respectable banker, he believes old man Castro will allow him to marry his daughter. He tells the senior Morton he is Sandy Morton. Since it's been 25 years, the elder Morton takes him at his word and embraces Oakhurst as his son. Oakhurst moves to San Francisco. Oakhurst becomes a banker. Oakhurst is now a member of the same bank where Don Castro does his business.
Sandy Morton finds out Oakhurst is impersonating him in his father's bank. Infuriated, he travels home and confronts his father with the truth. He reveals the entire deception. It stuns the elder Morton. Oakhurst feels the elder Morton will feel betrayed. With his head bent, Oakhurst turns to leave the room, but the elder Morton calls him back. Old Morton declares he will excuse the duplicity. He further declares that the bank will change its name to Alex. Morton, Sons, and Oakhurst. Oakhurst married Jovita and Sandy marries new girlfriend Mary Morris.


== Cast ==


== Production ==


=== Theme ===

The bonds of friendship are the constant theme pervading both the play and the film. Oakhurst represents the true friend who has your back when things go sideways. The struggles facing both Jack Oakhurst and Sandy Morton cements their friendship and ultimately turn their relationship into a life-long partnership.


=== Development ===

Bret Harte's (Bret  Hahrt) (1836–1902), was an American short-story writer and poet. His best works featured miners, gamblers, and other characters of the California Gold Rush. His career spanned more than four decades. Harte's books including The Luck of Roaring Camp, The Outcasts of Poker Flat and M'liss, helped fashion the standards for writing Western fiction. All three books were adapted into silent films.
Harte fashioned his play "Two Men of Sandy Bar" by drawing from his grab-bag of previously created western characters. The original 1876 play was first staged in Chicago. Further performances were then staged in New York and London. The play received mixed reviews. The New York Times observed - ". . . the plot of the play is simply an amplification of the story "A Passage in the Life of Mr. John Oakhurst" originally published in the Sunday edition of The Times.This film is a five reel filmization of the Bret Harte's 1876 play. This was the 4th time Hollywood had turned a Bret Harte story into a movie. After his death in 1902, they would turn 14 stories into films.

Olga Charlotte Printzlau wrote the scenario for this photoplay. Her scenario used most of the characters in the original play, but her pacing was different along with her sequence of events. Even then, it was still difficult to follow - see § Release and reception.
She was born in Philadelphia, Pennsylvania in 1891. She was  25 years old when she wrote the scenario for this film based on the Bret Harte book. Printzlau worked for various companies during her writing career. During the three years 1915 to 1918 she created Scenarios for Universal. Between 1915 and 1933, she wrote scenarios for 69 films. She died on July 8, 1962, in Hollywood, California.


=== Marketing ===
Based on an American Film Institute standard, films with a running time of forty-five minutes or longer are considered feature films. In 1915, feature films were becoming more the trend in Hollywood. In 1916, Universal formed a three-tier branding system for their releases. Universal films decided to label their films according to the size of their budget and status. Bear in mind, Universal, unlike the top-tier studios, did not own any theaters to market its feature films. By branding their product, Universal gave theater owners and audiences a quick reference guide. Branding would assist theater owners in making a judgment for films they were about to lease and help fans decide which movies they wanted to see.
Universal released three different types of feature motion pictures:
Red feather Photoplays – low budget feature films
Bluebird Photoplays – Mainstream feature release and more ambitious productions
Jewel – prestige motion pictures featuring high budgets using prominent actorsThis film carried Universal’s “Red Feather” brand, designating a low-budget feature film.


=== Pre-production ===

Hobart Van Zandt Bosworth was a well-known Leading man for Universal. He could play a variety of characters because of his vast experience on both stage and screen. Hobart was in his thirties when he made this movie. After Universal signed a 21-year-old Emory Johnson, Hobart thought he saw a potential mega-career for the 21-year-old. Hobart decided to mentor the young actor. After finishing The Yaqui (1916 film), they immediately went on to this movie. The relationship bore fruit and continued into the next decade.Hobart Bosworth, Emory Johnson, Jack Curtis, Gretchen Lederer, Yona Landowska and Charles H. Hickman had all worked together filming The Yaqui. They were all Universal contract actors. The transition to this film was relatively easy.


==== Location ====
Universal was committed to capturing the atmosphere of the early West depicted in the Bret Harte stories.
Location scouting determined San Diego, California would best serve this purpose. Universal shot all the film's exteriors in San Diego, California and the surrounding area.


=== Post-production ===
The theatrical release of this film totaled 5 reels. As is often the case, the listed time for this feature-length movie varies. At the time, the average time per 1,000-foot 35mm reel varied between 10 and 15 minutes per reel. Thus, the total time for this movie is computed between 50 and 75 minutes.


== Release and reception ==
The film was copyrighted on March 6, 1916 under the number LP7770, and officially released in US theaters on April 3, 1916.The critics liked this film especially Universal's use of picturesque settings, period costuming, romantic subject matter and strong characters. A sampling of reviews is shown below.


== Preservation status ==
A report published by the United States Library of Congress in September 2013 states that 70 percent of all American silent feature films are lost. According to the Library of Congress website, this film has a current status of  “No holdings located in archives,” thus it is presumed all copies of this film are lost.


== Anecdotes ==
To lend an air of authenticity to John Oakhurst character, Hobart Bosworth wore an old-fashioned set of Mexican Spurs. The 40-year-old spurs were silver mounted with large rowels. They were the property of one of the oldest Spanish family in the area. See the Spurs in the Gallery photo with the caption - "Oakhurst kissing the hand of Jovitas."
The article cited here opens with "The Universal leading ladies have been appearing lately in old fashioned gowns... " The article further states - "Gretchen Lederer in "Two Men of Sandy Bar" has taken infinite pains to reproduce the styles of the fifties in her customs and has succeeded admirably. Crinolines, introduced in the forties, had grown too large proportions. . . "  All of this can be seen in the photograph of Gretchen Lederer shown in the Gallery section.
Isadore Bernstein was a staff writer at Universal. In hopes of capturing the same inspiration Bret Harte felt when he wrote the original stage play, Bernstein traveled to Sonora, California, “Queen of the Southern Mines.” Once he arrived, he worked on the continuity for the film.


== Gallery ==


== See also ==
Bret Harte (May 10, 2018). Tale of the Argonauts. Aeterna Classics. pp. 30–. ISBN 978-3-96376-744-9.
Bret Harte (December 1, 2016). Two Men of Sandy Bar: A Drama. Floating Press. ISBN 978-1-77667-497-8.
I. G. Edmonds (1977). Big U: Universal in the silent days. A. S. Barnes, Incorporated. ISBN 978-0-498-01809-1.
Clive Hirschhorn (2000). The Universal Story. Hamlyn. ISBN 978-0-600-59736-0.


== References ==


== External links ==
Two Men of Sandy Bar on IMDb
Brooks, Noah (September 1902). "Bret Harte: A Biographical And Critical Sketch". Overland Monthly, and Out West Magazine. XL (3): 201–207. Retrieved June 4, 2019.
Anthony Slide (February 1, 2010). Silent Players: A Biographical and Autobiographical Study of 100 Silent Film Actors and Actresses. University Press of Kentucky. ISBN 978-0-8131-3745-2.