"Frozen Heart" is a song from the 2013 Disney animated film Frozen, with music and lyrics by Kristen-Anderson Lopez and Robert Lopez and performed in the film's prologue by a group of icemen.


== Production and writing ==
The Lopez songwriting duo explained that the song "has origins in a type of song used in past Disney films, like the 'Song of the Roustabouts' from Dumbo and 'Fathoms Below' from The Little Mermaid". Kristen said, "I guess we were in a meeting, and I kept saying: ‘if we could just have a song which basically said the ice is beautiful and dangerous and set up a little mystery'", while Robert added that the "masculine energy of the song establishes the expansiveness of the story". He said, "I think that’s why 'Fathoms Below' is in The Little Mermaid. It’s telling the boys this is going to be a story with songs, but there’s going to be something in it for everyone... It’s not just a princess movie. And Frozen isn't just a princess movie. It’s got a lot of action and fun and entertainment and stuff like that, and 'Frozen Heart' kind of tells you there’s going to be some violence in this story." From Director wrote, "The ‘Frozen Heart’ sequence plays like a fairy tale prophecy – a small story that brings ill tidings – and it's interesting to note that early drafts of the screenplay included a more explicit prophecy, hints of which can be heard in the song ‘Spring Pageant’ on the Deluxe Edition of the soundtrack album. The decision to drop this prophecy in favour of something less direct is one of a number of smart moves made by screenwriter and co-director Jennifer Lee."


== Synopsis ==

The song is sung by a group of ice harvesters who are cutting blocks of ice from a frozen lake. Throughout the song, Kristoff and  Sven try to join the adult harvesters, but are constantly shut out, and at the conclusion, try their best to emulate the harvesters. The harvesters pile the ice onto a massive horse-drawn ice sled, then ride off, under a night sky lit up by the Northern lights. The beat is supplied by the cutting noise as their saws cut through the ice, and gradually picks up as the song progresses.


=== Foreshadowing ===
Many of the lyrics supplied throughout the song foreshadow things to come in the movie, especially in regard to Anna's and Elsa's actions.  

At the end of "For the First Time in Forever (Reprise)" when Anna is struck in the heart by Elsa, an oboe is playing the melody for "Frozen Heart" in the background; specifically matching the lyrics "Cut through the heart, cold and clear / Strike for love and strike for fear!"
The line "And break the frozen heart" at the end of the first verse, foreshadowing Anna freezing solid in the climax, but freeing herself by choosing to save Elsa from Hans, rather than saving herself by kissing Kristoff.
The line "So cut through the heart, cold and clear / Strike for love, and strike for fear" also foreshadows that only true love can break/thaw a frozen heart.
The concluding "Beware the frozen heart..." foreshadows the fatal ice in Anna's heart, put there by Elsa accidentally. It is also intriguingly ambiguous on just who should beware: those who know the person with a frozen heart (Elsa), the person suffering from a frozen heart themselves (Anna), or someone who is coldhearted (Hans).


== Composition ==
"Frozen Heart" is the opening number of Frozen, and is "a mood-establishing tune sung by workers cutting through ice". It is in Dorian mode. The motif from Frozen Heart "plays just after Elsa strikes Anna's heart with ice after the reprise to "For the First Time in Forever", reinforcing the theme of frozen hearts. In the opening song, they warn: "beware the frozen heart". The Meaning of Repentance argues "This is a foreshadow of things to come, as we face this concept in multiple ways throughout the plot". WeirdArtBrown argues the song is in the tradition of the "Opening with a choral arrangement, preferably a work song" genre, seen in such songs as: "Virginia Company" in Pocahontas, “Fathoms Below” from The Little Mermaid, “The Ballad of Sweeney Todd" from Sweeney Todd, and “Look Down" from Les Miserables. The site describes this song as "a throaty chant sung by an anonymous group of men harvesting ice".
HomebodyAbroad noted "Each of these songs, except the opening "Frozen Heart" sequence, are presented from flawed characters who don't quite have everything in place." It argues "The opening song of the movie works like the chorus of an ancient Greek play; the nameless characters pour onto the screen and present the thematic elements of the play". It notes the line "And break the frozen heart" is potent as it foreshadows in an unsubtle manner the metaphorical and literal frozen hearts of Elsa and Anna respectively. "Strike for love and strike for fear" is also an important line as "Love and fear are the two counter-balances of this whole story. This is the core theme of the movie, and a good paradigm for viewing much of life." Barnabas File said "it foreshadows the key themes of the film—the beauty and danger of ice (the created order) as well as the tension between love and fear (the human condition)". From Director described it as "a song that’s much more than it seems", adding "‘Frozen Heart’ is a surprisingly violent song to begin a Disney Princess film with".


== International versions ==
When the movie was first released in 2013, it numbered 42 versions worldwide, to which 3 more where added in the following years, raising the number of official versions to 45.The Korean language version sung by Kim Cheol-han, Park Sang-jun, Lee Sang-ik and Lee Jae-ho appeared on the Gaon Music Chart's download sub-chart; however, it did not appear on the main Gaon Singles Chart. The Italian version, along with the whole Italian adaptation of the movie, was awarded best foreign dubbing worldwide by Disney.


== Critical reception ==
GeeksOfDoom wrote "The brute voices behind this opening track, all unified and macho in the vein of “I'll Make a Man Out of You” from Mulan and “Song of Mor’Du” from Brave, are strong in vocal quality and attitude. “This icy force, both foul and fair, has a frozen heart worth mining,” they chant. The song swings in an entrancing motion as the men warn about the perilous ice". The site also deemed it a TOP 5 TRACK from the film, along with two songs and two score pieces. Rochester City newspaper said "Both the album opener “Frozen Heart” and the character-establishing “Do You Want to Build a Snowman?” deeply resemble Disney's song output under Alan Menken (Beauty and the Beast, Aladdin, The Little Mermaid”) and that helps them feel instantly familiar". The Kilt wrote "The first two songs in the album, “Frozen Heart” and “Do You Want to Build a Snowman,” give the listener a basic understanding of what Frozen is about", and said of the former: "“Frozen Heart” is a dark, but lively tune that represents the beauty, danger and power of ice. It has a chaotic, yet beautiful and clever mix of exciting Scandinavian folk and sinister orchestral music." DadInACape wrote "“Frozen Heart” starts the film off strong with a solid, sea-chanty-esque rhythm".


== Charts ==


== References ==