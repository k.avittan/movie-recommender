Cottage cheese is a fresh cheese curd product with a mild flavor. It is also known as curds and whey. It is not aged. It is made by draining the cheese, as opposed to pressing it to make Paneer—retaining some of the whey, keeping the curds loose. An important step in the manufacturing process distinguishing cottage cheese from other fresh cheeses is the adding of a "dressing" to the curd grains, usually cream, which is largely responsible for the taste of the product.
Cottage cheese is low in calories compared to other types of cheese, making it popular among dieters and some health devotees. It can be used with a wide variety of foods such as yogurt, fruit, toast, granola, in salads, as a dip, and as a replacement for mayonnaise.


== History ==


=== Origin ===
A popular story on the origin of cheese was taken from Homer's Odyssey, in which the poet describes how the Cyclops Polyphemus made cheese by storing milk in animal stomachs. The enzymes from the stomach would have induced a coagulation process separating the curds from the milk.Cheese is thought to have originated in the Middle East around 5,000 BC. Evidence of cheese can be found in a band of carvings on the walls of an ancient Mesopotamian temple that date back to 3,000 BC. The ancient carvings show the process in which the civilization created a cheese-like substance, using salt and milk to create a salty sour curd mixture believed to be somewhat similar to today's cottage cheese. As Rome expanded its empire, they spread the knowledge of cheese, discovering many new forms of it. The Spanish brought cows and the knowledge of how to craft cheese to the Americas by 1493.


=== Popularization ===
In late 19th century Minnesota, when milk went sour, farmers sometimes made something they called "Dutch cheese", which is said to have been similar to modern industrial cottage cheese, in order not to waste the bad milk. In the early 20th century farmers in northeastern British Columbia made something they called "homesteader's cheese", which is said to have been similar to modern industrial cottage cheese (a "Dutch cheese" also existed there at the time, but this was something else). The term cottage cheese first began to be used for such simple home-made cheese in America in the mid-19th century.The first American cheese factory opened in 1868, beginning the wholesale cheese industry. Popularity in the United States of industrial cheese in general increased greatly at the end of the 19th century; by the turn of the century, farm production of cheese had become significant.

Cottage cheese was widely promoted in America during the First World War, along with other dairy products, to save meat for infantry rations. This promotion was shown in many war posters, including one which claimed that one pound of cottage cheese contains more protein than a pound of lamb, pork, beef, or chicken.
After the First World War, cottage cheese quickly became more popular. Thirty million pounds of cottage cheese was produced in 1919 (out of 418 million pounds of cheese in general in 1920), but by 1928 87 million pounds was manufactured. Consumption peaked in the United States in the 1970s when dieting became popular and some $1.3 billion of it was sold per year, but in the 1980s yogurt became more popular and sales dropped considerably further in the 2000s.In 2016, a Wall Street Journal article theorized that cottage cheese might be ready for a resurgence following the popularity of Greek yogurt due to its high levels of protein and low levels of sugar.


== Manufacture ==
Since the 1930s, industrial cottage cheese has been manufactured using pasteurized skim milk, or in more modern processes using concentrated nonfat milk or reconstituted nonfat dry milk. A bacterial culture that produces lactic acid (Lactococcus lactis ssp. lactis or L. lactis ssp. cremoris strains such as are usually used) or a food-grade acid such as vinegar is added to the milk, which allows the milk to curdle and parts to solidify, and it is heated until the liquid reaches 142–143 °F (61–62 °C), after which it is cooled to 90 °F (32 °C). The solids, known as curd, form a gelatinous skin over the liquid (known as whey) in the vat, which is cut into cubes with wires, allowing more whey to drain from the curds. The curds are then reheated to 120 °F (49 °C) for one or two hours. In Iowa in the early 1930s, hot water was poured into the vat, which further firms the curds. Once the curds have been drained and are mostly dry the mass is pressed to further dry the curds. The curds are then rinsed in water. Finally, salt and a "dressing" of cream is added, and the final product is packaged and shipped for consumption. Some modern manufactures add starches and gums to stabilize the product. Some smaller modern luxury creameries omit the first heating step but allow the milk to curdle much longer with bacteria to produce the curds, or use crème fraîche as dressing.Cottage cheese made with a food-grade acid must be labelled as "Direct Acid set".Usually, a small amount of low CO2-producing citrate-fermenting lactococci or leuconostoc bacterial strains are added to the starter mix for the production of diacetyl for added buttery or creamy flavors. Producers must be careful that the final product contains approximately 2 ppm diacetyl, and that the ratio of diacetyl to acetaldehyde is 3–5 to 1, to achieve the typical cottage cheese flavor. Too small a ratio and the product tastes grassy, too much and the taste becomes harsh.Cottage cheese is naturally a yellow colour due to the cream dressing, but to increase consumer acceptance and appeal of the final product titanium dioxide is usually added to the dressing to make it a brilliant white colour and enhance marketability of the finished product. In the United States the FDA allows the additive in many dairy products (not whole milk) up to 1% of total volume by weight, but it must be labelled in the ingredient list. It may also be used in Canada and the European Union. Relatively recently producers have added this ingredient in nanoparticle form. In the USA the FDA does not restrict nanoparticle technology usage in food, but in Europe it must be first submitted for approval as a food ingredient. According to the Project on Emerging Nanotechnologies it is found in hundreds of products, not always labelled as such, including many organic products, however a number of large US producers have denied using it.Cottage cheese may be marketed as a small-curd (<4 mm diameter) or large-curd (>8 mm diameter).


== Nutrition ==
Cottage cheese is popular among dieters and some health food devotees. It is also relatively popular among bodybuilders and athletes for its high content of casein protein while being relatively low in fat. Pregnant women are advised that cottage cheese is safe to eat, in contrast to some cheese products that are not recommended during pregnancy.The sour taste of the product is due to lactic acid, which is present at 124–452 mg/kg. Formic, acetic, propionic and butyric acid contribute to the aroma.


== Consumption ==
In the United States, cottage cheese is popular in many culinary dishes. It can be combined with fruit and sugar, salt and pepper, fruit purée, tomatoes, or granola and cinnamon. It can be eaten on toast, in salads, as a chip dip, as a replacement for mayonnaise in tuna salad, and as an ingredient in recipes such as jello salad and various desserts. Cottage cheese is also popular with fruit, such as pears, peaches, or mandarin oranges. Cottage cheese is sometimes used as a substitute for meat because it has high levels of protein, but fewer total calories and less fat than meats contain. Many recipes can use cottage cheese.


== See also ==
Cottage cheese boycott, a consumer boycott in 2011 in Israel against the rise of food prices
Faisselle, a French cheese, similar to cottage cheese
Fromage blanc, a soft French cheese
Mascarpone, an Italian cheese made from cream, coagulated with citric acid or acetic acid
Chhena, an Indian cheese, similar to cottage cheese
Paneer, another Indian cheese similar to Chhena
Ricotta, an Italian whey cheese
Quark, a European curd and cheese
Queso fresco, a Spanish and Latin American soft cheese


== References ==


== External links ==
 Media related to Cottage cheese at Wikimedia Commons