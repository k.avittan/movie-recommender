Old Maid is a Victorian card game for two or more players probably deriving from an ancient gambling game in which the loser pays for the drinks. The game includes an element of bluffing.


== History ==
The rules of the game are first recorded in 1884 by Green  and referred to in Bazaar, Exchange and Mart in 1883 as a "newly invented game". However, it may well be much older and derived from the French game of Vieux Garçon, whose rules first appear in 1853, or from the German game of Black Peter whose rules are recorded as early as 1821. All these games are probably ancient and derived from simple gambling games in which the aim was to determine a loser who had to pay for the next round of drinks (c.f. drinking game). They originally employed a pack of 32 or 52 French cards, the Queen of Diamonds or Jack of Spades typically being the odd card and the player who is last in and left holding a single Queen or Jack becoming the "old maid", "vieux garçon" or "black Peter" depending on the game.


== Rules ==
There are retail card decks specifically crafted for playing Old Maid, but the game can just as easily be played with a standard 52-card deck. When using a regular deck, a card is either added or removed, resulting in one unmatchable card. The most popular choices are to remove the ace or queen of hearts or to add a single joker. It is also possible to remove one card face-down from the top of the deck before hands are dealt; if this is done, players will not know which card is unmatchable. The unmatchable card becomes the "old maid," and whoever holds it at the end of the game is the loser.
The dealer shuffles and deals all of the cards to the players, one card at a time. Some players may have one or two more cards than others; this is acceptable. Players look at their cards and discard any pairs they have (e.g., two kings, two sevens, etc.) face up. Players do not discard three of a kind. In common variants, the suit colors of a discarded pair must match: spades (♠) must match with clubs (♣) and diamonds (♦) must match with hearts (♥).
Beginning with the dealer, each player takes turns offering their hand face-down to the person on their left. That person selects a card without looking and adds it to their hand. This player then sees if the selected card makes a pair with any of their original cards. If so, the pair is discarded face up as well. The player who just took a card then offers their hand to the person on their left, and so on. 
The objective of the game is to continue to take cards, discarding pairs, until no more pairs can be made. The player with the card that has no match is "stuck with the old maid" and loses.  When playing with more than two players, the game is somewhat unusual in that it has one distinct loser rather than one distinct winner.


== Scabby Queen ==
Scabby Queen is Old Maid with played with a standard pack of cards from which the Queen of Clubs has been removed. The player left with the 'Scabby Queen' (Queen of Spades) is the loser and receives a number of raps on the knuckles with the edge of the pack. The number of raps is decided by reshuffling the pack and getting the loser to draw a card. They get the number of raps based on the face value of the card or, if it is a Jack or King, 10 raps, if it is a Queen, 21 raps. If the loser draws a red card he or she receives soft raps; if a black card, hard raps.


== Black Peter ==

The equivalent game in many European countries is known (in each country's own language) as "Peter" or "Black Peter", and is played with special cards, typically 31 or 37, in which the odd one out is typically a chimney sweep or a black cat. The game can also be played with a standard, 32-card, pack from which a black Jack is removed. The loser often gets a smudge on his or her face with a piece of soot or piece of burnt cork.


== Variants ==
In some variants, all players discard only after the dealer has had their turn to take a card.
Alternatively, play can proceed in reverse order, with players taking a new card before giving one up. In this variation, players can be stuck in "old maid purgatory," i.e. with one card and no way to get rid of it.
Jackass. A variant played in Trinidad. Remove the Jack of Diamonds; the Jack of Hearts is then the odd card. The player left holding it is the 'Jackass'.
A variant in East Asia is called baba-nuki (ババ抜き, "old maid") in Japan and dodukjapki (도둑잡기, "catching the thief") in Korea. It is played exactly as old maid, but instead of removing a queen or any other card, a joker is added, and player who is left with it loses.
A variant played in the Philippines, is called ungguy-ungguyan. The game is played as old maid except any card can be removed at the start of the game. That card is revealed at the end of the game and the person left with its "partner" (the odd card) loses and is called unggoy (Tagalog for monkey). A similar variant exists in Indonesia by the name of "Kartu Setan" which literally translates to "Ghost Card", and in Japan by the name of jiji-nuki (ジジ抜き, "old man").
In Brazil, two variants of the "Old Maid" game are played: One called "Fedor", literally "Stink", played with a regular deck out of which one card has been removed; the other one, played with a specialty deck is called "Jogo do Mico", or "Capuchin Monkey Game".  The cards depict animals, each one having a male and a female card representation; only the capuchin monkey (mico) is unpaired.


== See also ==
Donkey
Happy Families
Hearts


== References ==


== External links ==
Rules of Card Games: Old Maid on Pagat.com