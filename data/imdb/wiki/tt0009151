The standard 52-card deck of French-suited playing cards is the most common pack of playing cards used today. In English-speaking countries it is the only traditional pack used for playing cards; in many countries of the world, however, it is used alongside other traditional, often older, standard packs with different suit symbols and pack sizes. The most common pattern worldwide and the only pattern commonly available in Britain and America is the English pattern pack. The second most common is the Belgian-Genoese pattern, designed in France, but whose use spread to Spain, Italy, the Ottoman Empire, the Balkans and much of North Africa and the Middle East. In addition to those, there are other major international and regional patterns.


== Composition ==
A standard 52-card pack comprises 13 ranks in each of the four French suits: clubs (♣), diamonds (♦), hearts (♥) and spades (♠), with reversible (double-headed) court cards (face cards). Each suit includes an Ace, a King, Queen and Jack, each depicted alongside a symbol of its suit; and numerals or pip cards from the Deuce (Two) to the Ten, with each card depicting that many symbols (pips) of its suit. Anywhere from one to six (most often two or three since the mid-20th century) Jokers, often distinguishable with one being more colourful than the other, are added to commercial decks, as some card games require these extra cards.


== Design ==

The most popular standard pattern of the French deck is the English pattern (pictured below), sometimes referred to as the International pattern or Anglo-American pattern. The second most common is the Belgian-Genoese pattern, which was designed in France for export and spread to Spain, Italy, the Ottoman Empire, the Balkans and much of North Africa and the Middle East. There are also numerous others such as the Berlin pattern, Nordic pattern, Dondorf Rhineland pattern (pictured right) and the variants of the European pattern.
Modern playing cards carry index labels on opposite corners or in all four corners to facilitate identifying the cards when they overlap and so that they appear identical for players on opposite sides. For the court cards, this comprises the initial letter or letters from the name of that card. In English countries they are lettered A, K, Q and J for Ace, King, Queen and Jack. In other countries the letters may vary. Germany uses A, K, D and B (As, König, Dame and Bube); Russia uses T, K, D and B (Tuz, Korol, Dama and Valet); Sweden uses E, K, D and Kn (Ess, Kung, Dam and Knekt) and France uses 1, R, D, V (1, Roi, Dame, and Valet).
Although French-suited, 52-card packs are the most common playing cards used internationally, there are many countries or regions where the traditional pack size is only 36 (Russia, Bavaria) or 32 (north and central Germany, Austria) or where regional cards with smaller packs are preferred for many games. For example, 40- or 48-card Italian-suited packs are common in Italy; 40- and 48-card Spanish-suited packs on the Iberian peninsula; and 36-card German-suited packs are very common in Bavaria and Austria. In addition, tarot cards are required for games such as French tarot (78 cards), which is widely played in France, and the Tarock family of games (42 or  54 cards) played in countries like Austria and Hungary.


== History ==

The English pattern pack originated in Britain which was importing French playing cards from Rouen and Antwerp by 1480. The earliest cards of the English pattern date to around 1516. But Britain only started manufacturing its own cards towards the end of the 16th century, when card production began in London. These were based on the Rouen pattern, but unlike the traditional French cards, they dropped the names on the court cards. The English pattern evolved, in the process losing "some of its Rouen flavour and elegance and became more and more stylised. The figures took more space in the cards and many details were distorted."All early cards of this type were single-headed, but around 1860, the double-headed cards, universally used on modern decks, appeared. Corner indices were added around 1880. During the 19th century, the English pattern spread all over the world and is now used almost everywhere, even in countries where traditional patterns and other suits are popular. In America, the English pattern was copied onto wider cards.The fanciful design and manufacturer's logo commonly displayed on the ace of spades began under the reign of James I of England, who passed a law requiring an insignia on that card as proof of payment of a tax on local manufacture of cards. Until August 4, 1960, decks of playing cards printed and sold in the United Kingdom were liable for taxable duty and the ace of spades carried an indication of the name of the printer and the fact that taxation had been paid on the cards. The packs were also sealed with a government duty wrapper.


== Size of the cards ==
In the United States, standard playing cards are available in both "wide" and "narrow" sizes, referred to by one manufacturer as either "poker" or "bridge" sized; nominal dimensions are summarized in the adjacent table. However, there is no formal requirement for precise adherence and minor variations are produced by various manufacturers in different countries. In Germany, for example, standard Poker and Rummy packs by ASS Altenburger and Ravensburger measure 92 × 59 mm. Austria's Piatnik sells packs marketed for Bridge, Poker and Whist measuring 89 × 58 mm; while Britain's Waddingtons produce generic packs sized at 88 × 58 mm.
The slightly narrower cards are more suitable for games such as bridge and some types of poker, where a number of cards must be held or concealed in a player's hand. In most U.S. casino poker games, plastic bridge (narrow) sized cards are used; this is for both ease of use and dealing, and the plastic cards last much longer than paper decks. U.S. casino shuffling machines have traditionally been designed for bridge-size (narrow) cards for these reasons. In other table games, such as 21 (blackjack), a modern casino may use hundreds or even thousands of decks per day, so paper cards are used for those, for economic reasons. Poker-size (wide) paper decks are used for 21 and other similar games. Other sizes are also available, such as a smaller 'patience' size (usually 1 3⁄4 × 2 3⁄8 in or 44 × 60 mm) and larger 'jumbo' ones for card tricks. 
The thickness and weight of modern playing cards are subject to numerous variables related to their purpose of use and associated material design for durability, stiffness, texture and appearance.


== Markings ==
Some decks include additional design elements. Casino blackjack decks may include markings intended for a machine to check the ranks of cards, or shifts in rank location to allow a manual check via an inlaid mirror. Many casino decks and solitaire decks have four indices instead of just two. Some modern decks have bar code markings on the edge of the face to enable them to be sorted by machine (for playing duplicate bridge, especially simultaneous events where the same hands may be played at many different venues). Some decks have large indices for clarity. These are sometimes sold as 'seniors' cards' for older people with limited eyesight, but may also be used in games like stud poker, where being able to read cards from a distance is a benefit and hand sizes are small. 


== Four-colour packs ==

The standard French-suited pack uses black for the spades and clubs, and red for the hearts and diamonds. However, some packs use four colours for the suits in order to make it easier to tell them apart. There are several schemes: a common one is the English Poker format with black spades (♠), red hearts (♥), blue diamonds (♦) and green clubs (♣). Another common system is based on the German suits and uses green spades (♠) and yellow diamonds (♦) with red hearts (♥) and black clubs (♣).


== Nomenclature ==
When giving the full written name of a specific card, the rank is given first followed by the suit, e.g., "ace of spades" or "Ace of Spades". Shorthand notation may reflect this by listing the rank first, "A♠"; this is common usage when discussing poker; but it is equally common in more general sources to find the suit listed first, as in "♠K" for a single card or "♠AKQ" for multiple cards. This common practice when writing about bridge as it helps differentiate between the card(s) and the contract (e.g. "4♥", a contract of four hearts). Tens may be either abbreviated to T or written as 10.


== Nicknames ==

Though specific design elements of the court cards are rarely used in play and many differ between designs, a few are notable. 

Face cards or court cards – jacks, queens and kings.
One-eyed Royals – the jack of spades and jack of hearts (often called the "one-eyed jacks") and the king of diamonds are drawn in profile; therefore, these cards are commonly referred to as "one-eyed". The rest of the courts are shown in full or oblique face.
The jack of diamonds is sometimes known as "laughing boy".
Wild cards – When deciding which cards are to be made wild in some games, the phrase "acey, deucey or one-eyed jack" (or "deuces, aces, one-eyed faces") is sometimes used, which means that aces, twos, and the one-eyed jacks are all wild.
Suicide kings – The king of hearts is typically shown with a sword behind his head, making him appear to be stabbing himself. Similarly, the one-eyed king of diamonds is typically shown with an ax behind his head with the blade facing toward him. These depictions, and their blood-red colour, inspired the nickname "suicide kings".
The king of diamonds is traditionally armed with an ax, while the other three kings are armed with swords; thus, the king of diamonds is sometimes referred to as "the man with the axe". This is the basis of the trump "one-eyed jacks and the man with the axe". Poker may be played with wild cards, often "Aces, Jacks, and the King with the Axe".
The ace of spades, unique in its large, ornate spade, is sometimes said to be the death card or the picture card, and in some games is used as a trump card.
The queen of spades usually holds a sceptre and is sometimes known as "the bedpost queen", though more often she is called the "black lady". She also is the only queen facing left.
In many decks, the queen of clubs holds a flower. She is thus known as the "flower queen", though this design element is among the most variable; the Bicycle Poker deck depicts all queens with a flower styled according to their suit.
"2" cards are also known as deuces.
"3" cards are also known as treys.


== Combinations ==
It has been shown that because of the large number of possibilities from shuffling a 52-card deck (52!, equaling roughly 8.0658×1067 or 80,658 vigintillion possibilities), it is probable that no two fair card shuffles have ever yielded exactly the same order of cards.


== Unicode ==
As of Unicode 7.0, playing cards are now represented. Note that the following chart ("Cards", Range: 1F0A0–1F0FF) includes cards from the Tarot Nouveau deck, as well as the standard 52-card deck.


== See also ==
500 decks come with extra ranks.
French playing cards
German playing cards
Italian playing cards
Spanish playing cards
Stripped decks come with fewer ranks.
Tarot Nouveau, the most common French-suited tarot game deck


== Notes ==


== References ==