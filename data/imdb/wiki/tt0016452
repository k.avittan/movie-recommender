The genre of travel literature encompasses outdoor literature, guide books, nature writing, and travel memoirs.One early travel memoirist in Western literature was Pausanias, a Greek geographer of the 2nd century AD. In the early modern period, James Boswell's Journal of a Tour to the Hebrides (1786) helped shape travel memoir as a genre.


== History ==

Early examples of travel literature include Pausanias' Description of Greece in the 2nd century CE, Safarnama (book of Travels) of Nasir Khusraw (1003-1077) the Journey Through Wales (1191) and Description of Wales (1194) by Gerald of Wales, and the travel journals of Ibn Jubayr (1145–1214) and Ibn Battuta (1304–1377), both of whom recorded their travels across the known world in detail. The travel genre was a fairly common genre in medieval Arabic literature.Il Milione, The Travels of Marco Polo, describing Marco Polo's travels through Asia between 1271 and 1295 is a classic of travel literature.Travel literature became popular during the Song dynasty (960–1279) of medieval China. The genre was called 'travel record literature' (youji wenxue), and was often written in narrative, prose, essay and diary style. Travel literature authors such as Fan Chengda (1126–1193) and Xu Xiake (1587–1641) incorporated a wealth of geographical and topographical information into their writing, while the 'daytrip essay' Record of Stone Bell Mountain by the noted poet and statesman Su Shi (1037–1101) presented a philosophical and moral argument as its central purpose.One of the earliest known records of taking pleasure in travel, of travelling for the sake of travel and writing about it, is Petrarch's (1304–1374) ascent of Mount Ventoux in 1336. He states that he went to the mountaintop for the pleasure of seeing the top of the famous height. His companions who stayed at the bottom he called frigida incuriositas ("a cold lack of curiosity"). He then wrote about his climb, making allegorical comparisons between climbing the mountain and his own moral progress in life.
Michault Taillevent, a poet for the Duke of Burgundy, travelled through the Jura Mountains in 1430 and recorded his personal reflections, his horrified reaction to the sheer rock faces, and the terrifying thunderous cascades of mountain streams. Antoine de la Sale (c. 1388–c. 1462), author of Petit Jehan de Saintre, climbed to the crater of a volcano in the Lipari Islands in 1407, leaving us with his impressions. "Councils of mad youth" were his stated reasons for going. In the mid-15th century, Gilles le Bouvier, in his Livre de la description des pays, gave us his reason to travel and write:

Because many people of diverse nations and countries delight and take pleasure, as I have done in times past, in seeing the world and things therein, and also because many wish to know without going there, and others wish to see, go, and travel, I have begun this little book.By the 16th century accounts to travels to India and Persia had become common enough that they had been compiled into collections such as the Novus Orbis ("New World") by Simon Grynaeus, and collections by Ramusio and Richard Hakluyt. In 1589, Hakluyt (c. 1552–1616) published Voyages. 16th century travelers to Persia included the brothers Robert Shirley and Anthony Shirley, and for India Duarte Barbosa, Ralph Fitch, Ludovico di Varthema, Cesare Federici, and Jan Huyghen van Linschoten.In the 18th century, travel literature was commonly known as the book of travels, which mainly consisted of maritime diaries. In 18th-century Britain, almost every famous writer worked in the travel literature form. Captain James Cook's diaries (1784) were the equivalent of today's best-sellers. Alexander von Humboldt's Personal narrative of travels to the equinoctial regions of America, during the years 1799–1804, originally published in French, was translated to multiple languages and influenced later naturalists, including Charles Darwin.
Other later examples of travel literature include accounts of the Grand Tour. Aristocrats, clergy, and others with money and leisure time travelled Europe to learn about the art and architecture of its past. One tourism literature pioneer was Robert Louis Stevenson (1850–1894) with An Inland Voyage (1878), and Travels with a Donkey in the Cévennes (1879), about his travels in the Cévennes (France), is among the first popular books to present hiking and camping as recreational activities, and tells of commissioning one of the first sleeping bags.Other notable writers of travel literature in the 19th century include the Russian Ivan Goncharov, who wrote about his experience of a tour around the world in Frigate "Pallada" (1858), and Lafcadio Hearn, who interpreted the culture of Japan with insight and sensitivity.The 20th century's interwar period has been described as a heyday of travel literature when many established writers such as Graham Greene, Robert Byron, Rebecca West, Freya Stark, Peter Fleming and Evelyn Waugh were traveling and writing notable travel books.In the late 20th century there was a surge in popularity of travel writing, particularly in the English-speaking world with writers such as Bruce Chatwin, Paul Theroux, Jonathan Raban, Colin Thubron, and others. While travel writing previously had mainly attracted interest by historians and biographers, critical studies of travel literature now also developed into an academic discipline in its own right.


== Travel books ==

Travel books come in styles ranging from the documentary, to the literary, as well as the journalistic, and from memoir to the humorous to the serious. They are often associated with tourism and include guide books. Travel writing may be found on web sites, in periodicals, on blogs and in books. It has been produced by a variety of writers, including travelers, military officers, missionaries, explorers, scientists, pilgrims, social and physical scientists, educators, and migrants. 
Travel literature often intersects with essay writing, as in V. S. Naipaul's India: A Wounded Civilization (1976), whose trip became the occasion for extended observations on a nation and people. This is similarly the case in Rebecca West's work on Yugoslavia, Black Lamb and Grey Falcon (1941).Sometimes a writer will settle into a locality for an extended period, absorbing a sense of place while continuing to observe with a travel writer's sensibility. Examples of such writings include Lawrence Durrell's Bitter Lemons (1957), Bruce Chatwin's widely acclaimed In Patagonia (1977) and The Songlines (1987), Deborah Tall's The Island of the White Cow: Memories of an Irish Island (1986), and Peter Mayle's best-selling A Year in Provence (1989) and its sequels.
Travel and nature writing merge in many of the works by Sally Carrighar, Gerald Durrell and Ivan T. Sanderson. Sally Carrighar's works include One Day at Teton Marsh (1965), Home to the Wilderness (1973), and Wild Heritage (1965). Gerald Durrell's My Family and Other Animals (1956) is an autobiographical work by the British naturalist. It tells of the years that he lived as a child with his siblings and widowed mother on the Greek island of Corfu between 1935 and 1939. It describes the life of the Durrell family in a humorous manner, and explores the fauna of the island. It is the first and most well-known of Durrell's "Corfu trilogy", together with Birds, Beasts, and Relatives and The Garden of the Gods (1978). 
Ivan T. Sanderson published Animal Treasure, a report of an expedition to the jungles of then-British West Africa; Caribbean Treasure, an account of an expedition to Trinidad, Haiti, and Surinam, begun in late 1936 and ending in late 1938; and Living Treasure, an account of an expedition to Jamaica, British Honduras (now Belize) and the Yucatán. These authors are naturalists, who write in support of their fields of study. 
Another naturalist, Charles Darwin, wrote his famous account of the journey of HMS Beagle at the intersection of science, natural history and travel.A number of writers famous in other fields have written about their travel experiences. Examples are Samuel Johnson's A Journey to the Western Islands of Scotland (1775); Charles Dickens' American Notes for General Circulation (1842); Mary Wollstonecraft's Letters Written during a Short Residence in Sweden, Norway, and Denmark (1796); Hilaire Belloc's The Path To Rome (1902); D. H. Lawrence's Twilight in Italy and Other Essays (1916); Mornings in Mexico and Other Essays (1927); Rebecca West's Black Lamb and Grey Falcon (1941); and John Steinbeck's Travels with Charley: In Search of America (1962).


=== Contemporary writers of travel books ===
Englishmen Eric Newby, H. V. Morton, the Americans Bill Bryson and Paul Theroux, and Welsh author Jan Morris are or were widely acclaimed as travel writers (though Morris has frequently claimed herself as a writer of 'place' rather than travel per se).Bill Bryson in 2011 won the Golden Eagle Award from the Outdoor Writers and Photographers Guild. On 22 November 2012, Durham University officially renamed the Main Library the Bill Bryson Library for his contributions as the university's 11th chancellor (2005–11). Paul Theroux was awarded the 1981 James Tait Black Memorial Prize for his novel The Mosquito Coast, which was adapted for the 1986 movie of the same name. He was also awarded in 1989 the Thomas Cook Travel Book Award  for Riding the Iron Rooster. 
In 2005, Jan Morris was awarded the Golden PEN Award by English PEN for "a Lifetime's Distinguished Service to Literature".


== Adventure literature ==
In the world of sailing Joshua Slocum's Sailing Alone Around the World (1900) is a classic of outdoor adventure literature. In April 1895, Joshua Slocum set sail from Boston, Massachusetts and in  Sailing Alone Around the World, he described his departure in the following manner:

I had resolved on a voyage around the world, and as the wind on the morning of April 24, 1895 was fair, at noon I weighed anchor, set sail, and filled away from Boston, where the Spray had been moored snugly all winter. ... A thrilling pulse beat high in me. My step was light on deck in the crisp air. I felt there could be no turning back, and that I was engaging in an adventure the meaning of which I thoroughly understood.More than three years later, on June 27, 1898, Slocum returned to Newport, Rhode Island, having circumnavigated the world.


== Guide books ==

A guide book or travel guide is "a book of information about a place, designed for the use of visitors or tourists". An early example is Thomas West's guide to the English Lake District, published in 1778. Thomas West, an English priest, popularized the idea of walking for pleasure in his guide to the Lake District of 1778. In the introduction he wrote that he aimed: to encourage the taste of visiting the lakes by furnishing the traveller with a Guide; and for that purpose, the writer has here collected and laid before him, all the select stations and points of view, noticed by those authors who have last made the tour of the lakes, verified by his own repeated observations.  To this end he included various 'stations' or viewpoints around the lakes, from which tourists would be encouraged to appreciate the views in terms of their aesthetic qualities.  Published in 1778 the book was a major success.It will usually include full details relating to accommodation, restaurants, transportation, and activities. Maps of varying detail and historical and cultural information are also often included. Different kinds of guide books exist, focusing on different aspects of travel, from adventure travel to relaxation, or aimed at travelers with different incomes, or focusing on sexual orientation or types of diet. Travel guides can also take the form of travel websites.


== Travel journals ==

A travel journal, also called road journal, is a record made by a traveller, sometimes in diary form, of the traveler's experiences, written during the course of the journey and later edited for publication. This is a long-established literary format; an early example is the writing of Pausanias (2nd century AD) who produced his Description of Greece based on his own observations. James Boswell published his The Journal of a Tour to the Hebrides in 1786 and Goethe published his Italian Journey, based on diaries, in 1816. Fray Ilarione da Bergamo and Fray Francisco de Ajofrín wrote travel accounts of colonial Mexico in the 1760s. Fannie Calderón de la Barca, the Scottish-born wife of the Spanish ambassador to Mexico 1839–1842, wrote Life in Mexico, an important travel narrative of her time there, with many observations of local life. 
A British traveller, Mrs Alec Tweedie, published a number of travelogues, ranging from Denmark (1895) and Finland (1897), to the U.S. (1913), several on Mexico (1901, 1906, 1917), and one on Russia, Siberia, and China (1926). A more recent example is Che Guevara's The Motorcycle Diaries. A travelogue is a film, book written up from a travel diary, or illustrated talk describing the experiences of and places visited by traveller. American writer Paul Theroux has published many works of travel literature, the first success being The Great Railway Bazaar.  
Anglo-American Bill Bryson is known for A Walk in the Woods, made into a Hollywood film of the same name.


== Slave travel narratives ==

The writings of escaped slaves of their experience under slavery and their escape from it is a type of travel literature that developed during the 18th and 19th centuries, detailing how slaves escaped the restrictive laws of the southern United States and the Caribbean to find freedom. As John Cox says in Traveling South, "travel was a necessary prelude to the publication of a narrative by a slave, for slavery could not be simultaneously experienced and written."A particularly famous slave travel narrative is Frederick Douglass' autobiographical Narrative, which is deeply intertwined with his travel experiences, beginning with his travels being entirely at the command of his masters and ending with him traveling when and where he wishes. Solomon Northup's Twelve Years a Slave is a more traditional travel narrative, and he too overcomes the restrictions of law and tradition in the south to escape after he is kidnapped and enslaved. Harriet Ann Jacobs' Incidents includes significant travel that covers a small distance, as she escapes one living situation for a slightly better one, but also later includes her escape from slavery to freedom in the north.


== Fiction ==
Some fictional travel stories are related to travel literature. Although it may be desirable in some contexts to distinguish fictional from non-fictional works, such distinctions have proved notoriously difficult to make in practice, as in the famous instance of the travel writings of Marco Polo or John Mandeville. Examples of fictional works of travel literature based on actual journeys are: 

Joseph Conrad's Heart of Darkness (1899), which has its origin in an actual voyage Conrad made up the River Congo
Jack Kerouac's On the Road (1957) and The Dharma Bums (1958) are fictionalized accounts of his travels across the United States during the late 1940s and early 1950s
Travel writer Kira Salak's novel, The White Mary (2008), a contemporary example of a real-life journey transformed into a work of fiction, which takes place in Papua New Guinea and the Congo.


== Travel blogs ==
In the 21st century, travel literature became a genre of social media in the form of travel blogs, with travel bloggers using outlets like personal blogs, Pinterest, Twitter, Facebook and Instagram to convey information about their adventures, and provide advice for navigating particular countries, or for traveling generally. Travel blogs were among the first instances of blogging, which began in the mid-1990s. In 2018 the most popular self-hosted blogging platform is WordPress, due to its ease of use. A ton of travel blogs have emerged as the main resource of information travelers. Notable travel bloggers include:

The Blonde Abroad
Global Munchkins
My Life's a Travel Movie


== Scholarship ==
The systematic study of travel literature emerged as a field of scholarly inquiry in the mid-1990s, with its own conferences, organizations, journals, monographs, anthologies, and encyclopedias.  Important, pre-1995 monographs are: Abroad (1980) by Paul Fussell, an exploration of British interwar travel writing as escapism; Gone Primitive: Modern Intellects, Savage Minds (1990) by Marianna Torgovnick, an inquiry into the primitivist presentations of foreign cultures; Haunted Journeys: Desire and Transgression in European Travel Writing (1991) by Dennis Porter, a close look at the psychological correlatives of travel; Discourses of Difference: An Analysis of Women's Travel Writing by Sara Mills, an inquiry into the intersection of gender and colonialism during the 19th century; Imperial Eyes: Travel Writing and Transculturation (1992), Mary Louise Pratt's influential study of Victorian travel writing's dissemination of a colonial mind-set; and Belated Travelers (1994), an analysis of colonial anxiety by Ali Behdad.


== Travel awards ==
Prizes awarded annually for travel books have included the Thomas Cook Travel Book Award, which ran from 1980 to 2004, the  Boardman Tasker Prize for Mountain Literature, and the Dolman Best Travel Book Award, which began in 2006. The North American Travel Journalists Association holds an annual awards competition honoring travel journalism in a multitude of categories, ranging across print and online media.


== See also ==
Adventure travel – type of niche tourism
British Guild of Travel Writers
Imaginary voyage – Narrative in a fictional frame of travel account.
Rihla – Book by Ibn Battuta
Travel documentary, a documentary film or television program that describes travel
Travel itinerary – Travel Itinerary.
Travelogues of Palestine – Descriptions of the region of Palestine by travellers


== References ==


== Bibliography ==
Adams, Percy G., ed. (1988). Travel Literature Through the Ages: An Anthology. New York and London: Garland. ISBN 0-8240-8503-5.
Adams, Percy G. (1983). Travel Literature and the Evolution of the Novel. Lexington: University press of Kentucky. ISBN 0-8131-1492-6.
Barclay, Jennifer and Logan, Amy (2010). AWOL: Tales for Travel-Inspired Minds: Random House of Canada. ISBN 9780307368416.
Batten, Charles Lynn (1978). Pleasurable Instruction: Form and Convention in Eighteenth-Century Travel Literature. Berkeley: University of California Press. ISBN 978-0-520-03260-6. OCLC 4419780.
Chaney, Edward (1998). The Evolution of the Grand Tour: Anglo-Italian Cultural Relations Since the Renaissance. London: Frank Cass. ISBN 978-0-7146-4577-3. OCLC 38304358.
Chatzipanagioti-Sangmeister, Julia (2006). Griechenland, Zypern, Balkan und Levante: eine kommentierte Bibliographie der Reiseliteratur des 18. Jahrhunderts (in German). Eutin: Lumpeter and Lasel. ISBN 978-3-9810674-2-2. OCLC 470750661.
Cox, Edward Godfrey (1935). A Reference Guide To The Literature Of Travel. Including Voyages, Geographical Descriptions, Adventures, Shipwrecks and Expeditions. Seattle: University of Washington. Vol. 1
Cox, John D. (2005). Traveling South: Travel Narratives and the Construction of American Identity. University of Georgia Press. ISBN 9780820330860.
Diekmann, Anya and Hannam, Kevin (2010). Beyond Backpacker Tourism: Mobilities and Experiences: Channel View Publications. ISBN 1845412060.
Fussell, Paul (1963). "Patrick Brydone: The Eighteenth-Century Traveler As Representative Man". Literature As a Mode of Travel. New York: New York Public Library. pp. 53–67. OCLC 83683507.
Hargett, James M. (1985). "Some Preliminary Remarks on the Travel Records of the Song Dynasty (960-1279)". Chinese Literature: Essays, Articles, Reviews. 7 (1/2): 67–93. doi:10.2307/495194. JSTOR 495194.
Speake, Jennifer (2003). Literature of Travel and Exploration: An Encyclopedia. New York: Fitzroy Dearborn. ISBN 1-57958-247-8. OCLC 55631133.
Stolley, Karen (1992). El lazarillo de ciegos caminantes: un itinerario crítico (in Spanish). Hanover, New Hampshire: Ediciones del Norte. ISBN 978-0-910061-49-0. OCLC 29205545.
Batten, Charles Lynn (1978). Pleasurable Instruction: Form and Convention in Eighteenth-Century Travel Literature. Berkeley: University of California Press. ISBN 978-0-520-03260-6. OCLC 4419780.
Chaney, Edward (1998). The Evolution of the Grand Tour: Anglo-Italian Cultural Relations Since the Renaissance. London: Frank Cass. ISBN 978-0-7146-4577-3. OCLC 38304358.
Chatzipanagioti-Sangmeister, Julia (2006). Griechenland, Zypern, Balkan und Levante: eine kommentierte Bibliographie der Reiseliteratur des 18. Jahrhunderts (in German). Eutin: Lumpeter and Lasel. ISBN 978-3-9810674-2-2. OCLC 470750661.
Cox, Edward Godfrey (1935–1949). A Reference Guide To The Literature Of Travel. Including Voyages, Geographical Descriptions, Adventures, Shipwrecks and Expeditions. 1–3. Seattle: University of Washington – via Hathi Trust.;  also Vol. 1 via Internet Archive
Fussell, Paul (1963). "Patrick Brydone: The Eighteenth-Century Traveler As Representative Man". Literature As a Mode of Travel. New York: New York Public Library. pp. 53–67. OCLC 83683507.
Hargett, James M. (1985). "Some Preliminary Remarks on the Travel Records of the Song Dynasty (960-1279)". Chinese Literature: Essays, Articles, Reviews. 7 (1/2): 67–93. doi:10.2307/495194. JSTOR 495194.
William Thomas Lowndes (1869). "Voyages and Travels".  In Henry G. Bohn (ed.). Bibliographer's Manual of English Literature. 5. London: Bell and Daldy.
Speake, Jennifer (2003). Literature of Travel and Exploration: An Encyclopedia. New York: Fitzroy Dearborn. ISBN 1-57958-247-8. OCLC 55631133.
Stolley, Karen (1992). El lazarillo de ciegos caminantes: un itinerario crítico (in Spanish). Hanover, New Hampshire: Ediciones del Norte. ISBN 978-0-910061-49-0. OCLC 29205545.


== Further reading ==
"Essay on travel literature". The Cambridge History of English and American Literature (1907–1921).
Bangs, Jeremy D.  "The Travels of Elkanah Watson" (McFarland & Company, 2015)
Beautiful England (series of travel books from 1910 to 1950s)
Lawless, Jill (2000). Wild East: Travels in the New Mongolia. ECW Press. ISBN 1-55022-434-4
Picador Travel Classics
Roy, Pinaki. "Reflections on the Art of Producing Travelogues".  Images of Life: Creative and Other Forms of Writing. Ed.  Mullick, S. Kolkata: The Book World, 2014 (ISBN 978-93-81231-03-6). pp. 111–29.
Salzani, Carlo & Tötösy de Zepetnek, Steven. "Bibliography for Work in Travel Studies." CLCWeb: Comparative Literature and Culture (Library) (2010–).
Thompson, Carl (2011). Travel Writing. Routledge. ISBN 1136720804


== External links ==
American Journeys, collection of primary exploration accounts of the Americas.
Historical British travel writers: an extensive open access library on the Vision of Britain site.
"The Literature of Travel, 1700–1900". Bartleby.com.