A Playmate is a female model featured in the centerfold/gatefold of Playboy magazine as Playmate of the Month (PMOM). The PMOM's pictorial includes nude photographs and a centerfold poster, along with a pictorial biography and the "Playmate Data Sheet", which lists her birthdate, measurements, turn-ons, and turn-offs. At the end of the year, one of the 12 Playmates of the Month is named Playmate of the Year (PMOY). Every Playmate of the Month is awarded a prize of US$25,000 and each Playmate of the Year receives an additional prize of US$100,000 plus a car (specifically, a short-term lease of a car) and other discretionary gifts. In addition, Anniversary Playmates are usually chosen to celebrate a milestone year of the magazine.
Playboy encourages potential Playmates to send photos with "girl next door" appeal for consideration; others may submit photos of Playmate candidates, and may be eligible for a finder's fee if their model is selected. In addition, "casting calls" are held regularly in major US cities to offer opportunities for women to test for Playboy. Until just prior to the death of Hugh Hefner, he personally selected each Playmate of the Month and every Playmate of the Year, taking into account an annual readers' poll.
According to Playboy, there is no such thing as a former Playmate because "Once a Playmate, always a Playmate".


== History ==
Marilyn Monroe, who was featured in the first issue, was the only one to appear as "Sweetheart of the Month". The first model called a Playmate of the Month was Margie Harrison, Miss January 1954, in the second issue of Playboy. Generally a woman may appear only once as a Playmate, but in the early years of the magazine, some models were featured multiple times. Marilyn Waltz (February 1954, April 1954, April 1955 –  her first appearance was as Margaret Scott) and Janet Pilgrim (July 1955, December 1955, and October 1956) are tied for the most appearances. Margie Harrison (January 1954, June 1954) and Marguerite Empey (May 1955, February 1956) are the only other women to appear more than once as Playmates.
Under current law in most US jurisdictions, publishing nude pictures of a model younger than 18 would be a felony. However, in the early years of the magazine, laws regarding corruption of a minor were less well established. Several playmates –  including Nancy Crawford (April 1959), Donna Michelle (December 1963), Linda Moon (October 1966), Patti Reynolds (September 1965) and Teddi Smith (July 1960) –  posed when they were 17. Elizabeth Ann Roberts (January 1958) –  whose pictorial was called "Schoolmate Playmate" –  posed when she was 16. Hugh Hefner and Roberts' mother were arrested as a result, but the case was subsequently dismissed because Roberts' mother had signed a statement that her daughter was 18 before the photo shoot. In 1979 Ursula Buchfellner posed for the German edition of Playboy when she was 16 and subsequently posed for the American edition (October 1979) when she was 18. Dutch Playmate twins Karin and Mirjam van Breeschooten appeared at age 17 in their country's edition of Playboy in June 1988, at 18, they were Misses September 1989 in the US version.
Some women have become Playmates in their 30s. Dolores Donlon (August 1957) is the oldest Playmate to date, appearing in her shoot at the age of 36.


=== Playmate firsts ===
First Playmate: Margie Harrison (Miss January 1954) in the second issue of Playboy. Marilyn Monroe, who was featured in the first issue, was the only one to appear as "Sweetheart of the Month".
First Playmate to be chosen three times: Marilyn Waltz (Miss February 1954, April 1954, and April 1955 – her first appearance was as Margaret Scott)
First and only month in Playboy history (to date) to not have a Playmate – March 1955 (no issue published)
First centerfold (two-page): Janet Pilgrim (Miss July 1955)
First fold-out centerfold (three-page): Marian Stafford (Miss March 1956)
First foreign-born Playmate: Marion Scott (Miss May 1956) was born in Germany.
Youngest Playmate ever featured: Elizabeth Ann Roberts (Miss January 1958) appeared at age 16.
First issue to feature two Playmates (two in the same month, in separate photos): Pat Sheehan and Mara Corday (Misses October 1958)
First Playmate to become Playmate of the Year (1960): Ellen Stratton (Miss December 1959)
First Asian-American Playmate: China Lee (Miss August 1964)
First African-American Playmate: Jennifer Jackson (Miss March 1965)
First twin Playmates Mary Collinson and Madeleine Collinson (Miss October 1970)
First Playmate to show clearly visible pubic hair: Liv Lindeland (Miss January 1971)
First Playmate to pose for a full frontal nude centerfold: Marilyn Cole (Miss January 1972)
First Playmate to pose for a full frontal nude centerfold with clearly visible entire pubic hair: Bonnie Large (Miss March 1973)
First Hispanic-American Playmate: Ester Cordet (Miss October 1974) was from Panama.
First cousins to be Playmates: Elaine Morton (Miss June 1970) and Karen Elaine Morton (Miss July 1978)
First video Playmate (1982): Lonny Chin was also the magazine centerfold in the January 1983 issue.
First mother and daughter to be Playmates: Carol Eden (Miss December 1960) and her daughter Simone Eden (Miss February 1989)
First triplets to be Playmates: Erica, Nicole and Jaclyn Dahm (Misses December 1998)
First Playmate to pose with shaved pubic area: Dalene Kurtis (Miss September 2001)
First Mexican-American Playmate of the Year: Raquel Pomplun (2013 and Miss April 2012)
First openly transgender Playmate: Ines Rau (Miss November 2017)
First amputee Playmate: Marsha Elle (Miss April 2020)


== Playmate of the Year ==
In the early days of Playboy, there was no official prize for the most popular Playmate at the end of each year.  Although February 1954 Playmate Marilyn Waltz gained much popularity, receiving more fan mail than any other Playmate that year, she was not crowned PMOY. Neither was December 1956 Playmate Lisa Winters who was named Playmate of the Year, and although Joyce Nizzari, Miss December 1958, was named the "most popular" Playmate of 1958, the PMOY competition was first officially won in 1960 by Miss December 1959, Ellen Stratton. The 2009 PMOY, Ida Ljungqvist, was the 50th PMOY and the second model of African descent to win the title, the first being Renee Tenison.
A PMOY makes her appearance the year following her first appearance as PMOM.  This feature is usually published in the June issue, although sometimes it has been published in the May or July issue.  Until 2003, each year's Playmate of the Year would routinely appear on the cover of her PMOY issue, in which her PMOY pictorial is featured.  However, from 2003 to 2005, PMOYs did not appear on the covers of their PMOY issues, and 2007 PMOY Sara Jean Underwood did not either. Instead, celebrities appearing in celebrity pictorials in the PMOY issues appeared on the covers.  The 2010 PMOY, Hope Dworaczyk, did appear on the cover, and was the first model to be the subject of a three-dimensional Playboy centerfold photograph.
The average age of a Playmate of the Year, 23.5 years, is slightly higher than that of a Playmate of the Month, 22.4 years.
In 1964, at the age of 18, Donna Michelle became the youngest PMOY ever. Jo Collins (1965) and Christa Speck (1962) both were 19. Sharon Clark (1971), Karen McDougal (1998) and Ida Ljungqvist (2009) became PMOY at the age of 27. Only three women became PMOY in their 30s: Tiffany Fallon (2005) at 31, Kathy Shower (1986) at 33, and Eugena Washington (2016) at 31.


== Notable Playmates ==
Playmates who attained fame for reasons in addition to their Playboy appearance(s) include (with the date of their appearance):

Pamela Anderson (February 1990), actress
Marina Baker (March 1987), journalist, children's book author, mayor of Telscombe, East Sussex, England
Bebe Buell (November 1974), author and mother of Liv Tyler
Lisa Dergan (July 1998), spokesmodel for St. Pauli Girl and Guess? Jeans; also Fox Sports correspondent
Daphnée Duplaix (July 1997), actress
Hope Dworaczyk (April 2009), television host and contestant on Celebrity Apprentice
Erika Eleniak (July 1989), actress
Lindsey Gayle Evans (October 2009), Miss Louisiana Teen USA 2008
Tiffany Fallon (December 2004), Miss Georgia USA 2001, model, actress and Celebrity Apprentice contestant.
Lena Forsén (November 1972), her image would later become a ubiquitous standard test image in the field of digital image processing, where the image is known as Lenna
Claudia Jennings (November 1969), actress
Scarlett Keegan (September 2004), former child model, international print model, actress –  minor roles include: Walk Hard: The Dewey Cox Story, HBO's Entourage, and Weezer's " Beverly Hills" music video.
Connie Kreski (January 1968), actress
Angela Little (August 1998), actress
Jayne Mansfield (February 1955), singer and actress
Shae Marks (May 1994), actress
Jenny McCarthy (October 1993), actress, author, comedian; MTV's Singled Out
Julie McCullough (February 1986), actress –  played nanny "Julie Costello" on several episodes of Growing Pains until she was fired for having posed nude in Playboy.
Shanna Moakler (December 2001), Miss USA 1995, ex-wife of Travis Barker, and co-star of reality TV show Meet the Barkers
Kelly Marie Monaco (April 1997), Emmy-nominated actress, winner of Dancing with the Stars reality show in 2005, long-running actress of General Hospital
Cynthia Myers (December 1968), actress
Janice Pennington (May 1971), model on The Price Is Right
Brande Roderick (April 2000), actress and Celebrity Apprentice contestant
Nikki Schieler (September 1997), actress
Anna Nicole Smith  (May 1992), Guess? jeans model, actress, and widow of an octogenarian businessman. The lawsuit over his inheritance reached the US Supreme Court.
Victoria Silvstedt (December 1996), Swedish supermodel, actress, radio and TV hostess, published singer, and a former national-level skier in Sweden
Martha Smith (July 1973), actress
Stella Stevens (January 1960), actress
Dorothy Stratten (August 1979), actress whose life and murder were portrayed in Star 80 (1983) and in Death of a Centerfold: The Dorothy Stratten Story (1981)
Jeana Tomasino ("Jeana Keough") (November 1980), actress, model, original cast member of The Real Housewives of Orange County, and client on Thintervention with Jackie Warner
Shannon Tweed (November 1981), actress co-star of reality-TV show Gene Simmons Family Jewels
Eugena Washington (December 2015), fashion model, America's Next Top Model (cycle 7) contestant
Kelly Wearstler ("Kelly Gallagher") (September 1994), designer
Teri Weigel (April 1986), adult movie performer
Victoria Zdrok (October 1994), attorney, clinical psychologist, sex therapist; second model to appear as centerfold for both Playboy and Penthouse


== See also ==
List of Penthouse Pets
List of people in Playboy 1953–59
List of people in Playboy 1960–69
List of people in Playboy 1970–79
List of people in Playboy 1980–89
List of people in Playboy 1990–99
List of people in Playboy 2000–09
List of people in Playboy 2010–19
List of Playboy Playmates of the Month
List of Playboy videos
List of Playmates of the Year
Playboy lists
Playboy Bunny


== Notes ==


== References ==
Edgren, Gretchen (1996). The Playmate Book: Five Decades of Centerfolds. Santa Monica, California: General Publishing Group. ISBN 1-57544-128-4.
Edgren, Gretchen; Hugh M. Hefner (2005). The Playmate Book: Six Decades of Centerfolds. Taschen Books. ISBN 3-8228-4824-7.


== External links ==
Official website
Playmate Directory
All playmates centerfolds