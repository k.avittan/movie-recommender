A patrol is commonly a group of personnel, such as law enforcement officers, military personnel, or private security contractors that are assigned to monitor a specific geographic area. 
This is also often referred to as a beat.


== Military ==

In military tactics, a patrol is a sub-subunit or small tactical formation, sent out from a military organization by land, sea or air for the purpose of combat, reconnaissance, or a combination of both. The basic task of a patrol is to follow a known route with the purpose of investigating some feature of interest or, in the assignment of a fighting patrol (US combat patrol), to find and engage the enemy. A patrol can also mean a small cavalry or armoured unit, subordinate to a troop or platoon, usually comprising a section or squad of mounted troopers, or two AFVs (often tanks).


== Law enforcement ==

In non-military law enforcement, patrol officers are law enforcement officers assigned to monitor specified geographic areas—that is, to move through their areas at regular intervals looking out for any signs of problems of any kind.  They are the officers most commonly encountered by the public, as their duties include responding to calls for service, making arrests, resolving disputes, taking crime reports, and conducting traffic enforcement, and other crime prevention measures. A patrol officer is often the first to arrive on the scene of any incident; what such an officer does or fails to do at the scene can greatly influence the outcome of any subsequent investigation. The patrol officer, as the person who is in the field daily, is often closest to potential crime and may have developed contacts who can provide information.
The Philadelphia Foot Patrol Experiment, a randomized control trial conducted by Temple University, has shown that foot patrols reduce crime. With the resources to patrol 60 locations, researchers identified the highest violent crime corners in the city, using data from 2006 to 2008. Police commanders designed 120 foot patrol areas around these corners, and stratified randomization was used to assign pairs of foot patrols with similar crime rates as either a comparison or a target area. Officers generally patrolled in pairs with two pairs assigned to each foot patrol. After three months, relative to the comparison areas, violent crime decreased 23%.
Official records of police activities during the intervention period reveal the following in the target areas:

Drug‐related incident detections increased 15%
Pedestrian stops increased 64%
Vehicle stops increased 7%
Arrests increased 13%An emerging trend within patrol is the supplement of basic police patrol with that of private security agencies. The privatization of police is explored in James Pastor's book The Privatization of Police in America: An Analysis and Case Study.Law enforcement patrols don't always just enforce the laws during the patrols. They also try and have community relations, will investigate traffic accidents and transport criminals. They will go to schools to talk about their jobs or about drugs and safe driving. In some large cities, the police chief will go around to meet and talk with business owners, residents or anyone in the city.


== Etymology ==
From French patrouiller from Old French patouiller (“to paddle, paw about, patrol”) from patte (“a paw”)


== Non-law enforcement patrols ==


=== Schools ===
Some elementary schools use the term patrol to refer to students who are selected to monitor safety in the classroom or to those students who assist crossing guards with safety of children crossing busy streets.  Another common term for this use of patrol is hall monitor.


=== Scouting ===

In Scouting, a patrol is six to eight Scouts (youth members) under the leadership of one of their number who is appointed Patrol Leader and supported by a Second or Assistant Patrol Leader. This is the basic unit of a Scout troop. The Patrol method is an essential characteristic of Scouting by which it differs from all other organizations, using the natural dynamics of the gang for an educational purpose.


== References ==


== External links ==