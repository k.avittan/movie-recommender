Nanook of the North is a 1922 American silent documentary film by Robert J. Flaherty, with elements of docudrama, at a time when the concept of separating films into documentary and drama did not yet exist.In the tradition of what would later be called salvage ethnography, Flaherty captured the struggles of the Inuk man named Nanook and his family in the Canadian Arctic. Some have criticized Flaherty for staging several sequences, but the film is generally viewed as standing "alone in its stark regard for the courage and ingenuity of its heroes."In 1989, Nanook of the North was one of the first 25 films selected by the Library of Congress for preservation in the United States National Film Registry for being "culturally, historically, or aesthetically significant".


== Plot ==

The documentary follows the lives of an Inuk, Nanook, and his family as they travel, search for food, and trade in the Ungava Peninsula of northern Quebec, Canada. Nanook; his wife, Nyla; and their family are introduced as fearless heroes who endure rigors no other race could survive.  The audience sees Nanook, often with his family, hunt a walrus, build an igloo, go about his day, and perform other tasks.


== Production ==


=== Development ===
In 1910 Flaherty was hired as an explorer and prospector along the Hudson Bay for the Canadian Pacific Railway. Learning about the lands and people there, Flaherty decided to bring a camera with him on his third expedition in 1913, but knowing nothing about film, Flaherty took a three-week course on cinematography in Rochester, New York.


=== Filming ===
Using a Bell & Howell camera, a portable developing and printing machine, and some lighting equipment, Flaherty spent 1914 and 1915 shooting hours of film of Inuit life. By 1916, Flaherty had enough footage that he began test screenings and was met with wide enthusiasm. However, in 1916, Flaherty dropped a cigarette onto the original camera negative (which was highly flammable nitrate stock) and lost 30,000 feet of film. With his first attempt ruined, Flaherty decided to not only return for new footage, but also to refocus the film on one Inuit family as he felt his earlier footage was too much of travelogue. Spending four years raising money, Flaherty was eventually funded by French fur company Revillon Frères and returned to the North and shot from August 1920 to August 1921. As a main character, Flaherty chose the celebrated hunter of the Itivimuit tribe, Allakariallak. The full collaboration of the Inuit was key to Flaherty's success as the Inuit were his film crew and many of them knew his camera better than he did.


== Controversies ==
Flaherty has been criticized for deceptively portraying staged events as reality. "Nanook" was in fact named Allakariallak (pronounced [al.la.ka.ɢi.al.lak]). Flaherty chose this nickname because of its seeming genuineness which makes it more marketable to Euro-American audiences. The "wife" shown in the film was not really his wife. According to Charlie Nayoumealuk, who was interviewed in Nanook Revisited (1990), "the two women in Nanook – Nyla (Alice [?] Nuvalinga) and Cunayou (whose real name we do not know) were not Allakariallak's wives, but were in fact common-law wives of Flaherty." And although Allakariallak normally used a gun when hunting, Flaherty encouraged him to hunt after the fashion of his recent ancestors in order to capture the way the Inuit lived before European colonization of the Americas. Flaherty also exaggerated the peril to Inuit hunters with his claim, often repeated, that Allakariallak had died of starvation less than two years after the film was completed, whereas in fact he died at home, likely of tuberculosis.Furthermore, it has been criticized for portraying Inuit people as subhuman Arctic beings, without technology or culture which reproduces the historical image that situates them outside modern history. It was also criticized for comparing Inuit people to animals. The film is considered to be an artifact of popular culture at the time and also a result of a historical fascination for Inuit performers in exhibitions, zoos, fairs, museums and early cinema.Flaherty defended his work by stating, "one often has to distort a thing in order to catch its true spirit." Later filmmakers have pointed out that the only cameras available to Flaherty at the time were both large and immobile, making it impossible to effectively capture most interior shots or unstructured exterior scenes without significantly modifying the environment and subject action.


=== Building of the igloo ===
The building of the igloo is one of the most celebrated sequences in the film, but interior photography presented a problem. Building an igloo large enough for a camera to enter resulted in the dome collapsing, and when they finally succeeded in making the igloo it was too dark for photography. Instead, the images of the inside of the igloo in the film were actually shot in a special three-walled igloo for Flaherty's bulky camera so that there would be enough light for it to capture interior shots.


=== Visit to the trade post of the white man ===
In the "Trade Post of the White Man" scene, Nanook and his family arrive in a kayak at the trading post and one family member after another emerge from a small kayak, akin to a clown car at the circus. Going to trade his hunt from the year, including the skins of foxes, seals, and polar bears, Nanook comes in contact with the white man and there is a funny interaction as the two cultures meet. The trader plays music on a gramophone and tries to explain how a man 'cans' his voice. Bending forward and staring at the machine, Nanook puts his ear closer as the trader cranks the mechanism again. The trader removes the record and hands it to Nanook who at first peers at it and then puts it in his mouth and bites it. The scene is meant to be a comical one as the audience laughs at the naivete of Nanook and people isolated from Western culture. In truth, the scene was entirely scripted and Allakariallak knew what a gramophone was.


=== Hunting of the walrus ===
It has been noted that in the 1920s, when Nanook was filmed, the Inuit had already begun integrating the use of Western clothing and were using rifles to hunt rather than harpoons, but this does not negate that the Inuit knew how to make traditional clothing from animals found in their environment, could still fashion traditional weapons and were perfectly able to make use of them if found to be preferable for a given situation.

The film is not technically sophisticated; how could it be, with one camera, no lights, freezing cold, and everyone equally at the mercy of nature? But it has an authenticity that prevails over any complaints that some of the sequences were staged. If you stage a walrus hunt, it still involves hunting a walrus, and the walrus hasn't seen the script. What shines through is the humanity and optimism of the Inuit.


== Reception ==
As the first "nonfiction" work of its scale, Nanook of the North was ground-breaking cinema. It captured many authentic details of a culture little known to outsiders, and it was filmed in a remote location. Hailed almost unanimously by critics, the film was a box-office success in the United States and abroad. In the following years, many others would try to follow Flaherty's success with "primitive peoples" films. In 2005, film critic Roger Ebert described the film's central figure, Nanook, as "one of the most vital and unforgettable human beings ever recorded on film." In a 2014 Sight and Sound poll, film critics voted Nanook of the North the seventh-best documentary film of all time.On review aggregator website Rotten Tomatoes, the film holds an approval rating of 100% based on 30 reviews, with an average rating of 8.68/10. The site's critics' consensus reads: "An enthralling documentary and a visual feat, Nanook of the North fascinates with its dramatic depiction of life in an extremely hostile environment."


== Legacy ==
At the time, few documentaries had been filmed and there was little precedent to guide Flaherty's work. Since Flaherty's time, staging, attempting to steer documentary action, or presenting re-enactment as naturally captured footage has come to be considered unethical.In its earliest years (approx. 1895–1902), film production was dominated by actualities—short pictures of real people in real places. Robert Flaherty's great innovation was simply to combine the two forms of actuality, infusing the exotic journey with the details of indigenous work and play and life.


== Home media ==
In 1999, Nanook of the North was digitally remastered and released on DVD by The Criterion Collection. It includes an interview with Flaherty's widow (and Nanook of the North co-editor), Frances Flaherty, photos from Flaherty's trip to the arctic, and excerpts from a TV documentary, Flaherty and Film. In 2013, Flicker Alley released a remastered Blu-ray version that includes six other arctic films.


== Popular culture ==


=== Music ===
Frank Zappa dreamed he was Nanook in the lyrics to his 1974 songs "Don't Eat the Yellow Snow" & "Nanook Rubs It".
The music video to Australian alternative rock band Regurgitator's 1995 song "Blubber Boy" parodies the movie.
Since 2015 Tanya Tagaq has performed interpretations of the entire film accompanied variously by a drummer, violinist, guitarist and thereminist.


=== Film ===
The 1923 silent film comedy A Tough Winter parodied Nanook of the North.  It was produced by Hal Roach, directed by Charley Chase, and starred Snub Pollard with Marie Mosquini and James Finlayson.
Kabloonak is a 1994 film about the making of Nanook of the North. Charles Dance plays Flaherty and Adamie Quasiak Inukpuk (a relative of Nanook) plays Nanook.


=== Television ===
In episode 2 of the 2015 series Documentary Now! (IFC), "Kunuk Uncovered" is a mockumentary parodying the 1990 documentary about the making of Nanook of the North titled Nanook Revisited, especially addressing the staging and manipulation of the original documentary.


== See also ==
Docudrama
Nanook (mythology)
List of films with a 100% rating on Rotten Tomatoes, a film review aggregator website


== Notes ==


== References ==

Bibliography


== Further reading ==
Nanook of the North essay by Daniel Eagan in America's Film Legacy: The Authoritative Guide to the Landmark Movies in the National Film Registry, A&C Black, 2010 ISBN 0826429777, pages 83–84


== External links ==
Nanook of the North essay by Patricia R. Zimmermann and Sean Zimmermann Auyash on the National Film Registry website
Nanook of the North at the American Film Institute Catalog
Nanook of the North on IMDb
Nanook of the North at AllMovie
Nanook of the North is available for free download at the Internet Archive
Experimental footage done with Nanook of the North at the Internet Archive
Great Movies: Nanook of the North by Roger Ebert
Nanook of the North an essay by Dean W. Duncan at the Criterion Collection