One Week of Life is a 1919 American silent drama film produced and distributed through Goldwyn Pictures. It was directed by Hobart Henley and starred Pauline Frederick. It is now considered to be a lost film.


== Plot ==
As described in a film magazine, Mrs. Sherwood (Frederick) finds life unbearable with her drunken husband Kingsley Sherwood (Holding) and longs for "one week of life" with LeRoy Scott (Ainsworth), her lying lover. She considers herself alone, making no attempt to redeem the weakling she had promised to honor and obey. Her lover finds an almost exact duplicate of her in young Marion Roche (also Frederick), discusses the situation with both women, and plans a substitution where Marion takes the place of Mrs. Sherwood while the later pretends to visit a child that is ill. LeRoy escapes with the erring Mrs. Sherwood, but Marion's situation becomes complicated when Kingsley sees finer qualities in his supposed wife than in the real Mrs. Sherwood. At Marion's suggestion, Kingsley tries to give up drinking. He does not suspect any substitution until he finds a letter that Marion has dropped, and he sets a trap to discover her purpose. He manages to enter her bedroom after she returns at night from a friendly visit, and it is exposed that his wife never left to visit a sick child, because there never was one. Marion is overwhelmed, but she has become interested in the plucky self-struggle Kingsley has put up, while he attributes his reform to her encouragement. As they become interested in each other word comes that the erring wife and her lover have been drowned while out in a canoe. In the end there is the redeemed man and the woman responsible for his redemption.


== Cast ==
Pauline Frederick - Mrs. Sherwood & Marion Roche
Thomas Holding - Kingsley Sherwood
Sidney Ainsworth - LeRoy Scott
Corinne Barker - Lola Canby
Percy Challenger -


== Production ==
The plot involving the redemption of a drunken husband was timely given the passage of the Wartime Prohibition Act, which took effect June 30, 1919, and banned the sale of alcoholic beverages, and the ratification of the Eighteenth Amendment to the United States Constitution in January of the same year.


== See also ==
List of lost films


== References ==


== External links ==
One Week of Life on IMDb
One Week of Life at AllMovie
Lantern slide to the film
Lobby poster(Wayback)