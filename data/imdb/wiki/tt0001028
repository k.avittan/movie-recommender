Salome (; Greek: Σαλώμη; Hebrew: שלומית‎, Shlomit, related to שָׁלוֹם: shalom,"peace"; 10s–60s AD), the daughter of Herod II and Herodias, granddaughter of Herod the Great, is known from accounts in the New Testament, where she appears as an unnamed daughter of Herodias, and an account by Flavius Josephus, where the daughter of Herodias is named Salome. In the New Testament she is mentioned as the stepdaughter of Herod Antipas, demanding and receiving the head of John the Baptist. According to Josephus, she was first married to her uncle Philip the Tetrarch, after whose death (AD 34), she married her cousin Aristobulus of Chalcis, thus becoming queen of Chalcis and Armenia Minor.
The gospel story of her dance at the birthday celebration of her stepfather, who had John the Baptist beheaded at her request, inspired art, literature and music over an extended period of time. Among the paintings are those by Titian and Gustave Moreau. Oscar Wilde's eponymous play, and its subsequent setting by Richard Strauss, are among the literary and musical realisations which endeavoured to portray her. She also appeared in film, for instance in a 1953 Salome movie starring Rita Hayworth in the title role.


== First-century accounts and sources ==
Salome is commonly identified with the daughter of Herodias who, according to the New Testament (Mark 6:21–29 and Matthew 14:3–11), danced for Herod. In his Jewish Antiquities, Josephus mentions marriages and children of the daughter of Herodias named Salome.


=== New Testament ===
According to Mark 6:21–29 a daughter of Herodias danced before Herod at his birthday celebration, and in doing so gave her mother the opportunity to obtain the head of John the Baptist. Although the New Testament accounts do not mention a name for the girl, this daughter of Herodias is often identified with Salome. According to Mark's gospel, Herodias bore a grudge against John for stating that Herod's marriage to her was unlawful; she encouraged her daughter to demand that John be executed.
Mark's account (6:21–28) reads:A convenient day arrived when Herod spread an evening meal on his birthday for his high officials and the military commanders and the most prominent men of Galilee. The daughter of Herodias came in and danced, pleasing Herod and those dining with him. The king said to the girl: "Ask me for whatever you want, and I will give it to you." Yes, he swore to her: "Whatever you ask me for, I will give it to you, up to half my kingdom." So she went out and said to her mother: “What should I ask for?” She said: “The head of John the Baptist." She immediately rushed in to the king and made her request, saying: "I want you to give me right away on a platter the head of John the Baptist." Although this deeply grieved him, the king did not want to disregard her request, because of his oaths and his guests. So the king immediately sent a bodyguard and commanded him to bring John’s head. So he went off and beheaded him in the prison and brought his head on a platter. He gave it to the girl, and the girl gave it to her mother. When his disciples heard of it, they came and took his body and laid it in a tomb.
The parallel passage in the Gospel of Matthew (14:6–11):But on Herod's birthday, the daughter of Herodias danced before them: and pleased Herod. Whereupon he promised with an oath, to give her whatsoever she would ask of him. But she being instructed before by her mother, said: Give me here in a dish the head of John the Baptist. And the king was struck sad: yet because of his oath, and for them that sat with him at table, he commanded it to be given. And he sent, and beheaded John in the prison. And his head was brought in a dish: and it was given to the damsel, and she brought it to her mother.
Some ancient Greek versions of Mark read "Herod's daughter Herodias" (rather than "daughter of the said Herodias"). To scholars using these ancient texts, both mother and daughter had the same name. However, the Latin Vulgate Bible translates the passage as it is above, and western Church Fathers, therefore, tended to refer to Salome as "Herodias's daughter" or just "the girl". Nevertheless, because she is otherwise unnamed in the Bible, the idea that both mother and daughter were named Herodias gained some currency in early modern Europe.Herodias's daughter is arguably not Salome the disciple, who is a witness to the Crucifixion of Jesus in Mark 15:40. However, the apocryphal Book of the Resurrection of Christ, pseudonymically attributed to the apostle Bartholomew, names a "Salome the temptress" as among the women who went to the empty tomb; perhaps reflecting an early tradition that Salome, the daughter of Herodias, was at the tomb.


=== Josephus ===
Salome is mentioned as a stepdaughter of Herod Antipas in Josephus's Jewish Antiquities (Book XVIII, Chapter 5, 4):Herodias, [...], was married to Herod, the son of Herod the Great, who was born of Mariamne, the daughter of Simon the high priest, who had a daughter, Salome; after whose birth Herodias took upon her to confound the laws of our country, and divorced herself from her husband while he was alive, and was married to Herod, her husband's brother by the father's side, he was tetrarch of Galilee; but her daughter Salome was married to Philip, the son of Herod, and tetrarch of Trachonitis; and as he died childless, Aristobulus, the son of Herod, the brother of Agrippa, married her; they had three sons, Herod, Agrippa, and Aristobulus;
According to William Smith's Dictionary of Greek and Roman Antiquities:


=== Coins ===
A few coins with portraits of Aristobulus and Salome have been found.


== Salome in the arts ==

The story of her dance before Herod with the head of John the Baptist on a silver platter led medieval Christian artists to depict her as the personification of the lascivious woman, a temptress who lures men away from salvation.Christian traditions depict her as an icon of dangerous female seductiveness, notably in regard to the dance mentioned in the New Testament, which is thought to have had an erotic element to it, and in some later transformations it has further been iconized as the Dance of the Seven Veils. Other elements of Christian tradition concentrate on her lighthearted and cold foolishness that, according to the gospels, led to John the Baptist's death. Bryant G. Wood Ph.D supplies a quote from David Flusser, a leading expert on early Christianity, that her "biographical profile suggests a normal, moral personality". Nevertheless, a similar motif was struck by Oscar Wilde in his Salome, in which she plays a femme fatale. This parallel representation of the Christian iconography, made even more memorable by Richard Strauss' opera based on Wilde's work, is as consistent with Josephus' account as the traditional Christian depiction; however, according to the Romanized Jewish historian, Salome lived long enough to marry twice and raise several children. Few literary accounts elaborate the biographical data given by Josephus.Despite Josephus' account, she was not consistently called Salome until the nineteenth century when Gustave Flaubert (following Josephus) referred to her as "Salome" in his short story "Herodias".


=== Painting and sculpture ===
This biblical story has long been a favorite of painters. Painters who have done notable representations of Salome include Masolino da Panicale, Filippo Lippi, Benozzo Gozzoli, Leonardo da Vinci followers Andrea Solario and Bernardino Luini, Lucas Cranach the Elder, Titian, Caravaggio, Guido Reni, Fabritius, Henri Regnault, Georges Rochegrosse, Gustave Moreau, Lovis Corinth and Federico Beltran-Masses.
Titian's version (illustration c.1515) emphasizes the contrast between the innocent girlish face and the brutally severed head. Because of the maid by her side, this Titian painting, like others of the subject, is also considered to be Judith with the Head of Holofernes. Unlike Salome who goes nameless in the Christian bible, Judith is a Judeo-Christian mythical patriot whose story is perhaps less psychological and as she was a widow, may not be particularly girlish nor innocent in representations.In Moreau's version (illustration) the figure of Salome is emblematic of the femme fatale, a fashionable trope of fin-de-siecle decadence. In his 1884 novel À rebours, Frenchman Joris-Karl Huysmans describes the depiction of Salome in Moreau's painting:

No longer was she merely the dancing-girl who extorts a cry of lust and concupiscence from an old man by the lascivious contortions of her body; who breaks the will, masters the mind of a King by the spectacle of her quivering bosoms, heaving belly and tossing thighs; she was now revealed in a sense as the symbolic incarnation of world-old Vice, the goddess of immortal Hysteria, the Curse of Beauty supreme above all other beauties by the cataleptic spasm that stirs her flesh and steels her muscles, – a monstrous Beast of the Apocalypse, indifferent, irresponsible, insensible, poisoning.


=== Sacred vocal music ===
Salome appears as a character in Alessandro Stradella's oratorio S. Giovanni Battista (St. John the Baptist), composed in 1676, which includes "Queste lagrime e sospiri", an aria sung by the Salome character.


=== Theatre and literature ===
In 1877 Gustave Flaubert's Three Tales were published, including "Herodias". In this story full responsibility for John's death is given to Salome's mother Herodias and the priests who fear his religious power. Salome herself is shown as a young girl who forgets the name of the man whose head she requests as she is asking for it. Jules Massenet's 1881 opera Hérodiade was based on Flaubert's short story.The 1934 fantasy novella A Witch Shall Be Born by Robert E. Howard, one of the Conan the Barbarian cycle, features an evil prehistorical witch named Salome, and it is clearly implied that she was an earlier incarnation of the New Testament character of the same name.Salome is shown in the mystery play as a personification of Carl Jung's pleasure in The Red Book. Through dream analysis and active imagination, she is seen as the "daughter of Elijah": a non-historical but rather metaphysical and symbolic relationship between Pleasure/Salome and Elijah/Forethinking where one cannot act without properly function without the otherThrough interactions with Salome, Jung learns of how he neglected the emotional feeling side of his personality and the difficulties of accepting that part of himself that he suppressed.


==== Oscar Wilde's play ====

Salomé's story was made the subject of a Symbolist play by Oscar Wilde that was first banned in London in 1892 while rehearsals were underway, and that subsequently premiered in Paris in 1896, under the French name Salomé. In Wilde's play, Salome takes a perverse fancy for John the Baptist, and causes him to be executed when John spurns her affections. In the finale, Salome takes up John's severed head and kisses it.Because at the time British law forbade the depiction of biblical characters on stage, Wilde wrote the play originally in French, and then produced an English translation (titled Salome). To this Granville Bantock composed incidental music, which was premiered at the Court Theatre, London, on 19 April 1918.


==== Operas based on Wilde's play ====

The Wilde play (in a German translation by Hedwig Lachmann) was edited down to a one-act opera by Richard Strauss. The opera Salome, which premiered in Dresden in 1905, is famous for the Dance of the Seven Veils. As with the Wilde play, it turns the action to Salome herself, reducing her mother to a bit-player, though the opera is less centered on Herod's motivations than the play.Shortly after the success of Strauss' opera, Antoine Mariotte created another opera based on Wilde's original French script. It was premiered on 30 October 1908 at the Grand Théâtre at Lyon. This opera was revived only in 2005 at the Montpellier Festival.


==== Ballet ====
In 1907 Florent Schmitt received a commission from Jacques Rouché to compose a ballet, La tragédie de Salomé, for Loie Fuller to perform at the Théâtre des Arts. Another Salome ballet was composed by the Japanese composer Akira Ifukube in 1948. Danish choreographer Flemming Flindt's ballet Salome with music by Peter Maxwell Davies premiered in 1978. Choreographer Arthur Pita was commissioned by San Francisco Ballet for his version of a Salome ballet in 2017.


==== Poetry ====
In "Salome" (1896) by the Greek poet Constantine Cavafy, Salome instigated the death of John the Baptist as part of a futile effort to get the interest of "a young sophist who was indifferent to the charms of love". When Salome presents to him the Baptist's head, the sophist rejects it, remarking in jest "Dear Salome, I would have liked better to get your own head". Taking the jest seriously, the hopelessly infatuated Salome lets herself be beheaded and her head is duly brought to the sophist, who however rejects it in disgust and turns back to studying the Dialogues of Plato.Salome poetry was also written by, among others, Ai (1986), Nick Cave (1988), and Carol Ann Duffy (1999).


=== Other music ===
A descriptive piano piece by Mel Bonis entitled Salomé (1909) is part of her series, Femmes de Légende.A 1989 album entitled Salome Dances for Peace by the string quartet Kronos Quartet.Songs about Salome have been written by, among others, Archibald Joyce (1907, 1912), Tommy Duncan (1952), Karel Kryl (1965), Drs. P (1974), John Cale (1978), Kim Wilde (1984), U2 (1990), Andrew Lloyd Webber (1993), Liz Phair (1993), Kurt Elling (1995), Susan McKeown (1995), Mark St. John Ellis as Elijah's Mantle (1995), Old 97's (1997), The Changelings (1997), Loudovikos ton Anogeion (1997), The Residents (1998), Enrique Bunbury (1998), Chayanne (1999), Patti Smith (2000), Killing Miranda (2001), Gary Jules' "Pills" (2001), The Booda Velvets (2001), Stormwitch (2004), Flipron (2006), Xandria (2007), Pete Doherty (2009), Saltatio Mortis (2009), 9GOATS BLACK OUT (2009), Justin Vivian Bond (2011), Regina Spektor and Kaya (2012), Behemoth (2014), Wovenhand (2014), Marriages (2015), and Jarvis Cocker (2017).


=== Film ===
Wilde's Salome has often been made into a film, notably a 1923 silent film, Salome, starring Alla Nazimova in the title role and a 1988 Ken Russell play-within-a-film treatment, Salome's Last Dance, which also includes Wilde and Lord Alfred Douglas as characters. Steven Berkoff filmed his stage version of the play in 1992.In the 1950 film Sunset Boulevard, the principal character Norma Desmond is portrayed as writing a screenplay for a silent film treatment of the legend of Salome, attempting to get the screenplay produced, and performing one of the scenes from her screenplay after going mad.Other Salome films include:

Salomé (1918), starring Theda Bara in the title role. Flavius Josephus was credited for the story.
Salomé (1953), starring Rita Hayworth in the title role.
Salomé (1972), starring Carmelo Bene and Donyale Luna in the title role.
Salome (1986), a French-Italian production.
Salomé (2002), directed by Carlos Saura, using flamenco dance.
Wilde Salome (2011), a film by Al Pacino. Salomé is played by Jessica Chastain.


== See also ==
List of biblical figures identified in extra-biblical sources
List of names for the biblical nameless


== References ==


== Further reading ==
Gillman, Florence Morgan (2003). Herodias: At Home in the Fox's Den. Interfaces. Collegeville, Minn.: Liturgical Press. ISBN 0-8146-5108-9
Claudel, Paul-André (2013). Salomé: Destinées imaginaires d'une figure biblique, Paris: Ellipses. ISBN 978-2729883171


== External links ==
Salome: Was the Dancing Daughter of Herodias a Child? | Marg Mowczko
Video Lecture on Salome by Dr. Henry Abramson