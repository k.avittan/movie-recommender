The term "behind closed doors" is used in several sports, primarily association football, to describe matches played where spectators are not allowed in the stadium to watch. The reasons for this may include punishment for a team found guilty of a certain act in the past, stadium safety issues, public health concerns, or to prevent potentially dangerous clashes between rival supporters. In football it is predicated by articles 7, 12 and 24 of FIFA's disciplinary code.

Crowdless games are a rare although not unheard-of occurrence in North American sports. When they do occur, it is usually the result of events beyond the control of the teams or fans, such as weather-related concerns, public health concerns, or wider civil disturbances unrelated to the game. For instance, the COVID-19 pandemic caused the entire 2020 Major League Baseball season to be played behind closed doors.


== Examples ==


=== Brazil ===
In Brazil, the practice of games without public access is known as "closed gates" (in Portuguese, portões fechados), even referred as such in the Brazilian Football Confederation's rulebook. Once it was applied to a whole tournament: two rounds of the Campeonato Catarinense second division in 2014 were behind closed doors because the participant clubs did not deliver the security checks for their stadia. Sanitary reasons dictated the restriction in 2009, where two games of the Série D were played behind closed doors due to the H1N1 flu pandemic.


=== France ===
The French league has a tough line on misconduct. Each smoke grenade sent to the pitch results in a fine, which can then result in playing behind closed doors. If the crowd isn't managed, the club may also be punished. Hence, almost every season, a handful of matches are played behind closed doors.


=== North America ===
"Behind closed doors" or "crowdless" matches in the major North American sports leagues have historically been exceedingly rare. Most professional sports in the United States have been "gate-driven leagues:" they relied upon ticket sales and other game-day expenditures such as parking and concessions for the majority of their income, and going crowdless would deprive the leagues of that revenue, while still leaving the leagues responsible for the salaries of the participants. Broadcasting revenues have only in the 21st century risen to become a revenue source substantial enough to offset the loss of ticket sales, and even then it has only been such for the highest level major leagues. Amateur leagues also have the ability to survive crowdless, given their much lower expenses; at the recreational level, most games do not draw crowds. In between, professional minor leagues and fringe leagues cannot practically operate crowdless; Minor League Baseball and the Canadian Football League both cancelled their summer 2020 seasons rather than attempt to go crowdless (the CFL has substantial television revenue, but the league had lost money even with that revenue, making it impossible to operate without ticket sales).
Hooliganism has been less of a factor in North American sports than in the rest of the world. Local law enforcement, private security contractors working for either the team or a league, and national agencies such as the United States Department of Homeland Security take large roles in preventing situations of fan violence before they can occur by restricting access to known troublesome fans either at the gate or at the stage of selling tickets (such as "do not sell" lists), along with heavy restrictions on bringing in items and screening with metal detectors and pat-down searches where bringing in a weapon or explosive device can result in immediate arrest and permanent banishment from a venue, and other examples such as the "clear bag" policy, which only allows spectators to bring in bags that can be easily seen through.
Teams also have incentive to prevent fan violence due to forfeit rules which come with penalties to their records and playoff positioning, and league sanctions such as fines and the stripping of draft picks due to neglecting to create a safe environment for players, which in turn can affect teams for years beyond a violent event. Also sporting events in North America are considered to be more of a family-friendly and uniting affair, as sports fans tend to respect each other as fans of a common sport. In other countries around the world, sports matches are sometimes proxies for bitter and long-standing ethnic, political, and religious divisions; this is less so in the United States, where the broad variety of sports options manifests as different ethnic groups preferring different sports, reducing the chance at intercultural rivalry within a single sport.
Furthermore, the much larger geographical footprint of the North American sports leagues mean that even fierce rivals are often based hundreds or even thousands of miles apart. For example, while the Dallas Cowboys are considered to be the bitter rivals of every other team in the NFC East, their nearest division is based almost 1,100 miles (1,800 km) from the Dallas metropolitan area by air, and over 1,300 miles (2,100 km) from Dallas by road. Fans elsewhere in the world can easily travel to most if not all of their league's stadiums by road or by train, and bus and rail carriers have evolved there to cater to the expected demand. In contrast, fans of the North American sports leagues would need to travel by air if they wanted to attend most of their team's road games. In contrast to the local derbies of European soccer, some North American teams in the same metropolitan areas, especially in baseball and (gridiron) football, are separated into opposite conferences or leagues so that they are among the least frequent opponents on their schedules, inhibiting the development of a crosstown rivalry and allowing fans in a metropolitan area to support both teams with minimal conflict. Even if many thousands of fans suddenly had the means and inclination to do that on a regular basis, the North American commercial aviation industry at present would not have sufficient spare capacity to accommodate them – even events such as the Super Bowl which draw the most interest from fans willing to travel cause a major logistical challenge for airlines and airport authorities. As a result, unlike in most smaller and more densely populated countries, teams are typically not required to (and typically do not) set aside whole sections of their stadiums for opposing fans. The lack of dedicated sections for opposing fans creates even further disincentive for them to travel to away games.
All of these reduce the chances of hooliganism, eliminate the need to segregate fans from opposing teams, and ultimately negate the threat of closed door game due as a consequence of fan violence. In rare circumstances where a serious incident has occurred (such the 2004 Pacers–Pistons brawl in the NBA), sports authorities have leaned toward identifying and excluding the specific people involved as opposed to indiscriminately punishing the wider, law-abiding fan base. In contrast to the rest of the world where "behind closed doors" games are given out as penalties for previous violations or to prevent potential violence (stadium safety issues, checkered history of rival supporters), such occurrences in North America have happened for entirely different reasons.


== History ==


=== 1980–81 European Cup Winners' Cup ===
After rioting by fans in the first leg against Castilla in Spain, West Ham United were forced to play the second leg at Upton Park in October 1980 to an empty stadium.


=== 1982–83 European Cup ===
After rioting by fans in a semifinal at Anderlecht in Belgium the previous April, Aston Villa were forced to begin their defence of the European Cup at an empty Villa Park in September 1982, with the match kicking off at 2:30 pm on a Wednesday afternoon.


=== 1987–88 European Cup ===
After UEFA ban resulting from the incidents in the match between Real Madrid and Bayern Munich in a semifinal in Spain the previous April, Real Madrid were forced to play the match at home against Napoli in the European Cup at an empty Santiago Bernabeu Stadium on 16 September 1987.


=== 2007 Italian football ===
As a result of a policeman being killed during rioting at a Serie A match between Catania and Palermo on 2 February, the Italian Football Federation suspended all Italian matches indefinitely. Subsequently, matches resumed but many clubs were ordered to play their games behind closed doors until their stadiums met with updated security regulations.


=== 2009 Italian football ===
Juventus were ordered to play a home game behind closed doors after their fans had racially abused Internazionale striker Mario Balotelli during a 1–1 Serie A draw in April 2009.


=== 2009 Davis Cup match in Sweden ===
In March 2009, Sweden's Davis Cup tennis team was slated to host a qualifying match against Israel's team in the city of Malmo. The Leftist local authorities in Malmo, particularly the city's then-Mayor, did not want an Israeli team to play there and pushed to cancel the match. When the Swedish Tennis Association angrily noted that a cancellation would result in Sweden suffering a forfeit loss and being eliminated from the Davis Cup altogether, it was agreed that the Sweden-Israel match would be played without spectators on nebulous "security grounds". Rioting outside the arena led to numerous arrests, and in the end Sweden lost to Israel and saw their 2009 Davis Cup run end anyway.


=== 2009–10 UEFA Europa League ===
FC Dinamo București had to play two home games in European competitions behind closed doors after their match against FC Slovan Liberec on 25 August 2009 was abandoned in the 88th minute due to a pitch invasion by Dinamo fans.


=== 2009 Mexico Clausura ===
During the penultimate round of league games all teams had to play with closed doors due to the H1N1 swine flu outbreak in infected cities. Several games taking place in areas which were badly affected by the outbreak were also played behind closed doors the following week. Games behind closed doors have been played regularly as a penalty for bad behavior of fans in Mexico, most recently an Apertura 2015 game in which Atlas hosted Querétaro at Estadio Jalisco because of last season quarterfinal game in which Atlas fans invaded the pitch against their hated rivals Guadalajara.


=== 2010–11 Heineken Cup ===
In rugby union, the 2010–11 Heineken Cup pool stage match between Edinburgh and Castres at Murrayfield was played behind closed doors on 20 December 2010. The match was originally scheduled for 19 December, but was postponed due to heavy snow in Edinburgh that covered the pitch and created major access issues for potential spectators. The competition organiser, European Rugby Cup, decided to hold the rescheduled match behind closed doors to remove any possible danger to spectators attempting to travel to the match.


=== Turkish football in 2011–12 ===
Starting with the 2011–12 season, the Turkish Football Federation (TFF) instituted a modified version of this rule. The penalty for a team sanctioned for crowd violence is now a ban on both ticket sales to, and attendance by, males over age 12 (as spectators). Women, and children under age 12 of either sex, are admitted free. The first game under the new rule took place on 20 September 2011, when Fenerbahçe hosted Manisaspor at Şükrü Saracoğlu Stadium in Istanbul. Over 41,000 women and children attended the match (plus a small number of men who had sneaked into the stadium). The experiment was so successful that the TFF planned to require that teams allocate an unspecified number of free tickets for women and children at all future club matches. Shortly thereafter, the TFF stated that it would reimburse clubs for free tickets given to women and children for regular league games (i.e., games not subject to crowd restrictions), and increased the upper age limit for "children" for the purposes of free ticketing to 15.


=== Ajax–AZ, 2011–12 KNVB Cup ===
A match between Eredivisie clubs Ajax and AZ in the fourth round of the 2011–12 KNVB Cup was replayed behind closed doors at Ajax's home ground, Amsterdam Arena, on 19 January 2012.
In the original match, held at the same venue on 21 December 2011, Ajax held a 1–0 lead when a fan ran onto the pitch and launched a karate kick from behind against AZ goalkeeper Esteban Alvarado, who responded by attacking the fan before police and security arrived. When Alvarado was sent off for retaliating against his attacker, AZ left the pitch, and the match was abandoned. The KNVB rescinded the red card and ordered the match replayed in its entirety.
After Ajax also apologised to their opponents, they were fined €10,000 for failing to provide adequate security to prevent the fan, who was serving a three-year stadium ban, from entering the arena. Ajax accepted the penalties, and announced that it had extended the fan's stadium ban by 30 years and banned him for life from the club and its season ticket list.


=== Barcelona–Las Palmas, 2017–18 La Liga ===
Surrounding violence in Catalonia due to the 2017 Catalan independence referendum, the match between FC Barcelona and UD Las Palmas in the 2017–18 La Liga was played behind closed doors. Barcelona first requested the LFP to postpone their match which was to be played on the same day as the referendum. This request was declined by the LFP, saying that, if Barcelona refused to play the match, their six points would be deducted. To protect the fans, as well as in protest to LFP's decision, Barcelona played the match behind closed doors, a first at their stadium, Camp Nou. The match ended 3–0 in favor of Barcelona.


=== Philippines–Qatar, 2019 FIBA Basketball World Cup Asian Qualifiers ===
As part of the sanctions handed to the Philippine men's basketball team in the aftermath of their involvement in their brawl against Australia during their home game on 2 July 2018, the Philippine national team is obliged to play their next home match in the second round of the 2019 FIBA Basketball World Cup Asian qualifiers behind closed doors with the succeeding two home games being placed under probation. The lone game that was played behind closed doors was their home game against Qatar on 17 September 2018 at the Smart Araneta Coliseum.


=== Major League Baseball ===

On 28 April 2015, Major League Baseball announced a game between the Chicago White Sox and Baltimore Orioles to be played at Oriole Park at Camden Yards in Baltimore that evening would instead start at 2:06 p.m. EDT, with no fans being admitted.  The unprecedented decision came due to security concerns related to riots and civil unrest in the city, along with a 10 p.m. EDT curfew which would have required suspension of the game had it been played at its original time.  The game was to have been the last game of a three-game series, but the first two had already been postponed due to the unrest. The game was televised in the Baltimore and Chicago markets, and was also offered as a "free game of the day" on MLB's streaming service nationwide. Unofficially, some fans were able to watch the game through obstructed gates in left-center field, along with guests at the nearby Hilton Baltimore which overlooks the playing field. This was the first time a North American major league sports event was held in an empty sports venue; previously in minor league baseball, a 2008 Iowa Cubs game was played without public admittance due to flooding in Des Moines, Iowa, while a 2002 Charleston RiverDogs game in Charleston, South Carolina held a purposeful gimmick "Nobody Night" where no one was admitted to the park until the attendance figure was made official after the fifth inning; although approximately 1,800 spectators were in attendance, they were restricted to areas around the stadium (concession stands, etc.) until the end of the fifth inning when the attendance count was made.


=== PGA Tour golf ===
While large galleries consisting of thousands of fans are standard for top-level professional golf events in the United States, on 30 June 2012, the third round of the 2012 AT&T National at Congressional Country Club in Bethesda, Maryland was played without fans being admitted due to trees being downed and other storm damage caused by a derecho the night before. Additionally, the start of the third round was delayed to the early afternoon, instead of a morning start that is normal for a round of a golf tournament utilizing a stroke play format, in order to allow for the continuation of course clean-up efforts. This is the first of two recorded instances of any part of a PGA Tour event being played without a gallery present.In the 2016 Farmers Insurance Open at Torrey Pines Golf Course in the La Jolla community of San Diego, California, after inclement weather disrupted play on Sunday and several trees were ripped apart on the golf course, fans were barred from the premises for the Monday conclusion of the final round of the tournament due to safety issues. The 2018 edition of the same tournament also was finished behind closed doors on Monday.  The sudden death playoff had reached five holes without a winner, and darkness made it impossible to complete the tournament.  The event concluded Monday morning at 8:18 a.m. behind closed doors.


=== Ice hockey ===
The Charlotte Checkers of the American Hockey League have played in two such "behind closed doors" games, both caused by severe winter storms when the opposing team and game officials had already arrived in the city.  On 22 January 2016, the Checkers played against the Chicago Wolves behind closed doors because of a severe winter storm, and the same again on 17 January 2018, the Checkers played against the Bridgeport Sound Tigers behind closed doors as a result of inclement weather (ice and snow).  Both games were played at Bojangles' Coliseum.In 1985, a measles outbreak on campus led Boston University to hold home ice hockey games without spectators for a week, in order to control the disease's spread. Basketball games were also affected, and other large public events on campus were also temporarily banned.


== COVID-19 pandemic ==

Games without crowds have become the norm due to the COVID-19 pandemic due to government-imposed restrictions on large gatherings to contain the virus. Many events that were not postponed or cancelled were held behind closed doors as broadcast only events.


=== Sports ===


==== Association football ====
The 2020 Indian Super League Final between ATK (football club) and Chennaiyin FC was forced to play behind closed doors due to the Coronavirus pandemic. ATK (football club) won the 2019–20 season, beating Chennaiyin FC 3–1.
The UEFA Europa League match between Inter Milan and Ludogorets Razgrad in San Siro Stadium, Milan, Italy, as well as all football matches in Italy between 4 March and 3 April.
The 2020 AFC Cup match between Ceres–Negros and Bali United at the Rizal Memorial Stadium in Manila, Philippines.
The final of the English FA Cup was held behind closed doors at Wembley Stadium in London, and played between Arsenal and Chelsea.  The traditional pre-match performances of the National Anthem and cup hymn still took place.
Major League Soccer returned after the 2020 Major League Soccer season was suspended, playing the MLS is Back Tournament behind closed doors at ESPN Wide World of Sports
The UEFA Europa League matches between Olympiacos and Wolverhampton Wanderers, and LASK and Manchester United.
The UEFA Champions League matches between Valencia and Atalanta, and Paris Saint-Germain and Borussia Dortmund. Despite the ban on fans inside the stadium, thousands gathered outside to cheer PSG as they beat Borussia Dortmund.
Germany's Bundesliga announced plans to resume its 2019–20 season without spectators. On 6 May 2020, German Chancellor, Angela Merkel, gave approval for the Bundesliga, suspended since March 2020, to recommence in May 2020 with all remaining games to be played behind closed doors.
South Korea's K League began its 2020 season on 8 May and Japan's J.League planned a mid-June return.


==== Athletics ====
The 2020 Tokyo Marathon was held with only elite competitors allowed to attend.
Similarly, the London Marathon (originally scheduled for 26 April, but was postponed to 4 October) was also held only to attend elite competitors.


==== Australian rules football ====
The 2020 AFL season played its first round of matches without spectators before suspending the season.


==== Baseball ====
Japan's NPB held preseason games without fans until the season was postponed prior to opening day. The league planned to return without fans by mid-June 2020.
South Korea's KBO League began their 2020 season on 5 May without fans in attendance.
Taiwan's CPB League began play on 11 April without fans. Starting 8 May, a maximum of 1,000 fans were permitted to attend games.
Major League Baseball in the United States and Canada began its shortened 2020 season on 23 July and held without spectators.


==== Basketball ====
The 2020 NCAA Division I Men's Basketball Tournament and other associated tournaments. Additionally, the Big Ten Conference announced that all spring sports played by member schools would be held without fans. This executive order also resulted in the NCAA choosing to restrict attendance of this and women's basketball tournaments to the participants' families, outside of areas where public attendance has been fully restricted. Both tournaments themselves would suffer their first cancellation in the 81-year history, including winter and spring sports.
On 11 March 2020, the National Basketball Association announced that a game between the Brooklyn Nets and the Golden State Warriors to be played at Chase Center in San Francisco during the regular matches of the 2019–20 season on the following day would start at its normal time of 10:30 p.m. EDT, with no fans being admitted. This measure was taken due to concerns about the spread of COVID-19 in the Bay Area, which prompted most cities in the metropolitan area (including San Francisco) to issue a ban on public gatherings with 1,000 people or higher. The restrictions were made moot with the league suspending the season a few hours later after Rudy Gobert tested positive for COVID-19. Starting 30 July 2020, the game resumed at ESPN Wide World of Sports Complex inside Walt Disney World near Orlando, Florida for the remainder of the season.
The 2020 PBA season is the 45th season of the Philippine Basketball Association.  Just three days after the opening ceremony, the season was suspended on March 11, due to the COVID-19 pandemic and the enforcement of the enhanced community quarantine in Luzon.  On September 17, the PBA Board of Governors have approved a plan to restart the season on October 11 (originally on October 9). The Inter-Agency Task Force on Emerging Infectious Diseases (IATF-ID) gave a provisional approval on September 24. All games will be played in the "PBA bubble" inside the Clark Freeport Zone in Pampanga.


==== Combat sports ====


===== Mixed martial arts =====
ONE Championship's King of the Jungle in Singapore.
Since 14 March 2020, Ultimate Fighting Championship events have been held behind closed doors, such as UFC Fight Night 170 took place in Brasília, subsequent events were cancelled and resumed on 9 May at UFC 249.


===== Professional wrestling =====
WWE has moved all of its professional wrestling programs to a studio at its training facility in Orlando since 13 March 2020 with no audience, up to and including its flagship event WrestleMania 36 (originally scheduled to be held at Tampa, Florida's Raymond James Stadium on 5 April). Rival promotion All Elite Wrestling similarly moved its weekly program AEW Dynamite to a closed set, with the 18 March episode being aired from Daily's Place in Jacksonville, Florida.


===== Sumo =====
The Haru basho in Osaka, Japan. When the tournament went underway several wrestlers, used to fighting in a usually packed arenas, remark on how odd it is to fight in a virtually empty arena, with Enhō commenting, "It’s like I can’t raise my fighting spirit...It made me wonder what I'm fighting for."


==== Cricket ====
3 of the 2020 Pakistan Super League matches in Karachi and 1 fixture in Lahore was held without an audience. The Final match in Lahore, was also rescheduled from 22 to 18 March. The matches in National Stadium, Karachi were announced to take place without an audience from 13 March. International players were also allowed to leave the tournament.
In Australia, a cricket match between New Zealand black caps and Australia's team was held without an audience on 13 March.
In July 2020, the Wisden Trophy between England and West Indies was held, in which a series of 3 test matches was played between them.
In August and September 2020, Pakistan toured England to play a series of 3 test matches and 3 Twenty20 International matches, in "bio-secure" conditions behind closed doors.
In September 2020, Australia toured England to play a series of 3 ODI matches and 3 Twenty20 International matches, in "bio-secure" conditions behind closed doors.
2020 IPL was held entirely in UAE behind closed doors in "bio-secure" conditions.


==== Golf ====
The 2020 Players Championship was scheduled to play on its second day as it was due to plan without spectators, organizers had to cancel the remainder of the event. This subsequently had the PGA Tour to suspend the event until 11 June. With the Masters Tournament (the first men's major championship of 2020) being rescheduled from April to November, while the PGA Championship and U.S. Open also being rescheduled to August and September, respectively from their usual May and June schedule, and the Open Championship cancelled for the first time since 1945. The PGA Championship became the first men's major of the season and was held behind closed doors.Likewise, the early LPGA season was merely affected by the pandemic as tournaments in Asia had cancelled early in response with the outbreak in China, as the LPGA Tour had to suspend until 30 July. With the ANA Inspiration tournament (the first women's major championship of 2020) being rescheduled from April to September, while the KPMG Women's PGA Championship and U.S. Women's Open also being rescheduled to October and December, respectively, with the Evian Championship (first rescheduled to 6–9 August after being postponed from the original July schedule) cancelled for the first time in its 26-year history. However, the AIG Women's British Open became the first women's major of the season and was held behind closed doors, took place on the usual August schedule and went on as planned.


==== Gridiron football ====
A match between the Seattle Dragons and LA Wildcats of the XFL was scheduled to be played behind closed doors; the league's decision came shortly after Washington state governor Jay Inslee announced a ban on public assemblies of over 250 people. The game was ultimately never played as the virus forced the league to prematurely terminate its 2020 season.


==== Horse racing ====
The British Horseracing Authority announced on 16 March that all horse racing in the UK would be held behind closed doors until the end of March. Similarly, the annual Triple Crown races in the United States, including Kentucky Derby, Preakness Stakes and Belmont Stakes were all held behind closed doors. The Kentucky Derby and Preakness States were both moved from their usual May schedule to September and October, respectively. While the Belmont Stakes took place in the original June schedule.


==== Ice hockey ====
The Columbus Blue Jackets of the National Hockey League announced on 11 March 2020 that all of their remaining home games in the 2019–20 season at Nationwide Arena in Columbus, Ohio would be played behind closed doors due to an executive order by Ohio governor Mike DeWine that banned public gatherings with an attendance of 1,000 people or more, which was itself enacted in order to stop the spread of the aforementioned coronavirus outbreak. This would subsequently become moot the following day when the NHL suspended its season. Starting August 1, the 2020 Stanley Cup playoffs commenced in the two bubble locations of Edmonton and Toronto with no fans present.


==== Motorsport ====
In Formula One, the first scheduled Grand Prix of 2020 in Australia was cancelled, then the Bahrain Grand Prix (the second scheduled event of the Championship) which was expected to be media/TV-only event, was later postponed to 29 November 2020 and will be taking place behind closed doors. The first few races of a rescheduled 2020 season, including two Grands Prix at Red Bull Ring (the Austrian and Styrian Grand Prix), the Hungarian and two Grands Prix at Silverstone, took place in "bio-secure" conditions behind closed doors.
NASCAR's Folds of Honor QuikTrip 500 and Dixie Vodka 400 races at Atlanta Motor Speedway and Homestead–Miami Speedway were not expected to admit fans; the events were later postponed. NASCAR resumed its season with The Real Heroes 400 at Darlington Raceway without fans in attendance. Throughout the remainder of the 2020 season, some tracks allowed a limited number of fans to attend races, the first of these tracks being Talladega Superspeedway on June 22, Bristol Motor Speedway on July 15, and New Hampshire Motor Speedway on August 2, while other tracks held races without fans in attendance.
The NASCAR All-Star Race at Bristol Motor Speedway admitted 30,000 fans. The race is currently the most attended U.S. sporting event since the pandemic began.
The 2020 24 Hours of Le Mans marked the first time no fans will be in attendance.


==== Tennis ====
The 2020 BNP Paribas Open in Indian Wells, California was postponed shortly before the beginning of the qualifying stage after a COVID-19 case was confirmed within Riverside County and the county's health department declared a public health emergency. Subsequently, both the Association of Tennis Professionals (ATP) and Women's Tennis Association (WTA) announced that their seasons would be suspended until at least 31 July, with the 2020 French Open being rescheduled to September and Wimbledon being cancelled for the first time since 1945. Furthermore, the US Open was played without spectators on the original late-August date.


=== Non-sporting events ===
The 2020 Dansk Melodi Grand Prix in Copenhagen, Denmark (a qualification event to the Eurovision Song Contest 2020, which was later canceled) was held with no audience being admitted.
The 2020 Geneva International Motor Show was cancelled days before its opening day due to Switzerland's self-imposed limit on gatherings of over 1,000 people. The Car of the Year award was presented live behind closed doors. A few car manufacturers who stayed, including Koeniggsegg, unveiled their models pre-recorded at the show.


== See also ==
Catania football riot


== References ==


== External links ==
Juventus game behind closed doors
Atalanta game behind closed doors
Italian football to be played behind closed doors?
Play games behind closed doors
Italy backs closed-doors football
Turkey must play behind closed doors after fight
Dinamo handed default defeat