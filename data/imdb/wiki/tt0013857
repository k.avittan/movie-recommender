Bag tags, also known as baggage tags, baggage checks or luggage tickets, have traditionally been used by bus, train, and airline carriers to route checked luggage to its final destination. The passenger stub is typically handed to the passenger or attached to the ticket envelope:
a) to aid the passenger in identifying their bag among similar bags at the destination baggage carousel;
b) as proof—still requested at a few airports—that the passenger is not removing someone else's bag from the baggage reclaim hall; and
c) as a means for the passenger and carrier to identify and trace a specific bag that has gone astray and was not delivered at the destination. The carriers' liability is restricted to published tariffs and international agreements.


== History ==


=== Invention ===
The first "separable coupon ticket" was patented by John Michael Lyons of Moncton, New Brunswick on June 5, 1882. The ticket showed the issuing station, the destination, and a consecutive number for reference. The lower half of the ticket was given to the passenger, while the upper half, with a hole at the top, was inserted into a brass sleeve and then attached to the baggage by a strap.At some point, reinforced paper tags were introduced. These are designed not to detach as easily as older tags during transport.


=== Warsaw Convention ===
The Warsaw Convention of 1929, specifically Article Four, established the criteria for issuing a baggage check or luggage ticket. This agreement also established limit of liability on checked baggage.


=== Previous bag tags ===
Prior to the 1990s, airline bag tags consisted of a paper tag attached with a string.
The tag contained basic information:

Airline/carrier name
Flight number
Baggage tag number (composed of the two-letter airline code and six digits)
Destination airport codeThese tags became obsolete because they offered little security and were easy to replicate.


=== Current bag tags ===
Current bag tags include a bar code using the Interleaved 2 of 5 symbology. These bag tags are printed using a thermal or barcode printer on an adhesive thermal paper stock. This printed strip is then attached to the luggage at check-in, allowing automated sorting of the bags by bar code readers.
There are two ways that bar code baggage tags are read: hand held scanners, and in-line arrays. In-line arrays are built into the baggage conveyor system and use a 360-degree array of lasers or cameras to read the bar code tags from multiple angles because baggage and the orientation of the bar code tag can shift as the bag travels through the conveyor belt system. Camera systems are replacing lasers due to their greater ability to read damaged or folded tags.
One of the limitations of this system is that in order to read bar codes from the bottom of the belt, laser or camera arrays are placed below the gap between two sections of conveyor belt. Due to the frequent build-up of debris and dust on these lower arrays, the rate of successful reads can be low, although the camera systems improve the likelihood of reading tags from this position because of the algorithms used in their software. 
Frequently, the "read rate", the percentage of bar code tags successfully read by these arrays, can be as low as 85%. This means that more than one out of ten bar code baggage tags are not successfully read, and these bags are shunted off for manual reading, resulting in extra labor and delay. Systems employing cameras typically have better read rates than those using lasers - up to 99.5% in ideal conditions.
For flights departing from an international airport within the European Union, bag tags are issued with green edges. Passengers are eligible to take these bags through a separate "Blue Channel" (or alternatively the "Green Channel" = "nothing to declare") at Customs if arriving at another EU airport.
Bar codes cannot be automatically scanned without direct sight and undamaged print. Because of reading problems with poorly printed, obscured, crumpled, scored or otherwise damaged bar codes, some airlines have started using radio-frequency identification (RFID) chips embedded in the tags.
In the US, McCarran International Airport has installed an RFID system throughout the airport. Hong Kong International Airport has also installed an RFID system. The International Air Transport Association (IATA) is working to standardize RFID bag tags.
British Airways is currently conducting a trial to test re-usable electronic luggage tags featuring electronic paper technology. The passenger checks in using the British Airways smartphone app, then holds the smartphone close to the tag. The flight details and barcode are transmitted to the tag using NFC technology. Because the tag utilises electronic paper, the battery need only power the tag during the transmission of data.Fast Travel Global Ltd has developed a re-usable electronic luggage tag product called the eTag. This is also electronic paper-based but is not limited to a single airline. The passenger will check in using a supported airline's smartphone app and send the relevant flight information to the tag via Bluetooth Low Energy.
Qantas introduced Q Bag Tags in 2011. Unlike the British Airways tags, they do not feature a screen, which means there is no barcode to scan. This has limited the use of the tags to domestic flights within Australia on the Qantas network. The tags were initially given free of charge to members of the Qantas Frequent Flyer program with Silver, Gold or Platinum status. The tags can also be purchased for A$29.95.Over the last years, there have been numerous of initiatives to develop electronic bag tags, by both independent technology companies as well as some airlines. The main benefits of electronic bag tags include self-control and ease-of-use by passengers, time-saving by skipping queues at the airport, improved read rates compared to printed bag tags and, as electronic bag tags are adopted, significant operational cost reduction for the airlines.
The first company to successfully launch has been Rimowa in a partnership with Lufthansa in March, 2016. The concept of electronic bag tags has been gaining ground following that launch. On January 9, 2018, Lufthansa introduced a new electronic bag tag to their passengers, BAGTAG. BAGTAG is the first fully secure operational electronic bag tag that can be attached to any suitcase and has integrated radio-frequency identification technology.


== Identification ==
The first automated baggage sorting systems were developed in the 1980s by Eastern Air Lines at their Miami International Airport hub. Other airlines soon followed with their own systems, including United Air Lines, TWA, Delta, and American Airlines. None of these systems were interchangeable. In some systems, the bar code was used to represent a three-letter destination airport code, and in others it was a two-digit sorting symbol instructing the system at which pier to deliver the bag.
As a result of the bombing of Air India Flight 182 on June 23, 1985, the airline industry, led by IATA, convened the Baggage Security Working Group (BSWG) to change international standards and require passenger baggage reconciliation. The Chairman of the BSWG, John Vermilye of Eastern Airlines, proposed that the industry adopt the already-proven license plate system.
This concept used a barcode to represent the baggage tag number. At check-in, this number was associated with the passenger details, including flight number, destination, connection information, and even class of service to indicate priority handling.
Working with Allen Davidson of Litton Industries, with whom Eastern had developed the license plate concept, the BSWG adopted this system as the common industry standard for passenger baggage reconciliation. Initially the barcode, or license plate, was used to match baggage with passengers, ensuring that only the baggage of passengers who had actually boarded the flight were carried onto the aircraft. This standard was adopted  by IATA Resolution in 1987.By 1989, the license plate concept was expanded to become the industry standard for automated baggage sorting as well. The barcodes were enlarged to facilitate automated reading. The barcode was shown in two different orientations or in a "T" shape, called the "orthogonal" representation.
The term license plate is the official term used by the IATA, the airlines, and the airports for the ten-digit numeric code on a bag tag issued by a carrier or handling agent at check-in. The license plate is printed on the carrier tag in barcode form and in human-readable form (as defined in Resolution 740 in the IATA Passenger Services Conference Resolutions Manual, published annually by IATA).
The license plate is the index number linking the Baggage Source Message (BSM), sent by a carrier's departure control system, to the airport's baggage handling system. This message (BSM) contains the flight details and passenger information. Each digit in the license plate has a specific meaning. The automated baggage handling system scans the barcodes on the carrier tags and sorts the bags accordingly. Both the license plate number and the BSM are essential for automated sorting of baggage.
The human-readable license plate will have either a two-character or a three-digit IATA carrier code. For example, it may be either "BA728359" or "0125728359." "BA" would be the two-character IATA code for British Airways), and "125" would be the three-digit IATA carrier code. Nevertheless, the barcode will always be the full ten digits.
The first digit in the ten-digit license plate is not part of the carrier code. It can be in the range of zero to nine.
Zero is for interline or online tags, one is for fallback tags, and two is for "rush" tags. 
Fallback tags are pre-printed or demand-printed tags for use only by the airport's baggage handling system. These tags are used when there is a problem in communication between the carrier's departure control system and the airport's baggage handling system (as defined in IATA Recommended Practice 1740b). 
A "rush" bag is a bag that missed its original flight and is now flying unaccompanied.The purpose of numbers in the range of three to nine is not defined by the IATA, but they can be used by each carrier for their own specific needs. The first digit is commonly used as a million indicator for the normal six-digit tag number.Besides the license plate number, the tag also has:

Name of Airport of Arrival
Departure Time
IATA airport code of Airport of Arrival
Airline Code and Flight Number
Name of Passenger Identified with the Baggage (Last Name, First Name)


== References ==