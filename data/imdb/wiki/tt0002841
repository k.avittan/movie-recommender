Sir John Falstaff is a fictional character who appears in three plays by William Shakespeare and  is eulogized in a fourth. His significance as a fully developed character is primarily formed in the plays Henry IV, Part 1 and Part 2, where he is a companion to Prince Hal, the future King Henry V of England. A notable eulogy for Falstaff is presented in Act II, Scene III of Henry V, where Falstaff does not appear as a character on stage, as enacted by Mistress Quickly in terms that some scholars have ascribed to Plato's description of the death of Socrates after drinking hemlock. By comparison, Falstaff is presented as the buffoonish suitor of two married women in The Merry Wives of Windsor.
Though primarily a comic figure, Falstaff still embodies a kind of depth common to Shakespeare's major characters. A fat, vain, boastful, and cowardly knight, he spends most of his time drinking at the Boar's Head Inn with petty criminals, living on stolen or borrowed money. Falstaff leads the apparently wayward Prince Hal into trouble, and is ultimately repudiated after Hal becomes king. Falstaff has since appeared in other media, notably in operas by Giuseppe Verdi, Ralph Vaughan Williams, and Otto Nicolai, and in Orson Welles' 1966 film Chimes at Midnight. The operas focus on his role in The Merry Wives of Windsor, while the film adapts from the Henriad and The Merry Wives. Welles, who played Falstaff in his film, considered the character to be "Shakespeare's greatest creation".


== Role in the plays ==

Falstaff appears in three of Shakespeare's plays, Henry IV, Part 1, Henry IV, Part 2, and The Merry Wives of Windsor. His death is mentioned in Henry V but he has no lines, nor is it directed that he appear on stage. However, many stage and film adaptations have seen it necessary to include Falstaff for the insight he provides into King Henry V's character. The most notable examples in cinema are Laurence Olivier's 1944 version and Kenneth Branagh's 1989 film, both of which draw additional material from the Henry IV plays.
The character is known to have been very popular with audiences at the time, and for many years afterwards. According to Leonard Digges, writing shortly after Shakespeare's death, while many plays could not get good audiences, "let but Falstaff come, Hal, Poins, the rest, you scarce shall have a room".


=== Henry IV, Part 1 ===

King Henry is troubled by the behaviour of his son and heir, the Prince of Wales. Hal (the future Henry V) has forsaken the Royal Court to waste his time in taverns with low companions. This makes him an object of scorn to the nobles and calls into question his royal worthiness. Hal's chief friend and foil in living the low life is Sir John Falstaff. Fat, old, drunk, and corrupt as he is, he has a charisma and a zest for life that captivates the Prince.
Hal likes Falstaff but makes no pretense at being like him. He enjoys insulting his dissolute friend and makes sport of him by joining in Poins' plot to disguise themselves and rob and terrify Falstaff and three friends of loot they have stolen in a highway robbery, purely for the fun of watching Falstaff lie about it later, after which Hal returns the stolen money. Rather early in the play, in fact, Hal informs us that his riotous time will soon come to a close, and he will re-assume his rightful high place in affairs by showing himself worthy to his father and others through some (unspecified) noble exploits. Hal believes that this sudden change of manner will amount to a greater reward and acknowledgment of prince-ship, and in turn earn him respect from the members of the court.
On the way to this climax, we are treated to Falstaff, who has "misused the King's press damnably", not only by taking money from able-bodied men who wished to evade service but by keeping the wages of the poor souls he brought instead who were killed in battle ("food for powder, food for powder"). Left on his own during Hal's battle with Hotspur, Falstaff dishonourably counterfeits death to avoid attack by Douglas. After Hal leaves Hotspur's body on the field, Falstaff revives in a mock miracle. Seeing he is alone, he stabs Hotspur's corpse in the thigh and claims credit for the kill. Though Hal knows better, he allows Falstaff his disreputable tricks. Soon after being given grace by Hal, Falstaff states that he wants to amend his life and begin "to live cleanly as a nobleman should do".


=== Henry IV, Part 2 ===

The play focuses on Prince Hal's journey toward kingship, and his ultimate rejection of Falstaff. However, unlike Part One, Hal's and Falstaff's stories are almost entirely separate, as the two characters meet only twice and very briefly. The tone of much of the play is elegiac, focusing on Falstaff's age and his closeness to death, which parallels that of the increasingly sick king.
Falstaff is still drinking and engaging in petty criminality in the London underworld. He first appears, followed by a new character, a young page whom Prince Hal has assigned him as a joke. Falstaff enquires what the doctor has said about the analysis of his urine, and the page cryptically informs him that the urine is healthier than the patient. Falstaff delivers one of his most characteristic lines:  "I am not only witty in myself, but the cause that wit is in other men." Falstaff promises to outfit the page in "vile apparel" (ragged clothing). He then complains of his insolvency, blaming it on "consumption of the purse." They go off, Falstaff vowing to find a wife "in the stews" (i.e., the local brothels).
The Lord Chief Justice enters, looking for Falstaff.  Falstaff at first feigns deafness in order to avoid conversing with him, and when this tactic fails pretends to mistake him for someone else.  As the Chief Justice attempts to question Falstaff about a recent robbery, Falstaff insists on turning the subject of the conversation to the nature of the illness afflicting the King. He then adopts the pretense of being a much younger man than the Chief Justice:  "You that are old consider not the capacities of us that are young."  Finally, he asks the Chief Justice for one thousand pounds to help outfit a military expedition, but is denied.

He has a relationship with Doll Tearsheet, a prostitute, who gets into a fight with Ancient Pistol, Falstaff's ensign. After Falstaff ejects Pistol, Doll asks him about the Prince. Falstaff is embarrassed when his derogatory remarks are overheard by Hal, who is present disguised as a musician. Falstaff tries to talk his way out of it, but Hal is unconvinced. When news of a second rebellion arrives, Falstaff joins the army again, and goes to the country to raise forces. There he encounters an old school friend, Justice Shallow, and they reminisce about their youthful follies. Shallow brings forward potential recruits for the loyalist army: Mouldy, Bullcalf, Feeble, Shadow and Wart, a motley collection of rustic yokels. Falstaff and his cronies accept bribes from two of them, Mouldy and Bullcalf, not to be conscripted.
In the final scene, Falstaff, having learned from Pistol that Hal is now King, travels to London in expectation of great rewards. But Hal rejects him, saying that he has now changed, and can no longer associate with such people. The London lowlifes, expecting a paradise of thieves under Hal's governance, are instead purged and imprisoned by the authorities.


=== Henry V ===

Although Falstaff does not appear on stage in Henry V, his death is the main subject of Act 2, Scene 3, in which Mistress Quickly delivers a memorable eulogy:


=== The Merry Wives of Windsor ===

Falstaff arrives in Windsor very short on money. To obtain financial advantage, he decides to court two wealthy married women, Mistress Ford and Mistress Page. Falstaff decides to send the women identical love letters and asks his servants – Pistol and Nym – to deliver them to the wives. When they refuse, Falstaff sacks them, and, in revenge, the men tell Ford and Page (the husbands) of Falstaff's intentions. Page is not concerned, but the jealous Ford persuades the Host of the Garter Inn to introduce him to Falstaff as a 'Master Brook' so that he can find out Falstaff's plans. 
When the women receive the letters, each goes to tell the other, and they quickly find that the letters are almost identical. The "merry wives" are not interested in the ageing, overweight Falstaff as a suitor; however, for the sake of their own amusement and to gain revenge for his indecent assumptions towards them both, they pretend to respond to his advances.
This all results in great embarrassment for Falstaff. Mr. Ford poses as 'Mr. Brook' and says he is in love with Mistress Ford but cannot woo her as she is too virtuous. He offers to pay Falstaff to court her, saying that once she has lost her honour he will be able to tempt her himself. Falstaff cannot believe his luck, and tells 'Brook' he has already arranged to meet Mistress Ford while her husband is out. Falstaff leaves to keep his appointment and Ford soliloquises that he is right to suspect his wife and that the trusting Page is a fool.
When Falstaff arrives to meet Mistress Ford, the merry wives trick him into hiding in a laundry basket ("buck basket") full of filthy, smelly clothes awaiting laundering. When the jealous Ford returns to try and catch his wife with the knight, the wives have the basket taken away and the contents (including Falstaff) dumped into the river. Although this affects Falstaff's pride, his ego is surprisingly resilient. He is convinced that the wives are just playing hard to get with him, so he continues his pursuit of sexual advancement, with its attendant capital and opportunities for blackmail.
Again Falstaff goes to meet the women but Mistress Page comes back and warns Mistress Ford of her husband's approach again. They try to think of ways to hide him other than the laundry basket which he refuses to get into again. They trick him again, this time into disguising himself as Mistress Ford's maid's obese aunt, known as "the fat woman of Brentford". Ford tries once again to catch his wife with the knight but ends up beating the "old woman", whom he despises, and throwing her out of his house. Black and blue, Falstaff laments his bad luck.
Eventually the wives tell their husbands about the series of jokes they have played on Falstaff, and together they devise one last trick which ends up with the Knight being humiliated in front of the whole town. They tell Falstaff to dress as "Herne, the Hunter" and meet them by an old oak tree in Windsor Forest (now part of Windsor Great Park). They then dress several of the local children as fairies and get them to pinch and burn Falstaff to punish him.
The wives meet Falstaff, and almost immediately the "fairies" attack. After the chaos, the characters reveal their true identities to Falstaff. Although he is embarrassed, Falstaff takes the joke surprisingly well, as he sees it was what he deserved. Ford says he must pay back the 20 pounds 'Brook' gave him and takes the Knight's horses as recompense. Eventually they all leave together and Mistress Page even invites Falstaff to come with them: "let us every one go home, and laugh this sport o'er by a country fire; Sir John and all".


== Origins ==


=== John Oldcastle ===
Shakespeare originally named Falstaff "John Oldcastle". Lord Cobham, a descendant of the historical John Oldcastle, complained, forcing Shakespeare to change the name. Shakespeare's Henry IV plays and Henry V adapted and developed the material in an earlier play called The Famous Victories of Henry V, in which Sir John "Jockey" Oldcastle appears as a dissolute companion of the young Henry. Prince Hal refers to Falstaff as "my old lad of the castle" in the first act of the play; the epilogue to Henry IV, Part 2, moreover, explicitly disavows any connection between Falstaff and Oldcastle: "Oldcastle died a martyr, and this is not the man."The historical Oldcastle was a Lollard who was executed for heresy and rebellion, and he was respected by many Protestants as a martyr. In addition to the anonymous The Famous Victories of Henry V, in which Oldcastle is Henry V's companion, Oldcastle's history is described in Raphael Holinshed's Chronicles, Shakespeare's usual source for his histories.


=== Cobhams ===
It is not clear, however, if Shakespeare characterised Falstaff as he did for dramatic purposes, or because of a specific desire to satirise Oldcastle or the Cobhams. Cobham was a common butt of veiled satire in Elizabethan popular literature; he figures in Ben Jonson's Every Man in His Humour and may have been part of the reason The Isle of Dogs was suppressed. Shakespeare's desire to burlesque a hero of early English Protestantism could indicate Roman Catholic sympathies, but Henry Brooke, 11th Baron Cobham was sufficiently sympathetic to Catholicism that in 1603, he was imprisoned as part of the Main Plot to place Arbella Stuart on the English throne, so if Shakespeare wished to use Oldcastle to embarrass the Cobhams, he seems unlikely to have done so on religious grounds.
The Cobhams appear to have intervened while Shakespeare was in the process of writing either The Merry Wives of Windsor or the second part of Henry IV. The first part of Henry IV was probably written and performed in 1596, and the name Oldcastle had almost certainly been allowed by Master of the Revels Edmund Tilney. William Brooke, 10th Baron Cobham may have become aware of the offensive representation after a public performance; he may also have learned of it while it was being prepared for a court performance (Cobham was at that time Lord Chamberlain). As father-in-law to the newly widowed Robert Cecil, Cobham certainly possessed the influence at court to get his complaint heard quickly. Shakespeare may have included a sly retaliation against the complaint in his play The Merry Wives of Windsor (published after the Henry IV series). In the play, the paranoid, jealous Master Ford uses the alias "Brook" to fool Falstaff, perhaps in reference to William Brooke. At any rate, the name is Falstaff in the Henry IV, Part 1 quarto, of 1598, and the epilogue to the second part, published in 1600, contains this clarification:


=== Sir John Fastolf ===
The new name "Falstaff" probably derived from the medieval knight Sir John Fastolf (who may also have been a Lollard). The historical John Fastolf fought at the Battle of Patay against Joan of Arc, which the English lost. Fastolf's previous actions as a soldier had earned him wide respect, but he seems to have become a scapegoat after the debacle. He was among the few English military leaders to avoid death or capture during the battle, and although there is no evidence that he acted with cowardice, he was temporarily stripped of his knighthood. Fastolf appears in Henry VI, Part 1 in which he is portrayed as an abject coward. In the First Folio his name is spelled "Falstaffe", so Shakespeare may have directly appropriated the spelling of the name he used in the earlier play. In a further comic double meaning, the name implies impotence.


=== Robert Greene ===
It has been suggested that the dissolute writer Robert Greene may also have been an inspiration for the character of Falstaff. This theory was first proposed in 1930 and has recently been championed by Stephen Greenblatt. Notorious for a life of dissipation and debauchery somewhat similar to Falstaff, he was among the first to mention Shakespeare in his work (in Greene's Groats-Worth of Wit), suggesting to Greenblatt that the older writer may have influenced Shakespeare's characterisation.


== Cultural adaptations ==

There are several works about Falstaff, inspired by Shakespeare's plays:


=== Drama ===
Falstaff's Wedding (1766), by William Kenrick was set after the events of Henry IV, Part 2. To restore his financial position after his rejection by Hal, Falstaff is forced to marry Mistress Ursula (a character briefly mentioned by Shakespeare, whom Falstaff has "weekly" promised to marry). The play exists in two very different versions. In the first version Falstaff is drawn into Scroop's plot to murder the king, but wins back Henry's favour by exposing the plot. In the second this story is dropped for a purely farcical storyline.


=== Music ===

Falstaff (1799), Antonio Salieri's opera, with a libretto by Carlo Prospero Defranceschi, which is based upon The Merry Wives of Windsor.
Falstaff (1838), an opera by Michael William Balfe to an Italian libretto by S. Manfredo Maggione that is based upon The Merry Wives of Windsor.
Die lustigen Weiber von Windsor (1849) by Otto Nicolai, based upon The Merry Wives of Windsor.
Le songe d'une nuit d'été (1850), an opera by Ambroise Thomas in which Shakespeare and Falstaff meet.
Falstaff (1893), Giuseppe Verdi's last opera, with a libretto by Arrigo Boito. It is mostly based upon The Merry Wives of Windsor.
Falstaff (1913), a "symphonic study" by Elgar, which is a sympathetic and programmatic musical portrait.
At the Boar's Head (1925), a short opera by Gustav Holst based on the Henry IV plays.
Sir John in Love (1929), an opera by composer Ralph Vaughan Williams based upon The Merry Wives of Windsor.
Plump Jack (1985/2005), an opera with both libretto and music by composer Gordon Getty, adapted from the text of both the Henry IV and Henry V plays.


=== Film and television ===
On film, Falstaff appeared in Laurence Olivier's acclaimed 1944 version of Henry V. Although Falstaff does not appear in the play, Olivier inserted an original scene depicting the fat knight - played by George Robey, who first previously performed the role in a stage production of Henry IV, Part 1 in 1935 - as a dying, heartbroken old man attended by Mistress Quickly, pathetically reliving in his mind his rejection by Henry. This was immediately followed by the actual scene from the play of Mistress Quickly describing Falstaff's death to his grieving followers.
Orson Welles's Chimes at Midnight (1966) compiles the two Henry IV plays into a single, condensed storyline, while adding a handful of scenes from Richard II and Henry V. The film, also known as Falstaff, features Welles himself in the title role, with film critic Vincent Canby stating in 1975 that it "may be the greatest Shakespearean film ever made, bar none".
Falstaff appeared in the 1960 series An Age of Kings, which was actually a 15 part series depicting Shakespeare's history plays from Richard II to Richard III; in the Henry IV episodes he was played by Frank Pettingell.
In the 1979 season of the BBC Shakespeare series, in both parts of Henry IV Falstaff was played by Anthony Quayle, and in The Merry Wives of Windsor which followed in the 1982 season, by Richard Griffiths.
In Kenneth Branagh's acclaimed 1989 version of Henry V, Falstaff, here played by Robbie Coltrane, as in the Olivier version is given an original scene, this time dying in his bed and attended by Mistress Quickly, while downstairs his followers share a flashback - put together from various bits from both parts of Henry IV - showing the fat knight carousing with Henry back when he was "madcap prince" Hal, but it ends abruptly when the prince makes an ominous hint that some day when he becomes King he will be banishing his old friend. Later,  prior to the actual scene where Mistress Quickly describes his death, there is a fleeting close-up shot of her sadly examining the knight's now deceased body one last time before going downstairs to his followers.
Falstaff appeared in the Michael Bogdanov/Michael Pennington's English Shakespeare Company's presentation of Shakespeare's plays concerning The Wars of the Roses; originally taped live during their final tour with the series in 1989. In the Henry IV episodes, Falstaff was played by Barry Stanton, who later played the Chorus in Henry V.  Although Falstaff never actually appeared in the production of Henry V, there is a humorous scene in silhouette prior to the scene where Mistress Quickly describing his funeral, depicting Falstaff's funeral procession, with a group of soldiers staggering under the weight of his coffin (an obvious nod to the final scene in Chimes at Midnight).
Gus Van Sant’s My Own Private Idaho is partially a retelling of the Henry IV plays, set in the contemporary US, and with the character of Bob Pigeon (William Richert) representing Falstaff.
In the 2012 television series The Hollow Crown, which likewise consisted of Shakespeare's plays concerning the Wars of the Roses, Falstaff was played by Simon Russell Beale. Just as in Olivier's and Branagh's film versions of Henry V, the Falstaff in this series appeared in the Henry V episode as well the Henry IV ones, sadly recollecting his rejection by his former friend while he is dying.
In Phyllida Lloyd's 2017 all-female Donmar Warehouse production of Henry IV (combining both parts), which was videotaped and broadcast, Sophie Stanton played Falstaff.
In the 2019 Netflix film The King, Falstaff (played by Joel Edgerton) proposes to Henry V the military tactics employed by the English in the Battle of Agincourt and dies in the battle.


=== Print ===
Alexander Smith (pseud.) "Sir John Falstaff a Notorious Highwayman" in A Compleat History of the Lives and Robberies of the most Notorious Highway-Men, Foot-Pads, Shop-Lifts, and Cheats, of Both Sexes (London: J. Morphew, 1714)
James White's book Falstaff's Letters (1796) purports to be a collection of letters written by Falstaff, provided by a descendant of Mistress Quickly's sister. She had inherited them from Mistress Quickly herself, who kept them in a drawer in the Boar's Head Tavern until her death in "August 1419".
The Life of Sir John Falstaff (1858), a novel by Robert Barnabas Brough.
Falstaff (1976), a novel by Robert Nye.


== See also ==
Onufry Zagłoba, similar character in Henryk Sienkiewicz's series of three novels, The Trilogy
Volstagg the Voluminous, a Marvel Comics character and companion to Thor created as an homage to Falstaff


== Notes and references ==


== Sources ==


== Further reading ==


== External links ==
Henry the Fourth part 1 at Project Gutenberg.