Edmond Hoyle (1672 – 29 August 1769) was a writer best known for his works on the rules and play of card games. The phrase "according to Hoyle" (meaning "strictly according to the rules") came into the language as a reflection of his generally perceived authority on the subject; since that time, use of the phrase has expanded into general use in situations in which a speaker wishes to indicate an appeal to a putative authority.


== Early life undocumented ==
Little is known about Hoyle's life; he is primarily known through his books. Much of what is written about him is untrue or exaggerated. The suggestion that he trained at the bar seems unfounded.


== Treatise on whist ==
By 1741, Hoyle began to tutor members of high society at the game of whist, selling his students a copy of his manuscript notes. Hoyle expanded the manuscript and published A Short Treatise on the Game of Whist in 1742, selling it for the high price of one guinea.
When the book quickly sold out, rather than publish a new edition, Hoyle sold the rights to Whist to bookseller Francis Cogan for 100 guineas, an enormous sum for a small pamphlet. Before Cogan was able to publish a second edition, two printers pirated the work, giving the author as "A Gentleman" rather than Hoyle. The printers disguised their identities by publishing under false names, one as Webster, the other as Webb. Cogan published second and third editions of Whist and two months later, obtained an injunction against the pirates which he announced in a fourth edition (all 1743). To distinguish the genuine editions from the piracies, Cogan paid Hoyle twopence per copy to autograph the genuine works. The piracies were profitable to Hoyle, though a disaster for Cogan who was forced to lower the price of the book to match the pirates and to pay for Hoyle's signature.


=== Superseded by new rules ===
The rules of whist published in A Short Treatise on the Game of Whist were regarded as authoritative until 1864, after which time they were superseded by the new rules written by John Loraine Baldwin and adopted by the Arlington and Portland clubs.


== Other published works ==

Cogan published other works by Hoyle: A Short Treatise on the Game of Backgammon (1743), the curious An Artificial Memory for Whist (1744), and more short treatises on the games of piquet and chess (1744) and quadrille (1744).
Cogan became bankrupt in 1745 and sold the Hoyle copyrights to Thomas Osborne, who published Hoyle's treatises with much more success.Hoyle wrote a treatise on the game of brag (1751), a book on probability theory (1754), and one on chess (1761). Over time, Hoyle's work pushed off the market Charles Cotton's ageing The Compleat Gamester, which had been considered the "standard" English-language reference work on the playing of games – especially gambling games – since its publication in 1674.


=== Collected edition ===
In 1748, Osborne stopped publishing the individual treatises and instead sold a collected edition under the title Mr. Hoyle's Treatises of Whist, Quadrille, Piquet, Chess and Back-Gammon. The whist treatise was described as the eighth edition. The fourteenth edition (1765) was the last published during Hoyle's lifetime. Fifteenth and sixteenth editions appeared after his death, with the autograph reproduced by woodblock print.


=== Reprints ===
The books were frequently reprinted in Ireland, something that was permitted as the English copyright statute, the Statute of Anne, did not extend to Ireland. One edition was printed in Edinburgh. Hoyle's writing was translated into many continental languages; first Portuguese (1753), then German (1754), French (1761), Italian (1768), Russian (1769), and Dutch (1790).Various facsimile and revised editions have appeared over the decades and centuries, often titled Hoyle's Rules or Hoyle's Games in English.


== Legacy and modern usage ==
Because of his contributions to gaming, Hoyle was a charter inductee into the Poker Hall of Fame in 1979, even though he died 60 years before poker was invented.The phrase according to Hoyle still has some currency in contemporary English, meaning 'correctly or properly; according to an authority or rule'.  In British English, a Hoyle can refer to any authoritative card-game rule book, similar to the usage of a Baedeker to refer to any travel guide.
Many modern books of collected rule sets for card games (and sometimes other games, such as board games, billiards, etc.) contain the name "Hoyle" in their titles, but the moniker does not mean that the works are directly derivative of Edmond Hoyle's (in much the same way that many modern dictionaries contain "Webster" in their titles without necessarily relating to the work of Noah Webster).


== References ==


== Bibliography ==
By a Gentleman (1743) A Short Treatise on the Game of Whist, W. Webster, Bath and London
Edmond Hoyle (1743) A Short Treatise on the Game of Whist, 3rd Ed., F. Cogan, London
Edmond Hoyle (1744) A Short Treatise on the Game of Piquet, George and Alexander Ewing, Dublin
Edmond Hoyle (1745) A Short Treatise on the Game of Quadrille, George and Alexander Ewing, Dublin
Edmond Hoyle (1761) An Essay Towards Making the Game of Chess Easily Learned,, Thomas Osborne, London
Edmond Hoyle (1764) An Essay Towards Making the Doctrine of Chances Easy to Those Who Understand Vulgar Arithmetick Only, Thomas Osborne, London
Edmond Hoyle (1775) Mr. Hoyle's Games of Whist, Quadrill, Piquet, Chess and Back-Gammon, Thomas Osborne, London