The Last Edition is a 1925 American silent drama directed by Emory Johnson based on the story by Emilie Johnson. The photoplay is set in San Francisco, California and stars Ralph Lewis as a pressman at the San Francisco Chronicle newspaper. The movie was released on November 8, 1925 by Film Booking Offices of America.The motion picture was filmed in and around the "Old Chronicle Building" located at 690 Market Street in downtown San Francisco. However, "In 1924, the Chronicle commissioned a new headquarters at 901 Mission Street on the corner of 5th Street."  The Chronicle completed the move in 1925, shortly after the film crews were finished shooting.


== Plot ==
The story starts by introducing us to Tom McDonald played by Ralph Lewis. Tom is a pressman and the assistant foreman in the San Francisco Chronicle pressroom. Tom finds out he was passed over for the job of press foreman. The job was given to a younger man. Though disappointed, he takes solace knowing his son Ray McDonald, played by Ray Hallor, has a good job in the district attorney's office. Tom also has a daughter – Polly played by Frances Teague.
Clarence Walker played by Rex Lease is a reporter for the Chronicle,  He works in the same building as Tom. We find out; Clarence secretly admires Polly McDonald. Currently, Clarence is also working on a hot story about a gang of bootleggers. The bootleggers feel Clarence is getting to close to their operations. They devise a plan to throw Clarence off their track. They create a news diversion by setting up another hot story for Clarence to follow. They frame prominent attorney Ray McDonald on a bribery charge.
Clarence jumps on the new lead, investigates the accusations and quickly files his bribery story with the Chronicle's front desk. Clarence's account is deemed a headliner for the newspaper. When the editor checks, they find out there is just enough time for his Clarence's bribery story to make the last edition of the Chronicle. Just as the story is about to hit the presses, Tom McDonald finds out the evening's headline is about his son Ray. After reading the article, Tom knows in his heart, Ray could not be guilty of any bribery charge. Tom becomes enraged and attempts to destroy the press.
Tom's attempted destruction fails. During this time, the entire Chronicle plant burns to the ground. Tom is blamed for the fire. He is immediately thrown in jail, coincidentally in the same cell as his son.
Clarence believes Tom when he says his son is innocent. Clarence teams up with Polly to investigate the circumstances surrounding the bribery charge. After working together, they uncover evidence exonerating both father and son. A new plant is constructed; Tom is promoted to foreman; Clarence marries Polly.


== Restoration ==
At one time, it was believed no copies of this film had survived. In 2011, a Bay Area film preservationist – Rob Byrne discovered a surviving copy of the film, in the archives of the EYE Film Institute. EYE is located in Amsterdam, Netherlands. A collaboration was arranged between the EYE Film Institute and the San Francisco Silent Film Festival.
The highly flammable nitrate film was restored to create two new 35mm exhibition prints. One copy was made available to EYE Film Institute and the other to the US Library of Congress. Research on the movie also led to the discovery of an original trailer for the film located in the archives of the Library of Congress.
Another problem was encountered by the restoration team. The original intertitles for the American film were in English. The intertitles for the discovered copy were in Dutch. Two volunteers were able to translate the Dutch intertitles back to English.


== Cast ==


== See also ==
List of rediscovered films


== References ==


== External links ==
The Last Edition on IMDb
The Last Edition at AllMovie
The Last Edition at SilentEra
website about the restoration