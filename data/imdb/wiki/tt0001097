Vanity is the excessive belief in one's own abilities or attractiveness to others. Prior to the 14th century it did not have such narcissistic undertones, and merely meant futility. The related term vainglory is now often seen as an archaic synonym for vanity, but originally meant boasting in vain, i.e. unjustified boasting; although glory is now seen as having a predominantly positive meaning, the Latin term from which it derives, gloria, roughly means boasting, and was often used as a negative criticism.


== In religion and philosophy ==

In many religions, vanity, in its modern sense, is considered a form of self-idolatry in which one likens oneself to the greatness of God for the sake of one's own image, and thereby becomes separated and perhaps in time divorced from the Divine grace of God. In Christian teachings, vanity is an example of pride, one of the seven deadly sins.  Also, in the Baháʼí Faith, Baha'u'llah uses the term 'vain imaginings'.Philosophically, vanity may be a broader form of egotism and pride. Friedrich Nietzsche wrote that "vanity is the fear of appearing original: it is thus a lack of pride, but not necessarily a lack of originality."  One of Mason Cooley's aphorisms is "Vanity well fed is benevolent. Vanity hungry is spiteful."


== Symbolism ==
In Western art, vanity was often symbolized by a peacock, and in Biblical terms, by the Whore of Babylon. During the Renaissance, vanity was invariably represented as a naked woman, sometimes seated or reclining on a couch. She attends to her hair with comb and mirror. The mirror is sometimes held by a demon or a putto. Symbols of vanity include jewels, gold coins, a purse, and the figure of death.Some depictions of vanity include scrolls that read Omnia Vanitas ("All is Vanity"), a quotation from the Latin translation of the Book of Ecclesiastes. Although the term vanitas (Latin, "emptiness") originally meant not obsession by one's appearance, but the ultimate fruitlessness of humankind's efforts in this world, the phrase summarizes the complete preoccupation of the subject of the picture.
"The artist invites us to pay lip-service to condemning her," writes Edwin Mullins, "while offering us full permission to drool over her. She admires herself in the glass, while we treat the picture that purports to incriminate her as another kind of glass—a window—through which we peer and secretly desire her." The theme of the recumbent woman often merged artistically with the non-allegorical one of a reclining Venus.
In his table of the Seven deadly sins, Hieronymus Bosch depicts a bourgeois woman admiring herself in a mirror held up by a devil; behind her is an open jewelry box. A painting attributed to Nicolas Tournier, which hangs in the Ashmolean Museum, is An Allegory of Justice and Vanity: a young woman holds a balance, symbolizing justice; she does not look at the mirror or the skull on the table before her. Johannes Vermeer's painting Girl with a Pearl Earring is sometimes believed to depict the sin of vanity, because the young girl has adorned herself before a glass without further positive allegorical attributes.
All is Vanity, by Charles Allan Gilbert (1873–1929), carries on this theme. An optical illusion, the painting depicts what appears to be a large grinning skull. Upon closer examination, it reveals itself to be a young woman gazing at her reflection in the mirror. In the film The Devil's Advocate, Satan (Al Pacino) claims that "vanity is his favourite sin".
Such artistic works served to warn viewers of the ephemeral nature of youthful beauty, as well as the brevity of human life and the inevitability of death.


== See also ==


== References ==