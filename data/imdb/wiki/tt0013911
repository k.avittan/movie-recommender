Canyon of the Fools is a 1923 American silent Western film directed by Val Paul and starring Harry Carey that was released by Film Booking Offices of America (F.B.O.).


== Plot ==
As described in a film magazine, Robert McCarthy (Carey) beats his way on a passenger train to a mining camp located near an unexplored waste called the Canyon of the Fools. En route he discovers that his former sweetheart May (Clayton) is on the train, having promised to marry Jim Harper (Stanton) whom she is to meet at the camp. Bob is seen by Sheriff Maricopia Jim (Curtis) as he arrives at the camp. The Sheriff hires him as deputy on his promise to help run down the head of an outlaw gang that is operating in the canyon. Bob's one desire is to revenge himself on a man named Torrance who double-crossed him and had him unjustly convicted of a crime. Bob makes his presence known to May, who still loves him even though engaged to Jim. Together they go into the canyon. Bob discovers a secret mine being operated by a mysterious person named Polhill. The outlaws and their leader have long been seeking Polhill's treasure. It turns out Jim Harper is the leader of the bandits. He follows Bob into the mine entrance and stuns him with a blow from behind. Jim captures May and makes her a prisoner of Polhill's room above the mine, and then the gang seizes the gold. After several adventures Bob rescues May and is rewarded with her love and Polhill's gold.


== Cast ==
Harry Carey as Robert 'Bob' McCarthy
Carmen Arselle as Incarnación
Marguerite Clayton as May
Jack Curtis as Maricopia Jim
Mignonne Golden as Aurelia
Joe Harris as Terazaz (credited as Joseph Harris)
Charles Le Moyne as Swasey (credited as Charles J. Le Moyne)
Murdock MacQuarrie as Sproul
Vester Pegg as Knute
Fred R. Stanton as Jim Harper / Polhill (credited as Fred Stanton)


== Preservation ==
Once thought to be lost, Canyon of the Fools was one of ten silent films digitally preserved in Russia's Gosfilmofond archive and provided to the Library of Congress in October 2010.


== References ==


== External links ==
Canyon of the Fools on IMDb
Canyon of the Fools at AllMovie