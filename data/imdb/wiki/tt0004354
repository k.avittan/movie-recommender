Constance Alice Talmadge (April 19, 1898 – November 23, 1973) was an American silent film star. She was the sister of actresses Norma and Natalie Talmadge.


== Early life ==
Talmadge was born on April 19, 1898 in Brooklyn, New York to poor parents, Fred and Peg Talmadge. Her father was an alcoholic, and left them when she was still very young. Her mother made a living by doing laundry. When a friend recommended that Talmadge's mother use older sister Norma as a model for title slides in flickers, which were shown in early nickelodeons, Peg decided to do so. This led all three sisters into an acting career.


== Career ==

She began making films in 1914, in a Vitagraph comedy short, In Bridal Attire (1914). Her first major role was as the Mountain Girl and Marguerite de Valois in D.W. Griffith's Intolerance (1916).
Griffith re-edited Intolerance repeatedly after its initial release, and even shot new scenes long after it was in distribution. Grace Kingsley found Talmadge in her dressing room at the Fine Arts Studio, in Los Angeles, in the midst of making up for some new shots.
"Did you really drive those galloping brutes of horses?" asked Kingsley.
"Indeed I did," said Talmadge. "Two women sat behind me at the Auditorium the other night. They said, 'Of course she never really drove those horses herself. Somebody doubled for her.' Know what I did? I turned around and told them, 'I wish I could show you my knees, all black and blue even yet from being cracked up against the dashboard of that chariot!'"So popular was Talmadge's portrayal of the tomboyish Mountain Girl, Griffith released in 1919 the Babylonian sequence from Intolerance as a new, separate film called The Fall of Babylon. He refilmed her death scene to allow for a happy ending.
Her friend Anita Loos, who wrote many screenplays for her, appreciated her "humour and her irresponsible way of life". Over the course of her career, Talmadge appeared in more than 80 films, often in comedies such as A Pair of Silk Stockings (film) (1918), Happiness à la Mode (1919), Romance and Arabella (1919), Wedding Bells (1921 film) (1921), and The Primitive Lover (1922).

Talmadge, along with her sisters, was heavily billed during her early career. According to her 1923 Blue Book of the Screen biography, she was "5'5" tall, 120 lbs, with blonde hair and brown eyes, ... an outdoor girl who loved activities."When Talmadge was asked by a writer for Green Book magazine what sort of stories she wanted to do in 1920, she said: "Although no less than sixty manuscripts are submitted to me every week, it is exceedingly difficult to get exactly the kind of comedy I especially want. I want comedies of manners, comedies that are funny because they delight one’s sense of what is ridiculously human in the way of little everyday commonplace foibles and frailties – subtle comedies, not comedies of the slap stick variety."

"I enjoy making people laugh. Secondly, because this type of work comes easiest and most naturally to me, I am not a highly emotional type. My sister could cry real tears over two sofa cushions stuffed into a long dress and white lace cap, to look like a dead baby, and she would do it so convincingly that 900 persons out front would weep with her. That is real art, but my kind of talent would lead me to bounce that padded baby up and down on my knee with absurd grimaces that would make the same 900 roar with laughter.
"You see, in my way, I take my work quite as seriously as my sister does hers – I would be just as in earnest about making the baby seem ridiculous as she would about making it seem real. I am not fitted to be a vamp type. There is nothing alluring, or exotic, or erotic, or neurotic about me. I could not pull the vamp stuff to save my life, but if I am assigned a vamp role in a comedy, and I had such a part in my fourth First National picture, In Search of a Sinner. I play it with all the seriousness and earnestness and sincerity with which a real vamp would play it, except that I, of course, over-emphasize all the characteristics of the vampire. I try to handle a comedy role much the same way that a cartoonist handles his pencils. If he is drawing the picture of the late Theodore Roosevelt, with a few strokes he emphasizes Teddy’s eye-glasses and teeth, leaving his ears and nostrils and the lines of his face barely suggestive. One must leave a great deal to the imagination on the screen, because in the span of one short hour we sometimes have to develop a character from girlhood to womanhood through three marriages and two divorces, and perhaps travel half way round the world besides; so, like the cartoonist, I try to emphasize the salient characteristics, which, of course, in my particular work, bring out the humorous side of the person I am portraying."

With the advent of talkies in 1929, Talmadge left Hollywood. Her sister Norma did make a handful of appearances in talking films, but for the most part the three sisters retired all together, investing in real estate and other business ventures. Only a few of her films survive today.


== Personal life ==

She was married four times; all the unions were childless:

Her first marriage, to John Pialoglou (1893-1959), a Greek tobacco importer, occurred in 1920 at a double wedding with Dorothy Gish and James Rennie; she divorced Pialoglou two years later. Her marriage to him, a Greek subject, caused her to lose her natural-born U.S. citizenship; following her divorce, she had to apply for U.S. naturalization.
She married Scottish soldier Alastair William Mackintosh (grandfather of author Edward St Aubyn) in February 1926, divorcing him in 1927 on grounds of adultery.
She married Townsend Netcher in May 1929, divorcing in 1931.
She married Walter Michael Giblin in 1939. This marriage lasted until his death on May 1, 1964.Talmadge's mother fostered the belief she might one day return to films. “Success and fame cast a spell that can never been quite shaken off,” her mother pointed out in her autobiography. “A woman, because of her love, may say, and in the fervor of the moment believe, that she is ready to give up her chosen work. But there is sure to come a time when keen longing and strong regret for her lost career dominate over the more placid contentments of love and marriage. Then unhappiness and friction ensue.”
She died of pneumonia. Along with her sister Norma, Mary Pickford, and Douglas Fairbanks, Talmadge inaugurated the tradition of placing her footprints in concrete outside Grauman's Chinese Theater. She left a trail of five footprints in her slab.
Her star on the Hollywood Walk of Fame is at 6300 Hollywood Blvd.


== Filmography ==


== Notes ==


== References ==
The Griffith Actresses. By Anthony Slide. New York: A.S. Barnes and Company, 1973.
The Talmadge Sisters. By Margaret L. Talmadge. New York: J. B. Lippincott Company, 1924.
The Quality You Need Most. By Constance Talmadge in Green Book Magazine, April, 1914.
1900 United States Federal Census, Brooklyn Ward 8, Kings, New York; Roll T623_1047; Page: 4B; Enumeration District: 109.
1910 United States Federal Census, Brooklyn Ward 29, Kings, New York; Roll  T624_982; Page: 13B; Enumeration District: 933; Image: 948.
1920 United States Federal Census, Manhattan Assembly District 15, New York, NY; Roll  T625_1212; Page: 7A; Enumeration District: 1061; Image: 877.
1905 New York State Census for Kings County, Brooklyn, New York.
U.S. Passport Applications, 1795–1925, Ancestry.com.


== External links ==

Bio
Constance Talmadge on IMDb
Talmadge Sisters at the Women Film Pioneers Project
Constance Talmadge at Silent Ladies
Photographs of Constance Talmadge and bibliography
Constance Talmadge at Find a Grave