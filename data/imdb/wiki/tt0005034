Bridge Builder is a puzzle video game series, created by indie developer Alex Austin. Bridge Builder is the first in the series, followed by four sequels in different versions. They were developed and published by Chronic Logic, the only exception being Bridge Project, which was developed by Invent4 Entertainment. Some of the games also feature secondary publishers.
The objective of each game is to construct a bridge which a train may cross over, given a limited budget and materials.


== Gameplay ==

Each level is composed of a section of land, such as a river or chasm, and an initially laid track that shows the train's starting and ending points. A number of anchor points in and around the chasm  are where the player anchors the bridge from. Should the player's train cross the bridge successfully and under budget, the level is passed. Later games add additional constraints such as a boat that must pass underneath between two consecutive train crossings.
Although Bridge Builder only allows the player steel beams, bridges in later installments can be created using a variety of materials. Along with varying strengths of steel, the player can also use cable or hydraulics that expand or contract for some purpose, such as to allow a ship to pass underneath the bridge. In all games, the player is constrained by a budget that determines the maximum amount of material that can be used and still pass the level.
There is no penalty for failing a level; the player may try and test infinitely many different designs before finding one that succeeds. A poorly designed bridge may break as soon as the player begins to test it, unable to support its own weight. While testing the bridge, the stress on each section can be seen, representing both tensile stress and compressive stress. If the stress becomes too great, that section breaks and disintegrates, possibly jeopardizing the entire structure.


== Games ==
Bridge Builder (2000)
Pontifex (2001)
Pontifex II (later renamed Bridge Construction Set) (2002)
Bridge Building Game (2006)
Bridge It (2011)
Bridge It Add-on Pack (2012)
Bridge It Plus (Bridge It Reloaded in Germany) (2012)
Bridge Project (Bridge Builder 2 in Germany) (2013)


== Reception and legacy ==
Pontifex II won the Independent Games Festival Audience Award 2003.
In praise of the original Bridge Builder's flexibility, 1UP.com stated that "the sheer variety of solutions provide more entertainment than expected and Bridge Builder quickly becomes less of a game and more of a toy."
Peter Stock, creator of Armadillo Run, cited Bridge Builder as an inspiration for his game.


== References ==