On February 3, 1975, 197 people fell ill aboard a Japan Airlines Boeing 747 en route from Anchorage, Alaska to Copenhagen, Denmark after consuming an in-flight meal contaminated with Staphylococci. 144 people 
needed hospitalization, making it the largest food poisoning incident aboard a commercial airliner.


== Aircraft and passengers ==
The incident occurred aboard a Boeing 747 operated by Japan Airlines. The registration number of the aircraft is not known. At the time of the incident, Japan Airlines had both the 747-100 and the 747-200B in their long-distance fleet.The aircraft was carrying 344 passengers. The exact number of crew members is not known, but the fact that 364 meals were taken on board indicates a crew of 20. Most of the passengers on the charter flight were Japanese salesmen of The Coca-Cola Company and their family members, who had won a trip to Paris.


== Sequence of events ==
The flight originated from Tokyo Haneda Airport and made a fuel stop at Ted Stevens Anchorage International Airport. After crossing the Arctic, another fuel stop at Copenhagen Airport was scheduled before the flight was to continue to its final destination at Paris Charles de Gaulle Airport.
The aircraft reached European airspace after an uneventful flight. 90 minutes prior to the scheduled landing at Copenhagen, the flight attendants served ham omelettes for breakfast.
About one hour after breakfast, while approaching Copenhagen, 196 passengers and one flight attendant fell ill with nausea, vomiting, diarrhea and abdominal cramps. 144 of them were so severely ill that they required hospitalization, 30 were in critical condition. The other 53 were treated in makeshift emergency rooms.
As none of the doctors in Denmark spoke Japanese, and only few of the passengers were fluent in Danish or English, Japanese-speaking staff from Copenhagen restaurants were summoned to the hospital to act as translators.


== Investigation ==
The investigation team was led by United States Public Health Service officer Dr. Mickey S. Eisenberg of the Alaska State Health Department.
Laboratory tests of stool and vomit samples from passengers, as well as 33 samples of leftover ham omelettes, detected Staphylococcus aureus. Elevated concentrations of toxines produced by the staphylococci were also detected in the ham, explaining the extremely short incubation time.


=== Contamination of meals ===
The investigation began by tracing the pathogens back to its origin, and focused on the facilities of International Inflight Catering, an Anchorage-based subsidiary of Japan Airlines, where the meals had been prepared. It was found that three cooks had prepared the meals, one of whom had infected lesions on the index and middle finger of his right hand. The lesions on the cook’s fingers were found to be infected with staphylococci. Tests revealed identical phage types and antibiotic resistances for all samples, indicating that the cook was the source of the contamination.The aircraft had four galleys from which 354 meals were served, 40 in first class and 108 from each galley on the main deck. According to Eisenberg, the suspect cook had prepared meals for three of the four galleys. He had bandaged the lesions but not reported them to his superior, as he considered them trivial. Also, management had not verified that he was in good health, despite being required to do so, according to Eisenberg.The suspect cook had prepared all 40 omelettes served in first class, as well as 72 out of 108 for one of the main deck galleys. Furthermore, he handled all 108 omelettes for another galley (sources differ on whether he put the ham on these omelettes, or both cooks took the slices of ham for the omelettes they prepared from the same container). He had therefore handled a total of 220 meals. According to Eisenberg’s hypothesis, 36 people who received a meal from one of the front galleys, as well as the 108 who were served their meal from the rear one, had eaten meals which were not contaminated.


=== Spread of pathogens ===
According to microbiologists, it can take as few as 100 staphylococci to cause food poisoning. In-flight catering logistics provided ideal conditions for the bacteria to grow and release toxins, which induce severe nausea, vomiting, diarrhea and abdominal cramps. Being heat-resistant, the toxins were not destroyed when the omelettes were heated.Prior to being served, the meals had been stored at room temperature in the kitchen for 6 hours, then refrigerated (albeit at an insufficient 10°C) for 14½ hours and then stored in the aircraft ovens, again without refrigeration, for another 8 hours. Had the food been kept properly refrigerated from the time it was prepared until it was ready to be served, the outbreak would not have occurred.Danish doctors stated that most of those who fell ill had occupied seats in the front section of the aircraft, consistent with Eisenberg’s hypothesized distribution pattern of the contaminated omelettes. 86% of those who ate omelettes handled by the suspect cook fell ill, whereas none of those who ate one of the other omelettes developed symptoms.


== Aftermath ==
Japan Airlines’ catering manager, 52-year-old Kenji Kuwabara, committed suicide upon learning that the incident had been caused by one of his cooks. He was the only fatality.Investigators emphasized that people with infected lesions should not handle food and that food should be stored at temperatures low enough to inhibit the growth of bacteria.It was just chance that the pilot and first officer had not eaten any of the contaminated omelettes, as the airline had no regulations regarding crew meals. As the pilots’ biological clocks were on Alaska time rather than European time, they had opted for a dinner of steaks instead of omelettes—had they not done so, they might not have been capable of landing the aircraft safely. Eisenberg suggested that cockpit crew members eat different meals prepared by different cooks to prevent food poisoning outbreaks from incapacitating the entire crew, a rule subsequently implemented by the Federal Aviation Administration.


== References ==