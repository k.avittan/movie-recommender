Dogs Playing Poker, by Cassius Marcellus Coolidge, refers collectively to an 1894 painting, a 1903 series of sixteen oil paintings commissioned by Brown & Bigelow to advertise cigars, and a 1910 painting. All eighteen paintings in the overall series feature anthropomorphized dogs, but the eleven in which dogs are seated around a card table have become well known in the United States as examples of kitsch art in home decoration. Depictions and reenactments of the series have appeared in many films, television shows, theater productions, and other popular culture art forms.
Critic Annette Ferrara has  described Dogs Playing Poker as "indelibly burned into ... the American collective-schlock subconscious ... through incessant reproduction on all manner of pop ephemera".The first painting, Coolidge's 1894 Poker Game, sold  for $658,000 at a 2015 auction.


== Coolidge paintings ==

The title of Coolidge's original 1894 painting is Poker Game.
The titles in the Brown & Bigelow series are:

A Bachelor's Dog – reading the mail
A Bold Bluff  – poker (originally titled Judge St. Bernard Stands Pat on Nothing)
Breach of Promise Suit – testifying in court
A Friend in Need (1903) – poker, cheating
His Station and Four Aces (1903) – poker
New Year's Eve in Dogville – ballroom dancing
One to Tie Two to Win – baseball
Pinched with Four Aces – poker, illegal gambling
Poker Sympathy – poker
Post Mortem – poker, camaraderie
The Reunion – smoking and drinking, camaraderie
Riding the Goat – Masonic initiation
Sitting up with a Sick Friend (1905) – poker, gender relations
Stranger in Camp – poker, camping
Ten Miles to a Garage – travel, car trouble, teamwork
A Waterloo (1906) – poker (originally titled Judge St. Bernard Wins on a Bluff)These were followed in 1910 by a similar painting, Looks Like Four of a Kind. Other Coolidge paintings featuring anthropomorphized dogs include Kelly Pool, which shows dogs playing kelly pool.
Some of the compositions in the series are modeled on paintings of human card-players by such artists as Caravaggio, Georges de La Tour, and Paul Cézanne.On February 15, 2005, the originals of A Bold Bluff and Waterloo were auctioned as a pair to an undisclosed buyer for US $590,400. The previous top price for a Coolidge was $74,000. In 2015, Poker Game sold for $658,000, currently the highest price paid for a Coolidge.


== In popular culture ==
In the TV sitcom Cheers, Sam Malone loves the paintings (in particular one of Dogs Playing Blackjack) while his more sophisticated lover, Diane Chambers, hates them. Sam says that he sees something new every time he looks at it.
The set for the TV show Roseanne had a reproduction of one of the paintings in Roseanne and Dan's bedroom.
The cover of the 1981 album, Moving Pictures, by Rush, features A Friend in Need as one of the three pictures being moved.
In the 1984 play The Foreigner, a character complains that she doesn't want to be in her motel room because there is a "Damn picture on the wall of some dogs playin' poker."
In the 1985 film Police Academy 2: Their First Assignment, slobbish police officer Vinnie Schtulman (Peter Van Norden) has a framed print of "Waterloo" and, below it, an unframed print of "Pinched With Four Aces" (which features four dogs in old-fashioned police uniforms raiding a poker game).
The animated television series The Simpsons has made several references to the paintings, such as in "Treehouse of Horror IV" (1993) when Homer is driven to screaming insanity simply by looking at the surrealness of the painting.
The music video for Snoop Dogg's 1993 song, "What's My Name", depicts dogs playing craps while smoking cigars and wearing sunglasses.
Dogs Playing Poker TV ads were aired during ESPN Sunday Night Football during the 1998 and 1999 NFL seasons.
The 1998 season four episode "Sinking Ship" of the TV series NewsRadio spoofs the 1997 film Titanic. As the characters are shown fleeing the sinking ship/broadcasting studio they dump famous artworks but hold on to a Dogs Playing Poker, which a character claims is a "great picture".
In the 1999 film The Thomas Crown Affair, Banning believes she finds a stolen Claude Monet painting in Crown's house. On expert examination it turns out to be a fake painted over a copy of Poker Sympathy, a Dogs Playing Poker canvas.
In a 2000 episode of the TV series That '70s Show, "Hunting", Dogs Playing Poker is parodied by the characters taking the places of the dogs.
In an episode of Animaniacs, a young Pablo Picasso's artistic frustration is demonstrated by his producing a DPP painting.
In an episode of White Collar the main protagonist, who is considered an expert on art, jokes about hanging a DPP on a wall.
In an episode of Courage the Cowardly Dog, Courage goes into a DPP painting and picks up an untouched card hand.  He laughs and puts it down, which shocks the other dogs upon seeing that the hand is a royal flush.  Courage is then kicked out of the painting by one of the dogs.
In an episode of My Gym Partner's a Monkey, Adam is told to look inside his brain, and what he see is reminiscent of a Dogs Playing Poker painting.
In the Lilo & Stitch: The Series 2003 episode "Finder", Stitch (who was adopted by Lilo as a "dog") plays a game of poker with his experiment "cousins" with cookies in place of poker chips.
In a The Far Side cartoon a homeless artist lies in the street, surrounded by unsold paintings similar to DPP but depicting other animals such as giraffes, bugs, chickens, and gators. The caption recalls that someone said, "Hey, have you ever tried dogs playing poker"?
In the 2003 film, Looney Tunes: Back in Action, a number of dog characters in the series are seen playing poker at Yosemite Sam's casino.
In the 2004 remake of Around the World in 80 Days, Monique has a painting of Dogs Playing Poker in her sketchbook.
In the 2005 Suite Life of Zack and Cody episode "Hotel Inspector", London tells Maddie that she saw a painting of dogs playing poker, and that she wants Maddie to throw her dog a poker-themed party. When Maddie tells her the dogs weren't really playing poker, London replies, "If they weren't playing poker, then how did the dalmatian win all the money?"
In Wizards of the Coast's Magic: The Gathering 2004 Arena league, a card "Mise" portrays dogs playing Magic.
In the 2005 video game Psychonauts, when a player revisits the stage Black Velvetopia after completing it, Edgar Teglee´s mental projection and the dog artists that inhabit his mind will appear in the hub playing poker. Talking to Edgar in the Asylum will also trigger a new dialogue, "Arent we all just dogs playing poker?"  
In the 2006 Family Guy episode "Saving Private Brian", Mayor West is discovered playing poker with dogs. In the episode "Road to Rhode Island", Stewie comments on the Dogs Playing Poker paintings hanging on a wall, and suggests that since Jesus is alone in one of the other paintings, the dogs should invite him to their card game.
In the TV series Boy Meets World, Eric is cleaning out the garage when he finds one of the Dogs Playing Poker paintings, and shows his parents.
In the 2009 animated film Up, several dog characters are briefly seen playing poker, using a pile of Milk-Bones as poker chips.
In the 2010 video game Fallout New Vegas, in a cyberdog training area, the dogs are depicted learning how to play poker.
In the 2011 video game Terraria  a painting, Goblins playing poker, depicts goblins playing poker.
In "Lawnmower Dog", a 2013 episode of the animated series Rick and Morty, five intelligent dogs play poker and smoke cigars while using their advanced robotic suits.
In the 2013 video game Poker Night 2, Brock Samson may, at any point during a given game, come to the amused realization that Sam is a dog playing poker. Sam doesn't recognize the reference.
In the 2015 computer game Undertale, a dog plays poker, and later  Go Fish, against itself in a restaurant.
In the 2016 film, The Accountant, the paintings are discussed by the lead characters. Later, a copy of A Friend in Need is used as a cover to hide a Jackson Pollock painting.
In the 2018 television series Disenchantment episode "Love's Tender Rampage", the characters walk past a shop in which dogs are playing poker.
In the 2019  Mickey Mouse episode titled "You, Me, and Fifi," Goofy, Pluto, and several other Disney dog-themed characters play a card game similar to poker, but is revealed to be Go Fish when Goofy cries out "GO FISH!"
In the 2019 animated film Toy Story 4, a painting of Charles Muntz and the dogs from the 2009 film Up playing poker can be seen at the antiques store.
In the 2019 Carmen Sandiego season one episode 5 "The Duke of Vermeer Caper", Zack mocks Princess Cleo's assistant as "His idea of art is probably a painting of dogs playing poker!".
Snoop Dogg references the painting in the album cover to his 2019 album, I Wanna Thank Me
In the 2020 Ray Donovan season seven episode "Passport and a Gun", Jim Sullivan rewards young Ray for his successful debut as a debt collector with a valued and framed Dogs Playing Poker painting, A Friend in Need.
In the SciFi book and audio book from Audible "Home Front", in the Expeditionary Force series, the very advanced Elder artificial intelligence, owns one of the original paintings, although they discuss that there are 11 paintings.


== See also ==
William Wegman
Laying Down the Law, 1840 painting


== References ==


== Further reading ==
Harris, Maria Ochoa. "It's A Dog's World, According to Coolidge", A Friendly Game of Poker (Chicago Review Press, 2003).


== External links ==
 Media related to Dogs Playing Poker at Wikimedia Commons