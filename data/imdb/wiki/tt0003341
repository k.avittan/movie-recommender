Mildred Harris (November 29, 1901 – July 20, 1944) was an American film actress during the early part of the 20th century. Harris began her career in the film industry as a child actress when she was 11 years old. She was also the first wife of Charlie Chaplin.


== Early life ==
Mildred Harris was born in Cheyenne, Wyoming, to Harry Harris, a telegraph operator, and Anna Parsons Foote. Harris made her first screen appearance at the age of 11 in the 1912 Francis Ford and Thomas H. Ince-directed Western short The Post Telegrapher. She followed the film with various juvenile roles, often appearing opposite child actor Paul Willis. In 1914, she was hired by The Oz Film Manufacturing Company to portray Fluff in The Magic Cloak of Oz and Button-Bright in His Majesty, the Scarecrow of Oz. In 1916, at the age of 15, she appeared as a harem girl in Griffith's film Intolerance.


== Career ==

In the 1920s, Harris transitioned from child actress to leading lady roles opposite leading men such as Conrad Nagel, Charley Chase, Milton Sills, Lionel Barrymore, Rod La Rocque and the Moore brothers, Owen and Tom.  She appeared in Frank Capra's 1928 silent drama The Power of the Press with Douglas Fairbanks, Jr. and Jobyna Ralston and the same year, starred in Universal Pictures first sound film Melody of Love opposite Walter Pidgeon.She found the transition to the "talkies" difficult and her career slowed dramatically.  She performed in vaudeville and burlesque and, at one point, toured with comedian Phil Silvers. She was critically praised for her performance in the 1930 film adaptation of the Broadway musical No, No Nanette.  In the 1936 Three Stooges comedy Movie Maniacs, she portrayed a temperamental and demanding film starlet who, while receiving a pedicure, is startled by stooge Curly Howard striking a match on the sole of her foot.
Harris continued to work in film in the early 1940s, largely through the kindness of her former director, Cecil B. DeMille, who cast her in bit parts in 1942's Reap the Wild Wind (starring Paulette Goddard, who, like Harris, was once married to Charlie Chaplin), and 1944's The Story of Dr. Wassell. Her last film appearance was in the posthumously-released 1945 film Having A Wonderful Crime.


== Personal life ==

The 16-year-old Harris met actor Charlie Chaplin in mid-1918, dated, and came to believe she was pregnant by him, but the pregnancy was found to be a false alarm. They married privately on October 23, 1918, in Los Angeles. She subsequently did become pregnant. The couple quarreled about her contract with Louis B. Mayer and her career. Chaplin felt she was not his intellectual equal. Their child Norman Spencer died in July 1919, at only three days of age, and the couple separated in the autumn of 1919.
Chaplin moved to the Los Angeles Athletic Club. Harris tried to keep up appearances, believing a happy marriage was possible but, in 1920, she filed for divorce based on mental cruelty. Chaplin accused her of infidelity, and though he would not name her lover publicly, actress Alla Nazimova was suspected. The divorce was granted in November 1920, with Harris receiving $100,000 (US$1,276,246 in 2019 dollars) in settlement and some community property.In 1924, Harris married Everett Terrence McGovern. The union lasted until November 26, 1929, when Harris filed for divorce in Los Angeles, on grounds of desertion. The couple had one son, Everett Terrence McGovern, Jr., in 1925. In 1934, she married the former football player William P. Fleckenstein in Asheville, North Carolina.


== Death ==
The couple remained married until Harris's death on July 20, 1944, of pneumonia following a major abdominal operation. She had been ill for three weeks.  She is interred in the Abbey of the Psalms Mausoleum at the Hollywood Forever Cemetery in Los Angeles.


== Legacy ==
Harris has a star on the Hollywood Walk of Fame at 6307 Hollywood Boulevard in Los Angeles. In 1992, she was portrayed by Milla Jovovich in the biographical film Chaplin.


== Filmography ==


== References ==


== External links ==

Mildred Harris on IMDb
Mildred Harris at AllMovie
Mildred Harris at Find a Grave
Silent Era People
Mildred Harris at Virtual history