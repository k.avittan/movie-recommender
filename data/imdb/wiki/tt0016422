In law, a verdict is the formal finding of fact made by a jury on matters or questions submitted to the jury by a judge. In a bench trial, the judge's decision near the end of the trial is simply referred to as a finding. In England and Wales, a coroner's findings used to be called verdicts but are, since 2009, called conclusions (see Coroner § Conclusions (previously called verdicts)).


== Etymology ==
The term "verdict", from the Latin veredictum, literally means "to say the truth" and is derived from Middle English verdit, from Anglo-Norman: a compound of ver ("true", from the Latin vērus) and dit ("speech", from the Latin dictum, the neuter past participle of dīcere, to say).


== Criminal law ==

In a criminal case, the verdict, which may be either "not guilty" or "guilty"—except in Scotland where the verdict of "not proven" is also available—is handed down by the jury. Different counts in the same case may have different verdicts.
A verdict of guilty in a criminal case is generally followed by a judgment of conviction rendered by judge, which in turn be followed by sentencing.
In U.S. legal nomenclature, the verdict is the finding of the jury on the questions of fact submitted to it. Once the court (the judge) receives the verdict, the judge enters judgment on the verdict. The judgment of the court is the final order in the case. If the defendant is found guilty, he can choose to appeal the case to the local Court of Appeals.


== Compromise verdict ==
A compromise verdict is a "verdict which is reached only by the surrender of conscientious convictions upon one material issue by some jurors in return for a relinquishment by others of their like settled opinion upon another issue and the result is one which does not command the approval of the whole panel", and, as such, is not permitted.


== Directed verdict ==
In a jury trial, a directed verdict is an order from the presiding judge to the jury to return a particular verdict.  Typically, the judge orders a directed verdict after finding that no reasonable jury could reach a decision to the contrary. After a directed verdict, there is no longer any need for the jury to decide the case.  
A judge may order a directed verdict as to an entire case or only to certain issues.
In a criminal case in the United States, once the prosecution has closed its case, the defendant may move for a directed verdict.  If granted, the verdict will be "not guilty".  The prosecution may never seek a directed verdict of guilty, as the defendant has a constitutional right to present a defense and rebut the prosecution's case and have a jury determine guilt or innocence (where a defendant has waived his/her right to a jury trial and allowed the judge to render the verdict, this still applies).
In the American legal system, the concept of directed verdict has largely been replaced by judgment as a matter of law.


== General verdict ==
A general verdict is one in which the jury makes a complete finding and single conclusion on all issues presented to it. First, the jury finds the facts, as proved by the evidence, then it applies the law as instructed by the court, and finally it returns a verdict in one conclusion that settles the case. Such verdict is reported as follows:
"We the Jury find the issues for the plaintiff (or defendant, as the case may be) and assess his damages at one hundred thousand dollars."


== Sealed verdict ==
A sealed verdict is a verdict put into a sealed envelope when there is a delay in announcing the result, such as waiting for the judge, the parties and the attorneys to come back to court. The verdict is kept in the sealed envelope until court reconvenes and then handed to the judge. This practice is virtually the default in many U.S. jurisdictions or may be the preference of the judge involved.


== Special verdict ==
In English law, a special verdict is a verdict by a jury that makes specific factual conclusions rather than (or in addition to) the jury's declaration of guilt or liability.   For example, jurors may write down a specific monetary amount of damages, or a finding of proportionality, in addition to the jury's ultimate finding of liability. A special jury verdict form may be used to have the jury answer directed questions as to the required elements for a cause of action or special issues, and to demarcate monetary awards of damages by economic and non-economic damages, beneficiary and/or specific categories of damages (lost earning capacity, funeral expenses, loss of consortium, pain and suffering, etc.). In the words of William Blackstone, "The jury state the naked facts, as they find them to be proved, and pray the advice of the court thereon". Special verdicts are intended to focus the jury's attention on the important questions at hand.The judge forced a special verdict in the famous 1884 case of R v. Dudley and Stephens, which established a precedent that necessity is not a defence to a charge of murder, but generally it is recommended that such verdicts should only be returned in the most exceptional cases.The jury has a historic function of tempering rules of law by common sense brought to bear upon the facts of a specific case. For this reason Justices Black and Douglas indicated their disapproval of special verdicts even in civil cases.


== See also ==
Virtual jury research


== References ==


== External links ==
Chisholm, Hugh, ed. (1911). "Verdict" . Encyclopædia Britannica (11th ed.). Cambridge University Press.