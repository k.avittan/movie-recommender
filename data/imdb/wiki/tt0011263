Haunted Spooks is a 1920 American silent Southern Gothic comedy film produced and co-directed by Hal Roach, starring Harold Lloyd and Mildred Davis.


== Plot ==
The action in Haunted Spooks centres around Harold's romantic problems. It is set in the South ("[go] down the Mississippi and turn to the right").
The opening sequence has an uncle reading a telegram regarding a will. It tells him that his niece Mildred will inherit the house and plantation provided she lives there for a year with her husband. He tells his wife that they must scare them out of the house. A lawyer visits the niece to tell her of the will. She tells him she isn't married and he says he can resolve the problem.
We then jump to Harold who is disappointed in love and vying for the attention of the Other Girl in rivalry with her other potential suitor. They compete to be first to ask her father for her hand in marriage. Harold wins but when he returns to the girl she is in the arms of yet a third man, so he gives up. He then tries, with notable lack of success, to commit suicide. Firstly using a gun he finds on a path, which turns out to be a water-pistol; then standing in front of a tram, which takes a sudden turn; then he ties a rock around his neck and jumps off a low bridge into a lake, but this fails as it is only inches deep; he then picks a second bridge, but lands in a boat; and finally stands in front of a car, which stops in time, but contains the lawyer from the earlier scene. He takes Harold to Mildred and arranges their marriage.
They then drive off to the mansion, with some jokes en route: the gesticulating passengers in the car in front appear to be signalling right then left, preventing overtaking; the birds in the back seat pecking his head.
They reach the mansion and the uncle plays a series of tricks to make the house appear haunted. A series of people appear in white sheets and covered in flour until the prank is uncovered. In a more unusual prank a pair of trousers walk on their own, having a little black boy inside. We see Harold's hair stand on end then fall.
The film ends with the couple inquiring about each other's name and entering the bedroom together.


== Cast ==
Harold Lloyd as The Boy
Mildred Davis as Mildred Hillary, The Girl
Wallace Howe as The Uncle
Sammy Brooks as Short Butler (uncredited)
William Gillespie as The Lawyer (uncredited)
Mark Jones as Kitchen Staff Member (uncredited)
Gaylord Lloyd (uncredited)
Sam Lufkin as Man arguing in car (uncredited)
Ernest Morrison as Little Boy (uncredited)
Dee Lampton as Tall Butler (uncredited)
Charles Stevenson as Winning Suitor (uncredited)
Blue Washington as Negro Butler (uncredited)
Noah Young as Chef (uncredited)


== Production ==
The film began shooting on August 9, 1919 and halted on the 23rd of that month due to an accident. Lloyd was posing for publicity photos, and a prop bomb exploded in his hand. He lost two fingers, his face was badly burned and he was temporarily blinded. In subsequent films, he wore a prosthetic glove fitted with artificial fingers. After four months of recovering the film resumed on January 5, 1920 through the 25th. There was very little public knowledge about the loss of Lloyd's fingers, but people knew of the accident. Lloyd wanted it that way because he did not want people to watch his films for sympathy or curiosity, but to watch his films "because they were good, laughable, and delightful comedies".Lloyd's salary doubled after his return to this film. After this movie, he moved away from slapstick and went towards more intellectual 
and romantic comedies. Three years after this film was made, Harold Lloyd married his co-star Mildred Davis, whose brother Jack was a member of Hal Roach's Our Gang comedies. They remained married until her death in 1969.Haunted Spooks was filmed on location at Hancock Park and what is now MacArthur Park. The scenes at the haunted mansion were filmed at Hal Roach Studios, in Culver City, California. The film had a lot of racist humor in it, revolving mainly around the frightened black servants in the house showing then-stereotypical behavior such as shaking knees and wide eyes. One of the servants was even played by a white actor in blackface. "One can even conclude that the film's title is a racist slur" according to critic Christopher Workman.


== See also ==
Harold Lloyd filmography


== References ==


== External links ==
Haunted Spooks on IMDb