Montmartre (UK:  mon-MAR-trə, US:  mohn-, French: [mɔ̃maʁtʁ] (listen)) is a large hill in Paris's 18th arrondissement. It is 130 m (430 ft) high and gives its name to the surrounding district, part of the Right Bank in the northern section of the city. The historic district established by the City of Paris in 1995 is bordered by rue Caulaincourt and rue Custine on the north, rue de Clignancourt on the east, and boulevard de Clichy and boulevard de Rochechouart to the south, containing 60 ha (150 acres). Montmartre is primarily known for its artistic history, the white-domed Basilica of the Sacré-Cœur on its summit, and as a nightclub district. The other church on the hill, Saint Pierre de Montmartre, built in 1147, was the church of the prestigious Montmartre Abbey. On August 15, 1534, Saint Ignatius of Loyola, Saint Francis Xavier and five other companions bound themselves by vows in the Martyrium of Saint Denis, 11 rue Yvonne Le Tac, the first step in the creation of the Jesuits.Near the end of the 19th century and at the beginning of the twentieth, during the Belle Époque, many artists lived in, had studios, or worked in or around Montmartre, including Amedeo Modigliani, Claude Monet, Pierre-Auguste Renoir, Edgar Degas, Henri de Toulouse-Lautrec, Suzanne Valadon, Piet Mondrian, Pablo Picasso, Camille Pissarro, and Vincent van Gogh. Montmartre is also the setting for several hit films.
This site is served by metro, with line 2 stations at Anvers, Pigalle, and Blanche, and line 12 stations at Pigalle, Abbesses, Lamarck – Caulaincourt, and Jules Joffrin.


== Etymology ==
The toponym Mons Martis, Latin for "Mount of Mars", survived into Merovingian times, gallicised as Montmartre.


== History ==
Archaeological excavations show that the heights of Montmartre were occupied from at least Gallo-Roman times. Texts from the 8th century cite the name of mons Mercori (Mount Mercury), and a 9th-century text speaks of Mount Mars. Excavations in 1975 north of the Church of Saint-Pierre found coins from the 3rd century and the remains of a major wall. Earlier excavations in the 17th century at the Fontaine-du-But (2 rue Pierre-Dac) found vestiges of Roman baths from the 2nd century.

The butte owes its particular religious importance to the text entitled Miracles of Saint-Denis, written before 885 by Hilduin, abbot of the monastery of Saint-Denis, which recounted how Saint Denis, a Christian bishop, was decapitated on the hilltop in 250 AD on orders of the Roman prefect Fescennius Sisinius for preaching the Christian faith to the Gallo-Roman inhabitants of Lutetia. According to Hilduin, Denis collected his head and carried it as far as the fontaine Saint-Denis (on modern impasse Girardon), then descended the north slope of the hill, where he died.  Hilduin wrote that a church had been built "in the place formerly called Mont de Mars, and then, by a happy change, 'Mont des Martyrs'."In 1134, king Louis VI purchased the Merovingian chapel and built on the site the church of Saint-Pierre de Montmartre, still standing. He also founded The Royal Abbey of Montmartre, a monastery of the Benedictine order, whose buildings, gardens and fields occupied most of Montmartre. He also built a small chapel, called the Martyrium, at the site where it was believed that Saint Denis had been decapitated. It became a popular pilgrimage site. In the 17th century, a priory called abbaye d'en bas was built at that site, and in 1686 it was occupied by a community of nuns.The abbey was destroyed in 1790 during the French Revolution, and the convent demolished to make place for gypsum mines. The church of Saint-Pierre was saved. At the place where the chapel of the Martyrs was located (now 11 rue Yvonne-Le Tac), an oratory was built in 1855. It was renovated in 1994.By the 15th century, the north and northeast slopes of the hill were the site of a village surrounded by vineyards, gardens and orchards of peach and cherry trees. The first mills were built on the western slope in 1529, grinding wheat, barley and rye. There were thirteen mills at one time, though by the late nineteenth century only two remained,During the 1590 Siege of Paris, in the last decade of the French Wars of Religion, Henry IV placed his artillery on top of the butte of Montmartre to fire down into the city. The siege eventually failed when a large relief force approached and forced Henry to withdraw.
In 1790, Montmartre was located just outside the limits of Paris. That year, under the revolutionary government of the National Constituent Assembly, it became the commune of Montmartre, with its town hall located on place du Tertre, site of the former abbey. The main businesses of the commune were wine making, stone quarries and gypsum mines. (See Mines of Paris).  The mining of gypsum had begun in the Gallo-Roman period, first in open air mines and then underground, and continued until 1860. The gypsum was cut into blocks, baked, then ground and put into sacks. Sold as 'montmartarite, it was used for plaster, because of its resistance to fire and water. Between the 7th and 9th centuries, most of the sarcophagi found in ancient sites were made of molded gypsum. In modern times, the mining was done with explosives, which riddled the ground under the butte with tunnels, making the ground very unstable and difficult to build upon. The construction of the Basilica of Sacré-Cœur required making a special foundation that descended 40 metres under the ground to hold the structure in place. A fossil tooth found in one of these mines was identified by Georges Cuvier as an extinct equine, which he dubbed Palaeotherium, the "ancient animal". His sketch of the entire animal in 1825 was matched by a skeleton discovered later.


=== 19th century ===

Russian soldiers occupied Montmartre during the battle of Paris in 1814. They used the altitude of the hill for artillery bombardment of the city.
Montmartre remained outside of the city limits of Paris until January 1, 1860, when it was annexed to the city along with other communities (faubourgs) surrounding Paris, and became part of the 18th arrondissement of Paris.
In 1871, Montmartre was the site of beginning of the revolutionary uprising of the Paris Commune. During the Franco-Prussian War, the French army had stored a large number of cannon in a park at the top of the hill, near where the Basilica is today. On 18 March 1871, the soldiers from the French Army tried to remove the cannon from the hilltop. They were blocked by members of the politically-radicalised Paris National Guard, who captured and then killed two French army generals, and installed a revolutionary government that lasted two months. The heights of Montmartre were retaken by the French Army with heavy fighting at the end of May 1871, during what became known as "Bloody Week".In 1870, the future French prime minister during World War I, Georges Clemenceau, was appointed mayor of the 18th arrondissement, including Montmartre, by the new government of the Third Republic, and was also elected to the National Assembly. A member of the radical republican party, Clemenceau tried unsuccessfully to find a peaceful compromise between the even more radical Paris Commune and the more conservative French government. The Commune refused to recognize him as mayor, and seized the town hall. He ran for a seat in the council of the Paris Commune, but received less than eight hundred votes.  He did not participate in the Commune, and was out of the city when the Commune was suppressed by the French army. In 1876, he again was elected as deputy for Montmartre and the 18th arrondissement.The Basilica of the Sacré-Cœur was built on Montmartre from 1876 to 1919, financed by public subscription as a gesture of expiation for the suffering of the city during the Franco-Prussian War and the 1871 Paris Commune. Its white dome is a highly visible landmark in the city, and near it artists set up their easels each day amidst the tables and colourful umbrellas of the place du Tertre.
By the 19th century, the butte was famous for its cafés, guinguettes with public dancing, and cabarets. Le Chat Noir at 84 boulevard de Rochechouart was founded in 1881 by Rodolphe Salis, and became a popular haunt for writers and poets. The composer Eric Satie earned money by playing the piano there. The Moulin Rouge at 94 boulevard de Clichy was founded in 1889 by Joseph Oller and Charles Zidler; it became the birthplace of the French cancan.  Artists who performed in the cabarets of Montmartre included Yvette Guilbert, Marcelle Lender, Aristide Bruant, La Goulue, Georges Guibourg, Mistinguett, Fréhel, Jane Avril, and Damia.


== Artists gather ==

During the Belle Époque from 1872 to 1914, many notable artists lived and worked in Montmartre, where the rents were low and the atmosphere congenial. Pierre-Auguste Renoir rented space at 12 rue Cortot in 1876 to paint Bal du moulin de la Galette, showing a dance at Montmartre on a Sunday afternoon. Maurice Utrillo lived at the same address from 1906 to 1914, and Raoul Dufy shared an atelier there from 1901 to 1911. The building is now the Musée de Montmartre. Pablo Picasso, Amedeo Modigliani and other artists lived and worked in a building called Le Bateau-Lavoir during the years 1904–1909, where Picasso painted one of his most important masterpieces, Les Demoiselles d'Avignon. Several noted composers, including Erik Satie, lived in the neighbourhood. Most of the artists left after the outbreak of World War I, the majority of them going to the Montparnasse quarter.Artists' associations such as Les Nabis and the Incohérents were formed and individuals including Vincent van Gogh, Pierre Brissaud, Alfred Jarry, Jacques Villon, Raymond Duchamp-Villon, Henri Matisse, André Derain, Suzanne Valadon, Edgar Degas, Henri de Toulouse-Lautrec, Théophile Steinlen, and African-American expatriates such as Langston Hughes worked in Montmartre and drew some of their inspiration from the area.
The last of the bohemian Montmartre artists was Gen Paul (1895–1975), born in Montmartre and a friend of Utrillo. Paul's calligraphic expressionist lithographs, sometimes memorializing picturesque Montmartre itself, owe a lot to Raoul Dufy.
Among the last of the neighborhood's bohemian gathering places was R-26, an artistic salon frequented by Josephine Baker, Le Corbusier and Django Reinhardt. Its name was immortalized by Reinhardt in his 1947 tribute song "R. vingt-six".


== Modern-day ==

There is a small vineyard in the Rue Saint-Vincent, which continues the tradition of wine production in the Île de France; it yields about 500 litres per year.The Musée de Montmartre is in the house where the painters Maurice Utrillo and Suzanne Valadon lived and worked in second-floor studios. The house was Pierre-Auguste Renoir's first Montmartre address, and he painted several of his masterpieces there. Many other well known personalities moved through the premises.
The mansion in the garden at the back is the oldest hotel on Montmartre, and one of its first owners was Claude de la Rose, a 17th-century actor known as Rosimond, who bought it in 1680. Claude de la Rose was the actor who replaced Molière, and who, like his predecessor, died on stage.
Nearby, day and night, tourists visit such sights as Place du Tertre and the cabaret du Lapin Agile, where the artists had worked and gathered. Many renowned artists, such as painter and sculptor Edgar Degas and film director François Truffaut, are buried in the Cimetière de Montmartre and the Cimetière Saint-Vincent. Near the top of the butte, Espace Dalí showcases surrealist artist Salvador Dalí's work.
Montmartre is an officially designated historic district with limited development allowed in order to maintain its historic character.
A funicular railway, the Funiculaire de Montmartre, operated by the RATP, ascends the hill from the south while the Montmartre bus circles the hill.
Downhill to the southwest is the red-light district of Pigalle. That area is, today, largely known for a wide variety of stores specializing in instruments for rock music. There are also several concert halls, also used for rock music. The actual Moulin Rouge theatre is also in Pigalle, near the Blanche métro station.


== In popular culture ==


=== Films ===
The Heart of a Nation (released 1943) features a family resident in Montmartre from 1870 to 1939.
An American in Paris (1951), with Gene Kelly and Leslie Caron, was the winner of the Oscar for the best film of 1951. Many important scenes, including the last scenes, take place in Montmartre, although most of the film was shot in Hollywood.
Moulin Rouge told the story of the life and lost loves of painter Henri de Toulouse-Lautrec.
French Cancan (1954), a French musical comedy with Jean Gabin and María Félix, takes place in Montmartre, and tells the story of the Moulin Rouge and the invention of the famous dance. The director, Jean Renoir, was the son of the painter Pierre-Auguste Renoir, who painted several important works while living in Montmartre.
The Great Race (1965), shows Professor Fate in the "Hannibal 8" driving down the Basilica steps after a wrong turn while racing to the Eiffel tower.
Ronin (1998): Outside of the café at the beginning and end.
Amélie (2001): the story of a young Parisian woman determined to help the lives of others and find her true love, is set in Montmartre and includes a key scene in the gardens below the Basilica.
Moulin Rouge! (2001): a musical film set in Montmartre, is about the night club and a young writer (Ewan McGregor) who falls in love with a famous courtesan (Nicole Kidman).
Remake (2003): Bosnian war film tells the parallel coming-of-age stories of a father living in Sarajevo during World War II and his son living through the Siege of Sarajevo during the Bosnian War. Part of the film was shot in Paris and important scene take place in Montmartre. The film stars François Berléand and Évelyne Bouix.
La Môme (2007) (La vie en rose): tells the life of French singer Édith Piaf who was discovered while singing in Pigalle, bordering Montmartre.
Beauty and the Beast (2017): live action version of a 1991 animated film. The film features a scene in which Belle (Emma Watson) and Beast (Dan Stevens) are magically transported to the abandoned attic of a windmill atop Montmartre.
Bastille Day (2016) opens with a pickpocket (the main antagonist) pickpocketing on the stairs in front of the Sacré-Cœur with an accomplice.


=== Songs ===
In "La Bohème", a 1965 song by singer-songwriter Charles Aznavour, a painter recalls his youthful years in a Montmartre that has, for him, ceased to exist: "I no longer recognize/Either the walls or the streets/That had seen my youth/At the top of a staircase/I look for my studio/Of which nothing survives/In its new décor/Montmartre seems sad/And the lilacs are dead'). The song is a farewell to what, according to Aznavour, were the last days of Montmartre as a site of bohemian activity.
In the Slade song, "Far, Far Away", the second verse begins... "I've seen the Paris lights from high upon Montmartre..."
In the John Denver song, "A Country Girl in Paris", the third verse begins... "Up upon Montmartre when she stops to rest awhile..."


== Main sights ==

The Place du Tertre, known for the artists who paint tourists for pleasure and money
The Bateau-Lavoir site, the former home and studio of many well-known artists including Pablo Picasso
The Dalida house in Rue d'Orchampt
The Place Dalida
The Place Pigalle and the Moulin Rouge in the south
The marché Saint-Pierre, area of the cloth sellers, in the south-east
The working class districts with immigrant communities: Barbès (Maghreb) in the southeast, Château Rouge in the east
The boulevard de Rochechouart (metro stations: Anvers and Pigalle) for its concert halls (La Cigale, L'Elysée-Montmartre, Le Trianon, La Boule Noire) inspired by the 19th century cabarets
The cimetière de Montmartre
The Rue Lepic with its Les Deux Moulins café, made famous by the 2001 film Le Fabuleux Destin d'Amélie Poulain
Erik Satie's house
The Place Marcel-Aymé, site of the R-26 artistic salon and the statue Le passe muraille
Le Chat noir and the Lapin Agile cabarets whose clientele at the beginning of the 20th century was mainly French artists
The Moulin de la Galette
The funiculaire de Montmartre, a funicular railway used instead of the steps to ascend the highest part of the hill
The Place Émile-Goudeau, where the Bateau-Lavoir was the home and studio of many great painters
Place Jean-Marais
The Espace Dalí, a museum dedicated to several of the surrealist's masterpieces
The Wall of Love in the Jehan Rictus garden square
The Martyrium of Saint Denis


== See also ==


== References ==


== Bibliography ==
Brigstocke, Julian. The Life of the City: Space, Humour, and the Experience of Truth in Fin-de-siècle Montmartre (Ashgate, 2014) xv + 230pp online review
Cate,  Phillip Dennis and Mary Shaw. The Spirit of Montmartre: Cabarets, Humor, and the Avant-Garde 1875–1905 (Rutgers University Press, 1996)
Weisberg, Gabriel, ed. Montmartre and the Making of Mass Culture (Rutgers U. Press, 2001)


=== In French ===
Sarmant, Thierry (2012). Histoire de Paris: Politique, urbanisme, civilisation. Editions Jean-Paul Gisserot. ISBN 978-2-755-803303.
Dictionnaire Historique de Paris. Le Livre de Poche. 2013. ISBN 978-2-253-13140-3.
Vie quotidienne a Montmartre au temps de Picasso, 1900–1910 (Daily Life on Montmartre in the Times of Picasso) was written by Jean-Paul Crespelle, an author-historian who specialized in the artistic life of Montmartre and Montparnasse.


== External links ==
 Media related to Montmartre at Wikimedia Commons