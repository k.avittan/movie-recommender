In Christian hamartiology, eternal sins, unforgivable sins, unpardonable sins, or ultimate sins are sins which will not be forgiven by God. One eternal or unforgivable sin (blasphemy against the Holy Spirit) is specified in several passages of the Synoptic Gospels, including Mark 3:28–29, Matthew 12:31–32, and Luke 12:10.


== Jewish doctrine ==
Judaism teaches that no sin can overcome God's forgiveness of sins. In Deuteronomy 29:17–19, it says:

Perchance there is among you some man or woman … whose heart is even now turning away from the Eternal our God to go and worship the gods of those nations, … When [for example] a man hears the words of these sanctions, he may fancy himself immune, thinking, ‘I shall be safe, though I follow my own willful heart.’… The Eternal will never forgive such individuals.
Rabbis interpret "the Eternal will never forgive such individuals" and blasphemy against the Holy Spirit being unforgivable as exaggerations of severity rather than as decrees or condemnations.


== Christian doctrine ==


=== New Testament passages ===
Several passages in the New Testament are frequently interpreted as referring to the unforgivable sin:

Matthew 12:30-32: "Whoever is not with me is against me, and whoever does not gather with me scatters.  And so I tell you, any sin and blasphemy can be forgiven. But blasphemy against the Spirit will not be forgiven.  Anyone who speaks a word against the Son of Man will be forgiven, but anyone who speaks against the Holy Spirit will not be forgiven, either in this age or in the age to come."
Mark 3:28–30: 28 Verily I say unto you, All sins shall be forgiven unto the sons of men, and blasphemies wherewith soever they shall blaspheme: 29 But he that shall blaspheme against the Holy Ghost hath never forgiveness, but is in danger of eternal damnation. 30 - Because they said, He hath an unclean spirit.
Luke 12:8-10: "I tell you, whoever acknowledges me before men, the Son of Man will also acknowledge him before the angels of God. But he who disowns me before men will be disowned before the angels of God. And everyone who speaks a word against the Son of Man will be forgiven, but anyone who blasphemes against the Holy Spirit will not be forgiven."


=== Eastern Christianity ===
The importance of prayer (1 Thessalonians 5:17: "pray without ceasing") and humility (Jesus Prayer: "Lord Jesus Christ, Son of God, have mercy on me, a sinner") in Christianity is reflected by an Orthodox catechism as follows:
Jesus Christ called the Holy Spirit "Spirit of Truth" (John 14:17; 15:26; 16:13) and warned us, "All manner of sin and blasphemy shall be forgiven unto men; but the blasphemy against the Holy Spirit shall not be forgiven unto men" (Matthew 12:31)."Blasphemy against the Holy Spirit" is conscious and hardened opposition to the truth, "because the Spirit is truth" (1 John 5:6). Conscious and hardened resistance to the truth leads man away from humility and repentance, and without repentance there can be no forgiveness. That is why the sin of blasphemy against the Spirit cannot be forgiven, since one who does not acknowledge his sin does not seek to have it forgiven. 


=== Roman Catholicism ===

The Catechism of the Catholic Church teaches that, while no sin is absolutely "unforgivable", some sins represent a deliberate refusal to repent and accept the infinite mercy of God; a person committing such a sin refuses God's forgiveness, which can lead to self-condemnation to hell. In other words, one damns oneself by final impenitence (refusal to repent), as taught by John Paul II:
The images of hell that Sacred Scripture presents to us must be correctly interpreted...hell indicates the state of those who freely and definitively separate themselves from God...“To die in mortal sin without repenting and accepting God’s merciful love means remaining separated from him for ever by our own free choice. This state of definitive self-exclusion from communion with God and the blessed is called ‘hell’”...“Eternal damnation”, therefore, is not attributed to God's initiative because in his merciful love he can only desire the salvation of the beings he created. In  reality, it is the creature who closes himself to his love. Damnation consists precisely in definitive separation from God, freely chosen by the human person and confirmed with death that seals his choice for ever. God’s judgement ratifies this state.
The Catholic Encyclopedia cites Matthew 12:22–32; Mark 3:22–30; Luke 12:10 (cf. 11:14–23) and defines "the unforgivable sin"—or sin against the Holy Ghost—as follows: ″... to sin against the Holy Ghost is to confound Him with the spirit of evil, it is to deny, from pure malice, the Divine character of works manifestly Divine.″ The article further states that "sin against the Son of Man" may be forgiven because it is committed against the human person of Christ, which veils the Divine with a "humble and lowly appearance," and therefore such sin is excusable because it is committed through "man's ignorance and misunderstanding." The Church Fathers considered additional interpretations, Augustine of Hippo calling it one of the more difficult passages of Scripture. Thomas Aquinas summarized the Church Fathers' treatments and proposed three possible explanations: 1. That an insult directed against any of the Three Divine Persons may be considered a sin against the Holy Spirit; and/or 2. That persisting in mortal sin till death, with final impenitence, as Augustine proposed, frustrates the work of the Holy Spirit, to whom is appropriated the remission of sins; and/or 3. That sins against the quality of the Third Divine Person, being charity and goodness, are conducted in malice, in that they resist the inspirations of the Holy Spirit to turn away from or be delivered from evil. Such sin may be considered graver than those committed against the Father through frailty (the quality of the Father being power), and those committed against the Son through ignorance (the quality of the Son being wisdom). Thomas Aquinas lists, or has responded to, six sins that go against the Holy Spirit:
despair: which consists in thinking that one's own malice is greater than Divine Goodness, as the Master of the Sentences teaches,
presumption: if a man wants to obtain glory without merits or pardon without repentance
resistance to the known truth,
envy of a brother's spiritual good, i.e., of the increase of Divine grace in the world,
impenitence, i.e., the specific purpose of not repenting a sin,
obstinacy, whereby a man, clinging to his sin, becomes immune to the thought that the good searched in it is a very little one.Thomas Aquinas explains that the unforgivability of blasphemy against the Holy Spirit means that it removes the entrance to these means of salvation; however, it cannot hinder God in taking away this obstacle by way of a miracle.However, the Church further believes there is no offence, however serious, that cannot be taken away by Baptism, or absolved from in the Confessional—that no one, however wicked and guilty, may not confidently hope for forgiveness. The Catechism says that Christ desires "the gates of forgiveness should always be open to anyone who turns away from sin." As did St Augustine, the Catholic Church today teaches that only dying unrepentant for one's sins is the only unforgivable sin. Indeed, in Dominum et vivificantem Pope John Paul II writes "According to such an exegesis, 'blasphemy' does not properly consist in offending against the Holy Spirit in words; it consists rather in the refusal to accept the salvation which God offers to man through the Holy Spirit, working through the power of the Cross", and "If Jesus says that blasphemy against the Holy Spirit cannot be forgiven either in this life or in the next, it is because this "non-forgiveness" is linked, as to its cause, to "non-repentance," in other words to the radical refusal to be converted. This means the refusal to come to the sources of Redemption, which nevertheless remain "always" open in the economy of salvation in which the mission of the Holy Spirit is accomplished."


=== Protestantism ===

Protestant denominations and theologians have taken various approaches in defining the sin against the Holy Spirit.
John Calvin wrote:

I say, therefore, that he sins against the Holy Spirit who, while so constrained by the power of divine truth that he cannot plead ignorance, yet deliberately resists, and that merely for the sake of resisting.
Similarly, Jacob Arminius defined it as "the rejection and refusing of Jesus Christ through determined malice and hatred against Christ". However, Arminius differed with Calvin in believing that the sin could be committed by believers, a conclusion he reached through his interpretation of Hebrews 6:4–6.Some modern Protestant interpretations of the sin include the deliberate labeling of good as evil, as rejecting the conviction of the Holy Spirit, of publicly attributing the work of the Holy Spirit to Satan, and attributing the work of Jesus to Satan (under this interpretation, the sin could only have been committed in the first century AD). For example, The United Methodist Church, which was founded by John Wesley, upholds:

that the penalty of eternal separation from God with no hope of return applies in scripture only in two cases—either, as in Hebrews 6 and 10, to persons who willfully, publically [sic] and explicitly reject Jesus as Savior after having confessed him, or, as in the gospels, to those who blaspheme against the Holy Spirit by declaring that the works of Jesus were the works of the Evil one. A prominent Methodist catechism, "A Catechism on the Christian Religion: The Doctrines of Christianity with Special Emphasis on Wesleyan Concepts" states:The unpardonable sin is blasphemy against the Holy Spirit. Blasphemy includes ridicule and attributing the works of the Holy Spirit to the devil.
Regardless of their interpretation, Protestant interpreters generally agree that one who has committed the sin is no longer able to repent, so one who is fearful that they have committed it has not done so.


=== Mormonism ===

Members of The Church of Jesus Christ of Latter-day Saints, or Mormons, have a similar understanding of eternal sin. Joseph Smith, the founder of the Latter Day Saint movement, said in the King Follett discourse:

All sins shall be forgiven, except the sin against the Holy Ghost; for Jesus will save all except the sons of perdition. What must a man do to commit the unpardonable sin? He must receive the Holy Ghost, have the heavens opened unto him, and know God, and then sin against him. After a man has sinned against the Holy Ghost, there is no repentance for him. He has got to say that the sun does not shine while he sees it; he has got to deny Jesus Christ when the heavens have been opened unto him, and to deny the plan of salvation with his eyes open to the truth of it; and from that time he begins to be an enemy.
Church apostle and later President of the Church, Spencer W. Kimball, stated that "the sin against the Holy Ghost requires such knowledge that it is manifestly impossible for the rank and file [of the church] to commit such a sin".


== See also ==

Anantarika-karma
Shirk (Islam)
Lincoln's House Divided Speech Which also references the episode in Matthew Chapter 12 and Mark Chapter 3.


== References ==


== External links ==
CCEF Counseling article
What is the Unforgivable Sin? by Jeremy Myers