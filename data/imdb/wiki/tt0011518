Nothing But Thirty (Chinese: 三十而已; pinyin: Sān Shí Ér Yǐ) is a 2020 Chinese drama television series directed by Zhang Xiaobo and starring Jiang Shuying, Tong Yao and Mao Xiaotong. The series follows the story of three vastly different urban women who have reached their thirties while facing challenges at a crucial stage in their lives, as they leave behind their youthful, carefree 20s to embrace the "adult" life of a 30-year-old, and ultimately decide to take matters into their own hands. The series airs on Dragon Television and Tencent Video starting July 17, 2020.


== Plot ==
The story chronicles the life of three women with different backgrounds and different personalities.
Gu Jia is strong willed, and is both a housewife and a businesswoman. She helped her husband become the CEO of a fireworks company, and fought rich housewives who bullied her son. Wang Manni is rebellious, and is confident at both her workplace and at home. Zhong Xiaoqin is content with her average life, with a normal job and a husband who likes fish.
However, their lives are suddenly disrupted by external factors. Gu Jia begins to suspect her husband Xu Huanshan of having a young mistress, Wang Manni faces difficulties with sales and relationships, and Zhong Xiaoqin's idea of a perfect marriage is destroyed. How will the three women deal with the challenges of turning thirty?
Additionally a couple with a son close in age to Gu Jia's son Ziyan, who operate a Food truck appear as silent characters. They occasionally interact with the main character and consistently appear at the end of the show before the closing credits.


== Cast ==


=== Main ===
Jiang Shuying as Wang Manni (王漫妮), a senior sales person at Mishil.
Tong Yao as Gu Jia (顾佳), wife of Xu Huanshan. She is intelligent and determined.
Mao Xiaotong as Zhong Xiaoqin (钟晓芹), an employee at a property management company. Wife of Chen Yu.


=== Supporting ===
Yang Le as Chen Yu (陈屿), executive editor of the Breaking News Department of a TV station. The husband of Zhong Xiaoqin.
Li Zefeng as Xu Huanshan (许幻山), a nerdy designer and then the CEO of a fireworks company, husband of Gu Jia.
Yang Lixin as Gu Jinghong (顾景鸿), Gu Jia's father.
Yan Zidong as Zhong Xiaoyang (钟晓阳), Zhong Xiaoqin's boyfriend.
Edward Ma as Liang Zhengxian (梁正贤), an Asian-American businessman and playboy, boyfriend of Wang Manni.
Zhang Yue as Lin Youyou (林有有), an amusement park employee and Xu Huanshan's mistress.
Mao Yi as Jiang Chen (姜辰), owner of a café and Wang Manni's former boyfriend.
Wang Zijian as Zhang Zhi (张志) Director of Quzhou's local government and Wang Manni's boyfriend.
Samantha Ko as Zhao Jingyu (赵静语), Liang Zhengxian's fiancée.
Dai Jiaoqian as Miss Lu (陆姐), Chen Yu's superior.
Yang Xinming as Uncle Yu (于伯), a barber in Quzhou.
Yang Yuting as Mrs. Wang (王太太), a wealthy woman who is friends with Gu Jia.
Cai Die as Xiao Bai (小白)
Fu Miao as Boss
Walley Wei as Zoe
Wiyona Yeung Lau Ching as Amanda, a Hong Kong salesperson at Mishil.
Tian Yitong as Daisy, the assistant head salesperson at Mishil.
Wang Renjun as father of Shen Jie (沈杰), employee of Xu Huanshan.
The name "Sān Shí Ér Yǐ" (三十而已) based on a sentence "As thirty, I stood firm" (三十而立) comes from Analects.


== Reception ==
Nothing But Thirty received mainly positive reviews. Douban gave the drama 7.4 out of 10.


== References ==


== External links ==
Nothing But Thirty at Douban (in Chinese)
Nothing But Thirty on IMDb