Righting Wrongs (also known as Above the Law, and known in the Philippines as Fight to Win II) is a 1986 Hong Kong action film produced and directed by Corey Yuen, and also produced by and starring Yuen Biao, both of whom also serve as the film's action directors. The film also co-stars Cynthia Rothrock, Melvin Wong, Wu Ma, Roy Chiao and director Yuen himself. Righting Wrongs is the one of Yuen Biao's better known films that he made without film industry compatriots Sammo Hung and Jackie Chan.


== Title ==
The film's Hong Kong English language title is Righting Wrongs. The international version (dubbed in English) is titled Above the Law, which was also the title adopted for The Weinstein Company's 2007 US DVD release on their Dragon Dynasty label.


== Plot ==
Jason Ha Ling-ching (Yuen Biao) is a dedicated, by the books prosecutor who has tried to maintain patience and tolerance under the somewhat flimsy laws of the court. However, when his mentor is publicly gunned down in New Zealand and when the key witness of Ha's latest case and his entire family are wiped out over night, Ha can no longer go by the book.
Ha initial plan is to take the law into his own hands and kill the two men he believes called for his witness' murder. He is successful in killing the first, which causes the Hong Kong police department to wake up and take action to regain order. Enter Senior Inspector Cindy Si (Cynthia Rothrock), who is put on the case to find the killer under her superior, Superintendent Wong Ching-wai (Melvin Wong). However, when Ha goes to kill the second defendant, Chow Ting-kwong (James Tien), he is already dead. Unbeknownst to them, both of the defendants were working under an even higher power, known only as "Crown". However, it is soon discovered that "Crown" is none other than Superintendent Wong, who was also Chow's killer.
Once Si realizes that Wong is the true mastermind behind all of the recent murders taking place, she and Ha finally work together to bring him in to prove he is not "above the law". However, depending on the version of the story told, both Si and Ha fall victim to their ambition, with Si killed with a large screw through the neck by Wong and Ha falling to his demise after killing Wong in an out of control airplane. However, in another version of the story, Si survives her attack from Wong, and while Ha still kills Wong, he falls victim to a different, and rather ironic, fate: Ha is given an eight-year sentence in prison under the charge of manslaughter (English dub), or a life sentence in prison under the charge of First Degree Murder (Mandarin dub).


== Cast ==
Yuen Biao as Jason Ha Ling-ching
Cynthia Rothrock as Senior Inspector Cindy Si
Roy Chiao as Magistrate Judge
Melvin Wong as Superintendent Wong Ching-wai
Louis Fan as Sammy Yu Chi-man
Corey Yuen as Bad Egg
Sandy Chan as Jason Ha's girlfriend
Chung Fat as Red Porsche Policeman
Wu Ma as Uncle Tsai
Peter Cunningham as Black Assassin
Lau Sing-ming as Sammy's Grandfather
Karen Sheperd as Karen
Tai Po as Yellow Shirt Cop
James Tien - Chow Ting-kwong
Hsu Hsia as Mr. Leung (protected witness)
Lau Chau-sang as Cop
Chow Kam-kong as Station Cop
Stephen Chan as Hung
Siu Bo as Cop guarding Mr. Leung / Hanger Thug (2 roles)
Yuen Miu as Cop guarding Mr. Leung
King Lee as Cop
Paul Chang as Four Eyes Bill
Hsiao Hou
Fruit Chan


== Theme song ==
Proud (狂傲)
Composer: Akira Mitake
Lyricist: Lo Keok-chim
Singer: Jacky Cheung


== Production information ==
Cynthia Rothrock was originally supposed to act in Jackie Chan's 1987 film, Armour of God, Rothrock was declined. But due to Jackie Chan's injury halting production of the film, however Rothrock was reassigned to play as the lead actress in this film.
This is Rothrock's personal favorite of her own films.
The airplane stunt finale was shot in New Zealand, rather than Hong Kong.
Supposedly, many of the times Rothrock was doubled, she would be doubled by stuntman and fight choreographer Meng Hoi.
Rothrock did not speak Cantonese or Mandarin, so all of her lines were spoken in English and then later dubbed into the respective languages.
According to Rothrock, despite being an impressive martial arts villain on screen in this film, actor Melvin Wong apparently had no formal martial arts training whatsoever prior to this film.
According to Melvin Wong, the airplane hangar fight between him and Yuen Biao was "guest directed" by none other than Sammo Hung.
The film has two main versions in Cantonese and Mandarin the primary difference being additional scenes for the Mandarin version and another ending.


== Accolades ==


== See also ==
List of Hong Kong films
Yuen Biao filmography


== References ==


== External links ==
Righting Wrongs on IMDb