Flames of Passion (1922) was a British silent film drama directed by Graham Cutts, starred Mae Marsh and C. Aubrey Smith.
The film was made by the newly formed Graham-Wilcox Productions company, a joint venture between Cutts and producer Herbert Wilcox.  The entrepreneurial Wilcox tempted American star Marsh to England with a high salary offer, believing this would improve the film's marketability in the U.S. She was paid £1,000 a week.The gamble paid off as it became the first post-war British film to be sold to the U.S. The final reel of the film was filmed in the bi-pack color process Prizma Color.


== Plot ==
The wife of a wealthy barrister seduces her chauffeur, with whom she falls in love.  She gives birth to a baby, apparently without her husband knowing anything about her pregnancy.
The child is killed by the chauffeur during a car accident—he was visibly drunk when driving. The result is a showpiece trial at the Old Bailey, presumably of the chauffeur on a charge of infanticide, in which the woman at first tries to protect her lover, but is forced finally under cross-examination to make a dramatic public confession that the dead infant was hers. By the end of the film, she returns to her husband.


== Cast ==
Mae Marsh as Dorothy Hawke
C. Aubrey Smith as Richard Hawke, K.C.
Hilda Bayley as Kate Watson
Herbert Langley as Arthur Watson
Allan Aynesworth as Forbes
Eva Moore as Aunt
George K. Arthur as Friend
Henry Vibart as Lord Chief Justice


== Reception ==
Flames of Passion proved controversial with critics, many of whom found the subject matter lurid, sensationalist and distasteful. Cinemagoers had no such qualms, and turned the film into a big box-office hit, Wilcox's first commercial success.This was the first British film to be sold for distribution in the United States following World War I where it was shown under the title A Woman's Secret.


== Preservation status ==
A print with Dutch titles exists at the British Film Institute.


== See also ==
List of early colour feature films
List of lost films


== References ==


== External links ==
Flames of Passion on IMDb
Flames of Passion at BFI Database
Flames of Passion at SilentEra