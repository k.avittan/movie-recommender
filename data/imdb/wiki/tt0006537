Combat (French for fight) is a purposeful violent conflict meant to weaken, establish dominance over, or kill the opposition, or to drive the opposition away from a location where it is not wanted or needed.
Combat is typically between opposing military forces in warfare. Combat violence can be unilateral, whereas fighting implies at least a defensive reaction. A large-scale fight is known as a battle. A verbal fight is commonly known as an argument.
Combat effectiveness, in the strategic field, requires combat readiness. In military areas, the term is applied also to personnel, that has to receive proper training and be qualified to carry out combat operations in the unit to which they are assigned.


== Background ==
Combat may take place under a specific set of rules or unregulated. Examples of rules include the Geneva Conventions (covering the treatment of people in war), medieval chivalry, the Marquess of Queensberry rules (covering boxing) and several forms of combat sports.

Combat in warfare involves two or more opposing military organizations, usually fighting for nations at war (although guerrilla warfare and suppression of insurgencies can fall outside this definition). Warfare falls under the laws of war, which govern its purposes and conduct, and protect the rights of combatants and non-combatants.
Combat may be armed (using weapons) or unarmed (not using weapons). Hand-to-hand combat (melee) is combat at very close range, attacking the opponent with the body (striking, kicking, strangling, etc.) and/or with a melee weapon (knives, swords, batons, etc.), as opposed to a ranged weapon.
Hand-to-hand combat can be further divided into three sections depending on the distance and positioning of the combatants:

Clinch fighting
Ground fighting
Stand-up fighting


== See also ==
Mock combat, a mode often used in training for true combat


== References ==


== Sources ==
Martin van Creveld: The Changing Face of War: Lessons of Combat, from the Marne to Turkey. Maine, New England 2007.


== Further reading ==
Wong, Leonard. 2006. “Combat Motivation in Today’s Soldiers: U.S. Army War College Strategic Studies Institute.”Armed Forces & Society, vol. 32: pp. 659–663.
Gifford, Brian. 2005. “Combat Casualties and Race: What Can We Learn from the 2003-2004 Iraq Conflict?” Armed Forces & Society, vol. 31: pp. 201–225.
Herspring, Dale. 2006. “Undermining Combat Readiness in the Russian Military, 1992-2005.” Armed Forces & Society, Jul 2006; vol. 32: pp. 513–531.
Ben-Shalom, Uzi; Lehrer, Zeev; and Ben-Ari, Eyal. 2005. “Cohesion during Military Operations: A Field Study on Combat Units in the Al-Aqsa Intifada.” Armed Forces & Society, vol. 32: pp. 63–79.
Woodruff, Todd; Kelty, Ryan; Segal, Archie Cooper, David R. 2006. “Propensity to Serve and Motivation to Enlist among American Combat Soldiers.” Armed Forces & Society, Apr 2006; vol. 32: pp. 353–366.
Dienstfrey, Stephen. 1988. “Women Veterans’ Exposure to Combat.” Armed forces & Society, vol. 14: pp. 549–558.