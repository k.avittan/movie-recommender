Defenders of the Faith is the ninth studio album by English heavy metal band Judas Priest, released on 4 January 1984 by Columbia Records. The album was certified platinum by the RIAA, and spawned the singles "Freewheel Burning", "Some Heads Are Gonna Roll", and "Love Bites".


== Recording and release ==
Defenders of the Faith was recorded at Ibiza Sound Studios, Ibiza, Spain, and mixed from September to November 1983 at DB Recording Studios and Bayshore Recording Studios in Coconut Grove, Miami, Florida. The LP and cassette tape were released on 4 January 1984, and the album appeared on CD in July. A remastered CD was released in May 2001. Simultaneously with the album's release, the band kicked off their tour in Europe, with the bulk of concerts taking place in North America during the spring and summer.
Stylistically, Defenders of the Faith did not greatly depart from its predecessor and featured a similar formula of short, up-tempo metal anthems with stadium shout-along choruses, although progressive elements returned on some tracks such as "The Sentinel". The track "Rock Hard Ride Free" is actually a re-recording of a track entitled "Fight for Your Life" which was recorded during the 1982 Screaming For Vengeance sessions but not included on that album.
The album was an immediate success, only going one spot below Screaming for Vengeance on the US Billboard 100 Albums Chart. Some critics nonetheless objected to the lack of a standout single comparable to "Breaking the Law" or "You've Got Another Thing Comin', and the album's general similarity to Screaming for Vengeance.
The 30th-anniversary release of the album (released in March 2015) came with a double CD of a live show recorded on 5 May 1984 at the Long Beach Arena in Long Beach, California on their Defenders of the Faith Tour and was originally recorded for a radio broadcast.


== Packaging ==
The cover art by Doug Johnson (who also designed the Hellion in Screaming for Vengeance) depicts the Metallian, a ram-horned, tiger-like land assault creature with Gatling guns and tank tracks conceptualized by the band. The back cover contains a message:

Rising from darkness where Hell hath no mercy and the screams for vengeance echo on forever. Only those who keep the faith shall escape the wrath of the Metallian ... Master of all metal.


== Controversy ==
"Eat Me Alive" was listed at number 3 on the Parents Music Resource Center's "Filthy Fifteen", a list of 15 songs the organization found most objectionable. PMRC co-founder Tipper Gore stated the song was about oral sex at gunpoint. In response to the allegations, Priest recorded the song "Parental Guidance" on the follow-up album Turbo.

In a uniquely British way, Rob's S&M lyrics were intended to be tongue in cheek—and certainly not "corrupting", as Tipper Gore and the Parents Music Resource Center (PMRC) took them to be. They certainly didn't warrant being included on the PMRC's "Filthy 15" list a few months after the album was released. For us, the song was a bit of fun—but I won’t deny that we included it with full knowledge that it would get media attention. Little did we know at that time that its inclusion on the "Filthy 15" would be the precursor to a far more disturbing predicament for us.


== Promotion ==
In December 1983, the band released the "Freewheel Burning" single and performed the song in a short UK/Germany tour in the same month. In January 1984, the band embarked on the Metal Conqueror Tour across Europe, North America and Japan. On this tour, the band played every song from the album live, with the exception of "Eat Me Alive". During the band's 2008 tour in support of Nostradamus, they played many songs which had never been played live before, one of them being "Eat Me Alive".  To date, this made Defenders of the Faith the second Judas Priest album from which every song had been played live (the first being Rocka Rolla), followed by British Steel during the 2009, British Steel 30th anniversary tour.


== Legacy ==
Anthrax guitarist Scott Ian has been quoted as saying that Mark Dodson's engineer work on this album and Sin After Sin inspired the band to work with him as the producer of their 1988 album State of Euphoria.
"Jawbreaker" was covered by Rage for the single "Higher than the Sky" and also appeared on the tribute album A Tribute to Judas Priest: Legends of Metal and All G.U.N. Bonustracks CD. In 2010, "Jawbreaker" was covered by Swedish power metal band Sabaton on the Re-Armed Edition of their album Metalizer. In 2017, "Jawbreaker" was covered by Swiss band Burning Witches on their debut self-titled album.
"The Sentinel" was covered by Machine Head for the special edition version of their 2011 album Unto the Locust.
"Night Comes Down" was covered by Katatonia for the Japanese edition of their 2016 album The Fall of Hearts.


== Track listing ==
All tracks are written by Glenn Tipton, Rob Halford and K. K. Downing, except where noted.30th Anniversary Edition - Bonus Live CDs


== Personnel ==
Judas PriestRob Halford – vocals
K. K. Downing – guitars
Glenn Tipton – guitars
Ian Hill – bass
Dave Holland – drumsProductionProduced by Tom Allom
Engineered by Mark Dodson, assisted by Christian Eser, Bruce Hensal, David Roeder, Ben King, and Buddy Thornton
Cover design by Doug Johnson, based on a concept by Judas Priest


== Charts ==


== Certifications ==


== References ==