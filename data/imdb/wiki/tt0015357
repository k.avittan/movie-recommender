Swords to ploughshares (or plowshares) is a concept in which military weapons or technologies are converted for peaceful civilian applications.
The phrase originates from the Book of Isaiah:

Many peoples shall come and say, "Come, let us go up to the mountain of the LORD, to the house of the God of Jacob; that he may teach us his ways and that we may walk in his paths." For out of Zion shall go forth instruction, and the word of the Lord from Jerusalem. He shall judge between the nations, and shall arbitrate for many peoples; they shall beat their swords into plowshares, and their spears into pruning hooks; nation shall not lift up sword against nation, neither shall they learn war any more. –
The ploughshare (Hebrew: אֵת‎ ’êṯ, also translated coulter) is often used to symbolize creative tools that benefit humankind, as opposed to destructive tools of war, symbolized by the sword (Hebrew: חֶרֶב‎ ḥereḇ), a similar sharp metal tool with an arguably opposite use.
In addition to the original Biblical Messianic intent, the expression "beat swords into ploughshares" has been used by disparate social and political groups.
An ongoing example as of 2013 is the dismantling of nuclear weapons and the use of their contents as fuel in civilian electric power stations, the Megatons to Megawatts Program. Nuclear fission development, originally accelerated for World War II weapons needs, has been applied to many civilian purposes since its use at Hiroshima and Nagasaki, including electricity and radiopharmaceutical production.


== Biblical references ==
This analogy is used several times in the Old Testament or Tanakh, in both directions, such as in the following verses:

He shall judge between the nations, and shall arbitrate for many peoples; they shall beat their swords into plowshares, and their spears into pruning hooks; nation shall not lift up sword against nation, neither shall they learn war any more.
Beat your plowshares into swords, and your pruning hooks into spears; let the weakling say, “I am a warrior.” 
He shall judge between many peoples, and shall arbitrate between strong nations far away; they shall beat their swords into plowshares, and their spears into pruning hooks; nation shall not lift up sword against nation, neither shall they learn war any more.
An expression of this concept can be seen in a bronze statue in the United Nations garden called Let Us Beat Swords into Plowshares, a gift from the Soviet Union sculpted by Evgeniy Vuchetich, representing the figure of a man hammering a sword into the shape of a ploughshare.


== Practical applications ==
After World War II, military surplus AFVs were sometimes converted into bulldozers, agricultural, and logging tractors, as seen in the American television series Axe Men. Two are currently preserved at the Swords and Ploughshares Museum in Canada. French farmers sometimes used modified versions of the obsolete FT-17 tank, and similar vehicles, based on the T-34 tank, remain in widespread use in the former USSR. Robert Crawford, a British agricultural engineer and collector of classic tractors, owns a Sherman tank that was adapted to plough Lincolnshire's fields in response to the shortage of crawler tractors.
From the 1970s onwards, several anti-war musicians play guitars made from military surplus weapons. Jamaican reggae star Pete Tosh famously owned a Stratocaster built around an M-16 rifle. In the present day the Escopetarra, a guitar converted from the AK-47, is the signature instrument of César López, Souriya Sunshine and Sami Lopakka of the Finnish death metal band Sentenced.
Nitrogen mustard, developed from the chemical weapon mustard gas developed in World War I, became the basis for the world's first chemotherapy drug, mustine, developed through the 1940s.
Swedish aid organization IM Swedish Development Partner launched Humanium Metal, using metal from illegal handguns to create everyday objects. The first product announced was headphones by Yevo.
The Global Positioning System was originally developed to enable more accurate strikes with long-range weapons by the United States, but its purpose was later expanded to include civilian applications such as personal navigation assistants.
The Plowshares movement (British, Christian, founded by Daniel Berrigan), Trident Ploughshares (British) and Pitstop Ploughshares (US, Christian) are peace movements, inspired by the book of Isaiah, in which participants attempt to damage or destroy modern weapons, such as nuclear missiles.
The Megatons to Megawatts Program, agreed to in 1993 by the United States and Russia, successfully converted 500 metric tonnes of fuel from Soviet-era nuclear warheads into fuel for nuclear power plants over a period of 20 years.


== In political and popular culture ==
Twelve-term US Congressman and three-time presidential candidate Ron Paul wrote a book entitled Swords into Plowshares: A Life in Wartime and a Future of Peace and Prosperity, in which he discusses growing up during World War II and living his life through war after war.
In his farewell address, U.S. President Dwight Eisenhower, when speaking about the military–industrial complex, stated:Until the latest of our world conflicts, the United States had no armaments industry. American makers of plowshares could, with time and as required, make swords as well. But now we can no longer risk emergency improvisation of national defense; we have been compelled to create a permanent armaments industry of vast proportions. Added to this, three and a half million men and women are directly engaged in the defense establishment. We annually spend on military security more than the net income of all United States corporations.
For his first and second inaugurations, U.S. President Richard Nixon took the oath of office with his hand on two family Bibles, opened to Isaiah 2:2–4.
In their speeches at the signing of the 1979 Egypt–Israel Peace Treaty, Jimmy Carter, Anwar Sadat, and Menachem Begin all referenced the saying in calling for peace.
In Ronald Reagan's address to the 42nd Session of the United Nations General Assembly in New York, New York.Cannot swords be turned to plowshares? Can we and all nations not live in peace? In our obsession with antagonisms of the moment, we often forget how much unites all the members of humanity. Perhaps we need some outside, universal threat to make us recognize this common bond. I occasionally think how quickly our differences world-wide would vanish if we were facing an alien threat from outside this world. And yet, I ask you, is not an alien force already among us? What could be more alien than war and the threat of war?
The popular anti-war song "The Vine and Fig Tree" repeats the verse
The song "The End of the Innocence" by Don Henley (1989) uses the Joel inverted version of the phrase:
"Heal the World" by Michael Jackson (1991):
Finale of the musical Les Misérables:
A poem by Israeli poet Yehuda Amichai:


== See also ==
Anti-war movement
Atomic gardening
Guns vs butter
Operation Plowshare
Plowshares movement


== References ==