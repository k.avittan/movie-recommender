"Monster Mash" is a 1962 novelty song by Bobby "Boris" Pickett. The song was released as a single on Gary S. Paxton's Garpax Records label in August 1962 along with a full-length LP called The Original Monster Mash, which contained several other monster-themed tunes. The "Monster Mash" single was number one on the Billboard Hot 100 chart on October 20–27 of that year, just before Halloween. It has been a perennial Halloween favorite ever since.


== Background ==
Pickett was an aspiring actor who sang with a band called the Cordials at night, while going to auditions during the day. One night, while performing with his band, Pickett did a monologue in imitation of horror movie actor Boris Karloff while performing the Diamonds' "Little Darlin'". The audience loved it, and fellow band member Lenny Capizzi encouraged Pickett to do more with the Karloff imitation.Pickett and Capizzi composed "Monster Mash" and recorded it with Gary S. Paxton, pianist Leon Russell, Johnny MacRae, Rickie Page, and Terry Berg, credited as "The Crypt-Kickers". (Mel Taylor, drummer for the Ventures, is sometimes credited with playing on the record as well, while Russell, who arrived late for the session, appears on the single's B-side, "Monster Mash Party".) The song was partially inspired by Paxton's earlier novelty hit "Alley Oop", as well as by the Mashed Potato dance craze of the era. A variation on the Mashed Potato was danced to "Monster Mash", in which the footwork was the same but Frankenstein-style monster gestures were made with the arms and hands.
The producers made extensive use of sound effects in the recording; the sound of a coffin opening was imitated by a rusty nail being pulled out of a board. The sound of a cauldron bubbling was simulated by water being bubbled through a straw, and the chains rattling were simply chains being dropped on a tile floor.


== Synopsis ==
The song is narrated by a mad scientist whose monster, late one evening, rises from his lab to perform a new dance, with a name implying it is inspired by the Mashed Potato, a popular dance of the early 1960s. The dance becomes "the hit of the land" when the scientist throws a party for other monsters, among them classic 1940s horror film icons such as the Wolfman, Igor, Count Dracula and his son. In addition to narrating the song in the Karloff voice, Pickett also impersonated fellow horror film actor Bela Lugosi as Count Dracula with the line, "Whatever happened to my Transylvania Twist?", and character actor Peter Lorre as Igor (a role Lorre never played, though he did play numerous deformed eccentrics). The mad scientist explains that the twist has been replaced by the Monster Mash, which Dracula embraces by joining the house band, the Crypt-Kicker Five. The story closes with the mad scientist inviting "you, the living" to the party at his castle.


== Re-releases and other versions ==
The song was re-released several times and appeared on the U.S. Billboard charts on two occasions after the original release: August 1970 and May 1973. The BBC had banned the record from airplay in 1962 on the grounds that the song was "too morbid". It was re-released in the United Kingdom in 1973, where it peaked at number three in early October. In Canada, it reached number one on August 4, 1973. In the U.S., the record re-entered the Hot 100 on May 5, 1973, peaking at number ten on August 11. On the edition of September 15, 1973 of American Top 40, Casey Kasem mistakenly said that the record had accumulated 40 weeks on the Hot 100, which then would have been the all-time record, only for a listener to inform Kasem later that the record's three weeks on the Hot 100 in 1970 had been included in the 1973 run, thus reducing the total to 37 weeks. The record has not charted on the Hot 100 since then. The 1970 reissue on the Parrot label (Parrot 348) was certified as a Gold record (for sales of over one million copies) on August 28, 1973.
To celebrate the 1973 release, Bobby and the Crypt-Kickers toured Dallas and St. Louis around the 1973 Halloween holiday. On this tour, the Crypt-Kickers were composed of Brian Ray (now guitarist with Paul McCartney), drummer Brian Englund, keyboardist Don Chambers, singer Jean Ray, and others. "Monster Mash" re-entered the British charts again on November 2, 2008 at number 60."Monsters' Holiday", a Christmas-themed follow-up, was recorded by Pickett and released in December 1962, peaking at number 30 on the Billboard chart. The tune was penned by the renowned novelty song composer Paul Harrison.
In 1974, Buck Owens had re-invented "Monsters' Holiday", giving it the title "It's a Monsters' Holiday". That song peaked number six on Billboard in mid-September of the same year, and number nine in Canada.In 1985, with American culture experiencing a growing awareness of rap music, Pickett released "Monster Rap", which describes the mad scientist's frustration at being unable to teach the dancing monster from "Monster Mash" how to talk. The problem is solved when he teaches the monster to rap. A movie musical based on the song starring Pickett was released in 1995. During the 2004 presidential election, Pickett turned the song into a campaign video and re-titled it "Monster Slash", with lyrics by environmental campaigner Peter Altman, in which he critiqued President George W. Bush's environmental policies.
The song is featured in the fifteenth episode of the Simpsons' fourth season, when radio animator Marty accidentally plays it for Valentine's Day. 
In 1989, Stuart Hersh began managing Pickett, and upon learning Pickett did not own the master recording to "Monster Mash", the pair in 1993 recorded a perfect copy of the original "Monster Mash" that they then used to undercut the owners of the track, Universal. According to Hersh, the company was unconcerned with the song's legacy and was instead "preoccupied with how much money they could get off (the song)". Pickett was missing out on thousands of dollars in licensing fees, but Hersh and Pickett's goal was not just to make money. They sought instead to help keep "Monster Mash" relevant: by ensuring "Monster Mash" would be affordable to independent production houses, they ensured it would always have a place in culture.The backing singers for the record were The Blossoms, led by Darlene Love.On the Digital Song Sales chart, the song peaked at position 27 during 2005, 2007 to 2011 and from 2013 to 2017, and is played annually on the Sirius XM Satellite Radio.In 2012, the song ranked at number 25 on Billboard.The "Transylvania Twist", mentioned in the lyrics of "Monster Mash", was written into a full song for the film Spookley the Square Pumpkin.
Comedian Nick Wiger appeared annually on the Comedy Bang! Bang! Halloween podcast as a character named Leo Karpatze who originally wrote Monster Mash but was forced to change the lyrics due to profanity. Wiger would appear on the show and pretend to have a new song to sing but would only sing the same profanity-laced version of Monster Mash.


== Chart history ==


=== Weekly charts ===


== Cover versions ==
"Monster Mash" became a pop music standard, and many cover versions have been recorded over the years.

The 1962 album Monster Mash by American television host and radio personality John Zacherle includes his version of the song "Monster Mash".
The Beach Boys played the song live as an early staple of their live performances, and a live performance was released on their 1964 live album Beach Boys Concert with lead vocals by Mike Love.
The Bonzo Dog Doo-Dah Band released a version on their 1969 album Tadpoles. Their version includes a reference to the Crypt-Kicker 5. They also performed it on an episode of the British TV series Do Not Adjust Your Set.
The 1996 album Test for Echo by the Canadian rock trio Rush includes an instrumental track, "Limbo", which contains several samples from "Monster Mash", including the line "Whatever happened to my Transylvania Twist?"
The 1960s and early 70s group Children Of The Night performed their version of "Monster Mash" on Juke Box Jury.
In 1977 Vincent Price issued a version in the U.K. on EMI Records.
British ska band Bad Manners covered the song for their 1980 debut album, Ska 'n' B.
In 1994, Alvin and the Chipmunks covered the song for their Halloween TV special Trick or Treason.
Blair Packham wrote and performed "One Hit Wonder" in 2004 about the later years of Bobby Pickett's musical career and Pickett's relationship with his most-recognized song.
Allstar Weekend remixed that song in October 8, 2011.
In 1977, the "Monsters" recorded "The Monster Mash" on H&L Records.
In 1988, The Big O recorded a version featured over the end credits for Return of the Living Dead Part II.
Adam Irigoyen, Kenton Duty and Davis Cleveland covered this song for Spooky Buddies.
In 2019, Bootsy Collins and Buckethead released their "Bootsy & Buckethead Monster Mash" musical video on YouTube.


=== Misfits version ===
Horror punk band the Misfits recorded a cover version of "Monster Mash" in 1997 as part of a promotion surrounding a DVD release of the 1967 stop motion film Mad Monster Party?. Their version was released as a single in 1999, and a new version was recorded for their 2003 album Project 1950.


==== Background ====
The Misfits recorded their version of "Monster Mash" in 1997 in a recording studio in Newark, New Jersey. Bassist Jerry Only later stated that "The 'Monster Mash' was a no-brainer for the Misfits to cover as a timeless Halloween release. The song was always a childhood favorite of mine." The recording was the first time that Only had performed lead vocals for the band, while then-Misfits lead singer Michale Graves provided backing vocals. On October 18, 1997 a live performance of the song was recorded and broadcast from the studio of the New Jersey-based cable television music show Power Play, the same studio in which the song was recorded.The Misfits' version of "Monster Mash" was used in a cross-promotion campaign arranged by Deluxo and John Cafiero, who had directed the Misfits' music videos for "American Psycho" and "Dig Up Her Bones", to create awareness of a home video release of the 1967 Rankin/Bass stop motion film Mad Monster Party?. Deluxo had recently acquired the rights to the film in hopes of restoring it, but could not find a quality print. They eventually found a quality 16 mm print and the Misfits hosted a special screening of the film at Anthology Film Archives in New York City, with the "American Psycho" music video as an opening featurette. Notable attendees at the screening included members of the Misfits, Cafiero, Marky Ramone, Jimmy Gestapo of Murphy's Law, and executives from both Geffen Records and Roadrunner Records. Deluxo restored Mad Monster Party? from the 16 mm print and re-released it on VHS in limited numbers. A music video for the Misfits' live recording of "Monster Mash" was created interspersing clips from the film with footage from their "Power Play" performance, and was intended to be included as a bonus feature on a forthcoming DVD release. However, due to complications over distribution rights, the DVD was not released and the VHS version was discontinued. A DVD version of the film was eventually released in 2003 by Anchor Bay Entertainment, restored from a different 35 mm print.
As part of the promotional campaign, an image of Boris Karloff as he appears in Mad Monster Party? was used under license from his daughter Sarah Karloff in artwork for a 7" vinyl single of the Misfits' version of "Monster Mash". Percepto Records used the same image of Karloff for the cover of the film's soundtrack album which was released in September 1998. The Misfits single was intended to be released the following month, but was delayed due to lack of time to properly mix the studio recording. Instead the artwork was used in connection with a free MP3 download of the band's live recording of "Monster Mash", available through their website as a "virtual single". The image was used again as cover artwork when the finished single was released physically in October 1999 as the first release of the band's new label Misfits Records.The Misfits' studio recording of "Monster Mash" was included on their 2001 compilation album Cuts from the Crypt. At the insistence of the band's label Roadrunner Records, the live recording was omitted from the album and only the studio version was used. The Misfits recorded a new version of the song, featuring Cafiero on backing vocals, for their 2003 album Project 1950. By this time the band had a new lineup consisting of Jerry Only (bass guitar and lead vocals), Dez Cadena (guitar and backing vocals), and Marky Ramone (drums). Only called this version of the song "probably the best version we've ever done. I'm really happy with it. We've recorded it several times in the past and this is the hardest hitting version of them all." Ramone commented: "'Monster Mash' always reminded me of Boris Karloff. Our version's a lot faster than the original, which was a slower 4/4 beat. I always liked the way the drums seemed to come in out of nowhere after Igor walked across the room." Cadena, meanwhile, remarked that "If I didn't know any better ['Monster Mash'] could have been written for the Misfits."


==== Release ====
Two separate pressings of the Misfits single were released, both issued on 7" vinyl in 1999. The first pressing had the studio version on both sides and consisted of 1,000 copies: 800 on green vinyl, 100 on red, and 100 on gold. It was sold by the band while on tour and also by mail order through their "Fiend Club" fan club and official website. 1,000 promotional CD copies of the studio version were also pressed and used for limited solicitation to radio stations. A second pressing of the vinyl single had the studio version on side A and the live version on side B and consisted of 1,000 copies on glow-in-the-dark vinyl, available exclusively by mail order through the band's website. The single sold out and is no longer in print.


==== Personnel ====
Jerry Only – bass guitar, lead vocals
Michale Graves – backing vocals
Doyle Wolfgang von Frankenstein – guitar, backing vocals
Dr. Chud – drums


== Certifications ==


== See also ==
List of 1960s one-hit wonders in the United States


== References ==


== External links ==
Lyrics of this song at MetroLyrics