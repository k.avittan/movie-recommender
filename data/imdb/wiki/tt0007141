Officer 666 is a 1916 silent film made in Australia, based on a successful Broadway comedy of 1912. The film was directed by Fred Niblo who would go on to direct The Mark of Zorro, The Three Musketeers,  Blood and Sand and over forty more films.


== Plot ==
In New York, millionaire Travers Gladwin (Fred Niblo) learns of a plot to steal his collection of paintings by some art criminals led by Alfred Wilson (Sydney Stirling). Gladwin pretends to leave to go on holiday, but actually remains in the area disguised as a policeman. Wilson and his gang arrive, with Wilson posing as Gladwin, and a young lady as his fiancee. Gladwin tries to stop the robbery but eventually lets Wilson go in order to save the honour of the young lady.


== Cast ==
Fred Niblo as Travers Gladwin
Enid Bennett as Helen Burton
Marion Marcus Clarke as Mrs Burton
Sydney Stirling as Alfred Wilson
Maurice Dudley as Watkins
Henry Matsumoto as Bateato
Pirie Bush as Whitney Barnes
Edwin Lester as Detective Kearney
George Bryant as Captain Stone
Matee Brown as Sadie
Reine Connelly as Celeste


== Original play ==
The film is an adaptation of Augustin MacHugh's 1912 comedy play which was produced on Broadway by George M. Cohan and Sam Harris, then toured the United States during 1912 and 1913. Niblo toured with the play in Australia in 1912-13. The play was based on the 1912 novel by Barton Currie and Augustin MacHugh.
The play was also filmed in 1914 and 1920.


== Production ==
The movie was shot shortly after Get-Rich-Quick Wallingford (1916) and was completed shortly before Niblo returned to America in June 1915. It was mostly shot at J. C. Williamson's film studio in Melbourne with some location work.


== Reception ==
Although completed by late May 1915 the film was not released until nearly a year later. It was not a success at the box office.The Sunday Times called it "a happy piece of farce acting, cleverly photo graphed, and excellent in its sustained fun" and "the most successful [picture] that has been issued from the Williamson studios."The Moving Picture World called it "a very fine comedy, and one of the best produced in this country. Fred Niblo still retains his great personality on the screen."Unlike most Australian silent movies, some of it survives, and the original print is located at the National Screen and Sound Archive in Canberra, Australia.


== Notes ==


== External links ==
Officer 666 on IMDb
Officer 666 at National Film and Sound Archive
Original story at Project Gutenberg
Officer 666 at AustLit