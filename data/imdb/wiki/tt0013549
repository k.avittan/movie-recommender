Réunion (French: La Réunion [la ʁe.y.njɔ̃] (listen); previously Île Bourbon) is an overseas department and region of the French Republic and an Indian Ocean island in East Africa, east of Madagascar and 175 km (109 mi) southwest of Mauritius. As of January 2020, it had a population of 859,959.As in France, the official language is French. In addition, the majority of the region's population speaks Réunion Creole.
Administratively, Réunion is one of the overseas departments of France. Like the other four overseas departments, it is also one of the 18 regions of France, with the modified status of overseas region, and an integral part of the republic with the same status as Metropolitan France. Réunion is an outermost region of the European Union and, as an overseas department of France, part of the eurozone.


== History ==
The island has been inhabited since the 16th century, when people from France and Madagascar settled there. Slavery was abolished on 20 December 1848 (a date celebrated yearly on the island), when the French Second Republic abolished slavery in the French colonies. Indentured workers continued to be brought to Réunion from South India, among other places, however. The island became an overseas department of France in 1946.
Not much is known of Réunion's history prior to the arrival of the Portuguese in the early 16th century. Arab traders were familiar with it by the name Dina Morgabin, "Western Island". The island is possibly featured on a map from 1153 AD by Al Sharif el-Edrisi. The island might also have been visited by Swahili or Austronesian (Ancient Indonesian–Malaysian) sailors on their journey to the west from the Malay Archipelago to Madagascar.The first European discovery of the area was made around 1507 by Portuguese explorer Diogo Fernandes Pereira, but the specifics are unclear. The uninhabited island might have been first sighted by the expedition led by Dom Pedro Mascarenhas, who gave his name to the island group around Réunion, the Mascarenes. Réunion itself was dubbed Santa Apolónia after a favourite saint, which suggests that the date of the Portuguese discovery could have been 9 February, her saint day. Diogo Lopes de Sequeira is said to have landed on the islands of Réunion and Rodrigues in 1509.By the early 1600s, nominal Portuguese rule had left Santa Apolónia virtually untouched. The island was then occupied by France and administered from Port Louis, Mauritius. Although the first French claims date from 1638, when François Cauche and Salomon Goubert visited in June 1638, the island was officially claimed by Jacques Pronis of France in 1642, when he deported a dozen French mutineers to the island from Madagascar. The convicts were returned to France several years later, and in 1649, the island was named Île Bourbon after the French royal House of Bourbon. Colonisation started in 1665, when the French East India Company sent the first settlers.

"Île de la Réunion" was the name given to the island in 1793 by a decree of the Convention Nationale (the elected revolutionary constituent assembly) with the fall of the House of Bourbon in France, and the name commemorates the union of revolutionaries from Marseille with the National Guard in Paris, which took place on 10 August 1792. In 1801, the island was renamed "Île Bonaparte", after First Consul Napoleon Bonaparte. 
During the Napoleonic Wars, the island was invaded by a Royal Navy squadron led by Commodore Josias Rowley in 1810, who used the old name of "Bourbon". When it was restored to France by the Congress of Vienna in 1815, the island retained the name of "Bourbon" until the fall of the restored Bourbons during the French Revolution of 1848, when the island was once again given the name "Île de la Réunion".From the 17th to 19th centuries, French colonisation, supplemented by importing Africans, Chinese and Indians as workers, contributed to ethnic diversity in the population. From 1690, most of the non-Europeans were enslaved. The colony abolished slavery on 20 December 1848. Afterwards, many of the foreign workers came as indentured workers. The opening of the Suez Canal in 1869 reduced the importance of the island as a stopover on the East Indies trade route.

During the Second World War, Réunion was under the authority of the Vichy regime until 30 November 1942, when Free French forces took over the island with the destroyer Léopard.Réunion became a département d'outre-mer (overseas département) of France on 19 March 1946. INSEE assigned to Réunion the department code 974, and the region code 04 when regional councils were created in 1982 in France, including in existing overseas departments which also became overseas regions.
Over about two decades in the late 20th century (1963–1982), 1,630 children from Réunion were relocated to rural areas of metropolitan France, particularly to Creuse, ostensibly for education and work opportunities. That program was led by influential Gaullist politician Michel Debré, who was an MP for Réunion at the time. Many of these children were abused or disadvantaged by the families with whom they were placed. Known as the Children of Creuse, they and their fate came to light in 2002 when one of them, Jean-Jacques Martial, filed suit against the French state for kidnapping and deportation of a minor. Other similar lawsuits were filed over the following years, but all were dismissed by French courts and finally by the European Court of Human Rights in 2011.In 2005 and 2006, Réunion was hit by a crippling epidemic of chikungunya, a disease spread by mosquitoes. According to the BBC News, 255,000 people on Réunion had contracted the disease as of 26 April 2006. The neighbouring islands of Mauritius and Madagascar also suffered epidemics of this disease during the same year. A few cases also appeared in mainland France, carried by people travelling by airline. The French government of Dominique de Villepin sent an emergency aid package worth €36 million and deployed about 500 troops in an effort to eradicate mosquitoes on the island.


== Politics ==

Réunion sends seven deputies to the French National Assembly and three senators to the Senate.


== Administrative divisions ==

Administratively, Réunion is divided into 24 communes (municipalities) grouped into four arrondissements. It is also subdivided into 49 cantons, meaningful only for electoral purposes at the departmental or regional level. It is a French overseas department, hence a French overseas region. The low number of communes, compared with French metropolitan departments of similar size and population, is unique: most of its communes encompass several localities, sometimes separated by significant distances.


=== Municipalities (communes) ===

The communes voluntarily grouped themselves into five intercommunalities for cooperating in some domains, apart from the four arrondissements to which they belong for purposes of applying national laws and executive regulation. After some changes in the composition, name and status of intercommunalities, all of them operate with the status of agglomeration communities, and apply their own local taxation (in addition to national, regional, departmental, and municipal taxes) and have an autonomous budget decided by the assembly representing all member communes. This budget is also partly funded by the state, the region, the department, and the European Union for some development and investment programs. Every commune in Réunion is now a member of an intercommunality with its own taxation, to which member communes have delegated their authority in various areas.


== Foreign relations ==
Although diplomacy, military, and French government matters are handled by Paris, Réunion is a member of La Francophonie, the Indian Ocean Commission, the International Trade Union Confederation, the Universal Postal Union, the Port Management Association of Eastern and Southern Africa, and the World Federation of Trade Unions in its own right.


== Geography ==

 
The island is 63 km (39 mi) long; 45 km (28 mi) wide; and covers 2,512 km2 (970 sq mi). It is above a hotspot in the Earth's crust. The Piton de la Fournaise, a shield volcano on the eastern end of Réunion Island, rises more than 2,631 m (8,632 ft) above sea level and is sometimes called a sister to Hawaiian volcanoes because of the similarity of climate and volcanic nature. It has erupted more than 100 times since 1640, and is under constant monitoring, most recently erupting on 2 April 2020. During another eruption in April 2007, the lava flow was estimated at 3,000,000 m3 (3,900,000 cu yd) per day. The hotspot that fuels Piton de la Fournaise also created the islands of Mauritius and Rodrigues.
The Piton des Neiges volcano, the highest point on the island at 3,070 m (10,070 ft) above sea level, is northwest of the Piton de la Fournaise. Collapsed calderas and canyons are south west of the mountain. While the Piton de la Fournaise is one of Earth's most active volcanoes, the Piton des Neiges is dormant. Its name is French for "peak of snows", but snowfall on the summit of the mountain is rare. The slopes of both volcanoes are heavily forested. Cultivated land and cities like the capital city of Saint-Denis are concentrated on the surrounding coastal lowlands. Offshore, part of the west coast is characterised by a coral reef system. Réunion also has three calderas: the Cirque de Salazie, the Cirque de Cilaos and the Cirque de Mafate. The last is accessible only on foot or by helicopter.

		
		
		
		


=== Climate ===

The climate in Réunion is tropical, but temperature moderates with elevation. The weather is cool and dry from May to November, but hot and rainy from November to April. Precipitation levels vary greatly within the island, with the east being much wetter than the west. More than 6 m of rain a year fall on some parts of the east and less than 1 m a year falls on the west coast. Réunion holds the world records for the most rainfall in 12-, 24-, 72- and 96-hour periods.


=== Beaches ===
Réunion hosts many tropical and unique beaches. They are often equipped with barbecues, amenities, and parking spaces. Hermitage Beach is the most extensive and best-preserved lagoon in Réunion Island and a popular snorkelling location. It is a white sand beach lined with casuarina trees under which the locals often organise picnics. La Plage des Brisants is a well-known surfing spot, with many athletic and leisurely activities taking place. Each November, a film festival is also organised in La Plage des Brisant's. Movies are projected on a large screen in front of a crowd. Beaches at Boucan Canot are surrounded by a stretch of restaurants that particularly cater to tourists. L'Étang-Salé on the west coast is a particularly unique beach as it is covered in black sand consisting of tiny fragments of basalt. This occurs when lava contacts water, it cools rapidly and shatters into the sand and fragmented debris of various size. Much of the debris is small enough to be considered sand. Grand Anse is a tropical white-sand beach lined with coconut trees in the south of Réunion, with a rock pool built for swimmers, a pétanque playground, and a picnic area. Le Vieux Port in Saint Philippe is a green-sand beach consisting of tiny olivine crystals, formed by the 2007 lava flow, making it one of the youngest beaches on Earth.

		
		
		
		


== Environment ==

Since 2010, Réunion is home to a UNESCO World Heritage Site that covers about 40% of the island's area and coincides with the central zone of the Réunion National Park.


=== Wildlife ===

Réunion is home to a variety of birds such as the white-tailed tropicbird (French: paille en queue). Its largest land animal is the panther chameleon, Furcifer pardalis. Much of the west coast is ringed by coral reef which harbours, among other animals, sea urchins, conger eels, and parrot fish. Sea turtles and dolphins also inhabit the coastal waters. Humpback whales migrate north to the island from the Antarctic waters annually during the Southern Hemisphere winter (June–September) to breed and feed, and can be routinely observed from the shores of Réunion during this season. At least 19 species formerly endemic to Réunion have become extinct following human colonisation. For example, the Réunion giant tortoise became extinct after being slaughtered in vast numbers by sailors and settlers of the island.
Between 2010 and 2017, 23 shark attacks occurred in the waters of Réunion, of which nine were fatal. In July 2013, the Prefect of Réunion Michel Lalande announced a ban on swimming, surfing, and bodyboarding off more than half of the coast. Lalande also said 45 bull sharks and 45 tiger sharks would be culled, in addition to the 20 already killed as part of scientific research into the illness ciguatera.Migrations of humpback whales contributed to a boom of whale watching industries on Réunion, and watching rules have been governed by the OMAR (Observatoire Marin de la Réunion) and Globice (Groupe local d'observation et d'identification des cétacés).

		
		
		
		
		


=== Gardening and Bourbon roses ===
The first members of the "Bourbon" group of garden roses originated on this island (then still Île Bourbon, hence the name) from a spontaneous hybridisation between Damask roses and Rosa chinensis, which had been brought there by the colonists. The first Bourbon roses were discovered on the island in 1817.


== Demographics ==

Ethnic groups present include people of African, Indian, European, Malagasy and Chinese origin. Local names for these are Yabs, Cafres, Malbars and Chinois. All of the ethnic groups comprising the island are immigrant populations that have come to Réunion from Europe, Asia and Africa over the centuries. There are no indigenous people on the island, as it was originally deserted. These populations have mixed from the earliest days of the island's colonial history (the first settlers married women from Madagascar and of Indo-Portuguese heritage), resulting in a majority population of mixed race and of "Creole" culture.

How many people of each ethnicity live in Réunion is not known exactly, since the French census does not ask questions about ethnic origin, which applies in Réunion because it is a part of the 1958 constitution, and also because of the extent of racial mixing on the island. According to estimates, Whites make up roughly one quarter of the population, Malbars make up more than 25% of the population and people of Chinese ancestry form roughly 3%. The percentages for mixed race people and those of Afro-Malagasy origins vary widely in estimates. Also, some people of Vietnamese ancestry live on the island, though they are very few in number.Tamils are the largest group among the Indian community. The island's community of Muslims from north western India, particularly Gujarat, and elsewhere is commonly referred to as zarabes.
Creoles (a name given to those born on the island, regardless of ethnic origins) make up the majority of the population. Groups that are not Creole include people recently arrived from Metropolitan France (known as zoreilles) and those from Mayotte and the Comoros as well as immigrants from Madagascar and Sri Lankan Tamil refugees.


=== Historical population ===


=== Religion ===

The predominant religion is Christianity, notably Roman Catholicism, with a single (Latin Rite) jurisdiction, the Roman Catholic Diocese of Saint-Denis-de-La Réunion. Religious Intelligence estimates Christians to be 86.9% of the population, followed by Hindus (6.7%) and Muslims (3.2%). Chinese folk religion and Buddhism are also represented, among others.
Most large towns have a Hindu temple and a mosque.


== Culture ==

Réunionese culture is a blend (métissage) of European, African, Indian, Chinese and insular traditions. The most widely spoken language, Réunion Creole, derives from French.


=== Language ===
French is the only official language of Réunion. Although not official, Réunion Creole is the native language of a large part of the population and is spoken alongside French. Creole is used informally and orally in some administration offices whereas the official language of any administration office as well as education is French.Because of the diverse population, other languages are also spoken, including Comorian language varieties (especially Shimaore), Malagasy by recent immigrants from Mayotte and Madagascar, Mandarin, Hakka and Cantonese by members of the Chinese community, but fewer people speak these languages as younger generations start to converse in French and Réunion Creole. There are significant number of speakers of Indian languages, mostly Tamil, Gujarati and Urdu. Arabic is taught in mosques and spoken by a small community of Muslims. Cantonese, Arabic, Tamil is also taught as an optional language in some schools.


=== Music ===

There are two music genres which originated in Réunion: sega, which originated earlier and is also traditional in Mauritius, Rodrigues and Seychelles and maloya, which originated in the 19th century and is only found in Réunion.


=== Sport ===
Moringue is a popular combat/dance sport similar to capoeira.
There are several famous Réunionese sportsmen and women like the handballer Jackson Richardson, as well as the karateka Lucie Ignace.
Professional footballers include Dimitri Payet, Ludovic Ajorque, Florent Sinama Pongolle and Guillaume Hoarau. Laurent Robert and ex-Hibernian and Celtic player Didier Agathe have also featured in movies. Agathe appeared in A Shot at Glory, whilst Robert was in Goal!.
Réunion has a number of contributions to worldwide professional surfing.  It has been home to notable pro surfers including Jeremy Flores, Johanne Defay and Justine Mauvin.  Famous break St Leu has been host to several world surfing championship competitions.
Since 1992, Réunion has hosted a number of ultramarathons under the umbrella name of the Grand Raid. As of 2018, four different races compose the Grand Raid: the Diagonale des Fous, The Trail de Bourbon, the Mascareignes, and the Zembrocal Trail.


== Media ==


=== Broadcasting ===
Réunion has a local public television channel, Réunion 1ère, which now forms part of France Télévision, and also receives France 2, France 3, France 4, France 5 and France 24 from metropolitan France, as well as France Ô, which shows programming from all of the overseas departments and territories. There are also two local private channels, Télé Kréol and Antenne Réunion.
It has a local public radio station, formerly Radio Réunion, but now known as Réunion 1ère, like its television counterpart. It also receives the Radio France networks France Inter, France Musique and France Culture. The first private local radio station, Radio Freedom, was introduced in 1981. They broadcast daily content about weather and local services.


=== Newspapers ===
Two main newspapers:

Journal de l'île de La Réunion
Le Quotidien (Réunionese newspaper)


=== Film ===
Adama (animated there)
Mississippi Mermaid (1969) (filmed there)


=== Blogs ===
Visit Reunion (English language blog and instagram page)
Reunion Island Tourism blog (English/French tourism blog)


== Economy ==

In 2017, the GDP of Réunion at market exchange rates, not at PPP, was estimated at 19.7 billion euros (US$22.3 bn) and the GDP per capita (also at market exchange rates) was 22,900 euros (US$25,900). Economic growth has been at around +3.0% per year in real terms since 2014.Sugar was traditionally the chief agricultural product and export. Tourism is now an important source of income. The island's remote location combined with its stable political alignment with Europe makes it a key location for satellite receiving stations and naval navigation.GDP sector composition in 2013 (contribution of each sector to the total gross value added):
Unemployment is a major problem on Réunion, although the situation has improved markedly since the beginning of the 2000s: the unemployment rate, which stood above 30% from the early 1980s to the early 2000s, declined to 24.6% in 2007, then rebounded to 29.8% in 2011 due to the 2008 global financial crisis and subsequent Great Recession, but declined again after 2011, reaching 22.4% in 2016, its lowest level in 40 years. Since 2016, the unemployment rate has risen again, and reached 24.3% in 2018, nonetheless the fourth lowest yearly figure in 40 years.In 2014, 40% of the population lived below the poverty line (defined by INSEE as 60% of Metropolitan France's median income; in 2014 the poverty line for a family of two parents and two young children was €2,064 (US$2,743) per month).Rum distillation contributes to the island's economy.  A "Product of France", it is shipped to Europe for bottling, then shipped to consumers around the world.


== Public services ==


=== Health ===
In 2005–2006, Réunion experienced an epidemic of chikungunya, a viral disease similar to dengue fever brought in from East Africa, which infected almost a third of the population because of its transmission through mosquitoes. The epidemic has since been eradicated. See the History section for more details.


=== Transport ===

Roland Garros Airport serves the island, handling flights to mainland France, India, Madagascar, Mauritius, Tanzania, Comoros, Seychelles, South Africa, China and Thailand. Pierrefonds Airport, a smaller airport, has some flights to Mauritius and Madagascar. In 2019 a light rail system was proposed to link Le Barachois with the airport.


== See also ==


== References ==


== Bibliography ==
James Rogers and Luis Simón.  The Status and Location of the Military Installations of the Member States of the European Union and Their Potential Role for the European Security and Defence Policy (ESDP). Brussels: European Parliament, 2009.  25 pp.


== External links ==


=== Government ===
Réunion – The severe island – Official French website (in English)
Departmental Council website
Régional council website


=== General information ===
Official tourism website
Réunion at Curlie
 Wikimedia Atlas of Réunion
UNESCO World Heritage Site datasheet