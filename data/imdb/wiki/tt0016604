"The Blue Danube" is the common English title of "An der schönen, blauen Donau", Op. 314 (German for "On the Beautiful Blue Danube"), a waltz by the Austrian composer Johann Strauss II, composed in 1866. Originally performed on 15 February 1867 at a concert of the Wiener Männergesangsverein (Vienna Men's Choral Association), it has been one of the most consistently popular pieces of music in the classical repertoire. Its initial performance was considered only a mild success, however, and Strauss is reputed to have said, "The devil take the waltz, my only regret is for the coda—I wish that had been a success!"After the original music was written, the words were added by the Choral Association's poet, Joseph Weyl. Strauss later added more music, and Weyl needed to change some of the words. Strauss adapted it into a purely orchestral version for the 1867 Paris World's Fair, and it became a great success in this form. The instrumental version is by far the most commonly performed today. An alternate text was written by Franz von Gernerth, "Donau so blau" (Danube so blue). "The Blue Danube" premiered in the United States in its instrumental version on 1 July 1867 in New York, and in the UK in its choral version on 21 September 1867 in London at the promenade concerts at Covent Garden.When Strauss's stepdaughter, Alice von Meyszner-Strauss, asked the composer Johannes Brahms to sign her autograph-fan, he wrote down the first bars of "The Blue Danube", but adding "Leider nicht von Johannes Brahms" ("Unfortunately not by Johannes Brahms").


== Composition notes ==
The work commences with an extended introduction in the key of A major with shimmering (tremolo) violins and a horn spelling out the familiar waltz theme, answered by staccato wind chords, in a subdued mood. It rises briefly into a loud passage but quickly dies down into the same restful nature of the opening bars. A contrasting and quick phrase in D major anticipates the waltz before three quiet downward-moving bass notes "usher in" the first principal waltz melody.
The first waltz theme is a familiar gently rising triad motif played by cellos and horns in the tonic (D major), accompanied by the harp; the Viennese waltz beat is accentuated at the end of each 3-note phrase. The Waltz 1A triumphantly ends its rounds of the motif, and waltz 1B follows in the same key; the genial mood is still apparent.Waltz 2A glides in quietly (still in D major) before a short contrasting middle section in B-flat major. The entire section is repeated.
A more dour waltz 3A is introduced in G major before a fleeting eighth-note melodic phrase (waltz 3B). A loud Intrada (introduction) in Gm is then played. Waltz 4A starts off in a romantic mood (it is in F major) before a more joyous waltz 4B in the same key.
After another short Intrada in A, cadencing in F-sharp minor, sonorous clarinets spell out the poignant melody of waltz 5A in A. Waltz 5B is the climax, punctuated by cymbal crashes. Each of these may be repeated at the discretion of the performer.The coda recalls earlier sections (3A and 2A) before furious chords usher in a recap of the romantic Waltz 4A. The idyll is cut short as the waltz hurries back to the famous waltz theme 1A again. This statement is also cut short, however, by the final codetta: a variation of 1A is presented, connecting to a rushing eighth-note passage in the final few bars: repeated tonic chords underlined by a snare drum roll and a bright-sounding flourish.
A typical performance lasts around 10 minutes, with the seven-minute main piece, followed by a three-minute coda.


== Instrumentation ==
The Blue Danube is scored for the following orchestra: 


== Choral version ==
The "Beautiful Blue Danube" was first written as a song for a carnival choir (for bass and tenor), with rather satirical lyrics (Austria having just lost a war with Prussia). The original title was also referring to a poem about the Danube in the poet Karl Isidor Beck's hometown, Baja in Hungary, and not in Vienna. Later Franz von Gernerth wrote new, more "official-sounding" lyrics:


== In popular culture ==
The specifically Viennese sentiment associated with Strauss's melody has made it an unofficial Austrian national anthem.The piece was prominently used in Stanley Kubrick's 1968 film 2001: A Space Odyssey. After a leap from humanity's prehistoric past to its spacefaring future, the first two-thirds of The Blue Danube are heard as a space plane approaches and docks with a space station; it concludes while another spacecraft travels from the station to the Moon. The piece is then reprised over the film's closing credits.Moose Charlap and Chuck Sweeney wrote a popular song with lyrics, named "How Blue", based on "The Blue Danube", recorded by The Mills Brothers in 1954.


== References ==


=== Sources ===
Lloyd, Norman, The Golden Encyclopedia of Music, New York: Golden Press, a division of Western Publishing, Inc., 1968.
Jeroen H.C. Tempelman, "By the Beautiful Blue Danube in New York", Vienna Music, no. 101 (Winter 2012), pp. 28–31


== External links ==
The Blue Danube: Scores at the International Music Score Library Project
Sheet music for "On the Beautiful Blue Danube", John Church Company, 1868.