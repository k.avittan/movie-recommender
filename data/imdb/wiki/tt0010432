Harvest of Shame was a 1960 television documentary presented by broadcast journalist Edward R. Murrow on CBS that showed the plight of American migrant agricultural workers. It was Murrow's final documentary for the network; he left CBS at the end of January 1961, at John F. Kennedy's request, to become head of the United States Information Agency. An investigative report intended "to shock Americans into action," it was "the first time millions of Americans were given a close look at what it means to live in poverty" by their televisions.The program was an installment of the television documentary series CBS Reports, widely seen as the successor to Murrow's highly regarded 1951–1958 CBS program See It Now. Murrow's close associate, Fred W. Friendly, who coproduced See It Now, was the executive producer of CBS Reports. Their colleague, Edward P. Morgan, had taken up the issue of migrant labor in his CBS Radio Network commentaries. Morgan's assistant had visited Senator Harry F. Byrd's Northern Virginia farm during the apple harvest and was outraged by the conditions of the migrant laborers working there. According to Murrow biographer Joseph Persico, Friendly decided that the issue was a natural for Murrow, long seen as a champion of the oppressed.While Murrow and Friendly are often seen as the forces behind the show, broadcast historians such as the late Edward Bliss, Jr. have also given credit to "Harvest of Shame" producer/reporter David Lowe. Lowe did much of the legwork, including a number of the interviews featured on the installment.
The program originally aired just after Thanksgiving Day in November 1960. The December 5, 1960 edition of  Time quoted producer Lowe as saying,

We felt that by scheduling the program the day after Thanksgiving, we could stress the fact that much of the food cooked for Thanksgiving throughout the country was picked by migratory workers. We hoped that the pictures of how these people live and work would shock the consciousness of the nation.


== Summary ==
The opening is voiced over footage of migrant workers, including a number of African-Americans, being recruited:

This scene is not taking place in the Congo. It has nothing to do with Johannesburg or Cape Town. It is not Nyasaland or Nigeria. This is Florida. These are citizens of the United States, 1960. This is a shape-up for migrant workers. The hawkers are chanting the going piece rate at the various fields. This is the way the humans who harvest the food for the best-fed people in the world get hired. One farmer looked at this and said, "We used to own our slaves; now we just rent them."
Murrow describes the story as an American story, which moves from Florida to New Jersey. Murrow calls it a 1960s Grapes of Wrath of unrepresented people, who work 136 days of the year and make $900 a year. Throughout the documentary are numerous interviews, conducted by David Lowe in his nine months of field reporting, of people working in the industry.
The report starts in Florida. There, Mrs. Doby, the mother of nine children, describes picking strawberries and cherries. The children work with her except for her baby. Her average dinner consists of a pot of beans or corn. She cannot afford milk more than once a week for her children.
Jerome, a 9-year-old child, is taking care of his three infant sisters while his mother is at work. The family sleeps on a bed with a hole in it because of rats, as Jerome says. The mother cannot afford daycare, which costs $0.85 per day. After working from 6 a.m.
to 4 p.m. she has made only $1.
The people travel on buses for thousands of miles to look for work and sometimes sleep outside during their travels. They often have too little food for dinner. The travels can take up to four days, with rides lasting about ten hours each, without stops for food or facilities.
The story moves from Florida to North Carolina. A camp in Elizabeth City has one source of water for the whole camp and no sanitary facilities. Mrs. Brown, 37, started working in the fields when she was 8. She hopes to get out of the work but does not think that will happen.
Murrow describes the complaints most workers have, such as bad housing, flies, mosquitoes, dirty beds and mattresses, unsanitary toilets, and the lack of hot water for bathing. One employer of hundreds of workers is asked, "Are the [migrant workers] happy people?" He states, "They love it."
The trucks move north from North Carolina to Virginia and Maryland for beans, asparagus, and tomatoes. Every year, there are accidents on the road that often result in injury or death since workers are very compactly packed into the backs of trucks. There is no interstate standard for the transportation of migrant workers.
Murrow then describes the conditions in labor camps. In New Jersey, a labor camp has two outhouses and two water taps. Families live in one room and often sleep in one bed. Lunch is served to children in the camp: a bottle of milk and a couple of crackers. Children of migrant laborers have low levels of literacy, and out of 5000 children of migrant workers, only one finishes high school. The children are anxious to get an education, but they cannot get help from their parents, often illiterate themselves.
While federal legislation is hoped for by the workers, issues include the variability in terms of wages, types of labor, and the transient nature of the lives of migrant workers. For example, the crop does not always sell at the same price because the largest purchasers can dictate the prices from their buying power, which affects how the workers are paid. Farm labor is excluded from federal legislation despite the fact that the laborers travel from state to state every year. In November, the cycle starts all over again, the migrants moving back south.
Murrow's closing words are these:

The migrants have no lobby. Only an enlightened, aroused and perhaps angered public opinion can do anything about the migrants. The people you have seen have the strength to harvest your fruit and vegetables. They do not have the strength to influence legislation. Maybe we do. Good night, and good luck.


== Murrow and National Security Council ==
After Murrow joined the National Security Council as a propagandist, his position led to an embarrassing incident shortly after taking the job when he asked the BBC not to show Harvest of Shame to avoid damaging the European view of the US. The BBC refused, having bought the program in good faith. British newspapers delighted in the irony of the situation, with a Daily Sketch writer saying: "if Murrow builds up America as skillfully as he tore it to pieces last night, the propaganda war is as good as won."


== See also ==


== References ==


== External links ==
Harvest of Shame on YouTube
Harvest of Shame on IMDb
Harvest of Shame at AllMovie