Excellency is an honorific style given to certain high-level officers of a sovereign state, officials of an international organization, or members of an aristocracy. Once entitled to the title "Excellency", the holder usually retains the right to that courtesy throughout their lifetime, although in some cases the title is attached to a particular office, and is held only for the duration of that office.Generally people addressed as Excellency are heads of state, heads of government, governors, ambassadors, Catholic bishops and high ranking ecclesiates and others holding equivalent rank (e.g., heads of international organizations). Members of Royalty generally have distinct addresses (Majesty, Highness, etc.)
It is sometimes misinterpreted as a title of office in itself, but in fact is an honorific that precedes various titles (such as Mr. President, and so on), both in speech and in writing. In reference to such an official, it takes the form His or Her Excellency; in direct address, Your Excellency, or, less formally, simply Excellency
The abbreviation HE is often used instead of His/Her Excellency; alternatively it may stand for His Eminence.


== Government ==


=== Heads of state and government ===
In most republican nation states, the head of state is formally addressed as His or Her Excellency.If a republic has a separate head of government, that official is almost always addressed as Excellency as well. If the nation is a monarchy, however, the customs may vary.  For example, in the case of Australia, all ambassadors, high commissioners, state governors and the governor-general and their spouses are entitled to the use of Excellency.
Governors of colonies in the British Empire were entitled to be addressed as Excellency and this remains the position for the governors of what are now known as British Overseas Territories.


=== International diplomacy ===
In various international organizations, notably the UN and its agencies, Excellency is used as a generic form of address for all republican heads of state and heads of government. It is often granted to the organization's head as well, and to those chiefs of UN diplomatic missions, such as Resident Coordinators (who are the designated representatives of the Secretary-General), who are accredited at the Head of State level (like an Ambassador), or at the lower Head of Government level.
In recent years, some international organizations, such as the Organization for Security and Co-operation in Europe, or the European Union, have designated their permanent representatives in third countries as ambassadors, although they do not represent sovereign entities. This is now largely accepted, and because these ambassadors rank after the UN representative in the orders of precedence of representatives of international organizations, the UN coming naturally first as pre-eminent, the UN Resident Coordinators are now also commonly but informally referred to in diplomatic circles as ambassadors, although the UN itself does not refer to them in this way.


=== International judiciary ===
Judges of the International Court of Justice are also called Your Excellency.


== Monarchy ==


=== Royalty ===
In some monarchies the husbands, wives, or children, of a royal prince or princess, who do not possess a princely title themselves, may be entitled to the style. For example, in Spain spouses or children of a born infante or infanta are addressed as Excellency, if not accorded a higher style.
Also, former members of a royal house or family, who did have a royal title but forfeited it, may be awarded the style afterwards. Examples are former husbands or wives of a royal prince or princess, including Alexandra, Countess of Frederiksborg, following her divorce from Prince Joachim of Denmark. Likewise, Count Carl Johan Bernadotte of Wisborg, who lost his succession rights to the Swedish throne and discontinued use of his royal titles in 1946 when he married the commoner Elin Kerstin Margaretha Wijkmark, was accorded the style.
In some emirates (e.g., Kuwait or Qatar), only the Emir, heir apparent and prime minister are called His Highness. Their children are styled with the lower treatment of His/Her Excellency (unless they possess a higher honorific).


=== Nobility ===
In Spain members of the high nobility, holding the dignity of grandee, are addressed as The Most Excellent Lord/Lady.
In Denmark, some counts (lensgrever), historically those related by blood or marriage to the monarch, who have entered a morganatic marriage or otherwise left the Royal Family have the right to be styled as Your Excellency, e.g., the Counts of Danneskiold-Samsøe, some of the counts of Rosenborg and the Countess of Frederiksborg (ad personam).
In the Sultanate of Sulu, senior nobility and holders of royal offices that are granted the title of Datu Sadja are addressed as His/Her Excellency.


=== Knights ===
Excellency can also attach to a prestigious quality, notably in an order of knighthood. For example, in the Empire of Brazil, it was attached to the highest classes, each time called Grand Cross, of all three imperial orders: Imperial Order of Pedro I, Imperial Order of the Southern Cross (in this case, also enjoying the military honours of a Lieutenant general) and Order of the Rose.
In modern days, Knights Collar and Knights Grand Cross of the Spanish Orders of Chivalry, like the Order of Charles III, Order of Isabella the Catholic, Order of Civil Merit, Order of Alfonso X the Wise, Royal Order of Sports Merit, Civil Order of Health, as well as recipients of the Grand Cross of Military, Naval, and Aeronautical Merit are addressed as such. Furthermore, Knights Grand Cross of the Order of Saint Gregory the Great and the Order of St. Sylvester of the Holy See, Knights of the Order of the Golden Fleece, and Knights Grand Cross of several other orders of high prestige, are often addressed as Excellency.


== Ecclesiastical use ==
By a decree of the Sacred Congregation of Ceremonial of 31 December 1930 the Holy See granted bishops of the Catholic Church the title of Most Reverend Excellency (Latin: Excellentia Reverendissima). In the years following the First World War, the ambassadorial title of Excellency, previously given to nuncios, had already begun to be used by other Catholic bishops. The adjective Most Reverend was intended to distinguish the religious title from that of Excellency given to civil officials.
The instruction Ut sive sollicite of the Holy See's Secretariat of State, dated 28 March 1969, made the addition of Most Reverend optional, sanctioning what had been (except possibly for the beginnings of letters and the like) always the practice.
According to the letter of the decree of 31 December 1930, titular patriarchs too were to be addressed with the title of (Most Reverend) Excellency, but in practice the Holy See continued to address them with the title of Beatitude, which was formally sanctioned for them with the motu proprio Cleri sanctitati of 2 June 1957.
Cardinals, even those who were bishops, continued to use the title of Eminence.
In some English-speaking countries, the honorific of Excellency does not apply to bishops other than the nuncio. In English law, Anglican archbishops are granted the title of Grace (Your Grace, His Grace, as for a duke), and bishops are granted the title of Lordship. The same titles are extended by courtesy to their Catholic counterparts, and continue in use in most countries that are or have been members of the Commonwealth. An exception is former British East Africa (Kenya, Uganda, Tanzania).


== By country ==


=== Albania ===
The President, the Chairman of the Parliament and the Prime Minister are addressed as "His/Her Excellency".


=== Brazil ===
In 1991, the Brazilian Presidential Office issued a composition manual to establish the appropriate usage of the Portuguese language for all government agencies. The manual states that the title of Excelência (Excellency) is the proper form used to address the President and Vice President, all members of Parliament and judges, among other officials.


=== Cambodia ===
In the Kingdom of Cambodia, Deputy Prime Ministers, Senior Ministers, members of the Council of Ministers, Secretary of State, and members of Parliament are addressed as "His/Her Excellency" (Khmer: ឯកឧត្ដម, Aek Oudom).


=== Commonwealth of Nations ===
Within the Commonwealth of Nations, the following officials usually use the style His or Her Excellency:

The Commonwealth Secretary-General;
Presidents of Commonwealth republics;
Governors and governors-general, and the spouses of governors-general;
Commonwealth high commissioners;
Foreign ambassadors;
Foreign dignitaries who are entitled to the style in their own countries.While reference may be made to the Queen's Most Excellent Majesty, the style Excellency is not used with reference to the Queen.


=== Chile ===
The President of Chile and the President of the Chamber of Deputies of Chile are addressed by the style "His/Her Excellency".


=== Germany ===
The President of Germany and Chancellor of Germany are both addressed by the style "Excellency" in international diplomacy, albeit not domestically.


=== India ===
The President of India and governors of Indian states are addressed as Rāshtrapati Mahoday (राष्ट्रपति महोदय/ महोदया,Honourable President) and Rājyapāl Mahoday or (if lady) Rājyapāl Mahodaya (if lady) (राज्यपाल महोदय/ , Honourable Governor) respectively.
His/Her Excellency, a custom dating from the ancient times wherein the Samrāt and Sāmrājñi (सम्राट, साम्राज्ञी/Empror,Empress), Generals, Kings, Ambassadors were addressed. A classic example is addressing Devvrat (Bhishma) as महामहिम भीष्म (His/Your Excellency Bhishma) in Mahabharat.
However the Constitution makers  approved will discontinue "ancient era" styles of Mahāmahim. The same release states that in English (which is the other language in which subsidiary official communications are released in the Central Government of India in its capacity of Sahāyak Rājabhāśhā: Subsidiary Officiating language) the style Honourable shall replace the erstwhile His/Her/Your Excellency. The newer style will be Honourable.
The corresponding changes in releases from the President's Secretariat shall be from Mahāmahim to Rāshtrapatiji. The release also talks about the styles of other dignitaries, like governors. "Hon'ble" will be used before the titles "president" and "governor", while the traditional honorifics Shri or Smt. (Shrimati) should precede the name.
However, "Excellency" will continue to be used, only for interaction of leaders with foreign dignitaries and foreign dignitaries with our leaders as is customary international practice.


=== Ireland ===
The President of Ireland is addressed as Your Excellency or in the Irish language, a Shoilse. Alternatively, one may address the president simply as President or in the Irish language a Uachtaráin.


=== Jordan ===
Like many countries that once formed part of the Ottoman Empire, His/Her Excellency is used as the style for those with the title of Bey or Pasha.  In Arabic the latter titles are often included between the first and last names of the holder, while in English the titles are not usually included and the style of His/Her Excellency is used on its own. Those styled this way include government ministers, senior military officers, and the husbands and children of Princesses.


=== Kenya ===
The President of Kenya is addressed as "His/Her Excellency".
The governors of the counties and diplomats are also addressed as "His/Her Excellency".


=== Malaysia ===
The governors (Yang di-Pertua Negeri) of Melaka, Penang, Sabah, and Sarawak are addressed as "His Excellency" (Tuan Yang Terutama). High commissioners and ambasadors are also addressed as "His/Her Excellency" (Tuan/Puan Yang Terutama).


=== Myanmar ===
The President of Myanmar, First Lady of Myanmar, State Counsellor of Myanmar, Vice-Presidents of Myanmar, Speaker of Pyidaungsu Hluttaw, Speaker of the House of Nationalities, Speaker of the House of Representatives of Myanmar, Governor of the Central Bank of Myanmar, members of the Cabinet of Myanmar, Chief Ministers of States and Regions of Myanmar, mayors and ambassadors are addressed as "His/Her Excellency" while justices of the Supreme Court of Myanmar are addressed as "The Honourable".


=== Nigeria ===
The President of Nigeria and Vice-President of Nigeria shares the style "His/Her Excellency" with the various governors and their deputies of the country's regional states as well as their wives.


=== Pakistan ===
The Prime Minister of Pakistan and the President of Pakistan, are both addressed as "His/Her Excellency".


=== Peru ===
The President of the Republic of Peru is addressed as "His/Her Excellency" (In Spanish: Su Excelencia) if in diplomatic context.


=== Philippines ===
The President (Filipino: Ang Pangulo; Spanish and colloquially: Presidente) is addressed in English as "Your Excellency" and "Sir" or "Ma'am" thereafter, and is referred to "His/Her Excellency", along with the Vice President of the Philippines. The President can also less formally be addressed as "Mister/Madame President", and "Mister/Madame Vice President" for the Vice President In Filipino, the President may be referred to with the more formal title of "Ang Mahál na Pangulo", with "mahál" connoting greatness and high social importance. However, unlike the English honorific, the Vice President is simply addressed "Ang Pangalawang Pangulo" without the honorific "mahal".
The incumbent president, Rodrigo Duterte, has expressed dislike for the traditional title. After assuming office in June 2016, he ordered that the title, along with all honorifics, be dropped from official communications, events, and materials but instead, he be addressed only as "Mayor" since people are already used to calling him as such due to Duterte being the longest-serving Mayor of Davao City and that his cabinet officials only be addressed as "Secretary". Other government officials followed suit by abandoning use of "The Honorable". However, despite the prior unofficial abandonment, the President continues to be addressed as "Excellency" in formal correspondences and petitions, either verbally or written.
All other local and national government officials are styled "The Honorable"; both titles, however, may be glossed in Filipino as Ang Kagalang-galang.


=== Portugal ===
In the Portuguese Republic, the proper style of the President is His/Her Excellency (Portuguese: Sua Excelência).


=== Somalia ===
The President of Somalia is addressed as "His/Her Excellency" or "Jaale". Jaale was also a title used by armed forces staff officers of all branches, especially in the Aden Adde-Shermarke Era, and the Barre Era but has now is rare and has become a title for civil servants and senior government secretaries.


=== South Africa ===
The President of South Africa (and historically the State President of the South African Republic), is (and was) addressed as "His/Her Excellency" if in a formal context.


=== South Korea ===
The President of South Korea is addressed as "His/Her Excellency" if in a formal context both inside and outside of South Korea.


=== Spain ===
Spain uses the title The Most Excellent extensively as a formal address to high officers of the state. The following officials receive the treatment:

The Prime Minister and former prime ministers, current and former deputy prime ministers of the central government, current and former government ministers, current junior ministers (secretaries of State) and the undersecretary of the Foreign Ministry.
Government delegates.
Ambassadors and ministers plenipotentiaries of first and second rank.
Captain generals, generals of the army, admiral generals, generals of the Air, lieutenant generals, admirals, divisional generals, vice-admirals, brigadier generals, and counter admirals.
The president, councillors, and secretary general of the Council of State, as well as the chief counsels of the Council of State.
The president and former presidents of each of the autonomous communities, as well as the sitting councillors (regional ministers).
The president of the Congress of Deputies, the president of the Senate, all members of the Cortes Generales (although the later are usually addressed as "His/Her Lordship" (sp. Su señoría).
The members of the General Council of the Judiciary, the president and the judges of the Spanish Supreme Court, the Spanish Attorney General, and the members of the Spanish Constitutional Court.
The presidents and numeraries of the eight Royal Academies.
The governor of the Bank of Spain.
The presidents of the three Foral Deputation (provincial governments) of the Basque Country, and the president of the Deputation of Barcelona.
The rectors of the Universities (usually addressed as His/Her Excellency and Magnificence).
The Grandees of Spain and holders of civil or military orders of chivalry granted by the King of Spain.


=== Sri Lanka ===
The President of Sri Lanka is addressed as His/Her Excellency. Alternatively, one may address the president simply as Mr. President.


=== Sweden ===
The Swedish language title and forms of address are Hans/Hennes Excellens (His/Her Excellency) and Ers Excellens (Your Excellency).
During most of the 20th century in Sweden, only three officials (other than foreign ambassadors accredited in Sweden and Swedish ambassadors at their post) were granted to the style of Excellency: the Prime Minister, the Minister for Foreign Affairs and the Marshal of the Realm (the highest ranking courtier). They were indeed collectively referred to as "the three excellencies" (Swedish: de tre excellenserna) In the 1970s it fell out of custom in Sweden to address the Prime Minister and the Minister of Foreign Affairs as such, although they continue to be addressed as such in United Nations protocol and in other diplomatic writing.Prior to the 19th century, a Lord of the Realm (Swedish: En af Rikets Herrar) and a member of the Council of the Realm were also entitled to the style as Excellency.


=== Thailand ===
The Prime Minister of Thailand, Deputy Prime Ministers, other cabinet members, governors and ambassadors are addressed as "His/Her Excellency". Meaning in Thai language of His/Her Excellency is phanahūajaothan (พณหัวเจ้าท่าน หรือ ฯพณฯ).


=== Turkey ===
The President of Turkey is addressed as "His/Her Excellency".


=== United States ===
In the United States, the form Excellency was commonly used for George Washington during his service as commander-in-chief of the Continental Army and later when President of the United States, but it began to fall out of use with his successor, and today has been replaced in direct address with the simple Mr. President or the Honorable. Diplomatic correspondence to President Abraham Lincoln during the American Civil War, as during the Trent Affair, for instance, frequently referred to him as His Excellency. In many foreign countries and in United Nations protocol, the President and the Secretary of State are usually referred to as Excellency.
The form Excellency was used for the governors of most of the original Thirteen Colonies, and the practice formally continued after independence.  For example, the term was formerly used in Georgia on the state governor's letterhead, the text of executive orders, any document that requires the governor's signature, and informal settings.  However, in most states the practice fell out of use (or was never introduced) and the title Honorable is now used instead; an exception is in reference to the Governor of Michigan, who is traditionally afforded the courtesy title. Other governors are sometimes addressed as Excellency at public events, though it is less common and the product of custom and courtesy rather than of legislation.
Though the U.S. President and U.S. ambassadors are traditionally accorded the style elsewhere, the U.S. government does not use Excellency for its own chiefs of missions, preferring Honorable instead.


== See also ==
Canadian honorifics
Ecclesiastical titles and styles
His Excellency (opera)
Style (manner of address)


== Notes ==


== References ==