"Streets of Laredo" (Laws B01, Roud 23650), also known as the "Cowboy's Lament", is a famous American cowboy ballad in which a dying cowboy tells his story to another cowboy. Members of the Western Writers of America chose it as one of the Top 100 Western songs of all time.Derived from the traditional folk song "The Unfortunate Rake", the song has become a folk music standard, and as such has been performed, recorded and adapted numerous times, with many variations. The title refers to the city of Laredo, Texas.
The old-time cowboy Frank H. Maynard (1853–1926) of Colorado Springs, Colorado, claimed authorship of the revised Cowboy's Lament, and his story was widely reported in 1924 by the journalism professor Elmo Scott Watson, then on the faculty of the University of Illinois at Urbana-Champaign.


== Lyrics ==


== Origin ==
The song is widely considered to be a traditional ballad. It was first published in 1910 in John Lomax's Cowboy Songs and Other Frontier Ballads.The lyrics appear to be primarily descended from an Irish folk song of the late 18th century called "The Unfortunate Rake", which also evolved (with a time signature change and completely different melody) into the New Orleans standard "St. James Infirmary Blues". The Irish ballad shares a melody with the British sea-song "Spanish Ladies". The Bodleian Library, Oxford, has copies of a 19th-century broadside entitled "The Unfortunate Lad", which is a version of the British ballad. Some elements of this song closely presage those in the "Streets of Laredo" and in the "St. James Infirmary Blues".

As I was a walking down by the “Lock”,
As I was walking one morning of late,
Who did I spy but my own dear comrade,
Wrapp'd in flannel, so hard is his fate.Chorus.Had she but told me when she disordered me,
Had she but told me of it at the time,
I might have got salts and pills of white mercury,
But now I'm cut down in the height of my prime.I boldly stepped up to him and kindly did ask him,
Why he was wrapp'd in flannel so white?
My body is injured and sadly disordered,
All by a young woman, my own heart's delight.My father oft told me, and of[ten] times chided me,
And said my wicked ways would never do,
But I never minded him, nor ever heeded him,
[I] always kept up in my wicked ways.Get six jolly fellows to carry my coffin,
And six pretty maidens to bear up my pall,
And give to each of them bunches of roses,
That they may not smell me as they go along.[Over my coffin put handsful of lavender,
Handsful of lavender on every side,
Bunches of roses all over my coffin,
Saying there goes a young man cut down in his prime.]Muffle your drums, play your pipes merrily,
Play the death [dead] march as you go along.
And fire your guns right over my coffin,
There goes an unfortunate lad to his home.


== Recorded versions ==
Recordings of the song have been made by Vernon Dalhart, Eddy Arnold, Johnny Cash, Johnny Western, Joan Baez, Burl Ives, Jim Reeves, Roy Rogers, Marty Robbins, Chet Atkins, Arlo Guthrie, Norman Luboff Choir, Rex Allen, Willie Nelson, Waylon Jennings and many country and western singers, as well as avant garde rocker John Cale, the British pop group Prefab Sprout, Snakefarm, Mercury Rev, Jane Siberry, Suzanne Vega and Paul Westerberg. There is also a version on RCA's How The West Was Won double album, Bing Crosby  –  1960.  Harry James recorded a version on his 1966 album Harry James & His Western Friends (Dot DLP 3735 and DLP 25735).
The song plays a prominent role in the book and film Bang the Drum Slowly, in which a version of the song is sung. The words from the title replace the words "beat the drum slowly" from the lyrics below. This in turn is the phrase used in the song "Bang the Drum Slowly" on the album Red Dirt Girl by Emmylou Harris. The song is featured in the films Brokeback Mountain and Night on Earth. John Ford used the song in his 1948 movie "Three Godfathers" and was sung by Harry Carey Jr. [IMdb]. The lyrics are also (indirectly) the source of the title of Peter S. Beagle's 1965 travelogue of a cross-USA trip by Heinkel scooter, "I See by My Outfit." It was also sung by the character Al Swearengen in an episode of the TV series Deadwood, by the character Bret Maverick in the episode 'The Belcastle Brand' of the TV series Maverick, and by the character Martin Kellum in the episode 'Song for Dying' of the TV series Gunsmoke. It is also memorably sung by Italian actor/comedian Roberto Benigni in Jim Jarmusch's 1986 breakout movie Down By Law. Roberto Benigni sings "Streets of Laredo" in Jim Jarmusch's 1991 movie, Night on Earth.
Vince Gill recorded a version of three verses of the Irish ballad The Bard of Armagh (which takes the same tune) followed by three verses of this song on the album Long Journey Home, a compilation of songs about Irish emigration and the links between Irish and American folk and country music also featuring Van Morrison, the Chieftains, Mary Black, Elvis Costello and others, in 1998.
The same tune is used for the Irish lament "Bold Robert Emmet" and the sea chanty "Spanish Ladies" .
Louis MacNeice wrote a poem called "The Streets of Laredo" about the bombing of London during World War Two. The rhythms of the poem resemble the lyrics of the song, and the 1948 book Holes in the Sky states that his wife Hedli Anderson sang the poem.
The song is a featured motif in John Irving's 14th Novel 'Avenue of Mysteries'. The good gringo "el gringo bueno" sings the song incessantly, even in his sleep. The band from Circo de La Maravilla plays the song at Lupe's funeral.


== Other versions ==
The Kingston Trio performed this comedy version as "Laredo?" on their 1961 album College Concert:

The Smothers Brothers performed a similar comedy version on their 1962 album The Two Sides of the Smothers Brothers.
Peter S. Beagle's travelogue "I See By My Outfit" takes its name from this version of the song; in the book, he and his friend Phil refer to it as their "theme song."
Allan Sherman also performed a parody of the song; his version was titled "Streets of Miami", and was about vacationing Manhattan lawyers. Garrison Keillor's album Songs of the Cat has a feline-themed parody, "As I Walked Out".
Marty Robbins' 1959 album Gunfighter Ballads and Trail Songs features his hit "El Paso", similar in form and content to "Streets of Laredo".  The 1960 follow-up More Gunfighter Ballads and Trail Songs has a version of the original.
Doc Watson's version, St. James Hospital, combines some of the "cowboy" lyrics with a tune resembling St. James Infirmary and lyrics drawn from that song, and contains the unmistakable "bang the drum slowly" verse.
New Mexican satirist Jim Terr's parody, "Santa Fe Cowboy," "is about the kind of cowboys who wear Gucci hats and spurs by Yves St. Laurent."A portion of "Streets of Laredo" was sung by a group of cowboys in Season 2, Episode 5: Estralita on the TV show Wanted Dead or Alive which first aired on 10/3/1959.The lyrics of Pete Seeger's "Ballad of Sherman Wu" are patterned after "Streets of Laredo'" and is set to the same tune.  The song presages the American Civil Rights Movement and recounts the refusal of Northwestern University's Psi Upsilon fraternity to accept Sherman Wu because of his Chinese heritage. The song deliberately echoes "Streets of Laredo", beginning:

The words of the labor song "The Ballad of Bloody Thursday" – inspired by a deadly clash between strikers and police during the 1934 San Francisco longshoremen's strike – also follow the "Streets of Laredo" pattern and tune.
As for The Cowboy's Lament/Streets of Laredo itself, Austin E. and Alta S. Fife in Songs of the Cowboys (1966) say,

There are hundreds of texts, with variants so numerous that scholars will never assemble and analyze them all.
Note that some versions of printed lyrics, such as Lomax's 1910 version, have been bowdlerized, eliminating, for example, subtle mentions of drunkenness and/or prostitution.  Johnny Cash's 1965 recording substitutes "dram-house" for the traditional "Rosie's," i.e. the saloon for the brothel (though Burl Ives' 1949 recording retains the more logical, "first down to Rosie's, and then to the card-house...").  This bowdlerization renders nonsensical the next phrase, "...and then to the card-house," as though drinking and gambling took place in separate establishments.  One of the Fifes' sources "exaggerating somewhat, says that there were originally seventy stanzas, sixty-nine of which had to be whistled."An intermediately bowdlerized version of "The Cowboy's Lament":


== Derivative musical works ==
Billy Bragg has cited this ballad as the musical inspiration for his version of Woody Guthrie's "The Unwelcome Guest".
"No Man's Land" (sometimes known as "Green Fields of France"), written in 1976 by Eric Bogle, makes use of a similar melody and contains the refrain "did they beat the drums slowly, did they play the fifes lowly".
The song "Streets of the East Village" by The Dan Emery Mystery Band shows a definite influence from this song as well.
The song "Streets of Whitechapel" sung by J. C. Carroll is an updated version of this ballad.
The composer Samuel Barber adapted a variation on the "Streets of Laredo" tune as the principal theme in the "Allegretto" movement of Excursions, op. 20.
The tune was used for The Homing Waltz, a song written by Johnny Reine and Tommie Connor and recorded by Vera Lynn in 1952.
Different words and a chorus were added in 1960 under the title "Only The Heartaches" by Wayne P. Walker, with additional words by Jess Edwins and Terry Kennedy.  It was a minor hit in some countries by Houston Wells and The Marksmen and has been recorded by many other artists. The chorus begins "There's gold in the mountains, gold in the valleys..."
The song "Blackwatertown" by The Handsome Family is another updated version of this song, framing the narrator's downfall as the resultant of an affair with a young woman employed in the publishing industry. It was released on The Rose And The Briar, a 2004 CD compilation and companion to The Rose & the Briar: Death, Love and Liberty in the American Ballad, edited by Sean Wilentz and Greil Marcus.
The 2010 video game Fallout: New Vegas contains a song called "The Streets of New Reno", performed by JE Sawyer. The song is a Fallout universe adaptation of "The Streets of Laredo".
The song "The Streets of Laredo" appears on the albums Sings the Ballads of the True West and American IV by Johnny Cash.  Cash also recorded two other versions with different lyrics on his first Christmas album (1963), and then again as "The Walls of a Prison" on his From Sea to Shining Sea album in 1967.
"When I Was a Young Girl", a female version of the same theme, was popular on the folk music circuits in the late 1950s and early 1960s, and was recorded by Barbara Dane and Odetta before being revived by Nina Simone, Leslie Feist, and Marlon Williams.
In 1995, Judy Collins used the tune of "Streets of Laredo" for the song "Bard of my Heart", about her late son Clark, on her album, Shameless.
The tune and lyrics of "Streets of Laredo" were used in the 1973 film Bang the Drum Slowly, a sports drama based on Mark Harris's novel of the same name.  The movie was directed by John D. Hancock and starred Michael Moriarty and Robert De Niro.  Character actor Vincent Gardenia received an Oscar nomination for Best Supporting Actor for his work in the film.
“Streets of Laredo” is used as the theme music at the beginning of the Coen Brothers’ The Ballad of Buster Scruggs (2018); in its last segment, Brendan Gleeson sings “The Unfortunate Rake”.


== Television & cinema ==
Two verses of this song are sung by a character during a wake in the "Snow White , Blood Red" episode of "Murder , she wrote" (1988-1989)
Lines from the song also feature in the British found-footage horror film The Borderlands (2013 film).
Lines from the song are sung by the Irishman, played by Brendan Gleeson, in the western film The Ballad of Buster Scruggs.


== References ==


== External links ==
Walker, Rob, Linkage with St. James Infirmary Blues (historical investigation).
Discussion of origin at Mudcat.org
Episodic literary serial based on the song
cowboypoetry.com
Lithograph of the Unfortunate Lad.
Marcelo Pisarro, "Escuchen mi triste historia/ Hear my sad story", 1975 Main Street, March 2016.