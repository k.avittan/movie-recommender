Lawful Larceny is a 1930 American pre-Code melodramatic film, directed by Lowell Sherman from Jane Murfin's screenplay. The screenplay, a melodrama, was based on the play of the same name by Samuel Shipman, which originally was a comedy. It starred a staple of the early RKO stable, Bebe Daniels, along with Kenneth Thomson, Olive Tell and Lowell Sherman, who reprised the role he had created in the original Broadway play. This film was a remake of the 1923 silent film version of the same name, produced by Famous Players-Lasky Corporation


== Plot ==
When Marion Corsey's husband, Andrew, is conned out of a small fortune by Vivian Hepburn, she dedicates herself to recovering the money. In order to do so, she hides her identity and insinuates herself into the social circle of Vivian, by becoming her secretary, and studies the tactics employed by the sexy con-artist. While employed by Vivian, Marion meets Guy Tarlow, Vivian's love interest. However, Guy seems to be interested Marion.
Taking advantage of Guy's interest, Marion turns the tables on the con-artists and, using Vivian's own strategy, she cons Guy out of all the funds which were taken from Andrew. When her reverse larceny is discovered, Vivian enlists the help of Judge Perry, who is romantically interested in Vivian, in an attempt to recover her ill-gotten gains. In the end, however, Marion is able to prove that Vivian's gambling club is not run honestly, and that Vivian herself is both a cheater and a thief. In light of the evidence, the Judge and Guy end their pursuit of Marion, and Vivian slinks away. Although their marriage is damaged, and may be over, Marion and Andrew decide to stay together as friends, and see how things work out.


== Cast ==
Bebe Daniels as Marion Dorsey
Kenneth Thomson as Andrew Dorsey
Lowell Sherman as Guy Tarlow
Olive Tell as Vivian Hepburn
Purnell B. Pratt as Judge Perry
Lou Payne as Davis
Bert Roach as French
Maude Turner Gordon as Mrs. Davis
Helene Millard as Mrs. French
Charles Coleman as Butler(Cast list as per AFI Database)


== Notes ==
This film was a remake of a 1923 silent film of the same name, also based on Samuel Shipman's play, which starred Hope Hampton and Conrad Nagel, and was directed by Allan Dwan. Produced by the Famous Players-Lasky Corporation, it was distributed by Paramount Pictures.The play by Samuel Shipman, on which Jane Murfin's screenplay is based, was originally a comedy, and ran at the Theatre Republic (which is still in existence, now known as the New Victory Theater) in 1922. It was directed by Bertram Harrison, produced by A.H. Woods, and starred Margaret Lawrence, Alan Dinehart, and Lowell Sherman.Olive Tell, cast in the less than sympathetic role of the sexy con-artist, was the wife of associate producer Henry Hobart.In 1958, the film entered the public domain in the United States because the claimants did not renew its copyright registration in the 28th year after publication.


== References ==


== External links ==
Lawful Larceny on IMDb