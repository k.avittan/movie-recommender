A bob cut, also known as a bob or dog-ear cut, is a short- to medium-length haircut, in which the hair is typically cut straight around the head at about jaw-level, often with a fringe (or "bangs") at the front. The standard bob is normally cut either between or just below the tips of the ears, and well above the shoulders.


== History ==

Historically, women in the West have usually worn their hair long.  Although young girls, actresses and a few "advanced" or fashionable women had worn short hair even before World War I—for example in 1910 the French actress Polaire is described as having "a shock of short, dark hair", a cut she adopted in the early 1890s—the style was not considered generally respectable until given impetus by the inconvenience of long hair to girls engaged in war work.In 1909, Antoni Cierplikowski, called Antoine de Paris, Polish hairdresser who became the world's first celebrity hairdresser, started a fashion for a short bob cut, which was inspired by Joan of Arc. In the 1920s, he introduced the shingle cut which became popular with daring young women — the Bloomsbury set and flappers. Among his clients were world-famous female personalities like Coco Chanel, Queen Marie of Romania, Sarah Bernhardt, Greta Garbo, U.S. First Lady Eleanor Roosevelt and Brigitte Bardot. 
English society beauty Lady Diana Cooper, who had bobbed hair as a child, kept the style through her teenage years and continued in 1914 as an adult.  Renowned dancer and fashion trendsetter Irene Castle introduced her "Castle bob" to a receptive American audience in 1915, and by 1920 the style was rapidly becoming fashionable. Popularized by film star Mary Thurman in the early 1920s and by Colleen Moore and Louise Brooks in the mid to late 1920s, it was still seen as a somewhat shocking statement of independence in young women known as flappers, as older people were used to seeing girls wearing long dresses and heavy Edwardian-style hair. Hairdressers, whose training was mainly in arranging and curling long hair, were slow to realise that short styles for women had arrived to stay, and so barbers in many cities found lines of women outside their shops, waiting to be shorn of hair that had taken many years to grow.

Although as early as 1922 the fashion correspondent of The Times was suggesting that bobbed hair was passé, by the mid-1920s the style (in various versions, often worn with a side-parting, curled or waved, and with the hair at the nape of the neck "shingled" short), was the dominant female hairstyle in the Western world.  The style was spreading even beyond the West, as women who rejected traditional roles adopted the bob cut as a sign of modernity. Close-fitting cloche hats had also become very popular, and couldn't be worn with long hair. Well-known bob-wearers were actresses Clara Bow and Joan Crawford, as well as Dutch film star Truus van Aalten.
As the 1930s approached, women started to grow their hair longer, and the sharp lines of the bob were abandoned.


=== 1960s and beyond ===

In the mid 1960s, Vidal Sassoon made it popular again, using the shape of the early bob and making it more stylish in a simpler cut. Its resurgence coincided with the arrival of the "mop top" Beatle cut for men. Those associated with the bob at that time included the fashion designers Mary Quant and Jean Muir, actresses Nancy Kwan, Carolyn Jones, Barbara Feldon and Amanda Barrie, and singers as diverse as Keely Smith, Cilla Black, Billie Davis, Juliette Gréco, Mireille Mathieu and Beverly Bivens of the American group We Five.  It was also popular with African-Americans in the mid-to-late 1960s, reflected in groups including Diana Ross & The Supremes and The Marvelettes.
Many styles and combinations of the "bob" have evolved since. In the late 1980s, Siouxsie Sioux, lead singer of Siouxsie and the Banshees, and Corinne Drewery, singer of "Swing Out Sister", had bob cuts for a short time. Singer Linda Ronstadt sported a very "Louise Brooks" inspired bob on the cover of two Grammy award winning albums in the late 1980s. 1987's Trio album with Dolly Parton and Emmylou Harris and her 1989 release Cry Like A Rainstorm, Howl Like The Wind. She also wears the cut in the video for her duet with James Ingram, "Somewhere Out There". Anna Wintour, editor-in-chief of American Vogue since 1988, apparently had hers trimmed every day (Times 2, 10 July 2006). In the early 1990s Cyndi Lauper had a bob haircut with very unusual colors; soon afterwards, the cut became identified with Uma Thurman's character of Mia Wallace in Quentin Tarantino's 1994 film Pulp Fiction. In the mid to late 1990s, T-Boz of TLC also had a bob haircut with very unusual colors that was asymmetrical with bangs.  Also, for the first two seasons and the first two episodes of the third season of Lois and Clark: The New Adventures of Superman, the character of Lois Lane (Teri Hatcher) had a trademark bob haircut.  Also, in Barry Sonnenfeld's 1997 film Men in Black, the character of Dr. Laurel Weaver (Linda Fiorentino) also sported a bob.


=== 2000s revival ===

In 2006, the bob was adopted by the singer Madonna and, as a move away from boho-chic, by actress Sienna Miller.In November 2005, Canadian ice dancer Kristina Lenko was asked to join ITV1's new series, Dancing on Ice. She went to her stylist in Toronto and told him "Do whatever you like." He cut Lenko's waist length hair into what is referred to as an A-line bob, where the hairs shorter in the back and gradually longer toward the front, with the longest pieces toward the front of the face. Later, ex–Spice Girl Victoria Beckham decided to cut her own hair into such a style, helping to raise its popularity worldwide with girls asking hairdressers for a "Pob"—Beckham's nickname Posh Spice conflated with "bob".
In 2007, R&B singer Rihanna had a bob haircut in the video for "Umbrella". She has stated that she got her inspiration from Charlize Theron in Æon Flux. 
Keira Knightley had a bob in her short TV ad for Coco Mademoiselle. Actress Christina Ricci also had a bob for live-action movie version for 60s anime series Speed Racer and later onwards. Katie Holmes got a bob cut with bangs in 2007.
At her third show in Brisbane, Australia, Britney Spears wore the bob throughout her concert. Jenny McCarthy is known for a sporting an A-line bob. Kate Bosworth is said to have popularized the bob in 2008. Shoulder-length bobs became popular after being sported by stars such as Heidi Klum and Jessica Alba. A shaggy version of the bob was popularized by Dianna Agron and Rooney Mara.


== Types ==

		
		
		
A-line bob: A typical bob cut, with slightly longer hair in front that frames the face, typically curling under the chin.
Buzz-cut bob: Where it is shoulder-length in the front and close-cropped at the back.
Chin-length bob: Cut straight to the chin, with or without bangs.
Inverted bob: Similar to an A-line bob, but with stacked layers in the back. The perimeter of the cut is curved rather than being a straight line. This cut is also commonly called a "graduated bob".
Shaggy bob: A messy bob layered with a razor.
Shingle bob: a cut that is tapered very short in the back, exposing the hairline at the neck. The hair on the sides is formed into a single curl or point on each cheek.
Shoulder-length bob: A blunt bob that reaches the shoulders and has very few layers.


== See also ==
Bobby pin
"Bernice Bobs Her Hair", a short story by F. Scott Fitzgerald on the subject.
Karen (slang), a pejorative that is associated with a stereotypical 21st-century woman with a bob
Pageboy, a similar hairstyle, usually a bit longer than a bob
Lob cut


== References ==


== External links ==
 Media related to Bob haircut at Wikimedia Commons
"History: 25 Hairstyles of the Last 100 Years" - 25: Bob/Finger Wave, 1920s. Retrieved 2011, August 23 at Listverse.com.
"How to Style a Bob" - "Wikihow" - 5 Ways to Style a Bob
 The dictionary definition of bob cut at Wiktionary