The Dogs of War is a 1980 American war film based upon the 1974 novel of the same name by Frederick Forsyth. Largely filmed in Belize, it was directed by John Irvin and starred Christopher Walken and Tom Berenger. In it a small mercenary unit of soldiers is privately hired to depose the president of a fictional African country so that a British tycoon can gain access to a platinum deposit.
The title is based on a phrase from William Shakespeare's play Julius Caesar: "Cry, 'Havoc!', and let slip the dogs of war."


== Plot ==
Having escaped from Central America, mercenaries James Shannon, Drew, Derek, Michel, Terry and Richard, get an offer from a British businessman, Roy Endean. He is interested in "certain resources" in the small African nation of Zangaro, which is run by the brutal dictator, General Kimba.
Shannon goes on a reconnaissance mission to Zangaro's capital of Clarence and meets a British documentary filmmaker named North. However, Shannon's activities arouse the suspicions of the police and he is arrested, severely beaten and imprisoned. His wounds are treated by Dr. Okoye, a physician who was formerly a moderate political leader. North agitates for Shannon's release and he is deported after two days of torture.
When Shannon tells Endean that there is no chance of an internal coup, Endean offers him $100,000 to overthrow Kimba by invading Zangaro with a mercenary army. Endean intends to install a puppet government led by Colonel Bobi, Kimba's greedy former ally. This would allow Endean to exploit the country's newly discovered platinum resources, an agreement guaranteed by Colonel Bobi. Shannon refuses the offer and instead proposes to his estranged wife that they start a new life in America. When she turns him down, he accepts Endean's offer on condition that he will have complete control of the operation.
Provided with a million dollars for expenses, Shannon contacts some of his associates from Central America and they meet up to plan the coup. The group illegally procures a supply of Uzi submachine guns, ammunition, rocket launchers, mines and other weapons from arms dealers. North encounters Shannon by chance and suspects him of being a CIA agent. Shannon asks Drew to scare North away without hurting him but instead North is killed by a hitman hired by Endean to follow Shannon and his crew. A furious Shannon kills the assassin in turn and leaves his body at Endean's house during a dinner party held for Colonel Bobi.
To transport the team to the coast of Zangaro, Shannon hires a small freighter and crew. At sea, the team is joined by a group of Zangaron exiles trained as soldiers by a former mercenary colleague. Once ashore, the mercenaries use their array of weapons to attack the military garrison where Kimba lives. Drew enters a shack in the barracks' courtyard and is killed by a young woman with a baby who shoots him in the back with a pistol. After the mercenaries storm the burning, bullet-scarred ruins of the garrison, Shannon blasts his way into Kimba's mansion. There he finds the dictator stuffing bricks of bills into a briefcase and kills him.
Endean arrives by helicopter with Colonel Bobi and they enter the presidential residence, where they find Shannon and Dr. Okoye awaiting their overdue arrival. Shannon introduces Dr. Okoye as Zangaro's new president and silences Endean by shooting Bobi when he protests.
Shannon, Derek and Michel load the body of Drew onto a Land Rover in line with the toast they drank on planning the operation, "Everyone Comes Home". The scene finishes with the mercenaries driving through the deserted streets of Clarence.


== Cast ==


== Production ==
United Artists bought rights to the novel in 1974. Don Siegel was going to direct but did not like the screenplay. Abbey Mann wrote a new screenplay. Then Michael Cimino was brought in to buy a screenplay. Norman Jewison became attached as producer-director and Gary Devore rewrote the script. Jewison decided to produce, not direct, and John Irvin became director.The opening Central American scene was filmed at the Miami Glider Port southwest of Miami, Florida. Later African country scenes were filmed in Belize City, Belize, and the surrounding area. The manually-turned swinging bridge shown during the attack is one of the largest of its kind in the world. The film features several weapons that were prominent in popular culture during the 1980s. The Uzi submachine guns used in the film (changed from the German Second World War vintage MP 40s of the novel) were a mix of real Uzis and set-dressed Ingram MAC-10s. Shannon's grenade launcher, depicted in the promotional poster, dubbed the "XM-18" in the film, is a Manville gun – a design later used by the MM-1 grenade launcher.This was only the second international feature for director John Irvin, who previously worked as documentary maker during the Vietnam War. He went on to direct stars such as Arnold Schwarzenegger (Raw Deal), Don Cheadle (Hamburger Hill) and Michael Caine (Shiner).
Cinematographer Jack Cardiff had previously directed an account of mercenaries in Africa entitled Dark of the Sun.  Composer Geoffrey Burgon concludes the film with A. E. Housman's Epitaph for an Army of Mercenaries sung over the end titles.


== Release ==


=== Home media ===
The Dogs of War was released to DVD by MGM Home Video on 20 November 2001 as a Region 1 widescreen DVD and later on by Twilight Time (under license from MGM) as a multiregion widescreen Blu-ray DVD. It is also available to buy on VUDU's (Fandango) streaming platform.


== References ==


== External links ==
The Dogs of War on IMDb
The Dogs of War at Rotten Tomatoes