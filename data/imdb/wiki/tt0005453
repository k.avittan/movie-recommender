When Calls the Heart is a Canadian-American television drama series, inspired by Janette Oke's book of the same name from her Canadian West series, and developed by Michael Landon Jr. The series began airing on the Hallmark Channel in the United States on January 11, 2014, and on April 16, 2014 on Super Channel in Canada.The series originally debuted as a two-hour television movie pilot in October 2013, starring Maggie Grace as young teacher Elizabeth Thatcher and Stephen Amell as Royal Northwest Mounted Police officer Wynn Delaney. In the television series Erin Krakow is cast as her niece, whose name is also Elizabeth Thatcher (played by Poppy Drayton in the movie), and Daniel Lissing plays a Mountie named Jack Thornton, with Lori Loughlin reprising her role as coal mine widow Abigail Stanton.On March 21, 2018, Hallmark renewed the series for a sixth season. The season premiered with a two-hour Christmas special that was broadcast as part of Hallmark's Countdown to Christmas event, and was to continue for a 10-episode run starting in February 2019. However, due to the 2019 college admissions bribery scandal involving Loughlin and her subsequent removal from all Hallmark properties, the season was put on a "retooling" hiatus and resumed in May to conclude in June, with Loughlin's scenes edited out.On April 13, 2019, the series was renewed for a seventh season which aired from February 23 to April 26, 2020.On April 26, 2020, Hallmark Channel announced via a video from Krakow that the series would return for an eighth season in 2021.


== Plot ==
When Calls the Heart tells the story of Elizabeth Thatcher (Erin Krakow), a young teacher accustomed to her high-society life. She receives her first classroom assignment in Coal Valley, a small coal-mining town in Western Canada which is located just south of Robb, Alberta. There, life is simple, but often fraught with challenges. Elizabeth charms most everyone in Coal Valley, except Royal Northwest Mounted Police Constable Jack Thornton (Daniel Lissing). He believes Thatcher's wealthy father has doomed the lawman's career by insisting he be assigned in town to protect the shipping magnate's daughter. Wishing to thrive in this 1910 coal town, Elizabeth must learn the ways of the Canadian frontier.Abigail Stanton's (Lori Loughlin) husband, the foreman of the mine, and her only son—along with 45 other miners, were killed in an explosion, due to the mining-company site manager's irresponsible management and lack of due care in his management of the mine. The newly widowed women find their faith tested when they must go to work in the mine to keep a roof over their heads, food on the table, and help pay the wage for the town's teacher. The town of Coal Valley was renamed Hope Valley in first episode of the second season after the coal mine was closed.


== Episodes ==


== Cast and characters ==


=== Main ===
Erin Krakow as Elizabeth Thatcher Thornton: Elizabeth Thatcher is a passionate young teacher from city life. She found herself in Coal Valley, a small community on the Canadian frontier in need of a teacher, and quickly discovers it is not an easy place to live, lacking the creature comforts and luxuries of her privileged life with her family.
Daniel Lissing as Jack Thornton (seasons 1–5): Jack Thornton is a Mountie and a man of stout character and integrity. He often finds himself protecting and saving people. Upon his first meeting of Elizabeth, Jack developed an instant dislike and it appeared she irritated him. However, as like Elizabeth, he grew to like her and eventually falls in love with her over time. He dies off-screen in a Mountie training camp in Season 5.
Lori Loughlin as Abigail Stanton (seasons 1–6): Abigail Stanton is a Coal Valley woman whose husband and son died in the mining accident. She is a very kind woman, and she stands strongly for the things she believes in. She is one of the first women to welcome Elizabeth and the two women quickly become friends. Abigail's character was written off the show, halfway through season 6, after Loughlin's involvement in the 2019 college admissions bribery scandal, through the idea that Abigail had travelled “back east” to take care of her sick mother.
Jack Wagner as Bill Avery: Bill Avery is a quiet but skilled man who acts as the town's sheriff and becomes the town's judge.
Martin Cummins as Henry Gowen: Henry Gowen is a businessman who owned the late Coal Valley mining company and was arrested for stealing town funds. With the help of Abigail, he is given a second chance and starts his own oiling business in the town.
Pascale Hutton as Rosemary LeVeaux Coulter: Rosemary "Rosie" LeVeaux is a flamboyant, enthusiastic actress from New York. She was briefly engaged to Jack Thornton, but called it off due to their differences. She decides to stay in town, eventually falling in love with the sawmill owner, Lee Coulter.
Kavan Smith as Leland Coulter (season 2-present): Lee Coulter is a kind, laid-back, patient man, who opens a sawmill in Hope Valley.
Andrea Brooks as Faith Carter (recurring season 2–5,7; main season 6): Faith Carter is a nurse who visits from Elizabeth's home city to come work in Hope Valley.
Paul Greene as Carson Shepherd (recurring season 4–5; main season 6-present): Carson Shepherd is a doctor who works in Hope Valley and falls in love with Faith.
Chris McNally as Lucas Bouchard (season 6–present): Lucas Bouchard is a businessman who re-opens the saloon and acts as one of Elizabeth's suitors after Jack dies.
Kevin McGarry as Nathan Grant (season 6–present): Nathan Grant is the new Mountie of Hope Valley and acts as another one of Elizabeth's suitors.
Eva Bourne as Clara Stanton Flynn (recurring season 2–5; main season 6–present): Clara Stanton is Abigail's daughter-in-law who works at the Cafe and marries Jessie Flynn.
Aren Buchholz as Jesse Flynn (recurring season 3–5; main season 6–present): Jesse is a former outlaw, who now lives in Hope Valley.


=== Recurring ===
Charlotte Hegele as Julie Thatcher (seasons 1–2, 5-6)
Erica Carroll as Dottie Ramsey (seasons 1–5)
Chelah Horsdal as Cat Montgomery (season 1)
Laura Bertram as Mary Dunbar (seasons 1–3)
Steve Bacic as Charles Spurlock (season 1)
Mark Humphrey as Frank Hogan (seasons 2–5)
Max Lloyd-Jones as Tom Thornton (seasons 2, 5)
Garwin Sanford as William Thatcher (seasons 2, 5)
Lynda Boyd as Grace Thatcher (season 2)
Marcus Rosner as Charles Kensington (season 2)
Kristina Wagner as Nora Avery (seasons 2–3)
Carter Ryan Evancic as Cody Stanton (seasons 3–6)
Spencer Drever as Cyrus Rivera (season 4)
Jeremy Guilbaut as Ray Wyatt (season 4)


=== Notable guest stars ===
Brooke Shields as Charlotte Thornton (season 3)
Josie Bissett as AJ Foster (seasons 4-5)
Niall Matter as Shane Cantrell (season 4) 
Michael Hogan as Archie Grant (season 7) 
James Brolin as Circuit Judge Jedidiah Black (season 1) 


== Production ==
The series, originally planned to be filmed in Colorado, is filmed south of Vancouver, British Columbia, on a farm surrounded by vineyards. The fictional frontier town of Coal Valley (later Hope Valley) was erected in late 2013. Some of the set trimmings and a stage coach came from the Hell on Wheels set. The Thatcher home is the University Women's Club of Vancouver.The series was renewed for a second season, which aired from April 25 to June 13, 2015. Hallmark Channel announced in July 2015 that the series had been renewed for a third season, which aired from February 21 to April 10, 2016, with a sneak peek airing during the 2015 Christmas season.In mid-2016, it was announced that Season 4 would premiere on the Hallmark Channel Christmas Day with a two-hour special. On April 11, 2016, Lissing and Krakow announced via the series' Facebook page that Hallmark Channel had renewed the series for a fourth season, which aired from February 19 to April 23, 2017.On April 24, 2017, series star Erin Krakow announced via the Hallmark Channel website that the show would return for a fifth season, which premiered in February 2018 and ended in April. Filming for season five began in Vancouver on August 22, 2017, and ended on December 21, 2017.On March 14. 2019, Hallmark announced it had dropped Loughlin from future company projects due to her role in the 2019 college admissions bribery scandal. On April 10, 2019, it was announced that season six would resume on May 5, 2019, with Loughlin's scenes edited out.On April 13, 2019, the series was renewed for a seventh season which premiered on February 23, 2020.On April 26, 2020, Hallmark Channel announced via a video from Krakow that the series would return for an eighth season, which will premiere in 2021.


== Broadcast ==
The first season of the series was subsequently picked up by CBC Television for rebroadcast as a summer series in 2015. The network has since aired all six seasons. The series became available internationally on Netflix in August 2017.


== Spin-off ==
A spin-off of the show, When Hope Calls, was announced at Hallmark's Television Critics Association summer press tour on July 26, 2018. The series debuted on August 30, 2019, as the initial offering of the Hallmark Movies Now streaming service.


== References ==


== External links ==
Official website
When Calls the Heart on IMDb