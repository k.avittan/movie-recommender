Charlotte Mary Brame (usually known as Charlotte M. Brame, last name sometimes mistakenly given as Braeme; appeared under pseudonyms in America, notably Bertha M. Clay, and was sometimes identified by the name of her most famous novel, Dora Thorne) (1 November 1836 – 25 November 1884) was an English novelist.


== Biography ==
She was born in Hinckley, Leicestershire, to Benjamin and Charlotte Agnes Law, devout Roman Catholics. After attending convent schools in Bristol and Preston and a finishing school in Paris, she worked as a governess before marrying Phillip Edward Brame (1839–1886), a London-based jeweller, on 7 January 1863. The couple had nine children, of which four lived to adulthood. Since Brame was a poor businessman and a drunkard, Charlotte found herself forced to support the family with her writing. Her books were very successful with the public, but her earnings were severely diminished by piracy, particularly in the United States. The family lived in London, Manchester, and Brighton before returning to Hinckley, where she died in 1884. She owed money at her death, and her children were taken into guardianship; her husband committed suicide in May 1886.

Her biographer, Gregory Drozdz, writes:Her stories appeared in popular weekly publications such as Bow Bells, the London Reader, and the Family Herald. She was incredibly prolific, writing somewhere in the region of 130 novels during her lifetime. Charlotte Brame's fiction was invariably set in English country houses... Against this milieu, she reworked the theme of love in all of its multifarious aspects—old love, young love, jealousy, suspicion, misalliance, and improvident marriage. High morals such as honour, a sense of duty, and self-sacrifice are lauded as the greatest of virtues. The books also contain strong descriptive passages, some of which are drawn from her associations with Leicestershire... Her literary endeavours, in a male-dominated field, her works of charity, and her personal stamina and resilience, in the face of family tragedy and ill health, represent a triumph in adversity.


== Legacy ==
Following Brame's death, the pen-name "Bertha M Clay" was used by other writers, including her own daughter. Among these were William J. Benners, William Cook, John R. Coryell, Frederick Dacre, Frederick Dey, Charles Garvice, Thomas C. Harbaugh, and Thomas W. Henshaw.


== References ==


== Sources ==
Gregory Drozdz, Charlotte Mary Brame: Hinckley's Forgotten Daughter (G. Drozdz, 1984).
Charlotte M. Brame (1836-1884): Towards a Primary Bibliography (Victorian Secrets, 2012), compiled By Graham Law, Gregory Drozdz and Debby Mcnally.


== Further reading ==
Johannsen, Albert. The House of Beadle & Adams. 3 Vols. Norman, Oklahoma: University of Oklahoma Press, 1950.


== External links ==
Charlotte Mary Brame was Hinckley’s famous writer of over 200 romantic novels
Works by Charlotte M. Brame at Project Gutenberg
Works by or about Charlotte Mary Brame at Internet Archive
Works by or about Bertha M. Clay at Internet Archive
Works by or about Dora Thorne at Internet Archive
Works by Charlotte Mary Brame at LibriVox (public domain audiobooks) 
DNB bio: Gregory Drozdz, "Brame, Charlotte Mary (1836–1884)," Oxford Dictionary of National Biography (Oxford University Press, 2004)
American Women's Dime Novel Project bio
UNCG American Publishers' Trade Bindings: Charlotte M. Brame
Charlotte M. Brame (1836-1884): Towards a Primary Bibliography (Victorian Secrets, 2012), compiled By Graham Law, Gregory Drozdz and Debby Mcnally.