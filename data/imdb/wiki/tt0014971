A potentially hazardous object (PHO) is a near-Earth object – either an asteroid or a comet – with an orbit that can make close approaches to the Earth and large enough to cause significant regional damage in the event of impact. They are defined as having a minimum orbital intersection distance with Earth of less than 0.05 astronomical units (19.5 lunar distances) and an absolute magnitude of 22 or brighter. 98% of the known potentially hazardous objects are not an impact threat over the next 100 years. Only about 38 potentially hazardous objects are listed on the Sentry Risk Table as entries are removed when they become known not to be a threat over the next hundred years or more.Most of these objects are potentially hazardous asteroids (PHAs), and a few are comets. As of September 2020 there are 2,116 known PHAs (about 9% of the total near-Earth population), of which 157 are estimated to be larger than one kilometer in diameter (see list of largest PHAs below). Most of the discovered PHAs are Apollo asteroids (1,730) and fewer belong to the group of Aten asteroids (171).A potentially hazardous object can be known not to be a threat to Earth for the next 100 years or more, if its orbit is reasonably well determined. Potentially hazardous asteroids with some threat of impacting Earth in the next 100 years are listed on the Sentry Risk Table. As of June 2020, only about 38 potentially hazardous asteroids are listed on the Sentry Risk Table. Most potentially hazardous asteroids are ruled out as hazardous to at least several hundreds of years when their competing best orbit models become sufficiently divergent, but recent discoveries whose orbital constraints are little-known have divergent or incomplete mechanical models until observation yields further data. After several astronomical surveys, the number of known PHAs has increased tenfold since the end of the 1990s (see bar charts below). The Minor Planet Center's website List of the Potentially Hazardous Asteroids also publishes detailed information for these objects.


== Overview ==

An object is considered a PHO if its minimum orbit intersection distance (MOID) with respect to Earth is less than 0.05 AU (7,500,000 km; 4,600,000 mi) – approximately 19.5 lunar distances – and its absolute magnitude is brighter than 22, approximately corresponding to a diameter above 140 meters (460 ft). This is big enough to cause regional devastation to human settlements unprecedented in human history in the case of a land impact, or a major tsunami in the case of an ocean impact. Such impact events occur on average around once per 10,000 years. NEOWISE data estimates that there are 4,700 ± 1,500 potentially hazardous asteroids with a diameter greater than 100 meters.


=== Levels of hazard ===

The two main scales used to categorize the impact hazards of asteroids are the Palermo Technical Impact Hazard Scale and the Torino Scale.


=== Potentially hazardous comets ===
Short-period comets currently with an Earth-MOID less than 0.05 AU include: 109P/Swift-Tuttle, 55P/Tempel–Tuttle, 15P/Finlay, 289P/Blanpain, 255P/Levy, 206P/Barnard–Boattini, 21P/Giacobini–Zinner, and 73P/Schwassmann–Wachmann.


=== Numbers ===

In 2012 NASA estimated 20 to 30 percent of these objects have been found. During an asteroid's close approaches to planets or moons other than the Earth, it will be subject to gravitational perturbation, modifying its orbit, and potentially changing a previously non-threatening asteroid into a PHA or vice versa. This is a reflection of the dynamic character of the Solar System.
Several astronomical survey projects such as Lincoln Near-Earth Asteroid Research, Catalina Sky Survey and Pan-STARRS continue to search for more PHOs. Each one found is studied by various means, including optical, radar, and infrared to determine its characteristics, such as size, composition, rotation state, and to more accurately determine its orbit. Both professional and amateur astronomers participate in such observation and tracking.


=== Size ===
Asteroids larger than approximately 35 meters across can pose a threat to a town or city. However the diameter of most small asteroids is not well determined, as it is usually only estimated based on their brightness and distance, rather than directly measured, e.g. from radar observations. For this reason NASA and the Jet Propulsion Laboratory use the more practical measure of absolute magnitude (H). Any asteroid with an absolute magnitude of 22.0 or brighter is assumed to be of the required size.Only a coarse estimation of size can be found from the object's magnitude because an assumption must be made for its albedo which is also not usually known for certain. The NASA near-Earth object program uses an assumed albedo of 0.14 for this purpose. In May 2016, the asteroid size estimates arising from the Wide-field Infrared Survey Explorer and NEOWISE missions have been questioned. Although the early original criticism had not undergone peer review, a more recent peer-reviewed study was subsequently published.


== Largest PHAs ==
With a mean diameter of approximately 7 kilometers, Apollo asteroid (53319) 1999 JM8 is likely the largest known potentially hazardous object, despite its fainter absolute magnitude of 15.2, compared to other listed objects in the table below (note: calculated mean-diameters in table are inferred from the objects brightness and its (assumed) albedo. They are only an approximation.). The lowest numbered PHA is 1566 Icarus.


=== Statistics ===
Below is a list of the largest PHAs (based on absolute magnitude H) discovered in a given year. Historical data of the cumulative number of discovered PHA since 1999 are displayed in the bar charts—one for the total number and the other for objects larger than one kilometer.


== Gallery ==


== See also ==


== Notes ==


== References ==


== External links ==
Sentry: Earth Impact Monitoring (current) (NASA NEO Program)
Very Close Approaches (<0.01 AU) of PHAs to Earth 1900-2200
TECA Table of Asteroids Next Closest Approaches to the Earth, Sormano Astronomical Observatory
MBPL - Minor Body Priority List (PHA Asteroids), Sormano Astronomical Observatory
Responding to the Threat of Potentially-Hazardous Near Earth Objects (PDF)
Risk of comet hitting Earth is greater than previously thought, say researchers, The Guardian, 22 December 2015
Conversion of Absolute Magnitude to Diameter, Minor Planet Center


=== Minor Planet Center ===
List of the Potentially Hazardous Asteroids (PHAs)
Asteroid Hazards, Part 2: The Challenge of Detection on YouTube (min. 7:14)
Asteroid Hazards, Part 3: Finding the Path on YouTube (min. 5:38)