Joseph Medicine Crow (October 27, 1913 – April 3, 2016) was a war chief, author, and historian of the Crow Nation of Native Americans. His writings on Native American history and reservation culture are considered seminal works, but he is best known for his writings and lectures concerning the Battle of the Little Bighorn in 1876. He received the Bronze Star Medal and the Légion d'honneur for service during World War II, and the Presidential Medal of Freedom in 2009.
He was the last surviving war chief of the Crow Nation and the last living Plains Indian war chief. He was a founding member of the Traditional Circle of Indian Elders and Youth.


== Early life ==
Joseph Medicine Crow (his Crow name meant High Bird) was born in 1913 on the Crow Indian Reservation near Lodge Grass, Montana, to Amy Yellowtail and Leo Medicine Crow. As the Crow kinship system was matrilineal, he was considered born for his mother's people, and gained his social status from that line. Property and hereditary positions were passed through the maternal line. Chief Medicine Crow, Leo's father, was a highly distinguished and honored chief in his own right, who at the age of 22 became a war chief. He set a standard for aspiring warriors and was his son's inspiration.

His maternal step-grandfather, White Man Runs Him, was a scout for US General George Armstrong Custer and an eyewitness to the Battle of the Little Bighorn in 1876.  Joe Medicine Crow's cousin is Pauline Small, the first woman elected to office in the Crow Tribe of Indians.


== Education ==
When he was young, Medicine Crow heard direct oral testimony about the Battle of the Little Bighorn in 1876 from his step-grandfather, White Man Runs Him, who had been a scout for General George Armstrong Custer.Beginning in 1929, when he was in eighth grade, Medicine Crow attended Bacone College in Muskogee, Oklahoma, which also had preparatory classes for students of high school age. He studied until he completed an Associate of Arts degree in 1936. He went on to study sociology and psychology for his bachelor's degree from Linfield College in 1938. He earned a master's degree in anthropology from the University of Southern California in Los Angeles in 1939; he was the first member of the Crow tribe to obtain a master's degree. His thesis, The Effects of European Culture Contact upon the Economic, Social, and Religious Life of the Crow Indians, has become a well-respected work about Crow culture. He began work toward a doctorate, and by 1941 had completed the required coursework. He did not complete his Ph.D., due to the United States' entry into World War II.Medicine Crow taught at Chemawa Indian School for a year in 1941, then took a defense industry job in the shipyards of Bremerton, Washington in 1942.


== World War II ==
After spending the latter half of 1942 working in the naval ship yards in Bremerton, Washington, Medicine Crow joined the U.S. Army in 1943. He became a scout in the 103rd Infantry Division, and fought in World War II. Whenever he went into battle, he wore his war paint (two red stripes on his arms) beneath his uniform and a sacred yellow painted eagle feather, provided by a "sundance" medicine man, beneath his helmet.Medicine Crow completed all four tasks required to become a war chief: touching an enemy without killing him (counting coup), taking an enemy's weapon, leading a successful war party, and stealing an enemy's horse. He touched a living enemy soldier and disarmed him after turning a corner and finding himself face to face with a young German soldier:

The collision knocked the German's weapon to the ground. Mr. Crow lowered his own weapon and the two fought hand-to-hand. In the end Mr. Crow got the best of the German, grabbing him by the neck and choking him. He was going to kill the German soldier on the spot when the man screamed out 'mama.' Mr. Crow then let him go.
He also led a successful war party and stole fifty horses owned by the Nazi SS from a German camp, singing a traditional Crow honor song as he rode off.Medicine Crow is the last member of the Crow tribe to become a war chief. He was interviewed and appeared in the 2007 Ken Burns PBS series The War, describing his World War II service. Filmmaker Ken Burns said, "The story of Joseph Medicine Crow is something I've wanted to tell for 20 years."


== Tribal spokesman ==
After serving in the Army, Medicine Crow returned to the Crow Agency. In 1948, he was appointed tribal historian and anthropologist. He worked for the BIA beginning in 1951. He served as a board member or officer on the Crow Central Education Commission almost continuously since its inception in 1972. In 1999, he addressed the United Nations.Medicine Crow was a frequent guest speaker at Little Big Horn College and the Little Big Horn Battlefield Museum. He also was featured in several documentaries about the battle, because of his family's associated oral history. He wrote a script "that has been used at the reenactment of the Battle of Little Big Horn held every summer in Hardin since 1965."Medicine Crow was a founding member of Little Bighorn College and of the Buffalo Bill Historical Center in Cody, Wyoming beginning in 1976.As historian, Medicine Crow was the "keeper of memories" of his tribe. He preserved the stories and photographs of his people in an archive in his house and garage.  His books include Crow Migration Story, Medicine Crow, the Handbook of the Crow Indians Law and Treaties, Crow Indian Buffalo Jump Techniques, and From the Heart of Crow Country. He also wrote a book for children entitled Brave Wolf and the Thunderbird.


== Death ==
Medicine Crow continued to write and lecture at universities and public institutions until his death, at the age of 102, on April 3, 2016. He was in hospice care in Billings, Montana.  He is survived by his only son Ron Medicine Crow, daughters Vernelle Medicine Crow and Diane Reynolds, and stepdaughter Garnet Watan.


== Honors ==
Medicine Crow received honorary doctorates from Rocky Mountain College in 1999, his alma mater the University of Southern California in 2003, and Bacone College in 2010. He was an ambassador and commencement speaker at the latter, a college established for Native Americans, for more than 50 years.
His memoir, Counting Coup: Becoming a Crow Chief on the Reservation and Beyond, was chosen in 2007 by the National Council for the Social Studies as a "Notable Tradebook for Young People."
On June 25, 2008, Medicine Crow received two military decorations: the Bronze Star for his service in the U.S. Army, and the French Legion of Honor Chevalier medal, both for service during World War II.  His other military awards include the Combat Infantryman Badge, Army Good Conduct Medal, American Campaign Medal, European-African-Middle Eastern Campaign Medal, and World War II Victory Medal.
On July 17, 2008, Senators Max Baucus, Jon Tester, and Mike Enzi introduced a bill to award him the Congressional Gold Medal; however, the bill did not garner the required sponsorship of two-thirds of the senate to move forward.
Medicine Crow received the Presidential Medal of Freedom (the highest civilian honor awarded in the United States) from President Barack Obama on August 12, 2009. During the White House ceremony, Obama referred to Medicine Crow as bacheitche, or a "good man," in the Crow language.


== Bibliography ==
The Image Taker: The Selected Stories and Photographs of Edward S. Curtis [Foreword] (World Wisdom, 2009) ISBN 978-1-933316-70-3
The Earth Made New: Plains Indian Stories of Creation [Foreword] (World Wisdom, 2009) ISBN 978-1-933316-67-3
Native Spirit: The Sun Dance Way [Introduction] (World Wisdom, 2007) ISBN 978-1-933316-27-7
Native Spirit and The Sun Dance Way DVD (World Wisdom, 2007)
Counting Coup: Becoming a Crow Chief on the Reservation and Beyond (National Geographic Children's Books, 2006) ISBN 978-0-7922-5391-4
All Our Relatives: Traditional Native American Thoughts about Nature, [foreword] (World Wisdom, 2005) ISBN 978-0-941532-77-8
From the Heart of the Crow Country: The Crow Indians' Own Stories (Bison Books, 2000) ISBN 978-0-8032-8263-6
Brave Wolf and the Thunderbird (Abbeville Press, 1998) ISBN 978-0-7892-0160-7
The Last Warrior (Sunset Productions, July 1995) ISBN 978-99953-31-04-7
Keep the Last Bullet For Yourself (The True Story of Custer's Last Stand) [Introduction] (Reference Publications, 1980)
Memoirs of a White Crow Indian [Introduction] (University of Nebraska Press, 1976) ISBN 978-0-8032-5800-6
The Crow Indians: 100 years of acculturation (Wyola Elementary School, 1976)


== References ==


== External links ==
Joe Medicine Crow: Life and Work (film clips, articles, and slideshows)
Tribal historian honored as 2005 'Montana Tourism Person of the Year'
Cast Member in Documentary about Crow and Shoshone Sun Dance and Tribal Culture'
Appearances on C-SPAN
Joe Medicine Crow at Find a Grave