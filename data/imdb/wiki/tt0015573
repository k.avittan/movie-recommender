The Rime of the Ancient Mariner (originally The Rime of the Ancyent Marinere) is the longest major poem by the English poet Samuel Taylor Coleridge, written in 1797–98 and published in 1798 in the first edition of Lyrical Ballads. Some modern editions use a revised version printed in 1817 that featured a gloss. Along with other poems in Lyrical Ballads, it is often considered a signal shift to modern poetry and the beginning of British Romantic literature.
The Rime of the Ancient Mariner recounts the experiences of a sailor who has returned from a long sea voyage. The mariner stops a man who is on his way to a wedding ceremony and begins to narrate a story. The wedding-guest's reaction turns from bemusement to impatience to fear to fascination as the mariner's story progresses, as can be seen in the language style: Coleridge uses narrative techniques such as personification and repetition to create a sense of danger, the supernatural, or serenity, depending on the mood in different parts of the poem.


== Synopsis ==

The poem begins with an old grey-bearded sailor, the Mariner, stopping a guest at a wedding ceremony to tell him a story of a sailing voyage he took long ago. The Wedding-Guest is at first reluctant to listen, as the ceremony is about to begin, but the mariner's glittering eye captivates him.
The mariner's tale begins with his ship departing on its journey. Despite initial good fortune, the ship is driven south by a storm and eventually reaches the icy waters of the Antarctic. An albatross appears and leads the ship out of the ice jam where it is stuck, but even as the albatross is fed and praised by the ship's crew, the mariner shoots the bird:

The crew is angry with the mariner, believing the albatross brought the south wind that led them out of the Antarctic. However, the sailors change their minds when the weather becomes warmer and the mist disappears:

They soon find that they made a grave mistake in supporting this crime, as it arouses the wrath of spirits who then pursue the ship "from the land of mist and snow"; the south wind that had initially blown them north now sends the ship into uncharted waters near the equator, where it is becalmed:

The sailors change their minds again and blame the mariner for the torment of their thirst. In anger, the crew forces the mariner to wear the dead albatross about his neck, perhaps to illustrate the burden he must suffer from killing it, or perhaps as a sign of regret:

After a "weary time", the ship encounters a ghostly hulk. On board are Death (a skeleton) and the "Night-mare Life-in-Death", a deathly pale woman, who are playing dice for the souls of the crew. With a roll of the dice, Death wins the lives of the crew members and Life-in-Death the life of the mariner, a prize she considers more valuable. Her name is a clue to the mariner's fate: he will endure a fate worse than death as punishment for his killing of the albatross. One by one, all of the crew members die, but the mariner lives on, seeing for seven days and nights the curse in the eyes of the crew's corpses, whose last expressions remain upon their faces:

Eventually, this stage of the mariner's curse is lifted after he begins to appreciate the many sea creatures swimming in the water. Despite his cursing them as "slimy things" earlier in the poem, he suddenly sees their true beauty and blesses them ("A spring of love gush'd from my heart, And I bless'd them unaware"). As he manages to pray, the albatross falls from his neck and his guilt is partially expiated. It then starts to rain, and the bodies of the crew, possessed by good spirits, rise again and help steer the ship. In a trance, the mariner hears two spirits discussing his voyage and penance, and learns that the ship is being powered supernaturally:

Finally the mariner wakes from his trance and comes in sight of his homeland, but is initially uncertain as to whether or not he is hallucinating:

The rotten remains of the ship sink in a whirlpool, leaving only the mariner behind. A hermit on the mainland who has spotted the approaching ship comes to meet it in a boat, rowed by a pilot and his boy. When they pull the mariner from the water, they think he is dead, but when he opens his mouth, the pilot shrieks with fright. The hermit prays, and the mariner picks up the oars to row. The pilot's boy laughs, thinking the mariner is the devil, and cries, "The Devil knows how to row". Back on land, the mariner is compelled by "a woful agony" to tell the hermit his story.
As penance for shooting the albatross, the mariner, driven by the agony of his guilt, is now forced to wander the earth, telling his story over and over, and teaching a lesson to those he meets:

After finishing his story, the mariner leaves, and the wedding-guest returns home, waking the next morning "a sadder and a wiser man".
The poem received mixed reviews from critics, and Coleridge was once told by the publisher that most of the book's sales were to sailors who thought it was a naval songbook. Coleridge made several modifications to the poem over the years. In the second edition of Lyrical Ballads, published in 1800, he replaced many of the archaic words.


== Inspiration for the poem ==

The poem may have been inspired by James Cook's second voyage of exploration (1772–1775) of the South Seas and the Pacific Ocean; Coleridge's tutor, William Wales, was the astronomer on Cook's flagship and had a strong relationship with Cook. On this second voyage Cook crossed three times into the Antarctic Circle to determine whether the fabled great southern continent Terra Australis existed. Critics have also suggested that the poem may have been inspired by the voyage of Thomas James into the Arctic.According to William Wordsworth, the poem was inspired while Coleridge, Wordsworth, and Wordsworth's sister Dorothy were on a walking tour through the Quantock Hills in Somerset. The discussion had turned to a book that Wordsworth was reading, A Voyage Round The World by Way of the Great South Sea (1726) by Captain George Shelvocke. The book told of a privateering voyage in 1719 during which a melancholy sailor, Simon Hatley, shot a black albatross:

We all observed, that we had not the sight of one fish of any kind, since we were come to the Southward of the streights of le Mair, nor one sea-bird, except a disconsolate black Albatross, who accompanied us for several days ... till Hattley, (my second Captain) observing, in one of his melancholy fits, that this bird was always hovering near us, imagin'd, from his colour, that it might be some ill omen ... He, after some fruitless attempts, at length, shot the Albatross, not doubting we should have a fair wind after it.
As they discussed Shelvocke's book, Wordsworth proffered the following developmental critique to Coleridge, which importantly contains a reference to tutelary spirits: "Suppose you represent him as having killed one of these birds on entering the south sea, and the tutelary spirits of these regions take upon them to avenge the crime." By the time the trio finished their walk, the poem had taken shape.
Bernard Martin argues in The Ancient Mariner and the Authentic Narrative that Coleridge was also influenced by the life of Anglican clergyman John Newton, who had a near-death experience aboard a slave ship.The poem may also have been inspired by the legends of the Wandering Jew, who was forced to wander the earth until Judgement Day for a terrible crime, found in Charles Maturin's Melmoth the Wanderer, M. G. Lewis' The Monk (a 1796 novel Coleridge reviewed), and the legend of the Flying Dutchman.It is argued that the harbour at Watchet in Somerset was the primary inspiration for the poem, although some time before, John Cruikshank, a local acquaintance of Coleridge's, had related a dream about a skeleton ship manned by spectral sailors. In September 2003, a commemorative statue, by Alan B. Herriot of Penicuik, Scotland, was unveiled at Watchet harbour.


== Coleridge's comments ==

In Biographia Literaria, Coleridge wrote:

The thought suggested itself (to which of us I do not recollect) that a series of poems might be composed of two sorts. In the one, incidents and agents were to be, in part at least, supernatural, and the excellence aimed at was to consist in the interesting of the affections by the dramatic truth of such emotions, as would naturally accompany such situations, supposing them real. And real in this sense they have been to every human being who, from whatever source of delusion, has at any time believed himself under supernatural agency. For the second class, subjects were to be chosen from ordinary life...In this idea originated the plan of the 'Lyrical Ballads'; in which it was agreed, that my endeavours should be directed to persons and characters supernatural, or at least Romantic; yet so as to transfer from our inward nature a human interest and a semblance of truth sufficient to procure for these shadows of imagination that willing suspension of disbelief for the moment, which constitutes poetic faith. ... With this view I wrote the 'Ancient Mariner'.
In Table Talk, Coleridge wrote:

Mrs Barbauld once told me that she admired The Ancient Mariner very much, but that there were two faults in it -- it was improbable, and had no moral.   As for the probability, I owned that that might admit some question; but as to the want of a moral, I told her that in my own judgement the poem had too much; and that the only, or chief fault, if I might say so, was the obtrusion of the moral sentiment so openly on the reader as a principle or cause of action in a work of such pure imagination. It ought to have had no more moral than the Arabian Nights' tale of the merchant's sitting down to eat dates by the side of a well, and throwing the shells aside, and lo! a genie starts up, and says he must kill the aforesaid merchant, because one of the date shells had, it seems, put out the eye of the genie's son.


== Wordsworth's comments ==
Wordsworth wrote to Joseph Cottle in 1799:

From what I can gather it seems that the Ancient Mariner has upon the whole been an injury to the volume, I mean that the old words and the strangeness of it have deterred readers from going on. If the volume should come to a second Edition I would put in its place some little things which would be more likely to suit the common taste.
However, when Lyrical Ballads was reprinted, Wordsworth included it despite Coleridge's objections, writing:

The Poem of my Friend has indeed great defects; first, that the principal person has no distinct character, either in his profession of Mariner, or as a human being who having been long under the control of supernatural impressions might be supposed himself to partake of something supernatural; secondly, that he does not act, but is continually acted upon; thirdly, that the events having no necessary connection do not produce each other; and lastly, that the imagery is somewhat too laboriously accumulated. Yet the Poem contains many delicate touches of passion, and indeed the passion is every where true to nature, a great number of the stanzas present beautiful images, and are expressed with unusual felicity of language; and the versification, though the metre is itself unfit for long poems, is harmonious and artfully varied, exhibiting the utmost powers of that metre, and every variety of which it is capable. It therefore appeared to me that these several merits (the first of which, namely that of the passion, is of the highest kind) gave to the Poem a value which is not often possessed by better Poems.


== Early criticisms ==

Upon its release, the poem was criticized for being obscure and difficult to read. The use of archaic spelling of words was seen as not in keeping with Wordsworth's claims of using common language. Criticism was renewed again in 1815–16, when Coleridge added marginal notes to the poem that were also written in an archaic style. These notes or glosses, placed next to the text of the poem, ostensibly interpret the verses much like marginal notes found in the Bible. There were many opinions on why Coleridge inserted the gloss. Charles Lamb, who had deeply admired the original for its attention to "Human Feeling", claimed that the gloss distanced the audience from the narrative, weakening the poem's effects. The entire poem was first published in the collection of Lyrical Ballads. Another version of the poem was published in the 1817 collection entitled Sibylline Leaves (see 1817 in poetry).


== Interpretations ==
On a surface level the poem explores a violation of nature and the resulting psychological effects on the mariner and on all those who hear him. According to Jerome McGann the poem is like a salvation story. The poem's structure is multi-layered text based on Coleridge's interest in higher criticism. "Like the Iliad or Paradise Lost or any great historical product, the Rime is a work of transhistorical rather than so-called universal significance. This verbal distinction is important because it calls attention to a real one. Like The Divine Comedy or any other poem, the Rime is not valued or used always or everywhere or by everyone in the same way or for the same reasons."George Whalley, in his 1946–47 essay, "The Mariner and the Albatross", suggests that the Ancient Mariner is an autobiographical portrait of Coleridge himself, comparing the mariner's loneliness with Coleridge's own feelings of loneliness expressed in his letters and journals.


== In popular culture ==

In addition to being referred to in several other notable works, due to the popularity of the poem the phrase "albatross around one's neck" has become an English-language idiom referring to "a heavy burden of guilt that becomes an obstacle to success".The phrase "Water, water, every where, / Nor any drop to drink" has appeared widely in popular culture, but usually given in a more natural modern phrasing as "Water, water, everywhere / But not a drop to drink"; some such appearances have, in turn, played on the frequency with which these lines are misquoted.The poem is referenced in the script of the 1939 film The Adventures of Sherlock Holmes.The Iron Maiden song "Rime of the Ancient Mariner" from their fifth studio album Powerslave (1984) was inspired by and based on the poem, and quotes the poem in its lyrics.The Slint song "Good Morning, Captain" from their album Spiderland (1991) was a lesser known tribute to Coleridge's seafaring epic.The Bastille song "Weight of Living, Pt. I" from their first album Bad Blood (2013) was also based on the poem. Its lyrics refer to someone having an albatross around their neck.Donald Duck tries to recite the poem in Carl Barks' 1966 cartoon "The Not-So-Ancient Mariner", published in the Walt Disney's Comics and Stories series.In the Stephen King novel 11/22/63 the main character says "It was like being tapped by Coleridge’s Ancient Mariner, who stoppeth one of three".


== Notes ==


== References ==
Gardner, Martin, The Annotated Ancient Mariner, New York: Clarkson Potter, 1965; Reprinted by Prometheus Books, 19??, ISBN 1-59102-125-1
John Livingston Lowes, The Road to Xanadu – A Study in the Ways of the Imagination, Houghton Mifflin, 1927
Scott, Grant F. "'The Many Men so Beautiful': Gustave Doré's Illustrations to 'The Rime of the Ancient Mariner,'" Romanticism 16.1 (2010): 1–24.


== External links ==
Illustrations from The Rime of the Ancient Mariner, Gustave Doré illustrations from the University at Buffalo Libraries’ Rare & Special Books collection
The Rime of the Ancient Mariner, text from Project Gutenberg
The Rime of the Ancient Mariner, audiobook (Jane Aker) from Project Gutenberg
The Rime of the Ancient Mariner: Critical Analysis and Summary
 The Rime of the Ancient Mariner public domain audiobook at LibriVox
Abstracts of literary criticism of The Rime of the Ancient Mariner