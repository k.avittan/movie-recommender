The Leopard's Spots: A Romance of the White Man's Burden—1865–1900 is the first novel of Thomas Dixon's Ku Klux Klan trilogy, and was followed by The Clansman: A Historical Romance of the Ku Klux Klan (1905), and The Traitor: A Story of the Fall of the Invisible Empire (1907). In the novel, published in 1902, Dixon offers an account of Reconstruction in which he portrays a Reconstruction leader (and former slave driver), Northern carpetbaggers, and emancipated slaves as the villains; Ku Klux Klan members are heroes. While the playbills and program for The Birth of a Nation claimed The Leopard's Spots as a source in addition to The Clansman, recent scholars do not accept this.A passage from the Book of Jeremiah (13:23) is included on the title page: "Can the Ethiopian change his skin, or the leopard his spots?" The title conveyed the idea that as leopards could not change their spots, people of African origin could not change what Dixon, as a racist and white supremacist, viewed as inherently negative character traits.


== A reply to Uncle Tom's Cabin ==
Harriet Beecher Stowe's landmark novel of 1852, Uncle Tom's Cabin; or, Life Among the Lowly, had a profound effect on attitudes toward African Americans and slavery in the U.S. and is said to have "helped lay the groundwork for the Civil War". It was still widely read fifty years after its publication. According to Dixon, whose contact with the work was a dramatized version, Stowe "grossly misrepresent[ed]" the American South, and he felt her sympathetic portrayal of African Americans demanded revision. So as to make it clear he is answering Stowe, he presents his version of Stowe's characters, using Stowe's character names.


== Characters ==


=== Using names of characters in Uncle Tom's Cabin ===
Simon Legree – Ex-slave driver and Reconstruction leader 
Tom Camp – In Stowe's novel Tom (no last name) is a humble African-American slave and "Mr. Shelby's best hand". Dixon's Tom is a former Confederate soldier, a poor white Christian whose family is victimized by black men.
Hon. Tim Shelby – Political boss. In Uncle Tom's Cabin Arthur Shelby was Tom's owner, who "sold him South". His son George Shelby is also a character.
George Harris, Jr – An educated negro


=== Other characters ===
Charles Gaston – A man who dreams of making it to the Governor's Mansion 
Sallie Worth – A daughter of the old-fashioned South 
Gen. Daniel Worth – Sallie Worth's father 
Mrs. Worth – Sallie's mother 
The Rev. John Durham – A preacher who threw his life away 
Flora – Tom's daughter 
Allan Mcleod – A scalawag (Union sympathizer) 
Everett Lowell – Member of Congress from Boston 
Helen Lowell – Everett's daughter 
Major Stuart Dameron – Head of the Ku Klux Klan 
Hose Norman – poor white man 
Leonidas


== Dramatization ==
A dramatization by Dixon, with the same title, was produced in New York in 1913.


== References ==


== Further reading ==
Bloomfield, Maxwell. "Dixon's "The Leopard's Spots": A Study in Popular Racism," American Quarterly,  Vol. 16, No. 3 (Autumn, 1964), pp. 387–401 in JSTOR


== External links ==
Full text of The Leopard's Spots, Documenting the American South, University of North Carolina
 The leopard's Spots public domain audiobook at LibriVox