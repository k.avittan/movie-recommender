SureFire, LLC. is an American company based in Fountain Valley, California. Their main products are flashlights, headlamps, weapon-mounted lights and laser sights. In addition, Surefire makes knives, sound suppressors, earplugs, Picatinny Rails and batteries. The company is a major supplier of flashlights to the U.S. Armed Forces. Surefire products are commonly used in the US by law enforcement agencies.


== History ==
John Matthews founded Newport Corporation in 1969, which specialized in industrial lasers. After Matthews developed a laser sight; he, Peter Hauk, and Ed Reynolds founded the spin-off company, Laser Products Corporation on October 17, 1979. In 1984, the company supplied the Los Angeles Police Department and Los Angeles County Sheriff's Department with shotgun laser sights for use during the 1984 Summer Olympics. Laser Products developed its first "SureFire" branded product — a handgun mounted light in 1985. Laser Products Corporation became SureFire, LLC in 2000. More recently, SureFire attained ISO9001:2000 certification in 2008.


== Products ==


=== Flashlights ===
Surefire produces flashlights of widely varying sizes and power outputs from single-cell lights to large 20-cell HID models. Most of its flashlights are powered by primary lithium CR123A batteries that allow for compact size and low weight while offering high power output. These batteries offer a long shelf-life, but are more costly than zinc-based cells. Several flashlights are available with rechargeable battery packs. In 2010, Surefire released the E2L AA, designed to operate on AA batteries, as does the Minimus™ AA headlamp. Surefire sells rechargeable "LFP123A" lithium ion batteries for all LED flashlights as a replacement for CR123A. Surefire also produces military weapon lights for mounting on handguns, rifles, sub-machine guns and shotguns. Surefire's Z2 CombatLight is standard issue to the FBI and the Federal Air Marshal Service, and their various handheld lights are a frequent choice of police, military, fire, and EMS personnel. Some models of handheld flashlights include an integrated "strike bezel" for use as a self-defense weapon. All of Surefire's flashlights are manufactured in the United States, although components such as LEDs are sometimes sourced overseas.
Most Surefire flashlight models are made of anodized aluminum alloy in various colors, while a few models are made of Nitrolon, a proprietary glass-filled polyamide nylon plastic. All flashlights are weatherproof and have various accessories, including red (night), blue (blood trail), green (reading maps/charts) and infrared (night vision compatible) filters, beam diffusers, beam covers, lanyards, pouches/holsters and spare battery/bulb carriers. Some models use incandescent bulbs, while most others use LEDs with electronically controlled power regulation and adjustable brightness. The company used Seoul Semiconductor and Cree XR-E LEDs in flashlights introduced in 2007. More recently, flashlights with a strobe function, used for signalling or to disorientate were introduced.A more notable product is the SureFire M6 Guardian, a flashlight with a 250 or 500 lumen beam from a xenon bulb. Fifteen M6 flashlights were used to illuminate the Stonehenge for the June 2008 cover photo of National Geographic Magazine. The New York Times referred to the M6 as being of the "design school that might be called Modern Militant, the most familiar example of which is the Hummer." This was later used in SureFire's 2007 product catalogue.


=== Other products ===

In addition to flashlights, the company produces headlamps, helmet mounted lights with an infra-red IFF strobe and weapon-mounted lights for various rifles and handguns. Some models of handgun-mounted lights have an integrated laser sight.A large weapon-light is manufactured by Surefire; the HellFighter Weaponlight uses a 35 watt high intensity discharge (HID) bulb powered by a 12 volt car battery or by two BA-5590 military batteries. The bulb is shock-isolated, outputs about 3000 lumens and is mounted in a textured parabolic reflector that provides a beam that is optimized for throw, but also with additional spill light for peripheral illumination. It is bright enough to illuminate targets hundreds of yards away and is intended to be attached to a machine gun.Surefire produces Picatinny rail forends, sound suppressors, muzzle brakes, flash suppressors, EarPro branded hearing protection and tactical communication earpieces, and Combat Utility knives. These knives combine some features of a multi-tool with the blade of a combat knife. In late 2010, Surefire introduced STANAG compatible high capacity magazines with a capacity of 60 or 100 rounds.


== See also ==
Insight Technology
Maglite
Pelican Products
Streamlight


== References ==


== External links ==
Official SureFire website
SureFire Flashlight comparisons