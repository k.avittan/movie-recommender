The Night Wolves (Russian: Ночные Волки, tr. Nochnye Volki) are a Russian motorcycle club.


== History ==
The club began forming out of a mixture of rock music fans and motorcycle enthusiasts who held then-illegal rock concerts in Moscow (see Censorship in the Soviet Union) as far back as 1983. By 1989 the club was consolidated as an informal motorcycle group bearing the name "Night Wolves", during the Perestroika era of the Soviet Union. It became the first official bike club in the USSR. As of the beginning of 2018, the Night Wolves now have chapters in Russia, Ukraine, Latvia, Germany, Bulgaria , Serbia, Australia, Slovakia, Belarus, Philippines, Bosnia and Herzegovina, Montenegro, Czech Republic and Macedonia. Alexander Zaldostanov (also known as The Surgeon) became the leader of the club in 1989.
The club runs multiple rock clubs and arranges many rock concerts in Russia. It also runs several tattoo parlors and is one of the founders of the annual International Moscow Tattoo Convention. In 1995, they launched a clothing line, "Wolf Wear," and established the "Wolf Engineering" custom shop subsidiary. It has gathered mechanics and general motorcycle enthusiasts from all across Russia, runs several motorcycle repair and custom shops and also developed the Wolf-Ural (Волк-Урал) motorcycle together with the manufacturer IMZ-Ural. They usually ride Harley-Davidsons. Another subsidiary is the racing team "Wolf Racing" which was created in 2001 and participates in and organizes various events in Russia.
A member of the Night Wolves, Valery Bely, was shot and killed in a dispute with a rival biker club, Three Roads, in November 2012. The head of Chechnya, Ramzan Kadyrov, joined the group in August 2014. In May 2015, Zaldostanov inaugurated the group's Chechen branch in Grozny and named Kadyrov its honorary leader.The Night Wolves set up a compound in Slovakia near Bratislava, not far from the Czech border, housing old tanks and armored vehicles.


== Activism ==
The club has long taken an interest in the political and social life of Russia, engaging in youth social issues, forging close links to the Kremlin and establishing a friendship with President Putin. They also contributed to the invasion of Crimea by patrolling the streets of the peninsula with unmarked soldiers sent from Russia and fought for the pro-Russian side in the Donbass war. They are involved with the Russian Orthodox Church and make a motorcycle pilgrimage to holy Russian Orthodox sites several times a year.


== Relationship with the Kremlin ==
The club has close ties to Vladimir Putin and a generally Russian nationalist sentiment. Putin has described them as his "friends" and appeared at their rallies, riding a Harley-Davidson trike. Yale University historian Timothy Snyder believes it serves "as a paramilitary and propaganda arm of the Putin regime."The Night Wolves are funded by the Kremlin, receiving "several hundred million rubles a year" according to a report in 2013. In May 2015, Alexei Navalny claimed that the Russian government had given 56 million rubles to the group in the preceding year and a half. In October 2015, Night Wolves received a donation of 12,000,000 rubles from the Russian government to 'build a "Patriot" youth center in Sevastopol in Crimea'.The Night Wolves were referred to as "Putin's Angels" in western media coverage in 2015.


== Involvement in Ukraine ==

Members of the Night Wolves have fought on the side of pro-Russian militants during the Crimean Crisis and the Donbass war. They blockaded the main routes into Sevastopol and participated in attacks on a natural gas facility and the naval headquarters in the city. In April 2015, Agence France-Presse stated that Alexei Vereshchyagin had fought against Ukrainian government forces in Luhansk.


== Shows ==
Their August 2014 show in Sevastopol, supporting the annexation of Crimea by Russia and depicting Ukraine as a country controlled by "fascists", was attended by an estimated 100,000 people and broadcast by Russian state television.During their New Year's shows for children, the Night Wolves portrayed the West as conspiring to destroy Russia. In a January 2015 interview with RIA Novosti, Zaldostanov stated that "When the kids come to us, we don't cut costs on special effects or efforts to convey to the children a feeling of true drive and danger."


== Sanctions ==


=== Canada ===
Canada added Zaldostanov to its sanctions list in February 2015. The Night Wolves were included in June 2015.


=== Germany and Poland ===
In 2015, when the Night Wolves were planning to celebrate the "Victory Day" — victory over the Nazi Germany in the "Great Patriotic War" — on May 9 with a trip from Moscow to Berlin, the government of Poland refused them entry due to the club members' active support of Putin, and Germany cancelled the bikers' Schengen visas. On their way to Berlin, despite visa issues, Night Wolves commemorated the deaths of Polish officers murdered by NKVD in Katyn by lighting candles in their name on 26 April, although Night Wolves and their leader are outspoken supporters of Joseph Stalin, who ordered the murders.A Polish motorcyclists' organization calling itself the Katyń Motorocycle Raid (KMR) criticized the ban and said that Night Wolves already made two such trips in the past, and Polish public opinion was divided on the decision, with 38.7% supporting it and 28.5% opposing. After the ban, the KMR declared that it would light candles on the graves of fallen Red Army soldiers in the name of the Night Wolves. Families of the Katyń massacre's victims stated that they did not wish to be associated with the KMR.Bikers who wanted to enter Germany at the Berlin Schönefeld International Airport were denied entry. On 6 May, one member, who was trying to enter Germany by ship from Helsinki to Travemünde, was deported to Finland.


=== United States ===
In December 2014, the United States announced sanctions against the Night Wolves due to their involvement in attacks on a gas distribution station in Strikolkove and the Ukrainian naval headquarters in Sevastopol, and recruitment of fighters for the war in Donbass.


== Views ==


=== On Stalin ===
According to Peter Pomerantsev, the Night Wolves admire Joseph Stalin, the Soviet dictator and one of the three main Allied leaders of World War II. On their ride to Berlin for a rally marking the anniversary of the end of World War II in 2015, they displayed red flags with portraits of Stalin and shouted World War II-era Red Army oath "For the Motherland! For Stalin!" According to Alexei Magister, the president of a biker club linked to the Night Wolves, "We are not harming anyone, we are just honouring the memory of those who fell to destroy Fascism".On their ride to Berlin in 2015, the Night Wolves paid homage at the Russia's memorial to Polish prisoners of war killed in the Katyn massacre by the NKVD on Stalin's and Beria's orders in 1940.


=== On Anti-Maidan protests ===
In January 2015, the group's leader, the Ukraine-born Alexander Zaldostanov, expressed support for a Russian Anti-Maidan movement which said it was ready to use violence to stop anti-government protesters.


=== On foreign countries ===

In an interview, Zaldostanov said "For the first time we showed resistance to the global Satanism, the growing savagery of Western Europe, the rush to consumerism that denies all spirituality, the destruction of traditional values, all this homosexual talk, this American democracy."Andrei Bobrovsky, the leader of the Night Wolves ride from Moscow to Berlin, told Sputnik (the Russian government-controlled news agency) in May 2016, "The Slovaks received us very well. It seems that we [Russians] and them [Slovaks] are soul mates. We're not only Slavs, but we're also very similar in the way we think."In an interview with Finnish journalist Juha Portaankorva in 2016, Zaldostanov said "I would love Finland to stay a good neighbor of Russia. I would love the Finns to trust Russia more than America."


=== On LGBT people ===
In January 2015, Zaldostanov suggested that "Death to faggots" could be an alternate name for the Russian anti-Maidan movement. At a February 2015 "Anti-Maidan" rally in Moscow organized by Zaldostanov, popular slogans included "We don't need Western ideology and gay parades!"


== Membership ==
According to some of the Night Wolves, a prospective member must be male, from a former Soviet country, invited by an existing member, and must participate in club events for two years before becoming a member.The Night Wolves claim not to discriminate on religious grounds. The Night Wolves are openly homophobic and do not allow gay men to join the club; women are also not allowed to join.The Night Wolves have over 7000 members, including Ramzan Kadyrov, the Head of the Chechen Republic.There is a Night Wolves Macedonia.


== Criticism ==

In the very beginning the Night Wolves were often in conflict with other biking clubs. Yevgeny Vorobyev, the leader of the Three Roads, accused them of becoming too politicised. The representative of the Hells Angels in Russia told the Moscow Times, "We call each other brothers and avoid being linked to any religious and political organizations, as these are things that divide people. To express my political views, I vote."Another view describes Ulson Gunnar in New Eastern Outlook, an online magazine of the Institute of Oriental Studies of the Russian Academy of Sciences. For him, the affair of the Night Wolves shows the increasing hypocrisy of the West.


== Media ==
Mark Galeotti, writing in The Moscow Times, described the Night Wolves as "a case study in the Kremlin's strategy of adopting and taming potentially hostile groups and using them precisely as tools of control—counter-counterculture, as it were."


== See also ==
Alexander Peresvet
Hybrid warfare


== References ==


== External links ==
Official website (in Russian)