The Marfa lights, also known as the Marfa ghost lights, have been observed near U.S. Route 67 on Mitchell Flat east of Marfa, Texas, in the United States. They have gained some fame as onlookers have attributed them to paranormal phenomena such as ghosts, UFOs, or will-o'-the-wisp. Scientific research suggests that most, if not all, are atmospheric reflections of automobile headlights and campfires.


== Overview ==
According to Judith Brueske, "The 'Marfa Lights' of west Texas have been called many names over the years, such as ghost lights, weird lights, mystery lights, or Chinati lights. The favorite place from which to view the lights is a widened shoulder on Highway 90 about nine miles east of Marfa. The lights are most often reported as distant spots of brightness, distinguishable from ranch lights and automobile headlights on Highway 67 (between Marfa and Presidio, to the south) primarily by their aberrant movements."Robert and Judy Wagers define "Classic Marfa Lights" as being seen south-southwest of the Marfa Lights Viewing Center (MLVC). They define the left margin of the viewing area as being aligned along the Big Bend Telephone Company tower as viewed from the MLVC, and the right margin as Chinati Peak as viewed from the MLVC.Referring to the Marfa Lights View Park east of Marfa, James Bunnell states, "you might just see mysterious orbs of light suddenly appear above desert foliage. These balls of light may remain stationary as they pulse on and off with intensity varying from dim to almost blinding brilliance. Then again, these ghostly lights may dart across the desert...or perform splits and mergers. Light colors are usually yellow-orange but other hues, including green, blue and red are also seen. Marfa Mystery Lights (MLs) usually fly above desert vegetation but below background mesas."


== History ==
The first historical record of the Marfa lights was in 1883 when a young cowhand, Robert Reed Ellison, saw a flickering light while he was driving cattle through Paisano Pass and wondered if it was the campfire of Apache Indians. Other settlers told him they often saw the lights, but that when they investigated they found no ashes or other evidence of a campsite. Joe and Anne Humphreys next reported seeing the lights in 1885. Both stories appear in Cecilia Thompson's book History of Marfa and Presidio County, Texas 1535-1946, which was published in 1985.The first published account of the lights appeared in the July 1957 issue of Coronet magazine. In 1976 Elton Miles's Tales of the Big Bend included stories dating to the 19th century and a photograph of the Marfa lights by a local rancher.Bunnell lists 34 Marfa lights sightings from 1945 through 2008. Monitoring stations were put in place starting in 2003. He has identified "an average of 9.5 MLs on 5.25 nights per year", but believes that the monitoring stations may only be finding half of the Marfa lights in Mitchell Flat.


== Explanations ==


=== Atmospheric phenomena ===
Skeptic Brian Dunning notes that the designated "View Park" for the lights, a roadside park on the south side of U.S. Route 90 about 9 miles (14 km) east of Marfa, is at the site of Marfa Army Airfield, where tens of thousands of personnel were stationed between 1942 and 1947, training American and Allied pilots. This massive field was then used for years as a regional airport, with daily airline service. Since Marfa AAF and its satellite fields are each constantly patrolled by sentries, they consider it unlikely that any unusual phenomena would remain unobserved and unmentioned. According to Dunning, the likeliest explanation is that the lights are a sort of mirage caused by sharp temperature gradients between cold and warm layers of air.  Marfa is at an elevation of 4,688 ft (1,429 m) above sea level, and temperature differentials of 40–50 °F (22–28 °C) between high and low temperatures are quite common.


=== Car lights ===
In May 2004 a group from the Society of Physics Students at the University of Texas at Dallas spent four days investigating and recording lights observed southwest of the view park using traffic volume-monitoring equipment, video cameras, binoculars, and chase cars. Their report made the following conclusions:
U.S. Highway 67 is visible from the Marfa lights viewing location.
The frequency of lights southwest of the view park correlates with the frequency of vehicle traffic on U.S. 67.
The motion of the observed lights was in a straight line, corresponding to U.S. 67.
When the group parked a vehicle on U.S. 67 and flashed its headlights, this was visible at the view park and appeared to be a Marfa light.
A car passing the parked vehicle appeared as one Marfa light passing another at the view park.They came to the conclusion that all the lights observed over a four-night period southwest of the view park could be reliably attributed to automobile headlights traveling along U.S. 67 between Marfa and Presidio, Texas.


==== Spectroscopic analysis ====
For 20 nights in May 2008, scientists from Texas State University used spectroscopy equipment to observe lights from the Marfa lights viewing station. They recorded a number of lights that "could have been mistaken for lights of unknown origin", but in each case the movements of the lights and the data from their equipment could be easily explained as automobile headlights or small fires.


== See also ==


== References ==
Notes

Bibliography

Manuel Borraz Aymerich and Vicente-Juan Ballester Olmos, "The Marfa Lights: Examining the Photographic Evidence (2003-2007), 2020
James Bunnell,"Strange Lights in West Texas" Lacey Publishing Company, 29 Bounty Road West, Benbrook, TX 76132-1003, USA, 2015
James Bunnell,"Hunting Marfa Lights" Lacey Publishing Company, 29 Bounty Road West, Benbrook, TX 76132-1003, USA, 2009
Darack, Ed (2008). "Unlocking the Atmospheric Secrets of the Marfa Mystery Lights". Weatherwise. 61 (3): 36–43. doi:10.3200/WEWI.61.3.36-43.
Herbert Lindee, "Ghosts Lights of Texas," Skeptical Inquirer, Vol. 166, No. 4, Summer 1992, pp. 400–406
Elton Miles, "Tales of the Big Bend," Texas A&M University Press, 1976, pp. 149–167
Paul Moran, "The Mystery of the Texas Ghost Light," Coronet Magazine, July 1957
Dennis Stacy, "The Marfa Lights, A Viewer's Guide," Seale & Stacy, Box 12434, San Antonio, Texas 78212, USA, 1989
Stephan, Karl D.; Bunnell, James; Klier, John; Komala-Noor, Laurence (2011). "Quantitative intensity and location measurements of an intense long-duration luminous object near Marfa, Texas". Journal of Atmospheric and Solar-Terrestrial Physics. 73 (13): 1953. Bibcode:2011JASTP..73.1953S. doi:10.1016/j.jastp.2011.06.002.
David Stipp, "Marfa, Texas, Finds a Flickering Fame in Mystery Lights," Wall Street Journal, March 21, 1984, p. A1.
Cecilia Thompson, History of Marfa and Presidio County, Texas 1535-1946, Volume 1, 1535-1900 (Marfa, TX: The Presidio County Historical Commission, 1985), 194, 197


== External links ==
Marfa Lights from the Handbook of Texas Online
DeMystifying the "Marfa Lights"
"Marfa Lights" – from the Skeptic's Dictionary
Discussion of the Marfa Lights (and other 'ghost lights')
Texas Monthly article "The Truth Is Out There"
Dunning, Brian (11 April 2007). "Skeptoid #38: The Marfa Lights: A Real American Mystery". Skeptoid.