The Woman Next Door (French: La Femme d'à côté) is a 1981 French film directed by François Truffaut. Reminiscent of the medieval legend of Tristan and Iseult but set among young middle-class people in a provincial city, it tells the story of a fatal romance between a loving husband (Gérard Depardieu) and the attractive woman (Fanny Ardant) who moves in next door. The last of Truffaut's serious films, being followed by the more light-hearted Vivement dimanche!, it was the 39th highest-grossing film of the year, with a total of 1,087,600 admissions in France.


== Plot summary ==
Bernard lives happily with his wife Arlette and young son Thomas in a village outside Grenoble. One day a married couple, Philippe and Mathilde, move into the house next door. Bernard and Mathilde are shocked at meeting each other because years before, when both single, they had a stormy affair which ended painfully. At first Bernard avoids Mathilde, until a chance meeting in a supermarket reawakens long-buried passions and soon, while openly good neighbours, in secret they pursue an affair. Though both find the strain of living their normal family and working lives unbearable, it is Bernard who cracks first. After publicly revealing his violent passion for Mathilde at a garden party, he keeps away from her and the two households try to get on with their lives. But the rejected Mathilde then cracks and, after publicly collapsing at the tennis club, is hospitalised with depression. When she is released, she finds that to get far away from Bernard her husband has moved them out of the village. One night Bernard is woken by a banging shutter on the empty house next door and gets up to investigate. In the house, he spots Mathilde in the darkness. After they have made love on the bare floor, taking a gun out of her handbag she shoots first him and then herself.


== Cast ==
Gérard Depardieu as Bernard Coudray
Fanny Ardant as Mathilde Bauchard
Henri Garcin as Philippe Bauchard
Michèle Baumgartner as Arlette Coudray
Roger Van Hool as Roland Duguet
Véronique Silver as Madame Odile Jouve
Philippe Morier-Genoud as the Doctor
Olivier Bedquaert as Thomas Coudray


== References ==


== External links ==
The Woman Next Door on IMDb