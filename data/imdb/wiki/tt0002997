Rudy Kurniawan (born 10 October 1976, in Jakarta, Indonesia) is an Indonesian convicted criminal and perpetrator of wine fraud.
He was found to be offering more magnums of the limited edition 1947 Château Lafleur than had been produced, and his Clos St. Denis Grand Cru was labelled with a fictitious vintage. He was sentenced to 10 years' imprisonment in 2013,  setting  7 November 2020 as his earliest possible release date.


== Early life and education ==
His birth name was Zhen Wang Huang, but his Chinese father reportedly gave him an Indonesian last name to help him maintain "autonomy".
It has been said that his transliterated Chinese name is Zhen Wang Huang.Kurniawan attended California State University, Northridge, in the late nineties, arriving in the US on a student visa around 1998.In 2003, U.S. Immigration and Customs Enforcement ordered Kurniawan to submit to a voluntary deportation; he elected to stay in the United States as an illegal alien.


== Family ==
Kurniawan's family is in Indonesia. His mother's two brothers Hendra Rahardja and Eddy Tansil  also committed massive fraud and were sentenced to prison.


== Wine career ==
Kurniawan began buying and selling large amounts of rare wines in the early part of the 2000s, spending as much as $1 million a month buying auction lots by 2006. At the same time, he began hosting tastings of rare wines with other collectors; he showed so much affinity for the ultraluxury Burgundy producer, Domaine de la Romanée-Conti, at these events that he became known as "Dr. Conti". At this time, he was described as possessing "arguably the greatest cellar on Earth."Eventually, he wound up consigning lots with John Kapon in two major auctions at Acker, Merrall & Condit in 2006, netting $10.6 million ($13.4 million in 2019 dollars) in the first and $24.7 million ($31.3 million in 2019 dollars) in the second. The second auction was the record for a single sale of wine at auction, beating the previous record by more than $10 million. During the two auctions, Kurniawan offered for sale eight magnums of 1947 Château Lafleur. A few days after the second sale, Kurniawan secured a loan for $8.84 million ($11.2 million in 2019 dollars) from Acker Merrall & Condit, secured by the wine and art in his collection.In April 2007, Kurniawan consigned several magnums of 1982 Château Le Pin at Christie's in Los Angeles; the bottles were featured on the auction catalog's cover. Representatives from Le Pin contacted the auction house and indicated that the bottles were fake; Christie's withdrew the lot from auction after further review of the bottles. At the 2007 TASTE3 food and wine conference held in Napa, California, David Molyneux-Berry, the former head of the wine department at Sotheby's, noted that only five magnums of the 1947 Lafleur were produced, indicating that Kurniawan's wines sold in 2006 were assuredly fakes.In 2008, Kurniawan consigned several bottles allegedly made by Domaine Ponsot from the Clos St. Denis Grand Cru appellation, with vintages ranging from 1945 through 1971. According to Laurent Ponsot, head of Domaine Ponsot, the domaine had never made a Clos St. Denis prior to 1982. Ponsot contacted the auction house, and the lots were removed. Ponsot later met with Kurniawan and was left wondering if Rudy was actually a counterfeiter or simply the last person to innocently handle counterfeited wine. Kurniawan, when asked after the auction lots were withdrawn where the wine came from, said "we try our best to get it right, but it's Burgundy, and sometimes shit happens."After the Ponsot sale was called off, Kurniawan's luck had almost entirely run out. Bill Koch filed a lawsuit against him in 2009, alleging Kurniawan knowingly sold fake bottles to him and other collectors, both at auction and privately. He also defaulted on a $10 million loan from the auction house Acker, Merrall & Condit, where he sold much of his wine, including the withdrawn Ponsot sale. In February 2012, Spectrum Wine Auctions had to withdraw several lots of wine, worth an estimated $785,000,  from an auction in London when allegations emerged that they were consigned by Kurniawan through a second party.


== Arrest ==
On the morning of 8 March 2012, the FBI arrested Kurniawan at his home in Arcadia, California. When agents searched his house, they found inexpensive Napa wines with notes indicating they would be passed off as older vintages of Bordeaux, corks, stamps, labels, and other tools involved in counterfeiting wine. He was indicted on several counts of mail fraud and wire fraud in New York on 9 March. Later investigations indicated that Kurniawan was purchasing inexpensive, though old, Burgundy wines and re-labeling them with prestigious producer names and vintages.


== Prosecution ==
Kurniawan's indictment was updated on 8 April 2013, consolidating the fraud charges, and adding two new charges—one for selling a faked jeroboam of Château Mouton-Rothschild 1945 in 2006 for $48,259 ($61.2 thousand in 2019 dollars) and one for selling six bottles of Domaine Georges Roumier Bonnes Mares 1923 at a 2006 auction for $95,000 ($120 thousand in 2019 dollars). Domaine Georges Roumier did not produce wine prior to 1924, according to lead prosecutor Jason Hernandez, ergo the 1923 bottles must be fakes.The initial court date for the criminal hearing was set for 9 September. Due to the court date conflicting with the harvest period for wine grapes in the Northern hemisphere, on 6 May 2013, the judge decided to allow three witnesses (Aubert de Villaine of Domaine de la Romanée-Conti, Christophe Roumier of Domaine Georges Roumier, and Laurent Ponsot of Domaine Ponsot) to testify before trial on videotape, as they would be unavailable to appear in Manhattan at that time.Kurniawan's trial began on 9 December 2013, and concluded on 18 December 2013, when the jury found him guilty. The judge later sentenced him to 10 years' imprisonment.


== Imprisonment ==
Kurniawan, Federal Bureau of Prisons #62470-112, is currently incarcerated at CI Reeves I & II Correctional Facility in Pecos, Texas. His earliest possible release is November 7, 2020. As he had been living in the United States illegally since 2003, he will be deported to Indonesia upon his release.


== Victims ==
Victims of Kurniawan's fraud include Bill Koch, who sued Kurniawan in 2009 alleging he sold fake bottles at auction and in private sales—specifically, a 1947 Pétrus, a 1945 Comte Georges de Vogüé Musigny Cuvée Vielles Vignes and two bottles of 1934 Domaine de la Romanée-Conti that were sold at the first of Kurniawan's two auctions in 2006. Koch and Kurniawan settled out of court in July 2014 for $3 million in damages, and Kurniawan should be completely debriefed regarding his knowledge of counterfeiting in the wine industry. Acker, Merrall, & Condit was still owed, as of February 2012, almost $3.5 million from their loans to Kurniawan. In January 2015, the contents of Kurniawan's personal wine collection was examined for non-fake wines that could be sold to help repay his victims. The authentic wines in his collection were sold for $1.5 million. 


== Movie ==
The 2016 documentary  Sour Grapes directed by Jerry Rothwell and Reuben Atlas retells the events in the wine counterfeiting story of Rudy Kurniawan. The film states that as many as 10,000 counterfeit bottles created by Kurniawan may still be in private collections.


== References ==


== External links ==
Copy of the indictment in USA v. Kurniawan.
Sealed complaint in USA v. Kurniawan.
Copy of the suit filed in Koch v. Kurniawan.