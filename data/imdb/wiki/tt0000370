Scrooge, or, Marley's Ghost is a 1901 British short  silent drama film, directed by Walter R. Booth, featuring the miserly Ebenezer Scrooge confronted by Jacob Marley's ghost and given visions of Christmas past, present, and future. It is the earliest known film adaptation of Charles Dickens's 1843 novella A Christmas Carol. The film,  "although somewhat flat and stage-bound to modern eyes,"  according to Ewan Davidson of BFI Screenonline, "was an ambitious undertaking at the time," as, "not only did it attempt to tell an 80 page story in five minutes, but it featured impressive trick effects, superimposing Marley's face over the door knocker and the scenes from his youth over a black curtain in Scrooge's bedroom."


== The film ==
Filmed in 35mm and in black and white, this short silent film was produced by the English film pioneer R. W. Paul, and directed by Walter R. Booth and was filmed at Paul's Animatograph Works. It was released in November 1901. As was common in cinema's early days, the filmmakers chose to adapt an already well-known story, in this case A Christmas Carol by Charles Dickens, in the belief that the audience's familiarity with the story would result in the need for fewer intertitles. It was presented in 'Twelve Tableaux' or scenes. The film contains the first use of intertitles in a film.Evidence suggests that Paul's version of A Christmas Carol was based as much on J. C. Buckstone's popular stage adaptation Scrooge as on Dickens' original story. Like the play, the film dispenses with the different ghosts that visit Scrooge, instead relying upon the figure of Jacob Marley, draped in a white sheet, to point out the error of Scrooge's ways. The film featured impressive trick effects by 1901 standards, superimposing Marley's face over the door knocker, and displaying the scenes from his youth on a black curtain in Scrooge's bedroom. R. W. Paul was a trick film specialist; Walter Booth, credited as the film's director, was a well-known magician as well as a trick and comic film specialist.  The film makes early use of dissolving between scenes. Some scenes are tinted.


== Content ==

The only known surviving footage, about 3 minutes and 26 seconds in length, is preserved by the British Film Institute. This footage starts with Bob Cratchit showing someone out of Scrooge's office on Christmas Eve, just before he and Scrooge leave for the night, and ends at a scene where Scrooge is shown his own grave. The film does not show the Ghosts of Christmas Past, Christmas Present, or Christmas Yet to Come, instead relying on the ghost of Marley to present the visions to Scrooge.


== See also ==
Cinema of the United Kingdom
The Death of Poor Joe the oldest surviving film featuring a Charles Dickens character. (Joe of Bleak House)
List of ghost films
Adaptations of A Christmas Carol


== References ==


== External links ==
Scrooge, or, Marley's Ghost on IMDb
 Scrooge, or, Marley's Ghost on YouTube
Scrooge, or, Marley's Ghost on the British Film Institute website