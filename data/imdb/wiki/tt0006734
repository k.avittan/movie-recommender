The Idiots (Danish: Idioterne) is a 1998 Danish comedy-drama film written and directed by Lars von Trier. It is his first film made in compliance with the Dogme 95 Manifesto, and is also known as Dogme #2. It is the second film in von Trier's Golden Heart Trilogy, preceded by Breaking the Waves (1996) and succeeded by Dancer in the Dark (2000). It is among the first films to be shot entirely with digital cameras.


== Plot ==
A seemingly anti-bourgeois group of adults spend their time seeking their "inner idiot" to release their inhibitions. They do so by behaving in public as if they were developmentally disabled.
At a restaurant, the patrons are disturbed by the group's mischief, barely contained by their supposed "handler", Susanne; however, single diner Karen develops an appreciation of their antics. The members of the group refer to this behaviour as "spassing", a neologism derived from "spasser", the Danish equivalent of "spaz". Karen leaves in a taxi with the people from the restaurant, and she finds herself at a big house. The apparent leader of the group, Stoffer, is supposed to be selling the house (which belongs to his uncle), but instead it becomes the focal point for group activities.
The "spassing" is a self-defeating attempt by the group to challenge the establishment through provocation. The self-styled idiots feel that the society-at-large treats their intelligence uncreatively and unchallengingly; thus, they seek the uninhibited self-expression that they imagine a romantic ideal of disability will allow.
Stoffer, at his birthday party, wishes for a "gangbang", and both clothes and inhibitions are soon discarded. Then, when Stoffer calls for the group members to let idiocy invade their personal daily lives, Karen takes up the challenge. She takes Susanne back to her house, where they are greeted with surprise by Karen's mother. Karen has been missing for two weeks, following the death of her young baby; she offers no explanation of where she has been. Karen attempts to spaz in front of her family by dribbling the food she is eating, but this results in a violent slap from her husband, Anders. Karen and Susanne leave the house together.


== Cast ==


== Production ==
The Idiots is a co-production of companies from Denmark, France, Italy, the Netherlands, Spain, and Sweden. It was filmed during May 1997 and June 1997. The script was written over just four days, between 16 May 1997 to 19 May 1997.


== Style ==
The confession of a Dogme 95 film is an idea adapted by Thomas Vinterberg in the first Dogme 95 film: Make a confession if there were things happening on the shoot which are not in accordance with the strict interpretation of the Dogme 95 rules. It is written from the director's point of view. Accordingly, von Trier made the following confession:

In relation to the production of Dogme 2 "The Idiots", I confess:
To have used a stand-in in one case only (the sexual intercourse scene).
To have organised payment of cash to the actors for shopping of accessories (food).
To have intervened with the location (by moving sources of light – candlelight – to achieve exposure).
To have been aware of the fact that the production had entered into an agreement of leasing a car (without the knowledge of the involved actor).
All in all, and apart from the above, I feel to have lived up to the intentions and rules of the manifesto: Dogme95.
In order to not violate Dogme 95 rule 2, forbidding the use of non-diegetic music, a harmonica player was recorded during the shooting of some scenes, including the end credits, even if he is not seen onscreen.


== Release ==
The Idiots provoked a storm of publicity and debates, one of which was about the fictional representation of disability. Film critic Mark Kermode's reaction was to shout "Il est merde! Il est merde! (sic)" from the back of the auditorium during the official screening of the film at Cannes, a spontaneous review for which he was ejected from the venue.Channel 4 aired the film unedited in 2005 as part of the channel's "Banned" season exploring censorship and cinematic works. Viewer complaints prompted an Ofcom investigation, which came out in favour of Channel 4. In its ruling, Ofcom found the film "not in breach" of the relevant Code under the specific circumstances of the broadcast, that is "the serious contextualisation of the film within a season examining the censorship of film and television, its artistic purpose, the channel which transmitted it, the strong warnings before the film and prior to the scene in question and the scheduling after midnight." Ofcom added the caveat that, "While we do not consider the film was in breach of the Code on this occasion, we must consider carefully the acceptability of any similar content on an individual basis."The film is classified as adult-only in Argentina, Australia (though it has been shown uncut on TV with an MA rating), Chile, New Zealand, Norway, South Korea, Spain, Taiwan, and the United Kingdom. In Switzerland and Germany, the film ran uncut with a 16-years rating in theaters, followed by a DVD release with the same rating and several uncut television airings.


== Reception ==


=== Critical reception ===
On review aggregator website Rotten Tomatoes, the film has a 71% score based on 31 reviews, with an average rating of 6.48/10. Metacritic reports a 47 out of 100 rating based on 15 critics, indicating "mixed or average reviews".The Idiots was ranked #76 in Empire magazine's "The 100 Best Films of World Cinema" in 2010. The magazine had previously given it a full five star rating on its release in UK cinemas. It is listed as #941 in the film reference book '1001 Movies You Must See Before You Die'.


=== Controversy ===
More controversy arose over the sexual content, which was unusually explicit for a narrative film. The Idiots contains a shower scene in which a member of the group (in character as an "idiot") has an erection and, later, a group sex scene that includes one couple (faces not seen, stand-ins from the porn industry) having unsimulated penetrative (vaginal) sexual intercourse. Both instances of explicit content are in view only for a few seconds. The film was cleared for theatrical release by the British Board of Film Classification, receiving an 18 certificate. When it was shown on Film4 (then FilmFour) in 2000, the erection and the intercourse were obscured by pixelization, following an order from the Independent Television Commission.


=== Accolades ===
The film was shown in competition at the 1998 Cannes Film Festival.

Bodil Awards (1999)
Won: Best Actress, Bodil Jørgensen
Won: Best Supporting Actor, Nikolaj Lie Kaas
Won: Best Supporting Actress, Anne Louise Hassing
Nominated: Best Film
Cannes Film Festival (1998)
Nominated: Palme d'Or
European Film Awards (1998)
Nominated: European Film Award, Best Screenwriter
London Film Festival (1998)
Won: FIPRESCI Prize, Lars von Trier
Robert Festival (1999)
Won: Best Actress, Bodil Jørgensen
Valladolid International Film Festival (1998)
Nominated: Golden Spike, Lars von Trier


== References ==


== External links ==
The Idiots on IMDb
The Idiots at the TCM Movie Database
The Idiots at Rotten Tomatoes
The Idiots at Metacritic