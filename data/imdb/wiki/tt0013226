"Deep in the Heart of Texas" is an American popular song about Texas.
The 1941 song features lyrics by June Hershey and music by Don Swander. There were no fewer than five versions in the Billboard charts in 1942.
"Deep in the Heart of Texas" spent five weeks at the top of Your Hit Parade in 1942 during its twelve weeks stay.


== Notable recordings ==


=== 1942 chart recordings ===
Alvino Rey and his Orchestra (vocal by Bill Schallen and Skeets Herfurt), recorded November 21, 1941, Bluebird 11391) - this topped the Billboard charts in 1942 during a ten-week stay.
Ted Weems and His Orchestra (vocal by Perry Como - recorded on December 9, 1941 for Decca Records in Los Angeles, California. It was a single release (4138 A) on the flip side of the song "Ollie Ollie Out's in Free." This also charted with a peak position of #23.
The Merry Macs - recorded December 23, 1941 for Decca Records, catalogue No. 4136. Chart position peak No. 11
Bing Crosby with Woody Herman and his Woodchoppers (recorded January 18, 1942,  Decca 4162) This reached the No. 3 spot in the charts in 1942.
Horace Heidt's Musical Knights - recorded January 28, 1942, Columbia 36525 - this achieved a top position of No.7.


=== Other notable versions ===
Tommy Tucker (February 2, 1942, Okeh 6583)
Gene Autry recorded February 24, 1942 for Okeh Records 6643.
Bob Grant and His Orchestra - included in the album Songs Of Our Times 1942 (1959)
Freddy Cannon on his debut 1960 album The Explosive Freddy Cannon.
Ray Charles - included in his album The Genius Hits the Road (1960)
Duane Eddy - his 1962 single reached No. 78 in the Billboard charts.
Hank Thompson - included in the album The Best Of Hank Thompson. Vol.2 (1967)
Kidsongs on their "Yankee Doodle Dandy", VHS, DVD, and album
Country singer Moe Bandy released a version of it on his album, Salutes the American Cowboy / Songs of the American Cowboy in 1982.
George Strait included in the album For the Last Time: Live from the Astrodome (2003)
Nickel Creek - recorded for his album Little Cowpoke (2003)


== Film appearances ==
1942 The song's title was used for the name of a 1942 Western film of the same name starring Johnny Mack Brown as a man instrumental in restoring Texas to the United States following the American Civil War. It featured Tex Ritter and the Jimmy Wakely Trio singing the title song.
1942 Gene Autry sang the song in Stardust on the Sage (1942).
1942 Gene Autry, Smiley Burnette, and Joe Strauch Jr. sang the song in Heart of the Rio Grande (1942).
1944 Wing and a Prayer - Sung by William Eythe in the cockpit of a fighter plane as a gun test with machine gun blasts for the handclaps.
1948 A Foreign Affair - Sung by US military personnel at the Lorelei nightclub.
1950 I'll Get By - Performed by June Haver and Gloria DeHaven with the Harry James Orchestra.
1951 Rich, Young, and Pretty - Performed by Jane Powell
1952 With a Song in My Heart
1985 Part of the chorus is sung by Pee-wee Herman in Pee-wee's Big Adventure (with the claps performed by passersby) to prove he's in Texas
2003 In Head of State, Chris Rock's character is campaigning for President and leads a crowd in singing the song, actually an homage to the use of the song in Pee-wee's Big Adventure


== Other usage ==
The University of Texas Longhorn Band performs the song during each football pregame at Darrell K. Royal-Texas Memorial Stadium; The Spirit of Houston Cougar Marching Band often performs the tune for home football games and the Texas Christian University Horned Frog Marching Band performs an arrangement before each game at Amon G. Carter Stadium.  Fans sing "Take Me Out to the Ballgame", followed by "Deep In the Heart of Texas" during the seventh-inning stretch of Houston Astros, San Antonio Missions, Rice Owls, Houston Cougars, and Baylor Bears baseball games, and in the middle of the fifth inning at Texas Rangers games. It is also played after every victorious San Antonio Spurs game. It is also played in the middle of a water break at Houston Dynamo games. It is played before every Houston Texans game, 15 minutes before kickoff. 
In 1942, the BBC banned the song during working hours on the grounds that its infectious melody might cause wartime factory-hands to neglect their tools while they clapped in time with the song.Country music singer and native Texan George Strait has the song played before he gets on stage.


== See also ==
"The Yellow Rose of Texas"
Culture of Texas


== References ==