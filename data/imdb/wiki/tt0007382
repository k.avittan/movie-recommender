A sphinx ( SFINGKS, Ancient Greek: σφίγξ [spʰíŋks], Boeotian: φίξ [pʰíːks], plural sphinxes or sphinges) is a mythical creature with the head of a human, a falcon, a cat, or a sheep and the body of a lion with the wings of an eagle.
In Greek tradition, the sphinx has the head of a woman, the haunches of a lion, and the wings of a bird. She is mythicized as treacherous and merciless, and will kill and eat those who cannot answer her riddle. This deadly version of a sphinx appears in the myth and drama of Oedipus.Unlike the Greek sphinx, which was a woman, the Egyptian sphinx is typically shown as a man (an androsphinx (Ancient Greek: ανδρόσφιγξ)). In addition, the Egyptian sphinx was viewed as benevolent but having a ferocious strength similar to the malevolent Greek version.  Both were thought of as guardians and often flank the entrances to temples.In European decorative art, the sphinx enjoyed a major revival during the Renaissance. Later, the sphinx image, initially very similar to the original Ancient Egyptian concept, was exported into many other cultures, albeit they're often interpreted quite differently due to translations of descriptions of the originals and through the evolution of the concept in relation to other cultural traditions.
Sphinx depictions are generally associated with architectural structures such as royal tombs or religious temples.


== Egypt ==

The largest and most famous sphinx is the Great Sphinx of Giza, situated on the Giza Plateau adjacent to the Great Pyramids of Giza on the west bank of the Nile River and facing east (29°58′31″N 31°08′15″E). The sphinx is located southeast of the pyramids. While the date of its construction is not known for certain, the general consensus among Egyptologists is that the head of the Great Sphinx bears the likeness of the pharaoh Khafra, dating it to between 2600 and 2500 BCE. However, a fringe minority of late 20th century geologists have claimed evidence of water erosion in and around the Sphinx enclosure which would prove that the Sphinx predates Khafra, at around 10000 to 5000 BCE, a claim that is sometimes referred to as the Sphinx water erosion hypothesis but which has little support among Egyptologists and contradicts almost all other evidence.What names their builders gave to these statues is not known. At the Great Sphinx site, a 1400 BCE inscription on a stele belonging to the 18th dynasty pharaoh Thutmose IV lists the names of three aspects of the local sun deity of that period, Khepera–Rê–Atum. Many pharaohs had their heads carved atop the guardian statues for their tombs to show their close relationship with the powerful solar deity Sekhmet, a lioness. Besides the Great Sphinx, other famous Egyptian sphinxes include one bearing the head of the pharaoh Hatshepsut, with her likeness carved in granite, which is now in the Metropolitan Museum of Art in New York, and the alabaster Sphinx of Memphis, currently located within the open-air museum at that site. The theme was expanded to form great avenues of guardian sphinxes lining the approaches to tombs and temples as well as serving as details atop the posts of flights of stairs to very grand complexes. Nine hundred sphinxes with ram heads, representing Amon, were built in Thebes, where his cult was strongest.
The Great Sphinx has become an emblem of Egypt, frequently appearing on its stamps, coins, and official documents.


== Greece ==

In the Bronze Age, the Hellenes had trade and cultural contacts with Egypt. Before the time that Alexander the Great occupied Egypt, the Greek name, sphinx, was already applied to these statues. The historians and geographers of Greece wrote extensively about Egyptian culture. Herodotus called the ram-headed sphinxes Criosphinxes and called the hawk-headed ones Hieracosphinxes.The word sphinx comes from the Greek Σφίγξ, apparently from the verb σφίγγω (sphíngō), meaning "to squeeze", "to tighten up". This name may be derived from the fact that, in a pride of lions, the hunters are the lionesses, and kill their prey by strangulation, biting the throat of prey and holding them down until they die. However, the historian Susan Wise Bauer suggests that the word "sphinx" was instead a Greek corruption of the Egyptian name "shesepankh", which meant "living image", and referred rather to the statue of the sphinx, which was carved out of "living rock" (rock that was present at the construction site, not harvested and brought from another location), than to the beast itself.Apollodorus describes the sphinx as having a woman's face, the body and tail of a lion and the wings of a bird. Pliny the Elder mentions that Ethiopia produces plenty of sphinxes, with brown hair and breasts. Statius describes her as a winged monster, with pallid cheeks, eyes tainted with corruption, plumes clotted with gore and talons on livid hands. Sometimes, the wings are specified to be those of an eagle, and the tail to be serpent-headed.There was a single sphinx in Greek mythology, a unique demon of destruction and bad luck. According to Hesiod, she was a daughter of Orthrus and either Echidna or the Chimera, or perhaps even Ceto;. According to Apollodorus and Lasus, she was a daughter of Echidna and Typhon. The Sphinx is called Phix (Φίξ) by Hesiod in line 326 of the TheogonyThe sphinx was the emblem of the ancient city-state of Chios, and appeared on seals and the obverse side of coins from the 6th century BCE until the 3rd century CE.


=== Riddle of the Sphinx ===

The Sphinx is said to have guarded the entrance to the Greek city of Thebes, asking a riddle to travellers to allow them passage. The exact riddle asked by the Sphinx was not specified by early tellers of the myth, and was not standardized as the one given below until late in Greek history.

It was said in late lore that Hera or Ares sent the Sphinx from her Aethiopian homeland (the Greeks always remembered the foreign origin of the Sphinx) to Thebes in Greece where she asked all passersby the most famous riddle in history: "Which creature has one voice and yet becomes four-footed and two-footed and three-footed?" She strangled and devoured anyone who could not answer. Oedipus solved the riddle by answering: "Man—who crawls on all fours as a baby, then walks on two feet as an adult, and then uses a walking stick in old age". By some accounts (but much more rarely), there was a second riddle: "There are two sisters: one gives birth to the other and she, in turn, gives birth to the first. Who are the two sisters?" The answer is "day and night" (both words—ἡμέρα and νύξ, respectively—are feminine in Ancient Greek). This second riddle is also found in a Gascon version of the myth and could be very ancient.Bested at last, the Sphinx then threw herself from her high rock and died; or, in some versions Oedipus killed her. An alternative version tells that she devoured herself. In both cases, Oedipus can therefore be recognized as a "liminal" or threshold figure, helping effect the transition between the old religious practices, represented by the death of the Sphinx, and the rise of the new, Olympian gods.


==== The riddle in popular culture ====
In Jean Cocteau's retelling of the Oedipus legend, The Infernal Machine, the Sphinx tells Oedipus the answer to the riddle in order to kill herself so that she did not have to kill anymore, and also to make him love her. He leaves without ever thanking her for giving him the answer to the riddle. The scene ends when the Sphinx and Anubis ascend back to the heavens.
There are mythic, anthropological, psychoanalytic and parodic interpretations of the Riddle of the Sphinx, and of Oedipus's answer to it. Sigmund Freud describes "the question of where babies come from" as a riddle of the Sphinx.Numerous riddle books use the Sphinx in their title or illustrations.Michael Maier, in his book the Atalanta Fugiens (1617) writes the following remark about the Sphinx's riddle, in which he states that its solution is the Philosopher's Stone:

Sphinx is indeed reported to have had many Riddles, but this offered to Oedipus was the chief, "What is that which in the morning goeth upon four feet; upon two feet in the afternoon; and in the Evening upon three?" What was answered by Oedipus is not known. But they who interpret concerning the Ages of Man are deceived. For a Quadrangle of Four Elements are of all things first to be considered, from thence we come to the Hemisphere having two lines, a Right and a Curve, that is, to the White Luna; from thence to the Triangle which consists of Body, Soul and Spirit, or Sol, Luna and Mercury. Hence Rhasis in his Epistles, "The Stone," says he, "is a Triangle in its essence, a Quadrangle in its quality."

		
		
		


== South and Southeast Asia ==

A composite mythological being with the body of a lion and the head of a human being is present in the traditions, mythology and art of South and Southeast Asia. Variously known as purushamriga (Sanskrit, "man-beast"), purushamirugam (Tamil, "man-beast"), naravirala (Sanskrit, "man-cat") in India, or as nara-simha (Sanskrit, "man-lion") in Sri Lanka, manusiha or manuthiha (Pali, "man-lion") in Myanmar, and norasingh (from Pali, "man-lion", a variation of the Sanskrit "nara-simha") or thep norasingh ("man-lion deity"), or nora nair in Thailand. Although, just like the "nara-simha", he has a head of a lion and the body of a human.
In contrast to the sphinxes in Egypt, Mesopotamia, and Greece, of which the traditions largely have been lost due to the discontinuity of the civilization, the traditions related to the "Asian sphinxes" are very much alive today. The earliest artistic depictions of "sphinxes" from the South Asian subcontinent are to some extent influenced by Hellenistic art and writings. These hail from the period when Buddhist art underwent a phase of Hellenistic influence. Numerous sphinxes can be seen on the gateways of Bharhut stupa, dating to the 1st century B.C.In South India, the "sphinx" is known as purushamriga (Sanskrit) or purushamirugam  (Tamil), meaning "human-beast". It is found depicted in sculptural art in temples and palaces where it serves an apotropaic purpose, just as the "sphinxes" in other parts of the ancient world. It is said by the tradition, to take away the sins of the devotees when they enter a temple and to ward off evil in general. It is therefore often found in a strategic position on the gopuram or temple gateway, or near the entrance of the sanctum sanctorum.

The purushamriga plays a significant role in daily as well as yearly ritual of South Indian Shaiva temples. In the shodhasha-upakaara (or sixteen honors) ritual, performed between one and six times at significant sacred moments through the day, it decorates one of the lamps of the diparadhana or lamp ceremony. And in several temples the purushamriga is also one of the vahana or vehicles of the deity during the processions of the Brahmotsava or festival.

In Kanya Kumari district, in the southernmost tip of the Indian subcontinent, during the night of Shiva Ratri, devotees run 75 kilometres while visiting and worshiping at twelve Shiva temples. This Shiva Ottam (or Run for Shiva) is performed in commemoration of the story of the race between the Sphinx and Bhima, one of the heroes of the epic Mahabharata.
The Indian conception of a sphinx that comes closest to the classic Greek idea is in the concept of the Sharabha, a mythical creature, part lion, part man and part bird, and the form of Sharabha that god Shiva took on to counter Narasimha's violence.
In Sri Lanka and India, the sphinx is known as narasimha or man-lion. As a sphinx, it has the body of a lion and the head of a human being, and is not to be confused with Narasimha, the fourth reincarnation of the deity Vishnu; this avatar or incarnation is depicted with a human body and the head of a lion. The "sphinx" narasimha is part of the Buddhist tradition and functions as a guardian of the northern direction and also was depicted on banners.
In Burma, the sphinx is known as manussiha (manuthiha). It is depicted on the corners of Buddhist stupas, and its legends tell how it was created by Buddhist monks to protect a new-born royal baby from being devoured by ogresses.

Nora Nair, Norasingh and Thep Norasingh are three of the names under which the "sphinx" is known in Thailand. They are depicted as upright walking beings with the lower body of a lion or deer, and the upper body of a human. Often they are found as female-male pairs. Here, too, the sphinx serves a protective function. It also is enumerated among the mythological creatures that inhabit the ranges of the sacred mountain Himapan.


== Europe ==

The revived Mannerist sphinx of the late 15th century is sometimes thought of as the "French sphinx". Her coiffed head is erect and she has the breasts of a young woman. Often she wears ear drops and pearls as ornaments. Her body is naturalistically rendered as a recumbent lioness. Such sphinxes were revived when the grottesche or "grotesque" decorations of the unearthed  Domus Aurea of Nero were brought to light in late 15th-century Rome, and she was incorporated into the classical vocabulary of arabesque designs that spread throughout Europe in engravings during the 16th and 17th centuries. Sphinxes were included in the decoration of the loggia of the Vatican Palace by the workshop of Raphael (1515–20), which updated the vocabulary of the Roman grottesche.
The first appearances of sphinxes in French art are in the School of Fontainebleau in the 1520s and 1530s and she continues into the Late Baroque style of the French Régence (1715–1723). From France, she spread throughout Europe, becoming a regular feature of the outdoors decorative sculpture of 18th-century palace gardens, as in the Upper Belvedere Palace in Vienna, Sanssouci Park in Potsdam, La Granja in Spain, Branicki Palace in Białystok, or the late Rococo examples in the grounds of the Portuguese Queluz National Palace (of perhaps the 1760s), with ruffs and clothed chests ending with a little cape.

Sphinxes are a feature of the neoclassical interior decorations of Robert Adam and his followers, returning closer to the undressed style of the grottesche. They had an equal appeal to artists and designers of the Romanticism and subsequent Symbolism movements in the 19th century. Most of these sphinxes alluded to the Greek sphinx and the myth of Oedipus, rather than the Egyptian, although they may not have wings.


== Freemasonry ==

The sphinx imagery has historically been adopted into Masonic architecture and symbology.Among the Egyptians, sphinxes were placed at the entrance of the temples to guard their mysteries, by warning those who penetrated within that they should conceal a knowledge of them from the uninitiated. Champollion said that the sphinx became successively the symbol of each of the gods. The placement of the sphinxes expressed the idea that all the gods were hidden from the people, and that the knowledge of them, guarded in the sanctuaries, was revealed to initiates only.
As a Masonic emblem, the sphinx has been adopted as a symbol of mystery, and as such often is found as a decoration sculptured in front of Masonic temples, or engraved at the head of Masonic documents. It cannot, however, be properly called an ancient, recognized symbol of the order. Its introduction has been of comparatively recent date, and rather as a symbolic decoration than as a symbol of any particular dogma.


== Similar hybrid creatures ==


=== With feline features ===
Gopaitioshah – The Persian Gopat or Gopaitioshah is another creature that is similar to the Sphinx, being a winged bull or lion with human face. The Gopat have been represented in ancient art of Iran since late second millennium BCE, and was a common symbol for dominant royal power in ancient Iran. Gopats were common motifs in the art of Elamite period, Luristan, North and North West region of Iran in Iron Age, and Achaemenid art, and can be found in texts such as the Bundahishn, the Dadestan-i Denig, the Menog-i Khrad, as well as in collections of tales, such as the Matikan-e yusht faryan and in its Islamic replication, the Marzubannama.
Löwenmensch figurine – The 32,000-year-old Aurignacian Löwenmensch figurine, also known as "lion-human" is the oldest known anthropomorphic statue, discovered in the Hohlenstein-Stadel, a German cave in 1939.
Manticore – The Manticore (Early Middle Persian: Mardyakhor or Martikhwar, means: Man-eater) is an Iranian legendary hybrid creature and another similar creature to the sphinx.
Narasimha – Narasimha ("man-lion") is described as an incarnation (Avatar) of Vishnu within the Puranic texts of Hinduism who takes the form of half-man/half-Asiatic lion, having a human torso and lower body, but with a lion-like face and claws.


=== Without feline features ===
Not all human-headed animals of antiquity are sphinxes. In ancient Assyria, for example, bas-reliefs of shedu bulls with the crowned bearded heads of kings guarded the entrances of temples.
Many Greek mythological creatures who are archaic survivals of previous mythologies with respect to the classical Olympian mythology, like the centaurs, are similar to the Sphinx.


== Gallery ==

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		


== See also ==


== Notes ==


== References ==
Clay, Jenny Strauss, Hesiod's Cosmos, Cambridge University Press, 2003. ISBN 978-0-521-82392-0.
Stewart, Desmond. Pyramids and the Sphinx. [S.l.]: Newsweek, U.S., 72. Print.
Taheri, Sadreddin (2013). "Gopat (Sphinx) and Shirdal (Gryphon) in the Ancient Middle East". Tehran: Honarhay-e Ziba Journal, Vol. 17, No. 4.
Kallich, Martin. "Oedipus and the Sphinx." Oedipus: Myth and Drama. N.p.: Western, 1968. N. pag. Print.


== Further reading ==
Dessenne, André. La Sphinx: Étude iconographique (in French). De Boccard, 1957.


== External links ==
Sphinx Head Found in Greek Tomb
The Sphinx: Totally Random Trivia - slideshow by Life magazine