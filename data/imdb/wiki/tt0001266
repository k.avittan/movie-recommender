In Enemy Hands is a 2004 American submarine film directed by Tony Giglio and starring William H. Macy, Til Schweiger, Thomas Kretschmann, Scott Caan and Lauren Holly. The film follows an American submarine crew getting captured by a German submarine crew and taken prisoner aboard their U-boat.


== Plot ==
The movie begins with old film footage of World War II with a narrator explaining that Germany produced hundreds of U-boats to control the Atlantic. In 1942, groups of U-boats known as "wolfpacks" sank over 1,000 Allied ships. The Germans began winning the war and if they continue destroying the Allies, Europe will fall. In 1943, Roosevelt and Churchill declared that stopping the U-boats was their main priority. With new technology and the United States committed to the war, the Allies begin destroying the U-boats and bringing an end to the wolfpacks.
In June 1943, Lt. Cmdr. Randall Sullivan (Caan) talks about his upcoming mission with Admiral Kentz (Berkeley). Kentz asks about Sullivan's COB Nathan Travers (Macy) and says he's a good man and Sullivan could learn something from him and bids Sullivan farewell. Meanwhile, Travers prepares to leave home as his wife Rachel (Holly) makes him promise to come home safe. Two months later, Travers is on board the fictional USS Swordfish (based on the real submarine USS Swordfish in World War II), captained by Sullivan where they do constant drill exercises. Meanwhile, the fictional U-429 (based on the real U-429 submarine), captained by Jonas Herdt (Schweiger), momentarily survives and destroys an American destroyer. After playing chess with his watch officer Ludwig Cremer (Kretschmann), Jonas receives a message from home that Hamburg got bombed, where his daughter's school was destroyed and there were no survivors, implying that she died. On the Swordfish, XO Teddy Goodman (Gregg) becomes increasingly sick with a rash on his stomach, which the doctor believes is meningitis, a contagious disease that can be fatal in some cases. Unknown to the crew, Sullivan has a rash on his arm, showing that he's contracted meningitis as well. Another U-boat, the U-821, sinks the British merchant vessel Achilles.
Radio operator Virgil Wright (Huntington) hears music played by Glenn Miller from the U-821, and Sullivan prepares the crew to attack. They manage to destroy the U-821, but the U-boat fires a torpedo before being hit. The torpedo hits near and damages the Swordfish, killing most of the crew, including Goodman, who dies from his sickness. Travers, Sullivan and six other crew members: Wright, Abers (Sisto), Ox (Gallagher), Miller (Somerholder), Cooper (Giovinazzo) and Romano (Morgan) abandon ship and are taken prisoner by the U-429. They're split up in two groups: Travers, Ox, Cooper and Miller in the bow and Sullivan, Wright, Abers and Romano in the stern. Wright nurses Sullivan and discovers his rash, where Abers recognizes it as meningitis and the group realizes that if the Germans don't kill them, the disease will. Days later, when the Germans prepare to attack an American destroyer, the fictional USS Logan (based on the real USS Logan), Travers and his group break free and stop the launch. The Logan attacks the U-429 with depth charges that makes Sullivan's group break free too, where Sullivan gets killed in the process. Meanwhile, the meningitis spreads and kills two thirds of the German crew and Romano. Later on, Travers has an hallucination of Rachel, who reminds him of his promise to come home.
With no other choice, Jonas decides to have Travers' men work with his crew in order to save them all by going to the US coast and be taken into custody. As both crews reluctantly work together, Jonas explains to Travers that he saved Travers' men because it was protocol to capture only the captain and COB of an enemy ship. He personally saved all of them because he's grown tired of the war and he felt strong for himself by saving lives instead of taking them. Jonas says if they come across either enemy, they must guarantee their men will go home. During their travel to the US coast, Klause (Heger), the U-429's quartermaster, becomes disillusioned with Jonas working with the Americans and orchestrates a mutiny, along with two other crew members named Bauer and Christophe. Abers and Wright subdue Christophe, who makes a distress call to other U-boats, and engineer Hans (Thorsen) knocks out Bauer to save Ox. Klause unsuccessfully attempts to use a torpedo to blow up the boat and stabs Jonas in the back until Travers snaps his neck, killing him. With his dying breath, Jonas gives command of the boat to Cremer. The crew decides to make contact with the Logan but they're attacked by the U-1221, another U-boat that responded to the distress call. Enduring heavy damage as they evade every torpedo attack, the crew uses the last torpedo to try to destroy the U-1221, but it doesn't detonate. The Logan locates and destroys the U-1221 with its cannons. When Travers makes contact with the Logan, Captain Samuel Littleton (Ellis) orders Travers to take the Enigma. Travers falsifies that they're sinking and disconnects with the Logan, keeping his promise to Cremer to never let the U-429 be captured. The crew floods the boat and are rescued by the Logan.
Returning home, Travers argues with Kentz about the Germans saving their lives. Kentz says the Germans are still the enemy but he'll do his best to have them taken care of. Travers and Rachel are reunited and they go visit Cremer in a POW compound, where Rachel thanks Cremer for saving her husband's life. Travers gives him cigarettes and tells Cremer it's good to see him as Travers leaves and Cremer watches on.


== Cast ==
William H. Macy as COB Nathan Travers
Til Schweiger as Kapitän Jonas Herdt
Scott Caan as Lieutenant-Commander Randall Sullivan
Thomas Kretschmann as 1st Watch Officer Ludwig Cremer
Lauren Holly as Rachel Travers
Connor Donne as Lieutenant Bauer
Clark Gregg as Lieutenant Teddy Goodman
Carmine Giovinazzo as Cooper
Sam Huntington as Virgil Wright
Jeremy Sisto as Abers
Ian Somerholder as Miller
Branden Morgan as Romano
Patrick Gallagher as Ox
Chris Ellis as Captain Samuel Littleton
Rene Heger as Klause
Sven-Ole Thorsen as Hans


== Alternate titles ==
When the film was shown on Albanian television, it was given a title that translates as Underwater Prison in English, and had similar titles in some other countries' TV schedules U-429: Underwater Prison in Armenia.


== References ==


== External links ==
In Enemy Hands on IMDb
In Enemy Hands at Rotten Tomatoes