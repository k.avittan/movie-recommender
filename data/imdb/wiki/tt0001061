Elopement, colloquially speaking, is often used to refer to a marriage conducted in sudden and secretive fashion, usually involving a hurried flight away from one's place of residence together with one's beloved with the intention of getting married. Elopements, in which a couple runs away together and seeks the consent of their parents later,  differ greatly from abductions and bride kidnapping in which there is no consent of the bride and/or groom. To elope, most literally, means to run away and to not come back to the point of origin.


== Background ==
Today the term "elopement" is colloquially used for any marriage performed in haste, with a limited public engagement period or without a public engagement period. Some couples elope because they wish to avoid parental or religious objections. In addition, the term elopement is used in psychiatric hospitals to refer to a patient leaving the psychiatric unit without authorization.In some modern cases, the couple collude together to elope under the guise of a bride kidnapping, presenting their parents with a fait accompli. In most cases, however, the men who resort to capturing a wife are often of lower social status, because of poverty, disease, poor character or criminality. They are sometimes deterred from legitimately seeking a wife because of the payment the woman's family expects, the bride price (not to be confused with a dowry, paid by the woman's family).


== Modern elopements ==
As millennials age, they are waiting longer to get married than previous generations. Further, 91% of millennials that are planning on future marriage would consider eloping, and three out of five previously married millennials would elope if they had to do it all over again. Searches for elopement photography ideas on Pinterest increased by 128 percent in 2019, with other related terms like "elopements at city halls" and "elopements in forests" also seeing increases in volume.


=== Modern definition ===
Some view elopement as an intentionally small and intimate wedding experience which allows more emphasis to be placed on the commitment between the couple. Eloping is an opportunity for two people to craft an authentic true-to-them experience that aligns with their ethics and supports their future goals, rather than focus on traditions they may not identify with. 


=== Modern Elopement Attendees ===
Though elopements are thought of as just being the couple and officiant, modern elopements often include close family and friends. Most professionals in the elopement industry view anything under 25 attendees as an elopement. 


== Examples ==


=== United Kingdom ===
In England, a legal prerequisite of religious marriage is the "reading of the banns"—for any three Sundays in the three months prior to the intended date of the ceremony, the names of every couple intending marriage has to be read aloud by the priest(s) of their parish(es) of residence, or the posting of a 'Notice of Intent to Marry' in the registry office for civil ceremonies. The intention of this is to prevent bigamy or other unlawful marriages by giving fair warning to anybody who might have a legal right to object. In practice, however, it also gives warning to the couples' parents, who sometimes objected on purely personal grounds. To work around this law, it is necessary to get a special licence from the Archbishop of Canterbury—or to flee somewhere the law did not apply, across the border to Gretna Green, Scotland, for instance.
For civil marriages notices must be posted for 28 clear days, at the appropriate register office.


=== Philippines ===
In the Philippines, elopement is called "tanan". Tanan is a long-standing practice in Filipino culture when a woman leaves her home without her parents' permission to live a life with her partner. Usually she will elope during the nighttime hours and is awaited by her lover nearby, who then takes her away to a location not of her origin. The next morning, the distraught parents are clueless to the whereabouts of their daughter. Tanan often occurs as a result of an impending arranged marriage or in defiance to parents' dislike of a preferred suitor.


=== Indonesia ===
In Indonesia, an elopement is considered as "kawin lari" or in literal translation, marriage on a run ("kawin", means marriage (slang), "lari" means running/fleeing). This happens if the groom or the bride didn't get the permission to get married with each other. As Indonesia is a religiously strict country, a couple couldn't get married without parent's (or next closest living relative) consent, hence, it is rarely practiced. Thus, most Indonesian couples who engage in elopement often end up marrying without their marriage recognized/registered by the government.


=== West Asia ===
In Assyrian society, elopement ("Jelawta" or "Jenawta") against parental request is very disreputable, and is rarely practised. In the 19th and early 20th century, Assyrians had heavily guarded their females from abduction and also consensual elopement, when it came to their neighbours such as Kurds, Azeris and Turks, who would abduct Assyrian women and marry them, in some cases forcefully, where they would convert them to Islam.


== See also ==
Bride kidnapping
Marriage law


== References ==