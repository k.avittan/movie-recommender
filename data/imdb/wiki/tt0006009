Scarlet Lady is a cruise ship operated by Virgin Voyages. She is the inaugural ship for the cruise line and was delivered on 14 February 2020 by Fincantieri of Italy. She is set to debut on 16 October 2020. She will operate exclusively as an "adults-only" ship for guests aged 18 and over, sailing mainly four-to-five-night Caribbean itineraries from Miami, Florida.


== Construction and career ==


=== Planning ===
On 4 December 2014, founder of Virgin Group, Richard Branson, announced that Virgin Group was forming Virgin Cruises, together with the backing of Bain Capital, and revealed plans to build two new cruise ships.On 23 June 2015, Virgin Cruises announced that it signed a letter of intent with Italian shipbuilder, Fincantieri, for three cruise ships that could each accommodate approximately 2,800 guests and 1,150 crew for seven-day Caribbean voyages. The three-ship order reportedly cost US$2.55 billion, with the first ship scheduled to begin operating in 2020. The order's contract for the ships was formally signed on 18 October 2016, the same day Virgin Cruises rebranded as Virgin Voyages.On 20 July 2018, Virgin Voyages revealed the name of the first ship as Scarlet Lady, in honor of sister brand, Virgin Atlantic's, second plane.


=== Construction and delivery ===
Temporarily billed as Virgin I and known in the shipyard as "hull number 6287," the first ship for Virgin Voyages began construction with the steel-cutting ceremony at Fincantieri's shipyard in Sestri Ponente near Genoa on 22 March 2017.On 31 October 2017, the ship's keel-laying and coin ceremonies were held. On 20 July 2018, the dock was flooded for the first time to allow two hull sections to be connected. The ship was assembled from 399 sections.On 8 February 2019, Scarlet Lady was floated out from the shipyard, accompanied by an ceremony in which a bottle of champagne was triggered to strike the hull. Interior outfitting work began upon her float out.Scarlet Lady performed two rounds of sea trials in November 2019. She set off from Genoa to Marseille between 15 November and 18 November and sailed in the reverse direction for her second round between 27 November and 30 November.Scarlet Lady was completed and presented by Fincantieri on 13 February 2020 and formally delivered to Virgin Voyages on 14 February 2020 in Sestri Ponente. She was scheduled to be christened in Miami on 19 March 2020, but the christening has been postponed, due to the COVID-19 pandemic.


=== Debut ===
The ship's debut and inaugural festivities were all impacted by the pandemic and the cruise line's subsequent suspension of operations.
Scarlet Lady began by hosting media and travel industry representatives on stops in Dover and Liverpool before continuing her re-positioning voyage to North America. Virgin Voyages had prepared preview events in New York City, but cancelled them due to the pandemic, so the ship continued her re-positioning to Miami instead.Upon her arrival in Miami, Scarlet Lady was scheduled to perform two pre-inaugural voyages in late-March 2020. Her first was scheduled from 26 to 29 March and her second from 29 March to 1 April. Both three-night sailings would embark from Miami and visit Virgin Voyages' new private island resort, The Beach Club at Bimini, located on the Bimini islands in the Bahamas.On 12 March 2020, Virgin Voyages had announced an initial postponement of the ship's inaugural season, with the pre-inaugural sailings slated to begin on 15 July 2020. However, these were cancelled in May 2020.Scarlet Lady was originally scheduled to sail her maiden voyage on 1 April 2020, visiting Key West and "The Beach Club" at Bimini, which was initially postponed in March 2020 to 7 August 2020. In May 2020, the cruise line announced that the ship will not begin sailing until 16 October 2020.


=== Deployments ===
On 23 June 2015, Branson initially announced the first three ships in the fleet would be based at PortMiami for week-long, round-trip Caribbean itineraries, with the first ship beginning operations in 2020. In January 2019, Virgin Voyages released details of Scarlet Lady's itineraries, with revisions from earlier plans. She was now scheduled to sail shorter itineraries, performing four-to-five-night round-trip Caribbean cruises to either: Havana, Cuba, Puerto Plata, Dominican Republic, or Costa Maya, Mexico. All voyages would include a stop at The Beach Club at Bimini.In June 2019, in response to the United States' government ban on passengers from entering Cuba via passenger and recreational vessels, Virgin Voyages swapped out all visits to Havana with calls to Key West, Florida. It also added a fourth itinerary option to visit Cozumel, Mexico on five-night sailings, and added two seven-night holiday sailings to Puerto Plata and San Juan, Puerto Rico. All voyages will retain their visits to The Beach Club at Bimini. Scarlet Lady's deployment schedule is expected to continue through 2021.


== Design and concept ==


=== Adults-only ===
On 31 October 2017, Branson and president and CEO of Virgin Voyages, Tom McAlpin, revealed that the company's first ship would be exclusive to adults, requiring that all guests be ages 18 and over. McAlpin explained that this move was made to appeal largely to travel professionals within the more premium market looking for an "elevated" experience. Chief Commercial Officer of Virgin Voyages, Nirmal Saverimuttu, also attributed the decision to the company's aim to target "people who would not typically consider a cruise holiday" and might be interested in a "cooler" and more "intimate" setting.


=== Offerings ===
On Scarlet Lady, Virgin Voyages bundles all passengers' fares as one price per cabin, as opposed to pricing each voyage per guest. Consequently, prices appear higher than those of Virgin's competitors. Virgin explained that all fares would cover dining charges, fitness classes, internet access, and gratuities, among other offerings, but expenses incurred from alcoholic beverages, retail, and shore excursions would be charged separately.The ship does not include any buffet or dining rooms, and instead, houses approximately 20 different dining and drinking establishments. Other venues include a multi-function theater and an onboard tattoo and body piercing parlor.


=== Technology and specifications ===

Scarlet Lady has a total of 1,408 passenger cabins and 813 crew cabins for a maximum capacity of 4,400 passengers and crew, which can accommodate over 2,770 passengers and 1,160 crew. Of the 1,408 passenger cabins, there are 78 suites, 1,030 balcony cabins, 95 window cabins, and 105 inside cabins. She is equipped with balconies on 86% of her cabins, with 93% of the cabins featuring an outside view. The ship's livery consists of a silver hull, red funnel, and tinted windows, and also includes the Virgin "mermaid guide" figure featured on sister brands, Virgin Atlantic and Virgin America.Scarlet Lady has 17 decks, a length of 277.2 metres (909 ft), a draft of 8.05 metres (26.4 ft), and a beam of 41 metres (135 ft). She is powered by a diesel-electric genset system, with four Wärtsilä engines, producing 48 megawatts (64,000 hp). Main propulsion is via two propellers, each driven by a 16 megawatts (21,000 hp) electric motor. The system gives the vessel a service speed of 20 knots (37 km/h; 23 mph) and a maximum speed of 22 knots (41 km/h; 25 mph). Wärtsilä also provided the ship's navigation systems, a hybrid scrubber system to remove sulfur dioxide from the exhaust, and a selective catalytic reduction system to reduce nitrogen oxide emissions. The ship partially produces her own energy through a 1 megawatt (1,300 hp) production system that uses the waste heat from her diesel engines.


== Incidents and accidents ==


=== 2020 crew member death ===

On 22 May 2020, it was reported that a 32-year-old male Filipino crew member of Scarlet Lady had been found dead in his cabin. Virgin Voyages confirmed the man's death was not related to the coronavirus, and the United States Coast Guard confirmed that he had died from "apparent self-harm." Scarlet Lady sailed into PortMiami later that day, where the body was disembarked.


== References ==


== External links ==
 Media related to IMO 9804801 at Wikimedia Commons