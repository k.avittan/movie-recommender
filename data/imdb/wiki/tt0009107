The Range Rider is a 1951-1953 American Western television series that was first transmitted in syndication.  A single lost episode surfaced and was broadcast in 1959. The Range Rider was also broadcast on British television during the 1960s, and in Melbourne, Australia during the 1950s.


== Synopsis ==
Jock Mahoney, later star of CBS's Yancy Derringer, played the title character in seventy-nine black-and-white half-hour episodes, along with partner Dick West, played by Dick Jones, later star of the syndicated series Buffalo Bill, Jr. The character had no name other than Range Rider. His reputation for fairness, fighting ability, and accuracy with his guns was known far and wide, even by Indians. Mahoney towered over Jones, conveying the idea that Dick West was a youth rather than a full-grown adult.Stanley Andrews, the first host of the syndicated anthology series, Death Valley Days, appeared in seventeen episodes of The Range Rider in different roles, including "Pack Rat" and "Marked for Death" in 1951 and "Marshal from Madero" in 1953. Gregg Barton similarly guest starred in sixteen episodes. Harry Lauter, later a co-star with Willard Parker on CBS's Tales of the Texas Rangers, appeared eleven times, including the episodes "Ten Thousand Reward" and "Dim Trails" (both in 1951), "Ambush in Coyote Canyon" (1952), and "Convict at Large" and "Marshal from Madero" (both in 1953). William Fawcett, prior to NBC's Fury, guest starred in nine episodes, including in "Diablo Posse", as Matt Ryan in "Last of the Pony Express", "Dim Trails" (all 1951), and "Shotgun Stage" (1952).The show was a production of Gene Autry's Flying A Productions, and Autry himself was the executive producer. The theme tune was "Home on the Range" though in later episodes this was played at a fast tempo without the song. The two main characters were the only consistent ones. Five to six names of other actors were given at the end of each episode, but not the parts they played.


== Other guest stars ==
Jim Bannon — "Stage to Rainbow's End", "The Crooked Fork", and "Marked for Death" (all 1951), and "Badmen of Rimrock" (1953)
Jeanne Bates — "The Black Terror" (1953)
Pamela Blake — "West of Cheyenne" (1953)
Rand Brooks — "Dim Trails" (1951) and "Shotgun Stage" (1952)
Lonnie Burr — then as a child actor, he appeared as Jimmy in the title role of "The Holy Terror" (1953).
Harry Cheshire — five times, including "The Secret Lode" and "Red Jack" (1951)
Phyllis Coates — twice, including "Pale Horse" (1952)
Steve Conte in "Ten Thousand Reward" (1951)
Gail Davis — twice, including as Ann Carter in "Greed Rides the Range" and in "Outlaw's Double" (both 1952)
Edgar Dearing — "Pack Rat", "Indian Sign", "False Trail", and "The Fatal Bullet" (all 1951)
John Doucette — four times, including "The Border City Affair" (1953)
James Griffith — seven episodes, including "The Flying Arrow" and "Ghost of Poco Loco" (both 1951)
Alan Hale, Jr. — five times, including the episodes "Bad Medicine", "Diablo Posse", and "Last of the Pony Express" (all 1951)
Don C. Harvey — five times, including "The Baron of Broken Bow", "Marked Bullets", and "Red Jack" (all 1951)
Myron Healey — six times, including "Gold Hill" (1952)
Darryl Hickman — in "Fight Town" (1952)
Sherry Jackson — then a child actor in "Dead Man's Shoes" (1951) and "Secret of the Red Raven" (1952)
Brad Johnson —  three times, including "The Border City Affair" and "Bullets and Badmen" (1953)
I. Stanford Jolley — twice, including "The Black Terror" and "Hideout" (both 1953)
Tom Keene — as Lang in "The Grand Fleece" (1951)
Fred Krone — "Convict at Large", "The Buckskin", "The Chase", "Outlaw Territory" (all 1953)
Tom London — "The Hawk" and "Dead Man's Shoes" (both 1951)
Kenneth MacDonald — seven episodes, mostly as a sheriff
Kermit Maynard — "Sealed Justice" (1951) and "Jimmy the Kid" (1952)
Eve Miller — "Stage To Rainbow's End" and "The Crooked Fork" (both 1951)
Ewing Mitchell — eleven episodes, mostly as a law-enforcement officer
Clayton Moore — as Martin Wickett in "Ambush in Coyote Canyon" (1952) and as Dan Meighan in "The Saga of Silver Town" (1953)
Dennis Moore — "Ten Thousand Reward" (1951)
Jimmy Noel, walk-on parts in five episodes (1952-1953)
J. Pat O'Malley — three times, including "Diablo Posse" (1951)
John M. Pickard — "The Holy Terror" and "The Buckskin" (both 1953)
Denver Pyle — fourteen times, including "Six Gun Party", "Gunslinger in Paradise", and "Big Medicine Man" (all 1951)
Mike Ragan — five episodes (1951-1952)
Marshall Reed — "Pack Rat" and "Sealed Justice" (1951)
Gloria Saunders — five episodes
Karen Sharpe — "The Chase" (1953)
Glenn Strange  — twice, including the role of Chief Black Cloud in "Indian War Party" (1952)
Lyle Talbot — four episodes, including "The Secret of Superstition Peak" (1952) and "West of Cheyenne" (1953)
Gloria Talbott — in "Gold Hill" (1952)
Dub Taylor — three episodes
Minerva Urecal — "Bad Men of Rimrock" and "Outlaw Territory" (both 1953)
Lee Van Cleef — three times, including as Rocky Hatch in "Greed Rides the Range" (1952)
Pierre Watkin — twice, including "Blind Canyon" (1952)
Robert J. Wilke — eight episodes, including "Right of Way" (1951)
Gloria Winters — four times, including "Pack Rat", as Sally Roberts in "Ghost of Poco Loco" (both 1951), and "Blind Canyon" (1952)
Sheb Wooley — four times, including "The Treasure of Santa Dolores" and "The Old Timer's Trail" (both 1953), and "Outlaw Pistols" (1953, the series finale)
Chief Yowlachie — twice, "Sealed Justice" and "Big Medicine Man" (1951)


== DVD release ==
In 2006, Timeless Media Group released a licensed 2-DVD (Region 1), 10 episode best-of collection. Subsequently, a second licensed set was released, this time consisting of 20 episodes on six DVDs.Between 2005–2007, Alpha Home Entertainment released five unlicensed best-of DVDs (region 0), with four episodes on each.Though the series is not actually in the public domain, various episodes also appear in numerous unlicensed budget TV western DVD collections.


== Cultural references ==
In The A-Team episode When you Comin' Back, Range Rider? (Season 2, episodes 5-6), Murdock is seen watching an episode of The Range Rider in his room at the psychiatric hospital. He adopts the persona of the Range Rider as the team pursues wild mustang rustlers and is frequently seen wearing a mask of the Range Rider he cut from a cereal box.


== References ==


== External links ==
The Range Rider on IMDb