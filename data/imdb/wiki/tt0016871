Fine Manners is a 1926 American black-and-white silent comedy film directed initially by Lewis Milestone and completed by Richard Rosson for Famous Players-Lasky/Paramount Pictures. After an argument with actress Gloria Swanson, director Milestone walked off the project, causing the film to be completed by Rosson, who had picked up directorial tricks while working as an assistant director to Allan Dwan. The success of the film, being Rosson's first directorial effort since he co-directed Her Father's Keeper in 1917 with his brother Arthur Rosson, won him a long-term contract with Famous Players-Lasky.


== Plot ==
Burlesque chorus girl Orchid Murphy (Gloria Swanson) attracts the attention of wealthy Brian Alden (Eugene O'Brien), who is posing as a writer while "slumming" in the city. Finding her manner quite refreshing compared to the women he usually meets in his circle, he falls in love with her and confesses his wealth. After she agrees to marriage, he leaves for a six-month tour of South America, and Orchid takes a course in "fine manners" to better prepare herself for Brian's world. She becomes too polished, however, and when asked by Brian to marry him upon his return, is happy to become herself again.


== Cast ==
Gloria Swanson as Orchid Murphy
Eugene O'Brien as Brian Alden
Helen Dunbar as Aunt Agatha
Roland Drew as Buddy Murphy
John Miltern as Courtney Adams
Jack La Rue as New Year's Eve Celebrant
Ivan Lebedeff as Prince


== Critical reception ==
Berkeley Daily Gazette wrote that in her first time in the role of a burlesque chorus girl, Gloria Swanson is "better than ever" and has "added another interesting screen portrayal to her long list of successes."  Miami News called the film "a most laughable comedy" and reported "Critics say this is Gloria's triumph".  St. Petersburg Times wote that Fine Manners stands head and shoulders above anything Gloria has done for the past year," and note that the story was written specifically for her. They wrote that the film "will prove to be the star's most popular vehicle."Conversely, The New York Times noted  that Fine Manners was reminiscent of George Bernard Shaw's play, Pygmalion, writing "The photoplay has been constructed with meticulous attention to the edicts of the movie school of conventionalities; true characterization, intrigue and subtlety are conspicuously absent. Still, the idea of introducing a chorus girl from a burlesque show and having her try valiantly to grasp the ways of a less demonstrative society, does bring to mind Shaw's cockney heroine."  They noted that writers underscore the differences between the societal ranks of the two protagonists by emphasizing Orchid's ignorance of social amenities and by her being assigned a common name.  While granting that there are scenes in which the cinematography is clever, they made note that the story itself is not very absorbing.


== Preservation ==
The film has survived the decades and is preserved in several archive houses such as George Eastman House, The Library of Congress and the Museum of Modern Art.


== References ==


== External links ==
Fine Manners on IMDb
Fine Manners synopsis at AllMovie