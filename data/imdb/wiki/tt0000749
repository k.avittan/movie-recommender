Rescued from an Eagle's Nest is a 1908 American silent action-drama film produced by Edwin S. Porter for Edison Studios and directed by J. Searle Dawley. It features the first leading screen role of the legendary American filmmaker D. W. Griffith, whose directorial debut was released just six months after he performed in this production. Griffith's casting in the Edison "photoplay" began when he found himself stranded and broke in New York City after a play he authored had failed. Desperate for money, he responded to Edison's offer to pay $15 to anyone who submitted a useable treatment or scenario based on the Puccini opera Tosca. Porter rejected Griffith's submission, but the studio executive did offer him the lead role in Rescued from an Eagle's Nest. A full print of this film survives in the extensive collection of moving images at the Museum of Modern Art. Only one other film in which Griffith appears as an actor survives. 


== Plot ==
A woodsman leaves a hut followed by a woman with their baby. Nearby some men chop down a tree. The baby is left outside the hut, but an eagle flies away with it. The mother comes outside and sees what has happened. She picks up a gun and aims, but decides against it. She tells the woodsmen and they get to the cliff where the eagle's nest is. One of the men is let down on a rope to the nest. However the eagle attacks, but he kills it and kicks it off the cliff. He then picks up the baby, is hoisted up the cliff, and returns the baby to its mother.


== Cast ==
D. W. Griffith as father
Henry B. Walthall as fellow woodsman
Miss Earle as mother
Jinnie Frazer as Baby


== Production and reception ==
Most of the production was filmed indoors on sets at Edison's new studio facilities located at the intersection of Decatur Avenue and Oliver Place in the Bronx. The brief outdoor footage showing the woodsmen chopping down a tree was taken at an undetermined site, although the location was likely a short distance north of the Bronx. 
Although very primitive by modern film-production standards, the simulations presented on screen of a stuffed, articulated eagle flapping its wings while appearing to hold a real child in flight no doubt thrilled some filmgoers in 1908; however, the reviewer at the time for The Moving Picture World found the overall effect unconvincing. "The trick", he remarked, "of the eagle with its wire wings is too evident to the audience, while the fight between the man and eagle is poor and out of vision."


== References ==


== See also ==
List of American films of 1908
1908 in film


== External links ==
Rescued from an Eagle's Nest on IMDb