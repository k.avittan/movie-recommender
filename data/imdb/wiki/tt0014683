An Artist's Model is a two-act musical by Owen Hall, with lyrics by Harry Greenbank and music by Sidney Jones, with additional songs by Joseph and Mary Watson, Paul Lincke, Frederick Ross, Henry Hamilton and Leopold Wenzel.  It opened at Daly's Theatre in London, produced by George Edwardes and directed by James T. Tanner, on 2 February 1895, transferring to the Lyric Theatre on 28 May 1895, and ran for a total of 392 performances.  The piece starred Marie Tempest (and later Florence Perry) in the title role, Hayden Coffin, Letty Lind, Leonora Braham, Eric Lewis, Maurice Farkoa, Marie Studholme, and Louie Pounds.  It also had a Broadway run at the former Broadway Theatre from December 21, 1895 through February 8, 1896.The success of A Gaiety Girl in 1893, the first musical by the team of Hall, Greenbank and Jones (followed by another such success, The Shop Girl in 1894), had confirmed to Edwardes that he was on the right track.  He immediately set the team to work on An Artist's Model.  Edwardes wanted his Daly's Theatre musicals to be slightly more sophisticated than his light and simple Gaiety Theatre musicals.  Hall's new book kept the snappy dialogue of the previous work, but paired it with a romantic plot, tacked on at the last minute when Edwardes managed to engage the popular Marie Tempest, and a role was quickly written in for her.  This lucky chance set up the formula for a series of successes for the Edwardes-Hall-Jones-Greenbank team at Daly's Theatre.The story is set in France. The eponymous model, having married a millionaire and been left a widow, returns to the studio in order to recover the affections of a lovelorn artist. He repulses her advances and she becomes engaged to an English nobleman, but then the artist woos her. The Times opening night review thought the story was weak (it was likely edited after that) but praised the lyrics and music.An Artist's Model was succeeded by The Geisha, which was to be the biggest international hit the British musical theatre had known, playing for 760 performances in its original London run and thousands of performances on the Continent (one source counts some 8,000 in Germany alone) and in America and then touring for decades in Britain.  Still more hits followed.


== Musical numbers ==
Act I   –   An Artist's Studio in Paris.No. 1. Chorus – "With brush on hand and palette gay our varied talents we display"
No. 2. Carbonnet and Chorus – "Oh, come and peep when the world's asleep at gay Bohemiah"
No. 3. Sir George – "Though pictures as a connoisseur I don't pretend to criticize"
No. 4. Madame Amélie – "A few young ladies I receive to finish at a special fee"
No. 5. Madame Amélie – "It's really hard, when times are bad and tradesmen unforgiving"
No. 6. Rudolph – "Is love a dream that fades with dawn of day, too sweet to last night night has passed away"
No. 7. Daisy – "Oh, I'm a simple little maid who really doesn't know a thing"
No. 8. Chorus and Recitative – Adèle – "Queen of the Studio, welcome right royally!  Where has your Majesty been?"
No. 9. Adèle and Students' Chorus – "What life so sweet, what life so free as that the merry student leads!"
No. 10. Rudolph and Adèle – "Oh, maid of witching grace, mankind at will disarming"
No. 11. Entrance of School Girls – "We six little misses from a French girls' school, an embodiment of blisses"
No. 12. Algernon, Apthorpe and Carbonnet – "Now won't you come along with us and have a jolly lark?"
No. 13. Finale Act I – "Ah, here is the truant at last!  Oh, Daisy, what have you been doing?"Act II   –   Ball-room in a Country House.No. 14. Chorus – "Number five at last!  Now don't forget it ends the set, so hurry through it fast"
No. 15. Daisy and Chorus – "A Tom-tit lived in a tip-top tree, and a mad little, bad little bird was he"
No. 16. Concerted Piece – "We've reached our destination, and I'm glad of it!"
No. 17. Algernon, Madame Amélie and Sir George – "By a pretty little proverb, it was settled long ago"
No. 18. Adèle – "Sundown and dark, and over me the spell of shadowland"
No. 19. Rudolph and Chorus – "The dearest spot on the wide, wide earth to the heart of a man of English birth"
No. 20. Laughing Song – Carbonnet – "In London at the present day I love to spend my money"
No. 21. Fancy Dress Lancers
No. 22. Valse Chantée – "Music and laughter float on the air.  Tears may come after; why should we care?"
No. 23. Daisy – "When people doze, or criticize and stare in good Societee"
No. 24. Dance – Sir Roger de Coverley
No. 25. Rudolph – "Moon in the blue above, pale is your silver light"
No. 26. Finale – "On y revient toujours!  We come with hearts grown fonder, back to the life that each of us loves best!"Supplementary Numbers.No. 27. Adèle – "Love is a man's delight, a fancy of today!"
No. 28. Maud and Carbonnet – "I'm glad that Paris pleases you ... It charms me altogether"
No. 29. Carbonnet – "I've met my fate, I am in love with Trilby"
No. 30. Cripps and Chorus – "Though round the world I've often been with Cook's or else with Gaze's"
No. 31. Madame Amélie – "Do you remember all the bonnets that you bought?"
No. 32. Adèle – "On a silent summer's night, when the moon shone clear and bright"
No. 33. Adèle – "Oh, what would women do, ha, ha! if men were all like you, ha, ha!"
No. 34. Madame Amélie – "Mon militaire big and brave, he means to try the married bliss"
No. 35. Sir George, Amélie, Algernon & Smoggins – "Though neglected in the past, they've created me at last."
No. 36. "Ta-Ta Land" – "A noble dame some children had, up to ev'ry game"
No. 37. Chorus – "Hands Off!" – "England to arms! the need is nigh, the danger at your gate"
No. 38. Chorus – "Henrietta" – "In a quiet little village not so very far away"


== Roles and original cast ==
Adéle (a rich widow, formerly an Artist's Model) – Marie Tempest
Lady Barbara Cripps – Leonora Braham
Lucien (a French schoolboy) – Nina Cadiz
Jessie, Rose, Christine, Ruby and Violet (art students) – Marie Studholme,  Kate Cannon, Alice Davis, Kate Adams and Lettice Fairfax
Geraldine (a Model) – Hetty Hamer
Amy Cripps – Louie Pounds
Jane – Sybil Grey
Miss Manvers – Nellie Gregory
Daisy Vane – (Sir George St. Alban's ward) – Letty Lind
Rudolph Blair – (an Art Student) – C. Hayden Coffin
Sir George St. Alban – (a Diplomatist) – Eric Lewis
Archie Pendillon   (an Art Student) – Yorke Stephens
Earl of Thamesmead (Lady Barbara's brother) – Lawrance D'Orsay
Algernon St. Alban (Sir George's son) – Farren Soutar
Carbonnet, Apthorpe and Maddox (art students) – Maurice Farkoa, Gilbert Porteous and  Conway Dixon
James Cripps (Lady Barbara's husband) – E. M. Robson (later Leedham Bantock)
Smoggins – W. Blakeley
Mme. Amélie (a Schoolmistress in Paris) – Lottie Venne (later Lydia Thompson and then Juliette Nesville.


== References ==


== External links ==
Vocal score of An Artist's Model
Profile of Hall discussing An Artist's Model and other shows
List of musical numbers and links to MIDI files
Listing of Sidney Jones shows
Daly's Theatre site includes links to programme and other images