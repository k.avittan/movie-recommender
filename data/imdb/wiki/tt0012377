Last Chance U is an American documentary web television series that is produced and premiered by Netflix. The six-episode first season explores the football program at East Mississippi Community College, which features several collegiate athletes that have had trouble in their lives and struggled with finding structure. The players are then required to perform at the junior college (JUCO) level, under the stewardship of coach Buddy Stephens, in order to prove themselves and return to Division I.The series' second season returned to Mississippi, but transitioned to Independence Community College in Kansas for the show's third season, which premiered on July 21, 2018. This was followed by a return to Independence for the fourth season; it debuted on July 19, 2019. The final season took place at Laney College in Oakland, California and premiered on July 28, 2020. In 2020, it was announced that a scripted drama based on the first two seasons would be produced by and starring Courteney Cox.


== Synopsis ==
The first two seasons focus on all aspects of the football program at East Mississippi Community College, one of the most successful JUCO programs in the country. Major themes include the academic struggles of the players - some of whom have come from severely disadvantaged backgrounds. This is set against an overall redemption and coming-of-age “last chance” theme for the group  of men struggling to find their place. Team academic advisor Brittany Wagner is featured prominently as she is tasked with getting all team members to graduate on time. Head coach Buddy Stephens' struggles with controlling his temper is also a major theme, which is often juxtaposed with his devout Christian faith that he attempts to impart on the team.


=== Season 1 ===
The crew followed the EMCC Lions during their 2015 season as they attempted to capture their fourth JUCO national title. While the team appeared dominant for much of the year, their season was derailed after a brawl broke out during their game with Mississippi Delta. EMCC was disqualified from the state playoffs and a potential berth to the national championship game.Ranking: NJCAA released prior to game.


=== Season 2 ===
Netflix returned to Scooba to follow their 2016 season. Once again holding national championship aspirations, the team faced a major hurdle in that only 32 of their players were eligible for their opening game with Jones County Junior College due to suspensions related to the previous season's brawl. EMCC lost that game, 27–25, their first season-opening loss since 2010. The Lions would go on to win the rest of their games, but were left out of the national championship game when they finished the season ranked No. 3 in the polls.Ranking: NJCAA released prior to game.


=== Season 3 ===
Despite being invited back to EMCC for a third season, producers decided to move the show to Independence Community College of Kansas.  The new location is different in that ICC has historically had much lower expectations than EMCC; in 2016, it ended the season 5–4, its first winning season in ten years.  The ICC Pirates had a very successful recruiting campaign for the 2017 season, landing many acclaimed players who began at NCAA Division I schools.
Jeff Carpenter, the long-time Voice of the Indy Pirates delivers the back story of the team and the town of Independence, KS.
Ranking: NJCAA released prior to game.


=== Season 4 ===
The fourth season continues in Independence, where the team fails to live up to high preseason expectations, finishing 2–8. After the season, Coach Brown is forced to resign for insensitive remarks. The season received the 2020 Emmy Award for Outstanding Serialized Sports Documentary.Ranking: NJCAA released prior to game.


=== Season 5 ===
The fifth season takes place in Oakland, California at Laney College alongside football head coach John Beam. It premiered in July 2020.


== Reception ==
The series was given a positive review by SB Nation's Jason Kirk, who summed it up as a "carefully crafted drama with personalities to care about."


== Featured staff ==


=== EMCC ===
Buddy Stephens (head coach)
Brittany Wagner (academic advisor)
Marcus Wood (offensive coordinator)
Davern Williams (defensive line coach)
Ed Holly (defensive coordinator, season 2)
Clint Trickett (quarterbacks coach)
Cade Wilkerson (running backs coach)


=== ICC ===
Jason Brown (head coach)
Jason Martin (defensive coordinator, secondary coach)
Kiyoshi Harris (offensive coordinator, offensive line coach)
Frank Diaz (quarterback coach)
Raechal Martin (head athletic trainer)
Tammy Geldenhuys (athletic director)
Latonya Pinkard (English teacher, associate professor)
Heather Mydosh (English teacher)
Daniel Barwick (president)
Jeff Carpenter (voice of the Pirates)


=== Laney ===
John Beam (head coach, athletic director)
Josh Ramos (defensive coordinator, assistant head coach)
Jeff Haagenson (offensive coordinator)
Kevin Evans (offensive line coach)
Bryan Coughlan (defensive line coach)
Rob Crowley (quarterback coach)
Adam Robinson (wide receivers coach)
Derrick Gardner (cornerbacks coach)
Rick Becker (trainer)


== Players ==


== Crew ==
Benjamin Cotner – executive producer
Edgar Doumerc – sound department
Joe Labracio – executive producer
Adam Leibowitz – producer
Lisa Nishimura – executive producer
Dawn Ostroff – executive producer
Adam Ridley – producer, director, editor
Jihan Robinson – executive producer
James D. Stern – executive producer
Lucas Smith – executive producer
Greg Whiteley – director, executive producer
Sam Young – sound department
Yuri Tománek – original music
Joseph Minadeo – original music


== References ==


== External links ==
Official website 
Last Chance U on IMDb
EMCC athletics
ICC athletics