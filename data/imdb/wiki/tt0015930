The House Without a Key is a novel that was written in 1925 by Earl Derr Biggers.  It is the first of the Charlie Chan mysteries written by Biggers.
The novel, which takes place in 1920s Hawaiʻi, spends time acquainting the reader with the look and feel of the islands of that era from the standpoint of both white and non-white inhabitants, and describes social class structures and customs which have largely vanished in the 21st century.


== Plot summary ==
The novel deals with the murder of a former member of Boston society who has lived in Hawaiʻi for a number of years.  The main character is the victim's nephew, a straitlaced young Bostonian bond trader, who came to the islands to try to convince his aunt Minerva, whose vacation has extended many months, to return to Boston.  The nephew, John Quincy Winterslip, soon falls under the spell of the islands himself, meets an attractive young woman, breaks his engagement to his straitlaced Bostonian fiancee Agatha, and decides as the murder is being solved to move to San Francisco.  In the interval, he is introduced to many levels of Hawaiian society and is of some assistance to Detective Charlie Chan in solving the mystery.
The novel's denouement is nearly identical to that in the final Perry Mason novel by Erle Stanley Gardner, The Case of the Postponed Murder (1970).


== Literary significance and criticism ==
The novel is remarkable in two respects. First, although Chan is ostensibly the detective, his role in the book is fairly small. He does figure out the solution to the case, but it is at the same time as Winterslip, and it is the Bostonian who has the honor of collaring the murderer.
Second, the novel's portrayal of the Chinese, specifically Charlie Chan, is forward-looking for its era. The Bostonians find it hard to accept a Chinese detective on the case, but the locals know him by reputation and show him respect. While some of the descriptions evince some of the stereotypes of the day, Chan is portrayed sympathetically, as an equal to the whites that surround him. (Indeed, Charlie Chan's superior, a white Captain of Detectives, puts him on the case and trusts him completely.)


== Adaptations ==
It was adapted for film twice, as The House Without a Key in 1926 and as Charlie Chan's Greatest Case in 1933.


== Trivia ==
Charlie Chan does not speak his first word until page 82 (first paperback edition).The novel was written by Biggers at the Halekulani hotel on Waikīkī Beach which features a restaurant named "House Without a Key".


== References ==


== External links ==
The House Without a Key at Faded Page (Canada)
Charlie Chan, The Enduring Detective by Marv Lachman