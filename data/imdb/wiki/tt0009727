The Turn of a Friendly Card is the fifth studio album by the British progressive rock band The Alan Parsons Project, released in 1980 by Arista Records. It is a concept album that revolves around the theme of gambling and of the inherent effects that arise from gambling addiction.  The title piece, which appears on side 2 of the LP, is a 16-minute suite broken up into five tracks, with the five tracks listed as sub-sections. The Turn of a Friendly Card spawned the hits "Games People Play" and "Time", the latter of which was Eric Woolfson's first lead vocal appearance.


== Track listing ==
All songs written and composed by Alan Parsons and Eric Woolfson.

Though numbered as a single work, "The Turn of a Friendly Card" is split into five tracks.
The Turn of a Friendly Card was remastered and reissued in 2008 with the following bonus tracks:

"May Be a Price to Pay" (Intro/demo)
"Nothing Left to Lose" (Basic backing track)
"Nothing Left to Lose" (Chris Rainbow overdub compilation)
"Nothing Left to Lose" (Early studio version with Woolfson's guide vocal)
"Time" (Early studio attempt)
"Games People Play" (Rough mix)
"The Gold Bug" (Demo)


== Personnel ==
Stuart Elliott – drums, percussion
David Paton – bass guitar
Ian Bairnson – electric, acoustic and classical guitars; pedal steel guitar on "Time"
Eric Woolfson – piano, harpsichord, lead vocals
Alan Parsons – Projectron on "Games People Play", whistling and finger snaps on "The Gold Bug", Clavinet on "The Gold Bug" and "The Ace of Swords", harpsichord on "The Ace of Swords", additional vocals on "Time"
Chris Rainbow – lead and backing vocals
Elmer Gantry – lead vocal
Lenny Zakatek – lead and backing vocals
The Philharmonia Orchestra, arranged and conducted by Andrew PowellProduced and engineered by Alan Parsons
Executive producer: Eric Woolfson
Mastering consultant: Chris Blair
Sleeve concept: Lol Creme and Kevin Godley

Ted Jensen Original LP UK & US Pressings.


=== Additional instrumentation ===
"The Gold Bug", which references the same-titled short story by Edgar Allan Poe, includes a whistling part by Parsons, who imitates the style of Ennio Morricone's legendary Spaghetti Western film themes, and wordless vocals by Rainbow, while the main theme is played on an alto saxophone. The saxophone player, originally credited as Mel Collins, is instead credited on the liner notes for the remastered edition as "A session player in Paris whose name escapes us"; this refers to the fact that the saxophone part is a composite of several separate takes. Similarly, the accordion part on "Nothing Left to Lose" is credited in the liner notes to "An unidentified Parisian session player". Also on "The Gold Bug", the newer liner notes credit a "Harmonized Rotating Triangle" to drummer Stuart Elliott. This refers to the phasing sound effects heard throughout the rhythm-free introduction to the piece.


== Charts ==


== Certifications and sales ==


== Covers ==
The album's title track was covered by German funeral doom metal band Ahab for their album The Boats of the "Glen Carrig" in 2015.


== Notes ==