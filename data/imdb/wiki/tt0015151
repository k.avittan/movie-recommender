Anne Marie "Ree" Drummond (née Smith, born January 6, 1969) is an American blogger, author, food writer, photographer and television personality who lives on a working ranch outside of Pawhuska, Oklahoma. In February 2010, she was listed as No. 22 on Forbes' Top 25 Web Celebrities. Her blog, The Pioneer Woman, which documents Drummond's daily life as a ranch wife and mother, was named Weblog of the Year 2009, 2010 and 2011 at the Annual Weblog Awards (The Bloggies).
Capitalizing on the success of her blog, Drummond currently stars in her own television program, also entitled The Pioneer Woman, on The Food Network which began in 2011. She has also appeared on Good Morning America, Today Show, The View, The Chew and The Bonnie Hunt Show. She has been featured in Ladies' Home Journal, Woman's Day, People and Southern Living. She has also written numerous cookbooks, children's book and an autobiography.


== Early life ==
Anne Marie, nicknamed Ree, grew up in a home overlooking the grounds of a country club in the oil town of Bartlesville, Oklahoma, with two brothers, Doug and Mike, and a younger sister, Betsy. She graduated from Bartlesville High School in 1987 after which she left Oklahoma to attend college in Los Angeles, California. She graduated from the University of Southern California in 1991, having first studied journalism before switching to gerontology. After graduation she hoped to attend law school in Chicago, but her plans changed unexpectedly when she met and married her husband, Ladd Drummond.Her father, William Dale Smith, an orthopedic surgeon, and her mother Gerre Schwert are divorced. "Bill" Smith, as he is more commonly known, later married his current wife, Patsy.Drummond was raised Episcopalian. She is an alumna of Pi Beta Phi sorority.


== Blog (ThePioneerWoman.com) ==
Drummond began blogging in May 2006, initially using the subdomain pioneerwoman.typepad.com within the Typepad blogging service. She registered her own domain – thepioneerwoman.com – on October 18, 2006.
Drummond's blog, titled The Pioneer Woman, was originally titled Confessions of a Pioneer Woman. The latter is now the title of a section within the site. The site is hosted by Rackspace.
Drummond writes about topics such as ranch life and homeschooling.  About a year after launching her blog, she posted her first recipe and a tutorial on "How to Cook a Steak". The tutorial was accompanied by 20 photos explaining the cooking process in what she calls "ridiculous detail".  Her stories about her husband, family, and country living, and her step-by-step cooking instructions and elaborate food photography, proved highly popular with readers.  Confessions of a Pioneer Woman won honors at the Weblog Awards (also known as the Bloggies) in 2007, 2008, 2009, and 2010. In 2009 and 2010 it took the top prize as Weblog of the Year.As of September 2009, Drummond's blog reportedly received 13 million page views per month. On May 9, 2011, the blog's popularity had risen to approximately 23.3 million page views per month and 4.4 million unique visitors. According to an article in The New Yorker, "This is roughly the same number of people who read The Daily Beast". An article in the Toronto newspaper The Globe and Mail described it as "[s]lickly photographed with legions of fans . . . arguably the mother of all farm girl blogs." The blog has been referenced in the Los Angeles Times, The New York Times, and BusinessWeek.  In 2009, TIME magazine named Drummond's Confessions of a Pioneer Woman one of the "25 Best Blogs" in the world. Estimates for her site's income suggest she is making a million dollars or more per year from display (advertisement) income alone.Drummond's blog is especially noted for its visually descriptive recipes and high-quality photography.


== Food community (TastyKitchen.com) ==
In April 2008, Drummond held a giveaway contest in the cooking section of her blog The Pioneer Woman in which she asked readers to share one of their favorite recipes.  The response was an unexpected 5,000+ recipes in less than 24 hours. She realized that she had not only grown a community of loyal readers but a community of food lovers as well. She immediately sought a way to catalog the recipes and make them searchable for all.A little over a year later, on July 14, 2009, Drummond announced the launch of TastyKitchen.com – a simple and free online community website with the tagline Favorite Recipes from Real Kitchens Everywhere!. The site was built for her food-loving readers as a place where they could easily contribute, search for and print recipes. In addition to sharing recipes, users can create personal membership profiles and communicate with one another via posts and direct messages. Users also have the ability to rate and review recipes.Tasty Kitchen quickly rose to become a favorite among food bloggers who could link their recipes back to posts on their own websites, thus exposing themselves to a wider readership.


== Books ==
The Pioneer Woman Cooks: Recipes from an Accidental Country Girl
Drummond's first cookbook, The Pioneer Woman Cooks: Recipes from an Accidental Country Girl, was published in October 2009 after reaching the top spot on Amazon.com's preorder list for hardcover books.  A New York Times reviewer described Drummond as "funny, enthusiastic and self-deprecating", and commented: "Vegetarians and gourmands won’t find much to cook here, but as a portrait of a real American family kitchen, it works."Black Heels to Tractor Wheels
In 2007, Drummond began writing a series on her blog titled From Black Heels to Tractor Wheels. In the series, she chronicled her personal love story detailing how, in the process of relocating from Los Angeles to Chicago, she wound up settling down with a cowboy on a cattle ranch in rural Oklahoma. In February 2011, the series was compiled into a book and published by William Morrow, an imprint of HarperCollins. It quickly rose to No. 2 on both The New York Times Best Seller list for hardcover nonfiction and The Wall Street Journal's list.Charlie the Ranch Dog
In April 2011, Drummond published a children's book titled Charlie the Ranch Dog, featuring her family's beloved Basset Hound Charlie. According to Publishers Weekly, “Adult readers will recognize in Charlie’s voice the understated humor that has made Drummond’s blog so successful; kids should find it irresistible.” The book was illustrated by Diane deGroat, an illustrator of more than 120 children's books.The Pioneer Woman Cooks: Food from My Frontier
Drummond's second cookbook, The Pioneer Woman Cooks: Food from My Frontier,  released in March 2012 and was a #1 New York Times Bestseller.Charlie and the Christmas Kitty
Diane deGroat again illustrates this children book about the family's Basset Hound. Released in December 2012.
The Pioneer Woman Cooks: A Year of Holidays: 140 Step-by-Step Recipes for Simple, Scrumptious Celebrations
A cookbook for holidays throughout the year. Released October 29, 2013.
Charlie and the New Baby
Another children's book about the family's basset hound, illustrated by Diane deGroat. Released on April 29, 2014.
Charlie the Ranch Dog: Charlie Goes to the Doctor
An I Can Read story about Charlie the basset hound's trip to the doctor, illustrated by Diane deGroat. Released June 17, 2014.Charlie the Ranch Dog: Stuck in the Mud
An I Can Read story about Charlie the basset hound, illustrated by Diane deGroat. Released January 6, 2015.Charlie Plays Ball
A Children's book about Charlie the basset hound, illustrated by Diane deGroat. Released March 24, 2015.The Pioneer Woman Cooks: Dinnertime
A cookbook featuring 125 dinner recipes. Released October 20, 2015.Charlie the Ranch Dog: Rock Star
An I Can Read story about Charlie the basset hound, illustrated by Diane deGroat. Released November 17, 2015.Little Ree
A children's book about a little girl named Ree and her adventures in the country, illustrated by Jacqueline Rogers. Released March 28, 2017 The Pioneer Woman Cooks: Come and Get It!
A cookbook featuring 120 simple and delicious recipes. Released October 24, 2017.Little Ree: Best Friends Forever!
A children's book about a little girl named Ree and her best friend, Hyacinth, illustrated by Jacqueline Rogers. Released March 27, 2018 


== Television ==
Drummond made her television debut on an episode of Throwdown! with Bobby Flay  when the celebrity chef was challenged by her (in a change from the show's normal format) to a special Thanksgiving face-off. Flay traveled to her Oklahoma ranch for the event. The episode aired on the Food Network on Wednesday, November 17, 2010. Drummond's home cooking beat Flay's gourmet-style spread in a tight contest. Music artist and fellow Oklahoma resident Trisha Yearwood was one of the judges.
In April 2011, the Food Network announced that Drummond would host her own daytime television series on the network. The Pioneer Woman premiered on Saturday, August 27, 2011.


== Film ==
On March 19, 2010, Drummond confirmed media reports that Columbia Pictures had acquired the film rights to her book From Black Heels to Tractor Wheels. The production company was reported to be in talks with Reese Witherspoon to star as Drummond in a motion picture based on the book. However, as of 2019, the movie  has stalled with the project landing in development hell.


== Personal life ==
On September 21, 1996, Drummond married Ladd Drummond (born January 22, 1969), a fourth-generation member of a prominent Osage County cattle ranching family whom she refers to as "the Marlboro Man" in her books and her blog. They spent their honeymoon in Australia and live on a remote working cattle ranch approximately 8 miles west of Pawhuska, Oklahoma.  They have four children – Alex, Paige, Bryce and Todd. Alex is a graduate of Texas A&M University, while Paige is currently a sophomore at the University of Arkansas. The Drummonds homeschool their sons in parts of the summer.
In late 2016, the Drummonds opened The Mercantile, a restaurant retail store located in a 100-year-old downtown Pawhuska building that they bought and began renovating in 2012.In April 2018, the Drummonds also opened a bed and breakfast, called "The Pioneer Woman Bed and Breakfast".


== Awards ==
Seventh Annual Weblog Awards – The 2007 Bloggies 2007 Best Kept Secret Weblog – Confessions of a Pioneer Woman (won)                  Eighth Annual Weblog Awards – The 2008 Bloggies 2008 Best Food Weblog – The Pioneer Woman Cooks (won)  2008 Best Writing of a Weblog – Confessions of a Pioneer Woman (won)                 Ninth Annual Weblog Awards – The 2009 Bloggies 2009 Weblog of the Year – Confessions of a Pioneer Woman (won)  2009 Best Designed Weblog – Confessions of a Pioneer Woman (won)  2009 Best Photography of a Weblog – Pioneer Woman Photography (won)  2009 Best Food Weblog – The Pioneer Woman Cooks (nominated)  2009 Most Humorous Weblog – Confessions of a Pioneer Woman (nominated)  2009 Best Writing of a Weblog – Confessions of a Pioneer Woman (nominated)             Tenth Annual Weblog Awards – The 2010 Bloggies 2010 Weblog of the Year – The Pioneer Woman (won)  2010 Best Writing of a Weblog – The Pioneer Woman (won)  2010 Best Designed Weblog – The Pioneer Woman (won)                


== Bibliography ==
Drummond, Ree (2009). The Pioneer Woman Cooks: Recipes from an Accidental Country Girl. HarperCollins. ISBN 978-0-06-165819-8. Excerpts available at Google Books.
Drummond, Ree (2011). The Pioneer Woman: Black Heels to Tractor Wheels – A Love Story. HarperCollins. ISBN 978-0-06-199716-7. Excerpts available at Google Books.
Drummond, Ree (2011). Charlie the Ranch Dog. HarperCollins. ISBN 978-0-06-199655-9.
Drummond, Ree (2012). The Pioneer Woman Cooks: Recipes from My Frontier. HarperCollins. ISBN 978-0-06-199718-1.


== References ==


== External links ==
The Pioneer Woman blog
Tasty Kitchen food community
Pioneer Woman cooking show on Food Network
Ree Drummond on Twitter 
Ree Drummond on Facebook
Ree Drummond on Flickr
Video on Vimeo
Ree Drummond on IMDb