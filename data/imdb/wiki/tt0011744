Mark Noble (April 18, 1881 – January 9, 1978), known as Noble Johnson, was an American actor and film producer. He appeared in films such as The Mummy (1932), The Most Dangerous Game (1932),  King Kong (1933) and Son of Kong (1933).


== Biography ==
Standing 6'2" at 215 pounds, his impressive physique and handsome features made him in demand as a character actor and bit player. In the silent era, he assayed a wide variety of characters of different races in a plethora of films, primarily serials, westerns and adventure movies. While Johnson was cast as black in many films, he also played Native American and Latino parts and "exotic" characters such as Arabians or even a devil in hell in Dante's Inferno (1924).
The old orthochromatic film stock of the early days was less discriminating about a person's color, as were black-and-white stocks in general, permitting some African-American actors a break, as their color was washed out or less obvious when photographed in black and white. As late as the early 1960s, there were very few African-American members of the Screen Actors Guild. Because there was a lack of opportunity for them as black performers, they were confined mostly to race films until the 1960s.
Noble was good friends with fellow actor Lon Chaney, his schoolmate in Colorado, and was also an entrepreneur, founding, in 1916, his own studio to produce what were called "race films", movies made for the African-American audience, which was ignored by the "mainstream" film industry. The Lincoln Motion Picture Company, in existence until 1921, was an African American film company apart from director Harry A. Gant, and the first to produce movies portraying African-Americans as real people instead of as racist caricatures (Johnson was followed into the race film business by Oscar Micheaux and others). Johnson, who served as president of the company and was its primary asset as a star actor, helped support the studio by acting in other companies' productions such as 20,000 Leagues Under the Sea (1916), and investing his pay from those films in Lincoln.
Lincoln's first picture was The Realization of a Negro's Ambition (1916). For four years, Johnson managed to keep Lincoln a going concern, primarily through his extraordinary commitment to African-American filmmaking. However, he reluctantly resigned as president in 1920 because he no longer could continue his double business life, maintaining a demanding career in Hollywood films while trying to run a studio.
In the 1920s, Johnson was a very busy character actor, appearing in silent films such as The Four Horsemen of the Apocalypse (1921) with Rudolph Valentino, Cecil B. DeMille's original The Ten Commandments (1923), The Thief of Bagdad (1924), and Dante's Inferno (1924). He made the transition to sound films, appearing in The Mysterious Dr. Fu Manchu (1929) as Li Po, in Moby Dick (1930) as Queequeg to John Barrymore's Captain Ahab, and in the Boris Karloff film The Mummy (1932) as "the Nubian". He was also the Native Chief on Skull Island in the classic King Kong (1933) (and its sequel The Son of Kong, 1933) and appeared in Frank Capra's classic Lost Horizon (1937) as one of the porters. One of his later films was John Ford's She Wore a Yellow Ribbon (1949), in which he played Native American Chief Red Shirt. He retired from the movie industry in 1950.
Johnson died of natural causes on January 9, 1978 in Yucaipa, California. He is buried in the Garden of Peace at Eternal Valley Memorial Park in Newhall, California.


== Selected filmography ==


== References ==


== External links ==
Noble Johnson on IMDb
Noble Johnson at the Horror Film Site
Noble Johnson at Find a Grave