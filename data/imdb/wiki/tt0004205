Land of the Lost is a 2009 American adventure comedy film directed by Brad Silberling,  written by Chris Henchy and Dennis McNicholas and starring Will Ferrell, Danny McBride and Anna Friel, loosely based on the 1974 Sid and Marty Krofft television series of the same name. The film was theatrically released on June 5, 2009 by Universal Pictures.
The film received generally negative reviews from critics and was a box office bomb, grossing just $68 million against its $100 million budget. It received seven Golden Raspberry Award nominations, including Worst Picture, winning Worst Prequel, Remake, Rip-off or Sequel.


== Plot ==
Pompous paleontologist Rick Marshall has a low-level job at the La Brea Tar Pits, three years after a disastrous interview with Matt Lauer of Today became a viral video and ruined his career. Doctoral candidate Holly Cantrell tells him that his controversial theories combining time warps and paleontology inspired her. She shows him a fossil with an imprint of a cigarette lighter that he recognizes as his own along with a crystal made into a necklace that gives off strong tachyon energy. She convinces him to finish his tachyon amplifier and come help her on a seemingly routine expedition to the cave where Holly found the fossil, which is in the middle of nowhere. With cave gift shop owner Will Stanton they raft into the cave, where Marshall has detected high levels of tachyons. He activates the tachyon amplifier, triggering an earthquake that opens a time warp into which the raft falls. The group finds themselves in a desert, filled with various items from many eras, and without the amplifier. They rescue a primate-like creature, Cha-Ka of the Pakuni tribe, who becomes their friend and guide.
The gang spends a night in a cave after surviving a meeting with a fast, intelligent Tyrannosaurus they nickname "Grumpy", who stalks Marshall for calling him stupid.  Marshall receives a telepathic message begging for help and finds himself in ancient ruins. The group encounters a race of lizard men called Sleestaks before meeting the one who sent Marshall the telepathic message, Enik the Altrusian. He explains that he was exiled by the evil Zarn who is attempting to take over Earth with his Sleestak minions, but Enik can prevent this if Marshall retrieves the tachyon amplifier.
The group stumble upon a desert where many things from across time end up and they encounter many Compsognathus, Dromaeosaurs, Grumpy, and a female Allosaurus. The Allosaurus and Grumpy battle it out over a previously killed ice-cream seller until they sense Marshall and chase him. Marshall kills the Allosaurus with liquid nitrogen and finds that the amplifier was eaten by the Allosaurus. The amplifier is then stolen by a Pteranodon and taken to its nest. The group arrives at the nest and Marshall lightly steps through the Pteranodon eggs to retrieve the amplifier, but when he reaches it, it stops broadcasting the soundtrack to Marshall's favorite musical A Chorus Line. When the eggs begin to hatch, Holly realizes that the music was acting as a sort of lullaby keeping the baby Pteranodons asleep. Marshall, Will and Holly belt out "I Hope I Get It", with Cha-ka inexplicably joining in, displaying an impressive singing voice.
Marshall, Will and Cha-ka celebrate their good fortune. Meanwhile, Holly pockets a dinosaur egg and learns from a recording left by the long-deceased Zarn that Enik deceived them and he is actually the one planning to invade Earth. But she is captured by the Sleestaks to be brought to the Library of Skulls for judgment. The others saved her from being executed for helping Enik, but the villain—now possessing the amplifier, and mind-controlling the Sleestaks—leaves them to open a portal to Earth. Marshall pole vaults into Grumpy's mouth and after removing an intestinal blockage, befriends him. He joins the others to defeat the Sleestak army and confront Enik. After the crystal link between the Land of the Lost and Earth is shattered, Enik reveals the portal will close forever. Thinking fast, Marshall grabs Holly's crystal and inserts it into the port, knowing that the substitute crystal won't hold for long. Will chooses to stay behind to live a better life and to prevent Enik from following Marshall and Holly back to Earth, learning later that female Pakuni are very attractive.
A triumphant Marshall again appears on Today with the dinosaur egg Holly brought back to promote his new book Matt Lauer Can Suck It. The egg left behind on the Today set hatches a baby Sleestak, which hisses as the screen goes black.


== Cast ==
Will Ferrell as Dr. Rick Marshall, a renowned paleontologist and author, determined to prove his theory on time warps, thus embarking on a time travel experiment.
Anna Friel as Holly Cantrell, Rick's biggest fan.
Danny McBride as Will Stanton, a gift shop owner who takes Holly and Rick to a tachyon rich cave.
Jorma Taccone as Cha-Ka, a Pakuni who befriends Rick, Holly, and Will.
John Boylan as Enik, an Altrusian who plans to invade and conquer Earth, and deceives Rick and the others into believing otherwise; he also controls the Sleestak.
Matt Lauer as Himself
Leonard Nimoy as voice of The Zarn
Douglas Tait as SleestakThe original actors who played Holly and Will in the TV series, Kathy Coleman and Wesley Eure, filmed cameos for the film. However, the final version of the film cut these scenes. Bobb'e J. Thompson, Kiernan Shipka (uncredited), Dylan Sprayberry, and Sierra McCormick appeared in a cameo as Tar Pit Kids and Raymond Ochoa as an uncredited boy in a museum.


== Production ==
Production for the film began on March 4, 2008. Only one week's worth of filming was shot using a large-scale soundstage with green screen technology. The rest of filming took place on location in places such as the Dumont Dunes in the Mojave Desert, the La Brea Tar Pits in Hancock Park, and Trona, California.


=== Marketing ===
The first trailer was shown during Super Bowl XLIII. Subway Restaurants, which paid to appear in the film and had cross promotions with (appearing on their cups), unveiled the second trailer exclusively on their website. JW Marriott Hotels and Pop Rocks also purchased rights to market with film tie-ins. Syfy aired a marathon of the original series on Memorial Day in 2009 in coordination with the studio to have frequent film clips and an interview with Sid and Marty Krofft. After the film's release, another marathon aired on Chiller on June 6. The majority of the first two seasons were also made available on Hulu. Ahead of the film's release, Universal also released the complete series on DVD; it had previously been released by Rhino Home Video. The entire series is also available via download from Xbox Live. Two different games were released online to promote the film. "Chakker" was available to play on the film's official Web site while "Crystal Adventure" was a free downloadable game for iPhones from Kewlbox. Both Subway and MapQuest hosted an online sweepstakes on their respective Web sites with various movie-related merchandise given away as prizes. Both sweepstakes ran from May 18 through June 7 of 2009. Ferrell also appeared on the season 4 premiere of Man vs. Wild, which aired June 2, 2009, to promote the film.


=== Music ===
The score to Land of the Lost was composed by Michael Giacchino, who recorded his score with an 88-piece ensemble of the Hollywood Studio Symphony and a 35-person choir. On May 10, it was also announced by Dave Mustaine on TheLiveLine that some music from Megadeth would appear in the film. Whether this would be music from the new record was not entirely clear, however during the phone message Mustaine stated that there was new music playing in the background of the message. However parts of the song "The Right to Go Insane", from the 2009 album Endgame, can be heard near the end of the film. In the film, Rick Marshall sings the original Land of the Lost theme and two other tracks (Tracks 5 and 27) utilize parts of the theme as well., The musical A Chorus Line plays a part in the story, and Ferrell sings Cher's 1998 dance pop hit "Believe". Varèse Sarabande released the soundtrack album on June 9, 2009 (tracks 30-32 are bonus tracks).


== Differences from original series ==
The film serves as a parody of the original TV series, similar to Starsky & Hutch and The Brady Bunch Movie. In the original series, the main characters were the father and two children. While the first names remain the same, the film converts the Holly character into an unrelated research assistant to allow for more risque humor because she is the main character's love interest.  Will, instead of being a son, is an amusement park operator and survivalist.  Rick Marshall is a paleontologist in the film, not a park ranger as in the original series. The film's budget also uses CGI special effects rather than the puppet and stop motion animation effects that defined the original series.  While the original Saturday morning show targeted a child audience, the film was designed for a more adult audience and includes profanity, sex, drug references, and other adult-oriented items.


== Reception ==


=== Critical response ===

Rotten Tomatoes reports an approval rating of 26% based on 188 reviews, with an average rating of 4.1/10. The site's critical consensus reads, "Only loosely based on the original TV series, Land of the Lost is decidedly less kid-friendly and feels more like a series of inconsistent sketches than a cohesive adventure comedy." On Metacritic the film has a weighted average score of 32 out of 100, based on 32 critics, indicating "generally unfavorable reviews". Audiences polled by CinemaScore gave the film an average grade of "C+" on an A+ to F scale.The Wall Street Journal stated that it "isn't worth the celluloid it's printed on", Entertainment Weekly remarked that "it leaves you feeling splattered", the New York Daily News called it "a high-concept disaster", The Hollywood Reporter labeled it "lame", and The Miami Herald commented that "the whole thing feels at least three summers too stale." A few critics professed admiration for it, notably Roger Ebert who gave the film three stars out of four and wrote:

The dinosaurs are so obviously not really there in shots where they menace humans that you could almost say their shots are about how they're not really there. Confronted with such effects, the actors make not the slightest effort to appear terrified, amazed or sometimes even mildly concerned. Some might consider that a weakness. I suspect it is more of a deliberate choice, and I say I enjoyed it.
Empire magazine's Sam Toy put the film #8 on his best of the year list. On February 1, 2010, the film led the 30th Golden Raspberry Awards with seven nominations (tied with Transformers: Revenge of the Fallen) including Worst Picture, Worst Actor (Ferrell), Worst Director (Silberling), Worst Screenplay, Worst Supporting Actor (Taccone), Worst Screen Couple (Ferrell and any co-star, creature or "comic riff") and Worst Prequel, Remake, Rip-off or Sequel. The film won the Worst Prequel, Remake, Rip-off or Sequel award.


=== Response from creators ===
At the Savannah Film Festival in 2011, Ron Meyer (president of Universal Pictures), said that "Land of the Lost was just crap. I mean, there was no excuse for it. The best intentions all went wrong." In 2012, Danny McBride defended the film, saying "There are the purists, who I always read about, that are like, 'I can't believe you're raping my childhood.' If Land of the Lost is your childhood, and we're raping it, I apologize. I think the show is awesome, and I think [screenwriters] Chris Henchy and Dennis McNicholas keep the mythology intact without taking it too seriously. If it was taken too seriously, it's just Jurassic Park. We've seen that movie before. This is a more interesting take on that tone."Sid & Marty Krofft apologized for the film at a 2017 Comic-Con appearance, calling it "one of the worst films ever made", saying that they had very little involvement in the film and only sporadically visited the set.


=== Awards ===


=== Box office ===
On its opening day of June 5, the film was a box office flop by grossing only $7.9 million. The film performed under expectations in its first weekend in theaters, its $19 million opening was far less than the expected $30 million. The film's box office results fell far behind that of the 2009 comedy The Hangover, which opened during the same weekend. The film's opening weekend gross was about two-thirds what Universal reportedly expected to earn. It made $69 million worldwide. In 2014, the Los Angeles Times listed the film as one of the most expensive box office flops of all time.


=== Home media ===
The DVD was released on October 13, 2009, with sales reaching $20,286,563 as of August 2011.


== See also ==
Land of the Lost (1974 TV series), the original children's television series created by Sid and Marty Krofft
List of Land of the Lost episodes
Land of the Lost characters and species
Land of the Lost (1991 TV series), the TV remake of the original series
The Land That Time Forgot (2009 film), a mockbuster made by The Asylum intended to capitalize on this film.


== References ==


== External links ==
Land of the Lost on IMDb
Land of the Lost at the TCM Movie Database
Land of the Lost at AllMovie
Land of the Lost at Box Office Mojo
Land of the Lost at Rotten Tomatoes
Land of the Lost at Metacritic
Interview with Sid and Marty Krofft on Sci Fi Channel