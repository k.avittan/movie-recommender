Approximately twenty tropical cyclones enter the Philippine area of responsibility yearly, an area which incorporates parts of the Pacific Ocean, the West Philippine Sea, and the Philippine Archipelago (with the exception of Tawi-Tawi province). In each year, ten cyclones are usually expected to be typhoons, with five having the potential to be destructive ones. According to a 2013 Time Magazine article, the Philippines is "the most exposed country in the world to tropical storms". In the Philippine languages, tropical cyclones are generally called bagyo.Typhoons can hit the Philippines any time of the year, with the months of June to September being most active, with August being the most active individual month and May the least active. Typhoons usually move east to west across the country, heading north or west as they go. Storms most frequently make landfall on the islands of Eastern Visayas, Bicol region, and northern Luzon, whereas the southern island and region of Mindanao is largely free of typhoons. Climate change is likely to worsen the situation, with extreme weather events including typhoons posing various risks and threats to the Philippines.The deadliest overall tropical cyclone to affect the Philippines is believed to have been the Haiphong typhoon, which is estimated to have killed up to 20,000 people as it passed over the country in September 1881. In modern meteorological records, the deadliest storm was Typhoon Yolanda (international name Haiyan), which became the strongest landfalling tropical cyclone on record as it crossed the Visayas in central Philippines on November 7–8, 2013. The wettest known tropical cyclone to impact the archipelago was the July 14–18, 1911 cyclone which dropped over 2,210 millimetres (87 in) of rainfall within a 3-day, 15-hour period in Baguio. Tropical cyclones usually account for at least 30 percent of the annual rainfall in the northern Philippines while being responsible for less than 10 percent of the annual rainfall in the southern islands.  PAGASA Senior Weather Specialist Anthony Lucero told the newsite Rappler that the number of destructive typhoons have increased recently but it is too early to call it a trend.Tropical cyclones entering the Philippine Area of Responsibility, as well as tropical depressions that form within it, are given a local name by the Philippine Atmospheric, Geophysical and Astronomical Services Administration (PAGASA), which also raises public storm signal warnings as deemed necessary.Preparation and response to typhoons is coordinated by the National Disaster Risk Reduction and Management Council (NDRRMC). Each Philippine province and local government in the Philippines has a corresponding Disaster Risk Reduction and Management Office (DRRMO). Each provincial and local government is required to set aside 5% of its yearly budget for disaster  risk reduction, preparations, and response.The frequency of typhoons in the Philippines have made typhoons a significant part of everyday ancient and modern Filipino culture.


== Etymology ==
Bagyo (sometimes spelled bagyu or bagyio) is the word for "typhoon" or "storm" in most Philippine languages, including Tagalog, Visayan, Ilocano, Bicolano, Hanunó'o, Aklanon, Pangasinan and Kapampangan. It is derived from Proto-Austronesian *baRiuS, meaning "typhoon". Cognates in other Austronesian languages include Sama baliw ("wind"), Amis faliyos or farios ("typhoon"); Saisiyat balosh ("typhoon"), Babuza bayus ("storm"), Puyuma variw, Bintulu bauy ("wind"), Kelabit bariw ("storm wind"), and Chamorro pakyo ("typhoon").


== Storm naming conventions: local and international names ==
The Joint Typhoon Warning Center in Honolulu started monitoring and naming storms in the Western Pacific region in 1945, originally using female names in English alphabetical order. That list was revised in 1979 by introducing male names to be used in alternation with the female names. The Philippine Weather Bureau started naming storms within their area of responsibility in 1963, using female Filipino names ending in the former native alphabetical order. The Bureau continued to monitor typhoons until the agency's abolition in 1972, after which its duties were transferred to the newly-established PAGASA. This often resulted in a Western Pacific cyclone carrying two names: an international name and a local name used within the Philippines. This two-name scheme is still followed today.
In 2000, cyclone monitoring duties in the Western Pacific were transferred from the JTWC to the Japan Meteorological Agency, the RSMC of the World Meteorological Organization. The international naming scheme of the typhoons was replaced with a sequential list of names contributed by 14 nations in the region, including the Philippines. The new scheme largely uses terms for local features of the contributing nation, such as animals, plants, foods and adjectives in the native language. The rotation of names is based on the alphabetical order of the contributing nations. The Philippines, however, would maintain its own naming scheme for its local forecasts. In 2001, PAGASA revised its naming scheme to contain longer annual lists with a more mixed set of names.
Currently, the JMA and PAGASA each assign names to typhoons that form within or enter the Philippine Area of Responsibility. The JMA naming scheme for international use contains 140 names described above. The list is not restricted by year; the first name to be used in a typhoon season is the name after the last-named cyclone of the preceding season. The PAGASA naming scheme for Philippine use contains four lists, each containing twenty-five names arranged in alphabetical order. Every typhoon season begins with the first name in the assigned list, and the rolls of names are each reused every four years. An auxiliary list of ten names is used when the main list in a year had been exhausted. Not all Western Pacific cyclones are given names by both weather agencies, as JMA does not name tropical depressions, and PAGASA does not name cyclones outside the Philippine Area of Responsibility.
In the case of both weather agencies, names are retired after a typhoon that carried it caused severe or costly damage and loss of life. Retirement is decided by the agencies' committees, although in PAGASA's case, names are routinely retired when the cyclone caused at least 300 deaths or ₱1 billion in damage in the Philippines. Retired names are replaced with another name for the next rotation, for JMA by the nation that submitted the retired name, and for PAGASA with a name sharing the same first letter as the retired name.


== Variability in activity ==
On an annual time scale, activity reaches a minimum in May, before increasing steadily to June, and spiking from July to September, with August being the most active month for tropical cyclones in the Philippines. Activity reduces significantly in October. The most active season, since 1945, for tropical cyclone strikes on the island archipelago was 1993 when nineteen tropical cyclones moved through the country (though there were 36 storms that were named by PAGASA). There was only one tropical cyclone which moved through the Philippines in 1958. The most frequently impacted areas of the Philippines by tropical cyclones are northern Luzon and eastern Visayas. A ten-year average of satellite determined precipitation showed that at least 30 percent of the annual rainfall in the northern Philippines could be traced to tropical cyclones, while the southern islands receive less than 10 percent of their annual rainfall from tropical cyclones.


== Tropical cyclone warning signals ==
The Philippine Atmospheric, Geophysical and Astronomical Services Administration (PAGASA) releases tropical cyclone warnings in the form of tropical cyclone warning signals. An area having a storm signal may be under:

TCWS #1 - Tropical cyclone winds of 30 km/h (19 mph) to 60 km/h (37 mph) are expected within the next 36 hours. (Note: If a tropical cyclone forms very close to the area, then a shorter lead time is seen on the warning bulletin.)
TCWS #2 - Tropical cyclone winds of 61 km/h (38 mph) to 120 km/h (75 mph) are expected within the next 24 hours.
TCWS #3 - Tropical cyclone winds of 121 km/h (75 mph) to 170 km/h (110 mph) are expected within the next 18 hours.
TCWS #4 - Tropical cyclone winds of 171 km/h (106 mph) to 220 km/h (140 mph) are expected within 12 hours.
TCWS #5 - Tropical cyclone winds greater than 220 km/h (140 mph) are expected within 12 hours.These tropical cyclone warning signals are usually raised when an area (in the Philippines only) is about to be hit by a tropical cyclone.  As a tropical cyclone gains strength and/or gets nearer to an area having a storm signal, the warning may be upgraded to a higher one in that particular area (e.g. a signal No. 1 warning for an area may be increased to signal #3).  Conversely, as a tropical cyclone weakens and/or gets farther to an area, it may be downgraded to a lower signal or may be lifted (that is, an area will have no storm signal).
Classes for preschool are canceled when signal No. 1 is in effect. Elementary and high school classes and below are cancelled under signal No. 2 and classes for colleges, universities and below are cancelled under signal Nos. 3, 4 and 5.


== Deadliest cyclones ==


== Wettest recorded tropical cyclones ==


== Most destructive ==


== See also ==

2019 Pacific typhoon season
2020 Pacific typhoon season
List of Pacific typhoon seasons (1939 onwards)
List of retired Philippine typhoon namesFor other storms impacting the Philippines in deadly seasons, see:

Effects of the 2009 Pacific typhoon season in the Philippines
Effects of the 2013 Pacific typhoon season in the Philippines


== References ==


== External links ==
Philippine Tropical Cyclone Update
Typhoon2000
Monthly typhoon tracks: 1951-2010
Typhoon Haiyan coverage by CBSnews
CRS International relief organization quickly mobilizes to help Philippine Typhoon victims