The Devil's Punch Bowl is a 282.2-hectare (697-acre) visitor attraction and biological Site of Special Scientific Interest situated just to the east of the village of Hindhead in the English county of Surrey. It is part of the Wealden Heaths Phase II Special Protection Area.The Punch Bowl is a large natural amphitheatre and is the source of many stories about the area. The London to Portsmouth road (the A3) skirted the rim of the site before the Hindhead Tunnel was built in 2011. The land is now owned and maintained by the National Trust as part of the "Hindhead Commons and the Devil's Punch Bowl" property. The highest point of the rim of the bowl is Gibbet Hill, which is 272 metres (892 ft) above sea level and commands a panoramic view that includes, on a clear day, the skyline of London some 38 miles (61 km) away.The Devil's Punch Bowl was featured on the 2005 TV programme Seven Natural Wonders as one of the wonders of the South.


== Etymology ==

The name Devil's Punch Bowl dates from at least 1768, the year that John Rocque's map of the area was published. This was 18 years before the murder of the unknown sailor on Gibbet Hill, so this event was clearly not the origin of the name. Prior to 1768, it was marked as "ye Bottom" on a map by John Ogilby dated 1675. The northern end of the Bowl is known as Highcombe Bottom which exists in different variants: Hackombe Bottom, Hacham Bottom, and Hackham Bottom.


== Natural history ==
The soil in this part of Surrey has two layers — an upper layer of sandstone, with clay beneath. This deep depression is believed to be the result of erosion caused by spring water beneath the sandstone, causing the upper level to collapse. With its steep sides, the Devil's Punch Bowl has become a natural nature reserve, filled with heathland, streams and woodland.
The site has abundant wildlife. Most woodland species can be seen easily - including lesser spotted woodpecker and redstart. It has been known for the wood warbler, a rare summer visitor, but the last documented sighting was in 2009.


== Local legends ==

Local legend has colourful theories as to its creation. According to one story, the Devil became so irritated by all the churches being built in Sussex during the Middle Ages that he decided to dig a channel from the English Channel through the South Downs and flood the area.  As he began digging, he threw up huge lumps of earth, each of which became a local landmark — such as Chanctonbury Ring, Cissbury Ring and Mount Caburn. He got as far as the village of Poynings (an area known as the Devil's Dyke) when he was disturbed by a cock crowing. (One version of this story claims that it was the prayers of St Dunstan that made all the local cocks crow earlier than usual.) The devil assumed that dawn was about to break and leapt into Surrey, creating the Devil's Punch Bowl where he landed.
Another story goes that, in his spare time, he hurled lumps of earth at the god Thor to annoy him. The hollow out of which he scooped the earth became the Punch Bowl. The local village of Thursley means Thor's place. An alternative version of this story says that Thor threw the earth at the Devil, who was annoying Thor by jumping across the Devil's Jumps.


== Development and protected status ==

The beauty of the area and the diversity of nature it attracts that has gained the Devil's Punch Bowl the title of a Site of Special Scientific Interest. This status has recently helped save the Devil's Punch Bowl from above-ground redevelopment of the A3, which was needed to relieve traffic congestion in the area, as this section of the A3 was single-carriageway.
The National Trust co-operated with developers Balfour Beatty who designed the twin-bore Hindhead Tunnel, running underneath the surrounding area. The tunnel preserves not only the area from the road widening originally proposed but also removes the heavy traffic congestion which previously affected this section of the A3 in peak hours. The parking and cafe are provided by the National Trust. The old A3 road, apart from a small stub to the National Trust cafe, and small private lane to the youth hostel, has been removed and the land reinstated.The Hindhead youth hostel, run by the Youth Hostel Association, used to be located inside the bowl but closed in 2015.


== In fiction ==
Punch Bowl Farm, at the northern end of the Devil's Punch Bowl, was the home of children's novelist Monica Edwards from 1947 until 1998. In her books she renamed the farm Punchbowl Farm. Edwards also wrote about the area, including her years of observation of badger families, in her various volumes of memoirs. In Charles Dickens' novel Nicholas Nickleby, Nicholas and Smike visit it on their journey to Portsmouth.
The third novel in the Horatio Hornblower series, Flying Colours by C.S. Forester, makes a one-line reference to the Devil's Punch Bowl in chapter eighteen as Hornblower is returning to London: "Even the marvellous beauty of the Devil's Punch Bowl was lost on Hornblower as they drove past it."
The "Devil's Punch-Bowl in Surrey" is briefly mentioned in The Shining Pyramid, a short story by Arthur Machen and in 'The Manhood of Edward Robinson", the fifth story in Agatha Christie's The Listerdale Mystery and Other Stories. The area is the setting for Sabine Baring-Gould's novel The Broom-squire.


== Legacy Project ==

A lottery award from Heritage Lottery Fund was made in 2012 for a project with young people from schools in the area, celebrating the landscape. Several sculptures marked the completion in early 2013 and a carving from a 3 tonne block of Portland stone by Jon Edgar now sits on the spine of the former A3 near the visitor centre.


== See also ==
Cheesefoot Head
Devil's Dyke, Sussex
Devil's Jumps, Churt
The Devil's Farmhouse
Hindhead Tunnel
List of Sites of Special Scientific Interest in Surrey
Torberry Hill
River Lyd, Devon Lydford Gorge, Devil's Cauldron


== References ==


== External links ==
Hindhead Commons and the Devil's Punch Bowl
A3 Hindhead Tunnel — Mott MacDonald Project Page
Highways Agency — A3 Hindhead Improvement