As of 21:25, Sunday, November 8, 2020 (UTC)
The Missing Women Commission of Inquiry was a commission in British Columbia ordered by the Lieutenant Governor in Council on September 27, 2010, to evaluate the response of law enforcement to reports of missing and murdered women. The commission concluded its Inquiry in December 2012, and outlined 63 recommendations to the Provincial government and relevant law enforcement.  The Inquiry itself received criticism from various civil society group and Indigenous communities, regarding its investigative structure, as well as, the lack of government action after the Inquiry to fulfill its recommendations.   


== History ==
On December 9, 2007, Robert Pickton, a pig farmer from Port Coquitlam, was convicted of second-degree murder in the deaths of six women. He was also charged in the deaths of an additional twenty women, many of them from Vancouver's Downtown Eastside. These charges were, however, stayed by the Crown in 2010. In December 2007, he was sentenced to life in prison, with no possibility of parole for 25 years. It was the longest sentence then available under Canadian law for murder.
Following Pickton's arrest, there was increased attention given to a prior attempted murder charge of a sex worker in March 1997. Pickton was alleged to have stabbed her several times during an altercation at the farm. The victim informed police that Pickton had handcuffed her, but that she escaped after suffering several lacerations, disarming him, and stabbing him with his weapon. The charge was dismissed in January 1998. The mandate of the commission included evaluating why the 1997 charges against Pickton were stayed.
The Missing Women Commission of Inquiry had four mandates:

Evaluate the response of the police to reports of missing women from Downtown Eastside of Vancouver,
Evaluate the reasons for staying charges against Robert Pickton in January 1998,
Recommend changes regarding how missing women and suspected homicides are handled
Recommend changes to how cases are handled when they involve more than one investigating organizationIn 2012, the Commission issued a final report which included a number of recommendations. The commission closed its office on August 1 of the following year.


== The Inquiry and Overrepresentation of Indigenous Women ==
The Inquiry’s executive report notes that Indigenous women experience a “heightened vulnerability” to violence, disproportionally facing higher rates of violence with harsher severity. The Inquiry found Indigenous women alarmingly overrepresented in missing women cases, being approximately 33% of the cases, despite making up approximately 3% of the Province's total population. Additionally, the Inquiry found that Indigenous women have increased susceptibility to violence and other violent crimes such as “sexual assault, murder, and serial predation”. The Inquiry suggests a correlation between the increased risks of violence Indigenous women face to the systematic level of marginalization and inequality Indigenous women face within British Columbia, Canada, and globally. This systematic marginalization of Indigenous women throughout the Inquiry was linked to Canada’s colonial history, finding that such colonial links created an “unjust” relationship between the Canadian government and their Indigenous peoples. When considering the disproportionate number of Indigenous women represented in Inquiry, it condemned possible correlations between a women’s “high-risk lifestyle” to their “vulnerability to predation”. Beyond that, Indigenous women’s overrepresentation in missing women cases was considered “not within the mandate” of the Inquiry, and a matter better handled for Federal authority.


== Reaction to the Inquiry ==
From the Indigenous community
As an extension of The Missing Women Commission of Inquiry, the National Inquiry into Missing and Murdered Indigenous Women and Girls was formed by the Government of Canada in September 2016The Indigenous community and Canadian government have focused their attention on systemic causes of all forms of violence and institutional policies and practices implemented in response to violence. The community has been open, allowing anyone who has experienced violence to participate in the inquiry, from family member to survivors. Together, the inquiry went through a four part sequence to bring awareness to the issue. First, a Truth-Gathering Process to hear and share testimonies from participants, then involving experts such as elders, academics, legal experts, front line workers, young people, specialists and others to provide their recommendation on system causes of violence and possible solutions. Lastly, the report was presented to the National Inquiry.
The Indigenous and local communities have engaged in multiple actions to raise awareness and ensure Murdered and Missing Indigenous Women receive the attention they deserve.
In October 2020, Coalition on Murdered and Missing Indigenous Women and Girls in B.C. demanded an action plan from provincial leaders. In an open letter, the coalition called on each party to commit to fully implement the recommendations from the Legal Strategy Coalition on Violence Against Indigenous Women (LSC); Red Women Rising: Indigenous Women Survivors in Vancouver’s Downtown Eastside; the Path Forward Indigenous Women and Girls Safety Action Plan; the report of the Inter-American Commission on Human Rights, Missing and Murdered Indigenous Women in British Columbia, Canada; the report of the United Nations Committee on the Elimination of Discrimination against Women on its Article 8 Inquiry into murders and disappearances of Indigenous women and girls in Canada; and the National Inquiry into Missing and Murdered Indigenous Women and Girls.From the community in British Columbia  
SisterWatch/Project Sister Watch
SisterWatch was developed in collaboration with police and community to help address and respond to the remaining gaps in care for missing and murdered women in B.C. The committee has been working together since 2016 to eliminate violence against women and girls in Downtown Eastside Vancouver. The collaboration between police and the Indigenous community allows for two way information-sharing practices to support women.In addition to the committee of community leaders, Vancouver Police Department and members of the Women’s Memorial March Committee, The SisterWatch Tip Line is available for people to call who need support or know information about crimes against women.Project Rescue
Residents of downtown Vancouver noticed violent drug dealers were preying on addicted marginalized women. Police admitted they had heard anecdotal evidence of this, but cases were never reported. The Vancouver Police Department (VPD) arranged for a team of detectives to set up an investigation, for the first time in history, the VPD laid charged for criminal organization. The ongoing investigation was successfully in reducing the violence of marginalized women. 


== Post Inquiry Actions ==
After the conclusion of the inquiry in 2012, two urgent outcomes and 63 separate recommendations to the BC government were made. Among these included recommendations for additional funding to centres providing services for sex workers, as well as enhanced public transit along highway 16, sections of which are where many of the missing and murdered women had been abducted from. Following the inquiry the provincial government took action on many of the recommendations of the report. Some of these commitments included the establishment of a compensation fund for children of missing and murdered women, funding for safer public transit along highway 16, and investments in community organizations to support sex workers. In the Ministry of Justice’s final update on the inquiry they stated that “action has been taken on over three quarters of the recommendations directed at the Province”.
The Missing Women Commission of Inquiry and its consequences eventually gave way to the National Inquiry into Missing and Murdered Indigenous Women and Girls, sponsored by the Canadian federal government. This program began in 2016 with the goal of developing a more inclusive methodology as well as expanding the inquiry nationally. However this initiative has been criticized for similar reasons that the Missing Women Commission of Inquiry has.


== Criticism of the Inquiry ==
Following the release of the Inquiry in 2012, scholars, non-governmental organizations, the families of the missing women, and various Indigenous communities across British Columbia and Canada, spoke out against the Inquiry. A civil society response by the British Columbia Civil Liberties Association, Pivot Legal Society, and West Coast LEAF[1] deemed the Inquiry a keynote example of “what should not be done in conducting a public inquiry involving marginalized communities”, with multiple other grassroots and legal organizations sharing similar sentiments. While the critiques of the Inquiry varied, the three common criticisms emerged: the lack of community inclusion, inadequate investigative framework, and failures in addressing the underlying issues contributing to missing women.


=== Lack of Community Inclusion ===
The lack of inclusion of the marginalized women and communities in focus is a common critique of the Inquiry. Many legal, human rights, and Indigenous organizations found the Inquiry’s limited representation of the population in question decreased its reliability and effectiveness. These groups found the Inquiry to be overly reliant on Western European traditions of study and investigation, hindering its ability from the start to conduct a comprehensive investigation into the issue. Additionally, many found the Inquiry’s mandate, directives, terms of references, processes, and recommendations to lack consideration and input from the community in question. Due to this lack of community inclusion, various groups found the Inquiry to be merely an application of governmental inquiry procedures, rather than a way to address and reduces the issue of missing women.


=== Inadequate Framework ===
The Inquiry's framework design was criticized and was thought by many to hinder the effectiveness of the investigation. Input from relevant witnesses, families, and civil society organizations was limited due to a lack of proper funding for legal representation. When individuals were granted sufficient legal funding, they faced arguable legal misrepresentation, with the commission using just two lawyers to advocate for the diverse voices of impoverished communities and Indigenous people. Along with inadequate funding, the Inquiry's investigation was limited by government time constraints, reducing the full participation of marginalized individuals and relevant organizations, as well as, limiting the insight the Inquiry was able to gain.
Many critics found the Inquiry was misled from the beginning, as the Commissioner of the Inquiry, Wally Oppal, previously expressed doubts about the usefulness of any inquiry regarding the issue of missing women and marginalized communities. Along with concerns regarding his openness to an inquiry, Oppal's position as Commissioner was further questioned in relation to his close ties to the ruling Liberal government, as he previously served as a Member of Legislative Assembly of British Columbia for the Liberals. In addition to the Commissioner of the Inquiry presenting conflicting interests, legal rights groups found a number of the personnel tasked with conducting the inquiry had previous employment relations with Vancouver Police Department, arguably influencing the neutrality of the Inquiry. The Inquiry’s credibility was further questioned as lack of community inclusion funding resulted in the underrepresentation of community voices, favouring government, and police representation.


=== Lack of Underlying Acknowledgment ===
Prior to the Inquiry’s publication, the commission garnered attention when Dr. Bruce Miller, a Canadian scholar who specializes in Canadian-Indigenous relations, had his report on systematic racism excluded from the Inquiry’s investigation. After the Inquiry was released to the public, it was seen that the issue and consequences of systemic racism were not considered or addressed in the Inquiry.  
Critics found the Inquiry selective of their background history, with deliberate ignorance of Canada’s colonial history and its modern implications of Indigenous communities, especially Indigenous women. Jodi Beniuk, an Indigenous scholar, deemed the Inquiry as a “public spectacle” used by the government and police authorities to regain institutional legitimacy. Many found the Inquiry’s choice, use, and reference to historical events, themes, and evidence to be supportive of government narratives, rather than critical of the government and police authorities' shortcomings. Chief Jackie Thomas of the Saik'uz First Nation rejected the inquiry entirely, viewing it as an attempt at "rewashing" the issue of missing women, failing to address both the individuals and underlying issues that contribute to the problem. 


== Similar projects ==
The high profile of the BC investigation caused other Canadian jurisdictions to create new task forces to deal with missing persons cold cases.  Often these cases involve marginalized women, including sex workers, drug users, and Aboriginals.  In Manitoba, Project Devote was looking for 28 people as of 2012. Since 2003, Project KARE has been looking for "cases of murdered or missing high risk persons from all parts of Alberta".


== See also ==
Highway of Tears
Green River Killer
Disappeared Indigenous Women
Death of Tina Fontaine


== References ==


== External links ==
Missing Women Commission of Inquiry
[2]
CTV.ca | B.C. missing women investigation cost $70M
RCMP Media Relations Website
missing people.net