The Meddler is a 2015 American comedy-drama film written and directed by Lorene Scafaria. The film stars Susan Sarandon, Rose Byrne and J. K. Simmons. Principal photography began on March 30, 2015 in Los Angeles. It was screened in the Special Presentations section of the 2015 Toronto International Film Festival. The film was released on April 22, 2016, by Sony Pictures Classics.


== Plot ==
An aging widow, Marnie (Susan Sarandon) is lonely and heartbroken. When her daughter, Lori (Rose Byrne), moves to Los Angeles, she  follows along with the hope of restarting her life. She begins interfering with Lori's life, but soon she meets other people who are more in need of her assistance, and she starts helping them.


== Cast ==


== Production ==
Principal photography on the film began on March 30, 2015, in Los Angeles.


== Release ==
On August 25, 2015, Sony Pictures Classics acquired the North American rights to the film.
The film had its world premiere at the 2015 Toronto International Film Festival on September 14, 2015  and also screened at the 2016 Tribeca Film Festival. The film was released on April 22, 2016 and has earned $4.25 million at the domestic box office as of August 2016.


== Reception ==
The Meddler received positive reviews from critics. The film holds an 85% approval rating on review aggregator website Rotten Tomatoes, based on 143 reviews, with an average rating of 6.96/10. The website's critical consensus reads, "The Meddler transcends its cutesy title and familiar premise with a heartfelt look at family dynamics that's honored by a marvelous performance from Susan Sarandon." On Metacritic, the film holds a rating of 68 out of 100, based on 32 critics, indicating "generally favorable reviews".Peter Debruge of Variety gave the film a positive review, stating that "writer-director Lorene Scafaria’s sophomore feature returns to what works for her, as she draws upon personal experience to deliver a heartfelt dramedy that audiences are sure to appreciate. Bound to be among Sony Classics’ top 2016 performers, The Meddler serves as a lovely valentine not just to Scafaria’s mom, Gail, but to mothers everywhere — including the luminous Susan Sarandon in a role that seems to come naturally." Kevin Jaugernauth of Indiewire.com gave the film a B- writing : "But it’s the movie as it stands that must be assessed, and “The Meddler” is earnest and honest, perhaps much like Marnie. The character is eager to help and be involved, and the film carries much of that same spirit: it will try to please you with one thing, but if that doesn’t work, it has another way to make you smile just around the corner. And just like Marnie, it’s hard to resist."Richard Lawson of Vanity Fair named it the best film of 2016.


== References ==


== External links ==
Official website
The Meddler on IMDb
The Meddler at Box Office Mojo
The Meddler at Metacritic
The Meddler at Rotten Tomatoes