Keeping Up Appearances is a British sitcom created and written by Roy Clarke. It originally aired on BBC1 from 1990 to 1995. The central character is an eccentric and snobbish lower middle class social climber, Hyacinth Bucket (Patricia Routledge), who insists that her surname is pronounced "Bouquet". The show comprised five series and 44 episodes, four of which are Christmas specials. Production ended when star Patricia Routledge moved on to other projects. All episodes have since been released on DVD.
The sitcom follows Hyacinth in her attempts to prove her social superiority, and to gain standing with those she considers upper class. Her attempts are constantly hampered by her lower class extended family, whom she is desperate to hide. Much of the humour comes from the conflict between Hyacinth's vision of herself and the reality of her underclass background. In each episode, she lands in a farcical situation as she battles to protect her social credibility.
Keeping Up Appearances was a great success in the UK, and also captured large audiences in the US, Canada, Australia, Denmark, Finland, Sweden, Ireland, Belgium, and the Netherlands. By February 2016, it had been sold nearly 1,000 times to overseas broadcasters, making it BBC Worldwide's most exported television programme. In a 2004 BBC poll it placed 12th in Britain's Best Sitcom. In a 2001 Channel 4 poll Hyacinth was ranked 52nd on their list of the 100 Greatest TV Characters. The show has been syndicated on Gold and Drama in the UK, on PBS member stations in the United States and on 7TWO and 9Gem in Australia.
In September 2016, BBC One transmitted a 30-minute prequel, titled Young Hyacinth, in which Kerry Howard plays 19-year-old Hyacinth Walton, who is working as a maid in 1950s Britain.


== Background ==
Hyacinth Bucket (Patricia Routledge) – who insists her surname is pronounced Bouquet (although her husband Richard has said, "It was always 'Bucket' until I met you!") – is an overbearing, social-climbing snob, originally from a lower-class background, whose main mission in life is to impress others with her refinement and pretended affluence.
She is terrified that her background will be revealed, and goes to great lengths to hide it. Hyacinth likes to spend her days visiting stately homes (convinced she will meet and strike up a friendship with the upper-class owners, especially if they are aristocratic) and hosting "executive-style" candlelight suppers (with her Royal Worcester double-glazed Avignon china and Royal Doulton china with "the hand-painted periwinkles").She ostentatiously brags about her possessions, including her "white slimline telephone with automatic redial", which she always answers with "The Bouquet residence, the lady of the house speaking." (Frequently she receives misdialled calls asking for a Chinese take-away, making her angry.) She speaks in an exaggerated RP-like accent with Northern undertones, while her relatives speak in broad Northern accents. Her neighbours speak in milder RP accents. When flustered, Hyacinth regresses to her native Northern accent for a while.
Hyacinth's attempts to impress make the lives of those around her difficult; her continual efforts to improve her social position usually involve inviting her unwitting neighbours and friends to "exclusive candlelit suppers". Although Hyacinth is not deterred by the lack of response to her attempts, nearly everyone around her lives in fear of being invited, and usually makes frantic attempts to excuse themselves.
Her husband Richard (Clive Swift) bears the brunt of the suffering. He initially worked for the council but, at the beginning of series 3, reluctantly accepts early retirement. Although he loves her with a long-suffering endurance, he is notably exasperated by her plans and her habit of making extravagant and unnecessary purchases.
Although she lives to impress others, Hyacinth regularly competes with the middle-class neighbours (whom she considers snobbish show-offs) such as Sonia Barker-Finch, Delia Wheelwright and Lydia Hawksworth (who alone of Hyacinth's rivals seems to be an actual snob, as she disdains kiwifruit as "lower middle class".) Hyacinth sometimes says things like "I haven't a snobbish bone in my body" or "I can't abide such snobbery like that" when talking about those she considers her competition.
Always hindering Hyacinth's best efforts to impress, and providing an unwelcome reminder of her less-than-refined roots, are her lower-class sisters Daisy (Judy Cornwell) and Rose (Shirley Stelfox in series 1; Mary Millar thereafter), and Daisy's proudly "bone-idle" husband Onslow (Geoffrey Hughes). They, along with Hyacinth's senile father, frequently turn up inconveniently (usually in their clapped–out Ford Cortina Mk IV – which always makes a characteristic backfire when it arrives), with Hyacinth going to great lengths to avoid them (saying: "Richard, you know I love my family, but that's no reason why I should have to acknowledge them in broad daylight!"). Rose is very sexually aggressive and promiscuous, adding to Hyacinth's embarrassment.
Hyacinth's senile father frequently has flashbacks to the Second World War, and often exhibits bizarre behaviour, sometimes involving embarrassing situations with women (Onslow describes him as "barmy"). Two relatives of whom Hyacinth is not ashamed are her wealthy sister Violet (Anna Dawson) and her unseen son Sheridan. Violet frequently telephones Hyacinth for advice, allowing her to loudly announce to anyone in earshot, "It's my sister Violet – the one with a Mercedes, swimming pool/sauna and room for a pony". However, Violet's social acceptability is damaged by the eccentric behaviour of her cross-dressing, equestrian-loving turf accountant husband Bruce, whom she violently attacks because of his behaviour.
Hyacinth also tries to impress people with the intellectual prowess of her beloved son Sheridan (who actually only takes a course in needlework at a polytechnic). Hyacinth boasts about the "psychic" closeness of their relationship and how often he writes and phones her, although he never writes and only phones his mother to ask for money (much to the despair of Richard). Hyacinth is blissfully oblivious of the seemingly obvious hints that Sheridan, who lives with a man named Tarquin (who makes his own curtains, wears silk pyjamas, and has won prizes for embroidery), is gay. It is at one point implied that Sheridan has come out to his father.
Hyacinth's neighbour Elizabeth Warden (Josephine Tewson) is frequently invited round to the Buckets' for coffee. Though she is ordinarily calm, Elizabeth's nerves go to pieces in Hyacinth's house, causing her to smash Hyacinth's china and spill coffee and biscuits on Hyacinth's Burmese rug. She is married with a daughter away at university, yet her husband works abroad and, like Sheridan, neither character ever appears.
Elizabeth is occasionally able to "one-up" Hyacinth herself by reminding her neighbour that her daughter is at university, whilst Sheridan is studying at a mere polytechnic. Liz's brother Emmet (David Griffin) moves in with her at the beginning of series 2 after a messy divorce. Hyacinth, upon learning that Emmet is a musician, frequently and abruptly sings out-of-key at him to try to get a part in one of his productions, making him terrified of leaving the house, lest she see him ("She'll sing at me!"). Emmet's problems are made worse by Hyacinth's mistaken belief that his frightened reactions indicate that he is infatuated with her; which, in fact, could not be further from the truth.
Hyacinth frequently confronts the postman with complaints, such as having to receive mail bearing second class stamps, harassing him to the point that he will go to extreme lengths not to face her; and she often forces workmen and other visitors to her home to remove their shoes before entering. Michael, the vicar of the local church (Jeremy Gittins) is also loath to face the overbearing Hyacinth, to whom he refers (to others) as "the Bucket woman". The vicar and his wife sometimes exact comic revenge on Hyacinth for her snobbishness; on one occasion, when she was one of a group of volunteer helpers at the church, the vicar's wife saw to it that Hyacinth's hand went up prematurely and assigned her the job of cleaning the church toilets.


== Cast ==


== Episodes ==

Keeping Up Appearances aired for five series, four Christmas specials, and one short Children in Need special specials from 29 October 1990 to 25 December 1995. 2 later specials were produced in 2004 and 2008 for PBS TV. The series officially ended after the episode "The Pageant", because Patricia Routledge wanted to focus on other TV and theatre work, including Hetty Wainthropp Investigates which began airing in 1996. Clive Swift, who portrayed Richard, stated in a BBC interview that Routledge "didn't want to be remembered as simply 'Mrs. Bucket'".


== Specials ==
"The Memoirs of Hyacinth Bucket" (1997)
In 1997, BBC America produced a one-off TV special retrospective clip show which was later aired on  PBS. Daisy and Onslow find Hyacinth's diary and start reading in it. The TV-special airs clips from previous episodes.
"Life Lessons From Onslow" (2008)
In 2008 another special was also issued by PBS starring Onslow. It shows him as he presents lessons for life when hosting an Open University program, by showing various moments from the series. This was the only episode not written or directed by Roy Clarke and Harold Snoad. This special was written by Michelle Street and directed by Duane Huey. Both specials subsequently have been issued on DVD.


== Production ==


=== Locations ===

Exterior shots around Hyacinth's house were recorded at 117 Heather Road in the village of Binley Woods, Warwickshire, east of Coventry.Exterior shots around Daisy's and Onslow's council terrace were taken in Stoke Aldermoor in Coventry. Other exterior street and town shots were filmed in Royal Leamington Spa and in various towns in Warwickshire, along with many scenes from the large town of Northampton, mainly the church hall. Some scenes were also shot in Swindon, Oxford, and Bristol. One scene was shot in Oslo, Norway. One episode was filmed in Great Yarmouth, mainly in the Great Yarmouth Pleasure Beach Theme Park.
The opening sequence shows Hyacinth writing an invitation to one of her trademark candlelight suppers; this invitation lists Hyacinth's address as "Waney Edge, Blossom Avenue, Fuddleton". In the same sequence, the invitation is eventually sent to an address in Eddleton. Neither town actually exists. However, there are several references to the characters being in the West Midlands throughout the series, as when Hyacinth said that she could become the "Barbara Cartland of the West Midlands Social Circuit Scene" in the episode "The Hostess". In another episode, police officers wearing West Midlands Police jumpers escort Richard home and several West Midlands Travel buses are also seen throughout the series.


=== Vehicles ===
Richard's car is a Rover 200-series (SD3) saloon. Early episodes show a light blue 1987 216S bearing the number plate D541 EXL, but later episodes feature a sky-blue 1989 216SE EFi model (with the same number plate except for one letter, now D541 EFL).Onslow drives a 1978 Ford Cortina (number plate VSD 389S) that is in poor condition and backfires loudly almost every time it starts or stops, embarrassing Hyacinth, and frequently crushing her hopes of creating a perfect impression with new people. Onslow is also the owner of the rusting carcass of a Hillman Avenger in his front garden, in which lives Onslow's dog that always barks at Hyacinth as she approaches.Violet and Bruce own a Mercedes-Benz W126 S-class and later a Mercedes-Benz W202 C-class.
Neighbour Elizabeth drives a white 1989 Austin Metro City hatchback with number plate F434 RLA (which, despite being the subject of comments from Hyacinth about its age, is newer than Richard's car).


=== After Keeping Up Appearances ===
Various shows related to the programs were released:

The Memoirs of Hyacinth BucketIn March 1997, Geoffrey Hughes and Judy Cornwell reprised their roles as Onslow and Daisy for a special compilation episode recorded for broadcast in the United States on PBS. The show sees the pair introduce classic clips from the series.

Comedy ConnectionsIn 2004, the documentary series featured an episode dedicated to Keeping Up Appearances. Stars Clive Swift, Josephine Tewson, Judy Cornwell and David Griffin, along with writer Roy Clarke and producer/director Harold Snoad, all discussed the series. Clips from an interview with Patricia Routledge from 2002 were also included.
The episode revealed that there were serious artistic differences between Clarke and Snoad. Specifically, Clarke refused to act as anything but a writer and rarely visited the set or location shoots, necessitating that Snoad make minor rewrites to accommodate the realities of taping. This infuriated Clarke, who disliked any tampering of his work, and Snoad thought Clarke was being unreasonable. The situation was mitigated in later series by the hiring of a production assistant whose sole job was to keep Clarke apprised of Snoad's changes, and to keep Snoad informed of Clarke's opinion of them.
Life Lessons from OnslowIn early 2008, Geoffrey Hughes reprised his role as Onslow once again for a clipshow of the series; this was to be broadcast on American television, and sees him teaching a credit course at the Open University, and has selected "successful relationships" as his subject matter. The special was also released on Region 1 DVD.

Mind Your ManorsAs described by both Roy Clarke and Geoffrey Hughes as part of a discussion of the character of Onslow in Radio 4's Archive on 4: On Northern Men broadcast in 2009, as well as referenced in in-house BBC 1997-8 literature, when Keeping Up Appearances didn't return after Patricia Routledge announced she no longer wished to play Hyacinth, Clarke's proposed spin-off series would have seen Onslow forced to take a supposedly "easy" job tending the gardens of a large manor house estate owned by a doddery and rather forgetful old Lord. When the Lord passes away suddenly, Onslow, Daisy and Rose (who it is implied was having a fling with the Lord, causing the heart attack which led to his passing) end up 'temporarily' moving into the manor house to mind it, due to a legal dispute with the deceased Lord's estranged brother (whom Rose immediately starts lusting after) who wants the land. 
A mislaid deeds paper left by the forgetful old Lord would supposedly clarify who has rights to the estate, buried somewhere in the gardens that Onslow is left to ineptly try and keep in shape. Most of the conflict would have come from the Lord's brother and the manor's snooty neighbours, the wealthy Hyde-Whytes, who are not happy about the "commoners" who have taken up residence next door (although it later transpires that Mr. Hyde-Whyte used to know Onslow in their youth and is indebted to him after Onslow once took the fall for him to stop him being arrested)
Su Pollard was approached to play the role of Miss Dorothy "Dotty" Henshaw, the Lord's eccentric and easily flustered cook / house-keeper who remains with the estate, and some preliminary location shooting was reportedly undertaken, though this may be connected with the "missing" final episode of Series 5, "The Bishop Affair". This proposed follow-up series was ultimately not commissioned, with Geoffrey Hughes continuing with his portrayal as Onslow in the above listed specials instead.
Mary Millar, who played Rose from series two to series five, died on 10 November 1998. George Webb, who played Daddy, died on 30 December 1998. Charmian May, who played Mrs. Councillor Nugent, died on 24 October 2002. Geoffrey Hughes, who played Onslow, died on 27 July 2012. Shirley Stelfox, who played Rose in series one, died on 7 December 2015. Clive Swift, who played Richard, died on 1 February 2019.


== Merchandise ==


=== Audio ===
In 1998, the BBC released three episodes of the show: "A Job for Richard", "Country Retreat" and "Sea Fever" on audio cassette. Clive Swift reprised his role as Richard recording a narrative to compensate for the lack of images.


=== VHS ===
BBC Video released three videos featuring episodes from the series.

How to Enhance Your Husband's RetirementThis was released in 1993 and featured the episodes: "Iron Age Remains", "What to Wear when Yachting" and "How to Go on Holiday Without Really Trying".

Sea FeverThis was released in 1994 and featured the episodes: "Sea Fever" and "A Job for Richard".

Rural RetreatThis was released in 1995 and featured the episodes: "Country Retreat", "Let There Be Light" and "Please Mind Your Head".


=== DVD ===
United States – Region 1My Way or the Hyacinth Way (volume 1) – released 18 March 2003 (contains the complete series 1)
Hints from Hyacinth (volume 2) – released 18 March 2003 (contains episodes 1–5 from series 2)
Home is Where the Hyacinth Is (volume 3) – released 18 March 2003 (contains episodes 6–10 from series 2)
Deck the Halls with Hyacinth (volume 4) – released 18 March 2003 (contains all 4 Christmas Specials from 1991, 1993, 1994 & 1995)
Hyacinth in Full Bloom – released 18 March 2003 (contains volumes 1–4)
Everything's Coming Up Hyacinth (volume 5) – released 3 February 2004 (contains the complete series 3)
Some Like it Hyacinth (volume 6) – released 3 February 2004 (contains the complete series 4)
Living the Hyacinth Life (volume 7) – released 3 February 2004 (contains episodes 1–6 from series 5)
Hats Off to Hyacinth (volume 8) – released 3 February 2004 (contains episodes 7–10 from series 5 & the special "The Memoirs of Hyacinth Bucket")
Hyacinth Springs Eternal – released 3 February 2004 (contains volumes 5–8)
The Full Bouquet – released 7 September 2004 (complete series – contains volumes 1–8)
The Full Bouquet: Special Edition – released 9 September 2008 (complete series re-release, everything from volumes 1–8 & special "Life Lessons from Onslow")
Life Lessons from Onslow – released 9 September 2008
Collector's Edition – 5 November 2013 (contains all 5 series and every special)United Kingdom – Region 2Series 1 & 2 – released 7 March 2003
Series 3 & 4 – released 16 February 2004 (includes the 1991 Christmas special)
Series 5 – released 26 December 2006 (includes Christmas Specials from 1993, 1994 & 1995)
The Essential Collection – released 8 October 2007 (complete series)
The Complete Collection – released 23 September 2013 (complete series re-release, slimmer version)
Christmas Specials – released 3 November 2014 (contains all 4 Christmas specials)
The Complete Collection - released 22 January 2018 (HMV Exclusive)Australia – Region 4Series 1 & 2 – released 1 September 2003
Series 3 & 4 – released 8 July 2004
Series 5 – released 3 November 2005
Christmas Specials – released 11 November 2005
The Complete Collection – released 1 October 2005
Life Lessons from Onslow – released 15 April 2010
The Full Bouquet – released 3 November 2011


=== Streaming ===
In the United States, the complete series was available via streaming through Netflix and Amazon Video,  but is no longer available to stream on either platform. However, it is available for streaming on BritBox. In Australia the complete series is available for streaming on Stan as of 2018.


=== Books ===
Three books related to the series have been released in the UK. Two were written by Jonathan Rice and published by BBC Books and the other one was written by Harold Snoad (the director of Keeping Up Appearances) and was published by Book Guild Publishing.

Hyacinth Bucket's book of etiquette for the socially less fortunateThis was first published in 1993, and is a light-hearted guide to manners, as seen through Hyacinth Bucket's eyes. It is based on the TV series' scripts and contains many black-and-white photos of scenes from the show.

Hyacinth Bucket's Hectic Social CalendarThis was published in 1995 and is presented in a diary format chronicling a year in Hyacinth Bucket's life, with typical comments about her relations and neighbours.

It's Bouquet – Not BucketThis was published in late 2009, the book includes rare photos which were taken during the filming of Keeping Up Appearances. The book contains full plot synopses for all episodes, main cast details, filming locations for all episodes which used outside shots, and stories of some entertaining events which happened during filming.


==== Overseas books ====
Due to the popularity of Keeping Up Appearances in the United States, books about the series have also been published across the Atlantic.

Keeping Up Appearances: A Companion to the SeriesThis comical series guidebook was published in the late 1990s by WLIW21. It was co-authored by mother-and-daughter writers Georgene and Mary Lee Costa. It features summary descriptions of each episode, cast and crew biographies, series photographs and an interview with Harold Snoad.
Since it was written during the filming of the final series of episodes, Snoad included the co-authors of the guide as extras in the episode "The Fancy Dress Ball".


== Theatre adaptation ==
In 2010, the television show was adapted into a play entitled Keeping Up Appearances that toured theatres in the UK. The cast included Rachel Bell as Hyacinth, Kim Hartman as Elizabeth, Gareth Hale as Onslow, Steven Pinder as Emmet, Debbie Arnold as Rose, David Janson (who had previously appeared in the TV show as the postman) as Mr Edward Milson, a new character created for the stage show, Christine Moore as Daisy and Sarah Whitlock as Mrs Debden.Main character Richard Bucket, Hyacinth's husband, does not appear in the production, but is frequently referred to: Hyacinth addresses him off-stage and talks to him on the phone. The main plot of the show revolves around Emmet directing a play at the local village hall, but when Hyacinth is cast in the play's leading role disaster is in the making.
This adaptation, directed by playwright Johnny Culver, made its American premiere in New York City in March 2015, at the Fifth Avenue Presbyterian Church Theater Fellowship/Jones Auditorium. Culver directed an additional production of this adaptation with performances on 5 August 2017 and 6 August 2017 at Saint Luke's Episcopal Church in Forest Hills, Queens.


== Prequel ==

In 2015, it was announced that a prequel to the series entitled Young Hyacinth would be made, following a 19-year-old Hyacinth Bucket during the early postwar years and set some forty years before the events of Keeping Up Appearances. The cast were announced on 11 April 2016. Hyacinth was played by Kerry Howard; Rose by Katie Redford; Daisy by Katherine Pearce; Violet by Tamla Kari and Daddy by Mark Addy. It was broadcast on BBC One on 2 September 2016.The prequel sees Hyacinth working as a maid and attempting to manage her family so they appear well above their means. In the special, Hyacinth is in a relationship with a Mr. William Hitchcock, whom Hyacinth forces to dress up and wear a hat when they walk. The special ends with her drunken Daddy falling into the canal, which Hyacinth's employers see, causing her great embarrassment. However, she blames Daddy's behaviour on "an old war injury".Approximately 4.14 million viewers watched the show within seven days of its broadcast, making it the 22nd most watched BBC One show for the week ending 4 September. A total of 4.39 million viewers watched the show within 28 days of its initial broadcast.


== References ==


== External links ==
Keeping Up Appearances at BBC Online
Keeping Up Appearances at BBC Programmes 
Keeping Up Appearances at British Comedy Guide
Keeping Up Appearances on IMDb
Keeping Up Appearances at the BFI's Screenonline
Keeping Up Appearances at TV.com