The Four Feathers is a 1902 adventure novel by British writer A. E. W. Mason that has inspired many films of the same title. In December 1901, Cornhill Magazine announced the title as one of two new serial stories to be published in the forthcoming year. Against the background of the Mahdist War, young Feversham disgraces himself by quitting the army, which others perceive as cowardice, symbolized by the four white feathers they give him. He redeems himself with acts of great courage and wins back the heart of the woman he loves.


== Plot summary ==
The novel tells the story of a British officer, Harry Feversham, who resigns from his commission in the Royal North Surrey Regiment just before Lord Garnet Wolseley's 1882 expedition to Egypt to suppress the rising of Colonel Ahmed Orabi. He is censured for cowardice by three of his comrades—Captain Trench and Lieutenants Castleton and Willoughby—signified by their delivery of three white feathers to him. His fiancée, Ethne Eustace, breaks off their engagement and also gives him a white feather. His best friend in the regiment, Captain Durrance, becomes a rival for Ethne.
Harry talks with Lieutenant Sutch, a friend of his late father who is an imposing retired general. He questions his own motives, but says he will redeem himself by acts that will convince his critics to take back the feathers. He travels on his own to Egypt and Sudan, where in 1882 Muhammad Ahmed proclaimed himself the Mahdi (Guided One) and raised a Holy War. On 26 January 1885, his Dervish forces captured Khartoum and killed its British governor, General Charles George Gordon. Most of the action over the next six years takes place in the eastern Sudan, where the British and Egyptians held Suakin. Durrance is blinded by sunstroke and invalided. Castleton is reportedly killed at Tamai, where a British square is briefly broken by a Mahdi attack.
Harry's first success comes when he recovers lost letters of Gordon. He is aided by a Sudanese Arab, Abou Fatma. Later, disguised as a mad Greek musician, Harry gets imprisoned in Omdurman, where he rescues Captain Trench, who had been captured on a reconnaissance mission. They escape.
Learning of his actions, Willoughby and Trench give Ethne the feathers they had taken back from Harry. He returns to England, and sees Ethne for what he thinks is one last time, as she has decided to devote herself to the blind Durrance. But Durrance tells her his blindness is curable (a white lie) and frees her for Harry. Ethne and Harry wed, and Durrance travels to "the East" as a civilian.


== Film, TV and theatrical adaptations ==
This novel's story has been adapted as films several times, with all films retaining much of the same storyline.In the 1929 silent version, a square of Highlanders is broken, but saved by Feversham and the Egyptian garrison of a besieged fort. Set in the 1880s, its great moment comes when wild hippos in a river attack the Dervishes pursuing Feversham.
The films each feature a British square broken in a dramatic battle sequence. This is only mentioned in the novel, in a battle in which the square recovered. The various film versions differ in the precise historical context.
The 2002 version starring Heath Ledger is set during the 1884–85 campaign. The British infantry square was broken in the battle of Abu Klea and the British are forced to retreat. Critics complained that the film did not explore the characters sufficiently, and had historical inaccuracies in uniform dress. The central battle is more accurately treated in the film Khartoum (1966). The enemy forces, Islamic rebels called Dervishes, or The Mahdi, are the same, as are the geographic settings of Britain, Egypt and the Sudan.
The various film versions are as follows:


== References ==


== External links ==
 The Four Feathers at Project Gutenberg
 The Four Feathers public domain audiobook at LibriVox
The Four Feathers -- 1903 edition at Internet Archive