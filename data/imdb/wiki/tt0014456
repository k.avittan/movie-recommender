NCT Dream is a South Korean boyband which serves as the third sub-unit of K-pop boyband NCT, formed by SM Entertainment in 2016 then as the teen-aged unit of NCT, later evolved away from their youth image and was re-branded in 2020 when the members became adults. The group debuted on August 25, 2016 with the single "Chewing Gum", with seven members Mark, Renjun, Jeno, Haechan,  Jaemin,  Chenle, and Jisung, then at an average age of 15.6 years old. NCT Dream has been internationally recognised as among most notable teen-aged artists of their time, known for music reflecting the growth of the youth. The narrative of their title tracks have conveyed thoughts following different stages of adolescent development, with transition from innocence to rebellion and growth. So far, NCT Dream have released four extended plays, six singles, and headlined one international tour in Asia. Commercial success of their 2019 EP release We Boom turned NCT Dream into one of the top 10 best physical sellers of South Korea in 2019 and earned them Bonsang Awards at the 34th Golden Disc Awards and 2020 Seoul Music Awards.Although NCT Dream was initially formed to follow an aged-based admission-graduation system in which members would move on from the unit after reaching the Korean age of 20 (19 internationally), in 2020 SM Entertainment announced the admission-graduation system to be removed, with previously graduated member Mark set to return, and NCT Dream will continue future activities as a flexible-style group with the seven original members. The announcement was followed by over half a million copies pre-ordered for their fourth Korean EP, titled Reload, which was released in April 2020 to mark the shift in their concept.NCT Dream is the first and only Asian artist to have appeared three times consecutively on Billboard's "21 Under 21" list, in 2018, 2019 and 2020, at #20, #13 and #19 respectively, chosen for their industry impact on sales, streaming and social media. In 2018, when Time compiled a list of "25 Most Influential Teens" of the year, all members of NCT Dream were featured, highlighting their connection - as teenagers - to teenagers. They were the only Korean artists on both lists.


== History ==


=== Pre-debut ===
Prior to debut, Mark, Haechan, Jaemin, Jisung, and Jeno were previously a part of SM Rookies, the pre-debut training team created by SM Entertainment. Chenle previously had a solo career as a child singer, had released three albums and headlined one solo concert in China, before coming to South Korea at age 14 to debut with NCT Dream.NCT was first announced in January 2016 by Lee Soo-man at a presentation, where he talked about a group that would debut different teams around the world.Mark and Haechan are two members who had previously debuted in other sub-units of NCT and known to the public. Mark debuted in the group's first unit, NCT U, on April 4, 2016. On July 7, Haechan made his debut in the second unit, NCT 127, alongside Mark. The two members, together with five other new members, Renjun, Jeno, Jaemin, Chenle and Jisung, made their debut under the unit group NCT Dream which was announced as the third sub-unit of NCT on August 18, 2016.


=== 2016–2017: Debut, first music show win with The First, and We Young ===
NCT Dream released their debut single, "Chewing Gum", on August 24, 2016. Their debut stage was on August 25 on M Countdown. The song was ranked #2 on the World Digital Songs sales chart and #23 on the Spotify Viral 50 chart on its release. The Korean and Mandarin version of the song would later be included in NCT Dream first single album.
On February 1, 2017, SM Entertainment announced NCT Dream would release their first single album. It was revealed Jaemin would not be participating in the release in order to have time recovering from an injury to his herniated disc. The group's single album, The First, was released on February 9 with its lead single, "My First and Last". The album debuted at number one on the Gaon Chart. The unit had their comeback stage on M Countdown, performing the title track, "My First and Last", and B-side track, "Dunk Shot". On February 14, NCT Dream was revealed to have won 1st place on the 100th episode of SBS MTV's The Show, marking the first music show win for a NCT unit.In the same month, NCT Dream was appointed as official ambassadors for the 2017 FIFA U-20 World Cup which was hosted in South Korea in the summer of 2017. On March 15, NCT Dream released the official song for the U-20 World Cup tournament, titled "Trigger the Fever", and went on to perform the song at different promotional events and at the opening ceremony of the U-20 World Cup.NCT Dream released their first EP, We Young, on August 17, 2017. The group's first comeback stage was held on M Countdown.  We Young peaked at No. 3 on Billboard World Albums chart in September.NCT Dream released their Christmas single, "Joy," as part of SM Station on December 15, 2017.


=== 2018: NCT 2018: Empathy, We Go Up, and Mark's graduation ===

NCT Dream promoted alongside NCT U and NCT 127 as NCT 2018 for the first quarter of 2018. On March 4, 2018, NCT Dream released their single, "Go", which was included in NCT's first album, NCT 2018 Empathy. The song marked a transition in NCT Dream's narrative, from the portrayal of innocence to a "more defiant attitude" of teenagers. It also marked the group's first comeback with Jaemin since their debut.
SM Entertainment announced in late August that NCT Dream would be releasing an EP titled We Go Up in September. On August 27, it was announced that the album would be the last comeback with Mark who would be graduating from the group in 2019 when he would become 19 and hence reach the age of adulthood in Korea. The EP was released on September 3, 2018 alongside the lead single of the same name. We Go Up entered Billboard's World Albums chart at number five and Heatseekers Albums chart at number seven, which marks NCT Dream's then best sales week in the U.S. The album also topped iTunes Albums charts in 15 countries.NCT Dream was featured on the 21 Under 21 2018: Music's Next Generation list by Billboard as the only Asian artist for the year. All members were listed on Time's 25 Most Influential Teens of 2018 list.On December 19, it was announced Haechan would be taking a hiatus from the group due to an injury, but he returned soon afterwards. On December 27, NCT Dream released their third and final single of the year titled "Candle Light" under the SM Entertainment's digital music project SM Station. The song was a message of gratitude and love - its lyrics written by Mark - and was then the last song featuring the original seven members before Mark officially graduated from the unit on December 31.


=== 2019: International collaborations, rise to prominence with We Boom and first Asia tour ===
In March 2019, NCT Dream's Korean members Jeno, Jaemin and Jisung went to Malaysia to represent K-pop stars at event "K-Wave & Halal Show", celebrating and meeting with South Korean President Moon Jae-in and First lady of South Korea.On June 6, 2019, NCT Dream released "Don't Need Your Love", a collaborative single with English singer HRVY. The song displayed a more mature side for the group, and credited for highlighting their maturing vocals. Haechan did not partake in the single due to ongoing activities with NCT 127. The song was released as part of SM Station Season 3.In July 2019, NCT Dream was named as the first global ambassadors of the World Scout Foundation, with the group releasing the English-language single "Fireflies", the proceeds of which were used to fund Scout activities in low-income countries. The group also performed at the 24th World Scout Jamboree in West Virginia, United States
on July 23.On July 26, NCT Dream released their third EP, We Boom, and its title track, "Boom", marking their first comeback without Mark. The album became NCT Dream's best selling album up till then, selling more than 300,000 units after a month of release. It also debut on Billboard World albums chart at #7 and NCT Dream was at #3 on Billboard Social 50 Chart NCT Dream received two wins for "Boom" on The Show on August 6, 2019 and on August 20, 2019, respectively. We Boom later became the best selling album released by any NCT sub-unit up until and in the year of 2019.The EP also turned NCT Dream into one of the top 10 best physical sellers of South Korea in 2019 and earned them Bonsang Awards at the 34th Golden Disc Awards and 2020 Seoul Music Awards.In September 2019, NCT Dream was included in Billboard's 21 Under 21 list for the second year in a row, being the first Asian artists to make the list for two consecutive years, this time rising to rank #13.In November 2019, the group featured on American-Canadian boy band PrettyMuch's INTL:EP with the song "Up To You", with lyrics praised for promoting consent.The group staged their first international concert tour, The Dream Show, starting with three dates in Seoul and two dates in Bangkok from November to December 2019.


=== 2020: The Dream, new concept with Reload and return of Mark ===
On January 22, NCT Dream released a Japan-exclusive compilation album titled The Dream, consisting of all previously released singles in their original Korean language. 
The album debuted atop the Oricon Albums Chart.In early 2020, NCT Dream continued with their international tour The Dream Show, with shows held in multiple locations in Japan as well as in South East Asia countries. From February 2020, several shows were cancelled due to the COVID-19 pandemic.As the topic of graduation gradually became more concerning since the prior year, on April 14, SM Entertainment announced that NCT Dream would release their fourth Korean EP, Reload with the current six members Renjun, Jeno, Haechan, Jaemin, Chenle, and Jisung to mark a new change in their concept. After the album's promotion period, the graduation system will be abolished and NCT Dream will continue its activity with all seven members, including original member Mark who previously graduated at the end of 2018. NCT Dream will function as a more fixed group with diverse concepts in each promotion - similar to NCT U - and the seven original members would continue their activities under the name NCT Dream as well as have the possibility to take part in other activities including other NCT units in the future.By April 28, 2020, a day before release and within two weeks from the first announcement, Reload has garnered over 500,000 unit pre-orders, establishing itself as NCT Dream's release with the highest pre-order to date. On April 29, NCT Dream released their EP Reload with its lead single "Ridin'", an urban trap track with lyrics highlighting the aspirations of NCT Dream in their coming new path. The music video for Ridin was released on the same day. On release, the album topped the iTunes album chart in 49 countries including the U.S. as well as topping China's largest music site QQ digital album sales chart. In South Korea, the title track Ridin reached #1 on Korea's largest music site Melon, making it the first track released by an NCT unit to top the chart. All five songs from the EP peaked within the top 10 on Melon digital chart. Within the following week, NCT Dream ranked No. 1 on Billboard's "Emerging Artists" chart.After the release of their fourth Korean EP, NCT Dream was the third group from SM Entertainment to hold a live online concert, titled "NCT Dream - Beyond the Dream Show", as part of the world's first online-dedicated live concert series Beyond LIVE. The live concert was held on May 10.NCT Dream rejoined promoting with the rest of NCT as NCT 2020 through the united group's second full-length album NCT 2020 Resonance Pt. 1, which was released on October 12. Included in the album is "Déjà Vu," which served as Mark's return to the group following Dream's re-organized structure.


== Members ==
Mark (마크)
Renjun (런쥔)
Jeno (제노)
Haechan (해찬)
Jaemin (재민)
Chenle (천러)
Jisung (지성)


== Discography ==


=== Extended plays ===
We Young (2017)
We Go Up (2018)
We Boom (2019)
Reload (2020)


== Awards and nominations ==


== Shows and concerts ==


=== Headlining ===
NCT Dream Show (2018)
NCT Dream Tour "The Dream Show" (2019)
NCT Dream - Beyond the Dream Show


=== Joint tours ===
SM Town Live World Tour VI (2017–2018)


== References ==