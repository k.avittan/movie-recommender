The idiom "the straw that broke the camel's back", alluding to the proverb "it is the last straw that breaks the camel's back", describes the seemingly minor or routine action that causes an unpredictably large and sudden reaction, because of the cumulative effect of small actions.
This gives rise to the phrase "the last straw", or "the final straw", meaning that the last one in a line of unacceptable occurrences causes a seemingly sudden, strong reaction.


== Origins and early uses ==
The earliest known version of the expression comes in a theological debate on causality by Thomas Hobbes and John Bramhall in 1654-1684:

The last Dictate of the Judgement, concerning the Good or Bad, that may follow on any Action, is not properly the whole Cause, but the last Part of it, and yet may be said to produce the Effect necessarily, in such Manner as the last Feather may be said to break a Horses Back, when there were so many laid on before as there want but that one to do it.
An essay of 1724 emphasizes not the fact of being the last cause, but rather of being a least cause, that is, a minor one:

Every thing must be at rest which has no Force to impell it; but as the least Straw breaks the Horse's Back, or a single Sand will turn the Beam of Scales which holds Weights as heavy as the World; so, without doubt, as minute Causes may determine the Actions of Men, which neither others, nor they themselves are sensible of...
Attested versions of the proverb include, in chronological order:

"It is the last feather that breaks the horse's back" (1677)
"It is the last straw that overloads the camel", mentioned as an "Oriental proverb" (1799)
"It was the last ounce that broke the back of the camel" (1832)
"The last straw will break the camel's back" (1836)
"As the last straw breaks the laden camel's back" (1848)
"This final feather broke the camel's back" (1876)Other variants are:
"The straw that broke the donkey's back"
"The last peppercorn breaks the camel's back"
"The melon that broke the monkey's back"
"The feather that broke the camel's back"
"The straw that broke the horse's back"
"The hair that broke the camel's back"
"The last ounce broke the camel's back"


== The last drop ==
The same sentiment is also expressed by the phrase "the last drop makes the cup run over", first found in English as "When the Cup is brim full before, the last (though least) superadded drop is charged alone to be the cause of all the running over" (1655).
The image of the last drop is also found in many other languages.


== Antecedents ==
The phrase has been compared with Seneca's discussion on why death is not to be feared. Starting with a mention of the commonplace "we do not suddenly fall on death, but advance towards it by slight degrees; we die every day" (non repente nos in mortem incidere, sed minutatim procedere; cotidie morimur), Seneca compares life to a water-clock:

It is not the last drop that empties the water-clock, but all that which previously has flowed out; similarly, the final hour when we cease to exist does not of itself bring death; it merely of itself completes the death-process. We reach death at that moment, but we have been a long time on the way.
Quemadmodum clepsydram non extremum stillicidium exhaurit, sed quicquid ante defluxit, sic ultima hora, qua esse desinimus, non sola mortem facit, sed sola consummat; tunc ad illam pervenimus, sed diu venimus.

In contrast to the imagery of the "last straw", which emphasizes dramatic final result, Seneca emphasizes the continuity of the final hour of life with all the hours that have come before.


== See also ==
Grasp at straws (wiktionary)


== References ==