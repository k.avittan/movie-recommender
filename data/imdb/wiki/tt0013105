Evidence, broadly construed, is anything presented in support of an assertion, because evident things are undoubted. There are two kind of evidence: intellectual evidence (the obvious, the evident) and empirical evidence (proofs).
The mentioned support may be strong or weak. The strongest type of evidence is that which provides direct proof of the truth of an assertion. At the other extreme is evidence that is merely consistent with an assertion but does not rule out other, contradictory assertions, as in circumstantial evidence.
In law, rules of evidence govern the types of evidence that are admissible in a legal proceeding. Types of legal evidence include testimony, documentary evidence, and physical evidence. The parts of a legal case which are not in controversy are known, in general, as the "facts of the case." Beyond any facts that are undisputed, a judge or jury is usually tasked with being a trier of fact for the other issues of a case. Evidence and rules are used to decide questions of fact that are disputed, some of which may be determined by the legal burden of proof relevant to the case. Evidence in certain cases (e.g. capital crimes) must be more compelling than in other situations (e.g. minor civil disputes), which drastically affects the quality and quantity of evidence necessary to decide a case.
Scientific evidence consists of observations and experimental results that serve to support, refute, or modify a scientific hypothesis or theory, when collected and interpreted in accordance with the scientific method.
In philosophy, the study of evidence is closely tied to epistemology, which considers the nature of knowledge and how it can be acquired.


== Intellectual evidence (the evident) ==
The first thing discovered in history is that evidence is related to the senses. A footprint has stayed in the language: the word anchors its origin in the Latin term evidentia, which comes from videre, vision. In this sense, evidence is what falls under our eyes. Something similar happened in ancient philosophy with Epicurus. He considered all knowledge to be based in sensory perception: if something is perceived by the senses, it is evident, it is always true (cf. Letter to Diogenes Laertius, X, 52).
Aristotle went beyond that concept of evidence as simple passive perception of the senses. He observed that, although all superior animals could have sensory experiences of things, only human beings had to conceptualize them and penetrate more and more into their reality (cf. Metaphysics, 449, b; About the Memory, 452, a; Physics I, c. 1). This certain understanding that the intellect obtains things when it sees them, it makes it in an innate and necessary way (it is not something acquired, as can be the habit of science, of which he speaks in Ethics IV). For Aristotle the evidence it not merely passive perception of reality, but a gradual process of discoveries, a knowledge that "determines and divides" better and better the "undetermined and undefined": it begins with what is most evident for us, in order to end with what is truer and more evident in nature.
Aquinas would later deepen the distinction of evidence quad nos and quad se already suggested by Aristotle (cf. Summa Th. I, q. 2, sol.). Neither of the two understood evidence in purely logical or formal terms, like many schools of thought tend to understand today. His theory of knowledge proves to be much richer. In philosophical realism, the senses (sight, sound, etc.) provide correct data of what reality is; they do not lie to us, unless they are atrophied. When the sensitive species (or the Aristotelian phantom) formed by the inferior powers is captured by intelligence, it immediately knows and abstracts data from reality; the intelligence with its light, through "study", "determination" and "division" will end up forming concepts, judgements and reasoning. That first immediate acquisition of reality, devoid of structured reasoning, is the first evidence captured by the intellect. Then the intellect is aware of other obvious truths (such as 2+2=4 or that "the total is greater than or equal to the part") when it compares and relates the previously assimilated knowledge.
Scholastic tradition considered that there existed some "primary principles of practical reason", known as immediately and clearly, that could never be broken or repealed. These moral principles would be the most nuclear of natural law. But in addition to those, there would be another part of natural law (formed by deductions or specifications of those principles) that could vary with time and with changing circumstances (cf. Summa Th. I-II, q. a. 5, sol.). In this way, the natural law would consist of some small immutable principles and by enormous variable content.
Finnis, Grisez and Boyle point out that what is self-evident cannot be verified by experience, nor derived from any previous knowledge, nor inferred from any basic truth through a middle ground. Immediately they point out that the first principles are evident per se nota, known only through the knowledge of the meanings of the terms, and clarify that "This does not mean that they are mere linguistic clarifications, nor that they are intuitions-insights unrelated to data. Rather, it means that these truths are known (nota) without any middle term (per se), by understanding what is signified by their terms." Then when speaking specifically about the practical principles, they point out that they are not intuitions without contents, but their data come from the object to which natural human dispositions tend, that motivate human behavior and guide actions (p. 108). Those goods to which humans primarily tend, which cannot be "reduced" to another good (it is to say, that they are not means to an end), they are considered "evident": "as the basic good are reasons with no further reasons" (p. 110).
George Orwell (2009) considered that one of the principal duties of today's world is to recover what is obvious. Actually, when the manipulation of language for political ends grows strongly, when "war is peace", "freedom is slavery", "ignorance is strength", it is important to rediscover the basic principles of the reason. Riofrio has designed a method to validate which ideas, principles or reasons can be considered "evident", testing in that ideas all the ten characteristics of the evident things.


== Empirical evidence (in science) ==

In scientific research evidence is accumulated through observations of phenomena that occur in the natural world, or which are created as experiments in a laboratory or other controlled conditions. Scientific evidence usually goes towards supporting or rejecting a hypothesis.
The burden of proof is on the person making a contentious claim.  Within science, this translates to the burden resting on presenters of a paper, in which the presenters argue for their specific findings. This paper is placed before a panel of judges where the presenter must defend the thesis against all challenges.
When evidence is contradictory to predicted expectations, the evidence and the ways of making it are often closely scrutinized (see experimenter's regress) and only at the end of this process is the hypothesis rejected: this can be referred to as 'refutation of the hypothesis'.  The rules for evidence used by science are collected systematically in an attempt to avoid the bias inherent to anecdotal evidence.


== Law ==

In law, the production and presentation of evidence depends first on establishing on whom the burden of proof lies.  Admissible evidence is that which a court receives and considers for the purposes of deciding a particular case. Two primary burden-of-proof considerations exist in law.  The first is on whom the burden rests.  In many, especially Western, courts, the burden of proof is placed on the prosecution in criminal cases and the plaintiff in civil cases.  The second consideration is the degree of certitude proof must reach, depending on both the quantity and quality of evidence.  These degrees are different for criminal and civil cases, the former requiring evidence beyond a reasonable doubt, the latter considering only which side has the preponderance of evidence, or whether the proposition is more likely true or false.  The decision maker, often a jury, but sometimes a judge, decides whether the burden of proof has been fulfilled.
After deciding who will carry the burden of proof, evidence is first gathered and then presented before the court:


=== Collection ===
In criminal investigation, rather than attempting to prove an abstract or hypothetical point, the evidence gatherers attempt to determine who is responsible for a criminal act.  The focus of criminal evidence is to connect physical evidence and reports of witnesses to a specific person.


=== Presentation ===
The path that physical evidence takes from the scene of a crime or the arrest of a suspect to the courtroom is called the chain of custody. In a criminal case, this path must be clearly documented or attested to by those who handled the evidence. If the chain of evidence is broken, a defendant may be able to persuade the judge to declare the evidence inadmissible.
Presenting evidence before the court differs from the gathering of evidence in important ways. Gathering evidence may take many forms; presenting evidence that tend to prove or disprove the point at issue is strictly governed by rules. Failure to follow these rules leads to any number of consequences. In law, certain policies allow (or require) evidence to be excluded from consideration based either on indicia relating to reliability, or broader social concerns. Testimony (which tells) and exhibits (which show) are the two main categories of evidence presented at a trial or hearing. In the United States, evidence in federal court is admitted or excluded under the Federal Rules of Evidence.


=== Burden of proof ===

The burden of proof is the obligation of a party in an argument or dispute to provide sufficient evidence to shift the other party's or a third party's belief from their initial position. The burden of proof must be fulfilled by both establishing confirming evidence and negating oppositional evidence. Conclusions drawn from evidence may be subject to criticism based on a perceived failure to fulfill the burden of proof.
Two principal considerations are:

On whom does the burden of proof rest?
To what degree of certitude must the assertion be supported?The latter question depends on the nature of the point under contention and determines the quantity and quality of evidence required to meet the burden of proof.
In a criminal trial in the United States, for example, the prosecution carries the burden of proof since the defendant is presumed innocent until proven guilty beyond a reasonable doubt. Similarly, in most civil procedures, the plaintiff carries the burden of proof and must convince a judge or jury that the preponderance of the evidence is on their side. Other legal standards of proof include "reasonable suspicion", "probable cause" (as for arrest), "prima facie evidence", "credible evidence", "substantial evidence", and "clear and convincing evidence".
In a philosophical debate, there is an implicit burden of proof on the party asserting a claim, since the default position is generally one of neutrality or unbelief. Each party in a debate will therefore carry the burden of proof for any assertion they make in the argument, although some assertions may be granted by the other party without further evidence. If the debate is set up as a resolution to be supported by one side and refuted by another, the overall burden of proof is on the side supporting the resolution.


== Types ==
Digital evidence
Personal experience
Physical evidence
Relationship evidence
Scientific evidence
Testimonial evidence
Trace evidence


== See also ==


== References ==


== External links ==
Evidence at PhilPapers
Zalta, Edward N. (ed.). "Evidence". Stanford Encyclopedia of Philosophy.
"Evidence". Internet Encyclopedia of Philosophy.
Evidence at the Indiana Philosophy Ontology Project
ASTM E141 Standard Practice for Acceptance of Evidence Based on the Results of Probability Sampling
"Evidence" . Encyclopædia Britannica (11th ed.). 1911.