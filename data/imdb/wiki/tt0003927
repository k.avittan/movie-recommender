Huda Beauty is a cosmetics line launched in 2013 by Iraqi-American businesswoman and makeup artist, Huda Kattan. The founder, Kattan, was chosen as one of "The 25 Most Influential People on the Internet" by Time in 2017, listed as one of The Richest Self-Made Women and one of the Top Three Beauty Influencers by Forbes. In the span of 5 years, the brand has built a positive reputation on some of its products, such as fake eyelashes series, a collection of foundation, eyeshadow and some face palettes.


== Background ==
In April 2010, Huda Kattan started the beauty-related blog, Huda Beauty, and a YouTube channel. She later found success on other platforms. Kattan launched a cosmetic line named after her channel in 2013. It has since become one of the world's fastest-growing beauty brands.As of 2020, Huda Beauty has more than 4 million subscribers on YouTube and is the number one account on the Instagram Beauty section in the world, with over 40 million followers. The contents of Kattan's channels are beauty tutorial-oriented: Kattan shares makeup techniques, skincare routines and personal preferred beauty products from multiple brands.


== Launch ==
The first Huda Beauty product was a collection of false eyelashes released through Sephora, in Dubai in 2011 and in the United States in 2015. The Kardashian sisters were reported to use Huda Beauty lashes, providing an early publicity boost to the label. As of 2018, Huda Beauty has an estimated net value of US$550 million and the company as a business is valued at over a billion dollars, according to Forbes.In December 2017, the company received a minority investment from TSG Consumer Partners, a private equity firm that had previously invested in beauty brands such as 'Smashbox' and 'Its Cosmetics' (both of which were purchased by major beauty conglomerates — Estée Lauder and L'Oréal).


== Products ==
Huda Beauty offers more than 140 products online or in-store. The beauty brand has launched an entire range of products, including lipstick collections, face palettes like highlighter and contour, false lashes, and a collaborative product with Tweezerman. Its Samantha Lashes #7, since launch, has been ranked as one of the best selling and highly reviewed lash products.In 2018, all launched products that together bring in at least $200 million in annual revenue. Time described this as “an internet based beauty brand age”, as internet-to-business beauty products have taken over a large percentage from the traditional beauty market. They own a significantly growing share of the whole market.


=== Price ===
The price of Huda Beauty's products has been widely seen as reasonable compared to similar products that are sharing similar markets. Though, among blog-to-brand beauty brands, that created by YouTubers or Instagram bloggers  the price of Huda Beauty's products are relatively high. Bloggers mostly launch beauty products that they set a price a bit lower than the regular market price, as their brand names and quality usually have not been tested through time.However, Huda Beauty's foundation sells at 65 Australian dollars in Australian Sephora stores, while Fenty beauty by Rihanna offers a similar market-targeted foundation by 50 dollars. Also, the first-line beauty company, Estee Lauder, sells its well-reviewed foundation “Double Wear” under 60 dollars at a department store, like Westfield and David Jones in Australia.
However, Huda Beauty's most well known product: fake lashes “#7 Samantha” still achieve success in the sale, although its price is 35 dollar in Australia. While SHU UEMURA, currently the first name in eye-related beauty section offers fake lashes for around 25 Australian dollars in department stores.


=== Review ===
The most welcomed products of the brand, according to its sale volume and net income is its fake lashes, which labeled as “#7”, and a collection of foundations. The foundation has widely commented as sufficient choice for a wide range of different skin tones.In the meanwhile, the eye shadow palette and fake eyelashes are the mostly positively reviewed products. While the face palettes and liquid lipstick or other products have seen a draw in the market share.


== Controversies and criticism ==


=== Genital lightening ===
Huda Beauty as an internet based tutorial and brand has sparked major controversy on some topics. The brand faced intense disagreement over a blog post advising women about ways to lighten the color of their genitals. Originally published on April 7, 2018, the page, named ”Why Your Vagina Gets Dark And How To Lighten It”, noted some tips from "trusted expert" and board-certified dermatologist Doris Day, MD. This post has been criticized as unprofessional and misleading.


=== Allegations of copying ===


==== Foundation vs Fenty Beauty's ====
In 2017, Huda Beauty announced that it would soon be debuting a foundation collection with a more diverse range of shades. Just after this, this collection been criticized by Fenty Beauty followers that “it copies Fenty Beauty’s "Pro Filt'r" foundation collection image”. Huda Beauty's "#FauxFilter" foundations have a selection of 30 shades, while Rihanna's brand - Fenty Beauty "Pro Filt'r" foundation collection has 40 shades. Others, however, applauded Huda Beauty for being one of the first brands to release an inclusive range of shades in Sephora stores globally.


=== Acne scar controversy ===
In 2015, a Youtuber from the UK, Em Ford, posted a video named “You Look Disgusting”. The video talks about the surrounding negative comments to her, for humanizing people behind the screens to end any kind of language bully. So far, as in October 2018, the video has been viewed more than 28 million times. There was a post from Huda that been blamed for shaming other peer Youtubers’ acne scars. The owner, Kattan, posted a line said the only thing worse than a breakout is the acne scar. Nevertheless, Huda used Ford's positive post to show “how ugly” the acne scars could be. After seeing Huda's post, Em Ford made a new post questioning if Huda as a popular brand wants to be one of the bully problems or one of the solutions. This ended with Kattan's replies to the posts and apology online.


== Marketing ==
The beauty industry is estimated to be worth $445 billion. A key driver to its rapid growth are influencers on visually focused social platforms like YouTube, Instagram and Pinterest who build blog-to-brand businesses such as Fenty Beauty. According to Forbes, the barrier to entry for cosmetic businesses has fallen, as entrepreneurs with a strong social media following can hire laboratories to test formulae on a contract basis and can sell their products through stores such as Sephora and Ulta. As of 2018, Blog to beauty brands take a relatively high portion of the beauty market.According to the instagram scheduling tool HopperHQ, which publishes an 'influencer rich list' estimating the value and income of internet influencers, Kattan could possibly earn up to AED 66,000 per sponsored post as the owner of Huda Beauty. However, Kattan does not do sponsored content on her channel. Kattan was named as one of Time Magazine's 25 most influential people online, alongside J.K Rowling and Kim Kardashian West.Huda Beauty is among the best-selling cosmetics brands in Sephora in the Middle East and Harrods in London. According to The Business of Fashion, Kattan's background as an Iraqi immigrant in America distinguishes her from other beauty influencers. She studied finance in the United States, and pursued a career as a makeup artist in Dubai.


== Fragrances ==
In 2018, Huda Beauty stated that they were entering the skincare and fragrance markets and announced a new collection of perfume.


== Personal ==
In 2020, after the murder of George Floyd, Huda Beauty donated $500,000 US Dollars to the NAACP.  


== References ==


== External links ==
Official Website