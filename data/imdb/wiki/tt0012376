Last Card is a shedding-type card game popular in New Zealand and Australia. It is similar in most aspects to Uno, Mau Mau or Crazy Eights but several rules differentiate it, for instance the function of a particular card.


== Objective ==
The first player to play their last card wins the game.


== Starting rules ==
Deal seven cards to each player. When the cards have been dealt to each player, the top card on the deck is flipped over to commence play. This card only dictates the starting suit and nothing else. 
If the first player is unable to follow suit they must pick up one card as normal.
Jokers are not used.


== Pick up rules ==
When a 5 has been played, the following player must draw five cards or play another 5 (stacking), forcing the next player to do the same or be forced to pick up ten (the original 5 plus the following 5) cards. (With there only being four 5 cards in a standard deck, the maximum draw is 20 cards.) This rule is still present if a player has used their last card, but still needs to pick up. Making someone pick up does not skip their turn.When a 2 has been played, the following player must draw two cards or play another 2 (stacking); the subsequent player is then required to play another 2 card or draw four (the original 2 plus the following 2) from the deck. (With there being four 2 cards in a standard pack the maximum draw required is 8 cards.) This rule is still present if a player has used their last card, but still needs to pick up.
When a 10 card has been played, the following player misses a turn.An Ace can be played regardless of the suit or value of the topmost card on the playing deck—that is, the Ace may be played at any time in the game. When playing an Ace, the player can decide freely the suit that has to be played next; from then on, play continues as normal, but on the suit selected by the player of the Ace. 
When a player has only one card remaining in their hand they must say 'Last Card'. Failure to do so before playing requires them to draw two cards from the deck and continue playing.


== Variations ==
There are a number of common variations of the game played in New Zealand.


=== Blocking ===
If a player has been made to pick up by a 2 or a 5 card being played (or a stack of these), then they may avoid having to pick up any cards by playing either a 3 or a Jack of the same suit (whether a 3 or a Jack is the card to play should be decided before the games starts). Play resumes at the person following the person who put down the first 2 or 5 card as per normal round.


=== Nomination ===
When a 2 or a 5 card is played the player may choose who has to pick up the cards. That player may in turn put down a 2 or a 5 and then choose another person to pick up instead (stacking applies). If they do stack then they can choose the person who put down the original 2 or 5. They may also block the pick up with a 3 or Jack.
When a 10 card is played the player may choose who will miss a turn. The 10 card does not stack, the player nominated to miss a turn can not play a 10 card and redirect the turn miss, however they may play a blocking card (3 or Jack) to block the missed turn. Play resumes after the person who played the 10.


=== Matching face values ===
A player may play a card of the same face value as the one on top of the playing deck to change the current suit. For example the Queen of Clubs may be played on the Queen of Diamonds and the suit will change to Clubs. 
Multiple cards of the same face value may be played at once as a stack and the top played suit becomes the current suit.
This also means that multiple pick up cards (2 or 5) of the same value can be played, and the player who has to pick up will have to pick up the sum of the stack. For example playing three 2's means the person picking up has to pick up 6 cards. That player may immediately play a card of the same value (stacking) and the stacked pick up is passed to either the following player or any other nominated player if nomination rules are in force. The person who needs to pick up can block as above if that rule is in play.
Multiple 10 cards can be played at once by the same player and a single person chosen to miss as many turns as there are cards in the stack. As above the person who will miss the turn(s) can not play a 10 in response, however they may play a 3 of the top-most suit to block the turn misses.


== See also ==
One-card


== References ==
OBT Card Games pages 17-19, 127 (glossary)