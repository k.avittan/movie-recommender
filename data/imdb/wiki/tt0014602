While Paris Sleeps, aka The Glory of Love, is a 1923 American silent drama film based on the novel The Glory of Love by Leslie Beresford (a.k.a. Pan), directed by Maurice Tourneur, and starring Lon Chaney and John Gilbert. The film is believed to be lost.


== Plot ==
Henri Santados (Lon Chaney) is an insane sculptor living in Paris and working in a wax museum owned by Father Marionette. He is in love with his model, Bebe Larvache, who cares nothing for him. Bebe meets a wealthy American, Dennis O'Keefe (John Gilbert) and they fall in love. A jealous Santados teams up with Father Marionette and they plot to get rid of O'Keefe. Meanwhile, O'Keefe's father, who strenuously objects to the relationship, convinces Bebe to leave his son alone, for his own good. She asks that she may spend one final night with Dennis at the Mardi Gras Festival. O'Keefe goes to pick Larvache up at the studio but Santados maneuvers her into a compromising position to make O'Keefe think that she is cheating on him. As the heartbroken O'Keefe leaves, he is captured by Marionette, who tortures him in the wax museum. Marionette calls Santados, and Bebe hears O'Keefe's voice over the phone. A friend of O'Keefe's, Georges Morier, manages to rescue him in time and rush him to the hospital where O'Keefe's father finally consents to the marriage.


== Cast ==
Lon Chaney as Santados, the mad sculptor
Jack F. McDonald as Father Marionette
Mildred Manning as Bebe Larvache
John Gilbert as Dennis O'Keefe
Hardee Kirkland as Dennis's father
J. Farrell MacDonald as George Morier


== Production ==
Tourneur shot the film in 1920 under the title The Glory of Love, but due to its gruesome nature, he was not able to get it released for three years. In 1923, the W. W. Hodkinson Corporation distributed the film theatrically, after changing the title to While Paris Sleeps. They probably wanted to distribute it since the star of the film, Lon Chaney, had achieved great notoriety in Hollywood during the preceding three years, starring in films such as A Blind Bargain, Outside the Law and The Penalty. Critics commented that the film "has all the appearances of a picture that might have been made three or four years ago", and they were right.Lon Chaney reunited with John Gilbert in 1924 in He Who Gets Slapped, and director Tod Browning later cast John Gilbert in his 1927 film The Show. Gilbert later became engaged to Greta Garbo, and drifted off into a string of big budget Hollywood romantic drama films before dying in 1936 from heart failure.


== References ==


== External links ==
While Paris Sleeps on IMDb
While Paris Sleeps at the TCM Movie Database