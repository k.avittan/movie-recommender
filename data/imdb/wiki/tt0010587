The Prince and Betty is a novel by P. G. Wodehouse. It was originally published in Ainslee's Magazine in the United States in January 1912, and, in a slightly different form, as a serial in Strand Magazine in the United Kingdom between February and April 1912. It was published in book form, in the United Kingdom by Mills & Boon on 1 May 1912. A substantially different version, which incorporated the plot of Psmith, Journalist, was published in the US by W.J. Watt & Company, New York on 14 February 1912.


== Plot introduction ==
The story tells of how unscrupulous millionaire Benjamin Scobell decides to build a casino on the small Mediterranean island of Mervo, dragging in the unwitting heir to the throne to help. Little does he know that his stepdaughter Betty has history with the young man John Maude, and his schemes lead to a rift between the newly reunited pair.


== Characters ==
Benjamin Scobell, a wealthy and unscrupulous financier
Betty Silver, Scobell's attractive stepdaughter
John Maude, unwitting heir to a princedom, long admired by Betty


== US novel version ==
The US novel version of The Prince and Betty combines the original story, transferred to a New York setting, with the plot of Psmith, Journalist, substantially rewritten to merge in the romance of John Maude (who becomes an American in this version) and Betty.


== Adaptations ==


=== Film ===
A silent, black-and-white film adaptation, also titled The Prince and Betty, was made in 1919.


=== Radio ===
A musical comedy adaptation of The Prince and Betty under the title Meet the Prince was first broadcast by the BBC in January 1934. The story was adapted and produced by John Watt and the music was by Kenneth Leslie-Smith, with lyrics by Henrik Ege and orchestral arrangements by Sydney Baynes. The cast included Esmond Knight as John Maude, Polly Walker as Della Morrison, Adele Dixon as Betty Silver, Ewart Scott as Lord Arthur Hayling, Sydney Keith as Mr Morrison, Bernard Ansell as Edwin Crump, Davy Burnaby as Benjamin Scobel, Floy Penrhyn as Marian Scobel, and C. Denier Warren as General Poineau. 
It was broadcast in the BBC Regional Programme on 1 January 1934 and repeated the next day in the BBC National Programme. A second production was broadcast on 28 September 1936 in the National Programme and repeated the following day in the Regional Programme.


== References ==


== External links ==
The Russian Wodehouse Society's page, with photos of book covers and a list of characters
The Prince and Betty (1919) on IMDb
The Prince and Betty (1919) at the AFI silent movie catalog
American edition at Project Gutenberg
 The Prince and Betty public domain audiobook at LibriVox