When the Clouds Roll By is a 1919 American comedy film starring Douglas Fairbanks and directed by Victor Fleming and Theodore Reed. After decades of not being seen by the public, the film was finally released in 2010 on DVD by Alpha Video with an original score by Don Kinnier.


== Plot ==
As described in a film magazine, Daniel Boone Brown (Douglas Fairbanks), a superstitious but ambitious young New Yorker, is the victim of demented psychiatrist Dr. Ulrich Metz (Herbert Grimwood) who, with the aid of numberless associates serving him in the interests of science, arranges circumstances intended to drive Daniel to suicide. In the midst of a series of bewildering misfortunes apparently emanating from broken mirrors, black cats, and similar sources, Daniel meets Greenwich Village artist Lucette Bancroft (Kathleen Clifford), and mutual love results. A Westerner who owns land in partnership with Lucette's uncle comes to the city and plot's with Daniel's uncle Curtis (Ralph Lewis) to defraud his partner. Daniel, after being driven to the verge of suicide by the scientist and his aides, is saved when it is discovered that Dr. Metz is insane. Daniel then follows the Westerner, who has convinced Lucette to return to the west with him, when a flood engulfs the train they are riding on. Daniel brings about a happy resolution.


== Cast ==
Douglas Fairbanks as Daniel Boone Brown
Kathleen Clifford as Lucette Bancroft
Frank Campeau as Mark Drake
Ralph Lewis as Curtis Brown
Daisy Jefferson as Bobby De Vere
Bull Montana as The Nightmare
Herbert Grimwood as Dr. Ulrich Metz
Albert MacQuarrie as Hobson
Victor Fleming as Himself
Thomas J. Geraghty as Himself (credited as T. J. G.)
William C. McGann as Himself
Harris Thorpe as Himself
Babe London as Switchboard Operator (uncredited)


== Reception ==
Fairbanks biographer Jeffrey Vance considers When the Cloud Rolls By to be the "best of all the contemporary Fairbanks comedies." "Executed at a breathless pace, When the Clouds Roll By is a masterful showpiece for the whirling cyclone of energy that was Douglas Fairbanks."  Vance's highest praise is for the elaborate dream sequence, which he deems "a virtual encapsulation of every gymnastic feat in the Fairbanks repertoire" and notes that Fairbanks's walk on the ceiling of his home anticipates the celebrated "dancing on the ceiling" sequence in Stanley Donen's Royal Wedding (1951). Vance also notes that the film's flood sequence conclusion presages a similar ending in Buster Keaton's classic Steamboat Bill, Jr. (1928).The film is recognized by American Film Institute in these lists:

2002: AFI's 100 Years...100 Passions – Nominated


== References ==


== External links ==
When the Clouds Roll By on IMDb
When the Clouds Roll By synopsis at AllMovie
Stills at silenthollywood.com
Stills and commentary at JonathanRosenbaum.net