The Easiest Way is a 1931 American pre-Code MGM drama film directed by Jack Conway. Adapted from the 1909 play of the same name written by Eugene Walter and directed by David Belasco, the film stars Constance Bennett, Adolphe Menjou, Robert Montgomery, Marjorie Rambeau, Anita Page, and Clark Gable


== Plot ==
Growing up in a poor working-class family, Laura (Constance Bennett) works hard to support her family. Laura's father, Ben (J. Farrell MacDonald) encourages his other daughter Peg (Anita Page) to marry a hard-working man named Nick (Clark Gable). Laura rejects a marriage proposal from the boy-next-door to become involved with William Brockton (Adolphe Menjou) a wealthy man many years her senior whom she met at a modeling job. She allows him to shower her with expensive gifts and moves into his luxury apartment.
Her newly found wealth does not come without any backlash, though. Her mother Agnes (Clara Blandick), notices a difference in Laura and that she is working more nights.  Dressing in wealthy attire, and arriving in a chauffeur driven car, she pays a visit to Peg, (now married to Nick).  Nick noticing her style, demands that she leaves his house immediately, as he wants no association with a kept woman.  Even though Laura realizes that she has become estranged from her family, she continues to stay with Brockton.
Sometime later, while vacationing in Colorado, she meets and falls in love with young newsman Jack Madison (Robert Montgomery). After a brief affair, and pledging their fidelity to one another, Jack is stationed in Argentina for several months, as Laura promises him that she will leave Brockton. She breaks the news to Brockton, returns all of his gifts, leaves his apartment, and takes a job at Macy's department store.
Laura, finds work at Macy's but is so financially strapped, she can't pay her rent.  She unsuccessfully asks one of her former colleagues Elfie St. Clair (Marjorie Rambeau) for a loan. Laura can't return to her room unless her rent is paid.  She takes "The Easiest Way", and calls Brockton, asking for a loan.  Brockton refuses and tells her he will only cooperate if she comes back to him on condition that she inform Jack of her decision.  She promises Brockton that she will, but deceives him by not telling Jack.  Jack returns to New York, phones Laura immediately and Laura invites him to her swank apartment.
Meanwhile, Elfie pops in on Laura to ask for money.  Desperate, Laura listens to Elfie, who advises her to leave Brockton and marry Jack, but under no circumstances tell Jack of her current set up.  Laura agrees.  But her plans to elope with Jack are cut short when Brockton unexpectedly shows up. Brockton, noticing Laura's packed bags, informs Jack of what happened during his absence. Laura tries to explain the situation, but Jack, furious that Laura had broken her promise of fidelity to him, leaves. Despite Brockton's offer to continue to care for her, Laura, leaves heart broken.   Traveling to her sister's home, her brother-in-law (Nick) invites her in. Nick, seeing that Laura had returned to her beginnings, comforts her with the promise that Madison will return when he gets "cool under the collar."


== Cast ==
Constance Bennett as Laura Murdock, a.k.a. "Lolly" by her immediate family
Adolphe Menjou as William Brockton
Robert Montgomery as Jack Madison
Anita Page as Peg Murdock Feliki
Marjorie Rambeau as Elfie St. Clair
J. Farrell MacDonald as Ben Murdock
Clara Blandick as Agnes Murdock
Clark Gable as Nick Feliki


=== Uncredited ===
Lynton Brent as Brockton Associate (uncredited)
Jack Hanlon as Andy Murdock (uncredited)
John Harron as  Chris Swoboda, Laura's Suitor (uncredited)
Dell Henderson as Bud Williams (uncredited)
Hedda Hopper as Mrs. Clara Williams (uncredited)
Charles Judels as Mr. Gensler (uncredited)


== Production ==
In the scene where Jack and Laura are horseback riding in the (ostensibly) Colorado mountains, he has them dismount so he can show her his "pet view"; actually, the famous eastward looking view of El Capitan and Half Dome in Yosemite National Park.
The film is based on a 1909 David Belasco produced Broadway hit play that starred Frances Starr. In 1917 Select/Selznick Pictures produced the first film version, a silent, which starred Clara Kimball Young and Rockliffe Fellowes. First National planned a film adaptation in 1927. Henry King was assigned as the director, whereas Belle Bennett and Conrad Veidt were set to star. David Fink, who produced the project, received many objections to the adaptation due to the play's immoral nature. Even though he dismissed this by reminding officials that "a film like Sadie Thompson" could be released, he later abandoned the project, discussably  [sic] because of censorship problems with the Hays Office.After Joseph M. Schenck & David O. Selznick and Universal Pictures briefly considered to work on an adaptation, one of the DeMilles picked up the project in March 1928, until a conversation with Will H. Hays motivated him to drop it. In 1929, a producer named Sam E. Rork working for Fox considered making the film, but he was warned not to "undertake a thing which other responsible companies have already decided would not be good for the industry." Pathé purchased the story in 1930, but it was resold by Pathe following another warning by Hays Office. Columbia Pictures was offered by Hays Office to adapt the film on condition that they retitled it and that they bring the story into conformity with the Production Code shortly before MGM bought the rights, but they rejected it. In November 1930, Irving Thalberg was set to produce, and he was contacted by the Hays Office, who complained that "the trouble with the adaptation is that it builds up audience sympathy for Laura Murdock and supplies her with the means of securing sympathetic excuses for, if not actual approval of, her weakness of character." The Hays Office labeled it "much more dangerous than the original play, which for a long time has itself been considered dangerous motion picture material" and commented that the story did not go "far enough in building up the idea that Laura is being punished."


== Clark Gable's role in the film ==
One other notable aspect of The Easiest Way is that Clark Gable's role as the affable, hard-working laundryman Nick was only the future "King of Hollywood's" second credited acting performance in a "talkie."  If this film had premiered just three weeks earlier, it would now be cited as Gable's acting debut in the sound era.  That distinction instead goes to the Pathé Western The Painted Desert, which was released 20 days before The Easiest Way, on January 18, 1931.  Gable's supporting roles in these two early talkies demonstrated to audiences and to studio executives his capability of performing a range of characters.  In The Easiest Way, Gable's good-natured character runs completely counter to the dark personality he portrays as Rance Brett, the rugged, villainous cowboy in The Painted Desert.  Yet, it was his "fan-favorite performance" as the outlaw Rance that made the greater impression on studio executives and subsequently earned Gable a contract with MGM.


== Foreign-language version ==
One foreign-language version was produced by MGM in French. It was titled Quand on est belle and starred Lili Damita in the part that Constance Bennett played in the English-language version.


== Reception ==
Ultimately, the film, like many others in the 1930s, was subject to censorship at the hands of the Production Code Administration. Several alternate endings were created for this film. Following its release, Columbia sent a complaint letter in which it accused the Hays Office of "unfairly preventing the studio from making the film, while allowing M-G-M to produce it." Meanwhile, censorship boards in Ireland, Nova Scotia, and Alberta objected to the film and prevented its release.


=== Box Office ===
According to MGM records the film earned $654,000 in the United States and Canada and $249,000 elsewhere resulting in a profit of $193,000.


== Home video release ==
Warner Archive Collection released the first Region 1 DVD on March 10, 2010.


== References ==


== External links ==
The Easiest Way on IMDb
The Easiest Way at AllMovie
The Easiest Way at the TCM Movie Database