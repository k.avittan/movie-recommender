Sins of the Parents  is a 1914 silent film written and directed by Ivan Abramson, and starring Sara Adler, a prominent Yiddish theatre actress in her first of two movie appearances.


== Plot ==
As was typical of Abramson's potboilers, Sins of the Parents involves complicated and often contrived plot twists arising out of family relations of the primary characters.
Laura Henderson (Adler) is an orphan, raised by her aunt Mary Sherman.  Sherman runs a boarding house, and boarder Angelo Angelini (a musician) is the apple of Laura's eye.  They are engaged to be married, but Angelo claims he must leave for a concert tour—but in truth he returns to his wife and child in Italy, crushing Laura.  Laura gives birth an illegitimate child, Ruth, but is forced to abandon her and moves to New York, where she falls under the care of Reverend Henry Bradley.  Bradley and Laura later marry, and Laura keeps the existence of Ruth a secret from Bradley.
Shifting 19 years later, Bradley (now a prison chaplain) and Laura now have a daughter (Aline) who is about to become engaged to the well-to-do Walter Jordan. Meanwhile, Ruth, now 20 years old, who had believed Mary was her mother, finds out that her real mother is Laura and departs for New York to find her. Meanwhile, Angelo is now living in New York under the name Angell with his son Tony, a general ne'er do well. Tony befriends Ruth (of course not knowing she is his half-sister) under false pretenses, and plans to sell her into white slavery in New Orleans. Ruth tries to escape him, and in the ensuing struggle Tony is shot dead just as Angelo enters the room. Ruth is arrested, and meets Chaplain Bradley in prison, where she divulges her story. Mary Sherman visits Ruth in prison, and then tells all to Laura. Laura breaks down when she learns that her first child is in prison, just while she had been celebrating Aline's engagement to Walter. This causes Aline, apparently out of learning of her mother's disgrace, to break off her engagement and commit suicide.
Moving to Ruth's murder trial, Angelo is about to testify as a witness to his son's murder just as Laura bursts into the courtroom and recognizes Angelo.  Learning that his own daughter killed his son, Angelo despairs and refuses to speak.  Laura is removed from the courtroom in hysterics.  The jury subsequently finds Ruth is not guilty by reason of self-defense and she is freed.  Bradley brings Ruth to their home, but Laura feels unworthy of his love and plans to leave with Ruth.  Bradley resigns as chaplain and insists that Laura stay.  Bradley's employer refuses his resignation due to his noble acts, and Laura begs forgiveness.


== Cast ==
Sara Adler as Laura Henderson
Paul Doucet
John W. Dillon
Ralf Henderson
Mabel Wright
Louise Corbin


== Background and reception ==
The Sins of the Parents was the first film produced by Ivan Abramson, for his newly created Ivan Film Productions.  The storyline is based on a Yiddish play titled God's Punishment (Gots shtrof), by Zalmon Libin.  Lead actress Sara Adler was a star of the Yiddish theatre and the wife of Jacob Adler, who Abramson had previously managed.  The film is one of only two movies in which Sara Adler appeared.The film reportedly opened to large crowds at the Grand Theatre in New York City, with Adler in attendance on opening night.


== References ==


== External links ==
Sins of the Parents on IMDb
Sins of the Parents at American Film Institute