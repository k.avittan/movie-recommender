The Outrage is a 1964 American Western film directed by Martin Ritt and starring Paul Newman, Laurence Harvey, Claire Bloom and Edward G. Robinson and William Shatner.


== Plot ==
Three disparate travelers, a disillusioned preacher (William Shatner), an unsuccessful prospector (Howard Da Silva), and a larcenous, cynical con man (Edward G. Robinson), meet at a decrepit railroad station in the 1870s Southwest United States. The prospector and the preacher were witnesses at the rape and murder trial of the notorious bandit Juan Carrasco (Paul Newman). The bandit duped an aristocratic Southerner, Colonel Wakefield (Laurence Harvey), into believing he knew the location of a lost Aztec treasure. The greedy "gentleman" allowed himself to be tied up while Carasco assaulted his wife Nina (Claire Bloom). These events lead to the stabbing of the husband and Carrasco was tried, convicted, and condemned for the crimes.
Everyone's account on the witness stand differed dramatically. Carrasco claimed that Wakefield was tied up with ropes while Nina was assaulted, after which he killed the colonel in a duel. The newlywed wife contends that she was the one who killed her husband because he accused her of leading on Carrasco and causing the rape. The dead man "testifies" through a third witness, an old Indian shaman (Paul Fix), who said that neither of those accounts was true. The shaman insists that the colonel used a jeweled dagger to commit suicide after the incident.
However, there was a fourth witness, the prospector, one with a completely new view of what actually took place. But can his version be trusted?


== Cast ==
Paul Newman as Juan Carrasco
Laurence Harvey as Husband
Claire Bloom as Wife
Edward G. Robinson as Con Man
William Shatner as The Preacher
Howard Da Silva as Prospector
Albert Salmi as Sheriff
Thomas Chalmers as Judge
Paul Fix as Indian


== Home media ==
The Outrage was released to DVD by Warner Home Video on February 17, 2009 in a Region 1 widescreen DVD.


== See also ==
List of American films of 1964


== References ==


== External links ==
The Outrage on IMDb
The Outrage at Rotten Tomatoes
The Outrage at AllMovie
The Outrage at the TCM Movie Database