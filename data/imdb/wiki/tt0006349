The Fenn Treasure was a cache of gold and jewels that Forrest Fenn, an art dealer and author from Santa Fe, New Mexico, hid in the Rocky Mountains of the United States. It was found approximately a decade later in 2020, in Wyoming.


== History ==
Forrest Fenn (August 22, 1930 - September 7, 2020) was a pilot in the United States Air Force, obtaining the rank of Major and awarded the Silver Star for his service in the Vietnam War where he flew 328 combat missions in 365 days. He retired from the Air Force and ran the Arrowsmith-Fenn Gallery with his partner Rex Arrowsmith, which became the Fenn Galleries which he operated with his wife, Peggy. The gallery was located in Santa Fe, New Mexico, and sold a variety of Native American artifacts, paintings, bronze sculptures, and other art, including forged copies of works by Modigliani, Monet, Degas, and other artists. The gallery reportedly grossed $6 million a year.In 1988, Fenn was diagnosed with cancer and given a prognosis that it was likely terminal. This inspired him to hide a treasure chest in an outdoor location with the purpose of creating a public search for it. He considered using the location as his final resting place as well. He recovered from the illness and in 2010 self-published The Thrill of the Chase: A Memoir, a collection of short stories from his life. He described a treasure chest that he said contains gold nuggets, rare coins, jewelry, and gemstones. He went on to write that he hid the chest "in the mountains somewhere north of Santa Fe". Fenn said that the stories in the book contain hints to the chest's location as well as the poem found in the chapter "Gold and More" that contains nine clues that will lead a searcher to the chest.  Fenn's book and story prompted a treasure hunt in the Rocky Mountains of New Mexico, Colorado, Wyoming, and Montana. Its value has been estimated as high as $2 million, depending on the appraisal of the items. Fenn claimed to make no money on the sale of the self-published books out of concern for being labeled a fraud by critics.Before the treasure hunt, Fenn came into conflict with authorities over federal antiquities law. Federal Bureau of Investigation (FBI) agents raided his home in 2009 as part of an investigation into artifact looting in the Four Corners area. Items in his possession reportedly included pieces of chain mail from the Pecos National Historical Park, human hair, a feathered talisman, and a bison skull, some of which were confiscated by federal authorities; no charges were filed. Two people targeted in the case committed suicide, and Fenn blamed the FBI for their deaths.Fenn died on September 7, 2020 at the age of 90.


== Deaths ==
Five people have died while searching for the treasure. In 2017, the chief of the New Mexico State Police, Pete Kassetas, publicly implored Fenn to end the treasure hunt, stating "He's putting lives at risk."
Randy Bilyeu went missing in January 2016 and was found dead in July. His body was discovered by workers along the Rio Grande, and an autopsy could not determine cause of death. Bilyeu's ex-wife publicly stated her belief that the Fenn Treasure was a hoax.
Jeff Murphy (age 53) of Batavia, Illinois, was found dead in Yellowstone National Park on June 9, 2017, after falling about 500 feet (150 m) down a steep slope. Yellowstone officials did not provide details to the public concerning their investigation, but KULR-TV filed a Freedom of Information Act request. The television station reports that Murphy's wife told park authorities that he was looking for the treasure when she first reported him missing.
Pastor Paris Wallace of Grand Junction, Colorado, told family members that he was searching for a buried treasure, but he failed to show up for a planned family meeting on June 14, 2017. His car was found parked near the Taos Junction Bridge and his body was found 5 to 7 miles (8.0 to 11.3 km) downstream along the Rio Grande.
Eric Ashby (age 31) was found dead in Colorado's Arkansas River on July 28, 2017. Friends and family stated that he had moved to Colorado in 2016 to look for the treasure, and was last seen on June 28 rafting on the river 10 to 15 miles (16 to 24 km) upstream from where his body was found. The raft overturned and Ashby had been missing since that time.
Michael Wayne Sexson (age 53) of Deer Trail, Colorado, was found dead by rescuers on March 21, 2020, alongside his unnamed 65-year-old male companion, who later recovered in hospital. Authorities were notified by the person who rented a pair of snowmobiles to the men. The pair were discovered within 5 miles (8.0 km) of a site they had been rescued from a month earlier, near the Dinosaur National Monument along the Utah-Colorado border.


== Controversy ==
There have been a number of notable controversies surrounding the treasure hunt. A few searchers have been cited or arrested for legal infractions.
An unidentified man searching for the treasure was arrested in New Mexico in 2013 and charged with damaging a cultural artifact for digging beneath an iron cross of a descanso near the Pecos River.
In April 2014, national park rangers detained Darrel Seller and Christy Strawn for having a metal detector and digging in Yellowstone National Park while searching for the Fenn treasure. On May 9 park rangers accused the couple of camping without a permit and starting a small fire.
Scott Conway was cited by New Mexico State Parks officers after he dug a large hole on state land near Heron Lake while looking for the Fenn treasure.
A Pennsylvania man, Robert Miller, was arrested for burglary, breaking and entering, and criminal damage to property in October 2018. Miller broke into Fenn's property and was hauling away a Spanish-style chest he thought was the treasure and was caught in the act and held at gunpoint until law enforcement arrived.
In December 2019, David Harold Hanson of Colorado Springs, Colorado, filed a lawsuit in U.S. District Court against Forrest Fenn. The lawsuit alleges Fenn made several fraudulent statements and deceived searchers.  Although Hanson attempted to reopen it, the case was closed.
In January 2020, David Christensen of Indiana had to be rescued by Yellowstone National Park rangers after he attempted to rappel over 850 feet (260 m) from a rope tied to a railing into the Grand Canyon of the Yellowstone. He was ordered to spend a week in jail and pay rescue costs of just over $4000.  He received a 5-year ban from the park, and disregarding Fenn's remarks that no climbing was required, remained convinced at his sentencing his solution was correct.


== Treasure chest ==
The treasure chest was said to be a bronze box estimated to have been forged in the 12th century. The chest features a bronze construction with a wood liner and locking front clasp. According to Fenn, it weighs about 22 pounds (10.0 kg) and its dimensions are 10 by 10 by 5 inches (250 mm × 250 mm × 130 mm). The chest features scenes and reliefs with knights scaling walls on ladders and maidens above throwing flowers down upon them. This style of work appears to be references to the Le Roman de la Rose poem about the pursuit of love and scaling the "Castle of Love" which gained popularity around the same time the chest was made. Because of the popularity of the treasure hunt, artists have made modern recreations based on Fenn's chest.


== Discovery ==
On June 6, 2020, Fenn posted on the searcher blog Thrill of the Chase that the treasure had been found. "It was under a canopy of stars in the lush, forested vegetation of the Rocky Mountains and had not moved from the spot where I hid it more than 10 years ago. I do not know the person who found it, but the poem in my book led him to the precise spot. I congratulate the thousands of people who participated in the search and hope they will continue to be drawn by the promise of other discoveries. So the search is over. Look for more information and photos in the coming days."This was subsequently confirmed via email by Fenn, who further disclosed in a news article that the finder was a male from the eastern United States who had sent him a photograph. The identity of the finder, the photograph, and the location of the treasure were not revealed.  On June 16, Fenn released additional photos on the Thrill of the Chase blog site including of himself examining the contents of the chest and one of it sitting in weathered condition implicitly on or near the site where it was found.  On July 22 Fenn stated on the Thrill of the Chase blog site that the treasure's finder had authorized him to disclose, in the interests of closure for many of its searchers, that it had been hidden in Wyoming.


== In popular culture ==
The Fenn treasure hunt has been featured in television shows, magazine articles, and books.

Forrest Fenn's idea to hide a treasure chest is credited as the inspiration for Douglas Preston's 2003 novel The Codex.
The treasure hunt was featured in a 2015 episode of Expedition Unknown, "Finding Fenn's Fortune". Series host Josh Gates interviewed Forrest Fenn before joining several groups of treasure hunters as they searched multiple states in the Rocky Mountains.
The Lure (2017) is a documentary feature film about the treasure directed by Tomas Leach.
A 2018 episode of the web series Buzzfeed Unsolved: True Crime, "The Treacherous Treasure Hunt of Forrest Fenn", features the treasure hunt.
On Wednesday, July 8, in an episode of Discovery Channel's Expedition Unknown: Uncovered taped prior to the treasure being found, Josh Gates tries to decode the clues of the poem.


== Books ==
Fenn, Forrest (2010). The Thrill of the Chase. One Horse Land & Cattle Co. ISBN 9780967091785.
Fenn, Forrest (2013). Too Far to Walk. One Horse Land & Cattle Co. ISBN 9780967091792.
Fenn, Forrest (2017). Once Upon a While. Phat Page Design. ISBN 9780692950555.
Fenn, Forrest (2018). Once Upon a While Revised. One Horse Land & Cattle Co. ISBN 9780692196281.
Ritt, Jordan (2015). A Treasure More Than Gold: How I found the solution to Forrest Fenn's poem. ISBN 9781478753742.


== References ==


== External links ==
Old Santa Fe Trading Company, Forrest Fenn's web site and blog, which hosts a self-published "Thrill of the Chase Resource Page".