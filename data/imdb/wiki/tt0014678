A scandal can be broadly defined as the strong social reactions of outrage, anger, or surprise, when accusations or rumours circulate or appear for some reason, regarding a person or persons who are perceived to have transgressed in some way. These reactions are usually noisy and may be conflicting, and they often have negative effects on the status and credibility of the person(s) or organisation involved. Society is scandalised when it becomes aware of breaches of moral norms or legal requirements, often when these have remained undiscovered or been concealed for some time. Such breaches have typically erupted from greed, lust or the abuse of power. Scandals may be regarded as political, sexual, moral, literary or artistic but often spread from one realm into another. The basis of a scandal may be factual or false, or a combination of both. In contemporary times, exposure of a scandalous situation is often made by mass media. 
Contemporary media has the capacity to spread knowledge of a scandal further than in previous centuries and public interest has encouraged many cases of confected scandals relating to well-known people as well as genuine scandals relating to politics and business. Some scandals are revealed by whistleblowers who discover wrongdoing within organizations or groups, such as Deep Throat (William Mark Felt) during the Watergate scandal in the 1970s in the United States. Whistleblowers may be protected by laws which are used to obtain information of misdeeds and acts detrimental to their establishments. However, the possibility of scandal has always created a tension between society's efforts to reveal wrong doing and its desire to cover them up ... and the act of covering up (or indeed of revealing) a contentious situation may become a scandal in itself. 


== Academic and literary ==

Academic dishonesty, also referred to as academic misconduct, is any type of cheating that occurs in relation to a formal academic exercise.
Although in the early part of the 19th century held the view that scandal does not mix with literature and science, some opined that a scattering of some amount of scandal in literature could enhance interest of people as scandal suits "the taste of almost every palate." Scandal, has however, been the subject of many books. Among the most famous of fictional stories about scandal are School for Scandal (1777) by Richard Brinsley Sheridan and The Scarlet Letter (1850) by Nathaniel Hawthorne.
Literary scandals result from some kind of fraud; either the authors are not who they say they are, or the facts have been misrepresented or they contain some defamation of another person. For example, two books by Holocaust survivors, Angel at the Fence by Herman Rosenblat and A Memoir of the Holocaust Years Misha Defonseca, were found to be based on false information, while a prize won by novelist Helen Darville created a scandal in 1994 around the author's fraudulently claimed ancestry.


== Political ==

A political scandal occurs when political corruption or other misbehavior is exposed. Politicians or government officials are accused of engaging in illegal, corrupt, or unethical practices. A political scandal can involve the breaking of the nation's laws or moral codes and may involve other types of scandal.


== Business ==

In 2012, Michael Woodford who successfully steered Olympus, a Japanese company to fame, turned a whistleblower when even as a CEO of the firm, he exposed the financial scandal worth $1.7 billion and fled Japan fearing for his life. Though persecuted his revelations proved to be true resulting in booking the culprits. Portraying a damaging status of corporate Japan, Woodford, in his memoirs has said: "I thought I was going to run a health-care and consumer electronics company, but found I had walked into a John Grisham novel."


== Media ==
Since the development of printing, the media has had greater power to expose scandals and since the advent of mass media, this power has increased. The media also has the capacity to support and/or oppose organizations and destabilize them thereby becoming involved in scandals themselves as well as reporting them.Following the Watergate scandal in the United States, other English-speaking countries have borrowed the suffix "gate" and added it to scandals of their own.


=== Journalism ===

Journalistic scandals relate to high-profile incidents or acts, whether done purposefully or by accident. It could be in violation of normally in vogue ethics and standards of journalism. It could also be in violation of the 'ideal' mission of journalism: to publish "news events and issues accurately and fairly."


=== Television ===

The American quiz show of the 1950s generated "hypnotic intensity" among viewers and contestants. The CBS Television show The $64,000 Question which started on 7 June 1955 and such other shows as The Big Surprise, Dotto, Tic Tac Dough, and Twenty One became the most publicized quiz shows, but soon generated scandals after a series of revelations that contestants of several popular television quiz shows conspired with the show's producers to rig the outcome. The quiz show scandals were driven by a drive for financial gain, a willingness of contestants to "play along" with the assistance, and the lack of regulation prohibiting the rigging of game shows. In October 1958, a New York grand jury was instituted by prosecutor Joseph Stone and the matter was examined with recording of closed-door testimony. Following this, the US Congress ruled rigging a quiz show a federal crime.The TV soap opera titled "Scandal" a popular show on the American Television ABC channel has been dubbed a "self-absorbed, overblown, overacted, pretentious, soliloquy-laden car-wreck-of-a-series."


== Sex scandals ==

A sex scandal is a scandal involving allegations or information about possibly-immoral sexual activities being made public. Sex scandals are often associated with sexual affairs of film stars, politicians, famous athletes and others in the public eye, and become scandals largely because of the prominence of the person involved, perceptions of hypocrisy on their part, or the non-normative or non-consensual nature of their sexual activity. A sex scandal may be based on reality, the product of false allegations, or a mixture of both.


== Sports ==

A desire for success and financial gain or the abuse of power in sport have also created many scandals both at an individual and the organisational level. Scandals arising from corruption have an impact of the credibility of sport. The World Anti-Doping Agency, as part of its role to "promote, coordinate and monitor the fight against drugs in sports", has showed that bribery, doping by athletes and doping sample-tampering, have occurred in collusion with national and international sporting organizations. Some consider that doping is "now endemic" in the world of sport and is becoming extremely pervasive, including more and more sports.One of the biggest individual scandals flowed from revelations that former American cycling champion Lance Armstrong had achieved success by consistent, long-term cheating. One of the biggest institutional sporting scandals is the 2015 FIFA corruption case. Doping scandals have plagued the Olympic games as well, such as in the Doping in East Germany scandal and the Asian Games in 1994. Scandals  in match games such as Major League baseball and cricket may relate to spot-fixing or gambling.


== See also ==
List of scandals with "-gate" suffix


== References ==


== Bibliography ==

Davis, John H. (8 December 2014). Perpetuation of the United States of America. Xlibris Corporation. ISBN 978-1-5035-2189-6.CS1 maint: ref=harv (link)
Dirks, Nicholas B (30 June 2009). The Scandal of Empire: India and the creation of imperial Britain. Harvard University Press. ISBN 978-0-674-03426-6.CS1 maint: ref=harv (link)
Ehrat, Johannes (2011). Power of Scandal: Semiotic and Pragmatic in Mass Media. University of Toronto Press. ISBN 978-1-4426-4125-9.CS1 maint: ref=harv (link)
Moeller, Robert R. (15 August 2008). Sarbanes-Oxley Internal Controls: Effective Auditing with AS5, CobiT, and ITIL. John Wiley & Sons. ISBN 978-0-470-28992-1.CS1 maint: ref=harv (link)
Ponceau, Peter Stephen Du (1834). A Discourse on the Necessity and the Means of Making Our National Literature Independent of that of Great Britain: Delivered Before the Members of the Pennsylvania Library of Foreign Literature and Science, on Saturday, Feb. 15, 1834. E. G. Dorsey.CS1 maint: ref=harv (link)
Vargo, Marc E (12 November 2013). Scandal: Infamous Gay Controversies of the Twentieth Century. Routledge. ISBN 978-1-317-76610-0.CS1 maint: ref=harv (link)
Williams, Anne; Head, Vivian (2008). Infamous Scandals. Book Sales. ISBN 978-0-7088-0365-3.CS1 maint: ref=harv (link)
Wilson, Colin; Wilson, Damon (31 May 2011). Scandal!: An Explosive Exposé of the Affairs, Corruption and Power Struggles of the Rich and Famous. Ebury Publishing. ISBN 978-0-7535-4732-8.CS1 maint: ref=harv (link)


== Further reading ==
Garment, S. (1991). Scandal: The Culture of Mistrust in American Politics. Anchor Books. ISBN 978-0-385-42511-7. 375 pages.