Tango Tangles is a 1914 American film comedy short starring Charles Chaplin and Roscoe Arbuckle. The action takes place in a dance hall, with a drunken Chaplin, Ford Sterling, and the huge, menacing, and acrobatic Arbuckle fighting over a girl. The supporting cast also features Chester Conklin and Minta Durfee.  The picture was written, directed and produced by Mack Sennett for Keystone Studios and distributed by Mutual Film Corporation.
In Tango Tangles, Charlie Chaplin appears without makeup and his usual mustache, baggy pants, and oversized shoes.  The film was shot at a dance hall without any sort of formal script.  Mack Sennett, in his 1954 autobiography King of Comedy, said of the impromptu nature of Tango Tangles, "We took Chaplin, [Ford] Sterling, [Roscoe] Arbuckle and [Chester] Conklin to a dance hall, turned them loose, and pointed a camera at them.  They made funny, and that was it."  Tango Tangles marked the last time that Ford Sterling and Chaplin appeared in the same film.  Sterling had decided to leave Keystone where he had gained most of his fame as the chief of the Keystone Cops.


== Reviews ==
The movie publication Bioscope wrote of Tango Tangles, "Jealousy in a dance room ends in a fight which is engaged by the dancers, musicians and attendants."
Another reviewer in The Cinema wrote, "The ballroom is soon converted into a battlefield which results in this Keystone being a real scream."


== Cast ==
Charles Chaplin - Tipsy Dancer
Roscoe Arbuckle - Clarinettist
Ford Sterling - Band leader
Chester Conklin - Guest in Police Costume
Minta Durfee - Guest
Charles Avery	... Guest in Straw Hat (uncredited)
Glen Cavender	... Drummer in band / Guest in Cone Hat (uncredited)
Alice Davenport	... Guest with Man in Overalls (uncredited)
Billy Gilbert	... Guest in cowboy hat (uncredited)
William Hauber	... Flutist (uncredited)
Bert Hunn	... Guest (uncredited)
George Jeske	... Cornet Player / Guest with Bow Tie (uncredited)
Edgar Kennedy	... Dance Hall Manager (uncredited)
Sadie Lampe	... Hat Check Girl (uncredited)
Hank Mann	... Guest in Overalls (uncredited)
Harry McCoy	... Piano Player (uncredited)
Rube Miller	... Guest Pushed Away (uncredited)
Dave Morris	... Dance Organizer (uncredited)
Eva Nelson	... Guest with Man in Cone Hat (uncredited)
Frank Opperman	... Clarinetist / Guest (uncredited)
Peggy Pearce	... Guest (uncredited)
Al St. John	... Guest in Convict Costume (uncredited)


== See also ==
List of American films of 1914
Charlie Chaplin filmography
Fatty Arbuckle filmography


== External links ==
Tango Tangles on IMDb
Tango Tangles on YouTube