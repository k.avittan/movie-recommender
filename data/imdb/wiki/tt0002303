The lesser of two evils principle, also referred to as the lesser evil principle and lesser-evilism, is the principle that when faced with selecting from two immoral options, the one which is least immoral should be chosen.


== In modern elections ==
In 2012, Huffington Post columnist Sanford Jay Rosen stated that the idea became a common practice for left-leaning voters in the United States due to their overwhelming disapproval of the United States government's support for the Vietnam War. Rosen stated: "Beginning with the 1968 presidential election, I often have heard from liberals that they could not vote for the lesser of two evils. Some said they would not vote; some said they would vote for a third party candidate. That mantra delivered us to Richard Nixon in 1972 until Watergate did him in. And it delivered us to George W. Bush and Dick Cheney in 2000 until they were termed out in 2009". Opponents of the modern usage of these terms in reference to electoral politics include revolutionaries who oppose the existing system as a whole as well as political moderates who advocate that third parties be given greater exposure in that system. For a particular voter in an election with more than two candidates, if the voter believes the most preferred candidate cannot win, the voter may be tempted to vote for the most favored viable candidate as a necessary evil or the lesser of two evils.
Supporters of lesser-evil tactics in the United States often cite United States politician Ralph Nader's presidential campaigns as examples of what can happen when a third-party candidate receives a significant number of votes. They claim that the mere existence of the third-party candidate essentially steals votes ("tilts" or "tips the scales") from the more progressive of the two main candidates and puts the election in favor of the "worse" candidate—because the small percentage that goes towards the third party candidate is a part "wasted" that could have instead gone to the lesser-evil candidate. For example, as the Green Party candidate in 2000, Nader garnered 2.7% of the popular vote and as a result is considered by some Democrats to have tipped the election to Bush. One counterargument is that Nader's candidacy likely increased turnout among liberals and that Al Gore took four of the five states—and thirty of the fifty-five electoral college votes—in which the outcome was decided by less than one percent of the vote. Others argue that supporters of Nader and other third party candidates draw from voters who would not vote for either Democrats or Republicans.
In the 2016 United States presidential election, both major candidates of the major parties—Hillary Clinton (D) and Donald Trump (R)—had a disapproval rating close to 60% by August 31, 2016. Green Party candidate Jill Stein invoked this idea in her campaign in her slogan "Don't vote for the lesser evil, fight for the greater good".
In his DarkHorse podcast, Bret Weinstein describes his Unity 2020 proposal for the 2020 presidential election as an option that, in case of failure, would not asymmetrically weaken voters' second best choice on a single political side, thereby avoiding the lesser evil paradox.In elections between only two candidates where one is mildly unpopular and the other immensely unpopular, opponents of both candidates frequently advocate a vote for the mildly unpopular candidate. For example, in the second round of the 2002 French presidential election graffiti in Paris told people to "vote for the crook, not the fascist". The "crook" in those scribbled public messages was Jacques Chirac of Rally for the Republic and the "fascist" was Jean-Marie Le Pen of the National Front. Chirac eventually won the second round having garnered 82% of the vote.


== Mythology ==
"Between Scylla and Charybdis" is an idiom derived from Homer's Odyssey. In the story, Odysseus chose to go near Scylla as the lesser of two evils. He lost six of his companions, but if he had gone near Charybdis all would be doomed. Because of such stories, having to navigate between the two hazards eventually entered idiomatic use. Another equivalent English seafaring phrase is "Between a rock and a hard place". The Latin line incidit in scyllam cupiens vitare charybdim ("he runs into Scylla, wishing to avoid Charybdis") had earlier become proverbial, with a meaning much the same as jumping from the frying pan into the fire. Erasmus recorded it as an ancient proverb in his Adagia, although the earliest known instance is in the Alexandreis, a 12th-century Latin epic poem by Walter of Châtillon.


== In popular culture ==
The principle of "the lesser of two evils" is sometimes jokingly changed to "the evil of two lessers", such as in the titles of these articles about the US presidential elections of  1988 and  2016.  One early variant of this, humorously contrasting the late songwriter Frank Loesser and his brother, was "the evil of two Loessers".In Patrick O'Brian's 1979 novel Fortune of War,  and in the 2003 movie Master and Commander: The Far Side of the World, Captain Jack Aubrey jokes that "in the Navy you must always choose the lesser of two weevils". "The lesser of two weevils" is also the title of a 2005 children's book by Steve Wide.


== See also ==
Consequentialism
Dilemma
False dilemma
Necessary evil
Principle of double effect
Two wrongs make a right
Trolley problem
Ticking time bomb scenario


== References ==


== External links ==