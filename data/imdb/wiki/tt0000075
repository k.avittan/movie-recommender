The Vanishing Lady or The Conjuring of a Woman at the House of Robert Houdin (French: Escamotage d'une dame chez Robert-Houdin) is an 1896 French short silent trick film directed by Georges Méliès.


== Synopsis ==
A magician walks onto a stage and brings out his assistant. He spreads a newspaper on the floor (thus demonstrating that no trap door is hidden there) and places a chair on top of it. He has his assistant sit in the chair, and spreads a blanket over her. When he removes the blanket, she has disappeared. He then waves his arms in the air and conjures up a skeleton. He places the blanket over the skeleton and removes it to reveal his assistant, alive and well.


== Production ==
Méliès himself is the magician in the film; his assistant is Jehanne d'Alcy.The film is based on a magic act developed by the French magician Buatier de Kolta. When the illusion was produced onstage, a trapdoor was used to create the appearances and disappearances; for the film, however, Méliès needed no trapdoor, using instead an editing technique called the substitution splice. The Vanishing Lady marks Méliès's first known use of the effect.


== Release ==
The Vanishing Lady was released by Méliès's Star Film Company and is numbered 70 in its catalogues. Though the surviving print of the film is in black-and-white, hand-colored prints of Méliès's films were also sold; the Méliès expert Jacques Malthête reconstructed a hand-colored version of the film in 1979, using authentic materials.


== References ==


== External links ==
The Vanishing Lady on IMDb
The Vanishing Lady on YouTube