Waiting for the Sirens' Call is the eighth studio album by English rock band New Order. The album was released on 28 March 2005 in the United Kingdom and 25 April 2005 in the United States, and was preceded by the single "Krafty" in February. Two additional singles from the album were released: "Jetstream", which features vocals by Ana Matronic from Scissor Sisters, and the title track of the album. The album was released at a time when the band were experiencing unprecedented recognition in the media.


== Production ==
Waiting for the Sirens' Call marks Phil Cunningham's recording and co-writing debut with New Order; although he had been playing live with the band since the Get Ready tour of 2001–2002. It is the first New Order album recorded without Gillian Gilbert who left the band in 2001 to look after her family. The album was recorded at Real World studios in 2003–2004, and production costs totalled £700,000. During the sessions the band also recorded seven songs intended for their next album, which was never completed as planned. These songs were shelved when Peter Hook quit the group in 2007. One song, "Hellbent", was eventually released in 2011 and all seven (plus a remix of "I Told You So") were released as the album Lost Sirens in 2013.
This album was the first and only New Order album to have a title track. This matches their current trend of now using song titles which are in the song lyrics, a practice New Order rarely did before their 2001 album Get Ready. The Japanese release includes several alternate versions of "Krafty" as bonus tracks, including one sung in Japanese. This was the first time that lead singer Bernard Sumner performed in a language other than English on record. The lyrics were translated by Masafumi Gotō. The US release of this album includes one extra track, a remix of "Guilt Is a Useless Emotion".


== Promotion ==
Promotional posters for the album utilised newly developed Hypertag technology to enable fans to download free content to their mobile phones, including ringtones, wallpapers or track previews, depending on the user's phone capability. This was one of the first implementations of such technology by the music industry.


== Reception ==
This album was chosen as one of Amazon.com's Top 100 Editor's Picks of 2005.
The track "Guilt Is a Useless Emotion", released as a promo single, was nominated for Best Dance Recording award at the 2006 Grammy Awards.
"Hey Now What You Doing" has been used in an advert for the Indesit Moon.
"Krafty" is featured on the soundtrack to the SXSW Award-winning Best Narrative Feature 2009 feature film Skills like This directed by Monty Miranda.


== Track listing ==
All tracks are written by New Order, except "Jetstream" written by New Order, S. Price and A. Lynch.US edition bonus track"Guilt Is a Useless Emotion" (Mac Quayle Vocal Mix) – 6:29Japanese edition bonus tracks"Krafty (Japanese Version)" – 4:33
"Krafty (The Glimmers Twelve Inch Extended)" –6:55
"Krafty (Phones Reality Remix)" – 7:07


== Personnel ==


=== New Order ===
Musician credits for New Order are not listed in the liner notes of the album's personnel. Below are the instruments that the group typically plays.

Bernard Sumner – vocals, guitars, synthesisers and programming
Peter Hook – 4 and 6-stringed bass
Stephen Morris – drums, synthesisers and programming
Phil Cunningham – synthesisers and programming, guitars


=== Production ===
The original liner notes list the album's personnel as follows:


== Charts ==


== Certifications and sales ==


== References ==