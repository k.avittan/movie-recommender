A hope chest, also called dowry chest, cedar chest, trousseau chest or glory box is a piece of furniture traditionally used to collect items such as clothing and household linen, by unmarried young women in anticipation of married life.
The term "hope chest" or "cedar chest" is used in the midwest or south of the United States; in the United Kingdom, the term is "bottom drawer"; while both terms, and "glory box" are used by women in Australia.


== Social context ==
Using her own needlework skills to construct a trousseau and stock her glory-box "was for the working girl the equivalent of planning and saving for marriage on the part of the provident and ambitious young man." 
The collection of a trousseau was a common coming-of-age rite until approximately the 1950s; it was typically a step on the road to marriage between courting a man and engagement. It wasn't always collected in a special chest, hence the alternative UK term bottom drawer, which refers to putting aside one drawer in a chest of drawers for collecting the trousseau undisturbed, but such a chest was an acceptable gift for a girl approaching a marriageable age.
Contents of a "hope chest" or "glory box" included typical dowry items such as clothing (especially a special dress), table linens, towels, bed linens, quilts and occasionally dishware.  As a bride would typically leave home on marriage, hope chests were sometimes made with an eye to portability, albeit infrequently. Examples of hand-made items made between 1916 and 1918 for a trousseau by a prospective bride are on display in the National Museum of Australia.  In this case, the trousseau—never used because its creator's fiancé was killed in World War I before the marriage took place—was stored in calico bags rather than in a chest.
The hope chest was often used for the firstborn girl of a family. Instead of just having sheets and household linen in the bottom drawer, this box would transport these goods and dowries and then later be used as a standard piece of furniture for the lady of the house to use. This dowry chest was often richly decorated, however over time dowry chests gradually became smaller, with jewelry boxes emerging instead of large dowry boxes.By contrast, a "bridal chest" was given to a bride at her wedding, by her husband, and so is not a "hope chest" in this sense.


== Historical origins ==

Cassone of renaissance Italy
Large decorated and showy chests, forming part of dynastic marriages in 15th and 16th century Italy. These were prized displays of wealth, of even more value than their contents.Dutch kast or German SchrankThese are tall wardrobe-like chests with double doors. These are larger than most hope chests, intended for regular service in the home after marriage, and so were constructed as to partially dismantle for transport.American settlersThe peak of the hope chest as folk art came with the waves of European immigrants to America. Many of these, from Scandinavia to the Northern Midwest and Germans in Pennsylvania (Amish, had long traditions of plainly constructed chests with extensive painted decoration.Arabic originsThese chests were also known as "dower chests" in the Middle East. "The more than 300,000 surviving documents in the Cairo Genizah are one of our richest sources of insight into daily life in Egypt from the ninth to the 19th centuries. Among them are numerous marriage contracts, and almost all refer to a dower chest. For it, we see two names used: The muqaddimah was specifically for the bride’s personal possessions; sunduq, which normally came in matching pairs, were for other goods. They were not usually elaborately decorated, except in the case of the ruling class."


== Suffocation hazard to children ==
There have been several instances of child deaths due to suffocation inside hope chests, due to the piece's traditional design which can trap children under a heavy and sometimes self-locking lid. In 1996, following reports of at least six child suffocation deaths, the manufacturer Lane Furniture recalled 12 million hope chests, the lids of which latched shut automatically, and could not be opened from the inside. Specifically, the recall applied to the locks of all "Lane" and "Virginia Maid" cedar chests manufactured between 1912 and 1987. As part of the recall, they provide new lock latch configuration replacement parts. However, they estimated that 6 million chests still used the recalled lock latch. As of 2020, Lane Furniture still offered replacement locks through their website.


== Styles ==
The typical hope chest is of lidded blanket chest form. In some traditions, there may also be one or two side-by-side drawers beneath. As with blanket chests, a small till may also be found inside for small items.


=== Lane Company chests ===
As the contents of such a chest would primarily be linens, construction in moth-repellent cedar, or at least a cedar lining, was popular. The Lane Company of Altavista, Virginia (active 1912-2001) were a notable maker of cedar chests. After developing production-line techniques for making ammunition boxes during World War I, they turned these production techniques (and a patented locking-mitre corner joint) into vast numbers of chests. This was aided by strong advertising, using a teenaged Shirley Temple as a model, in a campaign targeted at GIs and absentee sweethearts of World War II. They were particularly well known for their practice (since 1930) of distributing miniature (9" long) cedar chests to girls graduating from high-school as advertising gifts.  The Eastern Redcedar (Juniperus virginiana) is the "cedar" used in making moth-repelling cedar chests and drawers, as well as pencils.


=== Decoration ===
Decoration is not an intrinsic part of the hope chest, but often appears.

CarvingCarving was notable in the 17th and 18th century joined oak chests. The Hadley chests of Massachusetts are covered by extensive surface carving in the typical low-relief style of the period.PaintingThis is typically seen in the Scandinavian and German immigrant traditions and follows traditional styles. In many Arab countries, they are still referred to as "sanduq ‘arus", or “wedding box,” although modern trends have them made of metal rather than wood, and looking more like a footlocker. They are elaborately painted, often with a mosque dome or architectural design on the lid, with the color red predominating.GessoThe elaborate gilded gesso of the cassoni was produced by skilled and expensive craftsmen. This work has not been seen since and doesn't form part of the folk tradition.Sulfur inlaySulfur inlay is a rare technique in furniture and for chests it is only known for a short period. Between 1765 and around 1820, German immigrant cabinetmakers in Lancaster County, Pennsylvania, used it to decorate the surface of chests. The Deitrich chest of 1783 is now in the Smithsonian.


== In popular culture ==
In the 1920s, 'hope chest' was slang for a pack of cigarettes."Glory Box" is also the title of a song from the band Portishead. The song is about a woman who is giving up her time as a temptress to commit to a settled life with a man, asking him to take a look from her side every now and then.
In the 1985 movie Back to the Future, the character Lorraine McFly refers to having placed Marty McFly's pants "over there...on my hope chest" after he had been knocked unconscious. The joke implies her romantic interest in Marty when, unbeknownst to Lorraine, Marty is her son from the future.
In Glee, Kurt Hummel has claimed to own a hope chest. This is an ironic inversion of the hope chest's usual role for a prospective bride, as Kurt is a gay teenage boy.
In the 2012 film The Hobbit: An Unexpected Journey, Bilbo Baggins asks Kili to "please not do that" on his mother's glory box, when the dwarf cleans his boots on the chest. This suggests that in Middle Earth women (possibly from the high society) also used hope chests, at least in Hobbiton.
In Me and You and Everyone We Know, Peter and Robbies' neighbor has a hope chest, which is a part of the climax of the film.
In The New Yankee Workshop, Norm Abram demonstrates how to build and paint a Pennsylvania Dutch-style hope chest (called a dower chest in the episode) modeled on one on display at the Winterthur Museum in Delaware.


== See also ==
Dower
Bride price
Dowry


== References ==


== External links ==