Inside Out is a 1986 American drama film directed by Robert Taicher, written by Robert Taicher and Kevin Bartelme and starring Elliott Gould, Howard Hesseman and Jennifer Tilly.


== Plot ==
Jimmy Morgan, a financially successful entrepreneur, is watching his financial and personal life collapse around him as he sits in his posh New York City apartment. His estranged wife reveals her plan to leave with his daughter for Chicago. In the meantime, Morgan's stock market portfolio evaporates in a bad market, his business partner Leo Gross is embezzling money entrusted to their business for himself, and Morgan is gambling (and losing) on professional football games in larger and larger amounts.
Morgan orders food, prostitutes, and drugs over the telephone or (later in the film) using his computer and a modem. His television is on almost nonstop. As friends visit, Morgan is revealed to be an agoraphobe, and cannot seem to find the courage to leave his apartment even when doing so may save him financially or personally. He watches the world through his television, video cameras pointed out past his apartment door, and through his windows. Morgan's answering machine is as much the center of his world as his television, used to carefully screen calls and people.
Gross, his business partner, assures him that the temporary business losses will be ironed out, and that if Morgan is unavailable to solve the problem, he will be happy to do so on his behalf. In reality, he is redirecting this money for himself, and using Morgan's agoraphobia to his advantage. Meanwhile, Morgan, who gambles on National Football League games casually, has begun to bet more and more on these games to cover quickly mounting losses. Friends make efforts to help Jimmy and even to get him to leave the house, but begin to drift away the deeper he digs himself into his apartment. He finds himself truly alone as they give up, frustrated.
Jimmy makes a friendship over his apartment intercom with a homeless person that sleeps in the entryway. He occasionally orders food for the man over the course of days to weeks. Eventually the man disappears as quietly as he arrived. Finances become desperate as Morgan's savings disappear, and gambling losses destroy what little money Morgan has left. He is left in an empty apartment with only his answering machine, after all his possessions are foreclosed upon. Only in this final desperate situation does Morgan finally build the courage to leave his apartment to begin anew, and to contact his wife and daughter before they leave for Chicago.


== Cast ==
Elliott Gould as Jimmy Morgan
Howard Hesseman as Jack
Jennifer Tilly as Amy
Beah Richards as Verna
Dana Elcar as Leo Gross
Timothy Scott as The Homeless Guy
Sandy McPeak as Lewis Curlson
Nicole Nourmand as Heather
John Bleifer as Hyman
Robert Miranda as Sal
Meshach Taylor as Freddy
Kathleen Freeman as Mother (voice)


== Release ==
Inside Out was filmed in 1985. Elliott Gould expected to appear on behalf of the movie in late 1986 at film festivals in Breckenridge, Colorado; Berlin; and at the 1986 Chicago International Film Festival, where the movie premiered on November 1, and was nominated for the festival's Gold Hugo for Best Feature. Hemdale Releasing Corporation distributed the movie for a limited theatrical release in New York City, on August 21, 1987.


== References ==


== External links ==
Inside Out on IMDb