Something to Think About is a 1920 American silent drama film directed by Cecil B. DeMille. The film stars Elliott Dexter and Gloria Swanson. Prints of the film exist at the George Eastman Museum in Rochester, New York, and at the Filmmuseum in Amsterdam.


== Plot ==
As described in a film magazine, David Markely's (Dexter) affection for Ruth Anderson (Swanson) followed her from childhood and deepened with her womanhood. He is a young man of means but a cripple, while she is the daughter of a blacksmith. David persuades her father to allow him to have her educated. When she returns from school, the father realizes David's attitude towards Ruth and plans their marriage. Ruth, against her father's wishes, marries Jim Dirk (Blue), the young lover of her heart. A few years later Jim is killed in a subway accident. Ruth returns to her father for forgiveness but finds him blinded by the sparks from his forge and on the way to the county poorhouse. He is stubborn in his unforgiveness of her. She is about to take her own life when David rescues her, offering the protection of his name for her and the child that is about to be born to her. As his wife she eventually realizes a great love for him which he refuses to admit is anything but gratitude. The preachings of his housekeeper (McDowell) have an effect that brings about the reconciliation of Ruth and her father, and through the little boy Bobby (Moore) he becomes a member of the happy household.


== Cast ==
Elliott Dexter as David Markely
Gloria Swanson as Ruth Anderson
Monte Blue as Jim Dirk
Theodore Roberts as Luke Anderson
Claire McDowell as Housekeeper
Michael D. Moore as Bobby (credited as Mickey Moore)
Julia Faye as Banker's Daughter
Jim Mason as Country Masher
Togo Yamamoto as Servant
Theodore Kosloff as Clown
William Boyd (uncredited)


== Production notes ==
Something to Think About began filming on January 20, 1920, with a budget of $169,330. Filming completed on March 30, 1920. The film was released on October 20, 1920 and grossed a total of $915,848.51.


== References ==


== External links ==
Something to Think About on IMDb
Something to Think About at AllMovie
Billboard type paper lobby poster