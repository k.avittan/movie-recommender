The Pawtucket Red Sox (known colloquially as the PawSox) were a professional minor league baseball team based in Pawtucket, Rhode Island. The team was a member of the International League and was the Triple-A affiliate of the Boston Red Sox from 1973 to 2020. They played their home games at McCoy Stadium in Pawtucket, as the only professional baseball team in Rhode Island. Its most recent championship win was in 2014. Following the 2020 season, the team moved to Worcester, Massachusetts, to become the Worcester Red Sox.
The Pawtucket Red Sox were born as a Double-A Eastern League franchise in 1970. Three years later, Boston's Triple-A affiliate in the International League replaced the Eastern League PawSox. After enduring three different owners, at least two threats to move the team elsewhere, and bankruptcy, the PawSox were purchased from the International League by local industrialist Ben Mondor in January 1977. Over the next 38 years, Mondor (who died in 2010) and his heirs stabilized the franchise and turned it into a success; it was twice (1990 and 2003) selected the winner of Baseball America's Bob Freitas Award as the top Triple-A operation in minor league baseball, won the 1990 John H. Johnson President's Award, led its league in total attendance three times between 2004 and 2008, and captured three Governors' Cups as playoff champions.
On February 23, 2015, the team was sold to a group headed by then-Boston Red Sox president and chief executive officer Larry Lucchino and Rhode Island attorney James J. Skeffington. Thwarted in two attempts to replace McCoy Stadium with a new facility (first in adjacent Providence, then in a downtown site in Pawtucket), the club announced on August 17, 2018, that it would move to Worcester, 42 miles (68 km) away, in 2021. In November 2019, the franchise announced that the team would be called the Worcester Red Sox.
On June 30, 2020, it was announced that the 2020 Minor League Baseball season would not be played, another impact of the COVID-19 pandemic on sports. Thus, the team last played minor league games in Pawtucket during the 2019 season.


== Team history ==


=== Eastern League franchise (1970–1972) ===
The first team to be dubbed the Pawtucket Red Sox debuted at McCoy Stadium in 1970 as a member of the Double-A Eastern League. The franchise, owned by former Major League shortstop Joe Buzas, had spent the previous five seasons (1965–69) as the Pittsfield Red Sox after playing in four different Pennsylvania cities—Allentown, Johnstown, York and Reading—over seven years (1958–64).
After three seasons, Pawtucket's Eastern League franchise moved to Bristol, Connecticut, in 1973 to make room for the Triple-A PawSox, the former Louisville Colonels of the International League.
Carlton Fisk, the future Baseball Hall of Fame catcher, played for the Eastern League PawSox in 1970. Shortstop Rick Burleson and first baseman Cecil Cooper are among the players who toiled for both the Double-A and Triple-A versions of the team. This first edition of the PawSox franchise played for ten seasons as the Bristol Red Sox and then  spent 33 seasons (1983–2015) in New Britain, Connecticut, the last 21 of them as the Rock Cats. In 2016, the Rock Cats moved to Connecticut's capital city, and were rechristened the Hartford Yard Goats.
The Cleveland Indians had also placed an Eastern League club in Pawtucket, in 1966–67. The Pawtucket Indians moved to Waterbury, Connecticut, in 1968. The Pawtucket Slaters, a Boston Braves farm club in the Class B New England League, represented the city from 1946 to 1949, when the NEL disbanded.


=== Roots in Toronto and Louisville ===

The Triple-A team that is now the Pawtucket Red Sox began in 1896 as the Toronto Maple Leafs. After the American Association and its Louisville Colonels franchise folded in 1962 and the American League owners voted down Charlie O. Finley's agreement to move the Kansas City A's to Louisville in 1964, Louisville was ready for the return of baseball. In 1968 the Maple Leafs, the Red Sox' top minor league club since 1965, were bought by Walter J. Dilbeck and moved to Louisville where they became the new Louisville Colonels and retained their affiliation with the Red Sox. They played at Fairgrounds Stadium on the Kentucky State Fairgrounds. While in Louisville, star players included Carlton Fisk (1971), Dwight Evans (1972), and Cecil Cooper (1972). The Louisville Colonels made the International League playoffs in 1969 and 1972.


=== Early struggles and bankruptcy (1973–1976) ===
In 1972, the Kentucky State Fair Board remodeled Cardinal Stadium so it could accommodate football. The renovations made the stadium unsuitable for baseball; among other things, it was far too large for a Triple-A team. However, the stadium was later used by the latter-day Louisville Redbirds club, setting minor league attendance records and outdrawing several major league teams. Following the 1972 season, the Louisville Colonels of the International League moved to McCoy Stadium and became the Pawtucket Red Sox, with Buzas taking over as owner.
The first Triple-A team was a success on the field, led by future major leaguers Cecil Cooper and Dick Pole, winning the 1973 Governors' Cup Championship in their inaugural year in the league over the Charleston Charlies. Then, they followed up by defeating the Tulsa Oilers of the American Association to win the Junior World Series. The following season they finished 30 games below .500 and lost an estimated $40,000. Buzas then sold the team to Philip Anez, a Smithfield advertising executive, in January 1975.While the parent club was on their way to the 1975 World Series, the 1975 PawSox finished with a 53–87 mark. The team changed its name to the Rhode Island Red Sox for the 1976 season, but little changed on the field with a third straight sub-.500 season and falling attendance. Anez threatened to move his club to Jersey City, New Jersey. After the season, the franchise went bankrupt, unable to pay off $2 million worth of debt. The International League took it over, then awarded it in December 1976 to Massachusetts businessman Marvin Adelson, who renamed the team the New England Red Sox—and explored transferring it to Worcester. But after less than two months, in January 1977, the league revoked Adelson's franchise, alleging "nonperformance of terms and conditions".


=== The Ben Mondor era (1977–2015) ===
Although it appeared the Red Sox's stay in the Pawtucket area was about to come to an end, retired Lincoln businessman Ben Mondor stepped in and made sure the team remained in the city. Mondor was granted a brand-new franchise and restored the name to the Pawtucket Red Sox. However, it retained the old team's history and affiliation with the big-league Red Sox. Nonetheless, it was really in 1977 that the current Pawtucket Red Sox, and PawSox, were born.
Mondor's tenure began inauspiciously. While the PawSox rebounded to win the regular-season pennant, they only drew 1,000 fans per game—believed to be the fewest for a first-place team in the history of the International League. That year's edition of the PawSox fell in the Governors' Cup finals to Charleston in four straight games. However, it won its second league championship in 1984, and drew almost 199,000 fans, second in the league.
After several years of playing in a stadium that was barely suitable for a Triple-A team, in 1998 Mondor and team president Mike Tamburro oversaw the transformation of McCoy Stadium from an aging 1942 relic into its renovated form. While they kept the price of tickets at $6 and $10, parking is free. The PawSox led the league in attendance in 2008, when 636,788 fans saw baseball at McCoy, an average of 9,097 for each of the 70 openings. In 2005, they set a franchise record with 688,421 tickets sold during the year. Kevin Youkilis played for the team in 2003, and completed a streak he started while in Portland: he reached base in 71 consecutive games, tying future teammate Kevin Millar's minor-league records for consecutive games reaching base.
In addition to their success at the box office, the PawSox have excelled on the field. In 2000, Pawtucket set an all-time franchise record for victories with 82, as the team completed their fifth-straight winning season. Three years later the PawSox would top their own record by winning 83 games. In 2008, they won 85 games. The 1984 team defeated the now-defunct Maine Guides 3–2 to win the 1984 Governors' Cup trophy for their second championship in Pawtucket Red Sox history. In 2012, the PawSox defeated the Charlotte Knights to win the Governors' Cup for a third time. A fourth title was won in 2014 when the PawSox took down the Durham Bulls in five games.
The origins of PawSox are traced back to the first season in which Mondor owned the club. Three weeks before the 1977 season began the team lacked uniforms. BoSox vice president Haywood Sullivan stepped in and sent Pawtucket 48 sets of old home and away uniforms from the parent club. Although the home uniforms were fine for the team to use, the road uniforms had "Boston" stitched across the chest, which was a problem. Tamburro suggested using the name "PawSox" across the front, with each unstitched "Boston" letter replaced with one that spelled "PawSox". Thus, the PawSox name was born out of the necessity of a uniform crisis.
Mondor died on October 3, 2010, at the age of 85. His widow, Madeleine, became the new majority owner of the PawSox.


=== Sale and Rhode Island stadium plans (2015–2018) ===
On November 29, 2014, it was reported that members of the Boston Red Sox' ownership group were in the process of purchasing the PawSox from Madeleine Mondor and two long-time executives who also held stock in the team: president Tamburro and vice president and general manager Lou Schwechheimer.Nearly three months later, on February 23, the sale to Lucchino, Skeffington and their partners was formally announced. Lucchino added the title of chairman of the PawSox to his Boston responsibilities, and Skeffington became club president. Other partners included Rhode Island businessmen Bernard Cammarata, William P. Egan, Habib Gorgi, J. Terrence Murray and Thomas M. Ryan, as well as Fenway Sports Management (a division of the BoSox' parent company, Fenway Sports Group), and two limited partners in FSG, Arthur E. Nicholas and Frank M. Resnek.That day, the new owners also announced their intention to move the team out of McCoy Stadium and build a new baseball park six miles (9.65 km) to the south in downtown Providence, and begin play there as early as 2017. Skeffington said the club would be renamed the Rhode Island Red Sox upon the move. That name was previously used by the 1976 edition of the PawSox, before Mondor purchased the team and restored its Pawtucket identity.  In the weeks following announcement of the sale, Skeffington led a media tour of the proposed new stadium site on the Providence River and, with Lucchino, served as a point person in negotiations with state and local officials over public financing arrangements for the new park.However, Skeffington, 73, died from a heart attack while jogging near his Barrington home on May 17, 2015, disrupting the team's efforts to secure an agreement with Rhode Island officials. Then, on August 1, Lucchino announced his retirement as the CEO and president of the Boston Red Sox, effective at the end of the 2015 season.
In September, Governor Gina Raimondo told Lucchino that the riverfront parcel, consisting of public land formerly occupied by Interstate 195 and private property owned by Brown University, "was not suitable and there were too many obstacles that remained." In the wake of the setback, Lucchino said that the team preferred to remain in Rhode Island, but neither he nor other PawSox officials immediately commented about possible alternative locations. In the ensuing weeks, reports surfaced that Worcester and two other Massachusetts cities—Springfield and Fall River—might bid for the team.On November 5, Skeffington's position was filled when Dr. Charles Steinberg, longtime Lucchino aide and public affairs and PR executive with four big-league teams, including the Red Sox, became club president. Tamburro remained on board as vice chairman, and Dan Rea III became the PawSox' new general manager, after Schwechheimer departed to join an ownership group that purchased the Triple-A New Orleans Zephyrs. Amidst the uncertainty over its longterm home, Steinberg committed the team to remaining in Pawtucket for five seasons, through 2020, and to rebuilding its relationship with its fans.During the summer of 2016, the city, state and team began a feasibility study to determine the extent of needed renovations to McCoy Stadium. That study concluded that renovating McCoy would cost $68 million, while building a new stadium on the site would cost $78 million. On May 16, 2017, a downtown Pawtucket stadium proposal, The Ballpark at Slater Mill, was jointly announced by Lucchino and Pawtucket's mayor, Donald Grebien. The ballpark, to be built on a site bracketed by Interstate 95 and the Blackstone River, would cost an estimated $83 million, with the team footing $45 million, the state $23 million, and the city the remaining $15 million. But when the stadium project went before the Rhode Island General Assembly in 2018, the financing formula was amended to shift the risk of borrowing money from the state to investors, thus exposing them to potentially higher interest rates. The amended bill passed, and was signed into law by Raimondo on June 29, 2018.


=== Final season and relocation to Worcester (2019–2021) ===
The new financing arrangement was rejected by the PawSox ownership. On August 17, 2018, the team announced that it will relocate to a new stadium in Worcester in April 2021. The stadium will be part of a $240 million redevelopment of Worcester's Kelley Square and Canal District. The move would end the team's history in Pawtucket after 51 seasons.
Alluding to "controversy, disagreement and opposition", apparently on the part of Rhode Island legislators who changed the Pawtucket stadium's financing formula, Lucchino said that a 35-page letter of intent had been signed with Worcester's mayor and city manager for the downtown ballpark project to house the relocated team. The announcement capped a concerted three-year effort by Worcester and Massachusetts officials and local business leaders to woo the PawSox to anchor a downtown redevelopment that includes the stadium, new housing, a hotel, a parking garage and redesign of the Kelley Square intersection. On September 12, 2018, the Worcester City Council voted 9–1 to approve the stadium proposal, paving the way for the PawSox' relocation.

During the 2019 season of the International League, Pawtucket finished last in the North Division, with a record of 59–81. The team's final game was played on September 2, 2019; it was a 5–4 home victory over the Lehigh Valley IronPigs in 10 innings.While 2020 was planned to be the team's final season of play in the International League, the Minor League Baseball season was cancelled, due to the COVID-19 pandemic in the United States. During the 2020 Major League Baseball season, McCoy Stadium served as the alternate training site for the Boston Red Sox.


== "The Longest Game" ==

The PawSox played in and won the longest game in professional baseball history, a 33-inning affair against the Rochester Red Wings at McCoy Stadium. The game started on April 18, 1981. Play was suspended at 4:07 a.m. at the end of the 32nd inning. The game did not resume again until June 23, when the Red Wings returned to Pawtucket. Only one inning was needed, with the PawSox winning 3–2 in the bottom of the 33rd when first baseman Dave Koza drove in the leadoff hitter, second baseman Marty Barrett, with a bases-loaded single off Cliff Speck. Neither Speck nor Steve Grilli, the Red Wings losing pitcher, were even on the team's roster back in April. Future major league Hall of Fame players Cal Ripken Jr. and Wade Boggs played in the game.
On June 23, 2006, the PawSox celebrated the 25th anniversary of "The Longest Game" with events and festivities when they played the Columbus Clippers. The 35th anniversary was also commemorated on April 19, 2016.


== Perfect games ==
Tomo Ohka pitched a nine-inning perfect game for the Pawtucket Red Sox on June 1, 2000. Ohka retired all 27 batters he faced in a 2–0 win over the Charlotte Knights, and he needed just 76 pitches to toss the first nine-inning perfect game in the International League since 1952.
On August 10, 2003, Bronson Arroyo pitched the fourth nine-inning perfect game in the 121-year history of the International League as the PawSox beat the Buffalo Bisons 7–0 at McCoy Stadium. He needed 101 pitches to throw his masterpiece (73 strikes), struck out nine, and got 10 fly outs and eight ground outs from the Buffalo 27 batters. He went to a three-ball count to just three hitters all game. At the end of the month, he was promoted to the majors, and remained with the Red Sox until the 2005–06 offseason, when the Red Sox traded him to the Cincinnati Reds for Wily Mo Pena.


== Halls of fame ==

Several PawSox players and personnel have been inducted into the International League Hall of Fame. They are owner Ben Mondor, manager Joe Morgan, outfielder Jim Rice, third baseman Wade Boggs, and then-team president Mike Tamburro, now their Vice Chairman. Several former PawSox players have also been inducted into the National Baseball Hall of Fame in Cooperstown, New York, including Carlton Fisk, Boggs and Rice.
In July 2016, Rice, Boggs, and Mondor (represented by his widow Madeleine) became the inaugural class of inductees into the PawSox Hall of Fame. Additions are considered on an annual basis.


== Titles ==
The PawSox won the Governors' Cup, the championship of the IL, four times, and played in the championship series nine times. They also played in the championship of Triple-A baseball on three occasions: in 1973 they defeated the Tulsa Oilers 4 games to 1 in the Junior World Series, in 2012 they fell to the Reno Aces 10–3 in the Triple-A National Championship Game, and in 2014, the team was defeated by the Omaha Storm Chasers 4-2.


== Notable former players ==

(*) = rehab assignment


== Yearly results ==
(*= Won Governors' Cup)


== Playoff history ==


== Triple-A managerial history ==


== Broadcasters ==
As of March 2020, the announcers for the Pawsox Radio Network weere Josh Maurer, Mike Antonellis, Jim Cain and Steve McDonald, of URI football and men's basketball.Pawtucket has served as a springboard for Major League Baseball broadcasters. As of 2020, there are seven former PawSox radio and two television announcers broadcasting for Major League Baseball teams. In the below list, first are the years with the teams they have broadcast for, and second are the years the broadcaster was with the Red Sox.

Gary Cohen (New York Mets) (1989–present); MLB on CBS Radio (1986, 1991–1997); MLB on ESPN Radio (1998–present); (1987–88)
Dave Flemming (San Francisco Giants) (fill-in 2003, regular 2004–present) (2001–03)
Andy Freed (Tampa Bay Rays) (2005–present) (2001–04)
Dave Jageler (Washington Nationals) (2006–present) (2005)
Don Orsillo (Boston Red Sox) (2001–2015); San Diego Padres (2016–present) (1996–2000)
Glenn Geffner (Miami Marlins) (2008–present);  fill-in for San Diego Padres (1996–2002) and Boston Red Sox broadcaster (2005–07) (2006–07)
Aaron Goldsmith (Seattle Mariners) (2013–present) (2012)
Jeff Levering (Milwaukee Brewers) (2015–present) (2013–14)
Josh Maurer fill in for Boston Red Sox (2015) (2014–present)
Will Flemming (Boston Red Sox) (2018–present) fill in for Boston Red Sox (2018) (2015–2018)
Mike Monaco (Boston Red Sox, ACC Network) (2020–present) fill in for Boston Red Sox (2019); with PawSox (2017–2019)In addition, Dan Hoard is now the broadcaster for the NFL's Cincinnati Bengals. He was with the PawSox from 2006 to 2011. On April 24, 2013, it was announced that current broadcaster Bob Socci would become the New England Patriots play-by-play broadcaster starting with the 2013 season.Other former Pawtucket announcers include Bob Kurtz of the NHL's Minnesota Wild, Dave Shea, who spent time with the Washington Nationals, Bob Rodgers, Jack LeFaivre, Matt Pinto, and Mike Stenhouse.


== See also ==

Pawsox Radio Network


== References ==


== External links ==
Pawtucket Red Sox official website
Boston Red Sox prospects
Roster, splits, and situational stats