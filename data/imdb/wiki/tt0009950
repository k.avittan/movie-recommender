Bolshevism on Trial is a 1919 American silent propaganda film made by the Mayflower Photoplay Company and distributed through Lewis J. Selznick's Select Pictures Corporation.
Directed by Harley Knoles from a screenplay by Harry Chandlee, it is based on the 1909 novel Comrades: A Story of Social Adventure in California by Thomas Dixon, author of another novel that served as the basis for The Birth of a Nation. It premiered in April 1919 during the First Red Scare.


== Plot ==
Barbara, a wealthy female socialite intent on reforming capitalism is lured into the Socialist cause by Herman, a Socialist agitator. Her concerned boyfriend Norman hears her lecture on the virtues of international socialism and is converted to her views. Prompted by Herman, she raises money among her wealthy friends to buy Paradise Island off the Florida coast to establish a collective colony, a society of "happiness and plenty." Norman tries to raise money from his father and is rebuffed. His father expects Norman will benefit from the experience: "He'll get his island and a lesson along with it." When the wealthy colonists settled on their island, they elect Norman their "Chief Comrade." They quickly discover that none of them has any worthwhile skills. Most identify themselves as "assistant managers." Faced with disorganization, the colonists replace Norman with Herman, as the activist had long intended. He establishes a police force, abolishes marriage, and has the state assume ownership of the women and children. He imprisons Norman, which prompts Barbara's epiphany: "The poor deluded people will starve and die as they are in Russia."  She rejects Herman's advances and Norman's father arrives at the head of a Navy fleet to save the day. Norman lowers the Red flag and raises the American flag to general cheers.


== Primary cast ==
Robert Frazer as Captain Norman Worth
Leslie Stowe as Herman Wolff
Howard Truesdale as Colonel Worth
Jim Savage as Tom Mooney
Pinna Nesbit as Barbara Bozenta
Ethel Wright as Catherine Wolff
Valda Valkyrien as Elena Worth
May Hopkins as Blanche
Chief Standing Bear as Saka
J.G. Davis as Jim


== Critical reception ==
The film's advertising called it "the timeliest picture ever filmed" and reviews were good. "Powerful, well-knit with indubitably true and biting satire," said Photoplay. As a promotion device, the April 15, 1919, issue of Moving Picture World suggested staging a radical demonstration by hanging red flags around town and then have actors in military uniforms storm in to tear them down. Then distribute handbills to the confused and curious crowds assuring them that Bolshevism on Trial takes a stand against Bolshevism and "you will not only clean up but will profit by future business." When this publicity technique came to the attention of U.S. Secretary of Labor William B. Wilson, he expressed his dismay to the press: "This publication proposes by deceptive methods of advertising to stir every community in the United States into riotous demonstrations for the purpose of making profits for the moving picture business... ." He hoped to ban movies treating Bolshevism and Socialism.


== References ==


=== Notes ===


=== Bibliography ===
Hagedorn, Ann, Savage Peace: Hope and Fear in America, 1919 (NY: Simon & Schuster, 2007)
Hanson, Patricia King and Gevinson, Alan, eds., The American Film Institute Catalog of Motion Pictures Produced in the United States, vol.F1: Feature Films, 1911-1920 (Berkeley: University of California Press, 1988)


== External links ==
Bolshevism on Trial on IMDb