Burglar Bill is a 1977 children's picture book by Janet and Allan Ahlberg about a burglar who accidentally steals a baby. The book was a runner-up for the Kate Greenaway Medal in 1978.


== Plot summary ==
Burglar Bill is a thief and all of his possessions are stolen items, including the bed he goes to sleep in. On a typical night of thievery, Burglar Bill comes across a box with holes in, and takes it. Upon arriving home, he discovers that within the box is a baby. The baby and Burglar Bill end up spending a day together, but when Bill is putting the baby to bed, he hears an intruder downstairs. He confronts the burglar, who he discovers is Burglar Betty, and they talk to one another to find they have much in common. Bill mentions his new infant friend that he found the night before. He introduces Betty to the baby, only for them both to discover that the baby belongs to Betty. They both decide to give thievery up and return everything they stole to live happily together as a family.


== The Boyhood of Burglar Bill ==
The Boyhood of Burglar Bill by Allan Ahlberg was published in 2007. It is a middle-grade novel principally about football and friendship set in a Midland town in 1953. It is an autobiographical story rather than a true prequel to the picture book.


== Reception ==
Janet Ahlberg, winner of two Greenaway Medals, received runner-up for Burglar Bill (1977).The book has received mixed reviews on various online bookstores and websites. Some readers loved the book's charm and unique story, while others did not see it as being morally acceptable for a young child to read, as the subject of burglary is taken fairly light-heartedly without serious consequences.


=== Fan adaptations ===
A university project that instructed the recreation of a children's book as a short animated film used Burglar Bill as its subject. It was uploaded to YouTube by user Mgcmodels and titled "Burglar Bill Animation.avi". The video has attracted nearly 60,000 views since its upload on 10 April of 2011.


== Costume ==
The striking simplicity of Bill's burgling outfit – a black mask and cap and a striped shirt – has made Burglar Bill a popular party or Halloween costume choice.


== Buckingham Palace ==
Burglar Bill, played by Bradley Walsh, put in an appearance at the Children's Party at the Palace, a celebration of British children's literature held in Buckingham Palace Garden in 2006 in honour of the Queen's 80th birthday.


== Another Burglar Bill ==
Burglar Bill was a mock recitation piece written by F. Anstey for the satirical magazine Punch, or the London Charivari. With some other pieces it was published in 1888 as Burglar Bill, and other pieces for the young reciter. The piece tells the story of a burglar interrupted in his work by a lisping little girl who asks him to force open the stuck door of her doll's house. Moved by her friendliness, he does this, forgetting about the jewels he has come to steal, and is rewarded with a damson tartlet before escaping.


== References ==