Samson und Delila is an Austrian silent film, premiered in Vienna on 25 December 1922. It was released in the United Kingdom in October 1923 as Samson and Delilah. The film, the first to be made at the Rosenhügel Film Studios, which were still under construction at the time, was directed by Alexander Korda under the aegis of the Vita-Film company and was among the first epic films to be made in Austria.


== Cast ==
María Corda - Julia Sorel / Delila,
Franz Herterich - Prince Andrei Andrewiwitch/Abimelech
Ernst Arndt - Cassari, Impresario
Oskar Hugelmann - Ship's cook
Alfredo Boccolini - Samson
Franz Hauenstein - Ebn Ezra, Samson's friend
Paul Lukas - Ettore Ricco, tenorThe leading roles were played by Korda's wife, María Corda (Delila, and the opera singer Julia Sorel); Franz Herterich (Abimelech, King of the Philistines, and Prince Andrej Andrewiwitsch); Paul Lukas (Ettore Ricco, opera singer); and Ernst Arndt (Cassari, and theatre impresario). Samson himself, who features comparatively little, was played by Alfredo Galoar aka Alfredo Boccolini. Other parts were played by Oscar Hugelmann and Franz Hauenstein.


== Crew ==
The director was Alexander Korda on one of his earlier films, subcontracting to Vita-Film. He also contributed to the screenplay, on which other writers were Sydney Garrick and Ladislao Vajda. Cinematography and camera work were under Nicolas Farkas, Maurice Armand Mondet, and Josef Zeitlinger. Karl Hartl, at the beginning of his career, was assistant director and also editor. The set builder was Alexander Ferenczy and the special effects were provided by the pyrotechnician Oscar Wallenbach, while the costume design and wardrobe were provided by Remigius Geyling and Alexander Konja.


== Release dates ==
The film's German language release was in Vienna on 25 December 1922. It opened in Finland on 10 September 1923, in the UK in October 1923 and at Osaka in Japan on 18 June 1925. It was released in the United States in 1931, after being provided with a soundtrack.


== Critics ==
Reception was reasonably positive, although the actor playing Samson was not thought to be convincing. More serious criticism was aimed at the film's structure, which was not considered to tie the modern storyline and the Biblical storyline together sufficiently.


== References ==


== Sources ==
(in German)Filmportal.de: Samson und Delila
Samson und Delila on IMDb


== External links ==
Samson und Delila; Samson and Delilah on IMDb
Samson und Delila; Samson and Delilah at AllMovie
Samson and Delilah (1922) on YouTube