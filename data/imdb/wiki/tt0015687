The Clash of the Wolves is a 1925 American silent Western/adventure film produced and distributed by Warner Bros. Directed by Noel M. Smith, the film stars canine actor Rin Tin Tin, Charles Farrell and June Marlowe. It was filmed on location in Chatsworth, California, and at what would later become the Joshua Tree National Park. It was transferred onto 16mm film by Associated Artists Productions in the 1950s and shown on television. A 35mm print of the film was discovered in South Africa and restored in 2003. In 2004, The Clash of the Wolves was deemed "culturally, historically, or aesthetically significant" by the United States Library of Congress and selected for preservation in the National Film Registry.


== Plot ==
Lobo, wolfdog leader of a wolf pack, has a price on his head. One day suffering from a thorn in his paw, he is found by Dave Weston, a borax prospector and befriended. The animal returns love and loyalty. Later Lobo saves Dave from attacks of scheming villain William 'Borax' Horton, who has designs on Dave's claim. Once again the villain attacks the young prospector and leaves him for dead on the site of the claim. Lobo arrives and Dave sends him with a message to town for help. In the meantime a posse is hunting Lobo, but he manages to escape them and at the same time, decoy them to Dave. There, they learn that Lobo is man's best friend.


== Cast ==
Rin Tin Tin as Lobo
Nanette as Lobo's Mate
Charles Farrell as Dave Weston
June Marlowe as May Barstowe
Heinie Conklin as Alkali Bill
Will Walling as Sam Barstowe
Pat Hartigan as William 'Borax' Horton


== Reviews and reception ==
Michael L. Simmons wrote in the Exhibitors Trade Review, that "He (Rin-Tin-Tin) brings to the role of leader of a wolf-pack, an intelligence, a beauty of motion, an impressive cleverness that should find wide favor. He is a spectacle, in my opinion, well worth the price of admission." Simmons went on to say that "It is obvious throughout; every time the human cast stacks up alongside the exploits of the animal players, the latter stands out far ahead in the ability to compel interest." Motion Picture News reviewer George T. Pardy praised the performance of Rin-Tin-Tin, saying; "his work all through is extraordinary and far above that of his average doggish contemporaries in filmland...the thrills are many and pungent, mostly arising from the endeavors to trap or shoot Lobo of folks who know that there is a price set on the head of the kingly wolf." A review in The Film Daily was critical of the film stating, "No doubt the author is chiefly to blame for furnishing a script that is a mixture of dizzy melodrama, burlesque, caricature - anything in fact far removed from reality. Director Noel Smith struggled bravely with it. He deserves credit for getting over the dog sequences with a snap and a punch. The rest of the weak story seemed to have him licked."


=== Box Office ===
According to Warner Bros records the film earned $232,000 domestically and $38,000 foreign.


== Preservation status ==
A 35mm projection print of The Clash of the Wolves was found in South Africa and returned to the United States. It underwent restoration and preservation in 2003. Abridged and full versions survive in the Library of Congress Packard Campus for Audio-Visual Conservation.


== Accolades ==
In 2004, The Clash of the Wolves was deemed "culturally, historically, or aesthetically significant" by the United States Library of Congress and selected for preservation in the National Film Registry.


== References ==


== External links ==
The Clash of the Wolves essay by Susan Orlean at National Film Registry [1]
The Clash of the Wolves essay by Daniel Eagan in America's Film Legacy: The Authoritative Guide to the Landmark Movies in the National Film Registry, A&C Black, 2010 ISBN 0826429777, pages 107-108 [2]
The Clash of the Wolves on IMDb
The Story of Rin-Tin-Tin is available for free download at the Internet Archive
The Clash of the Wolves at the American Film Institute Catalog
The Clash of the Wolves at the TCM Movie Database
Lobby cards and still at silenthollywood.com