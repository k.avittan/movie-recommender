The type, functions, and characteristics of marriage vary from culture to culture, and can change over time. In general there are two types: civil marriage and religious marriage, and typically marriages employ a combination of both (religious marriages must often be licensed and recognized by the state, and conversely civil marriages, while not sanctioned under religious law, are nevertheless respected). Marriages between people of differing religions are called interfaith marriages, while marital conversion, a more controversial concept than interfaith marriage, refers to the religious conversion of one partner to the other's religion for sake of satisfying a religious requirement.


== Americas and  Europe ==
In the Americas and Europe, in the 21st century, legally recognized marriages are formally presumed to be monogamous (although some pockets of society accept polygamy socially, if not legally, and some couples choose to enter into open marriages). In these countries, divorce is relatively simple and socially accepted. In the West, the prevailing view toward marriage today is that it is based on a legal covenant recognizing emotional attachment between the partners and entered into voluntarily.
In the West, marriage has evolved from a life-time covenant that can only be broken by fault or death to a contract that can be broken by either party at will. Other shifts in Western marriage since World War I include:

There emerged a preference for maternal custody of children after divorce, as custody was more often settled based on the best interests of the child, rather than strictly awarding custody to the parent of greater financial means.
Both spouses have a formal duty of spousal support in the event of divorce (no longer just the husband)
Out of wedlock children have the same rights of support as legitimate children
In most countries, rape within marriage is illegal and can be punished
Spouses may no longer physically abuse their partners and women retain their legal rights upon marriage.
In some jurisdictions, property acquired since marriage is not owned by the title-holder. This property is considered marital and to be divided among the spouses by community property law or equitable distribution via the courts.
Marriages are more likely to be a product of mutual love, rather than economic necessity or a formal arrangement among families.
Remaining single by choice is increasingly viewed as socially acceptable and there is less pressure on young couples to marry. Marriage is no longer obligatory.
Interracial marriage is no longer forbidden.


== Asia and Africa ==

Key facts concerning the marriage law in Africa and Asia:

Marital rape is legal in most parts Africa and Asia alike.
Child marriage is legal in most parts of Africa and very few parts of Asia alike.
Arranged marriage  is prevalent in many parts of Africa and Asia alike, especially in rural regions.
Same-sex marriage is illegal in most parts of Africa and Asia alike.
Polygamy is legal in many parts of Africa and Asia, but tends to be illegal in most Communist countries and legal in most Muslim countries
Divorce is legal in all parts in Africa and Asia (except in the Philippines), but wives seeking divorce have fewer legal rights than husbands in Muslim countries than in Communist countries.
Dowries are a traditional aspect of marriage customs in most rural regions of Africa and Asia alike.Some societies permit polygamy, in which a man could have multiple wives; even in such societies however, most men have only one. In such societies, having multiple wives is generally considered a sign of wealth and power. The status of multiple wives has varied from one society to another.
In Imperial China, formal marriage was sanctioned only between a man and a woman, although among the upper classes, the primary wife was an arranged marriage with an elaborate formal ceremony while concubines could be taken on later with minimal ceremony. After the rise of Communism, only strictly monogamous marital relationships are permitted, although divorce is a relatively simple process.


== Monogamy, polyandry, and polygyny ==
Polyandry (a woman having multiple husbands) occurs very rarely in a few isolated tribal societies. These societies include some bands of the Canadian Inuit, although the practice has declined sharply in the 20th century due to their conversion from tribal religion to Christianity by Moravian missionaries. Additionally, the Spartans were notable for practicing polyandry.Societies which permit group marriage are extremely rare, but have existed in Utopian societies such as the Oneida Community.Today, many married people practice various forms of consensual nonmonogamy, including polyamory and Swinging. These people have agreements with their spouses that permit other intimate relationships or sexual partners. Therefore, the concept of marriage need not necessarily hinge on sexual or emotional monogamy.


=== Christian acceptance of monogamy ===
In the Christian society, a "one man one woman" model for the Christian marriage was advocated by Saint Augustine (354-439 AD) with his published letter The Good of Marriage. To discourage polygamy, he wrote it "was lawful among the ancient fathers: whether it be lawful now also, I would not hastily pronounce.  For there is not now necessity of begetting children, as there then was, when, even when wives bear children, it was allowed, in order to a more numerous posterity, to marry other wives in addition, which now is certainly not lawful." (chapter 15, paragraph 17) Sermons from St. Augustine's letters were popular and influential. In 534 AD Roman Emperor Justinian criminalized all but monogamous man/woman sex within the confines of marriage. The Codex Justinianus was the basis of European law for 1,000 years.
Several exceptions have existed for various Biblical figures, incestuous relationships such as Abraham and Sarah, Nachor and Melcha, Lot and his Daughters, Amram and Jochabed and more.Christianity for the past several years has continued to insist on monogamy as an essential of marriage.


=== Contemporary Western societies ===
In 21st century Western societies, bigamy is illegal and sexual relations outside marriage are generally frowned-upon, though there is a minority view accepting (or even advocating) open marriage.
However, divorce and remarriage are relatively easy to undertake in these societies. This has led to a practice called serial monogamy, which involves entering into successive marriages over time. Serial monogamy is also sometimes used to refer to cases where the couples cohabitate without getting married.


== Unique practices ==
Some parts of India follow a custom in which the groom is required to marry with an auspicious plant called Tulsi before a second marriage to overcome inauspicious predictions about the health of the husband. This also applies if the prospective wife is considered to be 'bad luck' or a 'bad omen' astrologically. However, the relationship is not consummated and does not affect their ability to remarry later.In the state of Kerala, India, the Nambudiri Brahmin caste traditionally practiced henogamy, in which only the eldest son in each family was permitted to marry. The younger children could have sambandha (temporary relationship) with Kshatriya or Nair women. This is no longer practiced, and in general the Nambudiri Brahmin men marry only from the Nambudiri caste and Nair women prefer to be married to Nair men. Tibetan fraternal polyandry (see Polyandry in Tibet) follows a similar pattern, in which multiple sons in a family all marry the same wife, so the family property is preserved; leftover daughters either become celibate Buddhist nuns or independent households. It was formerly practiced in Tibet and nearby Himalayan areas, and while it was discouraged by the Chinese after their conquest of the region, it is becoming more common again.In Mormonism, a couple may seal their marriage "for time and for all eternity" through a "sealing" ceremony conducted within LDS Temples. The couple is then believed to be bound to each other in marriage throughout eternity if they live according to their covenants made in the ceremony. Mormonism also allows living persons to act as proxies in the sealing ceremony to "seal" a marriage between ancestors who have been dead for at least one year and who were married during their lifetime. According to LDS theology, it is then up to the deceased individuals to accept or reject this sealing in the spirit world before their eventual resurrection. A living person can also be sealed to his or her deceased spouse, with another person (of the same sex as the deceased) acting as proxy for that deceased individual.One society that traditionally did without marriage entirely was that of the Na of Yunnan province in southern China. According to anthropologist Cia Hua, sexual liaisons among the Na took place in the form of "visits" initiated by either men or women, each of whom might have two or three partners each at any given time (and as many as two hundred throughout a lifetime). The nonexistence of fathers in the Na family unit was consistent with their practice of matrilineality and matrilocality, in which siblings and their offspring lived with their maternal relatives. In recent years, the Chinese state has encouraged the Na to acculturate to the monogamous marriage norms of greater China. Such programs have included land grants to monogamous Na families, conscription (in the 1970s, couples were rounded up in villages ten or twenty at a time and issued marriage licenses), legislation declaring frequent sexual partners married and outlawing "visits", and the withholding of food rations from children who could not identify their fathers. Many of these measures were relaxed in favor of educational approaches after Deng Xiaoping came into power in 1981. See also the Mosuo ethnic minority of China and their practice of walking marriage.


== See also ==
Civil union
Domestic partnership


== References ==


== External links ==
SEX AND MARRIAGE: An Introduction to The Cultural Rules Regulating Sexual Access and Marriage