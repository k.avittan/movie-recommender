Voyage to the Bottom of the Sea is a 1961 American science fiction disaster film, produced and directed by Irwin Allen, and starring Walter Pidgeon and Robert Sterling. The supporting cast includes Peter Lorre, Joan Fontaine, Barbara Eden, Michael Ansara, and Frankie Avalon. The film's storyline was written by Irwin Allen and Charles Bennett. The opening title credits theme song was sung by Frankie Avalon. The film was distributed by 20th Century Fox.


== Plot ==
The new, state of the art nuclear submarine, Seaview, is on diving trials in the Arctic Ocean. Seaview is designed and built by scientist and engineering genius Admiral Harriman Nelson (USN-Ret) (Walter Pidgeon). Captain Lee Crane (Robert Sterling) is the submarine's Commanding Officer. One of the on-board observers is Dr. Susan Hiller (Joan Fontaine), studying crew-related stress. The mission includes being out of radio contact for 96 hours while under the Arctic ice cap, but the ice begins to crack and melt, with boulder-size pieces crashing into the ocean around the submerged submarine; surfacing, they find the sky is on fire. After rescuing scientist Miguel Alvarez (Michael Ansara) and his dog at Ice Floe Delta, Seaview receives radio contact from Mission Director Inspector Bergan at the Bureau of Marine Exploration. He says that a meteor shower has pierced the Van Allen radiation belt, causing it to catch on fire, resulting in a world-threatening increase in heat all across the Earth. Nelson's on-board friend and scientist, retired Commodore Lucius Emery (Peter Lorre), concurs, saying that it is certainly possible. Bergan also tells Nelson that the President wants him at a UN Emergency Scientific Meeting as soon as possible.
Nelson and Commodore Emery devise a plan to end the catastrophe, and Seaview arrives in New York Harbor two days later. At the meeting, Nelson informs the UN that according to their calculations, if the heat increase is not stopped, it will become irreversible and that Earth has "a life expectancy of about three weeks". The Admiral and the Commander have come up with a plan to extinguish the fire. He proposes firing a nuclear missile at the burning belt from the best calculated location, in the Mariana Islands. Nelson posits that when fired from the right location and time, 1600 hours on 29 August, a nuclear explosion should overwhelm and extinguish the flames, away into space, "amputating" the belt from the Earth. Seaview, he advises, has the capability of firing the missile.
The Admiral's plan, however, is rejected by the chief scientist and head delegate, Vienna's Emilio Zucco (Henry Daniell). His reasons are that he knows the composition of gases in the belt and believes the sky fire will burn itself out at 173 degrees. Zucco's plan is to let the fire do just that; he feels the Admiral's plan is too risky. However, Nelson claims that Zucco's burn-out point is beyond that date and time, if the current temperature rise rate continues. At Zucco's urging, Nelson and Emery are shouted down and the plan is rejected. Despite their rejection, the Admiral and the Commodore quickly leave the proceedings; the only authorization they need will come directly from the President himself.
It is a race against the clock as Seaview speeds to reach the proper firing position, above the Pacific's Marianas trench. During this time, Nelson and Crane agree on tapping the Rio-to-London telephone cable in order to try to reach the President. However, an unsuccessful attempt on the Admiral's life makes it clear that there is a saboteur aboard. The confusion over who the saboteur might be revolves around both rescued scientist Miguel Alvarez, who has become a religious zealot regarding the catastrophe, and Dr. Hiller, who secretly admires Dr Zucco's plan. Other obstacles present themselves: a minefield and the crew's near-mutiny. Even Crane begins to doubt the Admiral's tactics and reasoning. During the cable tapping attempt, Crane and Alvarez battle a giant squid. Although the London cable connection is made, Nelson is told that there has been no contact with the U.S. for 35 hours. A hostile, unidentified submarine pursues them, diving deep into the Mariana Trench, exceeding its crush depth; the sub implodes before it can destroy Seaview.
The saboteur is revealed to be Dr. Hiller: Captain Crane happens by as she exits the ship's "Off Limits" Nuclear Reactor core, looking rather ill. Her detector badge has turned red, indicating that she has been exposed to a fatal dose of radiation. While struggling with the Captain above the submarine's populated shark tank, she falls in and is killed by the sharks. The Admiral learns that temperatures are rising faster than expected, confirming that Zucco's belief that the sky fire will burn itself out is in error.
Seaview reaches the Mariana Islands, and over Alvarez's threats and objections, a nuclear missile is launched toward the belt. It explodes, driving the burning flames away from the Earth and saving humanity.


== Cast ==
Walter Pidgeon as Admiral Harriman Nelson
Joan Fontaine as Dr. Susan Hiller
Barbara Eden as Lieutenant (JG) Cathy Connors
Peter Lorre as Commodore Lucius Emery
Robert Sterling as Captain Lee Crane
Michael Ansara as Miguel Alvarez
Frankie Avalon as Lieutenant (JG) Danny Romano
Regis Toomey as Dr. Jamieson
John Litel as Vice-Admiral B.J. Crawford
Howard McNear as Congressman Llewellyn Parker
Henry Daniell as Dr. Emilio Zucco
Robert Easton as 'Sparks'
Mark Slade as Seaman Jimmy 'Red' Smith
Charles Tannen as CPO Gleason
Del Monroe as Seaman Kowski
Jonathan Gilmore as Seaman George Young
Skip Ward as Crew Member


== Historical and technical background ==
The name of the film is an inversion of a phrase in common use at the time, concerning the exploration of the Arctic Ocean by nuclear submarines, namely, "a voyage to the top of the world".
From August 1, 1958, through August 5, 1958, USS Nautilus (the first nuclear-powered submarine), under the command of Commander (later Captain) William R. Anderson, steamed under the Arctic ice cap to make the first crossing from the Pacific to the Atlantic via the North Pole. On August 3, 1958, she became the first vessel to reach the North Pole.For this accomplishment, Nautilus and her crew were awarded the Presidential Unit Citation, the first Presidential Unit Citation awarded in peacetime. The citation began with the words, "For outstanding achievement in completing the first voyage in history across the top of the world, by cruising under the Arctic ice cap from the Bering Strait to the Greenland Sea".Nautilus 90 North (1959, with Clay Blair) was the first book Anderson wrote about the Arctic missions of USS Nautilus. It was named for the radio message he sent to the Chief of Naval Operations to announce that Nautilus had reached the pole. His second book about these missions, The Ice Diaries: The Untold Story of the Cold War's Most Daring Mission (with Don Keith), was completed shortly before Anderson's death. This second book includes many previously classified details.
On March 17, 1959, the nuclear submarine USS Skate, under the command of Commander (later Vice Admiral) James F. Calvert, became the first submarine to surface at the North Pole. While there, her crew scattered the ashes of Arctic explorer Sir Hubert Wilkins. Calvert wrote the book Surface at the Pole about this and the other Arctic missions of USS Skate.
The film Voyage to the Bottom of the Sea begins with Seaview in the Arctic for the final phase of her sea trials, including a dive under the ice cap.
Two milestones in underwater exploration were achieved in 1960, the year before Voyage to the Bottom of the Sea was released. On January 23, 1960, Jacques Piccard and Lieutenant Don Walsh (USN), in the bathyscaphe Trieste, made the first descent to the bottom of the Challenger Deep. The Challenger Deep is the deepest surveyed spot in the world's oceans, and is located in the Mariana Trench, southwest of Guam. From February 16, 1960, to May 10, 1960, the submarine USS Triton, under the command of Captain Edward L. Beach, Jr., made the first submerged circumnavigation of the world. Triton observed and photographed Guam extensively through her periscope during this mission, without being detected by the U.S. Navy on Guam.In the film, Seaview fires a missile from a position northwest of Guam to extinguish the "sky fire".
At the time that Voyage to the Bottom of the Sea was made, the Van Allen radiation belts had only recently been discovered, and much of what the film says about them was made up for the film. Discoveries since then clearly invalidate what the film says: the Van Allen belts (actually somewhat more radiation-dense portions of the magnetosphere) are made up of sub-atomic particles trapped by the Earth's magnetic field in the vacuum of space and cannot catch fire, as fire requires oxygen, fuel, and an ignition source, all of which are insufficient in the Van Allen Belts. Unburned hydrocarbon emissions have never reached concentrations that could support a "sky fire".


== The submarine Seaview ==

The film's submarine design is unique in that it features an eight-window bow viewport that provides panoramic undersea views. In the novelization by Theodore Sturgeon, the windows are described as "... oversized hull plates which happen to be transparent". They are made of "X-tempered herculite", a process developed by Nelson. In the film, Seaview has eight bow windows in the exterior shots, but only four appear in the interior shots showing the lower level Observation Room (the four upper windows are implied to be out of frame, at the top of the Observation Room). The lower hull also has an exterior shark-like bow flare, and the stern has 1961 Cadillac tail-fins.
In the film, the USOS Seaview (United States Oceanographic Survey) is under the authority of Nelson and the Bureau of Marine Exploration rather than the U.S. Navy. The novel mentions the bureau as being part of the U.S. Department of Science. The crew wears U.S. Army type dress uniforms with naval rank insignia rather than U.S. Navy uniforms.


== Production ==
The film was part of an upswing in science fiction and fantasy films of the era, including adaptations of Jules Verne's and H. G. Wells' works.The film marked Walter Pidgeon's return to filmmaking after some years committed to work in the theater. The role of Captain Crane was originally offered to David Hedison who played the role in the 1964 television series of the same name. Hedison turned the film down after completing Allen's The Lost World (1960) saying that he did not like the script.Set designer Herman Blumenthal did not approach the Navy to do research; he relied solely on pictures of naval vessels in the media.The theatrical release poster shown above is one of four posters that were produced to promote the film. Each one has different wording and slightly different artwork, and each one promotes the film from a different perspective. The above poster also promotes Sturgeon's novelization.


== Reception ==
Voyage to the Bottom of the Sea was previewed on June 18, 1961. It was released to theaters in early July 1961 and had run its course by late fall (September/October). The film played to mixed reviews from critics, but audiences made the film a popular success. Voyage to the Bottom of the Sea was made for US$2 million and brought in US$7 million at the box office.


== Impact ==
For the filming of Voyage to the Bottom of the Sea, a number of detailed sets, props and scale models were created to realize the Seaview submarine. After the film was finished the sets were placed in storage. When Irwin Allen decided to make a Voyage to the Bottom of the Sea television series, all he had to do was pull the sets out of storage. This was done at a fraction of the cost that he might have had if he had begun from scratch. The film reduced the cost of setting up the show and was the template for the type of stories that were done. The studios, having made the film, helped make the television series easier to produce.
The success of the television series encouraged Irwin Allen to produce other science fiction television shows. The most notable of these shows were Lost in Space, The Time Tunnel, and Land of the Giants.


== Other media ==


=== Television ===
The success of the feature film led to the 1964–1968 ABC television series, Voyage to the Bottom of the Sea. During the series run, the film's storyline was remade as a one-hour episode, written by Willam Welch, and titled "The Sky's on Fire". Other scenes from the film were also rewritten and incorporated into the television series.


=== Novels ===
In June 1961, Pyramid Books published a novelization of the film by science fiction writer Theodore Sturgeon. The book went on to be reprinted several times during the 1960s. One of those reprintings pictures Richard Basehart and David Hedison on the cover, but the book is still based on the Walter Pidgeon film. Collectors who want a novelization of the television series should find City Under the Sea. That novel uses the television characters, but should not be confused with either the television series or the later Irwin Allen film with the same name.
Sturgeon's book is based on an early version of the film's script and has the same basic story as the film. It also has a few characters that were not in the film, as well as some additional technical explanation. Some film scenes are different, while some others are wholly absent from the film. Likewise, some film scenes are missing from the book.
The original 1961 book cover portrays a submarine meeting a fanged sea serpent. This scene is not in the novel or the film. The submarine design on the cover does not match the Seaview shown in the film or the Seaview described in the novel: The submarine's bow is opaque, and her "Observation Room" is a rearward projection from the base of the conning tower. The basic shape of the Seaview's hull resembles that of the U.S. Navy's USS Skipjack, the first American nuclear-powered submarine with an "Albacore hull", including its cruciform stern and single-screw propeller.


=== Comics ===
A submarine design very similar to the one used on the 1961 book cover shows up in the 1962 Dell Comics series, Voyage to the Deep (with a similar mission to save the world), that was published to capitalize on the film's popularity. The submarine's mission took it to the Mariana Trench to stop the Earth from wobbling out of orbit. Dell later cancelled the title with issue #4. The submarine was named Proteus, later the name of the submersible seen in the science fiction film Fantastic Voyage (1966).
In 1961, Dell Comics created a full-color adaptation of the Voyage to the Bottom of the Sea film. The comic book was Four Color Comics #1230 and was illustrated by Sam Glanzman. It contains a few publicity stills from the film, plus a section on the history of submarines. In this adaptation, the Admiral's first name is Farragut instead of Harriman.
Mad magazine published a TV series parody entitled, "Voyage to See What's on the Bottom".


=== Other ===
There is also a family board game, manufactured by GemColor, that is tied to the film and not the television series. The game's product carton uses a photo of a wetsuited scuba diver with the eight-foot shooting miniature of the Seaview.
A Voyage to the Bottom of the Sea coloring book for children was released in the mid-1960s.
The film has since been released in multiple territories on VHS, DVD, and Blu-ray.


== See also ==
List of maritime science fiction works


== References ==


== Other sources ==
Tim Colliver, Seaview: The Making of Voyage to the Bottom of the Sea, 1992, Alpha Control press.
Voyage to the Bottom of the Sea (VHS)


== External links ==
Voyage to the Bottom of the Sea on IMDb
Voyage to the Bottom of the Sea at AllMovie
Voyage to the Bottom of the Sea at the TCM Movie Database
Voyage to the Bottom of the Sea at the American Film Institute Catalog
Voyage to the Bottom of the Sea at Box Office Mojo
Voyage to the Bottom of the Sea at Rotten Tomatoes
David Merriman's radio controlled model of the Seaview on YouTube
Mike's Voyage to the Bottom of the Sea Zone – the movie.