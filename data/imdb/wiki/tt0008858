Bachelor's Children is a domestic daytime drama broadcast that originated on Chicago's WGN in 1935–36, continuing on CBS and NBC until September 27, 1946.It followed the daily travails of two friends, Dr. Bob Graham and Sam Ryder, and the two women they loved. When Dr. Bob was a young man doing military service, his sergeant took care of him during a difficult time. Years later, the sergeant's dying request was that Bob become guardian of his two young daughters. Dr. Bob fell in love with Ruth Ann and Sam with her twin sister, Janet.
In a typical episode, Janet and Dr. Bob are together in his living room. Janet, who has been reading an art magazine, asks him if she can move one of his pictures to a place on the wall where it will get better light. Bringing a ladder from the pantry, he starts to help her. However, he has to leave when he receives a telephone call about a baby who has swallowed a button. Sam comes in to help Janet who is on the ladder. When she falls from the ladder, he catches her, and resting in his arms, she realizes for the first time that she has always loved him.
Hugh Studebaker had the role of Dr. Bob and Olan Soulé portrayed Sam Ryder. Also in the cast were Marjorie Hannan and Patricia Dunlap. Russ Young and Don Gordon were the announcers.


== Scripts and sponsors ==
The series was scripted by Bess McAllister Flynn, who lived in Des Plaines, Illinois during the 1920s and 1930s. In 1939, she reworked her plotlines into a book, Bachelor's Children: A Synopsis of the Radio Program, which summarized the story from the beginning to March 1, 1939. Published as a promotional product by Old Dutch Cleanser, the 25-page book included photographs of Bess Flynn, as well as the program's leading characters:  Dr. Bob, Ruth Ann, Janet, Ellen, Russ, Sam and the dog Rusty. The dedication read:

This story of Bachelor's Children is dedicated to the radio audience of America by the makers of Old Dutch Cleanser in the interest of better cleaning. The loyalty and friendly interest of this great audience is evidenced by the thousands upon thousands of letters we have received. We trust that Bachelor's Children will entertain you in the future as it has in the past, and that Old Dutch Cleanser will merit your continued use.On CBS, Bachelor's Children was sponsored by Old Dutch Cleanser from 1936 to 1941. In 1941–42, on NBC, it was sponsored by Colgate. Wonder Bread was the sponsor when the show returned to CBS from 1942 to 1946.


== Awards and honors ==
The program received the Movie-Radio Guide's 1941 Award as "radio's best daytime serial program," recognized as the "most representative script on the way of life of an average American family."  It was also "the only serial chosen by the Coordinator of Inter-American Affairs for transmission to Latin America."


== See also ==
List of radio soaps


== References ==


== Listen to ==
Bachelor's Children (September 21, 1939)
Bachelor's Children episodes on Old Time Radio Researchers Group website


== External links ==
Bachelor's Children episode log