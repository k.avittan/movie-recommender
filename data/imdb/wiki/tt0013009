Randolph Channing Crowder, Jr. (born December 2, 1983) is an American former college and professional football player who was a linebacker in the National Football League (NFL) for six seasons during the 2000s.  He played college football for the University of Florida, and was recognized as an All-American.  Crowder was drafted by the Miami Dolphins in the third round of the 2005 NFL Draft, and played his entire professional career for the Dolphins.  He co-hosted "The Kup & Crowder Show," weekdays from 1-3PM on 560 WQAM in Miami. He currently co-hosts "Hochman & Crowder" heard weekdays from 2-6PM on 560 WQAM.


== Early years ==
Crowder was born in State College, Pennsylvania, the son of All-American defensive lineman Randy Crowder and Pauline Pope-Crowder.  He was a highly regarded linebacker for North Springs High School in Sandy Springs, Georgia who garnered prep All-America recognition. He was named to SuperPrep's All-America Team and that publication ranked him among the nation's top 30 linebackers. He collected All-America honors from PrepStar and that publication ranked him among the Southeast's top 10 linebackers. He garnered All-America honors from Rivals.com and that group ranked him among the nation's top 15 outside linebackers. As a senior, he recorded 114 tackles in 2001 with 7.5 sacks sacks while also rushing for over 800 yards. He was named one of the top four linebackers in the state of Georgia by the Atlanta Journal-Constitution. He was team MVP in 2001.


=== Prep awards and honors ===
North Springs Spartans MVP (2001)
PrepStar All-American (2001)
Rivals.com All-American (2001)
SuperPrep All-American (2001)


== College career ==
Crowder accepted an athletic scholarship to attend the University of Florida in Gainesville, Florida, where he played for coach Ron Zook's Florida Gators football team in 2003 and 2004.  He was an original member of Florida's 2002 signing class, but delayed his enrollment until January 2003 in order to complete rehabilitation of his knee.  He joined the Gators and practiced with the team prior to the 2003 Outback Bowl.
As a freshman in 2003, Crowder played in 11 games with nine starts. Those nine starts were the most by a freshman linebacker in school history. He would end up leading the nation in tackles by a freshman during the season. He ranked first among linebackers, first among Florida's true freshmen and tied for ninth on the team with 816 plays despite missing two games. He became the first true freshman of 2003 to start for the Gators and was the first true freshman linebacker to start since Travis Harris started four games in 2000. Crowder underwent arthroscopic knee surgery on October 1 prior to the Mississippi game, and missed the game against the Rebels. He returned against eventual national champion LSU 10 days after surgery to lead the linebacking unit with seven tackles.
On the year, Crowder led the linebackers with 106 tackles (avg. 9.6 per game) - the second-highest total on the team. He also had five tackles for loss, two sacks, four pass breakups and a forced fumble. He led the defense with 12 tackles and one pass deflection against Arkansas, and led the defense against Vanderbilt with a then career-high 15 tackles. Crowder caused Kentucky quarterback Jared Lorenzen to throw a game-turning interception, that set up a touchdown and gave Florida the win.
As a team captain in 2004, he finished third on team with 73 tackles, earned the team's outstanding linebacker award. Crowder was the Gators' leader in tackles six games, and was third on squad with 8.5 tackles for loss. He intercepted his first career pass against Arkansas when he picked off a Matt Jones pass and returned if 22 yards in a 45-30 win. Crowder recorded a career-high two quarterback sacks against JaMarcus Russell of LSU. He notched an SEC and career-best 18 tackles and forced the second fumble of his career against Mississippi State. On the year, he started seven of nine games before a mid-foot sprain in the first quarter of the Georgia game and missed almost all of the next three contest. He recovered his third career fumble and notched three tackles in the Peach Bowl versus Miami.
Following his second college season, Crowder was recognized as a first-team All-SEC selection and an ESPN All-American.


=== Awards and honors ===
College Football News Freshman of the Year (2003)
Knoxville News-Sentinel All-Freshman Team (2003)
Rivals.com Freshman All-American (2003)
Scripps/FWAA Freshman All-American (2003)
Sporting News All-Freshman Team (2003)
SEC All-Freshman Team (2003)
Sporting News SEC Defensive Freshman of the Year (2003)
Associated Press first-team All-SEC (2003)
Coaches second-team All-SEC (2003)
ESPN first-team All-American (2004)
Coaches first-team All-SEC (2004)
Associated Press second-team All-SEC (2004)
Florida Gators Defensive Co-Captain (2004)
Florida Gators Outstanding Linebacker (2004)


== Professional career ==
Crowder was originally drafted by the Miami Dolphins in the third round (70th overall) in the 2005 NFL Draft. The pick used to select him was previously acquired from the Chicago Bears in the Marty Booker-Adewale Ogunleye trade from the year before. Then-head coach Nick Saban was familiar with Crowder, having coached in the SEC at LSU before coming to Miami.

Crowder signed a four-year contract with the team in July and went on to start 13 of the 16 games in which he played during his rookie season. He opened 11 games at the weakside spot and two in the middle. The Dolphins opened with either five or six defensive backs in the only three games he did not start. Along with running back Ronnie Brown and cornerback Travis Daniels, 2005 marked the first time since 1996 that three or more Dolphin rookies opened at least ten games. Crowder ranked second on the team with 90 tackles, the highest total by a Dolphins rookie since Zach Thomas tallied 164 in 1996. It also was the fifth-highest total among NFL rookies in 2005. Crowder added two fumble recoveries, two forced fumbles and four passes defensed on the year. He replaced an injured Zach Thomas (shoulder/ankle) at middle linebacker for games during the season. During the year, he posted a season-high 10 tackles on two occasions. Crowder collected seven tackles against the Tampa Bay Buccaneers on October 16 and also recovered an Earnest Graham fumble that led to a Ronnie Brown eight-yard TD run on the next play from scrimmage. Crowder prevented a potential touchdown against the Atlanta Falcons when he forced the ball loose from fullback Justin Griffith with the Falcons at the Dolphins' 8 yard-line, and was recovered by Travis Daniels. Crowder also recovered a J. P. Losman fumble against the Buffalo Bills on December 4.
Crowder was once again a reliable starter in 2006 and continued to progress as a player. He finished second on the Dolphins with 104 tackles (behind only Zach Thomas) and had one sack for nine yards in losses that season. It was Crowder's first 100-tackle season of his career. He had four games of double-figure tackles, and tied or led the team in tackles three times and finished second on the team in tackles six times during the season. He started all 16 games during the regular season, though he suffered a hip injury on November 19 against the Minnesota Vikings that forced him from that contest permanently. In week 12 of the 2008 season against the New England Patriots, he was ejected from the game after engaging in a fight with Patriots offensive tackle Matt Light.
An unrestricted free agent in the 2009 offseason, Crowder was re-signed to a three-year contract by the Dolphins on February 25, 2009. On December 6, 2009, In a matchup against the New England Patriots, Crowder made his first career interception and also sealed the win off for Miami. Crowder was placed on injured reserve on December 30. He suffered a right foot injury which made him miss a game versus the Houston Texans.
On July 29, 2011, the Dolphins released Crowder and then signed LB Kevin Burnett from the Chargers.  Just over a week later on August 9, 2011, Crowder abruptly announced his retirement from the NFL.On February 1, 2012 Crowder appeared on the Super Bowl’s Radio Row and announced his intention to restart his pro career.


== Personal ==
Crowder is the son of former NFL defensive lineman Randy Crowder, who was a sixth-round pick by the Dolphins in 1974.
On January 23, 2008, Crowder lost control of his truck on a wet exit ramp and crashed it into a tree.  He was unharmed and was charged with careless driving and leaving the scene of a crash.Prior to the October 27, 2007 New York Giants–Miami Dolphins game in London, England, Crowder reportedly stated he "couldn't find London on a map".  He later implied that he did not believe there were any black people in London as well.In a 2012 interview with NFL Films, Crowder confessed to urinating in his pants "every game" while in the NFL.On June 27, 2011 Crowder intimated on his new radio show that he had sold his jerseys while playing college football for the Florida Gators.  Crowder later reported that he had not sold his jerseys.
He was a commentator on WQAM during the 2011 NFL season.  His midday show on WQAM with NBC 6 news anchor, Adam Kuperstein was recently cancelled. He co-hosted "Hochman, Crowder & Krantz," weekdays from 2-6PM on WQAM, May 2017 fellow co-host Zach Krantz left. 
Crowder now co-hosts the Hochman and Crowder show heard from 2-6 pm on 790 the Ticket and on 560 the Joe WQAM.


== See also ==

2004 College Football All-America Team
List of Florida Gators football All-Americans
List of Florida Gators in the NFL Draft
List of Miami Dolphins players


== References ==


== External links ==
Channing Crowder – Florida Gators player profile
Channing Crowder – Miami Dolphins player profile