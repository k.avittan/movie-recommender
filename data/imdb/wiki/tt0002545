Twilight on Earth is the illumination of the lower atmosphere when the Sun is not directly visible because it is below the horizon. Twilight is produced by sunlight scattering in the upper atmosphere, illuminating the lower atmosphere so that Earth's surface is neither completely lit nor completely dark.  The word twilight is also used to denote the periods of time when this illumination occurs.The lower the Sun is beneath the horizon, the dimmer the twilight (other factors such as atmospheric conditions being equal). When the Sun reaches 18° below the horizon, the twilight's brightness is nearly zero, and evening twilight becomes nighttime. When the Sun again reaches 18° below the horizon, nighttime becomes morning twilight. Owing to its distinctive quality, primarily the absence of shadows and the appearance of objects silhouetted against the lit sky, twilight has long been popular with photographers, who sometimes refer to it as "sweet light", and painters, who often refer to it as the blue hour, after the French expression l'heure bleue.
Twilight should not be confused with auroras, which can have a similar appearance in the night sky at high latitudes.
By analogy with evening twilight, the word twilight is also sometimes used metaphorically, to imply that something is losing strength and approaching its end. For example, very old people may be said to be "in the twilight of their lives". The collateral adjective for twilight is crepuscular, which may be used to describe the behavior of animals that are most active during this period.


== Definitions by geometry ==
Twilight is defined according to the solar elevation angle θs, which is the position of the geometric center of the sun relative to the horizon. There are three established and widely accepted subcategories of twilight: civil twilight (nearest the horizon), nautical twilight, and astronomical twilight (farthest from the horizon).


== Definitions and illustrations by illumination ==

Three subcategories of twilight are established and widely accepted: civil twilight (brightest), nautical twilight, and astronomical twilight (darkest).


== Civil twilight ==
Civil twilight is the period when enough natural light remains that artificial light is not needed.
Morning civil twilight begins when the geometric center of the sun is 6° below the horizon and ends at sunrise. Evening civil twilight begins at sunset and ends  when the geometric center of the sun reaches 6° below the horizon. In the United States' military, the initialisms BMCT (begin morning civil twilight, i.e. civil dawn) and EECT (end evening civil twilight, i.e. civil dusk) are used to refer to the start of morning civil twilight and the end of evening civil twilight, respectively. Civil dawn is preceded by morning nautical twilight and civil dusk is followed by evening nautical twilight.
Under clear weather conditions, civil twilight approximates the limit at which solar illumination suffices for the human eye to clearly distinguish terrestrial objects. Enough illumination renders artificial sources unnecessary for most outdoor activities.  At civil dawn and at civil dusk sunlight clearly defines the horizon while the brightest stars and planets can appear.  As observed from the Earth (see apparent magnitude), sky-gazers know Venus, the brightest planet, as the "morning star" or "evening star" because they can see it during civil twilight.Lawmakers have enshrined the concept of civil twilight.  Such statutes typically use a fixed period after sunset or before sunrise (most commonly 20–30 minutes), rather than how many degrees the sun is below the horizon. Examples include the following periods: when drivers of automobiles must turn on their headlights (called lighting-up time in the UK); when hunting is restricted; when the crime of burglary is to be treated as nighttime burglary, which carries stiffer penalties in some jurisdictions.
The period may affect when extra equipment, such as anti-collision lights, is required for aircraft to operate.  In the US, civil twilight for aviation is defined in Part 1.1 of the Federal Aviation Regulations (FARs) as the time listed in the American Air Almanac.


== Nautical twilight ==


=== Definition ===
Morning nautical twilight (nautical dawn) begins when the geometric center of the sun is 12 degrees below the horizon in the morning and ends when the geometric center of the sun is 6 degrees below the horizon in the morning.
Evening nautical twilight begins when the geometric center of the sun is 6 degrees below the horizon in the evening and ends (nautical dusk) when the geometric center of the sun is 12 degrees below the horizon in the evening.


=== Nautical dawn and dusk ===
Nautical dawn is the moment when the geometric center of the Sun is 12 degrees below the horizon in the morning. It is preceded by morning astronomical twilight and followed by morning civil twilight. Nautical dusk is the moment when the geometric center of the Sun is 12 degrees below the horizon in the evening. It marks the beginning of evening astronomical twilight and the end of evening civil twilight.

Before nautical dawn and after nautical dusk, sailors cannot navigate via the horizon at sea as they cannot clearly see the horizon. At nautical dawn and nautical dusk, the human eye finds it difficult, if not impossible, to discern traces of illumination near the sunset or sunrise point of the horizon (first light after nautical dawn but before civil dawn and nightfall after civil dusk but before nautical dusk).Sailors can take reliable star sightings of well-known stars, during the stage of nautical twilight when they can distinguish a visible horizon for reference (i.e. after astronomic dawn or before astronomic dusk).  
Under good atmospheric conditions with the absence of other illumination, during nautical twilight, the human eye may distinguish general outlines of ground objects but cannot participate in detailed outdoor operations.Nautical twilight has military considerations as well. The initialisms BMNT (begin morning nautical twilight, i.e. nautical dawn) and EENT (end evening nautical twilight, i.e. nautical dusk) are used and considered when planning military operations. A military unit may treat BMNT and EENT with heightened security, e.g. by "standing to", in which everyone assumes a defensive position. This is partially due to tactics dating back to the French and Indian War (part of the Seven Years' War of 1756–1763), when combatants on both sides would launch attacks at nautical dawn or dusk.


== Astronomical twilight ==


=== Astronomical dawn and dusk ===
Astronomical dawn is the moment when the geometric center of the Sun is 18 degrees below the horizon in the morning. Astronomical dusk is the moment when the geometric center of the Sun is 18 degrees below the horizon in the evening. After astronomical dusk and before astronomical dawn, the sky is not illuminated by the sun.


==== Definition ====
Morning astronomical twilight begins (astronomical dawn) when the geometric center of the sun is 18° below the horizon in the morning and ends when the geometric center of the sun is 12° below the horizon in the morning. Evening astronomical twilight begins when the geometric center of the sun is 12° below the horizon in the evening and ends (astronomical dusk) when the geometric center of the sun is 18° below the horizon in the evening.
In some places (away from urban light pollution, moonlight, auroras, and other sources of light), where the sky is dark enough for nearly all astronomical observations, astronomers can easily make observations of point sources such as stars both during and after astronomical twilight in the evening and both before and during astronomical twilight in the morning. However, some critical observations, such as of faint diffuse items such as nebulae and galaxies, may require observation beyond the limit of astronomical twilight.  Theoretically, the faintest stars detectable by the naked eye (those of approximately the sixth magnitude) will become visible in the evening at astronomical dusk, and become invisible at astronomical dawn.

However, in other places, especially those with skyglow, astronomical twilight may be almost indistinguishable from night. In the evening, even when astronomical twilight has yet to end and in the morning when astronomical twilight has already begun, most casual observers would consider the entire sky fully dark.  Because of light pollution, observers in some localities, generally in and near large cities, may never have the opportunity to view anything but the brightest stars, irrespective of the presence of any twilight at all, nor to experience anything close to a truly dark sky.


== Times of occurrence ==


=== Between day and night ===
Observers within 48°34' of the Equator (within 50° of the Equator in May or November, within 57° of the Equator in April or October) can view twilight twice each day on every date of the year between astronomical dawn, nautical dawn, or civil dawn, and sunrise as well as between sunset and civil dusk, nautical dusk, or astronomical dusk. This also occurs for most observers at higher latitudes on many dates throughout the year, except those around the summer solstice. However, at latitudes closer than 9 degrees (between 81° and 90°) to either Pole, the Sun cannot rise above the horizon nor sink more than 18 degrees below it on the same day on any date, this example of twilight cannot occur because the angular difference between solar noon and solar midnight elevates less than 18 degrees.


=== Lasting from one day to the next ===
At latitudes greater than 48°34' North or South, on dates near the summer solstice, twilight can last from sunset to sunrise, since the Sun does not go more than 18 degrees below the horizon, so complete darkness does not occur even at solar midnight. These latitudes include many densely populated regions of the Earth, including the entire United Kingdom and other countries in northern Europe and even parts of central Europe.

Civil Twilight: between 60°34' and 65°44' North or South (between 62° and 67°10' North or South in May or November, between 69° and 74°10' North or South in April or October). In the northern hemisphere, includes the center of Iceland, Finland, Sweden, Norway, and Faroe Islands. In the southern hemisphere includes the Southern Ocean but at latitude smaller than 65°44' South. All night of civil twilight is also referred to white night.
Nautical Twilight: between 54°34' and 60°34' North or South (between 56° and 62° North or South in May or November, between 63° and 69° North or South in April or October). In the northern hemisphere, includes the center of Russia, Canada, Estonia, Latvia, Lithuania, and Denmark. In the southern hemisphere includes the southernmost point of South America, and Ushuaia in Argentina. All night of nautical twilight does not constitute a white night.
Astronomical Twilight: between 48°34' and 54°34' North or South (between 50° and 56° North or South in May or November, between 57° and 63° North or South in April or October). In the northern hemisphere, includes the center of Isle of Man, United Kingdom, Belarus, Ireland, Netherlands, Poland, Germany, Belgium, Czech Republic, Luxembourg, Guernsey, Ukraine, and Slovakia. In the southern hemisphere includes the center of South Georgia and the South Sandwich Islands, Bouvet Island, Heard Island, Falkland Islands. It also includes El Calafate and Río Gallegos in Argentina, and Puerto Natales in Chile. All night of astronomical twilight does not constitute a white night.


=== Between one night and the next ===
In Arctic and Antarctic latitudes in wintertime, the polar night only rarely produces complete darkness for 24 hours each day. This can occur only at locations within 5.5 degrees of latitude of the Pole, and there only on dates close to the winter solstice. At all other latitudes and dates, the polar night includes a daily period of twilight, when the Sun is not far below the horizon. Around winter solstice, when the solar declination changes slowly, complete darkness lasts several weeks at the Pole itself, e.g., from May 11 to July 31 at Amundsen–Scott South Pole Station. North Pole has the experience of this from November 13 to January 29.
Solar Noon at Civil Twilight during a polar night: Between 67°24' and 72°34' North or South.
Solar Noon at Nautical Twilight during a polar night: Between 72°34' and 78°34' North or South.
Solar Noon at Astronomical Twilight during a polar night: Between 78°34' and 84°34' North or South.
Solar Noon at Night during a polar night: Between 84°34' and 90° North or South.


=== Lasting for 24 hours ===
At latitudes within 9 degrees of either Pole, as the sun's angular elevation difference is less than 18 degrees, twilight can last for the entire 24 hours. This occurs for one day at latitudes near 9 degrees from the Pole and extends up to several weeks the further towards the Pole one goes. The only permanent settlement to experience this condition is Alert, Nunavut, Canada, where it occurs for a week in late February, and again in late October.


== Duration ==

The duration of twilight depends on the latitude and the time of the year. The apparent travel of the sun occurs at the rate of 15 degrees per hour (360° per day), but sunrise and sunset happen typically at oblique angles to the horizon and the actual duration of any twilight period will be a function of that angle, being longer for more oblique angles. This angle of the sun's motion with respect to the horizon changes with latitude as well as the time of year (affecting the angle of the Earth's axis with respect to the sun).
At Greenwich, England (51.5°N), the duration of civil twilight will vary from 33 minutes to 48 minutes, depending on the time of year. At the equator, civil twilight can last as little as 24 minutes. This is true because at low latitudes the sun's apparent movement is perpendicular to the observer's horizon. But at the poles, civil twilight can be as long as 2–3 weeks. In the Arctic and Antarctic regions, twilight (if there is any) can last for several hours. There is no astronomical twilight at the poles near the winter solstice (for about 74 days at the North Pole and about 80 days at the South Pole). As one gets closer to the Arctic and Antarctic circles, the sun's disk moves toward the observer's horizon at a lower angle. The observer's earthly location will pass through the various twilight zones less directly, taking more time.
Within the polar circles, twenty-four-hour daylight is encountered in summer, and in regions very close to the poles, twilight can last for weeks on the winter side of the equinoxes. Outside the polar circles, where the angular distance from the polar circle is less than the angle which defines twilight (see above), twilight can continue through local midnight near the summer solstice. The precise position of the polar circles, and the regions where twilight can continue through local midnight, varies slightly from year to year with Earth's axial tilt. The lowest latitudes at which the various twilights can continue through local midnight are approximately 60.561° (60°33′43″) for civil twilight, 54.561° (54°33′43″) for nautical twilight and 48.561° (48°33′43″) for astronomical twilight.Lowest latitude can be observed at local midnight in month of April:

Civil Twilight: 69°N
Nautical Twilight: 63°N
Astronomical Twilight: 57°NLowest latitude can be observed at local midnight in month of May:

Civil Twilight: 62°N
Nautical Twilight: 56°N
Astronomical Twilight: 50°NLowest latitude can be observed at local midnight in month of October:

Civil Twilight: 69°S
Nautical Twilight: 63°S
Astronomical Twilight: 57°SLowest latitude can be observed at local midnight in month of November: 

Civil Twilight: 62°S
Nautical Twilight: 56°S
Astronomical Twilight: 50°SThese are the largest cities of their respective countries where the various twilights can continue through local solar midnight:

Civil twilight from sunset to sunrise: Tampere, Umeå, Trondheim, Tórshavn, Reykjavík, Nuuk, Whitehorse, Anchorage, Arkhangelsk and Baltasound. In the Southern Hemisphere, there are no major permanent settlements far enough South to experience this.
Nautical twilight from civil dusk to civil dawn: Saint Petersburg, Moscow, Vitebsk, Vilnius, Riga, Tallinn, Wejherowo, Flensburg, Helsinki, Stockholm, Copenhagen, Oslo, Newcastle upon Tyne, Edinburgh, Glasgow, Belfast, Letterkenny, Petropavl, Grande Prairie, Juneau, Ushuaia, and Puerto Williams.
Astronomical twilight from nautical dusk to nautical dawn: Hulun Buir, Erdenet, Nur-Sultan, Samara, Kyiv, Minsk, Warsaw, Košice, Paris, Dublin, Zwettl, Prague, Stanley (Falkland Islands), Berlin, Hamburg, Luxembourg City, Brussels, Amsterdam, London, Cardiff, Vancouver, Calgary, Unalaska, Bellingham (largest in the continental USA), Rio Gallegos, and Punta Arenas.
Major cities that near astronomical twilight from nautical dusk to nautical dawn: Saguenay (48°25′0"N), Brest (48°23′26"N), Thunder Bay (48°22′56″N),  Vienna (48°12′30″N), Bratislava (48°8′38″N), Munich (48°8'0"N)Although Helsinki, Oslo, Stockholm, Tallinn, and Saint Petersburg also enter into nautical twilight after sunset, they do have noticeably lighter skies at night during the summer solstice than other locations mentioned in their category above, because they do not go far into nautical twilight. A white night is a night with only civil twilight which lasts from sunset to sunrise.At the winter solstice within the polar circle, twilight can extend through solar noon at latitudes below 72.561° (72°33′43″) for civil twilight, 78.561° (78°33′43″) for nautical twilight, and 84.561° (84°33′43″) for astronomical twilight.


== On other planets ==
Twilight on Mars is longer than on Earth, lasting for up to two hours before sunrise or after sunset. Dust high in the atmosphere scatters light to the night side of the planet. Similar twilights are seen on Earth following major volcanic eruptions.


== In religion ==


=== Christianity ===

In Christian practice, "vigil" observances often occur during twilight on the evening before major feast days or holidays. For example, the Easter Vigil is held in the hours of darkness between sunset on Holy Saturday and sunrise on Easter Day — most commonly in the evening of Holy Saturday or midnight — and is the first celebration of Easter, days traditionally being considered to begin at sunset.


=== Hinduism ===
Twilight is sacred in Hinduism. It is called गोधूळिवेळ gōdhūḷivēḷ in Marathi or गोधूलिवेला godhūlivelā in Hindi, గొధూళివేళ "godhoolivela" in Telugu, literally "cow dust time". Many rituals, including Sandhyavandanam and Puja, are performed at twilight hour. Eating of food is not advised during this time. Sometimes it is referred to as Asurasandhya vela. It is believed that Asuras are active during these hours. One of the avatars of Lord Vishnu, Narasimha, is closely associated with the twilight period. According to Hindu scriptures, a daemonic king, Hiranakashipa, performed penance and obtained a boon from Brahma that he could not be killed during day or night and neither by human nor animal. Lord Vishnu appeared in a half-man half-lion form (neither human nor animal), ended the life of Hiranakashipa during twilight (neither day nor night).


=== Islam ===
Twilight is important in Islam as it determines when certain universally obligatory prayers are to be recited. Morning twilight is when morning prayers (Fajr) are done, while evening twilight is the time for evening prayers (Maghrib prayer). Suhoor (early morning meal) time for fasting (during Ramadan and other times) ends at morning twilight (at dawn). Fasting is from the beginning of dawn to sunset. There is also an important discussion in Islamic jurisprudence between "true dawn" and "false dawn".


=== Judaism ===
In Judaism, twilight is considered neither day nor night; consequently it is treated as a safeguard against encroachment upon either. For example, the twilight of Friday is reckoned as Sabbath eve, and that of Saturday as Sabbath day; and the same rule applies to festival days.


== Gallery ==

		
		
		
		


== See also ==
 The dictionary definition of twilight at Wiktionary
 The dictionary definition of gloaming at Wiktionary
Belt of Venus
Earth's shadow, visible at twilight
Gloom
Green flash
Polar night


== References ==


== Footnotes ==


== Further reading ==
Mateshvili, Nina; Didier Fussen; Filip Vanhellemont; Christine Bingen; Erkki Kyrölä; Iuri Mateshvili; Giuli Mateshvili (2005). "Twilight sky brightness measurements as a useful tool for stratospheric aerosol investigations". Journal of Geophysical Research. 110 (D09209): D09209. Bibcode:2005JGRD..11009209M. doi:10.1029/2004JD005512.


== External links ==
Twilight Calculator Compute twilight times.
Twilight time calculator
Formulae to calculate twilight duration (archived) by Herbert Glarner.
The colors of twilight and sunset
HM Nautical Almanac Office Websurf Compute twilight times.
Geoscience Australia "Sunrise and sunset times" Compute twilight times.
An Excel workbook with VBA functions for twilight (dawn and dusk), sunrise, solar noon, sunset, and solar position (azimuth and elevation) by Greg Pelletier, translated from NOAA's online calculator for sunrise/sunset
Time and Date to find the current state of the sun in a specified place.