Billy Ray Butler (born William Raymond Butler, Jr.; April 18, 1986), nicknamed "Country Breakfast", is an American former professional baseball designated hitter and first baseman. He played in Major League Baseball (MLB) for the Kansas City Royals from 2007 to 2014, the Oakland Athletics from 2015 to 2016 and the New York Yankees in 2016. Butler was an MLB All-Star in 2012, and won the Silver Slugger Award and Edgar Martínez Award that season.


== Amateur career ==
Butler attended Wolfson High School in Jacksonville, Florida, where he was teammates with fellow first-round draft pick Eric Hurley, a pitcher selected by the Texas Rangers.  He was selected by the Kansas City Royals in the first round (14th overall) of the 2004 Major League Baseball Draft and signed for a $1.45 million bonus, $250,000 below the recommended slot value for that pick. He turned down a scholarship to play for the University of Florida, electing to play professional baseball.


== Minor league career ==
Butler made his professional debut with the Idaho Falls Chukars in the Rookie-level Pioneer League, where he batted .373 with 10 home runs and 68 RBIs in 73 games. He was the Kansas City Royals Minor League Player of the Year, a Pioneer League All-Star and a Rookie League All-Star. The next year, Butler was the California League Rookie of the Year, a 2005 High A All-Star, Baseball America 1st team Minor League All-Star, Kansas City Royals Minor League Player of the Year, California League All-Star and the California/Carolina League All-Star Game MVP.
In 2006, Butler led the Texas League with a .331 batting average. Despite taking time out to play for the U.S. national baseball team, Butler amassed 96 RBIs while playing right field for the Double-A Wichita Wranglers, while leading the Texas League in double plays grounded into with 25. A third baseman in high school, Butler played right field and left field for Wichita in 2006. He was also selected to play for the United States squad in the 2006 All-Star Futures Game, where he hit the go-ahead home run and was selected game MVP.


== Major League Baseball career ==


=== Kansas City Royals ===


==== 2007 ====
Butler made his major league debut on May 1, 2007, singling in his first at bat, but was sent to the minors after playing 10 games. He was recalled on June 19 after the Royals put slugger Mike Sweeney on the disabled list. Butler batted .292 in his rookie year with 8 home runs and 52 RBI.


==== 2008 ====
In Butler's sophomore season, he hit .275, with an on-base percentage of .324 and a slugging percentage of .400. He was demoted mid-season to Triple-A Omaha, but later recalled. In 443 plate appearances (118 more than his rookie year), he recorded 11 HR and 55 RBI.


==== 2009 ====
In 2009, Butler batted .301, while surpassing 500 at bats for the first time in his career. He solidified himself as the Royals' number three hitter with his emerging gap power, and improved discipline at the plate. Butler led the Royals with career highs in RBI (92) and doubles (51), nearly eclipsing Hal McRae's team record of 54. He also had a career high in home runs, with 21. Only five players had hit at least 20 home runs and 50 doubles in a season by his age. Butler was named the American League Player of the Week two times (August 2–8 and September 7–13).  He also played almost all of the 2009 season at first base, whereas before he was almost exclusively the designated hitter. He led all AL first basemen in errors, with 10.He was the American League Player of the Month for September. He was named the 2009 Royals Player of the Year.


==== 2010 ====
The 2010 season saw a slight drop in Butler's power numbers, dropping from 21 home runs and 92 RBI to 15 home runs and 78 runs batted in, respectively. However, he also posted a .318 batting average, the highest of his career to that point. His .388 on-base percentage was also a career best. He also hit 45 doubles. He grounded into 32 double plays, the most in the American League.  Butler spent the 2010 season playing primarily first base, where he was 5th among all AL first basemen in errors, with 6.


==== 2011 ====
On January 23, 2011, Butler agreed to a $30 million, four-year contract with the Royals.In 2011 season, the promotion of first baseman Kila Ka'aihue from Triple-A Omaha meant that Butler would spend time alternating between first base and designated hitter to open the 2011 campaign. However, with Ka'aihue's demotion in May, Eric Hosmer was called up from Triple-A Omaha and became the team's everyday first baseman, with Butler becoming the full-time DH. On July 26, Butler was a triple short of a cycle in a loss to the Boston Red Sox. Butler followed with four home runs in the next three games. 
Butler won the 2011 Hutch Award.
 He was the American League Player of the Week for July 25–31.


==== 2012 ====
On July 1, 2012, Butler was selected to his first All-Star Game as a reserve on the American League roster. No Royals player was selected by fan or player vote but American League All-Star Team manager Ron Washington made Butler one of his seven selections. At the time of his selection, Butler was hitting .297 with 16 home runs and 48 RBIs. He won the American League Player of the Week Award for the week ending on July 29. After the season, he was named a 2012 American League Silver Slugger DH and received the 2012 Edgar Martinez Outstanding Designated Hitter Award.


==== 2013 ====
On May 28, 2013, Butler got his 1,000th hit in the first inning, bringing in Alex Gordon against St. Louis Cardinals' rookie pitcher Tyler Lyons. He played in all 162 games, hit 15 home runs and drove in 82 runs while drawing a career high 79 walks. He had a slash line of .289/.374/.412. He grounded into 28 double plays, the most in the American League.


==== 2014 ====
Butler had one of the worst statistical years of his career in 2014, hitting only nine home runs with 66 RBI. His batting average, on base percentage, and slugging were all the lowest of his career. Leading into the World Series, Butler was batting .217 with 5 RBI in eight postseason contests. Following the season, the Royals declined Butler's club option, making him a free agent.


=== Oakland Athletics ===
On November 18, 2014, Butler signed a 3-year, $30 million contract with the Oakland Athletics. In 2015, Butler batted a career-low .251 with a .390 slugging percentage.Butler was involved in a clubhouse fight with Danny Valencia on August 19, 2016. He was placed on the seven-day concussion list on August 22 as a result. Both players were fined, but neither were suspended.  Two weeks later, on September 11, 2016, the Athletics released Butler from his contract. 
He batted .276 with four home runs and 31 RBIs in 85 games over the year.


=== New York Yankees ===
Butler signed with the New York Yankees on September 15, 2016 after a season-ending injury to Aaron Judge. He played in a dozen games, and was not re-signed after the season by the Yankees or signed by any other team, though he was just 30 years old.


=== Career statistics ===
In 1414 games over 10 seasons, Butler posted a .290 batting average (1479-for-5105) with 592 runs, 322 doubles, 5 triples, 147 home runs, 728 RBI, 500 bases on balls, .354 on-base percentage and .441 slugging percentage. He finished his career with a .992 fielding percentage as a first baseman. In 13 postseason games, he hit .262 (11-for-42) with 3 runs, 3 doubles, 8 RBI and 5 walks.


== Personal life ==
He is married to Katie Hansen Butler and has a daughter, Kenley, who was born December 10, 2008. They live most of the year in Scottsdale, Arizona and Idaho Falls, ID. He has family in central Florida as well. His nickname ("Country Breakfast") was coined by former St. Joseph News-Press sports editor Ross Martin in response to Butler's size.As of 2017, he was playing softball for a team named Pharmgrade in the Idaho Falls Parks & Rec Men's League.


== References ==


== External links ==
Career statistics and player information from MLB, or ESPN, or Baseball-Reference, or Fangraphs, or Baseball-Reference (Minors)
Billy Butler on Twitter