The Musketeers of Pig Alley is a 1912 American short drama and a gangster film. It is directed by D. W. Griffith and written by Griffith and Anita Loos. It is also credited for its early use of follow focus, a fundamental tool in cinematography.The film was released on October 31, 1912 and re-released on November 5, 1915 in the United States. The film was shot in Fort Lee, New Jersey where many other early film studios in America's first motion picture industry were based at the beginning of the 20th century. Location shots in New York City reportedly used actual street gang members as extras during the film.
It was also shown in Leeds Film Festival in November 2008, as part of Back to the Electric Palace, with live music by Gabriel Prokofiev, performed in partnership with Opera North.
In 2016, the film was added to the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".


== Plot ==
The film is about a poor married couple living in New York City. The husband works as a musician and must often travel for work. When returning, his wallet is taken by a gangster. His wife goes to a ball where a man tries to drug her, but his attempt is stopped by the same man who robbed the husband. The two criminals become rivals, and a shootout ensues. The husband gets caught in the shootout and recognizes one of the men as the gangster who took his money. The husband sneaks his wallet back and the gangster goes to safety in the couple's apartment. Policemen track the gangster down but the wife gives him a false alibi.


== Cast ==
Elmer Booth – Snapper Kid, Musketeers gang leader
Lillian Gish – The Little Lady
Clara T. Bracy – The Little Lady's Mother
Walter Miller – The Musician
Alfred Paget – Rival Gang Leader
John T. Dillon – Policeman
Madge Kirby – The Little Lady's Friend / In Alley
Harry Carey –  Snapper's Sidekick
Robert Harron – Rival Gang Member / In Alley / At Gangster's Ball
W. C. Robinson – Rival Gang Member (as Spike Robinson)
Adolph Lestina – The Bartender / On Street
Jack Pickford – Boy Gang Member / At Dance BallUncredited:

Gertrude Bambrick – Girl at Dance
Lionel Barrymore – The Musician's Friend
Kathleen Butler – On Street / At Dance
Christy Cabanne – At Dance
Donald Crisp – Rival Gang Member
Frank Evans – At Dance
Dorothy Gish – Girl in Street
Walter P. Lewis – In Alley / At Dance
Antonio Moreno – Musketeers Gang Member / At Dance
Marie Newton	At Dance
J. Waltham – In Alley


== Influence ==
In his book The Movie Stars, film historian Richard Griffith wrote of the scene where Lillian Gish passes another woman on the street (pictured):

     Griffith's camera in this scene happened to focus on the unforgettable face of the nameless girl
      in the center of the shot- and a murmurous wave swept audiences at this point in the film whenever 
      it was shown.  No one knows what became of this particular extra, but such raw material, and such 

      camera accidents, became the stuff of stardom later on."
In fact, the girl is Dorothy Gish, Lillian's sister.
In the Cold Case episode Torn (Season 4.21) Lily sees the victim of a 1919 homicide in an homage to the scene of Lillian Gish passing another woman on the street (pictured).


== See also ==
Lionel Barrymore filmography
Harry Carey filmography
Lillian Gish filmography
D. W. Griffith filmography


== References ==


== External links ==
The Musketeers of Pig Alley on IMDb
The Musketeers of Pig Alley on YouTube
The Musketeers of Pig Alley is available for free download at the Internet Archive
The Musketeers of Pig Alley is available for free download at the Internet Archive