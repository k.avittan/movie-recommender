Wild Hearts Can't Be Broken is a 1991 drama film directed by  Steve Miner. The screenplay concerns Sonora Webster Carver, a rider of diving horses. Gabrielle Anwar stars as Carver alongside Michael Schoeffling and Cliff Robertson. It is based on events in Carver's life as told in her memoir A Girl and Five Brave Horses.


== Plot ==
Sonora Webster lives with her sister and abusive aunt during the Great Depression.  She learns that because of her accidentally letting the cows loose and her suspension from school, her treasured horse Lightning has been sold and she will be placed in an orphanage.  Instead, Sonora slips out of the house during the night. She ends up at a county fair and sees a performance by Marie, a diving girl who rides a horse off a platform, and aspires to do the same. Doc Carver, Marie's employer, tells her she is too young but gives Sonora a job as a stable hand due to her ability with horses, and she begins traveling with them. Doc's son Al wins a wild horse in a card game and Sonora names him Lightning. She later surprises Doc by taming and riding Lightning, so he promises to train her as a diving girl if she can mount it while it's moving, which she succeeds after multiple attempts.
Marie's regular horse becomes sick, therefore Al decides to use Lightning in the shows. Sonora warns Marie to not kick him, but she ignores the warning and Lightning causes her to fall and dislocate her shoulder. With Marie unable to perform, Al asks Sonora if she can do the stunts. Although she has never dived with Lightning, their first jump is successful.  Marie becomes jealous, and as Doc tires of her diva-like behavior, she quits rather than share billing with Sonora. Al develops a romance with Sonora that strains his relationship with his father, leaving after a particularly bad fight.  Al promises to write to Sonora, but Doc hides his letters.  As Doc and the new stable hand Clifford leave the farm in search of work, Lightning falls ill with colic. Al returns, and he and Sonora work together to heal Lightning. Doc fails to find any jobs, but Al announces he has arranged a six-month contract to perform at the Steel Pier in Atlantic City, New Jersey, reconciling father and son. Doc passes away en route from a heart attack, and Al assumes his father's role as show presenter.  Sonora searches for Doc's jacket to give Al confidence on his first show, and finds one of Al's letters inside, confessing his love for her, letting him know she feels the same.
Al proposes to Sonora just before a performance in front of their biggest crowd, which she accepts.  The horse is a jittery stallion instead of her usual partner Lightning, who falters and trips due to a cymbal crash below.  Sonora keeps her eyes open as they fall into the water.  Both of them make it, but her vision is impaired, yet she hides this from Al. Sonora wakes the morning after to discover she is permanently blind from detached retinas in both eyes. To avoid a breach of contract lawsuit, Al must find another diving girl within a week, calling Marie, who returns. Meanwhile, Sonora misses diving terribly.  She tells Al of her desire to dive with Lightning again, and they work together to try to train her to mount him again, but it proves fruitless and Al gives up.  Sonora spends some quiet time with Lightning that night.
The next day, with Clifford's help, Marie is locked in her dressing room, and Sonora performs in her place with Lightning. Al shouts at her to come back down, but she continues and the jump is successful.  Her voiceover tells the audience that she continued diving for eleven more years with the audience never learning of her blindness, and of her happy marriage to Al.


== Cast ==
Gabrielle Anwar as Sonora Webster
Michael Schoeffling as Al Carver
Cliff Robertson as Doc Carver
Dylan Kussman as Clifford
Kathleen York as Marie
Frank Renzulli as Mr. Slater
Nancy Moore Atchison as Arnette Webster
Lisa Norman as Aunt Helen
Lorianne Collins as Clarabelle
Elizabeth Hayes as Miss Simpson
Laura Lee Norton as Mrs. Ellis
Michael J. Matusiak as Photographer
Jeff Woodward as Reporter #1
David Massry as Reporter #2
Cheri Brown as Attractive Girl
David Dwyer as Stagehand
Haley Aull as Little Girl
Ed Grady as Preacher
Katy Matson as Kid #1
Wendy Ball as Kid #2
Sam Aull as Kid #3
Carson Aull as Kid #4
Boyd Peterson as Farmer #1
Gene Walker as Farmer #2
Lowell D. Smith as Wrangler
Rick Warner as Doctor
Mark Jeffrey Miller as Candy Man
Tim Carter as Cymbal Player


== Reception ==
Upon the film's release, Sonora Webster Carver and her sister, Arnette French, saw the movie together, but Sonora was dissatisfied with its embellishments and felt that it bore little resemblance to reality. She told her sister, "the only thing true in it was that I rode diving horses, I went blind, and I continued to ride for another 11 years." Of the movie, French said it "made a big deal about having the courage to go on riding after she lost her sight. But, the truth was riding the horse was the most fun you could have and we just loved it so."The film currently holds a 73% fresh rating on Rotten Tomatoes from 11 reviews.


== References ==


== External links ==
Official website
Wild Hearts Can't Be Broken on IMDb
Wild Hearts Can't Be Broken at Box Office Mojo
Wild Hearts Can't Be Broken at Rotten Tomatoes
Wild Hearts Can't Be Broken DVD Review at Ultimate Disney