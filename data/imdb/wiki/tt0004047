Half-breed is a term, now considered derogatory, used to describe anyone who is of mixed race; although, in the United States, it usually refers to people who are half Native American and half European/white.


== Use by governments ==

In the 19th century the United States government set aside lands in the western states for people of American Indian and European or European-American ancestry known as the Half-Breed Tract. The Nemaha Half-Breed Reservation was established by the Treaty of Prairie du Chien of 1830. In Article 4 of the 1823 Treaty of Fond du Lac land was granted to the "half-breeds" of Chippewa descent on the islands and shore of St. Mary's River near Sault Ste. Marie.During the Pemmican War trials that began in 1818 in Montreal regarding the destruction of the Selkirk Settlement on the Red River the terms Half-Breeds, Bois-Brulés, Brulés and Métifs were defined as "Persons descended from Indian women by white men, and in these trials applied chiefly to those employed by the North-West Company".The Canadian government used the term half-breed in the late 19th and early 20th century for people who were of mixed Aboriginal and European ancestry. The North-West Half-Breed Commission established by the Canadian government after the North West Rebellion also used the term to refer to the Métis residents of the North-West Territories. In 1885 children born in the Northwest of Métis parents or "pure Indian and white parents" were defined as half-breeds by the commission and were eligible for "Half-breed" Scrip.In Alberta the Métis formed the "Halfbreed Association of Northern Alberta" in 1932.


== Geographical names ==
Halfbreed Lake National Wildlife Refuge and Halfbreed Lake in Montana


== In popular culture ==
"Half-Breed" is a country and western song recorded by Marvin Rainwater in 1959, which reached #16 on the US Country Chart."Half-Breed" is a song recorded by Cher and released as a single in 1973. On October 6, 1973, it became Cher's second U.S. number one hit as a solo artist, and it was her second solo single to hit the top spot in Canada on the same date.
Halfbreed is a memoir written by author Maria Campbell published in 1973. The book details her experience growing up as a Métis woman in Canada.


== Further reading ==
Hudson, Charles. Red, White, and Black: Symposium on Indians in the Old South, Southern Anthropological Society, 1971. ISBN 9780820303086.
Perdue, Theda. Mixed Blood Indians, The University of Georgia Press, 2003. ISBN 0-8203-2731-X.


== See also ==
Anglo-Metis
Bois-Brûlés
Métis people (Canada)
Métis people (United States)
Mixed blood


== Notes ==


== External links ==
Murray Parker: "The Half-breed Savage/ Quanah Parker", Texas Escapes
"Half-breed", Dictionary