The Artist's Wife is a 2019 American drama film directed by Tom Dolby. The film, shot in New York City and East Hampton, stars Bruce Dern, Lena Olin, Juliet Rylance, Avan Jogia, Catherine Curtin, and Stefanie Powers. The film premiered at the 2019 Hamptons International Film Festival. It was scheduled to release in New York City on April 3, 2020, but was postponed to September 25, 2020 due to the COVID-19 pandemic.


== Plot ==
The Artist's Wife centers on Claire (Lena Olin), the wife of famed artist Richard Smythson (Bruce Dern) and once a promising painter herself, who now lives in the shadow of her husband's illustrious career. While preparing work for a new exhibition, Richard is diagnosed with dementia. As Richard's memories and moods become lost and erratic, Claire attempts to shield his illness from the art community while trying to reconnect him with his estranged daughter (Juliet Rylance) and grandson (Ravi Cabot-Conyers). Claire, who has taken up painting once again, must decide whether to stand with her husband in the shadows or step into the spotlight herself.


== Cast ==
Lena Olin as Claire Smythson
Bruce Dern as Richard Smythson
Juliet Rylance as Angela Smythson
Avan Jogia as Danny
Tonya Pinkins as Liza Caldwell
Catherine Curtin as Joyce
Ravi Cabot-Conyers as Gogo
Stefanie Powers as Ada Risi


== Reception ==
On review aggregator Rotten Tomatoes, the film holds an approval rating of 68% based on 37 reviews, with an average rating of 6.74/10.


== Film festivals ==
Hamptons International Film Festival
(Next Exposure Grant Winner)
Palm Springs International Film Festival
Mill Valley Film Festival
Sonoma International Film Festival
(Best US Independent Feature)Whistler Film Festival
Sarasota Film Festival
Gold Coast International Film Festival
(Audience Award for Best Narrative Feature)


== References ==


== External links ==
The Artist's Wife on IMDb
The Artist's Wife Official Website
https://www.theartistswifetickets.com