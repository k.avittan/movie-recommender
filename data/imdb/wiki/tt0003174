Much Ado About Nothing is a comedy by William Shakespeare thought to have been written in 1598 and 1599. The play was included in the First Folio, published in 1623.
Through "noting" (sounding like "nothing", and meaning gossip, rumour, overhearing), Benedick and Beatrice are tricked into confessing their love for each other, and Claudio is tricked into believing that Hero is not a maiden. Claudio therefore rejects Hero at the altar. But Hero is vindicated, and marriages are arranged for both pairs.


== Characters ==


== Synopsis ==

In Messina, a messenger brings news that Don Pedro will return that night from a successful battle, along with Claudio and Benedick. Beatrice asks the messenger about Benedick, and mocks Benedick's ineptitude as a soldier. Leonato explains that "There is a kind of merry war betwixt Signor Benedick and her."On the soldiers' arrival, Leonato invites Don Pedro to stay for a month, and Benedick and Beatrice resume their "merry war". Pedro's illegitimate brother, Don John, is also introduced. Claudio's feelings for Hero are rekindled, and he informs Benedick of his intention to court her. Benedick, who openly despises marriage, tries to dissuade him. Don Pedro encourages the marriage. Benedick swears that he will never marry. Don Pedro laughs at him, and tells him that he will when he has found the right person.
A masquerade ball is planned. Therein a disguised Don Pedro woos Hero on Claudio's behalf. Don John uses this situation to get revenge on him, telling Claudio that Don Pedro is wooing Hero for himself. Claudio rails against the entrapments of beauty. But the misunderstanding is later resolved, and Claudio is promised Hero's hand in marriage.
Meanwhile, Benedick and Beatrice have danced together, trading disparaging remarks under cover of their masks. Benedick is stung at hearing himself described as "the prince's jester, a very dull fool", and yearns to be spared the company of "Lady Tongue". Don Pedro and his men, bored at the prospect of waiting a week for the wedding, concoct a plan to match-make between Benedick and Beatrice. They arrange for Benedick to overhear a conversation in which they declare that Beatrice is madly in love with him but too afraid to tell him. Hero and Ursula likewise ensure that Beatrice overhears a conversation in which they themselves discuss Benedick's undying love for her. Both Benedick and Beatrice are delighted to think that they are the object of unrequited love, and both resolve to mend their faults and declare their love.
Meanwhile, Don John plots to stop the wedding and embarrass his brother and wreak misery on Leonato and Claudio. He tells Don Pedro and Claudio that Hero is "disloyal", and arranges for them to see his associate, Borachio, enter her bedchamber and engage amorously with her (it is actually Hero's chambermaid). Claudio and Don Pedro are duped, and Claudio vows to publicly humiliate Hero.

The next day, at the wedding, Claudio denounces Hero before the stunned guests, and he storms off with Don Pedro. Hero faints. A humiliated Leonato expresses his wish for her to die. The presiding friar intervenes, believing Hero innocent. He suggests that the family fake Hero's death to inspire Claudio with remorse. Prompted by the day's stressful events, Benedick and Beatrice confess their love for each other. Beatrice then asks Benedick to kill Claudio as proof of his devotion. Benedick hesitates but is swayed. Leonato and Antonio blame Claudio for Hero's supposed death and threaten him, to little effect. Benedick arrives and challenges him to a duel. 
On the night of Don John's treachery, the local Watch overheard Borachio and Conrade discussing their "treason" and "most dangerous piece of lechery that ever was known in the commonwealth", and arrested them therefore. Despite their ineptness (headed by constable Dogberry), they obtain a confession and inform Leonato of Hero's innocence. Don John has fled, but a force is sent to capture him. Claudio, remorseful and thinking Hero dead, agrees to her father's demand that he marry Antonio's daughter, "almost the copy of my child that's dead".After Claudio swears to marry this other bride, this bride is revealed to be Hero. Claudio is overjoyed. Beatrice and Benedick publicly confess their love for each other. Don Pedro taunts "Benedick the married man", and Benedick counters that he finds the Prince sad, advising him: "Get thee a wife". As the play draws to a close, a messenger arrives with news of Don John's capture, but Benedick proposes to postpone deciding Don John's punishment until tomorrow, so the couples can enjoy their newfound happiness. The couples dance and celebrate as the play ends.


== Sources ==
In the sixteenth century, stories of lovers deceived into believing each other false were common currency in northern Italy. Shakespeare's immediate source may have been one of the Novelle ("Tales") by Matteo Bandello of Mantua (possibly the translation into French by François de Belleforest), which dealt with the tribulations of Sir Timbreo and his betrothed Fenicia Lionata, in Messina, after King Piero's defeat of Charles of Anjou. Another version, featuring lovers Ariodante and Ginevra, with the servant Dalinda impersonating Ginevra on the balcony, appears in Book V of Orlando Furioso by Ludovico Ariosto (published in an English translation in 1591).  The character of Benedick has a counterpart in a commentary on marriage in Orlando Furioso. But the witty wooing of Beatrice and Benedick is apparently original, and very unusual in style and syncopation. One version of the Claudio–Hero plot is told by Edmund Spenser in The Faerie Queene (Book II, Canto iv).


== Date and text ==
The earliest printed text states that Much Ado About Nothing was "sundry times publicly acted" prior to 1600. It is likely that the play made its debut in the autumn or winter of 1598–1599. The earliest recorded performances are two at Court in the winter of 1612–1613, during festivities preceding the marriage of Princess Elizabeth with Frederick V, Elector Palatine (14 February 1613). The play was published in quarto in 1600 by the stationers Andrew Wise and William Aspley. This was the only edition prior to the First Folio in 1623.


== Analysis and criticism ==


=== Style ===
The play is predominantly written in prose. The substantial verse sections achieve a sense of decorum.


=== Setting ===
Much Ado About Nothing is set in Messina, a port on the island of Sicily, when Sicily is ruled by Aragon. The action of the play takes place mainly at the home and grounds of Leonato's Estate.


=== Themes and motifs ===


==== Gender roles ====

Benedick and Beatrice quickly became the main interest of the play. They are considered the leading roles even though their relationship is given equal or lesser weight in the script than Claudio and Hero's situation.  Charles II wrote 'Benedick and Beatrice' beside the title of the play in his copy of the Second Folio. The provocative treatment of gender is central and should be considered in its Renaissance context. This was reflected and emphasized in certain plays of the period, but was also challenged. Amussen notes that the undoing of traditional gender clichés seems to have inflamed anxieties about the erosion of social order. It seems that comic drama could be a means of calming such anxieties. Ironically, the play's popularity suggests that this only increased interest in such behavior. Benedick wittily gives voice to male anxieties about women's "sharp tongues and proneness to sexual lightness". In the patriarchal society of the play, the men's loyalties were governed by conventional codes of honour, camaraderie, and a sense of superiority over women. Assumptions that women are by nature prone to inconstancy are shown in the repeated jokes about cuckoldry, and partly explain Claudio's readiness to believe the slander against Hero. This stereotype is turned on its head in Balthazar's song "Sigh No More", which presents men as the deceitful and inconstant sex that women must suffer.


==== Infidelity ====
Several characters seem to be obsessed with the idea that a man has no way to know if his wife is faithful and that women can take full advantage of this. Don John plays upon Claudio's pride and his fear of cuckoldry, which leads to the disastrous first wedding. Many of the males easily believe that Hero is impure, and even her father readily condemns her with very little proof. This motif runs through the play, often referring to horns (a symbol of cuckoldry).
In contrast, Balthasar's song "Sigh No More" tells women to accept men's infidelity and continue to live joyfully. Some interpretations say that Balthasar sings poorly, undercutting the message. This is supported by Benedick's cynical comments about the song where he compares it to a howling dog. In the 1993 Branagh film, Balthasar sings it beautifully: it is given a prominent role in the opening and finale, and the message seems to be embraced by the women.


==== Deception ====
There are many examples of deception and self-deception in the play. The games and tricks played on people often have the best intentions: to make people fall in love, or to help someone get what they want, or to lead someone to realize their mistake. But not all are well-meant: Don John convinces Claudio that Don Pedro wants Hero for himself, and Borachio meets 'Hero' (who is actually Margaret) in Hero's bedroom window. These modes of deceit play into a complementary theme of emotional manipulation, the ease with which the characters' sentiments are redirected and their propensities exploited as a means to an end. The characters' feelings for each other are played as vehicles to reach an ultimate goal of engagement rather than seen as an end in themselves.


==== Masks and mistaken identity ====
Characters are constantly pretending to be others or are otherwise mistaken for others. Margaret is mistaken for Hero, leading to Hero's disgrace. During a masked ball (in which everyone must wear a mask), Beatrice rants about Benedick to a masked man who is actually Benedick, but she acts unaware of this. During the same celebration, Don Pedro pretends to be Claudio and courts Hero for him. After Hero is proclaimed dead, Leonato orders Claudio to marry his "niece" who is actually Hero.


==== Nothing ====

Another motif is the play on the words nothing and noting. These were near-homophones in Shakespeare's day. Taken literally, the title implies that a great fuss ("much ado") is made of something which is insignificant ("nothing"), such as the unfounded claims of Hero's infidelity, and that Benedick and Beatrice are in love with each other. Nothing is also a double entendre: "an O-thing" (or "n othing" or "no thing") was Elizabethan slang for "vagina", derived from women having "nothing" between their legs. The title could also be understood as Much Ado About Noting: much of the action centers around interest in others and critique of others, written messages, spying, and eavesdropping. This attention is mentioned directly several times, particularly concerning "seeming", "fashion", and outward impressions.
Examples of noting as noticing occur in the following instances: (1.1.131–132)

Claudio: Benedick, didst thou note the daughter of Signor Leonato?Benedick: I noted her not, but I looked on her.
and (4.1.154–157).

Friar: Hear me a little,For I have only been silent so long
And given way unto this course of fortune
By noting of the lady.
At (3.3.102–104), Borachio indicates that a man's clothing doesn't indicate his character:

Borachio: Thou knowest that the fashion of a doublet, or a hat, or a cloak is nothing to a man.
A triple play on words in which noting signifies noticing, musical notes, and nothing, occurs at (2.3.47–52):

Don Pedro: Nay pray thee, come;Or if thou wilt hold longer argument,
Do it in notes.Balthasar: Note this before my notes:
There's not a note of mine that's worth the noting.Don Pedro: Why, these are very crotchets that he speaks –
Note notes, forsooth, and nothing!
Don Pedro's last line can be understood to mean: "Pay attention to your music and nothing else!" The complex layers of meaning include a pun on "crotchets," which can mean both "quarter notes" (in music), and whimsical notions.
The following are puns on notes as messages: (2.1.174–176),

Claudio: I pray you leave me.Benedick: Ho, now you strike like the blind man – 'twas the boy that stole your meat, and you'll beat the post.
in which Benedick plays on the word post as a pole and as mail delivery in a joke reminiscent of Shakespeare's earlier advice "Don't shoot the messenger"; and (2.3.138–142)

Claudio: Now you talk of a sheet of paper, I remember a pretty jest your daughter told us of.Leonato: O, when she had writ it and was reading it over, she found Benedick and Beatrice between the sheet?
in which Leonato makes a sexual innuendo, concerning sheet as a sheet of paper (on which Beatrice's love note to Benedick is to have been written), and a bedsheet.


== Performance history ==

The play was very popular in its early decades, and it continues to be a standard wherever Shakespeare is performed. In a poem published in 1640, Leonard Digges wrote: "let but Beatrice / And Benedick be seen, lo in a trice / The Cockpit galleries, boxes, all are full."

After the theatres re-opened during the Restoration, Sir William Davenant staged The Law Against Lovers (1662), which inserted Beatrice and Benedick into an adaptation of Measure for Measure. Another adaptation, The Universal Passion, combined Much Ado with a play by Molière (1737). Shakespeare's text had been revived by John Rich at Lincoln's Inn Fields (1721). David Garrick first played Benedick in 1748, and continued to play him until 1776.The great nineteenth-century stage team Henry Irving and Ellen Terry counted Benedick and Beatrice as their greatest triumph, and Charles Kemble also had a great success as Benedick. John Gielgud made Benedick one of his signature roles between 1931 and 1959, playing opposite Diana Wynyard, Peggy Ashcroft, and Margaret Leighton. The longest running Broadway production is A. J. Antoon's 1972 staging, starring Sam Waterston, Kathleen Widdoes, and Barnard Hughes. Derek Jacobi won a Tony Award for playing Benedick in 1984. Jacobi had also played Benedick in the Royal Shakespeare Company's highly praised 1982 production. Director Terry Hands produced the play on a stage-length mirror against an unchanging backdrop of painted trees. Sinéad Cusack played Beatrice. In 2013, Vanessa Redgrave and James Earl Jones (then in their seventies and eighties, respectively) played Beatrice and Benedick on stage at The Old Vic, London.


=== Actors, theatres and awards ===

c. 1598: In the original production by the Lord Chamberlain's Men, William Kempe played Dogberry and Richard Cowley played Verges.
1748: David Garrick played Benedick for the first time.
1887: Henry Irving and Ellen Terry played Benedick and Beatrice.
1931: John Gielgud played Benedick for the first time at the Old Vic Theatre and it stayed in his repertory until 1959.
1960: A Tony Award Nomination for "Best Performance by a Leading Actress in a Play" went to Margaret Leighton for her role played in Much Ado.
1973: A Tony Award Nomination for "Best Featured Actor in a Play" went to Barnard Hughes as Dogberry in the New York Shakespeare Festival production.
1973: A Tony Award Nomination for "Best Performance by a Leading Actress in a Play" went to Kathleen Widdoes.
1983: The Evening Standard Award for the "Best Actor" went to Derek Jacobi.
1985: A Tony Award Nomination for "Best Performance by a Leading Actress in a Play" was received by Sinéad Cusack.
1985: The Tony Award for "Best Performance by a Leading Actor in a Play" went to Derek Jacobi as Benedick.
1987: Tandy Cronyn as Beatrice and Richard Monette as Benedick in a production at the Stratford Festival directed by Peter Moss
1989: The Evening Standard Award for "Best Actress" went to Felicity Kendal as Beatrice in Elijah Moshinsky's production at the Strand Theatre.
1994: The Laurence Olivier Award for "Best Actor" went to Mark Rylance as Benedick in Matthew Warchus' production at the Queen's Theatre.
2006: The Laurence Olivier Award for "Best Actress" was received by Tamsin Greig as Beatrice in the Royal Shakespeare Company's production in the Royal Shakespeare Theatre, directed by Marianne Elliott.
2007: Zoë Wanamaker appeared as Beatrice and Simon Russell Beale as Benedick in a National Theatre production directed by Nicholas Hytner.
2011: Eve Best appeared as Beatrice and Charles Edwards as Benedick at Shakespeare's Globe, directed by Jeremy Herrin.
2011: David Tennant as Benedick alongside Catherine Tate as Beatrice in a production of the play at the Wyndham's Theatre, directed by Josie Rourke. An authorized recording of this production is available to download and watch from Digital Theatre.
2012: Meera Syal as Beatrice and Paul Bhattacharjee as Benedick in an Indian setting, directed by Iqbal Khan for the Royal Shakespeare Company, part of the World Shakespeare Festival
2013: Vanessa Redgrave as Beatrice and James Earl Jones as Benedick in a production at The Old Vic directed by Mark Rylance.
2013: A German-language production (Viel Lärm um Nichts), translated and directed by Marius von Mayenburg at the Schaubuhne am Lehniner Platz, Berlin.
2017: Beatriz Romilly as Beatrice and Matthew Needham as Benedick in a Mexican setting, at Shakespeare's Globe, directed by Matthew Dunster.
2018: Mel Giedroyc as Beatrice and John Hopkins as Benedick in a modern Sicilian setting, at the Rose Theatre, Kingston, directed by Simon Dormandy.
2019: Danielle Brooks as Beatrice and Grantham Coleman as Benedick with an all Black cast set in contemporary Georgia, at The Public Theater, directed by Kenny Leon. This version was broadcast on PBS Great Performances on 22 November 2019.


== Adaptations ==


=== Music ===
The operas Montano et Stéphanie (1799) by Jean-Élie Bédéno Dejaure and Henri-Montan Berton, Béatrice et Bénédict (1862) by Hector Berlioz, Beaucoup de bruit pour rien (pub. 1898) by Paul Puget and Much Ado About Nothing by Sir Charles Villiers Stanford (1901) are based upon the play.Erich Wolfgang Korngold composed music for a production in 1917 at the Vienna Burgtheater by Max Reinhardt.In 2006 the American Music Theatre Project produced The Boys Are Coming Home, a musical adaptation by Berni Stapleton and Leslie Arden that sets Much Ado About Nothing in America during the Second World War.
The title track of the 2009 Mumford & Sons album Sigh No More uses quotes from this play in the song. The title of the album is also a quotation from the play.In 2015, Billie Joe Armstrong wrote the music for a rock opera adaptation of the play, These Paper Bullets, which was written by Rolin Jones.Opera McGill recently commissioned a new operatic adaptation of the play with music by James Garner and libretto adapted by Patrick Hansen which premieres in Montréal in 2022.


=== Film ===
The first cinematic version in English may have been the 1913 silent film directed by Phillips Smalley.Martin Hellberg's 1964 East German film Viel Lärm um nichts was based on the Shakespeare play. In 1973 a Soviet film adaptation was directed by Samson Samsonov which starred Galina Jovovich and Konstantin Raikin.The first sound version in English released to cinemas was the highly acclaimed 1993 film by Kenneth Branagh. It starred Branagh as Benedick, Branagh's then-wife Emma Thompson as Beatrice, Denzel Washington as Don Pedro, Keanu Reeves as Don John, Richard Briers as Leonato, Michael Keaton as Dogberry, Robert Sean Leonard as Claudio, Imelda Staunton as Margaret, and Kate Beckinsale in her film debut as Hero.
The 2001 Hindi film Dil Chahta Hai is a loose adaptation of the play.In 2011, Joss Whedon completed filming of an adaptation, released in June 2013. The cast includes Amy Acker as Beatrice, Alexis Denisof as Benedick, Nathan Fillion as Dogberry, Clark Gregg as Leonato, Reed Diamond as Don Pedro, Fran Kranz as Claudio, Jillian Morgese as Hero, Sean Maher as Don John, Spencer Treat Clark as Borachio, Riki Lindhome as Conrade, Ashley Johnson as Margaret, Tom Lenk as Verges, and Romy Rosemont as the sexton. Whedon's adaptation is a contemporary revision with an Italian-mafia theme.
In 2012 a filmed version of the live 2011 performance at The Globe was released to cinemas and on DVD. The same year, a filmed version of the 2011 performance at Wyndham's Theatre was made available for download or streaming on the Digital Theatre website.In 2015, a modern movie version of this play was created by Owen Drake entitled Messina High, starring Faye Reagan.


=== Television and web series ===
There have been several screen adaptations of Much Ado About Nothing, and almost all of them have been made for television. An adaptation is the 1973 New York Shakespeare Festival production by Joseph Papp, shot on videotape and released on VHS and DVD, that presents more of the text than Kenneth Branagh's version. It is directed by A. J. Antoon. The Papp production stars Sam Waterston, Kathleen Widdoes, and Barnard Hughes.
The 1984 BBC Television version stars Lee Montague as Leonato, Cherie Lunghi as Beatrice, Katharine Levy as Hero, Jon Finch as Don Pedro, Robert Lindsay as Benedick, Robert Reynolds as Claudio, Gordon Whiting as Antonio and Vernon Dobtcheff as Don John. An earlier BBC television version with Maggie Smith and Robert Stephens, adapted from Franco Zeffirelli's stage production for the National Theatre Company's London stage production, was broadcast in February 1967.In 2005 the BBC adapted the story by setting it in the modern-day studios of Wessex Tonight, a fictional regional news programme, as part of the ShakespeaRe-Told season, with Damian Lewis, Sarah Parish, and Billie Piper.The 2014 YouTube web series Nothing Much to Do is a modern retelling of the play, set in New Zealand.In 2019, PBS recorded a live production of Public Theater free Shakespeare in the Park 2019 production at the Delacorte Theater in New York City’s Central Park for part of its Great Performances. The all-Black cast features Danielle Brooks and Grantham Coleman as the sparring lovers Beatrice and Benedick, directed by Tony Award winner Kenny Leon with choreography by Tony Award nominee Camille A. Brown. The cast also includes Jamar Brathwaite (Ensemble), Chuck Cooper (Leonato), Javen K. Crosby (Ensemble), Denzel DeAngelo Fields (Ensemble), Jeremie Harris (Claudio), Tayler Harris (Ensemble), Erik Laray Harvey (Antonio/Verges), Kai Heath (Messenger), Daniel Croix Henderson (Balthasar), Tyrone Mitchell Henderson (Friar Francis/Sexton), Tiffany Denise Hobbs (Ursula), Lateefah Holder (Dogberry), LaWanda Hopkins (Dancer), Billy Eugene Jones (Don Pedro), Margaret Odette (Hero), Hubert Point-Du Jour (Don John), William Roberson (Ensemble), Jaime Lincoln Smith (Borachio), Jazmine Stewart (Ensemble), Khiry Walker (Conrade/Ensemble), Olivia Washington (Margaret) and Latra A. Wilson (Dancer).


=== Literature ===
In 2016, Lily Anderson released a young adult novel called The Only Thing Worse Than Me Is You, a modernized adaptation of Much Ado About Nothing whose main characters, Trixie Watson and Ben West, attend a "school for geniuses".In 2017, a YA adaptation was released by author Mckelle George called Speak Easy, Speak Love, where the events of the play take place in the 1920s, focused around a failing speakeasy.In 2018, author Molly Booth released a summer YA novel adaptation called Nothing Happened, where Claudio and Hero are a homosexual couple, Claudia and Hana.


== See also ==
Margaret (moon), a moon of Uranus, named after the character from Much Ado About Nothing


== References ==


== External links ==
 Much Ado About Nothing public domain audiobook at LibriVox

 Much Ado About Nothing at Project Gutenberg
Much Ado About Nothing at Standard Ebooks
Annotated text with line numbers, search engine, and scene summaries
Text of the play at MIT
Much Ado About Nothing at the British Library
Lesson plans for teaching Much Ado About Nothing at Web English Teacher
Much Ado About Nothing A modern re-telling in flash comic format provided by the Stratford Shakespeare Festival of Canada
"Hero. The quiet daughter of Leonato and cousin of the gay Beatrice . . ." . New International Encyclopedia. 1905.