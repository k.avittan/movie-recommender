The Parish of the Holy Sacrifice, also the Church of the Holy Sacrifice, is a landmark Catholic chapel on the University of the Philippines Diliman campus. It belongs to the Roman Catholic Diocese of Cubao and its present parish priest is Rev. Fr. Jose S. Tupino III. More popularly known as the UP Chapel, the church was constructed under the supervision of Fr. John P. Delaney, who began ministering to the spiritual needs of the campus in 1947.
Known for its architectural design, the church is recognized as a National Historical Landmark and a Cultural Treasure by the National Historical Commission of the Philippines, and the National Museum of the Philippines respectively. It was designed by the late National Artist of the Philippines for Architecture, Leandro Locsin, one of four National Artists who collaborated on the project. According to a post from the UP Diliman website, it is the only structure in the country where the works of five national artists can be found. Alfredo Juinio served as the structural engineer for the project.
The church is adjacent to the U.P. Health Service Building and the U.P. Shopping Center, and is serviced by all of the university's jeepney routes.


== History ==

Before the Church of the Holy Sacrifice was erected in the early 1950s, a university chapel served the University of the Philippines community, under the apostolate of the Society of Jesus.  Fr. John P. Delaney served as the first Jesuit chaplain.The domed Parish of the Holy Sacrifice (PHS) is both an architectural wonder and a rallying point of faith. Catholics in the faculty, student body, staff, and alumni worked tirelessly in the early 1950s to raise funds for the structure.  The faculty pledged a part of their monthly salary, while the students contributed from their own allowances to fund the construction of the church.In December 1955, the inner structure of the church which includes the pews, crucifix, tabernacle, altar, marble floor design, and skylight were worked on, as well as the outside landscaping, pathways, and fences. Eventually, the construction of the chapel was finished through the efforts of the UP Catholic Community headed by Fr. Delaney, and the genius of young UP graduates such as Engr. David Consunji, Engr. Alfredo Juinio, and National Artist Leandro Locsin. Its interior was an obra maestra of yet another conglomeration of now National Artists: Vicente Manansala and Ang Kiukok, Arturo Luz, and Napoleon Abueva. Structural, electric and water systems were also planned by Felisberto G. Reyes, Lamberto UN Ocampo, and Jose M. Segovia with Agapito S. Pineda.
On December 21, 1955, the Archbishop of the Roman Catholic Archdiocese of Manila officiated at the first Mass in the newly constructed chapel and on December 24, 1955, Fr. Delaney celebrated Mass and delivered his last homily entitled “The Reality of Christmas”. A transcript of his this last homily is published in "The Chapel Chismis", a collection of Fr. Delaney's letters to the UP community.
On May 30, 1977, the chaplaincy was elevated to a university parish by Cardinal Jaime Sin.  Msgr. Manny Gabriel became its first parish priest.  To date, eight parish priests have served the parish, two of them were later ordained bishops, namely, Bishop Mylo Vergara of the Roman Catholic Diocese of Pasig and Archbishop Ramon Arguelles of the Roman Catholic Archdiocese of Lipa.


== Architecture and Interior ==

Initially, Leandro Locsin designed the church for the Ossorio family, who were planning to build a chapel in Negros. Unfortunately, the plans for the chapel were scrapped when Frederic Ossorio, the head of the family, left for the United States. However, in 1955, Father Delaney commissioned Locsin to design a chapel that was open and could easily accommodate 1,000 people. The Church of Holy Sacrifice became the first circular chapel with the altar in its center in the country, and the first to have a thin shell concrete dome. This was his first major architectural commission.
Locsin chose the round plan as the most suited for giving the congregation a sense of participation in the mass. The center of the plan is the altar, which is elevated from the floor by three steps. The separation of the choir and congregation was dissolved through this design. The ceiling of the concrete dome church was left bare and a dramatic use of colored lights mark the changing seasons of worship.
The dome of the church is supported by thirty two columns located along its rim. These columns terminate a third of the way up where they support the ledge-like ring beam.  This ring beam, in turn, supports the 3-inch thick concrete shell of the dome that spans 29.26 metres (96.0 ft). The unique design of the dome allows in natural lighting and ventilation. In the middle of the dome is a circular skylight, which supports the triangular bell tower. The bell tower, then extends to the interior, supporting the crucifix. The arrangement of the interior of the church is concentric, with the altar in the middle.
The fifteen murals depicting the Stations of the Cross that adorn the circular walls are by Vicente Manansala assisted by Ang Kiukok. The fifteenth mural of the "Resurrection of Christ" is on the wall of the sacristy. The cross depicting both a suffering and a risen Christ and the marble altar are the handiwork of Napoleon Abueva. The floor mural, executed in terazzo and radiating from the altar, is by Arturo Luz. This floor mural is also called the "River of Life". Fernando Zobel de Ayala had studies to fill the outer wall with calligraphic interpretations. However, the project was not executed. In 1968, Jose Maceda, another national artist, premiered his concert, Pagsamba at the PHS, and repeated it in 1978 and 1998 at the same venue.On January 12, 2005, the church was recognized as a National Historical Landmark and a Cultural Treasure by the National Historical Institute and the National Museum, respectively. During the recognition ceremony, National Historical Institute Chairman Ambeth R. Ocampo lauded the church as a "masterpiece of Filipino artistry and ingenuity". Currently, a project aims to restore the dome of the historic church.


== Boundaries ==
From the junction of Don Mariano Marcos Ave., and Juan Luna St., East, through Juan Luna St., Southward through the Eastern Limits of East: From the Eastern limits of Pook Dagohoy, Southward thru Katipunan Ave. Eastward through Montalban St. Southward through the Eastern limits of UP High School and UP Integrated School.


== Facilities ==
Schools:

Parish of the Holy Sacrifice Outreach Program U.P. Campus, Quezon CityCatholic Student Organizations:

The Parish is not only home to the church but also to the tambayans of various catholic student groups such as UP Student Catholic Action (UPSCA), In Christ Thrust for University Students (ICTUS), the Parish Youth Ministry and the Campus Ministry Office.Gardens:

Inside the parish grounds are seven (7) gardens, namely: Garden of Communion, Garden of Love and Friendship, Garden of the Family, Garden of Religions, Garden of Justice and Peace, Garden of Mother and Healing and the Garden of Eternal Life.Mortuary:

Resurrection MortuaryPre-school Program:

While the University of the Philippines does not have a pre-school program in its system, the Parish of the Holy Sacrifice Outreach Program is open children ages 3–6 years old. Programs include Toddlers (3 y/o), Nursery (3.5-4 y/o), Kindergarten (4.5-5 y/o) and Preparatory (6 y/o).


== Profile ==
Name of Parish:

Parish of the Holy SacrificeFeast Day:

Solemnity of the Body and Blood of Christ (Corpus Christi)Titular:

Holy SacrificeDate Established:

May 30, 1977Official Social Media Pages:

Facebook: https://www.facebook.com/parishoftheholysacrifice/
Instagram: @parishoftheholysacrificeOfficial Website:

www.parishoftheholysacrifice.phCurrent Parish Priest

Rev. Fr. Jose S. Tupino IIIFormer Parish Priests


== Landmark ==
It has been declared a Philippines National Historical Landmark.


== See also ==
University of the Philippines Diliman
Roman Catholic Diocese of Cubao
Thin-shell structure
Leandro Locsin


== References ==


=== Bibliography ===


== External links ==
The University: A document from the UP Diliman website 
Resolution No. 5, s. 2005     Declaring the Church of the Holy Sacrifice in the University of the Philippines, Diliman, Quezon City a National Historical Landmark
Official Site of the Roman Catholic Diocese of Cubao