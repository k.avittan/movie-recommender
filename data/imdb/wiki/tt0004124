"Chapter 7: The Reckoning" is the seventh episode of the first season of the American streaming television series The Mandalorian. It was written by the series' showrunner Jon Favreau, directed by Deborah Chow, and released on Disney+ on December 18, 2019. The episode stars Pedro Pascal as The Mandalorian, a lone bounty hunter on the run with "the Child". The episode won one Primetime Emmy Award.


== Plot ==
The Mandalorian receives a message from Greef Karga. Karga's town has been overrun by Imperial troops led by the Client, who is desperate to recover the Child. Karga proposes that the Mandalorian use the Child as bait in order to kill the Client and free the town. In return, Karga will square things with the Guild, which would allow the Mandalorian and the Child to live in peace. Sensing a trap, the Mandalorian recruits Cara Dune and Kuiil the Ugnaught to assist him. Kuill has also rebuilt and reprogrammed IG-11 to act as a nurse droid instead of a bounty hunter, and the group journeys to Nevarro. Upon arrival they meet Karga and his associates, but en route to the town are attacked by Mynocks. Karga is injured but the Child uses the Force to heal his wound.
In return, Karga shoots his associates and explains that his original plan was to kill the Mandalorian and take the Child to the Client, but that after the Child saved his life, he could not go through with it. The group formulates a new plan: Karga will pretend that Dune captured the Mandalorian, and all three will enter the town to meet the Client. Meanwhile, Kuiil will return the Child to the ship. During the meeting, the Client receives a call from Moff Gideon, whose stormtroopers and deathtroopers surround the building and open fire, killing the Client. Gideon arrives and boasts that the Child will soon be in his possession. In the desert outside town, two scout troopers track and kill Kuiil and kidnap the Child. The last shot shows Kuiil in a heap on the ground.


== Production ==


=== Development ===
The episode was directed by Deborah Chow and written by Jon Favreau.


=== Casting ===
Nick Nolte was cast as the voice of Kuiil in November 2018. Giancarlo Esposito, Carl Weathers and Werner Herzog joined the main cast in December 2018 as Moff Gideon, Greef Karga and the Client, respectively. Gina Carano and Taika Waititi also co-star as Cara Dune and the voice of IG-11. Additional guest starring actors cast for this episode include Adam Pally as a bike scout trooper and Dave Reaves as a Zabrak fighter. Brendan Wayne and Lateef Crowder are credited as stunt doubles for the Mandalorian. Wayne had worked closely with Pascal to develop the character.Misty Rosas, Rio Hackford and Chris Bartlett are credited as performance artists for Kuiil, IG-11 and the RA-7 droid, respectively. Gene Freeman and John Dixon are credited as stunt doubles for Greef Karga and the Client. "The Child" was performed by various puppeteers. The episode features several stormtroopers in a single scene. Since the production team did not have enough suits for the troopers, they contacted the 501st Legion, a fan group dedicated to cosplay of stormtroopers, Imperial officers, and other Star Wars villains, to fill in the extra roles.


=== Music ===
Ludwig Göransson composed the soundtrack for the episode.


== Reception ==
"The Reckoning" received critical acclaim. On Rotten Tomatoes, the episode holds an approval rating of 100% with an average rating of 8.46/10, based on 26 reviews. The website's critics consensus reads, "The Mandalorian's disparate plot threads coalesce in thrilling fashion during "The Reckoning", with familiar faces making a welcome return and the emotional stakes going into hyperdrive." On Metacritic, the episode received an average score of 70/100 among 29 reviews.In a positive review, Tyler Hersko, of IndieWire, felt the episode was "a superb slice of westernized sci-fi and a much-needed breath of fresh air that will leave viewers rabid to discover what happens next week in the season finale." Alan Sepinwall, of the Rolling Stone, felt that "the episode is as thrilling and entertaining as it is because the season unfolded so simply and carefully."The episode won a Primetime Emmy Award for Outstanding Cinematography for a Single-Camera Series (Half-Hour).


== References ==


== External links ==
Chapter 7: The Reckoning on Disney+
Chapter 7: The Reckoning on IMDb
Official website
Chapter 7: The Reckoning on Wookieepedia, a Star Wars wiki