Three Wishes is a 1995 American drama-fantasy film directed by Martha Coolidge and starring Patrick Swayze, Mary Elizabeth Mastrantonio and Joseph Mazzello.


== Plot ==
In the present, Tom Holman, a family man is beset with financial problems that make him emotionally distant from his wife and family. While driving in his car with his wife and two daughters, he almost runs into a man walking into a cemetery. Tom's vision of the man causes him to have a flashback to a similar incident in his childhood.
In 1955, Jean Holman (Mary Elizabeth Mastrantonio), is a single mother caring for her two sons, 11-year old Tom (Joseph Mazzello) and 5-year old Gunny (Seth Mumy). Jean's husband is a soldier who was reported missing in action during the Korean War and is presumed dead.  Tom, who deeply misses his father, feels left out of father-son activities (baseball and camping) in the community. Jean is concerning at Gunny's repeated complaints of stomach pains and his medical condition has gone undiagnosed. Further,  While driving one day with Tom and Gunny, Jean accidentally runs into a drifter named Jack McCloud (Patrick Swayze), breaking his leg. Feeling sorry for him, Jean invites Jack and his female dog Betty Jane to stay at her home until his leg has healed. After some initial difficulties in adapting to this new lifestyle, Jack soon finds himself loved by the family, who all want him to stay. Jack starts teaching baseball to Tom and the two of them develop a strong bond. Meanwhile, Gunny believes that there is more to Jack and Betty Jane than meets the eye, and he is determined to find out what.
Gunny soon finds out Betty Jane is a genie, not merely a dog. Meanwhile, Jack's bond with the family gets closer and closer, and Tom comes to see Jack as a surrogate father.  At the same time, Tom's baseball coach, Coach Schramka (Jay O. Sanders) recognizes Jack as a former famous baseball player, who disappeared years ago during Second World War. Jack denies it.  Phil sees Jack as a threat, a potential rival for the affections of Jane. Tom too comes to reject Jack after Jack decides to move away when he is healed, leaving Tom to feel himself abandoned by a father a second time. Jean also feels sad with Jack's decision but admits the impossibility of staying with Jack. After Jack says goodbye to Tom, Tom flees on his bike to a nearby hill. Suddenly, Tom changes his mind and heads back to the highway to find Jack getting into a truck before Tom can reach him.
On the 4th of July Party, the titular wishes are being fulfilled.  Gunny realizes his wish to fly in the middle of the fireworks, unseen by the public congregated in the park. Tom's wish is fulfilled when his long-lost father is discovered alive, and he returns home after being freed from his imprisonment by the People's Republic of China.
Back to present day, adult Tom, enters the cemetery. Tom meets Jack again. He talks with Jack briefly, discovering that Jack's real wish wasn't that Tom's father would come back home, but that Tom would be happy with the family he had. Afterwards, Jack disappears, and a renewed Tom holds his wife close.  Meanwhile, the camera focuses on a tomb with a headstone that reveals that Jack McCloud died on August 6, 1944.


== Cast ==
Patrick Swayze as Jack McCloud
Mary Elizabeth Mastrantonio as Jean Holman
Joseph Mazzello as Tom Holman
Michael O'Keefe as Adult Tom Holman
Seth Mumy as Gunny Holman
D. B. Sweeney (uncredited) as Jeffrey Holman
David Marshall Grant as Phil
Jay O. Sanders as Coach Schramka
Diane Venora as Joyce
Scott Patterson as Mr. Holman
Neal McDonough as Policeman


== Reception ==
Three Wishes was poorly received by critics, as the film holds a 13% rating on Rotten Tomatoes.


== References ==


== External links ==
Three Wishes on IMDb
Three Wishes at AllMovie
Three Wishes at Box Office Mojo
Three Wishes at Rotten Tomatoes