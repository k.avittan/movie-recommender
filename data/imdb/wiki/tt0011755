In law and in religion, testimony is a solemn attestation as to the truth of a matter.


== Etymology ==
The words "testimony" and "testify" both derive from the Latin word testis, referring to the notion of a disinterested third-party witness.


== Law ==
In the law, testimony is a form of evidence that is obtained from a witness who makes a solemn statement or declaration of fact. Testimony may be oral or written, and it is usually made by oath or affirmation under penalty of perjury. To be admissible in court and for maximum reliability and validity, written testimony is usually witnessed by one or more persons who swear or affirm its authenticity also under penalty of perjury. Unless a witness is testifying as an expert witness, testimony in the form of opinions or inferences is generally limited to those opinions or inferences that are rationally based on the perceptions of the witness and are helpful to a clear understanding of the witness' testimony.
Legitimate expert witnesses with a genuine understanding of legal process and the inherent dangers of false or misleading testimony refrain from making statements of fact. They also recognize that they are in fact not witnesses to an alleged crime or other event in any way, shape or form. Their expertise is in the examination of evidence or relevant facts in the case. They should make no firm judgement or claim or accusation about any aspect of the case outside their narrow range of expertise. They also should not allege any fact they can't immediately and credibly prove scientifically.
For example, a hair sample from a crime scene entered as evidence by the prosecution should be described by an expert witness as "consistent with" a sample collected from the defendant, rather than being described as a "match". A wide range of factors make it physically impossible to prove for certain that two hair or tissue samples came from a common source.
Having not actually witnessed the defendant at the scene, the expert witness can not state for a fact that the sample is a match to the defendant, particularly when the samples were collected at different times and different places by different collectors using different collection methods. Ultimately, the testimony of expert witnesses is regarded as supportive of evidence rather than evidence in and of itself, and a good defense attorney will point out that the expert witness is not in fact a witness to anything, but rather an observer.
When a witness is asked a question, the opposing attorney can raise an objection, which is a legal move to disallow or prevent an improper question to others, preferably before the witness answers, and mentioning one of the standard reasons, including:

argumentative
asked and answered
best evidence rule
calls for speculation
calls for a conclusion
compound question or narrative
hearsay
inflammatory
incompetent witness (e.g., child, mental or physical impairment, intoxicated)
irrelevant, immaterial (the words "irrelevant" and "immaterial" have the same meaning under the Federal Rules of Evidence. Historically, irrelevant evidence referred to evidence that has no probative value, i.e., does not tend to prove any fact. Immaterial refers to evidence that is probative, but not as to any fact material to the case. See Black's Law Dictionary, 7th Ed.).
lack of foundation
leading question
privilege
vague
ultimate issue testimonyThere may also be an objection to the answer, including:

non-responsiveUp until the mid-20th century, in much of the United States, an attorney often had to follow an objection with an exception to preserve the issue for appeal. If an attorney failed to "take an exception" immediately after the court's ruling on the objection, he waived his client's right to appeal the issue. Exceptions have since been abolished, due to the widespread recognition that forcing lawyers to take them was a waste of time.
When a party uses the testimony of a witness to show proof, the opposing party often attempts to impeach the witness. This may be done using cross-examination, calling into question the witness's competence, or by attacking the character or habit of the witness. So, for example, if a witness testifies that he remembers seeing a person at 2:00 pm on a Tuesday and his habit is to be at his desk job on Tuesday, then the opposing party would try to impeach his testimony related to that event.


== Religion ==
Christians in general, especially within the Evangelical tradition, use the term "to testify" or "to give one's testimony" to mean "to tell the story of how one became a Christian". Commonly it may refer to a specific event in a Christian's life in which God did something deemed particularly worth sharing. Christians often give their testimony at their own baptism or at evangelistic events. Many Christians have also published their testimonies on the internet.


=== Types ===
Many holiness churches devote a portion of their mid-week service to allow members to give a personal testimony about their faith and experiences in living the Christian life.In the Religious Society of Friends, the word testimony is used to refer to the ways in which Friends testify or bear witness to their beliefs in their everyday lives. In this context, the word testimony refers not to the underlying belief, but the committed action which arises out of their beliefs, which testifies to their beliefs. Common areas in which modern Friends are said to testify include testimony towards peace, testimony to simplicity, testimony to truth and integrity, and testimony to equality.
In some religions (most notably Mormonism and Islam) many adherents testify as a profession of their faith, often to a congregation of believers. In Mormonism, testifying is also referred to as "bearing one's testimony", and often involves the sharing of personal experience—ranging from a simple anecdote to an account of personal revelation—followed by a statement of belief that has been confirmed by this experience. Within Mormon culture, the word "testimony" has become synonymous with "belief". Although "testimony" and "belief" are often used interchangeably, they are inherently different. Most Mormons believe that when faith is acted upon, individuals can receive a spiritual witness which solidifies belief into testimony. Mormons are taught that if the exercise of faith brings forth good works, they can know their religious principles are true. An individual who no longer believes in the religion is referred to as having "lost their testimony".


== Large-group awareness training ==
In the context of large-group awareness training, anecdotal testimony may operate in the forms of "sharing" or delivering a "share".


== Literature ==

Some published oral or written autobiographical narratives are considered "testimonial literature" particularly when they present evidence or first person accounts of human rights abuses, violence and war, and living under conditions of social oppression. This usage of the term comes originally from Latin America and the Spanish term "testimonio" when it emerged from human rights tribunals,  truth commissions, and other international human rights instruments in countries such as Chile and Argentina. One of the most famous, though controversial, of these works to be translated into English is I, Rigoberta Menchú. The autobiographies of Frederick Douglass can be considered among the earliest significant English-language works in this genre.


== Philosophy ==
In philosophy, testimony is a proposition conveyed by one entity (person or group) to another entity, whether through speech or writing or through facial expression, that is based on the entity's knowledge base. The proposition believed on the basis of a testimony is justified if conditions are met which assess, among other things, the speaker's reliability (whether her testimony is true often) and the hearer's possession of positive reasons (for instance, that the speaker is unbiased).We can also, rationally accept a claim on the basis of another person's testimony unless at least one of the following is found to be true:

The claim is implausible;
The person or the source in which the claim is quoted lacks credibility;
The claim goes beyond what the person could know from his or her own experience and competence.


== See also ==


== References ==


== External links ==
Bethel Music Testimonies
Examples of Christian testimonies