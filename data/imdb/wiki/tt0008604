Seventh Son of a Seventh Son is the seventh studio album by English heavy metal band Iron Maiden. It was released on 11 April 1988 in the United Kingdom by EMI Records and in the United States by Capitol Records. A concept album, it incorporates elements of progressive rock, seen in the length and complex structure of the title track. It was the band's last studio album to feature the Piece of Mind-era lineup until 2000's Brave New World, with guitarist Adrian Smith leaving the band in January 1990 after he did not approve of the direction the band were aiming for on their next album No Prayer for the Dying. It was also the first Iron Maiden album to feature keyboards.
Like The Number of the Beast and later Fear of the Dark, The Final Frontier, and The Book of Souls, the album debuted at number one on the UK Albums Chart.


== Background, writing and concept ==
The idea to base the album around the folklore concept of the seventh son of a seventh son came to bassist Steve Harris after he read Orson Scott Card's Seventh Son. Harris stated, "It was our seventh studio album and I didn't have a title for it or any ideas at all. Then I read the story of the seventh son, this mystical figure that was supposed to have all these paranormal gifts, like second sight and what have you, and it was more, at first, that it was just a good title for the seventh album, you know? But then I rang Bruce [Dickinson, vocalist] and started talking about it and the idea just grew."After his songwriting contributions were rejected from the band's previous album, 1986's Somewhere in Time, Dickinson felt that his role within the band had diminished, as he "just became the singer", but felt renewed enthusiasm when Harris explained the concept to him; "I thought, 'What a great idea! Brilliant!' And of course I was really chuffed, too, because he'd actually rung me to talk about it and ask me if I had any songs that might fit that sort of theme. I was like, 'Well, no, but give me a minute and I'll see what I can do.'" Speaking about the record in later years, however, Dickinson remarked that "we almost did [something great]", explaining that, "it was only half a concept album. There was no attempt to see it all the way through, like we really should have done. Seventh Son... has no story. It's about good and evil, heaven and hell, but isn't every Iron Maiden record?"In addition to Dickinson's return to writing, the album was also notable for its number of co-written pieces, in contrast to its predecessor, with five of the eight tracks being collaborative efforts. According to Harris, this was probably because they "spent more time checking up on each other to see what everybody else was up to, just to make sure the story fitted properly and went somewhere". To make sure each song fit with the record's concept, the band drew up a basic outline for the story, which Harris states "didn't make the actual writing any easier ... I probably took longer over the writing I've done on this album than any I've done before. But the stuff we all started coming up with, once we'd agreed that we were definitely going for a fully fledged 'concept' album, really startled me. It was so much better than anything we'd done in ages".Stylistically, Seventh Son of a Seventh Son developed the sounds first heard on Somewhere in Time, although, on this occasion, the synth effects were created by keyboards rather than bass or guitar synthesisers. According to Dickinson, the band decided not to hire a keyboard player, with the parts being "mainly one-finger stuff from Adrian [Smith, guitarist], Steve, the engineer or whoever had a finger free at the time". Harris was fond of the development, in spite of the fact that the record did not sell as well as its predecessor in the United States; "I thought it was the best album we did since Piece of Mind. I loved it because it was more progressive—I thought the keyboards really fitted in brilliantly—'cause that's the influences I grew up with, and I was so pissed off with the Americans, because they didn't really seem to accept it. Everyone said afterwards that it was a European-sounding album. I'm not so sure about that. What's a European-sounding album? To me, it's just a Maiden-sounding album."

Seventh Son of a Seventh Son and its supporting tour marked the last appearance of Adrian Smith until he returned to the band in 1999. The guitarist left during the pre-production stages of the band's following album, 1990's No Prayer for the Dying, as he was unhappy with the more "street-level" direction the group were taking, professing that he "thought we were heading in the right direction with the last two albums" and that he "thought we needed to keep going forward, and it just didn't feel like that to me".


== Songs ==
"The Clairvoyant" was the first track written for the album. According to Steve Harris, the song's lyrics were inspired by the death of psychic Doris Stokes, after which he wondered to himself whether "she could foresee her own death". Harris then began to write the song "Seventh Son of a Seventh Son", which gave him the idea of turning the full album into a concept record given that the main character would also have the power of clairvoyance.According to Smith, the song "Can I Play with Madness" "actually started life as a ballad I had been working on called 'On the Wings of Eagles'. Then Bruce had a verse for it but wanted to change the title to 'Can I Play with Madness'. I must admit, it did sound better that way. So we took that one and Steve liked it, too. It was Steve who came up with the time change in the middle and the instrumental passage, which again gave it that lift it needed." According to Dickinson, however, Harris' addition resulted in "a big row ... Adrian absolutely hated it."Of the album's remaining songs, Metal Hammer states that "Moonchild" is loosely based on the Aleister Crowley novel of the same name, while "Infinite Dreams" is about a character who "implores a spiritualist to unlock the meaning behind his tortured dreams", although Sputnikmusic state that the song also explores "themes of reality, life after death, and the meaning of life". The final track, "Only the Good Die Young", closes the storyline and was later featured in an episode of the 1980s' TV series Miami Vice. The record opens and closes with an identical brief acoustic piece accompanied by two verses of lyrics, written by Dickinson, which, according to Sputnikmusic, "foreshadows doom and failure for the protagonist" and "wraps up the album"."The Evil That Men Do", "The Clairvoyant" and "Can I Play with Madness" have been played live the most frequently following the Seventh Tour of a Seventh Tour.


== Promotion ==
To promote the album, the band hosted an evening of television, radio and press interviews at Castle Schnellenberg in Attendorn, Germany prior to the record's release, before holding a small number of "secret" club shows, under the name "Charlotte and the Harlots", at Empire, Cologne and L'Amour, New York. In May, the group set out on a supporting tour which saw them perform to more than two million people worldwide over seven months. In August, the band headlined the Monsters of Rock festival at Donington Park for the first time before a crowd of 107,000, the largest in Donington's history, and recorded a concert video, entitled Maiden England at the NEC, Birmingham in November. To recreate the album's keyboards onstage, the group recruited Michael Kenney, Steve Harris' bass technician, to play the keys throughout the tour, during which he would perform the song "Seventh Son of a Seventh Son" on a forklift truck under the alias of "The Count" (for which he would wear a black cape and mask).


== Cover artwork ==
According to Rod Smallwood, the band's manager, the brief given to Derek Riggs (the group's then regular artist) was, unlike with previous albums, to create "simply something surreal and bloody weird". Riggs confirms that "they said they wanted one of my surreal things. 'It's about prophecy and seeing into the future, and we want one of your surreal things.' That was the brief ... I had a limited time to do the picture, and I thought it was pretty weird their concept, so I just went with that."According to Dickinson, his revitalised enthusiasm, brought about by Harris's idea to make a concept album, carried forward into the cover artwork, saying, "I was probably responsible in a large part for the cover, with Derek." Dickinson states that the idea to set the painting in a polar landscape may have originated from when he showed Riggs a Gustave Doré piece, depicting traitors frozen in a lake of ice in the ninth circle of Dante's Inferno. In contradiction of this, Riggs states that the setting was because he "might have just seen a documentary about the North Pole or something ... I wanted something that was a departure from all the cityscapes and things. It was about prophecy and seeing the future, and so I just wanted something distant. And then they said, on the back, 'Could you stick all the other Eddies in the ice?' So I did."Speaking about the depiction of the band's mascot, Eddie, Riggs states that "I thought, you know, I don't feel like painting all of Eddie, so I'll get rid of him. I'll chop him off, and make it look kind of non-pleasant." In addition to the lobotomy and cyborg enhancements, left over from the Piece of Mind and Somewhere in Time album covers respectively, this incarnation also comes with an in utero baby in his left hand and an apple, inspired by the Garden of Eden and featuring a red and green yin and yang. On top of this, Eddie's head is on fire, which Riggs states is "a symbol for inspiration", an idea which he "stole" from Arthur Brown.


== Critical reception ==
The album has received consistent critical praise since its release, with AllMusic rating it 4.5 out of 5, saying that the addition of keyboards "restores the crunch that was sometimes lacking in the shinier production of the previous album" and that it "ranks among their best work". Sputnikmusic scored the album 4 out of 5, and, while they state that "the band has better releases, such as Powerslave and Somewhere in Time", they argue that it is "lyrically ... one of Maiden's finest efforts". In 2005, the album was ranked No. 305 in Rock Hard magazine's book of The 500 Greatest Rock & Metal Albums of All Time. In his biography of Maiden Paul Stenning stated, "this line-up of the group hit its peak in 1988." The album ranked #11 in Loudwire magazine's list "Top 25 Progressive Metal Albums of All Time."Although Geoff Barton states that contemporary reviews contained "a definite reaction against Maiden emphasising their prog-rock pretensions", and that "one critic ... slammed Maiden for Seventh Son... and accused them of having regressed into Genesis-style prog rockers from the 70s", Kerrang! were extremely positive upon the album's release, awarding full marks and stating that "[with Seventh Son of a Seventh Son] Iron Maiden have given rock music back its direction and its pride" and that the record "will eventually be hailed alongside such past milestones as Tommy, Tubular Bells and [The] Dark Side of the Moon."


== Commercial performance ==
The album debuted at No. 1 in the UK Albums Chart (their first since The Number of the Beast) as well as No. 12 in the US, while the singles "Can I Play with Madness", "The Evil That Men Do" and "The Clairvoyant (live)" reached No. 3, No. 5 and No. 6 positions respectively in the UK Singles Chart. Smith highlights "Can I Play with Madness" as "our first proper hit single."


== Track listing ==


=== Notes ===
The US version of the 1995 reissue bonus disc features a somewhat different track order, with "Infinite Dreams" playing after "The Prisoner".


== Personnel ==
Production and performance credits are adapted from the album liner notes.


=== Iron Maiden ===
Bruce Dickinson – lead vocals
Dave Murray – guitar
Adrian Smith – guitar, synthesiser
Steve Harris – bass guitar, string synthesiser
Nicko McBrain – drums, percussion


=== Production ===
Martin "Disappearing Armchair" Birch – producer, engineer, mixing, tape operator
Stephane Wissner – engineer, assistant engineer
Bernd Maier – engineer, assistant engineer
George Marino – mastering engineer
Derek Riggs – sleeve illustrations
Ross Halfin – photography
Rod Smallwood – management, sleeve concept
Andy Taylor – management
Hugh Gilmour – reissue design (1998 edition)


== Charts ==


==== Notes ====


== Certifications ==


== References ==