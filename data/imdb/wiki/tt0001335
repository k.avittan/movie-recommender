Magdalen College ( MAWD-lin) is a constituent college of the University of Oxford. It was founded in 1458 by William of Waynflete. Today, it is the fourth wealthiest college, with a financial endowment of £332.1 million as of 2019 and one of the strongest academically, setting the record for the highest Norrington Score in 2010 and topping the table twice since then. It is home to several of the University's distinguished chairs, including the Agnelli-Serena Professorship, the Sherardian Professorship, and the four Waynflete Professorships.
The large, square Magdalen Tower is an Oxford landmark, and it is a tradition, dating to the days of Henry VII, that the college choir sings from the top of it at 6 a.m. on May Morning. The college stands next to the River Cherwell and the University of Oxford Botanic Garden. Within its grounds are a deer park and Addison's Walk.


== History ==


=== Foundation ===
Magdalen College was founded in 1458 by William of Waynflete, Bishop of Winchester and Lord Chancellor and named after St Mary Magdalene. The College succeeded a university hall called Magdalen Hall, founded by Waynflete in 1448, and from which the college drew most of its earliest scholars. Magdalen Hall was suppressed when the College was founded. The name was revived for a second Magdalen Hall, established in the college's grounds around 1490, which in the 19th century was moved to Catte Street and became Hertford College. 
Waynflete also established a school, now Magdalen College School, an independent school located nearby on the other side of the Cherwell. Waynflete was assisted by a large bequest from Sir John Fastolf, who wished to fund a religious college.Magdalen College took over the site of St John the Baptist Hospital, alongside the Cherwell, initially using the hospital's buildings until new construction was completed between 1470–1480. At incorporation in 1458, the college consisted of a president and six scholars. In 1487 when the Founder's Statutes were written, the foundation consisted of a President, 40 fellows, 30 demies, four chaplain priests, eight clerks, 16 choristers, and appointed to the Grammar School, a Master and an usher.The founder's statutes included provision for a choral foundation of men and boys (a tradition that has continued to the present day) and made reference to the pronunciation of the name of the college in English. The college's name is pronounced like the adjective maudlin because the late medieval English name of Mary Magdalene was Maudelen, derived from the Old French Madelaine.


=== English Civil War ===
Oxford and Magdalen College were supporters of the Royalist cause during the English Civil War. In 1642, Magdalen College donated over 296 lbs of plate to fund the war effort – the largest donation by weight of any Oxford college. Magdalen College, commanding a position on the banks of the Cherwell that overlooked Magdalen Bridge and the road from London, had tactical significance for the King's forces. Royalist ordnance was parked in the Magdalen's Grove, and Prince Rupert is thought to have quartered in the college. During the first Siege of Oxford, Charles I surveyed the battle from Magdalen Tower.


=== Expulsion of the Fellows ===

During the 1680s, King James II made several moves to reintroduce Catholicism into the then Anglican university. In 1687, he attempted to install Anthony Farmer as president of Magdalen. The fellows rejected this, not just because Farmer was reputedly a Catholic and had a tarnished reputation, but also as he was not a fellow of the college, and therefore ineligible under the statutes. The fellows elected instead one of their own, John Hough. James eventually offered a compromise candidate in the form of the moderate Bishop of Oxford, Samuel Parker, but he too was rejected by the fellows as they considered the role filled. Parker was admitted by force and the fellows and demies (scholars) who had defied the king were expelled, replaced by the king's choice of Catholics or moderate Anglicans. Parker died in 1688 and was replaced by Bonaventure Giffard, a Catholic under whose tenure the Chapel converted to Catholicism. The expulsion of the fellows marked a turning point in the University's relationship with the Crown: Brockliss writes, "the royalist and Anglican University established at the Restoration had had to make a choice and it had chosen Anglicanism." James' interference with the college fed resentment in Anglicans who used it as evidence that his rule was autocratic. On 25 October 1688, shortly before the Glorious Revolution and overthrow of James II by William of Orange, James' appointments were reversed and Hough and the expelled fellows were restored to the college. This event is marked every year at a special banquet, the Restoration Dinner, for Magdalen fellows, demies, and academic clerks.


=== 20th–21st centuries ===
Magdalen's prominence since the mid-20th century owes much to such famous fellows as C. S. Lewis and A. J. P. Taylor, and its academic success to the work of such dons as Thomas Dewar Weldon. During World War II, RAF Maintenance Command was headquartered at Magdalen.Magdalen College owns and manages the Oxford Science Park to the south of Oxford, a science and technology park home to over 100 companies. The Oxford Science Park opened in 1991, with Magdalen as part owner. The college acquired the rest of the ownership in 2016.Like many of Oxford's colleges, Magdalen admitted its first mixed-sex cohort in 1979, after more than half a millennium as a men-only institution. Between 2015 and 2017, 47.2% of UK undergraduates admitted to Magdalen were from state schools; 12.2% were of BME ("black and ethnic minority") heritage and 0.7% were black. Of the 300 undergraduate offers made by Magdalen between 2017 and 2019, 25 (one in twelve) went to pupils from Eton College or Westminster School. In 2015, Magdalen topped Oxford's Norrington Table of college undergraduate examination results, and its average score over the 2006–2016 period is the best among the colleges.


== Buildings ==

The college grounds stretch north and east from the college, and include most of the area bounded by Longwall Street, the High Street (where the porter's lodge is located), and St Clement's. The college features a variety of architectural styles, and has been described as "a medieval nucleus with two incomplete additions, one from the eighteenth and one from the nineteenth century".The college is organised around five quads. The irregularly shaped St John's Quad is the first on entering the college, and includes the Outdoor Pulpit and old Grammar Hall. It connects to the Great Quad (the Cloister) via the Perpendicular Gothic Founders Tower, which is richly decorated with carvings and pinnacles and has carved bosses in its vault. The Chaplain's Quad runs along the side of the Chapel and Hall, to the foot of the Great Tower. St Swithun's Quad and Longwall Quad (which contains the Library) date from the late 19th and early 20th centuries, and make up the southwest corner of the college.


=== Original buildings ===
The college is built on the site of St John the Baptist Hospital, which was dissolved in 1457 and its property granted to William of Waynflete. Some of the hospital buildings were reused by the college, and the kitchens survive today as the college bar, the Old Kitchen Bar.New construction began in 1470 with the erection of a wall around the site by mason William Orchard. Following this, Orchard also worked on the chapel, hall, and the cloister, including the Muniment and Founder's Towers, with work completed around 1480.


=== Cloister ===

The Cloister or Great Quad is the "medieval nucleus" of the college. It was constructed between 1474 and 1480, also by Orchard, although several modifications were made later. Access to the Cloister from St John's Quad is via the Founder's Tower or Muniment Tower. The chapel and the hall make up the southern side of the quad. It is also home to the junior, middle, and senior common rooms, and the old library.
In 1508, grotesques known as hieroglyphics were added to the Cloister. These are thought to be allegorical, and include four hieroglyphics in front of the old library that represent scholarly subjects: science, medicine, law, and theology. The other hieroglyphics have been assigned symbolism relating to virtues that should be encouraged by the college (e.g. the lion and pelican grotesques in front of the Senior Common Room representing courage and parental affection) or vices that should be avoided (the manticore, boxers, and lamia in front of the Junior Common Room, representing pride, contention, and lust). In 2017, repair work was undertaken to restore the severely damaged boxers statue.In 1822, the north side of the Cloister was knocked down, ostensibly due to disrepair. This decision was controversial, provoking protests from the fellows and in the contemporary press, and it was rebuilt shortly afterwards.In the early 1900s, renovations were performed, and it was returned to a more medieval character. Student rooms were installed in the (very large) roof space in the 1980s.


=== Chapel ===

The chapel is a place of worship for members of the college and others in the University of Oxford community and beyond. As a High Anglican chapel, its tradition is influenced by the Catholic Revival in the Church of England. Said and sung services are held daily during term. The choir sings Choral Evensong or Evening Prayer every day at 6:00 pm except on Mondays. On Sundays, a Sung Eucharist is offered in the morning at 11:00 am, whilst Compline (Night Prayer) is sung several times per term. Mass is also sung on major holy days.The chapel itself is a grade I listed building built between 1474–1480. The roof, giving the impression of a stone vaulted ceiling, is in fact a facsimile made from plaster added in 1790 by James Wyatt. Wyatt's redevelopment of the chapel included a number of modifications to make it more Gothic in character, but other than the ceiling, Wyatt's contributions were removed during a later redesign in 1828.The stained glass windows facing St John's Quad feature a grisaille depiction of the Last Judgement. These windows, dating from 1792, are a reconstruction by Francis Eginton of an earlier 17th-century window that was destroyed in a storm. It had been uninstalled during World War II to protect it from damage, and was only restored in the 1990s. Much of the glass had been thought lost, until it was rediscovered in the ventilation tunnels under the New Building.


=== Magdalen Tower ===

Construction of Magdalen's Great Tower began in 1492 by another mason, William Raynold. It might have been intended to replace an existing belfry remaining from the hospital, and probably was originally envisioned to stand alone. By the time it was completed in 1509, additional buildings had been built either side, creating the roughly triangular Chaplain's quad between the chapel and the High.The tower contains a peal of ten bells hung for English change ringing. They were cast at a number of different foundries and the heaviest, weighing 17 cwt, was cast in 1623.The tower is 144 feet tall and an imposing landmark on the eastern approaches to the city centre. It has been the model for other towers, including Mitchell Tower of the University of Chicago, Manhattan's First Presbyterian Church, and All Saints' Church in Churchill, Oxfordshire. It forms the centre of the May Morning celebrations in Oxford.


=== The New Building ===

During the 18th and 19th centuries, there were numerous attempts made to redesign the site to better suit the College's needs. The New Building began construction in 1733 as a part of Edward Holdsworth's designs from 1731. It is built in a Palladian style, and features a colonnade.
It was conceived as one side of a new "Great Quadrangle", and in anticipation of this the building's ends had been left unfinished. However, Holdsworth's full vision was never completed. The idea was revisited several times by later architects, including by architects James Wyatt—who plans (never realised) included partially demolishing the existing, Medieval quad (the Cloister) and refinishing the neoclassical New Building in a Georgian Gothic style—and John Buckler. In the 19th Century, John Nash and Humphrey Repton both submitted designs for new, open quadrangles that incorporated the New Building.Ultimately, the idea of integrating the New Building into a new quad was abandoned, and the ends of the building were finally completed in 1824 with two returns designed by Thomas Harrison. Today, it stands apart from the Cloister, overlooking four croquet lawns on one side and the Grove deer park on the other. It is used for accommodation for undergraduates and fellows, including historically Edward Gibbon and C. S. Lewis, and also houses the wine cellar.


=== Daubeny laboratory ===

Opposite the main college site and overlooking the Botanic Garden is the 19th century Daubeny Laboratory.The Garden had been established between 1622 and 1633 as a physic garden (that is, a garden to study the medicinal value of plants) on land inherited by Magdalen from St. John's Hospital. The Daubeny Laboratory, and neighbouring Professor's House, were founded by the polymath and Magdalen fellow Charles Daubeny after he was appointed to the Sherardian Chair of Botany in 1834.Daubeny set about a number of additions to the location, erecting new glasshouses and in 1836 creating an on-site residence for the Professor of Botany. This replaced an earlier residence that had been demolished in 1795 when the road was widened. The new residence was an extension of the library, which had been created out of a glasshouse by an earlier Sherardian professor, John Sibthorp, to house the Sherard herbarium. After Daubeny's death, this was assimilated to house the growing collection. Later, it became accommodation for graduate students, the Professor's House, while the Sherard Herbarium is now part of the Fielding-Druce Herbarium held in the Department of Plant Sciences.Daubeny, who was also the Aldrichian Professor of Chemistry, had found the chemistry laboratory in the basement of the old Ashmolean Museum, what is now the History of Science Museum, to be "notoriously unworthy of a great University" and desired a better science facility. He petitioned the College to be allowed to build one, and the Daubeny laboratory was completed in 1848. The Daubeny Laboratory was preceded by the anatomy school and laboratory at Christ Church which opened in 1767, and would be followed later in the century by other college laboratories including the Balliol-Trinity Laboratories. Daubeny's laboratory was a two-storey room with benches and cupboards encircled by a gallery, and became the principal chemistry lab for the University. In 1902, due to growing student numbers and poor ventilation, the laboratory trappings were removed and it was refitted as a lecture hall. In 1973, most of the Daubeny Laboratory building was reconfigured into graduate student accommodation. The Daubeny lab itself is now a conference space.


=== St Swithun's quad ===

In 1880–1884, the college extended westwards onto the former site of Magdalen Hall. The hall was an independent university hall that developed from Magdalen College School, not the earlier Magdalen Hall founded by William Waynflete. Most of Magdalen Hall's buildings were destroyed by fire in 1820, though the Grammar Hall survived and was restored by Joseph Parkinson. The hall moved to Catte Street in 1822 and was incorporated as Hertford College in 1874.The new construction, St Swithun's quad (sometimes given as St. Swithin's quad), was designed by George Frederick Bodley and Thomas Garner in keeping with the Gothic style. They had originally designed three sides of a square, though only the south and west sides were built. In 1928, Giles Gilbert Scott extended the building north and westwards, forming the adjacent Longwall quad.


=== Modern buildings and acquisitions ===

Several new additions to the college were made in the late 20th century. The Waynflete Building, which is located across Magdalen Bridge from the main college site, was designed by Booth, Ledeboer, and Pinckheard and completed in 1964. Magdalen has a number of additional annexes near to the main site for accommodation, including in Cowley Place and Longwall Street.
The Grove Buildings, located north of Longwall quad between Longwall Street and the Grove, were built in 1994–1999 by Porphyrios Associates. They are home to accommodation, Magdalen's 160-seat auditorium, and the Denning Law Library. During term time, the auditorium hosts film screenings organised by the Magdalen Film Society.Along Addison's Walk is the Holywell Ford site, where most of the graduate accommodation is located. Holywell Ford house was built by Clapton Crabb Rolfe in 1888 on the location of an older mill, and was acquired by Magdalen in the 1970s. Additional blocks of accommodation were built in 1994-5 by RH Partnership Ltd.


=== Libraries ===

In addition to the University's central and departmental libraries, Oxford's colleges maintain their own libraries. The original college library, the Old Library, is located in the Cloister and accessed via Founder's Tower or the President's Lodgings. It contains a large collection of manuscripts from before the 19th century. Consultation of material is typically by appointment, although the Old Library itself may be visited by the public during certain exhibitions.In 1931, the New Library, now called the Longwall Library, was established in the former Magdalen College School building in Longwall Quad and became the college's main library for students. It was opened by Edward VIII when he was a student at Magdalen. It was renovated between 2014 and 2016 by Wright & Wright Architects and reopened by Prince William, Duke of Cambridge.In addition, the college maintains the Denning Law Library in the Grove building, a reference library for Magdalen's law students, and the specialist Daubeny and McFarlane collections of 19th century scientific works and medieval history works respectively. Items from the Daubeny and McFarlane libraries may be brought to the Longwall Library for consultation on request.


== Grounds ==


=== The Grove ===

The Grove or deer park is a large meadow which occupies most of the north west of the college's grounds, from the New Building and the Grove Buildings to Holywell Ford. During the winter and spring, it is the home of a herd of fallow deer. It is possible to view the meadow and the deer from the path between New Buildings and Grove Quad, and also from the archway in New Buildings.
In the 16th century, long before the introduction of the deer, the grove consisted of gardens, orchards, and bowling greens. During the Civil War, it was used to house a regiment of soldiers. At one point in the 19th century it was home to three traction engines belonging to the works department of the college.  By the 20th century it had become well-wooded with many large trees, but most of them were lost to Dutch elm disease in the 1970s.


=== Water meadow and Addison's Walk ===

The water meadow is a flood-meadow to the eastern side of the College, bounded on all sides by the Cherwell. In wet winters, some or all of the meadow may flood, as the meadow is lower lying than the surrounding path. All around the edge of the meadow is a tree-lined path, Addison's Walk, named for the fellow Joseph Addison (1672–1719), which connects to Holywell Ford and the Fellows' Garden.Addison's Walk is popular with College members and visitors. C. S. Lewis wrote a poem about the walk, Chanson d'Aventure or What the Bird Said Early in the Year, which is commemorated on a plaque near the gate to Holywell Ford.Thanks to the frequent flooding, the meadow is one of the few places in the UK that the snake's head fritillary, Fritillaria meleagris, may be seen growing wild. These flowers grow in very few places, and have been recorded growing in the meadow since around 1785. Once the flowering has finished, the deer herd is moved in for the summer and autumn.


=== Bat Willow meadow and the Fellows' Garden ===

Further east of the water meadow are Bat Willow meadow and the Fellows' Garden. They are separated from the water meadow and each other by other branches of the Cherwell, and may be accessed from Addison's Walk. Bat Willow meadow features Y, a 10 metre high sculpture of a branching tree by Mark Wallinger, commissioned for the College's 550th anniversary in 2008. Due to their age and infection with honey fungus, the willow trees were cut down in 2018 and replanted, and the wood used to make cricket bats.The Fellows' Garden is located further north along the bank of the Cherwell than Bat Willow meadow, directly behind the Oxford Centre for Islamic Studies. This long and narrow garden follows the Cherwell to the edge of the University Parks. In spring, the ground is covered with flowers. In summer, there are some flowers, many different shrubs, and the varied trees provide dappled cover from the sun.
Further north is Magdalen's sports ground.


== Choir ==

Magdalen is one of the three choral foundations in Oxford, meaning that the formation of the choir was part of the statutes of the college, the other choral foundations being New College and Christ Church.The choir consists of 12 academical clerks, or choral scholars, who are students at the college, and 16 boys aged seven to thirteen (some of whom stay on until age fourteen), all of whom have scholarships at Magdalen College School. The school was founded for this purpose but has long since become an independent public school.
The choristers' day begins at 7:30, with an early morning practice before school. There is further practice immediately after school, followed by Choral Evensong six nights a week, in term; the Tuesday service is sung by the boys only, and the Friday service only by the Academical Clerks. On Sundays there is a practice at 9:30 am followed by Eucharist, then a further afternoon practice followed by Evensong which ends at 7:00 pm. As of September 2010, the choristers no longer sing on most Saturday evenings following the formation of the Consort of Voices, a mixed voice choir which sings on most Saturdays in full term.
The choir records regularly. In 2005, it was nominated for a Grammy Award for its CD, With a Merrie Noyse, of music by Orlando Gibbons. Other recent works include the BBC's The Blue Planet and Paul McCartney's classical piece Ecce Cor Meum. The choir has numerous college duties as well as a recording and touring schedule. Traditionally the choir sings at college Gaudies and at other special events throughout the year, as well as performing on social occasions such as Carols by Candlelight before Christmas and the famous May Morning.


=== May Morning ===

Annually at 6 a.m. on 1 May, the choir perform Hymnus Eucharisticus from the top of Magdalen's Tower to crowds below on Madgalen Bridge and the High Street. This tradition dates back 500 years, and forms the central event in Oxford's May Morning celebration.


=== Organists ===

The choir has had many well-known organists, such as Daniel Purcell, Sir John Stainer (1860–1872) and Bernard Rose (1957–1981). Among the other notable former directors and masters of the choir, who hold the title of Informator Choristarum, are John Sheppard (1543–c.1552), Sir William McKie, Haldane Campbell Stewart and the composer Bill Ives (1991–2009), possibly better known as a former King's Singer. A disc of his music, Listen Sweet Dove, is amongst the choir's releases.
The current Informator Choristarum, since January 2017, is Mark Williams. He succeeded Daniel Hyde, who had been organ scholar at King's College Cambridge, following Hyde's appointment as Organist and Director of Music of Saint Thomas Church, New York.Past organ scholars include Dudley Moore, Matthew Martin and Paul Brough. Past academical clerks include John Mark Ainsley, Harry Christophers (founder and director of The Sixteen), James Whitbourn, Robin Blaze, Roderick Williams and conductor/composer Gregory Rose.


== Student life ==


=== Accommodation ===

Undergraduate students of the college are guaranteed accommodation during term for their entire degree, typically in the Waynflete building in their first year and "inside-walls" in the Cloister, St Swithun's Quad, the New Building and so on in subsequent years. Graduate students are guaranteed at least two years of accommodation. Unlike undergraduates, graduates are not required to move out between terms and typically live "outside walls", including in Holywell Ford, the Daubeny Laboratory, and Professor's House.Accommodation charges are inclusive of heating, power, and internet access, and weekly cleaning by the college scouts (housekeepers), but does not include catering. Three cafeteria style meals a day are served in the hall, and other food is available in the Old Kitchen Bar.In addition to a dinner cafeteria service served in the hall, three Formal Halls are held a week during term time. These are three course sit-down dinners and require college members to wear their gowns. Additional banquets commemorate special occasions, including the Restoration Dinner.


=== Events and societies ===

The body of undergraduate and graduate students are known as the junior and middle common rooms (JCR and MCR) respectively. They each elect committees of students annually to organise welfare events, socials, and banquets.In addition to clubs and societies associated with the Oxford University Student Union operated at the university level, Magdalen members may also participate in several college societies. The Atkin Society and the Sherrington Society are two subject-specific societies, for law students and medicine students respectively. They organise talks and social events. The Atkin society is named for lawyer James Atkin, Baron Atkin, a former demy at Magdalen, and also organises an annual moot court. The Sherrington Society is named after Nobel laureate Sir Charles Scott Sherrington, former Waynflete Professor of Physiology. The college also has a poetry discussion forum called the Florio Society, named for 16th century college alumnus John Florio.A number of other societies put on events throughout the year. These include the Magdalen Players, a drama society; the Magdalen Music Society; and the Magdalen Film Society, which screens films during term time in the Grove Auditorium. The Magdalen College Music Society is a chapter of the Oxford University Music Society and incorporates a non-auditioned mixed choir, a chamber orchestra, and a saxophone ensemble. The society performs recitals in college on Thursdays during term time.The Magdalen College Trust is an independent charity that is funded by annual subscriptions from students and fellows. It encourages college members to engage in charity work, and funds charitable causes.


=== Academia ===
In the Norrington Table's history Magdalen has been top three times, in 2010, 2012 and 2015. When over half its finalists achieved firsts in 2010, it claimed the record for the highest ever Norrington Score.Magdalen College students set the record in the University Challenge television competition, winning on four occasions (1997, 1998, 2004, and 2011).


=== Sports ===
Magdalen members have access to a variety of sports facilities. The sports grounds, accessible from the main college via Addison's Walk, include pitches for cricket, soccer, hockey, and rugby; also available on site are tennis courts and squash courts. In addition, the college buys gym membership at the Iffley Road sports complex on behalf of all its students. The college keeps a boathouse on The Isis (the length of the Thames as it passes through Oxford) for the Magdalen College Boat Club (MCBC).The Magdalen College Boat Club (MCBC), a rowing club, was founded in 1859. It participates in the two annual Oxford bumps races, Eights Week and Torpids. In recent history, the MCBC men's rowers won Eights Week between 2004 and 2007, and the Torpids most recently in 2008 (for the men's rowers) and 2016 (women's).As well as the MCBC, Magdalen College is represented by teams in football, hockey, rugby, netball, cricket, lacrosse, squash and pool, amongst others.


== College stamp ==
A college stamp was issued in the 1960s and the 1970s to prepay local delivery of mail by the college porters. It was short-lived and only a few stamps exist. One on cover is known and is detailed in the Great Britain Philatelic Society Journal.


== Notable members ==
See also: List of alumni of Magdalen College, Oxford, List of Presidents of Magdalen College, Oxford, and Category:Fellows of Magdalen College, Oxford


=== Politics ===

Magdalen College has taught members of several royal families. These include King Edward VIII, who attended while Prince of Wales from 1912–1914, after which he left without graduating; Jigme Khesar Namgyel Wangchuck, the king of Bhutan, who read for an MPhil in politics in 2000; and Crown Prince Haji Al-Muhtadee Billah, first in line to the throne of Brunei, who enrolled in the Foreign Service Programme (now known as the Diplomatic Studies Programme) in 1995 under an assumed name.Among the political figures taught at Magdalen was Cardinal Thomas Wolsey, who studied theology. He graduated at 15, uncommonly early even for the time, but remained in Oxford for further study and eventually became a Fellow of Magdalen. Wolsey rose from humble origins to become Lord Chancellor and the Archbishop of York, obtaining great political power and becoming adviser to King Henry VIII. Wolsey left a lasting legacy in Oxford by founding Cardinal College, which Henry VIII would complete and refound as Christ Church after Wolsey's fall from power.More recent Magdalen alumni to become politicians include Malcolm Fraser, former Prime Minister of Australia, and John Turner, former Prime Minister of Canada. Many members of the UK Parliament have been alumni of Magdalen. In the current House of Commons sit alumni Jeremy Hunt MP and John Redwood MP. In the House of Lords sit alumni William Hague, Baron Hague of Richmond, former Leader of the Conservative Party; Dido Harding, Baroness Harding of Winscombe; John Hutton, Baron Hutton of Furness; Michael Jay, Baron Jay of Ewelme; Matt Ridley, 5th Viscount Ridley; and Stewart Wood, Baron Wood of Anfield, former Tutorial Fellow.


=== Arts ===


==== Literature ====

Joseph Addison, for whom Addison's walk is named, was a Fellow of Magdalen during the 17th century. He is known for his play Cato, a Tragedy based on the life of Cato the Younger at the end of the Roman Republic. Popular with the American Founding Fathers, the play may have served as a literary inspiration for the American Revolution.The 19th-century poet, playwright, and aesthete Oscar Wilde read Greats at Magdalen from 1874 to 1878. During this time, he won the University's Newdigate English Verse Prize and graduated with a double first. After his time at Magdalen, he became famous for his works including the novel The Picture of Dorian Gray and the play The Importance of Being Earnest.
Wilde began an affair in 1891 with Alfred Douglas, who was then himself a student at Magdalen. The disapproval of Douglas's father over Wilde's relationship with his son led to Wilde's prosecution and conviction in 1895 for "gross indecency", that is to say, homosexual behaviour, and a sentence to two years' hard labour. Wilde described "the two great turning-points in my life were when my father sent me to Oxford, and when society sent me to prison". After his release from prison, Wilde moved to France and spent the last three years of his life in poverty. He was posthumously pardoned in 2017 under Turing's Law.The prolific author Sir Compton Mackenzie OBE, who wrote over one hundred novels, plays, and biographies, read modern history at Magdalen. He is known for his fiction, including Sinister Street—which features St. Mary's College, Oxford as a stand-in for Magdalen—and Monarch of the Glen. Compton Mackenzie co-founded the Scottish National Party and was knighted in 1952.C. S. Lewis, writer and alumnus of University College, was a Fellow and English tutor at Magdalen for 29 years, from 1925 to 1954. Lewis was one of the Inklings, an informal writing society that also included J. R. R. Tolkien and would meet in Lewis's rooms at Magdalen. Under Lewis's tutelage was the future Poet Laureate Sir John Betjeman. Though Betjeman failed the maths portion of the entrance exams, he was offered a place to read English on the strength of his poetry, which had impressed the President of Magdalen and former Professor of Poetry Sir Thomas Herbert Warren. Lewis and Betjeman had a difficult relationship and Betjeman struggled academically. Betjeman left having failed to obtain a degree in 1928, but was made a doctor of letters by the university in 1974.Seamus Heaney, who received the Nobel Prize in Literature in 1995, was a Fellow of Magdalen from 1989 to 1994.


==== Theatre ====

The director Peter Brook CBE is both an alumnus and honorary Fellow of Magdalen. He was described in 2008 as "our greatest living theatre director". Fellow director Katie Mitchell OBE read English at Magdalen, and is known for her collaborations with Martin Crimp. In 2017, she received the President's Medal of the British Academy for her work in contemporary theatre and opera, and she has been described as British theatre's "king in exile".


==== Music ====
In 1957, the organist and composer Bernard Rose OBE was appointed Magdalen's Informator Choristarum, choir master. Among his students were Harry Christophers CBE, a composer and an artistic director for the Handel and Haydn Society who was an academical clerk and later honorary Fellow at Magdalen; and Dudley Moore CBE, comedic actor and jazz musician, who studied at Magdalen on an organ scholarship.Andrew Lloyd Webber, Baron Lloyd-Webber, composer of musicals including Evita and The Phantom of the Opera, studied history at Magdalen for a term in 1965, before dropping out to pursue music at the Royal Academy of Music. Andrew Lloyd Webber has received a number of awards for his work, including a lifetime achievement Tony Award.


=== Humanities ===

Hormuzd Rassam, the native Assyriologist, studied at Magdalen for 18 months between accompanying archaeologist Sir Austen Henry Layard on his first and second expeditions. When Layard retired from archaeology, the British Museum appointed Rassam to continue on his own. Rassam made several important discoveries: in 1853 at Nineveh, Rassam discovered the clay tablets that contained the Epic of Gilgamesh; in 1879 he discovered the Cyrus Cylinder in the ruins of Babylon; and in 1880–1881 he uncovered the city of Sippar. He was the first Middle Eastern archaeologist, but his contributions were dismissed by some of his contemporaries and by the end of his life, his name had been removed from plaques and visitor guides at the British Museum. Layard would describe him as "one whose services have never been acknowledged".The economist A. Michael Spence attended Magdalen on a Rhodes Scholarship, and graduated with a BA in mathematics in 1967. In 2001, he shared the Nobel Memorial Prize in Economic Sciences for his work on information flows. He is an honorary fellow at Magdalen.Philosopher A. C. Grayling CBE read for his DPhil at Magdalen, completing his studies in 1981. In 2011, he founded the New College of the Humanities. An analytic philosopher, Grayling is known for his criticism of religion, including in his 2013 book The God Argument, and his arguments for voting reform, as in his 2017 book Democracy and Its Crises.
Niall Ferguson, a well-known historian, also studied at Magdalen.


=== Sciences ===

Magdalen counts among its alumni several recipients of the Nobel Prize in Physiology or Medicine. Sir Howard Florey was an Australian pharmacologist who studied at Magdalen on a Rhodes Scholarship, graduating in 1924. He shared the Nobel Prize in Physiology or Medicine in 1945 for the development of penicillin. Sir Peter Medawar CBE read for a BA in zoology at Magdalen, receiving a first, and later for a DPhil, supervised by Florey. His research into tissue grafting and immune rejection led to the discover of acquired immune tolerance and became the basis of organ transplantation. For this work, he shared the 1960 Nobel Prize in Physiology or Medicine.Like Florey before him, Australian neurophysiologist Sir John Eccles also came to Magdalen on a Rhodes Scholarship, where he read for his DPhil. He was taught by an earlier neurophysiologist who received the Nobel in 1932, Sir Charles Scott Sherrington, who held the Waynflete Professorship in Physiology at Magdalen. In 1963 Eccles received the Nobel Prize in Physiology or Medicine for his research into synapses. Eccles was also known for his contributions to philosophy, writing on the mind-body problem and becoming an honorary member of the American Philosophical Society.Sir Peter J. Ratcliffe held the Nuffield Professorship of Clinical Medicine between 2003 and 2006, and is still a supernumerary fellow at Magdalen. He shared the 2019 Nobel Prize in Physiology or Medicine for his work on the oxygen sensing of cells. Other former Nuffield Professors of Clinical Medicine include Sir David Weatherall, who founded the Weatherall Institute of Molecular Medicine in 1989, and Sir John Bell GBE, who is also an alumnus of the college. The current holder of the chair is Richard Cornall, who was appointed in 2019.Two Fellows of Magdalen have been awarded the Nobel Prize in Physics: Erwin Schrödinger in 1933, while he was a Fellow; and Anthony James Leggett KBE in 2003, who had been a Fellow from 1963 to 1967.Due to Magdalen's close relationship with Oxford's Botanic Garden and as the home of the Sherardian Chair of Botany, Magdalen has been associated with many accomplished botanists. Historic Sherardian Professors include John Sibthorp, in whose name the Sibthorpian Professorship of Rural Economy, later known as the Sibthorpian Professorship of Plant Sciences, was founded; and Charles Daubeny, who also held the Aldrichian Chair of Chemistry and founded the Daubeny laboratory. The Sherardian Chair has been held since 2009 by Liam Dolan, who studies the emergence of land plants.Likewise, many distinguished scientists have held Waynflete Professorships at Magdalen. These include the mathematician J. H. C. Whitehead, who held the Waynflete Professorship of Pure Mathematics between 1947 and 1960. During this time, he was also the president of the London Mathematical Society, which established the Whitehead and Senior Whitehead prizes in his honour. He is remembered for his fundamental contributions to topology. The chair was held from 1984 until he retired in 2006 by Daniel Quillen, who received the Fields Medal for his work in algebraic K-theory. It is currently held by Ben Green.


== Gallery ==

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		


== References ==


== External links ==
Official site
Virtual Tour of Magdalen College
Website of Magdalen College Choir
Fletcher's History of Oxford: Magdalen College
A history of the choristers of Magdalen Chapel, Oxford
Website of Magdalen Middle Common Room