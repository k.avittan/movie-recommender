I Still Know What You Did Last Summer is a 1998 slasher film directed by Danny Cannon and written by Trey Callaway. Jennifer Love Hewitt, Freddie Prinze Jr. and Muse Watson reprise their roles, with Brandy, Mekhi Phifer, and Matthew Settle joining the cast. It is the second installment in the I Know What You Did Last Summer franchise. The film takes place one year after the events of the first film.
I Still Know What You Did Last Summer received negative reviews and grossed $84 million on a budget of $24 million. It was shot in Mexico and California. It is a sequel to I Know What You Did Last Summer (1997) and was followed by I'll Always Know What You Did Last Summer (2006).


== Plot ==
One year after brutal murders of her friends Helen Shivers and Barry Cox by the vengeful fisherman, Ben Willis, Julie James is attending summer classes in Boston. She suffers from memories and nightmares of the murders. Julie's roommate, Karla, receives a phone call from a local radio station and wins a vacation for four to the Bahamas when the radio host asks "what is the capital of Brazil?" and the girls answer "Rio de Janeiro" – despite the fact the answer is "Brasilia". Julie invites her boyfriend, Ray, who declines but later changes his mind. That evening, Ray and his co-worker, Dave, drive to Boston to surprise Julie but stop due to a body in the middle of the road. When Ray discovers the body is a mannequin, Ben appears and kills Dave with his hook. Ben then chases Ray in a truck, but Ray escapes and falls down a hill.
The next morning, Julie, Karla, Karla's boyfriend, Tyrell, and their friend, Will, depart for the trip. The group arrives at the hotel in Tower Bay and checks in. That evening at the hotel's bar, Julie is singing karaoke when the words "I Still Know What You Did Last Summer" roll onto the screen. Terrified, she runs back to her room. At the dock, Darick, the dockhand is tying up the boat.  He is attacked by the fisherman.  Later on Olga, the housekeeper is working and finds bloody sheets.  She is then attacked.  While the others get into the hot tub, Julie is in her room and notices that her toothbrush is missing and searches her room before finding Darick dead in the closet. She finds her friends and they return to find no sign of Darick's body and the hotel manager refuses to believe her story. By the pool we see Titus getting assaulted.  Meanwhile, Ray survived his injuries and heads out to rescue Julie.
The next day, the group finds Olga, Titus, and Mr. Brooks murdered and the two-way radio, their only way of contact, is destroyed. Isolated, the group goes to the room of Estes, the boat hand porter, and finds that he has been using voodoo against them. Estes appears, explaining he was trying to protect them after realizing their answer to the radio station's question was incorrect. He tells them Ben and his wife, Sarah, had two children: a son and a daughter. Ben murdered Sarah when he found out about an affair. Estes goes missing and Will volunteers to find him, while Ray takes a boat to the island. Julie, Karla, and Tyrell return to the hotel and find Nancy, the bartender, hiding in the kitchen.
Ben appears in the kitchen and kills Tyrell. The girls retreat to the attic, where Karla is attacked by Ben. Julie and Nancy rescue Karla and run to the storm cellar, where they find Ben's victims. Will bursts in and takes the girls back to the hotel, stating he saw Ben on the beach. At the hotel, Will tells them Estes attacked him and is bleeding from the stomach. Nancy and Karla leave to find a first aid kit but find Estes impaled with a harpoon. Ben appears, kills Nancy, and attacks Karla. While Julie tends to Will, he reveals it is not his blood and asks Julie what her favorite radio station is, revealing he was the radio host and killed Estes.
Will drags Julie to a graveyard and reveals he is Ben's son. Ben appears and attacks Julie before Ray arrives and engages in a fight with Will. When Ben tries to stab Ray, he accidentally stabs Will instead. While Ben is distraught from killing his son, Julie shoots him dead. Back at the hotel, Karla is found alive and they are rescued by the coast guard.
Sometime later, Ray and Julie have married. Ray is brushing his teeth and the bathroom door is locked while he is occupied. Julie sits down on the bed and looks in the mirror, seeing Ben underneath. She screams as Ben pulls her under the bed.


== Cast ==


== Production ==
While the film is set in the Bahamas, it was actually shot at El Tecuan Marina Resort Costa Alegre in Jalisco, Mexico; Los Angeles, California; and Sony Pictures Studios in Culver City, California.


== Music ==
"Sugar Is Sweeter" (CJ Bolland; Danny Saber remix featuring Justin Warfield) – 4:57
"How Do I Deal" (Jennifer Love Hewitt) – 3:23
"Relax" (Deetah) – 3:51
"Hey Now Now" (Swirl 360) – 4:37
"Blue Monday" (Orgy) – 4:32
"Polite" (Bijou Phillips) – 4:25
"Try to Say Goodbye" (Jory Eve) – 3:35
"Testimony" (Grant Lee Buffalo) – 3:59
"(Do You) Wanna Ride" (Reel Tight) – 3:33
"Getting Scared" (Imogen Heap) – 4:51
"Górecki" (Lamb) – 6:22
"Julie's Theme" (John Frizzell) – 2:52The soundtrack was released on November 17, 1998 by Warner Bros. Records. On January 19, 1999, "How Do I Deal" was released a single, backed by Jory Eve's "Try to Say Goodbye." A music video for "How Do I Deal" was made available to music television networks.

The song "Eden" by Belgian rock/pop group Hooverphonic was also featured in the film, but did not appear on the final soundtrack. The song appeared early in the film, when Julie looked at the picture of Helen beside her bed.


== Reception ==


=== Box office ===
The sequel made a gross $16.5 million at 2,443 theaters during its opening weekend. Unlike the original, the sequel opened at #2 at the box office and dropped to #5 only a week later. At the end of its 15-week run, the film grossed $40,020,622 in the United States and $44 million internationally, bringing the total worldwide gross to just over $84 million against a $24 million budget.


=== Critical response ===
The film received highly negative reviews and has an approval rating of 7% and an average rating of 3.3/10 on Rotten Tomatoes out of 56 total reviews. The site's critical consensus reads, "Boring, predictable and bereft of thrills or chills, I Still Know What You Did Last Summer is exactly the kind of rehash that gives horror sequels a bad name." It also has a 21 score on Metacritic compared to 52 for the original. The highest review score on Metacritic was 60 which came from Variety who said, "Purists will find the pic's obviousness disappointing, but there's no question that the film delivers a sufficient shock quotient to satisfy its youthful target audience." The film, much like the original, has obtained a cult following. Audiences polled by CinemaScore gave the film an average grade of "B" on an A+-F scale. It also won the award for Worst Sequel at the 1998 Stinkers Bad Movie Awards.


== Other media ==


=== Reboot ===

On August 15, 2006, a straight-to-DVD reboot titled I'll Always Know What You Did Last Summer was released. The film is unrelated to the two previous films and features no returning cast members. It was originally proposed to continue where I Still Know What You Did Last Summer left off. Instead, the film features an unrelated plot with a brief mention of the first two films.


=== Books ===
In 1998, a paperback version of the screenplay for I Still Know What You Did Last Summer was published by Pocket Books.


== References ==


== External links ==

I Still Know What You Did Last Summer on IMDb
I Still Know What You Did Last Summer at AllMovie
I Still Know What You Did Last Summer at Box Office Mojo
I Still Know What You Did Last Summer at Rotten Tomatoes