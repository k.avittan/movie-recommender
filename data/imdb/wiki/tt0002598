Mighty Servant 2 was a 29,000-ton semi-submersible, heavy-lift ship operated by Dockwise. The ship drew worldwide attention in 1988 for transporting the mine-damaged USS Samuel B. Roberts from Dubai to Newport, Rhode Island. 
The ship was built in 1983 by Oshima Shipbuilding Co. Ltd. in Ōshima, Japan, for Dutch shipping firm Wijsmuller Transport, which merged in 1993 with Dock Express Shipping to become Breda-based offshore heavy lifting group Dockwise Shipping B.V. The vessel capsized in 1999 and was scrapped in 2000.


== Service ==
Mighty Servant 2 was capable of carrying the heaviest semi-submersible drilling units, harsh-environment deep-water jack-up rigs and large floating production tension-leg platforms, semi-submersibles and spars with drafts of up to 14 metres. Most of the ship's cargoes were oil platforms and related gear.  When loading its mammoth burdens, the ship took thousands of tons of water into ballast tanks, submerging its cargo deck underwater. The cargo would be floated into position, whereupon Mighty Servant 2 would slowly pump out its ballast, lifting the deck up to sailing position.


=== USS Samuel B. Roberts ===
Her most famous job was the July 1988 haulage of the U.S. Navy's USS Samuel B. Roberts from Dubai to Newport, Rhode Island after the guided missile frigate struck a mine in the Persian Gulf on 14 April 1988. The 125-meter frigate barely fit on Mighty Servant 2's cargo deck; holes had to be cut to accommodate the frigate's sonar dome and stabilizing fins. The loading, which was supervised by divers and underwater video monitors, took about 12 hours, with gaps measured in mere centimetres. About 20 of the frigate's 200-man crew remained aboard during the 8,100-mile, 30-day voyage. The USS Fahrion escorted the USS Samuel B. Roberts from Dubai through the Straits of Hormuz.  The job received worldwide media coverage.


== Sister ships ==
Mighty Servant 2 had two sister ships, Mighty Servant 1, built in 1983, and Mighty Servant 3, built in 1984. Both were built for Wijsmuller Transport.
On 6 December 2006, Mighty Servant 3 sank in 62 metres (203 ft) of water near the port of Luanda, Angola, while offloading the drilling platform Aleutian Key. There were no casualties.  In August 2009, Dockwise Shipping reported it had been returned to service.


== Incidents and accidents ==


=== 1999 capsizing ===
Mighty Servant 2 capsized and sank on 2 November 1999 near the Indonesian island of Singkep (0.48°S 104.2°E﻿ / -0.48; 104.2). The ship was en route from Singapore to Cabinda, Angola, bearing the North Nemba Topsides 8,790-ton offshore production module. The vessel tipped onto its side in 35 metres (115 ft) of water in reportedly calm seas. A hydrographic survey of the area later discovered an unmarked single isolated pinnacle of granite directly on the ship's course. Five crew members died, including two Dutch and two Filipino men. Mighty Servant 2 was declared a total loss. In 2000, the wreck was renamed T 2, transported to Alang, India, and scrapped.


== See also ==
MV Blue Marlin.


== References ==


== External links ==
Photos of Mighty Servant 2 hauling USS Samuel B. Roberts (FFG 58)
Photos of the wreck of Mighty Servant 2
Model of sister ship, Mighty Servant 1
Page about sister ship, Mighty Servant 3
Dockwise Shipping B.V.