The Man Who Saw Tomorrow is a 1981 documentary-style movie about the predictions of French astrologer and physician Michel de Notredame (Nostradamus).
The Man Who Saw Tomorrow is narrated (some might say "hosted") by Orson Welles. The film depicts many of Nostradamus' predictions as evidence of Nostradamus' predicting ability, though as with other works, nothing is offered which conclusively proves his accuracy.  The last quarter of the film discusses Nostradamus' supposed prediction for the then future of the 1980s, 1990s and beyond. There are no scientifically testable predictions directly included in this film, only suggestions and allusions.
The film was produced and directed by Paul Drane for Australian Seven television network.  It was hosted by actor John Waters.  It set a ratings record, leading to a repeat broadcast two weeks later.


== Welles' view ==
Welles, though he agreed to host the film, was not a believer in the subject matter presented.  Welles' main objection to the generally accepted translations of Nostradamus' quatrains (so called because Nostradamus organized all his works into a series of four lined prose, which were then collected into "centuries", or groups of 100 such works) relates in part to the translation efforts.  While many skilled linguists have worked on the problem of translating the works of Nostradamus, all have struggled with the format the author used.
Nostradamus lived and wrote during a period of political and religious censorship.  Because of this it is said he disguised his writings not only with somewhat cryptic language, but in four different languages (Latin, French, Italian and Greek).  Not content with such obfuscation, Nostradamus is also said to have used anagrams to further confuse potential inquisitors (particularly with respect to names and places).
Welles himself completely rejected the central theme of the film after having made it.  It is not known if Welles was contractually obligated to narrate the film, or if he simply grew disenchanted with its subject matter and presentation after completing it.  Perhaps Welles' most public criticism of the subject matter of the film occurred during a guest appearance on an early 1980s episode of The Merv Griffin Show; "One might as well make predictions based on random passages from the phone book", he offered when asked about the film, before moving on to discuss other projects more interesting to him personally.


== Alleged predictions of Nostradamus in The Man Who Saw Tomorrow ==
The accidental death of King Henry II of France (1559).
The French Revolution (1789–1799).
The rise and fall of Napoleon Bonaparte (1799–1815).
The American Revolution (1775–1783).
The assassination of U.S. President Abraham Lincoln (1865).
The rise and fall of Adolf Hitler (1933–1945).
World War II (1939–1945).
The Holocaust.
The atomic bombings of Hiroshima and Nagasaki (1945).
A conspiracy to assassinate U.S. President John F. Kennedy (1963).
The assassination of Robert F. Kennedy (1968).
Islamic Revolution of Iran (1979).
The 1980s could be the time for Ted Kennedy to run to the U.S. presidency, after the Chappaquiddick incident (1969).  Eventually, Kennedy ran for the Presidency in 1980, when he tried to defeat incumbent President Jimmy Carter in the Democratic Party primaries.
Inventions and technological advances.
The end of the rivalry between the Soviet Union and the United States ("One day, the two great masters would be friends...The eastern ruler would be vanquished" but which could just as easily have referred to the Holy Roman Empire and the Ottoman Empire).
A major earthquake striking Los Angeles in May 1988.
A "King of Terror", wearing a blue turban, described as "the terror of mankind", would rise to power from Greater Arabia during the late 1990s, wage war around the world and spread the influence of Islamic fundamentalism, along with the decreasing influence of Christianity. Nostradamus claims that the "King of Terror" would form an alliance with Russia. According to Nostradamus, the "King of Terror" and Russia would wage World War III against the West (United States, United Kingdom and France), starting with a nuclear strike on New York City ("the sky will burn at 45 degrees, fire approaches the great new city"). Nostradamus claims that World War III would last about 27 years, and the war would destroy cities and kill millions.  The "King of Terror" would eventually be defeated himself, after the two farthest northern neighbors US (Eagle) and Russia (Bear) form an alliance and destroy "King of Terror".
After World War III, there will be a "peace of a thousand years".


== 1991 remake ==
On February 20, 1991, during the Gulf War, NBC aired a remake of this film, hosted and narrated by Charlton Heston. Much of the same footage, voice acting, and musical score was retained from the original movie. Much of Heston's narration constituted a verbatim reprise of Welles' original presentation. There were, however, some significant differences between the 1991 remake and the original film:

Much of the religious content was omitted.  The three Antichrists became three "great despotic tyrants", and the religious dimensions of the third tyrant's war were eliminated.
The remake was shortened to one hour, leading to the omission of all references to the Robert F. Kennedy assassination, Francisco Franco, the Edward VIII abdication crisis, Jeanne Dixon, and to allegations surrounding the disinterment of Nostradamus in 1791.  Discussion of the remaining predictions was abridged.
As Ted Kennedy had failed in his 1980 Presidential bid and did not run for the Presidency in 1984, or any time thereafter, references to his possible Presidency were eliminated.
The third tyrant's war, and the tragedies preceding that war, were made to seem less severe than in Welles' original documentary.
Saddam Hussein was implied to be the third tyrant.
The remake discussed the 1989 Loma Prieta earthquake, alleging that this earthquake may have been the one predicted in Welles' movie.
Discussion of Nostradamus' alleged predictions was rearranged, in that the remake included discussion of Napoleon with the other two tyrants, rather than with the French Revolution.
References to the end of the world were eliminated.The nature of the overall changes served the purpose of brevity, reducing an eighty-eight-minute film to a one-hour television broadcast, including commercial interruptions.  Moreover, the predictions relating to the third tyrant were adapted to serve U.S. propaganda purposes during the then-occurring Gulf War. The severity of the natural disasters preceding the tyrant's emergence was reduced, as a worldwide famine leading to cannibalism, and earthquakes and flooding in various European cities, had clearly not taken place.  Similarly, the severity of the third tyrant's war was diminished, omitting references to the conquest of Europe, nuclear war, and the destruction of New York City, as well as the prediction that the war would begin in 1994.  Significantly, to avoid offence to Muslims, negative references to Islam, and references to the religious nature of the tyrant (calling him an Antichrist and a "strong master of Mohammedan law") and his war were eliminated, as well as the portrayal of the then-current Gulf conflict as a religious war.
Although David L. Wolper was the executive producer of The Man Who Saw Tomorrow and arranged for the film to be re-edited into the 1991 television special, he was quoted as saying before the broadcast, "If you're asking me if I believe any of this ... the answer is a most definite 'no.'"


== Reception ==
Roger Ebert noted in March 1988 that Californians were renting the film based on its prediction of an earthquake in May, 2 months later. "Sales clerks at the busy 20/20 Video Store on La Cienega Boulevard told me the tape is renting like crazy, and the overnight fee has been raised to $6, reflecting the demand.  Spokesmen for Warner Bros Home Video confirm that "The Man Who Foretold the Future" has emerged as a surprise hit from their backlist."The San Bernardino Sun ran a piece that quoted, ""We have a couple of copies, and it's always gone.  It's a non-stop rental," said Keith Cramer, assistant manager at the Wherehouse on Highland Avenue in San Bernardino.  "Customers bringing it back say it's a pretty interesting movie. I've had a couple of people go so far as to say they're planning on it (the earthquake).  One person even joked they were planning their vacation so they'd be in Arizona then.""In April 1988, the Los Angeles Journal, referring to earthquake predictions, quoted David Wolper as joking "If the quake does happen, we'll sell a lot more copies, maybe enough to rebuild my house."Regarding the 1991 edit, Variety TV wrote "[T]he special makes a halfhearted effort to cram current events in the Persian Gulf into Nostradamus' prediction of a third great tyrant (after Napoleon and Hitler). While Saddam Hussein stands a fair chance of wreaking the kind of global havoc supposedly forecast according to the earlier film, this special ignores some of the more apocalyptic scenarios of that '81 pic to focus unconvincingly on events to date.  As is so often the case, Nostradamus' obscure quatrains are easily manipulated to suit the needs of the manipulators."


== References ==


== External links ==
The Man Who Saw Tomorrow on IMDb
AVClub, Orson Welles spouts authoritative nonsense in The Man Who Saw Tomorrow
The Prophecies of Nostradamus (1979)
The Man Who Saw Tomorrow (1981), VHS copy
The Man Who Saw Tomorrow (1991), NBC version