import csv
import wikipedia
from tqdm import tqdm
import os
batch = open("batch_1.txt",'r')
count = 0
threshold= 1000

with open('focused_movie_titles.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in tqdm(csv_reader):
        if line_count < threshold:
            # print(f'Column names are {", ".join(row)}')
            line_count += 1
        elif line_count >= threshold:
            # print(row[0],row[2])
            try:
                movie_name = row[2]
                file_to_write = open(os.path.join(os.getcwd(),"data","imdb","wiki",row[0]),'w')
                wk = wikipedia.page(movie_name)
                file_to_write.write(wk.content)
                file_to_write.close()
            except:
                file_to_write.close()
        

# for i in batch:
#     wk = wikipedia.page(i)
#     print(wk.title,count)
#     count+=1
    