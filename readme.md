![alt text](Movie_Recommender.png)
## Week 1 Submission
Group Name: Group 7<br>
Project Topic: Movie Recommender System<br>
GitHub Repo: https://gitlab.com/k.avittan/movie-recommender <br>

### Teammates:
|Name| Email-ID|
|--|--|
|Jagadeesh Sudhakaraiah| jsudhaka@uncc.edu|
|Keerthi Avittan Elamparithi| kelampar@uncc.edu|
|Sahith Chandra Ananthoju| sanantho@uncc.edu|
|Venkata Sai Nanda Kishore Khanderao| vkhander@uncc.edu|
|Sreekar Nedunuri| snedunur@uncc.edu|

<br><br>

## Week 3 Submission
